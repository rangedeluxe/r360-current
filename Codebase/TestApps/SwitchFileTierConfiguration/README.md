This script will toggle an R360 dev or QA environment between two different file-tier servers (an IIS server and a NoIIS server).

Usage:

* Copy the script to the app tier. `D:\WFSUtils` is a good spot.
* Edit the variables at the top of the file as appropriate for the environment.
* Run the script.

Your Windows user needs to be a member of the Administrators group on the DIT/FIT Client server (so the script can stop and start Windows services).

Operation:

* The script will read all the appropriate config files, and show you whether they indicate an IIS or NoIIS configuration.
* The script then prompts you to switch the configuration (by typing "IIS" or "NoIIS"), or to exit (by hitting Enter).
* If you choose to switch the configuration, the script will back up all the config files, update them, and restart all appropriate Windows services.
