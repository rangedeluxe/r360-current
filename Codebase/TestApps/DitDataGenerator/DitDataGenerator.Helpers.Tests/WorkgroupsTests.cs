﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.Helpers.Tests
{
    [TestClass]
    public class WorkgroupsTests
    {
        private Workgroups _workGroupList;

        public WorkgroupsTests()
        {
            _workGroupList = new Workgroups(900, 10, "Testing client",false);
        }

        [TestMethod]
        public void ValidateWorkgroupsCount()
        {
            Assert.AreEqual(10, _workGroupList.WorkgroupCount());
        }

        [TestMethod]
        public void ValidateWorkgroup()
        {
            Workgroup workgroup = _workGroupList.GetWorkgroup(0);
            Assert.AreEqual(900, workgroup.WorkgroupId, "Client Id");
            Assert.AreEqual("Testing client900", workgroup.WorkgroupName, "Client Name");
        }

        [TestMethod]
        public void ValidateRandomReturnsObject()
        {            
            Assert.IsNotNull(_workGroupList.GetRandomWorkgroup());
        }
    }
}
