﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wfs.Logging.Performance.Fakes;

namespace Wfs.Logging.Performance
{
    [TestClass]
    public class PerformanceLoggerTests
    {
        private IHighResolutionTimerProvider CreateFakeTimer(Func<TimeSpan> getNextTimerElapsed = null)
        {
            return new StubIHighResolutionTimerProvider
            {
                StartTimer = () => new StubIHighResolutionTimer
                {
                    ElapsedGet = () => getNextTimerElapsed?.Invoke() ?? TimeSpan.Zero
                }
            };
        }

        [TestMethod]
        public void OutputIsInitiallyEmpty()
        {
            var logger = new PerformanceLogger(CreateFakeTimer());

            Assert.AreEqual(0, logger.GetMeasuredActions().Count);
        }
        [TestMethod]
        public void AfterStartingTimer_NoElapsedTimeIsReturned()
        {
            var logger = new PerformanceLogger(CreateFakeTimer());
            logger.StartPerformanceTimer("Action 1");
            Assert.AreEqual(0, logger.GetMeasuredActions().Count);
        }
        [TestMethod]
        public void AfterStoppingTimer_ElapsedTimeIsRecorded()
        {
            var elapsed = TimeSpan.Zero;
            var logger = new PerformanceLogger(CreateFakeTimer(() => elapsed));
            var timer = logger.StartPerformanceTimer("Action 1");

            elapsed = TimeSpan.FromSeconds(1.5);
            timer.Stop();

            var measuredActions = logger.GetMeasuredActions();
            Assert.AreEqual(1, measuredActions.Count, "Count");
            Assert.AreEqual("Action 1", measuredActions[0].ActionName, "Name");
            Assert.AreEqual(1, measuredActions[0].Timings.Count, "Timing count");
            Assert.AreEqual(TimeSpan.FromSeconds(1.5), measuredActions[0].Timings[0], "Timing");
        }
        [TestMethod]
        public void ElapsedTimeIsCapturedFirstTimeTimerIsStopped()
        {
            var elapsed = TimeSpan.FromSeconds(1.5);
            var logger = new PerformanceLogger(CreateFakeTimer(() => elapsed));
            var timer = logger.StartPerformanceTimer("Action 1");

            timer.Stop();
            elapsed = TimeSpan.FromSeconds(2.5);
            timer.Stop();

            var measuredActions = logger.GetMeasuredActions();
            Assert.AreEqual(1, measuredActions.Count, "Count");
            Assert.AreEqual(1, measuredActions[0].Timings.Count, "Timing count");
            Assert.AreEqual(TimeSpan.FromSeconds(1.5), measuredActions[0].Timings[0], "Timing");
        }
        [TestMethod]
        public void MultipleMeasurementsForTheSameAction()
        {
            var logger = new PerformanceLogger(CreateFakeTimer());

            var timer1 = logger.StartPerformanceTimer("Action 1");
            timer1.Stop();
            var timer2 = logger.StartPerformanceTimer("Action 1");
            timer2.Stop();

            var measuredActions = logger.GetMeasuredActions();
            Assert.AreEqual(1, measuredActions.Count, "Action count");
            Assert.AreEqual("Action 1", measuredActions[0].ActionName, "Action name");
            Assert.AreEqual(2, measuredActions[0].Timings.Count, "Timing count");
        }
        [TestMethod]
        public void MultipleActions_AreReturnedInTheOrderTheyWereAdded()
        {
            const int count = 100;
            var logger = new PerformanceLogger(CreateFakeTimer());

            for (var i = 0; i < count; ++i)
            {
                var timer = logger.StartPerformanceTimer($"Action {i}");
                timer.Stop();
            }

            var measuredActions = logger.GetMeasuredActions();
            Assert.AreEqual(count, measuredActions.Count, "Action count");
            for (var i = 0; i < count; ++i)
                Assert.AreEqual($"Action {i}", measuredActions[i].ActionName);
        }
        [TestMethod]
        public void NestedCalls_AreShownInTheOrderTheyWereStarted_NotTheOrderTheyWereStopped()
        {
            var logger = new PerformanceLogger(CreateFakeTimer());
            var timer1 = logger.StartPerformanceTimer("Action 1");
            var timer2 = logger.StartPerformanceTimer("Action 2");
            timer2.Stop();
            timer1.Stop();

            var measuredActions = logger.GetMeasuredActions();
            Assert.AreEqual(2, measuredActions.Count);
            Assert.AreEqual("Action 1", measuredActions[0].ActionName, "First action");
            Assert.AreEqual("Action 2", measuredActions[1].ActionName, "Second action");
        }
    }
}