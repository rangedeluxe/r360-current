﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Wfs.Logging.Performance
{
    [TestClass]
    public class PerformanceLogReporterTests
    {
        [TestMethod]
        public void GivenEmptyList_ReturnsEmptyList()
        {
            var report = PerformanceLogReporter.GetReport(new PerformanceLoggerActionStatistics[] {});
            Assert.AreEqual(0, report.Count(), "Count");
        }
        [TestMethod]
        public void OneAction_WithNoTimings()
        {
            // Probably won't happen in actual practice, but...

            var statistics = new PerformanceLoggerActionStatistics("Action 1", new TimeSpan[] { });

            var report = PerformanceLogReporter.GetReport(new[] { statistics }).ToList();

            Assert.AreEqual(1, report.Count, "Count");
            Assert.AreEqual("Timings: Action 1: called 0x, median time 0:00:00, total time 0:00:00", report[0]);
        }
        [TestMethod]
        public void WithOneTiming()
        {
            var statistics = new PerformanceLoggerActionStatistics("Action 1", new[] {TimeSpan.FromSeconds(1.5)});

            var report = PerformanceLogReporter.GetReport(new[] {statistics}).ToList();

            Assert.AreEqual(1, report.Count, "Count");
            Assert.AreEqual("Timings: Action 1: called 1x, median time 0:00:01.5, total time 0:00:01.5", report[0]);
        }
        [TestMethod]
        public void WithAnOddNumberOfTimings()
        {
            var statistics = new PerformanceLoggerActionStatistics("Action 1", new[]
            {
                TimeSpan.FromSeconds(1.5),
                TimeSpan.FromSeconds(3.1),
                TimeSpan.FromSeconds(1.7),
                TimeSpan.FromSeconds(2.0),
                TimeSpan.FromSeconds(2.8),
            });

            var report = PerformanceLogReporter.GetReport(new[] { statistics }).ToList();

            Assert.AreEqual(1, report.Count, "Count");
            Assert.AreEqual("Timings: Action 1: called 5x, median time 0:00:02, total time 0:00:11.1", report[0]);
        }
        [TestMethod]
        public void WithAnOddNumberOfTimings_UsesTheLargerOfTheTwoCenterValuesAsTheMedian()
        {
            var statistics = new PerformanceLoggerActionStatistics("Action 1", new[]
            {
                TimeSpan.FromSeconds(1.5),
                TimeSpan.FromSeconds(1.7),
                TimeSpan.FromSeconds(2.0),
                TimeSpan.FromSeconds(2.1),
                TimeSpan.FromSeconds(2.8),
                TimeSpan.FromSeconds(3.1),
            });

            var report = PerformanceLogReporter.GetReport(new[] { statistics }).ToList();

            Assert.AreEqual(1, report.Count, "Count");
            Assert.AreEqual("Timings: Action 1: called 6x, median time 0:00:02.1, total time 0:00:13.2", report[0]);
        }
        [TestMethod]
        public void MultipleActions()
        {
            var statistics1 = new PerformanceLoggerActionStatistics("Action 1", new[] {TimeSpan.FromSeconds(1.5)});
            var statistics2 = new PerformanceLoggerActionStatistics("Action 2", new[] {TimeSpan.FromSeconds(9.5)});

            var report = PerformanceLogReporter.GetReport(new[] {statistics1, statistics2}).ToList();

            Assert.AreEqual(2, report.Count, "Count");
            Assert.AreEqual("Timings: Action 1: called 1x, median time 0:00:01.5, total time 0:00:01.5", report[0]);
            Assert.AreEqual("Timings: Action 2: called 1x, median time 0:00:09.5, total time 0:00:09.5", report[1]);
        }
    }
}