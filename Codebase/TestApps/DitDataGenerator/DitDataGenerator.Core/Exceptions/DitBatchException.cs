﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DitDataGenerator.Exceptions
{
    public class DitDataGeneratorException : Exception
    {
        public DitDataGeneratorException(string message) : base(message)
        {

        }

        public DitDataGeneratorException(string message, Exception e) : base(message, e)
        {

        }
    }
}
