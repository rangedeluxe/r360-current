﻿using System;

namespace DitDataGenerator.Exceptions
{
    public class BatchIdentifierLimitReachedException : DitDataGeneratorException
    {
        public BatchIdentifierLimitReachedException(string message) : base(message)
        {

        }

        public BatchIdentifierLimitReachedException(string message, Exception e) : base(message, e)
        {

        }
    }
}