﻿namespace DitDataGenerator.Services
{
    public interface ILogger
    {
        void LogEvent(string message);
        void LogWarning(string message);
        void LogError(string message);
    }
}
