﻿using System;
using System.IO;

namespace DitDataGenerator.Services
{
    public class TextWriterLogger : ILogger
    {
        private readonly TextWriter _writer;

        public TextWriterLogger(TextWriter writer)
        {
            _writer = writer;
        }

        public void LogEvent(string message)
        {
            _writer.WriteLine("INFO:" + message);
        }

        public void LogWarning(string message)
        {
            _writer.WriteLine("WARNING:" + message);
        }

        public void LogError(string message)
        {
            _writer.WriteLine("ERROR:" + message);
        }
    }
}
