﻿namespace DitDataGenerator.Batches
{
    public class ImageStatistics
    {        
        public int GrayImagesCount { get; set; }
        public int ColorImagesCount { get; set; }
        public int BitonalImagesCount { get; set; }
    }
}
