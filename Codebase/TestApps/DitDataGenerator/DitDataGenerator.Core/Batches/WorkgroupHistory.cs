﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using DitDataGenerator.Exceptions;
using DitDataGenerator.Helpers;

namespace DitDataGenerator.Batches
{
    public class WorkgroupHistory
    {
        private XDocument _ditDataGeneratorXmlDoc;
        private string _outputPath;
        public int NumberOfBatchFiles { get; set; }
        public int LastBatchIdOfPreviousRun { get; set; }
        public int LastBatchNumberOfPreviousRun { get; set; }
        public int MaxBatchId { get; set; }
        public int MaxBatchNumber { get; set; }
        public int LastBatchId { get; set; }
        public int LastBatchNumber { get; set; }


        public WorkgroupHistory(string outputDirectory, DateTime runDateTime, IWorkgroups workgroups,
            int numberOfBatchFiles)
        {
            NumberOfBatchFiles = numberOfBatchFiles;

            try
            {
                //Load history from xml doc
                LoadHistory(outputDirectory, runDateTime, workgroups);

                MaxBatchId = LastBatchIdOfPreviousRun + NumberOfBatchFiles;
                MaxBatchNumber = LastBatchNumberOfPreviousRun + NumberOfBatchFiles;
                UpdateElement("lastbatchid", (MaxBatchId).ToString());
                UpdateElement("lastbatchnumber", (MaxBatchNumber).ToString());

                //now save xml doc to the history xml file
                SaveHistory();
            }
            catch (Exception e)
            {
                throw new Exception(
                    $"Failed processing DitDataGenerator xml file {_outputPath} Error message: {e.Message}", e);
            }
        }

        public WorkgroupHistory()
        {
        }

        private void LoadHistory(string outputDirectory, DateTime runDateTime, IWorkgroups workgroups)
        {
            Directory.CreateDirectory(outputDirectory);
            _outputPath = outputDirectory
                          + @"\DitDataGenerator" + runDateTime.ToString("yyyyMMdd") + ".xml";

            if (File.Exists(_outputPath))
            {
                _ditDataGeneratorXmlDoc = XDocument.Load(_outputPath);
                IEnumerable<XElement> keys = _ditDataGeneratorXmlDoc.Descendants("keys");

                // Chek if keys element is missing.
                if (keys.Count() != 1)
                {
                    throw new Exception(
                        $"Number of keys elements in the xml should be one, however number of keys elements found are: '{keys.Count()}'");
                }

                LastBatchIdOfPreviousRun = LastBatchId =
                    Convert.ToInt32(keys.Descendants("lastbatchid").First().Value);
                LastBatchNumberOfPreviousRun = LastBatchNumber =
                    Convert.ToInt32(keys.Descendants("lastbatchnumber").First().Value);
            }
            else
            {
                InitializeDoc(workgroups);
            }
        }

        public int GetNewBatchId()
        {
            if (++LastBatchId > MaxBatchId)
            {
                throw new BatchIdentifierLimitReachedException("Number of batches exceeded Number of BatchFiles requested.");
            }

            return LastBatchId;
        }

        public int GetNewBatchNumber()
        {
            if (++LastBatchNumber > MaxBatchNumber)
            {
                throw new BatchIdentifierLimitReachedException("Number of batches exceeded Number of BatchFiles requested.");
            }

            return LastBatchNumber;
        }

        private void InitializeDoc(IWorkgroups workgroups)
        {
            RandomData random = new RandomData();
            _ditDataGeneratorXmlDoc = new XDocument(new XElement("history"));
            var workgroupsNode = new XElement("workgroups");


            // Add starting values for batchid and batch#
            var keysNode = new XElement("keys",
                new XElement("lastbatchid", NumberOfBatchFiles),
                new XElement("lastbatchnumber", NumberOfBatchFiles)
            );

            //Add workgroups details
            for (int i = 0; i < workgroups.WorkgroupCount(); i++)
            {
                var workgroup = workgroups.GetWorkgroup(i);
                var workgroupDetails = new XElement("workgroup",
                    new XAttribute("workgroupid", workgroup.WorkgroupId),
                    new XAttribute("workgroupname", workgroup.WorkgroupName),
                    new XAttribute("dda", random.RandomNumberString(14)),
                    new XAttribute("aba", random.RandomNumberString(9))
                );

                workgroupsNode.Add(workgroupDetails);
            }

            _ditDataGeneratorXmlDoc.Root?.Add(keysNode);
            _ditDataGeneratorXmlDoc.Root?.Add(workgroupsNode);

        }

        public string GetClientABA(int workgroup)
        {
            XElement result = _ditDataGeneratorXmlDoc.Descendants("workgroup")
                .FirstOrDefault(e1 => e1.Attribute("workgroupid")?.Value == workgroup.ToString());

            if (result != null)
            {
                return result.Attribute("aba")?.Value;
            }
            else
            {
                return "ABA not found";
            }
        }

        public string GetClientDDA(int workgroup)
        {
            XElement result = _ditDataGeneratorXmlDoc.Descendants("workgroup")
                .FirstOrDefault(e1 => e1.Attribute("workgroupid")?.Value == workgroup.ToString());

            if (result != null)
            {
                return result.Attribute("dda")?.Value;
            }
            else
            {
                return "DDA not found";
            }
        }

        public void SaveHistory()
        {
            _ditDataGeneratorXmlDoc.Save(_outputPath);
        }

        private void UpdateElement(string xmlElementName, string xmlElementValue)
        {
            //point to the element
            XElement result = _ditDataGeneratorXmlDoc.Descendants(xmlElementName).FirstOrDefault();

            // now update element value
            if (result != null)
            {
                result.Value = xmlElementValue;
            }
            else
            {
                throw new Exception($"element{xmlElementName} is missing in xml file: '{_ditDataGeneratorXmlDoc}'");
            }
        }
    }
}
