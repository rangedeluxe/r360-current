namespace DitDataGenerator.Batches
{
    public interface IRandomPercentageGenerator
    {
        int GetRandomPercent();
    }
}