﻿using System.Collections.Generic;

namespace DitDataGenerator.Batches
{
    public class DataEntryField
    {
        private readonly string _fieldName;
        private readonly string _value;
        public DataEntryField(string fieldName, string value)
        {
            _fieldName = fieldName;
            _value = value;
        }

        public string GetDataEntryField()
        {
            return $"{_fieldName}:{_value}";
        }
    }
}