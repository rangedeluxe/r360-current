using System;

namespace DitDataGenerator.Batches
{
    public class RandomPercentageGenerator : IRandomPercentageGenerator
    {
        private static readonly Random Random = new Random();
        public int GetRandomPercent()
        {
            //returns random integers that range from 0 to 99.
            return Random.Next(0, 100);
        }
    }
}