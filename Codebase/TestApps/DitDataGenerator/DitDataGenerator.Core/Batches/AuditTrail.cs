﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DitDataGenerator.Helpers;

namespace DitDataGenerator.Batches
{
    public class AuditTrail
    {
        private RandomData _randomData = new RandomData();
        private Dictionary<string, string> _auditTrail;

        public AuditTrail(string ABA, string DDA, string documentType)
        {
            _auditTrail = new Dictionary<string, string>()
            {
                {"Document ID", _randomData.RandomNumberString(2)},
                {"Applied OpID", _randomData.RandomString(32)},
                {"P2 Sequence", _randomData.RandomNumberString(1) },
                {"Pocket Cut ID", _randomData.RandomString(5)},
                {"P2 Pocket Seq Num", _randomData.RandomNumberString(1)},
                {"Audit Trail", _randomData.RandomString(64)},
                {"ARC Reason", _randomData.RandomString(15)},
                {"ARC Tracer", _randomData.RandomString(32)},
                {"Reject Job", _randomData.RandomString(25)},
                {"Reject Reason", _randomData.RandomString(32)},
                {"Orig Batch ID", _randomData.RandomNumberString(4)},
                {"Orig P1 Seq Num", _randomData.RandomNumberString(1)},
                {"ACH Returns Audit Trail", _randomData.RandomString(32)},
                {"Document Type",  documentType},
                {"Micr Mismatch", _randomData.RandomString(32)},
                {"Endpoint Name", _randomData.RandomString(32)},
                {"Endpoint ABA", ABA},
                {"Endpoint DDA", DDA},
                {"Item Identifier",  _randomData.RandomNumberString(22)},
                {"Transaction Identifier",  _randomData.RandomNumberString(22)},
                {"IMS Exceptions ID",  _randomData.RandomString(10)}
            };
        }

        public IEnumerable<string> GetAuditTrail()
        {
            return _auditTrail.OrderBy(pair => pair.Key, StringComparer.InvariantCultureIgnoreCase).Select(kvp => $"{kvp.Key}:{kvp.Value}").ToArray();
        }
    }
}
