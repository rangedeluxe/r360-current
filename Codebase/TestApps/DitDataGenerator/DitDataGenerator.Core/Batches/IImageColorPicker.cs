﻿using System.Collections.Generic;
using DitDataGenerator.Images;

namespace DitDataGenerator.Batches
{
    public interface IImageColorPicker
    {
        IReadOnlyList<ColourEncoding> GetNextImageColorEncodings();
    }
}
