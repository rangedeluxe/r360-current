﻿using System;
using System.Collections.Generic;

namespace DitDataGenerator.Batches.Structures
{
    public struct DatFileData
    {
        public DateTime BatchDate;
        public int WorkgroupId;
        public string WorkgroupName;
        public int SiteId;
        public int BatchId;
        public int BatchNumber;
        public string DDA;
        public string ABA;
        public string BatchImageDirectory;
        //the following will change as the file is built
        public int TransactionNumber;
        public int TransactionSequence;
        public int Amount;
        public string AccountNumber;
        
        public List<string> FileContent;

    }
}
