﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace DitDataGenerator.Batches
{
    public class Trailer
    {
        private readonly Dictionary<string, string> _trailerData;
        public Trailer(int docRevNumber, int itemPageNum, int fileTypeNum, int imageType, int compress, int xdpi, int ydpi, [CanBeNull] Dictionary<string, string> files)
        {
            _trailerData = new Dictionary<string, string>()
            {
                {">>DocRevNum", docRevNumber.ToString()},
                {">>ItemPageNum", itemPageNum.ToString()},
                {">>FileTypeNum", fileTypeNum.ToString()},
                {">>ImageType", imageType.ToString()},
                {">>Compress", compress.ToString()},
                {">>Xdpi", xdpi.ToString()},
                {">>Ydpi", ydpi.ToString()},
            };

            files?.Where(e => !string.IsNullOrWhiteSpace(e.Value)).ToList()
                .ForEach(file => _trailerData.Add(file.Key, file.Value));
        }

        public IEnumerable<string> GetTrailer()
        {
            //Filter out null and empty valeus Where(keyVal=> !string.IsNullOrWhiteSpace( keyVal.Value) )
            return _trailerData.Where(keyVal => !string.IsNullOrWhiteSpace(keyVal.Value))
                .OrderBy(pair => pair.Key, StringComparer.InvariantCultureIgnoreCase)
                .Select(kvp => $"{kvp.Key}:{kvp.Value}").ToArray();
        }
    }
}
