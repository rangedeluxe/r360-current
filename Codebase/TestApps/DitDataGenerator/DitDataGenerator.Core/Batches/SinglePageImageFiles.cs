namespace DitDataGenerator.Batches
{
    public class SinglePageImageFiles
    {
        public string ColorFrontFile { get; set; }
        public string GrayFrontFile { get; set; }
        public string BitonalFrontFile { get; set; }
        public string ColorBackFile { get; set; }
        public string GrayBackFile { get; set; }
        public string BitonalBackFile { get; set; }

    }
}