﻿namespace DitDataGenerator.Batches
{
    public class ImageTypePercentages
    {
        public int PercentAllTypesImages { get; set; }
        public int PercentBitonalAndGrayImages { get; set; }
        public int PercentBitonalAndColorImages { get; set; }
        public int PercentBitonalImages { get; set; }
    }
}
