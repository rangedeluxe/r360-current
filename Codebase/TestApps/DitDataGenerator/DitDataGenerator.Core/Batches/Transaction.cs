﻿using System.Collections.Generic;

namespace DitDataGenerator.Batches
{
    public class Transaction
    {
        public int TransactionNumber { get; set; }
        public int SequenceNumber { get; set; }

        public IList<Payment> Payments { get; set; } = new List<Payment>();
        public IList<Stub>Stubs { get; set; } = new List<Stub>();

        public IEnumerable<string> GetTransaction()
        {
            return new string[] { $"Transaction Number:{TransactionNumber}", $"Sequence Number:{SequenceNumber}" }; ;
        }
    }
}
