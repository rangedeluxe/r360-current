﻿using System;
using System.Collections.Generic;
using System.Linq;
using DitDataGenerator.Batches.Structures;
using DitDataGenerator.Helpers;
using DitDataGenerator.Images;
using DitDataGenerator.Metadata;
using ImageGenerators.Interfaces;
using Wfs.Logging.Performance;
using WFS.integraPAY.Online.ICONCommon;

namespace DitDataGenerator.Batches
{
    public class DatGenerator
    {
        private string _imageOutputDirectory;
        private int _itemCountPerBatch;
        private int _numberBatchFiles;
        private int _siteCode;
        private readonly Fields _fields;
        private IFileBuilder _fileBuilder;
        private DateTime _runDateTime;
        private IWorkgroups _workgroups;
        private WorkgroupHistory _workgroupHistory;
        private RandomData _random;
        private DatFileData _dat;
        private IImageColorPicker _imageColorPicker;

        private readonly IPerformanceLogger _performanceLogger;
        private const string HeaderDocumentTypeCheck = "Per/Bus Check";
        private const string HeaderDocumentTypeStub = "Stub";
        private const string DocumentTypeCheck = "Check";
        private const string DocumentTypeStub = "Stub";
        private TiffImageFilesOption _tiffImageFilesSetting;
        private readonly ImageStatistics _imageStatistics = new ImageStatistics();

        public DatGenerator(IFileBuilder fileBuilder,
            string imageOutputDirectory, int itemCountPerBatch, int numberBatchFiles,
            int workgroupIdPrefix, string workgroupNamePrefix, int siteCode,
            int workGroupCount,
            IPerformanceLogger performanceLogger, Fields fields, TiffImageFilesOption tiffImageFilesSetting, IImageColorPicker percentageImageColorPicker,
            bool singleWorkgroup)
        {
            _itemCountPerBatch = itemCountPerBatch;
            _fileBuilder = fileBuilder;
            _imageOutputDirectory = imageOutputDirectory;
            _numberBatchFiles = numberBatchFiles;
            _siteCode = siteCode;
            _performanceLogger = performanceLogger;
            _fields = fields;
            _tiffImageFilesSetting = tiffImageFilesSetting;
            _imageColorPicker = percentageImageColorPicker;

            _random = new RandomData();
            _workgroups = new Workgroups(workgroupIdPrefix, workGroupCount, workgroupNamePrefix,singleWorkgroup);
            _runDateTime = DateTime.Now;

            _workgroupHistory = new WorkgroupHistory(_imageOutputDirectory, _runDateTime, _workgroups, _numberBatchFiles);

            //Because we are generating single Image file for all Transactions we need three LazyImageGenerators.
            //If we want to generate separate  Image file for each Transaction then we weed only one ImageGenerators instance.
            BitonalImageGenerator = new LazyImageGenerator(new ImageGenerator(_random, performanceLogger));
            GrayImageGenerator = new LazyImageGenerator(new ImageGenerator(_random, performanceLogger));
            ColorImageGenerator = new LazyImageGenerator(new ImageGenerator(_random, performanceLogger));
        }

        public DatFileData DatFileData => _dat;
        public IImageGenerator BitonalImageGenerator { get; set; }
        public IImageGenerator GrayImageGenerator { get; set; }
        public IImageGenerator ColorImageGenerator { get; set; }

        private void BuildBatchFile()
        {
            using (_performanceLogger.StartPerformanceTimer("Build batch file"))
            {
                //set for new workgroup
                Workgroup workgroup = _workgroups.GetRandomWorkgroup();

                _dat = new DatFileData
                {
                    WorkgroupId = workgroup.WorkgroupId,
                    BatchDate = _runDateTime,
                    WorkgroupName = workgroup.WorkgroupName,
                    SiteId = _siteCode,
                    BatchId = _workgroupHistory.GetNewBatchId(),
                    BatchNumber = _workgroupHistory.GetNewBatchNumber(),
                    ABA = _workgroupHistory.GetClientABA(workgroup.WorkgroupId),
                    DDA = _workgroupHistory.GetClientDDA(workgroup.WorkgroupId)
                };
                //make sure the image directory for the batch exists
                _dat.BatchImageDirectory = _imageOutputDirectory + "\\00" + DatFileData.WorkgroupId + DatFileData.BatchId;

                _dat.FileContent = new List<string>();
                DatFileData.FileContent.Add(">>>>Self Configuring Tagged DIP <<<<");
                BuildTheBatchFile();
            }
        }
        public void BuildAllBatchFiles()
        {
            for (int i = 0; i < _numberBatchFiles; i++)
                BuildBatchFile();
        }

        private void BuildTheBatchFile()
        {
            _dat.TransactionNumber = 0;
            _dat.TransactionSequence = 0;
            // the following code will change
            //this will be changing as more pieces of the dat file are available
            int loopCountSingle = Convert.ToInt32(_itemCountPerBatch * .75);
            int loopCountMultiple = _itemCountPerBatch - loopCountSingle;
            //output single items
            for (int i = 0; i < loopCountSingle; i = i + 2)
            {
                _dat.TransactionNumber++;
                BuildCheck();
                BuildStub();
            }
            //output multiple items
            for (int i = 0; i < loopCountMultiple; i = i + 5)
            {
                _dat.TransactionNumber++;
                BuildCheck();
                BuildCheck();
                BuildCheck();
                BuildStub();
                BuildStub();
            }

            DatFileData.FileContent.Add("END:");
            // the file content is now in memory
            //  go output the file
            _fileBuilder.WriteFile(DatFileData.FileContent, GenerateBatchFileName());
        }

        private int _nameSeed;
        public string GenerateBatchFileName()
        {
            _nameSeed++;
            return "BatchFile_" + DatFileData.WorkgroupId + "_" + DatFileData.BatchId + "_" + _nameSeed + ".dat";
        }

        private void BuildCheck()
        {
            using (_performanceLogger.StartPerformanceTimer("Generate a payment"))
                BuildCheckInternal();
        }
        private void BuildCheckInternal()
        {
            GenerateHeader(HeaderDocumentTypeCheck);
            GenerateBatch();
            GenerateTransaction();
            GeneratePayment();
            GenerateAuditTrail(DocumentTypeCheck);
            GenerateTrailer(DocumentTypeCheck);
        }
        private void BuildStub()
        {
            using (_performanceLogger.StartPerformanceTimer("Generate a stub"))
                BuildStubInternal();
        }
        private void BuildStubInternal()
        {
            GenerateHeader(HeaderDocumentTypeStub);
            GenerateBatch();
            GenerateTransaction();
            GenerateStub();
            GenerateAuditTrail(DocumentTypeStub);
            GenerateTrailer(HeaderDocumentTypeStub);
        }
        public void GenerateAuditTrail(string documentType)
        {
            // add the audit trail
            AuditTrail auditTrail = new AuditTrail(DatFileData.ABA, DatFileData.DDA, documentType);
            AddToFile(auditTrail.GetAuditTrail());
        }

        public void GenerateBatch()
        {
            Batch theBatch = new Batch(DatFileData.BatchDate,
                DatFileData.BatchDate,
                DatFileData.BatchDate,
                DatFileData.BatchId,
                DatFileData.BatchNumber,
                DatFileData.WorkgroupId,
                DatFileData.WorkgroupName,
                DatFileData.SiteId);
            AddToFile(theBatch.GetBatch());
        }

        public void GenerateHeader(string name)
        {
            //add the header chunk
            Header theHeader = new Header(DateTime.Now, DatFileData.WorkgroupId, name);
            AddToFile(theHeader.GetHeader());
        }

        public void GeneratePayment()
        {
            //add the payment
            _dat.Amount = _random.RandomNumberLimited(1, 5000);
            _dat.AccountNumber = _random.RandomNumberString(10);
            Payment thePayment = new Payment()
            {
                AccountNumber = DatFileData.AccountNumber,
                AppliedAmount = DatFileData.Amount,
                RT = _random.RandomNumberString(8),
                SerialNumber = _random.RandomNumberString(10)
            };
            AddToFile(thePayment.GetPayment());
        }

        public void GenerateStub()
        {
            Stub stub = new Stub()
            {
                AppliedAmount = _random.RandomNumberLimited(1, 5000),
            };
            GenerateStubDataEntry(stub);
            AddToFile(stub.GetStub());
            AddToFile(stub.GetDataEntryFields());
        }


        public void GenerateStubDataEntry(Stub stub)
        {
            foreach (var stubDataEntryField in _fields.OptionalStubDataEntryFields)
            {
                stub.DataEntryFields.Add(new DataEntryField(stubDataEntryField.KeyType, _random.RandomString(_random.RandomNumberLimited(10, stubDataEntryField.Length))));
            }
        }

        public void GenerateTrailer(string doctype)
        {
            Trailer trailer;
            switch (_tiffImageFilesSetting)
            {
                case TiffImageFilesOption.SinglePage:
                    trailer = GenerateTrailerWithSinglePageImages(doctype);
                    break;
                case TiffImageFilesOption.NoImages:
                    trailer = new Trailer(0, 0, 2, 0, 0, 300, 300, null);
                    break;
                default: // TiffImageFilesOptions.MultiPage
                    trailer = GenerateTrailerWithMultiPageImage(doctype);
                    break;
            }

            //now output the trailer
            AddToFile(trailer.GetTrailer());
        }

        private Trailer GenerateTrailerWithMultiPageImage(string doctype)
        {
            string imageFileName;
            if (doctype == DocumentTypeCheck)
            {
                var checkDetails = BitonalImageGenerator.BuildCheckImage(GetImageParameters(ColourEncoding.None));
                imageFileName = checkDetails.FrontImageFileName;
            }
            else
            {
                var invoiceDetails = BitonalImageGenerator.BuildInvoiceImage(GetImageParameters(ColourEncoding.None));
                imageFileName = invoiceDetails.FrontImageFileName;
            }
            _imageStatistics.BitonalImagesCount++;
            return new Trailer(0, 0, 2, 0, 0, 300, 300, GetMultiPageFile(imageFileName));
        }
        private IImageFileDetails GenerateSinglePageImage(string doctype, ColourEncoding colourEncoding)
        {
            IImageFileDetails singleImageFiles;
            IImageGenerator imageGenerator = GetImageGenerator(colourEncoding);
            if (doctype == DocumentTypeCheck)
            {
                singleImageFiles = imageGenerator.BuildCheckImage(GetImageParameters(colourEncoding), true, false);
            }
            else
            {
                singleImageFiles = imageGenerator.BuildInvoiceImage(GetImageParameters(colourEncoding), true, false);
            }
            return singleImageFiles;
        }

        private IImageGenerator GetImageGenerator(ColourEncoding colourEncoding)
        {
            switch (colourEncoding)
            {
                case ColourEncoding.Bitonal:
                    return BitonalImageGenerator;
                case ColourEncoding.Color:
                    return ColorImageGenerator;
                case ColourEncoding.GrayScale:
                    return GrayImageGenerator;
                default:
                    return BitonalImageGenerator;
            }
        }

        private Trailer GenerateTrailerWithSinglePageImages(string doctype)
        {
            var imageColors = _imageColorPicker.GetNextImageColorEncodings();

            SinglePageImageFiles singlePageImageFiles = new SinglePageImageFiles();

            if (imageColors.Contains(ColourEncoding.Color))
            {
                var colorImage = GenerateSinglePageImage(doctype, ColourEncoding.Color);
                singlePageImageFiles.ColorFrontFile = colorImage.FrontImageFileName;
                singlePageImageFiles.ColorBackFile = colorImage.BackImageFileName;
                _imageStatistics.ColorImagesCount++;
            }

            if (imageColors.Contains(ColourEncoding.GrayScale))
            {
                var grayImage = GenerateSinglePageImage(doctype, ColourEncoding.GrayScale);
                singlePageImageFiles.GrayFrontFile = grayImage.FrontImageFileName;
                singlePageImageFiles.GrayBackFile = grayImage.BackImageFileName;
                _imageStatistics.GrayImagesCount++;
            }

            if (imageColors.Contains(ColourEncoding.Bitonal))
            {
                var bitonalImage = GenerateSinglePageImage(doctype, ColourEncoding.Bitonal);
                singlePageImageFiles.BitonalFrontFile = bitonalImage.FrontImageFileName;
                singlePageImageFiles.BitonalBackFile = bitonalImage.BackImageFileName;
                _imageStatistics.BitonalImagesCount++;
            }

            return new Trailer(0, 0, 2, 0, 0, 300, 300, GetSinglePageFiles(singlePageImageFiles));
        }

        private Dictionary<string, string> GetMultiPageFile(string imageFileName)
        {
            return new Dictionary<string, string>()
            {
                {cMagicKeywords.AAFullPath, imageFileName}
            };
        }
        private static Dictionary<string, string> GetSinglePageFiles(SinglePageImageFiles singlePageImageFiles)
        {
            return new Dictionary<string, string>()
                    {
                        {cMagicKeywords.AABitonalBackPath, singlePageImageFiles?.BitonalBackFile},
                        {cMagicKeywords.AABitonalFrontPath, singlePageImageFiles?.BitonalFrontFile},
                        {cMagicKeywords.AAColorBackPath, singlePageImageFiles?.ColorBackFile},
                        {cMagicKeywords.AAColorFrontPath, singlePageImageFiles?.ColorFrontFile},
                        {cMagicKeywords.AAGrayBackPath, singlePageImageFiles?.GrayBackFile},
                        {cMagicKeywords.AAGrayFrontPath, singlePageImageFiles?.GrayFrontFile}
                    };
        }

        public void GenerateTransaction()
        {
            _dat.TransactionSequence++;
            // add the transaction
            Transaction theTransaction = new Transaction()
            {
                SequenceNumber = DatFileData.TransactionSequence,
                TransactionNumber = DatFileData.TransactionNumber
            };
            AddToFile(theTransaction.GetTransaction());
        }
        private ImageParameters GetImageParameters(ColourEncoding colourEncoding)
        {
            return new ImageParameters(DatFileData.BatchImageDirectory, DatFileData.BatchId, DatFileData.TransactionSequence,
                DatFileData.AccountNumber, DatFileData.Amount, colourEncoding);
        }
        private void AddToFile(IEnumerable<string> theRecords)
        {
            DatFileData.FileContent.AddRange(theRecords);
        }

        public ImageStatistics GetImageStatistics()
        {
            return _imageStatistics;
        }
    }
}
