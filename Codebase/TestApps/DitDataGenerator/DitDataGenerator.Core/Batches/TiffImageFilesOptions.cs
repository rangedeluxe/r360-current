namespace DitDataGenerator.Batches
{
    public enum TiffImageFilesOption
    {
        MultiPage,
        SinglePage,
        NoImages
    }
}