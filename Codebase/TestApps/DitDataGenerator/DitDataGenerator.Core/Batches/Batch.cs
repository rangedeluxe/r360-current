﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DitDataGenerator.Batches
{
    public class Batch
    {
        private readonly DateTime _batchDate;
        private readonly DateTime _consolidationDate;
        private readonly DateTime _depositDate;
        private readonly DateTime _processingDate;
        private readonly DateTime _receiveDate;
        private readonly DateTime _systemDate;
        private readonly int _batchId;
        private readonly int _batchNumber;
        private readonly int _clientId;
        private readonly string _clientName;
        private readonly int _siteId;

        private readonly Dictionary<string, string> _batchData;

        public DateTime BatchDate => _batchDate;
        public int ClientId => _clientId;
        public Dictionary<string, string> BatchData => _batchData;

        public IList<Transaction> Transactions { get; set; } = new List<Transaction>();

        public Batch(DateTime batchDate, DateTime depositDate, DateTime systemDate, int batchId, int batchNumber, int clientId, string clientName, int siteId)
        {
            _batchDate = batchDate;
            _depositDate = depositDate;
            //Set all the other dates to the system date
            _systemDate = _processingDate = _receiveDate = _consolidationDate = systemDate;
            _batchId = batchId;
            _batchNumber = batchNumber;
            _clientId = clientId;
            _clientName = clientName;
            _siteId = siteId;
            _batchData = new Dictionary<string, string>()
            {
                {"Batch Date", $"{_batchDate:MM/dd/yyyy}"},
                {"Deposit Date", $"{_depositDate:MM/dd/yyyy}"},
                {"System Date", $"{_systemDate:MM/dd/yyyy}"},
                {"Process Date", $"{_processingDate:MM/dd/yyyy}"},
                {"Receive Date", $"{_receiveDate:MM/dd/yyyy}"},
                {"Consolidation Date", $"{_consolidationDate:MM/dd/yyyy}"},
                {"Batch ID", $"{_batchId}"},
                {"Batch Number", $"{_batchNumber}"},
                {"Client ID", $"{_clientId}"},
                {"Client Name", $"{_clientName}"},
                {"Site ID", $"{_siteId}"}
            };
        }

        public IEnumerable<string> GetBatch()
        {
            return _batchData.OrderBy(pair => pair.Key, StringComparer.InvariantCultureIgnoreCase).Select(kvp => $"{kvp.Key}:{kvp.Value}").ToArray();
        }
    }
}
