﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DitDataGenerator.Batches
{
    public class Header
    {
        private readonly DateTime _batchDate;
        private readonly int _workgroupId;
        private readonly string _docName;

        public Header(DateTime batchDate, int workgroupId, string docName)
        {
            _batchDate = batchDate;
            _workgroupId = workgroupId;
            _docName = docName;
        }

        public IEnumerable<string> GetHeader()
        {
            return new string[] { "BEGIN:", $">>DocTypeName:{_workgroupId} - {_docName}", $">>DocDate:{_batchDate:MM/dd/yyyy}" };
        }
    }
}
