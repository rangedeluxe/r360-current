using System.Collections.Generic;
using DitDataGenerator.Images;

namespace DitDataGenerator.Batches
{
    public class PercentageImageColorPicker : IImageColorPicker
    {
        private readonly ImageTypePercentages _imageTypePercent;
        private readonly IRandomPercentageGenerator _randomPercentageGenerator;        

        public PercentageImageColorPicker(ImageTypePercentages imageTypePercent, IRandomPercentageGenerator percentageGenerator)
        {
            _imageTypePercent = imageTypePercent;
            _randomPercentageGenerator = percentageGenerator;
        }
        public IReadOnlyList<ColourEncoding> GetNextImageColorEncodings()
        {   
            var imageColors = new List<ColourEncoding>();

            var percent = _randomPercentageGenerator.GetRandomPercent();
            int all = _imageTypePercent.PercentAllTypesImages;
            int gray = _imageTypePercent.PercentBitonalAndGrayImages;
            int color = _imageTypePercent.PercentBitonalAndColorImages;            

            if (percent < all)
            {
                imageColors.Add(ColourEncoding.Bitonal);
                imageColors.Add(ColourEncoding.Color);
                imageColors.Add(ColourEncoding.GrayScale);
            }
            else if (percent < all + gray)
            {
                imageColors.Add(ColourEncoding.Bitonal);
                imageColors.Add(ColourEncoding.GrayScale);
            }
            else if (percent < all + gray + color)
            {
                imageColors.Add(ColourEncoding.Bitonal);
                imageColors.Add(ColourEncoding.Color);
            }
            else // (rest is bitonal)
            {
                imageColors.Add(ColourEncoding.Bitonal);
            }

            return imageColors;
        }
    }
}