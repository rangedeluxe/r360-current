﻿using System.Collections.Generic;

namespace DitDataGenerator.Batches
{
    public class Payment
    {
        public double AppliedAmount { get; set; }
        public string AccountNumber;
        public string RT { get; set; }
        public string SerialNumber { get; set; }        

        public IEnumerable<string> GetPayment()
        {
            return new[]
            {
                $"Account Number:{AccountNumber}",
                $"Applied Amount:{AppliedAmount:0.00}",                
                $"RT:{RT}",
                $"Per Check Number:{SerialNumber}",
            };
        }
    }
}
