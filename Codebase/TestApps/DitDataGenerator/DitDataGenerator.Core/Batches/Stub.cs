﻿using System.Collections.Generic;
using System.Linq;

namespace DitDataGenerator.Batches
{
    public class Stub
    {
        public double AppliedAmount { get; set; }

        public IList<DataEntryField> DataEntryFields { get; set; } = new List<DataEntryField>();

        public IEnumerable<string> GetDataEntryFields()
        {
            return DataEntryFields.Select(dataEntryField => dataEntryField.GetDataEntryField());
        }

        public IEnumerable<string> GetStub()
        {
            return new[]
            {
                $"Applied Amount:{AppliedAmount:0.00}",
            };
        }
    }
}
