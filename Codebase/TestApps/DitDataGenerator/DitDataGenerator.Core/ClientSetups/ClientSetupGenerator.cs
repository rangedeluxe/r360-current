﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using DitDataGenerator.Metadata;
using WFS.integraPAY.Online.ICONCommon;

namespace DitDataGenerator.ClientSetups
{
    public class ClientSetupGenerator
    {
        private const int PaymentItemTypeNum = 200;
        private const int StubItemTypeNum = 201;
        private const string LcNum = "101";

        public ClientSetupGenerator(IEnumerable<Field> fields)
        {
            Fields = fields.ToList();
        }

        private IReadOnlyList<Field> Fields { get; }

        private XElement CreateAllItemTypeLcsElement()
        {
            return new XElement("AllItemtypeLCs",
                CreateItemTypeLcElement(PaymentItemTypeNum),
                CreateItemTypeLcElement(StubItemTypeNum));
        }
        private XElement CreateAllItemTypesElement()
        {
            return new XElement("AllItemtypes",
                CreateItemTypeElement(PaymentItemTypeNum, "Payment", SetupDocType.Check),
                CreateItemTypeElement(StubItemTypeNum, "Invoice", SetupDocType.NonCheck));
        }
        private XElement CreateAllItemTypeXKeywordsElement()
        {
            return new XElement("AllItemtypexKeywords",
                Fields.Select(CreateItemTypeXKeywordElement));
        }
        private XElement CreateAllKeyTypesElement()
        {
            return new XElement("AllKeytypes",
                Fields.Select(CreateKeyTypeElement));
        }
        private XElement CreateItemTypeElement(int itemTypeNum, string itemTypeName, SetupDocType docType)
        {
            return new XElement("ItemType",
                new XAttribute("itemtypenum", itemTypeNum.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("itemtypename", itemTypeName),
                new XAttribute("doctype", (int) docType));
        }
        private XElement CreateItemTypeLcElement(int itemTypeNum)
        {
            return new XElement("ItemTypeLC",
                new XAttribute("lcnum", LcNum),
                new XAttribute("itemtypenum", itemTypeNum));
        }
        private XElement CreateItemTypeXKeywordElement(Field field)
        {
            return new XElement("ItemtypexKeyword",
                new XAttribute("itemtype", GetItemTypeNum(field.DocType)),
                new XAttribute("keytype", field.KeyTypeNum));
        }
        private XElement CreateKeyTypeElement(Field field)
        {
            return new XElement("Keytype",
                new XAttribute("keytypenum", field.KeyTypeNum),
                new XAttribute("keytype", field.KeyType),
                new XAttribute("datatype", (int) field.DataType),
                new XAttribute("keytypelen", field.Length),
                new XAttribute("fieldtype", field.FieldType),
                new XAttribute("extracttype", field.ExtractType));
        }
        private XElement CreateLifeCycleElement()
        {
            return new XElement("LifeCycle",
                new XAttribute("lcnum", LcNum),
                CreateAllItemTypeLcsElement(),
                CreateAllItemTypesElement(),
                CreateAllItemTypeXKeywordsElement(),
                CreateAllKeyTypesElement());
        }
        private XElement CreateRootElement()
        {
            return new XElement("Report",
                CreateLifeCycleElement());
        }
        public ClientSetup Generate()
        {
            var root = CreateRootElement();
            return new ClientSetup(root);
        }
        private int GetItemTypeNum(SetupDocType docType)
        {
            switch (docType)
            {
                case SetupDocType.Check:
                    return PaymentItemTypeNum;
                case SetupDocType.NonCheck:
                    return StubItemTypeNum;
                default:
                    throw new ArgumentOutOfRangeException(nameof(docType), docType, null);
            }
        }
    }
}