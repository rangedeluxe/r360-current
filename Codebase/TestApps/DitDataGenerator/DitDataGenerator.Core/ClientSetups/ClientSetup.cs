﻿using System.Xml.Linq;

namespace DitDataGenerator.ClientSetups
{
    public class ClientSetup
    {
        public ClientSetup(XElement rootElement)
        {
            RootElement = rootElement;
        }

        public XElement RootElement { get; }
    }
}