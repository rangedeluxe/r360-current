using ImageGenerators.Interfaces;
using WFS.integraPAY.Online.ICONCommon;

namespace DitDataGenerator.Images
{
    /// <summary>
    /// Only generates a single check image and a single stub image, and then keeps returning
    /// their paths. This is a (hopefully temporary) hack because image generation is currently
    /// unacceptably slow.
    /// </summary>
    public class LazyImageGenerator : IImageGenerator
    {
        private ICheckDetails _checkPath;
        private IInvoiceDetails _invoicePath;
        private readonly IImageGenerator _realImageGenerator;

        public LazyImageGenerator(IImageGenerator realImageGenerator)
        {
            _realImageGenerator = realImageGenerator;
        }

        public ICheckDetails BuildCheckImage(ImageParameters parameters, bool includeBackImage = true, bool combineFrontAndBackImages = true)
        {
            return
                _checkPath ??
                (_checkPath = _realImageGenerator.BuildCheckImage(
                    CreateFakeParameters(parameters, SetupDocType.Check), includeBackImage, combineFrontAndBackImages));
        }
        public IInvoiceDetails BuildInvoiceImage(ImageParameters parameters, bool includeback = true, bool combineFrontAndBackImages = true)
        {
            return
                _invoicePath ??
                (_invoicePath = _realImageGenerator.BuildInvoiceImage(
                    CreateFakeParameters(parameters, SetupDocType.NonCheck), includeback, combineFrontAndBackImages));
        }
        private static ImageParameters CreateFakeParameters(ImageParameters parameters, SetupDocType documentType)
        {
            var transactionSequence = documentType == SetupDocType.Check ? 998 : 999;
            return new ImageParameters(
                parameters.BatchImageDirectory, 999, transactionSequence, "999", 999, parameters.ColourEncoding);
        }
    }
}