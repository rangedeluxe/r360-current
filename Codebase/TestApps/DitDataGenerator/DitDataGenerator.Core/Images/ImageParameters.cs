﻿namespace DitDataGenerator.Images
{
    public class ImageParameters
    {
        public ImageParameters(string batchImageDirectory, int batchId, int transactionSequence,
            string accountNumber, int amount, ColourEncoding colourEncoding)
        {
            AccountNumber = accountNumber;
            Amount = amount;
            BatchImageDirectory = batchImageDirectory;
            BatchId = batchId;
            TransactionSequence = transactionSequence;
            ColourEncoding = colourEncoding;
        }

        public ColourEncoding ColourEncoding { get; set; }
        public string AccountNumber { get; }
        public int Amount { get; }
        public string BatchImageDirectory { get; }
        public int BatchId { get; }
        public int TransactionSequence { get; }
    }
}