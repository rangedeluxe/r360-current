﻿using System.Collections.Generic;
using System.IO;
using DitDataGenerator.Helpers;
using ImageGenerators.Interfaces;
using ImageGenerators.Model;
using Wfs.Logging.Performance;

namespace DitDataGenerator.Images
{
    public class ImageGenerator : IImageGenerator
    {
        private readonly CheckImageGenerator _checkImageGenerator = new CheckImageGenerator();
        private readonly HashSet<string> _directoriesCreated = new HashSet<string>();
        private readonly InvoiceImageGenerator _invoiceImageGenerator = new InvoiceImageGenerator();
        private readonly IPerformanceLogger _performanceLogger;
        private readonly RandomData _random;

        public ImageGenerator(RandomData random, IPerformanceLogger performanceLogger)
        {
            _random = random;
            _performanceLogger = performanceLogger;
        }

        public ICheckDetails CheckDetails { get; } = new CheckDetails();
        public IInvoiceDetails InvoiceDetails { get; } = new InvoiceData();

        public ICheckDetails BuildCheckImage(ImageParameters parameters, bool includeBackImage = true, bool combineFrontAndBackImages = true)
        {
            using (_performanceLogger.StartPerformanceTimer("Generate a check image"))
                return BuildCheckImageInternal(parameters, includeBackImage, combineFrontAndBackImages);
        }
        private ICheckDetails BuildCheckImageInternal(ImageParameters parameters, bool includeback = true, bool combineFrontAndBackImages = true)
        {
            CheckDetails.FrontImageFileName = BuildImagePath(parameters);
            CheckDetails.Account = parameters.AccountNumber;
            CheckDetails.Amount = parameters.Amount;
            CheckDetails.CheckNumber = _random.RandomNumberString(8);
            CheckDetails.CombineFrontAndBackImages = combineFrontAndBackImages;
            CheckDetails.IncludeBack = includeback;
            _checkImageGenerator.CheckDetails = CheckDetails;
            _checkImageGenerator.CreateImageFiles();
            return _checkImageGenerator.CheckDetails;
        }
        public IInvoiceDetails BuildInvoiceImage(ImageParameters parameters, bool includeback = true, bool combineFrontAndBackImages = true)
        {
            using (_performanceLogger.StartPerformanceTimer("Generate an invoice image"))
                return BuildInvoiceImageInternal(parameters, includeback, combineFrontAndBackImages);
        }
        private IInvoiceDetails BuildInvoiceImageInternal(ImageParameters parameters, bool includeback = true, bool combineFrontAndBackImages = true)
        {
            InvoiceDetails.FrontImageFileName = BuildImagePath(parameters);
            InvoiceDetails.OrderNumber = _random.RandomNumberString(8);
            InvoiceDetails.InvoiceNumber = _random.RandomNumberString(8);
            InvoiceDetails.IncludeBack = includeback;
            InvoiceDetails.CombineFrontAndBackImages = combineFrontAndBackImages;
            _invoiceImageGenerator.InvoiceDetails = InvoiceDetails;
            _invoiceImageGenerator.CreateImageFiles();
            return InvoiceDetails;
        }
        private string BuildImagePath(ImageParameters parameters)
        {
            EnsureDirectoryExists(parameters.BatchImageDirectory);
            return Path.Combine(
                parameters.BatchImageDirectory,
                $"00{parameters.BatchId}{parameters.TransactionSequence}{parameters.ColourEncoding}.tif");
        }
        private void EnsureDirectoryExists(string directory)
        {
            // Don't even hit the disk if we don't have to
            if (_directoriesCreated.Contains(directory))
                return;

            Directory.CreateDirectory(directory);
            _directoriesCreated.Add(directory);
        }
    }
}