using ImageGenerators.Interfaces;

namespace DitDataGenerator.Images
{
    public interface IImageGenerator
    {
        ICheckDetails BuildCheckImage(ImageParameters parameters, bool includeBackImage = true,
            bool combineFrontAndBackImages = true);

        IInvoiceDetails BuildInvoiceImage(ImageParameters parameters, bool includeBackImage = true,
            bool combineFrontAndBackImages = true);
    }
}