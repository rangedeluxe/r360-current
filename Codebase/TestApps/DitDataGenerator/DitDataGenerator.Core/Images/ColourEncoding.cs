﻿using System;

namespace DitDataGenerator.Images
{
    public enum ColourEncoding
    {
        None,
        Color,
        GrayScale,
        Bitonal
    }
}