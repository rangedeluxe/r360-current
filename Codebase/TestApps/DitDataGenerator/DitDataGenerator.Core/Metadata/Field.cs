﻿using WFS.integraPAY.Online.ICONCommon;

namespace DitDataGenerator.Metadata
{
    public class Field
    {
        public Field(SetupDocType docType, int keyTypeNum, string keyType, ImageRpsDataType dataType,
            int length = 0, string fieldType = "", string extractType = "")
        {
            DocType = docType;
            KeyTypeNum = keyTypeNum;
            KeyType = keyType;
            DataType = dataType;
            Length = length;
            FieldType = fieldType;
            ExtractType = extractType;
        }

        public ImageRpsDataType DataType { get; }
        public SetupDocType DocType { get; }
        public string ExtractType { get; }
        public string FieldType { get; }
        public string KeyType { get; }
        public int KeyTypeNum { get; }
        public int Length { get; }
    }
}