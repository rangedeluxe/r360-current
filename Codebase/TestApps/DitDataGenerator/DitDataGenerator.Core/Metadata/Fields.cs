﻿using System.Collections.Generic;
using System.Linq;

namespace DitDataGenerator.Metadata
{
    public class Fields
    {
        public Fields(IEnumerable<Field> imageRpsBatchFields, 
            IEnumerable<Field> imageRpsItemFields,
            IEnumerable<Field> requiredStubDataEntryFields,
            IEnumerable<Field> optionalStubDataEntryFields )
        {
            ImageRpsBatchFields = imageRpsBatchFields.ToList();
            ImageRpsItemFields = imageRpsItemFields.ToList();
            RequiredStubDataEntryFields = requiredStubDataEntryFields.ToList();
            OptionalStubDataEntryFields = optionalStubDataEntryFields.ToList();
        }

        public IEnumerable<Field> AllFields => ImageRpsBatchFields
            .Concat(ImageRpsItemFields)
            .Concat(RequiredStubDataEntryFields)
            .Concat(OptionalStubDataEntryFields);
        public IReadOnlyList<Field> ImageRpsBatchFields { get; }
        public IReadOnlyList<Field> ImageRpsItemFields { get; }
        public IReadOnlyList<Field> RequiredStubDataEntryFields { get; }
        public IReadOnlyList<Field> OptionalStubDataEntryFields { get; }
    }
}