﻿using System.Collections.Generic;
using WFS.integraPAY.Online.ICONCommon;

namespace DitDataGenerator.Metadata
{
    public static class DefaultFieldProvider
    {
        /// <summary>
        /// Creates <see cref="Fields"/> representing the default fields for
        /// ImageRPS data generation.
        /// </summary>
        /// <remarks>
        /// The "ImageRPS standard" batch and item fields were built from the
        /// Client4400_Workgroup 4400_1_20140210_082951.xml sample client-setup file
        /// provided by Michael Lowe, with the data types specified in that file.
        /// This isn't the complete list of "ImageRPS standard" fields, but if it
        /// works for normal ImageRPS imports, it should work for the data generator.
        /// </remarks>
        public static Fields GetFields()
        {
            return new Fields(
                imageRpsBatchFields: GetImageRpsBatchFields(),
                imageRpsItemFields: GetImageRpsItemFields(),
                requiredStubDataEntryFields: GetRequiredStubDataEntryFields(),
                optionalStubDataEntryFields: GetOptionalStubDataEntryFields()
                );
        }

        private static Field CreateCheckField(int keyTypeNum, string keyType, ImageRpsDataType dataType, int length = 0, string fieldType = "", string extractType = "")
        {
            return new Field(SetupDocType.Check, keyTypeNum, keyType, dataType, length, fieldType, extractType);
        }
        private static IEnumerable<Field> GetImageRpsBatchFields()
        {
            // These IDs are the database BatchDataSetupFieldKey plus 200, which should
            // guarantee they're unique within the data generator.
            yield return CreateCheckField(219, "Abbrev Client Name", ImageRpsDataType.String, length: 10);
            yield return CreateCheckField(205, "Batch Date", ImageRpsDataType.Date);
            yield return CreateCheckField(225, "Batch Number", ImageRpsDataType.Numeric);
            yield return CreateCheckField(203, "Client Name", ImageRpsDataType.String, length: 30);
            yield return CreateCheckField(210, "Consolidation Date", ImageRpsDataType.Date);
            yield return CreateCheckField(211, "Consolidation Number", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(208, "Deposit Number", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(209, "Deposit Time", ImageRpsDataType.Numeric);
            yield return CreateCheckField(204, "Doc Grp ID", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(217, "Location ID", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(218, "Location Name", ImageRpsDataType.String, length: 40);
            yield return CreateCheckField(214, "P1 Operator ID", ImageRpsDataType.String, length: 8);
            yield return CreateCheckField(212, "P1 Station ID", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(215, "P2 Operator ID", ImageRpsDataType.String, length: 8);
            yield return CreateCheckField(213, "P2 Station ID", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(224, "Process Date", ImageRpsDataType.Date);
            yield return CreateCheckField(216, "Process Type", ImageRpsDataType.String, length: 20);
            yield return CreateCheckField(207, "Receive Date", ImageRpsDataType.Date);
            yield return CreateCheckField(226, "Reference Page", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(220, "Site ID", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(206, "System Date", ImageRpsDataType.Date);
            yield return CreateCheckField(221, "Work Type", ImageRpsDataType.String, length: 20);
            yield return CreateCheckField(226, "Per Check Number", ImageRpsDataType.String, 10, "A", "4");
            yield return CreateCheckField(227, "Account Number", ImageRpsDataType.String, 16, "A", "3");
            yield return CreateCheckField(227, "RT", ImageRpsDataType.String, 9, "A", "2");
        }
        private static IEnumerable<Field> GetImageRpsItemFields()
        {
            // These IDs are the database ItemDataSetupFieldKey plus 300, which should
            // guarantee they're unique within the data generator.
            yield return CreateCheckField(303, "Applied OpID", ImageRpsDataType.String, 12);
            yield return CreateCheckField(308, "ARC Reason", ImageRpsDataType.String, 20);
            yield return CreateCheckField(309, "ARC Tracer", ImageRpsDataType.String, 50);
            yield return CreateCheckField(307, "Audit Trail", ImageRpsDataType.String, 250);
            yield return CreateCheckField(302, "Document ID", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(315, "Document Type", ImageRpsDataType.String, 20);
            yield return CreateCheckField(318, "Endpoint DDA", ImageRpsDataType.String, 20);
            yield return CreateCheckField(317, "Endpoint Name", ImageRpsDataType.String, 30);
            yield return CreateCheckField(321, "IMS Exceptions ID", ImageRpsDataType.Numeric);
            yield return CreateCheckField(319, "Item Identifier", ImageRpsDataType.String, 22);
            yield return CreateCheckField(312, "Orig Batch ID", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(313, "Orig P1 Seq Num", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(306, "P2 Pocket Seq Num", ImageRpsDataType.String, 3);
            yield return CreateCheckField(304, "P2 Sequence", ImageRpsDataType.IdOrSequenceNumber);
            yield return CreateCheckField(305, "Pocket Cut ID", ImageRpsDataType.Numeric);
            yield return CreateCheckField(310, "Reject Job", ImageRpsDataType.String, 12);
            yield return CreateCheckField(311, "Reject Reason", ImageRpsDataType.String, 60);
            yield return CreateCheckField(320, "Transaction Identifier", ImageRpsDataType.String, 20);
        }
        private static IEnumerable<Field> GetRequiredStubDataEntryFields()
        {
            // "Applied Amount" needs to be in the client-setup field in order for batch files
            // to be able to import an "Amount" field (at least as of June 2017).
            yield return new Field(SetupDocType.NonCheck, 401, "Applied Amount", ImageRpsDataType.Currency);
        }

        private static IEnumerable<Field> GetOptionalStubDataEntryFields()
        {
            yield return new Field(SetupDocType.NonCheck, 402, "Data Entry 1", ImageRpsDataType.String,40,"A");
            yield return new Field(SetupDocType.NonCheck, 403, "Data Entry 2", ImageRpsDataType.String,40,"A");
            yield return new Field(SetupDocType.NonCheck, 404, "Data Entry 3", ImageRpsDataType.String, 40, "A");
            yield return new Field(SetupDocType.NonCheck, 405, "Data Entry 4", ImageRpsDataType.String, 40, "A");
            yield return new Field(SetupDocType.NonCheck, 406, "Data Entry 5", ImageRpsDataType.String, 40, "A");
            yield return new Field(SetupDocType.NonCheck, 407, "Data Entry 6", ImageRpsDataType.String, 40, "A");
            yield return new Field(SetupDocType.NonCheck, 408, "Data Entry 7", ImageRpsDataType.String, 40, "A");
            yield return new Field(SetupDocType.NonCheck, 409, "Data Entry 8", ImageRpsDataType.String, 40, "A");
            yield return new Field(SetupDocType.NonCheck, 410, "Data Entry 9", ImageRpsDataType.String, 40, "A");
            yield return new Field(SetupDocType.NonCheck, 411, "Data Entry 10", ImageRpsDataType.String, 40, "A");
        }
    }
}