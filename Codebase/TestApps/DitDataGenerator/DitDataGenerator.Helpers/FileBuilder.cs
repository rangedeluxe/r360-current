﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DitDataGenerator.Helpers
{
    public class FileBuilder : IFileBuilder
    {
        private string _theDirectory;
        
        public FileBuilder(string theDirectory)
        {
            _theDirectory = theDirectory;
            Directory.CreateDirectory(theDirectory);
        }
        

        public void WriteFile(IEnumerable<string> theRecords, string fileName)
        {
            string outputPath = _theDirectory + "/" + fileName;
            try
            {
                //Make sure the file does not exist
                if (File.Exists(outputPath))
                {
                    File.Delete(outputPath);
                }
                // Create the file to write to.
                using (StreamWriter sw = File.CreateText(outputPath))
                {
                    foreach (var record in theRecords)
                    {
                        sw.WriteLine(record);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
