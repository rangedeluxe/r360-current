﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DitDataGenerator.Helpers
{
    public interface IFileBuilder
    {
        void WriteFile(IEnumerable<string> theRecords, string fileName);
    }
}
