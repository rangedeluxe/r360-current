﻿using System;
using System.Linq;

namespace DitDataGenerator.Helpers
{
    public class RandomData
    {
        private static Random _random = new Random();
        const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmonpqrstuvwxyz0123456789,*";
        private const string _numbers = "0123456789";

        public string RandomString(int length)
        {
            return new string(Enumerable.Repeat(_chars, length)
                    .Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        public string RandomNumberString(int length)
        {
            return new string(Enumerable.Repeat(_numbers, length)
                    .Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        public int RandomNumberLimited(int minNumber, int maxNumber)
        {
            return _random.Next(minNumber, maxNumber);
        }
    }
}