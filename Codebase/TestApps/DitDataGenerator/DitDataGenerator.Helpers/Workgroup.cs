﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DitDataGenerator.Helpers
{
    public class Workgroup
    {
        public int WorkgroupId { get; set; }

        public string WorkgroupName { get; set; }
        public int BatchCounter { get; set; }
    }
}
