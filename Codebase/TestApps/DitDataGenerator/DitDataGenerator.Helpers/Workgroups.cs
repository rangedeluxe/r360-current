﻿using System;
using System.Collections.Generic;

namespace DitDataGenerator.Helpers
{
    public class Workgroups : IWorkgroups
    {
        private List<Workgroup> _workgroupList;
        private static Random _random = new Random();
        public Workgroups(int workgroupIdPrefix, int numberOfWorkgroups, string workgroupNamePrefix, bool singleWorkGroup)
        {
            _workgroupList = new List<Workgroup>();
            if (singleWorkGroup)
            {
                createSingleWorkgroup(workgroupIdPrefix, workgroupNamePrefix);
            }
            else
            {
                createWorkgroupList((workgroupIdPrefix * 1000) + 1, numberOfWorkgroups,workgroupNamePrefix);
            }
            
        }

        private void createSingleWorkgroup(int workgroupId,  string workgroupNamePrefix)
        {
            var workgroup = new Workgroup()
            {
                WorkgroupId = workgroupId,
                WorkgroupName = workgroupNamePrefix
            };
            _workgroupList.Add(workgroup);
        }

        private void createWorkgroupList(int workgroupSeed, int numberOfWorkgroups, string workgroupNamePrefix)
        {
            int workgroupId = workgroupSeed;
            for (int i = 0; i < numberOfWorkgroups; i++)
            {
                var workgroup = new Workgroup()
                {
                    WorkgroupId = workgroupId,
                    WorkgroupName = workgroupNamePrefix + workgroupId
                };
                _workgroupList.Add(workgroup);
                workgroupId++;
            }
        }

        public int WorkgroupCount()
        {
            return _workgroupList.Count;
        }

        public Workgroup GetRandomWorkgroup()
        {
            return _workgroupList[_random.Next(0, _workgroupList.Count)];
        }

        public Workgroup GetWorkgroup(int workgroup)
        {
            if (workgroup > _workgroupList.Count)
                return _workgroupList[0];
            return _workgroupList[workgroup];
        }
    }
}
