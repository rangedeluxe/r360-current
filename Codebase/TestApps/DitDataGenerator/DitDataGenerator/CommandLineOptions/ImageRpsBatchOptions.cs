﻿using CommandLine;

namespace DitDataGenerator.CommandLineOptions
{
    [Verb("ImageRpsBatch", HelpText = "Generate ImageRPS batch files.")]
    public class ImageRpsBatchOptions
    {
        [Value(0, MetaName = "count", Default = 1, HelpText = "Number of batch files to create.", Required = true)]
        public int BatchCount { get; set; }

        [Value(0, MetaName = "workgroupcount", Default = 500, HelpText = "Number of workgroups to create.", Required = false)]
        public int WorkgroupCount { get; set; }
    }
}