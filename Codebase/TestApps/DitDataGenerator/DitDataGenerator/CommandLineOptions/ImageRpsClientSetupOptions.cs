﻿using CommandLine;

namespace DitDataGenerator.CommandLineOptions
{
    [Verb("ImageRpsClientSetup", HelpText = "Generate ImageRPS client-setup files.")]
    public class ImageRpsClientSetupOptions
    {
        [Value(0, MetaName = "workgroupcount", Default = 500, HelpText = "Number of workgroups to create.", Required = false)]
        public int WorkgroupCount { get; set; }
    }
}