﻿using System;
using System.IO;
using CommandLine;
using DitDataGenerator.Batches;
using DitDataGenerator.ClientSetups;
using DitDataGenerator.CommandLineOptions;
using DitDataGenerator.Helpers;
using DitDataGenerator.Metadata;
using DitDataGenerator.Services;
using Wfs.Logging.Performance;

namespace DitDataGenerator
{
    public static class Program
    {
        private static class AppSettingNames
        {
            public const string ImageDirectory = "ImageDirectory";
            public const string ClientSetupDirectory = "ClientSetupDirectory";
            public const string BatchDirectory = "BatchDirectory";
            public const string ItemCountPerBatch = "ItemCountPerBatch";
            public const string WorkgroupIdPrefix = "WorkgroupIdPrefix";
            public const string WorkgroupNamePrefix = "WorkgroupNamePrefix";
            public const string SiteCode = "SiteCode";
            public const string TiffImageFiles = "TiffImageFiles";
            public const string PercentBitonalAndGrayImages = "PercentBitonalAndGrayImages";
            public const string PercentBitonalAndColorImages = "PercentBitonalAndColorImages";
            public const string PercentAllImageTypes = "PercentAllImageTypes";
            public const string SingleWorkgroup = "SingleWorkgroup";
        }

        public static int Main(string[] args)
        {
            ILogger logger = new TextWriterLogger(Console.Out);
            var performanceLogger = new PerformanceLogger(new StopwatchHighResolutionTimerProvider());

            try
            {
                return Parser.Default
                    .ParseArguments<ImageRpsBatchOptions, ImageRpsClientSetupOptions>(args)
                    .MapResult(
                        (ImageRpsBatchOptions options) => RunImageRpsBatch(logger, options, performanceLogger),
                        (ImageRpsClientSetupOptions options) => RunImageRpsClientSetup(logger, options),
                        errors => 1);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return 1;
            }
            finally
            {
                var performanceReport = PerformanceLogReporter.GetReport(performanceLogger.GetMeasuredActions());
                foreach (var line in performanceReport)
                    logger.LogEvent(line);
            }
        }
        private static int RunImageRpsBatch(ILogger logger, ImageRpsBatchOptions options,
            IPerformanceLogger performanceLogger)
        {
            using (performanceLogger.StartPerformanceTimer("Generate all ImageRPS batches"))
                return RunImageRpsBatchInternal(logger, options, performanceLogger);
        }
        private static int RunImageRpsBatchInternal(ILogger logger, ImageRpsBatchOptions options,
            IPerformanceLogger performanceLogger)
        {
            ConfigHelper configHelper = new ConfigHelper(logger);

            string imageDirectory = configHelper.GetAppSetting(AppSettingNames.ImageDirectory);
            string batchDirectory = configHelper.GetAppSetting(AppSettingNames.BatchDirectory);
            int itemCountPerBatch = Convert.ToInt32(configHelper.GetAppSetting(AppSettingNames.ItemCountPerBatch));
            int workgroupIdPrefix = Convert.ToInt32(configHelper.GetAppSetting(AppSettingNames.WorkgroupIdPrefix));
            string workgroupNamePrefix = configHelper.GetAppSetting(AppSettingNames.WorkgroupNamePrefix);
            int siteCode = Convert.ToInt32(configHelper.GetAppSetting(AppSettingNames.SiteCode));
            int batchCount = options.BatchCount;
            int workGroupCount = options.WorkgroupCount;
            string tiffImageFiles = configHelper.GetAppSetting(AppSettingNames.TiffImageFiles);
            int percentAllImageTypes = Convert.ToInt32(configHelper.GetAppSetting(AppSettingNames.PercentAllImageTypes));
            int percentBitonalAndColorImages = Convert.ToInt32(configHelper.GetAppSetting(AppSettingNames.PercentBitonalAndColorImages));
            int percentBitonalAndGrayImages = Convert.ToInt32(configHelper.GetAppSetting(AppSettingNames.PercentBitonalAndGrayImages));
            bool singleWorkgroup = Convert.ToBoolean(configHelper.GetAppSetting(AppSettingNames.SingleWorkgroup));

            int totalPercentages = percentBitonalAndGrayImages + percentAllImageTypes + percentBitonalAndColorImages;

            if (totalPercentages > 100)
            {
                throw new Exception($"The sum of all three Percentage Image Types is grater than 100. percentBitonalAndGrayImages={percentBitonalAndGrayImages} percentAllImageTypes={percentAllImageTypes} and percentBitonalAndColorImages={percentAllImageTypes})");
            }

            ImageTypePercentages imageTypePercent =
                new ImageTypePercentages
                {
                    PercentBitonalAndGrayImages = percentBitonalAndGrayImages,
                    PercentAllTypesImages = percentAllImageTypes,
                    PercentBitonalAndColorImages = percentBitonalAndColorImages
                };
            // Remaining percent, if any, is for BitonalImages.
            if (totalPercentages < 100) imageTypePercent.PercentBitonalImages = (100 - totalPercentages);

            FileBuilder fileBuilder = new FileBuilder(batchDirectory);

            logger.LogEvent("---------Starting File Generation---------");
            logger.LogEvent("Build Batch Files:");
            logger.LogEvent($"Creating {options.WorkgroupCount} workgroups");
            logger.LogEvent($"Creating {options.BatchCount} batches");

            var percentageImageColorPicker =
                new PercentageImageColorPicker(imageTypePercent, new RandomPercentageGenerator());

            DatGenerator datFile = new DatGenerator(fileBuilder, imageDirectory,
                itemCountPerBatch, batchCount, workgroupIdPrefix, workgroupNamePrefix, siteCode, workGroupCount,
                performanceLogger, DefaultFieldProvider.GetFields(), GetTiffImageFilesSetting(tiffImageFiles),
                percentageImageColorPicker, singleWorkgroup);

            datFile.BuildAllBatchFiles();

            ImageStatistics imageStatistics = datFile.GetImageStatistics();
            logger.LogEvent($"Images statistics Color:{imageStatistics.ColorImagesCount}, Bitonal:{imageStatistics.BitonalImagesCount}, and Gray:{imageStatistics.GrayImagesCount}.");
            logger.LogEvent("---------Data Generation Finished---------");
            return 0;
        }

        private static TiffImageFilesOption GetTiffImageFilesSetting(string tiffImageFiles)
        {
            const TiffImageFilesOption defaultValue = TiffImageFilesOption.MultiPage;
            if (tiffImageFiles != null)
            {
                tiffImageFiles = tiffImageFiles.ToLower();
                switch (tiffImageFiles)
                {
                    case "singlepage":
                        return TiffImageFilesOption.SinglePage;
                    case "noimages":
                        return TiffImageFilesOption.NoImages;
                    case "multipage":
                        return TiffImageFilesOption.MultiPage;
                    default://multipage
                        return defaultValue;
                }
            }
            return defaultValue;
        }

        private static int RunImageRpsClientSetup(ILogger logger, ImageRpsClientSetupOptions options)
        {
            ConfigHelper configHelper = new ConfigHelper(logger);

            var clientSetupDirectory = configHelper.GetAppSetting(AppSettingNames.ClientSetupDirectory);
            var workgroupIdPrefix = configHelper.GetAppSetting(AppSettingNames.WorkgroupIdPrefix);
            var workgroupNamePrefix = configHelper.GetAppSetting(AppSettingNames.WorkgroupNamePrefix);
            var siteCode = configHelper.GetAppSetting(AppSettingNames.SiteCode);
            bool singleWorkgroup = Convert.ToBoolean(configHelper.GetAppSetting(AppSettingNames.SingleWorkgroup));

            logger.LogEvent("---------Starting File Generation---------");
            logger.LogEvent("Build Client Setup Files:");
            logger.LogEvent($"Creating {options.WorkgroupCount} workgroups");

            var fields = DefaultFieldProvider.GetFields();
            var clientSetup = new ClientSetupGenerator(fields.AllFields).Generate();
            Directory.CreateDirectory(clientSetupDirectory);

            if (singleWorkgroup)
            {                
                clientSetup.RootElement.Save(Path.Combine(clientSetupDirectory,
                    $"Client{workgroupIdPrefix}_{workgroupNamePrefix}{workgroupIdPrefix}_{siteCode}_{DateTime.Now:yyyyMMdd_HHmmss}.xml"));
            }
            else
            {
                int clientId = Convert.ToInt32(workgroupIdPrefix + "001");
                for (int i = 0; i < options.WorkgroupCount; i++)
                {
                    clientSetup.RootElement.Save(Path.Combine(clientSetupDirectory,
                        $"Client{clientId}_{workgroupNamePrefix}{clientId}_{siteCode}_{DateTime.Now:yyyyMMdd_HHmmss}.xml"));
                    clientId++;
                }
            }

            logger.LogEvent("---------Data Generation Finished---------");
            return 0;
        }
    }
}