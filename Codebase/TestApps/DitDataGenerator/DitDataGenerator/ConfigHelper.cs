﻿using System;
using System.Configuration;
using DitDataGenerator.Services;

namespace DitDataGenerator
{
    public class ConfigHelper
    {
        private ILogger _logger;
        public ConfigHelper(ILogger logger)
        {
            _logger = logger;
        }
        public string GetAppSetting(string settingName)
        {
            return GetAppSetting(settingName, "Undefined");
        }

        // Generic app setting access
        public string GetAppSetting(string settingName, string defaultValue)
        {
            try
            {
                string value = ConfigurationManager.AppSettings[settingName];
                if (string.IsNullOrEmpty(value))
                    value = defaultValue;

                if (value != defaultValue)
                    _logger.LogEvent(string.Format("Using '{0}' = '{1}'", settingName, value));

                return value;
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Error reading configuration setting {0} - {1}", settingName, ex.Message));
                return defaultValue;
            }
        }
    }
}
