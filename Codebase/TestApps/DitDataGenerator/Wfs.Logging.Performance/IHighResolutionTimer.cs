﻿using System;

namespace Wfs.Logging.Performance
{
    public interface IHighResolutionTimer
    {
        TimeSpan Elapsed { get; }
    }
}