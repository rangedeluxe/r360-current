﻿using System.Collections.Generic;

namespace Wfs.Logging.Performance
{
    public class NullPerformanceLogger : IPerformanceLoggerWithStatistics
    {
        public IPerformanceTimer StartPerformanceTimer(string actionName)
        {
            return new NullPerformanceTimer();
        }
        public IReadOnlyList<PerformanceLoggerActionStatistics> GetMeasuredActions()
        {
            return new PerformanceLoggerActionStatistics[] { };
        }
    }
}