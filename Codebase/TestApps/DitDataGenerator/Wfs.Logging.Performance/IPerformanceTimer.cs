﻿using System;

namespace Wfs.Logging.Performance
{
    public interface IPerformanceTimer : IDisposable
    {
        void Stop();
    }
}