﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wfs.Logging.Performance
{
    public static class PerformanceLogReporter
    {
        public static IEnumerable<string> GetReport(IReadOnlyList<PerformanceLoggerActionStatistics> actions)
        {
            return actions.Select(GetReport);
        }
        private static string GetReport(PerformanceLoggerActionStatistics action)
        {
            var name = action.ActionName;
            var timings = action.Timings.OrderBy(timing => timing).ToList();
            var count = timings.Count;
            var median = timings.Any()
                ? timings[timings.Count / 2]
                : TimeSpan.Zero;
            var total = timings.Aggregate(TimeSpan.Zero, (accumulator, value) => accumulator + value);
            return $"Timings: {name}: called {count}x, median time {median:g}, total time {total:g}";
        }
    }
}