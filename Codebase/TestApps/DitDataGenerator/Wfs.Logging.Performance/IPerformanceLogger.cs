﻿namespace Wfs.Logging.Performance
{
    public interface IPerformanceLogger
    {
        IPerformanceTimer StartPerformanceTimer(string actionName);
    }
}