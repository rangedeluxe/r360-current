﻿namespace Wfs.Logging.Performance
{
    public interface IHighResolutionTimerProvider
    {
        IHighResolutionTimer StartTimer();
    }
}