﻿using System.Collections.Generic;

namespace Wfs.Logging.Performance
{
    public interface IPerformanceLoggerWithStatistics : IPerformanceLogger
    {
        IReadOnlyList<PerformanceLoggerActionStatistics> GetMeasuredActions();
    }
}