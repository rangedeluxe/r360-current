﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wfs.Logging.Performance
{
    public class PerformanceLoggerActionStatistics
    {
        public PerformanceLoggerActionStatistics(string actionName, IEnumerable<TimeSpan> timings)
        {
            ActionName = actionName;
            Timings = timings.ToList();
        }

        public string ActionName { get; }
        public IReadOnlyList<TimeSpan> Timings { get; }
    }
}