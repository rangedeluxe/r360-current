﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wfs.Logging.Performance
{
    public class PerformanceLogger : IPerformanceLoggerWithStatistics
    {
        private class Entry
        {
            public Entry(string actionName)
            {
                ActionName = actionName;
            }

            public string ActionName { get; }
            public TimeSpan? TimeSpan { get; set; }
        }

        private class EntryPerformanceTimer : IPerformanceTimer
        {
            private readonly Entry _entry;
            private readonly IHighResolutionTimer _timer;

            public EntryPerformanceTimer(Entry entry, IHighResolutionTimer timer)
            {
                _entry = entry;
                _timer = timer;
            }

            public void Dispose()
            {
                Stop();
            }
            public void Stop()
            {
                if (_entry.TimeSpan == null)
                    _entry.TimeSpan = _timer.Elapsed;
            }
        }

        private readonly List<Entry> _timings = new List<Entry>();
        private readonly IHighResolutionTimerProvider _timerProvider;

        public PerformanceLogger(IHighResolutionTimerProvider timerProvider)
        {
            _timerProvider = timerProvider;
        }

        public IReadOnlyList<PerformanceLoggerActionStatistics> GetMeasuredActions()
        {
            return _timings
                .Where(entry => entry.TimeSpan.HasValue)
                .GroupBy(entry => entry.ActionName, entry => entry.TimeSpan.GetValueOrDefault())
                .Select(group => new PerformanceLoggerActionStatistics(group.Key, group))
                .ToList();
        }
        public IPerformanceTimer StartPerformanceTimer(string actionName)
        {
            var entry = new Entry(actionName);
            _timings.Add(entry);

            var timer = _timerProvider.StartTimer();
            return new EntryPerformanceTimer(entry, timer);
        }
    }
}