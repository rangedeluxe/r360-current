﻿using System;
using System.Diagnostics;

namespace Wfs.Logging.Performance
{
    public class StopwatchHighResolutionTimer : IHighResolutionTimer
    {
        private readonly Stopwatch _stopwatch = Stopwatch.StartNew();

        public TimeSpan Elapsed => _stopwatch.Elapsed;
    }
}