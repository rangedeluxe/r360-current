﻿namespace Wfs.Logging.Performance
{
    public class StopwatchHighResolutionTimerProvider : IHighResolutionTimerProvider
    {
        public IHighResolutionTimer StartTimer()
        {
            return new StopwatchHighResolutionTimer();
        }
    }
}