﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using DitDataGenerator.Metadata;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.integraPAY.Online.ICONCommon;

namespace DitDataGenerator.ClientSetups
{
    [TestClass]
    public class ClientSetupGeneratorTests
    {
        private static void AssertHasElementsMatchingXPath(ClientSetup clientSetup, string xPathExpression,
            IReadOnlyList<string> withAttributes = null, int? expectedCount = null)
        {
            var elements = ElementsMatchingXPath(clientSetup, xPathExpression);
            Assert.IsTrue(elements.Any(), $"Found no elements matching XPath: {xPathExpression}");

            if (withAttributes != null)
            {
                foreach (var attributeName in withAttributes)
                {
                    foreach (var element in elements)
                    {
                        Assert.IsNotNull(element.Attribute(attributeName),
                            $"Expected attribute '{attributeName}' on element:\r\n{element}");
                    }
                }
            }

            if (expectedCount != null)
                Assert.AreEqual(expectedCount, elements.Count, "Count");
        }
        private static void AssertDoesNotHaveElementsMatchingXPath(ClientSetup clientSetup, string xPathExpression)
        {
            var elements = ElementsMatchingXPath(clientSetup, xPathExpression);
            Assert.AreEqual(0, elements.Count,
                $"Expected no elements matching XPath but found {elements.Count}: {xPathExpression}");
        }
        private static IReadOnlyList<XElement> ElementsMatchingXPath(ClientSetup clientSetup, string xPathExpression)
        {
            // Wrap the root in an XDocument so rooted XPath expressions (e.g. '/Report') will work
            var document = new XDocument(clientSetup.RootElement);

            var elements = ((IEnumerable) document.XPathEvaluate(xPathExpression)).Cast<XElement>().ToList();
            return elements;
        }
        private ClientSetup Generate(params Field[] fields)
        {
            return new ClientSetupGenerator(fields).Generate();
        }
        private string GetSortedIntegerAttributes(IEnumerable<XElement> elements, XName attributeName)
        {
            var intValues = elements
                .Select(element => element.Attribute(attributeName)?.Value)
                .Where(attributeValue => attributeValue != null)
                .Select(int.Parse)
                .OrderBy(value => value);
            return string.Join(", ", intValues);
        }

        [TestMethod]
        public void HasRootElement()
        {
            var clientSetup = Generate();
            AssertHasElementsMatchingXPath(clientSetup, "/Report");
        }

        // LifeCycle element: specifies the lcnum, which is used to filter the ItemTypeLC list.

        [TestMethod]
        public void HasLifeCycleElement()
        {
            var clientSetup = Generate();
            AssertHasElementsMatchingXPath(clientSetup, "/Report/LifeCycle",
                withAttributes: new[] {"lcnum"});
        }

        // ItemTypeLC elements are basically a many-many join between LifeCycle.lcnum
        // (singleton) and ItemType.itemtypenum. If any ItemTypeLC elements have an lcnum
        // that doesn't match LifeCycle.lcnum, they'll be ignored by the XClient - so we
        // just make sure all the ItemTypeLCs we generate have the same lcnum as LifeCycle.

        [TestMethod]
        public void HasItemTypeLcElements()
        {
            var clientSetup = Generate();
            AssertHasElementsMatchingXPath(clientSetup, "/Report/LifeCycle/AllItemtypeLCs/ItemTypeLC",
                withAttributes: new[] {"lcnum", "itemtypenum"});
        }
        [TestMethod]
        public void ItemTypeLcElements_LcNumAttributes_AllMatchLifeCycleLcNum()
        {
            var clientSetup = Generate();

            // Find the lcnum attribute of the top-level LifeCycle element
            var lifeCycleLcNum = clientSetup.LifeCycleElement().Attribute("lcnum")?.Value;
            Assert.IsNotNull(lifeCycleLcNum, "Could not find <LifeCycle> attribute's lcnum attribute");

            // All the ItemTypeLC elements should have that same lcnum attribute
            foreach (var itemTypeLcElement in clientSetup.ItemTypeLcElements())
            {
                Assert.AreEqual(lifeCycleLcNum, itemTypeLcElement.Attribute("lcnum")?.Value,
                    $"lcnum attribute of {itemTypeLcElement}");
            }
        }
        [TestMethod]
        public void ItemTypeLcElements_ItemTypeNumAttributes_AreDistinct()
        {
            var clientSetup = Generate();
            var itemTypeNums = clientSetup.ItemTypeLcElements()
                .Select(element => element.Attribute("itemtypenum")?.Value)
                .ToList();
            var distinctItemTypeNums = itemTypeNums.Distinct(StringComparer.InvariantCultureIgnoreCase);

            if (distinctItemTypeNums.Count() != itemTypeNums.Count)
            {
                Assert.Fail(
                    "Expected itemtypenum values to be distinct, but found: " + string.Join(", ", itemTypeNums));
            }
        }

        // Each ItemType element represents a particular (named) set of data-entry fields,
        // either payment or stub. Multiple item types can (and usually will) be imported
        // into one R360 workgroup, where we create all the data-entry fields that appear in
        // any of those item types (i.e. the union of all the sets of data-entry fields).
        // To be used by the XClient, every ItemType must be joined back to LifeCycle.lcnum
        // via an ItemTypeLC element. Presumably ImageRPS can sometimes output fields we
        // want to ignore, but for our data generator, we'll make sure every ItemType matches
        // an ItemTypeLC so it will get imported.

        [TestMethod]
        public void HasItemTypeElements()
        {
            var clientSetup = Generate();
            AssertHasElementsMatchingXPath(clientSetup, "/Report/LifeCycle/AllItemtypes/ItemType",
                withAttributes: new[] {"itemtypenum", "itemtypename", "doctype"});
        }
        [TestMethod]
        public void ItemTypeElements_ItemTypeNumAttributes_AllMatchItemTypeLcElements()
        {
            var clientSetup = Generate();

            Assert.AreEqual(
                GetSortedIntegerAttributes(clientSetup.ItemTypeLcElements(), "itemtypenum"),
                GetSortedIntegerAttributes(clientSetup.ItemTypeElements(), "itemtypenum"));
        }
        [TestMethod]
        public void ItemTypeElements_HaveAtLeastOnePaymentAndOneStub()
        {
            var clientSetup = Generate();
            var itemTypeElements = clientSetup.ItemTypeElements().ToList();

            var paymentItemTypeCount = itemTypeElements
                .Count(element => element.Attribute("doctype")?.Value == ((int) SetupDocType.Check).ToString());
            Assert.AreNotEqual(0, paymentItemTypeCount, "Payment ItemTypes");

            var stubItemTypeCount = itemTypeElements
                .Count(element => element.Attribute("doctype")?.Value == ((int) SetupDocType.NonCheck).ToString());
            Assert.AreNotEqual(0, stubItemTypeCount, "Stub ItemTypes");
        }

        // AllItemtypexKeywords specifies which fields (the keytype attribute, which joins to
        // Keytype.keytypenum) belong to each item type (the itemtype attribute, which joins
        // to ItemType.itemtypenum).

        [TestMethod]
        public void WithNoFieldsPassedIn_HasNoItemTypeXKeywordElements()
        {
            var noFields = new Field[] { };
            var clientSetup = Generate(noFields);

            AssertDoesNotHaveElementsMatchingXPath(clientSetup,
                "/Report/LifeCycle/AllItemtypexKeywords/ItemtypexKeyword");
        }
        [TestMethod]
        public void WithSomeFieldsPassedIn_HasItemTypeXKeywordElements()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Numeric),
            };
            var clientSetup = Generate(fields);

            AssertHasElementsMatchingXPath(clientSetup,
                "/Report/LifeCycle/AllItemtypexKeywords/ItemtypexKeyword",
                withAttributes: new[] {"itemtype", "keytype"});
        }
        [TestMethod]
        public void ItemTypeXKeywordElements_HaveItemType_MatchingAnItemTypeWithTheFieldsDocType()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Numeric),
            };
            var clientSetup = Generate(fields);

            // Simplifying assumption: the generator will associate each field with a single item type.
            // Find the item type it generated for check and the item type it generated for stub.
            var itemTypeElementsByDocType = clientSetup.ItemTypeElements()
                .ToDictionary(e => (SetupDocType) int.Parse(e.Attribute("doctype")?.Value ?? ""));
            var paymentItemTypeNum = itemTypeElementsByDocType[SetupDocType.Check].Attribute("itemtypenum")?.Value;
            var stubItemTypeNum = itemTypeElementsByDocType[SetupDocType.NonCheck].Attribute("itemtypenum")?.Value;

            var itemTypeXKeywordElements = clientSetup.ItemTypeXKeywordElements().ToList();
            Assert.AreEqual(3, itemTypeXKeywordElements.Count, "ItemtypexKeyword element count");
            Action<string, XElement> assertItemTypeNum = (expectedItemTypeNum, element) =>
            {
                Assert.AreEqual(expectedItemTypeNum, element.Attribute("itemtype")?.Value,
                    $"itemtype for {element}");
            };
            assertItemTypeNum(paymentItemTypeNum, itemTypeXKeywordElements[0]);
            assertItemTypeNum(stubItemTypeNum, itemTypeXKeywordElements[1]);
            assertItemTypeNum(stubItemTypeNum, itemTypeXKeywordElements[2]);
        }
        [TestMethod]
        public void ItemTypeXKeywordElements_HaveKeyType_FromTheField()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Numeric),
            };
            var clientSetup = Generate(fields);

            // Simplifying assumption: the generator will associate each field with a single item type.
            // So we should have exactly 3 ItemtypexKeyword elements.
            var itemTypeXKeywordElements = clientSetup.ItemTypeXKeywordElements().ToList();
            Assert.AreEqual(3, itemTypeXKeywordElements.Count, "ItemtypexKeyword element count");
            Assert.AreEqual("123", itemTypeXKeywordElements[0].Attribute("keytype")?.Value);
            Assert.AreEqual("124", itemTypeXKeywordElements[1].Attribute("keytype")?.Value);
            Assert.AreEqual("125", itemTypeXKeywordElements[2].Attribute("keytype")?.Value);
        }

        // Keytype elements define the actual fields. They're associated with their parent
        // ItemType via ItemtypexKeyword.

        [TestMethod]
        public void KeyTypeElement_WithNoFieldsPassedIn_HasNoKeyTypeElements()
        {
            var noFields = new Field[] { };
            var clientSetup = Generate(noFields);

            AssertDoesNotHaveElementsMatchingXPath(clientSetup, "/Report/LifeCycle/AllKeytypes/Keytype");
        }
        [TestMethod]
        public void KeyTypeElement_WithSomeFieldsPassedIn_HasOneKeyTypeElementPerField()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Numeric),
            };
            var clientSetup = Generate(fields);

            AssertHasElementsMatchingXPath(clientSetup, "/Report/LifeCycle/AllKeytypes/Keytype",
                withAttributes: new[] {"keytypenum", "keytype", "datatype", "keytypelen", "fieldtype", "extracttype"},
                expectedCount: 3);
        }
        [TestMethod]
        public void KeyTypeElement_KeyTypeNumAttribute_MatchesTheFieldKeyTypeNum()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Numeric),
            };
            var clientSetup = Generate(fields);

            var keyTypeElements = clientSetup.KeyTypeElements().ToList();
            Assert.AreEqual("123", keyTypeElements[0].Attribute("keytypenum")?.Value);
            Assert.AreEqual("124", keyTypeElements[1].Attribute("keytypenum")?.Value);
            Assert.AreEqual("125", keyTypeElements[2].Attribute("keytypenum")?.Value);
        }
        [TestMethod]
        public void KeyTypeElement_KeyTypeAttribute_MatchesTheFieldName()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Numeric),
            };
            var clientSetup = Generate(fields);

            var keyTypeElements = clientSetup.KeyTypeElements().ToList();
            Assert.AreEqual("Check Field 1", keyTypeElements[0].Attribute("keytype")?.Value);
            Assert.AreEqual("Stub Field 1", keyTypeElements[1].Attribute("keytype")?.Value);
            Assert.AreEqual("Stub Field 2", keyTypeElements[2].Attribute("keytype")?.Value);
        }
        [TestMethod]
        public void KeyTypeElement_DataTypeAttribute_MatchesTheFieldDataType()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.Numeric),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.String, length: 40),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Date),
                new Field(SetupDocType.NonCheck, 126, "Stub Field 3", ImageRpsDataType.Currency),
                new Field(SetupDocType.NonCheck, 127, "Stub Field 4", ImageRpsDataType.IdOrSequenceNumber),
            };
            var clientSetup = Generate(fields);

            var keyTypeElements = clientSetup.KeyTypeElements().ToList();
            Assert.AreEqual(5, keyTypeElements.Count);
            Action<ImageRpsDataType, XElement> assertDataType = (dataType, element) =>
            {
                Assert.AreEqual(((int) dataType).ToString(), element.Attribute("datatype")?.Value);
            };
            assertDataType(ImageRpsDataType.Numeric, keyTypeElements[0]);
            assertDataType(ImageRpsDataType.String, keyTypeElements[1]);
            assertDataType(ImageRpsDataType.Date, keyTypeElements[2]);
            assertDataType(ImageRpsDataType.Currency, keyTypeElements[3]);
            assertDataType(ImageRpsDataType.IdOrSequenceNumber, keyTypeElements[4]);
        }
        [TestMethod]
        public void KeyTypeElement_KeyTypeLenAttribute_MatchesTheFieldDataType()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.String, length: 12),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.String, length: 40),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Numeric),
            };
            var clientSetup = Generate(fields);

            var keyTypeElements = clientSetup.KeyTypeElements().ToList();
            Assert.AreEqual("12", keyTypeElements[0].Attribute("keytypelen")?.Value);
            Assert.AreEqual("40", keyTypeElements[1].Attribute("keytypelen")?.Value);
            Assert.AreEqual("0", keyTypeElements[2].Attribute("keytypelen")?.Value);
        }
        [TestMethod]
        public void KeyTypeElement_FieldTypeAttribute_MatchesTheFieldFieldType()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.Numeric, fieldType: "A"),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.Numeric, fieldType: "D"),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Numeric),
            };
            var clientSetup = Generate(fields);

            var keyTypeElements = clientSetup.KeyTypeElements().ToList();
            Assert.AreEqual("A", keyTypeElements[0].Attribute("fieldtype")?.Value);
            Assert.AreEqual("D", keyTypeElements[1].Attribute("fieldtype")?.Value);
            Assert.AreEqual("", keyTypeElements[2].Attribute("fieldtype")?.Value);
        }
        [TestMethod]
        public void KeyTypeElement_ExtractTypeAttribute_MatchesTheFieldExtractType()
        {
            var fields = new[]
            {
                new Field(SetupDocType.Check, 123, "Check Field 1", ImageRpsDataType.Numeric, extractType: "3"),
                new Field(SetupDocType.NonCheck, 124, "Stub Field 1", ImageRpsDataType.Numeric, extractType: "1"),
                new Field(SetupDocType.NonCheck, 125, "Stub Field 2", ImageRpsDataType.Numeric),
            };
            var clientSetup = Generate(fields);

            var keyTypeElements = clientSetup.KeyTypeElements().ToList();
            Assert.AreEqual("3", keyTypeElements[0].Attribute("extracttype")?.Value);
            Assert.AreEqual("1", keyTypeElements[1].Attribute("extracttype")?.Value);
            Assert.AreEqual("", keyTypeElements[2].Attribute("extracttype")?.Value);
        }
    }
}