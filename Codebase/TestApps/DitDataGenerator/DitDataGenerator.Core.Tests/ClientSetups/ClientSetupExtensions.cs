﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.ClientSetups
{
    public static class ClientSetupExtensions
    {
        private static IEnumerable<XElement> Descendants(XContainer parent, XName name)
        {
            var matches = parent.Descendants(name).ToList();
            Assert.AreNotEqual(0, matches.Count, $"Expected at least one {name} element");
            return matches;
        }
        private static XElement Descendant(XContainer parent, XName name)
        {
            var matches = parent.Descendants(name).ToList();
            Assert.AreEqual(1, matches.Count, $"Expected exactly one {name} element");
            return matches.Single();
        }
        public static IEnumerable<XElement> ItemTypeElements(this ClientSetup clientSetup)
        {
            return Descendants(clientSetup.RootElement, "ItemType");
        }
        public static IEnumerable<XElement> ItemTypeLcElements(this ClientSetup clientSetup)
        {
            return Descendants(clientSetup.RootElement, "ItemTypeLC");
        }
        public static IEnumerable<XElement> ItemTypeXKeywordElements(this ClientSetup clientSetup)
        {
            return Descendants(clientSetup.RootElement, "ItemtypexKeyword");
        }
        public static IEnumerable<XElement> KeyTypeElements(this ClientSetup clientSetup)
        {
            return Descendants(clientSetup.RootElement, "Keytype");
        }
        public static XElement LifeCycleElement(this ClientSetup clientSetup)
        {
            return Descendant(clientSetup.RootElement, "LifeCycle");
        }
    }
}