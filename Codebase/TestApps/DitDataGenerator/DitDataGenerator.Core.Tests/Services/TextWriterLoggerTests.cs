﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.Services
{
    [TestClass]
    public class TextWriterLoggerTests
    {
        [TestMethod]
        public void LogEvent()
        {
            var output = new StringWriter();
            ILogger logger = new TextWriterLogger(output);

            logger.LogEvent("Event");

            Assert.AreEqual("INFO:Event\r\n", output.ToString());
        }
        [TestMethod]
        public void LogWarning()
        {
            var output = new StringWriter();
            ILogger logger = new TextWriterLogger(output);

            logger.LogWarning("Warning");

            Assert.AreEqual("WARNING:Warning\r\n", output.ToString());
        }
        [TestMethod]
        public void LogError()
        {
            var output = new StringWriter();
            ILogger logger = new TextWriterLogger(output);

            logger.LogError("Error");

            Assert.AreEqual("ERROR:Error\r\n", output.ToString());
        }
    }
}