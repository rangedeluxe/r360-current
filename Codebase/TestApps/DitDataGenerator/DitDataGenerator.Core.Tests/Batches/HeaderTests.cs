﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DitDataGenerator.Batches;

namespace DitDataGenerator.Batches.Tests
{
    [TestClass]
    public class HeaderTests : BatchTester
    {
        [TestMethod]
        public void ValidPaymentHeader()
        {
            var header = new Header(Convert.ToDateTime("05/30/2017"),1,"Per/Bus Check");

            var headerLines = header.GetHeader().ToList();

            Assert.AreEqual(3,headerLines.Count(), "Per/Bus Check Line Count");
            AssertFieldValue(headerLines, "BEGIN", "");
            AssertFieldValue(headerLines, ">>DocTypeName", "1 - Per/Bus Check");
            AssertFieldValue(headerLines, ">>DocDate", "05/30/2017");
        }

        [TestMethod]
        public void ValidStubHeader()
        {
            var header = new Header(Convert.ToDateTime("05/30/2017"), 1, "Stub");

            var headerLines = header.GetHeader().ToList();

            Assert.AreEqual(3, headerLines.Count(), "Stub Line Count");
            AssertFieldValue(headerLines, "BEGIN", "");
            AssertFieldValue(headerLines, ">>DocTypeName", "1 - Stub");
            AssertFieldValue(headerLines, ">>DocDate", "05/30/2017");
        }
    }
}

