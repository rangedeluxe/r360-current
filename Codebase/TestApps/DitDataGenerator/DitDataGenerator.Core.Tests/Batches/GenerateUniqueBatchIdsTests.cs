﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DitDataGenerator.Exceptions;

namespace DitDataGenerator.Batches
{
    [TestClass]
    public class GenerateUniqueBatchIdsTests
    {
        private WorkgroupHistory _history;

        [TestInitialize]
        public void SetUpWorkgroupHistory()
        {
            _history = new WorkgroupHistory();
            _history.LastBatchId = 3;
            _history.LastBatchIdOfPreviousRun = 3;
            _history.LastBatchNumber = 3;
            _history.LastBatchNumberOfPreviousRun = 3;
            _history.NumberOfBatchFiles = 3;
            _history.MaxBatchId = 3 + 3;
            _history.MaxBatchNumber = 3 + 3;
        }


        [TestMethod]
        public void UniqueBatchIdsTest()
        {
            var batchId = _history.GetNewBatchId();

            Assert.AreEqual(batchId, _history.LastBatchIdOfPreviousRun + 1);

            var batchId2 = _history.GetNewBatchId();

            Assert.AreEqual(batchId2, _history.LastBatchIdOfPreviousRun + 2);
        }

        [TestMethod]
        public void UniqueBatchNumbersTest()
        {
            var batchId = _history.GetNewBatchNumber();

            Assert.AreEqual(batchId, _history.LastBatchNumberOfPreviousRun + 1);

            var batchId2 = _history.GetNewBatchNumber();

            Assert.AreEqual(batchId2, _history.LastBatchNumberOfPreviousRun + 2);
        }


        [TestMethod]
        public void MaxBatchIdsTest()
        {
            bool pass = false;
            // _history.NumberOfBatchFiles is set to 3; when we get 4th the ID an exception should be generated.
            var batchId1 = _history.GetNewBatchId();
            var batchId2 = _history.GetNewBatchId();
            var batchId3 = _history.GetNewBatchId();
            try
            {
                var batchId4 = _history.GetNewBatchId();
            }
            catch (BatchIdentifierLimitReachedException e)
            {
                pass = true;
            }
            Assert.IsTrue(pass);

        }

        [TestMethod]
        public void MaxBatchNumbersTest()
        {
            bool pass = false;
            // _history.NumberOfBatchFiles is set to 3; when we get 4th the an exception should be generated.
            var batchId1 = _history.GetNewBatchNumber();
            var batchId2 = _history.GetNewBatchNumber();
            var batchId3 = _history.GetNewBatchNumber();
            try
            {
                var batchId4 = _history.GetNewBatchNumber();
            }
            catch (BatchIdentifierLimitReachedException e)
            {
                pass = true;
            }
            Assert.IsTrue(pass);
        }
    }
}
