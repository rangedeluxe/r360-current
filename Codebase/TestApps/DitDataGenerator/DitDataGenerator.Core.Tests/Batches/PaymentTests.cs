﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.Batches.Tests
{
    [TestClass]
    public class PaymentTests : BatchTester
    {
        private Payment CreatePayment(double appliedAmount = 0.0, string accountNumber = "AccountNumber", string rt = "rt", string serialNumber = "SerialNumber")
        {
            return new Payment()
            {
                AppliedAmount = appliedAmount,
                AccountNumber = accountNumber,
                RT = rt,
                SerialNumber = serialNumber
            };
        }

        [TestMethod]
        public void GetPaymentTest()
        {
            var payment = CreatePayment(appliedAmount: 5.00, accountNumber: "123", rt: "456", serialNumber: "789");

            var paymentLines = payment.GetPayment().ToList();
            Assert.AreEqual(4, paymentLines.Count(), "Payment Line Count");
            AssertFieldValue(paymentLines, "Applied Amount", "5.00");
            AssertFieldValue(paymentLines, "Account Number", "123");
            AssertFieldValue(paymentLines, "RT", "456");
            AssertFieldValue(paymentLines, "Per Check Number", "789");
        }

        [TestMethod]
        public void PaymentAppliedAmountTest()
        {
            var payment = CreatePayment(appliedAmount: 1.00);

            Assert.AreEqual(1.00, payment.AppliedAmount, "Applied Amount Test");
        }

        [TestMethod]
        public void AccountNumberTest()
        {
            var payment = CreatePayment(accountNumber: "1234");

            Assert.AreEqual("1234", payment.AccountNumber, "Account Number Test");
        }

        [TestMethod]
        public void RTTest()
        {
            var payment = CreatePayment(rt: "1234");

            Assert.AreEqual("1234", payment.RT, "RT Test");
        }

        [TestMethod]
        public void SerialNumberTest()
        {
            var payment = CreatePayment(serialNumber: "1234");

            Assert.AreEqual("1234", payment.SerialNumber, "Serial Number Test");
        }
    }
}
