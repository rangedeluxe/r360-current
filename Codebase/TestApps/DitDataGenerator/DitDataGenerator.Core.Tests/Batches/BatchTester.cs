﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.Batches.Tests
{
    public class BatchTester
    {
        public void AssertFieldValue(List<string> dataLines,string keyword, string value)
        {
            var dataLine = dataLines.Find(item => item.StartsWith($"{keyword}:"));
            Assert.AreEqual($"{keyword}:{value}", dataLine, keyword, $"AssertFieldValue {keyword}");
        }
    }
}