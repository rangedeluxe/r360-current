﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.Batches.Tests
{
    [TestClass]
    public class StubTests : BatchTester
    {

        private Stub CreateStub(double appliedAmount = 0.0)
        {
            return new Stub()
            {
                AppliedAmount = appliedAmount
            };
        }

        [TestMethod]
        public void GetStubTest()
        {
            var stub = CreateStub(appliedAmount: 5.00);

            var stubLines = stub.GetStub().ToList();
            Assert.AreEqual(1, stubLines.Count(), "Stub Line Count");
            AssertFieldValue(stubLines, "Applied Amount", "5.00");
        }
        [TestMethod]
        public void StubAppliedAmountTest()
        {
            var stub = CreateStub(appliedAmount: 1.00);

            Assert.AreEqual(1.00, stub.AppliedAmount, "Applied Amount Test");
        }

        [TestMethod]
        public void StugDataEntryFieldsTest()
        {
            var stub = CreateStub(appliedAmount: 100.00);

            stub.DataEntryFields.Add(new DataEntryField("Field1", "Value1"));
            stub.DataEntryFields.Add(new DataEntryField("Field2", "Value2"));
            stub.DataEntryFields.Add(new DataEntryField("Field3", "Value3"));

            var stubDataEntryLines = stub.GetDataEntryFields().ToList();

            Assert.AreEqual(3, stubDataEntryLines.Count());
            AssertFieldValue(stubDataEntryLines, "Field1", "Value1");
            AssertFieldValue(stubDataEntryLines, "Field2", "Value2");
            AssertFieldValue(stubDataEntryLines, "Field3", "Value3");
        }
    }
}
