﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.Batches.Tests
{
    [TestClass]
    public class TrailerTests : BatchTester
    {
        [TestMethod]
        public void ValidTrailerTest()
        {
            var trailer = new Trailer(0, 0, 2, 0, 0, 300, 300,
                new Dictionary<string, string> {{">>FullPath", "C:\\TestPath\\TestFileName.tif"}});


        var trailerLines = trailer.GetTrailer().ToList();

            Assert.AreEqual(8, trailerLines.Count(), "TrailerLineCount");
            AssertFieldValue(trailerLines, ">>DocRevNum", "0");
            AssertFieldValue(trailerLines, ">>ItemPageNum", "0");
            AssertFieldValue(trailerLines, ">>FileTypeNum", "2");
            AssertFieldValue(trailerLines, ">>ImageType", "0");
            AssertFieldValue(trailerLines, ">>Compress", "0");
            AssertFieldValue(trailerLines, ">>Xdpi", "300");
            AssertFieldValue(trailerLines, ">>Ydpi", "300");
            AssertFieldValue(trailerLines, ">>FullPath", "C:\\TestPath\\TestFileName.tif");
        }        
    }
}
