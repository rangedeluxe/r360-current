using System.Collections.Generic;
using System.Linq;

namespace DitDataGenerator.Batches
{
    public static class Extensions
    {
        public static bool AnyItemBeginsWith(this List<string> list, string item)
        {
            return list.Any(listItem => listItem.StartsWith(item));
        }
    }
}