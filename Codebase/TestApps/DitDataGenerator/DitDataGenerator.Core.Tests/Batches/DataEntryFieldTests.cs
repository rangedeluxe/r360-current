﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.Batches.Tests
{
    [TestClass]
    public class DataEntryFieldTests
    {
        [TestMethod]
        public void DataEntryField()
        {
            var dataEntryField = new DataEntryField("Test1","Value1");

            Assert.AreEqual("Test1:Value1",dataEntryField.GetDataEntryField());
        }
    }
}
