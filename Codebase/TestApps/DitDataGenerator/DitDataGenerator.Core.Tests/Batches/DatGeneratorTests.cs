﻿using System.Collections.Generic;
using System.Linq;
using DitDataGenerator.Helpers;
using DitDataGenerator.Images;
using DitDataGenerator.Metadata;
using ImageGenerators.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Wfs.Logging.Performance;
using WFS.integraPAY.Online.ICONCommon;

namespace DitDataGenerator.Batches
{
    [TestClass]
    public class DatGeneratorTests
    {
        private readonly Dictionary<string, string> _fileNames = new Dictionary<string, string>
        {
            {"BitonalFront", "bf"},
            {"BitonalBack", "bb"},
            {"ColorFront", "cf"},
            {"ColorBack", "cb"},
            {"GrayFront", "gf"},
            {"GrayBack", "gb"}
        };
        private string BitonalFrontFile => $"{cMagicKeywords.AABitonalFrontPath}:{_fileNames["BitonalFront"]}";
        private string BitonalBackFile => $"{cMagicKeywords.AABitonalBackPath}:{_fileNames["BitonalBack"]}";
        private string ColorFrontFile => $"{cMagicKeywords.AAColorFrontPath}:{_fileNames["ColorFront"]}";
        private string ColorBackFile => $"{cMagicKeywords.AAColorBackPath}:{_fileNames["ColorBack"]}";
        private string GrayFrontFile => $"{cMagicKeywords.AAGrayFrontPath}:{_fileNames["GrayFront"]}";
        private string GrayBackFile => $"{cMagicKeywords.AAGrayBackPath}:{_fileNames["GrayBack"]}";
        private string MultiFile => $"{cMagicKeywords.AAFullPath}:{_fileNames["BitonalFront"]}";

        public DatGenerator CreateDataGenerator(TiffImageFilesOption tiffImageFilesOption)
        {
            var performanceLogger = Substitute.For<IPerformanceLogger>();
            var fileBuilder = Substitute.For<IFileBuilder>();
            var percentageImageColorPicker =
                new PercentageImageColorPicker(new ImageTypePercentages {PercentAllTypesImages = 100},
                    new RandomPercentageGenerator());

            var dataGenerator = new DatGenerator(fileBuilder: fileBuilder, imageOutputDirectory: @"..",
                itemCountPerBatch: 1, numberBatchFiles: 1, workgroupIdPrefix: 9800, workgroupNamePrefix: "77", siteCode: 65, workGroupCount: 500,
                performanceLogger: performanceLogger, fields: DefaultFieldProvider.GetFields(),
                tiffImageFilesSetting: tiffImageFilesOption,  percentageImageColorPicker: percentageImageColorPicker, singleWorkgroup: false)
            {
                BitonalImageGenerator = Substitute.For<IImageGenerator>(),
                ColorImageGenerator = Substitute.For<IImageGenerator>(),
                GrayImageGenerator = Substitute.For<IImageGenerator>()
            };

            dataGenerator.BitonalImageGenerator.BuildCheckImage(Arg.Any<ImageParameters>(),Arg.Any<bool>(), Arg.Any<bool>()).Returns(new CheckDetails { FrontImageFileName = _fileNames["BitonalFront"], BackImageFileName = _fileNames["BitonalBack"] });
            dataGenerator.ColorImageGenerator.BuildCheckImage(Arg.Any<ImageParameters>(), Arg.Any<bool>(), Arg.Any<bool>()).Returns(new CheckDetails { FrontImageFileName = _fileNames["ColorFront"], BackImageFileName = _fileNames["ColorBack"] });
            dataGenerator.GrayImageGenerator.BuildCheckImage(Arg.Any<ImageParameters>(), Arg.Any<bool>(), Arg.Any<bool>()).Returns(new CheckDetails { FrontImageFileName = _fileNames["GrayFront"], BackImageFileName = _fileNames["GrayBack"] });

            dataGenerator.BitonalImageGenerator.BuildInvoiceImage(Arg.Any<ImageParameters>(), Arg.Any<bool>(), Arg.Any<bool>()).Returns(new InvoiceData { FrontImageFileName = _fileNames["BitonalFront"], BackImageFileName = _fileNames["BitonalBack"] });
            dataGenerator.ColorImageGenerator.BuildInvoiceImage(Arg.Any<ImageParameters>(), Arg.Any<bool>(), Arg.Any<bool>()).Returns(new InvoiceData { FrontImageFileName = _fileNames["ColorFront"], BackImageFileName = _fileNames["ColorBack"] });
            dataGenerator.GrayImageGenerator.BuildInvoiceImage(Arg.Any<ImageParameters>(), Arg.Any<bool>(), Arg.Any<bool>()).Returns(new InvoiceData { FrontImageFileName = _fileNames["GrayFront"], BackImageFileName = _fileNames["GrayBack"] });

            return dataGenerator;
        }

        [TestMethod]
        public void GenerateCheckSingleFileTest()
        {
            var dataGenerator = CreateDataGenerator(TiffImageFilesOption.SinglePage);
            dataGenerator.BuildAllBatchFiles();

            dataGenerator.BitonalImageGenerator.Received().BuildCheckImage(Arg.Any<ImageParameters>(),true,false);
            dataGenerator.ColorImageGenerator.Received().BuildCheckImage(Arg.Any<ImageParameters>(), true, false);
            dataGenerator.GrayImageGenerator.Received().BuildCheckImage(Arg.Any<ImageParameters>(), true, false);
            var fileContent = dataGenerator.DatFileData.FileContent;

            // Count is two because we are checking for Check and stub.
            Assert.AreEqual(2, fileContent.Count(n => n == BitonalFrontFile));
            Assert.AreEqual(2, fileContent.Count(n => n == BitonalBackFile));
            Assert.AreEqual(2, fileContent.Count(n => n == ColorFrontFile));
            Assert.AreEqual(2, fileContent.Count(n => n == ColorBackFile));
            Assert.AreEqual(2, fileContent.Count(n => n == GrayFrontFile));
            Assert.AreEqual(2, fileContent.Count(n => n == GrayBackFile));
            Assert.IsFalse(fileContent.AnyItemBeginsWith(">>FullPath:"));
        }
        [TestMethod]
        public void GenerateCheckMultiFileTest()
        {
            var dataGenerator = CreateDataGenerator(TiffImageFilesOption.MultiPage);
            dataGenerator.BuildAllBatchFiles();

            dataGenerator.BitonalImageGenerator.Received().BuildCheckImage(Arg.Any<ImageParameters>());
            dataGenerator.ColorImageGenerator.DidNotReceive().BuildCheckImage(Arg.Any<ImageParameters>());
            dataGenerator.GrayImageGenerator.DidNotReceive().BuildCheckImage(Arg.Any<ImageParameters>());
            var fileContent = dataGenerator.DatFileData.FileContent;

            CheckForNotContainSingleFiles(fileContent);
            // Count is two because we are checking for Check and stub.
            Assert.AreEqual(2, fileContent.Count(n => n == MultiFile));
        }

        [TestMethod]
        public void GenerateCheckNoImagesFileTest()
        {
            var dataGenerator = CreateDataGenerator(TiffImageFilesOption.NoImages);
            dataGenerator.BuildAllBatchFiles();

            dataGenerator.BitonalImageGenerator.DidNotReceive().BuildCheckImage(Arg.Any<ImageParameters>());
            dataGenerator.ColorImageGenerator.DidNotReceive().BuildCheckImage(Arg.Any<ImageParameters>());
            dataGenerator.GrayImageGenerator.DidNotReceive().BuildCheckImage(Arg.Any<ImageParameters>());
            var fileContent = dataGenerator.DatFileData.FileContent;

            CheckForNotContainSingleFiles(fileContent);
            Assert.IsFalse(fileContent.AnyItemBeginsWith(">>FullPath:"));
        }

        private void CheckForNotContainSingleFiles(List<string> fileContent)
        {
            CollectionAssert.DoesNotContain(fileContent, BitonalFrontFile);
            CollectionAssert.DoesNotContain(fileContent, BitonalBackFile);
            CollectionAssert.DoesNotContain(fileContent, ColorFrontFile);
            CollectionAssert.DoesNotContain(fileContent, ColorBackFile);
            CollectionAssert.DoesNotContain(fileContent, GrayFrontFile);
            CollectionAssert.DoesNotContain(fileContent, GrayBackFile);
        }
    }
}