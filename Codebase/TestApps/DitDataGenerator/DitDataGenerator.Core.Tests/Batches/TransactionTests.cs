﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.Batches.Tests
{
    [TestClass]
    public class TransactionTests : BatchTester
    {
        private Transaction CreateTransaction(int sequenceNumber = 1, int transactionNumber = 1)
        {
            return new Transaction()
            {
                SequenceNumber = sequenceNumber,
                TransactionNumber = transactionNumber
            };
        }

        [TestMethod]
        public void GetTransactionTest()
        {
            var transaction = CreateTransaction(sequenceNumber: 1, transactionNumber: 1);

            var transactionLines = transaction.GetTransaction().ToList();

            Assert.AreEqual(2, transactionLines.Count(), "Transaction Line Count");
            AssertFieldValue(transactionLines, "Transaction Number", "1");
            AssertFieldValue(transactionLines, "Sequence Number", "1");
        }

        [TestMethod]
        public void SequenceNumberTest()
        {
            var transaction = CreateTransaction(sequenceNumber: 5);

            Assert.AreEqual(5, transaction.SequenceNumber, "Sequence Number Test");
        }

        [TestMethod]
        public void TransactionNumberTest()
        {
            var transaction = CreateTransaction(transactionNumber: 5);

            Assert.AreEqual(5, transaction.TransactionNumber, "Transaction Number Test");
        }

        [TestMethod]
        public void TransactionPaymentTest()
        {
            var transaction = CreateTransaction();

            transaction.Payments.Add(new Payment
            {
                AppliedAmount = 1.00,
                AccountNumber = "123",                
                RT = "456",
                SerialNumber = "789"
            });

            Assert.AreEqual(1, transaction.Payments.Count, "Payment Count");
            Assert.AreEqual(1.00, transaction.Payments[0].AppliedAmount, "AppliedAmount");
            Assert.AreEqual("123", transaction.Payments[0].AccountNumber, "AccountNumber");            
            Assert.AreEqual("456", transaction.Payments[0].RT, "RT");
            Assert.AreEqual("789", transaction.Payments[0].SerialNumber, "SerialNumber");
        }

        [TestMethod]
        public void TransactionStubTest()
        {
            var transaction = CreateTransaction();

            transaction.Stubs.Add(new Stub
            {
                AppliedAmount = 1.00
            });

            Assert.AreEqual(1, transaction.Stubs.Count, "Stub Count");
            Assert.AreEqual(1.00, transaction.Stubs[0].AppliedAmount, "AppliedAmount");
        }
    }
}
