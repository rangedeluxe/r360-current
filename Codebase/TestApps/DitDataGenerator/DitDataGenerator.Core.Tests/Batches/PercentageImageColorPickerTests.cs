﻿using System;
using System.Collections.Generic;
using System.Linq;
using DitDataGenerator.Images;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using NSubstitute.Core;

namespace DitDataGenerator.Batches
{
    [TestClass]
    public class PercentageImageColorPickerTests
    {
        [TestMethod]
        public void PickAllThreePercentCombinationsTest()
        {
            var percentages = new ImageTypePercentages
            {
                PercentBitonalAndColorImages = 25,
                PercentAllTypesImages = 25,
                PercentBitonalAndGrayImages = 25
            };

            var picker = GetPercentageImageColorPicker(percentages, PreDefinedPercentages.GetNextUniformPercentage);

            List<ColourEncoding> colourEncodingPicked = new List<ColourEncoding>();
            for (var i = 0; i < 12; i++)
            {
                colourEncodingPicked.AddRange(picker.GetNextImageColorEncodings());
            }

            Assert.AreEqual(25, colourEncodingPicked.Count);

            // PercentBitonalAndColorImages = 25,PercentAllTypesImages = 25,PercentBitonalAndGrayImages = 25, bitonal 25%
            // below 25% {0, 1, 10, 20},  above 25 abd below 50% {30, 40, 50} and above 50% below 75 {60, 70} above 75% { 80, 90, 99}

            Assert.AreEqual(7, colourEncodingPicked.Count(e => e == ColourEncoding.Color));
            Assert.AreEqual(6, colourEncodingPicked.Count(e => e == ColourEncoding.GrayScale));
            Assert.AreEqual(12, colourEncodingPicked.Count(e => e == ColourEncoding.Bitonal));

        }

        [TestMethod]
        public void PickTwoPercentCombinationsTest()
        {
            var percentages = new ImageTypePercentages
            {
                PercentBitonalAndColorImages = 25,
                PercentAllTypesImages = 0,
                PercentBitonalAndGrayImages = 25
            };

            var picker = GetPercentageImageColorPicker(percentages, PreDefinedPercentages.GetNextNonUniformPercentage);

            List<ColourEncoding> colourEncodingPicked = new List<ColourEncoding>();
            for (var i = 0; i < 30; i++)
            {
                colourEncodingPicked.AddRange(picker.GetNextImageColorEncodings());
            }

            Assert.AreEqual(48, colourEncodingPicked.Count);

            // PercentBitonalAndColorImages = 25,PercentAllTypesImages = 0,PercentBitonalAndGrayImages = 25, bitonal 50%
            //{ 0, 2, 88, 1, 5, 17, 28, 10, 9, 0, 20, 65, 30, 77, 40, 99, 36, 50, 23, 60, 98, 70, 3, 80, 48, 90, 60, 99,32, 12}
            // below 25% {0,2,1,5,17,10,9,0,20,23,3,12}  above 25 abd below 50% {28,30,40,36,50,48,32,50} and above 50% below 75 {60,65,70,60} above 75% { 88,77,99,98,80,90,99}

            Assert.AreEqual(6, colourEncodingPicked.Count(e => e == ColourEncoding.Color));
            Assert.AreEqual(12, colourEncodingPicked.Count(e => e == ColourEncoding.GrayScale));
            Assert.AreEqual(30, colourEncodingPicked.Count(e => e == ColourEncoding.Bitonal));

        }

        private PercentageImageColorPicker GetPercentageImageColorPicker(ImageTypePercentages percentages,
            Func<CallInfo, int> getNextPercentage)
        {
            IRandomPercentageGenerator percentageGenerator = Substitute.For<IRandomPercentageGenerator>();
            percentageGenerator.GetRandomPercent().Returns(getNextPercentage);

            return new PercentageImageColorPicker(percentages, percentageGenerator);
        }

        private static class PreDefinedPercentages
        {
            private static readonly int[] Uniform = {0, 1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 99};

            private static readonly int[] NonUniform =
            {
                0, 2, 88, 1, 5, 17, 28, 10, 9, 0, 20, 65, 30, 77, 40, 99, 36, 50, 23, 60, 98, 70, 3, 80, 48, 90, 60, 99,
                32, 12
            };

            private static int _index;

            public static int GetNextUniformPercentage(CallInfo arg)
            {
                if (_index == Uniform.Length) _index = 0;
                return Uniform[_index++];
            }

            public static int GetNextNonUniformPercentage(CallInfo arg)
            {
                if (_index == NonUniform.Length) _index = 0;
                return NonUniform[_index++];
            }
        }
    }
}
