﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DitDataGenerator.Batches;

namespace DitDataGenerator.Batches.Tests
{
    [TestClass]
    public class BatchTests : BatchTester
    {
        private Batch CreateBatch(DateTime? batchDate = null, DateTime? depositDate = null, DateTime? systemDate = null, 
            int batchId = 1, int batchNumber = 1, int clientId = 1, string clientName = "ClientName", int siteId = 1)
        {
            return new Batch(batchDate ?? DateTime.Now, depositDate ?? DateTime.Now, systemDate ?? DateTime.Now, batchId, batchNumber, clientId, clientName, siteId);
        }

        [TestMethod]
        public void BatchDatePropertyTest()
        {
            Batch batch = CreateBatch(batchDate: Convert.ToDateTime("01/01/2017"));

            Assert.AreEqual(Convert.ToDateTime("01/01/2017"), batch.BatchDate, "Batch Date Property Test");
        }

        [TestMethod]
        public void ClientIdPropertyTest()
        {
            Batch batch = CreateBatch(clientId: 4);

            Assert.AreEqual(4, batch.ClientId, "Client Id Property Test");
        }

        [TestMethod]
        public void GetBatchTest()
        {
            Batch batch = CreateBatch(batchDate: Convert.ToDateTime("01/01/2017"), depositDate: Convert.ToDateTime("01/02/2017"), systemDate: Convert.ToDateTime("01/01/2017"), 
                batchId: 500, batchNumber: 100, clientId: 25, clientName: "Unit Test Client Name", siteId: 15);

            var batchLines = batch.GetBatch().ToList();

            Assert.AreEqual(11,batchLines.Count(),"BatchLineCount");
            AssertFieldValue(batchLines, "Batch Date", "01/01/2017");
            AssertFieldValue(batchLines, "Deposit Date", "01/02/2017");
            AssertFieldValue(batchLines, "System Date", "01/01/2017");
            AssertFieldValue(batchLines, "Process Date", "01/01/2017");
            AssertFieldValue(batchLines, "Receive Date", "01/01/2017");
            AssertFieldValue(batchLines, "Consolidation Date", "01/01/2017");
            AssertFieldValue(batchLines, "Batch ID", "500");
            AssertFieldValue(batchLines, "Batch Number", "100");
            AssertFieldValue(batchLines, "Client ID", "25");
            AssertFieldValue(batchLines, "Client Name", "Unit Test Client Name");
            AssertFieldValue(batchLines, "Site ID", "15");
        }

        [TestMethod]
        public void BatchDateTest()
        {
            Batch batch = CreateBatch(batchDate: Convert.ToDateTime("01/01/2017"));

            Assert.AreEqual("01/01/2017", batch.BatchData["Batch Date"], "Batch Date Test");
        }

        [TestMethod]
        public void DepositDateTest()
        {
            Batch batch = CreateBatch(depositDate: Convert.ToDateTime("01/01/2017"));

            Assert.AreEqual("01/01/2017", batch.BatchData["Deposit Date"], "Deposit Date Test");
        }

        [TestMethod]
        public void SystemDateTest()
        {
            Batch batch = CreateBatch(systemDate: Convert.ToDateTime("01/01/2017"));

            Assert.AreEqual("01/01/2017", batch.BatchData["System Date"], "System Date");
            Assert.AreEqual("01/01/2017", batch.BatchData["Process Date"], "Processing Date");
            Assert.AreEqual("01/01/2017", batch.BatchData["Receive Date"], "Receive Date");
            Assert.AreEqual("01/01/2017", batch.BatchData["Consolidation Date"], "Consolidation Date");
        }

        [TestMethod]
        public void BatchIdTest()
        {
            Batch batch = CreateBatch(batchId: 500);

            Assert.AreEqual("500", batch.BatchData["Batch ID"], "BatchId Test");
        }

        [TestMethod]
        public void ClientNameTest()
        {
            Batch batch = CreateBatch(clientName: "Unit Test Customer");

            Assert.AreEqual("Unit Test Customer", batch.BatchData["Client Name"], "Client Name Test");
        }

        [TestMethod]
        public void SiteIdTest()
        {
            Batch batch = CreateBatch(siteId: 50);

            Assert.AreEqual("50", batch.BatchData["Site ID"], "SiteId Test");
        }

        [TestMethod]
        public void BatchTransactionTest()
        {
            Batch batch = CreateBatch();

            batch.Transactions.Add(new Transaction
            {
                SequenceNumber = 1,
                TransactionNumber = 1
            });

            batch.Transactions.Add(new Transaction
            {
                SequenceNumber = 2,
                TransactionNumber = 1
            });

            Assert.AreEqual(2,batch.Transactions.Count,"Transaction Count");
            Assert.AreEqual(1, batch.Transactions[0].SequenceNumber, "Transaction 1, Sequence Number 1, Sequence Number");
            Assert.AreEqual(1, batch.Transactions[0].TransactionNumber, "Transaction 1, Sequence Number 1, Transaction Number");

            Assert.AreEqual(2, batch.Transactions[1].SequenceNumber, "Transaction 1, Sequence Number 2, Sequence Number");
            Assert.AreEqual(1, batch.Transactions[1].TransactionNumber, "Transaction 1, Sequence Number 2, Transaction Number");
        }
    }
}
