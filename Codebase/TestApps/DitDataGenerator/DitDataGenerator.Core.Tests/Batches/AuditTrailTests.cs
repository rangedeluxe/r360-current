﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DitDataGenerator.Batches.Tests
{
    [TestClass]
    public class AuditTrailTests : BatchTester
    {        
        [TestMethod]
        public void GetAuditTrailTest()
        {
            var auditTrail = new AuditTrail("123456","98764","Check");

            var auditTrailLines = auditTrail.GetAuditTrail().ToList();
            Assert.AreEqual(21, auditTrailLines.Count(), "Audit Trail Line Count");
            AssertFieldValue(auditTrailLines, "Endpoint ABA", "123456");
            AssertFieldValue(auditTrailLines, "Endpoint DDA", "98764");
            AssertFieldValue(auditTrailLines, "Document Type", "Check");
        }
    }
}
