﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HuntingtonKeepAlive
{
    public partial class KeepActiveCookie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var statusCodeSetting = ConfigurationManager.AppSettings.Get("httpresponsestatuscode");
            var statusCode = 200;
            if (!string.IsNullOrEmpty(statusCodeSetting)) { int.TryParse(statusCodeSetting, out statusCode); }

            Request.RequestContext.HttpContext.Response.StatusCode = statusCode;
            
        }
    }
}