using System;

namespace ImageGenerators.Common
{
    public class ImageGeneratorException : Exception
    {
        /// <summary>
        /// To raise ImageGeneratorException and send the actual exception back as inner exception.
        /// </summary>
        /// <param name="message">Holds the new exception message</param>
        /// <param name="innerException">To pass the inner exception</param>
        public ImageGeneratorException(String message, Exception innerException) : base(message, innerException)
        {

        }

        /// <summary>
        /// standard exception.
        /// </summary>       
        public ImageGeneratorException(String message) : base(message)
        {

        }
    }
}