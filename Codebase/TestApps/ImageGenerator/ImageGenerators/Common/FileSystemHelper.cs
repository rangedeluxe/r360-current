﻿using System;
using System.IO;
using System.Security;

namespace ImageGenerators.Common
{
    public static class FileSystemHelper
    {
        /// <summary>
        /// The set of invalid filename characters.
        /// </summary>
        private static readonly char[] InvalidFilenameChars;
        /// <summary>
        /// The set of invalid path characters.
        /// </summary>
        private static readonly char[] InvalidPathChars;

        static FileSystemHelper()
        {
            InvalidFilenameChars = Path.GetInvalidFileNameChars();
            InvalidPathChars = Path.GetInvalidPathChars();
        }

        public static bool ValidateFileName(string fileName)
        {
            try
            {
                var theFileName = Path.GetFileName(fileName);
                var path = Path.GetFullPath(fileName);
                return (ValidateString(theFileName, InvalidFilenameChars)
                        &&
                        ValidatePath(path)
                );
            }
            catch (SecurityException ex)
            {
                //The caller does not have the required permissions. 
                throw ex;
            }
            catch (Exception)
            {
                //Don’t throw exception, juts return false; let the calling party decide how to handle invalid path. 
                return false;
            }

        }

        public static bool ValidatePath(string path)
        {
            return ValidateString(path, InvalidPathChars);
        }

        private static bool ValidateString(string input, char[] invalidChars)
        {
            return !string.IsNullOrWhiteSpace(input) && input.IndexOfAny(invalidChars) == -1;
        }

    }
}
