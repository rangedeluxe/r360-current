namespace ImageGenerators.Common
{
    public enum LockboxColorMode
    {
        COLOR_MODE_BITONAL = 1,
        COLOR_MODE_COLOR = 2,
        COLOR_MODE_GRAYSCALE = 4,
    }
}