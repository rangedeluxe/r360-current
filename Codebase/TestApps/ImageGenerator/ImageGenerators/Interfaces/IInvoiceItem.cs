﻿namespace ImageGenerators.Interfaces
{
    public interface IInvoiceItem
    {
        string ItemCode { get; set; }
        int? Quantity { get; set; }
        decimal? UnitPrice { get; set; }
    }
}