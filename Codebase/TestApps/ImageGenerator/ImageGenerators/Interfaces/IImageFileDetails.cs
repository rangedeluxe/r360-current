﻿using System.Drawing.Imaging;

namespace ImageGenerators.Interfaces
{
    public interface IImageFileDetails
    {
        string FrontImageFileName { get; set; }

        string BackImageFileName { get; set; }

        ImageFormat ImageFormat { get; set; }

    }
}