﻿using ImageGenerators.Model;

namespace ImageGenerators.Interfaces
{
    public interface IInvoiceDetails:IImageFileDetails
    {
        decimal? Amount { get; set; }
        string Company { get; set; }
        string CustomerName { get; set; }
        string CustomerNumber { get; set; }
        bool CombineFrontAndBackImages { get; set; }
        string Date { get; set; }
        bool IncludeBack { get; set; }
        string InvoiceNumber { get; set; }
        System.Collections.Generic.List<InvoiceItem> Items { get; set; }
        string OrderNumber { get; set; }
        string Payee { get; set; }
        string Payer { get; set; }       
    }
}