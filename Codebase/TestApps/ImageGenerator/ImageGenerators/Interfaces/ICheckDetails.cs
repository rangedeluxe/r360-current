﻿using System.Xml.Schema;

namespace ImageGenerators.Interfaces
{
    public interface ICheckDetails : IImageFileDetails
    {
        string Account { get; set; }
        decimal? Amount { get; set; }
        string CheckNumber { get; set; }
        string Date { get; set; }
        bool IncludeBack { get; set; }
        bool CombineFrontAndBackImages { get; set; }
        string Memo { get; set; }
        string Payee { get; set; }
        string Payer { get; set; }
        string RoutingNumber { get; set; }
        string TransactionCode { get; set; }
        string WatermarkText { get; set; }
    }
}