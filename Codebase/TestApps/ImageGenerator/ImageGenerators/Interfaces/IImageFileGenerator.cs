﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImageGenerators.Interfaces
{
    public interface IImageFileGenerator : IDisposable
    {

        //Image details
        int ImageWidth { get; set; }
        int ImageHeight { get; set; }
        int XResolution { get; set; }
        int YResolution { get; set; }        
        Bitmap FrontBmp { get; }
        Bitmap BackBmp { get; }        
        void SetImageDefaults();
        void Validate();
        void CreateImageFiles();
    }
}
