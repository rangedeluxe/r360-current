using System;
using System.Drawing;
using System.Drawing.Imaging;
using ImageGenerators.Common;
using ImageGenerators.Interfaces;

namespace ImageGenerators.Model
{
    public class InvoiceImageGenerator : IImageFileGenerator
    {

        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public int XResolution { get; set; }
        public int YResolution { get; set; }
        public ImageFormat ImageFormat { get; set; }
        public Bitmap FrontBmp { get; }
        public Bitmap BackBmp { get; }
        public string FrontImageFileName { get; set; }
        public string BackImageFileName { get; set; }

        public IInvoiceDetails InvoiceDetails { get; set; }

        public void SetImageDefaults()
        {
            // Implement this once ImageGeneratorWrapper is refactored.
        }

        public void Validate()
        {

            if (InvoiceDetails == null)
            {
                throw new ImageGeneratorException("Check Details are missing.");
            }
            if (!FileSystemHelper.ValidateFileName(InvoiceDetails.FrontImageFileName))
            {

                throw new ImageGeneratorException("Front Image File Name is invalid.");
            }

            //BackImageFileName not a required filed, validate it only if it exists. Use backing field not the property.
            if (!String.IsNullOrWhiteSpace(InvoiceDetails.BackImageFileName))
            {
                if (!FileSystemHelper.ValidateFileName(InvoiceDetails.BackImageFileName))
                {
                    throw new ImageGeneratorException("Back Image File Name has invalid characters.");
                }
            }

            if (string.IsNullOrWhiteSpace(InvoiceDetails.OrderNumber) || string.IsNullOrWhiteSpace(InvoiceDetails.InvoiceNumber))
            {
                throw new ImageGeneratorException("Invoice Number and Order Number are required.");
            }

        }

        public void CreateImageFiles()
        {
            Validate();
            ImageGeneratorWrapper.GenerateInvoiceImage(InvoiceDetails);
        }

        public void Dispose()
        {
            // Implement this once ImageGeneratorWrapper is refactored.
        }
    }
}