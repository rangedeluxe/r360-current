﻿using System;
using System.Drawing.Imaging;
using System.IO;
using ImageGenerators.Interfaces;

namespace ImageGenerators.Model
{
    public class CheckDetails : ICheckDetails
    {
        public CheckDetails()
        {
            ImageFormat = ImageFormat.Tiff;
            IncludeBack = false;
            CombineFrontAndBackImages = false;
        }
        private string _backImageFileName;

        public string CheckNumber { get; set; }

        public string RoutingNumber { get; set; }

        public string Account { get; set; }

        public string TransactionCode { get; set; }

        public decimal? Amount { get; set; }

        public string Payee { get; set; }

        public string Payer { get; set; }

        public string WatermarkText { get; set; }

        public string Date { get; set; }

        public bool CombineFrontAndBackImages { get; set; }
        public string Memo { get; set; }

        public bool IncludeBack { get; set; }

        public string FrontImageFileName { get; set; }

        public string BackImageFileName
        {
            get
            {
                //if BackImageFileName is empty add _back to FrontImageFileName and use it.
                if (IncludeBack && String.IsNullOrEmpty(_backImageFileName))
                {
                    // Default back file name to *_back.*
                    if (FrontImageFileName != null)
                    {
                        return Path.Combine(
                            path1: Path.GetDirectoryName(path: FrontImageFileName), path2: Path.GetFileNameWithoutExtension(FrontImageFileName)
                                                                                           + "_back" + Path.GetExtension(FrontImageFileName)
                        );
                    }
                }

                return _backImageFileName;
            }
            set { _backImageFileName = value; }
        }
        public ImageFormat ImageFormat { get; set; }
    }
}
