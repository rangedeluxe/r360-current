﻿using System;
using System.Drawing;
using ImageGenerators.Common;
using ImageGenerators.Interfaces;

namespace ImageGenerators.Model
{
    public class CheckImageGenerator : IImageFileGenerator
    {
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public int XResolution { get; set; }
        public int YResolution { get; set; }
        public Bitmap FrontBmp { get; }
        public Bitmap BackBmp { get; }

        public ICheckDetails CheckDetails { get; set; }

        public void Dispose()
        {
            // Implement this once ImageGeneratorWrapper is refactored.
        }
        public void SetImageDefaults()
        {
            // Implement this once ImageGeneratorWrapper is refactored.
        }

        public void Validate()
        {
            if (CheckDetails == null)
            {
                throw new ImageGeneratorException("Check Details are missing.");
            }
            if (!FileSystemHelper.ValidateFileName(CheckDetails.FrontImageFileName))
            {

                throw new ImageGeneratorException("Front Image File Name is invalid.");
            }

            //BackImageFileName is not a required filed, validate it only if it exists.
            if (!String.IsNullOrWhiteSpace(CheckDetails.BackImageFileName))
            {
                if (!FileSystemHelper.ValidateFileName(CheckDetails.BackImageFileName))
                {
                    throw new ImageGeneratorException("Back Image File Name has invalid characters.");
                }
            }

            if (string.IsNullOrWhiteSpace(CheckDetails.CheckNumber) || string.IsNullOrWhiteSpace(CheckDetails.Account))
            {
                throw new ImageGeneratorException("Check Number and Account are required.");
            }
        }

        public void CreateImageFiles()
        {
            Validate();
            ImageGeneratorWrapper.GenerateCheckImage(CheckDetails);
        }

    }
}