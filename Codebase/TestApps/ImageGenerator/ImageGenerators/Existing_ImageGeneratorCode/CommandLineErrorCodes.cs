namespace ImageGenerators
{
    public enum CommandLineErrorCodes
    {
        Success = 0,
        CommandLineParsingError = 1,
        CommandLineParsingException = 2,
        DocumentGenerationException = 3,
        ImageCreationException = 4,
        FileSavingException = 5
    }
}