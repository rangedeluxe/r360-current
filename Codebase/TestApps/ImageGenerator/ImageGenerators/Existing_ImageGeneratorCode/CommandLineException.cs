using System;

namespace ImageGenerators
{
    public class CommandLineException : Exception
    {
        /// <summary>
        /// standard exception.
        /// </summary>       
        public CommandLineException(string message, CommandLineErrorCodes errorCode, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        public CommandLineErrorCodes ErrorCode { get; set; }
    }
}