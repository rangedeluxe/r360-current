﻿using System.Drawing.Imaging;

namespace ImageGenerators
{
    public interface IDocumentGenerator
    {
        void CreateImages(bool includeBackImage);

        void SaveMultiPage(string fileName, ImageFormat imageFormat);
        void Save(string frontFileName, string backFileName, ImageFormat imageFormat);
    }
}
