﻿namespace ImageGenerators
{
  public class CheckDocumentData
    {
        public string CheckNumber { get; set; }

        public string Rt { get; set; }

        public string Account { get; set; }

        public string TransactionCode { get; set; }

        public decimal? Amount { get; set; }

        public string Payee { get; set; }

        public string Payer { get; set; }

        public string WatermarkText { get; set; }

        public string Date { get; set; }

        public string Memo { get; set; }
    }
}
