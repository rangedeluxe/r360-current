﻿using System.Collections.Generic;

namespace ImageGenerators
{
    class InvoiceDocumentData
    {
        public string InvoiceNumber { get; set; }

        public decimal? Amount { get; set; }

        public string Date { get; set; }

        public string Company { get; set; }

        public string CustomerName { get; set; }

        public string CustomerNumber { get; set; }

        public string OrderNumber { get; set; }

        public List<InvoiceItem> Items { get; set; }
    }
}
