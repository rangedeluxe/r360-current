﻿using System;
using System.Collections;

namespace ImageGenerators
{
    public static class ipoImageCommon
    {
        /// <summary>
        /// Enumerations for PaymentTypesKey
        /// Added by : SAS
        /// </summary>
        public enum PaymentTypeKey
        {
            INVALID = -1,
            CHECK = 0,
            ACH = 1,
            WIRE = 2,
            CREDIT_CARD = 3
        }


        /// <summary>
        /// Enumerations for Table        
        /// </summary>
        public enum TableName
        {
            CHECK = 0,
            STUBS = 2,
        }

        /// <summary>
        /// Detect if the object is of Check or Document
        /// </summary>
        public enum ObjectType
        {
            INVALID = 0,
            CHECK = 1,
            DOCUMENT = 2
        }

        /// <summary>
        /// Remittance Type
        /// </summary>
        public enum RemittanceType
        {

            UNKNOWN = -1,
            STRUCTURED = 1,
            UNSTRUCTURED = 2,
            RELATED = 3
        }


        /// <summary>
        /// Data Entry Table names 
        /// </summary>
        public const string CHECKS_TABLE = "checks";
        public const string STUBS_TABLE = "stubs";
        /// <summary>
        /// Checks Data Entry columns
        /// </summary>


        public const string CHECKS_DATA_ENTRY_RECEIVING_COMPANY = "receivingcompany";
        public const string CHECKS_DATA_ENTRY_RECEIVING_INDIVIDUAL = "receivingindividual";
        public const string CHECKS_DATA_ENTRY_ACH_BATCH_NUMBER = "achbatchnumber";
        public const string CHECKS_DATA_ENTRY_COMPANY_ID = "companyid";
        public const string CHECKS_DATA_ENTRY_COMPANY_NAME = "companyname";
        public const string CHECKS_DATA_ENTRY_STANDARD_ENTRY_CLASS = "standardentryclass";
        public const string CHECKS_DATA_ENTRY_ENTRY_DESCRIPTION = "entrydescription";
        public const string CHECKS_DATA_ENTRY_COMPANY_DATA = "companydata";
        public const string CHECKS_DATA_ENTRY_DESCRIPTIVE_DATE = "descriptivedate";
        public const string CHECKS_DATA_ENTRY_EFFECTIVE_DATE = "effectivedate";
        public const string CHECKS_DATA_ENTRY_SETTLEMENT_DATE = "settlementdate";
        public const string CHECKS_DATA_ENTRY_ELECTRONIC_TRANS_CD = "electronictransactioncode";
        public const string CHECKS_DATA_ENTRY_ACCOUNT_NUMBER = "accountnumber";
        public const string CHECKS_DATA_ENTRY_INDIVIDUAL_ID = "individualid";
        public const string CHECKS_DATA_ENTRY_TRACE_NUMBER = "tracenumber";
        public const string CHECKS_DATA_ENTRY_BAI_FILE_CREATION_DATE = "baifilecreationdate";
        public const string CHECKS_DATA_ENTRY_BAI_FILE_ID = "baifileid";
        public const string CHECKS_DATA_ENTRY_CUSTOMER_ACCOUNT_NUMBER = "customeraccountnumber";
        public const string CHECKS_DATA_ENTRY_TYPE_CODE = "typecode";
        public const string CHECKS_DATA_ENTRY_BAI_BANK_REFERENCE = "baibankreference";
        public const string CHECKS_DATA_ENTRY_BAI_CUSTOMER_REFERENCE = "baicustomerreference";
        public const string CHECKS_DATA_ENTRY_BAI_TEXT = "baitext";

        /// <summary>
        /// Checks table Columns
        /// </summary>
        public const string CHECKS_AMOUNT = "amount";
        public const string CHECKS_REMITTER = "remittername";
        public const string CHECKS_SOURCE_PROCESSING_DATE_KEY = "sourceprocessingdatekey";
        public const string WORK_GROUP_LNG_NAME = "workgrouplongname";
        public const string CHECKS_DDA = "dda";


        /// <summary>
        /// Stubs Data Entry columns 
        /// </summary>
        public const string STUBS_DATA_ENTRY_REMIT_BENEF = "remittancebeneficiary";

        /// <summary>
        /// Stubs table Columns
        /// </summary>
        public const string STUBS_INVOICE = "invoice";

        /// <summary>
        /// ACH Document Image Constants
        /// </summary>
        public const string ACH_PAYMENT_HEADING = "ACH Payment";
        public const string ACH_BATCH_INFORMATION = "ACH Batch Information";
        public const string ACH_BATCH_NUMBER = "ACH Batch Number:";
        public const string ACH_COMPANY_ID = "Company ID:";
        public const string ACH_COMPANY_NAME = "Company Name:";
        public const string ACH_ENTRY_CLASS = "Entry Class:";
        public const string ACH_ENTRY_DESCRIPTION = "Entry Description:";
        public const string ACH_COMPANY_DATA = "Company Data:";
        public const string ACH_DESCRIPTIVE_DATE = "Descriptive Date:";
        public const string ACH_EFFECTIVE_DATE = "Effective Date:";
        public const string ACH_SETTLEMENT_DATE = "Settlement Date:";
        public const string ACH_ENTRY_DETAIL = "ACH Entry Detail";
        public const string ACH_TRANSACTION_CODE = "Transaction Code:";
        public const string ACH_ACCOUNT_NUMBER = "Account Number:";
        public const string ACH_AMOUNT = "Amount:";
        public const string ACH_INDIVIDUAL_ID = "Individual ID:";
        public const string ACH_RECEIVING_COMPANY = "Receiving Company:";
        public const string ACH_TRACE_NUMBER = "Trace Number:";
        public const string ACH_RAW_ADDENDA = "ACH Raw Addenda";
        public const string ACH_PARSED_ADDENDA = "ACH Parsed Addenda";


        /// <summary>
        /// Wire Transfer Document Image Constants
        /// </summary>
        public const string WIRE_PAYMENT_HEADING = "Wire Transfer Payment (BAI2)";
        public const string WIRE_FILE_CREATION = "File Creation:";
        public const string WIRE_FILE_ID = "File ID:";
        public const string WIRE_CUSTOMERT_ACCOUNT = "Customer Account:";
        public const string WIRE_PAYMENT_TYPE = "Payment Type:";
        public const string WIRE_AMOUNT = "Amount:";
        public const string WIRE_BANK_REFERENCE_NO = "Bank Reference Number:";
        public const string WIRE_CUSTOMER_REFERENCE_NO = "Customer Reference Number:";
        public const string WIRE_UNPARSED_TEXT = "Unparsed Text:";
        public const string WIRE_PARSED_TEXT = "Parsed Text:";

        /// <summary>
        /// Font Constant
        /// </summary>
        public const string FONT_NAME_E13B = "MICRE13B Match Tryout";
        public const string FONT_NAME_ARIAL = "Arial";
        public const string FONT_NAME_COURIER_NAME = "Courier New";


        /// <summary>
        /// ACH Raw Addenda Data Example
        /// </summary>
        public const string ACH_RAW_DATA_1 = "00000000011111111112222222222333333333344444444445555555555666666666677777777778";
        public const string ACH_RAW_DATA_2 = "12345678901234567890123456789012345678901234567890123456789012345678901234567890";


        /// <summary>
        /// ACH Parsed Addenda Image Column Names
        /// </summary>
        public const string ADDENDA_BPR_MONE = "BPR Monetary";
        public const string ADDENDA_AMT = "Amount";
        public const string ADDENDA_NUM = "Number";
        public const string ADDENDA_INVOICE_AMT = "Invoice Amount";
        public const string ADDENDA_BPR_ACT = "BPR Account";
        public const string ADDENDA_RMR_REF = "RMR Reference";
        public const string ADDENDA_RMR_MONE = "RMR Monetary";
        public const string ADDENDA_RMR_TOT = "RMR Total";
        public const string ADDENDA_RMR_DISC = "RMR Discount";
        public const string ADDENDA_ACCOUNT = "Account";
        public const string ADDENDA_INVOICE = "Invoice";
        public const string REASSOCIATION = "Reassociation";
        public const string TRACE = "Trace";



        /// <summary>
        /// ACH Parsed Addenda Display Name in Stubs and StubsDataEntry
        /// </summary>
        public const string DATA_ENTRY_COL_BPR_MONE_AMT = "bpr monetary amount";
        public const string DATA_ENTRY_COL_BPR_ACT_NUM = "bpr account number";
        public const string DATA_ENTRY_COL_RMR_REF_NUM = "rmr reference number";
        public const string DATA_ENTRY_COL_RMR_MONE_AMT = "rmr monetary amount";
        public const string DATA_ENTRY_COL_RMR_TOT_INV_AMT = "rmr total invoice amount";
        public const string DATA_ENTRY_COL_RMR_DISC_AMT = "rmr discount amount";
        public const string DATA_ENTRY_COL_AMT = "amount";
        public const string DATA_ENTRY_COL_ACCOUNT_NUM = "accountnumber";



        /// <summary>
        /// Payment Type Constants
        /// </summary>
        public const string INCOMING_MONEY_TRANSFER = "- Incoming Money Transfer";
        public const string OUTGOING_MONEY_TRANSFER = "- Outgoing Money Transfer";
        public const string UNKNOWN_MONEY_TRANSFER = "- Unknown";

        public const string INCOMING_MONEY_TRANSFER_TYPE_CODE = "195";
        public const string OUTGOING_MONEY_TRANSFER_TYPE_CODE = "495";


        /// <summary>
        /// Wire Transfer Stubs and StubEntry Fields for Parsed Structured Remittance Data
        /// </summary>        
        public const string REMITTANCE_ORIGINATOR = "remittanceoriginator";
        public const string REMITTANCE_BENEFICIARY = "remittancebeneficiary";
        public const string REMITTANCE_PRIM_DOC_INFO = "primaryremittancedocinfo";
        public const string REMITTANCE_ACT_AMT_PAID = "actualamountpaid";
        public const string REMITTANCE_GROSS_AMT = "grossamount";
        public const string REMITTANCE_DISC_AMT = "discountamount";
        public const string REMITTANCE_ADJ_INFO = "adjustmentinformation";
        public const string REMITTANCE_DATE = "remittancedate";
        public const string REMITTANCE_SEC_DOC_INFO = "secondaryremittancedocinfo";
        public const string REMITTANCE_FREE_TEXT = "remittancefreetext";

        /// <summary>
        /// Wire Transfer Stubs and StubEntry Fields for Parsed unStructured Remittance Data
        /// </summary>
        public const string BPR_MONETARY_AMT = "bprmonetaryamount";
        public const string BPR_ACT_NO = "bpraccountnumber";
        public const string RMR_REF_NO = "rmrreferencenumber";
        public const string RMR_MONETARY_AMT = "rmrmonetaryamount";
        public const string RMR_TOT_INV_AMT = "rmrtotalinvoiceamount";
        public const string RMR_DISC_AMT = "rmrdiscountamount";
        public const string ACCOUNT_NUMBER = "accountnumber";
        public const string ACH_GRID_AMOUNT = "amount";
        public const string ACH_INVOICE_NUMBER = "invoicenumber";
        public const string REASSOCIATION_TRACE_NUMBER = "reassociationtracenumber";



        /// <summary>
        /// Structured Addenda Column name constants
        /// </summary>
        public const string REMITTANCE = "Remittance";
        public const string ORIGINATOR = "Originator";
        public const string BENEFICIARY = "Beneficiary";
        public const string PRIMARY = "Primary";
        public const string DOC_INFO = "Doc Info";
        public const string ACTUAL = "Actual";
        public const string AMT_PAID = "Amount Paid";
        public const string GROSS = "Gross";
        public const string AMT = "Amount";
        public const string DISCOUNT = "Discount";
        public const string ADJUSTMENT = "Adjustment";
        public const string INFORMATION = "Information";
        public const string DATE = "Date";
        public const string SECONDARY = "Secondary";
        public const string FREE_TEXT = "Free Text";

        /// <summary>
        /// Related addenda Column name constants
        /// </summary>
        public const string RELATED_REMITTANCE_INFO_COL_NM = "Related Remittance Info";
        /// <summary>
        /// Wire Transfer Stubs and StubEntry Fields for Parsed unStructured Remittance Data
        /// </summary>
        public const string RELATED_REMITTANCE_INFO = "relatedremittanceinfo";

        /// <summary>
        /// Get Parsed Structured remittance Data field names
        /// </summary>
        /// <param name="eTableName"></param>
        /// <param name="alTableFields"></param>
        public static ArrayList GetRemittanceFields(ipoImageCommon.RemittanceType enRemittanceType)
        {
            ArrayList arRemFieldList = new ArrayList();
            switch (enRemittanceType)
            {
                case RemittanceType.STRUCTURED:
                    arRemFieldList.Add(REMITTANCE_ORIGINATOR);
                    arRemFieldList.Add(REMITTANCE_BENEFICIARY);
                    arRemFieldList.Add(REMITTANCE_PRIM_DOC_INFO);
                    arRemFieldList.Add(REMITTANCE_ACT_AMT_PAID);
                    arRemFieldList.Add(REMITTANCE_GROSS_AMT);
                    arRemFieldList.Add(REMITTANCE_DISC_AMT);
                    arRemFieldList.Add(REMITTANCE_ADJ_INFO);
                    arRemFieldList.Add(REMITTANCE_DATE);
                    arRemFieldList.Add(REMITTANCE_SEC_DOC_INFO);
                    arRemFieldList.Add(REMITTANCE_FREE_TEXT);
                    break;
                case RemittanceType.UNSTRUCTURED:
                    arRemFieldList.Add(BPR_MONETARY_AMT);
                    arRemFieldList.Add(BPR_ACT_NO);
                    arRemFieldList.Add(RMR_REF_NO);
                    arRemFieldList.Add(RMR_MONETARY_AMT);
                    arRemFieldList.Add(RMR_TOT_INV_AMT);
                    arRemFieldList.Add(RMR_DISC_AMT);
                    break;
                case RemittanceType.RELATED:
                    arRemFieldList.Add(RELATED_REMITTANCE_INFO);
                    break;
            }
            return arRemFieldList;
        }


        /// <summary>
        /// Get the field names
        /// </summary>
        /// <param name="eTableName"></param>
        /// <param name="alTableFields"></param>
        public static ArrayList GetFields(TableName eTableName)
        {
            ArrayList arFieldList = new ArrayList();

            switch (eTableName)
            {
                case TableName.CHECK:
                    arFieldList.Add(ipoImageCommon.CHECKS_AMOUNT);
                    arFieldList.Add(ipoImageCommon.CHECKS_REMITTER);
                    arFieldList.Add(ipoImageCommon.CHECKS_SOURCE_PROCESSING_DATE_KEY);
                    arFieldList.Add(ipoImageCommon.CHECKS_DDA);
                    arFieldList.Add(CHECKS_DATA_ENTRY_COMPANY_NAME);
                    arFieldList.Add(CHECKS_DATA_ENTRY_EFFECTIVE_DATE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_RECEIVING_COMPANY);
                    arFieldList.Add(CHECKS_DATA_ENTRY_RECEIVING_INDIVIDUAL);
                    arFieldList.Add(CHECKS_DATA_ENTRY_ACH_BATCH_NUMBER);
                    arFieldList.Add(CHECKS_DATA_ENTRY_COMPANY_ID);
                    arFieldList.Add(CHECKS_DATA_ENTRY_ENTRY_DESCRIPTION);
                    arFieldList.Add(CHECKS_DATA_ENTRY_COMPANY_DATA);
                    arFieldList.Add(CHECKS_DATA_ENTRY_DESCRIPTIVE_DATE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_SETTLEMENT_DATE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_ACCOUNT_NUMBER);
                    arFieldList.Add(CHECKS_DATA_ENTRY_INDIVIDUAL_ID);
                    arFieldList.Add(CHECKS_DATA_ENTRY_TRACE_NUMBER);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_FILE_CREATION_DATE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_FILE_ID);
                    arFieldList.Add(CHECKS_DATA_ENTRY_CUSTOMER_ACCOUNT_NUMBER);
                    arFieldList.Add(CHECKS_DATA_ENTRY_TYPE_CODE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_BANK_REFERENCE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_CUSTOMER_REFERENCE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_TEXT);
                    arFieldList.Add(CHECKS_DATA_ENTRY_STANDARD_ENTRY_CLASS);
                    arFieldList.Add(CHECKS_DATA_ENTRY_ELECTRONIC_TRANS_CD);
                    break;
                case TableName.STUBS:
                    arFieldList.Add(ipoImageCommon.STUBS_INVOICE);
                    arFieldList.Add(ipoImageCommon.STUBS_DATA_ENTRY_REMIT_BENEF);
                    break;
            }

            return arFieldList;
        }

        /// <summary>
        /// Get the object type
        /// </summary>
        /// <param name="irRequest"></param>
        /// <returns></returns>
        public static ObjectType GetObjectType(IItem irRequest)
        {
            string strObjectType = string.Empty;
            strObjectType = irRequest.GetType().ToString();
            switch (strObjectType)
            {
                case ("WFS.RecHub.Common.cCheck"):
                    return ObjectType.CHECK;
                case ("WFS.RecHub.Common.cDocument"):
                    return ObjectType.DOCUMENT;
                default:
                    return ObjectType.INVALID;
            }
        }
    }
}
