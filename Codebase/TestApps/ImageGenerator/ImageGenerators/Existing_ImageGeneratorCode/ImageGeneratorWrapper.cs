﻿using System;
using System.Collections.Generic;
using ImageGenerators.Interfaces;

namespace ImageGenerators
{
    public class ImageGeneratorWrapper
    {
        public static void GenerateImage(string[] args)
        {
            CommandLineArguments commandLineArguments = new CommandLineArguments();
            if (!commandLineArguments.ParseCommandLineArguments(args)
                || commandLineArguments.ShowHelp)
            {
                throw new CommandLineException("An error occurred when parsing the command line arguments.", CommandLineErrorCodes.CommandLineParsingError, null);
            }

            try
            {
                GenerateImage(commandLineArguments);
            }
            catch (Exception ex)
            {
                throw new CommandLineException("An error occurred when parsing the command line arguments.", CommandLineErrorCodes.CommandLineParsingException, ex);
            }

        }

        private static IDocumentGenerator CreateDocumentGenerator(CommandLineArguments commandLineArguments)
        {
            IDocumentGenerator imageGenerator = null;
            switch (commandLineArguments.DocumentType)
            {
                case DocumentType.Check:
                    imageGenerator = CreateCheckImageGenerator(commandLineArguments);
                    break;

                case DocumentType.Invoice:
                    imageGenerator = CreateInvoiceImageGenerator(commandLineArguments);
                    break;
            }

            return imageGenerator;
        }

        private static IDocumentGenerator CreateInvoiceImageGenerator(CommandLineArguments commandLineArguments)
        {
            InvoiceDocumentData invoiceImageData = new InvoiceDocumentData
            {
                InvoiceNumber = commandLineArguments.Number,
                Date = commandLineArguments.Date,
                Company = commandLineArguments.Payee,
                CustomerName = commandLineArguments.Payer,
                Amount = commandLineArguments.Amount,
                CustomerNumber = commandLineArguments.CustomerNumber,
                OrderNumber = commandLineArguments.OrderNumber
            };

            if (!String.IsNullOrEmpty(commandLineArguments.ItemCode))
            {
                InvoiceItem item = new InvoiceItem
                {
                    ItemCode = commandLineArguments.ItemCode,
                    UnitPrice = commandLineArguments.ItemUnitPrice,
                    Quantity = commandLineArguments.ItemQuantity
                };

                invoiceImageData.Items = new List<InvoiceItem> { item };
            }

            return new InvoiceDocumentGenerator { DocumentData = invoiceImageData };
        }

        private static IDocumentGenerator CreateCheckImageGenerator(CommandLineArguments commandLineArguments)
        {
            CheckDocumentData checkImageData = new CheckDocumentData
            {
                CheckNumber = commandLineArguments.Number,
                Rt = commandLineArguments.Rt,
                Account = commandLineArguments.Account,
                TransactionCode = commandLineArguments.TransactionCode,
                Amount = commandLineArguments.Amount,
                Payee = commandLineArguments.Payee,
                Payer = commandLineArguments.Payer,
                WatermarkText = commandLineArguments.WaterMark,
                Date = commandLineArguments.Date,
                Memo = commandLineArguments.Memo
            };

            return new CheckDocumentGenerator { DocumentData = checkImageData };
        }

        public static string GetUsageString()
        {
            return CommandLineArguments.GetUsageString();
        }

        public static void GenerateCheckImage(ICheckDetails checkDetails)
        {
            CommandLineArguments commandLineArguments = new CommandLineArguments();
            commandLineArguments.ParseCheckDetails(checkDetails);
            GenerateImage(commandLineArguments);
        }

        public static void GenerateInvoiceImage(IInvoiceDetails invoiceDetails)
        {
            CommandLineArguments commandLineArguments = new CommandLineArguments();
            commandLineArguments.ParseInvoiceDetails(invoiceDetails);
            GenerateImage(commandLineArguments);
        }

        private static void GenerateImage(CommandLineArguments commandLineArguments)
        {
            IDocumentGenerator imageGenerator;
            try
            {
                imageGenerator = CreateDocumentGenerator(commandLineArguments);
            }
            catch (Exception ex)
            {
                throw new CommandLineException("An error occurred when creating the document generator.", CommandLineErrorCodes.DocumentGenerationException, ex);
            }

            try
            {
                imageGenerator.CreateImages(commandLineArguments.IncludeBack);
            }
            catch (Exception ex)
            {
                throw new CommandLineException("An error occurred when creating the images.", CommandLineErrorCodes.ImageCreationException, ex);
            }

            try
            {
                if (commandLineArguments.MultiPage)
                {
                    imageGenerator.SaveMultiPage(commandLineArguments.OutputFileName,
                        commandLineArguments.OutputImageType);
                }
                else
                {
                    imageGenerator.Save(commandLineArguments.OutputFileName, commandLineArguments.BackOutputFileName,
                        commandLineArguments.OutputImageType);
                }

            }
            catch (Exception ex)
            {
                throw new CommandLineException("An error occurred when attempting to save the images..", CommandLineErrorCodes.FileSavingException, ex);
            }

            IDisposable disposableGenerator = imageGenerator as IDisposable;
            if (disposableGenerator != null)
            {
                disposableGenerator.Dispose();
            }
        }
    }

}
