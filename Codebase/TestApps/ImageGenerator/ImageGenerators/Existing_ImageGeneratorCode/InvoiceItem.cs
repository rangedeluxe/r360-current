﻿namespace ImageGenerators
{
    public class InvoiceItem
    {
        public string ItemCode { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? Quantity { get; set; }
    }
}
