﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ImageGenerators
{
    public static class GraphicsHelpers
    {
        /// <summary>
        /// Rotate Text
        /// </summary>
        /// <param name="text"></param>
        /// <param name="graphic"></param>
        /// <param name="textBrush"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static Image RotateText(string text, Graphics graphic, Font font, SolidBrush textBrush, float angle)
        {
            //Measure size of the text
            SizeF textSize = graphic.MeasureString(text, font);
            SizeF newSize = GetRotatedTextImageSize(textSize, angle);

            //Instantiate a new image for rotated text
            Bitmap rotatedText = new Bitmap((int)(Math.Round(newSize.Width)), (int)(Math.Round(newSize.Height)));

            //Get graphic object of new instantiated image for painting
            Graphics textGraphic = Graphics.FromImage(rotatedText);
            textGraphic.InterpolationMode = InterpolationMode.High;

            //Calculate coordination of center of the image
            float ox = (float)newSize.Width / 2f;
            float oy = (float)newSize.Height / 2f;

            //Apply transformations (translation, rotation, reverse translation)
            textGraphic.TranslateTransform(ox, oy);
            textGraphic.RotateTransform(angle);
            textGraphic.TranslateTransform(-ox, -oy);

            //Calculate the location of drawing text
            float x = (rotatedText.Width - textSize.Width) / 2f;
            float y = (rotatedText.Height - textSize.Height) / 2f;

            //Draw the string
            textGraphic.DrawString(text, font, textBrush, x, y);

            //Return the image of rotated text
            return rotatedText;
        }

        /// <summary>
        /// Rotated Text image
        /// </summary>
        /// <param name="fontSize"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static SizeF GetRotatedTextImageSize(SizeF fontSize, float angle)
        {
            double theta = angle * Math.PI / 180.0;

            while (theta < 0.0)
                theta += 2 * Math.PI;

            double adjacentTop, oppositeTop;
            double adjacentBottom, oppositeBottom;

            if ((theta >= 0.0 && theta < Math.PI / 2.0) || (theta >= Math.PI && theta < (Math.PI + (Math.PI / 2.0))))
            {
                adjacentTop = Math.Abs(Math.Cos(theta)) * fontSize.Width;
                oppositeTop = Math.Abs(Math.Sin(theta)) * fontSize.Width;
                adjacentBottom = Math.Abs(Math.Cos(theta)) * fontSize.Height;
                oppositeBottom = Math.Abs(Math.Sin(theta)) * fontSize.Height;
            }
            else
            {
                adjacentTop = Math.Abs(Math.Sin(theta)) * fontSize.Height;
                oppositeTop = Math.Abs(Math.Cos(theta)) * fontSize.Height;
                adjacentBottom = Math.Abs(Math.Sin(theta)) * fontSize.Width;
                oppositeBottom = Math.Abs(Math.Cos(theta)) * fontSize.Width;
            }

            int nWidth = (int)Math.Ceiling(adjacentTop + oppositeBottom);
            int nHeight = (int)Math.Ceiling(adjacentBottom + oppositeTop);

            return new SizeF(nWidth, nHeight);
        }
    }
}
