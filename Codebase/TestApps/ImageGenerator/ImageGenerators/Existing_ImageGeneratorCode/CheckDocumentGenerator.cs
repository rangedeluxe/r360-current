﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using WFS.RecHub.Common;

namespace ImageGenerators
{
    class CheckDocumentGenerator : DocumentGenerator
    {
        public CheckDocumentGenerator()
        {
            SetImageDefaults();
        }

        private void SetImageDefaults()
        {
            ImageWidth = 4000;
            ImageHeight = 3300;
            XResolution = 300;
            YResolution = 300;
        }

        public override void CreateImages(bool includeBackImage)
        {
            CreateFrontImage();

            if (includeBackImage)
            {
                CreateBackImage();
            }
        }

        private void CreateFrontImage()
        {
            Assembly assemblyImage = Assembly.GetExecutingAssembly();
            using (Stream imageStream = assemblyImage.GetManifestResourceStream("ImageGenerators.Resources.Check.tif"))
            {
                if (imageStream == null)
                {
                    throw new Exception("Failed to retrieve the \"ImageGenerators.Resources.Check.tif\" resource from the manifest.");
                }
                FrontBmp = new Bitmap(imageStream);
                ImageWidth = FrontBmp.Width;
                ImageHeight = FrontBmp.Height;

                using (Graphics grfx = Graphics.FromImage(FrontBmp))
                {
                    using (SolidBrush blackBrush = new SolidBrush(Color.Black))
                    using (SolidBrush grayBrush = new SolidBrush(Color.LightGray))
                    using (Font basicFont = new Font(ipoImageCommon.FONT_NAME_ARIAL, 10, FontStyle.Bold))
                    {
                        // draw some text on it
                        int x = 25;
                        int y = 25;

                        string text;

                        if (!String.IsNullOrEmpty(DocumentData.WatermarkText))
                        {
                            using (Font watermarkFont = new Font(ipoImageCommon.FONT_NAME_ARIAL, 85, FontStyle.Regular))
                            using (Image rotatedText = GraphicsHelpers.RotateText(DocumentData.WatermarkText, grfx,
                                                                                  watermarkFont, grayBrush, (float)20.0))
                            {
                                grfx.DrawImage(rotatedText, new Point(200, 50));
                            }
                        }

                        if (!String.IsNullOrEmpty(DocumentData.Payer))
                        {
                            grfx.DrawString(DocumentData.Payer, basicFont, blackBrush,
                                new RectangleF(x, y + 10, ImageWidth - x, y));
                        }

                        y += 65;
                        x += 450;
                        //Effective Date
                        if (!String.IsNullOrEmpty(DocumentData.Date))
                        {
                            DateTime temp = DateTime.ParseExact(DocumentData.Date, "yyyyMMdd", null);
                            text = temp.ToString("MMMM") + " " + temp.ToString("dd") + "," + temp.Year.ToString();
                            grfx.DrawString(text, basicFont, blackBrush, new RectangleF(x, y, ImageWidth - x, y));
                        }

                        y += 35;
                        x = 90;

                        //Pay Order of
                        if (!String.IsNullOrEmpty(DocumentData.Payee))
                        {
                            grfx.DrawString(DocumentData.Payee, basicFont, blackBrush,
                                new RectangleF(x, y, ImageWidth - x, y));
                        }

                        //Amount                    
                        x += 490;
                        if (DocumentData.Amount.HasValue)
                        {
                            text = DocumentData.Amount.Value.ToString("N2");
                            grfx.DrawString(text, basicFont, blackBrush, new RectangleF(x, y, ImageWidth - x, y));
                            y += 30;
                            x = 30;
                            text = DocumentData.Amount.Value.ToLegalAmount().Text;
                            grfx.DrawString(text, basicFont, blackBrush, text.ToLegalAmountRectangle(x, y, ImageWidth));
                        }
                        
                        // Memo
                        y += 85;
                        x = 50;
                        if (!String.IsNullOrEmpty(DocumentData.Memo))
                        {
                            grfx.DrawString(DocumentData.Memo, basicFont, blackBrush,
                                new RectangleF(x, y, ImageWidth - x, y));
                        }

                        // RT
                        // y += 85;
                        y += 35;
                        x = 38;
                        if (!String.IsNullOrEmpty(DocumentData.Rt))
                        {
                            var rt = "|: " + DocumentData.Rt + " |:";
                            grfx.DrawString(rt, basicFont, blackBrush,
                                new RectangleF(x, y, ImageWidth - x, y));
                        }

                        //Account
                        x = 400;
                        if (!String.IsNullOrEmpty(DocumentData.Account))
                        {
                            var account = DocumentData.Account + " :|";
                            grfx.DrawString(account, basicFont, blackBrush,
                                new RectangleF(x, y, ImageWidth - x, y));
                        }
                    }
                }
            }
        }

        private void CreateBackImage()
        {
            Assembly assemblyImage = Assembly.GetExecutingAssembly();
            using (Stream imageStream = assemblyImage.GetManifestResourceStream("ImageGenerators.Resources.CheckBack.tif"))
            {
                if (imageStream == null)
                {
                    throw new Exception("Failed to retrieve the \"ImageGenerators.Resources.CheckBack.tif\" resource from the manifest.");
                }
                BackBmp = new Bitmap(imageStream);
            }
        }

        public CheckDocumentData DocumentData { get; set; }
    }
}
