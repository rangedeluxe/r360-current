﻿using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 03/12/2011
*
* Purpose: Interface for items
*
* Modification History
* CR 32562 JMC 03/02/2011 
*    -New interface exposing BankID, LockboxID, ProcessingDateKey, PICSDate, DepositDate, BatchID, 
*     TransactionID, TxnSequence, BatchSequence, FileDescriptor, DefaultColorMode properties
* CR 50564 JMC 02/24/2012
*   -Added SiteCode property.
* CR 50983 WJS 03/14/2012
*  -Added batchsitecode property, remove siteCode property
* WI 89972 CRG 03/01/2013
*	Modify ipoCommon for .NET 4.5 and NameSpace
* WI 143419 DJW 5/30/2014 
*    Changed BatchID to SourceBatchID
* WI 148949 DJW 6/19/2013
*    Added SessionID to methods for sprocs that needed it (related to RAAM)
* WI 166398 SAS 9/17/2014
*    Added ImportTypeShortName field
* WI 166924 SAS 9/19/2014
*   Updated the datatype for Source BatchID    
* WI 175186 SAS 10/30/2014
*    Update ImportTypeShortName field to BatchSourceName  
* WI 176351 SAS 11/06/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName
******************************************************************************/
namespace ImageGenerators
{
    public enum LockboxColorMode
    {
        COLOR_MODE_BITONAL = 1,
        COLOR_MODE_COLOR = 2,
        COLOR_MODE_GRAYSCALE = 4,
    }


    /// <summary></summary>
    public interface IItem
    {

        /// <summary></summary>
        int BatchSiteCode
        {
            get;
        }

        /// <summary></summary>
        int BankID
        {
            get;
        }

        /// <summary></summary>
        int LockboxID
        {
            get;
        }

        /// <summary></summary>
        int ProcessingDateKey
        {
            get;
        }

        /// <summary></summary>
        string PICSDate
        {
            get;
        }

        /// <summary></summary>
        DateTime DepositDate
        {
            get;
        }

        /// <summary></summary>
        long BatchID
        {
            get;
        }

        Guid SessionID
        {
            get;
        }

        /// <summary></summary>
        long SourceBatchID
        {
            get;
        }

        /// <summary></summary>
        int TransactionID
        {
            get;
        }

        /// <summary></summary>
        int TxnSequence
        {
            get;
        }

        /// <summary></summary>
        int BatchSequence
        {
            get;
        }

        /// <summary></summary>
        string FileDescriptor
        {
            get;
        }

        /// <summary></summary>
        LockboxColorMode DefaultColorMode
        {
            get;
        }

        /// <summary></summary>
        string BatchSourceShortName
        {
            get;
        }

        /// <summary></summary>
        string ImportTypeShortName
        {
            get;
        }

        /// <summary>
        /// Gets the payment type key.
        /// </summary>
        /// <value>
        /// The payment type key.
        /// </value>
        int PaymentType
        {
            get;
        }
    }
}
