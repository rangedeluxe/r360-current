﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace ImageGenerators
{
    public abstract class DocumentGenerator : IDocumentGenerator, IDisposable
    {
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public int XResolution { get; set; }
        public int YResolution { get; set; }

        private Bitmap _frontBmp;
        public Bitmap FrontBmp
        {
            get { return _frontBmp; }
            set
            {
                if (_frontBmp != null)
                {
                    _frontBmp.Dispose();
                }
                _frontBmp = value;
            }
        }

        private Bitmap _backBmp;
        public Bitmap BackBmp
        {
            get { return _backBmp; }
            set
            {
                if (_backBmp != null)
                {
                    _backBmp.Dispose();
                }
                _backBmp = value;
            }
        }


        protected void InitializeImage(ref Bitmap bmp)
        {
            if (bmp == null)
            {
                bmp = new Bitmap(ImageWidth, ImageHeight, PixelFormat.Format32bppArgb);
            }

            bmp.SetResolution(XResolution, YResolution);
            using (Graphics grfx = Graphics.FromImage(bmp))
            {
                // fill the background with white
                using (SolidBrush whiteBrush = new SolidBrush(Color.White))
                {
                    grfx.FillRectangle(whiteBrush, 0, 0, ImageWidth, ImageHeight);
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                if (FrontBmp != null)
                {
                    FrontBmp.Dispose();
                }

                if (BackBmp != null)
                {
                    BackBmp.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }


        public virtual void CreateImages(bool includeBackImage)
        {
            FrontBmp = new Bitmap(ImageWidth, ImageHeight);
            if (includeBackImage)
            {
                BackBmp = new Bitmap(ImageWidth, ImageHeight);
            }
        }

        public virtual void Save(string frontFileName, string backFileName, ImageFormat imageFormat)
        {
            if (String.IsNullOrEmpty(frontFileName))
            {
                throw new ArgumentException("Argument \"frontFileName\" cannot be null or empty.");
            }

            if (FrontBmp == null)
            {
                throw new Exception("Attempting to save the document's front image, but the front image was not created or was disposed of.");
            }

            SaveImage(frontFileName, FrontBmp, imageFormat);

            if (!String.IsNullOrEmpty(backFileName))
            {
                SaveImage(backFileName, BackBmp, imageFormat);
            }
        }

        private void SaveImage(string fileName, Bitmap bmp, ImageFormat imageFormat)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                throw new ArgumentException("Argument \"fileName\" cannot be null or empty.");
            }

            if (bmp == null)
            {
                throw new ArgumentNullException("bmp");
            }

            bmp.Save(fileName, imageFormat);
        }

        public virtual void SaveMultiPage(string fileName, ImageFormat imageFormat)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                throw new ArgumentException("Argument \"fileName\" cannot be null or empty when saving image.");
            }

            if (BackBmp == null)
            {
                // If there aren't multiple pages, just save as a single page image
                Save(fileName, String.Empty, imageFormat);
                return;
            }

            System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.SaveFlag;
            ImageCodecInfo encoderInfo = ImageCodecInfo.GetImageEncoders().First(i => i.MimeType == "image/tiff");
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.MultiFrame);

            // Save the front image
            FrontBmp.Save(fileName, encoderInfo, encoderParameters);

            encoderParameters.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.FrameDimensionPage);

            // Add the back image
            FrontBmp.SaveAdd(BackBmp, encoderParameters);

            // Close out the file
            encoderParameters.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.Flush);
            FrontBmp.SaveAdd(encoderParameters);
        }

        private bool _disposed = false;
    }
}

