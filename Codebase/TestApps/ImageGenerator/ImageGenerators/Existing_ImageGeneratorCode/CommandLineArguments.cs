﻿using System;
using System.Drawing.Imaging;
using System.IO;
using ImageGenerators.Interfaces;

namespace ImageGenerators
{
    public enum DocumentType
    {
        Unspecified = 0,
        Check,
        Invoice
    };


    class CommandLineArguments
    {
        public CommandLineArguments()
        {
            InitializeDefaults();
            DocumentType = DocumentType.Unspecified;
        }

        private void InitializeDefaults()
        {
            OutputImageType = ImageFormat.Tiff;
            MultiPage = false;
            IncludeBack = false;
        }

        public DocumentType DocumentType { get; set; }
        public string Number { get; set; }
        public string Rt { get; set; }
        public string Account { get; set; }
        public string TransactionCode { get; set; }
        public decimal? Amount { get; set; }
        public string Payee { get; set; }
        public string Payer { get; set; }
        public string OutputFileName { get; set; }
        public string BackOutputFileName { get; set; }
        public ImageFormat OutputImageType { get; set; }
        public string WaterMark { get; set; }
        public string Date { get; set; }
        public string Memo { get; set; }
        public bool MultiPage { get; set; }
        public bool IncludeBack { get; set; }

        public bool ShowHelp { get; set; }

        public string Error
        {
            get { return _error; }
        }

        public bool ParseCommandLineArguments(string[] args)
        {
            if ((args == null) || (args.Length < 1))
            {
                return false;
            }

            string flag = String.Empty;

            foreach (string arg in args)
            {
                if (String.IsNullOrEmpty(flag)) // expecting a flag
                {
                    if (arg.StartsWith("-") || arg.StartsWith("/"))
                    {
                        flag = arg.Substring(1).Trim();
                    }
                    else
                    {
                        _error = String.Format("Expecting a flag (starting with '-' or '/'), but found value \"{0}\"", arg);
                        return false;
                    }

                    if (IsStandaloneFlag(flag))
                    {
                        ParseFlagValue(flag, null);
                        flag = String.Empty;  // standalone flags require no value
                    }
                }
                else // expecting a value
                {
                    string value = arg.Trim();
                    if (!ParseFlagValue(flag, value))
                    {
                        return false;
                    }
                    flag = String.Empty;
                }
            }

            if (!String.IsNullOrEmpty(flag))
            {
                _error = String.Format("Expecting a value for flag \"{0}\".", flag);
                return false;
            }

            return ValidateArguments();
        }

        private bool IsStandaloneFlag(string flag)
        {
            return (flag.ToLower() == IncludeBackFlag)
                || (flag.ToLower() == MultiPageFlag);
        }

        private bool ValidateArguments()
        {
            if (DocumentType == DocumentType.Unspecified)
            {
                _error = String.Format("The document type (-{0}) must be specified.", DocumentTypeFlag);
                return false;
            }

            if (String.IsNullOrEmpty(OutputFileName))
            {
                _error = String.Format("An output file (-{0}) must be specified.", OutputFileFlag);
                return false;
            }

            if (IncludeBack && String.IsNullOrEmpty(BackOutputFileName))
            {
                // Default back file name to *_back.*
                BackOutputFileName = Path.Combine(Path.GetDirectoryName(OutputFileName),
                                                  Path.GetFileNameWithoutExtension(OutputFileName)
                                                  + "_back" + Path.GetExtension(OutputFileName));
            }

            return true;
        }

        private bool ParseFlagValue(string flag, string value)
        {
            switch (flag.ToLower())
            {
                case DocumentTypeFlag:
                    if (DocumentType != DocumentType.Unspecified)
                    {
                        _error = "Only one document type can be specified.";
                        return false;
                    }
                    else
                    {
                        DocumentType = GetDocumentTypeFromCmdArg(value);
                        if (DocumentType == DocumentType.Unspecified)
                        {
                            _error = String.Format("A document type of \"{0}\" is not valid.", value);
                            return false;
                        }
                    }
                    break;

                case AccountFlag:
                    Account = value;
                    break;

                case AmountFlag:
                    try
                    {
                        Amount = Decimal.Parse(value);
                    }
                    catch (Exception)
                    {
                        _error = String.Format("The value \"{0}\", specified for the item unit price \"-{1}\", is not a valid number.)", value, AmountFlag);
                        return false;
                    }
                    break;

                case NumberFlag:
                    Number = value;
                    break;

                case PayeeFlag:
                    Payee = value;
                    break;

                case PayerFlag:
                    Payer = value;
                    break;

                case RtFlag:
                    Rt = value;
                    break;

                case TransactionCodeFlag:
                    TransactionCode = value;
                    break;

                case OutputFileFlag:
                    OutputFileName = value;
                    break;

                case BackOutputFileFlag:
                    BackOutputFileName = value;
                    break;

                case OutputImageTypeFlag:
                    OutputImageType = GetOutputImageTypeFromCmdArg(value);
                    if (OutputImageType == null)
                    {
                        _error = String.Format("An image type of \"{0}\" is not valid.", value);
                        return false;
                    }
                    break;

                case WaterMarkFlag:
                    WaterMark = value;
                    break;

                case DateFlag:
                    Date = value;
                    break;

                case MemoFlag:
                    Memo = value;
                    break;

                case ShortHelpFlag:
                case LongHelpFlag:
                    ShowHelp = true;
                    break;

                case MultiPageFlag:
                    MultiPage = true;
                    break;

                case IncludeBackFlag:
                    IncludeBack = true;
                    break;

                case CustomerNumberFlag:
                    CustomerNumber = value;
                    break;

                case OrderNumberFlag:
                    OrderNumber = value;
                    break;

                case ItemCodeFlag:
                    ItemCode = value;
                    break;

                case ItemUnitPriceFlag:
                    try
                    {
                        ItemUnitPrice = Decimal.Parse(value);
                    }
                    catch (Exception)
                    {
                        _error = String.Format("The value \"{0}\", specified for the item unit price \"-{1}\", is not a valid number.)", value, ItemUnitPriceFlag);
                        return false;
                    }
                    break;

                case ItemQuantityFlag:
                    try
                    {
                        ItemQuantity = Int32.Parse(value);
                    }
                    catch (Exception)
                    {
                        _error = String.Format("The value \"{0}\", specified for the item quantity \"-{1}\", is not a valid number.)", value, ItemQuantityFlag);
                        return false;
                    }
                    break;

                default:
                    _error = String.Format("Flag \"{0}\" is invalid.", flag);
                    return false;
            }

            return true;
        }

        private ImageFormat GetOutputImageTypeFromCmdArg(string imageTypeArg)
        {
            ImageFormat imageFormat = null;
            switch (imageTypeArg.ToLower())
            {
                case TiffImageTypeId:
                    imageFormat = ImageFormat.Tiff;
                    break;

                default:
                    imageFormat = null;
                    break;
            }

            return imageFormat;
        }

        private DocumentType GetDocumentTypeFromCmdArg(string value)
        {
            DocumentType documentType = DocumentType.Unspecified;

            switch (value.ToLower())
            {
                case CheckDocumentTypeId:
                    documentType = DocumentType.Check;
                    break;

                case InvoiceDocumentTypeId:
                    documentType = DocumentType.Invoice;
                    break;

                default:
                    documentType = DocumentType.Unspecified;
                    break;
            }

            return documentType;
        }


        private string _error;

        private const string DocumentTypeFlag = "documenttype";
        private const string NumberFlag = "number";
        private const string RtFlag = "rt";
        private const string AccountFlag = "account";
        private const string TransactionCodeFlag = "transactioncode";
        private const string AmountFlag = "amount";
        private const string PayeeFlag = "payee";
        private const string PayerFlag = "payer";
        private const string OutputFileFlag = "outputfile";
        private const string BackOutputFileFlag = "backoutputfile";
        private const string OutputImageTypeFlag = "outputImageType";
        private const string WaterMarkFlag = "watermark";
        private const string DateFlag = "date";
        private const string MemoFlag = "memo";
        private const string ShortHelpFlag = "h";
        private const string LongHelpFlag = "help";
        private const string IncludeBackFlag = "includeback";
        private const string MultiPageFlag = "multipage";
        private const string CustomerNumberFlag = "customernumber";
        private const string OrderNumberFlag = "ordernumber";
        private const string ItemCodeFlag = "itemcode";
        private const string ItemUnitPriceFlag = "unitprice";
        private const string ItemQuantityFlag = "itemquantity";

        private const string CheckDocumentTypeId = "check";
        private const string InvoiceDocumentTypeId = "invoice";

        private const string TiffImageTypeId = "tiff";


        internal static string GetUsageString()
        {
            string usage = "Usage:\n"
                           + "  ImageGenerator -documentType check -outputFile <output file name>\n"
                           + "                 [-number <check number>] [-amount <check mount>]\n"
                           + "                 [-payee <payee name> [-payer <payer name>] [-date <date>]\n"
                           + "                 [-watermark <watermark text>] [-memo <memo text>]\n"
                           + "                 [-includeback] [-multipage]\n"
                           + "                 [-backoutputfile <output file name for back image>]\n\n"
                           + "  ImageGenerator -documentType invoice -outputFile <output file name>\n"
                           + "                 [-number <invoice number> [-customerNumber <customer number>]\n"
                           + "                 [-payee <payee name> [-payer <payer name>]\n"
                           + "                 [-date <date>] [-orderNumber <order number>]\n"
                           + "                 [[-itemCode <invoice item code>]\n"
                           + "                 [-unitPrice <item unit price>] [-itemQuantity <item quantity>]\n"
                           + "                 | [-amount <invoice total amount>]]\n"
                           + "\n"
                           + "Options:\n"
                           + "  -h -help  Show the usage screen.";
            return usage;
        }

        public string CustomerNumber { get; set; }

        public string OrderNumber { get; set; }

        public string ItemCode { get; set; }

        public decimal? ItemUnitPrice { get; set; }

        public int? ItemQuantity { get; set; }

        public void ParseCheckDetails(ICheckDetails checkDetails)
        {
            DocumentType = DocumentType.Check;
            Account = checkDetails.Account;
            Amount = checkDetails.Amount;
            Number = checkDetails.CheckNumber;
            Payee = checkDetails.Payee;
            Payer = checkDetails.Payer;
            Rt = checkDetails.RoutingNumber;
            TransactionCode = checkDetails.TransactionCode;
            WaterMark = checkDetails.WatermarkText;
            Date = checkDetails.Date;
            Memo = checkDetails.Memo;
            OutputFileName = checkDetails.FrontImageFileName;
            BackOutputFileName = checkDetails.BackImageFileName;
            OutputImageType = checkDetails.ImageFormat ?? ImageFormat.Tiff;
            IncludeBack = checkDetails.IncludeBack;
            MultiPage = checkDetails.CombineFrontAndBackImages;
        }

        public void ParseInvoiceDetails(IInvoiceDetails invoiceDetails)
        {
            DocumentType = DocumentType.Invoice;
            Number = invoiceDetails.InvoiceNumber;
            CustomerNumber = invoiceDetails.CustomerNumber;
            Payee = invoiceDetails.Payee;
            Payer = invoiceDetails.Payer;
            Date = invoiceDetails.Date;
            OrderNumber = invoiceDetails.OrderNumber;
            Amount = invoiceDetails.Amount;
            OutputFileName = invoiceDetails.FrontImageFileName;
            IncludeBack = invoiceDetails.IncludeBack;
            MultiPage = invoiceDetails.CombineFrontAndBackImages;
            BackOutputFileName = invoiceDetails.BackImageFileName;
            OutputImageType = invoiceDetails.ImageFormat;

            if (invoiceDetails.Items != null && invoiceDetails.Items.Count > 0)
            {
                // For now only one line item is supported.
                ItemCode = invoiceDetails.Items[0].ItemCode;
                ItemUnitPrice = invoiceDetails.Items[0].UnitPrice;
                ItemQuantity = invoiceDetails.Items[0].Quantity;

            }
        }
    }
}
