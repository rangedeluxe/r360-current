﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace ImageGenerators
{
    class InvoiceDocumentGenerator : DocumentGenerator
    {
        public InvoiceDocumentGenerator()
        {
            SetImageDefaults();
        }

        private void SetImageDefaults()
        {
            ImageWidth = 4000;
            ImageHeight = 3300;
            XResolution = 300;
            YResolution = 300;
        }

        public override void CreateImages(bool includeBackImage)
        {
            CreateFrontImage();

            if (includeBackImage)
            {
                CreateBackImage();
            }
        }

        private void CreateFrontImage()
        {
            Assembly assemblyImage = Assembly.GetExecutingAssembly();
            using (Stream imageStream = assemblyImage.GetManifestResourceStream("ImageGenerators.Resources.Invoice.tif"))
            {
                if (imageStream == null)
                {
                    throw new Exception("Failed to retrieve the \"ImageGenerators.Resources.Invoice.tif\" resource from the manifest.");
                }
                FrontBmp = new Bitmap(imageStream);
                ImageWidth = FrontBmp.Width;
                ImageHeight = FrontBmp.Height;

                using (Graphics grfx = Graphics.FromImage(FrontBmp))
                {
                    using (SolidBrush blackBrush = new SolidBrush(Color.Black))
                    using (Font fntEntryHeader = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Bold))
                    using (Font fntEntryHeaderBold = new Font(ipoImageCommon.FONT_NAME_ARIAL, 18, FontStyle.Bold))
                    using (Font fntEntryData = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Regular))
                    using (new Font(ipoImageCommon.FONT_NAME_COURIER_NAME, 12, FontStyle.Regular))
                    {
                        // draw some text on it
                        int x;
                        int y = 175;

                        // Company name
                        SizeF companyNameSize = grfx.MeasureString(DocumentData.Company, fntEntryData);
                        x = (ImageWidth - (int)companyNameSize.Width) / 2;
                        x = x - 20;  // offset for non-centered image
                        grfx.DrawString(DocumentData.Company, fntEntryHeaderBold, blackBrush, new PointF(x, y));

                        // Customer name
                        x = 370;
                        y = 395;
                        grfx.DrawString(DocumentData.CustomerName, fntEntryHeader, blackBrush, new PointF(x, y));

                        // Customer number
                        x = 495;
                        y = 493;
                        grfx.DrawString(DocumentData.CustomerNumber, fntEntryHeader, blackBrush, new PointF(x, y));

                        // Invoice number
                        x = 375;
                        y = 650;
                        grfx.DrawString(DocumentData.InvoiceNumber, fntEntryHeader, blackBrush, new PointF(x, y));

                        // Order number
                        x = 850;
                        y = 650;
                        grfx.DrawString(DocumentData.OrderNumber, fntEntryHeader, blackBrush, new PointF(x, y));

                        // Invoice date
                        x = 1265;
                        y = 650;
                        if (!String.IsNullOrEmpty(DocumentData.Date))
                        {
                            DateTime date = DateTime.ParseExact(DocumentData.Date, "yyyyMMdd", null);
                            grfx.DrawString(String.Format("{0:MM/dd/yyyy}", date), fntEntryHeader, blackBrush, new PointF(x, y));
                        }

                        // Invoice items
                        decimal grandTotal = 0.0M;

                        if (DocumentData.Items != null)
                        {
                            grandTotal = ProcessInvoiceItems(grfx, fntEntryData, blackBrush);
                        }
                        else
                        {
                            if (DocumentData.Amount.HasValue)
                            {
                                grandTotal = DocumentData.Amount.Value;
                            }
                        }

                        // Grand total
                        y = 1670;
                        string grandTotalString = grandTotal.ToString("N2");
                        SizeF grandTotalStringSize = grfx.MeasureString(grandTotalString, fntEntryData);
                        x = 1520 - (int)grandTotalStringSize.Width;
                        grfx.DrawString(grandTotalString, fntEntryData, blackBrush, new PointF(x, y));
                    }
                }
            }
        }

        private decimal ProcessInvoiceItems(Graphics grfx, Font fntEntryData, SolidBrush blackBrush)
        {
            decimal grandTotal = 0.0M;

            int y = 895;

            foreach (InvoiceItem item in DocumentData.Items)
            {
                int x = 205;
                // Item code
                grfx.DrawString(item.ItemCode, fntEntryData, blackBrush, new PointF(x, y));
                decimal total = 0.0M;
                if (item.UnitPrice.HasValue)
                {
                    string unitPriceString = item.UnitPrice.Value.ToString("N2");
                    SizeF unitPriceSize = grfx.MeasureString(unitPriceString, fntEntryData);
                    x = 853 - (int)unitPriceSize.Width;
                    grfx.DrawString(unitPriceString, fntEntryData, blackBrush, new PointF(x, y));
                    if (item.Quantity.HasValue)
                    {
                        total = item.UnitPrice.Value * item.Quantity.Value;
                    }
                    else
                    {
                        total = item.UnitPrice.Value;
                    }
                }

                if (item.Quantity.HasValue)
                {
                    string quantityString = item.Quantity.Value.ToString();
                    SizeF quantitySize = grfx.MeasureString(quantityString, fntEntryData);
                    x = 1178 - (int)quantitySize.Width;
                    grfx.DrawString(quantityString, fntEntryData, blackBrush, new PointF(x, y));
                }

                if (total != 0)
                {
                    string totalString = total.ToString("N2");
                    SizeF totalSize = grfx.MeasureString(totalString, fntEntryData);
                    x = 1520 - (int)totalSize.Width;
                    grfx.DrawString(totalString, fntEntryData, blackBrush, new PointF(x, y));
                }
                y += 43;

                grandTotal += total;
            }

            return grandTotal;
        }

        private void CreateBackImage()
        {
            Assembly assemblyImage = Assembly.GetExecutingAssembly();
            using (Stream imageStream = assemblyImage.GetManifestResourceStream("ImageGenerators.Resources.InvoiceBack.tif"))
            {
                if (imageStream == null)
                {
                    throw new Exception("Failed to retrieve the \"ImageGenerators.Resources.InvoiceBack.tif\" resource from the manifest.");
                }
                BackBmp = new Bitmap(imageStream);
            }
        }

        public InvoiceDocumentData DocumentData { get; set; }
    }
}
