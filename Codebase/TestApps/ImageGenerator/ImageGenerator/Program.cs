﻿using System;
using ImageGenerators;

namespace ImageGenerator
{
    class Program
    {
        static int Main(string[] args)
        {
            var retCode = 0;
            try
            {
                ImageGeneratorWrapper.GenerateImage(args);
            }
            catch (Exception e)
            {
                retCode = -1; // Set default error code to cover any non 'CommandLineException' exceptions.
                Console.WriteLine(e.Message);
                var commandLineException = e as CommandLineException;
                if (commandLineException != null)
                {
                    retCode = (int)commandLineException.ErrorCode;
                    Console.WriteLine(Environment.NewLine);
                    if (e.InnerException != null)
                    {
                        Console.WriteLine(Environment.NewLine);
                        Console.WriteLine(e.InnerException.Message);
                    }

                    if (commandLineException.ErrorCode == CommandLineErrorCodes.CommandLineParsingError)
                    {
                        DisplayUsage();
                    }

                }

            }
            return retCode;
        }

        static void DisplayUsage(string error = null)
        {
            if (!String.IsNullOrEmpty(error))
            {
                Console.WriteLine(error);
                Console.WriteLine();
            }
            Console.WriteLine(ImageGeneratorWrapper.GetUsageString());
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine(@"Process complete; press 'Enter' key to continue . . .");
            Console.ReadLine();
        }
    }
}
