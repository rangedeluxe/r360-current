﻿using System;
using System.Drawing.Imaging;
using ImageGenerators.Common;
using ImageGenerators.Interfaces;
using ImageGenerators.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ImageGenerators.UnitTests
{
    [TestClass]
    public class CheckImageGeneratorUnitTests
    {
        [TestMethod]
        [ExpectedException(typeof(ImageGeneratorException))]
        public void GenerateCheckImageFrontSideInvalidInput()
        {
            IImageFileGenerator imgGenerator = new CheckImageGenerator
            { CheckDetails = GetCheckDetailsIncomplete() };

            imgGenerator.Validate();
        }


        [TestMethod]
        public void GenerateCheckImageFrontSide_ValidInput()
        {
            IImageFileGenerator imgGenerator = new CheckImageGenerator
            { CheckDetails = GetCheckDetailsComplete() };

            try
            {
                imgGenerator.Validate();
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
            }

        }

        private CheckDetails GetCheckDetailsIncomplete()
        {
            return new CheckDetails
            {
                CheckNumber = "001",
                Account = "abcd",
                Date = "01/01/01",
                Memo = "test",
                TransactionCode = "2w3e",
                WatermarkText = "water mark",
                ImageFormat = ImageFormat.Tiff,
            };

        }
        private CheckDetails GetCheckDetailsComplete()
        {
            return new CheckDetails
            {
                CheckNumber = "001",
                Account = "abcd",
                Date = "01/01/01",
                Memo = "test",
                TransactionCode = "2w3e",
                WatermarkText = "water mark",
                ImageFormat = ImageFormat.Tiff,
                FrontImageFileName = "TestImage.tif"
            };

        }
    }
}
