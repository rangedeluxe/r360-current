﻿using System.Collections.Generic;
using System.Drawing.Imaging;
using ImageGenerators.Common;
using ImageGenerators.Interfaces;
using ImageGenerators.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ImageGenerators.UnitTests
{
    [TestClass]
    public class InvoiceImageGeneratorTests
    {
        [TestMethod]
        [ExpectedException(typeof(ImageGeneratorException))]
        public void GenerateInvoiceImageFrontSide_InvalidInput()
        {
            InvoiceImageGenerator invoiceGenerator = new InvoiceImageGenerator { InvoiceDetails = GetBadInvoice() };
            invoiceGenerator.Validate();

        }

        [TestMethod]
        public void GenerateInvoiceImageFrontSide()
        {
            InvoiceImageGenerator invoiceGenerator = new InvoiceImageGenerator { InvoiceDetails = GetGoodInvoice() };
            //If valid no exception is generated.
            invoiceGenerator.Validate();
        }

        private IInvoiceDetails GetFrontBackCombinedInvoice()
        {
            return new InvoiceData
            {
                Amount = 2001,
                BackImageFileName = "InvoiceImageBack.tif",
                Company = "Xyz",
                CustomerName = "Abc",
                CustomerNumber = "123345",
                Date = "20170101",
                ImageFormat = ImageFormat.Tiff,
                FrontImageFileName = "InvoiceImage.tif",
                IncludeBack = true,
                CombineFrontAndBackImages = true,
                InvoiceNumber = "9101112",
                Payer = "Test",
                Payee = "Tester",
                OrderNumber = "1234567890",
                Items = new List<InvoiceItem> {
                    new InvoiceItem { UnitPrice = 100, ItemCode = "ZZQ123", Quantity = 10}
                }
            };
        }

        private IInvoiceDetails GetGoodInvoice()
        {
            return new InvoiceData
            {
                Amount = 2001,
                BackImageFileName = "InvoiceImageBack.tif",
                Company = "Xyz",
                CustomerName = "Abc",
                CustomerNumber = "123345",
                Date = "20170101",
                ImageFormat = ImageFormat.Tiff,
                FrontImageFileName = "InvoiceImage.tif",
                IncludeBack = true,
                InvoiceNumber = "9101112",
                Payer = "Test",
                Payee = "Tester",
                OrderNumber = "1234567890",
                Items = new List<InvoiceItem> {
                    new InvoiceItem { UnitPrice = 100, ItemCode = "ZZQ123", Quantity = 10}
                }
            };
        }


        private IInvoiceDetails GetBadInvoice()
        {
            return new InvoiceData
            {
                Amount = 2001,
                BackImageFileName = "InvoiceImageBack.tif"
            };
        }
    }
}
