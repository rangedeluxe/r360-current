﻿using System.Collections.Generic;
using ImageGenerators.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ImageGenerators.UnitTests
{
    [TestClass]
    public class FileSystemHelperTests
    {
        static readonly List<string> BadFiles = new List<string> { ">../Test.tif", "Tes\"t.tif", "abs/Te|st.tif", ".../ test<ing / Test.ti>f" };
        static readonly List<string> GoodFiles = new List<string> { "../Test.tif", "Test.tif", "abs/Test.tif", "T/est.tif", ".../ testing / Test.tif" };

        static readonly List<string> BadPaths = new List<string> { ">../Test.tif", "..|./Test.tif", "<abs/Test.tif", ".../ |/test<ing / Test.tif" };
        static readonly List<string> GoodPaths = new List<string> { "..", ".", "../abs/", "c:/", ".../ testing / Test.tif" };


        [TestMethod]
        public void ValidateGoodFileNames()
        {
            foreach (var file in GoodFiles)
            {
                var retVal = FileSystemHelper.ValidateFileName(file);
                Assert.IsTrue(retVal);
            }
        }

        [TestMethod]
        public void ValidateBadFileNames()
        {

            foreach (var badFile in BadFiles)
            {
                var retVal = FileSystemHelper.ValidateFileName(badFile);

                Assert.IsFalse(retVal);
            }
        }


        [TestMethod]
        public void ValidateGoodPaths()
        {
            foreach (var path in GoodPaths)
            {
                var retVal = FileSystemHelper.ValidatePath(path);
                Assert.IsTrue(retVal);
            }
        }

        [TestMethod]
        public void ValidateBadPaths()
        {

            foreach (var badPath in BadPaths)
            {
                var retVal = FileSystemHelper.ValidatePath(badPath);

                Assert.IsFalse(retVal);
            }
        }



    }
}
