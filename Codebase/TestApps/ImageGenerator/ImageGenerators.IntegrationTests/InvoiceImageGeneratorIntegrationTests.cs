﻿using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using ImageGenerators.Interfaces;
using ImageGenerators.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ImageGenerators.IntegrationTests
{
    [TestClass]
    public class InvoiceImageGeneratorIntegrationTests
    {
        [TestMethod]
        public void GenerateInvoiceImageFrontSide()
        {
            InvoiceImageGenerator invoiceGenerator = new InvoiceImageGenerator { InvoiceDetails = GetGoodInvoice() };
            invoiceGenerator.CreateImageFiles();

            Assert.IsTrue(File.Exists("InvoiceImage.tif"));
            Assert.IsTrue(File.Exists("InvoiceImageBack.tif"));
        }

        [TestMethod]
        public void GenerateInvoiceImageFrontBackCombined()
        {
            InvoiceImageGenerator invoiceGenerator = new InvoiceImageGenerator { InvoiceDetails = GetFrontBackCombinedInvoice() };
            invoiceGenerator.CreateImageFiles();
            Assert.IsTrue(File.Exists("InvoiceImageFrontBack.tif"));
        }

        private IInvoiceDetails GetFrontBackCombinedInvoice()
        {
            return new InvoiceData
            {
                Amount = 2001,                
                Company = "Xyz",
                CustomerName = "Abc",
                CustomerNumber = "123345",
                Date = "20170101",
                ImageFormat = ImageFormat.Tiff,
                FrontImageFileName = "InvoiceImageFrontBack.tif",
                IncludeBack = true,
                CombineFrontAndBackImages = true,
                InvoiceNumber = "9101112",
                Payer = "Test",
                Payee = "Tester",
                OrderNumber = "1234567890",
                Items = new List<InvoiceItem> {
                    new InvoiceItem { UnitPrice = 100, ItemCode = "ZZQ123", Quantity = 10}
                }
            };
        }

        private IInvoiceDetails GetGoodInvoice()
        {
            return new InvoiceData
            {
                Amount = 2001,
                BackImageFileName = "InvoiceImageBack.tif",
                Company = "Xyz",
                CustomerName = "Abc",
                CustomerNumber = "123345",
                Date = "20170101",
                ImageFormat = ImageFormat.Tiff,
                FrontImageFileName = "InvoiceImage.tif",
                IncludeBack = true,
                InvoiceNumber = "9101112",
                Payer = "Test",
                Payee = "Tester",
                OrderNumber = "1234567890",
                Items = new List<InvoiceItem> {
                    new InvoiceItem { UnitPrice = 100, ItemCode = "ZZQ123", Quantity = 10}
                }
            };
        }
    }
}
