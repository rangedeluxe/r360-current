﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ImageGenerators.IntegrationTests
{
    [TestClass]
    public class ImageGeneratorCommandLineIntegrationTests
    {
        private string[] args =
        {
            "-documentType", "check", "-outputFile", "Test_CMD_Check_Image.tif",
            "-number", "1234", "-amount", "200.50" ,  "-payee", "TestPayee", "-payer",
            "TestName"
        };

        private string[] argsFrontBack =
        {
            "-documentType", "check", "-outputFile", "Test_CMD_Check_Front_Back_Image.tif",
            "-number", "1234", "-amount", "200.50" ,  "-payee", "TestPayee", "-payer",
            "TestName", "-includeback"
        };

        private string[] argsFails =
        {
            "-documentType", "check", "-outputFile"
        };


        [TestMethod]
        public void GenerateCheckImageFrontSide()
        {
            ImageGeneratorWrapper.GenerateImage(args);
            Assert.IsTrue(File.Exists("Test_CMD_Check_Image.tif"));
        }

        [TestMethod]
        public void GenerateCheckImageFrontAndBack()
        {
            ImageGeneratorWrapper.GenerateImage(argsFrontBack);
            Assert.IsTrue(File.Exists("Test_CMD_Check_Front_Back_Image.tif"));
            Assert.IsTrue(File.Exists("Test_CMD_Check_Front_Back_Image_back.tif"));
        }

        [TestMethod]
        [ExpectedException(typeof(CommandLineException))]
        public void GenerateCheckImageFails()
        {
            ImageGeneratorWrapper.GenerateImage(argsFails);
        }

    }
}
