﻿using System.Drawing.Imaging;
using System.IO;
using ImageGenerators.Interfaces;
using ImageGenerators.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ImageGenerators.IntegrationTests
{
    [TestClass]
    public class CheckImageGeneratorIntegrationTests
    {

        private CheckDetails CheckDetails = new CheckDetails
        {
            Account = "12345",
            Amount = 1000,
            CheckNumber = "001",
            Date = "20010101",
            Memo = "test",
            Payee = "test",
            Payer = "test",
            RoutingNumber = "01234567",
            TransactionCode = "2w3e",
            WatermarkText = "water mark",
            ImageFormat = ImageFormat.Tiff,
            FrontImageFileName = "IntegrationTest_Check.tif"

        };

        private CheckDetails CheckDetailsFrontAndBackSeperateFiles = new CheckDetails
        {
            Account = "12345",
            Amount = 1000,
            CheckNumber = "001",
            Date = "20010101",
            Memo = "test",
            Payee = "test",
            Payer = "test",
            RoutingNumber = "01234567",
            TransactionCode = "2w3e",
            WatermarkText = "water mark",
            ImageFormat = ImageFormat.Tiff,
            FrontImageFileName = "IntegrationTest_Front_Check2.tif",
            BackImageFileName = "IntegrationTest_Back_Check2.tif",
            IncludeBack = true

        };



        private CheckDetails CheckDetailsFrontAndBack = new CheckDetails
        {
            Account = "12345",
            Amount = 1000,
            CheckNumber = "001",
            Date = "20010101",
            Memo = "test",
            Payee = "test",
            Payer = "test",
            RoutingNumber = "01234567",
            TransactionCode = "2w3e",
            WatermarkText = "water mark",
            ImageFormat = ImageFormat.Tiff,
            FrontImageFileName = "IntegrationTest_Check2.tif",
            IncludeBack = true

        };



        [TestMethod]
        public void GenerateCheckImageFrontSide()
        {
            IImageFileGenerator imgGenerator = new CheckImageGenerator
            { CheckDetails = CheckDetails };
            imgGenerator.CreateImageFiles();

            Assert.IsTrue(File.Exists(CheckDetails.FrontImageFileName));
        }


        [TestMethod]
        public void GenerateCheckImageFrontAndBack()
        {
            IImageFileGenerator imgGenerator = new CheckImageGenerator
            { CheckDetails = CheckDetailsFrontAndBack };
            imgGenerator.CreateImageFiles();
            Assert.IsTrue(File.Exists(CheckDetails.FrontImageFileName));
            Assert.IsTrue(File.Exists(CheckDetailsFrontAndBack.BackImageFileName));
        }


        [TestMethod]
        public void GenerateCheckImageFrontAndBackSeperateFiles()
        {
            IImageFileGenerator imgGenerator = new CheckImageGenerator
            { CheckDetails = CheckDetailsFrontAndBackSeperateFiles };
            imgGenerator.CreateImageFiles();
        }
    }
}
