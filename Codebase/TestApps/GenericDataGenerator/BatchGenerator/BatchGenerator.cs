﻿using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using BatchGenerator.Models;
using System.IO;
using System.Linq;
using Transaction = BatchGenerator.Models.Transaction;
using BatchGenerator.Utilities;

namespace BatchGenerator
{
	public class BatchGenerator
	{
		private readonly ILogger _logger;
		private readonly IConfiguration _configuration;
		readonly Random _rand;
		private Settings _settings;
		public BatchGenerator(IConfiguration configuration, ILogger logger)
		{
			_configuration = configuration;
			_logger = logger;
			_rand = new Random();
		}

		public bool BuildBatchFile(StreamWriter stream, Settings settings)
		{
			try
			{
				_settings = settings;
				var data = BuildData();
				var serializer = new Serializer<Models.File>();
				serializer.Serialize(data, stream);
				return true;
			}
			catch (Exception e)
			{
				_logger.Error(e);
			}
			return false;
		}

		public bool BuildBatchFile()
		{
			var settings = Settings.LoadSettings(_configuration);
			string fileName;
			using (var writer =
				new StreamWriter(fileName = $"{settings.OutputFolder}GENERIC_{DateTime.Now.ToString("MMddyyyy_HHmmssfff")}.XML"))
			{
				_logger.Info($"Creating file {fileName}");
				return BuildBatchFile(writer, settings);
			}
		}

		/// <summary>
		/// We build the data in the Generator so the business logic stays here and does not move to the models.  Plus there
		/// are connections between the POCO's objects like TransId is 1-N per batch but a Transaction should not know that (again
		/// business logic in the generator).
		/// </summary>
		/// <returns></returns>
		private Batch BuildBatch()
		{
			return new Batch()
			{
				BatchCueID = _rand.Next(100, 700).ToString(),
				BatchDate = _settings.DepositDate,
				BatchID = _rand.Next(1, 100).ToString(),
				BatchNumber = _rand.Next(1, 100).ToString(),
				DepositDate = _settings.DepositDate,
				ProcessingDate = _settings.DepositDate,
				PaymentType = "SWIFT",
				WorkgroupID = _currentBatch.WorkgroupID ?? "-1",
				BatchSource = _currentBatch.BatchSource
			};
		}

		private int _transactionId;
		private Transaction BuildTransaction()
		{
			_transactionId++;
			return new Transaction()
			{
				TransactionID = _transactionId.ToString()
			};
		}

		private int paymentCount;
		private Payment BuildPayment()
		{
			int clientIdx = paymentCount++;
			if (_currentBatch.ClientAccounts != null && paymentCount >= _currentBatch.ClientAccounts.Count)
			{
				paymentCount = 0;
			}

			return new Payment()
			{
				AccountNumber = DataHelper.GetNumber(_rand, 9),
				Amount = DataHelper.GetAmount(_rand, 8),
				PayerName = DataHelper.GetString(_rand, 16),
				RT = DataHelper.GetNumber(_rand, 10),
				Serial = DataHelper.GetNumber(_rand, 10),
				TransactionCode = "1",
				ABA = _currentBatch.ClientAccounts.Count > 0 ? _currentBatch.ClientAccounts[clientIdx].ABA : "",
				DDA = _currentBatch.ClientAccounts.Count > 0 ? _currentBatch.ClientAccounts[clientIdx].DDA : ""
			};
		}

		// This is public so we can set it for unit tests
		public string[] GraphicFiles;
		private Img BuildImg()
		{
			if (GraphicFiles == null)
			{
				GraphicFiles = Directory.GetFiles(_currentBatch.ImageFolder, "*." + _currentBatch.ImageType);
				if (GraphicFiles.GetLength(0) == 0)
				{
					throw new Exception(
						$"No GraphicFiles of type {_currentBatch.ImageType} found in folder {_currentBatch.ImageFolder}");
				}
			}

			return new Img()
			{
				ImageFilePath = GraphicFiles[_rand.Next(0, GraphicFiles.GetLength(0))],
				ColorMode = _currentBatch.ColorMode,
				FrontBack = _currentBatch.FrontBack
			};
		}

		private RemittanceData BuildRemittanceData()
		{
			if (_currentBatch.RemittanceDataFieldName.Contains(":"))
			{
				var split = _currentBatch.RemittanceDataFieldName.Split(":");
				if (split.GetLength(0) != 2)
				{
					throw new Exception("Invalid RemittanceData");
				}

				return new RemittanceData()
				{
					FieldName = split[0],
					FieldValue = SegmentParser.ParseSegment(split[1])
				};
			}
			return new RemittanceData()
			{
				FieldName = _currentBatch.RemittanceDataFieldName,
				FieldValue = DataHelper.GetString(_rand, 20)
			};
		}

		private BatchSettings _currentBatch;
		public Models.File BuildData()
		{
			var f = new Models.File();
			_logger.Info($"Creating {_settings.Batches.Count} batches.");
			f.Batches = new List<Batch>();
			Batch b;
			foreach (var batch in _settings.Batches)
			{
				_currentBatch = batch;
				_transactionId = 0;
				int nCnt = batch.FixedCount ? batch.BatchCount : _rand.Next(1, _currentBatch.BatchCount + 1);
				for (var bc = 0; bc < nCnt; ++bc)
				{
					f.Batches.Add(b = BuildBatch());
					_transactionId = 0;
					b.Transactions = new List<Transaction>();
					_logger.Info($"Creating {_currentBatch.TransactionCount} transactions for BatchID {b.BatchID}.");
					for (var j = 0; j < _currentBatch.TransactionCount; ++j)
					{
						Transaction t;
						b.Transactions.Add(t = BuildTransaction());

						t.Payments = new List<Payment>();
						_logger.Info(
							$"Creating {_currentBatch.PaymentCount} transactions for BatchID {b.BatchID}, transaction {t.TransactionID}.");
						paymentCount = 0;
						for (var k = 0; k < _currentBatch.PaymentCount; ++k)
						{
							Payment p;
							t.Payments.Add(p = BuildPayment());

							p.Images = new List<Img>
							{
								BuildImg()
							};

							p.RemittanceDataRecord = new List<RemittanceData>
							{
								BuildRemittanceData()
							};
						}
					}
				}
			}
			// Scramble the order of the batches! 
			f.Batches = f.Batches.OrderBy(a => _rand.Next()).ToList();
			return f;
		}
	}
}
