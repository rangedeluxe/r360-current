﻿using BatchGenerator.Utilities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace BatchGenerator
{
	[Serializable]
	public class ClientAccount
	{
		public string ABA { get; set; }
		public string DDA { get; set; }
	}

	[Serializable]
	public class BatchSettings
	{
		public BatchSettings()
		{
			ClientAccounts = new List<ClientAccount>();
		}

		public int TransactionCount { get; set; }
		public int PaymentCount { get; set; }

		public string RemittanceDataFieldName { get; set; }

		public List<ClientAccount> ClientAccounts { get; set; }
		
		public string BatchSource { get; set; }
		public string ColorMode { get; set; }
		public string FrontBack { get; set; }
		public string ImageFolder { get; set; }
		public string ImageType { get; set; }

		public string WorkgroupID { get; set; }

		public int BatchCount { get; set; }
		public bool FixedCount { get; set; }
	}


	[Serializable]
	public class Settings
	{
		public string OutputFolder { get; set; }
		public string BatchFileCount { get; set; }

		public string DepositDate { get; set; }
		public List<BatchSettings> Batches { get; set; }

		public static Settings LoadSettings(IConfiguration configuration)
		{
			Settings settings;

			if (configuration != null)
			{
				settings = new Settings
				{
					OutputFolder = configuration["OutputFolder"],
					DepositDate = configuration["DepositDate"],
					BatchFileCount = configuration["BatchFileCount"],
					Batches = configuration.GetSection("Batches").Get<List<BatchSettings>>()
				};
			}
			else
			{
				settings = new Settings
				{
					OutputFolder = "***INVALID#####",
					DepositDate = "<@CURRENTDATE>",
					BatchFileCount = "2",
					Batches = new List<BatchSettings>()
				};

			}

			if (settings.DepositDate.Contains("<@CURRENTDATE"))
			{
				settings.DepositDate = DataHelper.CalculateDate(settings.DepositDate);
			}

			if (!settings.OutputFolder.EndsWith('\\'))
			{
				settings.OutputFolder += '\\';
			}

			return settings;
		}
	}
}
