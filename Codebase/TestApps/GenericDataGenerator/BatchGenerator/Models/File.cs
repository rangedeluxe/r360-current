﻿using System;
using System.Collections.Generic;

namespace BatchGenerator.Models
{
	[Serializable]
    public class File
    {
	    /// <remarks/>
	    [System.Xml.Serialization.XmlElementAttribute("Batch", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
	    public List<Batch> Batches { get; set; }
	}
}
