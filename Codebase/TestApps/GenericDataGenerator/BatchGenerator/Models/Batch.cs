﻿using System;
using System.Collections.Generic;

namespace BatchGenerator.Models
{
	[Serializable]
    public class Batch
    {
		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute("Transaction", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
		public List<Transaction> Transactions { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string BatchCueID { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string BatchDate { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string BatchID { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string BatchNumber { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string DepositDate { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string ProcessingDate { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string PaymentType { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string WorkgroupID { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string BatchSource { get; set; }

	}
}
