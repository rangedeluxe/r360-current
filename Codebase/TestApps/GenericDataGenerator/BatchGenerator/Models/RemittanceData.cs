﻿using System;


namespace BatchGenerator.Models
{
	[Serializable]
    public class RemittanceData
	{
		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string FieldName { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string FieldValue { get; set; }
	}
}
