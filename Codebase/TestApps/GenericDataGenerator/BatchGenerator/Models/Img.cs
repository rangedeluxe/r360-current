﻿using System;

namespace BatchGenerator.Models
{
	[Serializable]
    public class Img
	{
		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string ImageFilePath { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string ColorMode { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string FrontBack { get; set; }
	}
}
