﻿using System;
using System.Collections.Generic;

namespace BatchGenerator.Models
{
	[Serializable]
    public class Payment
    {
	    [System.Xml.Serialization.XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
	    [System.Xml.Serialization.XmlArrayItemAttribute("Img", typeof(Img), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
		public List<Img> Images { get; set; }

	    [System.Xml.Serialization.XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
	    [System.Xml.Serialization.XmlArrayItemAttribute("RemittanceData", typeof(RemittanceData), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
		public List<RemittanceData> RemittanceDataRecord { get; set; }

	    /// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string AccountNumber { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string Amount { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string PayerName { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string RT { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string Serial { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string TransactionCode { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string ABA { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
		public string DDA { get; set; }

	}
}
