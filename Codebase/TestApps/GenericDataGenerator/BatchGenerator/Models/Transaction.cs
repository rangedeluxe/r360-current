﻿using System;
using System.Collections.Generic;

namespace BatchGenerator.Models
{
	[Serializable]
    public class Transaction
    {
		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute("Payment", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
		public List<Payment> Payments { get; set; }

		/// <remarks/>
		[System.Xml.Serialization.XmlAttributeAttribute()]
	    public string TransactionID { get; set; }
    }
}

