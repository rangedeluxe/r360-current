﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BatchGenerator.Utilities
{
    public static class DataHelper
    {
	    public static string GetNumber(Random random, int length)
	    {
		    length = random.Next(1, length + 1); // Random.Next is (1 - N-1) first param inclusive second exclusive
		    StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < length; ++i)
		    {
			    sb.Append(random.Next(0, 10).ToString());
		    }

		    var result = sb.ToString().TrimStart('0');
		    if (result.Length == 0)
		    {
			    return random.Next(1, 10).ToString(); // don't want a blank string.
		    }

		    return result;
	    }
	    public static string GetAmount(Random random, int length)
	    {
		    var s = GetNumber(random, length);
		    if (s.Length <= 2)
		    {
			    switch (s.Length)
			    {
				    case 1:
					    return $"0.0{s}";
				    case 2:
					    return $"0.{s}";
			    }
		    }
		    return (s.Substring(0, s.Length - 2) + "." + s.Substring(s.Length - 2, 2));
	    }

	    public static string GetString(Random random, int length)
	    {
		    const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		    return new string(Enumerable.Repeat(chars, length)
			    .Select(s => s[random.Next(s.Length)]).ToArray());
	    }

	    public static string CalculateDate(string template)
	    {
		    var regex = new Regex(Regex.Escape(template));
		    var parts = template.Split('~');
		    if (parts.GetLength(0) == 1)
		    {
			    template = regex.Replace(template, DateTime.Now.ToString("yyyy-MM-dd"), 1);
			    return template;
		    }
		    if (parts.GetLength(0) != 2)
		    {
			    throw new Exception("Invalid CURRENTDATE format.  Date must be in the format <@CURRENTDATE> or <@CURRENTDATE~N>, where N days will be subtracted from the current date for the date value.");
		    }

		    var days = Convert.ToInt32(parts[1].Substring(0, parts[1].Length - 1));
		    template = regex.Replace(template, DateTime.Now.AddDays(-days).ToString("yyyy-MM-dd"), 1);
		    return template;
	    }
		public static string GetDate(Random random)
		{
			var dt = DateTime.Now;
		    int year = random.Next(dt.AddYears(-1).Year, dt.AddYears(1).Year);
		    int month = random.Next(1, 13);
		    int days;
		    switch (month)
		    {
			    case 1:
			    case 3:
			    case 5:
			    case 7:
			    case 8:
			    case 10:
			    case 12:
				    days = 32;
				    break;
			    case 2:
				    days = 29;
				    break;
			    case 4:
			    case 9:
			    case 11:
				    days = 31;
				    break;
			    default:
				    days = 1;
				    break;
		    }

		    int day = random.Next(1, days);
		    return $"{year.ToString("D4")}-{month.ToString("D2")}-{day.ToString("D2")}";
	    }
	}
}
