﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace BatchGenerator.Utilities
{
    public class Serializer<T>
    {
	    public void Serialize(T poco, StreamWriter stream)
		{
			var xmlWriterSettings = new XmlWriterSettings() { Indent = true };
			var serializer = new XmlSerializer(typeof(T));
		    using (var writer = XmlWriter.Create(stream, xmlWriterSettings))
		    {
			    serializer.Serialize(writer, poco);
		    }
	    }
    }
}
