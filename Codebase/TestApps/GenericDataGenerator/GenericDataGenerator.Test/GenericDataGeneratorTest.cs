using System;
using System.Collections.Generic;
using System.IO;
using BatchGenerator;
using BatchGenerator.Utilities;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GenericDataGenerator.Test
{
	[TestClass]
	public class GenericDataGeneratorTest
	{
		private static Mock<IConfiguration> _configuration;
		private static Mock<NLog.ILogger> _logger;


		[AssemblyInitialize]
		public static void TestInitialize(TestContext context)
		{
			_logger = new Mock<NLog.ILogger>();
			_configuration = new Mock<IConfiguration>();
		}

		[TestMethod]
		public void BuildBatchFileTest()
		{
			var ms = new MemoryStream();
			var writer = new StreamWriter(ms);
			var generator = new BatchGenerator.BatchGenerator(_configuration.Object, _logger.Object);

			var settings = new Settings()
			{
				OutputFolder = "",
				BatchFileCount = "1",
				DepositDate = "<@CURRENTDATE>"
			};

			settings.Batches = new List<BatchSettings>
			{
				new BatchSettings()
				{
					TransactionCount = 2,
					BatchSource = "Frodo",
					PaymentCount = 2,
					ClientAccounts = new List<ClientAccount>(),
					ColorMode = "1",
					FrontBack = "1",
					ImageFolder = "@@@",
					ImageType = "tif",
					RemittanceDataFieldName = "Don't care"
				}
			};
			settings.Batches[0].ClientAccounts.Add(new ClientAccount() {ABA = "000021222", DDA = "242342342"});

			generator.GraphicFiles = new[] {"Frodo.gif", "bilbo.gif", "smog.gif"};

			Assert.IsTrue(generator.BuildBatchFile(writer, settings));
			Assert.IsFalse(generator.BuildBatchFile(writer, null));
			Assert.IsTrue(settings.Batches[0].ImageFolder == "@@@");
			Assert.IsTrue(settings.Batches[0].ImageType == "tif");
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void InvalidPathTest()
		{
			var generator = new BatchGenerator.BatchGenerator(_configuration.Object, _logger.Object);
			generator.BuildBatchFile();
		}

		[TestMethod]
		public void CalculateDateTest()
		{
			var d1 = DataHelper.CalculateDate("<@CURRENTDATE>");
			DateTime dt = DateTime.Now;
			DateTime.TryParse(d1, out var dt2);
			Assert.IsTrue(dt.Day == dt2.Day && dt.Month == dt2.Month && dt.Year == dt2.Year);

			d1 = DataHelper.CalculateDate("<@CURRENTDATE~1>");
			dt = dt.AddDays(-1);
			DateTime.TryParse(d1, out dt2);

			Assert.IsTrue(dt.Day == dt2.Day && dt.Month == dt2.Month && dt.Year == dt2.Year);

		}

		[TestMethod]
		public void SettingsTest()
		{
			var settings = Settings.LoadSettings(null);
			Assert.IsTrue(settings != null);
		}

		[TestMethod]
		public void GetDateTest()
		{
			var date = DataHelper.GetDate(new Random());
			DateTime dt;
			Assert.IsTrue(DateTime.TryParse(date, out dt));
		}


		[TestMethod]
		public void GetAmountTest()
		{
			var amt = DataHelper.GetAmount(new Random(), 1);
			decimal d;
			Assert.IsTrue(decimal.TryParse(amt, out d));
			amt = DataHelper.GetAmount(new Random(), 2);
			Assert.IsTrue(decimal.TryParse(amt, out d));
			amt = DataHelper.GetAmount(new Random(), 5);
			Assert.IsTrue(decimal.TryParse(amt, out d));

		}
	}
}
