﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using NLog;
using NLog.Web;


namespace GenericDataGenerator
{
    class Program
    {
	    public static IConfiguration Configuration { get; set; }
	    private static readonly string Version = "1.20";
		static int Main(string[] args)
        {
	        var jsonFile = "appsettings.json";
	        var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
	        if (args.Length > 0)
	        {
		        var parm1 = args[0].ToUpper();
		        if (parm1.Contains("/V") || parm1.Contains("-V"))
		        {
			        logger.Info($"GenericDataGenerator Version {Version}");
			        return 0;
		        }

		        if (args[0].ToUpper().Contains(".JSON"))
		        {
			        jsonFile = args[0];
		        }
		        else
		        {
			        logger.Log(LogLevel.Info, args.Length > 1 ? $"Ignoring unknown input arguments: \"{string.Join(", ", args)}\"." : $"Ignoring unknown input argument: \"{args[0]}\".");
		        }
	        }

	        var builder = new ConfigurationBuilder()
		        .SetBasePath(Directory.GetCurrentDirectory())
		        .AddJsonFile(jsonFile);
	        try
	        {
		        Configuration = builder.Build();
		        RunFromConfig(logger);
	        }
	        catch (Exception ex)
	        {
		        logger.Log(LogLevel.Fatal, ex + "\r\nPress any key to continue.");
		        Console.ReadKey();
		        return 1;
	        }
	        return 0;
		}

		private static void RunFromConfig(Logger logger)
		{
			try
			{
				int fileCount = Convert.ToInt32(Configuration["BatchFileCount"]);
				logger.Info($"Creating {fileCount} GraphicFiles.");
				for (int i = 0; i < fileCount; ++i)
				{
					BatchGenerator.BatchGenerator generator = new BatchGenerator.BatchGenerator(Configuration, logger);
					generator.BuildBatchFile();
				}
				logger.Info($"Done creating {fileCount} GraphicFiles.");
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}
			logger.Info($"GenericDataGenerator Version {Version}\r\n");
			Console.WriteLine("Press any key to continue.");
			Console.ReadKey();
		}
	}
}
