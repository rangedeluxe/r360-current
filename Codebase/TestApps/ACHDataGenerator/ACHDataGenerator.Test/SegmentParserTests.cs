﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BatchGenerator.Utilities;

namespace ACHDataGenerator.Test
{
	[TestClass]
	public class SegmentParserTests
	{

		[TestMethod]
		[ExpectedException(typeof(Exception))]
		public void SegmentParseExceptionTest()
		{
			string segment = "RMR*<@NUMBER~5>*<@NUMBER~15*<@NUMBER~10>*<@NUMBER~12>\\";
			SegmentParser.ParseSegment(segment);
		}

		[TestMethod]
		[ExpectedException(typeof(Exception))]
		public void SegmentParseExceptionTest2()
		{
			string template = "RMR*<@NUMBER>*<@NUMBER~15>*<@NUMBER~10>*<@NUMBER~12>\\";
			SegmentParser.ParseSegment(template);
		}
		
		[TestMethod]
		public void SegmentNumberTest()
		{
			var segment = "RMR*<@NUMBER~5>*<@NUMBER~15>*<@NUMBER~10>*<@NUMBER~12>\\";
			var result = SegmentParser.ParseSegment(segment).TrimEnd('\\'); ;

			Assert.IsTrue(result.Contains("<@NUMBER") == false);
			var split = result.Split('*');
			var list = split.Skip(1).ToList();
			Assert.IsTrue(list.Count == 4);
			Int64 num;
			list.ForEach(x => Assert.IsTrue(Int64.TryParse(x.TrimEnd('\\'), out num)));
		}

		[TestMethod]
		public void SegmentDateTest()
		{
			var segment = "RMR*<@DATE>*<@DATE>*<@DATE>\\";
			var result = SegmentParser.ParseSegment(segment).TrimEnd('\\'); ;
			var split = result.Split('*');
			Assert.IsTrue(result.Contains("<@DATE") == false);
			var list = split.Skip(1).ToList();
			Assert.IsTrue(list.Count == 3);
			DateTime dt;
			list.ForEach(x => Assert.IsTrue(DateTime.TryParse($"{x.Substring(4, 2)}/{x.Substring(6, 2)}/{x.Substring(0, 4)}", out dt)));
		}

		[TestMethod]
		public void SegmentMoneyTest()
		{
			var segment = "RMR*<@MONEY~6>*<@MONEY~8>*<@MONEY~12>\\";
			var result = SegmentParser.ParseSegment(segment).TrimEnd('\\'); ;
			var split = result.Split('*');
			Assert.IsTrue(result.Contains("<@MONEY") == false);
			var list = split.Skip(1).ToList();
			Assert.IsTrue(list.Count == 3);
			decimal num;
			list.ForEach(x => Assert.IsTrue(decimal.TryParse(x.TrimEnd('\\'), out num)));
		}

		[TestMethod]
		public void SegmentStringTest()
		{
			var segment = "RMR*<@STRING~6>*<@STRING~8>*<@STRING~250>\\";
			var result = SegmentParser.ParseSegment(segment).TrimEnd('\\'); ;
			var split = result.Split('*');
			Assert.IsTrue(result.Contains("<@STRING") == false);
			var list = split.Skip(1).ToList();
			Assert.IsTrue(list.Count == 3);
			list.ForEach(x => Assert.IsTrue(x.Length > 0));
		}

		[TestMethod]
		public void SegmentSmallAmountTest()
		{
			var segment = "RMR*<@MONEY~0>*<@MONEY~1>\\";
			var result = SegmentParser.ParseSegment(segment).TrimEnd('\\'); ;
		}

		[TestMethod]
		[ExpectedException(typeof(Exception))]
		public void SegmentUnknownCookieTest()
		{
			var segment = "RMR*<@FRODOBAGGINS~6>\\";
			var result = SegmentParser.ParseSegment(segment);
		}


		[TestMethod]
		[ExpectedException(typeof(Exception))]
		public void SegmentCurrentDateTest()
		{
			var segment = "RMR*<@CURRENTDATE~6>\\";

			var result = SegmentParser.ParseSegment(segment).TrimEnd('\\');
			var split = result.Split('*');
			DateTime now = DateTime.Now;
			DateTime dt;
			DateTime.TryParse(split[1], out dt);
            Assert.IsTrue(dt == DateTime.Now.AddDays(-6).Date);

            segment = "RMR*<@CURRENTDATESHORT~6>\\";

			result = SegmentParser.ParseSegment(segment).TrimEnd('\\');
			split = result.Split('*');
			Assert.IsTrue(split[1].Length == 8);
			int num;
			var date = split[1];
			Assert.IsTrue(date.Length == 8);
			Assert.IsTrue(int.TryParse(date.Substring(4, 4), out num));
			Assert.IsTrue(num >= 1900 && num <= DateTime.Now.Year);
			Assert.IsTrue(int.TryParse(date.Substring(0, 2), out num));
			Assert.IsTrue(num >= 1 && num <= 12);
			Assert.IsTrue(int.TryParse(date.Substring(4, 2), out num));
			Assert.IsTrue(num >= 1 && num <= 31);

			segment = "RMR*<@CURRENTDATE>\\";

			result = SegmentParser.ParseSegment(segment);
			split = result.Split('*');
			DateTime.TryParse(split[1].TrimEnd('\\'), out dt);
			Assert.IsTrue(dt == new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));

			segment = "RMR*<@CURRENTDATESHORT>\\";

			result = SegmentParser.ParseSegment(segment);
			split = result.Split('*');
			date = split[1].TrimEnd('\\');
			DateTime.TryParse($"{date.Substring(0,2)}/{date.Substring(2,2)}/{date.Substring(4,4)}", out dt);
			Assert.IsTrue(dt == new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
			Assert.IsTrue(date.Length == 8);
			Assert.IsTrue(int.TryParse(date.Substring(0,2), out num));
			Assert.IsTrue(num >=1 && num <= 12);
			Assert.IsTrue(int.TryParse(date.Substring(2, 2), out num));
			Assert.IsTrue(num >= 1 && num <= 31);
			Assert.IsTrue(int.TryParse(date.Substring(4, 4), out num));
			Assert.IsTrue(num >= 1900 && num <= DateTime.Now.Year);

			segment = "RMR*<@CURRENTDATESHORT~1~2>\\";

			result = SegmentParser.ParseSegment(segment);
		}
	}
}
