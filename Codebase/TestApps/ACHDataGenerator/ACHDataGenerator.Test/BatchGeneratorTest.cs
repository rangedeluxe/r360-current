﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using BatchGenerator.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ACHDataGenerator.Test
{
	[TestClass]
	public class BatchGeneratorTest
	{
		private static List<string> records = null;
		private static Settings _settings = null;
		private static Mock<NLog.ILogger> logger;
		private static Mock<IConfiguration> configuration;

		[AssemblyInitialize]
		public static void TestInitialize(TestContext context)
		{
			var ms = new MemoryStream();
			var writer = new StreamWriter(ms);
			logger = new Mock<NLog.ILogger>();
			configuration = new Mock<IConfiguration>();
			var generator = new BatchGenerator(logger.Object);
			InitSettings(new[] { "CTX", "CCD", "PPD", "CIE", "IAT" });
			generator.BuildBatchFile(writer, Clone());

			writer.Flush();
			records = Encoding.UTF8.GetString(ms.ToArray()).Replace("\r\n", "~").Split('~').ToList().Where(x => x.Length != 0).ToList();
			ms.Close();
			writer.Close();
		}

		private static Settings Clone()
		{
			var settings = new Settings()
			{
				Batches = CloneList(_settings.Batches),
				DepositDate = _settings.DepositDate
			};
			return settings;
		}

		public static List<T> CloneList<T>(List<T> oldList)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			MemoryStream stream = new MemoryStream();
			formatter.Serialize(stream, oldList);
			stream.Position = 0;
			return (List<T>)formatter.Deserialize(stream);
		}

		private static Settings InitSettings(string[] classCodes)
		{
			var settings = new Settings()
			{
				Batches = new List<BatchSettings>(),
				BatchFileCount = "1",
				DepositDate = "7/27/2018"
			};

			foreach (var classCode in classCodes)
			{
				settings.Batches.Add(new BatchSettings { ClassCode = classCode });
				settings.Batches[settings.Batches.Count - 1].BatchCount = 50;
				settings.Batches[settings.Batches.Count - 1].GenerateDupFile = true;
				settings.Batches[settings.Batches.Count - 1].FixedCount = true;
				settings.Batches[settings.Batches.Count - 1].BatchDetailCount = 5;

				settings.Batches[settings.Batches.Count - 1].ClientAccounts = new List<ClientAccount>();
				settings.Batches[settings.Batches.Count - 1].ClientAccounts.Add(new ClientAccount() { ABA = "8888888", DDA = "3333333" });
				settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas = new List<Addenda>();
				settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas.Add(new Addenda() { Count = 1, Segment = "RMR*IV" });
				settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas.Add(new Addenda() { Count = 1, Segment = "BPR*C*<@MONEY~5>*C*ACH*CTX*01*65465465*DA*<@STRING~9>*<@STRING~9>\\TRN*1*<@STRING~5>\\N1*PR*<@STRING~10>\\N1*PE*<@STRING~10>" });
				settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas.Add(new Addenda() { Count = 1, Segment = "RMR*PO*<@STRING~5>**\\REF*IN*<@STRING~10>" });
				settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas.Add(new Addenda() { Count = 1, Segment = "RMR*IV*<@STRING~5>**<@MONEY~5>*<@MONEY~5>*0\\DTM*003*<@DATE>\\REF*PO*<@STRING~10>" });
			}

			ModelBase.Settings = settings;
			_settings = settings;
			return settings;
		}

		private static Settings InvalidClassCodeInitSettings()
		{
			var settings = new Settings()
			{
				Batches = new List<BatchSettings>(),
				BatchFileCount = "1",
				DepositDate = "7/27/2018"
			};

			var classCode = "XYZ";
			settings.Batches.Add(new BatchSettings { ClassCode = classCode });
			settings.Batches[settings.Batches.Count - 1].BatchCount = new Random().Next(1, 10);
			settings.Batches[settings.Batches.Count - 1].BatchDetailCount = new Random().Next(1, 5);

			settings.Batches[settings.Batches.Count - 1].ClientAccounts = new List<ClientAccount>();
			settings.Batches[settings.Batches.Count - 1].ClientAccounts
				.Add(new ClientAccount() { ABA = "8888888", DDA = "3333333" });
			settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas = new List<Addenda>();
			settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas
				.Add(new Addenda() { Count = 1, Segment = "RMR*IV" });
			settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas.Add(new Addenda()
			{
				Count = 1,
				Segment =
					"BPR*C*<@MONEY~5>*C*ACH*CTX*01*65465465*DA*<@STRING~9>*<@STRING~9>\\TRN*1*<@STRING~5>\\N1*PR*<@STRING~10>\\N1*PE*<@STRING~10>"
			});
			settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas
				.Add(new Addenda() { Count = 1, Segment = "RMR*PO*<@STRING~5>**\\REF*IN*<@STRING~10>" });
			settings.Batches[settings.Batches.Count - 1].ClientAccounts[0].Addendas.Add(new Addenda()
			{
				Count = 1,
				Segment = "RMR*IV*<@STRING~5>**<@MONEY~5>*<@MONEY~5>*0\\DTM*003*<@DATE>\\REF*PO*<@STRING~10>"
			});
			return settings;
		}

		[TestMethod]
		public void InvalidBatchTypeTest()
		{
			var ms = new MemoryStream();
			var writer = new StreamWriter(ms);
			var settings = InvalidClassCodeInitSettings();
			var generator = new BatchGenerator(logger.Object);
			Assert.AreEqual(generator.BuildBatchFile(writer, settings), false);
		}

		[TestMethod]
		public void SettingsLoadTest()
		{
			var generator = new BatchGenerator(logger.Object);
			var settings = generator.GetSettings(null);
			Assert.IsTrue(settings != null);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void SettingsExceptionTest()
		{
			var generator = new BatchGenerator(logger.Object);
			var settings = generator.GetSettings(new Mock<IConfiguration>().Object);
		}

		[TestMethod]
		[ExpectedException(typeof(DirectoryNotFoundException))]
		public void BuildBatchFileTest()
		{
			var generator = new BatchGenerator(logger.Object);
			generator.BuildBatchFile();
		}

		[TestMethod]
		public void BuildBatchDupFileTest()
		{
			var generator = new BatchGenerator(logger.Object);
			generator.BuildBatchFile(new StreamWriter(new MemoryStream()), ModelBase.Settings = Clone());
			generator.BuildDupTranFile(new StreamWriter(new MemoryStream()), ModelBase.Settings = Clone());
		}

		[TestMethod]
		public void FileHeaderValidationTest()
		{
			var header = records[0];
			// verify that first record is file header record
			Assert.IsTrue(header[0] == '1');

			// verify that there is ONLY one header record in the file
			var count = records.Count(x => x[0] == '1');
			Assert.IsTrue(count == 1);

		}

		[TestMethod]
		public void BatchAddendaTest()
		{
			var addendas = records.Where(x => x[0] == '7').ToList();
			Assert.IsTrue(addendas.Count > 1);
			Assert.IsTrue(addendas.Any(x => x.Contains("RMR")));
			Assert.IsTrue(addendas.Any(x => x.Contains("BPR")));
			Assert.IsTrue(addendas.Any(x => x.Contains("DTM")));
		}


		[TestMethod]
		public void FileFooterValidationTest()
		{
			var footer = records[records.Count - 1];
			// verify that first record is file footer record
			Assert.IsTrue(footer[0] == '9');

			// verify that there is ONLY one footer record in the file
			var count = records.Count(x => x[0] == '9');
			Assert.IsTrue(count == 1);

			// Validate Batch Count
			Assert.IsTrue(Convert.ToInt32(footer.Substring(1, 6)) == records.Count(x => x[0] == '5'));

			// Validate Block Count
			Assert.IsTrue(Convert.ToInt32(footer.Substring(7, 6)) == records.Count);

			// Validate Entry/Addenda Count
			Assert.IsTrue(Convert.ToInt32(footer.Substring(13, 8)) == records.Count(x => x[0] == '6') + records.Count(x => x[0] == '7'));

			// Validate Entry hash
			Assert.IsTrue(Convert.ToInt64(footer.Substring(21, 10)) == ComputeEntryHash());

			var allCodes = new[] { "27", "37", "22", "32" };

			// Validate Debit Total Amount
			Assert.IsTrue(Convert.ToInt64(footer.Substring(31, 12)) == ComputeTotal(records.Where(x => x[0] == '8').ToList(), allCodes, 20, 12));

			// Validate Credit Total Amount
			Assert.IsTrue(Convert.ToInt64(footer.Substring(43, 12)) == ComputeTotal(records.Where(x => x[0] == '8').ToList(), allCodes, 32, 12));

		}

		[TestMethod]
		public void ValidateRecordOrderTest()
		{
			char lastType = '\0';

			for (int i = 0; i < records.Count; ++i)
			{
				switch (lastType)
				{
					case '\0':
						Assert.IsTrue(records[i][0] == '1', "The first record must be a file header record");
						break;
					case '1':
						Assert.IsTrue(records[i][0] == '5', "The record immediately following a file header must be a batch header.");
						break;
					case '5':
						Assert.IsTrue(records[i][0] == '6', "The record immediately following a batch header must be a batch detail.");
						break;
					case '6':
						Assert.IsTrue(records[i][0] == '6' || records[i][0] == '7' || records[i][0] == '8', "The record immediately following a batch detail must be a batch detail or an addenda record.");
						break;
					case '7':
						Assert.IsTrue(records[i][0] == '6' || records[i][0] == '7' || records[i][0] == '8', "The record immediately following a batch detail must be a batch detail or an addenda record.");
						break;
					case '8':
						Assert.IsTrue(records[i][0] == '5' || records[i][0] == '9', "The record immediately following a batch footer must be a batch header or an file footer record.");
						break;
				}

				lastType = records[i][0];

			}
			Assert.IsTrue(records[records.Count - 1][0] == '9', "the last record in a file must be a file footer.");
			Assert.IsTrue(records.Count(x => x[0] == '1') == 1, "There can be only one file header record.");
			Assert.IsTrue(records.Count(x => x[0] == '9') == 1, "There can be only one file footer record.");
		}

		[TestMethod]
		public void ValidateBatchesTest()
		{
			bool match(string item) => item[0] == '5';

			var bds = new List<string>();
			int i = 0;
			var addendas = new List<string>();
			for (var index = records.FindIndex(match); index > -1; index = records.FindIndex(index + 1, match))
			{
				var header = records[index];
				bds.Clear();
				addendas.Clear();
				for (i = index + 1; i < records.Count && records[i][0] != '8'; i++)
				{
					Assert.IsTrue(records[i][0] == '6' || records[i][0] == '7', "Only Batch Detail and addenda records can follow a batch header record");
					if (records[i][0] == '6')  // sums against detail records in footer
					{
						bds.Add(records[i]);
					}
					if (records[i][0] == '7')  // sums against detail records in footer
					{
						addendas.Add(records[i]);
					}
				}
				Assert.IsTrue(i < records.Count, "Missing batch footer record.");
				var footer = records[i];
				ValidateBatch(header, footer, bds, addendas);
			}
		}

		#region Private Helper Methods

		private void ValidateBatch(string header, string footer, List<string> batchDetails, List<string> addendas)
		{
			Assert.IsTrue(batchDetails.Count <= _settings.Batches.First(x => x.ClassCode == header.Substring(50, 3)).BatchDetailCount);
			Assert.IsTrue(IsValidServiceClass(footer.Substring(01, 3)), "Invalid service class.");

			Assert.IsTrue(Convert.ToInt32(footer.Substring(4, 6)) == batchDetails.Count + addendas.Count);
			Assert.IsTrue(Convert.ToInt32(footer.Substring(10, 10)) == ComputeEntryHash(batchDetails), "Invalid batch footer entry hash.");
			Assert.IsTrue(ComputeTotal(batchDetails, new[] { "27", "37" }, 29, 10) == Convert.ToInt32(footer.Substring(20, 12)), "Debit totals do not match the footer.");
			Assert.IsTrue(ComputeTotal(batchDetails, new[] { "22", "32" }, 29, 10) == Convert.ToInt32(footer.Substring(32, 12)), "Credit totals do not match the footer.");
			// Validate Batch Number
			Assert.IsTrue(footer.Substring(87, 7) == header.Substring(87, 7), "Header and footer batch numbers do not match.");
		}


		private Int64 ComputeTotal(List<string> toSum, string[] transactionCodes, int start, int length)
		{
			var type = toSum.Where(x => transactionCodes.Contains(x.Substring(1, 2))).ToList();
			Int64 totals = 0;

			type.ForEach(x => totals += Convert.ToInt32(x.Substring(start, length)));

			return totals;
		}

		private Int64 ComputeEntryHash(List<string> batchDetails)
		{
			Int64 total = 0;
			batchDetails.ForEach(bh => total += Convert.ToInt64(bh.Substring(3, 8)));

			var sTotalHash = total.ToString();
			if (sTotalHash.Length > 10)
			{
				sTotalHash = sTotalHash.Remove(0, sTotalHash.Length - 10);
			}

			return Convert.ToInt64(sTotalHash);
		}

		private Int64 ComputeEntryHash()
		{
			return ComputeEntryHash(records.Where(x => x[0] == '6').ToList());
		}

		private bool IsValidServiceClass(string code)
		{
			return code == "220"; // only 220 for DIT
		}
		#endregion Private Helper Methods
	}
}
