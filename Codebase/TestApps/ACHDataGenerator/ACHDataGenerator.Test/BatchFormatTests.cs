using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BatchGenerator.Models;
using BatchGenerator.Utilities;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ACHDataGenerator.Test
{
	[TestClass]
	public class BatchFormatTests
	{
		private StreamWriter writer = null;
		private MemoryStream ms = null;
		private string actual = string.Empty;
		readonly List<string> validCodes = new List<string>();
		static readonly List<int> validTransactionCodes = new List<int>();

		[TestInitialize]
		public void TestInitialize()
		{
			var buffer = new byte[96];
			ms = new MemoryStream(buffer);
			actual = string.Empty;
			writer = new StreamWriter(ms);
			validCodes.Add("CTX");
			validCodes.Add("PPD");
			validCodes.Add("CIE");
			validCodes.Add("IAT");
			validCodes.Add("CDD");
			validTransactionCodes.Add(22);
			validTransactionCodes.Add(27);
			validTransactionCodes.Add(32);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			ms.Close();
			writer.Close();
			writer.Dispose();
		}

		[TestMethod]
		public void FileHeaderTest()
		{
			var fh = new FileHeader();

			int num = 0;
			fh.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("1", GetFieldString(fh, "RecordTypeCode"));
			Assert.AreEqual(2, GetFieldString(fh, "PriorityCode").Length);
			Assert.AreEqual(10, GetFieldString(fh, "ImmediateDestination").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(fh, "ImmediateOrigin"), out num));
			Assert.IsTrue(IsValidDate(GetFieldString(fh, "FileCreationDate")));
			Assert.AreEqual(1, GetFieldString(fh, "FileIDModifier").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(fh, "RecordSize"), out num));
			Assert.AreEqual(2, GetFieldString(fh, "BlockingFactor").Length);
			Assert.AreEqual(1, GetFieldString(fh, "FormatCode").Length);
			Assert.AreEqual(23, GetFieldString(fh, "ImmediateDestinationName").Length);
			Assert.AreEqual(23, GetFieldString(fh, "ImmediateOriginName").Length);
			Assert.AreEqual(8, GetFieldString(fh, "ReferenceCode").Length);
		}

		[TestMethod]
		public void HelperTest()
		{
			var fh = new FileHeader();
			Assert.AreEqual("", GetFieldString(fh, "FrodoBaggins"));
			Assert.IsFalse(IsValidDate("112219999"));
			var settings = new Settings() {BatchFileCount = "1", OutputFolder = "c:\\temp\\"};
			Assert.AreEqual(settings.BatchFileCount, "1");
			Assert.AreEqual(settings.OutputFolder, "c:\\temp\\");
			var bg = new BatchGenerator(new Mock<IConfiguration>().Object, new Mock<NLog.ILogger>().Object);
			SegmentParser.GetAmount(new Random(), 6);
		}

		[TestMethod]
		public void BatchHeaderTest()
		{
			var bh = new BatchHeader("CTX", new Settings {DepositDate = DateTime.Now.ToString("MM/dd/yyyy")});
			int num = 0;
			bh.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("5", GetFieldString(bh, "RecordTypeCode"));
			Assert.IsTrue(int.TryParse(GetFieldString(bh, "ServiceClassCode"), out num));
			Assert.AreEqual(16, GetFieldString(bh, "CompanyName").Length);

			Assert.AreEqual(20, GetFieldString(bh, "CompanyDiscretionaryData").Length);

			Assert.IsTrue(int.TryParse(GetFieldString(bh, "CompanyIdentification"), out num));

			Assert.IsTrue(IsValidEntryCode(GetFieldString(bh, "EntryClassCode")));
			Assert.AreEqual(10, GetFieldString(bh, "CompanyDescription").Length);
			Assert.AreEqual(6, GetFieldString(bh, "CompanyDescriptiveDate").Length);

			Assert.IsTrue(IsValidDate(GetFieldString(bh, "EffectiveEntryDate")));
			Assert.AreEqual("000", GetFieldString(bh, "SettlementDate"));
			Assert.AreEqual(1, GetFieldString(bh, "OriginatorStatusCode").Length);
			Assert.AreEqual(9, GetFieldString(bh, "OriginatingDFI").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bh, "BatchNumber"), out num));
		}

		private Settings InitSettings(string classCode, int count = 1)
		{
			var settings = new Settings();
			settings.Batches = new List<BatchSettings>();
			settings.Batches.Add(new BatchSettings { ClassCode = classCode });
			settings.Batches[0].ClientAccounts = new List<ClientAccount>();
			settings.Batches[0].ClientAccounts.Add(new ClientAccount() { ABA = "8888888", DDA = "3333333"});
			settings.Batches[0].ClientAccounts[0].Addendas.Add(new Addenda(){Count = count, Segment= "BPR*C*<@MONEY~5>*C*ACH*CTX*01*65465465*DA*<@STRING~9>*<@STRING~9>\\TRN*1*<@STRING~5>\\N1*PR*<@STRING~10>\\N1*PE*<@STRING~10>" });
			settings.Batches[0].ClientAccounts[0].Addendas.Add(new Addenda() { Count = count, Segment = "RMR*PO*<@STRING~5>**\\REF*IN*<@STRING~10>" });
			settings.Batches[0].ClientAccounts[0].Addendas.Add(new Addenda() { Count = count, Segment = "RMR*IV*<@STRING~5>**<@MONEY~5>*<@MONEY~5>*0\\DTM*003*<@DATE>\\REF*PO*<@STRING~10>" });
			settings.DepositDate = DateTime.Now.ToString("MM/dd/yyyy");
			ModelBase.Settings = settings;
			return settings;
		}

		[TestMethod]
		public void CTXBatchDetailTest()
		{
			var settings = InitSettings("CTX");
			var bd = new CTXBatchDetail(settings.Batches[0].ClientAccounts[0]);
			int num = 0;
			bd.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("6", GetFieldString(bd, "RecordTypeCode"));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TransactionCode"), out num));
			Assert.IsTrue(IsValidTransactioncode(num));
			Assert.AreEqual(9, GetFieldString(bd, "DFIIdentification").Length);
			Assert.AreEqual("8888888", GetFieldString(bd, "DFIIdentification").Trim());
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "DFIAccountNumber"), out num));
			Assert.AreEqual("3333333", GetFieldString(bd, "DFIAccountNumber").Trim());
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "Amount"), out num));
			Assert.AreEqual(15, GetFieldString(bd, "IdentificationNumber").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "NumberAddendaRecords"), out num));
			Assert.AreEqual(16, GetFieldString(bd, "ReceivingCompany").Length);
			Assert.AreEqual(2, GetFieldString(bd, "Reserved").Length);
			Assert.AreEqual(2, GetFieldString(bd, "DiscretionaryData").Length);
			Assert.AreEqual(1, GetFieldString(bd, "AddendaRecordIndicator").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TraceNumber"), out num));
		}

		[TestMethod]
		public void CTXBatchDetailMultipleAddendaTest()
		{
			var settings = InitSettings("CTX", 7);
			var bd = new CTXBatchDetail(settings.Batches[0].ClientAccounts[0]);
		
			int num = 0;
			bd.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("6", GetFieldString(bd, "RecordTypeCode"));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TransactionCode"), out num));
			Assert.IsTrue(IsValidTransactioncode(num));
			Assert.AreEqual(9, GetFieldString(bd, "DFIIdentification").Length);
			Assert.AreEqual("8888888", GetFieldString(bd, "DFIIdentification").Trim());
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "DFIAccountNumber"), out num));
			Assert.AreEqual("3333333", GetFieldString(bd, "DFIAccountNumber").Trim());
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "Amount"), out num));
			Assert.AreEqual(15, GetFieldString(bd, "IdentificationNumber").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "NumberAddendaRecords"), out num));
			Assert.AreEqual(16, GetFieldString(bd, "ReceivingCompany").Length);
			Assert.AreEqual(2, GetFieldString(bd, "Reserved").Length);
			Assert.AreEqual(2, GetFieldString(bd, "DiscretionaryData").Length);
			Assert.AreEqual(1, GetFieldString(bd, "AddendaRecordIndicator").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TraceNumber"), out num));
		}


		[TestMethod]
		public void CCDBatchDetailTest()
		{
			var settings = InitSettings("CCD");
			var bd = new CCDBatchDetail(settings.Batches[0].ClientAccounts[0]);
			int num = 0;
			bd.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("6", GetFieldString(bd, "RecordTypeCode"));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TransactionCode"), out num));
			Assert.IsTrue(IsValidTransactioncode(num));
			Assert.AreEqual(9, GetFieldString(bd, "DFIIdentification").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "DFIAccountNumber"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "Amount"), out num));
			Assert.AreEqual(15, GetFieldString(bd, "IdentificationNumber").Length);
			Assert.AreEqual(22, GetFieldString(bd, "CompanyName").Length);
			Assert.AreEqual(2, GetFieldString(bd, "DiscretionaryData").Length);
			Assert.AreEqual(1, GetFieldString(bd, "AddendaRecordIndicator").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TraceNumber"), out num));
		}

		[TestMethod]
		public void CIEBatchDetailTest()
		{
			var settings = InitSettings("CIE");
			var bd = new CIEBatchDetail(settings.Batches[0].ClientAccounts[0]);
			int num = 0;
			bd.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("6", GetFieldString(bd, "RecordTypeCode"));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TransactionCode"), out num));
			Assert.IsTrue(IsValidTransactioncode(num));
			Assert.AreEqual(9, GetFieldString(bd, "DFIIdentification").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "DFIAccountNumber"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "Amount"), out num));
			Assert.AreEqual(15, GetFieldString(bd, "IdentificationNumber").Length);
			Assert.AreEqual(22, GetFieldString(bd, "IndividualName").Length);
			Assert.AreEqual(2, GetFieldString(bd, "DiscretionaryData").Length);
			Assert.AreEqual(1, GetFieldString(bd, "AddendaRecordIndicator").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TraceNumber"), out num));
		}

		[TestMethod]
		public void IATBatchDetailTest()
		{
			var settings = InitSettings("IAT");
			var bd = new IATBatchDetail(settings.Batches[0].ClientAccounts[0]);
			int num = 0;
			bd.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("6", GetFieldString(bd, "RecordTypeCode"));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TransactionCode"), out num));
			Assert.IsTrue(IsValidTransactioncode(num));
			Assert.AreEqual(9, GetFieldString(bd, "DFIIdentification").Length);
            Assert.IsTrue(int.TryParse(GetFieldString(bd, "NumberAddendaRecords"), out num));
            Assert.IsTrue(int.TryParse(GetFieldString(bd, "Amount"), out num));
            Assert.AreEqual(35, GetFieldString(bd, "DFIAccountNumber").Length);
			Assert.AreEqual(1, GetFieldString(bd, "OFACIndicator1").Length);
			Assert.AreEqual(1, GetFieldString(bd, "OFACIndicator2").Length);
            Assert.AreEqual(1, GetFieldString(bd, "AddendaRecordIndicator").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TraceNumber"), out num));
		}

		[TestMethod]
		public void PPDBatchDetailTest()
		{
			var settings = InitSettings("PPD");
			var bd = new PPDBatchDetail(settings.Batches[0].ClientAccounts[0]);
			int num = 0;
			bd.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("6", GetFieldString(bd, "RecordTypeCode"));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TransactionCode"), out num));
			Assert.IsTrue(IsValidTransactioncode(num));
			Assert.AreEqual(9, GetFieldString(bd, "DFIIdentification").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "DFIAccountNumber"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "Amount"), out num));
			Assert.AreEqual(15, GetFieldString(bd, "IdentificationNumber").Length);
			Assert.AreEqual(22, GetFieldString(bd, "IndividualName").Length);
			Assert.AreEqual(2, GetFieldString(bd, "DiscretionaryData").Length);
			Assert.AreEqual(1, GetFieldString(bd, "AddendaRecordIndicator").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "AddendaRecordIndicator"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bd, "TraceNumber"), out num));
		}

		[TestMethod]
		public void BatchFooterTest()
		{
			var settings = InitSettings("CTX");
			var batch = new Batch
			{
				BatchHeader = new BatchHeader("CTX", settings)
			};
			batch.BatchHeader.BatchNumber = "00001";
			batch.BatchDetails.Add(new CTXBatchDetail(settings.Batches[0].ClientAccounts[0]));

			var bf = new BatchFooter(batch);
			int num = 0;
			bf.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("8", GetFieldString(bf, "RecordTypeCode"));
			Assert.IsTrue(IsValidServiceClass(GetFieldString(bf, "ServiceClassCode")));
			Assert.IsTrue(IsValidServiceClass(GetFieldString(bf, "ServiceClassCode")));
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "EntryAddendaCount"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "EntryHash"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "TotalDebitAmount"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "TotalCreditAmount"), out num));

			Assert.AreEqual(10, GetFieldString(bf, "CompanyIdentification").Length);
			Assert.AreEqual(19, GetFieldString(bf, "MessageAuthentication").Length);
			Assert.AreEqual(6, GetFieldString(bf, "Reserved").Length);
			Assert.AreEqual(8, GetFieldString(bf, "OriginDFIIdentification").Length);
			Assert.AreEqual(7, GetFieldString(bf, "BatchNumber").Length);
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "BatchNumber"), out num));
		}

		[TestMethod]
		public void FileFooterTest()
		{
			var settings = InitSettings("CTX");
			var batch = new Batch
			{
				BatchHeader = new BatchHeader("CTX", settings)
			};
			batch.BatchHeader.BatchNumber = "00001";
			batch.BatchDetails.Add(new CTXBatchDetail(settings.Batches[0].ClientAccounts[0]));
			batch.BatchFooter = new BatchFooter(batch);
			
			var batches = new List<Batch>();
			batches.Add(batch);
			var bf = new FileFooter(batches, 1);
			int num = 0;
			bf.WriteRecord(writer);
			writer.Flush();
			actual = Encoding.UTF8.GetString(ms.ToArray());

			Assert.AreEqual(96, actual.Length);
			Assert.AreEqual("9", GetFieldString(bf, "RecordTypeCode"));
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "BatchCount"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "BlockCount"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "EntryHash"), out num));
			
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "EntryAddendaCount"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "TotalDebitAmount"), out num));
			Assert.IsTrue(int.TryParse(GetFieldString(bf, "TotalCreditAmount"), out num));

			Assert.AreEqual(39, GetFieldString(bf, "Reserved").Length);
		}


		private bool IsValidServiceClass(string code)
		{
			return code == "220"; // only 220 for DIT
		}

		private bool IsValidTransactioncode(int num)
		{
			return validTransactionCodes.Contains(num);
		}

		private bool IsValidEntryCode(string code)
		{
			return validCodes.Contains(code);
		}

		private bool IsValidDate(string date)
		{
			if (date.Length != 6)
				return false;
			string year = date.Substring(0, 2);
			string month = date.Substring(2, 2);
			string day = date.Substring(4, 2);
			DateTime dt;
			return DateTime.TryParse($"{month}/{day}/{year}", out dt);
		}

		private string GetFieldString(ModelBase m, string field)
		{
			var tuple = m.StartAndLength.FirstOrDefault(x => x.Item1 == field);
			if (tuple == null)
				return "";
			return actual.Substring(tuple.Item2, tuple.Item3);
		}
	}
}
