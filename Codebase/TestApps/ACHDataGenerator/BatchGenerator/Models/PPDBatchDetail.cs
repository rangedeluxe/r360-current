﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACHDataGenerator;

namespace BatchGenerator.Models
{
	public class PPDBatchDetail : DetailBase
	{
		public PPDBatchDetail(ClientAccount clientAccount) : base(clientAccount)
		{
			StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
			StartAndLength.Add(new Tuple<string, int, int>("TransactionCode", 1, 2));
			StartAndLength.Add(new Tuple<string, int, int>("DFIIdentification", 3, 9));
			StartAndLength.Add(new Tuple<string, int, int>("DFIAccountNumber", 12, 17));
			StartAndLength.Add(new Tuple<string, int, int>("Amount", 29, 10));
			StartAndLength.Add(new Tuple<string, int, int>("IdentificationNumber", 39, 15));
			StartAndLength.Add(new Tuple<string, int, int>("IndividualName", 54, 22));
			StartAndLength.Add(new Tuple<string, int, int>("DiscretionaryData", 76, 2));
			StartAndLength.Add(new Tuple<string, int, int>("AddendaRecordIndicator", 78, 1));
			StartAndLength.Add(new Tuple<string, int, int>("TraceNumber", 79, 15));
			Addendas = new List<AddendaBase>();
			RecordTypeCode = ParsedRecordType(RecordType.BATCH_DETAIL);
			TransactionCode = GetTransactionCode();
			Amount = GetAmount();
			IdentificationNumber = "IndividualId".PadLeft(15, ' '); 
			IndividualName = GetIndividualName();
			DiscretionaryData = "XX";
			// this could be either PPD or CIE so we check here:
			var code = GetType().ToString().Substring(22, 3).ToUpper();
			AddendaRecordIndicator = Settings.Batches.First(n => n.ClassCode == code).ClientAccounts.Sum(a => a.Addendas.Count) > 0 ? "1" : "0";
			TraceNumber = GetTraceNumber();
		}

		private string GetIndividualName()
		{
			string[] names = {"FRODO BAGGINS", "SAMWISE", "GANDALF", "JACOB MURREY", "JJ JET PLANE"};
			return names[random.Next(0, names.Length - 1)];
		}

		public string IndividualName { get; set; }

		public string IdentificationNumber { get; set; }
		public string DiscretionaryData { get; set; }
	}
}
