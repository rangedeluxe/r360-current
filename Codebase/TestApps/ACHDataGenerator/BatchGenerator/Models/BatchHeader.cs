﻿using System;
using ACHDataGenerator;
using BatchGenerator.Utilities;

namespace BatchGenerator.Models
{
	public class BatchHeader : ModelBase
	{
		public string ServiceClassCode { get; set; }
		public string CompanyName { get; set; }
		public string CompanyDiscretionaryData { get; set; }
		public string CompanyIdentification { get; set; }
		public string EntryClassCode { get; set; }
		public string CompanyDescription { get; set; }
		public string CompanyDescriptiveDate { get; set; }
		public string EffectiveEntryDate { get; set; }
		public string SettlementDate { get; set; }
		public string OriginatorStatusCode { get; set; }
		public string OriginatingDFI { get; set; }
		public string BatchNumber { get; set; }

		private static int _batchNumber = 1;
		public string ISOOriginatingCurrencyCode { get; set; }
		public string ISODestinationCurrencyCode { get; set; }

		public string ForeignExchangeIndicator { get; set; }
		public string ForeignExchangeRefIndicator { get; set; }
		public string ForeignExchangeReference { get; set; }
		public string ISODestinationCountryCode { get; set; }

		public BatchHeader(string entryClassCode, Settings settings)
		{
			StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
			StartAndLength.Add(new Tuple<string, int, int>("ServiceClassCode", 1, 3));
			StartAndLength.Add(new Tuple<string, int, int>("CompanyName", 4, 16));
			if (entryClassCode == "IAT")
			{
				StartAndLength.Add(new Tuple<string, int, int>("ForeignExchangeIndicator", 20, 2));
				StartAndLength.Add(new Tuple<string, int, int>("ForeignExchangeRefIndicator", 22, 1));
				StartAndLength.Add(new Tuple<string, int, int>("ForeignExchangeReference", 23, 15));
				StartAndLength.Add(new Tuple<string, int, int>("ISODestinationCountryCode", 38, 2));
			}
			else
			{

				StartAndLength.Add(new Tuple<string, int, int>("CompanyDiscretionaryData", 20, 20));
			}
			StartAndLength.Add(new Tuple<string, int, int>("CompanyIdentification", 40, 10));
			StartAndLength.Add(new Tuple<string, int, int>("EntryClassCode", 50, 3));
			StartAndLength.Add(new Tuple<string, int, int>("CompanyDescription", 53, 10));
			if (entryClassCode == "IAT")
			{
				StartAndLength.Add(new Tuple<string, int, int>("ISOOriginatingCurrencyCode", 63, 3));
				StartAndLength.Add(new Tuple<string, int, int>("ISODestinationCurrencyCode", 66, 3));
			}
			else
			{
				StartAndLength.Add(new Tuple<string, int, int>("CompanyDescriptiveDate", 63, 6));
			}
			StartAndLength.Add(new Tuple<string, int, int>("EffectiveEntryDate", 69, 6));
			StartAndLength.Add(new Tuple<string, int, int>("SettlementDate", 75, 3));
			StartAndLength.Add(new Tuple<string, int, int>("OriginatorStatusCode", 78, 1));
			StartAndLength.Add(new Tuple<string, int, int>("OriginatingDFI", 79, 9));
			StartAndLength.Add(new Tuple<string, int, int>("BatchNumber", 87, 7));
			RecordTypeCode = ParsedRecordType(RecordType.BATCH_HEADER);
			SettlementDate = "000";
			ServiceClassCode = GetServiceClassCode();
			CompanyName = "DELUXE CORP";
			CompanyIdentification = "0720000961";
			EntryClassCode = entryClassCode;
			if (EntryClassCode == "IAT")
			{
				ISOOriginatingCurrencyCode = GetCurrencyCode();
				ISODestinationCurrencyCode = GetCurrencyCode();
				ForeignExchangeIndicator = "FF";
				ForeignExchangeRefIndicator = "3";
				ForeignExchangeReference = "FOREIGNEXCHREF ";
				ISODestinationCountryCode = "US";
			}
			else
			{
				CompanyDiscretionaryData = "DISCRESIONARYDATA";
				CompanyDescriptiveDate = DateTime.Now.ToString("yyMMdd");
			}

			CompanyDescription = "PAYMENT";
			EffectiveEntryDate = DateTime.Parse(settings.DepositDate).ToString("yyMMdd");
			OriginatorStatusCode = "1";
			OriginatingDFI = " 072000096";
			BatchNumber = (_batchNumber++).ToString().PadLeft(7, '0');
		}
	}
}
