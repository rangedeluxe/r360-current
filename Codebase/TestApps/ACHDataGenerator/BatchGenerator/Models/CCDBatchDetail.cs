﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACHDataGenerator;

namespace BatchGenerator.Models
{
	public class CCDBatchDetail : DetailBase
	{
		public CCDBatchDetail(ClientAccount clientAccount) : base(clientAccount)
		{
			StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
			StartAndLength.Add(new Tuple<string, int, int>("TransactionCode", 1, 2));
			StartAndLength.Add(new Tuple<string, int, int>("DFIIdentification", 3, 9));
			StartAndLength.Add(new Tuple<string, int, int>("DFIAccountNumber", 12, 17));
			StartAndLength.Add(new Tuple<string, int, int>("Amount", 29, 10));
			StartAndLength.Add(new Tuple<string, int, int>("IdentificationNumber", 39, 15));
			StartAndLength.Add(new Tuple<string, int, int>("CompanyName", 54, 22));
			StartAndLength.Add(new Tuple<string, int, int>("DiscretionaryData", 76, 2));
			StartAndLength.Add(new Tuple<string, int, int>("AddendaRecordIndicator", 78, 1));
			StartAndLength.Add(new Tuple<string, int, int>("TraceNumber", 79, 15));
			Addendas = new List<AddendaBase>();
			RecordTypeCode = ParsedRecordType(RecordType.BATCH_DETAIL);
			TransactionCode = GetTransactionCode();
			Amount = GetAmount();
			IdentificationNumber = "000004534445".PadLeft(15, '0');
			CompanyName = GetCompanyName();
			DiscretionaryData = "XX";
			// This first will work cuz we would not be here if no CCD Class codes in file
			AddendaRecordIndicator = Settings.Batches.First(n => n.ClassCode == "CCD").ClientAccounts.Sum(a => a.Addendas.Count) > 0 ? "1" : "0";
			TraceNumber = GetTraceNumber();
		}

		private string GetCompanyName()
		{
			string[] names = { "BOEING", "GOOGLE", "ESTWING", "APPLE", "JASONS TRUST" };
			return names[random.Next(0, names.Length)];
		}
		public string CompanyName { get; set; }

		public string IdentificationNumber { get; set; }
		public string DiscretionaryData { get; set; }
	}
}
