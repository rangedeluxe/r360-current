﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using ACHDataGenerator;
using BatchGenerator.Utilities;

namespace BatchGenerator.Models
{
	public class CTXAddenda : AddendaBase
	{
		const char defaultDelimiter = '\\';

		public static int WriteAddenda(StreamWriter writer, DetailBase detail, int sequence, char segmentDelimiter)
		{
			string parsedSegment = "";
			if (SegmentParser.ParseStaticSegment(detail.ClientAccount.Addendas[0].Segment, segmentDelimiter, ref parsedSegment))
			{
				if (detail.ClientAccount.Addendas.Count > 1)
				{
					throw new Exception("Static addenda's must have a count of 1");
				}
				writer.WriteLine(parsedSegment);
				return 1;
			}

			int blockCount = 0;
			CTXAddenda addenda = null;
			List<AddendaBase> addendas = new List<AddendaBase>();
			// Write static data only for first two records that do are already 80 bytes for paymentinfo
			var records = BuildNonStaticAddendaPayload(detail.ClientAccount, segmentDelimiter);

			int idx = 1;
			for (int i = 0; i < 2; ++i)
			{
				addenda = new CTXAddenda(detail, idx)
				{
					PaymentInfo = StaticData(i, segmentDelimiter),
					DetailSequenceNumber = sequence.ToString().PadLeft(7, '0')
				};
				addenda.WriteRecord(writer);
				addendas.Add(addenda);
				idx++;
				blockCount++;
			}

			records = StaticData(2, segmentDelimiter) + records + StaticData(3, segmentDelimiter);
			var payload = string.Empty;
			while (records.Length > 0)
			{
				if (records.Length > 80)
				{
					payload = records.Substring(0, 80);
					records = records.Substring(80);
				}
				else
				{
					payload = records.PadRight(80, ' ');
					records = string.Empty;
				}
				addenda = new CTXAddenda(detail, idx++)
				{
					DetailSequenceNumber = sequence.ToString().PadLeft(7, '0'),
					PaymentInfo = payload
				};
				addenda.WriteRecord(writer);
				addendas.Add(addenda);
				blockCount++;
			}

			detail.Addendas = addendas;
			return blockCount;
		}

		private static Random _rand = null;
		private static Random Random
		{
			get
			{
				if (_rand == null)
				{
					_rand = new Random();
				}
				return _rand;
			}
		}

		private static string BuildNonStaticAddendaPayload(ClientAccount clientAccount, char segmentDelimiter)
		{
			var sb = new StringBuilder();
			var random = Random;
			foreach (var addenda in clientAccount.Addendas)
			{
				var count = random.Next(1, addenda.Count + 1);
				for (int i = 0; i < count; ++i)
				{
					try
					{
						sb.Append(SegmentParser.ParseSegment(addenda.Segment));
					}
					catch (Exception e)
					{
						Console.WriteLine(e);
						throw;
					}
				}
			}
			if (segmentDelimiter == defaultDelimiter)
			{
				return sb.ToString();
			}
			return sb.ToString().Replace(defaultDelimiter, segmentDelimiter);
		}

		public CTXAddenda(DetailBase detail, int addendaSequence)
		{
			StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
			StartAndLength.Add(new Tuple<string, int, int>("AddendaTypeCode", 1, 2));
			StartAndLength.Add(new Tuple<string, int, int>("PaymentInfo", 3, 80));
			StartAndLength.Add(new Tuple<string, int, int>("AddendaSequenceNumber", 83, 4));
			StartAndLength.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));
			RecordTypeCode = ParsedRecordType(RecordType.BATCH_ADDENDA);
			AddendaTypeCode = GetAddendaType(detail);
			AddendaSequenceNumber = addendaSequence.ToString().PadLeft(4, '0');
		}

		private static List<string> staticData = null;
		private static string StaticData(int row, char segmentDelimiter)
		{
			if (staticData == null)
			{
				staticData = new List<string>();
				staticData.Add("ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*172");
				staticData.Add($"4*U*00400*076913354*0*P*>\\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*004");
				staticData.Add($"010\\ST*820*000000268\\");
				staticData.Add($"SE*19*000000268\\GE*1*76913354\\IEA*1*076913354\\");
			}
			if (segmentDelimiter == defaultDelimiter)
			{
				return staticData[row];
			}
			return staticData[row].Replace(defaultDelimiter, segmentDelimiter);
		}

	}
}
