﻿using System;
using System.Linq;

namespace BatchGenerator.Models
{
	public class BatchFooter : ModelBase
	{
		public BatchFooter(Batch batch)
		{
			StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
			StartAndLength.Add(new Tuple<string, int, int>("ServiceClassCode", 1, 3));
			StartAndLength.Add(new Tuple<string, int, int>("EntryAddendaCount", 4, 6));
			StartAndLength.Add(new Tuple<string, int, int>("EntryHash", 10, 10));
			StartAndLength.Add(new Tuple<string, int, int>("TotalDebitAmount", 20, 12));
			StartAndLength.Add(new Tuple<string, int, int>("TotalCreditAmount", 32, 12));
			StartAndLength.Add(new Tuple<string, int, int>("CompanyIdentification", 44, 10));
			StartAndLength.Add(new Tuple<string, int, int>("MessageAuthentication", 54, 19));
			StartAndLength.Add(new Tuple<string, int, int>("Reserved", 73, 6));
			StartAndLength.Add(new Tuple<string, int, int>("OriginDFIIdentification", 79, 8));
			StartAndLength.Add(new Tuple<string, int, int>("BatchNumber", 87, 7));
			RecordTypeCode = ParsedRecordType(RecordType.BATCH_FOOTER);
			ServiceClassCode = GetServiceClassCode();

            int addendaCountMultiplier = (batch.BatchHeader.EntryClassCode == "IAT") ? (8) : (1);
			EntryAddendaCount = (batch.BatchDetails.Count + batch.BatchDetails.Sum(a => (a.Addendas.Count *addendaCountMultiplier))).ToString().PadLeft(6, '0');
			EntryHash = ComputeEntryHash(batch);
			decimal totals = batch.BatchDetails.Where(d => d.TransactionCode == "27" || d.TransactionCode == "37")
				                 .Sum(a => Convert.ToDecimal(a.Amount));
			TotalDebitAmount = totals.ToString().PadLeft(12,'0');
			totals = batch.BatchDetails.Where(d => d.TransactionCode == "22" || d.TransactionCode == "32")
				.Sum(a => Convert.ToDecimal(a.Amount));
			TotalCreditAmount = totals.ToString().PadLeft(12, '0');
			CompanyIdentification = "0720000961";
			MessageAuthentication = "";
			Reserved = "";
			OriginDFIIdentification = " 072000096";
			BatchNumber = batch.BatchHeader.BatchNumber;
		}

		public string ComputeEntryHash(Batch batch)
		{
			Int64 total = batch.BatchDetails.Sum(r => Convert.ToInt64(r.DFIIdentification));

			var sTotalHash = total.ToString();
			if (sTotalHash.Length > 10)
			{
				sTotalHash = sTotalHash.Remove(0, sTotalHash.Length - 10);
			}

			return sTotalHash;
		}

		public string ServiceClassCode { get; set; }
		public string EntryAddendaCount { get; set; }
		public string EntryHash { get; set; }

		public string TotalDebitAmount { get; set; }
		public string TotalCreditAmount { get; set; }
		public string CompanyIdentification { get; set; }
		public string MessageAuthentication { get; set; }
		public string Reserved { get; set; }
		public string OriginDFIIdentification { get; set; }
		public string BatchNumber { get; set; }
	}
}
