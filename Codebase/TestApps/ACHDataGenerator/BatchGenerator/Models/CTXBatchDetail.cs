﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACHDataGenerator;

namespace BatchGenerator.Models
{
	public class CTXBatchDetail : DetailBase
	{
		public CTXBatchDetail(ClientAccount clientAccount) : base(clientAccount)
		{
			StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
			StartAndLength.Add(new Tuple<string, int, int>("TransactionCode", 1, 2));
			StartAndLength.Add(new Tuple<string, int, int>("DFIIdentification", 3, 9));
			StartAndLength.Add(new Tuple<string, int, int>("DFIAccountNumber", 12, 17));
			StartAndLength.Add(new Tuple<string, int, int>("Amount", 29, 10));
			StartAndLength.Add(new Tuple<string, int, int>("IdentificationNumber", 39, 15));
			StartAndLength.Add(new Tuple<string, int, int>("NumberAddendaRecords", 54, 4));
			StartAndLength.Add(new Tuple<string, int, int>("ReceivingCompany", 58, 16));
			StartAndLength.Add(new Tuple<string, int, int>("Reserved", 74, 2));
			StartAndLength.Add(new Tuple<string, int, int>("DiscretionaryData", 76, 2));
			StartAndLength.Add(new Tuple<string, int, int>("AddendaRecordIndicator", 78, 1));
			StartAndLength.Add(new Tuple<string, int, int>("TraceNumber", 79, 15));
			Addendas = new List<AddendaBase>();
			RecordTypeCode = ParsedRecordType(RecordType.BATCH_DETAIL);
			TransactionCode = GetTransactionCode();
			Amount = GetAmount();
			IdentificationNumber = "07000096";
			var addenda = Settings.Batches.Single(n => n.ClassCode == "CTX").ClientAccounts.Sum(a => a.Addendas.Count);
			NumberAddendaRecords = addenda > 0 ? addenda.ToString().PadLeft(4, '0') : "0000";
			ReceivingCompany = GetReceivingCompany();
			Reserved = "  ";
			DiscretionaryData = "XX";
			// This first will work cuz we would not be here if no CTX Class codes in file
			AddendaRecordIndicator = Settings.Batches.First(n => n.ClassCode == "CTX").ClientAccounts.Sum(a => a.Addendas.Count) > 0 ? "1" : "0";
			TraceNumber = GetTraceNumber();
		}

		private string GetReceivingCompany()
		{
			string[] companies = { "FREDS BANK LLC", "DAKOTA BANK", "FIRST NATIONAL BANK", "DELUXE CORP", "WELLS FARGO" };

			return companies[random.Next(0, companies.Length)];
		}

		public string DiscretionaryData { get; set; }

		public string IdentificationNumber { get; set; }
		public string NumberAddendaRecords { get; set; }
		public string ReceivingCompany { get; set; }
		public string Reserved { get; set; }
	}
}
