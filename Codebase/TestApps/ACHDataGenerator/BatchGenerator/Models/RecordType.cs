﻿
namespace BatchGenerator.Models
{
    public enum RecordType
    {
		FILE_HEADER = '1',
	    BATCH_HEADER = '5',
	    BATCH_DETAIL = '6',
	    BATCH_ADDENDA = '7',
	    BATCH_FOOTER ='8',
		FILE_FOOTER = '9'
	}
}
