﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACHDataGenerator;

namespace BatchGenerator.Models
{
	public class IATBatchDetail : DetailBase
	{
		public IATBatchDetail(ClientAccount clientAccount) : base(clientAccount)
		{
			StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
			StartAndLength.Add(new Tuple<string, int, int>("TransactionCode", 1, 2));
			StartAndLength.Add(new Tuple<string, int, int>("DFIIdentification", 3, 9));
            StartAndLength.Add(new Tuple<string, int, int>("NumberAddendaRecords", 12, 4));
            StartAndLength.Add(new Tuple<string, int, int>("Reserved", 16, 13));
			StartAndLength.Add(new Tuple<string, int, int>("Amount", 29, 10));
            StartAndLength.Add(new Tuple<string, int, int>("DFIAccountNumber", 39, 35));
            StartAndLength.Add(new Tuple<string, int, int>("Reserved2", 74, 2));
            StartAndLength.Add(new Tuple<string, int, int>("OFACIndicator1", 76, 1));
			StartAndLength.Add(new Tuple<string, int, int>("OFACIndicator2", 77, 1));
			StartAndLength.Add(new Tuple<string, int, int>("AddendaRecordIndicator", 78, 1));
			StartAndLength.Add(new Tuple<string, int, int>("TraceNumber", 79, 15));
			Addendas = new List<AddendaBase>();
			RecordTypeCode = ParsedRecordType(RecordType.BATCH_DETAIL);
			TransactionCode = GetTransactionCode();

			Amount = GetAmount();
			Reserved = " ".PadLeft(13, ' ');
			Reserved2 = "  ";
			OFACIndicator1 = "X";
			OFACIndicator2 = "Y";
			Blank = ClientAccount.DDA;
			AddendaRecordIndicator = Settings.Batches.First(n => n.ClassCode == "IAT").ClientAccounts.Sum(a => a.Addendas.Count) > 0 ? "1" : "0";
            NumberAddendaRecords = (AddendaRecordIndicator == "0") ? ("0000") : ("0008");
			TraceNumber = GetTraceNumber();
		}

        public string NumberAddendaRecords { get; set; }
        public string Blank { get; set; }
		public string Reserved { get; set; }
		public string Reserved2 { get; set; }
		public string OFACIndicator1 { get; set; }
		public string OFACIndicator2 { get; set; }
	}
}
