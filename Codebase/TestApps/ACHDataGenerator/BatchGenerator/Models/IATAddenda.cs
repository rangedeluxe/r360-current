﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BatchGenerator.Models
{
    public class IATAddenda : AddendaBase
    {
        public string AddendaTypeCode10 { get; set; } = "10";
        public string AddendaTypeCode11 { get; set; } = "11";
        public string AddendaTypeCode12 { get; set; } = "12";
        public string AddendaTypeCode13 { get; set; } = "13";
        public string AddendaTypeCode14 { get; set; } = "14";
        public string AddendaTypeCode15 { get; set; } = "15";
        public string AddendaTypeCode16 { get; set; } = "16";
        public string AddendaTypeCode17 { get; set; } = "17";
        public string TransactionTypeCode { get; set; } = string.Empty;
        public string Amount { get; set; } = string.Empty;
        public string ReceiverName { get; set; } = string.Empty;
        public string OriginatorName { get; set; } = string.Empty;
        public string OriginatorStreetAddress { get; set; } = string.Empty;
        public string OriginatorCityAndState { get; set; } = string.Empty;
        public string OriginatorCountryAndPostalCode { get; set; } = string.Empty;
        public string OriginatingDFIName { get; set; } = string.Empty;
        public string OriginatingDFIQualifier { get; set; } = string.Empty;
        public string OriginatingDFIIdentification { get; set; } = string.Empty;
        public string OriginatingDFIBranchCountryCode { get; set; } = string.Empty;
        public string ReceivingDFIName { get; set; } = string.Empty;
        public string ReceivingDFIQualifier { get; set; } = string.Empty;
        public string ReceivingDFIIdentification { get; set; } = string.Empty;
        public string ReceivingDFIBranchCountryCode { get; set; } = string.Empty;
        public string ReceiverIdentification { get; set; } = string.Empty;
        public string ReceiverStreetAddress { get; set; } = string.Empty;
        public string ReceiverCityAndState { get; set; } = string.Empty;
        public string ReceiverCountryAndPostalCode { get; set; } = string.Empty;
        public string AddendaInformation { get; set; } = string.Empty;
        public string AddendaSequenceNumber { get; set; } = string.Empty;

        public List<Tuple<string, int, int>> IATAddenda10 = new List<Tuple<string, int, int>>();
        public List<Tuple<string, int, int>> IATAddenda11 = new List<Tuple<string, int, int>>();
        public List<Tuple<string, int, int>> IATAddenda12 = new List<Tuple<string, int, int>>();
        public List<Tuple<string, int, int>> IATAddenda13 = new List<Tuple<string, int, int>>();
        public List<Tuple<string, int, int>> IATAddenda14 = new List<Tuple<string, int, int>>();
        public List<Tuple<string, int, int>> IATAddenda15 = new List<Tuple<string, int, int>>();
        public List<Tuple<string, int, int>> IATAddenda16 = new List<Tuple<string, int, int>>();
        public List<Tuple<string, int, int>> IATAddenda17 = new List<Tuple<string, int, int>>();

        public IATAddenda(DetailBase detail)
        {
            RecordTypeCode = ParsedRecordType(RecordType.BATCH_ADDENDA);
            AddendaTypeCode = GetAddendaType(detail);
            TransactionTypeCode = GetTransactionTypeCode();
            Amount = detail.Amount.PadLeft(18, '0');
            ReceiverName = GetReceivingCompany();
            OriginatorName = GetOriginatorName();
            OriginatorStreetAddress = GetOriginatorStreetAddress();
            OriginatorCityAndState = GetOriginatorCityAndState();
            OriginatorCountryAndPostalCode = GetOriginatorCountryAndPostalCode();
            OriginatingDFIName = "SWISS BANK AG";
            OriginatingDFIQualifier = "02";
            OriginatingDFIIdentification = "UBSWCHZH12A";
            OriginatingDFIBranchCountryCode = GetCountryCode();
            ReceivingDFIName = "FIRST NATIONAL BANK";
            ReceivingDFIQualifier = "01";
            ReceivingDFIIdentification = "104000016";
            ReceivingDFIBranchCountryCode = "US";
            ReceiverIdentification = "65049721";
            ReceiverStreetAddress = "14818 REDMAN AVE";
            ReceiverCityAndState = "OMAHA*NE";
            ReceiverCountryAndPostalCode = "US*68154";

            IATAddenda10.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
            IATAddenda10.Add(new Tuple<string, int, int>("AddendaTypeCode10", 1, 2));
            IATAddenda10.Add(new Tuple<string, int, int>("TransactionTypeCode", 3, 3));
            IATAddenda10.Add(new Tuple<string, int, int>("Amount", 6, 18));
            IATAddenda10.Add(new Tuple<string, int, int>("ReceiverName", 46, 35));
            IATAddenda10.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));

            IATAddenda11.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
            IATAddenda11.Add(new Tuple<string, int, int>("AddendaTypeCode11", 1, 2));
            IATAddenda11.Add(new Tuple<string, int, int>("OriginatorName", 3, 35));
            IATAddenda11.Add(new Tuple<string, int, int>("OriginatorStreetAddress", 38, 35));
            IATAddenda11.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));

            IATAddenda12.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
            IATAddenda12.Add(new Tuple<string, int, int>("AddendaTypeCode12", 1, 2));
            IATAddenda12.Add(new Tuple<string, int, int>("OriginatorCityAndState", 3, 35));
            IATAddenda12.Add(new Tuple<string, int, int>("OriginatorCountryAndPostalCode", 38, 35));
            IATAddenda12.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));

            IATAddenda13.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
            IATAddenda13.Add(new Tuple<string, int, int>("AddendaTypeCode13", 1, 2));
            IATAddenda13.Add(new Tuple<string, int, int>("OriginatingDFIName", 3, 35));
            IATAddenda13.Add(new Tuple<string, int, int>("OriginatingDFIQualifier", 38, 2));
            IATAddenda13.Add(new Tuple<string, int, int>("OriginatingDFIIdentification", 40, 34));
            IATAddenda13.Add(new Tuple<string, int, int>("OriginatingDFIBranchCountryCode", 74, 3));
            IATAddenda13.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));

            IATAddenda14.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
            IATAddenda14.Add(new Tuple<string, int, int>("AddendaTypeCode14", 1, 2));
            IATAddenda14.Add(new Tuple<string, int, int>("ReceivingDFIName", 3, 35));
            IATAddenda14.Add(new Tuple<string, int, int>("ReceivingDFIQualifier", 38, 2));
            IATAddenda14.Add(new Tuple<string, int, int>("ReceivingDFIIdentification", 40, 34));
            IATAddenda14.Add(new Tuple<string, int, int>("ReceivingDFIBranchCountryCode", 74, 3));
            IATAddenda14.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));

            IATAddenda15.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
            IATAddenda15.Add(new Tuple<string, int, int>("AddendaTypeCode15", 1, 2));
            IATAddenda15.Add(new Tuple<string, int, int>("ReceiverIdentification", 3, 15));
            IATAddenda15.Add(new Tuple<string, int, int>("ReceiverStreetAddress", 18, 35));
            IATAddenda15.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));

            IATAddenda16.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
            IATAddenda16.Add(new Tuple<string, int, int>("AddendaTypeCode16", 1, 2));
            IATAddenda16.Add(new Tuple<string, int, int>("ReceiverCityAndState", 3, 35));
            IATAddenda16.Add(new Tuple<string, int, int>("ReceiverCountryAndPostalCode", 38, 35));
            IATAddenda16.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));

            IATAddenda17.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
            IATAddenda17.Add(new Tuple<string, int, int>("AddendaTypeCode17", 1, 2));
            IATAddenda17.Add(new Tuple<string, int, int>("AddendaInformation", 3, 80));
            IATAddenda17.Add(new Tuple<string, int, int>("AddendaSequenceNumber", 83, 4));
            IATAddenda17.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));
        }

        public int WriteAllIATAddendas(StreamWriter writer)
        {
            int blockCount = 0;

            StartAndLength = IATAddenda10;
            WriteRecord(writer);
            blockCount++;

            StartAndLength = IATAddenda11;
            WriteRecord(writer);
            blockCount++;

            StartAndLength = IATAddenda12;
            WriteRecord(writer);
            blockCount++;

            StartAndLength = IATAddenda13;
            WriteRecord(writer);
            blockCount++;

            StartAndLength = IATAddenda14;
            WriteRecord(writer);
            blockCount++;

            StartAndLength = IATAddenda15;
            WriteRecord(writer);
            blockCount++;

            StartAndLength = IATAddenda16;
            WriteRecord(writer);
            blockCount++;

            if (PaymentInfo.Length > 0)
            {
                StartAndLength = IATAddenda17;

                int substrlen = (PaymentInfo.Length > 80) ? (80) : (PaymentInfo.Length);
                AddendaInformation = PaymentInfo.Substring(0, substrlen);
                AddendaSequenceNumber = "0001";
                WriteRecord(writer);
                blockCount++;

                if (PaymentInfo.Length > 80)
                {
                    substrlen = (PaymentInfo.Length > 160) ? (80) : (PaymentInfo.Length - 80);
                    AddendaInformation = PaymentInfo.Substring(80, substrlen);
                    AddendaSequenceNumber = "0002";
                    WriteRecord(writer);
                    blockCount++;
                }
            }

            return blockCount;
        }

        public static int WriteAddenda(StreamWriter writer, DetailBase detail, int detailSequence, string RemittanceAddendaData)
        {
            IATAddenda addenda = new IATAddenda(detail);

            addenda.DetailSequenceNumber = detailSequence.ToString().PadLeft(7, '0');
            addenda.PaymentInfo = RemittanceAddendaData;
            detail.Addendas.Clear();
            detail.Addendas.Add(addenda);
            return addenda.WriteAllIATAddendas(writer);
        }

        protected string GetTransactionTypeCode()
        {
            // NOTE: There may be other codes that would be valid here...
            string[] validCodes = { "PPD", "WEB", "TEL", "CCD", "ARC" };
            return validCodes[random.Next(0, validCodes.Length)];
        }

        protected string GetReceivingCompany()
        {
            string[] companies = { "FREDS BANK LLC", "DAKOTA BANK", "FIRST NATIONAL BANK", "DELUXE CORP", "WELLS FARGO" };

            return companies[random.Next(0, companies.Length)];
        }

        protected string GetOriginatorName()
        {
            string[] originators = { "BOEING", "GOOGLE", "ESTWING", "APPLE", "JASONS TRUST" };
            return originators[random.Next(0, originators.Length)];
        }

        protected string GetOriginatorStreetAddress()
        {
            string[] streetAddresses = { "1414 NORTH 205TH STREET", "3680 VICTORIA STREET", "1600 PENNSYLVANIA AVE" };
            return streetAddresses[random.Next(0, streetAddresses.Length)];
        }

        protected string GetOriginatorCityAndState()
        {
            string[] cityAndState = { "OMAHA*NE", "SHOREVIEW*MN", "WASHINGTON*DC" };
            return cityAndState[random.Next(0, cityAndState.Length)];
        }

        protected string GetOriginatorCountryAndPostalCode()
        {
            string[] postalCode = { "US*68022", "US*55126", "US*99999" };
            return postalCode[random.Next(0, postalCode.Length)];
        }
    }
}
