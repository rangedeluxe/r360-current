﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BatchGenerator.Models
{
    public class AddendaBase : ModelBase
    {
        public string AddendaTypeCode { get; set; } = string.Empty;
        public string PaymentInfo { get; set; } = string.Empty;
        public string DetailSequenceNumber { get; set; } = string.Empty;
        public string AddendaSequenceNumber { get; set; } = string.Empty;

        protected string GetAddendaType(DetailBase detail)
        {
            switch (detail.ClassCode)
            {
                case "CCD":
                case "CTX":
                case "PPD":
                    return "05";
                case "IAT":
                    return "17";
                case "CIE": // TODO: What is this 
                default: return "05";
            }
        }
    }
}
