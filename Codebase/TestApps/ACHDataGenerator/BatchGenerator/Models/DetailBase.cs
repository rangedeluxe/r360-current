﻿using System.Collections.Generic;
using System.Linq;
using ACHDataGenerator;

namespace BatchGenerator.Models
{
    public class DetailBase : ModelBase
    {
	    public DetailBase(ClientAccount clientAccount)
	    {
		    ClientAccount = clientAccount;
	    }

        public ClientAccount ClientAccount { get; set; }
	    public List<AddendaBase> Addendas { get; set; }
	    public string TransactionCode { get; set; }
	    public string Amount { get; set; }
	    public string DFIIdentification => ClientAccount.ABA;
	    public string DFIAccountNumber => ClientAccount.DDA.PadRight(17, ' ');
	    public string AddendaRecordIndicator { get; set; }
	    public string TraceNumber { get; set; }
        public string ClassCode { get { return GetClassCode(); } }

        protected string GetAmount()
	    {
		    return random.Next(1, 999999).ToString().PadLeft(10, '0');
	    }

	    protected string GetTransactionCode()
	    {
		    int[] validCodes = { 22, 27, 32 };

		    return validCodes[random.Next(0, validCodes.Length)].ToString();
	    }
		
	    protected int traceNumber = 0;
	    protected string GetTraceNumber()
	    {
		    return (++traceNumber).ToString().PadLeft(15, '0');
	    }

        protected string GetClassCode()
        {
            string[] validClassCodes = { "CTX", "PPD", "CIE", "IAT", "CCD" };
            var classCode = this.GetType().ToString().Split('.')[2].Substring(0, 3).ToUpper();
            if (validClassCodes.Contains(classCode))
                return classCode;
            return "UNK";
        }

	}
}
