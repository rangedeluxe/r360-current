﻿using System;

namespace BatchGenerator.Models
{
    public class FileHeader : ModelBase
    {
		public string PriorityCode { get; set; }
		public string ImmediateDestination { get; set; }
		public string ImmediateOrigin { get; set; }
		public string FileCreationDate { get; set; }
		public string FileCreationTime { get; set; }
		public string FileIDModifier { get; set; }
		public string RecordSize { get; set; }
		public string BlockingFactor { get; set; }
		public string FormatCode { get; set; }
		public string ImmediateDestinationName { get; set; }
		public string ImmediateOriginName { get; set; }
		public string ReferenceCode { get; set; }

	    private static char fileModifier = 'A';

	    public FileHeader()
	    {
		    RecordTypeCode = ParsedRecordType(RecordType.FILE_HEADER);
		    PriorityCode = "01";
		    ImmediateDestination = " 072000096";
			ImmediateOrigin = " 072000096";
		    FileCreationDate = DateTime.Now.ToString("yyMMdd");
		    FileCreationTime = DateTime.Now.ToString("HHmm");
		    FileIDModifier = fileModifier.ToString();
		    RecordSize = "094";
		    BlockingFactor = "10";
		    FormatCode = "1";
		    ImmediateDestinationName = "MI POST-C RED MICHIGAN";
		    ImmediateOriginName = "Deluxe Corp";
		    ReferenceCode = random.Next(0, 5000).ToString().PadLeft(8, '0');

		    fileModifier++;

			StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
		    StartAndLength.Add(new Tuple<string, int, int>("PriorityCode", 1, 2));
		    StartAndLength.Add(new Tuple<string, int, int>("ImmediateDestination", 3, 10));
		    StartAndLength.Add(new Tuple<string, int, int>("ImmediateOrigin", 13, 10));
		    StartAndLength.Add(new Tuple<string, int, int>("FileCreationDate", 23, 6));
		    StartAndLength.Add(new Tuple<string, int, int>("FileCreationTime", 29, 4));
		    StartAndLength.Add(new Tuple<string, int, int>("FileIDModifier", 33, 1));
		    StartAndLength.Add(new Tuple<string, int, int>("RecordSize", 34, 3));
		    StartAndLength.Add(new Tuple<string, int, int>("BlockingFactor", 37, 2));
		    StartAndLength.Add(new Tuple<string, int, int>("FormatCode", 39, 1));
		    StartAndLength.Add(new Tuple<string, int, int>("ImmediateDestinationName", 40, 23));
		    StartAndLength.Add(new Tuple<string, int, int>("ImmediateOriginName", 63, 23));
		    StartAndLength.Add(new Tuple<string, int, int>("ReferenceCode", 86, 8));
	    }
	}
}
