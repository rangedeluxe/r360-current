﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BatchGenerator.Models
{
    public class BasicAddenda : AddendaBase
    {
        public BasicAddenda(DetailBase detail, int sequence)
        {
            StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
            StartAndLength.Add(new Tuple<string, int, int>("AddendaTypeCode", 1, 2));
            StartAndLength.Add(new Tuple<string, int, int>("PaymentInfo", 3, 80));
            StartAndLength.Add(new Tuple<string, int, int>("AddendaSequenceNumber", 83, 4));
            StartAndLength.Add(new Tuple<string, int, int>("DetailSequenceNumber", 87, 7));
            RecordTypeCode = ParsedRecordType(RecordType.BATCH_ADDENDA);
            AddendaTypeCode = GetAddendaType(detail);
            AddendaSequenceNumber = sequence.ToString().PadLeft(4, '0');
        }

        public static int WriteAddenda(StreamWriter writer, DetailBase detail, int detailSequence, string paymentInfo)
        {
            if (paymentInfo.Trim().Length == 0)
                return 0;

            BasicAddenda addenda = new BasicAddenda(detail,1);
            addenda.DetailSequenceNumber = detailSequence.ToString().PadLeft(7, '0');
            int substrlen = (paymentInfo.Length > 80) ? (80) : (paymentInfo.Length);
            addenda.PaymentInfo = paymentInfo.Substring(0, substrlen);
            addenda.WriteRecord(writer);
            detail.Addendas.Clear();
            detail.Addendas.Add(addenda);

            return 1;
        }

    }
}
