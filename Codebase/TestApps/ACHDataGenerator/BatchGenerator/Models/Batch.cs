﻿using System.Collections.Generic;

namespace BatchGenerator.Models
{
    public class Batch
	{
	    public Batch()
	    {
			BatchDetails = new List<DetailBase>();
	    }
	    public BatchHeader BatchHeader {get;set;}
		
		public List<DetailBase> BatchDetails { get; set; }
		public BatchFooter BatchFooter { get; set; }
	}
}
