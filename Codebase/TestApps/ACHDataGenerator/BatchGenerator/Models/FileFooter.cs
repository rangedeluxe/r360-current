﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BatchGenerator.Models
{
    public class FileFooter : ModelBase
	{
		public string BatchCount { get; set; }
		public string BlockCount { get; set; }
		public string EntryAddendaCount { get; set; }
		public string EntryHash { get; set; }
		public string TotalDebitAmount { get; set; }
		public string TotalCreditAmount { get; set; }
		public string Reserved { get; set; }
	    public FileFooter(List<Batch> batches, int blockCount)
	    {
			RecordTypeCode = ParsedRecordType(RecordType.FILE_FOOTER);
			BatchCount = batches.Count.ToString().PadLeft(6, '0');
		    BlockCount = blockCount.ToString().PadLeft(6, '0');
		    int count = batches.Sum(x => x.BatchDetails.Count);
		    foreach (var b in batches)
		    {
                int addendaCountMultiplier = (b.BatchHeader.EntryClassCode == "IAT") ? (8) : (1);
			    count += b.BatchDetails.Sum(x => (x.Addendas.Count * addendaCountMultiplier));
		    }

		    EntryAddendaCount = count.ToString().PadLeft(8, '0');
		    EntryHash = ComputeHash(batches).PadLeft(10, '0');
		    TotalDebitAmount = batches.Sum(s => Convert.ToDecimal(s.BatchFooter.TotalDebitAmount)).ToString()
			    .PadLeft(12, '0');
		    TotalCreditAmount = batches.Sum(s => Convert.ToDecimal(s.BatchFooter.TotalCreditAmount)).ToString()
			    .PadLeft(12, '0');
		    Reserved = "";
			StartAndLength.Add(new Tuple<string, int, int>("RecordTypeCode", 0, 1));
			StartAndLength.Add(new Tuple<string, int, int>("BatchCount", 1, 6));
		    StartAndLength.Add(new Tuple<string, int, int>("BlockCount", 7, 13));
		    StartAndLength.Add(new Tuple<string, int, int>("EntryAddendaCount", 13, 8));
		    StartAndLength.Add(new Tuple<string, int, int>("EntryHash", 21, 10));
		    StartAndLength.Add(new Tuple<string, int, int>("TotalDebitAmount", 31, 12));
		    StartAndLength.Add(new Tuple<string, int, int>("TotalCreditAmount", 43, 12));
		    StartAndLength.Add(new Tuple<string, int, int>("Reserved", 55, 39));
	    }

		private string ComputeHash(List<Batch> batches)
		{
			Int64 total = 0;
			batches.ForEach(x => total += Convert.ToInt64(x.BatchFooter.EntryHash));

			var sTotalHash = total.ToString();
			if (sTotalHash.Length > 10)
			{
				sTotalHash = sTotalHash.Remove(0, sTotalHash.Length - 10);
			}

			return sTotalHash;
		}
	}
}
