﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

namespace BatchGenerator.Utilities
{
	public static class SegmentParser
	{

		private static string GetNumber(Random random, int length)
		{
			length = random.Next(1, length + 1); // Random.Next is (1 - N-1) first param inclusive second exclusive
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < length; ++i)
			{
				sb.Append(random.Next(0, 10).ToString());
			}

			var result = sb.ToString().TrimStart('0');
			if (result.Length == 0)
			{
				return random.Next(1, 10).ToString(); // don't want a blank string.
			}

			return result;
		}
		public static string GetAmount(Random random, int length)
		{
			var s = GetNumber(random, length);
			if (s.Length <= 2)
			{
				switch (s.Length)
				{
					case 1:
						return $"0.0{s}";
					case 2:
						return $"0.{s}";
				}
			}
			return (s.Substring(0, s.Length - 2) + "." + s.Substring(s.Length - 2, 2));
		}

		private static string GetString(Random random, int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return new string(Enumerable.Repeat(chars, length)
				.Select(s => s[random.Next(s.Length)]).ToArray());
		}

		private static string RandomDate(Random random)
		{
			int year = random.Next(1999, 2019);
			int month = random.Next(1, 13);
			int days = 0;
			switch (month)
			{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					days = 32;
					break;
				case 2:
					days = 29;
					break;
				case 4:
				case 9:
				case 11:
					days = 31;
					break;
				default:
					days = 1;
					break;
			}

			int day = random.Next(1, days);
			return $"{year.ToString("D4")}{month.ToString("D2")}{day.ToString("D2")}";
		}
		public delegate string TemplateFunction(Random rand, int length);

		private static string ParseTemplateField(string segment, string template, TemplateFunction f)
		{
			var parts = template.Split('~');
			Random rand = new Random();
			int len = 0;
			if (parts.GetLength(0) < 2)
			{
				throw new Exception($"Invalid {template} COOKIE.");
			}

			try
			{
				len = Convert.ToInt32(parts[1].Substring(0, parts[1].Length - 1));
			}
			catch
			{
				throw new Exception($"Invalid {template} COOKIE.");
			}

			var regex = new Regex(Regex.Escape(template));
			segment = regex.Replace(segment, f(rand, len), 1);
			return segment;
		}

		public static string ParseSegment(string segment)
		{
			int idx = 0;
			int len = 0;
			var templates = new List<string>();
			string s = segment;
			var rand = new Random();
			if (segment.Count(x => x == '<') != segment.Count(x => x == '>'))
			{
				throw new Exception($"Invalid segment, mismatched <> at segment {segment}.");
			}

			while ((idx = s.IndexOf('<')) >= 0)
			{
				len = s.IndexOf('>') - idx + 1;
			

				templates.Add(s.Substring(idx, len));
				s = s.Substring(idx + len);
			}

			return ParseCookies(segment, templates);
		}

		private static string _static = "<@STATIC";
		public static bool ParseStaticSegment(string segment, char delimiter, ref string parsedSegment)
		{
			if (segment.Contains(_static))
			{
				if (!segment.StartsWith(_static))
				{
					throw new Exception("@STATIC must be the first and only segment in addenda.");
				}
				parsedSegment = segment.Replace($"{_static}~", "").Replace(">", "").Replace('\\', delimiter).PadRight(94, ' ');
				return true;
			}
			return false;
		}

		public const string CurrentDate = "@CURRENTDATE";
		public const string CurrentDateShort = "@CURRENTDATESHORT";
		public static string ParseCookies(string segment, List<string> cookies)
		{
			var rand = new Random();
			cookies.ForEach(x =>
			{
				if (x.Contains("@MONEY"))
				{
					segment = ParseTemplateField(segment, x, GetAmount);
				}
				else if (x.Contains("@DATE"))
				{
					var regex = new Regex(Regex.Escape(x));
					segment = regex.Replace(segment, RandomDate(rand), 1);
				}
				else if (x.Contains("@NUMBER"))
				{
					segment = ParseTemplateField(segment, x, GetNumber);
				}
				else if (x.Contains("@STRING"))// String
				{
					segment = ParseTemplateField(segment, x, GetString);
				}
				else if (x.Contains(CurrentDateShort)) // Today's date minus factor in short format
				{
					segment = CalculateDate(segment, x, true);
				}
				else if (x.Contains(CurrentDate)) // Today's date minus factor 
				{
					segment = CalculateDate(segment, x);
				}
				else
				{
					throw new Exception($"Unknown segment type of {x}");
				}
			});
			return segment;
		}

		private static string CalculateDate(string segment, string template, bool shortDate = false)
		{
			var regex = new Regex(Regex.Escape(template));
			var parts = template.Split('~');
			if (parts.GetLength(0) == 1)
			{
				segment = regex.Replace(segment, shortDate ? DateTime.Now.ToString("MMddyyyy") : DateTime.Now.ToString("MM/dd/yyyy"), 1);
				return segment;
			}
			if (parts.GetLength(0) != 2)
			{
				throw new Exception("Invalid CURRENTDATE format.  Date must be in the format <@CURRENTDATE> or <@CURRENTDATE~N>, where N days will be subtracted from the current date for the date value.");
			}

			var days = Convert.ToInt32(parts[1].Substring(0, parts[1].Length - 1));
			segment = regex.Replace(segment, shortDate ? DateTime.Now.AddDays(-days).ToString("MMddyyyy") : DateTime.Now.AddDays(-days).ToString("MM/dd/yyyy"), 1);
			return segment;
		}
	}
}
