﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BatchGenerator.Models;
using BatchGenerator.Utilities;
using Microsoft.Extensions.Configuration;
using NLog;

namespace ACHDataGenerator
{
	public class BatchGenerator
	{
		private readonly ILogger _logger;
		private readonly IConfiguration _configuration = null;
		readonly Random rand = null;

		public BatchGenerator(IConfiguration configuration, ILogger logger)
		{
			_configuration = configuration;
			_logger = logger;
			rand = new Random();
		}

		public BatchGenerator(ILogger logger)
		{
			_logger = logger;
			rand = new Random();
		}

		public Settings GetSettings(IConfiguration configuration)
		{
			Settings settings;

			if (configuration != null)
			{
				settings = new Settings
				{
					OutputFolder = configuration["OutputFolder"],
					DepositDate = configuration["DepositDate"],
					BatchFileCount = configuration["BatchFileCount"],
					Batches = configuration.GetSection("Batches").Get<List<BatchSettings>>()
				};
			}
			else
			{
				settings = new Settings()
				{
					OutputFolder = "##INVALID_PATH##",
					BatchFileCount = "",
					Batches = new List<BatchSettings>(),
					DepositDate = "<@CURRENTDATE>"
				};
			}

			if (!settings.OutputFolder.EndsWith('\\'))
			{
				settings.OutputFolder += '\\';
			}
			ModelBase.Settings = settings;
			// Verify that the file is valid, IE. no repeating class codes.
			var classCodes = new[] { "CTX", "PPD", "CIE", "IAT", "CCD" };
			foreach (var code in classCodes)
			{
				if (settings.Batches.Count(n => n.ClassCode == code) > 1)
				{
					throw new ArgumentException($"Invalid JSON file. Class code {code} is repeated");
				}
			}

			// Here we take 1 - BatchCount per class code so each file is different.
			settings.Batches.Where(b => b.FixedCount == false).ToList().ForEach(x => x.BatchCount = rand.Next(1, x.BatchCount + 1));
			if (settings.DepositDate.Contains(SegmentParser.CurrentDate))
			{
				settings.DepositDate = SegmentParser.ParseCookies(settings.DepositDate, new List<string> { settings.DepositDate });
			}

			return settings;
		}

		private int _blockCount = 0;
		private BatchSettings _batchSettings = null;
		private List<Batch> _batches;

		private char GetAddendaSegmentDelimiter()
		{
			char segmentDelimiter = '\\';
			if (_batchSettings.SegmentDelimiter != null && _batchSettings.SegmentDelimiter.Length > 0)
			{
				if (_batchSettings.SegmentDelimiter.Length == 1)
				{
					return _batchSettings.SegmentDelimiter[0];
				}
				return _batchSettings.SegmentDelimiter[rand.Next(0, _batchSettings.SegmentDelimiter.Length)];
			}

			return '\\';
		}

		public bool BuildBatchFile(StreamWriter writer, Settings settings)
		{

			_blockCount = 0;

			_batches = new List<Batch>(); // Need to save all the pieces so we can get counts for file footer
			var batchCount = settings.Batches.Sum(x => x.BatchCount);
			Batch batch = null;
			int batchDetailCount = 0;
			FileHeader header = CreateFileHeader(writer);
			_logger.Info($"Creating {batchCount} batches.");
			for (int bc = 0; bc < batchCount; bc++)
			{
				batch = new Batch();
				try
				{
					batch.BatchHeader = CreateBatchHeader(writer, settings, out batchDetailCount);
					var delimiter = GetAddendaSegmentDelimiter();
					_logger.Info($"Creating {batchDetailCount} batch detail records of record type {batch.BatchHeader.EntryClassCode}.");
					for (int i = 0; i < batchDetailCount; ++i)
					{
						batch.BatchDetails.Add(CreateBatchDetail(writer, i, delimiter));
					}
					batch.BatchFooter = CreateBatchFooter(writer, batch, settings);
				}
				catch (Exception e)
				{
					_logger.Log(LogLevel.Error, e.Message);
					return false;
				}
				_batches.Add(batch);
			}
			CreateFileFooter(writer, _batches);
			_logger.Info("Batch complete.");
			return true;
		}

		public bool BuildBatchFile()
		{
			var settings = GetSettings(_configuration);

			var fileName = string.Empty;
			using (var writer =
				new StreamWriter(fileName = $"{settings.OutputFolder}ACH_{DateTime.Now.ToString("MMddyyyy_HHmmssfff")}.dat"))
			{
				_logger.Info($"Creating file {fileName}");

				if (BuildBatchFile(writer, settings))
				{
					if (_batchSettings.GenerateDupFile)
					{
						BuildDupTranFile();
					}
					return true;
				}
				return false;
			}
		}

		public void BuildDupTranFile(StreamWriter writerOut, Settings settings)
		{
			var oldbd = _batches[0].BatchDetails[0];
			_blockCount = 0;
			using (var writer = new StreamWriter(new MemoryStream()))
			{
				// Store the first batch detail record so we can add it back
				// Make all new file in memory.  This will populate our _batches with new records, which
				// we then add the dup record from above to and recompute.
				BuildBatchFile(writer, settings);
			}

			_batches[0].BatchDetails.Add(oldbd);
			_batches[0].BatchFooter.ComputeEntryHash(_batches[0]);
			CreateFileHeader(writerOut);
			int sequence = 0;
			foreach (var b in _batches)
			{
				sequence = 0;
				b.BatchHeader.WriteRecord(writerOut);
				var delimiter = GetAddendaSegmentDelimiter();
				foreach (var bd in b.BatchDetails)
				{
					bd.WriteRecord(writerOut);
					_blockCount += CTXAddenda.WriteAddenda(writerOut, bd, sequence + 1, delimiter);

				}
				b.BatchFooter.WriteRecord(writerOut);
			}
			FileFooter f = new FileFooter(_batches, _blockCount);
			f.WriteRecord(writerOut);
		}

		private void BuildDupTranFile()
		{
			var settings = GetSettings(_configuration);
			string fileName = $"{settings.OutputFolder}ACH_{DateTime.Now.ToString("MMddyyyy_HHmmssfffDupTran")}.dat";
			_logger.Info($"Creating batch file with duplicate record: {fileName}");
			using (var writer = new StreamWriter(fileName))
			{
				BuildDupTranFile(writer, settings);
			}

		}


		private void CreateFileFooter(StreamWriter writer, List<Batch> batches)
		{
			_blockCount++;

			_logger.Info("Creating file footer (File Control Record).");
			FileFooter f = new FileFooter(batches, _blockCount);
			f.WriteRecord(writer);
		}

		private BatchFooter CreateBatchFooter(StreamWriter writer, Batch batch, Settings settings)
		{
			_blockCount++;
			_logger.Info("Creating batch footer.");
			BatchFooter f = new BatchFooter(batch);
			f.WriteRecord(writer);
			// In the JSON file we specify the number of batches to produce by type.  Each time we process
			// a batch of that type we decrement the total count for that type by one.  Once this types
			// count reaches zero, we delete it so we can focus in the CreateHeader call on the types that have not
			// reached there max.
			if (_batchSettings.BatchCount == 0)
			{
				settings.Batches.Remove(_batchSettings);
			}

			return f;
		}

		private DetailBase CreateBatchDetail(StreamWriter writer, int sequence, char segmentDelimiter)
		{
			_blockCount++;
			DetailBase d = null;
			try
			{
				Type type = Type.GetType($"BatchGenerator.Models.{_batchSettings.ClassCode.ToUpper()}BatchDetail");
				d = Activator.CreateInstance(type, _batchSettings.ClientAccounts[rand.Next(0, _batchSettings.ClientAccounts.Count)]) as DetailBase;
			}
			catch (Exception e)
			{
				_logger.Error($"Invalid Class Code {_batchSettings.ClassCode.Substring(0, 3)}.  Aborting");
				throw;
			}

			d.WriteRecord(writer);

            if (d.ClientAccount.Addendas.Count > 0)
            {
                switch (d.ClassCode)
                {
                    case "CTX":
                        _blockCount += CTXAddenda.WriteAddenda(writer, d, sequence + 1, segmentDelimiter);
                        break;
                    case "IAT":
                        _blockCount += IATAddenda.WriteAddenda(writer, d, sequence + 1, d.ClientAccount.Addendas[0].Segment);
                        break;
                    case "PPD":
                    case "CCD":
                    case "CIE":
                        _blockCount += BasicAddenda.WriteAddenda(writer, d, sequence + 1, d.ClientAccount.Addendas[0].Segment);
                        break;
                }
            }
			return d;
		}

		private BatchHeader CreateBatchHeader(StreamWriter writer, Settings settings, out int batchDetailCount)
		{
			_blockCount++;
			// Here we randomly pick the class code from all the codes defined in the file that have not met there file limit
			Random r = new Random();
			int idx = r.Next(0, settings.Batches.Count);
			_batchSettings = settings.Batches[idx];
			batchDetailCount = _batchSettings.BatchDetailCount;

			// Now that we created a batch of this class code, decrement the count so we do not make to many.  We will delete this
			// class code in the CreateFooter once the count reaches zero.
			_batchSettings.BatchCount--;

			_logger.Info($"Creating batch header record for class code {_batchSettings.ClassCode}");
			BatchHeader h = new BatchHeader(_batchSettings.ClassCode, settings);
			h.WriteRecord(writer);
			return h;
		}

		private FileHeader CreateFileHeader(StreamWriter writer)
		{
			_blockCount++;
			FileHeader h = new FileHeader();
			h.WriteRecord(writer);
			return h;
		}
	}
}
