﻿using System;
using System.Collections.Generic;

namespace ACHDataGenerator
{
	[Serializable]
	public class BatchSettings
	{
		public BatchSettings()
		{
			ClientAccounts = new List<ClientAccount>();
		}
		public string ClassCode { get; set; }
		public string SegmentDelimiter { get; set; }
		public int BatchCount { get; set; }
		public int BatchDetailCount { get; set; }
		public List<ClientAccount> ClientAccounts { get; set; }
		public bool GenerateDupFile { get; set; }

		public bool FixedCount { get; set; }
	}

	[Serializable]
	public class ClientAccount
	{
		public ClientAccount()
		{
			Addendas = new List<Addenda>();
		}

		public string ABA { get; set; }
		public string DDA { get; set; }
		public List<Addenda> Addendas { get; set; }
	}

	[Serializable]
	public class Addenda
	{
		public int Count { get; set; }
		public string Segment { get; set; }
	}

	[Serializable]
	public class Settings
	{
		public string OutputFolder { get; set; }
		public string BatchFileCount { get; set; }

		public string DepositDate { get; set; }

		public List<BatchSettings> Batches { get; set; }
	}
}
