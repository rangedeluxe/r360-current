﻿namespace VolumeCreateDATFiles
{
    partial class CreateDATFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumDatFiles = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NumChecks = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NumTransactions = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NumDocs = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.NumBatches = new System.Windows.Forms.TextBox();
            this.Create = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.directoryDATFiles = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ClientName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.DocumentDocTypeName = new System.Windows.Forms.TextBox();
            this.CheckDocTypeNameLabel = new System.Windows.Forms.Label();
            this.CheckDocTypeName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.LockboxID = new System.Windows.Forms.TextBox();
            this.ImageUse = new System.Windows.Forms.Label();
            this.ImageToUse = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // NumDatFiles
            // 
            this.NumDatFiles.Location = new System.Drawing.Point(250, 61);
            this.NumDatFiles.Name = "NumDatFiles";
            this.NumDatFiles.Size = new System.Drawing.Size(100, 20);
            this.NumDatFiles.TabIndex = 0;
            this.NumDatFiles.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of DAT Files to Create";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Max Number of Checks per Trans";
            // 
            // NumChecks
            // 
            this.NumChecks.Location = new System.Drawing.Point(187, 79);
            this.NumChecks.Name = "NumChecks";
            this.NumChecks.Size = new System.Drawing.Size(100, 20);
            this.NumChecks.TabIndex = 2;
            this.NumChecks.Text = "4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Number of Transactions";
            // 
            // NumTransactions
            // 
            this.NumTransactions.Location = new System.Drawing.Point(187, 35);
            this.NumTransactions.Name = "NumTransactions";
            this.NumTransactions.Size = new System.Drawing.Size(100, 20);
            this.NumTransactions.TabIndex = 6;
            this.NumTransactions.Text = "5";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.NumDocs);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.NumTransactions);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.NumChecks);
            this.groupBox1.Location = new System.Drawing.Point(64, 270);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 221);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Batch info";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(155, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Max Number of Docs per Trans";
            // 
            // NumDocs
            // 
            this.NumDocs.Location = new System.Drawing.Point(187, 119);
            this.NumDocs.Name = "NumDocs";
            this.NumDocs.Size = new System.Drawing.Size(100, 20);
            this.NumDocs.TabIndex = 8;
            this.NumDocs.Text = "3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Number of Batches PerDAT";
            // 
            // NumBatches
            // 
            this.NumBatches.Location = new System.Drawing.Point(251, 97);
            this.NumBatches.Name = "NumBatches";
            this.NumBatches.Size = new System.Drawing.Size(100, 20);
            this.NumBatches.TabIndex = 9;
            this.NumBatches.Text = "5";
            // 
            // Create
            // 
            this.Create.Location = new System.Drawing.Point(177, 594);
            this.Create.Name = "Create";
            this.Create.Size = new System.Drawing.Size(75, 23);
            this.Create.TabIndex = 12;
            this.Create.Text = "Create";
            this.Create.UseVisualStyleBackColor = true;
            this.Create.Click += new System.EventHandler(this.Create_Click);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(289, 594);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 13;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Directory to place DAT Files";
            // 
            // directoryDATFiles
            // 
            this.directoryDATFiles.Location = new System.Drawing.Point(197, 20);
            this.directoryDATFiles.Name = "directoryDATFiles";
            this.directoryDATFiles.Size = new System.Drawing.Size(185, 20);
            this.directoryDATFiles.TabIndex = 14;
            this.directoryDATFiles.Text = "\\\\wschwarzxpvm\\ImageRPSTest";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.ClientName);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.DocumentDocTypeName);
            this.groupBox2.Controls.Add(this.CheckDocTypeNameLabel);
            this.groupBox2.Controls.Add(this.CheckDocTypeName);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.LockboxID);
            this.groupBox2.Location = new System.Drawing.Point(64, 123);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(384, 141);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "LocboxInfo";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Client Name";
            // 
            // ClientName
            // 
            this.ClientName.Location = new System.Drawing.Point(187, 47);
            this.ClientName.Name = "ClientName";
            this.ClientName.Size = new System.Drawing.Size(100, 20);
            this.ClientName.TabIndex = 20;
            this.ClientName.Text = "Wayne";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "DocumentDocTypeName";
            // 
            // DocumentDocTypeName
            // 
            this.DocumentDocTypeName.Location = new System.Drawing.Point(187, 109);
            this.DocumentDocTypeName.Name = "DocumentDocTypeName";
            this.DocumentDocTypeName.Size = new System.Drawing.Size(100, 20);
            this.DocumentDocTypeName.TabIndex = 18;
            // 
            // CheckDocTypeNameLabel
            // 
            this.CheckDocTypeNameLabel.AutoSize = true;
            this.CheckDocTypeNameLabel.Location = new System.Drawing.Point(7, 88);
            this.CheckDocTypeNameLabel.Name = "CheckDocTypeNameLabel";
            this.CheckDocTypeNameLabel.Size = new System.Drawing.Size(110, 13);
            this.CheckDocTypeNameLabel.TabIndex = 17;
            this.CheckDocTypeNameLabel.Text = "CheckDocTypeName";
            // 
            // CheckDocTypeName
            // 
            this.CheckDocTypeName.Location = new System.Drawing.Point(186, 81);
            this.CheckDocTypeName.Name = "CheckDocTypeName";
            this.CheckDocTypeName.Size = new System.Drawing.Size(100, 20);
            this.CheckDocTypeName.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "LockboxID";
            // 
            // LockboxID
            // 
            this.LockboxID.Location = new System.Drawing.Point(187, 20);
            this.LockboxID.Name = "LockboxID";
            this.LockboxID.Size = new System.Drawing.Size(100, 20);
            this.LockboxID.TabIndex = 14;
            this.LockboxID.Text = "22";
            // 
            // ImageUse
            // 
            this.ImageUse.AutoSize = true;
            this.ImageUse.Location = new System.Drawing.Point(73, 504);
            this.ImageUse.Name = "ImageUse";
            this.ImageUse.Size = new System.Drawing.Size(74, 13);
            this.ImageUse.TabIndex = 18;
            this.ImageUse.Text = "Image To Use";
            // 
            // ImageToUse
            // 
            this.ImageToUse.Location = new System.Drawing.Point(252, 504);
            this.ImageToUse.Name = "ImageToUse";
            this.ImageToUse.Size = new System.Drawing.Size(100, 20);
            this.ImageToUse.TabIndex = 17;
            // 
            // CreateDATFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 643);
            this.Controls.Add(this.ImageUse);
            this.Controls.Add(this.ImageToUse);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.directoryDATFiles);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Create);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.NumBatches);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NumDatFiles);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateDATFiles";
            this.Text = "CreateDATFiles";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NumDatFiles;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NumChecks;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox NumTransactions;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox NumDocs;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox NumBatches;
        private System.Windows.Forms.Button Create;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox directoryDATFiles;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox DocumentDocTypeName;
        private System.Windows.Forms.Label CheckDocTypeNameLabel;
        private System.Windows.Forms.TextBox CheckDocTypeName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox LockboxID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox ClientName;
        private System.Windows.Forms.Label ImageUse;
        private System.Windows.Forms.TextBox ImageToUse;
    }
}

