﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VolumeCreateDATFiles
{
    public class cMagicKeywords
    {
        public const string TAGGED_DIP = ">>>>Self Configuring Tagged DIP <<<<";
        public const string BEGIN_TEXT = "BEGIN:";
        public const string End = "END";
        public const string FullPath = "FullPath";

        public const string AADocTypeName = ">>DocTypeName";
        public const string AAFullPath = ">>FullPath";
        public const string AADocDate = ">>DocDate";
        public const string ClientID = "Client ID";
        public const string BatchID = "Batch ID";
        public const string ProcessDate = "Process Date";
        public const string SystemDate = "System Date";
        public const string DepositDate = "Deposit Date";
        public const string TransactionNumber = "Transaction Number";
        public const string SequenceNumber = "Sequence Number";
        public const string RT = "RT";
        public const string Account = "Account";
        public const string AccountNumber = "Account Number";
        public const string Serial = "Serial";
        public const string TransactionCode = "Transaction Code";
        public const string RemitterName = "RemitterName";
        public const string RemitterSpaceName = "Remitter Name";
        public const string SiteID = "Site ID";
        public const string Amount = "Amount";
        public const string AppliedAmount = "Applied Amount";
        public const string DocumentType = "Document Type";
        public const string BatchNumber = "Batch Number";
    }
}
