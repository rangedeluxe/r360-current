﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VolumeCreateDATFiles
{
    public partial class CreateDATFiles : Form
    {

        private int _lockboxID, _numberOfTransactions, _numChecks, _numBatches, _numDocuments;
        private int _numberOfDatFiles;
        private Random rand;
        public CreateDATFiles()
        {
            InitializeComponent();
            rand = new Random();
        }

        private void Create_Click(object sender, EventArgs e)
        {


            if (CheckParameters())
            {
                for (int i = 0; i < _numberOfDatFiles; i++)
                {

                    bool rtnval = BuildDatFile(i);
                    if (!rtnval)
                    {
                        break;
                    }
                }
            }
           
        }


        private bool CheckParameters()
        {

            bool rtnval = true;
            if (!Int32.TryParse(NumDatFiles.Text, out _numberOfDatFiles))
            {
                MessageBox.Show("Number of Dat Files must be a number");
                rtnval = false;
                return rtnval;
            }
            if (!Int32.TryParse(NumTransactions.Text, out _numberOfTransactions))
            {
                MessageBox.Show("Could not convert number of transactions to a number");
                rtnval = false;
                return rtnval;
            }
            if (!Int32.TryParse(NumChecks.Text, out _numChecks))
            {
                MessageBox.Show("Could not convert number of checks to a number");
                rtnval = false;
                return rtnval;
            }
            if (!Int32.TryParse(NumDocs.Text, out _numDocuments))
            {
                MessageBox.Show("Could not convert number of documents to a number");
                rtnval = false;
                return rtnval;
            }
            if (!Int32.TryParse(NumBatches.Text, out _numBatches))
            {
                MessageBox.Show("Could not convert number of batches to a number");
                rtnval = false;
                return rtnval;
            }
            if (!Int32.TryParse(LockboxID.Text, out _lockboxID))
            {
                MessageBox.Show("Could not convert number of lockbox to a number");
                rtnval = false;
                return rtnval;
            }
            return rtnval;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool BuildDatFile(int i)
        {
            bool rtnval = true;
            string directory = directoryDATFiles.Text;
            
            try
            {
                
                string nameOfFile = String.Format("{0}.dat",i.ToString());
                string filename = Path.Combine(directory, Path.GetFileName(nameOfFile));
                using (TextWriter  tw = new System.IO.StreamWriter(filename, true))
                {
                    
                    tw.WriteLine(cMagicKeywords.TAGGED_DIP);
                    for (int j = 0; j < _numBatches; j++)
                    {
                        BuildBatch(tw);
                    }
                    tw.WriteLine(cMagicKeywords.End);
                  
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to build path. Excepction is " + ex.Message);
                rtnval = false;

            }

            return rtnval;
        }

        private void BuildBatch(TextWriter tw)
        {
            int numTransaction, numChecks, numDocuments;

            //get number of transactions between 1 and max transactions
            numTransaction = rand.Next(1, _numberOfTransactions + 1);
           

            for (int i = 0; i < numTransaction; i++)
            {
                //get number of checks between 1 and max checks
                numChecks = rand.Next(1, _numChecks + 1);
                //get number of documents between 1 and max documents
                numDocuments = rand.Next(1, _numDocuments + 1);
                //add checks
                for (int j = 0; j < numChecks; j++)
                {
                    BuildItem(tw, true);
                }
                //add documents
                for (int k = 0; k < numDocuments; k++)
                {
                    BuildItem(tw, false);
                }
               
            }
        }

        private void BuildItem(TextWriter tw, bool bCheck)
        {
            tw.WriteLine(cMagicKeywords.BEGIN_TEXT);

            tw.WriteLine(cMagicKeywords.AADocTypeName);
            tw.WriteLine(cMagicKeywords.AADocDate);

            tw.WriteLine(cMagicKeywords.ClientID + _lockboxID.ToString());
            tw.WriteLine("Client Name:" + ClientName.Text);


            tw.WriteLine("Transaction Number:" + ClientName.Text);
            tw.WriteLine("Batch ID:" + ClientName.Text);
        }

    }
}
