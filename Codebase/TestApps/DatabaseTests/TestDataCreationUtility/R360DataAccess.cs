﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DatabaseTestCommon.JsonData;

namespace TestDataCreationUtility
{
    public class R360DataAccess : IDisposable
    {
        public class BatchItem
        {
            public DateTime DepositDate;
            public int SourceBatchId;
        }

        public string DBServer { get; set; } = string.Empty;
        public string DBDatabase { get; set; } = string.Empty;
        public string DBLogin { get; set; } = string.Empty;
        public string DBPassword { get; set; } = string.Empty;

        private SqlConnection dbConnection = null;

        public R360DataAccess(string dbServer, string dbDatabase, string dbLogin, string dbPassword)
        {
            DBServer = dbServer;
            DBDatabase = dbDatabase;
            DBLogin = dbLogin;
            DBPassword = dbPassword;
            dbConnection = new SqlConnection(GetConnectionString());
            dbConnection.Open();
        }

        public void Dispose()
        {
            if (dbConnection != null)
                dbConnection.Close();
            dbConnection = null;
        }

        public BatchListJson GetBatchList(int BankId, int WorkgroupId, List<BatchItem>Batches = null)
        {
            var batchList = new BatchListJson()
            {
                SiteBankId = BankId,
                SiteClientAccountId = WorkgroupId,
                DepositDate = DateTime.Now.Date,
                ImmutableDate = DateTime.Now.Date,
                SourceProcessingDate = DateTime.Now.Date
            };

            if (Batches == null)
                batchList.Batches = GetAllBatches(BankId, WorkgroupId);
            else
            {
                batchList.Batches = new List<BatchJson>();
                foreach (var batchItem in Batches)
                {
                    var batches = GetBatches(BankId, WorkgroupId, batchItem.DepositDate, batchItem.SourceBatchId);
                    if (batches != null)
                        batchList.Batches.AddRange(batches);
                }
            }

            return batchList;
        }

        public List<BatchJson> GetBatches(int BankId, int WorkgroupId, DateTime? DepositDate = null, int? SourceBatchId = null)
        {
            var BankKey = GetBankKey(BankId);
            var ClientAccountKey = GetClientAccountKey(BankId, WorkgroupId);
            if ((BankKey < 0) || (ClientAccountKey < 0))
                return null;

            var sql = $"SELECT " +
                      $"*, " +
                      $"RecHubData.dimBatchSources.ShortName AS BatchSource, " +
                      $"RecHubData.dimBatchPaymentTypes.ShortName AS PaymentType, " +
                      $"RecHubData.dimBatchExceptionStatuses.ExceptionStatusName AS BatchExceptionStatus, " +
                      $"RecHubData.dimDepositStatuses.DespositDisplayName AS DepositStatusName, " +
                      $"DATETIMEFROMPARTS(DepositDateKey/10000,(DepositDateKey/100)%100,DepositDateKey%100,0,0,0,0) AS DepositDate, " +
                      $"DATETIMEFROMPARTS(ImmutableDateKey/10000,(ImmutableDateKey/100)%100,ImmutableDateKey%100,0,0,0,0) AS ImmutableDate, " +
                      $"DATETIMEFROMPARTS(SourceProcessingDateKey/10000,(SourceProcessingDateKey/100)%100,SourceProcessingDateKey%100,0,0,0,0) AS SourceProcessingDate " +
                      $"FROM RecHubData.factBatchSummary " +
                      $" INNER JOIN RecHubData.dimBatchSources ON (RecHubData.factBatchSummary.BatchSourceKey=RecHubData.dimBatchSources.BatchSourceKey) " +
                      $" INNER JOIN RecHubData.dimBatchPaymentTypes ON (RecHubData.factBatchSummary.BatchPaymentTypeKey=RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey) " +
                      $" INNER JOIN RecHubData.dimBatchExceptionStatuses ON (RecHubData.factBatchSummary.BatchExceptionStatusKey=RecHubData.dimBatchExceptionStatuses.BatchExceptionStatusKey) " +
                      $" INNER JOIN RecHubData.dimDepositStatuses ON (RecHubData.factBatchSummary.DepositStatus=RecHubData.dimDepositStatuses.DepositStatus) " +
                      $"WHERE (BankKey={BankKey}) AND (ClientAccountKey={ClientAccountKey}) ";
            if ((DepositDate != null) && (SourceBatchId != null))
            {
                int depdate = (DepositDate.Value.Year * 10000) + (DepositDate.Value.Month * 100) + DepositDate.Value.Day;
                sql += $"AND (DepositDateKey={depdate}) AND (SourceBatchID={SourceBatchId.Value}) ";
            }
            sql += $"ORDER BY BatchId";
            var ds = ExecSQLSelect(sql);
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;

            var batches = ds.Tables[0].ToList<BatchJson>();
            foreach (var batch in batches)
                batch.Transactions = GetTransactions(batch.BatchId);

            return batches;
        }

        public List<BatchJson> GetAllBatches(int BankId, int WorkgroupId)
        {
            return GetBatches(BankId, WorkgroupId, null, null);
        }
        
        public List<TransactionJson> GetTransactions(long BatchId)
        {
            var sql = $"SELECT " + 
                      $"*, " + 
                      $"RecHubData.dimTransactionExceptionStatuses.ExceptionStatusName AS TransactionExceptionStatus " +
                      $"FROM RecHubData.factTransactionSummary " + 
                      $" INNER JOIN RecHubData.dimTransactionExceptionStatuses ON (RecHubData.factTransactionSummary.TransactionExceptionStatusKey=RecHubData.dimTransactionExceptionStatuses.TransactionExceptionStatusKey) " +
                      $"WHERE (BatchId={BatchId}) " + 
                      $"ORDER BY TxnSequence";

            var ds = ExecSQLSelect(sql);
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;
            var transactions = ds.Tables[0].ToList<TransactionJson>();

            foreach (var transaction in transactions)
            {
                transaction.Checks = GetChecks(BatchId,transaction.TransactionID.Value);
                transaction.Stubs = GetStubs(BatchId,transaction.TransactionID.Value);
            }

            return transactions;
        }

        public List<CheckJson> GetChecks(long BatchId, int TransactionId)
        {
            var sql = $"SELECT " + 
                      $"*, " + 
                      $"RecHubData.dimDDAs.ABA AS ABA, " +
                      $"RecHubData.dimDDAS.DDA AS DDA " +
                      $"FROM RecHubData.factChecks " + 
                      $" INNER JOIN RecHubData.dimDDAs ON (RecHubData.factChecks.DDAKey=RecHubData.dimDDAs.DDAKey) " +
                      $"WHERE (BatchId={BatchId}) AND (TransactionId={TransactionId}) " + 
                      $"ORDER BY TxnSequence, SequenceWithinTransaction";

            var ds = ExecSQLSelect(sql);
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;
            var checks = ds.Tables[0].ToList<CheckJson>();

            foreach (var check in checks)
                check.RemittanceData = GetRemittanceData(BatchId, check.BatchSequence, true);

            return checks;
        }

        public List<StubJson> GetStubs(long BatchId, int TransactionId)
        {
            var sql = $"SELECT " + 
                      $"* " + 
                      $"FROM RecHubData.factStubs " + 
                      $"WHERE (BatchId={BatchId}) AND (TransactionId={TransactionId}) " + 
                      $"ORDER BY TxnSequence, SequenceWithinTransaction";

            var ds = ExecSQLSelect(sql);
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;
            var stubs = ds.Tables[0].ToList<StubJson>();

            foreach (var stub in stubs)
                stub.RemittanceData = GetRemittanceData(BatchId, stub.BatchSequence, false);

            return stubs;
        }

        public List<RemittanceDataJson> GetRemittanceData(long BatchId, int BatchSequence, bool IsCheck)
        {
            var ds = ExecSQLSelect($"SELECT " +
                                    $"    RecHubData.dimWorkgroupDataEntryColumns.FieldName, " +
                                    $"    RecHubData.factDataEntryDetails.DataEntryValue AS Value " +
                                    $"FROM " +
                                    $"    RecHubData.factDataEntryDetails INNER JOIN " +
                                    $"    RecHubData.dimWorkgroupDataEntryColumns ON(RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey= RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey)" +
                                    $"WHERE " +
                                    $"    (BatchId={BatchId}) AND " +
                                    $"    (BatchSequence={BatchSequence}) AND " +
                                    $"    (RecHubData.dimWorkgroupDataEntryColumns.IsCheck={(IsCheck ? 1 : 0)}) " +
                                    $"ORDER BY BatchSequence, FieldName ");
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;
            return ds.Tables[0].ToList<RemittanceDataJson>();
        }

        public List<BatchSourceJson> GetBatchSources()
        {
            var ds = ExecSQLSelect($"SELECT " +
                                    "RecHubData.dimBatchSources.*, " +
                                    "RecHubData.dimImportTypes.ShortName as ImportTypeName " +
                                    "FROM RecHubData.dimBatchSources INNER JOIN RecHubData.dimImportTypes ON (RecHubData.dimBatchSources.ImportTypeKey=RecHubData.dimImportTypes.ImportTypeKey)");
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;

            return ds.Tables[0].ToList<BatchSourceJson>();
        }

        public BankJson GetBankAndClients(int BankId, int WorkgroupId)
        {
            var ds = ExecSQLSelect($"SELECT * FROM RecHubData.dimBanks WHERE (SiteBankID={BankId}) AND (MostRecent=1)");
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;

            var bank = ds.Tables[0].ToList<BankJson>()[0];
            bank.ClientAccounts = GetClientAccountsForBank(BankId, WorkgroupId);
            if (!bank.SiteCodeId.HasValue)
                bank.SiteCodeId = -1;

            return bank;
        }

        public List<ClientAccountJson> GetClientAccountsForBank(int BankId, int WorkgroupId)
        {
            var ds = ExecSQLSelect($"SELECT * FROM RecHubData.dimClientAccounts WHERE (SiteBankID={BankId}) AND (SiteClientAccountID={WorkgroupId}) AND (MostRecent=1)");
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;

            var clients = ds.Tables[0].ToList<ClientAccountJson>();
            foreach (var client in clients)
                client.DataEntrySetups = GetDataEntrySetupsForClient(BankId,WorkgroupId);

            return clients;
        }

        public List<DataEntrySetupJson> GetDataEntrySetupsForClient(int BankId, int WorkgroupId)
        {
            var ds = ExecSQLSelect($"SELECT DISTINCT BatchSourceKey FROM RecHubData.dimWorkgroupDataEntryColumns WHERE (SiteBankID={BankId}) AND (SiteClientAccountID={WorkgroupId})");
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;

            var setups = new List<DataEntrySetupJson>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string BatchSourceKey = row["BatchSourceKey"].ToString();
                setups.Add(new DataEntrySetupJson()
                {
                    SiteBankId = BankId,
                    SiteClientAccountId = WorkgroupId,
                    BatchSource = GetBatchSourceName(BatchSourceKey),
                    DataEntryColumns = GetDataEntryColumnsForSetup(BankId, WorkgroupId, BatchSourceKey)
                });
            }
            return setups;
        }

        public List<DataEntryColumnJson> GetDataEntryColumnsForSetup(int BankId, int WorkgroupId, string BatchSourceKey)
        {
            var ds = ExecSQLSelect($"SELECT * FROM RecHubData.dimWorkgroupDataEntryColumns WHERE (SiteBankID={BankId}) AND (SiteClientAccountID={WorkgroupId}) AND (BatchSourceKey={BatchSourceKey})");
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return null;
            var columns = ds.Tables[0].ToList<DataEntryColumnJson>();
            return columns;
        }

        public string GetBatchSourceName(string BatchSourceKey)
        {
            var ds = ExecSQLSelect($"SELECT TOP 1 ShortName FROM RecHubData.dimBatchSources WHERE (BatchSourceKey={BatchSourceKey})");
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return string.Empty;
            return ds.Tables[0].Rows[0]["ShortName"].ToString().Trim();
        }

        public int GetBankKey(int BankId)
        {
            var ds = ExecSQLSelect($"SELECT TOP 1 BankKey FROM RecHubData.dimBanks WHERE (SiteBankId={BankId})");
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return -1;
            int BankKey = -1;
            if (int.TryParse(ds.Tables[0].Rows[0]["BankKey"].ToString().Trim(), out BankKey))
                return BankKey;
            return -1;
        }

        public int GetClientAccountKey(int BankId, int WorkgroupId)
        {
            var ds = ExecSQLSelect($"SELECT TOP 1 ClientAccountKey FROM RecHubData.dimClientAccounts WHERE (SiteBankId={BankId}) AND (SiteClientAccountId={WorkgroupId})");
            if ((ds == null) || (ds.Tables.Count == 0) || (ds.Tables[0].Rows.Count == 0))
                return -1;
            int ClientAccountKey = -1;
            if (int.TryParse(ds.Tables[0].Rows[0]["ClientAccountKey"].ToString().Trim(), out ClientAccountKey))
                return ClientAccountKey;
            return -1;
        }

        private DataSet ExecSQLSelect(string sql)
        {
            DataSet dataSet = null;
            var adapter = new SqlDataAdapter(sql, dbConnection);
            dataSet = new DataSet();
            adapter.Fill(dataSet);
            return dataSet;
        }

        private bool ExecSQLCommand(string sql)
        {
            bool rval = true;
            SqlCommand cmd = new SqlCommand(sql, dbConnection);
            cmd.ExecuteScalar();
            return (rval);
        }

        private string GetConnectionString()
        {
            return ("Data Source=" + DBServer + ";" +
                   "Initial Catalog=" + DBDatabase + ";" +
                   "User ID=" + DBLogin + ";" +
                   "Password=" + DBPassword + ";");
        }
    }
}
