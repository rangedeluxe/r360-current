﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using DatabaseTestCommon.Helpers;

namespace TestDataCreationUtility
{
    public class TestFileGenerator
    {
        public class BatchItem
        {
            DateTime DepositDate;
            int SourceBatchId;
        }

        public string DBServer { get; set; } = string.Empty;
        public string DBDatabase { get; set; } = string.Empty;
        public string DBLogin { get; set; } = string.Empty;
        public string DBPassword { get; set; } = string.Empty;
        public string OutputPath { get; set; } = string.Empty;
        public int Bank { get; set; } = -1;
        public int Workgroup { get; set; } = -1;
        public DateTime BaseDateForCurrentDate { get; set; } = DateTime.Today;
        public List<R360DataAccess.BatchItem> BatchItems { get; set; } = null;

        public bool IncludeBatchSources { get; set; } = false;
        public bool IncludeBankClient { get; set; } = false;
        public bool IncludeBatches { get; set; } = false;

        public string ErrorMessages { get; set; } = string.Empty;

        public TestFileGenerator(string dbServer, string dbDatabase, string dbLogin, string dbPassword, string outputPath)
        {
            DBServer = dbServer;
            DBDatabase = dbDatabase;
            DBLogin = dbLogin;
            DBPassword = dbPassword;
            OutputPath = outputPath;
        }

        public void GenerateFiles()
        {
            ErrorMessages = string.Empty;
            try
            {
                var db = new R360DataAccess(DBServer, DBDatabase, DBLogin, DBPassword);
                BatchDateConverter.BaseDateForCurrentDate = BaseDateForCurrentDate;

                var jsonSerializer = new JsonSerializer()
                {
                    Formatting = Formatting.Indented,
                    NullValueHandling = NullValueHandling.Ignore,
                    DateFormatHandling = DateFormatHandling.IsoDateFormat
                };
                JsonWriter jsonWriter;

                if (IncludeBatchSources)
                {
                    var batchSources = db.GetBatchSources();
                    jsonWriter = new JsonTextWriter(new StreamWriter(Path.Combine(OutputPath, "BatchSources.json")));
                    jsonSerializer.Serialize(jsonWriter, batchSources);
                    jsonWriter.Close();
                }

                if (IncludeBankClient)
                {
                    var bank = db.GetBankAndClients(Bank, Workgroup);
                    jsonWriter = new JsonTextWriter(new StreamWriter(Path.Combine(OutputPath, "BankClient.json")));
                    jsonSerializer.Serialize(jsonWriter, bank);
                    jsonWriter.Close();
                }

                if (IncludeBatches)
                {
                    var banks = db.GetBatchList(Bank, Workgroup, BatchItems);
                    jsonWriter = new JsonTextWriter(new StreamWriter(Path.Combine(OutputPath, "Batches.json")));
                    jsonSerializer.Serialize(jsonWriter, banks);
                    jsonWriter.Close();
                }

            }
            catch (Exception ex)
            {
                ErrorMessages += $"EXCEPTION: {ex.Message}\n";
            }
        }

 

    }
}
