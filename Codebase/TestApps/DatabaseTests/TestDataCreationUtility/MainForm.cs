﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace TestDataCreationUtility
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            tbServer.Text = ConfigurationManager.AppSettings["DBServer"]?.ToString().Trim() ?? string.Empty;
            tbDatabase.Text = ConfigurationManager.AppSettings["DBDatabase"]?.ToString().Trim() ?? string.Empty;
            tbLogin.Text = ConfigurationManager.AppSettings["DBLogin"]?.ToString().Trim() ?? string.Empty;
            tbPassword.Text = ConfigurationManager.AppSettings["DBPassword"]?.ToString().Trim() ?? string.Empty;
            tbBank.Text = ConfigurationManager.AppSettings["Bank"]?.ToString().Trim() ?? string.Empty;
            tbWorkgroup.Text = ConfigurationManager.AppSettings["Workgroup"]?.ToString().Trim() ?? string.Empty;
            tbOutputPath.Text = ConfigurationManager.AppSettings["OutputPath"]?.ToString().Trim() ?? string.Empty;
            dtBaseDate.Value = DateTime.Today;
        }

        private void btnCreateFiles_Click(object sender, EventArgs e)
        {
            int tmpInt;

            DisableControls();
            tbResults.Text = "Processing...";
            Application.DoEvents();

            var testFileGenerator = new TestFileGenerator(
                tbServer.Text, 
                tbDatabase.Text, 
                tbLogin.Text, 
                tbPassword.Text,
                tbOutputPath.Text);

            testFileGenerator.Bank = int.TryParse(tbBank.Text.Trim(), out tmpInt) ? tmpInt : -1;
            testFileGenerator.Workgroup = int.TryParse(tbWorkgroup.Text.Trim(), out tmpInt) ? tmpInt : -1;
            testFileGenerator.BaseDateForCurrentDate = dtBaseDate.Value.Date;
            testFileGenerator.IncludeBatchSources = cbIncludeBatchSources.Checked;
            testFileGenerator.IncludeBankClient = cbIncludeBankClient.Checked;
            testFileGenerator.IncludeBatches = cbIncludeBatches.Checked;

            if (cbAllBatches.Checked)
                testFileGenerator.BatchItems = null;
            else
            {
                testFileGenerator.BatchItems = new List<R360DataAccess.BatchItem>();
                AddBatchItemToList(testFileGenerator.BatchItems, tbDepositDate01.Text, tbBatchId01.Text);
                AddBatchItemToList(testFileGenerator.BatchItems, tbDepositDate02.Text, tbBatchId02.Text);
                AddBatchItemToList(testFileGenerator.BatchItems, tbDepositDate03.Text, tbBatchId03.Text);
                AddBatchItemToList(testFileGenerator.BatchItems, tbDepositDate04.Text, tbBatchId04.Text);
                AddBatchItemToList(testFileGenerator.BatchItems, tbDepositDate05.Text, tbBatchId05.Text);
                AddBatchItemToList(testFileGenerator.BatchItems, tbDepositDate06.Text, tbBatchId06.Text);
                AddBatchItemToList(testFileGenerator.BatchItems, tbDepositDate07.Text, tbBatchId07.Text);
                AddBatchItemToList(testFileGenerator.BatchItems, tbDepositDate08.Text, tbBatchId08.Text);
            }

            testFileGenerator.GenerateFiles();

            tbResults.Text = (testFileGenerator.ErrorMessages.Length == 0) ? ("Completed.") : (testFileGenerator.ErrorMessages);

            EnableControls();
        }

        private void AddBatchItemToList(List<R360DataAccess.BatchItem>Batches, string DepositDate, string SourceBatchId)
        {
            DateTime depositDate;
            int sourceBatchId;

            if (DateTime.TryParse(DepositDate.Trim(), out depositDate) && int.TryParse(SourceBatchId.Trim(), out sourceBatchId))
                Batches.Add(new R360DataAccess.BatchItem() { DepositDate=depositDate, SourceBatchId=sourceBatchId });
        }

        private void DisableControls()
        {
            UpdateControlEnabledState(false);
        }

        private void EnableControls()
        {
            UpdateControlEnabledState(true);
        }

        private void UpdateControlEnabledState(bool state)
        {
            gbDatabase.Enabled = state;
            gbWorkgroup.Enabled = state;
            gbOutput.Enabled = state;
            gbBatches.Enabled = state;
            btnCreateFiles.Enabled = state;
        }

        private void cbAllBatches_CheckedChanged(object sender, EventArgs e)
        {
            gbBatchList.Enabled = !cbAllBatches.Checked;
        }
    }
}
