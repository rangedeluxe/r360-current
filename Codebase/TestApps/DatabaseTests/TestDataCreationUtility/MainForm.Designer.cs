﻿namespace TestDataCreationUtility
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDatabase = new System.Windows.Forms.GroupBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.tbDatabase = new System.Windows.Forms.TextBox();
            this.tbServer = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.lblServer = new System.Windows.Forms.Label();
            this.btnCreateFiles = new System.Windows.Forms.Button();
            this.tbResults = new System.Windows.Forms.TextBox();
            this.gbWorkgroup = new System.Windows.Forms.GroupBox();
            this.lblWorkgroup = new System.Windows.Forms.Label();
            this.lblBank = new System.Windows.Forms.Label();
            this.gbOutput = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtBaseDate = new System.Windows.Forms.DateTimePicker();
            this.cbIncludeBatches = new System.Windows.Forms.CheckBox();
            this.cbIncludeBatchSources = new System.Windows.Forms.CheckBox();
            this.cbIncludeBankClient = new System.Windows.Forms.CheckBox();
            this.tbOutputPath = new System.Windows.Forms.TextBox();
            this.lblOutputLocation = new System.Windows.Forms.Label();
            this.gbBatches = new System.Windows.Forms.GroupBox();
            this.cbAllBatches = new System.Windows.Forms.CheckBox();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.gbBatchList = new System.Windows.Forms.GroupBox();
            this.tbDepositDate01 = new System.Windows.Forms.MaskedTextBox();
            this.lblDepositDate = new System.Windows.Forms.Label();
            this.lblSourceBatchId = new System.Windows.Forms.Label();
            this.tbBatchId01 = new System.Windows.Forms.MaskedTextBox();
            this.tbDepositDate02 = new System.Windows.Forms.MaskedTextBox();
            this.tbDepositDate03 = new System.Windows.Forms.MaskedTextBox();
            this.tbDepositDate04 = new System.Windows.Forms.MaskedTextBox();
            this.tbDepositDate05 = new System.Windows.Forms.MaskedTextBox();
            this.tbDepositDate06 = new System.Windows.Forms.MaskedTextBox();
            this.tbDepositDate07 = new System.Windows.Forms.MaskedTextBox();
            this.tbDepositDate08 = new System.Windows.Forms.MaskedTextBox();
            this.tbBatchId02 = new System.Windows.Forms.MaskedTextBox();
            this.tbBatchId03 = new System.Windows.Forms.MaskedTextBox();
            this.tbBatchId04 = new System.Windows.Forms.MaskedTextBox();
            this.tbBatchId05 = new System.Windows.Forms.MaskedTextBox();
            this.tbBatchId06 = new System.Windows.Forms.MaskedTextBox();
            this.tbBatchId07 = new System.Windows.Forms.MaskedTextBox();
            this.tbBatchId08 = new System.Windows.Forms.MaskedTextBox();
            this.tbBank = new System.Windows.Forms.MaskedTextBox();
            this.tbWorkgroup = new System.Windows.Forms.MaskedTextBox();
            this.gbDatabase.SuspendLayout();
            this.gbWorkgroup.SuspendLayout();
            this.gbOutput.SuspendLayout();
            this.gbBatches.SuspendLayout();
            this.gbResults.SuspendLayout();
            this.gbBatchList.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDatabase
            // 
            this.gbDatabase.Controls.Add(this.tbPassword);
            this.gbDatabase.Controls.Add(this.tbLogin);
            this.gbDatabase.Controls.Add(this.tbDatabase);
            this.gbDatabase.Controls.Add(this.tbServer);
            this.gbDatabase.Controls.Add(this.lblPassword);
            this.gbDatabase.Controls.Add(this.lblLogin);
            this.gbDatabase.Controls.Add(this.lblDatabase);
            this.gbDatabase.Controls.Add(this.lblServer);
            this.gbDatabase.Location = new System.Drawing.Point(229, 12);
            this.gbDatabase.Name = "gbDatabase";
            this.gbDatabase.Size = new System.Drawing.Size(417, 117);
            this.gbDatabase.TabIndex = 28;
            this.gbDatabase.TabStop = false;
            this.gbDatabase.Text = "Database";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(97, 87);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(140, 20);
            this.tbPassword.TabIndex = 36;
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(97, 64);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(140, 20);
            this.tbLogin.TabIndex = 34;
            // 
            // tbDatabase
            // 
            this.tbDatabase.Location = new System.Drawing.Point(97, 41);
            this.tbDatabase.Name = "tbDatabase";
            this.tbDatabase.Size = new System.Drawing.Size(302, 20);
            this.tbDatabase.TabIndex = 32;
            // 
            // tbServer
            // 
            this.tbServer.Location = new System.Drawing.Point(97, 18);
            this.tbServer.Name = "tbServer";
            this.tbServer.Size = new System.Drawing.Size(302, 20);
            this.tbServer.TabIndex = 30;
            // 
            // lblPassword
            // 
            this.lblPassword.Location = new System.Drawing.Point(6, 85);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(85, 23);
            this.lblPassword.TabIndex = 35;
            this.lblPassword.Text = "Password:";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLogin
            // 
            this.lblLogin.Location = new System.Drawing.Point(6, 62);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(85, 23);
            this.lblLogin.TabIndex = 33;
            this.lblLogin.Text = "Login:";
            this.lblLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDatabase
            // 
            this.lblDatabase.Location = new System.Drawing.Point(6, 39);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(85, 23);
            this.lblDatabase.TabIndex = 31;
            this.lblDatabase.Text = "Database:";
            this.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblServer
            // 
            this.lblServer.Location = new System.Drawing.Point(6, 16);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(85, 23);
            this.lblServer.TabIndex = 29;
            this.lblServer.Text = "Server:";
            this.lblServer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnCreateFiles
            // 
            this.btnCreateFiles.Location = new System.Drawing.Point(9, 395);
            this.btnCreateFiles.Name = "btnCreateFiles";
            this.btnCreateFiles.Size = new System.Drawing.Size(94, 23);
            this.btnCreateFiles.TabIndex = 27;
            this.btnCreateFiles.Text = "Create Files";
            this.btnCreateFiles.UseVisualStyleBackColor = true;
            this.btnCreateFiles.Click += new System.EventHandler(this.btnCreateFiles_Click);
            // 
            // tbResults
            // 
            this.tbResults.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbResults.Location = new System.Drawing.Point(9, 19);
            this.tbResults.Multiline = true;
            this.tbResults.Name = "tbResults";
            this.tbResults.ReadOnly = true;
            this.tbResults.Size = new System.Drawing.Size(402, 75);
            this.tbResults.TabIndex = 46;
            this.tbResults.TabStop = false;
            // 
            // gbWorkgroup
            // 
            this.gbWorkgroup.Controls.Add(this.tbWorkgroup);
            this.gbWorkgroup.Controls.Add(this.tbBank);
            this.gbWorkgroup.Controls.Add(this.lblWorkgroup);
            this.gbWorkgroup.Controls.Add(this.lblBank);
            this.gbWorkgroup.Location = new System.Drawing.Point(12, 12);
            this.gbWorkgroup.Name = "gbWorkgroup";
            this.gbWorkgroup.Size = new System.Drawing.Size(211, 68);
            this.gbWorkgroup.TabIndex = 1;
            this.gbWorkgroup.TabStop = false;
            this.gbWorkgroup.Text = "Bank/Workgroup";
            // 
            // lblWorkgroup
            // 
            this.lblWorkgroup.Location = new System.Drawing.Point(6, 39);
            this.lblWorkgroup.Name = "lblWorkgroup";
            this.lblWorkgroup.Size = new System.Drawing.Size(85, 23);
            this.lblWorkgroup.TabIndex = 4;
            this.lblWorkgroup.Text = "Workgroup:";
            this.lblWorkgroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBank
            // 
            this.lblBank.Location = new System.Drawing.Point(6, 16);
            this.lblBank.Name = "lblBank";
            this.lblBank.Size = new System.Drawing.Size(85, 23);
            this.lblBank.TabIndex = 2;
            this.lblBank.Text = "Bank:";
            this.lblBank.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbOutput
            // 
            this.gbOutput.Controls.Add(this.label1);
            this.gbOutput.Controls.Add(this.dtBaseDate);
            this.gbOutput.Controls.Add(this.cbIncludeBatches);
            this.gbOutput.Controls.Add(this.cbIncludeBatchSources);
            this.gbOutput.Controls.Add(this.cbIncludeBankClient);
            this.gbOutput.Controls.Add(this.tbOutputPath);
            this.gbOutput.Controls.Add(this.lblOutputLocation);
            this.gbOutput.Location = new System.Drawing.Point(229, 135);
            this.gbOutput.Name = "gbOutput";
            this.gbOutput.Size = new System.Drawing.Size(417, 148);
            this.gbOutput.TabIndex = 37;
            this.gbOutput.TabStop = false;
            this.gbOutput.Text = "Output";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 23);
            this.label1.TabIndex = 40;
            this.label1.Text = "Base Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtBaseDate
            // 
            this.dtBaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtBaseDate.Location = new System.Drawing.Point(97, 44);
            this.dtBaseDate.Name = "dtBaseDate";
            this.dtBaseDate.Size = new System.Drawing.Size(111, 20);
            this.dtBaseDate.TabIndex = 41;
            // 
            // cbIncludeBatches
            // 
            this.cbIncludeBatches.AutoSize = true;
            this.cbIncludeBatches.Checked = true;
            this.cbIncludeBatches.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbIncludeBatches.Location = new System.Drawing.Point(97, 116);
            this.cbIncludeBatches.Name = "cbIncludeBatches";
            this.cbIncludeBatches.Size = new System.Drawing.Size(125, 17);
            this.cbIncludeBatches.TabIndex = 44;
            this.cbIncludeBatches.Text = "Include Batches.json";
            this.cbIncludeBatches.UseVisualStyleBackColor = true;
            // 
            // cbIncludeBatchSources
            // 
            this.cbIncludeBatchSources.AutoSize = true;
            this.cbIncludeBatchSources.Checked = true;
            this.cbIncludeBatchSources.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbIncludeBatchSources.Location = new System.Drawing.Point(97, 93);
            this.cbIncludeBatchSources.Name = "cbIncludeBatchSources";
            this.cbIncludeBatchSources.Size = new System.Drawing.Size(153, 17);
            this.cbIncludeBatchSources.TabIndex = 43;
            this.cbIncludeBatchSources.Text = "Include BatchSources.json";
            this.cbIncludeBatchSources.UseVisualStyleBackColor = true;
            // 
            // cbIncludeBankClient
            // 
            this.cbIncludeBankClient.AutoSize = true;
            this.cbIncludeBankClient.Checked = true;
            this.cbIncludeBankClient.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbIncludeBankClient.Location = new System.Drawing.Point(97, 70);
            this.cbIncludeBankClient.Name = "cbIncludeBankClient";
            this.cbIncludeBankClient.Size = new System.Drawing.Size(137, 17);
            this.cbIncludeBankClient.TabIndex = 42;
            this.cbIncludeBankClient.Text = "Include BankClient.json";
            this.cbIncludeBankClient.UseVisualStyleBackColor = true;
            // 
            // tbOutputPath
            // 
            this.tbOutputPath.Location = new System.Drawing.Point(97, 18);
            this.tbOutputPath.Name = "tbOutputPath";
            this.tbOutputPath.Size = new System.Drawing.Size(302, 20);
            this.tbOutputPath.TabIndex = 39;
            // 
            // lblOutputLocation
            // 
            this.lblOutputLocation.Location = new System.Drawing.Point(6, 16);
            this.lblOutputLocation.Name = "lblOutputLocation";
            this.lblOutputLocation.Size = new System.Drawing.Size(85, 23);
            this.lblOutputLocation.TabIndex = 38;
            this.lblOutputLocation.Text = "Path:";
            this.lblOutputLocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbBatches
            // 
            this.gbBatches.Controls.Add(this.gbBatchList);
            this.gbBatches.Controls.Add(this.cbAllBatches);
            this.gbBatches.Location = new System.Drawing.Point(12, 86);
            this.gbBatches.Name = "gbBatches";
            this.gbBatches.Size = new System.Drawing.Size(211, 303);
            this.gbBatches.TabIndex = 6;
            this.gbBatches.TabStop = false;
            this.gbBatches.Text = "Batches";
            // 
            // cbAllBatches
            // 
            this.cbAllBatches.AutoSize = true;
            this.cbAllBatches.Checked = true;
            this.cbAllBatches.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAllBatches.Location = new System.Drawing.Point(20, 24);
            this.cbAllBatches.Name = "cbAllBatches";
            this.cbAllBatches.Size = new System.Drawing.Size(180, 17);
            this.cbAllBatches.TabIndex = 7;
            this.cbAllBatches.Text = "All Batches for Bank/Workgroup";
            this.cbAllBatches.UseVisualStyleBackColor = true;
            this.cbAllBatches.CheckedChanged += new System.EventHandler(this.cbAllBatches_CheckedChanged);
            // 
            // gbResults
            // 
            this.gbResults.Controls.Add(this.tbResults);
            this.gbResults.Location = new System.Drawing.Point(229, 289);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(417, 100);
            this.gbResults.TabIndex = 45;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Results";
            // 
            // gbBatchList
            // 
            this.gbBatchList.Controls.Add(this.tbBatchId08);
            this.gbBatchList.Controls.Add(this.tbBatchId07);
            this.gbBatchList.Controls.Add(this.tbBatchId06);
            this.gbBatchList.Controls.Add(this.tbBatchId05);
            this.gbBatchList.Controls.Add(this.tbBatchId04);
            this.gbBatchList.Controls.Add(this.tbBatchId03);
            this.gbBatchList.Controls.Add(this.tbBatchId02);
            this.gbBatchList.Controls.Add(this.tbDepositDate08);
            this.gbBatchList.Controls.Add(this.tbDepositDate07);
            this.gbBatchList.Controls.Add(this.tbDepositDate06);
            this.gbBatchList.Controls.Add(this.tbDepositDate05);
            this.gbBatchList.Controls.Add(this.tbDepositDate04);
            this.gbBatchList.Controls.Add(this.tbDepositDate03);
            this.gbBatchList.Controls.Add(this.tbDepositDate02);
            this.gbBatchList.Controls.Add(this.tbBatchId01);
            this.gbBatchList.Controls.Add(this.lblSourceBatchId);
            this.gbBatchList.Controls.Add(this.lblDepositDate);
            this.gbBatchList.Controls.Add(this.tbDepositDate01);
            this.gbBatchList.Enabled = false;
            this.gbBatchList.Location = new System.Drawing.Point(9, 49);
            this.gbBatchList.Name = "gbBatchList";
            this.gbBatchList.Size = new System.Drawing.Size(191, 248);
            this.gbBatchList.TabIndex = 8;
            this.gbBatchList.TabStop = false;
            // 
            // tbDepositDate01
            // 
            this.tbDepositDate01.Location = new System.Drawing.Point(11, 38);
            this.tbDepositDate01.Mask = "00/00/0000";
            this.tbDepositDate01.Name = "tbDepositDate01";
            this.tbDepositDate01.Size = new System.Drawing.Size(82, 20);
            this.tbDepositDate01.TabIndex = 11;
            this.tbDepositDate01.ValidatingType = typeof(System.DateTime);
            // 
            // lblDepositDate
            // 
            this.lblDepositDate.AutoSize = true;
            this.lblDepositDate.Location = new System.Drawing.Point(8, 16);
            this.lblDepositDate.Name = "lblDepositDate";
            this.lblDepositDate.Size = new System.Drawing.Size(69, 13);
            this.lblDepositDate.TabIndex = 9;
            this.lblDepositDate.Text = "Deposit Date";
            // 
            // lblSourceBatchId
            // 
            this.lblSourceBatchId.AutoSize = true;
            this.lblSourceBatchId.Location = new System.Drawing.Point(96, 16);
            this.lblSourceBatchId.Name = "lblSourceBatchId";
            this.lblSourceBatchId.Size = new System.Drawing.Size(86, 13);
            this.lblSourceBatchId.TabIndex = 10;
            this.lblSourceBatchId.Text = "Source Batch ID";
            // 
            // tbBatchId01
            // 
            this.tbBatchId01.Location = new System.Drawing.Point(99, 38);
            this.tbBatchId01.Mask = "0000000";
            this.tbBatchId01.Name = "tbBatchId01";
            this.tbBatchId01.Size = new System.Drawing.Size(63, 20);
            this.tbBatchId01.TabIndex = 12;
            this.tbBatchId01.ValidatingType = typeof(int);
            // 
            // tbDepositDate02
            // 
            this.tbDepositDate02.Location = new System.Drawing.Point(11, 64);
            this.tbDepositDate02.Mask = "00/00/0000";
            this.tbDepositDate02.Name = "tbDepositDate02";
            this.tbDepositDate02.Size = new System.Drawing.Size(82, 20);
            this.tbDepositDate02.TabIndex = 13;
            this.tbDepositDate02.ValidatingType = typeof(System.DateTime);
            // 
            // tbDepositDate03
            // 
            this.tbDepositDate03.Location = new System.Drawing.Point(11, 90);
            this.tbDepositDate03.Mask = "00/00/0000";
            this.tbDepositDate03.Name = "tbDepositDate03";
            this.tbDepositDate03.Size = new System.Drawing.Size(82, 20);
            this.tbDepositDate03.TabIndex = 15;
            this.tbDepositDate03.ValidatingType = typeof(System.DateTime);
            // 
            // tbDepositDate04
            // 
            this.tbDepositDate04.Location = new System.Drawing.Point(11, 116);
            this.tbDepositDate04.Mask = "00/00/0000";
            this.tbDepositDate04.Name = "tbDepositDate04";
            this.tbDepositDate04.Size = new System.Drawing.Size(82, 20);
            this.tbDepositDate04.TabIndex = 17;
            this.tbDepositDate04.ValidatingType = typeof(System.DateTime);
            // 
            // tbDepositDate05
            // 
            this.tbDepositDate05.Location = new System.Drawing.Point(11, 142);
            this.tbDepositDate05.Mask = "00/00/0000";
            this.tbDepositDate05.Name = "tbDepositDate05";
            this.tbDepositDate05.Size = new System.Drawing.Size(82, 20);
            this.tbDepositDate05.TabIndex = 19;
            this.tbDepositDate05.ValidatingType = typeof(System.DateTime);
            // 
            // tbDepositDate06
            // 
            this.tbDepositDate06.Location = new System.Drawing.Point(11, 168);
            this.tbDepositDate06.Mask = "00/00/0000";
            this.tbDepositDate06.Name = "tbDepositDate06";
            this.tbDepositDate06.Size = new System.Drawing.Size(82, 20);
            this.tbDepositDate06.TabIndex = 21;
            this.tbDepositDate06.ValidatingType = typeof(System.DateTime);
            // 
            // tbDepositDate07
            // 
            this.tbDepositDate07.Location = new System.Drawing.Point(11, 194);
            this.tbDepositDate07.Mask = "00/00/0000";
            this.tbDepositDate07.Name = "tbDepositDate07";
            this.tbDepositDate07.Size = new System.Drawing.Size(82, 20);
            this.tbDepositDate07.TabIndex = 23;
            this.tbDepositDate07.ValidatingType = typeof(System.DateTime);
            // 
            // tbDepositDate08
            // 
            this.tbDepositDate08.Location = new System.Drawing.Point(11, 220);
            this.tbDepositDate08.Mask = "00/00/0000";
            this.tbDepositDate08.Name = "tbDepositDate08";
            this.tbDepositDate08.Size = new System.Drawing.Size(82, 20);
            this.tbDepositDate08.TabIndex = 25;
            this.tbDepositDate08.ValidatingType = typeof(System.DateTime);
            // 
            // tbBatchId02
            // 
            this.tbBatchId02.Location = new System.Drawing.Point(99, 64);
            this.tbBatchId02.Mask = "0000000";
            this.tbBatchId02.Name = "tbBatchId02";
            this.tbBatchId02.Size = new System.Drawing.Size(63, 20);
            this.tbBatchId02.TabIndex = 14;
            this.tbBatchId02.ValidatingType = typeof(int);
            // 
            // tbBatchId03
            // 
            this.tbBatchId03.Location = new System.Drawing.Point(99, 90);
            this.tbBatchId03.Mask = "0000000";
            this.tbBatchId03.Name = "tbBatchId03";
            this.tbBatchId03.Size = new System.Drawing.Size(63, 20);
            this.tbBatchId03.TabIndex = 16;
            this.tbBatchId03.ValidatingType = typeof(int);
            // 
            // tbBatchId04
            // 
            this.tbBatchId04.Location = new System.Drawing.Point(99, 116);
            this.tbBatchId04.Mask = "0000000";
            this.tbBatchId04.Name = "tbBatchId04";
            this.tbBatchId04.Size = new System.Drawing.Size(63, 20);
            this.tbBatchId04.TabIndex = 18;
            this.tbBatchId04.ValidatingType = typeof(int);
            // 
            // tbBatchId05
            // 
            this.tbBatchId05.Location = new System.Drawing.Point(99, 142);
            this.tbBatchId05.Mask = "0000000";
            this.tbBatchId05.Name = "tbBatchId05";
            this.tbBatchId05.Size = new System.Drawing.Size(63, 20);
            this.tbBatchId05.TabIndex = 20;
            this.tbBatchId05.ValidatingType = typeof(int);
            // 
            // tbBatchId06
            // 
            this.tbBatchId06.Location = new System.Drawing.Point(99, 168);
            this.tbBatchId06.Mask = "0000000";
            this.tbBatchId06.Name = "tbBatchId06";
            this.tbBatchId06.Size = new System.Drawing.Size(63, 20);
            this.tbBatchId06.TabIndex = 22;
            this.tbBatchId06.ValidatingType = typeof(int);
            // 
            // tbBatchId07
            // 
            this.tbBatchId07.Location = new System.Drawing.Point(99, 194);
            this.tbBatchId07.Mask = "0000000";
            this.tbBatchId07.Name = "tbBatchId07";
            this.tbBatchId07.Size = new System.Drawing.Size(63, 20);
            this.tbBatchId07.TabIndex = 24;
            this.tbBatchId07.ValidatingType = typeof(int);
            // 
            // tbBatchId08
            // 
            this.tbBatchId08.Location = new System.Drawing.Point(99, 220);
            this.tbBatchId08.Mask = "0000000";
            this.tbBatchId08.Name = "tbBatchId08";
            this.tbBatchId08.Size = new System.Drawing.Size(63, 20);
            this.tbBatchId08.TabIndex = 26;
            this.tbBatchId08.ValidatingType = typeof(int);
            // 
            // tbBank
            // 
            this.tbBank.Location = new System.Drawing.Point(97, 18);
            this.tbBank.Mask = "00000000";
            this.tbBank.Name = "tbBank";
            this.tbBank.Size = new System.Drawing.Size(74, 20);
            this.tbBank.TabIndex = 3;
            this.tbBank.ValidatingType = typeof(int);
            // 
            // tbWorkgroup
            // 
            this.tbWorkgroup.Location = new System.Drawing.Point(97, 42);
            this.tbWorkgroup.Mask = "00000000";
            this.tbWorkgroup.Name = "tbWorkgroup";
            this.tbWorkgroup.Size = new System.Drawing.Size(74, 20);
            this.tbWorkgroup.TabIndex = 5;
            this.tbWorkgroup.ValidatingType = typeof(int);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 429);
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.gbBatches);
            this.Controls.Add(this.gbOutput);
            this.Controls.Add(this.gbWorkgroup);
            this.Controls.Add(this.btnCreateFiles);
            this.Controls.Add(this.gbDatabase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Test Data Creation Utility";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.gbDatabase.ResumeLayout(false);
            this.gbDatabase.PerformLayout();
            this.gbWorkgroup.ResumeLayout(false);
            this.gbWorkgroup.PerformLayout();
            this.gbOutput.ResumeLayout(false);
            this.gbOutput.PerformLayout();
            this.gbBatches.ResumeLayout(false);
            this.gbBatches.PerformLayout();
            this.gbResults.ResumeLayout(false);
            this.gbResults.PerformLayout();
            this.gbBatchList.ResumeLayout(false);
            this.gbBatchList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDatabase;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.TextBox tbDatabase;
        private System.Windows.Forms.TextBox tbServer;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Button btnCreateFiles;
        private System.Windows.Forms.GroupBox gbWorkgroup;
        private System.Windows.Forms.Label lblWorkgroup;
        private System.Windows.Forms.Label lblBank;
        private System.Windows.Forms.GroupBox gbOutput;
        private System.Windows.Forms.TextBox tbOutputPath;
        private System.Windows.Forms.Label lblOutputLocation;
        private System.Windows.Forms.CheckBox cbIncludeBatchSources;
        private System.Windows.Forms.CheckBox cbIncludeBankClient;
        private System.Windows.Forms.CheckBox cbIncludeBatches;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtBaseDate;
        private System.Windows.Forms.GroupBox gbBatches;
        private System.Windows.Forms.CheckBox cbAllBatches;
        private System.Windows.Forms.GroupBox gbResults;
        private System.Windows.Forms.TextBox tbResults;
        private System.Windows.Forms.GroupBox gbBatchList;
        private System.Windows.Forms.MaskedTextBox tbWorkgroup;
        private System.Windows.Forms.MaskedTextBox tbBank;
        private System.Windows.Forms.MaskedTextBox tbBatchId08;
        private System.Windows.Forms.MaskedTextBox tbBatchId07;
        private System.Windows.Forms.MaskedTextBox tbBatchId06;
        private System.Windows.Forms.MaskedTextBox tbBatchId05;
        private System.Windows.Forms.MaskedTextBox tbBatchId04;
        private System.Windows.Forms.MaskedTextBox tbBatchId03;
        private System.Windows.Forms.MaskedTextBox tbBatchId02;
        private System.Windows.Forms.MaskedTextBox tbDepositDate08;
        private System.Windows.Forms.MaskedTextBox tbDepositDate07;
        private System.Windows.Forms.MaskedTextBox tbDepositDate06;
        private System.Windows.Forms.MaskedTextBox tbDepositDate05;
        private System.Windows.Forms.MaskedTextBox tbDepositDate04;
        private System.Windows.Forms.MaskedTextBox tbDepositDate03;
        private System.Windows.Forms.MaskedTextBox tbDepositDate02;
        private System.Windows.Forms.MaskedTextBox tbBatchId01;
        private System.Windows.Forms.Label lblSourceBatchId;
        private System.Windows.Forms.Label lblDepositDate;
        private System.Windows.Forms.MaskedTextBox tbDepositDate01;
    }
}

