﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AutoMapper;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.SchemaClasses;
using DatabaseTestCommon.JsonData;
using Newtonsoft.Json;

namespace DatabaseTestFramework
{
    public class DataLoadHelper
    {
        private readonly IMapper _mapper;
        private readonly IntegrationTest _integrationTest;
        private readonly RecHubData _recHubData;
        private readonly RecHubUser _recHubUser;

        public DataLoadHelper(RecHubData recHubData, RecHubUser recHubUser, IntegrationTest integrationTest)
        {
            _recHubData = recHubData;
            _recHubUser = recHubUser;
            _integrationTest = integrationTest;

            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<BankJson, BankDto>()
                    .ForMember(d => d.CreationDate, opt => opt.Ignore())
                    .ForMember(d => d.MostRecent, opt => opt.Ignore());
                c.CreateMap<ClientAccountJson, ClientAccountDto>()
                    .ForMember(d => d.CreationDate, opt => opt.Ignore())
                    .ForMember(d => d.ModificationDate, opt => opt.Ignore());
                c.CreateMap<SiteCodeJson, SiteCodeDto>();
            });

            config.AssertConfigurationIsValid();
            _mapper = config.CreateMapper();
        }

        private void ProcessBank(BankJson bankSetup)
        {
            // Update the client in from the bank if null
            bankSetup.CopyToChildren();

            var bankDto = _mapper.Map<BankDto>(bankSetup);
            _recHubData.InsertDimBank(bankDto);

            if (bankDto.ClientAccounts != null && bankDto.ClientAccounts.Any())
            {
                foreach (var clientSetup in bankDto.ClientAccounts)
                {
                    ProcessClientSetup(clientSetup);
                }
            }
        }

        private void ProcessClientSetup(ClientAccountDto clientAccount)
        {
            _recHubData.InsertDimClientAccount(clientAccount);
            if (clientAccount.OlWorkgroup != null)
            {
                _recHubUser.InsertOlEntity(new OLEntitiesDto { EntityId = clientAccount.OlWorkgroup.EntityId ?? 0 });
                _recHubUser.InsertOlWorkgroup(clientAccount.SiteBankId, clientAccount.SiteClientAccountId, clientAccount.OlWorkgroup);
            }
            if ( clientAccount.DataEntrySetups != null && clientAccount.DataEntrySetups.Any())
                ProcessDataEntrySetups(clientAccount.DataEntrySetups);
        }

        private void ProcessDataEntrySetups(List<DataEntrySetupDto> dataEntrySetups)
        {
            foreach (var dataEntrySetup in dataEntrySetups)
            {
                foreach (var dataEntryColumn in dataEntrySetup.DataEntryColumns)
                    _integrationTest.InsertDimWorkgroupDataEntryColumns(dataEntryColumn);
            }
        }

        public void LoadBankFile(string bankSetupPath, string searchPattern = "*.json")
        {
            FileInfo[] files =
                new DirectoryInfo(bankSetupPath).GetFiles(searchPattern);

            foreach (var file in files)
            {
                var bankSetup = JsonConvert.DeserializeObject<BankJson>(File.ReadAllText(file.FullName));

                ProcessBank(bankSetup);
            }
        }

        public void LoadBatchSourceFile(string batchSourcePath, string searchPattern = "*.json")
        {
            FileInfo[] files =
                new DirectoryInfo(batchSourcePath).GetFiles(searchPattern);

            foreach (var file in files)
            {
                var batchSources = JsonConvert.DeserializeObject<List<BatchSourceDto>>(File.ReadAllText(file.FullName));
                foreach (var batchSource in batchSources)
                {
                    _integrationTest.InsertBatchSource(batchSource);
                }
            }

        }

        public void LoadBatches(string batchPath, string searchPattern = "*.json")
        {
            FileInfo[] files =
                new DirectoryInfo(batchPath).GetFiles(searchPattern);

            foreach (var file in files)
            {
                var batchList = JsonConvert.DeserializeObject<BatchListJson>(File.ReadAllText(file.FullName));

                if (batchList.Batches != null && batchList.Batches.Any())
                {
                    foreach (var batch in batchList.Batches)
                    {
                        if (batch.SiteBankId == null)
                            batch.SiteBankId = batchList.SiteBankId;
                        if (batch.SiteClientAccountId == null)
                            batch.SiteClientAccountId = batchList.SiteClientAccountId;
                        if (batch.DepositDate == DateTime.MinValue)
                            batch.DepositDate = batchList.DepositDate;
                        if (batch.ImmutableDate == DateTime.MinValue)
                            batch.ImmutableDate = batchList.ImmutableDate;
                        if (batch.SourceProcessingDate == DateTime.MinValue)
                            batch.SourceProcessingDate = batchList.SourceProcessingDate;
                        if (batch.BatchSource == null)
                            batch.BatchSource = batchList.BatchSource;
                        if (batch.PaymentType == null)
                            batch.PaymentType = batchList.PaymentType;
                        if (batch.BatchExceptionStatus == null)
                            batch.BatchExceptionStatus = batchList.BatchExceptionStatus;
                        if (batch.DepositStatusName == null)
                            batch.DepositStatusName = batchList.DepositStatusName;
                        if (batch.BatchCueId == null)
                            batch.BatchCueId = batchList.BatchCueId;
                        if (batch.SystemType == null)
                            batch.SystemType = batchList.SystemType;
                        if (batch.DepositDDA == null)
                            batch.DepositDDA = batchList.DepositDDA;
                        _integrationTest.InsertBatch(batch);
                    }
                }
            }
        }

        public void LoadSiteCodeFile(string siteCodeSetupPath, string searchPattern = "*.json")
        {
            FileInfo[] files =
                new DirectoryInfo(siteCodeSetupPath).GetFiles(searchPattern ?? "*.json");

            foreach (var file in files)
            {
                var siteCode = JsonConvert.DeserializeObject<SiteCodeJson>(File.ReadAllText(file.FullName));

                _recHubData.InsertDimSiteCode(_mapper.Map<SiteCodeDto>(siteCode));
            }
        }
    }
}
