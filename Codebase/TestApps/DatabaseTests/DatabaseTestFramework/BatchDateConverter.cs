﻿using System;
using Newtonsoft.Json;

namespace DatabaseTestFramework
{
    public class BatchDateConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var batchDate = DateTime.Now;

            var batchDateString = reader.Value.ToString();
            if (batchDateString.ToUpper().Contains("@CURRENTDATE"))
            {
                string[] modifier;
                if ((modifier = batchDateString.Split('+')).Length == 2)
                {
                    if( int.TryParse(modifier[1],out var days))
                        batchDate = batchDate.AddDays(days);
                }
                if ((modifier = batchDateString.Split('-')).Length == 2)
                {
                    if (int.TryParse(modifier[1], out var days))
                        batchDate = batchDate.AddDays(-days);
                }
            }
            else
                DateTime.TryParse(batchDateString, out batchDate);

            return batchDate;
        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }
    }
}
