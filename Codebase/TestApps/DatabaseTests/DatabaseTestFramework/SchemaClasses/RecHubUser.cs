﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.SchemaClasses
{
    public class RecHubUser : R360Database
    {
        private IDatabaseManager _databaseManager;
        public RecHubUser(IDatabaseManager databaseManager, DateTime runDate) : base(databaseManager, runDate)
        {
            _databaseManager = databaseManager;
            CreateTable("SessionClientAccountEntitlements");
        }

        public void ExecuteOLEntitiesUpsert(List<OLEntitiesDto> olEntities)
        {
            foreach (var olEntity in olEntities)
            {
                try
                {
                    var sqlParams = new[]
                    {
                        _databaseManager.BuildSqlParameter("@parmUserId", SqlDbType.Int, olEntity.UserId),
                        _databaseManager.BuildSqlParameter("@parmEntityId", SqlDbType.Int, olEntity.EntityId),
                        _databaseManager.BuildSqlParameter("@parmDisplayBatchId", SqlDbType.Bit, olEntity.DisplayBatchId),
                        _databaseManager.BuildSqlParameter("@parmPaymentImageDisplayMode", SqlDbType.Int, olEntity.PaymentImageDisplayMode),
                        _databaseManager.BuildSqlParameter("@parmDocumentImageDisplayMode", SqlDbType.Int, olEntity.DocumentImageDisplayMode),
                        _databaseManager.BuildSqlParameter("@parmViewingDays", SqlDbType.Int, olEntity.ViewingDays),
                        _databaseManager.BuildSqlParameter("@parmMaximumSearchDays", SqlDbType.Int, olEntity.MaximumSearchDays),
                        _databaseManager.BuildSqlParameter("@paramEntityName", SqlDbType.VarChar, (object) olEntity.EntityName ??  DBNull.Value)
                    };

                    ExecuteStoredProcedure("usp_OLEntities_Upsert", sqlParams);
                }
                catch (Exception ex)
                {
                    throw new Exception($"An Error occurred while executing ExecuteOLEntitiesUpsert: {ex.Message}", ex);
                }

            }
        }

        public void InsertOlEntity(OLEntitiesDto olEntities)
        {
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.CommandType = CommandType.Text;
                //var columns = new List<string>()
                //{
                //    "EntityID", "DisplayBatchID", "CheckImageDisplayMode", "DocumentImageDisplayMode", "ViewingDays", "MaximumSearchDays",
                //    "BillingAccount", "BillingField1", "BillingField2"
                //};
                var columns = new List<string>()
                {
                    "EntityID", "DisplayBatchID", "CheckImageDisplayMode", "DocumentImageDisplayMode", "ViewingDays"
                };

                sqlCommand.Parameters.AddWithValue("@EntityID", olEntities.EntityId);
                sqlCommand.Parameters.AddWithValue("@DisplayBatchID", olEntities.DisplayBatchId);
                sqlCommand.Parameters.AddWithValue("@CheckImageDisplayMode", olEntities.PaymentImageDisplayMode);
                sqlCommand.Parameters.AddWithValue("@DocumentImageDisplayMode", olEntities.DocumentImageDisplayMode);
                sqlCommand.Parameters.AddWithValue("@ViewingDays", olEntities.ViewingDays);

                sqlCommand.CommandText = $"IF NOT EXISTS(SELECT 1 FROM RecHubUser.OLEntities WHERE EntityID = {olEntities.EntityId}) INSERT INTO RecHubUser.OLEntities ({string.Join<string>(",", columns)}) VALUES ({"@" + string.Join<string>(",@", columns)});";
                _databaseManager.ExecuteSqlCommand(sqlCommand);
            }
        }

        public void InsertOlWorkgroup(int siteBankId, int siteClientAccountId, OlWorkgroupDto olWorkgroup)
        {
            using (var sqlCommand = new SqlCommand())
            {
                var columns = new List<string>()
                {
                    "EntityID", "SiteBankID", "SiteClientAccountID", "CheckImageDisplayMode", "DocumentImageDisplayMode"
                };

                sqlCommand.Parameters.AddWithValue("@EntityID", olWorkgroup.EntityId);
                sqlCommand.Parameters.AddWithValue("@SiteBankID", siteBankId);
                sqlCommand.Parameters.AddWithValue("@SiteClientAccountID", siteClientAccountId);
                sqlCommand.Parameters.AddWithValue("@CheckImageDisplayMode", olWorkgroup.CheckImageDisplayMode);
                sqlCommand.Parameters.AddWithValue("@DocumentImageDisplayMode", olWorkgroup.DocumentImageDisplayMode);

                sqlCommand.CommandText = $"IF NOT EXISTS(SELECT 1 FROM RecHubUser.OLWorkgroups WHERE EntityID = {olWorkgroup.EntityId} AND SiteBankID = {siteBankId} AND SiteClientAccountID = {siteClientAccountId}) INSERT INTO RecHubUser.OLWorkgroups ({string.Join<string>(",", columns)}) VALUES ({"@" + string.Join<string>(",@", columns)});";
                _databaseManager.ExecuteSqlCommand(sqlCommand);
            }
        }

        public int InsertUser(string logonName, string logonEntityName = null, int? logonEntityId = null)
        {
            var results = new DataTable();

            results.Columns.Add("UserId", typeof(int));

            _databaseManager.ExecuteSqlCommand($"IF NOT EXISTS(SELECT 1 FROM RecHubUser.Users WHERE LogonName = '{logonName}') BEGIN INSERT INTO RecHubUser.Users(LogonName,RA3MSID,LogonEntityID,LogonEntityName) VALUES ('{logonName}','{Guid.NewGuid()}','{logonEntityId}','{logonEntityName}'); SELECT SCOPE_IDENTITY() AS UserId; END ELSE SELECT UserId FROM RecHubUser.Users WHERE LogonName = '{logonName}'", ref results);

            return (int)results.Rows[0].ItemArray[0];
        }
    }
}
