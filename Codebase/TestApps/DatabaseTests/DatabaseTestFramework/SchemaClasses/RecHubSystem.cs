﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DatabaseTestFramework;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.SchemaClasses
{
    public class RecHubSystem : R360Database
    {
        private readonly IDatabaseManager _databaseManager;

        public RecHubSystem(IDatabaseManager databaseManager, DateTime runDate) : base(
            databaseManager, runDate)
        {
            _databaseManager = databaseManager;

        }

        public bool Execute_DataImportQueue_Ins_JSONBatch(DataTable insertData)
        {
            var ret = true;

            var parms = new SqlParameter[]
            {
                _databaseManager.BuildSqlParameter("@parmDataImportQueueInsertTable", SqlDbType.Structured, insertData)
            };

            ExecuteStoredProcedure("usp_DataImportQueue_Ins_JSONBatch", parms);

            return ret;
        }
    }
}
