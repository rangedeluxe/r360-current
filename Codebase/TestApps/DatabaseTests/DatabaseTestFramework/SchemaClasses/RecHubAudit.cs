﻿using System;
using System.Collections.Generic;
using System.Text;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.SchemaClasses
{
    public class RecHubAudit : R360Database
    {
        private IDatabaseManager _databaseManager;
        public RecHubAudit(IDatabaseManager databaseManager, DateTime runDate) : base(databaseManager, runDate)
        {
            _databaseManager = databaseManager;
            CreateTable("dimAuditApplications");
            CreateTable("dimAuditColumns");
            CreateTable("dimAuditEvents");

            CreateStoredProcedure("usp_dimAuditApplications_Get");
            CreateStoredProcedure("usp_dimAuditColumns_Get");
        }
    }
}
