﻿using AutoMapper;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Extensions;
using DatabaseTestFramework.Interfaces;
using DatabaseTestCommon.JsonData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace DatabaseTestFramework.SchemaClasses
{
    public class IntegrationTest : R360Database
    {
        private readonly IDatabaseManager _databaseManager;
        private readonly IMapper _mapper;

        public IntegrationTest(IDatabaseManager databaseManager, DateTime runDate) : base(databaseManager, runDate)
        {
            _databaseManager = databaseManager;
            CreateStoredProcedure("usp_dimBatchSources_Ins");
            CreateStoredProcedure("usp_dimBatchPaymentTypes_Ins");
            CreateStoredProcedure("usp_dimBatchExceptionStatuses_Ins");
            CreateStoredProcedure("usp_dimDates_Ins");
            CreateStoredProcedure("usp_dimDDAs_GetSert");
            CreateStoredProcedure("usp_dimDepositStatus_Ins");
            CreateStoredProcedure("usp_dimImportTypes_Ins");
            CreateStoredProcedure("usp_dimTransactionExceptionStatuses_Ins");
            CreateStoredProcedure("usp_SessionClientAccountEntitlements_Ins");

            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<BatchJson, FactBatchSummaryDto>()
                    .ForMember(d => d.DepositDateKey, opt => opt.MapFrom(s => s.DepositDate.ToDateKey()))
                    .ForMember(d => d.ImmutableDateKey, opt => opt.MapFrom(s => s.ImmutableDate.ToDateKey()))
                    .ForMember(d => d.SourceProcessingDateKey, opt => opt.MapFrom(s => s.SourceProcessingDate.ToDateKey()));
                c.CreateMap<FactBatchSummaryDto, FactTransactionSummaryDto>()
                    .ForMember(d => d.CheckCount, opt => opt.Ignore())
                    .ForMember(d => d.CheckTotal, opt => opt.Ignore())
                    .ForMember(d => d.StubCount, opt => opt.Ignore())
                    .ForMember(d => d.StubTotal, opt => opt.Ignore());
                c.CreateMap<FactTransactionSummaryDto, FactCheckDto>();
                c.CreateMap<FactTransactionSummaryDto, FactStubDto>();
                c.CreateMap<CheckJson, FactCheckDto>()
                    .ForMember(d => d.SequenceWithinTransaction, opt => opt.Ignore());
                c.CreateMap<StubJson, FactStubDto>()
                    .ForMember(d => d.SequenceWithinTransaction, opt => opt.Ignore());
                c.CreateMap<RemittanceDataJson, RemittanceDataDto>()
                    .ForMember(d => d.BankKey, opt => opt.Ignore());
                c.CreateMap<FactCheckDto, RemittanceDataDto>()
                    .ForMember(d => d.FieldName, opt => opt.Ignore())
                    .ForMember(d => d.Value, opt => opt.Ignore());
                c.CreateMap<FactStubDto, RemittanceDataDto>()
                    .ForMember(d => d.FieldName, opt => opt.Ignore())
                    .ForMember(d => d.Value, opt => opt.Ignore())
                    .ForMember(d => d.IsCheck, opt=> opt.MapFrom( o=> false));
            });
            _mapper = config.CreateMapper();
        }

        public long GetBatchId(int depositDateKey, int siteBankId, int siteClientAccountId, int sourceBatchId)
        {
            var results = new DataTable();
            var sqlCommand = new StringBuilder();

            sqlCommand.Append("SELECT BatchID FROM RecHubData.factBatchSummary ");
            sqlCommand.Append("INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey ");
            sqlCommand.Append($"WHERE DepositDateKey = {depositDateKey} AND SiteBankID = {siteBankId} AND SiteClientAccountID = {siteClientAccountId} AND SourceBatchID = {sourceBatchId}");
            _databaseManager.ExecuteSqlCommand(sqlCommand.ToString(), ref results);

            return (long)results.Rows[0].ItemArray[0];
        }

        public List<SessionClientAccountEntitlementsDto>GetSessionClientAccountEntitlementsList()
        {
            var results = new DataTable();
            var entitlements = new List<SessionClientAccountEntitlementsDto>();

            _databaseManager.ExecuteSqlCommand("SELECT SiteBankID, SiteClientAccountID FROM RecHubData.dimClientAccounts", ref results);
            foreach (DataRow row in results.Rows)
            {
                var entitlement = new SessionClientAccountEntitlementsDto()
                {
                    SiteBankId = Convert.ToInt32(row["SiteBankID"].ToString()),
                    SiteClientAccountId = Convert.ToInt32(row["SiteClientAccountId"].ToString())
                };
                entitlements.Add(entitlement);
            }
            return entitlements;
        }

        public void InsertDimWorkgroupDataEntryColumns(DataEntryColumnDto dataEntryColumn)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.Int, dataEntryColumn.SiteBankId),
                _databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.Int,
                    dataEntryColumn.SiteClientAccountId),
                _databaseManager.BuildSqlParameter("@parmBatchSource", SqlDbType.VarChar, dataEntryColumn.BatchSource),
                _databaseManager.BuildSqlParameter("@parmIsCheck", SqlDbType.Bit, dataEntryColumn.IsCheck),
                _databaseManager.BuildSqlParameter("@parmIsActive", SqlDbType.Bit, dataEntryColumn.IsActive),
                _databaseManager.BuildSqlParameter("@parmIsRequired", SqlDbType.Bit, dataEntryColumn.IsRequired),
                _databaseManager.BuildSqlParameter("@parmMarkSense", SqlDbType.Bit, dataEntryColumn.MarkSense),
                _databaseManager.BuildSqlParameter("@parmDataType", SqlDbType.TinyInt, dataEntryColumn.DataType),
                _databaseManager.BuildSqlParameter("@parmScreenOrder", SqlDbType.Bit, dataEntryColumn.ScreenOrder),
                _databaseManager.BuildSqlParameter("@parmUILabel", SqlDbType.NVarChar, dataEntryColumn.UiLabel),
                _databaseManager.BuildSqlParameter("@parmFieldName", SqlDbType.NVarChar, dataEntryColumn.FieldName),
                _databaseManager.BuildSqlParameter("@parmSourceDisplayName", SqlDbType.NVarChar,
                    (dataEntryColumn.SourceDisplayName ?? "" ))
            };

            ExecuteStoredProcedure("usp_dimWorkgroupDataEntryColumns_Ins", sqlParams);
        }

        public void InsertBatch(BatchJson batch)
        {
            var factBatchSummaryDto = InsertFactBatchSummary(batch);

            if (batch.Transactions != null && batch.Transactions.Any())
            {
                var autoTransactionId = 1;
                var autoTxnSequence = 1;
                var batchSequence = 1;
                var checkSequence = 1;
                var stubSequence = 1;

                foreach (var transaction in batch.Transactions)
                {
                    if (transaction.TransactionExceptionStatus == null)
                        transaction.TransactionExceptionStatus = batch.TransactionExceptionStatus;
                    var factTransactionSummaryDto = _mapper.Map<FactBatchSummaryDto, FactTransactionSummaryDto>(factBatchSummaryDto);

                    var transactionId = transaction.TransactionID ?? autoTransactionId++;
                    var txnSequence = transaction.TxnSequence ?? autoTxnSequence++;

                    factTransactionSummaryDto.TransactionId = transactionId;
                    factTransactionSummaryDto.TxnSequence = txnSequence;

                    if (transaction.Checks != null && transaction.Checks.Any())
                    {
                        factTransactionSummaryDto.CheckCount = transaction.Checks.Count();
                        factTransactionSummaryDto.CheckTotal = transaction.Checks.Sum(c => c.Amount);
                    }
                    if ( transaction.Stubs != null && transaction.Stubs.Any())
                    {
                        factTransactionSummaryDto.StubCount = transaction.Stubs.Count();
                        factTransactionSummaryDto.StubTotal = transaction.Stubs.Sum(s => s.Amount);
                    }

                    InsertFactTransactionSummary(factTransactionSummaryDto, transaction.TransactionExceptionStatus);

                    if (transaction.Checks != null && transaction.Checks.Any())
                    {
                        var autoSequenceWithinTransaction = 1;
                        foreach (var check in transaction.Checks)
                        {
                            var factCheckDto =
                                _mapper.Map<FactTransactionSummaryDto, FactCheckDto>(factTransactionSummaryDto);
                            _mapper.Map<CheckJson, FactCheckDto>(check, factCheckDto);

                            factCheckDto.SequenceWithinTransaction =
                                check.SequenceWithTransaction ?? autoSequenceWithinTransaction++;
                            if (Int64.TryParse(factCheckDto.RoutingNumber, out var numericRoutingNumber))
                                factCheckDto.NumericRoutingNumber = numericRoutingNumber;
                            if (Int64.TryParse(factCheckDto.Serial, out var numericSerial))
                                factCheckDto.NumericSerial = numericSerial;

                            factCheckDto.BatchSequence = batchSequence++;
                            factCheckDto.CheckSequence = checkSequence++;
                            InsertFactCheck(factCheckDto);
                            foreach (var remittanceData in factCheckDto.RemittanceData)
                            {
                                _mapper.Map<FactCheckDto, RemittanceDataDto>(factCheckDto, remittanceData);
                                InsertFactDataEntryDetails(remittanceData);
                            }
                        }
                    }

                    if ((transaction.Stubs != null) && transaction.Stubs.Any())
                    {
                        var autoSequenceWithinTransaction = 1;
                        foreach (var stub in transaction.Stubs)
                        {
                            var factStubDto = _mapper.Map<FactTransactionSummaryDto, FactStubDto>(factTransactionSummaryDto);
                            _mapper.Map<StubJson, FactStubDto>(stub, factStubDto);
                            factStubDto.SequenceWithinTransaction = autoSequenceWithinTransaction++;
                            factStubDto.BatchSequence = batchSequence++;
                            factStubDto.StubSequence = stubSequence++;
                            InsertFactStub(factStubDto);
                            if (!string.IsNullOrEmpty(factStubDto.AccountNumber))
                            {
                                factStubDto.RemittanceData.Add(new RemittanceDataDto {FieldName = "AccountNumber", Value = factStubDto.AccountNumber});
                            }

                            if (!string.IsNullOrEmpty(factStubDto.Amount.ToString(CultureInfo.InvariantCulture)))
                            {
                                factStubDto.RemittanceData.Add(new RemittanceDataDto {FieldName = "Amount", Value = factStubDto.Amount.ToString(CultureInfo.InvariantCulture)});
                            }
                            foreach (var remittanceData in factStubDto.RemittanceData)
                            {
                                _mapper.Map<FactStubDto, RemittanceDataDto>(factStubDto, remittanceData);
                                InsertFactDataEntryDetails(remittanceData);
                            }
                        }
                    }

                }
                InsertAdvancedSearch(factBatchSummaryDto.BatchId);
            }
        }

        public FactBatchSummaryDto InsertFactBatchSummary(BatchJson batch)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.Int, batch.SiteBankId),
                _databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.Int, batch.SiteClientAccountId),
                _databaseManager.BuildSqlParameter("@parmDepositDateKey", SqlDbType.Int, batch.DepositDate.ToDateKey()),
                _databaseManager.BuildSqlParameter("@parmImmutableDateKey", SqlDbType.Int, batch.ImmutableDate.ToDateKey()),
                _databaseManager.BuildSqlParameter("@parmSourceProcessingDateKey", SqlDbType.Int, batch.SourceProcessingDate.ToDateKey()),
                _databaseManager.BuildSqlParameter("@parmSourceBatchID", SqlDbType.BigInt, batch.SourceBatchId),
                _databaseManager.BuildSqlParameter("@parmBatchNumber", SqlDbType.BigInt, batch.BatchNumber),
                _databaseManager.BuildSqlParameter("@parmBatchSource", SqlDbType.VarChar, batch.BatchSource),
                _databaseManager.BuildSqlParameter("@parmPaymentType", SqlDbType.VarChar, batch.PaymentType),
                _databaseManager.BuildSqlParameter("@parmBatchExceptionStatus", SqlDbType.VarChar, batch.BatchExceptionStatus),
                _databaseManager.BuildSqlParameter("@parmBatchCueID", SqlDbType.Int, batch.BatchCueId),
                _databaseManager.BuildSqlParameter("@parmDepositStatusName", SqlDbType.VarChar, batch.DepositStatusName),
                _databaseManager.BuildSqlParameter("@parmSystemType", SqlDbType.SmallInt, batch.SystemType),
                _databaseManager.BuildSqlParameter("@parmTransactionCount", SqlDbType.Int, batch.Transactions?.Count ?? 0),
                _databaseManager.BuildSqlParameter("@parmCheckCount", SqlDbType.Int, batch.Transactions?.Sum(t => t.Checks?.Count) ?? 0),
                _databaseManager.BuildSqlParameter("@parmScannedCheckCount", SqlDbType.Int, 0), //TO DO
                _databaseManager.BuildSqlParameter("@parmStubCount", SqlDbType.Int, batch.Transactions?.Sum(t => t.Stubs?.Count ?? 0) ?? 0),
                _databaseManager.BuildSqlParameter("@parmDocumentCount", SqlDbType.Int, 0), //TO DO
                _databaseManager.BuildSqlParameter("@parmCheckTotal", SqlDbType.Money, batch.Transactions?.Sum(t => t.Checks?.Sum(c => c.Amount) ?? 0) ?? 0),
                _databaseManager.BuildSqlParameter("@parmStubTotal", SqlDbType.Money, batch.Transactions?.Sum(t => t.Stubs?.Sum(s => s.Amount) ?? 0) ?? 0),
                _databaseManager.BuildSqlParameter("@parmBatchSiteCode", SqlDbType.Int, (object) batch.BatchSiteCode ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmDepositDDA", SqlDbType.VarChar, (object) batch.DepositDDA ?? DBNull.Value)
            };

            var keys = new DataTable();

            ExecuteStoredProcedure("usp_factBatchSummary_Ins", sqlParams, ref keys);

            var factBatchSummaryDto = _mapper.Map<BatchJson, FactBatchSummaryDto>(batch);

            if (Int64.TryParse(keys.Rows[0]["BatchID"].ToString(), out var batchId))
                factBatchSummaryDto.BatchId = batchId;
            if (int.TryParse(keys.Rows[0]["BankKey"].ToString(), out var bankKey))
                factBatchSummaryDto.BankKey = bankKey;
            if (int.TryParse(keys.Rows[0]["ClientAccountKey"].ToString(), out var clientAccountKey))
                factBatchSummaryDto.ClientAccountKey = clientAccountKey;
            if (int.TryParse(keys.Rows[0]["BatchSourceKey"].ToString(), out var batchSourceKey))
                factBatchSummaryDto.BatchSourceKey = batchSourceKey;
            if (int.TryParse(keys.Rows[0]["PaymentTypeKey"].ToString(), out var paymentTypeKey))
                factBatchSummaryDto.PaymentTypeKey = paymentTypeKey;
            if (int.TryParse(keys.Rows[0]["BatchExceptionStatusKey"].ToString(), out var batchExceptionStatusKey))
                factBatchSummaryDto.BatchExceptionStatusKey = batchExceptionStatusKey;
            if (int.TryParse(keys.Rows[0]["DepositStatus"].ToString(), out var depositStatus))
                factBatchSummaryDto.DepositStatus = depositStatus;
            if (int.TryParse(keys.Rows[0]["DepositStatusKey"].ToString(), out var depositStatusKey))
                factBatchSummaryDto.DepositStatusKey = depositStatusKey;

            return factBatchSummaryDto;
        }

        public void InsertAdvancedSearch(Int64 batchId)
        {
            var sqlParamChecks = new[]
            {
                _databaseManager.BuildSqlParameter("@parmBatchID", SqlDbType.BigInt, batchId),
                _databaseManager.BuildSqlParameter("@parmIsCheck", SqlDbType.Bit, 1)
            };
            var sqlParamStubs = new[]
            {
                _databaseManager.BuildSqlParameter("@parmBatchID", SqlDbType.BigInt, batchId),
                _databaseManager.BuildSqlParameter("@parmIsCheck", SqlDbType.Bit, 0)
            };

            ExecuteStoredProcedure("usp_fact_Upd_DataEntry", sqlParamChecks);
            ExecuteStoredProcedure("usp_fact_Upd_DataEntry", sqlParamStubs);
        }

        public void InsertFactCheck(FactCheckDto factCheck)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmBankKey", SqlDbType.Int, factCheck.BankKey),
                _databaseManager.BuildSqlParameter("@parmClientAccountKey", SqlDbType.Int, factCheck.ClientAccountKey),
                _databaseManager.BuildSqlParameter("@parmDepositDateKey", SqlDbType.Int, factCheck.DepositDateKey),
                _databaseManager.BuildSqlParameter("@parmImmutableDateKey", SqlDbType.Int, factCheck.ImmutableDateKey),
                _databaseManager.BuildSqlParameter("@parmSourceProcessingDateKey", SqlDbType.Int, factCheck.SourceProcessingDateKey),
                _databaseManager.BuildSqlParameter("@parmBatchID", SqlDbType.BigInt, factCheck.BatchId),
                _databaseManager.BuildSqlParameter("@parmSourceBatchID", SqlDbType.BigInt, factCheck.SourceBatchId),
                _databaseManager.BuildSqlParameter("@parmBatchNumber", SqlDbType.BigInt, factCheck.BatchNumber),
                _databaseManager.BuildSqlParameter("@parmBatchSourceKey", SqlDbType.VarChar, factCheck.BatchSourceKey),
                _databaseManager.BuildSqlParameter("@parmPaymentTypeKey", SqlDbType.VarChar, factCheck.PaymentTypeKey),
                _databaseManager.BuildSqlParameter("@parmBatchCueID", SqlDbType.Int, factCheck.BatchCueId),
                _databaseManager.BuildSqlParameter("@parmDepositStatus", SqlDbType.VarChar, factCheck.DepositStatus),
                _databaseManager.BuildSqlParameter("@parmTransactionID", SqlDbType.Int, factCheck.TransactionId),
                _databaseManager.BuildSqlParameter("@parmTxnSequence", SqlDbType.Int, factCheck.TxnSequence),
                _databaseManager.BuildSqlParameter("@parmSequenceWithinTransaction", SqlDbType.Int, factCheck.SequenceWithinTransaction),
                _databaseManager.BuildSqlParameter("@parmBatchSequence", SqlDbType.Int, factCheck.BatchSequence),
                _databaseManager.BuildSqlParameter("@parmCheckSequence", SqlDbType.Int, factCheck.CheckSequence),
                _databaseManager.BuildSqlParameter("@parmAmount", SqlDbType.Money, factCheck.Amount),
                _databaseManager.BuildSqlParameter("@parmNumericRoutingNumber", SqlDbType.BigInt, factCheck.NumericRoutingNumber),
                _databaseManager.BuildSqlParameter("@parmNumericSerial", SqlDbType.BigInt, factCheck.NumericSerial),
                _databaseManager.BuildSqlParameter("@parmRoutingNumber", SqlDbType.VarChar, factCheck.RoutingNumber),
                _databaseManager.BuildSqlParameter("@parmAccount", SqlDbType.VarChar, factCheck.Account),
                _databaseManager.BuildSqlParameter("@parmSerial", SqlDbType.VarChar, (object) factCheck.Serial ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmTransactionCode", SqlDbType.VarChar, (object) factCheck.TransactionCode ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmRemitterName", SqlDbType.VarChar, (object) factCheck.RemitterName ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmBatchSiteCode", SqlDbType.Int, (object) factCheck.BatchSiteCode ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmABA", SqlDbType.VarChar, factCheck.ABA),
                _databaseManager.BuildSqlParameter("@parmDDA", SqlDbType.VarChar, factCheck.DDA)
            };

            ExecuteStoredProcedure("usp_factChecks_Ins", sqlParams);
        }

        public void InsertFactStub(FactStubDto factStub)
        {
            var sqlParams = new[]
{
                _databaseManager.BuildSqlParameter("@parmBankKey", SqlDbType.Int, factStub.BankKey),
                _databaseManager.BuildSqlParameter("@parmClientAccountKey", SqlDbType.Int, factStub.ClientAccountKey),
                _databaseManager.BuildSqlParameter("@parmDepositDateKey", SqlDbType.Int, factStub.DepositDateKey),
                _databaseManager.BuildSqlParameter("@parmImmutableDateKey", SqlDbType.Int, factStub.ImmutableDateKey),
                _databaseManager.BuildSqlParameter("@parmSourceProcessingDateKey", SqlDbType.Int, factStub.SourceProcessingDateKey),
                _databaseManager.BuildSqlParameter("@parmBatchID", SqlDbType.BigInt, factStub.BatchId),
                _databaseManager.BuildSqlParameter("@parmSourceBatchID", SqlDbType.BigInt, factStub.SourceBatchId),
                _databaseManager.BuildSqlParameter("@parmBatchNumber", SqlDbType.BigInt, factStub.BatchNumber),
                _databaseManager.BuildSqlParameter("@parmBatchSourceKey", SqlDbType.VarChar, factStub.BatchSourceKey),
                _databaseManager.BuildSqlParameter("@parmPaymentTypeKey", SqlDbType.VarChar, factStub.PaymentTypeKey),
                _databaseManager.BuildSqlParameter("@parmBatchCueID", SqlDbType.Int, factStub.BatchCueId),
                _databaseManager.BuildSqlParameter("@parmDepositStatus", SqlDbType.VarChar, factStub.DepositStatus),
                _databaseManager.BuildSqlParameter("@parmSystemType", SqlDbType.Int, factStub.SystemType),
                _databaseManager.BuildSqlParameter("@parmTransactionID", SqlDbType.Int, factStub.TransactionId),
                _databaseManager.BuildSqlParameter("@parmTxnSequence", SqlDbType.Int, factStub.TxnSequence),
                _databaseManager.BuildSqlParameter("@parmSequenceWithinTransaction", SqlDbType.Int, factStub.SequenceWithinTransaction),
                _databaseManager.BuildSqlParameter("@parmBatchSequence", SqlDbType.Int, factStub.BatchSequence),
                _databaseManager.BuildSqlParameter("@parmStubSequence", SqlDbType.Int, factStub.StubSequence),
                _databaseManager.BuildSqlParameter("@parmIsCorrespondence", SqlDbType.Bit, factStub.IsCorrespondence),
                _databaseManager.BuildSqlParameter("@parmDocumentBatchSequence", SqlDbType.Int, factStub.DocumentBatchSequence),
                _databaseManager.BuildSqlParameter("@parmIsOMRDetected", SqlDbType.Bit, factStub.IsOMRDetected),
                _databaseManager.BuildSqlParameter("@parmAmount",SqlDbType.Money, factStub.Amount),
                _databaseManager.BuildSqlParameter("@parmAccountNumber", SqlDbType.VarChar, factStub.AccountNumber),
                _databaseManager.BuildSqlParameter("@parmBatchSiteCode", SqlDbType.Int, (object) factStub.BatchSiteCode ?? DBNull.Value)
            };

            ExecuteStoredProcedure("usp_factStubs_Ins", sqlParams);
        }

        public void InsertFactDataEntryDetails(RemittanceDataDto remittanceData)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmBankKey", SqlDbType.Int, remittanceData.BankKey),
                _databaseManager.BuildSqlParameter("@parmClientAccountKey", SqlDbType.Int, remittanceData.ClientAccountKey),
                _databaseManager.BuildSqlParameter("@parmDepositDateKey", SqlDbType.Int, remittanceData.DepositDateKey),
                _databaseManager.BuildSqlParameter("@parmImmutableDateKey", SqlDbType.Int, remittanceData.ImmutableDateKey),
                _databaseManager.BuildSqlParameter("@parmSourceProcessingDateKey", SqlDbType.Int, remittanceData.SourceProcessingDateKey),
                _databaseManager.BuildSqlParameter("@parmBatchID", SqlDbType.BigInt, remittanceData.BatchId),
                _databaseManager.BuildSqlParameter("@parmSourceBatchID", SqlDbType.BigInt, remittanceData.SourceBatchId),
                _databaseManager.BuildSqlParameter("@parmBatchNumber", SqlDbType.BigInt, remittanceData.BatchNumber),
                _databaseManager.BuildSqlParameter("@parmBatchSourceKey", SqlDbType.SmallInt, remittanceData.BatchSourceKey),
                _databaseManager.BuildSqlParameter("@parmPaymentTypeKey", SqlDbType.TinyInt, remittanceData.PaymentTypeKey),
                _databaseManager.BuildSqlParameter("@parmDepositStatus", SqlDbType.Int, remittanceData.DepositStatus),
                _databaseManager.BuildSqlParameter("@parmTransactionID", SqlDbType.Int, remittanceData.TransactionId),
                _databaseManager.BuildSqlParameter("@parmBatchSequence", SqlDbType.Int, remittanceData.BatchSequence),
                _databaseManager.BuildSqlParameter("@parmIsCheck", SqlDbType.Bit, remittanceData.IsCheck),
                _databaseManager.BuildSqlParameter("@parmFieldName", SqlDbType.VarChar, remittanceData.FieldName),
                _databaseManager.BuildSqlParameter("@parmValue", SqlDbType.VarChar, remittanceData.Value)
            };
            ExecuteStoredProcedure("usp_factDataEntryDetails_Ins", sqlParams);
        }

        public void InsertFactTransactionSummary(FactTransactionSummaryDto factTransactionSummaryDto, string transactionExceptionStatus)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmBankKey", SqlDbType.Int, factTransactionSummaryDto.BankKey),
                _databaseManager.BuildSqlParameter("@parmClientAccountKey", SqlDbType.Int, factTransactionSummaryDto.ClientAccountKey),
                _databaseManager.BuildSqlParameter("@parmDepositDateKey", SqlDbType.Int, factTransactionSummaryDto.DepositDateKey),
                _databaseManager.BuildSqlParameter("@parmImmutableDateKey", SqlDbType.Int, factTransactionSummaryDto.ImmutableDateKey),
                _databaseManager.BuildSqlParameter("@parmSourceProcessingDateKey", SqlDbType.Int, factTransactionSummaryDto.SourceProcessingDateKey),
                _databaseManager.BuildSqlParameter("@parmBatchID", SqlDbType.BigInt, factTransactionSummaryDto.BatchId),
                _databaseManager.BuildSqlParameter("@parmSourceBatchID", SqlDbType.BigInt, factTransactionSummaryDto.SourceBatchId),
                _databaseManager.BuildSqlParameter("@parmBatchNumber", SqlDbType.BigInt, factTransactionSummaryDto.BatchNumber),
                _databaseManager.BuildSqlParameter("@parmBatchSourceKey", SqlDbType.VarChar, factTransactionSummaryDto.BatchSourceKey),
                _databaseManager.BuildSqlParameter("@parmPaymentTypeKey", SqlDbType.VarChar, factTransactionSummaryDto.PaymentTypeKey),
                _databaseManager.BuildSqlParameter("@parmTransactionExceptionStatus", SqlDbType.VarChar, transactionExceptionStatus),
                _databaseManager.BuildSqlParameter("@parmBatchCueID", SqlDbType.Int, factTransactionSummaryDto.BatchCueId),
                _databaseManager.BuildSqlParameter("@parmDepositStatus", SqlDbType.VarChar, factTransactionSummaryDto.DepositStatus),
                _databaseManager.BuildSqlParameter("@parmSystemType", SqlDbType.SmallInt, factTransactionSummaryDto.SystemType),
                _databaseManager.BuildSqlParameter("@parmTransactionID", SqlDbType.Int, factTransactionSummaryDto.TransactionId),
                _databaseManager.BuildSqlParameter("@parmTxnSequence", SqlDbType.Int, factTransactionSummaryDto.TxnSequence),
                _databaseManager.BuildSqlParameter("@parmCheckCount", SqlDbType.Int, factTransactionSummaryDto.CheckCount),
                _databaseManager.BuildSqlParameter("@parmScannedCheckCount", SqlDbType.Int, factTransactionSummaryDto.ScannedCheckCount),
                _databaseManager.BuildSqlParameter("@parmStubCount", SqlDbType.Int, factTransactionSummaryDto.StubCount),
                _databaseManager.BuildSqlParameter("@parmDocumentCount", SqlDbType.Int, factTransactionSummaryDto.StubCount),
                _databaseManager.BuildSqlParameter("@parmCheckTotal", SqlDbType.Money, factTransactionSummaryDto.CheckTotal),
                _databaseManager.BuildSqlParameter("@parmStubTotal", SqlDbType.Money, factTransactionSummaryDto.StubTotal),
                _databaseManager.BuildSqlParameter("@parmOMRCount", SqlDbType.Money, factTransactionSummaryDto.OmrCount),
                _databaseManager.BuildSqlParameter("@parmBatchSiteCode", SqlDbType.Int, (object) factTransactionSummaryDto.BatchSiteCode ?? DBNull.Value)
            };

            ExecuteStoredProcedure("usp_factTransactionSummary_Ins", sqlParams);
        }

        public void InsertBatchExceptionStatus(BatchExceptionStatusJson batchExceptionStatus)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmBatchExceptionStatusKey", SqlDbType.SmallInt, (object) batchExceptionStatus.BatchExceptionStatusKey ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmExceptionStatusName", SqlDbType.VarChar, batchExceptionStatus.ExceptionStatusName),
                _databaseManager.BuildSqlParameter("@parmExceptionStatusDescription", SqlDbType.VarChar, batchExceptionStatus.ExceptionStatusDescription)
            };

            ExecuteStoredProcedure("usp_dimBatchExceptionStatuses_Ins", sqlParams);
        }

        public void InsertBatchSource(BatchSourceDto batchSource)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmBatchSourceKey", SqlDbType.SmallInt, (object) batchSource.BatchSourceKey ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmShortName", SqlDbType.VarChar, batchSource.ShortName),
                _databaseManager.BuildSqlParameter("@parmLongName", SqlDbType.VarChar, batchSource.LongName ?? $"Long {batchSource.ShortName}"),
                _databaseManager.BuildSqlParameter("@parmImportType", SqlDbType.VarChar, batchSource.ImportTypeName),
                _databaseManager.BuildSqlParameter("@parmIsActive", SqlDbType.Bit, batchSource.IsActive),
                _databaseManager.BuildSqlParameter("@parmEntityID", SqlDbType.Bit, (object) batchSource.EntityId ?? DBNull.Value)
            };

            ExecuteStoredProcedure("usp_dimBatchSources_Ins", sqlParams);
        }

        public void InsertDepositStatus(DepositStatusDto depositStatus)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmDepositStatusKey", SqlDbType.Int, depositStatus.DepositStatusKey),
                _databaseManager.BuildSqlParameter("@parmDepositStatus", SqlDbType.Int, depositStatus.DepositStatusValue),
                _databaseManager.BuildSqlParameter("@parmDespositDisplayName", SqlDbType.VarChar, depositStatus.DepositDisplayName)
            };

            ExecuteStoredProcedure("usp_dimDepositStatus_Ins", sqlParams);
        }

        public void InsertDimDate(DateTime date)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmDate", SqlDbType.DateTime, date)
            };

            ExecuteStoredProcedure("usp_dimDates_Ins", sqlParams);

        }

        public void InsertImportType(ImportTypeDto importType)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmImportTypeKey", SqlDbType.SmallInt, (object) importType.ImportTypeKey ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmShortName", SqlDbType.VarChar, importType.ShortName),
                _databaseManager.BuildSqlParameter("@parmIsActive", SqlDbType.Bit, importType.IsActive)
            };

            ExecuteStoredProcedure("usp_dimImportTypes_Ins", sqlParams);
        }

        public void InsertPaymentType(PaymentTypeDto paymentType)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmBatchPaymentTypeKey", SqlDbType.TinyInt, (object) paymentType.PaymentTypeKey ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmShortName", SqlDbType.VarChar, paymentType.ShortName),
                _databaseManager.BuildSqlParameter("@parmLongName", SqlDbType.VarChar, paymentType.LongName ?? $"Long {paymentType.ShortName}")
            };

            ExecuteStoredProcedure("usp_dimBatchPaymentTypes_Ins", sqlParams);
        }

        public void InsertSessionClientAccountEntitlements(List<SessionClientAccountEntitlementsDto> sessionEntitlementsDto)
        {
            foreach (var sessionEntitlementDto in sessionEntitlementsDto)
            {

                var sqlParams = new[]
                {
                    _databaseManager.BuildSqlParameter("@parmSessionID", SqlDbType.UniqueIdentifier,
                        sessionEntitlementDto.SessionId),
                    _databaseManager.BuildSqlParameter("@parmStartDateKey", SqlDbType.Int,
                        sessionEntitlementDto.StartDateKey),
                    _databaseManager.BuildSqlParameter("@parmEndDateKey", SqlDbType.Int,
                        sessionEntitlementDto.EndDateKey ?? sessionEntitlementDto.StartDateKey),
                    _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.Int,
                        sessionEntitlementDto.SiteBankId),
                    _databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.Int,
                        sessionEntitlementDto.SiteClientAccountId),
                    _databaseManager.BuildSqlParameter("@parmViewAhead", SqlDbType.Bit,
                        sessionEntitlementDto.ViewAhead),
                    _databaseManager.BuildSqlParameter("@parmViewingDays", SqlDbType.Int,
                        sessionEntitlementDto.ViewAheadDays),
                    _databaseManager.BuildSqlParameter("@parmMaxSearchDays", SqlDbType.Int,
                        sessionEntitlementDto.MaxSearchDays),
                    _databaseManager.BuildSqlParameter("@parmEntityID", SqlDbType.Int, sessionEntitlementDto.EntityId),
                    _databaseManager.BuildSqlParameter("@parmEntityName", SqlDbType.VarChar,
                        sessionEntitlementDto.EntityName ?? $"Entity {sessionEntitlementDto.EntityId}")
                };

                ExecuteStoredProcedure("usp_SessionClientAccountEntitlements_Ins", sqlParams);
            }
        }

        public void InsertTransactionExceptionStatus(TransactionExceptionStatus transactionExceptionStatus)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmTransactionExceptionStatusKey", SqlDbType.SmallInt, (object) transactionExceptionStatus.TransactionExceptionStatusKey ?? DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmExceptionStatusName", SqlDbType.VarChar, transactionExceptionStatus.ExceptionStatusName),
                _databaseManager.BuildSqlParameter("@parmExceptionStatusDescription", SqlDbType.VarChar, transactionExceptionStatus.ExceptionStatusDescription)
            };

            ExecuteStoredProcedure("usp_dimTransactionExceptionStatuses_Ins", sqlParams);
        }

        public void InsertWorkgroupBusinessRule(int siteBankId, int siteClientAccountId, WorkgroupBusinessRuleDto businessRuleDto)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.SmallInt, siteBankId),
                _databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.SmallInt, siteClientAccountId),
                _databaseManager.BuildSqlParameter("@parmInvoiceRequired", SqlDbType.Bit, businessRuleDto.InvoiceRequired),
                _databaseManager.BuildSqlParameter("@parmBalancingRequired", SqlDbType.Bit, businessRuleDto.BalancingRequired),
                _databaseManager.BuildSqlParameter("@parmPDBalancingRequired", SqlDbType.Bit, businessRuleDto.PostDepositBalancingRequired),
                _databaseManager.BuildSqlParameter("@parmPDPayerRequired", SqlDbType.Bit, businessRuleDto.PostDepositPayerRequired)
            };

            ExecuteStoredProcedure("usp_dimWorkgroupBusinessRules_Ins", sqlParams);
        }

        public void InsertWorkgroupDataEntryColumn(DataEntryColumnDto dataEntryColumn)
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.SmallInt, dataEntryColumn.SiteBankId),
                _databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.SmallInt, dataEntryColumn.SiteClientAccountId),
                _databaseManager.BuildSqlParameter("@parmBatchSource", SqlDbType.VarChar, dataEntryColumn.BatchSource),
                _databaseManager.BuildSqlParameter("@parmIsCheck", SqlDbType.Bit, dataEntryColumn.IsCheck),
                _databaseManager.BuildSqlParameter("@parmIsActive", SqlDbType.Bit, dataEntryColumn.IsActive),
                _databaseManager.BuildSqlParameter("@parmIsRequired", SqlDbType.Bit, dataEntryColumn.IsRequired),
                _databaseManager.BuildSqlParameter("@parmMarkSense", SqlDbType.Bit, dataEntryColumn.MarkSense),
                _databaseManager.BuildSqlParameter("@parmDataType", SqlDbType.TinyInt, dataEntryColumn.DataType),
                _databaseManager.BuildSqlParameter("@parmScreenOrder", SqlDbType.Int, dataEntryColumn.ScreenOrder),
                _databaseManager.BuildSqlParameter("@parmUILabel", SqlDbType.VarChar, dataEntryColumn.UiLabel),
                _databaseManager.BuildSqlParameter("@parmFieldName", SqlDbType.VarChar, dataEntryColumn.FieldName),
                _databaseManager.BuildSqlParameter("@parmSourceDisplayName", SqlDbType.VarChar, dataEntryColumn.SourceDisplayName ?? string.Empty)
            };

            ExecuteStoredProcedure("usp_dimWorkgroupDataEntryColumns_Ins", sqlParams);
        }
    }
}
