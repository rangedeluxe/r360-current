﻿using System;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.SchemaClasses
{
    public class RecHubCommon : R360Database
    {
        private IDatabaseManager _databaseManager;
        public RecHubCommon(IDatabaseManager databaseManager, DateTime runDate) : base(databaseManager, runDate)
        {
            _databaseManager = databaseManager;
            //These are required to load static data
            CreateStoredProcedure("usp_WfsRethrowException");
        }
    }
}
