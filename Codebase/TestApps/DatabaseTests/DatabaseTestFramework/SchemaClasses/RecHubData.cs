﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Xml;
using AutoMapper;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.SchemaClasses
{
    public class RecHubData : R360Database
    {
        private readonly IDatabaseManager _databaseManager;
        private static IMapper _mapper;

        public RecHubData(IDatabaseManager databaseManager, DateTime runDate) : base(databaseManager, runDate)
        {
            _databaseManager = databaseManager;
            var recHubConfig = new RecHubConfig(databaseManager, runDate);

            // Create necessary role
            _databaseManager.ExecuteSqlCommand("CREATE ROLE dbRole_RecHubData AUTHORIZATION  dbo;");

            //These are required for all fact tables - some additional tables are needed for specific tables
            CreateTable("dimSiteCodes");
            CreateTable("dimBanks");
            CreateTable("dimOrganizations");
            CreateTable("dimClientAccounts");
            CreateTable("dimBatchSources");
            CreateTable("dimBatchPaymentTypes");
            CreateTable("dimBatchExceptionStatuses");
            CreateTable("dimDates");
            CreateTable("dimDepositStatuses");
            CreateTable("dimDDAs");
            CreateTable("dimImportTypes");
            CreateTable("dimTransactionExceptionStatuses");
            CreateTable("dimWorkgroupDataEntryColumns");

            CreateStoredProcedure("usp_PopulateDatesDimension");

            var testHelper = new DataLoadHelper(this, new RecHubUser(databaseManager, runDate), new IntegrationTest(databaseManager, runDate));
            // load up some defaults
            InsertDimDate(runDate, runDate);
            recHubConfig.LoadStaticDataXml("RecHubData", "dimOrganizations");
            recHubConfig.LoadStaticDataXml("RecHubData", "dimImportTypes");
            recHubConfig.LoadStaticDataXml("RecHubData", "dimBatchExceptionStatuses");
            recHubConfig.LoadStaticDataXml("RecHubData", "dimDepositStatuses");
            recHubConfig.LoadStaticDataXml("RecHubData", "dimSiteCodes");
            //dimBatchSources.xml does not match current format of the database
            testHelper.LoadBatchSourceFile(Path.Combine(_databaseManager.TestDataPath, "DefaultData"), "DefaultBatchSources.json");
            recHubConfig.LoadStaticDataXml("RecHubData", "dimBatchPaymentTypes");
            recHubConfig.LoadStaticDataXml("RecHubData", "dimTransactionExceptionStatuses");

            //Setup mapping for each data access
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<DataRow, PostDePayerDto>()
                    .ForMember(d => d.RoutingNumber, opt => opt.MapFrom(s => s["RoutingNumber"]))
                    .ForMember(d => d.Account, opt => opt.MapFrom(s => s["Account"]))
                    .ForMember(d => d.PayerName, opt => opt.MapFrom(s => s["PayerName"]))
                    .ForMember(d => d.CreatedBy, opt => opt.MapFrom(s => s["CreatedBy"]))
                    .ForMember(d => d.IsDefault, opt => opt.MapFrom(s => s["IsDefault"]));
            });

            _mapper = config.CreateMapper();

        }

        public void InsertDimBank(BankDto bank)
        {
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.CommandType = CommandType.Text;

                var columns = new List<string>() { "SiteBankID", "MostRecent", "EntityID", "CreationDate", "BankName", "ABA" };

                sqlCommand.Parameters.AddWithValue("@SiteBankID", bank.SiteBankId);
                sqlCommand.Parameters.AddWithValue("@MostRecent", bank.MostRecent);
                sqlCommand.Parameters.AddWithValue("@EntityID", (object)bank.EntityId ?? DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@CreationDate", bank.CreationDate);
                sqlCommand.Parameters.AddWithValue("@BankName", bank.BankName ?? $"Bank {bank.SiteBankId}");
                sqlCommand.Parameters.AddWithValue("@ABA", (object)bank.Aba ?? DBNull.Value);

                sqlCommand.CommandText = $"IF NOT EXISTS(SELECT 1 FROM RecHubData.dimBanks WHERE SiteBankID = {bank.SiteBankId}) INSERT INTO RecHubData.dimBanks ({string.Join<string>(",", columns)}) VALUES ({"@" + string.Join<string>(",@", columns)});";
                _databaseManager.ExecuteSqlCommand(sqlCommand);
            }
        }

        public Int16 GetBatchSourceKey(string batchSource)
        {
            var results = new DataTable();

            _databaseManager.ExecuteSqlCommand($"SELECT BatchSourceKey FROM RecHubData.dimBatchSources WHERE ShortName = '{batchSource}';", ref results);

            return (Int16) results.Rows[0].ItemArray[0];
        }

        public Byte GetPaymentTypeKey(string paymentType)
        {
            var results = new DataTable();

            _databaseManager.ExecuteSqlCommand($"SELECT BatchPaymentTypeKey FROM RecHubData.dimBatchPaymentTypes WHERE ShortName = '{paymentType}';", ref results);

            return (Byte) results.Rows[0].ItemArray[0];
        }

        public List<PostDePayerDto> GetPayer(int siteBankId, int siteClientAccountId, string routingNumber, string account)
        {
            var dataTable = new DataTable();
            try
            {
                var parms = new SqlParameter[]
                {
                    _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.Int, siteBankId),
                    _databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountId),
                    _databaseManager.BuildSqlParameter("@parmRoutingNumber", SqlDbType.NVarChar, routingNumber),
                    _databaseManager.BuildSqlParameter("@parmAccount", SqlDbType.NVarChar, account)
                };
                ExecuteStoredProcedure("usp_WorkgroupPayers_Get", parms, out SqlParameter[] returnParms, ref dataTable);
                return dataTable.Rows.Count > 0 ? _mapper.Map<List<PostDePayerDto>>(dataTable.Rows) : new List<PostDePayerDto>();

            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing GetPayer: {ex.Message}", ex);
            }

        }

        public void InsertDimClientAccount(ClientAccountDto clientAccount)
        {
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.CommandType = CommandType.Text;
                var columns = new List<string>()
                {
                    "SiteCodeID", "SiteBankID", "SiteOrganizationID", "SiteClientAccountID", "MostRecent", "IsActive",
                    "CutOff", "OnlineColorMode", "IsCommingled", "DataRetentionDays", "ImageRetentionDays",
                    "CreationDate", "ModificationDate", "SiteClientAccountKey", "ShortName", "LongName", "POBox", "DDA",
                    "FileGroup"
                };
                sqlCommand.Parameters.AddWithValue("@SiteCodeID", clientAccount.SiteCodeId);
                sqlCommand.Parameters.AddWithValue("@SiteBankID", clientAccount.SiteBankId);
                sqlCommand.Parameters.AddWithValue("@SiteOrganizationID", -1);
                sqlCommand.Parameters.AddWithValue("@SiteClientAccountID", clientAccount.SiteClientAccountId);
                sqlCommand.Parameters.AddWithValue("@MostRecent", clientAccount.MostRecent);
                sqlCommand.Parameters.AddWithValue("@IsActive", clientAccount.IsActive);
                sqlCommand.Parameters.AddWithValue("@CutOff", clientAccount.Cutoff);
                sqlCommand.Parameters.AddWithValue("@OnlineColorMode", clientAccount.OnlineColorMode);
                sqlCommand.Parameters.AddWithValue("@IsCommingled", clientAccount.IsCommingled);
                sqlCommand.Parameters.AddWithValue("@DataRetentionDays", clientAccount.DataRetentionDays);
                sqlCommand.Parameters.AddWithValue("@ImageRetentionDays", clientAccount.ImageRetentionDays);
                sqlCommand.Parameters.AddWithValue("@CreationDate", clientAccount.CreationDate);
                sqlCommand.Parameters.AddWithValue("@ModificationDate", clientAccount.ModificationDate);
                sqlCommand.Parameters.AddWithValue("@SiteClientAccountKey", clientAccount.SiteClientAccountKey);
                sqlCommand.Parameters.AddWithValue("@ShortName", clientAccount.ShortName ?? $"Workgroup {clientAccount.ShortName}");
                sqlCommand.Parameters.AddWithValue("@LongName", clientAccount.LongName ?? $"Workgroup Long {clientAccount.LongName}");
                sqlCommand.Parameters.AddWithValue("@POBox", (object)clientAccount.PoBox ?? DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@DDA", (object)clientAccount.DDA ?? DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@FileGroup", (object)clientAccount.FileGroup ?? DBNull.Value);

                sqlCommand.CommandText = $"INSERT INTO RecHubData.dimClientAccounts ({string.Join<string>(",", columns)}) VALUES ({"@" + string.Join<string>(",@", columns)});";
                _databaseManager.ExecuteSqlCommand(sqlCommand);
            }
        }

        public void InsertDimSiteCode(SiteCodeDto siteCode)
        {
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.CommandType = CommandType.Text;
                var columns = new List<string>()
                {
                    "SiteCodeID", "CWDBActiveSite", "LocalTimeZoneBias", "AutoUpdateCurrentProcessingDate",
                    "CurrentProcessingDate", "CreationDate", "ModificationDate", "ShortName", "LongName"
                };

                sqlCommand.Parameters.AddWithValue("@SiteCodeID", siteCode.SiteCodeId);
                sqlCommand.Parameters.AddWithValue("@CWDBActiveSite", siteCode.CwdbActiveSite);
                sqlCommand.Parameters.AddWithValue("@LocalTimeZoneBias", siteCode.LocalTimeZoneBias);
                sqlCommand.Parameters.AddWithValue("@AutoUpdateCurrentProcessingDate", siteCode.AutoUpdateCurrentProcessingDate);
                sqlCommand.Parameters.AddWithValue("@CurrentProcessingDate", (object)siteCode.CurrentProcessingDate ?? DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@ShortName", siteCode.ShortName ?? $"Site {siteCode.SiteCodeId}");
                sqlCommand.Parameters.AddWithValue("@LongName", siteCode.LongName ?? $"LongSite {siteCode.SiteCodeId}");
                sqlCommand.CommandText = $"IF NOT EXISTS(SELECT 1 FROM RecHubData.dimSiteCodes WHERE SiteCodeID = {siteCode.SiteCodeId}) INSERT INTO RecHubData.dimSiteCodes ({string.Join<string>(",", columns)}) VALUES ({"@" + string.Join<string>(",@", columns)});";
                _databaseManager.ExecuteSqlCommand(sqlCommand);
            }
        }

        public bool ExecuteAdvancedSearch(AdvancedSearchParams tableParams, out XmlDocument xmlSearchResults, out DataTable dtSearchResults)
        {
            var dataTable = new DataTable();
            bool bolRetVal = false;

            xmlSearchResults = null;
            try
            {
                var parms = new SqlParameter[]
                {
                    _databaseManager.BuildSqlParameter("@parmAdvancedSearchBaseKeysTable", SqlDbType.Structured, tableParams.BaseRequest),
                    _databaseManager.BuildSqlParameter("@parmAdvancedSearchWhereClauseTable", SqlDbType.Structured, tableParams.WhereClause),
                    _databaseManager.BuildSqlParameter("@parmAdvancedSearchSelectFieldsTable", SqlDbType.Structured, tableParams.SelectFields),
                    _databaseManager.BuildSqlParameter("@parmSearchTotals", SqlDbType.Xml, null, ParameterDirection.Output)
                };
#if OldAdvSearch
                ExecuteStoredProcedure("usp_ClientAccount_Search", parms, out SqlParameter[] returnParms, ref dataTable);
#else
                ExecuteStoredProcedure("usp_AdvancedSearch_Get", parms, out SqlParameter[] returnParms, ref dataTable);
#endif
                if (returnParms != null && returnParms[0] != null && returnParms[0].Value != null)
                {
                    try
                    {
                        xmlSearchResults = new XmlDocument();
                        if (returnParms[0] != null && returnParms[0].Value != null && returnParms[0].Value.ToString().Length > 0)
                        {
                            xmlSearchResults.LoadXml(returnParms[0].Value.ToString());
                            bolRetVal = true;
                        }
                        else
                        {
                            bolRetVal = false; //(Database.lastException == null);
                        }
                    }
                    catch (XmlException)
                    {
                        xmlSearchResults = new XmlDocument();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"An Error occurred while executing ExecuteAdvancedSearch: {ex.Message}", ex);
                    }

                    dtSearchResults = dataTable;

                }
                else
                {
                    //xmlSearchResults = ipoXmlLib.GetXMLBase();
                    dtSearchResults = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing ExecuteAdvancedSearch: {ex.Message}", ex);
            }
            return bolRetVal;
        }

        public bool ExecuteAdvancedSearch(AdvancedSearchRequestDto request, out AdvSearchResults asResults, out DataTable dtResults)
        {
            var bolRetVal = false;

            try
            {
                XmlDocument xmlResults;
                bolRetVal = ExecuteAdvancedSearch(AdvancedSearchRequestDataTableTransformer.ToDataTable(request), out xmlResults, out dtResults);
                asResults = AdvSearchResults.DeserializeFromXML(xmlResults.SelectSingleNode("/Page/RecordSet"));
            }
            catch (Exception ex)
            {
                bolRetVal = false;
                throw new Exception($"An error occurred while executing ExecuteAdvancedSearch: {ex.Message}", ex);
            }
            return bolRetVal;
        }

        public DataTable ExecuteDashboard(Guid sessionId, int depositDateKey, DataTable workgroupTable)
        {
            var dataTable = new DataTable();
            try
            {
                var parms = new SqlParameter[]
                {
                    _databaseManager.BuildSqlParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionId),
                    _databaseManager.BuildSqlParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey),
                    _databaseManager.BuildSqlParameter("@parmWorkgroups", SqlDbType.Structured, workgroupTable)
                };
                ExecuteStoredProcedure("usp_factBatchSummary_Get_ClientAccountSummary_by_SP", parms, out SqlParameter[] returnParms, ref dataTable);
                return dataTable;

            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing ExecuteDashboardDetail: {ex.Message}", ex);
            }
        }

        public GetBatchSummaryResults GetBatchSummary(GetBatchSummaryRequest request)
        {
            var results = new GetBatchSummaryResults();

            try
            {
                var parms = new SqlParameter[]
                {
                    _databaseManager.BuildSqlParameter("@parmSessionID", SqlDbType.UniqueIdentifier, request.SessionId),
                    _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.Int, request.BankId, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.Int, request.ClientAccountId, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmDepositDateStart", SqlDbType.DateTime, request.StartDate, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmDepositDateEnd", SqlDbType.DateTime, request.EndDate, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmPaymentTypeID", SqlDbType.Int, request.PaymentTypeId, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmPaymentSourceID", SqlDbType.Int, request.PaymentSourceId, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmDisplayScannedCheck", SqlDbType.Int, (request.DisplayScannedCheckOnline ? 1 : 0), ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmStart", SqlDbType.Int, request.Start, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmLength", SqlDbType.Int, request.PageLength, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmOrderBy", SqlDbType.VarChar, request.OrderBy, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmOrderDirection", SqlDbType.VarChar, request.OrderDirection, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmSearch", SqlDbType.VarChar, request.Search, ParameterDirection.Input),
                    _databaseManager.BuildSqlParameter("@parmRecordsTotal", SqlDbType.Int, null, ParameterDirection.Output),
                    _databaseManager.BuildSqlParameter("@parmRecordsFiltered", SqlDbType.Int, null, ParameterDirection.Output),
                    _databaseManager.BuildSqlParameter("@parmTransactionRecords", SqlDbType.Int, null, ParameterDirection.Output),
                    _databaseManager.BuildSqlParameter("@parmCheckRecords", SqlDbType.Int, null, ParameterDirection.Output),
                    _databaseManager.BuildSqlParameter("@parmDocumentRecords", SqlDbType.Int, null, ParameterDirection.Output),
                    _databaseManager.BuildSqlParameter("@parmCheckAmount", SqlDbType.Money, null, ParameterDirection.Output)
                };

                ExecuteStoredProcedure("usp_factBatchSummary_Get", parms, out SqlParameter[] returnParms, ref results.BatchSummaryData);
                results.Success = true;
                int.TryParse(returnParms.FirstOrDefault(rp => rp.ParameterName == "@parmRecordsTotal")?.Value.ToString(), out int totalRecords);
                int.TryParse(returnParms.FirstOrDefault(rp => rp.ParameterName == "@parmRecordsFiltered")?.Value.ToString(), out int filteredRecords);
                int.TryParse(returnParms.FirstOrDefault(rp => rp.ParameterName == "@parmTransactionRecords")?.Value.ToString(), out int transactionCount);
                int.TryParse(returnParms.FirstOrDefault(rp => rp.ParameterName == "@parmCheckRecords")?.Value.ToString(), out int checkCount);
                int.TryParse(returnParms.FirstOrDefault(rp => rp.ParameterName == "@parmDocumentRecords")?.Value.ToString(), out int documentCount);
                decimal.TryParse(returnParms.FirstOrDefault(rp => rp.ParameterName == "@parmCheckAmount")?.Value.ToString(), out decimal checkTotal);
                results.TotalRecords = totalRecords;
                results.FilteredRecords = filteredRecords;
                results.TransactionCount = transactionCount;
                results.CheckCount = checkCount;
                results.DocumentCount = documentCount;
                results.CheckTotal = checkTotal;
            }
            catch (Exception ex)
            {
                results.Success = false;
                throw new Exception($"An Error occurred while executing ExecuteDashboardDetail: {ex.Message}", ex);
            }
            return results;
        }

        public void InsertDimDate(DateTime startDate, DateTime endDate)
        {
            var parms = new SqlParameter[]
            {
                _databaseManager.BuildSqlParameter("@parmStartDate", SqlDbType.DateTime, startDate),
                _databaseManager.BuildSqlParameter("@parmEndDate", SqlDbType.DateTime, endDate)
            };
            ExecuteStoredProcedure("usp_PopulateDatesDimension", parms);
//            _integrationTest.InsertDimDate(date);
        }

        public void InsertPayer(int siteBankId, int siteClientAccountId, string routingNumber, string account, string payerName, int userId, string userName, bool isDefault)
        {
            int errorCode = 0;
            int workgroupPayerKey = 0;

            var parms = new[]
            {
                _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.Int, siteBankId, ParameterDirection.Input),
                _databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountId, ParameterDirection.Input),
                _databaseManager.BuildSqlParameter("@parmRoutingNumber", SqlDbType.VarChar, routingNumber, ParameterDirection.Input),
                _databaseManager.BuildSqlParameter("@parmAccount", SqlDbType.VarChar, account, ParameterDirection.Input),
                _databaseManager.BuildSqlParameter("@parmPayerName", SqlDbType.VarChar, payerName, ParameterDirection.Input),
                _databaseManager.BuildSqlParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input),
                _databaseManager.BuildSqlParameter("@parmUserName", SqlDbType.VarChar, userName, ParameterDirection.Input),
                _databaseManager.BuildSqlParameter("@parmIsDefault", SqlDbType.Bit, isDefault, ParameterDirection.Input),
                _databaseManager.BuildSqlParameter("@parmWorkgroupPayerKey", SqlDbType.Int, workgroupPayerKey,
                    ParameterDirection.Output)
            };
            ExecuteStoredProcedure("usp_WorkgroupPayers_Upsert", parms);
        }
    }
}
