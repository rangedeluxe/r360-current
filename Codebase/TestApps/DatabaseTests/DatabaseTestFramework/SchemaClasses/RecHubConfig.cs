﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.SchemaClasses
{
    public class RecHubConfig : R360Database
    {
        private readonly IDatabaseManager _databaseManager;
        public RecHubConfig(IDatabaseManager databaseManager, DateTime runDate) : base(databaseManager, runDate)
        {
            _databaseManager = databaseManager;
            //These are required to load static data
            CreateStoredProcedure("usp_WFS_Process_StaticData");
            CreateFunction("udf_ParseSystemVersion");
        }

        public bool LoadStaticDataXml(string schemaName, string fileName)
        {
            var ret = true;
            XmlDocument staticDataXml = new XmlDocument();

            if (string.IsNullOrEmpty(Path.GetExtension(fileName)))
            {
                fileName += ".xml";
            }

            //use a stream reader to keep from getting an error saying line one is invalid data
            using (var streamReader = new StreamReader(Path.Combine(_databaseManager.ScriptPath, schemaName, "StaticData", fileName)))
            {
                staticDataXml.Load(streamReader);
            }

            try
            {
                var parms = new SqlParameter[]
                {
                    _databaseManager.BuildSqlParameter("@parmStaticData", SqlDbType.Xml, staticDataXml.InnerXml),
                    _databaseManager.BuildSqlParameter("@parmSystemVersion", SqlDbType.VarChar, "99.99.99.99"), //load everything in the fle
                };

                ret = ExecuteStoredProcedure("usp_WFS_Process_StaticData", parms);
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing usp_WFS_Process_StaticData for file {fileName}: {ex.Message}", ex);
            }
            return ret;
        }
    }

}
