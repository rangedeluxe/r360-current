﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.Extensions
{
    public static class ListExtensions
    {
        public static DataTable ToDataTable<T>(this IList<T> request) where T : ITableParam
        {
            var table = new DataTable();

            var properties = typeof(T).GetProperties();
            foreach (var p in properties)
            {
                var column = (p.GetCustomAttributes(typeof(MappingType), false).FirstOrDefault() as MapFromAttribute)
                             ?.Key ?? p.Name;
                table.Columns.Add(column, Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType);
            }

            foreach (var obj in request)
            {
                var row = table.NewRow();
                var objMembers = obj.GetType().GetProperties();
                foreach (var value in objMembers)
                {
                    var column =
                        (value.GetCustomAttributes(typeof(MapFromAttribute), false).FirstOrDefault() as MapFromAttribute
                        )?.Key ?? value.Name;
                    row[column] = value.GetValue(obj) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }

            return table;
        }
    }
}
