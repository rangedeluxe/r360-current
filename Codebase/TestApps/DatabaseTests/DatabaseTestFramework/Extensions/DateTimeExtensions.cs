﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.Extensions
{
    public static class DateTimeExtensions
    {
        public static int ToDateKey(this DateTime date)
        {
            return date.Year * 10000 + date.Month * 100 + date.Day;
        }
    }
}
