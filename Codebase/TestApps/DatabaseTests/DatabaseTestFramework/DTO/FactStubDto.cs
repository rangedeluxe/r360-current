﻿using System;
using System.Collections.Generic;

namespace DatabaseTestFramework.DTO
{
    public class FactStubDto
    {
        public int BankKey { get; set; }
        public int ClientAccountKey { get; set; }
        public int DepositDateKey { get; set; }
        public int ImmutableDateKey { get; set; }
        public int SourceProcessingDateKey { get; set; }
        public Int64 BatchId { get; set; }
        public int SourceBatchId { get; set; }
        public int BatchNumber { get; set; }
        public int BatchSourceKey { get; set; }
        public int PaymentTypeKey { get; set; }
        public int BatchCueId { get; set; }
        public int DepositStatus { get; set; }
        public int SystemType { get; set; } = 0;
        public int TransactionId { get; set; }
        public int TxnSequence { get; set; }
        public int SequenceWithinTransaction { get; set; }
        public int BatchSequence { get; set; }
        public int StubSequence { get; set; }
        public bool IsCorrespondence { get; set; }
        public int DocumentBatchSequence { get; set; }
        public bool IsOMRDetected { get; set; }
        public decimal Amount { get; set; } = 0;
        public string AccountNumber { get; set; } = null;
        public int? BatchSiteCode { get; set; } = null;
        public List<RemittanceDataDto> RemittanceData { get; set; }
    }
}
