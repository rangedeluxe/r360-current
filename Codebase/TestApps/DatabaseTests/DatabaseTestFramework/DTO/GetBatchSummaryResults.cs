﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DatabaseTestFramework.DTO
{
    public class GetBatchSummaryResults
    {
        public DataTable BatchSummaryData = new DataTable();
        public int TotalRecords { get; set; }
        public int FilteredRecords { get; set; }
        public int TransactionCount { get; set; }
        public int CheckCount { get; set; }
        public int DocumentCount { get; set; }
        public decimal CheckTotal { get; set; }
        public bool Success { get; set; }
        public List<string> ErrorList { get; set; } = new List<string>();
    }
}
