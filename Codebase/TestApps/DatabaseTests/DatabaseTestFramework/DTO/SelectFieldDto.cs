﻿using System;
using System.Collections.Generic;
using System.Text;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.DTO
{
    public class SelectFieldDto : ITableParam
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int DataType { get; set; }
        public string ReportTitle { get; set; }
        public int BatchSourceKey { get; set; }

        public static SelectFieldDto FromRequest(AdvancedSearchWhereClauseDto dto)
        {
            return new SelectFieldDto()
            {
                TableName = dto.Table == AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks
                    ? "Checks"
                    : dto.Table == AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs
                        ? "Stubs"
                        : string.Empty,
                FieldName = dto.FieldName,
                DataType = (int)dto.DataType,
                ReportTitle = dto.ReportTitle,
                BatchSourceKey = dto.BatchSourceKey
            };
        }
    }
}
