﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class OLEntitiesDto
    {
        public int UserId { get; set; }
        public int EntityId { get; set; }
        public bool DisplayBatchId { get; set; } = true;
        public int PaymentImageDisplayMode { get; set; } = 0;
        public int DocumentImageDisplayMode { get; set; } = 0;
        public int ViewingDays { get; set; } = 365;
        public int MaximumSearchDays { get; set; } = 365;
        public string EntityName { get; set; } = null;
    }
}
