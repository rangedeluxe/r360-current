﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class BankDto
    {
        public int SiteBankId { get; set; }
        public int? EntityId { get; set; } = null;
        public string BankName { get; set; } = null;
        public string Aba { get; set; } = null;
        public DateTime CreationDate { get; set; } = DateTime.Now;

        public bool MostRecent { get; set; } = true;

        public List<ClientAccountDto> ClientAccounts { get; set; }
    }
}
