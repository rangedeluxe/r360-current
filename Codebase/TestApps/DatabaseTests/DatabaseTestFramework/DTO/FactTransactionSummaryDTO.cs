﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class FactTransactionSummaryDto
    {
        public int BankKey { get; set; }
        public int ClientAccountKey { get; set; }
        public int DepositDateKey { get; set; }
        public int ImmutableDateKey { get; set; }
        public int SourceProcessingDateKey { get; set; }
        public Int64 BatchId { get; set; }
        public int SourceBatchId { get; set; }
        public int BatchNumber { get; set; }
        public int BatchSourceKey { get; set; }
        public int PaymentTypeKey { get; set; }
        public int BatchCueId { get; set; }
        public int DepositStatus { get; set; }
        public int SystemType { get; set; }
        public int TransactionId { get; set; }
        public int TxnSequence { get; set; }
        public int CheckCount { get; set; } = 0;
        public int ScannedCheckCount { get; set; } = 0;
        public int StubCount { get; set; } = 0;
        public int DocumentCount { get; set; } = 0;
        public int OmrCount { get; set; } = 0;
        public decimal CheckTotal { get; set; } = 0;
        public decimal StubTotal { get; set; } = 0;
        public int? BatchSiteCode { get; set; }

    }
}
