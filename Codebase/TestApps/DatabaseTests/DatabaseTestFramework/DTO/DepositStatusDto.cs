﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class DepositStatusDto
    {
        public int DepositStatusKey { get; set; }
        public int DepositStatusValue { get; set; }
        public string DepositDisplayName { get; set; }
    }
}
