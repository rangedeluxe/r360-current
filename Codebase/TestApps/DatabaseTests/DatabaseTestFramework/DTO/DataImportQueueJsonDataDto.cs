﻿using System;
using System.Collections.Generic;
using System.Text;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.DTO
{
    public class DataImportQueueJsonDataDto : ITableParam
    {
        public int AuditDateKey { get; set; }
        public string ClientProcessCode { get; set; }
        public Guid SourceTrackingId { get; set; }
        public Guid BatchTrackingId { get; set; }
        public string JsonDataDocument { get; set; }
    }
}
