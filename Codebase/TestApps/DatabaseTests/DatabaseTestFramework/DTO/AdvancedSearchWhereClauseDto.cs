﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class AdvancedSearchWhereClauseDto
    {
        public enum AdvancedSearchWhereClauseTable
        {
            Checks,
            Stubs,
            None
        }
        public enum AdvancedSearchWhereClauseOperator
        {
            None = 0,
            IsGreaterThan = 1,
            IsLessThan = 2,
            Equals = 3,
            BeginsWith = 4,
            EndsWith = 5,
            Contains = 6
        }

        public enum AdvancedSearchWhereClauseDataType
        {
            String = 1,
            Decimal = 7,
            Float = 6,
            DateTime = 11
        }

        public bool IsDataEntry { get; set; }
        public bool IsStandard { get; set; }
        public string StandardXmlColumnName { get; set; }
        public AdvancedSearchWhereClauseTable Table { get; set; }
        public string FieldName { get; set; }
        public string ReportTitle { get; set; }
        public AdvancedSearchWhereClauseOperator Operator { get; set; }
        public AdvancedSearchWhereClauseDataType DataType { get; set; }
        public string Value { get; set; }
        public int BatchSourceKey { get; set; }
        public string OrderByName { get; set; }
        public bool IsDisplayFieldOnly { get; set; }
        public bool IsDefault { get; set; }

        public static string GetTableName(AdvancedSearchWhereClauseTable table)
        {
            if (table == AdvancedSearchWhereClauseTable.Checks)
                return "Checks";
            if (table == AdvancedSearchWhereClauseTable.Stubs)
                return "Stubs";
            return "None";
        }
    }
}
