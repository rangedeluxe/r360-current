﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class FactCheckDto
    {
        public int BankKey { get; set; }
        public int ClientAccountKey { get; set; }
        public int DepositDateKey { get; set; }
        public int ImmutableDateKey { get; set; }
        public int SourceProcessingDateKey { get; set; }
        public Int64 BatchId { get; set; }
        public int SourceBatchId { get; set; }
        public int BatchNumber { get; set; }
        public int BatchSourceKey { get; set; }
        public int PaymentTypeKey { get; set; }
        public int BatchCueId { get; set; }
        public int DepositStatus { get; set; }
        public int TransactionId { get; set; }
        public int TxnSequence { get; set; }
        public int SequenceWithinTransaction { get; set; }
        public int BatchSequence { get; set; }
        public int CheckSequence { get; set; }
        public Int64 NumericRoutingNumber { get; set; } = 0;
        public Int64 NumericSerial { get; set; } = 0;
        public decimal Amount { get; set; } = 0;
        public int? BatchSiteCode { get; set; } = null;
        public string RoutingNumber { get; set; } = null;
        public string Account { get; set; } = null;
        public string TransactionCode { get; set; } = null;
        public string Serial { get; set; } = null;
        public string RemitterName { get; set; } = null;
        public string ABA { get; set; } = null;
        public string DDA { get; set; } = null;
        public List<RemittanceDataDto> RemittanceData { get; set; }
    }
}
