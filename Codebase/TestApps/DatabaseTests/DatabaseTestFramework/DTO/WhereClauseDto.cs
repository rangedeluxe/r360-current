﻿using System;
using System.Collections.Generic;
using System.Text;
using DatabaseTestFramework.Interfaces;
using DatabaseTestFramework.DTO;

namespace DatabaseTestFramework.DTO
{
    public class WhereClauseDto : ITableParam
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int DataType { get; set; }
        public string ReportTitle { get; set; }
        public string Operator { get; set; }
        public int BatchSourceKey { get; set; }
        public string DataValue { get; set; }

        public static WhereClauseDto FromRequest(AdvancedSearchWhereClauseDto dto)
        {
            return new WhereClauseDto()
            {
                TableName = dto.Table == AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks
                    ? "Checks"
                    : dto.Table == AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs
                        ? "Stubs"
                        : string.Empty,
                FieldName = dto.FieldName,
                DataType = ((int)dto.DataType),
                ReportTitle = dto.ReportTitle,
                Operator = dto.Operator == AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.BeginsWith
                    ? "Begins With"
                    : dto.Operator == AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Contains
                        ? "Contains"
                        : dto.Operator == AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.EndsWith
                            ? "Ends With"
                            : dto.Operator == AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals
                                ? "Equals"
                                : dto.Operator == AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan
                                    ? "Is Greater Than"
                                    : "Is Less Than",
                BatchSourceKey = dto.BatchSourceKey,
                DataValue = dto.Value
            };
        }
    }
}
