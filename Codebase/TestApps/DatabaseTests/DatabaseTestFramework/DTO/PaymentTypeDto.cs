﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class PaymentTypeDto
    {
        public int? PaymentTypeKey { get; set; } = null;
        public string ShortName { get; set; } = null;
        public string LongName { get; set; } = null;
    }
}
