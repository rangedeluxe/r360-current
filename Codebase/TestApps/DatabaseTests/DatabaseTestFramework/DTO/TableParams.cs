﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DatabaseTests.DTO
{
    public class TableParams
    {
        public DataTable BaseRequest { get; set; }
        public DataTable WhereClause { get; set; }
        public DataTable SelectFields { get; set; }
    }
}
