﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class TransactionExceptionStatus
    {
        public int? TransactionExceptionStatusKey { get; set; }
        public string ExceptionStatusName { get; set; }
        public string ExceptionStatusDescription { get; set; }
    }
}
