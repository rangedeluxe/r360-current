﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class AdvancedSearchRequestDto
    {
        public AdvancedSearchRequestDto()
        {
            WhereClauses = new List<AdvancedSearchWhereClauseDto>();
            SelectFields = new List<AdvancedSearchWhereClauseDto>();
            Paginate = true;
            RecordsPerPage = 10;
            MarkSenseOnly = false;
            CotsOnly = false;
            SortBy = "";
            SortByDir = "";
            SortByDisplayName = "";
        }

        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public DateTime DepositDateFrom { get; set; }
        public DateTime DepositDateTo { get; set; }
        public Guid SessionId { get; set; }
        public int RecordsPerPage { get; set; }
        public int StartRecord { get; set; }
        public bool CotsOnly { get; set; }
        public bool MarkSenseOnly { get; set; }
        public List<AdvancedSearchWhereClauseDto> WhereClauses { get; set; }
        public List<AdvancedSearchWhereClauseDto> SelectFields { get; set; }
        public string SortBy { get; set; }
        public string SortByDir { get; set; }
        public string SortByDisplayName { get; set; }
        public int PaymentSourceKey { get; set; }
        public int PaymentTypeKey { get; set; }
        public bool Paginate { get; set; }
        public bool DisplayBatchId { get; set; }
    }
}
