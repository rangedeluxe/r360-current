﻿using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework.DTO
{
    public class WorkgroupListDto : ITableParam
    {
        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
    }
}