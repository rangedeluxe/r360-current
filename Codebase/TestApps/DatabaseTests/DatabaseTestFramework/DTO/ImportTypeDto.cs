﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class ImportTypeDto
    {
        public int? ImportTypeKey { get; set; } = null;
        public bool IsActive { get; set; } = true;
        public string ShortName { get; set; } = null;
    }
}
