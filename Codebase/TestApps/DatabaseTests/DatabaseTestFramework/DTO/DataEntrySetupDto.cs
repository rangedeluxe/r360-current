﻿using System.Collections.Generic;

namespace DatabaseTestFramework.DTO
{
    public class DataEntrySetupDto
    {
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public string BatchSource { get; set; }
        public List<DataEntryColumnDto> DataEntryColumns { get; set; }
    }
}
