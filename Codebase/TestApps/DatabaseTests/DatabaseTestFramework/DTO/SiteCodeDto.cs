﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class SiteCodeDto
    {
        public int SiteCodeId { get; set; }
        public bool CwdbActiveSite { get; set; }
        public int LocalTimeZoneBias { get; set; }
        public bool AutoUpdateCurrentProcessingDate { get; set; }
        public DateTime? CurrentProcessingDate { get; set; }
        public string ShortName { get; set; } = null;
        public string LongName { get; set; } = null;
    }
}
