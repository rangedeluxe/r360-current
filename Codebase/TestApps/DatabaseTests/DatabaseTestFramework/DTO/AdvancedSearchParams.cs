﻿using System.Data;

namespace DatabaseTestFramework.DTO
{
    public class AdvancedSearchParams
    {
        public DataTable BaseRequest { get; set; }
        public DataTable WhereClause { get; set; }
        public DataTable SelectFields { get; set; }
    }
}
