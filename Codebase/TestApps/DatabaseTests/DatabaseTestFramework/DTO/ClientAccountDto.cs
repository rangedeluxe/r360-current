﻿using System;
using System.Collections.Generic;

namespace DatabaseTestFramework.DTO
{
    public class ClientAccountDto
    {
        public int SiteCodeId { get; set; }
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public bool MostRecent { get; set; }
        public bool IsActive { get; set; }
        public bool IsCommingled { get; set; }
        public bool Cutoff { get; set; }
        public int OnlineColorMode { get; set; }
        public int DataRetentionDays { get; set; }
        public int ImageRetentionDays { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public DateTime ModificationDate { get; set; } = DateTime.Now;
        public Guid SiteClientAccountKey { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public string PoBox { get; set; }
        public string DDA { get; set; }
        public string FileGroup { get; set; }
        public List<DataEntrySetupDto> DataEntrySetups { get; set; }
        public OlWorkgroupDto OlWorkgroup { get; set; }
        public WorkgroupBusinessRuleDto WorkgroupBusinessRule { get; set; }
        //public AuditInformationDto AuditInformation { get; set; }
        public List<int> FiList { get; set; }


    }
}
