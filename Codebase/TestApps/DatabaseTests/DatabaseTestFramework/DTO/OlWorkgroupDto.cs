﻿namespace DatabaseTestFramework.DTO
{
    public class OlWorkgroupDto
    {
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public int? EntityId { get; set; } = null;
        public int? ViewingDays { get; set; } = null;
        public int? MaximumSearchDays { get; set; }
        public int CheckImageDisplayMode { get; set; }
        public int DocumentImageDisplayMode { get; set; }
        public bool Hoa { get; set; } = false;
        public bool? DisplayBatchId { get; set; }
        public int InvoiceBalancingOption { get; set; }
        public string BillingAccount { get; set; }
        public string BillingField1 { get; set; }
        public string BillingField2 { get; set; }
    }
}
