﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;

namespace DatabaseTestFramework.DTO
{
    [Serializable, XmlRoot("RecordSet")]
    public class AdvSearchResults
    {
        public enum AdvSearchResultsSelectFieldDataType
        {
            [XmlEnum("1")] String = 1,
            [XmlEnum("6")] Float = 6,
            [XmlEnum("7")] Decimal = 7,
            [XmlEnum("11")] DateTime = 11
        }

        [XmlType("field")]
        public class AdvSearchResultsSelectField
        {
            [XmlAttribute("tablename")]
            public string TableName { get; set; } = string.Empty;

            [XmlAttribute("fieldname")]
            public string FieldName { get; set; } = string.Empty;

            [XmlAttribute("datatype")]
            public AdvSearchResultsSelectFieldDataType DataType { get; set; } = AdvSearchResultsSelectFieldDataType.String;

            [XmlAttribute("reporttitle")]
            public string ReportTitle { get; set; } = string.Empty;

            [XmlAttribute("ColID")]
            public string ColID { get; set; } = string.Empty;
        }

        [XmlAttribute]
        public int CheckCount { get; set; } = 0;

        [XmlAttribute]
        public decimal CheckTotal { get; set; } = 0;

        [XmlAttribute]
        public int DocumentCount { get; set; } = 0;

        [XmlAttribute]
        public int TotalRecords { get; set; } = 0;

        [XmlIgnore]
        public List<AdvSearchResultsSelectField> SelectFields { get; set; } = new List<AdvSearchResultsSelectField>();

        [XmlArray("SelectFields"), XmlArrayItem("field")]
        public AdvSearchResultsSelectField[] _selectfields { get; set; }

        public static AdvSearchResults DeserializeFromXML(XmlNode xmlNode)
        {
            AdvSearchResults results;

            try
            {
                var serializer = new XmlSerializer(typeof(AdvSearchResults),"");
                XmlReader reader = new XmlNodeReader(xmlNode);
                results = (AdvSearchResults)serializer.Deserialize(reader);
                results.SelectFields = results._selectfields?.ToList<AdvSearchResultsSelectField>();
            }
            catch (Exception ex)
            {
                results = new AdvSearchResults();
            }
            return results;
        }
    }
}
