﻿using System;

namespace DatabaseTestFramework.DTO
{
    public class FactBatchSummaryDto
    {
        public int BankKey { get; set; }
        public int ClientAccountKey { get; set; }
        public int DepositDateKey { get; set; }
        public int ImmutableDateKey { get; set; }
        public int SourceProcessingDateKey { get; set; }
        public Int64 BatchId { get; set; }
        public int SourceBatchId { get; set; }
        public int BatchNumber { get; set; }
        public int BatchSourceKey { get; set; }
        public int PaymentTypeKey { get; set; }
        public int BatchExceptionStatusKey { get; set; }
        public int BatchCueId { get; set; }
        public int DepositStatus { get; set; }
        public int SystemType { get; set; }
        public int DepositStatusKey { get; set; }
        public int? BatchSiteCode { get; set; }
    }
}
