﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class GetBatchSummaryRequest
    {
        public Guid SessionId { get; set; }
        public int BankId { get; set; }
        public int ClientAccountId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool DisplayScannedCheckOnline { get; set; }
        public int Start { get; set; }
        public int PageLength { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Search { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? PaymentSourceId { get; set; }
    }
}
