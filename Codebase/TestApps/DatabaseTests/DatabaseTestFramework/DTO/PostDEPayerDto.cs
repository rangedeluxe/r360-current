﻿namespace DatabaseTestFramework.DTO
{
    public class PostDePayerDto
    {
        //public int SiteBankId { get; set; }
        //public int SiteClientAccountId { get; set; }
        public string RoutingNumber { get; set; }
        public string Account { get; set; }
        public string PayerName { get; set; }
        public string CreatedBy { get; set; }
        public bool IsDefault { get; set; }
    }
}
