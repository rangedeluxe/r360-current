﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class DataEntryColumnDto
    {
        public enum DataTypes
        {
            Alphanumeric = 1,
            Float = 6,
            Money = 7,
            DateTime = 11
        }
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public string BatchSource { get; set; }
        public bool IsCheck { get; set; }
        public bool IsActive { get; set; }
        public bool IsRequired { get; set; }
        public bool MarkSense { get; set; }
        public DataTypes DataType { get; set; }
        public int ScreenOrder { get; set; }
        public string FieldName { get; set; }
        public string UiLabel { get; set; }
        public string SourceDisplayName { get; set; }
    }
}
