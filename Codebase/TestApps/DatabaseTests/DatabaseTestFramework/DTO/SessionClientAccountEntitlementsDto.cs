﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class SessionClientAccountEntitlementsDto
    {
        public Guid SessionId { get; set; }
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public int StartDateKey { get; set; }
        public int? EndDateKey { get; set; }
        public bool ViewAhead { get; set; } = true;
        public int ViewAheadDays { get; set; } = 180;
        public int MaxSearchDays { get; set; } = 180;
        public int EntityId { get; set; } = 1;
        public string EntityName { get; set; }
    }
}
