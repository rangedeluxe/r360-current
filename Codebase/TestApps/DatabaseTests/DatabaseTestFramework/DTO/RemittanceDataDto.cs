﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework.DTO
{
    public class RemittanceDataDto
    {
        public int BankKey { get; set; }
        public int ClientAccountKey { get; set; }
        public int DepositDateKey { get; set; }
        public int ImmutableDateKey { get; set; }
        public int SourceProcessingDateKey { get; set; }
        public Int64 BatchId { get; set; }
        public int SourceBatchId { get; set; }
        public int BatchNumber { get; set; }
        public int BatchSourceKey { get; set; }
        public int PaymentTypeKey { get; set; }
        public int DepositStatus { get; set; }
        public int TransactionId { get; set; }
        public int BatchSequence { get; set; }
        public bool IsCheck { get; set; } = true;
        public string FieldName { get; set; }
        public string Value { get; set; }
    }
}
