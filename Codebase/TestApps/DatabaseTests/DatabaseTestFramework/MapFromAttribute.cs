﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestFramework
{
    public class MapFromAttribute : Attribute
    {
        public string Key { get; set; }

        public MapFromAttribute(string key)
        {
            Key = key;
        }
    }
}
