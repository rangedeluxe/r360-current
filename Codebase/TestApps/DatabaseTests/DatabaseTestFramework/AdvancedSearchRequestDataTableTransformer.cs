﻿using DatabaseTestFramework.DTO;
using DatabaseTests.DTO;
using System.Collections.Generic;
using System.Linq;
using DatabaseTestFramework.Extensions;

namespace DatabaseTestFramework
{
    public class AdvancedSearchRequestDataTableTransformer
    {
        public static AdvancedSearchParams ToDataTable(AdvancedSearchRequestDto request)
        {
            var result = new AdvancedSearchParams();

            result.BaseRequest = new List<BaseRequestDto>()
                    { BaseRequestDto.FromRequest(request) }
                .ToDataTable();

            result.WhereClause = request.WhereClauses
                .Where(w => w.IsDataEntry)
                .Select(w => WhereClauseDto.FromRequest(w))
                .ToList()
                .ToDataTable();

            result.SelectFields = request.SelectFields
                .Select(s => SelectFieldDto.FromRequest(s))
                .ToList()
                .ToDataTable();

            return result;
        }
    }
}
