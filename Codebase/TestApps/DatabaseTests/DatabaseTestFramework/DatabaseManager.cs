﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using DatabaseTestFramework.Interfaces;
using Microsoft.Extensions.Configuration;

namespace DatabaseTestFramework
{
    public class DatabaseManager : IDatabaseManager
    {
        private readonly IConfigurationRoot _configuration;
        private SqlConnection _sqlConnection;
        private string _creatorConnectionString;
        private string _databaseName;
        private readonly bool _partitioned = false;
        private int _timeOut = 26000;
        private string _machineName = Environment.MachineName.Replace('-', '_');
        private string _userConnectionString;

        public string ScriptPath { get; }
        public string TestDataPath { get; }

        public enum DatabaseUser
        {
            Creator,
            User
        };

        public enum ScriptTypes
        {
            DataType,
            Function,
            Schema,
            Sequence,
            StoredProcedure,
            Table,
            View
        };

        //Not sure if we should read the values for a config file here or have the creator of the class pass in an IConfiguration object.
        public DatabaseManager(string appSettings)
        {
            try
            {
                _configuration = new ConfigurationBuilder().SetBasePath(AppContext.BaseDirectory).AddJsonFile(appSettings).Build();
                ScriptPath = _configuration["ScriptPath"];
                TestDataPath = _configuration["TestDataPath"];
                _partitioned = string.Equals(_configuration["Partitioned"], "true",
                    StringComparison.CurrentCultureIgnoreCase);
                _creatorConnectionString = _configuration["CreatorConnectionString"];
                _userConnectionString = _configuration["UserConnectionString"];
            }
            catch (Exception ex)
            {
                throw new Exception($"Error reading configuration file. {ex.Message}", ex);
            }
        }


        private void DropOldDatabase(string testName)
        {
            var databaseList = new List<string>();

            try
            {
                var sqlCommand = new SqlCommand
                {
                    Connection = _sqlConnection,
                    CommandType = CommandType.Text,
                    CommandText =
                        $"SELECT [name] FROM sys.databases where [name] LIKE 'IntegrationTests_{_machineName}_{testName}%' order by create_date;"
                };
                var sqlReader = sqlCommand.ExecuteReader();
                // We could issue a alter database and set to single user mode, but for now lets not use a sledgehammer. There might be a good
                // reason the database is in use....like someone is reviewing results of a test
                // We might want to add a param at some point that says how many old databases to keep
                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        databaseList.Add(sqlReader.GetString(sqlReader.GetOrdinal("name")));
                    }

                    sqlReader.Close();

                    foreach (var database in databaseList)
                    {
                        var dropCommand = new SqlCommand
                        {
                            Connection = _sqlConnection,
                            CommandType = CommandType.Text,
                            CommandText =
                                $"DROP DATABASE {database}"
                        };
                        try
                        {
                            dropCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (ex is SqlException)
                            {
                                // ignore a database in use error and continue deleting
                                if (!ex.Message.EndsWith("because it is currently in use."))
                                    throw new Exception(
                                        $"Error dropping database {ex.Message}", ex);
                            }
                            else
                            {
                                throw new Exception(
                                    $"Error dropping database {ex.Message}", ex);
                            }
                        }
                    }
                }
                else
                    sqlReader.Close();

            }
            catch (Exception ex)
            {
                throw new Exception(
                    $"Error dropping database {ex.Message}", ex);
            }

        }

        private void ExecuteSqlStatement(string sqlStatement)
        {

            try
            {

                using (var sqlCommand = new SqlCommand())
                {
                    sqlCommand.CommandTimeout = _timeOut;
                    var sqlCommands = sqlStatement.Split(new[] {"GO\r\n", "GO ", "GO\t"},
                        StringSplitOptions.RemoveEmptyEntries);
                    foreach (var sql in sqlCommands)
                    {
                        sqlCommand.CommandText = sql;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.Connection = _sqlConnection;
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exception)
            {
                if (exception is SqlException)
                {
                    var sqlException = exception as SqlException;
                    throw new Exception(
                        $"Error executing SQL statement {sqlException.LineNumber} {sqlException.Number} {sqlException.Message}", sqlException);
                }
                else
                {
                    throw new Exception(
                        $"Error executing SQL statement {exception.Message}", exception);
                }
            }
        }

        private void ExecuteScriptFile(ScriptTypes scriptType, string scriptName)
        {
            var sqlScript = File.ReadAllText(Path.Combine(ScriptPath, scriptType.ToString(), $"{scriptName}.sql"));
            if (!_partitioned)
                sqlScript = IgnorePartitions(sqlScript);
            ExecuteSqlStatement(sqlScript);
        }

        private void ExecuteScriptFile(ScriptTypes scriptType, string schemaName, string scriptName)
        {
            var sqlScript = File.ReadAllText(Path.Combine(ScriptPath, schemaName, scriptType.ToString(), $"{scriptName}.sql"));
            if (!_partitioned)
                sqlScript = IgnorePartitions(sqlScript);
            ExecuteSqlStatement(sqlScript);
        }

        private string IgnorePartitions(string sql)
        {
            return sql.Replace("$(OnDataPartition)", "").Replace("$(OnAuditPartition)", "").Replace("ON DataImport (AuditDateKey)", "").Replace("ON Sessions (CreationDateKey)", "").Replace("$(OnDataImportPartition)","");
        }

        public void CreateDatabase(string testName)
        {
            Connect(DatabaseUser.Creator, "master");
            // drop all old databases before we create a new one
            DropOldDatabase(testName);

            _databaseName = $"IntegrationTests_{_machineName}_{testName}_{DateTime.Now:yyyyMMddHHmmss}";

            var sqlCommand = new SqlCommand
            {
                Connection = _sqlConnection,
                CommandType = CommandType.Text,
                CommandText = $"CREATE DATABASE {_databaseName};"
            };

            sqlCommand.ExecuteNonQuery();
            Disconnect();
            Connect(DatabaseUser.Creator, _databaseName);
        }

        public void Connect(Enum databaseUser, string databaseName)
        {

            var connectionInfo = new SqlConnectionStringBuilder();
            connectionInfo.ConnectionString = _configuration[$"{databaseUser.ToString()}ConnectionString"];
            connectionInfo.InitialCatalog = databaseName;

            _sqlConnection = new SqlConnection { ConnectionString = connectionInfo.ConnectionString };

            _sqlConnection.Open();
        }

        public void Disconnect()
        {
            _sqlConnection.Close();
        }

        public void ExecuteSqlCommand(SqlCommand sqlCommand)
        {
            sqlCommand.Connection = _sqlConnection;
            sqlCommand.ExecuteNonQuery();
        }

        public void ExecuteSqlCommand(string sqlStatement)
        {
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = sqlStatement;
                sqlCommand.Connection = _sqlConnection;

                sqlCommand.ExecuteNonQuery();
            }
        }

        public void ExecuteSqlCommand(string sqlStatement, ref DataTable dataTable)
        {
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = sqlStatement;
                sqlCommand.Connection = _sqlConnection;

                var dataAdapter = new SqlDataAdapter(sqlCommand);
                dataAdapter.Fill(dataTable);
            }
        }

        public void CreateDataType(string schemaName, string dataTypeName)
        {
            ExecuteScriptFile(ScriptTypes.DataType, schemaName, dataTypeName);
        }

        public void CreateFunction(string schemaName, string functionName)
        {
            ExecuteScriptFile(ScriptTypes.Function, schemaName, functionName);
        }

        public void CreateSchema(string schemaName)
        {
            ExecuteScriptFile(ScriptTypes.Schema, $"{schemaName}Schema");
        }

        public void CreateSequence(string schemaName, string sequenceName)
        {
            ExecuteScriptFile(ScriptTypes.Sequence, schemaName, sequenceName);
        }

        public void CreateStoredProcedure(string schemaName, string storedProcedureName)
        {
            ExecuteScriptFile(ScriptTypes.StoredProcedure, schemaName, storedProcedureName);
        }

        public void CreateTable(string schemaName, string tableName)
        {
            ExecuteScriptFile(ScriptTypes.Table, schemaName, tableName);
        }

        public void CreateView(string schemaName, string viewName)
        {
            ExecuteScriptFile(ScriptTypes.View, schemaName, viewName);
        }

        public SqlParameter BuildSqlParameter(string parameterName, SqlDbType type, object value, ParameterDirection direction = ParameterDirection.Input)
        {
            SqlParameter objParameter = new SqlParameter();
            int intSize = 0;
            int i;

            objParameter.ParameterName = parameterName;
            objParameter.SqlDbType = type;
            objParameter.Value = value;
            objParameter.Direction = direction;

            if (type == SqlDbType.Xml)
            {
                if (value == null)
                {
                    objParameter.Size = 4096;
                }
                else
                {
                    i = 10;
                    intSize = 2 ^ i;
                    while (intSize < value.ToString().Length)
                    {
                        ++i;
                        intSize = 2 ^ i;
                    }
                    objParameter.Size = intSize;
                }
            }
            else if (type == SqlDbType.VarChar)
            {
                objParameter.Size = 1024;
            }

            return (objParameter);
        }

        public bool ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameter)
        {
            var dataTable = new DataTable();
            dataTable = null;
            return (ExecuteStoredProcedure(storedProcedureName, sqlParameter, out SqlParameter[] sqlOutSqlParameters, ref dataTable));
        }

        public bool ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameter, ref DataTable dataTable)
        {
            return (ExecuteStoredProcedure(storedProcedureName, sqlParameter, out SqlParameter[] sqlOutSqlParameters, ref dataTable));
        }
        public bool ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameter, out SqlParameter[] sqlOutSqlParameters)
        {
            var dataTable = new DataTable();
            dataTable = null;
            var results =ExecuteStoredProcedure(storedProcedureName, sqlParameter, out SqlParameter[] sqlOutParameters, ref dataTable);
            sqlOutSqlParameters = sqlOutParameters;
            return results;
        }

        public bool ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameters, out SqlParameter[] sqlOutSqlParameters, ref DataTable dataTable)
        {
            var success = false;

            SqlParameter[] returnParams = null;

            try
            {
                var sqlCommand = new SqlCommand
                {
                    Connection = _sqlConnection,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = storedProcedureName
                };

                foreach (var sqlParameter in sqlParameters)
                    sqlCommand.Parameters.Add(sqlParameter);

                try
                {
                    if (dataTable == null)
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    else
                    {
                        var dataSet = new DataSet();
                        var dataAdapter = new SqlDataAdapter
                        {
                            SelectCommand = sqlCommand
                        };

                        dataAdapter.Fill(dataSet);
                        if (dataSet?.Tables.Count > 0)
                        {
                            dataTable = dataSet.Tables[0];
                        }
                        else dataTable = null;

                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(
                        $"An Error occurred while executing Procedure {storedProcedureName}: {ex.Message}", ex);
                }

                returnParams = new SqlParameter[sqlParameters.Count(p => p.Direction == ParameterDirection.Output || p.Direction == ParameterDirection.InputOutput)];
                if (returnParams.Length > 0)
                {
                    var paramName = sqlParameters.FirstOrDefault(p =>
                        p.Direction == ParameterDirection.Output || p.Direction == ParameterDirection.InputOutput);
                    var start = sqlParameters.Select((value, index) => new { value, index }).Single(p => p.value.ParameterName == paramName.ParameterName);
                    //sqlCommand.Parameters.CopyTo(returnParams, start.index);
                    for (var index = start.index; index <= sqlCommand.Parameters.Count-1; index++ )
                    {
                        returnParams[index - start.index] = sqlCommand.Parameters[index];
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing Procedure {storedProcedureName}: {ex.Message}",
                    ex);
            }
            finally
            {
                sqlOutSqlParameters = returnParams;
                success = true;
            }

            return success;
        }
    }
}
