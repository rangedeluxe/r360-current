﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using DatabaseTestFramework.Interfaces;

namespace DatabaseTestFramework
{
    public class R360Database
    {
        private readonly IDatabaseManager _databaseManager;
        private DateTime _runDate;
        private readonly string _schemaName;

        protected R360Database(IDatabaseManager databaseManager, DateTime runDate)
        {
            _databaseManager = databaseManager;
            _runDate = runDate;
            _schemaName = GetType().Name;
            _databaseManager.CreateSchema(_schemaName);
        }

        public void CreateDataType(string dataTypeName)
        {
            _databaseManager.CreateDataType(_schemaName, dataTypeName);
        }
        public void CreateFunction(string functionName)
        {
            _databaseManager.CreateFunction(_schemaName, functionName);
        }

        public void CreateSequence(string sequenceName)
        {
            _databaseManager.CreateSequence(_schemaName, sequenceName);
        }

        public void CreateStoredProcedure(string storedProcedureName)
        {
            _databaseManager.CreateStoredProcedure(_schemaName, storedProcedureName);
        }

        public void CreateTable(string tableName)
        {
            var results = new DataTable();
            results.Columns.Add("Exists", typeof(int));

            _databaseManager.ExecuteSqlCommand($"IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{_schemaName}' AND TABLE_NAME = '{tableName}') SELECT 1 AS [Exists] ELSE SELECT 0 AS [Exists]", ref results);
            if ((int)results.Rows[0].ItemArray[0] == 0)
                _databaseManager.CreateTable(_schemaName, tableName);
        }

        public void CreateView(string viewName)
        {
            _databaseManager.CreateView(_schemaName, viewName);
        }

        public bool ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameters)
        {
            return _databaseManager.ExecuteStoredProcedure($"{_schemaName}.{storedProcedureName}", sqlParameters);
        }

        public void ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameters, ref DataTable dataTable)
        {
            _databaseManager.ExecuteStoredProcedure($"{_schemaName}.{storedProcedureName}", sqlParameters, ref dataTable);
        }

        public void ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameters, out SqlParameter[] sqlOutSqlParameters)
        {
            _databaseManager.ExecuteStoredProcedure($"{_schemaName}.{storedProcedureName}", sqlParameters, out sqlOutSqlParameters);
        }
        public void ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameters, out SqlParameter[] sqlOutSqlParameters, ref DataTable dataTable)
        {
            _databaseManager.ExecuteStoredProcedure($"{_schemaName}.{storedProcedureName}", sqlParameters, out sqlOutSqlParameters, ref dataTable);
        }
    }
}
