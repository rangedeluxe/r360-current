﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DatabaseTestFramework.Interfaces
{
    public interface IDatabaseManager
    {
        string ScriptPath { get; }
        string TestDataPath { get; }
        void CreateDatabase(string testName);
        void Connect(Enum databaseUser, string databaseName);
        void Disconnect();
        void ExecuteSqlCommand(SqlCommand sqlCommand);
        void ExecuteSqlCommand(string sqlStatement);
        void ExecuteSqlCommand(string sqlStatement, ref DataTable dataTable);
        void CreateDataType(string schemaName, string dataTypeName);
        void CreateFunction(string schemaName, string functionName);
        void CreateSchema(string schemaName);
        void CreateSequence(string schemaName, string sequenceName);
        void CreateStoredProcedure(string schemaName, string storedProcedureName);
        void CreateTable(string schemaName, string tableName);
        void CreateView(string schemaName, string viewName);
        SqlParameter BuildSqlParameter(string parameterName, SqlDbType type, object value,
            ParameterDirection direction = ParameterDirection.Input);
        bool ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameter);
        bool ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameter, ref DataTable dataTable);
        bool ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameter,
            out SqlParameter[] sqlOutSqlParameters);
        bool ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] sqlParameters,
            out SqlParameter[] sqlOutSqlParameters, ref DataTable dataTable);


    }
}
