﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTests.DTO
{
    public class WorkgroupBusinessRuleTestDto
    {
        public bool InvoiceRequired { get; set; } = false;
        public bool BalancingRequired { get; set; } = false;
        public bool PostDepositBalancingRequired { get; set; } = false;
        public bool PostDepositPayerRequired { get; set; } = false;
    }
}
