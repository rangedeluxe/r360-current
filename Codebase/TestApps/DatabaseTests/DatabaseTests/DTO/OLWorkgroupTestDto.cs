﻿namespace DatabaseTests.DTO
{
    public class OlWorkgroupTestDto
    {
        public int? EntityId { get; set; } = null;
        public int? ViewingDays { get; set; } = null;
        public int? MaximumSearchDays { get; set; } = null;
        public int CheckImageDisplayMode { get; set; }
        public int DocumentImageDisplayMode { get; set; }
        public bool Hoa { get; set; } = false;
        public bool? DisplayBatchId { get; set; }
        public int InvoiceBalancingOption { get; set; }
        public string BillingAccount { get; set; } = null;
        public string BillingField1 { get; set; } = null;
        public string BillingField2 { get; set; } = null;
    }
}
