﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace DatabaseTests.DTO
{
    public class DataImportQueueJsonBatch
    {
        public DateTime DepositDate { get; set; } = DateTime.Now;
        public DateTime BatchDate { get; set; } = DateTime.Now;
        public DateTime ProcessingDate { get; set; } = DateTime.Now;
        public int BankId { get; set; } = 0;
        public int ClientId { get; set; } = 0;
        public int BatchId { get; set; } = 0;
        public int BatchNumber { get; set; } = 0;
        public int BatchSiteCode { get; set; } = 0;
        public string BatchSource { get; set; }
        public string PaymentType { get; set; } = "ACH";
        public int BatchCueID { get; set; } = 0;
        public string BatchTrackingID { get; set; }
        public string ABA { get; set; }
        public string DDA { get; set; }
        public string FileHash { get; set; }
        public string FileSignature { get; set; }
    }
}
