﻿using System;
using System.Collections.Generic;
using System.Text;
using DatabaseTestFramework.DTO;

namespace DatabaseTests.DTO
{
    public class ClientAccountTestDto
    {
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public bool MostRecent { get; set; } = true;
        public bool IsActive { get; set; } = true;
        public bool Cutoff { get; set; } = true;
        public int OnlineColorMode { get; set; } = 0;
        public int DataRetentionDays { get; set; } = 999;
        public int ImageRetentionDays { get; set; } = 999;
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public DateTime ModificationDate { get; set; } = DateTime.Now;
        public Guid SiteClientAccountKey { get; set; } = new Guid();
        public string ShortName { get; set; } = null;
        public string LongName { get; set; } = null;
        public string PoBox { get; set; } = null;
        public string Dda { get; set; } = null;
        public string FileGroup { get; set; } = null;
        public OlWorkgroupTestDto OlWorkgroup { get; set; }
        public WorkgroupBusinessRuleDto WorkgroupBusinessRule { get; set; }
        public AuditInformationTestDto AuditInformation { get; set; }
        public List<int> FiList { get; set; }
    }
}
