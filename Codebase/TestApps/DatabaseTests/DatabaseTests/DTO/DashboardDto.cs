﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTests.DTO
{
    public class DashboardDto
    {
        public string EntityName { get; set; }
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public int EntityId { get; set; }
        public string WorkgroupName { get; set; }
        public string BatchSource { get; set; }
        public int OlWorkgroupId { get; set; }
        public int BatchSourceId { get; set; }
        public string PaymentType { get; set; }
        public int PaymentTypeId { get; set; }
        public int BatchCount { get; set; }
        public string Dda { get; set; }
        public int TransactionCount { get; set; }
        public int PaymentCount { get; set; }
        public double PaymentTotal { get; set; }
    }
}
