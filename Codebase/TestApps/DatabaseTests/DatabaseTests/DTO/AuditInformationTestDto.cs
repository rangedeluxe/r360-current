﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTests.DTO
{
    public class AuditInformationTestDto
    {
        public int UserId { get; set; }
        public string UserDisplayName { get; set; }
        public string EntityName { get; set; } = null;
    }
}
