﻿namespace DatabaseTests.Extensions
{
    public static class StringExtension
    {
        public static string NullIfWhiteSpace(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return null;
            return value;
        }
    }
}
