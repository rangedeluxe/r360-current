﻿using System;
using System.IO;
using DatabaseTestFramework;
using DatabaseTestFramework.Extensions;
using DatabaseTestFramework.Interfaces;
using DatabaseTestFramework.SchemaClasses;
using DatabaseTestFramework.DTO;

namespace DatabaseTests.CommonObjects
{
    public class AdvancedSearchDataContext
    {
        public IntegrationTest IntegrationTest { get; set; }
        public RecHubAudit RecHubAudit { get; set; }
        public RecHubCommon RecHubCommon { get; set; }
        public RecHubConfig RecHubConfig { get; set; }
        public RecHubData RecHubData { get; set; }
        public RecHubSystem RecHubSystem { get; set; }
        public RecHubUser RecHubUser { get; set; }
        public DataLoadHelper TestHelper { get; set; }
        public IDatabaseManager DatabaseManager { get; set; }
        public DateTime RunDate { get; set; }

        public AdvancedSearchDataContext(string AppSettingsFile, string DatabaseName)
        {
            DatabaseManager = new DatabaseManager(AppSettingsFile);
            RunDate = DateTime.Now;
            DatabaseManager.CreateDatabase(DatabaseName);
            RecHubCommon = new RecHubCommon(DatabaseManager, RunDate);
            RecHubSystem = new RecHubSystem(DatabaseManager, RunDate);
            IntegrationTest = new IntegrationTest(DatabaseManager, RunDate);
            RecHubData = new RecHubData(DatabaseManager, RunDate);
            RecHubUser = new RecHubUser(DatabaseManager, RunDate);
            TestHelper = new DataLoadHelper(RecHubData, RecHubUser, IntegrationTest);
            CreateDatabaseObjects();
        }

        public void LoadBatches(string BatchFileNameSpec)
        {
            TestHelper.LoadBatches(Path.Combine(DatabaseManager.TestDataPath, "AdvancedSearch", "Batches"), BatchFileNameSpec);
        }

        public void InsertSessionClientAccountEntitlements(Guid sessionId)
        {
            var entitlements = IntegrationTest.GetSessionClientAccountEntitlementsList();
            foreach (SessionClientAccountEntitlementsDto entitlement in entitlements)
            {
                entitlement.SessionId = sessionId;
                entitlement.StartDateKey = new DateTime(1980, 1, 1).ToDateKey();
            }
            IntegrationTest.InsertSessionClientAccountEntitlements(entitlements);
        }

        public void Disconnect()
        {
            DatabaseManager.Disconnect();
        }

        private void CreateDatabaseObjects()
        {
            RecHubData.CreateTable("factBatchSummary");
            RecHubData.CreateTable("factTransactionSummary");
            RecHubData.CreateTable("factAdvancedSearch");
            RecHubData.CreateTable("factChecks");
            RecHubData.CreateTable("factDataEntryDetails");
            RecHubData.CreateTable("factStubs");

            RecHubData.CreateDataType("AdvancedSearchBaseKeysTable");
            RecHubData.CreateDataType("AdvancedSearchWhereClauseTable");
            RecHubData.CreateDataType("AdvancedSearchSelectFieldsTable");

            RecHubData.CreateStoredProcedure("usp_ClientAccount_Search");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_GetSelectColumnList");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_GetResultsXML");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_GetSelectColumnsString");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_GetSessionInfo");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_GetSortString");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_GetWhereString");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_GetSearchParams");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_GetSelectNames");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_Get_BaseColumns");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_Get_DataEntryColumns");
            //RecHubData.CreateStoredProcedure("usp_factAdvancedSearch_Get");

            // Adv Search prototyping
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_GetResultsXML");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_GetSearchParams");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_GetSelectColumnList");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_GetSelectColumnsString");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_GetSelectNames");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_GetSessionInfo");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_GetSortString");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_Get_BaseColumns");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_Get_DataEntryColumns");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_Get_PrelimResults");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_GetWhereString");
            RecHubData.CreateStoredProcedure("usp_AdvancedSearch_Get");


            RecHubSystem.CreateSequence("ReceivablesBatchID");

            IntegrationTest.CreateStoredProcedure("usp_WorkgroupDataEntryColumns_Ins");
            IntegrationTest.CreateStoredProcedure("usp_AdvancedSearch_AddColumn");
            IntegrationTest.CreateStoredProcedure("usp_dimWorkgroupDataEntryColumns_Ins");
            IntegrationTest.CreateStoredProcedure("usp_factBatchSummary_Ins");
            IntegrationTest.CreateStoredProcedure("usp_factChecks_Ins");
            IntegrationTest.CreateStoredProcedure("usp_factStubs_Ins");
            IntegrationTest.CreateStoredProcedure("usp_factTransactionSummary_Ins");
            IntegrationTest.CreateStoredProcedure("usp_factDataEntryDetails_Ins");
            IntegrationTest.CreateStoredProcedure("usp_factAdvancedSearch_InsCots");
            IntegrationTest.CreateStoredProcedure("usp_factAdvancedSearch_Ins_WithDataEntry");
            IntegrationTest.CreateStoredProcedure("usp_factAdvancedSearch_Ins_NoDataEntry");
            IntegrationTest.CreateStoredProcedure("usp_factAdvancedSearch_Ins");

            IntegrationTest.CreateStoredProcedure("usp_AddDataEntryColumn");
            IntegrationTest.CreateStoredProcedure("usp_fact_Upd_DataEntry");


            TestHelper.LoadBatchSourceFile(Path.Combine(DatabaseManager.TestDataPath, "AdvancedSearch"), "BatchSources.json");
            TestHelper.LoadBankFile(Path.Combine(DatabaseManager.TestDataPath, "AdvancedSearch", "ClientSetups"), "Bank*.json");
        }
    }
}
