﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Extensions;

namespace DatabaseTests.CommonObjects
{
    public class TestHelper
    {
        public class ExpectedOrderItem
        {
            public int BatchNumber = -1;
            public int BatchSequence = -1;
            public int TransactionId = -1;
            public int TxnSequence = -1;
        }

        public static string GetDataTableAsXmlString(DataTable dataTable)
        {
            if (dataTable == null)
                return string.Empty;
            StringWriter stringWriter = new StringWriter();
            dataTable.WriteXml(stringWriter);
            return stringWriter.ToString();
        }

        public static void ValidateSearchResultCountsAndTotals(AdvSearchResults searchResults, int recordCount, int checkCount, decimal checkTotal, int documentCount, string testName, string message = null)
        {
            message = (string.IsNullOrEmpty(message)) ? (string.Empty) : (" - " + message);
            Assert.AreEqual(recordCount, searchResults.TotalRecords, $"{testName}{message} - XML Record Count");
            Assert.AreEqual(checkCount, searchResults.CheckCount, $"{testName}{message} - XML Check Count");
            Assert.AreEqual(checkTotal, searchResults.CheckTotal, $"{testName}{message} - XML Check Total");
            Assert.AreEqual(documentCount, searchResults.DocumentCount, $"{testName}{message} - XML Document Count");
        }

        public static bool DataTableIsInExpectedOrder(DataTable dataTable, List<ExpectedOrderItem> expectedResultOrder, bool reverseOrder = false)
        {
            if ((dataTable == null) || (expectedResultOrder == null))
                return false;
            if (dataTable.Rows.Count != expectedResultOrder.Count)
                return false;
            var listIndex = (reverseOrder) ? (expectedResultOrder.Count - 1) : 0;
            foreach (DataRow r in dataTable.Rows)
            {
                int batchNumber, batchSequence, transactionId, txnSequence;

                if (!int.TryParse(r["BatchNumber"].ToString(), out batchNumber))
                    return false;
                if (batchNumber != expectedResultOrder[listIndex].BatchNumber)
                    return false;

                if (!int.TryParse(r["BatchSequence"].ToString(), out batchSequence))
                    batchSequence = -1;
                if (!int.TryParse(r["TransactionID"].ToString(), out transactionId))
                    transactionId = -1;
                if (!int.TryParse(r["TxnSequence"].ToString(), out txnSequence))
                    txnSequence = -1;

                if (expectedResultOrder[listIndex].BatchSequence == -1)
                {
                    if ((transactionId != expectedResultOrder[listIndex].TransactionId))
                        return false;
                    if ((txnSequence != expectedResultOrder[listIndex].TxnSequence))
                        return false;
                }
                else
                {
                    if (batchSequence != expectedResultOrder[listIndex].BatchSequence)
                        return false;
                }

                listIndex += (reverseOrder) ? (-1) : (1);
            }
            return true;
        }

        public static void ValidateResultFieldsExist(List<AdvancedSearchWhereClauseDto>expectedFields, AdvSearchResults asResults, DataTable dtResults, string testName)
        {
            // Validate that expected fields are being returned in the XML results
            foreach (var field in expectedFields)
            {
                var found = false;
                var tblName = AdvancedSearchWhereClauseDto.GetTableName(field.Table);
                foreach (var resultField in asResults.SelectFields)
                {
                    if ((tblName.ToUpper() == resultField.TableName.ToUpper()) && (field.FieldName.ToUpper() == resultField.FieldName.ToUpper().Split("_")[0]))
                        found = true;
                }
                Assert.IsTrue(found, $"{testName} - Validate field {tblName}.{field.FieldName} exists in XML result.");
            }
            // Validate that fields named in the XML results exist in the data set result and are accessible
            foreach (var resultField in asResults.SelectFields)
            {
                var found = false;
#if OldAdvSearch
                var colName = resultField.TableName + resultField.FieldName + "_" + resultField.ColID;
#else 
                var colName = resultField.FieldName;
#endif
                try
                {
                    var value = dtResults.Rows[0][colName].ToString();
                    found = true;
                }
                catch (Exception)
                { }
                Assert.IsTrue(found, $"{testName} - Validate field {resultField.TableName}.{resultField.FieldName} exists in data set.");
            }
        }

        public static decimal GetDollarSumFromResultColumn(AdvSearchResults asResults, DataTable dtResults, string tableName, string fieldName)
        {
            string colName = GetColumnNameFromResultSet(asResults, tableName, fieldName, AdvSearchResults.AdvSearchResultsSelectFieldDataType.Decimal);

            if (colName.Length == 0)
                return 0;

            decimal total = 0;
            foreach (DataRow row in dtResults.Rows)
            {
                decimal amt = 0;
                decimal.TryParse(row[colName].ToString(), out amt);
                total += amt;
            }
            return total;
        }

        public static void ValidateStringFieldValuesFromList(List<string>values, AdvSearchResults asResults, DataTable dtResults, string tableName, string fieldName, string testName)
        {
            string colName = GetColumnNameFromResultSet(asResults, tableName, fieldName, AdvSearchResults.AdvSearchResultsSelectFieldDataType.String);

            if (colName.Length == 0)
                Assert.Fail($"{testName} - Validate field contents for {tableName}.{fieldName}.");
            bool validated = true;
            foreach (DataRow row in dtResults.Rows)
            {
                string value = row[colName].ToString().ToUpper().Trim();
                if (value.Length > 0)
                    validated &= (values.Exists(x => x.ToUpper().Trim() == value));
            }
            Assert.IsTrue(validated, $"{testName} - Validate field contents for {tableName}.{fieldName}.");
        }

        public static void ValidateMoneyFieldValuesFromList(List<decimal> values, AdvSearchResults asResults, DataTable dtResults, string tableName, string fieldName, string testName)
        {
            string colName = GetColumnNameFromResultSet(asResults, tableName, fieldName, AdvSearchResults.AdvSearchResultsSelectFieldDataType.Decimal);

            if (colName.Length == 0)
                Assert.Fail($"{testName} - Validate field contents for {tableName}.{fieldName}.");
            bool validated = true;
            foreach (DataRow row in dtResults.Rows)
            {
                decimal value = 0;
                if (decimal.TryParse(row[colName].ToString().ToUpper().Trim(), out value))
                    validated &= (values.Exists(x => x == value));
            }
            Assert.IsTrue(validated, $"{testName} - Validate field contents for {tableName}.{fieldName}.");
        }

        public static void ValidateMoneyFieldValuesFromList(List<decimal> values, DataTable dtResults, string tableName,
            string fieldName, string testName)
        {
            bool validated = true;
            foreach (DataRow row in dtResults.Rows)
            {
                if (decimal.TryParse(row[fieldName].ToString().ToUpper().Trim(), out var value))
                    validated &= (values.Exists(x => x == value));
            }
            Assert.IsTrue(validated, $"{testName} - Validate field contents for {tableName}.{fieldName}.");
        }

        public static void ValidateFloatFieldValuesFromList(List<float> values, AdvSearchResults asResults, DataTable dtResults, string tableName, string fieldName, string testName)
        {
            string colName = GetColumnNameFromResultSet(asResults, tableName, fieldName, AdvSearchResults.AdvSearchResultsSelectFieldDataType.Float);

            if (colName.Length == 0)
                Assert.Fail($"{testName} - Validate field contents for {tableName}.{fieldName}.");
            bool validated = true;
            foreach (DataRow row in dtResults.Rows)
            {
                float value = 0;
                if (float.TryParse(row[colName].ToString().ToUpper().Trim(), out value))
                    validated &= (values.Exists(x => x == value));
            }
            Assert.IsTrue(validated, $"{testName} - Validate field contents for {tableName}.{fieldName}.");
        }

        public static void ValidateDateTimeFieldValuesFromList(List<DateTime> values, AdvSearchResults asResults, DataTable dtResults, string tableName, string fieldName, string testName)
        {
            string colName = GetColumnNameFromResultSet(asResults, tableName, fieldName, AdvSearchResults.AdvSearchResultsSelectFieldDataType.DateTime);

            if (colName.Length == 0)
                Assert.Fail($"{testName} - Validate field contents for {tableName}.{fieldName}.");
            bool validated = true;
            foreach (DataRow row in dtResults.Rows)
            {
                DateTime value = DateTime.Now;
                if (DateTime.TryParse(row[colName].ToString().ToUpper().Trim(), out value))
                    validated &= (values.Exists(x => x == value));
            }
            Assert.IsTrue(validated, $"{testName} - Validate field contents for {tableName}.{fieldName}.");
        }

        private static string GetColumnNameFromResultSet(AdvSearchResults asResults, string tableName, string fieldName, AdvSearchResults.AdvSearchResultsSelectFieldDataType dataType)
        {
            string colName = string.Empty;

            foreach (var resultField in asResults.SelectFields)
            {
                if ((resultField.TableName.ToUpper() == tableName.ToUpper()) &&
                    (resultField.FieldName.ToUpper() == fieldName.ToUpper()) &&
                    (resultField.DataType == dataType))
#if OldAdvSearch
                    colName = resultField.TableName + resultField.FieldName + "_" + resultField.ColID;
#else
                    colName = resultField.FieldName;
#endif
            }
            return colName;
        }

    }
}
