using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseTestFramework;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Extensions;
using DatabaseTestFramework.Interfaces;
using DatabaseTestFramework.SchemaClasses;
using DatabaseTests.CommonObjects;

namespace DatabaseTests.AdvancedSearch
{
    [TestClass]
    public class AdvancedSearchCheckOnlyTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static AdvancedSearchDataContext _dataContext;
        private static short _batchSourceKey;
        private static byte _paymentTypeKey;
        private static Guid _sessionId;

        private class BasicCheckFieldTest
        {
            public string TestName = string.Empty;
            public string RTValue = string.Empty;
            public string AccountValue = string.Empty;
            public string SerialValue = string.Empty;
            public int ExpectedCheckCount;
            public decimal ExpectedCheckTotal;
            public int ExpectedDocumentCount;
            public int ExpectedTotalRecordCount;
        }

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _dataContext = new AdvancedSearchDataContext(_testContext.Properties["AppSettings"].ToString(), "AdvancedSearchCheckOnly");
            _dataContext.LoadBatches("CheckOnly.json");
            _batchSourceKey = _dataContext.RecHubData.GetBatchSourceKey("ImageRPS");
            _paymentTypeKey = _dataContext.RecHubData.GetPaymentTypeKey("Lockbox");
            _sessionId = Guid.NewGuid();
            _dataContext.InsertSessionClientAccountEntitlements(_sessionId);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _dataContext.Disconnect();
        }

         [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_AllItems()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName);
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today is correct");
            Assert.AreEqual(18, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday is correct");
            Assert.AreEqual(24, dtResults.AsEnumerable().Where(r => (string)r["PaymentSource"] == "ImageRPS").Count(), $"{_testContext.TestName} - All items have payment source of ImageRPS");
            Assert.AreEqual(24, dtResults.AsEnumerable().Where(r => (string)r["PaymentType"] == "Check").Count(), $"{_testContext.TestName} - All items have payment type of check");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_FilterByStandardDepositDate()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = _dataContext.RunDate.Date,
                DepositDateTo = _dataContext.RunDate.Date,
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 6, 6, 36701.3900M, 0, _testContext.TestName);
            Assert.AreEqual(6, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - All items are dated today.");
            Assert.AreEqual(6, dtResults.AsEnumerable().Where(r => (string)r["PaymentSource"] == "ImageRPS").Count(), $"{_testContext.TestName} - All items have payment source of ImageRPS");
            Assert.AreEqual(6, dtResults.AsEnumerable().Where(r => (string)r["PaymentType"] == "Check").Count(), $"{_testContext.TestName} - All items have payment type of check");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_FilterByStandardAmount()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                WhereClauses =
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "AmountFrom",
                        IsStandard = true,
                        Value = "100.00"
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "AmountTo",
                        IsStandard = true,
                        Value = "1000.00"
                    }
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 9, 9, 3981.6500M, 0, _testContext.TestName);
            Assert.AreEqual(9, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(2, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today is correct.");
            Assert.AreEqual(7, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday is correct.");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_FilterByStandardBatchNumber()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                WhereClauses =
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchNumberFrom",
                        IsStandard = true,
                        Value = "8"
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchNumberTo",
                        IsStandard = true,
                        Value = "11"
                    }
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 12, 12, 41415.3600M, 0, _testContext.TestName);
            Assert.AreEqual(12, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today is correct.");
            Assert.AreEqual(6, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday is correct.");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_FilterByStandardBatchId()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                WhereClauses =
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchIDFrom",
                        IsStandard = true,
                        Value = "11"
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchIDTo",
                        IsStandard = true,
                        Value = "15"
                    }
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 18, 18, 38846.2800M, 0, _testContext.TestName);
            Assert.AreEqual(18, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(0, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - No items dated today.");
            Assert.AreEqual(18, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - All items dated yesterday.");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_Pagination()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = true,
                RecordsPerPage = 7,
                StartRecord = 0
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            List<string> ItemTrackingList = new List<string>();

            // Run first page
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate first page results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "First Set");
            foreach (DataRow r in dtResults.Rows)
                if (!ItemTrackingList.Exists(item => (item == $"{r["SourceBatchID"]}-{r["BatchSequence"]}")))
                    ItemTrackingList.Add($"{r["SourceBatchID"]}-{r["BatchSequence"]}");
            Assert.AreEqual(7, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count (First Set)");

            // Run second page
            request.StartRecord = 7;
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate second page results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Second Set");
            foreach (DataRow r in dtResults.Rows)
                if (!ItemTrackingList.Exists(item => (item == $"{r["SourceBatchID"]}-{r["BatchSequence"]}")))
                    ItemTrackingList.Add($"{r["SourceBatchID"]}-{r["BatchSequence"]}");
            Assert.AreEqual(7, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count (Second Set)");

            // Run third page
            request.StartRecord = 14;
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate third page results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Third Set");
            foreach (DataRow r in dtResults.Rows)
                if (!ItemTrackingList.Exists(item => (item == $"{r["SourceBatchID"]}-{r["BatchSequence"]}")))
                    ItemTrackingList.Add($"{r["SourceBatchID"]}-{r["BatchSequence"]}");
            Assert.AreEqual(7, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count (Third Set)");

            // Run fourth/final page
            request.StartRecord = 21;
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate fourth/final page results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Fourth/Final Set");
            foreach (DataRow r in dtResults.Rows)
                if (!ItemTrackingList.Exists(item => (item == $"{r["SourceBatchID"]}-{r["BatchSequence"]}")))
                    ItemTrackingList.Add($"{r["SourceBatchID"]}-{r["BatchSequence"]}");
            Assert.AreEqual(3, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count (Fourth/Final Set)");

            // Validate that we saw all the items across all the pages
            Assert.AreEqual(24, ItemTrackingList.Count, $"{_testContext.TestName} - All unique items returned in pages.");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_FilterByBasicCheckFields()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                WhereClauses = new List<AdvancedSearchWhereClauseDto>()
            };

            DataTable dtResults;
            AdvSearchResults asResults;


            // Create a list of tests of various combinations of RT, account, and serial
            var tests = new List<BasicCheckFieldTest>()
            {
                new BasicCheckFieldTest()
                {
                    TestName = "Serial Value Only Filter",
                    RTValue = string.Empty,
                    AccountValue = string.Empty,
                    SerialValue = "6528",
                    ExpectedCheckCount = 1,
                    ExpectedCheckTotal = 333.8700M,
                    ExpectedDocumentCount = 0,
                    ExpectedTotalRecordCount = 1
                },
                new BasicCheckFieldTest()
                {
                    TestName = "Routing Value Only Filter",
                    RTValue = "025428913",
                    AccountValue = string.Empty,
                    SerialValue = string.Empty,
                    ExpectedCheckCount = 3,
                    ExpectedCheckTotal = 5060.4600M,
                    ExpectedDocumentCount = 0,
                    ExpectedTotalRecordCount = 3
                },
                new BasicCheckFieldTest()
                {
                    TestName = "Account Value Only Filter",
                    RTValue = string.Empty,
                    AccountValue = "32080958",
                    SerialValue = string.Empty,
                    ExpectedCheckCount = 3,
                    ExpectedCheckTotal = 1142.8200M,
                    ExpectedDocumentCount = 0,
                    ExpectedTotalRecordCount = 3
                },
                new BasicCheckFieldTest()
                {
                    TestName = "Routing And Account Filter",
                    RTValue = "025428913",
                    AccountValue = "64292703",
                    SerialValue = string.Empty,
                    ExpectedCheckCount = 1,
                    ExpectedCheckTotal = 2378.3100M,
                    ExpectedDocumentCount = 0,
                    ExpectedTotalRecordCount = 1
                },
                new BasicCheckFieldTest()
                {
                    TestName = "Account And Serial Filter",
                    RTValue = string.Empty,
                    AccountValue = "32080958",
                    SerialValue = "2962",
                    ExpectedCheckCount = 1,
                    ExpectedCheckTotal = 150.0100M,
                    ExpectedDocumentCount = 0,
                    ExpectedTotalRecordCount = 1
                },
                new BasicCheckFieldTest()
                {
                    TestName = "Routing, Account And Serial Filter",
                    RTValue = "095601171",
                    AccountValue = "5137457049",
                    SerialValue = "5214",
                    ExpectedCheckCount = 1,
                    ExpectedCheckTotal = 7195.8700M,
                    ExpectedDocumentCount = 0,
                    ExpectedTotalRecordCount = 1
                },
                new BasicCheckFieldTest()
                {
                    TestName = "Routing, Account And Serial NOT Found Filter",
                    RTValue = "095601171",
                    AccountValue = "5137457049",
                    SerialValue = "1458",
                    ExpectedCheckCount = 0,
                    ExpectedCheckTotal = 0.0000M,
                    ExpectedDocumentCount = 0,
                    ExpectedTotalRecordCount = 0
                }
            };

            // Now run each test in the list
            foreach (BasicCheckFieldTest test in tests)
            {
                request.WhereClauses.Clear();

                // Add a WHERE on routing number, if needed for this test
                if (!string.IsNullOrEmpty(test.RTValue))
                {
                    request.WhereClauses.Add(new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        IsDataEntry = true,
                        FieldName = "RT",
                        DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                        Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                        Value = test.RTValue
                    });
                }

                // Add a WHERE on account number, if needed for this test
                if (!string.IsNullOrEmpty(test.AccountValue))
                {
                    request.WhereClauses.Add(new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        IsDataEntry = true,
                        FieldName = "Account",
                        DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                        Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                        Value = test.AccountValue
                    });
                }

                // Add a WHERE on serial, if needed for this test
                if (!string.IsNullOrEmpty(test.SerialValue))
                {
                    request.WhereClauses.Add(new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        IsDataEntry = true,
                        FieldName = "Serial",
                        DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                        Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                        Value = test.SerialValue
                    });
                }

                _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

                // Validate results 
                TestHelper.ValidateSearchResultCountsAndTotals(asResults, test.ExpectedTotalRecordCount, test.ExpectedCheckCount, test.ExpectedCheckTotal, test.ExpectedDocumentCount, _testContext.TestName, test.TestName);
                if (dtResults != null)
                {
                    foreach (DataRow r in dtResults.Rows)
                    {
                        if (!string.IsNullOrEmpty(test.RTValue))
                            Assert.AreEqual(test.RTValue, r["RT"], $"{_testContext.TestName} - {test.TestName} - Routing Number Value Match");
                        if (!string.IsNullOrEmpty(test.AccountValue))
                            Assert.AreEqual(test.AccountValue, r["AccountNumber"], $"{_testContext.TestName} - {test.TestName} - Account Value Match");
                        if (!string.IsNullOrEmpty(test.SerialValue))
                            Assert.AreEqual(test.SerialValue, r["SerialNumber"], $"{_testContext.TestName} - {test.TestName} - Serial Value Match");
                    }
                }
            }
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortByStandardAmount()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrder = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "CheckAmount",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrder), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Amount");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrder, true), $"{_testContext.TestName} - Sort Descending - In Expected Order By Amount");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortByCheckFieldSerialNumber()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrder = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "SerialNumber",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrder), $"{_testContext.TestName} - Sort Ascending - In Expected Order By SerialNumber");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrder, true), $"{_testContext.TestName} - Sort Descending - In Expected Order By SerialNumber");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialBatchDepositDate()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "BATCHDEPOSITDATE",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHDEPOSITDATE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHDEPOSITDATE");
        }


        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialBatchId()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "BATCHBATCHID",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending BATCHBATCHID");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending BATCHBATCHID - DataTable Row Count (Sort ASC)");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expcted Order By Special BATCHBATCHID");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending BATCHBATCHID");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending BATCHBATCHID - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHBATCHID");

            // Now switch to other "special" version of batch id sort specifier.
            request.SortBy = "BATCHSOURCEBATCHID";
            request.SortByDir = "ASC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending BATCHSOURCEBATCHID");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending BATCHSOURCEBATCHID - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHSOURCEBATCHID");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending BATCHSOURCEBATCHID");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending BATCHSOURCEBATCHID - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - In Expected Order By Special BATCHSOURCEBATCHID");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialBatchNumber()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "BATCHBATCHNUMBER",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHBATCHNUMBER");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHBATCHNUMBER");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialTransaction()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "TRANSACTIONSTRANSACTIONID",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending TRANSACTIONSTRANSACTIONID");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending TRANSACTIONSTRANSACTIONID - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special TRANSACTIONSTRANSACTIONID");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending TRANSACTIONSTRANSACTIONID");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending TRANSACTIONSTRANSACTIONID - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special TRANSACTIONSTRANSACTIONID");

            // Now switch to other "special" transaction sort specifier.
            request.SortBy = "TRANSACTIONSTXNSEQUENCE";
            request.SortByDir = "ASC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending TRANSACTIONSTXNSEQUENCE");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending TRANSACTIONSTXNSEQUENCE - DataTable Row Count (Sort ASC)");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special TRANSACTIONSTXNSEQUENCE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending TRANSACTIONSTXNSEQUENCE");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending TRANSACTIONSTXNSEQUENCE - DataTable Row Count (Sort DESC)");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special TRANSACTIONSTXNSEQUENCE");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialChecksAmount()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "CHECKSAMOUNT",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special CHECKSAMOUNT");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special CHECKSAMOUNT");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialChecksSerial()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "CHECKSSERIAL",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special CHECKSSERIAL");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special CHECKSSERIAL");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialChecksRT()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "CHECKSRT",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special CHECKSRT");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special CHECKSRT");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialChecksAccount()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "CHECKSACCOUNT",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special CHECKSACCOUNT");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special CHECKSACCOUNT");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialChecksRemitter()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "CHECKSREMITTERNAME",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special CHECKSREMITTERNAME");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special CHECKSREMITTERNAME");

            // Now switch to other "special" remitter name sort
            request.SortBy = "CHECKSPAYER";
            request.SortByDir = "ASC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending CHECKSPAYER");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending CHECKSPAYER - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special CHECKSPAYER");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending CHECKSPAYER");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending CHECKSPAYER - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special CHECKSPAYER");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialBatchPaymentSource()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "BATCHPAYMENTSOURCE",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHPAYMENTSOURCE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHPAYMENTSOURCE");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialBatchPaymentType()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "BATCHPAYMENTTYPE",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHPAYMENTTYPE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHPAYMENTTYPE");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialChecksTransactionCode()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "CHECKSTRANSACTIONCODE",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special CHECKSTRANSACTIONCODE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special CHECKSTRANSACTIONCODE");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckOnly_SortBySpecialChecksBatchSequence()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 12 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 11 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 10 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 15, BatchSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, BatchSequence = 1 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "CHECKSBATCHSEQUENCE",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Ascending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Ascending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special CHECKSBATCHSEQUENCE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName, "Sort Descending");
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - Sort Descending - DataTable Row Count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special CHECKSBATCHSEQUENCE");
        }

    }
}
