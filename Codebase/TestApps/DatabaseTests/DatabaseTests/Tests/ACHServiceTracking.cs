﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using DatabaseTestFramework;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Extensions;
using DatabaseTestFramework.Interfaces;
using DatabaseTestFramework.SchemaClasses;
using DatabaseTests.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace DatabaseTests.Tests
{
    [TestClass]
    public class AchServiceTracking
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;

        private static IDatabaseManager _databaseManager;
        private static DateTime _runDate;

        private static RecHubSystem _recHubSystem;

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _databaseManager = new DatabaseManager(_testContext.Properties["AppSettings"].ToString());
            _runDate = DateTime.Now;
            _databaseManager.CreateDatabase("ACHServiceTracking");

            _recHubSystem = new RecHubSystem(_databaseManager, _runDate);

            _recHubSystem.CreateTable("DataImportQueue");
            _recHubSystem.CreateTable("DataImportTracking");
            _recHubSystem.CreateDataType("DataImportQueueJsonInsertTable");
            _recHubSystem.CreateStoredProcedure("usp_DataImportQueue_Ins_JSONBatch");
            _recHubSystem.CreateStoredProcedure("usp_DataImportTracking_Get");
            _recHubSystem.CreateStoredProcedure("usp_DataImportTracking_Ins");
            _recHubSystem.CreateStoredProcedure("usp_DataImportTracking_Upd");
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _databaseManager.Disconnect();
        }

        [TestMethod]
        public void ACHServiceTracking_usp_DataImportTracking_Get_OneBatchTrackingId()
        {
            var sourceTrackingId = Guid.NewGuid();
            var batchTrackingId = Guid.NewGuid();

            var importRequest = new List<DataImportQueueJsonDataDto>()
            {
                new DataImportQueueJsonDataDto
                {
                    AuditDateKey = 20190101,
                    ClientProcessCode = "ACHServiceTrackingTest",
                    SourceTrackingId = sourceTrackingId,
                    BatchTrackingId = batchTrackingId,
                    JsonDataDocument = JsonConvert.SerializeObject(new DataImportQueueJsonBatch
                    {
                        BatchSource = "ACHServiceTrackingTest",
                        BatchTrackingID = batchTrackingId.ToString()
                    })
                }
            }.ToDataTable();

            Assert.IsTrue(_recHubSystem.Execute_DataImportQueue_Ins_JSONBatch(importRequest),"Insert DataImportQueue data");

            var sqlParams = new List<SqlParameter>
            {
                _databaseManager.BuildSqlParameter("@parmSourceTrackingId", SqlDbType.UniqueIdentifier,
                    sourceTrackingId),
                _databaseManager.BuildSqlParameter("@parmFileStatus", SqlDbType.Int,2),
                _databaseManager.BuildSqlParameter("@parmCreationDate", SqlDbType.DateTime2,_runDate),
                _databaseManager.BuildSqlParameter("@parmCreatedBy", SqlDbType.VarChar,"ACHServiceTrackingTest")
            };
            Assert.IsTrue(_recHubSystem.ExecuteStoredProcedure("usp_DataImportTracking_Ins", sqlParams.ToArray()),"Insert DataTracking data");

            var actualResults = new DataTable();

            _recHubSystem.ExecuteStoredProcedure("usp_DataImportTracking_Get",
                new List<SqlParameter>
                {
                    _databaseManager.BuildSqlParameter("@parmSourceTrackingId", SqlDbType.UniqueIdentifier,
                        sourceTrackingId)
                }.ToArray(), ref actualResults);

            Assert.AreEqual(1, actualResults.Rows.Count, "Rows Returned");
            Assert.AreEqual(2, Convert.ToInt32(actualResults.Rows[0]["FileStatus"]));
            Assert.AreEqual($"<Batch BatchTrackingID=\"{importRequest.Rows[0]["BatchTrackingId"].ToString().ToUpper()}\" />", actualResults.Rows[0]["BatchTrackingIDs"].ToString());
        }

        [TestMethod]
        public void ACHServiceTracking_usp_DataImportTracking_Get_TwoBatchTrackingId()
        {
            var sourceTrackingId = Guid.NewGuid();
            var batchTrackingId1 = Guid.NewGuid();
            var batchTrackingId2 = Guid.NewGuid();

            var importRequest = new List<DataImportQueueJsonDataDto>()
            {
                new DataImportQueueJsonDataDto
                {
                    AuditDateKey = 20190101,
                    ClientProcessCode = "ACHServiceTrackingTest",
                    SourceTrackingId = sourceTrackingId,
                    BatchTrackingId = batchTrackingId1,
                    JsonDataDocument = JsonConvert.SerializeObject(new DataImportQueueJsonBatch
                    {
                        BatchSource = "ACHServiceTrackingTest",
                        BatchTrackingID = batchTrackingId1.ToString()
                    })
                },
                new DataImportQueueJsonDataDto
                {
                    AuditDateKey = 20190101,
                    ClientProcessCode = "ACHServiceTrackingTest",
                    SourceTrackingId = sourceTrackingId,
                    BatchTrackingId = batchTrackingId2,
                    JsonDataDocument = JsonConvert.SerializeObject(new DataImportQueueJsonBatch
                    {
                        BatchSource = "ACHServiceTrackingTest",
                        BatchTrackingID = batchTrackingId2.ToString()
                    })

                }
            }.ToDataTable();

            Assert.IsTrue(_recHubSystem.Execute_DataImportQueue_Ins_JSONBatch(importRequest), "Insert DataImportQueue data");

            var sqlParams = new List<SqlParameter>
            {
                _databaseManager.BuildSqlParameter("@parmSourceTrackingId", SqlDbType.UniqueIdentifier,
                    sourceTrackingId),
                _databaseManager.BuildSqlParameter("@parmFileStatus", SqlDbType.Int,2),
                _databaseManager.BuildSqlParameter("@parmCreationDate", SqlDbType.DateTime2,_runDate),
                _databaseManager.BuildSqlParameter("@parmCreatedBy", SqlDbType.VarChar,"ACHServiceTrackingTest")
            };
            Assert.IsTrue(_recHubSystem.ExecuteStoredProcedure("usp_DataImportTracking_Ins", sqlParams.ToArray()), "Insert DataTracking data");

            var actualResults = new DataTable();

            _recHubSystem.ExecuteStoredProcedure("usp_DataImportTracking_Get",
                new List<SqlParameter>
                {
                    _databaseManager.BuildSqlParameter("@parmSourceTrackingId", SqlDbType.UniqueIdentifier,
                        sourceTrackingId)
                }.ToArray(), ref actualResults);

            Assert.AreEqual(1, actualResults.Rows.Count, "Rows Returned");
            Assert.AreEqual(2, Convert.ToInt32(actualResults.Rows[0]["FileStatus"]));
            Assert.AreEqual($"<Batch BatchTrackingID=\"{importRequest.Rows[0]["BatchTrackingId"].ToString().ToUpper()}\" /><Batch BatchTrackingID=\"{importRequest.Rows[1]["BatchTrackingId"].ToString().ToUpper()}\" />", actualResults.Rows[0]["BatchTrackingIDs"].ToString());

        }
    }
}
