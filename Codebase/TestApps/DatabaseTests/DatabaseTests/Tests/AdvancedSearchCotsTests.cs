﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Extensions;
using DatabaseTests.CommonObjects;

namespace DatabaseTests.AdvancedSearch
{
    [TestClass]
    public class AdvancedSearchCotsTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static AdvancedSearchDataContext _dataContext;
        private static short _batchSourceKey;
        private static byte _paymentTypeKey;
        private static Guid _sessionId;

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _dataContext = new AdvancedSearchDataContext(_testContext.Properties["AppSettings"].ToString(), "AdvancedSearchCots");
            _dataContext.LoadBatches("Cots.json");
            _batchSourceKey = _dataContext.RecHubData.GetBatchSourceKey("ImageRPS");
            _paymentTypeKey = _dataContext.RecHubData.GetPaymentTypeKey("Lockbox");
            _sessionId = Guid.NewGuid();
            _dataContext.InsertSessionClientAccountEntitlements(_sessionId);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _dataContext.Disconnect();
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void Cots_AllItems()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                CotsOnly = true
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(1, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today");
            Assert.AreEqual(4, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void Cots_FilterByStandardBatchNumber()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                CotsOnly = true,
                WhereClauses =
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchNumberFrom",
                        IsStandard = true,
                        Value = "11"
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchNumberTo",
                        IsStandard = true,
                        Value = "11"
                    }
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 0, 0.0M, 0, _testContext.TestName);
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void Cots_FilterByStandardBatchId()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                CotsOnly = true,
                WhereClauses =
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchIDFrom",
                        IsStandard = true,
                        Value = "8"
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchIDTo",
                        IsStandard = true,
                        Value = "8"
                    }
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void Cots_FilterByStandardDepositDate()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = _dataContext.RunDate.Date,
                DepositDateTo = _dataContext.RunDate.Date,
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                CotsOnly = true
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void Cots_SortBySpecialBatchDepositDate()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                CotsOnly = true,
                SortBy = "BATCHDEPOSITDATE",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHDEPOSITDATE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHDEPOSITDATE");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void Cots_SortBySpecialBatchId()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                CotsOnly = true,
                SortBy = "BATCHBATCHID",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHBATCHID");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHBATCHID");

            // Now switch to other "special" version of batch id sort specifier.
            request.SortBy = "BATCHSOURCEBATCHID";
            request.SortByDir = "ASC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHSOURCEBATCHID");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHSOURCEBATCHID");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void Cots_SortBySpecialBatchNumber()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                CotsOnly = true,
                SortBy = "BATCHBATCHNUMBER",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHBATCHNUMBER");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHBATCHNUMBER");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void Cots_SortBySpecialTransaction()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                CotsOnly = true,
                SortBy = "TRANSACTIONSTRANSACTIONID",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special TRANSACTIONSTRANSACTIONID");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special TRANSACTIONSTRANSACTIONID");

            // Now switch to other "special" version of batch id sort specifier.
            request.SortBy = "TRANSACTIONSTXNSEQUENCE";
            request.SortByDir = "ASC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special TRANSACTIONSTXNSEQUENCE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 4, 8525.1300M, 1, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special TRANSACTIONSTXNSEQUENCE");
        }

    }
}
