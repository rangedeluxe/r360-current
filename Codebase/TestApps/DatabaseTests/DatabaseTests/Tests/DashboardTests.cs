﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using AutoMapper;
using DatabaseTestFramework;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Extensions;
using DatabaseTestFramework.Interfaces;
using DatabaseTestFramework.SchemaClasses;
using DatabaseTests.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatabaseTests.Tests
{
    [TestClass]
    public class DashboardTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;

        private static IDatabaseManager _databaseManager;
        private static DateTime _runDate;

        private static IntegrationTest _integrationTest;
        private static RecHubData _recHubData;
        private static RecHubSystem _recHubSystem;
        private static RecHubUser _recHubUser;
        private static DataLoadHelper _testHelper;

        private static IMapper _mapper;

        private static int _userId;
        private static Guid _sessionId;

        private void VerifyRow(DashboardDto expectedResults, List<DashboardDto> actualResults, string testName)
        {
            var results = actualResults.FirstOrDefault(r =>
                r.SiteBankId == expectedResults.SiteBankId &&
                r.SiteClientAccountId == expectedResults.SiteClientAccountId &&
                r.BatchSourceId == expectedResults.BatchSourceId &&
                r.PaymentTypeId == expectedResults.PaymentTypeId &&
                r.Dda.Trim() == expectedResults.Dda.Trim());

            Assert.IsNotNull(results, $"{testName} Could not find record. SiteBankID {expectedResults.SiteBankId}, SiteClientAccountID {expectedResults.SiteClientAccountId}, BatchSource {expectedResults.BatchSource}, PaymentType {expectedResults.PaymentType}, DDA {expectedResults.Dda}");
            Assert.AreEqual(expectedResults.BatchCount, results.BatchCount, $"{testName} BatchCount");
            Assert.AreEqual(expectedResults.TransactionCount, results.TransactionCount, $"{testName} TransactionCount");
            Assert.AreEqual(expectedResults.PaymentCount, results.PaymentCount, $"{testName} PaymentCount");
            Assert.AreEqual(expectedResults.PaymentTotal, results.PaymentTotal, $"{testName} PaymentTotal");
            Assert.AreEqual(expectedResults.BatchSource, results.BatchSource, $"{testName} BatchSource");
            Assert.AreEqual(expectedResults.PaymentType, results.PaymentType, $"{testName} PaymentType");
            Assert.AreEqual(expectedResults.EntityId, results.EntityId, $"{testName} EntityId");
            Assert.AreEqual(expectedResults.EntityName, results.EntityName, $"{testName} EntityName");
        }

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _databaseManager = new DatabaseManager(_testContext.Properties["AppSettings"].ToString());
            _runDate = DateTime.Now;
            //Create a database for Advanced Search tests
            _databaseManager.CreateDatabase("DashBoard");
            var recHubCommon = new RecHubCommon(_databaseManager, _runDate);

            _integrationTest = new IntegrationTest(_databaseManager, _runDate);
            _recHubData = new RecHubData(_databaseManager, _runDate);
            _recHubSystem = new RecHubSystem(_databaseManager, _runDate);
            _recHubUser = new RecHubUser(_databaseManager, _runDate);

            _testHelper = new DataLoadHelper(_recHubData, _recHubUser, _integrationTest);

            //Additional RecHubData objects
            _recHubData.CreateDataType("WorkgroupListTable");
            _recHubData.CreateTable("factDataEntryDetails");
            _recHubData.CreateTable("factBatchSummary");
            _recHubData.CreateTable("factTransactionSummary");
            _recHubData.CreateTable("factAdvancedSearch");
            _recHubData.CreateTable("factChecks");
            _recHubData.CreateTable("factStubs");
            _recHubData.CreateStoredProcedure("usp_factBatchSummary_Get_ClientAccountSummary_By_SP");
            _recHubData.CreateStoredProcedure("usp_ElectronicAccounts_Merge");
            _recHubData.CreateView("dimClientAccountsView");


            //Additional RecHubUser objects
            _recHubUser.CreateTable("OLEntities");
            _recHubUser.CreateTable("OLWorkgroups");
            _recHubUser.CreateTable("Users");
            _userId = _recHubUser.InsertUser("DatabaseTester1");


            //Additional RecHubSystem objects
            _recHubSystem.CreateSequence("ReceivablesBatchID");
            
            //Additional IntegrationTest objects
            _integrationTest.CreateStoredProcedure("usp_factBatchSummary_Ins");
            _integrationTest.CreateStoredProcedure("usp_factChecks_Ins");
            _integrationTest.CreateStoredProcedure("usp_factTransactionSummary_Ins");
            _integrationTest.CreateStoredProcedure("usp_factAdvancedSearch_InsCots");
            _integrationTest.CreateStoredProcedure("usp_factAdvancedSearch_Ins_WithDataEntry");
            _integrationTest.CreateStoredProcedure("usp_factAdvancedSearch_Ins_NoDataEntry");
            _integrationTest.CreateStoredProcedure("usp_factAdvancedSearch_Ins");

            _integrationTest.CreateStoredProcedure("usp_AddDataEntryColumn");
            _integrationTest.CreateStoredProcedure("usp_fact_Upd_DataEntry");

            //Load data for this test class
            _testHelper.LoadBatchSourceFile(Path.Combine(_databaseManager.TestDataPath, "Dashboard"), "BatchSources.json");
            _testHelper.LoadBankFile(Path.Combine(_databaseManager.TestDataPath, "Dashboard"), "Bank*.json");
            _testHelper.LoadBatches(Path.Combine(_databaseManager.TestDataPath, "Dashboard"), "Batches*.json");

            _sessionId = Guid.NewGuid();

            _integrationTest.InsertSessionClientAccountEntitlements(new List<SessionClientAccountEntitlementsDto>()
            {
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 1,
                    SiteClientAccountId = 4533,
                    EntityName = "integraPAY",
                    EntityId = 40,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 88,
                    SiteClientAccountId = 68,
                    EntityName = "ACH Service Inc",
                    EntityId = 21,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 999,
                    SiteClientAccountId = 91,
                    EntityName = "ImageRPS Inc",
                    EntityId = 11,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 999,
                    SiteClientAccountId = 92,
                    EntityName = "ImageRPS Inc",
                    EntityId = 11,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 999,
                    SiteClientAccountId = 93,
                    EntityName = "ImageRPS Inc",
                    EntityId = 11,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 999,
                    SiteClientAccountId = 94,
                    EntityName = "ImageRPS Inc",
                    EntityId = 11,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 999,
                    SiteClientAccountId = 95,
                    EntityName = "ImageRPS Inc",
                    EntityId = 11,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 999,
                    SiteClientAccountId = 96,
                    EntityName = "ImageRPS Inc",
                    EntityId = 11,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 1159511,
                    SiteClientAccountId = 2234322,
                    EntityName = "AutomationImports",
                    EntityId = 34,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 1159511,
                    SiteClientAccountId = 1123211,
                    EntityName = "AutomationImports",
                    EntityId = 34,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 1159511,
                    SiteClientAccountId = 7890001,
                    EntityName = "AutomationImports",
                    EntityId = 34,
                    StartDateKey = _runDate.ToDateKey()
                },
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 1159511,
                    SiteClientAccountId = 33338888,
                    EntityName = "AutomationImports",
                    EntityId = 34,
                    StartDateKey = _runDate.ToDateKey()
                }
            });

            //Setup mapping for each data access
            var config = new MapperConfiguration(c =>
            {
                c.CreateMap<DataRow, DashboardDto>()
                    .ForMember(d => d.EntityId, opt => opt.MapFrom(s => s["EntityID"]))
                    .ForMember(d => d.EntityName, opt => opt.MapFrom(s => s["EntityName"]))
                    .ForMember(d => d.SiteBankId, opt => opt.MapFrom(s => s["SiteBankID"]))
                    .ForMember(d => d.SiteClientAccountId,
                        opt => opt.MapFrom(s => s["SiteClientAccountID"]))
                    .ForMember(d => d.WorkgroupName, opt => opt.MapFrom(s => s["WorkgroupName"]))
                    .ForMember(d => d.Dda, opt => opt.MapFrom(s => s["DDA"]))
                    .ForMember(d => d.OlWorkgroupId, opt => opt.MapFrom(s => s["OLWorkgroupID"]))
                    .ForMember(d => d.BatchSourceId, opt => opt.MapFrom(s => s["BatchSourceId"]))
                    .ForMember(d => d.BatchSource, opt => opt.MapFrom(s => s["BatchSource"]))
                    .ForMember(d => d.PaymentTypeId, opt => opt.MapFrom(s => s["PaymentTypeID"]))
                    .ForMember(d => d.PaymentType, opt => opt.MapFrom(s => s["PaymentType"]))
                    .ForMember(d => d.BatchCount, opt => opt.MapFrom(s => s["BatchCount"]))
                    .ForMember(d => d.TransactionCount, opt => opt.MapFrom(s => s["TransactionCount"]))
                    .ForMember(d => d.PaymentCount, opt => opt.MapFrom(s => s["PaymentCount"]))
                    .ForMember(d => d.PaymentTotal, opt => opt.MapFrom(s => s["PaymentTotal"]));
            });

            _mapper = config.CreateMapper();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _databaseManager.Disconnect();
        }

        [TestMethod]
        public void Dashboard_Filtered()
        {

            var workgroupList = new List<WorkgroupListDto>()
            {
                 new WorkgroupListDto()
                {
                    BankId = 999,
                    WorkgroupId = 92,
                }
            }.ToDataTable();


            var results = _mapper.Map<List<DashboardDto>>(_recHubData.ExecuteDashboard(_sessionId, _runDate.ToDateKey(), workgroupList).Rows);

            Assert.AreEqual(2, results.Count, $"{_testContext.TestName} - Result rows count");

            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 92,
                EntityId = 11,
                WorkgroupName = "92 - GenericElkhorn",
                BatchSource = "GenericElkhorn",
                BatchSourceId = 105,
                PaymentType = "SWIFT",
                PaymentTypeId = 4,
                BatchCount = 3,
                Dda = "0123456668",
                TransactionCount = 12,
                PaymentCount = 12,
                PaymentTotal = 985243.28
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 92,
                EntityId = 11,
                WorkgroupName = "92 - GenericElkhorn",
                BatchSource = "GenericElkhorn",
                BatchSourceId = 105,
                PaymentType = "SWIFT",
                PaymentTypeId = 4,
                BatchCount = 1,
                Dda = "904676648",
                TransactionCount = 1,
                PaymentCount = 1,
                PaymentTotal = 21.83
            }, results, _testContext.TestName);
        }

        [TestMethod]
        public void DashBoard_FullSet()
        {
            var workgroupList = new List<WorkgroupListDto>()
            {
                new WorkgroupListDto
                {
                    BankId = 1,
                    WorkgroupId = 4533
                },
                new WorkgroupListDto
                {
                    BankId = 88,
                    WorkgroupId = 68
                },
                new WorkgroupListDto()
                {
                    BankId = 999,
                    WorkgroupId = 91,
                },
                new WorkgroupListDto()
                {
                    BankId = 999,
                    WorkgroupId = 92,
                },
                new WorkgroupListDto()
                {
                    BankId = 999,
                    WorkgroupId = 93,
                },
                new WorkgroupListDto()
                {
                    BankId = 999,
                    WorkgroupId = 94,
                },
                new WorkgroupListDto()
                {
                    BankId = 999,
                    WorkgroupId = 95,
                },
                new WorkgroupListDto()
                {
                    BankId = 999,
                    WorkgroupId = 96,
                },
                new WorkgroupListDto()
                {
                    BankId = 1159511,
                    WorkgroupId = 2234322,
                },
                new WorkgroupListDto()
                {
                    BankId = 1159511,
                    WorkgroupId = 1123211,
                },
                new WorkgroupListDto()
                {
                    BankId = 1159511,
                    WorkgroupId = 7890001,
                },
                new WorkgroupListDto()
                {
                    BankId = 1159511,
                    WorkgroupId = 33338888,
                }
            }.ToDataTable();


            var results = _mapper.Map<List<DashboardDto>>(_recHubData.ExecuteDashboard(_sessionId, _runDate.ToDateKey(), workgroupList).Rows);

            Assert.AreEqual(20,results.Count,$"{_testContext.TestName} - Result rows count");

            //Bank 1
            VerifyRow(new DashboardDto
            {
                EntityName = "integraPAY",
                SiteBankId = 1,
                SiteClientAccountId = 4533,
                EntityId = 40,
                WorkgroupName = "4533 - DEMO BOX 4533",
                BatchSource = "INTEGRAPAY-I1",
                BatchSourceId = 129,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "2226188748",
                TransactionCount = 6,
                PaymentCount = 6,
                PaymentTotal = 36701.39
            }, results, _testContext.TestName);

            //Bank 88
            VerifyRow(new DashboardDto
            {
                EntityName = "ACH Service Inc",
                SiteBankId = 88,
                SiteClientAccountId = 68,
                EntityId = 21,
                WorkgroupName = "68 - ACH Test Workgroup",
                BatchSource = "ACH Service",
                BatchSourceId = 115,
                PaymentType = "ACH",
                PaymentTypeId = 1,
                BatchCount = 2,
                Dda = "123456789",
                TransactionCount = 2,
                PaymentCount = 2,
                PaymentTotal = 1590.65
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "ACH Service Inc",
                SiteBankId = 88,
                SiteClientAccountId = 68,
                EntityId = 21,
                WorkgroupName = "68 - ACH Test Workgroup",
                BatchSource = "ACH Service",
                BatchSourceId = 115,
                PaymentType = "ACH",
                PaymentTypeId = 1,
                BatchCount = 4,
                Dda = "274866485",
                TransactionCount = 4,
                PaymentCount = 4,
                PaymentTotal = 13758.92
            }, results, _testContext.TestName);

            //Bank 999
            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 91,
                EntityId = 11,
                WorkgroupName = "91 - ImageRPSElkhorn",
                BatchSource = "ImageRPSElkhorn",
                BatchSourceId = 104,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "123456789",
                TransactionCount = 2,
                PaymentCount = 2,
                PaymentTotal = 2110.05
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 91,
                EntityId = 11,
                WorkgroupName = "91 - ImageRPSElkhorn",
                BatchSource = "ImageRPSElkhorn",
                BatchSourceId = 104,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "123456790",
                TransactionCount = 1,
                PaymentCount = 1,
                PaymentTotal = 94.56
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 91,
                EntityId = 11,
                WorkgroupName = "91 - ImageRPSElkhorn",
                BatchSource = "ImageRPSElkhorn",
                BatchSourceId = 104,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "123456792",
                TransactionCount = 1,
                PaymentCount = 1,
                PaymentTotal = 48.67
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 91,
                EntityId = 11,
                WorkgroupName = "91 - ImageRPSElkhorn",
                BatchSource = "ImageRPSElkhorn",
                BatchSourceId = 104,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "123456794",
                TransactionCount = 1,
                PaymentCount = 1,
                PaymentTotal = 31.27
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 91,
                EntityId = 11,
                WorkgroupName = "91 - ImageRPSElkhorn",
                BatchSource = "ImageRPSElkhorn",
                BatchSourceId = 104,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "123456796",
                TransactionCount = 1,
                PaymentCount = 1,
                PaymentTotal = 83.71
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 91,
                EntityId = 11,
                WorkgroupName = "91 - ImageRPSElkhorn",
                BatchSource = "ImageRPSElkhorn",
                BatchSourceId = 104,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "6549809123456789",
                TransactionCount = 12,
                PaymentCount = 12,
                PaymentTotal = 7782.09
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 92,
                EntityId = 11,
                WorkgroupName = "92 - GenericElkhorn",
                BatchSource = "GenericElkhorn",
                BatchSourceId = 105,
                PaymentType = "SWIFT",
                PaymentTypeId = 4,
                BatchCount = 3,
                Dda = "0123456668",
                TransactionCount = 12,
                PaymentCount = 12,
                PaymentTotal = 985243.28
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "ImageRPS Inc",
                SiteBankId = 999,
                SiteClientAccountId = 92,
                EntityId = 11,
                WorkgroupName = "92 - GenericElkhorn",
                BatchSource = "GenericElkhorn",
                BatchSourceId = 105,
                PaymentType = "SWIFT",
                PaymentTypeId = 4,
                BatchCount = 1,
                Dda = "904676648",
                TransactionCount = 1,
                PaymentCount = 1,
                PaymentTotal = 21.83
            }, results, _testContext.TestName);

            //Bank 1159511
            VerifyRow(new DashboardDto
            {
                EntityName = "AutomationImports",
                SiteBankId = 1159511,
                SiteClientAccountId = 1123211,
                EntityId = 34,
                WorkgroupName = "1123211 - 1123211",
                BatchSource = "AUTOMATIONIMAGERPS",
                BatchSourceId = 121,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "",
                TransactionCount = 4,
                PaymentCount = 0,
                PaymentTotal = 0
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "AutomationImports",
                SiteBankId = 1159511,
                SiteClientAccountId = 2234322,
                EntityId = 34,
                WorkgroupName = "2234322 - Automation-Filter",
                BatchSource = "AutomationACH",
                BatchSourceId = 127,
                PaymentType = "ACH",
                PaymentTypeId = 1,
                BatchCount = 2,
                Dda = "00000000081428899",
                TransactionCount = 3,
                PaymentCount = 3,
                PaymentTotal = 6050.00
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "AutomationImports",
                SiteBankId = 1159511,
                SiteClientAccountId = 2234322,
                EntityId = 34,
                WorkgroupName = "2234322 - Automation-Filter",
                BatchSource = "AUTOMATIONIMAGERPS",
                BatchSourceId = 121,
                PaymentType = "Card",
                PaymentTypeId = 3,
                BatchCount = 1,
                Dda = "55546980123456789",
                TransactionCount = 4,
                PaymentCount = 4,
                PaymentTotal = 258.21
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "AutomationImports",
                SiteBankId = 1159511,
                SiteClientAccountId = 2234322,
                EntityId = 34,
                WorkgroupName = "2234322 - Automation-Filter",
                BatchSource = "AUTOMATIONIMAGERPS",
                BatchSourceId = 121,
                PaymentType = "Cash",
                PaymentTypeId = 5,
                BatchCount = 1,
                Dda = "55546980123456789",
                TransactionCount = 4,
                PaymentCount = 4,
                PaymentTotal = 258.21
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "AutomationImports",
                SiteBankId = 1159511,
                SiteClientAccountId = 2234322,
                EntityId = 34,
                WorkgroupName = "2234322 - Automation-Filter",
                BatchSource = "AUTOMATIONIMAGERPS",
                BatchSourceId = 121,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "55546980123456789",
                TransactionCount = 4,
                PaymentCount = 4,
                PaymentTotal = 258.21
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "AutomationImports",
                SiteBankId = 1159511,
                SiteClientAccountId = 2234322,
                EntityId = 34,
                WorkgroupName = "2234322 - Automation-Filter",
                BatchSource = "AUTOMATIONIMAGERPS",
                BatchSourceId = 121,
                PaymentType = "SWIFT",
                PaymentTypeId = 4,
                BatchCount = 1,
                Dda = "55546980123456789",
                TransactionCount = 4,
                PaymentCount = 4,
                PaymentTotal = 258.21
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "AutomationImports",
                SiteBankId = 1159511,
                SiteClientAccountId = 2234322,
                EntityId = 34,
                WorkgroupName = "2234322 - Automation-Filter",
                BatchSource = "AutomationWIRE",
                BatchSourceId = 122,
                PaymentType = "Wire",
                PaymentTypeId = 2,
                BatchCount = 1,
                Dda = "7722343227",
                TransactionCount = 2,
                PaymentCount = 2,
                PaymentTotal = 230.00
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "AutomationImports",
                SiteBankId = 1159511,
                SiteClientAccountId = 7890001,
                EntityId = 34,
                WorkgroupName = "7890001 - Post Deposit Test Workgroup",
                BatchSource = "AUTOMATIONIMAGERPS",
                BatchSourceId = 121,
                PaymentType = "Check",
                PaymentTypeId = 0,
                BatchCount = 1,
                Dda = "{DDA}",
                TransactionCount = 4,
                PaymentCount = 4,
                PaymentTotal = 378.24
            }, results, _testContext.TestName);

            VerifyRow(new DashboardDto
            {
                EntityName = "AutomationImports",
                SiteBankId = 1159511,
                SiteClientAccountId = 33338888,
                EntityId = 34,
                WorkgroupName = "33338888 - 33338888",
                BatchSource = "AutomationACH",
                BatchSourceId = 127,
                PaymentType = "ACH",
                PaymentTypeId = 1,
                BatchCount = 9,
                Dda = "00000009112345667",
                TransactionCount = 11,
                PaymentCount = 11,
                PaymentTotal = 122680.64
            }, results, _testContext.TestName);
        }
    }
}
