﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseTestFramework.DTO;
using DatabaseTests.CommonObjects;

namespace DatabaseTests.AdvancedSearch
{
    [TestClass]
    public class AdvancedSearchCheckDataEntryTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static AdvancedSearchDataContext _dataContext;
        private static short _batchSourceKey;
        private static byte _paymentTypeKey;
        private static Guid _sessionId;

#if OldAdvSearch
        private const string CheckTestDateColumnName = "TestDate";
        private const string CheckTestFloatColumnName = "TestFloat";
        private const string CheckTestMoneyColumnName = "TestMoney";
        private const string CheckTestStringColumnName = "TestString";
#else
        private const string CheckTestDateColumnName = "TestDate_1_11";
        private const string CheckTestFloatColumnName = "TestFloat_1_6";
        private const string CheckTestMoneyColumnName = "TestMoney_1_7";
        private const string CheckTestStringColumnName = "TestString_1_1";
#endif

        private static List<AdvancedSearchWhereClauseDto> SelectAllFields = new List<AdvancedSearchWhereClauseDto>()
        {
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                IsDataEntry = true,
                FieldName = "TestString",
                ReportTitle = "Test String",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                IsDataEntry = true,
                FieldName = "TestFloat",
                ReportTitle = "Test Float",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                IsDataEntry = true,
                FieldName = "TestMoney",
                ReportTitle = "Test Money",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                IsDataEntry = true,
                FieldName = "TestDate",
                ReportTitle = "Test Date",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime
            }
        };

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _dataContext = new AdvancedSearchDataContext(_testContext.Properties["AppSettings"].ToString(), "AdvancedSearchCheckDataEntry");
            _dataContext.LoadBatches("CheckDataEntry.json");
            _batchSourceKey = _dataContext.RecHubData.GetBatchSourceKey("ImageRPS");
            _paymentTypeKey = _dataContext.RecHubData.GetPaymentTypeKey("Lockbox");
            _sessionId = Guid.NewGuid();
            _dataContext.InsertSessionClientAccountEntitlements(_sessionId);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _dataContext.Disconnect();
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_AllItems()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, 0, _testContext.TestName);
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today is correct");
            Assert.AreEqual(18, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday is correct");
            Assert.AreEqual(24, dtResults.AsEnumerable().Where(r => (string)r["PaymentSource"] == "ImageRPS").Count(), $"{_testContext.TestName} - All items have payment source of ImageRPS");
            Assert.AreEqual(24, dtResults.AsEnumerable().Where(r => (string)r["PaymentType"] == "Check").Count(), $"{_testContext.TestName} - All items have payment type of check");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_ValidateCheckFieldsReturned()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.WhereClauses.Clear();
            request.SelectFields = SelectAllFields;

            var ResultStringValueList = new List<string>()
            {
                "Balanced Multi-Check",
                "Balanced Single-Single",
                "Simple",
                "Test",
                "Unbalanced Multi-Multi",
                "Unbalanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                1.1111f,
                1.999f,
                2.2222f,
                2.999f,
                3.14159f,
                3.999f,
                4.3322f,
                4.999f,
                49.4328f,
                7.999f,
                8.999f,
                9.111f,
                9.222f,
                9.2783f,
                9.3423f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                0.00M,
                0.25M,
                1388.39M,
                147.59M,
                15.00M,
                177.65M,
                2247.54M,
                25.00M,
                45.00M,
                455.71M,
                837.99M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1),
                new DateTime(2019,8,10),
                new DateTime(2019,8,30),
                new DateTime(2019,8,31),
                new DateTime(2019,9,1),
                new DateTime(2019,9,2),
                new DateTime(2019,9,4),
                new DateTime(2019,9,5),
                new DateTime(2019,10,1),
                new DateTime(2019,10,2),
                new DateTime(2019,10,3),
                new DateTime(2019,10,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 30, 24, 75547.6700M, 0, _testContext.TestName);
            Assert.AreEqual(30, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestDateEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "08/10/2019"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Simple",
                "Test"
            };
            var ResultFloatValueList = new List<float>()
            {
                3.14159f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                147.59M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,8,10)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 2, 28176.2600M, 0, _testContext.TestName);
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestDateLessThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "08/10/2019"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                9.2783f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                177.65M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 177.6500M, 0, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestDateGreaterThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "10/03/2019"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Multi-Check",
                "Unbalanced Multi-Multi"
            };
            var ResultFloatValueList = new List<float>()
            {
                3.999f,
                4.3322f,
                4.999f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                0.00M,
                0.25M,
                1388.39M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,10,3),
                new DateTime(2019,10,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 6, 10358.1900M, 0, _testContext.TestName);
            Assert.AreEqual(8, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestDateRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "09/01/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/05/2019"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                8.999f,
                49.4328f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                25.00M,
                2247.54M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,9,2),
                new DateTime(2019,9,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 2, 2, 6815.7500M, 0, _testContext.TestName);
            Assert.AreEqual(2, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestFloatEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "8.999"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                8.999f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,9,2)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 4568.2100M, 0, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestFloatLessThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "2.00"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Unbalanced Multi-Multi",
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                1.999f,
                2.999f,
                1.1111f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                25.00M,
                455.71M,
                837.99M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,10,1),
                new DateTime(2019,10,2)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 3, 1083.4900M, 0, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestFloatGreaterThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "49.00"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                49.4328f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                2247.54M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,9,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 2247.5400M, 0, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestFloatRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "8.00"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "9.00"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                8.999f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,9,2)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 4568.2100M, 0, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestMoneyEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "177.65"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                9.2783f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                177.65M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 177.6500M, 0, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestMoneyLessThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "10.00"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Unbalanced Multi-Multi"
            };
            var ResultFloatValueList = new List<float>()
            {
                3.999f,
                4.999f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                0.00M,
                0.25M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,10,3),
                new DateTime(2019,10,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 2, 1078.0300M, 0, _testContext.TestName);
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestMoneyGreaterThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "1000.00"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Multi-Check",
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                4.3322f,
                49.4328f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                1388.39M,
                2247.54M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,9,4),
                new DateTime(2019,10,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 5, 5, 11527.7000M, 0, _testContext.TestName);
            Assert.AreEqual(5, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestMoneyRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "150.00"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "200.00"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                9.2783f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                177.65M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 177.6500M, 0, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestStringEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "Unbalanced Single-Single"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Unbalanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                9.3423f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,9,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 240.0500M, 0, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestStringContains()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Contains,
                    Value = "imp"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Simple",
                "Test"
            };
            var ResultFloatValueList = new List<float>()
            {
                3.14159f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                147.59M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,8,10)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 2, 28176.2600M, 0, _testContext.TestName);
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestStringBeginsWith()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.BeginsWith,
                    Value = "Sim"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Simple",
                "Test"
            };
            var ResultFloatValueList = new List<float>()
            {
                3.14159f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                147.59M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,8,10)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 2, 28176.2600M, 0, _testContext.TestName);
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestStringEndsWith()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.EndsWith,
                    Value = "est"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Simple",
                "Test"
            };
            var ResultFloatValueList = new List<float>()
            {
                3.14159f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                147.59M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,8,10)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 2, 28176.2600M, 0, _testContext.TestName);
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestStringRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "S"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "U"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Simple",
                "Test"
            };
            var ResultFloatValueList = new List<float>()
            {
                3.14159f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                147.59M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,8,10)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 2, 28176.2600M, 0, _testContext.TestName);
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestDateAndTestMoney()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "09/01/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/30/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "25.00"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Unbalanced Single-Single",
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                9.3423f,
                8.999f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,9,2),
                new DateTime(2019,9,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 2, 2, 4808.2600M, 0, _testContext.TestName);
            Assert.AreEqual(2, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestDateAndTestFloat()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "09/01/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/30/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "8.999"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                8.999f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,9,2)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 4568.2100M, 0, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckDataEntry_FilterByTestDateAndTestString()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "09/01/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/30/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "Unbalanced Single-Single"
                }
            };

            var ResultStringValueList = new List<string>()
            {
                "Unbalanced Single-Single"
            };
            var ResultFloatValueList = new List<float>()
            {
                9.3423f
            };
            var ResultDecimalValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultDateTimeValueList = new List<DateTime>()
            {
                new DateTime(2019,9,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 240.0500M, 0, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(4, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultDecimalValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultDateTimeValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

    }
}
