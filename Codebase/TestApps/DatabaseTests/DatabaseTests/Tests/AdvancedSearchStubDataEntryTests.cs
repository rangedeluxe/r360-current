﻿using DatabaseTestFramework.DTO;
using DatabaseTests.CommonObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DatabaseTests.AdvancedSearch
{
    [TestClass]
    public class AdvancedSearchStubDataEntryTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static AdvancedSearchDataContext _dataContext;
        private static short _batchSourceKey;
        private static byte _paymentTypeKey;
        private static Guid _sessionId;

#if OldAdvSearch
        private const string StubAmountColumnName = "Amount";
        private const string StubAccountNumberColumnName = "AccountNumber";
        private const string StubTestDateColumnName = "TestDate";
        private const string StubTestFloatColumnName = "TestFloat";
        private const string StubTestMoneyColumnName = "TestMoney";
        private const string StubTestStringColumnName = "TestString";
        private const int ReturnedDocumentCount = 48;
#else
        private const string StubAmountColumnName = "StubAmount";
        private const string StubAccountNumberColumnName = "StubAccountNumber";
        private const string StubTestDateColumnName = "TestDate_0_11";
        private const string StubTestFloatColumnName = "TestFloat_0_6";
        private const string StubTestMoneyColumnName = "TestMoney_0_7";
        private const string StubTestStringColumnName = "TestString_0_1";
        private const int ReturnedDocumentCount = 27;
#endif

        private static List<AdvancedSearchWhereClauseDto> SelectAllFields = new List<AdvancedSearchWhereClauseDto>()
        {
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = "TestString",
                ReportTitle = "Test String",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = "TestFloat",
                ReportTitle = "Test Float",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = "TestMoney",
                ReportTitle = "Test Money",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = "TestDate",
                ReportTitle = "Test Date",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = StubAccountNumberColumnName,
                ReportTitle = "Account Number",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = StubAmountColumnName,
                ReportTitle = "Amount",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
            }
        };

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _dataContext = new AdvancedSearchDataContext(_testContext.Properties["AppSettings"].ToString(), "AdvancedSearchStubDataEntry");
            _dataContext.LoadBatches("StubDataEntry.json");
            _batchSourceKey = _dataContext.RecHubData.GetBatchSourceKey("ImageRPS");
            _paymentTypeKey = _dataContext.RecHubData.GetPaymentTypeKey("Lockbox");
            _sessionId = Guid.NewGuid();
            _dataContext.InsertSessionClientAccountEntitlements(_sessionId);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _dataContext.Disconnect();
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_AllItems()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, ReturnedDocumentCount, _testContext.TestName);
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today is correct");
            Assert.AreEqual(18, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday is correct");
            Assert.AreEqual(24, dtResults.AsEnumerable().Where(r => (string)r["PaymentSource"] == "ImageRPS").Count(), $"{_testContext.TestName} - All items have payment source of ImageRPS");
            Assert.AreEqual(24, dtResults.AsEnumerable().Where(r => (string)r["PaymentType"] == "Check").Count(), $"{_testContext.TestName} - All items have payment type of check");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_ValidateStubFieldsReturned()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses.Clear();
            var ResultAccountNumberValueList = new List<string>()
            {
                "123456789",
                "2079900184489",
                "2079900184689",
                "2079900184989",
                "2079900188889",
                "2079900189589",
                "2079900195289",
                "2079900208589",
                "2079900219689",
                "2079900221489",
                "2079900226989",
                "2079900227789",
                "2079900230989",
                "223456789",
                "321654987",
                "456123789",
                "64292703",
                "654321987",
                "789456123",
                "96725340",
                "987654321",
                "BTK5UFWHV",
                "DMJOSJ6P7",
                "EUL2U5ONX",
                "JM0SY5EOW",
                "LS70FPO7H"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Multi-Check",
                "Balanced Multi-Multi",
                "Balanced Single-Single",
                "Test String 2",
                "Test String 3",
                "Unbalanced Multi-Multi",
                "Unbalanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.1111f,
                1.47653f,
                1.999f,
                2.2222f,
                2.999f,
                3.14159f,
                3.999f,
                4.3322f,
                4.999f,
                49.4328f,
                5.999f,
                6.999f,
                7.999f,
                8.999f,
                9.111f,
                9.222f,
                9.2783f,
                9.3423f,
                9.999f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                10.00M,
                100.00M,
                10000.00M,
                1078.03M,
                1388.39M,
                177.65M,
                2000.00M,
                20670.65M,
                22.22M,
                2247.54M,
                44.44M,
                455.71M,
                4568.21M,
                5000.00M,
                6440.43M,
                658.94M,
                66.66M,
                7000.00M,
                76.26M,
                800.00M,
                837.99M,
                88.27M,
                9000.00M,
                91.77M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                0.00M,
                0.25M,
                1388.39M,
                147.59M,
                15.00M,
                177.65M,
                2.50M,
                2247.54M,
                25.00M,
                35.00M,
                45.00M,
                455.71M,
                7891.77M,
                837.99M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1),
                new DateTime(2019,8,10),
                new DateTime(2019,8,30),
                new DateTime(2019,8,31),
                new DateTime(2019,9,1),
                new DateTime(2019,9,2),
                new DateTime(2019,9,4),
                new DateTime(2019,9,5),
                new DateTime(2019,10,1),
                new DateTime(2019,10,2),
                new DateTime(2019,10,3),
                new DateTime(2019,10,4),
                new DateTime(2019,10,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 48, 24, 75547.6700M, ReturnedDocumentCount, _testContext.TestName);
            Assert.AreEqual(48, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");

            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_ValidateStubAmountOnly()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = new List<AdvancedSearchWhereClauseDto>
            {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
                }
            };
            request.WhereClauses.Clear();

            var ResultAmountValueList = new List<decimal>()
            {
                10.00M,
                100.00M,
                10000.00M,
                1078.03M,
                1388.39M,
                177.65M,
                2000.00M,
                20670.65M,
                22.22M,
                2247.54M,
                44.44M,
                455.71M,
                4568.21M,
                5000.00M,
                6440.43M,
                658.94M,
                66.66M,
                7000.00M,
                76.26M,
                800.00M,
                837.99M,
                88.27M,
                9000.00M,
                91.77M
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 48, 24, 75547.6700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 48, 24, 75547.6700M, 27, _testContext.TestName);
#endif
            Assert.AreEqual(48, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(1, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");

            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_ValidateStubAccountNumberOnly()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = new List<AdvancedSearchWhereClauseDto>
            {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAccountNumberColumnName,
                    ReportTitle = "Account Number",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
                }
            };
            request.WhereClauses.Clear();

            var ResultAccountNumberValueList = new List<string>()
            {
                "123456789",
                "2079900184489",
                "2079900184689",
                "2079900184989",
                "2079900188889",
                "2079900189589",
                "2079900195289",
                "2079900208589",
                "2079900219689",
                "2079900221489",
                "2079900226989",
                "2079900227789",
                "2079900230989",
                "223456789",
                "321654987",
                "456123789",
                "64292703",
                "654321987",
                "789456123",
                "96725340",
                "987654321",
                "BTK5UFWHV",
                "DMJOSJ6P7",
                "EUL2U5ONX",
                "JM0SY5EOW",
                "LS70FPO7H"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 48, 24, 75547.6700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 48, 24, 75547.6700M, 27, _testContext.TestName);
#endif
            Assert.AreEqual(48, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(1, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");

            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAccountNumberEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAccountNumberColumnName,
                    ReportTitle = "Account Number",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "LS70FPO7H"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184689",
                "LS70FPO7H",
                "BTK5UFWHV",
                "2079900188889"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Multi-Multi"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.47653f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                91.77M,
                800.00M,
                2000.00M,
                5000.00M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                7891.77M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, 4, _testContext.TestName);
#endif
            Assert.AreEqual(8, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAccountNumberContains()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAccountNumberColumnName,
                    ReportTitle = "Account Number",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Contains,
                    Value = "70F"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184689",
                "LS70FPO7H",
                "BTK5UFWHV",
                "2079900188889"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Multi-Multi"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.47653f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                91.77M,
                800.00M,
                2000.00M,
                5000.00M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                7891.77M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, 4, _testContext.TestName);
#endif
            Assert.AreEqual(8, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAccountNumberBeginsWith()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAccountNumberColumnName,
                    ReportTitle = "Account Number",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.BeginsWith,
                    Value = "2079"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184989",
                "2079900227789",
                "2079900184489",
                "2079900230989",
                "2079900208589",
                "2079900221489",
                "2079900226989",
                "2079900189589",
                "2079900195289",
                "2079900184689",
                "2079900188889",
                "2079900219689",
                "LS70FPO7H",
                "BTK5UFWHV"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Multi-Check",
                "Balanced Multi-Multi",
                "Balanced Single-Single",
                "Unbalanced Single-Single",
                "Unbalanced Multi-Multi"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                2.2222f,
                4.3322f,
                49.4328f,
                9.3423f,
                1.999f,
                2.999f,
                3.999f,
                4.999f,
                5.999f,
                1.47653f,
                9.2783f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                837.99M,
                1388.39M,
                2247.54M,
                100.00M,
                837.99M,
                10.00M,
                22.22M,
                44.44M,
                66.66M,
                91.77M,
                177.65M,
                800.00M,
                2000.00M,
                5000.00M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                0.00M,
                0.25M,
                2.50M,
                837.99M,
                1388.39M,
                2247.54M,
                25.00M,
                7891.77M,
                177.65M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,10,1),
                new DateTime(2019,10,2),
                new DateTime(2019,10,3),
                new DateTime(2019,10,4),
                new DateTime(2019,10,5),
                new DateTime(2019,10,6),
                new DateTime(2019,9,1),
                new DateTime(2019,9,4),
                new DateTime(2019,9,5),
                new DateTime(2019,1,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 25, 11, 13411.1700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 25, 11, 13411.1700M, 14, _testContext.TestName);
#endif
            Assert.AreEqual(25, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAccountNumberEndsWith()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAccountNumberColumnName,
                    ReportTitle = "Account Number",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.EndsWith,
                    Value = "WHV"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184689",
                "LS70FPO7H",
                "BTK5UFWHV",
                "2079900188889"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Multi-Multi"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.47653f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                91.77M,
                800.00M,
                2000.00M,
                5000.00M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                7891.77M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, 4, _testContext.TestName);
#endif
            Assert.AreEqual(8, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAccountNumberGreaterThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAccountNumberColumnName,
                    ReportTitle = "Account Number",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "EUL"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "EUL2U5ONX",
                "JM0SY5EOW",
                "LS70FPO7H",
                "BTK5UFWHV",
                "2079900188889",
                "2079900184689"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Multi-Multi",
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                7.999f,
                9.222f,
                1.47653f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                88.27M,
                91.77M,
                800.00M,
                2000.00M,
                5000.00M,
                6440.43M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                25.00M,
                45.00M,
                7891.77M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,8,30),
                new DateTime(2019,9,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 10, 4, 14420.4700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 10, 4, 14420.4700M, 6, _testContext.TestName);
#endif
            Assert.AreEqual(10, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAccountNumberLessThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAccountNumberColumnName,
                    ReportTitle = "Account Number",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "199"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "123456789",
                "987654321",
                "654321987",
                "321654987",
                "789456123",
                "456123789"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Multi-Multi",
                "Test String 2",
                "Test String 3"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                3.14159f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                76.26M,
                100.00M,
                2000.00M,
                7000.00M,
                9000.00M,
                10000.00M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                147.59M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,8,10)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 12, 2, 28176.2600M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 12, 2, 28176.2600M, 6, _testContext.TestName);
#endif
            Assert.AreEqual(12, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAmountGreaterThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "20000.00"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "96725340"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Multi-Check"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                9.999f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                20670.65M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                35.00M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 4, 20670.6500M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 4, 20670.6500M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAmountLessThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "50.00"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900208589",
                "2079900221489",
                "2079900226989",
                "2079900189589",
                "2079900195289"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Unbalanced Multi-Multi"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.999f,
                2.999f,
                3.999f,
                4.999f,
                5.999f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                837.99M,
                10.00M,
                22.22M,
                44.44M,
                66.66M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                837.99M,
                25.00M,
                0.00M,
                0.25M,
                2.50M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,10,1),
                new DateTime(2019,10,2),
                new DateTime(2019,10,3),
                new DateTime(2019,10,4),
                new DateTime(2019,10,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 10, 2, 627.7800M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 10, 2, 627.7800M, 5, _testContext.TestName);
#endif
            Assert.AreEqual(10, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAmountEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "100.00"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900230989",
                "123456789",
                "987654321",
                "654321987",
                "321654987",
                "789456123",
                "456123789"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Unbalanced Single-Single",
                "Balanced Multi-Multi",
                "Test String 2",
                "Test String 3"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                9.3423f,
                3.14159f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                100.00M,
                10000.00M,
                9000.00M,
                7000.00M,
                2000.00M,
                76.26M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                25.00M,
                147.59M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,8,10),
                new DateTime(2019,9,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 13, 3, 28416.3100M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 13, 3, 28416.3100M, 7, _testContext.TestName);
#endif
            Assert.AreEqual(13, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAmountEquals_NoAdditionalFields()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields.Clear();
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "100.00"
                },
            };
            var ResultAmountValueList = new List<decimal>()
            {
                240.05M,
                24670.66M,
                3505.60M
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 13, 3, 28416.3100M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 13, 3, 28416.3100M, 7, _testContext.TestName);
#endif
            Assert.AreEqual(13, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.IsNull(asResults.SelectFields, $"{_testContext.TestName} - Validate returned field count.");

            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, dtResults, "Checks", "Amount", _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByAmountRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "800.00"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "900.00"
                }
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184989",
                "2079900208589",
                "2079900221489",
                "2079900226989",
                "2079900189589",
                "2079900195289"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Unbalanced Multi-Multi",
                "Balanced Multi-Check"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.999f,
                2.2222f,
                2.999f,
                3.999f,
                4.999f,
                5.999f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                837.99M,
                10.00M,
                22.22M,
                44.44M,
                66.66M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                837.99M,
                25.00M,
                0.00M,
                0.25M,
                2.50M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,10,1),
                new DateTime(2019,10,2),
                new DateTime(2019,10,3),
                new DateTime(2019,10,4),
                new DateTime(2019,10,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 12, 4, 1465.7700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 12, 4, 1465.7700M, 6, _testContext.TestName);
#endif
            Assert.AreEqual(12, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestStringEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "Balanced Single-Single"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184489",
                "2079900219689",
                "64292703",
                "96725340",
                "DMJOSJ6P7",
                "EUL2U5ONX",
                "JM0SY5EOW"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.1111f,
                7.999f,
                8.999f,
                9.111f,
                9.222f,
                9.2783f,
                49.4328f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                88.27M,
                177.65M,
                455.71M,
                658.94M,
                2247.54M,
                4568.21M,
                6440.43M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                15.00M,
                25.00M,
                45.00M,
                177.65M,
                455.71M,
                2247.54M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1),
                new DateTime(2019,8,30),
                new DateTime(2019,8,31),
                new DateTime(2019,8,10),
                new DateTime(2019,9,1),
                new DateTime(2019,9,2),
                new DateTime(2019,9,4),
                new DateTime(2019,10,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 7, 7, 14636.7500M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 7, 7, 14636.7500M, 7, _testContext.TestName);
#endif
            Assert.AreEqual(7, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }


        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestStringBeginsWith()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.BeginsWith,
                    Value = "Balanced Single"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184489",
                "2079900219689",
                "64292703",
                "96725340",
                "DMJOSJ6P7",
                "EUL2U5ONX",
                "JM0SY5EOW"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.1111f,
                7.999f,
                8.999f,
                9.111f,
                9.222f,
                9.2783f,
                49.4328f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                88.27M,
                177.65M,
                455.71M,
                658.94M,
                2247.54M,
                4568.21M,
                6440.43M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                15.00M,
                25.00M,
                45.00M,
                177.65M,
                455.71M,
                2247.54M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1),
                new DateTime(2019,8,30),
                new DateTime(2019,8,31),
                new DateTime(2019,8,10),
                new DateTime(2019,9,1),
                new DateTime(2019,9,2),
                new DateTime(2019,9,4),
                new DateTime(2019,10,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 7, 7, 14636.7500M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 7, 7, 14636.7500M, 7, _testContext.TestName);
#endif
            Assert.AreEqual(7, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestStringEndsWith()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.EndsWith,
                    Value = "Single"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184489",
                "2079900219689",
                "2079900230989",
                "64292703",
                "96725340",
                "DMJOSJ6P7",
                "EUL2U5ONX",
                "JM0SY5EOW"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single",
                "Unbalanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.1111f,
                7.999f,
                8.999f,
                9.111f,
                9.222f,
                9.2783f,
                9.3423f,
                49.4328f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                88.27M,
                100.00M,
                177.65M,
                455.71M,
                658.94M,
                2247.54M,
                4568.21M,
                6440.43M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                15.00M,
                25.00M,
                45.00M,
                177.65M,
                455.71M,
                2247.54M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1),
                new DateTime(2019,8,30),
                new DateTime(2019,8,31),
                new DateTime(2019,8,10),
                new DateTime(2019,9,1),
                new DateTime(2019,9,2),
                new DateTime(2019,9,4),
                new DateTime(2019,9,5),
                new DateTime(2019,10,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 8, 14876.8000M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 8, 14876.8000M, 8, _testContext.TestName);
#endif
            Assert.AreEqual(8, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestStringContains()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Contains,
                    Value = "Single"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184489",
                "2079900219689",
                "2079900230989",
                "64292703",
                "96725340",
                "DMJOSJ6P7",
                "EUL2U5ONX",
                "JM0SY5EOW"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single",
                "Unbalanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.1111f,
                7.999f,
                8.999f,
                9.111f,
                9.222f,
                9.2783f,
                9.3423f,
                49.4328f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                88.27M,
                100.00M,
                177.65M,
                455.71M,
                658.94M,
                2247.54M,
                4568.21M,
                6440.43M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                15.00M,
                25.00M,
                45.00M,
                177.65M,
                455.71M,
                2247.54M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1),
                new DateTime(2019,8,30),
                new DateTime(2019,8,31),
                new DateTime(2019,8,10),
                new DateTime(2019,9,1),
                new DateTime(2019,9,2),
                new DateTime(2019,9,4),
                new DateTime(2019,9,5),
                new DateTime(2019,10,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 8, 14876.8000M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 8, 14876.8000M, 8, _testContext.TestName);
#endif
            Assert.AreEqual(8, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestMoneyEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "455.71"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "DMJOSJ6P7"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.1111f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                455.71M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                455.71M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,10,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 455.7100M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 455.7100M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestMoneyGreaterThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "7000.00"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184689",
                "2079900188889",
                "LS70FPO7H",
                "BTK5UFWHV"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Multi-Multi"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.47653f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                91.77M,
                800.00M,
                2000.00M,
                5000.00M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                7891.77M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, 4, _testContext.TestName);
#endif
            Assert.AreEqual(8, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestMoneyLessThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "25.00"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900208589",
                "2079900221489",
                "2079900226989",
                "2079900189589",
                "2079900195289",
                "96725340"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Unbalanced Multi-Multi",
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.999f,
                2.999f,
                3.999f,
                4.999f,
                5.999f,
                9.111f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                10.00M,
                22.22M,
                44.44M,
                66.66M,
                658.94M,
                837.99M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                0.00M,
                0.25M,
                2.50M,
                15.00M,
                25.00M,
                837.99M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,8,31),
                new DateTime(2019,10,1),
                new DateTime(2019,10,2),
                new DateTime(2019,10,3),
                new DateTime(2019,10,4),
                new DateTime(2019,10,5),
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 11, 3, 1286.7200M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 11, 3, 1286.7200M, 6, _testContext.TestName);
#endif
            Assert.AreEqual(11, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestMoneyRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "40.00"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "50.00"
                }
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "JM0SY5EOW"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                9.222f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                88.27M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                45.00M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,8,30)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 88.2700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 88.2700M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestFloatEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "9.111"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "96725340"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                9.111f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                658.94M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                15.00M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,8,31)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 658.9400M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 658.9400M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestFloatLessThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "1.5"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184689",
                "2079900188889",
                "LS70FPO7H",
                "DMJOSJ6P7",
                "BTK5UFWHV"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single",
                "Balanced Multi-Multi"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.47653f,
                1.1111f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                91.77M,
                455.71M,
                800.00M,
                2000.00M,
                5000.00M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                455.71M,
                7891.77M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,1),
                new DateTime(2019,10,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 9, 3, 8347.4800M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 9, 3, 8347.4800M, 5, _testContext.TestName);
#endif
            Assert.AreEqual(9, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestFloatGreaterThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "10.0"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184489"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                49.4328f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                2247.54M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                2247.54M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 2247.5400M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 2247.5400M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestFloatRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "1.0"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "1.25"
                }
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "DMJOSJ6P7"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.1111f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                455.71M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                455.71M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,10,1)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 455.7100M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 455.7100M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestDateEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "09/04/2019"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900184489"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                49.4328f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                2247.54M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                2247.54M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 2247.5400M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 2247.5400M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestDateLessThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "08/15/2019"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900219689",
                "123456789",
                "987654321",
                "654321987",
                "321654987",
                "789456123",
                "456123789"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single",
                "Balanced Multi-Multi",
                "Test String 2",
                "Test String 3"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                3.14159f,
                9.2783f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                76.26M,
                100.00M,
                177.65M,
                2000.00M,
                7000.00M,
                9000.00M,
                10000.00M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                147.59M,
                177.65M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1),
                new DateTime(2019,8,10)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 13, 3, 28353.9100M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 13, 3, 28353.9100M, 7, _testContext.TestName);
#endif

            Assert.AreEqual(13, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestDateGreaterThan()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "10/04/2019"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900208589",
                "2079900221489",
                "2079900226989",
                "2079900189589",
                "2079900195289"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Unbalanced Multi-Multi"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                1.999f,
                2.999f,
                3.999f,
                4.999f,
                5.999f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                10.00M,
                22.22M,
                44.44M,
                66.66M,
                837.99M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                0.00M,
                0.25M,
                2.50M,
                25.00M,
                837.99M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,10,1),
                new DateTime(2019,10,2),
                new DateTime(2019,10,3),
                new DateTime(2019,10,4),
                new DateTime(2019,10,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 10, 2, 627.7800M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 10, 2, 627.7800M, 5, _testContext.TestName);
#endif
            Assert.AreEqual(10, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestDateRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "09/01/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/03/2019"
                }
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "64292703"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                8.999f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                4568.21M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,2)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 4568.2100M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 4568.2100M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestDateAndAmount()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "08/31/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "100.00"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "JM0SY5EOW"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                9.222f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                88.27M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                45.00M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,8,30)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 88.2700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 88.2700M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestDateAndTestMoney()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "09/01/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/30/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "25.00"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900230989",
                "223456789",
                "64292703"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Unbalanced Single-Single",
                "Balanced Multi-Check",
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                6.999f,
                8.999f,
                9.3423f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                100.00M,
                1078.03M,
                4568.21M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,2),
                new DateTime(2019,9,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 4, 5886.2900M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 4, 5886.2900M, 3, _testContext.TestName);
#endif
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestDateAndTestString()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "09/01/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/30/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Contains,
                    Value = "Unbalanced"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "2079900230989"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Unbalanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                9.3423f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                100.00M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 240.0500M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 240.0500M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubDataEntry_FilterByTestDateAndTestFloat()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "09/01/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/30/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "8.999"
                },
            };
            var ResultAccountNumberValueList = new List<string>()
            {
                "64292703"
            };
            var ResultTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultTestFloatValueList = new List<float>()
            {
                8.999f
            };
            var ResultAmountValueList = new List<decimal>()
            {
                4568.21M
            };
            var ResultTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,2)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate basic results 
#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 4568.2100M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 4568.2100M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
        }

    }
}
