﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Extensions;
using DatabaseTests.CommonObjects;

namespace DatabaseTests.AdvancedSearch
{
    [TestClass]
    public class AdvancedSearchStubOnlyTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static AdvancedSearchDataContext _dataContext;
        private static short _batchSourceKey;
        private static byte _paymentTypeKey;
        private static Guid _sessionId;

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _dataContext = new AdvancedSearchDataContext(_testContext.Properties["AppSettings"].ToString(), "AdvancedSearchStubOnly");
            _dataContext.LoadBatches("StubOnly.json");
            _batchSourceKey = _dataContext.RecHubData.GetBatchSourceKey("ImageRPS");
            _paymentTypeKey = _dataContext.RecHubData.GetPaymentTypeKey("Lockbox");
            _sessionId = Guid.NewGuid();
            _dataContext.InsertSessionClientAccountEntitlements(_sessionId);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _dataContext.Disconnect();
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubOnly_AllItems()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            var sxml = TestHelper.GetDataTableAsXmlString(dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual( 7, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today");
            Assert.AreEqual( 9, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubOnly_SelectStubAmount()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
#if OldAdvSearch
                    FieldName = "Amount",
#else
                    FieldName = "StubAmount",
#endif
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            var sxml = TestHelper.GetDataTableAsXmlString(dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 22, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(22, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(7, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today");
            Assert.AreEqual(15, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday");
            Assert.AreEqual(1, asResults.SelectFields.Count, $"{_testContext.TestName} - Validate returned field count.");

            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);
#if OldAdvSearch
            Assert.AreEqual(890023.81M, TestHelper.GetDollarSumFromResultColumn(asResults, dtResults, "Stubs", "Amount"), $"{_testContext.TestName} - Validate sum of stub amounts.");
#else
            Assert.AreEqual(890023.81M, TestHelper.GetDollarSumFromResultColumn(asResults, dtResults, "Stubs", "StubAmount"), $"{_testContext.TestName} - Validate sum of stub amounts.");
#endif
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubOnly_FilterByStandardBatchNumber()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                WhereClauses =
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchNumberFrom",
                        IsStandard = true,
                        Value = "11"
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchNumberTo",
                        IsStandard = true,
                        Value = "11"
                    }
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 9, 0, 0.0M, 15, _testContext.TestName);
            Assert.AreEqual(9, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(15, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - DataTable sum of stub counts");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubOnly_FilterByStandardBatchId()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                WhereClauses =
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchIDFrom",
                        IsStandard = true,
                        Value = "8"
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = _batchSourceKey,
                        Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                        StandardXmlColumnName = "BatchIDTo",
                        IsStandard = true,
                        Value = "8"
                    }
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 7, 0, 0.0M, 7, _testContext.TestName);
            Assert.AreEqual(7, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(7, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - DataTable sum of stub counts");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubOnly_FilterByStandardDepositDate()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = _dataContext.RunDate.Date,
                DepositDateTo = _dataContext.RunDate.Date,
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 7, 0, 0.0M, 7, _testContext.TestName);
            Assert.AreEqual(7, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(7, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - DataTable sum of stub counts");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubOnly_SortBySpecialBatchDepositDate()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 8, TxnSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 9, TxnSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 7, TxnSequence = 7 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 8, TxnSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 9, TxnSequence = 9 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "BATCHDEPOSITDATE",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHDEPOSITDATE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHDEPOSITDATE");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubOnly_SortBySpecialBatchId()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 8, TxnSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 9, TxnSequence = 9 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 8, TxnSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 9, TxnSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 7, TxnSequence = 7 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "BATCHBATCHID",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHBATCHID");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHBATCHID");

            // Now switch to other "special" version of batch id sort specifier.
            request.SortBy = "BATCHSOURCEBATCHID";
            request.SortByDir = "ASC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHSOURCEBATCHID");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHSOURCEBATCHID");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubOnly_SortBySpecialBatchNumber()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 8, TxnSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 9, TxnSequence = 9 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 8, TxnSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 9, TxnSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 7, TxnSequence = 7 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "BATCHBATCHNUMBER",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special BATCHBATCHNUMBER");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special BATCHBATCHNUMBER");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void StubOnly_SortBySpecialTransaction()
        {
            List<TestHelper.ExpectedOrderItem> expectedResultOrderAscending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 8, TxnSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 9, TxnSequence = 9 }
            };

            List<TestHelper.ExpectedOrderItem> expectedResultOrderDescending = new List<TestHelper.ExpectedOrderItem>() {
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 9, TxnSequence = 9 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 8, TxnSequence = 8 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 7, TxnSequence = 7 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 6, TxnSequence = 6 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 5, TxnSequence = 5 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 4, TxnSequence = 4 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 3, TxnSequence = 3 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 2, TxnSequence = 2 },
                new TestHelper.ExpectedOrderItem() { BatchNumber = 11, TransactionId = 1, TxnSequence = 1 },
                new TestHelper.ExpectedOrderItem() { BatchNumber =  8, TransactionId = 1, TxnSequence = 1 }
            };

            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false,
                SortBy = "TRANSACTIONSTRANSACTIONID",
                SortByDir = "ASC"
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special TRANSACTIONSTRANSACTIONID");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special TRANSACTIONSTRANSACTIONID");

            // Now switch to other "special" version of batch id sort specifier.
            request.SortBy = "TRANSACTIONSTXNSEQUENCE";
            request.SortByDir = "ASC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderAscending), $"{_testContext.TestName} - Sort Ascending - In Expected Order By Special TRANSACTIONSTXNSEQUENCE");

            // Now check that descending order works.
            request.SortByDir = "DESC";
            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            // Validate results 
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 16, 0, 0.0M, 22, _testContext.TestName);
            Assert.AreEqual(16, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable row count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Sum(r => (int)r["StubCount"]), $"{_testContext.TestName} - Sum of stub counts");
            Assert.IsTrue(TestHelper.DataTableIsInExpectedOrder(dtResults, expectedResultOrderDescending), $"{_testContext.TestName} - Sort Descending - In Expected Order By Special TRANSACTIONSTXNSEQUENCE");
        }

    }
}
