﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Xml;
using DatabaseTestFramework;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Interfaces;
using DatabaseTestFramework.SchemaClasses;
using DatabaseTests.DTO;
using DatabaseTests.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatabaseTests.Tests
{
    [TestClass]
    public class ClientAccountTest
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;

        private static IDatabaseManager _databaseManager;
        private static DateTime _runDate;

        private static IntegrationTest _integrationTest;
        private static RecHubAudit _recHubAudit;
        private static RecHubCommon _recHubCommon;
        private static RecHubData _recHubData;
        private static RecHubUser _recHubUser;
        private static DataLoadHelper _testHelper;

        private string GetFIsAsXml(IList<int> fiEntityIds)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriter xw = XmlWriter.Create(sb, new XmlWriterSettings() { OmitXmlDeclaration = true });
            xw.WriteStartElement("FIs");
            if (fiEntityIds != null)
                foreach (var id in fiEntityIds)
                {
                    xw.WriteElementString("ID", id.ToString());
                }
            xw.WriteEndElement();
            xw.Flush();
            return sb.ToString();
        }

        private List<SqlParameter> ClientAccountInsertParams(ClientAccountTestDto clientAccount)
        {
            var sqlParams = new List<SqlParameter>
            {
                _databaseManager.BuildSqlParameter("@parmIsActive", SqlDbType.SmallInt, clientAccount.IsActive),
                _databaseManager.BuildSqlParameter("@parmDataRetentionDays", SqlDbType.SmallInt, clientAccount.DataRetentionDays),
                _databaseManager.BuildSqlParameter("@parmImageRetentionDays", SqlDbType.SmallInt, clientAccount.ImageRetentionDays),
                _databaseManager.BuildSqlParameter("@parmShortName", SqlDbType.VarChar, clientAccount.ShortName),
                _databaseManager.BuildSqlParameter("@parmLongName", SqlDbType.VarChar, clientAccount.LongName ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmFileGroup", SqlDbType.VarChar, clientAccount.FileGroup ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmEntityID", SqlDbType.Int, clientAccount.OlWorkgroup.EntityId ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmViewingDays", SqlDbType.Int,clientAccount.OlWorkgroup.ViewingDays ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmMaximumSearchDays", SqlDbType.Int,clientAccount.OlWorkgroup.MaximumSearchDays ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmCheckImageDisplayMode", SqlDbType.TinyInt, clientAccount.OlWorkgroup.CheckImageDisplayMode),
                _databaseManager.BuildSqlParameter("@parmDocumentImageDisplayMode", SqlDbType.TinyInt, clientAccount.OlWorkgroup.DocumentImageDisplayMode),
                _databaseManager.BuildSqlParameter("@parmHOA", SqlDbType.Bit,clientAccount.OlWorkgroup.Hoa),
                _databaseManager.BuildSqlParameter("@parmDisplayBatchID", SqlDbType.Bit,clientAccount.OlWorkgroup.DisplayBatchId ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmInvoiceBalancingOption", SqlDbType.TinyInt, clientAccount.OlWorkgroup.InvoiceBalancingOption),
                _databaseManager.BuildSqlParameter("@parmUserID", SqlDbType.Int, clientAccount.AuditInformation.UserId),
                _databaseManager.BuildSqlParameter("@parmUserDisplayName", SqlDbType.VarChar, clientAccount.AuditInformation.UserDisplayName),
                _databaseManager.BuildSqlParameter("@parmDDAs", SqlDbType.Xml,DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmDataEntryColumns", SqlDbType.Xml,DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmDeadLineDay", SqlDbType.SmallInt,DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmRequiresInvoice", SqlDbType.Bit,DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmBusinessRules", SqlDbType.Xml,DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmFIs", SqlDbType.Xml,GetFIsAsXml(new List<int> {1})),
                _databaseManager.BuildSqlParameter("@parmBillingAccount", SqlDbType.VarChar,clientAccount.OlWorkgroup.BillingAccount ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmBillingField1", SqlDbType.VarChar,clientAccount.OlWorkgroup.BillingField1 ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmBillingField2", SqlDbType.VarChar,clientAccount.OlWorkgroup.BillingField2 ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmBusinessRulesInvoiceRequired", SqlDbType.Bit, clientAccount.WorkgroupBusinessRule?.InvoiceRequired ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmBusinessRulesBalancingRequired", SqlDbType.Bit,clientAccount.WorkgroupBusinessRule?.BalancingRequired ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmBusinessRulesPDBalancingRequired", SqlDbType.Bit,clientAccount.WorkgroupBusinessRule?.PostDepositBalancingRequired ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmBusinessRulesPDPayerRequired", SqlDbType.Bit,clientAccount.WorkgroupBusinessRule?.PostDepositPayerRequired ?? (object) DBNull.Value),
                _databaseManager.BuildSqlParameter("@parmEntityName", SqlDbType.VarChar, clientAccount.AuditInformation.EntityName ?? (object) DBNull.Value),
            };

            return sqlParams;
        }

        private void VerifyDimClientAccount(int clientAccountKey, ClientAccountTestDto expectedResults,  string testName)
        {
            var actualResults = new DataTable();

            _databaseManager.ExecuteSqlCommand(
                $"SELECT SiteBankID, SiteClientAccountID, MostRecent, IsActive, Cutoff, OnlineColorMode, IsCommingled, DataRetentionDays, ImageRetentionDays, ShortName, LongName, POBox, DDA, FileGroup FROM RecHubData.dimClientAccounts WHERE ClientAccountKey = {clientAccountKey}",
                ref actualResults);

            Assert.AreEqual(1, actualResults.Rows.Count, $"{testName} dimClientAccount RowCount");
            Assert.AreEqual(expectedResults.SiteBankId, Convert.ToInt32(actualResults.Rows[0]["SiteBankID"]), $"{testName} SiteBankID");
            Assert.AreEqual(expectedResults.SiteClientAccountId, Convert.ToInt32(actualResults.Rows[0]["SiteClientAccountID"]), $"{testName} SiteClientAccountID");
            Assert.AreEqual(expectedResults.MostRecent, Convert.ToBoolean(actualResults.Rows[0]["MostRecent"]), $"{testName} MostRecent");
            Assert.AreEqual(expectedResults.IsActive, Convert.ToBoolean(actualResults.Rows[0]["IsActive"]), $"{testName} IsActive");
            Assert.AreEqual(expectedResults.Cutoff, Convert.ToBoolean(actualResults.Rows[0]["Cutoff"]), $"{testName} Cutoff");
            Assert.AreEqual(expectedResults.OnlineColorMode, Convert.ToInt32(actualResults.Rows[0]["OnlineColorMode"]), $"{testName} OnlineColorMode");
            Assert.AreEqual(false, Convert.ToBoolean(actualResults.Rows[0]["IsCommingled"]), $"{testName} IsCommingled");
            Assert.AreEqual(expectedResults.DataRetentionDays, Convert.ToInt32(actualResults.Rows[0]["DataRetentionDays"]), $"{testName} DataRetentionDays");
            Assert.AreEqual(expectedResults.ImageRetentionDays, Convert.ToInt32(actualResults.Rows[0]["ImageRetentionDays"]), $"{testName} ImageRetentionDays");
            Assert.AreEqual(expectedResults.ShortName, actualResults.Rows[0]["ShortName"].ToString(), $"{testName} ShortName");
            Assert.AreEqual(expectedResults.LongName, actualResults.Rows[0]["LongName"].ToString().NullIfWhiteSpace(), $"{testName} LongName");
            Assert.AreEqual(expectedResults.PoBox, actualResults.Rows[0]["POBox"].ToString().NullIfWhiteSpace(), $"{testName} POBox");
            Assert.AreEqual(expectedResults.Dda, actualResults.Rows[0]["DDA"].ToString().NullIfWhiteSpace(), $"{testName} DDA");
            Assert.AreEqual(expectedResults.FileGroup, actualResults.Rows[0]["FileGroup"].ToString().NullIfWhiteSpace(), $"{testName} FileGroup");
        }

        private void VerifyDimWorkgroupBusinessRules(int siteBankId, int siteClientAccountId,
            WorkgroupBusinessRuleDto expectedResults, string testName)
        {
            var actualResults = new DataTable();

            _databaseManager.ExecuteSqlCommand(
                $"SELECT InvoiceRequired, BalancingRequired, PostDepositBalancingRequired, PostDepositPayerRequired FROM RecHubData.dimWorkgroupBusinessRules WHERE SiteBankID = {siteBankId} AND SiteClientAccountID = {siteClientAccountId}",
                ref actualResults);

            Assert.AreEqual(1, actualResults.Rows.Count, $"{testName} WorkgroupBusinessRules RowCount");
            Assert.AreEqual(expectedResults.InvoiceRequired, Convert.ToBoolean(actualResults.Rows[0]["InvoiceRequired"]), $"{testName} InvoiceRequired");
            Assert.AreEqual(expectedResults.BalancingRequired, Convert.ToBoolean(actualResults.Rows[0]["BalancingRequired"]), $"{testName} BalancingRequired");
            Assert.AreEqual(expectedResults.PostDepositBalancingRequired, Convert.ToBoolean(actualResults.Rows[0]["PostDepositBalancingRequired"]), $"{testName} PostDepositBalancingRequired");
            Assert.AreEqual(expectedResults.PostDepositPayerRequired, Convert.ToBoolean(actualResults.Rows[0]["PostDepositPayerRequired"]), $"{testName} PostDepositPayerRequired");
        }

        private void VerifyOlWorkgroup(int siteBankId, int siteClientAccountId, OlWorkgroupTestDto expectedResults, string testName)
        {
            var actualResults = new DataTable();

            _databaseManager.ExecuteSqlCommand(
                $"SELECT EntityID, ViewingDays, MaximumSearchDays, CheckImageDisplayMode, DocumentImageDisplayMode, HOA, DisplayBatchID, InvoiceBalancingOption, BillingAccount, BillingField1, BillingField2 FROM RecHubUser.OLWorkgroups WHERE SiteBankID = {siteBankId} AND SiteClientAccountID = {siteClientAccountId}",
                ref actualResults);

            Assert.AreEqual(1, actualResults.Rows.Count, $"{testName} OLWorkgroups RowCount");

            if (expectedResults.EntityId.HasValue)
            { // expecting a value
                if (!string.IsNullOrWhiteSpace(actualResults.Rows[0]["EntityID"].ToString()))
                {
                    int.TryParse(actualResults.Rows[0]["EntityID"].ToString().NullIfWhiteSpace(), out var actualEntityId);
                    Assert.AreEqual((int)expectedResults.EntityId, actualEntityId, $"{testName} EntityID");
                }
                else
                {
                    Assert.Fail($"Expected <{(int)expectedResults.EntityId}>, got <null> {testName} EntityID");
                }
            }
            else
            { // expecting a null value
                if (!string.IsNullOrWhiteSpace(actualResults.Rows[0]["EntityID"].ToString()))
                {
                    int.TryParse(actualResults.Rows[0]["EntityID"].ToString().NullIfWhiteSpace(), out var actualEntityId);
                    Assert.Fail($"Expected <null>, got <{actualEntityId}> {testName} EntityID");
                }
            }

            //this is going to need to be updated to check for an int null or an actual value.
            Assert.AreEqual(expectedResults.ViewingDays, actualResults.Rows[0]["ViewingDays"].ToString().NullIfWhiteSpace(), $"{testName} ViewingDays");
            //this is going to need to be updated to check for an int null or an actual value.
            Assert.AreEqual(expectedResults.MaximumSearchDays, actualResults.Rows[0]["MaximumSearchDays"].ToString().NullIfWhiteSpace(), $"{testName} MaximumSearchDays");
            Assert.AreEqual(expectedResults.CheckImageDisplayMode, Convert.ToInt32(actualResults.Rows[0]["CheckImageDisplayMode"]), $"{testName} CheckImageDisplayMode");
            Assert.AreEqual(expectedResults.DocumentImageDisplayMode, Convert.ToInt32(actualResults.Rows[0]["DocumentImageDisplayMode"]), $"{testName} DocumentImageDisplayMode");
            Assert.AreEqual(expectedResults.Hoa, Convert.ToBoolean(actualResults.Rows[0]["HOA"]), $"{testName} HOA");
            //this is going to need to be updated to check for an bool null or an actual value.
            Assert.AreEqual(expectedResults.DisplayBatchId, actualResults.Rows[0]["DisplayBatchID"].ToString().NullIfWhiteSpace(), $"{testName} DisplayBatchID");
            Assert.AreEqual(expectedResults.InvoiceBalancingOption, Convert.ToInt32(actualResults.Rows[0]["InvoiceBalancingOption"]), $"{testName} InvoiceBalancingOption");
            Assert.AreEqual(expectedResults.BillingAccount, actualResults.Rows[0]["BillingAccount"].ToString().NullIfWhiteSpace(), $"{testName} BillingAccount");
            Assert.AreEqual(expectedResults.BillingField1, actualResults.Rows[0]["BillingField1"].ToString().NullIfWhiteSpace(), $"{testName} BillingField1");
            Assert.AreEqual(expectedResults.BillingField2, actualResults.Rows[0]["BillingField2"].ToString().NullIfWhiteSpace(), $"{testName} BillingField2");

        }

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _databaseManager = new DatabaseManager(_testContext.Properties["AppSettings"].ToString());
            _runDate = DateTime.Now;
            //Create a database for Advanced Search tests
            _databaseManager.CreateDatabase("ClientAccount");
            _recHubAudit = new RecHubAudit(_databaseManager, _runDate);
            _recHubCommon = new RecHubCommon(_databaseManager, _runDate);

            _integrationTest = new IntegrationTest(_databaseManager, _runDate);
            _integrationTest.CreateStoredProcedure("usp_WorkgroupDataEntryColumns_Ins");
            _integrationTest.CreateStoredProcedure("usp_dimWorkgroupBusinessRules_Ins");

            _recHubData = new RecHubData(_databaseManager, _runDate);
            _recHubUser = new RecHubUser(_databaseManager, _runDate);
            _testHelper = new DataLoadHelper(_recHubData, _recHubUser,_integrationTest);

            //Additional RecHubCommon objects
            _recHubCommon.CreateDataType("AuditValueChangeTable");
            _recHubCommon.CreateStoredProcedure("usp_WFS_DataAudit_Ins");
            _recHubCommon.CreateStoredProcedure("usp_WFS_DataAudit_Ins_WithDetails");

            //Additional RecHubData objects
            _recHubData.CreateTable("dimWorkgroupBusinessRules");
            _recHubData.CreateTable("ElectronicAccounts");
            _recHubData.CreateView("dimBanksView");
            _recHubData.CreateView("dimClientAccountsView");
            _recHubData.CreateStoredProcedure("usp_ClientAccountsDataEntryColumns_Merge");
            _recHubData.CreateStoredProcedure("usp_dimClientAccounts_Ins");
            _recHubData.CreateStoredProcedure("usp_dimClientAccounts_Upd");
            _recHubData.CreateStoredProcedure("usp_dimWorkgroupBusinessRules_Merge");
            _recHubData.CreateStoredProcedure("usp_ElectronicAccounts_Merge");

            //Additional RecHubUser objects
            _recHubUser.CreateTable("OLEntities");
            _recHubUser.CreateTable("OLWorkgroups");
            _recHubUser.CreateTable("Users");
            _recHubUser.CreateStoredProcedure("usp_OLWorkgroups_Merge");

            //Additional RecHubAudit objects
            _recHubAudit.CreateTable("factDataAuditDetails");
            _recHubAudit.CreateTable("factDataAuditMessages");
            _recHubAudit.CreateTable("factDataAuditSummary");
            _recHubAudit.CreateStoredProcedure("usp_factDataAuditDetails_Ins");
            _recHubAudit.CreateStoredProcedure("usp_factDataAuditSummary_Ins");
            _recHubAudit.CreateStoredProcedure("usp_factDataAuditMessages_Ins");

            //Add a site code to be used by all the tests
            _testHelper.LoadSiteCodeFile(Path.Combine(_databaseManager.TestDataPath, "AdvancedSearch"), "SiteCode1.json");
            _testHelper.LoadBankFile(Path.Combine(_databaseManager.TestDataPath, "ClientAccount",
                "ClientSetups"), "UpdateAddWGBusinessRules.json");

        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _databaseManager.Disconnect();
        }

        [TestMethod]
        public void ClientAccount_usp_dimClientAccounts_Ins_RequiredFieldsOnly()
        {
            var siteBankId = 1;
            var siteClientAccountId = 789;
            var userId = -1;

            userId = _recHubUser.InsertUser($"RequiredFieldsOnly");

            var clientAccount = new ClientAccountTestDto
            {
                SiteBankId = siteBankId,
                SiteClientAccountId = siteClientAccountId,
                MostRecent = true,
                IsActive = true,
                ShortName = "LockboxShortName",
                DataRetentionDays = 365,
                ImageRetentionDays = 180,
                //FiList = new List<int> {1},
                OlWorkgroup = new OlWorkgroupTestDto
                {
                    CheckImageDisplayMode = 0,
                    DocumentImageDisplayMode = 0,
                    InvoiceBalancingOption = 0
                },
                AuditInformation = new AuditInformationTestDto
                {
                    UserId = userId,
                    UserDisplayName = $"Database Tester{userId}"
                }
            };

            var sqlParams = ClientAccountInsertParams(clientAccount);
            sqlParams.Add(
                _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.Int, clientAccount.SiteBankId));
            sqlParams.Add(_databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.Int,
                clientAccount.SiteClientAccountId));
            sqlParams.Add(_databaseManager.BuildSqlParameter("@parmClientAccountKey", SqlDbType.Int, DBNull.Value, ParameterDirection.Output));
            sqlParams.Add(_databaseManager.BuildSqlParameter("@parmErrorCode", SqlDbType.Int, DBNull.Value, ParameterDirection.Output));

            var success = _recHubData.ExecuteStoredProcedure("usp_dimClientAccounts_Ins", sqlParams.ToArray());
            Assert.AreEqual(true, success);

            var clientAccountKey = Convert.ToInt32(sqlParams.ToArray().FirstOrDefault(p => p.ParameterName == "@parmClientAccountKey")?.Value);

            VerifyDimClientAccount(clientAccountKey, clientAccount, _testContext.TestName);
            VerifyOlWorkgroup(clientAccount.SiteBankId, clientAccount.SiteClientAccountId, clientAccount.OlWorkgroup, _testContext.TestName);

            var businessRulesResults = new DataTable();

            _databaseManager.ExecuteSqlCommand(
                $"SELECT * FROM RecHubData.dimWorkgroupBusinessRules WHERE SiteBankID = {siteBankId} AND SiteClientAccountID = {siteClientAccountId}",
                ref businessRulesResults);

            Assert.AreEqual(0,businessRulesResults.Rows.Count,$"{_testContext.TestName} BusinessRulesCount");
        }

        [TestMethod]
        public void ClientAccount_usp_dimClientAccounts_Ins_WithWGBusinessRules()
        {
            var siteBankId = 1;
            var siteClientAccountId = 910;
            var userId = 6;

            _databaseManager.ExecuteSqlCommand($"SET IDENTITY_INSERT RecHubUser.Users ON;INSERT INTO RecHubUser.Users(UserID,RA3MSID,LogonName) VALUES ({userId},'{Guid.NewGuid()}','DBTester');SET IDENTITY_INSERT RecHubUser.Users OFF;");

            var clientAccount = new ClientAccountTestDto
            {
                SiteBankId = siteBankId,
                SiteClientAccountId = siteClientAccountId,
                MostRecent = true,
                IsActive = true,
                ShortName = "LockboxShortName",
                DataRetentionDays = 365,
                ImageRetentionDays = 180,
                FiList = new List<int> { 1 },
                OlWorkgroup = new OlWorkgroupTestDto
                {
                    CheckImageDisplayMode = 0,
                    DocumentImageDisplayMode = 0,
                    InvoiceBalancingOption = 0
                },
                WorkgroupBusinessRule = new WorkgroupBusinessRuleDto
                {
                    PostDepositPayerRequired = true
                },
                AuditInformation = new AuditInformationTestDto
                {
                    UserId = userId,
                    UserDisplayName = $"Database Tester{userId}"
                }
            };

            var sqlParams = ClientAccountInsertParams(clientAccount);
            sqlParams.Add(
                _databaseManager.BuildSqlParameter("@parmSiteBankID", SqlDbType.Int, clientAccount.SiteBankId));
            sqlParams.Add(_databaseManager.BuildSqlParameter("@parmSiteClientAccountID", SqlDbType.Int,
                clientAccount.SiteClientAccountId));
            sqlParams.Add(_databaseManager.BuildSqlParameter("@parmClientAccountKey", SqlDbType.Int, DBNull.Value, ParameterDirection.Output));
            sqlParams.Add(_databaseManager.BuildSqlParameter("@parmErrorCode", SqlDbType.Int, DBNull.Value, ParameterDirection.Output));

            var success = _recHubData.ExecuteStoredProcedure("usp_dimClientAccounts_Ins", sqlParams.ToArray());
            Assert.AreEqual(true, success);

            var clientAccountKey = Convert.ToInt32(sqlParams.ToArray().FirstOrDefault(p => p.ParameterName == "@parmClientAccountKey")?.Value);

            VerifyDimClientAccount(clientAccountKey, clientAccount, _testContext.TestName);
            VerifyOlWorkgroup(clientAccount.SiteBankId, clientAccount.SiteClientAccountId, clientAccount.OlWorkgroup, _testContext.TestName);
            VerifyDimWorkgroupBusinessRules(clientAccount.SiteBankId, clientAccount.SiteClientAccountId, clientAccount.WorkgroupBusinessRule, _testContext.TestName);
        }

        [TestMethod]
        public void ClientAccount_usp_dimClientAccounts_Upd_RequiredFieldsOnly()
        {
        }

        [TestMethod]
        public void ClientAccount_usp_dimClientAccounts_Upd_AddWGBusinessRules()
        {
            var siteBankId = 1;
            var siteClientAccountId = 234;
            var userId = -1;

            userId = _recHubUser.InsertUser($"AddWGBusinessRules");
            var dimClientAccount = new DataTable();

            _databaseManager.ExecuteSqlCommand(
                $"SELECT ClientAccountKey FROM RecHubData.dimClientAccounts WHERE SiteBankID = {siteBankId} AND SiteClientAccountID = {siteClientAccountId} AND MostRecent = 1",
                ref dimClientAccount);

            var clientAccount = new ClientAccountTestDto
            {
                SiteBankId = siteBankId,
                SiteClientAccountId = siteClientAccountId,
                MostRecent = true,
                IsActive = true,
                ShortName = "LockboxShortName",
                DataRetentionDays = 365,
                ImageRetentionDays = 180,
                Cutoff = true,
                FiList = new List<int> { 1 },
                OlWorkgroup = new OlWorkgroupTestDto
                {
                    CheckImageDisplayMode = 0,
                    DocumentImageDisplayMode = 0,
                    InvoiceBalancingOption = 0,
                    EntityId = 1
                },
                WorkgroupBusinessRule = new WorkgroupBusinessRuleDto
                {
                    PostDepositPayerRequired = true
                },
                AuditInformation = new AuditInformationTestDto
                {
                    UserId = userId,
                    UserDisplayName = $"Database Tester{userId}"
                }
            };

            var sqlParams = ClientAccountInsertParams(clientAccount);
            sqlParams.Add(_databaseManager.BuildSqlParameter("@parmClientAccountKey", SqlDbType.Int, Convert.ToInt32(dimClientAccount.Rows[0]["ClientAccountKey"])));
            sqlParams.Add(_databaseManager.BuildSqlParameter("@parmNewClientAccountKey", SqlDbType.Int, DBNull.Value, ParameterDirection.Output));
            sqlParams.Add(_databaseManager.BuildSqlParameter("@parmRAAMNotificationRequried", SqlDbType.Bit, DBNull.Value, ParameterDirection.Output));

            var success = _recHubData.ExecuteStoredProcedure("usp_dimClientAccounts_Upd", sqlParams.ToArray());

            var newClientAccountKey = Convert.ToInt32(sqlParams.ToArray().FirstOrDefault(p => p.ParameterName == "@parmNewClientAccountKey")?.Value);

            Assert.AreEqual(true, success);
            VerifyDimClientAccount(newClientAccountKey, clientAccount, _testContext.TestName);
            VerifyOlWorkgroup(clientAccount.SiteBankId, clientAccount.SiteClientAccountId, clientAccount.OlWorkgroup, _testContext.TestName);
            VerifyDimWorkgroupBusinessRules(clientAccount.SiteBankId, clientAccount.SiteClientAccountId, clientAccount.WorkgroupBusinessRule, _testContext.TestName);
        }

        [TestMethod]
        public void ClientAccount_usp_dimClientAccounts_Upd_WithWGBusinessRules()
        {
        }

        [TestMethod]
        public void ClientAccount_usp_dimClientAccounts_Get_HubConfiguration()
        {

        }

        [TestMethod]
        public void WorkgroupBusinessRules_Insert_With_usp_dimWorkgroupBusinessRules_Merge()
        {
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@paramSiteBankID", SqlDbType.Int, 1),
                _databaseManager.BuildSqlParameter("@paramSiteClientAccountID", SqlDbType.Int, 456),
                _databaseManager.BuildSqlParameter("@paramInvoiceRequired", SqlDbType.Bit, true),
                _databaseManager.BuildSqlParameter("@paramBalancingRequired", SqlDbType.Bit, false),
                _databaseManager.BuildSqlParameter("@paramPDBalancingRequired", SqlDbType.Bit, false),
                _databaseManager.BuildSqlParameter("@paramPDPayerRequired", SqlDbType.Bit, true)
            };

            _databaseManager.ExecuteStoredProcedure("RecHubData.usp_dimWorkgroupBusinessRules_Merge", sqlParams);

            var dataTable = new DataTable();

            _databaseManager.ExecuteSqlCommand("SELECT InvoiceRequired,BalancingRequired,PostDepositBalancingRequired,PostDepositPayerRequired FROM RecHubData.dimWorkgroupBusinessRules WHERE SiteBankID = 1 and SiteClientAccountID = 456",ref dataTable);

            Assert.IsNotNull(dataTable, $"{_testContext.TestName} Valid result set");
            Assert.AreEqual(1,dataTable.Rows.Count,$"{_testContext.TestName } RowCount");
            Assert.AreEqual("true", dataTable.Rows[0]["InvoiceRequired"].ToString().ToLower(), $"{_testContext.TestName} InvoiceRequired");
            Assert.AreEqual("false", dataTable.Rows[0]["BalancingRequired"].ToString().ToLower(), $"{_testContext.TestName} BalancingRequired");
            Assert.AreEqual("false", dataTable.Rows[0]["PostDepositBalancingRequired"].ToString().ToLower(), $"{_testContext.TestName} PostDepositBalancingRequired");
            Assert.AreEqual("true", dataTable.Rows[0]["PostDepositPayerRequired"].ToString().ToLower(), $"{_testContext.TestName} PostDepositPayerRequired");
        }

        [TestMethod]
        public void WorkgroupBusinessRules_Update_PostDepositPayerRequired_With_usp_dimWorkgroupBusinessRules_Merge()
        {
            _integrationTest.InsertWorkgroupBusinessRule(1, 123, new WorkgroupBusinessRuleDto());
            var sqlParams = new[]
            {
                _databaseManager.BuildSqlParameter("@paramSiteBankID", SqlDbType.Int, 1),
                _databaseManager.BuildSqlParameter("@paramSiteClientAccountID", SqlDbType.Int, 123),
                _databaseManager.BuildSqlParameter("@paramInvoiceRequired", SqlDbType.Bit, false),
                _databaseManager.BuildSqlParameter("@paramBalancingRequired", SqlDbType.Bit, false),
                _databaseManager.BuildSqlParameter("@paramPDBalancingRequired", SqlDbType.Bit, false),
                _databaseManager.BuildSqlParameter("@paramPDPayerRequired", SqlDbType.Bit, true)
            };

            _databaseManager.ExecuteStoredProcedure("RecHubData.usp_dimWorkgroupBusinessRules_Merge", sqlParams);

            var dataTable = new DataTable();

            _databaseManager.ExecuteSqlCommand("SELECT InvoiceRequired,BalancingRequired,PostDepositBalancingRequired,PostDepositPayerRequired FROM RecHubData.dimWorkgroupBusinessRules WHERE SiteBankID = 1 and SiteClientAccountID = 123", ref dataTable);
            Assert.IsNotNull(dataTable, $"{_testContext.TestName} Valid result set");
            Assert.AreEqual(1, dataTable.Rows.Count, $"{_testContext.TestName } RowCount");
            Assert.AreEqual("false", dataTable.Rows[0]["InvoiceRequired"].ToString().ToLower(), $"{_testContext.TestName} InvoiceRequired");
            Assert.AreEqual("false", dataTable.Rows[0]["BalancingRequired"].ToString().ToLower(), $"{_testContext.TestName} BalancingRequired");
            Assert.AreEqual("false", dataTable.Rows[0]["PostDepositBalancingRequired"].ToString().ToLower(), $"{_testContext.TestName} PostDepositBalancingRequired");
            Assert.AreEqual("true", dataTable.Rows[0]["PostDepositPayerRequired"].ToString().ToLower(), $"{_testContext.TestName} PostDepositPayerRequired");
        }
    }
}
