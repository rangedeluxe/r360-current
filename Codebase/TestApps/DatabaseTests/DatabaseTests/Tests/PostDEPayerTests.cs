﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using AutoMapper.Configuration.Conventions;
using DatabaseTestFramework;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Interfaces;
using DatabaseTestFramework.SchemaClasses;
using DatabaseTests.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatabaseTests.Tests
{
    [TestClass]
    public class PostDEPayerTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;

        private static IDatabaseManager _databaseManager;
        private static DateTime _runDate;

        private static IntegrationTest _integrationTest;
        private static RecHubData _recHubData;
        private static RecHubSystem _recHubSystem;
        private static RecHubUser _recHubUser;
        private static DataLoadHelper _testHelper;

        private DataTable GetPayer(int siteBankId, int siteClientAccountId)
        {
            var results = new DataTable();
            var sqlCommand = new StringBuilder();

            sqlCommand.Append("SELECT SiteBankID, SiteClientAccountID, RoutingNumber, Account, PayerName, IsDefault, CreatedBy FROM RecHubData.WorkgroupPayers ");
            sqlCommand.Append($"WHERE SiteBankID = {siteBankId} AND SiteClientAccountID = {siteClientAccountId}");
            _databaseManager.ExecuteSqlCommand(sqlCommand.ToString(), ref results);

            return results;

        }

        private void InsertPayer(int siteBankId, int siteClientAccountId, string routingNumber, string account, string payerName,
            string createdBy, bool isDefault = false)
        {
            var sqlCommand = new StringBuilder();

            sqlCommand.Append("INSERT INTO RecHubData.WorkgroupPayers(SiteBankID, SiteClientAccountID, RoutingNumber, Account, PayerName, IsDefault, CreatedBy)");
            sqlCommand.Append($"VALUES({siteBankId}, {siteClientAccountId}, '{routingNumber}', '{account}', '{payerName}', {(isDefault ? 1 : 0)}, '{createdBy}')");
            _databaseManager.ExecuteSqlCommand(sqlCommand.ToString());
        }

        private void VerifyRow(List<PostDePayerDto> expectedResults, List<PostDePayerDto> actualResults,
            string testName)
        {
            Assert.AreEqual(expectedResults.Count, actualResults.Count, $"{testName} - RowCount");
            for (int i = 0; i < expectedResults.Count; i++)
            {
                Assert.AreEqual(expectedResults[i].RoutingNumber, actualResults[i].RoutingNumber, $"{TestContext.TestName} - RoutingNumber");
                Assert.AreEqual(expectedResults[i].Account, actualResults[i].Account, $"{TestContext.TestName} - Account");
                Assert.AreEqual(expectedResults[i].PayerName, actualResults[i].PayerName, $"{TestContext.TestName} - PayerName");
                Assert.AreEqual(expectedResults[i].CreatedBy, actualResults[i].CreatedBy, $"{TestContext.TestName} - CreatedBy");
                Assert.AreEqual(expectedResults[i].IsDefault, actualResults[i].IsDefault, $"{TestContext.TestName} - IsDefault");

            }
        }

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _databaseManager = new DatabaseManager(_testContext.Properties["AppSettings"].ToString());
            _runDate = DateTime.Now;
            //Create a database for Post Deposit Exceptions Payer tests
            _databaseManager.CreateDatabase("PostDEPayer");

            var recHubAudit = new RecHubAudit(_databaseManager, _runDate);
            var recHubCommon = new RecHubCommon(_databaseManager, _runDate);
            _integrationTest = new IntegrationTest(_databaseManager, _runDate);
            _recHubData = new RecHubData(_databaseManager, _runDate);
            _recHubSystem = new RecHubSystem(_databaseManager, _runDate);
            _recHubUser = new RecHubUser(_databaseManager, _runDate);

            _testHelper = new DataLoadHelper(_recHubData, _recHubUser, _integrationTest);

            //Additional RecHubCommon objects
            recHubCommon.CreateStoredProcedure("usp_WFS_DataAudit_Ins");

            //Additional RecHubData objects
            _recHubData.CreateTable("WorkgroupPayers");
            _recHubData.CreateStoredProcedure("usp_WorkgroupPayers_Upsert");
            _recHubData.CreateStoredProcedure("usp_WorkgroupPayers_Get");

            //Additional RecHubUser objects
            _recHubUser.CreateTable("OLEntities");
            _recHubUser.CreateTable("OLWorkgroups");
            _recHubUser.CreateTable("Users");

            //Additional RecHubSystem objects
            _recHubSystem.CreateSequence("ReceivablesBatchID");

            //The audit tables have a references to the RecHubUser.User table.
            //Additional RecHubAudit objects
            recHubAudit.CreateTable("factDataAuditSummary");
            recHubAudit.CreateTable("factDataAuditMessages");
            recHubAudit.CreateStoredProcedure("usp_dimAuditApplications_Get");
            recHubAudit.CreateStoredProcedure("usp_factDataAuditSummary_Ins");
            recHubAudit.CreateStoredProcedure("usp_factDataAuditMessages_Ins");
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _databaseManager.Disconnect();
        }

        [TestMethod]
        public void PostDEPayer_Insert()
        {
            var userId = _recHubUser.InsertUser("Test1");

            _recHubData.InsertPayer(1, 123, "743675941", "60325407", "Test1", userId, "J. Tester", false);

            var results = GetPayer(1, 123);

            Assert.AreEqual(1,results.Rows.Count, $"{TestContext.TestName} - RowCount");
            Assert.AreEqual("743675941", (string)results.Rows[0].ItemArray[results.Columns.IndexOf("RoutingNumber")], $"{TestContext.TestName} - RoutingNumber");
            Assert.AreEqual("60325407", (string)results.Rows[0].ItemArray[results.Columns.IndexOf("Account")], $"{TestContext.TestName} - Account");
            Assert.AreEqual("J. Tester", (string)results.Rows[0].ItemArray[results.Columns.IndexOf("CreatedBy")], $"{TestContext.TestName} - CreatedBy");
        }

        [TestMethod]
        public void PostDEPayer_GetOneItemNoDefault()
        {
            InsertPayer(1,5, "104000016","123","PayerOne","T. Tester");
            var payerList = _recHubData.GetPayer(1, 5, "104000016", "123");

            var expectedResults = new List<PostDePayerDto>()
            {
                new PostDePayerDto
                {
                    RoutingNumber = "104000016",
                    Account = "123",
                    PayerName = "PayerOne",
                    CreatedBy = "T. Tester"
                }
            };

            VerifyRow(expectedResults, payerList, TestContext.TestName);
        }
    }
}
