﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DatabaseTestFramework;
using DatabaseTestFramework.Interfaces;
using DatabaseTestFramework.SchemaClasses;
using DatabaseTestFramework.DTO;
using DatabaseTestFramework.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatabaseTests.Tests
{
    [TestClass]
    public class BatchSummaryTest
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;

        private static IDatabaseManager _databaseManager;
        private static DateTime _runDate;

        private static IntegrationTest _integrationTest;
        private static RecHubData _recHubData;
        private static RecHubSystem _recHubSystem;
        private static RecHubUser _recHubUser;
        private static DataLoadHelper _testHelper;

        private static int _userId;
        private static Guid _sessionId;

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _databaseManager = new DatabaseManager(_testContext.Properties["AppSettings"].ToString());
            _runDate = DateTime.Now;
            //Create a database for Advanced Search tests
            _databaseManager.CreateDatabase("BatchSummary");
            var recHubCommon = new RecHubCommon(_databaseManager, _runDate);

            _integrationTest = new IntegrationTest(_databaseManager, _runDate);
            _recHubData = new RecHubData(_databaseManager, _runDate);
            _recHubSystem = new RecHubSystem(_databaseManager, _runDate);
            _recHubUser = new RecHubUser(_databaseManager, _runDate);
            _testHelper = new DataLoadHelper(_recHubData, _recHubUser, _integrationTest);

            //Additional RecHubData objects
            _recHubData.CreateTable("factDataEntryDetails");
            _recHubData.CreateTable("factAdvancedSearch");
            _recHubData.CreateTable("factBatchSummary");
            _recHubData.CreateTable("factTransactionSummary");
            _recHubData.CreateTable("factChecks");
            _recHubData.CreateTable("factStubs");
            _recHubData.CreateStoredProcedure("usp_factBatchSummary_Get");

            //Additional RecHubSystem objects
            _recHubSystem.CreateSequence("ReceivablesBatchID");

            //Additional RecHubUser objects
            _recHubUser.CreateStoredProcedure("usp_AdjustStartDateForViewingDays");
            _recHubUser.CreateTable("Users");
            _recHubUser.CreateTable("OLEntities");
            _recHubUser.CreateTable("OLWorkgroups");
            _userId = _recHubUser.InsertUser("DatabaseTester1");

            //Additional IntegrationTest objects
            _integrationTest.CreateStoredProcedure("usp_factAdvancedSearch_InsCots");
            _integrationTest.CreateStoredProcedure("usp_factAdvancedSearch_Ins_WithDataEntry");
            _integrationTest.CreateStoredProcedure("usp_factAdvancedSearch_Ins_NoDataEntry");
            _integrationTest.CreateStoredProcedure("usp_factAdvancedSearch_Ins");
            _integrationTest.CreateStoredProcedure("usp_factBatchSummary_Ins");
            _integrationTest.CreateStoredProcedure("usp_factChecks_Ins");
            _integrationTest.CreateStoredProcedure("usp_factTransactionSummary_Ins");

            _integrationTest.CreateStoredProcedure("usp_AddDataEntryColumn");
            _integrationTest.CreateStoredProcedure("usp_fact_Upd_DataEntry");


            //Load data for this test class
            _testHelper.LoadBatchSourceFile(Path.Combine(_databaseManager.TestDataPath, "BatchSummary"), "BatchSources.json");
            _testHelper.LoadBankFile(Path.Combine(_databaseManager.TestDataPath, "BatchSummary"), "Bank*.json");
            _testHelper.LoadBatches(Path.Combine(_databaseManager.TestDataPath, "BatchSummary"), "Batches*.json");

            _sessionId = Guid.NewGuid();

            _integrationTest.InsertSessionClientAccountEntitlements(new List<SessionClientAccountEntitlementsDto>()
            {
                new SessionClientAccountEntitlementsDto
                {
                    SessionId = _sessionId,
                    SiteBankId = 1,
                    SiteClientAccountId = 4533,
                    EntityName = "integraPAY",
                    EntityId = 40,
                    StartDateKey = _runDate.ToDateKey()
                }
            });

        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _databaseManager.Disconnect();
        }

        [TestMethod]
        public void BatchSummary_GrandTotals()
        {

            var results = _recHubData.GetBatchSummary(new GetBatchSummaryRequest
                {
                    BankId = 1,
                    ClientAccountId = 4533,
                    StartDate = _runDate,
                    EndDate = _runDate,
                    OrderBy = string.Empty,
                    OrderDirection = "asc",
                    Search = string.Empty,
                    Start = 100000,
                    PageLength = 10,
                    SessionId = _sessionId
                });

            Assert.AreEqual(0,results.BatchSummaryData.Rows.Count,$"{TestContext.TestName} - RowCount");
            Assert.AreEqual(1, results.TotalRecords, $"{TestContext.TestName} - TotalRecords");
            Assert.AreEqual(1, results.FilteredRecords, $"{TestContext.TestName} - FilteredRecords");
            Assert.AreEqual(4, results.TransactionCount, $"{TestContext.TestName} - TransactionCount");
            Assert.AreEqual(6, results.CheckCount, $"{TestContext.TestName} - CheckCount");
            Assert.AreEqual(0, results.DocumentCount, $"{TestContext.TestName} - DocumentCount");
            Assert.AreEqual(36701.39M, results.CheckTotal, $"{TestContext.TestName} - CheckTotal");
        }
    }
}
