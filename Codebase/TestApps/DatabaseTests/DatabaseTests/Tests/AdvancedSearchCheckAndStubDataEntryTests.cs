﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseTestFramework.DTO;
using DatabaseTests.CommonObjects;

namespace DatabaseTests.AdvancedSearch
{
    [TestClass]
    public class AdvancedSearchCheckAndStubDataEntryTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static AdvancedSearchDataContext _dataContext;
        private static short _batchSourceKey;
        private static byte _paymentTypeKey;
        private static Guid _sessionId;

#if OldAdvSearch
        private const string CheckTestDateColumnName = "TestDate";
        private const string CheckTestFloatColumnName = "TestFloat";
        private const string CheckTestMoneyColumnName = "TestMoney";
        private const string CheckTestStringColumnName = "TestString";
        private const string StubAmountColumnName = "Amount";
        private const string StubAccountNumberColumnName = "AccountNumber";
        private const string StubTestDateColumnName = "TestDate";
        private const string StubTestFloatColumnName = "TestFloat";
        private const string StubTestMoneyColumnName = "TestMoney";
        private const string StubTestStringColumnName = "TestString";
        private const int ReturnedDocumentCount = 48;
#else
        private const string CheckTestDateColumnName = "TestDate_1_11";
        private const string CheckTestFloatColumnName = "TestFloat_1_6";
        private const string CheckTestMoneyColumnName = "TestMoney_1_7";
        private const string CheckTestStringColumnName = "TestString_1_1";
        private const string StubAmountColumnName = "StubAmount";
        private const string StubAccountNumberColumnName = "StubAccountNumber";
        private const string StubTestDateColumnName = "TestDate_0_11";
        private const string StubTestFloatColumnName = "TestFloat_0_6";
        private const string StubTestMoneyColumnName = "TestMoney_0_7";
        private const string StubTestStringColumnName = "TestString_0_1";
        private const int ReturnedDocumentCount = 27;
#endif

        private static readonly List<AdvancedSearchWhereClauseDto> SelectAllCheckFields =
            new List<AdvancedSearchWhereClauseDto>()
            {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime
                }
            };

        private static readonly List<AdvancedSearchWhereClauseDto> SelectAllStubFields = new List<AdvancedSearchWhereClauseDto>()
        {
           new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = "TestString",
                ReportTitle = "Test String",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = "TestFloat",
                ReportTitle = "Test Float",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = "TestMoney",
                ReportTitle = "Test Money",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = "TestDate",
                ReportTitle = "Test Date",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = StubAccountNumberColumnName,
                ReportTitle = "Account Number",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
            },
            new AdvancedSearchWhereClauseDto()
            {
                BatchSourceKey = _batchSourceKey,
                Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                IsDataEntry = true,
                FieldName = StubAmountColumnName,
                ReportTitle = "Amount",
                DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
            }
        };

        //private static List<AdvancedSearchWhereClauseDto> SelectAllFields = SelectAllCheckFields;
       private static readonly List<AdvancedSearchWhereClauseDto> SelectAllFields = SelectAllCheckFields.Concat(SelectAllStubFields).ToList();
       /*
               private static List<AdvancedSearchWhereClauseDto> SelectAllFields = new List<AdvancedSearchWhereClauseDto>()
               {
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                       IsDataEntry = true,
                       FieldName = "TestString",
                       ReportTitle = "Test String",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
                   },
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                       IsDataEntry = true,
                       FieldName = "TestFloat",
                       ReportTitle = "Test Float",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float
                   },
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                       IsDataEntry = true,
                       FieldName = "TestMoney",
                       ReportTitle = "Test Money",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
                   },
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                       IsDataEntry = true,
                       FieldName = "TestDate",
                       ReportTitle = "Test Date",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime
                   },
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                       IsDataEntry = true,
                       FieldName = "TestString",
                       ReportTitle = "Test String",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
                   },
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                       IsDataEntry = true,
                       FieldName = "TestFloat",
                       ReportTitle = "Test Float",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float
                   },
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                       IsDataEntry = true,
                       FieldName = "TestMoney",
                       ReportTitle = "Test Money",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
                   },
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                       IsDataEntry = true,
                       FieldName = "TestDate",
                       ReportTitle = "Test Date",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime
                   },
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                       IsDataEntry = true,
                       FieldName = StubAccountNumberColumnName,
                       ReportTitle = "Account Number",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String
                   },
                   new AdvancedSearchWhereClauseDto()
                   {
                       BatchSourceKey = _batchSourceKey,
                       Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                       IsDataEntry = true,
                       FieldName = StubAmountColumnName,
                       ReportTitle = "Amount",
                       DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal
                   }
               };
       */
       [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _dataContext = new AdvancedSearchDataContext(_testContext.Properties["AppSettings"].ToString(), "AdvancedSearchCheckAndStubDataEntry");
            _dataContext.LoadBatches("CheckAndStubDataEntry.json");
            _batchSourceKey = _dataContext.RecHubData.GetBatchSourceKey("ImageRPS");
            _paymentTypeKey = _dataContext.RecHubData.GetPaymentTypeKey("Lockbox");
            _sessionId = Guid.NewGuid();
            _dataContext.InsertSessionClientAccountEntitlements(_sessionId);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _dataContext.Disconnect();
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_AllItems()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 24, 24, 75547.6700M, ReturnedDocumentCount, _testContext.TestName);
            Assert.AreEqual(24, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(6, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today is correct");
            Assert.AreEqual(18, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday is correct");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_CheckFieldsOnly_AllItems()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllCheckFields;

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 30, 24, 75547.6700M, ReturnedDocumentCount, _testContext.TestName);
            Assert.AreEqual(30, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(34, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            Assert.AreEqual(8, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today is correct");
            Assert.AreEqual(22, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday is correct");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_StubFieldsOnly_FilterByCheckAmountEqualsx()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllCheckFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>()
            {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "Amount",
                    ReportTitle = "Check Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "150.01"
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 150.0100M, 5, _testContext.TestName);
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(34, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_StubFieldsOnly_AllItems()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllStubFields;

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 48, 24, 75547.6700M, ReturnedDocumentCount, _testContext.TestName);
            Assert.AreEqual(48, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(36, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            Assert.AreEqual(22, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today is correct");
            Assert.AreEqual(26, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday is correct");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_StubFieldsOnly_FilterByStubAmountEquals()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllStubFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>()
            {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "22.22"
                }
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 10, 2, 627.7800M, ReturnedDocumentCount, _testContext.TestName);
            Assert.AreEqual(10, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(36, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            Assert.AreEqual(0, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.Date).Count(), $"{_testContext.TestName} - Count of items dated today is correct");
            Assert.AreEqual(10, dtResults.AsEnumerable().Where(r => DateTime.Parse((string)r["Deposit_Date"]) == _dataContext.RunDate.AddDays(-1).Date).Count(), $"{_testContext.TestName} - Count of items dated yesterday is correct");
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_ValidateAllFieldsReturned()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses.Clear();


            var ResultStubsAccountNumberValueList = new List<string>()
            {
                "123456789",
                "2079900184489",
                "2079900184689",
                "2079900184989",
                "2079900188889",
                "2079900189589",
                "2079900195289",
                "2079900208589",
                "2079900219689",
                "2079900221489",
                "2079900226989",
                "2079900227789",
                "2079900230989",
                "223456789",
                "321654987",
                "456123789",
                "64292703",
                "654321987",
                "789456123",
                "96725340",
                "987654321",
                "BTK5UFWHV",
                "DMJOSJ6P7",
                "EUL2U5ONX",
                "JM0SY5EOW",
                "LS70FPO7H"
            };
            var ResultStubsAmountValueList = new List<decimal>()
            {
                10.00M,
                100.00M,
                10000.00M,
                1078.03M,
                1388.39M,
                177.65M,
                2000.00M,
                20670.65M,
                22.22M,
                2247.54M,
                44.44M,
                455.71M,
                4568.21M,
                5000.00M,
                6440.43M,
                658.94M,
                66.66M,
                7000.00M,
                76.26M,
                800.00M,
                837.99M,
                88.27M,
                9000.00M,
                91.77M
            };
            var ResultStubsTestStringValueList = new List<string>()
            {
                "Balanced Multi-Check",
                "Balanced Multi-Multi",
                "Balanced Single-Single",
                "Test String 2",
                "Test String 3",
                "Unbalanced Multi-Multi",
                "Unbalanced Single-Single"
            };
            var ResultStubsTestFloatValueList = new List<float>()
            {
                1.1111f,
                1.47653f,
                1.999f,
                2.2222f,
                2.999f,
                3.14159f,
                3.999f,
                4.3322f,
                4.999f,
                49.4328f,
                5.999f,
                6.999f,
                7.999f,
                8.999f,
                9.111f,
                9.222f,
                9.2783f,
                9.3423f,
                9.999f
            };
            var ResultStubsTestMoneyValueList = new List<decimal>()
            {
                0.00M,
                0.25M,
                1388.39M,
                147.59M,
                15.00M,
                177.65M,
                2.50M,
                2247.54M,
                25.00M,
                35.00M,
                45.00M,
                455.71M,
                7891.77M,
                837.99M
            };
            var ResultStubsTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1),
                new DateTime(2019,8,10),
                new DateTime(2019,8,30),
                new DateTime(2019,8,31),
                new DateTime(2019,9,1),
                new DateTime(2019,9,2),
                new DateTime(2019,9,4),
                new DateTime(2019,9,5),
                new DateTime(2019,10,1),
                new DateTime(2019,10,2),
                new DateTime(2019,10,3),
                new DateTime(2019,10,4),
                new DateTime(2019,10,5)
            };
            var ResultChecksTestStringValueList = new List<string>()
            {
                "Balanced Multi-Check",
                "Balanced Single-Single",
                "Simple",
                "Test",
                "Unbalanced Multi-Multi",
                "Unbalanced Single-Single"
            };
            var ResultChecksTestFloatValueList = new List<float>()
            {
                1.1111f,
                1.999f,
                2.2222f,
                2.999f,
                3.14159f,
                3.999f,
                4.3322f,
                4.999f,
                49.4328f,
                7.999f,
                8.999f,
                9.111f,
                9.222f,
                9.2783f,
                9.3423f
            };
            var ResultChecksTestMoneyValueList = new List<decimal>()
            {
                0.00M,
                0.25M,
                1388.39M,
                147.59M,
                15.00M,
                177.65M,
                2247.54M,
                25.00M,
                45.00M,
                455.71M,
                837.99M
            };
            var ResultChecksTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,1,1),
                new DateTime(2019,8,10),
                new DateTime(2019,8,30),
                new DateTime(2019,8,31),
                new DateTime(2019,9,1),
                new DateTime(2019,9,2),
                new DateTime(2019,9,4),
                new DateTime(2019,9,5),
                new DateTime(2019,10,1),
                new DateTime(2019,10,2),
                new DateTime(2019,10,3),
                new DateTime(2019,10,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 72, 24, 75547.6700M, ReturnedDocumentCount, _testContext.TestName);
            Assert.AreEqual(72, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(40, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);

            TestHelper.ValidateStringFieldValuesFromList(ResultStubsAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStubsTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultStubsTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultStubsTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultChecksTestStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultChecksTestFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultChecksTestMoneyValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultChecksTestDateValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_FilterByCheckDateRangeAndStubAmount()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "09/04/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/06/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAmountColumnName,
                    ReportTitle = "Amount",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "100.00"
                }
            };

            var ResultStubsAccountNumberValueList = new List<string>()
            {
                "2079900230989"
            };
            var ResultStubsAmountValueList = new List<decimal>()
            {
                100.00M
            };
            var ResultStubsTestStringValueList = new List<string>()
            {
                "Unbalanced Single-Single"
            };
            var ResultStubsTestFloatValueList = new List<float>()
            {
                9.3423f
            };
            var ResultStubsTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultStubsTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,5)
            };
            var ResultChecksTestStringValueList = new List<string>()
            {
                "Unbalanced Single-Single"
            };
            var ResultChecksTestFloatValueList = new List<float>()
            {
                9.3423f
            };
            var ResultChecksTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultChecksTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 240.0500M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 240.0500M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(40, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);

            TestHelper.ValidateStringFieldValuesFromList(ResultStubsAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStubsTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultStubsTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultStubsTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultChecksTestStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultChecksTestFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultChecksTestMoneyValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultChecksTestDateValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_FilterByStubAccountNumberAndCheckMoney()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = StubAccountNumberColumnName,
                    ReportTitle = "Account Number",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Equals,
                    Value = "223456789"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "0.20"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "0.50"
                }
            };

            var ResultStubsAccountNumberValueList = new List<string>()
            {
                "223456789"
            };
            var ResultStubsAmountValueList = new List<decimal>()
            {
                1078.03M
            };
            var ResultStubsTestStringValueList = new List<string>()
            {
                "Balanced Multi-Check"
            };
            var ResultStubsTestFloatValueList = new List<float>()
            {
                6.999f
            };
            var ResultStubsTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultStubsTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,5)
            };
            var ResultChecksTestStringValueList = new List<string>()
            {
                "Unbalanced Multi-Multi"
            };
            var ResultChecksTestFloatValueList = new List<float>()
            {
                3.999f,
                4.999f
            };
            var ResultChecksTestMoneyValueList = new List<decimal>()
            {
                0.00M,
                0.25M
            };
            var ResultChecksTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,10,3),
                new DateTime(2019,10,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 2, 1078.0300M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 4, 2, 1078.0300M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(4, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(40, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);

            TestHelper.ValidateStringFieldValuesFromList(ResultStubsAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStubsTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultStubsTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultStubsTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultChecksTestStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultChecksTestFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultChecksTestMoneyValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultChecksTestDateValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_FilterByStubDateRangeAndCheckString()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "08/31/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/03/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.Contains,
                    Value = "Single"
                }
            };

            var ResultStubsAccountNumberValueList = new List<string>()
            {
                "EUL2U5ONX",
                "64292703"
            };
            var ResultStubsAmountValueList = new List<decimal>()
            {
                6440.43M,
                4568.21M
            };
            var ResultStubsTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultStubsTestFloatValueList = new List<float>()
            {
                7.999f,
                8.999f
            };
            var ResultStubsTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultStubsTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,1),
                new DateTime(2019,9,2)
            };
            var ResultChecksTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultChecksTestFloatValueList = new List<float>()
            {
                7.999f,
                8.999f
            };
            var ResultChecksTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultChecksTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,1),
                new DateTime(2019,9,2)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 2, 2, 11008.6400M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 2, 2, 11008.6400M, 2, _testContext.TestName);
#endif
            Assert.AreEqual(2, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(40, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);

            TestHelper.ValidateStringFieldValuesFromList(ResultStubsAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStubsTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultStubsTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultStubsTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultChecksTestStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultChecksTestFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultChecksTestMoneyValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultChecksTestDateValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_FilterByStubStringAndCheckFloatRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestString",
                    ReportTitle = "Test String",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.String,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.BeginsWith,
                    Value = "Unbalanced"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "9.30"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "9.40"
                }
            };

            var ResultStubsAccountNumberValueList = new List<string>()
            {
                "2079900230989"
            };
            var ResultStubsAmountValueList = new List<decimal>()
            {
                100.00M
            };
            var ResultStubsTestStringValueList = new List<string>()
            {
                "Unbalanced Single-Single"
            };
            var ResultStubsTestFloatValueList = new List<float>()
            {
                9.3423f
            };
            var ResultStubsTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultStubsTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,5)
            };
            var ResultChecksTestStringValueList = new List<string>()
            {
                "Unbalanced Single-Single"
            };
            var ResultChecksTestFloatValueList = new List<float>()
            {
                9.3423f
            };
            var ResultChecksTestMoneyValueList = new List<decimal>()
            {
                25.00M
            };
            var ResultChecksTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,5)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 240.0500M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 240.0500M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(40, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);

            TestHelper.ValidateStringFieldValuesFromList(ResultStubsAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStubsTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultStubsTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultStubsTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultChecksTestStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultChecksTestFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultChecksTestMoneyValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultChecksTestDateValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_FilterByCheckDateRangeAndStubMoney()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "07/31/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestDate",
                    ReportTitle = "Test Date",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.DateTime,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "09/01/2019"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "40.00"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestMoney",
                    ReportTitle = "Test Money",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Decimal,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "50.00"
                }
            };

            var ResultStubsAccountNumberValueList = new List<string>()
            {
                "JM0SY5EOW"
            };
            var ResultStubsAmountValueList = new List<decimal>()
            {
                88.27M
            };
            var ResultStubsTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultStubsTestFloatValueList = new List<float>()
            {
                9.222f
            };
            var ResultStubsTestMoneyValueList = new List<decimal>()
            {
                45.00M
            };
            var ResultStubsTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,8,30)
            };
            var ResultChecksTestStringValueList = new List<string>()
            {
                "Balanced Single-Single"
            };
            var ResultChecksTestFloatValueList = new List<float>()
            {
                9.222f
            };
            var ResultChecksTestMoneyValueList = new List<decimal>()
            {
                45.00M
            };
            var ResultChecksTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,8,30)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 88.2700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 1, 1, 88.2700M, 1, _testContext.TestName);
#endif
            Assert.AreEqual(1, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(40, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);

            TestHelper.ValidateStringFieldValuesFromList(ResultStubsAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStubsTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultStubsTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultStubsTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultChecksTestStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultChecksTestFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultChecksTestMoneyValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultChecksTestDateValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }

        [TestMethod, TestCategory("Advanced Search")]
        public void CheckAndStubDataEntry_FilterByCheckFloatRangeAndStubFloatRange()
        {
            var request = new AdvancedSearchRequestDto
            {
                BankId = 1,
                WorkgroupId = 1,
                DepositDateFrom = new DateTime(1980, 1, 1),
                DepositDateTo = new DateTime(2050, 12, 31),
                PaymentSourceKey = _batchSourceKey,
                PaymentTypeKey = _paymentTypeKey,
                SessionId = _sessionId,
                Paginate = false
            };
            request.SelectFields = SelectAllFields;
            request.WhereClauses = new List<AdvancedSearchWhereClauseDto>() {
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "4.30"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Checks,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "4.40"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                    Value = "1.0"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = _batchSourceKey,
                    Table = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseTable.Stubs,
                    IsDataEntry = true,
                    FieldName = "TestFloat",
                    ReportTitle = "Test Float",
                    DataType = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseDataType.Float,
                    Operator = AdvancedSearchWhereClauseDto.AdvancedSearchWhereClauseOperator.IsLessThan,
                    Value = "2.0"
                }
            };

            var ResultStubsAccountNumberValueList = new List<string>()
            {
                "2079900184689",
                "LS70FPO7H",
                "BTK5UFWHV",
                "2079900188889"
            };
            var ResultStubsAmountValueList = new List<decimal>()
            {
                91.77M,
                800.00M,
                2000.00M,
                5000.00M
            };
            var ResultStubsTestStringValueList = new List<string>()
            {
                "Balanced Multi-Multi"
            };
            var ResultStubsTestFloatValueList = new List<float>()
            {
                1.47653f
            };
            var ResultStubsTestMoneyValueList = new List<decimal>()
            {
                7891.77M
            };
            var ResultStubsTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,9,1)
            };
            var ResultChecksTestStringValueList = new List<string>()
            {
                "Balanced Multi-Check"
            };
            var ResultChecksTestFloatValueList = new List<float>()
            {
                4.3322f
            };
            var ResultChecksTestMoneyValueList = new List<decimal>()
            {
                1388.39M
            };
            var ResultChecksTestDateValueList = new List<DateTime>()
            {
                new DateTime(2019,10,4)
            };

            DataTable dtResults;
            AdvSearchResults asResults;

            _dataContext.RecHubData.ExecuteAdvancedSearch(request, out asResults, out dtResults);

#if OldAdvSearch
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, ReturnedDocumentCount, _testContext.TestName);
#else
            TestHelper.ValidateSearchResultCountsAndTotals(asResults, 8, 2, 7891.7700M, 4, _testContext.TestName);
#endif
            Assert.AreEqual(8, dtResults.Rows.Count, $"{_testContext.TestName} - DataTable Row Count");
            Assert.AreEqual(40, dtResults.Columns.Count, $"{_testContext.TestName} - DataTable Column Count");
            TestHelper.ValidateResultFieldsExist(request.SelectFields, asResults, dtResults, _testContext.TestName);

            TestHelper.ValidateStringFieldValuesFromList(ResultStubsAccountNumberValueList, asResults, dtResults, "Stubs", StubAccountNumberColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsAmountValueList, asResults, dtResults, "Stubs", StubAmountColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultStubsTestStringValueList, asResults, dtResults, "Stubs", StubTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultStubsTestFloatValueList, asResults, dtResults, "Stubs", StubTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultStubsTestMoneyValueList, asResults, dtResults, "Stubs", StubTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultStubsTestDateValueList, asResults, dtResults, "Stubs", StubTestDateColumnName, _testContext.TestName);
            TestHelper.ValidateStringFieldValuesFromList(ResultChecksTestStringValueList, asResults, dtResults, "Checks", CheckTestStringColumnName, _testContext.TestName);
            TestHelper.ValidateFloatFieldValuesFromList(ResultChecksTestFloatValueList, asResults, dtResults, "Checks", CheckTestFloatColumnName, _testContext.TestName);
            TestHelper.ValidateMoneyFieldValuesFromList(ResultChecksTestMoneyValueList, asResults, dtResults, "Checks", CheckTestMoneyColumnName, _testContext.TestName);
            TestHelper.ValidateDateTimeFieldValuesFromList(ResultChecksTestDateValueList, asResults, dtResults, "Checks", CheckTestDateColumnName, _testContext.TestName);
        }


    }
}
