﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DatabaseTestCommon.Helpers
{
    public class BatchDateConverter : JsonConverter 
    {
        public static string CurrentDateTag = "@CURRENTDATE";
        public static DateTime BaseDateForCurrentDate = DateTime.Today;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is DateTime)
            {
                int difference = Math.Abs((BaseDateForCurrentDate - ((DateTime)value).Date).Days);
                var tag = CurrentDateTag;
                if (difference > 0)
                {
                    if ((DateTime)value < BaseDateForCurrentDate)
                        tag += $"-{difference}";
                    else if ((DateTime)value > BaseDateForCurrentDate)
                        tag += $"+{difference}";
                }
                writer.WriteValue(tag);
            }
            else
                throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var batchDate = DateTime.Now;

            var batchDateString = reader.Value.ToString();
            if (batchDateString.ToUpper().Contains(CurrentDateTag))
            {
                string[] modifier;
                if ((modifier = batchDateString.Split('+')).Length == 2)
                {
                    if( int.TryParse(modifier[1],out var days))
                        batchDate = batchDate.AddDays(days);
                }
                if ((modifier = batchDateString.Split('-')).Length == 2)
                {
                    if (int.TryParse(modifier[1], out var days))
                        batchDate = batchDate.AddDays(-days);
                }
            }
            else
                DateTime.TryParse(batchDateString, out batchDate);

            return batchDate;
        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }
    }
}
