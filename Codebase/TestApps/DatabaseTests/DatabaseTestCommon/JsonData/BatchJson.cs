﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using DatabaseTestCommon.Helpers;

namespace DatabaseTestCommon.JsonData
{
    public class BatchJson
    {
        public Int64 BatchId { get; set; }
        [JsonConverter(typeof(BatchDateConverter))]
        public DateTime DepositDate { get; set; }
        [JsonConverter(typeof(BatchDateConverter))]
        public DateTime ImmutableDate { get; set; }
        [JsonConverter(typeof(BatchDateConverter))]
        public DateTime SourceProcessingDate { get; set; }
        public int SourceBatchId { get; set; }
        public int BatchNumber { get; set; }
        public int? BatchCueId { get; set; }
        public int? SystemType { get; set; }
        public int? BatchSiteCode { get; set; }
        public string DepositDDA { get; set; } = null;
        public int? SiteBankId { get; set; }
        public int? SiteClientAccountId { get; set; }
        public string BatchSource { get; set; } = null;
        public string PaymentType { get; set; } = null;
        public string BatchExceptionStatus { get; set; } = null;
        public string DepositStatusName { get; set; } = null;
        public string TransactionExceptionStatus { get; set; } = null;
        public List<TransactionJson> Transactions { get; set; }
    }
}
