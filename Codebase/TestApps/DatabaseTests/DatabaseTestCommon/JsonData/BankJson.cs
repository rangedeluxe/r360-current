﻿using System.Collections.Generic;

namespace DatabaseTestCommon.JsonData
{
    public class BankJson
    {
        public int? SiteCodeId { get; set; }
        public int SiteBankId { get; set; }
        public int? EntityId { get; set; } = null;
        public string BankName { get; set; } = null;
        public string Aba { get; set; } = null;

        public List<ClientAccountJson> ClientAccounts { get; set; }

        public void CopyToChildren()
        {
            if (ClientAccounts != null)
            {
                ClientAccounts.ForEach(c => c.SiteBankId = c.SiteBankId ?? SiteBankId);
                ClientAccounts.ForEach(c => c.SiteCodeId = c.SiteCodeId ?? SiteCodeId);
                ClientAccounts.ForEach(c => c.CopyToChildren(EntityId));
            }
        }
     }
}
