﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestCommon.JsonData
{
    public class BatchSourceJson
    {
        public int? BatchSourceKey { get; set; } = null;
        public bool IsActive { get; set; } = true;
        public string ImportTypeName { get; set; }
        public int? EntityId { get; set; } = null;
        public string ShortName { get; set; } = null;
        public string LongName { get; set; } = null;
    }
}
