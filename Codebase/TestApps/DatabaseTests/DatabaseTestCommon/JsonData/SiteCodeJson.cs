﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestCommon.JsonData
{
    public class SiteCodeJson
    {
        public int SiteCodeId { get; set; }
        public bool? CwdbActiveSite { get; set; } = false;
        public int? LocalTimeZoneBias { get; set; } = 0;
        public bool? AutoUpdateCurrentProcessingDate { get; set; } = false;
        public DateTime? CurrentProcessingDate { get; set; } = null;
        public string ShortName { get; set; } = null;
        public string LongName { get; set; } = null;
    }
}
