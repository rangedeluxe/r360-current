﻿namespace DatabaseTestCommon.JsonData
{
    public class WorkgroupBusinessRuleJson
    {
        public int? SiteBankId { get; set; }
        public int? SiteClientAccountId { get; set; }
        public bool InvoiceRequired { get; set; } = false;
        public bool BalancingRequired { get; set; } = false;
        public bool PostDepositBalancingRequired { get; set; } = false;
        public bool PostDepositPayerRequired { get; set; } = false;
    }
}
