﻿using System.Collections.Generic;

namespace DatabaseTestCommon.JsonData
{
    public class DataEntrySetupJson
    {
        public int? SiteBankId { get; set; }
        public int? SiteClientAccountId { get; set; }
        public string BatchSource { get; set; }
        public List<DataEntryColumnJson> DataEntryColumns { get; set; }

        public void CopyToChildren()
        {
            DataEntryColumns.ForEach(dc => dc.SiteBankId = dc.SiteBankId ?? SiteBankId);
            DataEntryColumns.ForEach(dc => dc.SiteClientAccountId = dc.SiteClientAccountId ?? SiteClientAccountId);
            DataEntryColumns.ForEach(dc =>
                dc.BatchSource = string.IsNullOrEmpty(dc.BatchSource) ? BatchSource : dc.BatchSource);
        }

    }
}
