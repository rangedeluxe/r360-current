﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DatabaseTestCommon.JsonData
{
    public class CheckJson
    {
        public decimal Amount { get; set; }
        public int? SequenceWithTransaction { get; set; }
        public string RoutingNumber { get; set; } = null;
        public string Account { get; set; } = null;
        public string TransactionCode { get; set; } = null;
        public string Serial { get; set; } = null;
        public string RemitterName { get; set; } = null;
        public string ABA { get; set; } = null;
        public string DDA { get; set; } = null;
        public List<RemittanceDataJson> RemittanceData { get; set; } = null;

        [JsonIgnore]
        public int BatchSequence { get; set; }
    }
}
