﻿using System.Collections.Generic;

namespace DatabaseTestCommon.JsonData
{
    public class TransactionJson
    {
        public int? TransactionID { get; set; }
        public int? TxnSequence { get; set; }
        public int CheckCount { get; set; }
        public int ScannedCheckCount { get; set; }
        public int StubCount { get; set; }
        public int DocumentCount { get; set; }
        public decimal CheckTotal { get; set; }
        public decimal StubTotal { get; set; }
        public string TransactionExceptionStatus { get; set; }
        public List<CheckJson> Checks { get; set; }
        public List<StubJson> Stubs { get; set; }
    }
}
