﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseTestCommon.JsonData
{
    public class ClientAccountJson
    {
        public int? SiteCodeId { get; set; }
        public int? SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public bool MostRecent { get; set; } = true;
        public bool IsActive { get; set; } = true;
        public bool IsCommingled { get; set; } = false;
        public bool Cutoff { get; set; } = true;
        public int OnlineColorMode { get; set; } = 0;
        public int DataRetentionDays { get; set; } = 999;
        public int ImageRetentionDays { get; set; } = 999;
        public Guid SiteClientAccountKey { get; set; } = new Guid();
        public string ShortName { get; set; } = null;
        public string LongName { get; set; } = null;
        public string PoBox { get; set; } = null;
        public string DDA { get; set; } = null;
        public string FileGroup { get; set; } = null;
        public List<DataEntrySetupJson> DataEntrySetups { get; set; }
        public OlWorkgroupJson OlWorkgroup { get; set; }
        public WorkgroupBusinessRuleJson WorkgroupBusinessRule { get; set; }
        //public AuditInformationDto AuditInformation { get; set; }
        public List<int> FiList { get; set; }

        public void CopyToChildren(int? entityId)
        {
            if (DataEntrySetups != null && DataEntrySetups.Any())
            {
                DataEntrySetups.ForEach(d => d.SiteBankId = d.SiteBankId ?? SiteBankId);
                DataEntrySetups.ForEach(d => d.SiteClientAccountId = d.SiteClientAccountId ?? SiteClientAccountId);
                DataEntrySetups.ForEach(d => d.CopyToChildren());
            }

            if (WorkgroupBusinessRule != null)
            {
                if (WorkgroupBusinessRule?.SiteBankId == null)
                    WorkgroupBusinessRule.SiteBankId = SiteBankId;
                if (WorkgroupBusinessRule?.SiteClientAccountId == null)
                    WorkgroupBusinessRule.SiteClientAccountId = SiteClientAccountId;
            }

            if (entityId != null)
                OlWorkgroup = new OlWorkgroupJson();

            if (OlWorkgroup != null)
            {
                if (OlWorkgroup?.SiteBankId == null)
                    OlWorkgroup.SiteBankId = SiteBankId;
                if (OlWorkgroup?.SiteClientAccountId == null)
                    OlWorkgroup.SiteClientAccountId = SiteClientAccountId;
                if (OlWorkgroup?.EntityId == null)
                    OlWorkgroup.EntityId = entityId;
            }
        }
    }
}
