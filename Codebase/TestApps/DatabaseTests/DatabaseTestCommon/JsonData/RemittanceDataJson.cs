﻿namespace DatabaseTestCommon.JsonData
{
    public class RemittanceDataJson
    {
        public string FieldName { get; set; }
        public string Value { get; set; }
    }
}
