﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DatabaseTestCommon.JsonData
{
    public class StubJson
    {
        public decimal Amount { get; set; }
        public int? SequenceWithinTransaction { get; set; }
        public bool IsCorrespondence { get; set; }
        public bool IsOMRDetected { get; set; }
        public string AccountNumber { get; set; }
        public List<RemittanceDataJson> RemittanceData { get; set; } = null;

        [JsonIgnore]
        public int BatchSequence { get; set; }
    }
}
