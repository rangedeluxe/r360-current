﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestCommon.JsonData
{
    public class BatchExceptionStatusJson
    {
        public int? BatchExceptionStatusKey { get; set; }
        public string ExceptionStatusName { get; set; }
        public string ExceptionStatusDescription { get; set; }
    }
}
