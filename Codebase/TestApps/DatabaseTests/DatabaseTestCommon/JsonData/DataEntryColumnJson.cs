﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseTestCommon.JsonData
{
    public class DataEntryColumnJson
    {
        public int? SiteBankId { get; set; }
        public int? SiteClientAccountId { get; set; }
        public string BatchSource { get; set; }
        public bool IsCheck { get; set; }
        public bool IsActive { get; set; } = true;
        public bool IsRequired { get; set; } = false;
        public bool MarkSense { get; set; } = false;
        public int DataType { get; set; }
        public int ScreenOrder { get; set; }
        public string FieldName { get; set; }
        public string UiLabel { get; set; }
        public string SourceDisplayName { get; set; }
    }
}
