﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using DatabaseTestCommon.Helpers;

namespace DatabaseTestCommon.JsonData
{
    public class BatchListJson
    {
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        [JsonConverter(typeof(BatchDateConverter))]
        public DateTime DepositDate { get; set; }
        [JsonConverter(typeof(BatchDateConverter))]
        public DateTime ImmutableDate { get; set; }
        [JsonConverter(typeof(BatchDateConverter))]
        public DateTime SourceProcessingDate { get; set; }
        public string BatchSource { get; set; }
        public string PaymentType { get; set; }
        public string BatchExceptionStatus { get; set; }
        public string DepositStatusName { get; set; }
        public int BatchCueId { get; set; } = 0;
        public int SystemType { get; set; } = 0;
        public int BatchSiteCode { get; set; } = -1;
        public string DepositDDA { get; set; } = null;
        public List<BatchJson> Batches { get; set; }
    }
}
