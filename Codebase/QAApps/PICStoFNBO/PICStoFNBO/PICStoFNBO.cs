﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PICStoFNBO
{
    class PICStoFNBO
    {
        const string USAGESTRING = "Usage: PICStoFNBO <Img path> <Target path> (<Start Date YYYYMMDD>) (<End Date YYYYMMDD>)";
        const int STARTDATEPARAM = 2;
        const int ENDDATEPARAM = 3;
        const int IMAGEPATHPARAM = 0;
        const int TARGETPATHPARAM = 1;

        static string imgPath, targetPath;
            
        static void Main(string[] args)
        {
            int startDate = 0,
                endDate = 99999999,
                folders;

            if (args.Length < 2)
            {
                Console.WriteLine("You must specify the root directory and the target directory. " + USAGESTRING);
            }
            else
            {
                imgPath = args[IMAGEPATHPARAM];
                targetPath = args[TARGETPATHPARAM];

                try
                {
                    switch (args.Length)
                    {
                        case 2:
                            startDate = 0;
                            break;
                        case 3:
                            startDate = Convert.ToInt32(args[STARTDATEPARAM]);
                            break;
                        case 4:
                            startDate = Convert.ToInt32(args[STARTDATEPARAM]);
                            endDate = Convert.ToInt32(args[ENDDATEPARAM]);
                            break;
                        default:
                            throw new ArgumentException("PICStoFNBO only takes 2, 3, or 4 parameters.");
                    } // end switch

                    Console.WriteLine("Processing from img directory " + imgPath + " to " + targetPath + " from {0} to {1}.", 
                        (startDate > 0 ? startDate.ToString("00000000") : "earliest available"),
                        (endDate < 99999999 ? endDate.ToString("00000000") : "latest available"));

                    folders = ProcessImageFolder(imgPath, startDate, endDate);
                    Console.WriteLine("Processed {0} folders.", folders);

                } // end try
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Specify start date and/or end date in YYYYMMDD format. " + USAGESTRING);
                } // end catch
            } // end else

            Console.ReadLine();
        } // end Main

        private static int ProcessImageFolder(string imgPath, int startDate, int endDate)
            // Finds all the Processing Date folders in the range startDate to endDate in the specified PICS path.
            // For each Processing Date, calls ProcessProcessingDate.
            // Returns the number of processing date folders processed.
        {
            int folders = 0;
            string [] processingDates;
            processingDates = Directory.GetDirectories(imgPath);

            foreach (string procDate in processingDates)
            {
                int pDate;
                try
                {
                    pDate = Convert.ToInt32(procDate.Substring(procDate.Length - 8));

                    if (pDate >= startDate & pDate <= endDate)
                    {
                        int banks = 0;
                        Console.WriteLine("Processing {0}.", pDate);
                        banks = ProcessProcessingDate(procDate, pDate);
                        Console.WriteLine("  Processed {0} banks.", banks);
                        folders++;
                    } // end if
                } // end try
                catch (Exception e)
                {
                    Console.WriteLine(procDate + " is not a valid processing date folder. Skipping directory.");
                    Console.WriteLine(e.Message);
                } // end catch
            } // end foreach procDate

            return folders;
        } // end ProcessImageFolder

        private static int ProcessProcessingDate(string processingDatePath, int pDate)
            // Finds all the banks in this Processing Date folder.
            // For each Bank, calls ProcessBank.
            // Returns the number of banks processed.
        {
            int banks = 0;

            string[] bankPaths = Directory.GetDirectories(processingDatePath);

            foreach (string bank in bankPaths)
            {
                try
                {
                    string[] path = bank.Split('\\');
                    int bankID = Convert.ToInt32(path[path.Length - 1]);

                    int lockboxes = 0;

                    Console.WriteLine("  Processing Bank {0}.", bankID);
                    lockboxes = ProcessBank(bank, pDate, bankID);
                    Console.WriteLine("    Processed {0} lockboxes.", lockboxes);
                    banks++;
                } // end try
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(bank + " is not a valid bank directory.");
                } // end catch
            } // end foreach

            return banks;
        } // end ProcessProcessingDate

        private static int ProcessBank(string bankPath, int processingDate, int bankID)
            // Finds all the lockboxes in this Bank folder.
            // For each lockbox:
            
        {
            int lockboxes = 0;

            string[] lockboxPaths = Directory.GetDirectories(bankPath);

            foreach (string lockbox in lockboxPaths)
            {
                try
                {
                    string[] path = lockbox.Split('\\');
                    int lockboxID = Convert.ToInt32(path[path.Length - 1]);

                    int images = 0;
                    
                    Console.WriteLine("    Processing Lockbox {0}.", lockboxID);
                    images = ProcessLockbox(lockbox, processingDate, bankID, lockboxID);
                    Console.WriteLine("      Processed {0} image files.", images);
                    lockboxes++;
                } // end try
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(lockbox + " is not a valid lockbox directory.");
                } // end catch
            } // end foreach

            return lockboxes;
        } // end ProcessBank

        private static int ProcessLockbox(string lockboxPath, int processingDate, int bankID, int lockboxID)
            // 1. Checks to see if the Lockbox Directory already exists in the target folder.
            //    a. If not, creates the Lockbox Directory.
            // 2. Checks to see if the Bank Directory already exists in the target Lockbox folder.
            //    a. If not, creates the Bank Directory.
            // 3. Creates the Processing Date Directory in the target Bank directory.
            // 4. Copies all of the files in the lockboxPath to the target Processing Date folder.
        {
            int images = 0;

            string workingPath = targetPath + "\\" + lockboxID;

            try
            {
                // 1. Check to see if Lockbox Directory already exists in the target path.
                if(!Directory.Exists(workingPath))
                {
                    Console.WriteLine("Creating Lockbox path " + workingPath);
                    Directory.CreateDirectory(workingPath);
                } // end if

                // 2. Check to see if the Bank Directory already exists in the Lockbox Directory.
                workingPath += "\\" + bankID;
                if (!Directory.Exists(workingPath))
                {
                    Console.WriteLine("Creating Bank path " + workingPath);
                    Directory.CreateDirectory(workingPath);
                } // end if

                // 3. Create the Processing Date Directory
                workingPath += "\\" + processingDate;
                if (!Directory.Exists(workingPath))
                {
                    Console.WriteLine("Creating Processing Date path " + workingPath);
                    Directory.CreateDirectory(workingPath);
                } // end if

                // 4. Copy images to target
                images = CopyImagesToTarget(lockboxPath, workingPath);

            } // end try
            catch(Exception e)
            {
                Console.WriteLine("Working path is " + workingPath);
                Console.WriteLine(e.Message);
            } // end catch

            return images;
        } // end ProcessLockbox

        static int CopyImagesToTarget(string lockboxPath, string destinationPath)
            // Copies all of the .tif files in the specified lockboxPath to the destination path
            // Returns the number of images copied.
        {
            int imagesCopied = 0;

            string[] imageTIFs = Directory.GetFiles(lockboxPath, "*.tif");

            foreach (string tif in imageTIFs)
            {
                string [] pathPieces = tif.Split('\\');
                string tifName = pathPieces[pathPieces.Length - 1];

                //Console.WriteLine("Copy " + tif + " to " + destinationPath + "\\" + tifName);
                File.Copy(tif, destinationPath + "\\" + tifName);

                imagesCopied++;
            } // end foreach
            return imagesCopied;
        } // end CopyImagesToTarget

    } // end class PICStoFNBO
} // end namespace PICStoFNBO
