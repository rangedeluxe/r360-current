﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Wfs.Logging
{
    public class InMemoryLog : IWfsLog
    {
        public class Entry
        {
            public IEnumerable Args { get; set; }
            public Exception Exception { get; set; }
            public WfsLogLevel Level { get; set; }
            public string Message { get; set; }
            public string Source { get; set; }
        }

        public InMemoryLog()
        {
            Entries = new List<Entry>();
        }

        public IList<Entry> Entries { get; private set; }

        public void Log(WfsLogLevel level, string message, IEnumerable args = null,
            string source = null, Exception exception = null)
        {
            Entries.Add(new Entry
            {
                Level = level,
                Message = message,
                Args = args,
                Source = source,
                Exception = exception,
            });
        }
        public void ReplayTo(IWfsLog target)
        {
            foreach (var entry in Entries)
            {
                target.Log(
                    level: entry.Level,
                    message: entry.Message,
                    args: entry.Args,
                    source: entry.Source,
                    exception: entry.Exception);
            }
        }
    }
}