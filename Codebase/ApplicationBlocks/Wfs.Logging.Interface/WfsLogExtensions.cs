﻿using System;
using System.Collections;

namespace Wfs.Logging
{
    public static class WfsLogExtensions
    {
        public static void Fatal(this IWfsLog log, string message, IEnumerable args = null,
            string source = null, Exception exception = null)
        {
            log.Log(WfsLogLevel.Fatal, message, args, source, exception);
        }
        public static void Error(this IWfsLog log, string message, IEnumerable args = null,
            string source = null, Exception exception = null)
        {
            log.Log(WfsLogLevel.Error, message, args, source, exception);
        }
        public static void Warn(this IWfsLog log, string message, IEnumerable args = null,
            string source = null, Exception exception = null)
        {
            log.Log(WfsLogLevel.Warn, message, args, source, exception);
        }
        public static void Info(this IWfsLog log, string message, IEnumerable args = null,
            string source = null, Exception exception = null)
        {
            log.Log(WfsLogLevel.Info, message, args, source, exception);
        }
        public static void Debug(this IWfsLog log, string message, IEnumerable args = null,
            string source = null, Exception exception = null)
        {
            log.Log(WfsLogLevel.Debug, message, args, source, exception);
        }
        public static void Trace(this IWfsLog log, string message, IEnumerable args = null,
            string source = null, Exception exception = null)
        {
            log.Log(WfsLogLevel.Trace, message, args, source, exception);
        }
    }
}