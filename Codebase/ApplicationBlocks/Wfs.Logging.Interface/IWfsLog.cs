﻿using System;
using System.Collections;

namespace Wfs.Logging
{
    public interface IWfsLog
    {
        void Log(WfsLogLevel level, string message, IEnumerable args = null,
            string source = null, Exception exception = null);
    }
}