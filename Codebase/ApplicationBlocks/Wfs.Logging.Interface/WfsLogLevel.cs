﻿namespace Wfs.Logging
{
    public enum WfsLogLevel
    {
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
    }
}