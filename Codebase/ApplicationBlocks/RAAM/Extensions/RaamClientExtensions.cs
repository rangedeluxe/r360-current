﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.R360RaamClient;

namespace WFS.RecHub.ApplicationBlocks.RAAM.Extensions
{
    public static class RaamClientExtensions
    {
        /// <summary>
        /// Populates a list of all of the authorized workgroups for the specified entity
        /// and it's subsequent children entities, recursively.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rc"></param>
        /// <param name="entityid"></param>
        /// <param name="tbuilder">
        /// This function populates a new DTO from a Raam Resource (workgroup).  
        /// </param>
        /// <returns></returns>
        public static IList<T> GetAuthorizedWorkgroupsForEntity<T>(this IRaamClient rc, int entityid, Func<ResourceDto, T> tbuilder)
        {
            // Get all the workgroups we have access to.
            var list = new List<T>();
            var ent = rc.GetAuthorizedEntitiesAndWorkgroups();

            // First, we need to find the root entity by the ID.  Kick out early if we don't have access to it.
            var root = FindEntity(ent, entityid);
            if (root == null)
                return list;

            // Now, we list up all of the workgroups associated.
            GetWorkgroupsForEntityRecursion(list, root, tbuilder);

            return list;
        }

        /// <summary>
        /// Returns if the current logged in user has access to the workgroup.
        /// </summary>
        /// <param name="rc"></param>
        /// <param name="bankid"></param>
        /// <param name="clientaccountid"></param>
        /// <returns></returns>
        public static bool IsAuthorizedForWorkgroup(this IRaamClient rc, int bankid, int clientaccountid)
        {
            var wg = rc.GetWorkgroupResource(bankid, clientaccountid);
            return wg != null && wg.ID > 0;
        }

        private static void GetWorkgroupsForEntityRecursion<T>(IList<T> currentList, EntityDto currentEntity, Func<ResourceDto, T> tbuilder)
        {
            // Depth first
            foreach (var e in currentEntity.ChildEntities)
                GetWorkgroupsForEntityRecursion(currentList, e, tbuilder);

            currentEntity.Resources
                .Where(x => x.ResourceTypeCode == "Workgroup")
                .ToList()
                .ForEach(r =>
                {
                    currentList.Add(tbuilder(r));
                });
        }

        private static EntityDto FindEntity(EntityDto root, int entid)
        {
            // If we found, kick out early.
            if (root.ID == entid)
                return root;

            // Traverse the tree.
            foreach (var c in root.ChildEntities)
            {
                var e = FindEntity(c, entid);
                if (e != null)
                    return e;
            }
            return null;
        }

    }
}
