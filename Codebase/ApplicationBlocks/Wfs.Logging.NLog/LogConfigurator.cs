using System.Collections.Specialized;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Wfs.Logging.NLog
{
    internal class LogConfigurator
    {
        private readonly NameValueCollection _appSettings;
        private ColoredConsoleTarget _coloredConsoleTarget;
        private FileTarget _fileTarget;
        private readonly IWfsLog _log;
        private LogLevel _minLevel;

        public LogConfigurator(NameValueCollection appSettings, IWfsLog log)
        {
            _appSettings = appSettings;
            _log = log;
        }

        public LoggingConfiguration Configuration { get; private set; }

        private LogLevel GetMinLevel()
        {
            var stringValue = _appSettings["LoggingDepth"];
            int intValue;
            if (int.TryParse(stringValue, out intValue))
            {
                switch (intValue)
                {
                    case 2:
                        return LogLevel.Trace;
                    case 1:
                        return LogLevel.Debug;
                    default:
                        return LogLevel.Info;
                }
            }
            return LogLevel.Info;
        }
        public void Initialize()
        {
            _minLevel = GetMinLevel();
            _coloredConsoleTarget = ColoredConsoleTargetFactory.Create();
            _fileTarget = FileTargetFactory.Create(_appSettings, _log);

            Configuration = LoggingConfigurationFactory.Create(
                _minLevel, _coloredConsoleTarget, _fileTarget);

            _log.Info("Loaded log configuration from WFS config file (min level = {0})", new[] {_minLevel});
            if (_fileTarget != null)
            {
                _log.Debug("Logfile current name: {0}", new[] {_fileTarget.FileName});
                _log.Debug("Logfile archive name: {0}", new[] {_fileTarget.ArchiveFileName});
                _log.Debug("Logfile archive size: {0} bytes", new[] {_fileTarget.ArchiveAboveSize});
            }
        }
    }
}