﻿using System;
using System.Collections;
using System.Configuration;
using System.Globalization;
using System.Linq;
using NLog;

namespace Wfs.Logging.NLog
{
    public class WfsLog : IWfsLog, IDisposable
    {
        private readonly ILogger _nlogLogger;

        public WfsLog()
        {
            // If NLog didn't autoload an NLog config file, load our own settings
            // from the app config.
            var configLog = new InMemoryLog();
            if (LogManager.Configuration == null)
            {
                var configurator = new LogConfigurator(ConfigurationManager.AppSettings, configLog);
                configurator.Initialize();
                LogManager.Configuration = configurator.Configuration;
            }

            _nlogLogger = LogManager.GetLogger("");

            configLog.ReplayTo(this);
        }
        public void Dispose()
        {
            Flush();
        }

        public void Flush()
        {
            LogManager.Flush();
        }
        public void Log(WfsLogLevel level, string message, IEnumerable args = null,
            string source = "", Exception exception = null)
        {
            var argArray = args != null
                ? (args as object[]) ?? args.Cast<object>().ToArray()
                : null;
            var entry = new LogEventInfo(
                ToNLog(level), source, CultureInfo.InvariantCulture, message, argArray, exception);
            _nlogLogger.Log(entry);
        }
        private LogLevel ToNLog(WfsLogLevel level)
        {
            switch (level)
            {
                case WfsLogLevel.Trace:
                    return LogLevel.Trace;
                case WfsLogLevel.Debug:
                    return LogLevel.Debug;
                case WfsLogLevel.Info:
                    return LogLevel.Info;
                case WfsLogLevel.Warn:
                    return LogLevel.Warn;
                case WfsLogLevel.Error:
                    return LogLevel.Error;
                case WfsLogLevel.Fatal:
                    return LogLevel.Fatal;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}