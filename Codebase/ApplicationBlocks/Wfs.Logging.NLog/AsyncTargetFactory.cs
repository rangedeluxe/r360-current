﻿using NLog.Targets;
using NLog.Targets.Wrappers;

namespace Wfs.Logging.NLog
{
    internal static class AsyncTargetFactory
    {
        public static AsyncTargetWrapper Create(Target target)
        {
            var asyncTargetWrapper = new AsyncTargetWrapper(target.Name + "Async", target)
            {
                OverflowAction = AsyncTargetWrapperOverflowAction.Block,
            };
            return asyncTargetWrapper;
        }
    }
}