﻿using NLog.Conditions;
using NLog.Targets;

namespace Wfs.Logging.NLog
{
    internal class ColoredConsoleTargetFactory
    {
        private readonly ColoredConsoleTarget _coloredConsoleTarget;

        private ColoredConsoleTargetFactory()
        {
            _coloredConsoleTarget = new ColoredConsoleTarget
            {
                Name = "ColoredConsole",
                Layout =
                    " ${pad:padding=-5:inner=${level:uppercase=true}}  ${longdate}  ${message}" +
                    "${when:when=logger != null && logger != '':inner=  [${logger}]}" +
                    "${onexception:${newline}${exception:toString}}",
            };
        }

        private void AddRowHighlightingRule(string expression, ConsoleOutputColor foregroundColor)
        {
            _coloredConsoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule
            {
                Condition = ConditionParser.ParseExpression(expression),
                ForegroundColor = foregroundColor,
            });
        }
        private void AddWordHighlightingRule(string regex, ConsoleOutputColor foregroundColor,
            ConsoleOutputColor backgroundColor = ConsoleOutputColor.NoChange)
        {
            _coloredConsoleTarget.WordHighlightingRules.Add(new ConsoleWordHighlightingRule
            {
                Regex = regex,
                ForegroundColor = foregroundColor,
                BackgroundColor = backgroundColor,
            });
        }
        public static ColoredConsoleTarget Create()
        {
            var factory = new ColoredConsoleTargetFactory();
            var target = factory.Execute();
            return target;
        }
        private ColoredConsoleTarget Execute()
        {
            AddRowHighlightingRule("level == LogLevel.Fatal", ConsoleOutputColor.Magenta);
            AddRowHighlightingRule("level == LogLevel.Error", ConsoleOutputColor.Red);
            AddRowHighlightingRule("level == LogLevel.Warn", ConsoleOutputColor.Yellow);
            AddRowHighlightingRule("level == LogLevel.Info", ConsoleOutputColor.Cyan);
            AddRowHighlightingRule("level == LogLevel.Debug", ConsoleOutputColor.DarkCyan);
            AddRowHighlightingRule("level == LogLevel.Trace", ConsoleOutputColor.DarkGray);
            AddRowHighlightingRule("true", ConsoleOutputColor.Gray);

            AddWordHighlightingRule(@"\d+-\d+-\d+ \d+:\d+:\d+\.\d+", ConsoleOutputColor.Gray);
            AddWordHighlightingRule(@" FATAL ", ConsoleOutputColor.Black, ConsoleOutputColor.Magenta);
            AddWordHighlightingRule(@" ERROR ", ConsoleOutputColor.White, ConsoleOutputColor.Red);
            AddWordHighlightingRule(@" WARN  ", ConsoleOutputColor.Black, ConsoleOutputColor.Yellow);
            AddWordHighlightingRule(@" INFO  ", ConsoleOutputColor.Black, ConsoleOutputColor.Cyan);
            AddWordHighlightingRule(@" DEBUG ", ConsoleOutputColor.Black, ConsoleOutputColor.DarkCyan);
            AddWordHighlightingRule(@" TRACE ", ConsoleOutputColor.Black, ConsoleOutputColor.DarkGray);
            AddWordHighlightingRule(@"  \[[^]]+\](  |$)", ConsoleOutputColor.DarkGreen);

            return _coloredConsoleTarget;
        }
    }
}