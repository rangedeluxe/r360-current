﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using NLog.Targets;

namespace Wfs.Logging.NLog
{
    internal static class FileTargetFactory
    {
        public static FileTarget Create(NameValueCollection appSettings, IWfsLog log)
        {
            var archiveFileName = GetArchiveFileName(appSettings, log);
            if (archiveFileName == null)
                throw new InvalidOperationException("No log filename was specified");

            var fileName = GetCurrentFileName(archiveFileName);
            var logFileMaxSizeBytes = GetLogFileMaxSizeBytes(appSettings);
            var maxArchiveFiles = GetMaxArchiveFiles(appSettings);
            return new FileTarget("File")
            {
                Encoding = Encoding.UTF8,
                Layout =
                    "${level:uppercase=true}\t${date:yyyy-MM-dd HH\\:mm\\:ss.ffffK}\t${logger}\t${message}" +
                    "${onexception:${newline}${exception:toString}}",
                ArchiveNumbering = ArchiveNumberingMode.DateAndSequence,
                ArchiveFileName = archiveFileName,
                ArchiveAboveSize = logFileMaxSizeBytes,
                ArchiveDateFormat = "yyyyMMdd",
                ArchiveEvery = FileArchivePeriod.Day,
                FileName = fileName,
                MaxArchiveFiles = maxArchiveFiles,
            };
        }
        private static string GetArchiveFileName(NameValueCollection appSettings, IWfsLog log)
        {
            var stringValue = appSettings["LogFilePath"];
            if (!string.IsNullOrEmpty(stringValue))
                return Regex.Replace(stringValue, @"{[^}]+}", "{#}");

            log.Warn("No log path was configured. Attempting to autodiscover log path.");
            var candidateDriveLetters = Enumerable.Range('D', 'Z' - 'D' + 1).Select(i => (char) i)
                .Concat(new[] {'C'});
            var dir = candidateDriveLetters
                .Select(driveLetter => driveLetter + @":\WFSApps\Logs")
                .FirstOrDefault(Directory.Exists);
            if (!string.IsNullOrEmpty(dir))
            {
                log.Info("Autodiscovered log path: {0}", new[] {dir});
                return Path.Combine(dir, "log.{#}.txt");
            }

            log.Error("Unable to autodiscover log path. File logging will be disabled.");
            return null;
        }
        private static string GetCurrentFileName(string archiveFileName)
        {
            return archiveFileName.Replace("{#}", "Current");
        }
        private static int GetLogFileMaxSizeBytes(NameValueCollection appSettings)
        {
            var stringValue = appSettings["LogFileMaxSize"];
            int intValue;
            if (int.TryParse(stringValue, out intValue))
            {
                return intValue * 1024;
            }
            return 2 * 1024 * 1024;
        }
        private static int GetMaxArchiveFiles(NameValueCollection appSettings)
        {
            var stringValue = appSettings["LogMaxArchiveFiles"];
            int intValue;
            if (int.TryParse(stringValue, out intValue))
            {
                return Math.Max(1, intValue);
            }
            return 100;
        }
    }
}