﻿using NLog;
using NLog.Config;
using NLog.Targets;

namespace Wfs.Logging.NLog
{
    internal static class LoggingConfigurationFactory
    {
        private static void ActivateRuleAsAsync(LoggingConfiguration configuration,
            LogLevel minLevel, Target innerTarget)
        {
            if (innerTarget == null)
                return;

            var asyncTarget = AsyncTargetFactory.Create(innerTarget);
            configuration.AddTarget(asyncTarget);
            configuration.AddRule(minLevel: minLevel, maxLevel: LogLevel.Fatal, target: asyncTarget);
        }
        public static LoggingConfiguration Create(LogLevel minLevel,
            ColoredConsoleTarget coloredConsoleTarget, FileTarget fileTarget)
        {
            var configuration = new LoggingConfiguration();
            ActivateRuleAsAsync(configuration, minLevel, coloredConsoleTarget);
            ActivateRuleAsAsync(configuration, minLevel, fileTarget);
            return configuration;
        }
    }
}