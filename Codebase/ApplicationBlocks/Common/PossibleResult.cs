﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.Common
{
    public abstract class PossibleResult<T>
    {
        public abstract T GetResult(T whenNoResult);
        public abstract T GetResult(Func<T> whenNoResult);
    }

    
}
