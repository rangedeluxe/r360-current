﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common.Crypto;

namespace WFS.RecHub.ApplicationBlocks.Common.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// This generic extension method is used to extend the 
        /// string object and parse it into the specified value type.
        /// </summary>
        /// <typeparam item="T">Generic type parameter</typeparam>
        /// <param name="value">String object to parse from</param>
        /// <returns>The string value parsed to type T</returns>
        public static T ParseTo<T>(this string item)
        {
            Type type = typeof(T);//determine the type
            T value = default(T);//default the value for the type

            if (!type.IsValueType)//if it's not a value type, return the default
                return default(T);

            var methodInfo = GetMethodInfo(type, "TryParse");//does the type have the TryParse method

            if (methodInfo == null)
                throw new ApplicationException("The column value type doesn't contain a TryParse method.");

            object result = methodInfo.Invoke(null, new object[] { item, value });

            if (result == null)//if invoking the method didn't work
                return default(T);

            if (!(bool)result)//if the TryParse method returned false
                return default(T);

            methodInfo = GetMethodInfo(type, "Parse");
            if (methodInfo == null)
                throw new ApplicationException("The column value type doesn't contain a Parse method.");

            return (T)methodInfo.Invoke(null, new object[] { item });

        }

        /// <summary>
        /// Returns the value of the specified string as an integer value
        /// </summary>
        /// <param name="value">String value to convert</param>
        /// <returns>Int64 (long) representation of the string value</returns>
        public static Int64 GetBigInteger(this string value)
        {
            Int64 returnValue = 0;

            if (!string.IsNullOrEmpty(value))
            {
                Int64 result = 0;
                bool success = Int64.TryParse(value, out result);
                if (success)
                    returnValue = result;
            }

            return returnValue;
        }

        /// <summary>
        /// Returns the value of the specified string as an integer value
        /// </summary>
        /// <param name="value">String value to convert</param>
        /// <returns>Integer representation of the string value</returns>
        public static int GetInteger(this string value)
        {
            int returnValue = 0;

            if (!string.IsNullOrEmpty(value))
            {
                int result = 0;
                bool success = int.TryParse(value, out result);
                if (success)
                    returnValue = result;
            }

            return returnValue;
        }

        /// <summary>
        /// Returns the value of the specified string as a double value
        /// </summary>
        /// <param name="value">String value to convert</param>
        /// <returns>Double representation of the string value</returns>
        public static double GetDouble(this string value)
        {
            double returnValue = 0;

            if (!string.IsNullOrEmpty(value))
            {
                double result = 0;

                bool success = double.TryParse(value, out result);
                if (success)
                    returnValue = result;
            }

            return returnValue;
        }

        public static string Encrypt(this string text)
        {
            var encryptedValue = string.Empty;
            var crypto = new cCrypto3DES();
            crypto.Encrypt(text, out encryptedValue);
            return encryptedValue;
        }
        
        
        public static string Decrypt(this string encryptedValue)
        {
            var decryptedValue = string.Empty;
            var crypto = new cCrypto3DES();
            crypto.Decrypt(encryptedValue, out decryptedValue);
            return decryptedValue;

        }

        public static ConnectionStringSettings ToConnectionStringSettings(this R360DBConnectionSettings r360DBConnectionSettings)
        {
            if (r360DBConnectionSettings == null)//defend here, don't create the object
                throw new ArgumentNullException("Must provide R360DBConnectionSettings");

            return r360DBConnectionSettings.Value;
        }

        private static MethodInfo GetMethodInfo(Type type, string methodName)
        {
            return (from m in type.GetMethods(BindingFlags.Public | BindingFlags.Static)
                    where m.Name == methodName
                    select m).FirstOrDefault();
        }
    }
}
