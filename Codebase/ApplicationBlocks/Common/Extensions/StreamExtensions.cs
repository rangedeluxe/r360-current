﻿using System.IO;

namespace WFS.RecHub.ApplicationBlocks.Common.Extensions
{
    public static class StreamExtensions
    {
        public static byte[] ToBytes(this Stream stream)
        {
            byte[] bytes = new byte[stream.Length];
            stream.Position = 0;
            stream.Read(bytes, 0, (int)stream.Length);
            return bytes;
        }
    }
}
