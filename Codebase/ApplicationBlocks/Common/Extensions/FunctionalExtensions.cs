﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.Common.Extensions
{
    public static class FunctionalExtensions
    {
        public static void Use<TDisposable>(this TDisposable disposable, Action<TDisposable> action) where TDisposable : IDisposable
        {
            using (disposable)
            {
                action(disposable);
            }
        }

        public static TResponse Use<TDisposable, TResponse>(this TDisposable disposable, Func<TDisposable, TResponse> function) where TDisposable : IDisposable
        {
            using (disposable)
            {
                return function(disposable);
            }
        }


        /// <summary>
        /// Higher-order function that accepts a function as a parameter
        /// and applies the function to a collection on demand. 
        /// </summary>
        /// <typeparam name="T">Generic parameter type for the input IEnumerable</typeparam>
        /// <typeparam name="R">Generic parameter type for the output IEnumerable</typeparam>
        /// <param name="enumerable">IEnumerable listing</param>
        /// <param name="function">Mapping function to perform on each item in the list</param>
        /// <returns>Newly mapped listing from one type to the other</returns>
        public static IEnumerable<R> Map<T, R>(this IEnumerable<T> enumerable, Func<T, R> function)
        {
            foreach (var e in enumerable)
                yield return function(e);
        }

        /// <summary>
        /// Higher-order function that accepts a function as a parameter
        /// and applies the function to a generic item on demand. 
        /// </summary>
        /// <typeparam name="T">Generic parameter type for the input parm</typeparam>
        /// <typeparam name="R">Generic parameter type for the result</typeparam>
        /// <param name="t">Generic type item to perform the mapping function on</param>
        /// <param name="function">Mapping function to perform on the generic item</param>
        /// <returns>Newly mapped item from one type to the other</returns>
        public static R Map<T, R>(this T t, Func<T, R> function)
        {
            return function(t);
        }

        public static T ToInstance<T>(this Type type) where T: class
        {
            //var instanceOfModule = Activator.CreateInstance(type);
            var instanceOfModule = type.Assembly.CreateInstance(type.FullName);
               
            if (instanceOfModule == null)
                throw new TypeLoadException($"Unable to create an instance of module: {type.Name} in assembly {type.Assembly.FullName}");

            if (!(instanceOfModule is T))
                throw new TypeLoadException($"Module {type.Name} in assembly {type.Assembly.FullName} must be of type {typeof(T)}. " +
                    $"It is of type {instanceOfModule.GetType().Name}");

            return instanceOfModule as T;
        }

        public static T ToInstance<T>(this Type type, object[] constructorArguments = null) where T : class
        {
            object instance;
            if (constructorArguments == null)
                instance = Activator.CreateInstance(type);
            else
                instance = Activator.CreateInstance(type, constructorArguments);

            if (instance == null)
                throw new TypeLoadException($"Unable to create an instance of module: {type.Name} in assembly {type.Assembly.FullName}");

            if (!(instance is T))
                throw new TypeLoadException($"Module: {type.Name} in assembly {type.Assembly.FullName} must be an {typeof(T)}.");

            return instance as T;
        }
        
        public static Assembly ToAssembly(this string assemblyFile)
        {
            Assembly assembly;
            if (string.IsNullOrEmpty(assemblyFile))//assume the assembly is already loaded in the executing assembly
                assembly = Assembly.GetExecutingAssembly();
            else
                assembly = Assembly.LoadFrom(assemblyFile);

            if (assembly == null)
                throw new TypeLoadException($"Unable to load the module assembly - {assemblyFile}.");

            return assembly;
        }
    }
}
