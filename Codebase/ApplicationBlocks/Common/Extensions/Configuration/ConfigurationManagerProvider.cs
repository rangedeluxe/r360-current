﻿using System;
using System.Configuration;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace WFS.RecHub.ApplicationBlocks.Common.Configuration
{
    /// <summary>
    /// Provides Key/Value pairs from the AppSettings.
    /// </summary>
    public class ConfigurationManagerProvider : IConfigurationProvider
    {
        public string GetSetting(string key)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(key))
                return null;

            return ConfigurationManager.AppSettings[key];
        }

        public void SetSetting(string key, string value)
        {
            throw new NotSupportedException();
        }
    }
}