﻿using System.IO;
using System.Xml;

namespace WFS.RecHub.ApplicationBlocks.Common.Extensions
{
    public static class XmlDocumentExtensions
    {
        /// <summary>
        /// Loads an XML string into an <see cref="XmlDocument"/>, with protection against
        /// XML entity injection attacks.
        /// </summary>
        public static void SafeLoadXml(this XmlDocument document, string xml)
        {
            document.SafeLoadXml(new StringReader(xml));
        }
        /// <summary>
        /// Loads XML from a reader into an <see cref="XmlDocument"/>, with protection against
        /// XML entity injection attacks.
        /// </summary>
        public static void SafeLoadXml(this XmlDocument document, TextReader reader)
        {
            var settings = new XmlReaderSettings {DtdProcessing = DtdProcessing.Prohibit, XmlResolver = null};
            var xmlReader = XmlReader.Create(reader, settings);
            document.Load(xmlReader);
        }
    }
}