﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.Common
{
    public static class Result
    {
        public static PossibleResult<T> None<T>() => new NoResult<T>();
        public static PossibleResult<T> Real<T>(T data) => new RealResult<T>(data);

        private class NoResult<T> : PossibleResult<T>
        {

            public override T GetResult(T whenNoResult) =>  whenNoResult;

            public override T GetResult(Func<T> whenNoResult) => whenNoResult();
        }

        private class RealResult<T> : PossibleResult<T>
        {
            private T Data { get; }
            public RealResult(T data)
            {
                this.Data = data;
            }

            public override T GetResult(T whenNoResult) => this.Data;

            public override T GetResult(Func<T> whenNoResult) => this.Data;
            
        }
    }
}
