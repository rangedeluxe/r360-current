﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.Common
{
    public class R360DBConnectionSettings
    {
        public ConnectionStringSettings Value { get; private set; }
        public string Name { get; private set; }
        public string ConnectionString { get; private set; }
        public string ProviderName { get; private set; }


        public R360DBConnectionSettings()
        {
            TryParse();
        }

        private void TryParse()
        {
            this.Value = ConfigurationManager.ConnectionStrings["R360ConnectionString"];
            if (this.Value == null)
                throw new ConfigurationErrorsException("R360ConnectionString is not configured");

            if (string.IsNullOrEmpty(this.Value.ProviderName))
                throw new ConfigurationErrorsException("R360ConnectionString ProviderName is missing or blank");
            else this.ProviderName = this.Value.ProviderName;
            if (string.IsNullOrEmpty(this.Value.ConnectionString))
                throw new ConfigurationErrorsException("R360ConnectionString ConnectionString is missing or blank");
            else this.ConnectionString = this.Value.ConnectionString;
            if (string.IsNullOrEmpty(this.Value.Name))
                throw new ConfigurationErrorsException("R360ConnectionString Name is missing or blank");
            else this.Name = this.Value.Name;
        }
    }
}
