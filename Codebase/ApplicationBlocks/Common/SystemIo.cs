﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;

namespace WFS.RecHub.ApplicationBlocks.Common
{
    public class SystemIo : ISystemIo
    {

        public bool Exists(string path)
        {
            return System.IO.Directory.Exists(path);
        }

        public string[] GetFiles(string path, string searchPattern)
        {
            return System.IO.Directory.GetFiles(path, searchPattern);
        }

        public string[] EnumerateFiles(string path, string searchPattern)
        {
            return System.IO.Directory.EnumerateFiles(path, searchPattern).ToArray();
        }

        public string Combine(string image, string batchPaymentSource, string pictures, string bankId,
            string workgroupId, string importTypeShortName)
        {
            return importTypeShortName.ToUpper() == "INTEGRAPAY"
                ? System.IO.Path.Combine(image, pictures, bankId, workgroupId)
                : System.IO.Path.Combine(image, batchPaymentSource, pictures, bankId, workgroupId);
        }
    }
}
