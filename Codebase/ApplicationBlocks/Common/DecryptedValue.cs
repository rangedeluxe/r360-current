﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;

namespace WFS.RecHub.ApplicationBlocks.Common
{
    public class DecryptedValue
    {
        public string Value { get; private set; }
        public DecryptedValue(string value)
        {
            TryToDecrypt(value);
        }

        private void TryToDecrypt(string value)
        {
            if (string.IsNullOrEmpty(value))
                this.Value = string.Empty;

            try
            {
                this.Value = this.Value.Decrypt();
            }
            catch
            {
                //Set it to the original value.  Chances are if the decrypt failed, it wasn't encrypted to begin with
                this.Value = value;
            } 
            
        }
    }
}
