﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.Common.Interfaces
{
    public interface ISystemIo
    {
        bool Exists(string path);

        string[] GetFiles(string path, string searchPattern);

        string[] EnumerateFiles(string path, string searchPattern);

        string Combine(string image, string batchPaymentSource, string pictures, string bankId,
            string workgroupId, string importTypeShortName);
    }
}
