﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.Common.Interfaces
{
    public interface IReadOnlyListRepository<TGetRequest, TResponse, TListRequest, TListResponse, TKey> : 
        IReadOnlyRepository<TGetRequest, TResponse, TKey>
    {
        TListResponse GetAll(TListRequest req);
    }
}
