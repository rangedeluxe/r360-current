﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.Common.Interfaces
{
    public interface IRepository<TRequest, TResponse, TKey> : 
        IReadOnlyRepository<TRequest, TResponse, TKey>, IDeleteOnlyRepository<TRequest, TResponse, TKey>, IWriteOnlyRepository<TRequest, TResponse, TKey>
    {
    }
}
