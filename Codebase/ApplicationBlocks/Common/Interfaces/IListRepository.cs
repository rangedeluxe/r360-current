﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.Common.Interfaces
{
    public interface IListRepository<TSaveRequest, TDeleteRequest, TGetRequest, TResponse, TListResponse, TKey> : 
        IReadOnlyRepository<TGetRequest, TResponse, TKey>, 
        IDeleteOnlyRepository<TDeleteRequest, TResponse, TKey>, 
        IWriteOnlyRepository<TSaveRequest, TResponse, TKey>
    {
        TListResponse GetAll();
    }
}
