﻿namespace WFS.RecHub.ApplicationBlocks.Common.Interfaces
{
    public interface ILookupRepository<TRequest, TResponse>
    {
        TResponse TryGet(TRequest request);
    }
}
