﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.Common.Interfaces
{
    public interface IWriteOnlyRepository<TRequest, TResponse, TKey>
    {
        TResponse Save(TRequest entity);
    }
}
