﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.ApplicationBlocks.Common.Objects;

namespace WFS.RecHub.ApplicationBlocks.Common
{
    public static class Utility
    {
        public static TValue GetPropertyValue<TContext, TValue>(TContext context, string propertyName)
        {
            if (context.GetType().GetProperty(propertyName) == null)
                return default(TValue);

            if (!typeof(TValue).IsValueType)
                return (TValue)context.GetType().GetProperty(propertyName).GetValue(context);

            return (TValue)context.GetType().GetProperty(propertyName).GetValue(context).ToString().ParseTo<TValue>();
        }

        public static byte[] ToBytes(Stream stream)
        {
            return stream.ToBytes();
        } 

        public static List<ObjectDifference> PropertyWiseObjectDiff(Object oldvalue, Object newvalue)
        {
            var output = new List<ObjectDifference>();
            if (oldvalue.GetType() != newvalue.GetType())
                return output;

            var props = oldvalue.GetType().GetProperties();
            foreach (var p in props)
            {
                var oldval = p.GetValue(oldvalue);
                var newval = p.GetValue(newvalue);

                if (!Object.Equals(oldval, newval))
                {
                    output.Add(new ObjectDifference()
                    {
                        NewValue = newval,
                        OldValue = oldval,
                        PropertyName = p.Name
                    });
                }
            }

            return output;
        }
    }
}
