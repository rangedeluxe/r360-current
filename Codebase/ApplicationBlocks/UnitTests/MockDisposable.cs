﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    internal class MockDisposable : IDisposable
    {
        public bool MyMethod()
        {
            return true;
        }

        public bool IsDisposed
        {
            get;
            private set;
        }

        public void Dispose()
        {
            IsDisposed = true;
        }
    }
}
