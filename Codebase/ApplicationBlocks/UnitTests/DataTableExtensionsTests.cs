﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using JetBrains.Annotations;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
// ReSharper disable ConditionIsAlwaysTrueOrFalse

namespace UnitTests
{
    [TestClass]
    public class DataTableExtensionsTests
    {
        [TestMethod]
        public void HasData_ReceivesNullDataTable_ReturnsFalse()
        {
            //arrange
            var expected = false;

            //act
            using (DataTable dt = GetNullDataTable())
            {
                var actual = dt.HasData();

                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void HasData_ReceivesDataTableWithNoRows_ReturnsFalse()
        {
            //arrange
            var expected = false;

            //act
            using (DataTable dt = GetDataTableWithNoRows())
            {
                var actual = dt.HasData();

                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void HasData_ReceivesDataTableWithRows_ReturnsTrue()
        {
            //arrange
            var expected = true;

            //act
            using (DataTable dt = GetDataTableWithRows())
            {
                var actual = dt.HasData();

                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void ToObjectList_ReceivesNullDataTable_ReturnsEmptyList()
        {
            using (var dataTable = GetNullDataTable())
            {
                var actual = dataTable.ToObjectList<Widget>();
                Assert.AreEqual(0, actual.Count, "Count");
            }
        }

        [TestMethod]
        public void ToObjectList_ReceivesDataTableWithNoRows_ReturnsEmptyList()
        {
            using (var dataTable = GetDataTableWithNoRows())
            {
                var actual = dataTable.ToObjectList<Widget>();
                Assert.AreEqual(0, actual.Count, "Count");
            }
        }

        [TestMethod]
        public void ToObjectList_LoadsIntegerProperties()
        {
            using (var dataTable = new DataTable())
            {
                dataTable.Columns.Add(nameof(Widget.Id), typeof(int));
                dataTable.Rows.Add(123);

                var actual = dataTable.ToObjectList<Widget>();

                Assert.AreEqual(1, actual.Count, "Count");
                Assert.AreEqual(123, actual[0].Id, "Id");
            }
        }

        [TestMethod]
        public void ToObjectList_LoadsStringProperties()
        {
            using (var dataTable = new DataTable())
            {
                dataTable.Columns.Add(nameof(Widget.Name), typeof(string));
                dataTable.Rows.Add("Widget1");

                var actual = dataTable.ToObjectList<Widget>();

                Assert.AreEqual(1, actual.Count, "Count");
                Assert.AreEqual("Widget1", actual[0].Name, "Name");
            }
        }

        [TestMethod]
        public void ToObjectList_LoadsBoolProperties_FromBitFields()
        {
            using (var dataTable = new DataTable())
            {
                dataTable.Columns.Add(nameof(Widget.IsActive), typeof(byte));
                dataTable.Rows.Add(1);
                dataTable.Rows.Add(0);

                var actual = dataTable.ToObjectList<Widget>();

                Assert.AreEqual(2, actual.Count, "Count");
                Assert.AreEqual(true, actual[0].IsActive, "[0]");
                Assert.AreEqual(false, actual[1].IsActive, "[1]");
            }
        }

        [TestMethod]
        public void ToObjectList_LoadsEnumProperties_FromIntegerFields()
        {
            using (var dataTable = new DataTable())
            {
                dataTable.Columns.Add(nameof(Widget.DayOfWeek), typeof(int));
                dataTable.Rows.Add((int) DayOfWeek.Tuesday);

                var actual = dataTable.ToObjectList<Widget>();

                Assert.AreEqual(1, actual.Count, "Count");
                Assert.AreEqual(DayOfWeek.Tuesday, actual[0].DayOfWeek, "DayOfWeek");
            }
        }

        [TestMethod]
        public void ToObjectList_RespectsMapFromAttribute()
        {
            using (var dataTable = new DataTable())
            {
                dataTable.Columns.Add(Widget.MappedFieldDatabaseName, typeof(int));
                dataTable.Rows.Add(123);

                var actual = dataTable.ToObjectList<Widget>();

                Assert.AreEqual(1, actual.Count, "Count");
                Assert.AreEqual(123, actual[0].DotNetField, $"DotNetField (from {Widget.MappedFieldDatabaseName})");
            }
        }

        #region Helper methods: get data tables

        private DataTable GetNullDataTable()
        {
            return null;
        }

        private DataTable GetDataTableWithNoRows()
        {
            return new DataTable();
        }

        private DataTable GetDataTableWithRows()
        {
            var dt = new DataTable();
            dt.Columns.Add("WidgetId");
            dt.Columns.Add("WidgetDescription");

            var row = dt.NewRow();
            dt.Rows.Add(row);
            return dt;
        }

        #endregion
        #region Nested class: Widget

        [UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
        private class Widget
        {
            public const string MappedFieldDatabaseName = "DatabaseField";

            public int Id { get; set; }
            public string Name { get; set; }
            public bool IsActive { get; set; }
            public DayOfWeek DayOfWeek { get; set; }
            [MapFrom(MappedFieldDatabaseName)]
            public int DotNetField { get; set; }
        }

        #endregion
    }
}
