﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
namespace UnitTests
{
    [TestClass]
    public class FunctionalExtensionsTests
    {
        [TestMethod]
        public void FunctionalExtensions_WithUseFunction_DisposesObject()
        {
            //arrange
            var disposable = new MockDisposable();
            var expected = true;

            //act
            disposable.Use(x => x.MyMethod());

            //assert
            Assert.AreEqual(expected, disposable.IsDisposed);
        }

        [TestMethod]
        public void FunctionalExtensions_WithoutUseFunction_DoesNotDisposeObject()
        {
            //arrange
            var disposable = new MockDisposable();
            var expected = false;

            //act
            disposable.MyMethod();

            //assert
            Assert.AreEqual(expected, disposable.IsDisposed);
        }
    }
}
