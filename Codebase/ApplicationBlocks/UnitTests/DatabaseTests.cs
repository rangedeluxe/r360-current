﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Common;
using WFS.RecHub.ApplicationBlocks.DataAccess;
using System.Data;
using System.Collections.Generic;
using System.Configuration.Fakes;
using System.Configuration;
using Microsoft.QualityTools.Testing.Fakes;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;

namespace UnitTests
{
    [TestClass]
    public class DatabaseTests
    {
        [TestMethod]
        public void Database_CreateSqlServerParm_IsSuccessful()
        {
            //arrange
            var expected = 1;
            var connectionStringSettings = new ConnectionStringSettings("sqlserverdatabase", "", "System.Data.SqlClient".Encrypt());

            using (ShimsContext.Create())
            {
                ShimConfigurationManager.ConnectionStringsGet = () =>
                {
                    return new ConnectionStringSettingsCollection() 
                    {
                        connectionStringSettings 
                    };
                };

                //act
                var actual = new List<DbParameter>
                {
                    Database.CreateParm("@WidgetId", Database.GetFactory(connectionStringSettings), DbType.Int64, ParameterDirection.Input, 1),
                }
                    .Count;

                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void Database_CreateOracleParm_IsSuccessful()
        {
            //arrange
            var expected = 1;
            var connectionStringSettings = new ConnectionStringSettings("oracledatabase", string.Empty, "System.Data.OracleClient".Encrypt());

            using (ShimsContext.Create())
            {
                ShimConfigurationManager.ConnectionStringsGet = () =>
                {
                    return new ConnectionStringSettingsCollection() 
                    {
                        connectionStringSettings 
                    };
                };

                //act
                var actual = new List<DbParameter>
                {
                    Database.CreateParm("@WidgetId", Database.GetFactory(connectionStringSettings), DbType.Int64, ParameterDirection.Input, 1),
                }
                    .Count;

                //assert
                Assert.AreEqual(expected, actual);
            }
        }
        
        [TestMethod]
        public void Database_CreateDb2Parm_IsSuccessful()
        {
            //arrange
            var expected = 1;
            var connectionStringSettings = new ConnectionStringSettings("db2database", string.Empty, "System.Data.OleDb".Encrypt());

            using (ShimsContext.Create())
            {
                ShimConfigurationManager.ConnectionStringsGet = () =>
                {
                    return new ConnectionStringSettingsCollection() 
                    {
                        connectionStringSettings 
                    };
                };

                //act
                var actual = new List<DbParameter>
                {
                    Database.CreateParm("@WidgetId", Database.GetFactory(connectionStringSettings), DbType.Int64, ParameterDirection.Input, 1),
                }
                    .Count;

                //assert
                Assert.AreEqual(expected, actual);
            }
        }
    }
}
