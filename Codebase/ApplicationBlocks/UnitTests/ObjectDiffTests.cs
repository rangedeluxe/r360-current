﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common;

namespace UnitTests
{
    [TestClass]
    public class ObjectDiffTests
    {
        [TestMethod]
        public void Diff_Should_Find_Name_Difference()
        {
            // Arrange
            var obj1 = new ExampleObject() { Name = "Apple", SomeDate = new DateTime(2015, 1, 1), SomeInt = 10 };
            var obj2 = new ExampleObject() { Name = "Orange", SomeDate = new DateTime(2015, 1, 1), SomeInt = 10 };

            // Act
            var diff = Utility.PropertyWiseObjectDiff(obj1, obj2);

            // Assert
            Assert.IsTrue(diff.Any(x => x.PropertyName == "Name" && x.OldValue.ToString() == "Apple" && x.NewValue.ToString() == "Orange"));
            Assert.IsTrue(diff.Count == 1);
        }

        [TestMethod]
        public void Diff_Should_Find_Int_Difference()
        {
            // Arrange
            var obj1 = new ExampleObject() { Name = "Apple", SomeDate = new DateTime(2015, 1, 1), SomeInt = 10 };
            var obj2 = new ExampleObject() { Name = "Apple", SomeDate = new DateTime(2015, 1, 1), SomeInt = 12 };

            // Act
            var diff = Utility.PropertyWiseObjectDiff(obj1, obj2);

            // Assert
            Assert.IsTrue(diff.Any(x => x.PropertyName == "SomeInt" && (int)x.OldValue == 10 && (int)x.NewValue == 12));
            Assert.IsTrue(diff.Count == 1);
        }
    }

    public class ExampleObject
    {
        public string Name { get; set; }
        public int SomeInt { get; set; }
        public DateTime SomeDate { get; set; }
    }
}
