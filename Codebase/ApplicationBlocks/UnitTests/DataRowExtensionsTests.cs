﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;

namespace UnitTests
{
    [TestClass]
    public class DataRowExtensionsTests
    {
        [TestInitialize]
        public void SetUp()
        {
            DataTable = new DataTable();
            DataTable.Columns.Add("WidgetInteger");
            DataTable.Columns.Add("WidgetBigInteger");
            DataTable.Columns.Add("WidgetString");
        }
        [TestCleanup]
        public void TearDown()
        {
            DataTable?.Dispose();
            DataTable = null;
        }

        private DataTable DataTable { get; set; }

        [TestMethod]
        public void GetInteger_ReceivesNullDataRow_ReturnsIntegerMinValue()
        {
            //arrange
            var expected = int.MinValue;
            var dr = GetNullDataRow();

            //act
            var actual = dr.GetInteger("widgetinteger");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetInteger_ReceivesDataRowWithoutColumnRequested_ReturnsIntegerMinValue()
        {
            //arrange
            var expected = int.MinValue;
            var dr = GetDataRow();

            //act
            var actual = dr.GetInteger("widget");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetInteger_ReceivesDataRowWithDBNullColumnRequested_ReturnsZero()
        {
            //arrange
            var expected = 0;
            var dr = GetDataRowWithDbNullValues();

            //act
            var actual = dr.GetInteger("widgetinteger");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetInteger_ReceivesDataRowWithBadValue_ReturnsZero()
        {
            //arrange
            var expected = 0;
            var dr = GetDataRowWithBadValues();

            //act
            var actual = dr.GetInteger("widgetinteger");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetInteger_ReceivesDataRowWithValidData_ReturnsCorrectValue()
        {
            //arrange
            var expected = int.MaxValue;
            var dr = GetDataRow();

            //act
            var actual = dr.GetInteger("widgetinteger");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBigInteger_ReceivesNullDataRow_ReturnsBigIntegerMinValue()
        {
            //arrange
            var expected = long.MinValue;
            var dr = GetNullDataRow();

            //act
            var actual = dr.GetBigInteger("widgetbiginteger");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBigInteger_ReceivesDataRowWithoutColumnRequested_ReturnsBigIntegerMinValue()
        {
            //arrange
            var expected = long.MinValue;
            var dr = GetDataRow();

            //act
            var actual = dr.GetBigInteger("widget");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBigInteger_ReceivesDataRowWithDBNullColumnRequested_ReturnsZero()
        {
            //arrange
            var expected = 0;
            var dr = GetDataRowWithDbNullValues();

            //act
            var actual = dr.GetBigInteger("widgetbiginteger");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBigInteger_ReceivesDataRowWithBadValue_ReturnsZero()
        {
            //arrange
            var expected = 0;
            var dr = GetDataRowWithBadValues();

            //act
            var actual = dr.GetBigInteger("widgetbiginteger");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBigInteger_ReceivesDataRowWithValidData_ReturnsCorrectValue()
        {
            //arrange
            var expected = long.MaxValue;
            var dr = GetDataRow();

            //act
            var actual = dr.GetBigInteger("widgetbiginteger");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ColumnExists_ReceivesNullDataRow_ReturnsFalse()
        {
            //arrange
            var expected = false;
            var dr = GetNullDataRow();

            //act
            var actual = dr.ColumnExists("widget");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ColumnExists_ReceivesValidDataRow_ReturnsTrue()
        {
            //arrange
            var expected = true;
            var dr = GetDataRow();

            //act
            var actual = dr.ColumnExists("widgetstring");

            //assert
            Assert.AreEqual(expected, actual);
        }

        private DataRow GetNullDataRow()
        {
            return null;
        }

        private DataRow GetDataRow()
        {
            var row = DataTable.NewRow();

            row["WidgetInteger"] = int.MaxValue;
            row["WidgetBigInteger"] = Int64.MaxValue;
            row["WidgetString"] = "widget";

            DataTable.Rows.Add(row);
            return row;
        }

        private DataRow GetDataRowWithDbNullValues()
        {
            var row = DataTable.NewRow();
            row["WidgetInteger"] = DBNull.Value;
            row["WidgetBigInteger"] = DBNull.Value;
            row["WidgetString"] = DBNull.Value;

            DataTable.Rows.Add(row);
            return row;
        }

        private DataRow GetDataRowWithBadValues()
        {
            var row = DataTable.NewRow();
            row["WidgetInteger"] = "this won't parse as an integer";
            row["WidgetBigInteger"] = "this won't parse as a long";

            DataTable.Rows.Add(row);
            return row;
        }
    }
}
