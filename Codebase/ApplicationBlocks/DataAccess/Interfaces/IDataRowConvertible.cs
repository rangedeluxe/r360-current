﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.DataAccess.Interfaces
{
    public interface IDataRowConvertible<T>
    {
        T ConvertTo(DataRow dataRow);
    }
}
