﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.DataAccess
{
    public class DatabaseParameters
    {
        public ConnectionStringSettings ConnectionInfo
        {
            get;
            set;
        }

        public CommandType CommandType
        {
            get;
            set;
        }

        public string CommandToExecute
        {
            get;
            set;
        }

        public IList<DbCommand> CommandList
        {
            get;
            set;
        }
    }
}
