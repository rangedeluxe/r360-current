﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;

namespace WFS.RecHub.ApplicationBlocks.DataAccess.Extensions
{
    public static class DataRowExtensions
    {
        /// <summary>
        /// This generic extension method is used to extend the 
        /// DataRow object and return the column value.
        /// </summary>
        /// <typeparam name="T">Generic type parameter</typeparam>
        /// <param name="dataRow">DataRow object to look up the column value on</param>
        /// <param name="columnName">The column name to look for on the DataRow object</param>
        /// <returns>The column value of type T</returns>
        public static T GetColumnValue<T>(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return default(T);

            if (!dataRow.Table.Columns.Contains(columnName))
                return default(T);

            if (dataRow[columnName] == DBNull.Value)
                return default(T);

            T t = dataRow[columnName].ToString().ParseTo<T>();
            if (t == null)
                return (T)dataRow[columnName];

            return t;
        }

        /// <summary>
        /// Returns the value of the specified data source column as an integer value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column Name from the data source</param>
        /// <returns>Integer representation of the DataRow object column data</returns>
        public static int GetInteger(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return int.MinValue;

            if (!dataRow.Table.Columns.Contains(columnName))
                return int.MinValue;

            if (dataRow[columnName] == DBNull.Value)
                return 0;

            int result = 0;
            if (int.TryParse(dataRow[columnName].ToString(), out result))
                return result;
            else
                return 0;
        }


        /// <summary>
        /// Returns the value of the specified data source column as an integer value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column Name from the data source</param>
        /// <returns>Integer representation of the DataRow object column data</returns>
        public static Int64 GetBigInteger(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return Int64.MinValue;

            if (!dataRow.Table.Columns.Contains(columnName))
                return Int64.MinValue;

            if (dataRow[columnName] == DBNull.Value)
                return 0;

            Int64 result = 0;
            if (Int64.TryParse(dataRow[columnName].ToString(), out result))
                return result;
            else
                return 0;

        }

        /// <summary>
        /// Returns the value of the specified data source column as a double value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column Name from the data source</param>
        /// <returns>Double representation of the DataRow object column data</returns>
        public static double GetDouble(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return double.MinValue;

            if (!dataRow.Table.Columns.Contains(columnName))
                return double.MinValue;

            if (dataRow[columnName] == DBNull.Value)
                return double.MinValue;

            double result = 0;
            if (double.TryParse(dataRow[columnName].ToString(), out result))
                return result;
            else
                return 0;

        }

        /// <summary>
        /// Returns the value of the specified data source column as a decimal value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column Name from the data source</param>
        /// <returns>Decimal representation of the DataRow object column data</returns>
        public static decimal GetDecimal(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return decimal.MinValue;

            if (!dataRow.Table.Columns.Contains(columnName))
                return decimal.MinValue;

            if (dataRow[columnName] == DBNull.Value)
                return decimal.MinValue;

            decimal result = 0;

            if (decimal.TryParse(dataRow[columnName].ToString(), out result))
                return result;
            else
                return 0;

        }

        /// <summary>
        /// Returns the value of the specified data source column as a decimal value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column Name from the data source</param>
        /// <returns>Decimal representation of the DataRow object column data</returns>
        public static Nullable<decimal> GetNullableDecimal(this DataRow dataRow, string columnName)
        {
            decimal d = GetDecimal(dataRow, columnName);
            return d == decimal.MinValue ? new Nullable<decimal>() : d;
        }

        /// <summary>
        /// Returns the value of the specified data source column as a double value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column Name from the data source</param>
        /// <returns>Double representation of the DataRow object column data</returns>
        public static Nullable<double> GetNullableDouble(this DataRow dataRow, string columnName)
        {
            double d = GetDouble(dataRow, columnName);
            return d == double.MinValue ? new Nullable<double>() : d;
        }

        /// <summary>
        /// Returns the value of the specified data source column as a guid value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column Name from the data source</param>
        /// <returns>Guid representation of the DataRow object column data</returns>
        public static Guid GetGuid(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return Guid.Empty;

            if (!dataRow.Table.Columns.Contains(columnName))
                return Guid.Empty;

            if (dataRow[columnName] == DBNull.Value)
                return Guid.Empty;

            var result = Guid.Empty;

            if (Guid.TryParse(dataRow[columnName].ToString(), out result))
                return result;
            else
                return Guid.Empty;

        }

        /// <summary>
        /// Returns the value of the specified data source column as an string value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column name from the data source</param>
        /// <returns>String representation of the DataRow object column data</returns>
        public static string GetString(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return string.Empty;

            if (!dataRow.Table.Columns.Contains(columnName))
                return string.Empty;

            if (dataRow[columnName] == DBNull.Value)
                return string.Empty;
            else
                return dataRow[columnName].ToString().Trim();
        }

        /// <summary>
        /// Returns the value of the specified data source column as an DateTime value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column name from the data source</param>
        /// <returns>DateTime representation of the DataRow object column data</returns>
        public static DateTime GetDateTime(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return DateTime.MinValue;

            if (!dataRow.Table.Columns.Contains(columnName))
                return DateTime.MinValue;

            if (dataRow[columnName] == DBNull.Value)
                return DateTime.MinValue;

            DateTime result = new DateTime();

            if (DateTime.TryParse(dataRow[columnName].ToString(), out result))
                return result;
            else
                return DateTime.MinValue;

        }

        /// <summary>
        /// Returns the value of the specified data source column as a boolean value
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column name from the data source</param>
        /// <returns>Boolean representation of the DataRow object column data</returns>
        public static bool GetBoolean(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return false;

            if (!dataRow.Table.Columns.Contains(columnName))
                return false;

            if (dataRow[columnName] == DBNull.Value)
                return false;

            bool booleanValue = false;

            if (bool.TryParse(dataRow[columnName].ToString(), out booleanValue))
                return booleanValue;
            else
                return false;
        }

        /// <summary>
        /// Returns true/false whether the requested column exists in the datarow
        /// </summary>
        /// <param name="dataRow">DataRow object passed in from a data source</param>
        /// <param name="columnName">Column name from the data source</param>
        /// <returns>True/False whether the column name exists in the datarow</returns>
        public static bool ColumnExists(this DataRow dataRow, string columnName)
        {
            if (dataRow == null)
                return false;

            if (!dataRow.Table.Columns.Contains(columnName))
                return false;

            return true;
        }

    }
}
