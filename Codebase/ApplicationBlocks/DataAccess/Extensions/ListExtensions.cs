﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;
using WFS.RecHub.ApplicationBlocks.DataAccess.Interfaces;

namespace WFS.RecHub.ApplicationBlocks.DataAccess.Extensions
{
    public static class ListExtensions
    {
        public static DataTable ToDataTableParams<T>(this IList<T> pList) where T : ITableParam
        {
            var props = typeof(T).GetProperties();
            var table = new DataTable();
            foreach (var p in props)
            {
                var mapfrom = (p.GetCustomAttributes(typeof(MapFromAttribute), false).FirstOrDefault() as MapFromAttribute)?.Key ?? p.Name;
                table.Columns.Add(mapfrom, Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType);
            }
            foreach (var obj in pList)
            {
                var row = table.NewRow();
                var members = obj.GetType().GetProperties();
                foreach (var value in members)
                {
                    var col =
                        (value.GetCustomAttributes(typeof(MapFromAttribute), false).FirstOrDefault() as MapFromAttribute)
                        ?.Key ?? value.Name;
                    row[col] = value.GetValue(obj) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
