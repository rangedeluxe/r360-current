﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.DataAccess.Extensions
{
    public static class DataSetExtensions
    {
        /// <summary>
        /// Returns true if the 1st table in the dataset has rows
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static bool HasData(this DataSet ds)
        {
            return HasData(ds, 0);
        }

        /// <summary>
        /// Returns true is the specified table in the dataset has rows
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="tableIndex"></param>
        /// <returns></returns>
        public static bool HasData(this DataSet ds, int tableIndex)
        {
            if (ds != null && ds.Tables.Count > tableIndex && ds.Tables[tableIndex].HasData())
                return true;
            else
                return false;
        }
    }
}
