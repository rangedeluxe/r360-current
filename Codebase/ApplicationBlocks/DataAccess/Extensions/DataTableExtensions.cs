﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace WFS.RecHub.ApplicationBlocks.DataAccess.Extensions
{
    public static class DataTableExtensions
    {
        /// <summary>
        /// Returns true if the datatable has rows
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool HasData(this DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
                return true;
            else
                return false;
        }

        public static List<T> ToObjectList<T>(this DataTable dt)
        {
            var props = typeof(T).GetProperties();
            var output = new List<T>();
            if (!dt.HasData())
                return output;

            foreach (DataRow r in dt.Rows)
            {
                var obj = Activator.CreateInstance<T>();
                foreach (var p in props)
                {
                    // Gather a list of possibilities for the keymapping.
                    var keys = new List<string>();
                    var mapfrom = p.GetCustomAttributes(typeof(MapFromAttribute), false).FirstOrDefault();
                    if (mapfrom != null)
                        keys.AddRange(((MapFromAttribute)mapfrom).Key
                            .Replace(" ", string.Empty)
                            .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                    keys.Add(p.Name);

                    // Find the key we should use, in priority order.
                    var key = keys.FirstOrDefault(x => dt.Columns.Contains(x));
                    if (key == null)
                        continue;

                    // Special case: if the property is bool and the column is some kind of int.
                    // BIT columns are returned as System.Byte, but if a stored proc returns a 1 or 0 and doesn't take
                    // special care to cast it to BIT, we could see an int instead. This should work for either.
                    var val = r.GetColumnValue<object>(key);
                    if (p.PropertyType == typeof(bool) && dt.Columns[key].DataType != typeof(bool))
                        val = Convert.ToInt32(val) != 0;

                    p.SetValue(obj, val);
                }
                output.Add(obj);
            }
            return output;
        }
    }
}
