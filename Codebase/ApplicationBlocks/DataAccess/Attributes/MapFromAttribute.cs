﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ApplicationBlocks.DataAccess.Attributes
{
    public class MapFromAttribute : Attribute
    {
        public string Key { get; set; }
        public MapFromAttribute(string key)
        {
            Key = key;
        }
    }
}