﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;

namespace WFS.RecHub.ApplicationBlocks.DataAccess
{
    public static class Database
    {
        public static DbProviderFactory GetFactory(ConnectionStringSettings connectionStringSettings)
        {
            if (connectionStringSettings == null)
                throw new ArgumentNullException("connectionStringSettings");


            return DbProviderFactories.GetFactory(new DecryptedValue(connectionStringSettings.ProviderName).Value);
        }

        /// <summary>
        /// Used to create a DbParameter object
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="dbProviderFactory">The DBProvider factory to use to create the parm</param>
        /// <param name="type">The data type to be used</param>
        /// <param name="direction">Input, Ouput, etc</param>
        /// <param name="value">The value of the paramater field</param>
        /// <returns>DBParameter object</returns>
        public static DbParameter CreateParm(string parmName, DbProviderFactory dbProviderFactory, DbType type, ParameterDirection direction, Object value)
        {
            return CreateParm(parmName, dbProviderFactory, type, direction, value, 0);
        }

        /// <summary>
        /// Used to create a DbParameter object
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="dbProviderFactory">The DBProvider factory to use to create the parm</param>
        /// <param name="type">The data type to be used</param>
        /// <param name="direction">Input, Ouput, etc</param>
        /// <param name="value">The value of the paramater field</param>
        /// <param name="size">The size of the parameter</param>
        /// <returns>DBParameter object</returns>
        public static DbParameter CreateParm(string parmName, DbProviderFactory dbProviderFactory, DbType type, ParameterDirection direction, Object value, int size)
        {
            DbParameter dbParm = null;

            if (dbProviderFactory == null)
                throw new ArgumentNullException("dbProviderFactory");

            dbParm = dbProviderFactory.CreateParameter();
            dbParm.ParameterName = parmName;
            dbParm.DbType = type;
            dbParm.Direction = direction;
            if (size > 0)
                dbParm.Size = size;

            if (value == null)
                dbParm.Value = DBNull.Value;
            else
                dbParm.Value = value;

            return dbParm;
        }

        public static DataSet GetDataSet(ConnectionStringSettings connectionStringSettings, CommandType commandType, string commandText, IList<DbParameter> parms)
        {
            DbProviderFactory dbProviderFactory = GetFactory(connectionStringSettings);

            using (DbConnection dbConnection = dbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = new DecryptedValue(connectionStringSettings.ConnectionString).Value;
                return GetDataSet(dbConnection, dbProviderFactory, commandType, commandText, parms);
            }
        }

        public static DataSet GetDataSet(ConnectionStringSettings connectionStringSettings, CommandType commandType, string commandText)
        {
            return GetDataSet(connectionStringSettings, commandType, commandText, null);
        }

        public static DataSet GetDataSet(DbConnection connection, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText)
        {
            return GetDataSet(connection, dbProviderFactory, commandType, commandText, null);
        }

        public static DataSet GetDataSet(DbConnection connection, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText, IList<DbParameter> parms)
        {
            using (DataSet ds = new DataSet())
            using (DbCommand cmd = dbProviderFactory.CreateCommand())
            using (DbDataAdapter da = dbProviderFactory.CreateDataAdapter())
            {
                ds.Locale = System.Globalization.CultureInfo.CurrentCulture;
                cmd.CommandText = commandText;
                cmd.CommandType = commandType;

                cmd.Connection = connection;
                da.SelectCommand = cmd;

                if (parms != null)
                {
                    foreach (DbParameter parm in parms)  // add the parms
                    {
                        cmd.Parameters.Add(parm);
                    }
                }

                if (connection.State != System.Data.ConnectionState.Open)
                    connection.Open();

                da.Fill(ds); // fill the dataset

                return ds;
            }

        }

        public static int ExecuteNonQuery(ConnectionStringSettings connectionStringSettings, CommandType commandType, string commandText, IList<DbParameter> parms)
        {
            DbProviderFactory dbFactory = GetFactory(connectionStringSettings);

            using (DbConnection dbConnection = dbFactory.CreateConnection())
            {
                dbConnection.ConnectionString = new DecryptedValue(connectionStringSettings.ConnectionString).Value;
                return ExecuteNonQuery(dbConnection, dbFactory, commandType, commandText, parms);
            }
        }

        public static int ExecuteNonQuery(ConnectionStringSettings connectionStringSettings, CommandType commandType, string commandText)
        {
            return ExecuteNonQuery(connectionStringSettings, commandType, commandText, null);
        }

        public static int ExecuteNonQuery(DbTransaction transaction, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText, IList<DbParameter> parms)
        {
            using (DbCommand command = CreateCommand(dbProviderFactory, commandType, commandText, parms))
            {
                return ExecuteNonQuery(transaction, command);
            }
        }

        public static int ExecuteNonQuery(DbTransaction transaction, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText)
        {
            return ExecuteNonQuery(transaction, dbProviderFactory, commandType, commandText, null);
        }

        public static int ExecuteNonQuery(DbConnection connection, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText)
        {
            return ExecuteNonQuery(connection, dbProviderFactory, commandType, commandText, null);
        }

        public static DbCommand CreateCommand(DbProviderFactory dbProviderFactory, CommandType commandType, string commandText, IList<DbParameter> parms)
        {
            using (DbCommand cmd = dbProviderFactory.CreateCommand())
            {
                cmd.CommandText = commandText;
                cmd.CommandType = commandType;
                if (parms != null)
                {
                    foreach (DbParameter parm in parms)  // add the parms
                    {
                        cmd.Parameters.Add(parm);
                    }
                }

                return cmd;
            }
        }

        public static int ExecuteNonQuery(DbConnection connection, IList<DbCommand> commandList)
        {
            if (commandList == null)
                throw new ArgumentNullException("commandList");

            int rowsAffected = 0;

            foreach (DbCommand command in commandList)
            {
                rowsAffected += ExecuteNonQuery(connection, command);

            }

            return rowsAffected;
        }

        public static int ExecuteNonQuery(DbConnection connection, DbCommand command)
        {

            if (command == null)
                throw new ArgumentNullException("command");

            if (connection == null)
                throw new ArgumentNullException("connection");

            command.Connection = connection;

            if (connection.State != System.Data.ConnectionState.Open)
                connection.Open();

            return command.ExecuteNonQuery();
        }

        public static int ExecuteNonQuery(DbTransaction transaction, DbCommand command)
        {

            if (command == null)
                throw new ArgumentNullException("command");

            if (transaction == null)
                throw new ArgumentNullException("transaction");

            command.Transaction = transaction;

            return ExecuteNonQuery(transaction.Connection, command);

        }

        public static int ExecuteNonQuery(DbConnection connection, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText, IList<DbParameter> parms)
        {
            using (DbCommand command = CreateCommand(dbProviderFactory, commandType, commandText, parms))
            {
                return ExecuteNonQuery(connection, command);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commands">List of command objects</param>
        /// <returns>The total number of rows affected by the transaction</returns>
        public static int ExecuteTransaction(IList<DatabaseParameters> parametersList)
        {
            if (parametersList == null)
                throw new ArgumentNullException("parametersList");

            int rowsAffected = 0;

            using (TransactionScope ts = new TransactionScope())
            {
                foreach (DatabaseParameters parameter in parametersList)
                {
                    using (DbConnection connection = GetFactory(parameter.ConnectionInfo).CreateConnection())
                    {
                        connection.ConnectionString = new DecryptedValue(parameter.ConnectionInfo.ConnectionString).Value;
                        rowsAffected += ExecuteNonQuery(connection, parameter.CommandList);
                    }
                }

                ts.Complete();
            }

            return rowsAffected;
        }

        public static T ExecuteScalar<T>(ConnectionStringSettings connectionStringSettings, CommandType commandType, string commandText, IList<DbParameter> parms)
        {
            DbProviderFactory dbProviderFactory = GetFactory(connectionStringSettings);

            using (DbConnection dbConnection = dbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = new DecryptedValue(connectionStringSettings.ConnectionString).Value;
                return ExecuteScalar<T>(dbConnection, dbProviderFactory, commandType, commandText, parms);
            }
        }

        public static T ExecuteScalar<T>(DbConnection connection, DbCommand command)
        {

            if (command == null)
                throw new ArgumentNullException("command");

            if (connection == null)
                throw new ArgumentNullException("connection");

            command.Connection = connection;

            if (connection.State != System.Data.ConnectionState.Open)
                connection.Open();

            return (T)command.ExecuteScalar();

        }

        public static T ExecuteScalar<T>(DbTransaction transaction, DbCommand command)
        {

            if (command == null)
                throw new ArgumentNullException("command");

            if (transaction == null)
                throw new ArgumentNullException("transaction");

            command.Transaction = transaction;

            return ExecuteScalar<T>(transaction.Connection, command);

        }

        public static T ExecuteScalar<T>(ConnectionStringSettings connectionStringSettings, CommandType commandType, string commandText)
        {
            return ExecuteScalar<T>(connectionStringSettings, commandType, commandText, null);
        }

        public static T ExecuteScalar<T>(DbTransaction transaction, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText, IList<DbParameter> parms)
        {
            using (DbCommand command = CreateCommand(dbProviderFactory, commandType, commandText, parms))
            {
                return ExecuteScalar<T>(transaction, command);
            }
        }

        public static T ExecuteScalar<T>(DbTransaction transaction, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText)
        {
            return ExecuteScalar<T>(transaction, dbProviderFactory, commandType, commandText, null);
        }

        public static T ExecuteScalar<T>(DbConnection connection, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText)
        {
            return ExecuteScalar<T>(connection, dbProviderFactory, commandType, commandText, null);
        }

        public static T ExecuteScalar<T>(DbConnection connection, DbProviderFactory dbProviderFactory, CommandType commandType, string commandText, IList<DbParameter> parms)
        {
            using (DbCommand command = CreateCommand(dbProviderFactory, commandType, commandText, parms))
            {
                return ExecuteScalar<T>(connection, command);
            }
        }
    }
}
