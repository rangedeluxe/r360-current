﻿using System;
using Wfs.Logging;

namespace WFS.LTA.Common
{
    public static class LtaLogExtensions
    {
        // ReSharper disable once InconsistentNaming
        public static void logError(this IWfsLog log, Exception exception, string message, string source)
        {
            log.Log(WfsLogLevel.Error, message, source: source, exception: exception);
        }
        // ReSharper disable once InconsistentNaming
        public static void logEvent(this IWfsLog log, string message, string source, LTAMessageType messageType,
            LTAMessageImportance messageImportance)
        {
            var level = ToWfsLogLevel(messageType, messageImportance);
            log.Log(level, message, args: null, source: source);
        }
        private static WfsLogLevel ToWfsLogLevel(LTAMessageType type, LTAMessageImportance importance)
        {
            switch (type)
            {
                case LTAMessageType.Error:
                    return WfsLogLevel.Error;
                case LTAMessageType.Warning:
                    return WfsLogLevel.Warn;
                case LTAMessageType.Information:
                    switch (importance)
                    {
                        case LTAMessageImportance.Essential:
                            return WfsLogLevel.Info;
                        case LTAMessageImportance.Verbose:
                            return WfsLogLevel.Debug;
                        case LTAMessageImportance.Debug:
                            return WfsLogLevel.Trace;
                        default:
                            throw new ArgumentOutOfRangeException("importance", importance, null);
                    }
                default:
                    throw new ArgumentOutOfRangeException("type", type, null);
            }
        }
    }
}