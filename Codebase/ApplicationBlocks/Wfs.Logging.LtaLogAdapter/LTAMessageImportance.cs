﻿namespace WFS.LTA.Common
{
    // ReSharper disable once InconsistentNaming
    public enum LTAMessageImportance
    {
        Essential,
        Verbose,
        Debug,
    }
}