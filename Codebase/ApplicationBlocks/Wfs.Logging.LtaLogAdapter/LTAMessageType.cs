﻿namespace WFS.LTA.Common
{
    // ReSharper disable once InconsistentNaming
    public enum LTAMessageType
    {
        Error,
        Information,
        Warning,
    }
}