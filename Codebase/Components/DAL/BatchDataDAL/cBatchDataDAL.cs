﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples	
* Date: 04/02/2011
*
* Purpose:
*
* Modification History
* CR 31536 JMC 04/02/2011
*   -New file.
* CR 49675 WJS 1/30/2012
*  - Pass in source processing date key
* CR 52261 WJS 5/15/2012
*   -Change GetItemDataSetupField and  GetBatchDataSetupField to use stored procedure and not inline sql
* WI 72613 CRG 02/18/2013
*	Convert cBatchDataDAL dynamic SQL to stored procedures
* WI 87322 CRG 02/18/2013
*	Remove SQLBatchData.cs
* WI 72164 CRG 02/18/2013
*	Convert cBatchDataDAL.InsertFactBatchData(int, int, int, int, int, int, int, List<cBatchData>) to a stored procedure call
* WI 72165 CRG 02/18/2013
*	Convert cBatchDataDAL.InsertFactItemData(int, int, int, int, int, int, int, int, int, List<cItemData>) to a stored procedure call
* WI 88724 CRG 02/21/2013 
*    - Overload the LogError function for two Parameters (Exception and Class name)
* WI 129176 CMC 02/11/2014 
*    - Modify BatchDataDAL to call new 2.01 stored proc RecHubData.usp_dimBatchDataSetupFields_Get_ByShortName
* WI 129251 CMC 02/11/2014 
*    - Modify BatchDataDAL to call new 2.01 stored proc RecHubData.usp_dimItemDataSetupFields_Get_ByShortName
* WI 135735 CMC 02/24/2014 
*    - Modified for 2.01
* WI 145906 DJW 06/05/2014
*    - Modified to handle the int BatchID to Int64 BatchID 
* WI 151771 CMC 07/02/2014
*    - Handle new stored proc param in GetBatchDataSetupField and GetItemDataSetupField 
* WI 166044 SAS 09/16/2014
*   -New function added for calling ImageRPS Stored procedures
* WI 166698 SAS 09/18/2014
*   -Functions updated for calling ImageRPS Stored procedures  
* WI 172888 SAS 10/17/2014
*   -Changes done to convert return data for Report Stored procedures
* WI 192043 TWE 02/25/2015
*    change due to parm list changes in stored procedure
******************************************************************************/
namespace WFS.RecHub.DAL
{

    /// <summary>
    /// Used to access Batch Data from database
    /// </summary>
    public class cBatchDataDAL : _DALBase
    {

        /// <summary>
        /// The INTEGRA PAY BATCH SOURCE NAME
        /// </summary>
        private const string INTEGRAPAYBATCHSOURCENAME = "integraPAY";

        /// <summary>
        /// Initializes a new instance of the <see cref="cBatchDataDAL" /> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        public cBatchDataDAL(string vSiteKey)
            : base(vSiteKey)
        {

        }

        /// <summary>
        /// Get OLTA Keys give DBO Keys by ID
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="CustomerID">The customer ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="ProcessingDate">The processing date.</param>
        /// <param name="DepositDate">The deposit date.</param>
        /// <param name="dtOLTAKeysTable">The OLTA keys table.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetDBOKeysToOLTAKeys_ByID(int BankID, int CustomerID, int LockboxID, long BatchID, DateTime ProcessingDate, DateTime DepositDate, out DataTable dtOLTAKeysTable)
        {
            bool bRtnval = false;
            dtOLTAKeysTable = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteCustomerID", SqlDbType.Int, CustomerID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmProcessingDate", SqlDbType.DateTime, ProcessingDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSourceKey", SqlDbType.VarChar, INTEGRAPAYBATCHSOURCENAME, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factBatchSummary_ConvertKeys_ByID", parms, out dtOLTAKeysTable);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetDBOKeysToOLTAKeys_ByID");
            }

            return bRtnval;
        }

        #region ItemData

        /// <summary>
        /// Gets the item data setup field.
        /// </summary>
        /// <param name="sBatchSourceName">Name of the s batch source.</param>
        /// <param name="modificationDate">The modification date.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetItemDataSetupField(string sBatchSourceName, DateTime? modificationDate, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmImportShortName", SqlDbType.VarChar, sBatchSourceName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmModificationDate", SqlDbType.DateTime, modificationDate, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_dimItemDataSetupFields_Get_ByShortName", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetItemDataSetupField(string sBatchSourceName, DateTime? modificationDate, out DataTable dt)");
            }

            if (bRtnval && dt != null)
            {
                EventLog.logEvent("GetItemDataSetupField Row Count = " + dt.Rows.Count.ToString()
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);

                for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
                {
                    string keyWord = dt.Rows[rowCount]["Keyword"].ToString();
                    int itemDataSetupFieldKey = 0;
                    int dataType = 0;
                    Int32.TryParse(dt.Rows[rowCount]["ItemDataSetupFieldKey"].ToString(), out itemDataSetupFieldKey);
                    Int32.TryParse(dt.Rows[rowCount]["DataType"].ToString(), out dataType);
                    // out put the keyword found to the log file
                    EventLog.logEvent("    keyword = (" + keyWord +
                                         ", " + itemDataSetupFieldKey.ToString() +
                                         ", " + dataType.ToString() + ")"
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);
                }
            }

            return bRtnval;
        }

        /// <summary>
        /// Gets the item data by transaction for Report.
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="TransactionID"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetItemDataByTransaction(Guid SessionID, int BankID, int LockboxID, long BatchID, DateTime DepositDate, int TransactionID, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubReport.usp_FactItemData_Get_ByTransaction", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetItemDataByTransaction(Guid SessionID,int BankID, int LockboxID, long BatchID, DateTime DepositDate, int TransactionID, out DataTable dt)");
            }
            return bRtnval;
        }

        /// <summary>
        /// Gets the item data by item for Report.
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="BatchSequence"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetItemDataByItem(Guid SessionID, int BankID, int LockboxID, long BatchID, DateTime DepositDate, int BatchSequence, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubReport.usp_FactItemData_Get_ByItem", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetItemDataByItem(Guid SessionID,int BankID, int LockboxID, long BatchID, DateTime DepositDate, int BatchSequence, out DataTable dt)");
            }

            return bRtnval;
        }


        #endregion

        #region BatchData

        /// <summary>
        /// GetBatchData Setup Fields. This will happen one time at startup
        /// </summary>
        /// <param name="sBatchSourceName">Name of the s batch source.</param>
        /// <param name="modificationDate">The modification date.</param>
        /// <param name="dt">The dt.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetBatchDataSetupField(string sBatchSourceName, DateTime? modificationDate, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmImportShortName", SqlDbType.VarChar, sBatchSourceName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmModificationDate", SqlDbType.DateTime, modificationDate, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_dimBatchDataSetupFields_Get_ByShortName", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "");
            }

            if (bRtnval && dt != null)
            {
                EventLog.logEvent("GetBatchDataSetupFields Row Count = " + dt.Rows.Count.ToString()
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);

                for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
                {
                    string keyWord = dt.Rows[rowCount]["Keyword"].ToString();
                    int batchDataSetupFieldKey = 0;
                    int dataType = 0;
                    Int32.TryParse(dt.Rows[rowCount]["BatchDataSetupFieldKey"].ToString(), out batchDataSetupFieldKey);
                    Int32.TryParse(dt.Rows[rowCount]["DataType"].ToString(), out dataType);
                    // out put the keyword found to the log file
                    EventLog.logEvent("    keyword = (" + keyWord +
                                         ", " + batchDataSetupFieldKey.ToString() +
                                         ", " + dataType.ToString() + ")"
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);
                }
            }

            return bRtnval;
        }


        public bool GetBatchInformation(Guid SessionID, long batchid, DateTime depositdate, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, depositdate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, batchid, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Int, 1, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmStartRecord", SqlDbType.Int, 1, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmRecordsPerPage", SqlDbType.Int, 1, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTotalRecords", SqlDbType.Int, 0, ParameterDirection.Output));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_BatchDetail", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchInformation(Guid SessionID, long batchid, DateTime depositdate, out DataTable dt)");
            }

            return bRtnval;
        }


        /// <summary>
        /// Gets the batch data for Report  
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetBatchData(Guid SessionID, int BankID, int LockboxID, long BatchID, DateTime DepositDate, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubReport.usp_factBatchData_Get", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchData(Guid SessionID, int BankID, int LockboxID, long BatchID, DateTime DepositDate, out DataTable dt)");
            }

            return bRtnval;
        }




        /// <summary>
        /// Does the fact batch data exist.
        /// </summary>
        /// <param name="bankID">The bank ID.</param>
        /// <param name="lockboxID">The lockbox ID.</param>
        /// <param name="processingDateKey">The processing date key.</param>
        /// <param name="batchID">The batch ID.</param>
        /// <param name="itemFound">if set to <c>true</c> [item found].</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool DoesFactBatchDataExist(int bankID, int lockboxID, int processingDateKey, long batchID, out bool itemFound)
        {
            bool bolRetVal = false;
            itemFound = false;
            SqlParameter[] parms;
            SqlParameter parmItemFound;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, lockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmProcessingDateKey", SqlDbType.Int, processingDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, batchID, ParameterDirection.Input));
                parmItemFound = BuildParameter("@parmItemFound", SqlDbType.Int, 0, ParameterDirection.Output);
                arParms.Add(parmItemFound);
                parms = arParms.ToArray();
                if (Database.executeProcedure("RecHubData.usp_factBatchDataExists", parms))
                {
                    if ((bool)parmItemFound.Value)
                    {
                        itemFound = true;
                    }
                    else {
                        itemFound = false;
                    }

                    bolRetVal = true;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DoesFactBatchDataExist(int bankID, int lockboxID, int processingDateKey, long batchID, out bool itemFound)");
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        #endregion


    }
}