﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Calvin Glomb
* Date:     05/21/2013
*
* Purpose:  Manages data transactions for Notifications
*
* Modification History
* WI 102494 CRG 05/21/2013
*	Create Notification DAL
* WI 109747 EAS 07/30/2013
*   -Add Constructor with Connection Type 
* WI 110341 CEJ 07/30/2013
*   -Remove usages of DALLib.SQLEncodeString in all the DALs
* WI 109747 EAS 07/30/2013
*   -DAL components called with RecHubAdmin Connection Type 
* WI 117118 JMC 10/14/2013
*   -Notification attachments was incorrectly calling: 
*    usp_factNotifications_Get_ByNotificationMessageGroup  Should be calling: 
*    usp_factNotificationFiles_Get_ByNotificationMessageGroup.
*******************************************************************************/
namespace WFS.RecHub.DAL
{

    /// <summary>
    /// 
    /// </summary>
    public class cNotificationDAL : _DALBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="cNotificationDAL"/> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        public cNotificationDAL(string vSiteKey)
            : base(vSiteKey)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="cNotificationsDAL"/> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        /// <param name="enmConnection">Connection type to use.</param>
        public cNotificationDAL(string vSiteKey, DAL._DMPObjectRoot.ConnectionType enmConnection)
            : base(vSiteKey, enmConnection) { }

        /// <summary>
        /// Gets the notification file types.
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetNotificationFileTypes(out DataTable dt)
        {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubData.usp_dimNotificationFileTypes_Get";
            try
            {
                arParms = new List<SqlParameter>();
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        /// <summary>
        /// Inserts the type of the notification file.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        /// <param name="fileTypeDesc">The file type desc.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns></returns>
        public bool InsertNotificationFileType(string fileType, string fileTypeDesc, out string errorMessage)
        {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubData.usp_dimNotificationFileTypes_Ins";
            string strErrorMessage = string.Empty;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmFileType", SqlDbType.VarChar, fileType, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmFileTypeDesc", SqlDbType.VarChar, fileTypeDesc, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms);
                if (Database.lastException != null)
                {
                    strErrorMessage = Database.lastException.Message;
                }
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                strErrorMessage = ex.Message;
                bolRetVal = false;
            }

            errorMessage = strErrorMessage;
            return (bolRetVal);
        }

        /// <summary>
        /// Updates the type of the notification file.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        /// <param name="fileTypeDesc">The file type desc.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns></returns>
        public bool UpdateNotificationFileType(string fileType, string fileTypeDesc, out string errorMessage)
        {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubData.usp_dimNotificationFileTypes_Upd";
            string strErrorMessage = string.Empty;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmFileType", SqlDbType.VarChar, fileType, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmFileTypeDesc", SqlDbType.VarChar, fileTypeDesc, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms);
                if (Database.lastException != null)
                {
                    strErrorMessage = Database.lastException.Message;
                }
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                strErrorMessage = ex.Message;
                bolRetVal = false;
            }

            errorMessage = strErrorMessage;
            return (bolRetVal);
        }

        /// <summary>
        /// Returns Notifications using the specified parameters.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="notificationFileTypeKey">The notification file type key.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderDir">The order dir.</param>
        /// <param name="startRecord">The start record.</param>
        /// <param name="maxRows">The max rows.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="dt">The dt.</param>
        /// <param name="filteredTotal">total of filtered entries</param>
        /// <param name="search">filter results by, on all visible cols</param>
        /// <returns></returns>
        public bool GetUserNotifications(Guid SessionID, DateTime startDate, DateTime endDate, int notificationFileTypeKey, 
            string orderBy, string orderDir, int startRecord, int maxRows, int timeZoneBias, string fileName, XmlDocument docReqXml, 
            out int totalRecords, out DataTable dt, out int filteredTotal, string search = "")
        {
            if (search == null || search == "null")
            {
                search = "";
            }
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            SqlParameter parmTotalRecords;
            const string PROCNAME = "RecHubData.usp_factNotifications_Get_ByUserID";
            var tempTotalFiltered = 0;
            try
            {
                arParms = new List<SqlParameter>();
                //arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmStartDate", SqlDbType.DateTime, startDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEndDate", SqlDbType.DateTime, endDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmNotificationFileTypeKey", SqlDbType.Int, notificationFileTypeKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmOrderBy", SqlDbType.VarChar, 20, orderBy));
                arParms.Add(BuildParameter("@parmOrderDir", SqlDbType.VarChar, 4, orderDir));
                arParms.Add(BuildParameter("@parmStartRecord", SqlDbType.Int, startRecord, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmRecordsToReturn", SqlDbType.Int, maxRows, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTimeZoneBias", SqlDbType.Int, timeZoneBias, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmFileName", SqlDbType.VarChar, 255, fileName));
                arParms.Add(BuildParameter("@parmSearchRequest", SqlDbType.Xml, docReqXml.InnerXml, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSearch", SqlDbType.VarChar, search , ParameterDirection.Input)); 
                var filtered =  BuildParameter("@parmFilteredRecords", SqlDbType.Int, tempTotalFiltered, ParameterDirection.Output);
                parmTotalRecords = BuildParameter("@parmTotalRecords", SqlDbType.Int, 0, ParameterDirection.Output);
                
                arParms.Add(filtered);
                arParms.Add(parmTotalRecords);
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
                totalRecords = (int)parmTotalRecords.Value;
                filteredTotal = (int) filtered.Value;
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                totalRecords = 0;
                bolRetVal = false;
                filteredTotal = 0;
            }

            return (bolRetVal);
        }

        /// <summary>
        /// Validates the notification to user.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <param name="notificationMessageGroup">The notification message group.</param>
        /// <param name="hasAccess">if set to <c>true</c> [has access].</param>
        /// <returns></returns>
        public bool ValidateNotificationToUser(Guid SessionID, int notificationMessageGroup, out bool hasAccess)
        {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            SqlParameter parmHasAccess;
            const string PROCNAME = "RecHubData.usp_factNotifications_ValidateUserAccess";
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmNotificationMessageGroup", SqlDbType.Int, notificationMessageGroup, ParameterDirection.Input));
                parmHasAccess = (BuildParameter("@parmHasAccess", SqlDbType.Int, 0, ParameterDirection.Output));
                arParms.Add(parmHasAccess);
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms);
                hasAccess = (int)parmHasAccess.Value > 0;
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                hasAccess = false;
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        /// <summary>
        /// Gets the notification.
        /// </summary>
        /// <param name="notificationMessageGroup">The notification message group.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetNotification(int notificationMessageGroup, int timeZoneBias, Guid SessionID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubData.usp_factNotifications_Get_ByNotificationMessageGroup";
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmNotificationMessageGroup", SqlDbType.Int, notificationMessageGroup, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTimeZoneBias", SqlDbType.Int, timeZoneBias, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                bolRetVal = false;
            }

            return (bolRetVal);
        }


        /// <summary>
        /// Gets the notification files.
        /// </summary>
        /// <param name="notificationMessageGroup">The notification message group.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetNotificationFiles(int notificationMessageGroup, Guid SessionID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubData.usp_factNotificationFiles_Get_ByNotificationMessageGroup";
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmNotificationMessageGroup", SqlDbType.Int, notificationMessageGroup, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                bolRetVal = false;
            }

            return (bolRetVal);
        }
    }
}
