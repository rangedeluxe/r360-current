using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO.AdvancedSearch;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Module:           DAL
*
* Filename:         cSearchDAL.cs
*
* Author:           Joel Caples
*
* Description: 
*
* Revisions:
* 
* CR 29398 5/3/2010 JNE  
*   - Modified Remittance Search to use stored procedure.
* CR 30162 & CR 30163 8/3/2010 JNE 
*   - Modified Remittance search to retrieve total records.
* CR 31831 JMC 11/11/2010
*   - Modified LockboxSearch function to return false only when
*     an error has occurred in the DAL.
* CR 32384 JNE 7/21/2011
*   - PreDefSearch items added.
* CR 30339 WJS 10/26/2011
*	-ServiceBrokerErrorSearch
* CR 87184 CRG 02/06/2013
*	Remove all SQL string files from the ipoDAL
* WI 100251 DRP 06/18/2013
*    - Add PaymentSource and PaymentType to the remmittance and lockbox/account search forms
* WI 149481 TWE 06/23/2014
*    Remove OLOrganizationID from user object
* WI 151989 TWE 07/07/2014
*    Fix IPOnline to work with new Batch ID and RAAM
* WI 155811 TWE 08/06/2014
*    Add permissions node to base document - fix stored procedure call
* WI 158204 CEJ 08/12/2014
*   Add new overload of GetLockboxDataEntryFields to SearchDAL to take Bank ID and WorkgroupID instead of clientAccountKey
******************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// Search Data Access Layer Object
	/// </summary>
	public class cSearchDAL : _DALBase {

		/// <summary>
		/// Initializes a new instance of the <see cref="cSearchDAL" /> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
		public cSearchDAL(string vSiteKey)
			: base(vSiteKey) {
		}

		/// <summary>
		/// Remittances the search.
		/// </summary>
		/// <param name="docReqXml">The doc req XML.</param>
		/// <param name="docSearchResultsTotals">The doc search results totals.</param>
		/// <param name="dtSearchResults">The data table search results.</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool RemittanceSearch(XmlDocument docReqXml, out XmlDocument docSearchResultsTotals, out DataTable dtSearchResults) {
			SqlParameter[] parms;
			SqlParameter[] ReturnParms;
			DataTable dt;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
                // if -1 is passed in convert it to String.Empty
                Replace(docReqXml, "BatchPaymentTypeKey", "-1", String.Empty);
                Replace(docReqXml, "BatchSourceKey", "-1", String.Empty);
				arParms.Add(BuildParameter("@parmSearchRequest", SqlDbType.Xml, docReqXml.OuterXml, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSearchTotals", SqlDbType.Xml, null, ParameterDirection.Output));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_RemittanceSearch", parms, out ReturnParms, out dt);
				if(ReturnParms != null && ReturnParms[1] != null && ReturnParms[1].Value != null) {
					try {
						docSearchResultsTotals = new XmlDocument();
						if(ReturnParms[1] != null && ReturnParms[1].Value != null && ReturnParms[1].Value.ToString().Length > 0) {
							docSearchResultsTotals.LoadXml(ReturnParms[1].Value.ToString());
						}
					} catch(XmlException) {
						docSearchResultsTotals = new XmlDocument();
					} catch {
						throw;
					}

					dtSearchResults = dt;
					bolRetVal = true;
				} else {
					docSearchResultsTotals = ipoXmlLib.GetXMLBase();
					dtSearchResults = null;
				}
			} catch(Exception ex) {
				EventLog.logEvent("An error occurred while Executing usp_RemittanceSearch: " + ex.Message
								, ""
								, MessageType.Error
								, MessageImportance.Essential);
				dtSearchResults = null;
				docSearchResultsTotals = new XmlDocument();
			}

			return bolRetVal;
		}

        /// <summary>
		/// Invoice search.
		/// </summary>
		/// <param name="docReqXml">The doc req XML.</param>
		/// <param name="docSearchResultsTotals">The doc search results totals.</param>
		/// <param name="dtSearchResults">The data table search results.</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool InvoiceSearch(XmlDocument docReqXml, out XmlDocument docSearchResultsTotals, out DataTable dtSearchResults)
        {
            SqlParameter[] parms;
            SqlParameter[] ReturnParms;
            DataTable dt;
            bool bolRetVal = false;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                // if -1 is passed in convert it to String.Empty
                Replace(docReqXml, "BatchPaymentTypeKey", "-1", String.Empty);
                Replace(docReqXml, "BatchSourceKey", "-1", String.Empty);
                arParms.Add(BuildParameter("@parmSearchRequest", SqlDbType.Xml, docReqXml.OuterXml, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSearchTotals", SqlDbType.Xml, null, ParameterDirection.Output));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_InvoiceSearch", parms, out ReturnParms, out dt);
                if (ReturnParms != null && ReturnParms[1] != null && ReturnParms[1].Value != null)
                {
                    try
                    {
                        docSearchResultsTotals = new XmlDocument();
                        if (ReturnParms[1] != null && ReturnParms[1].Value != null && ReturnParms[1].Value.ToString().Length > 0)
                        {
                            docSearchResultsTotals.LoadXml(ReturnParms[1].Value.ToString());
                        }
                    }
                    catch (XmlException)
                    {
                        docSearchResultsTotals = new XmlDocument();
                    }
                    catch
                    {
                        throw;
                    }

                    dtSearchResults = dt;
                    bolRetVal = true;
                }
                else
                {
                    docSearchResultsTotals = ipoXmlLib.GetXMLBase();
                    dtSearchResults = null;
                }
            }
            catch (Exception ex)
            {
                EventLog.logEvent("An error occurred while Executing usp_InvoiceSearch: " + ex.Message
                                , ""
                                , MessageType.Error
                                , MessageImportance.Essential);
                dtSearchResults = null;
                docSearchResultsTotals = new XmlDocument();
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the lockbox data entry fields.
        /// </summary>
        /// <param name="bankID">The Site Bank ID.</param>
        /// <param name="clientAccountID">The Site Client Account ID.</param>
        /// <param name="sessionId">The ID of the current Session under who's athority the request is made.</param>
        /// <param name="dt">The data table returned</param>
        /// <returns>
        /// Results of the database call
        /// </returns>
        public bool GetLockboxDataEntryFields(int bankID, int clientAccountID, Guid sessionId, out DataTable dt) {
            bool ReturnValue = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, clientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionId, ParameterDirection.Input));
                parms = arParms.ToArray();
                ReturnValue = Database.executeProcedure("RecHubData.usp_dimDataEntryColumns_Get_ByBankIDClientAccountID", parms, out dt);
            }
            catch (Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "GetLockboxDataEntryFields(int bankID, int clientAccountID, Guid sessionId, out DataTable dt)");
            }

            return ReturnValue;
        }

		/// <summary>
		/// Lockboxes the search.
		/// </summary>
		/// <param name="docReqXml">The doc req XML.</param>
		/// <param name="docSearchResultsTotals">The doc search results totals.</param>
		/// <param name="dtSearchResults">The dt search results.</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool LockboxSearch(TableParams tables, out XmlDocument docSearchResultsTotals, out DataTable dtSearchResults) {
			SqlParameter[] parms;
			SqlParameter[] ReturnParms;
			DataTable dt;
			dtSearchResults = null;
			docSearchResultsTotals = null;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmAdvancedSearchBaseKeysTable", SqlDbType.Structured, tables.BaseRequest, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmAdvancedSearchWhereClauseTable", SqlDbType.Structured, tables.WhereClause, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmAdvancedSearchSelectFieldsTable", SqlDbType.Structured, tables.SelectFields, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSearchTotals", SqlDbType.Xml, null, ParameterDirection.Output));
				parms = arParms.ToArray();
                Database.executeProcedure("RecHubData.usp_ClientAccount_Search", parms, out ReturnParms, out dt);
				if(ReturnParms != null && ReturnParms[3] != null && ReturnParms[3].Value != null) {
					try {
						docSearchResultsTotals = new XmlDocument();
						if(ReturnParms[3] != null && ReturnParms[3].Value != null && ReturnParms[3].Value.ToString().Length > 0) {
							docSearchResultsTotals.LoadXml(ReturnParms[3].Value.ToString());
							bolRetVal = true;
						} else {
							bolRetVal = (Database.lastException == null);
						}
					} catch(XmlException) {
						docSearchResultsTotals = new XmlDocument();
					} catch {
						throw;
					}

					dtSearchResults = dt;

				} else {
					docSearchResultsTotals = ipoXmlLib.GetXMLBase();
					dtSearchResults = null;
				}
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "LockboxSearch(XmlDocument docReqXml, out XmlDocument docSearchResultsTotals, out DataTable dtSearchResults)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the saved queries.
		/// </summary>
		/// <param name="UserID">The user ID.</param>
		/// <param name="dt">The data table returned</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool GetSavedQueries(int UserID, out DataTable dt) {
			SqlParameter[] parms;
			dt = null;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_PredefSearch_Get_ByUserID", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetSavedQueries(int UserID, out DataTable dt)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the saved query with XML.
		/// </summary>
		/// <param name="UserID">The user ID.</param>
		/// <param name="PredefSearchID">The predef search ID.</param>
		/// <param name="dt">The data table returned</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool GetSavedQueryWithXML(int UserID, Guid PredefSearchID, out DataTable dt) {
			SqlParameter[] parms;
			dt = null;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmPreDefSearchID", SqlDbType.UniqueIdentifier, PredefSearchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_PredefSearch_Get_ByPredefSearchID", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetSavedQueryWithXML(int UserID, Guid PredefSearchID, out DataTable dt)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the saved query.
		/// </summary>
		/// <param name="UserID">The user ID.</param>
		/// <param name="PredefSearchID">The predef search ID.</param>
		/// <param name="dt">The data table returned</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool GetSavedQuery(int UserID, Guid PredefSearchID, out DataTable dt) {
			SqlParameter[] parms;
			dt = null;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmPreDefSearchID", SqlDbType.UniqueIdentifier, PredefSearchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_PredefSearch_Get_ByUserID", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetSavedQuery(int UserID, Guid PredefSearchID, out DataTable dt)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Deletes the saved query.
		/// </summary>
		/// <param name="UserID">The user ID.</param>
		/// <param name="PredefSearchID">The predef search ID.</param>
		/// <param name="RowsReturned">The rows affected.</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool DeleteSavedQuery(int UserID, Guid PredefSearchID, out int RowsAffected) {
			SqlParameter[] parms;
			SqlParameter parmRowsReturned;
			RowsAffected = 0;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmPredefSearchID", SqlDbType.UniqueIdentifier, PredefSearchID, ParameterDirection.Input));
				parmRowsReturned = (BuildParameter("@parmRowsReturned", SqlDbType.Int, 0, ParameterDirection.Output));
				arParms.Add(parmRowsReturned);
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_UserPredefSearch_Del", parms);
				RowsAffected = bolRetVal ? (int) parmRowsReturned.Value : 0;
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "DeleteSavedQuery(int UserID, Guid PredefSearchID, out int RowsReturned)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Updates the saved query.
		/// </summary>
		/// <param name="UserID">The user ID.</param>
		/// <param name="PredefSearchID">The predef search ID.</param>
		/// <param name="IsDefault">The is default.</param>
		/// <param name="RowsReturned">The rows affected.</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool UpdateSavedQuery(int UserID, Guid PredefSearchID, int IsDefault, out int RowsAffected) {
			SqlParameter[] parms;
			SqlParameter parmRowsReturned;
			RowsAffected = 0;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmPredefSearchID", SqlDbType.UniqueIdentifier, PredefSearchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmIsDefault", SqlDbType.Int, IsDefault, ParameterDirection.Input));
				parmRowsReturned = (BuildParameter("@parmRowsReturned", SqlDbType.Int, 0, ParameterDirection.Output));
				arParms.Add(parmRowsReturned);
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_UserPredefSearch_Upd", parms);
				RowsAffected = bolRetVal ? (int) parmRowsReturned.Value : 0;
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "UpdateSavedQuery(int UserID, Guid PredefSearchID, int IsDefault, out int RowsReturned)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Updates the pre def search query.
		/// </summary>
		/// <param name="PredefSearchID">The predef search ID.</param>
		/// <param name="Name">The name.</param>
		/// <param name="Description">The description.</param>
		/// <param name="SearchParmsXML">The search parms XML.</param>
		/// <param name="RowsReturned">The rows affected.</param>
		/// <returns>
		/// Result of the database call
		/// </returns>
		public bool UpdatePreDefSearchQuery(Guid PredefSearchID, string Name, string Description, XmlDocument SearchParmsXML, out int RowsAffected) {
			SqlParameter[] parms;
			SqlParameter parmRowsReturned;
			RowsAffected = 0;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmPredefSearchID", SqlDbType.UniqueIdentifier, PredefSearchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmName", SqlDbType.VarChar, Name, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDescription", SqlDbType.VarChar, Description, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSearchParmsXML", SqlDbType.Xml, SearchParmsXML.OuterXml, ParameterDirection.Input));
				parmRowsReturned = (BuildParameter("@parmRowsReturned", SqlDbType.Int, 0, ParameterDirection.Output));
				arParms.Add(parmRowsReturned);
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_PredefSearch_Upd", parms);
				RowsAffected = bolRetVal ? (int) parmRowsReturned.Value : 0;
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "UpdatePreDefSearchQuery(Guid PredefSearchID, string Name, string Description, XmlDocument SearchParmsXML, out int RowsReturned)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Inserts the pre def saved query.
		/// </summary>
		/// <param name="Name">The name.</param>
		/// <param name="Description">The description.</param>
		/// <param name="objXML">The obj XML.</param>
		/// <param name="PreDefSearchID">The pre def search ID.</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool InsertPreDefSavedQuery(string Name, string Description, XmlDocument objXML, out Guid PreDefSearchID) {
			string strGuid = "";
			SqlParameter[] parms;
			SqlParameter parmPreDefSearchID;
			PreDefSearchID = Guid.Empty;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmName", SqlDbType.VarChar, Name, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDescription", SqlDbType.VarChar, Description, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSearchParmsXML", SqlDbType.Xml, objXML.OuterXml, ParameterDirection.Input));
				parmPreDefSearchID = (BuildParameter("@parmPreDefSearchID", SqlDbType.UniqueIdentifier, Guid.Empty, ParameterDirection.Output));
				arParms.Add(parmPreDefSearchID);
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_PredefSearch_Ins", parms);
				strGuid = bolRetVal ? parmPreDefSearchID.Value.ToString() : "";
				if(strGuid.Length > 0) {
					PreDefSearchID = new Guid(strGuid);
				} else {
					PreDefSearchID = Guid.Empty;
				}
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "InsertPreDefSavedQuery(string Name, string Description, XmlDocument objXML, out Guid PreDefSearchID)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Deletes the user saved query.
		/// </summary>
		/// <param name="UserID">The user ID.</param>
		/// <param name="PredefSearchIDDelete">The predef search ID delete.</param>
		/// <param name="RowsReturned">The rows affected.</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool DeleteUserSavedQuery(int UserID, Guid PredefSearchIDDelete, out int RowsAffected) {
			SqlParameter[] parms;
			SqlParameter parmRowsReturned;
			RowsAffected = 0;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmPredefSearchID", SqlDbType.UniqueIdentifier, PredefSearchIDDelete, ParameterDirection.Input));
				parmRowsReturned = (BuildParameter("@parmRowsReturned", SqlDbType.Int, 0, ParameterDirection.Output));
				arParms.Add(parmRowsReturned);
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_UserPredefSearch_Del", parms);
				RowsAffected = bolRetVal ? (int) parmRowsReturned.Value : 0;
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "DeleteUserSavedQuery(int UserID, Guid PredefSearchIDDelete, out int RowsReturned)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Inserts the user pre def saved query.
		/// </summary>
		/// <param name="iUserID">The i user ID.</param>
		/// <param name="IsDefault">The is default.</param>
		/// <param name="Name">The name.</param>
		/// <param name="Description">The description.</param>
		/// <param name="xmlSearchParmsDoc">The XML search parms doc.</param>
		/// <param name="PreDefSearchID">The pre def search ID.</param>
		/// <param name="iRowsAffected">The i rows affected.</param>
		/// <param name="iError">The i error.</param>
		/// <returns>
		/// Results of the database call
		/// </returns>
		public bool InsertUserPreDefSavedQuery(int iUserID, int IsDefault, string Name, string Description, XmlDocument xmlSearchParmsDoc, out Guid PreDefSearchID, out int iRowsAffected, out int iError) {
			string strGuid = string.Empty;
			SqlParameter[] parms;
			SqlParameter parmPreDefSearchID;
			SqlParameter parmRowsReturned;
			SqlParameter parmError;
			PreDefSearchID = Guid.Empty;
			iRowsAffected = 0;
			iError = 0;
			bool bolRetVal = false;
			try {
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, iUserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmIsDefault", SqlDbType.Int, IsDefault, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmName", SqlDbType.VarChar, Name, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDescription", SqlDbType.VarChar, Description, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSearchParmsXML", SqlDbType.Xml, xmlSearchParmsDoc.OuterXml, ParameterDirection.Input));
				parmPreDefSearchID = (BuildParameter("@parmPreDefSearchID", SqlDbType.UniqueIdentifier, Guid.Empty, ParameterDirection.Output));
				arParms.Add(parmPreDefSearchID);
				parmRowsReturned = (BuildParameter("@parmRowsReturned", SqlDbType.Int, 0, ParameterDirection.Output));
				arParms.Add(parmRowsReturned);
				parmError = (BuildParameter("@parmError", SqlDbType.Int, 1, ParameterDirection.Output));
				arParms.Add(parmError);
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_UserPredefSearchPredefSearch_Ins", parms);

				if(bolRetVal) {
					iError = (parmError.Value.ToString() != "" ? (int) parmError.Value : 1);
					if(iError == 0) {
						iRowsAffected = (parmRowsReturned.Value.ToString() != "") ? (int) parmRowsReturned.Value : 0;
						strGuid = (parmPreDefSearchID.Value.ToString() != "") ? parmPreDefSearchID.Value.ToString() : "";
						PreDefSearchID = (strGuid.Length > 0) ? new Guid(strGuid) : Guid.Empty;
					} else {
						iRowsAffected = 0;
						PreDefSearchID = Guid.Empty;
					}
				} else {
					iRowsAffected = 0;
					PreDefSearchID = Guid.Empty;
					iError = 1;
				}
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "InsertUserPreDefSavedQuery(int iUserID, int IsDefault, string Name, string Description, XmlDocument xmlSearchParmsDoc, out Guid PreDefSearchID, out int iRowsAffected, out int iError)");
			}

			return bolRetVal;
		}
        
        public bool UserWorkgroupEntitlements(Guid sessionID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_SessionClientAccountEntitlements_Get", parms, out tempdt);

                // remove after business layer is updated
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UserWorkgroupEntitlements(Guid sessionID, out DataTable dt)");
            }

            return bolRetVal;
        }

        public bool GetUserWorkgroupEntitlementsByWorkgroup(Guid sessionid, int bankid, int clientaccountid, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionid, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, bankid, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, clientaccountid, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_SessionClientAccountEntitlements_Get_ByWorkgroup", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetUserWorkgroupEntitlementsByWorkgroup(Guid sessionid, int bankid, int clientaccountid, out DataTable dt)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Get the list of payment sources
        /// </summary>
        /// <param name="ActiveOnly">true = get active only</param>
        /// <param name="dtSearchResults">the list</param>
        /// <returns>true = good return</returns>
        public bool GetPaymentSources(Guid sessionId, bool activeonly, out DataTable dtSearchResults)
        {

            bool bolRetVal = false;
            dtSearchResults = null;
            try
            {
                var arParms = new List<SqlParameter>();
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmActiveOnly", SqlDbType.Bit, activeonly, ParameterDirection.Input));
                var parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_ClientAccountBatchSources_Get_BySessionID", parms, out dtSearchResults);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetPaymentSources(bool ActiveOnly, out DataTable dtSearchResults)");
            }

            return bolRetVal;
        }
        /// <summary>
        /// Geet the list of payment types
        /// </summary>
        /// <param name="dtSearchResults">the list</param>
        /// <returns>true = good results</returns>
        public bool GetPaymentTypes(Guid sessionId, out DataTable dtSearchResults)
        {
            bool bolRetVal = false;
            dtSearchResults = null;
            try
            {
                var arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionId, ParameterDirection.Input));
                var parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_ClientAccountBatchPaymentTypes_Get_BySessionID", parms, out dtSearchResults);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetPaymentTypes(out DataTable dtSearchResults)");
            }

            return bolRetVal;
        }

        private void Replace(XmlDocument xDoc, string node, string searchVal, string replaceVal) {
            var xmlNode = xDoc.SelectSingleNode("//" + node);
            if (xmlNode != null)
            {
                string existingVal = xmlNode.InnerText;
                if (existingVal == searchVal)
                {
                    xmlNode.InnerText = replaceVal;
                }
            }
        }

        public bool GetPreDefSearchQueriesForUser(int userid, out DataTable rows)
        {
            bool bolRetVal = false;
            rows = null;
            try
            {
                var arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userid, ParameterDirection.Input));
                var parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_PredefSearch_Get_ByUserID", parms, out rows);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetPreDefSearchQueriesForUser(int userid, out DataTable rows)");
            }

            return bolRetVal;
        }

        public bool GetPreDefSearchQueryForUser(Guid id, int userid, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            try
            {
                var arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userid, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPreDefSearchID", SqlDbType.UniqueIdentifier, id, ParameterDirection.Input));
                var parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_PredefSearch_Get_ByPredefSearchID", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetPreDefSearchQueryForUser(Guid id, int userid, out DataTable dt)");
            }

            return bolRetVal;
        }
    }
}
