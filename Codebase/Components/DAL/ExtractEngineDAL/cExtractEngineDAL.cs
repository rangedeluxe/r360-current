﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 06/21/2013
*   -Initial Version 
* WI 130364 DLD 02/20/2014
*   -Verify database connectivity providing 'friendly' messages to user upon failure.
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 135744 BLR 04/08/2014
*   - Added a constant to represent the Name fields during audit inserts.
* WI 135786 BLR 04/13/2014
*   - Added a function to call the ExtractSequence Audit. 
* WI 134896 BLR 04/14/2014
*   - Added a function to call the Alert Log Insert method. 
* WI 136952 BLR 04/14/2014  
*   - Altered alerts for system-level.
* WI 143040 BLR 05/22/2014
*   - Removed dependence on the composite cBatchKey, only BatchID is utilized. 
* WI 146953 BLR 06/12/2014
*   - ExtractComplete eventlogs are now back to the workgroup-level.  
* WI 269414 JMC 03/10/2016
*   - Modified SQL call to ExtractAudit to include BankId and WorkgroupID.  
******************************************************************************/
namespace WFS.RecHub.DAL.ExtractEngine {

	public class cExtractEngineDAL : _DALBase, IExtractEngineDAL {

		public event OutputStatusMsgEventHandler OutputStatusMsg;
		public event OutputExtractLogMsgEventHandler OutputExtractLogMsg;

		public cExtractEngineDAL(string siteKey, string connectionString) : base(siteKey, connectionString) {
			
		}

		private void OnOutputStatusMsg(string msg) {
			if(OutputStatusMsg != null) {
				OutputStatusMsg(msg);
			}
		}

		private void OnOutputExtractLogMsg(string msg, MessageImportance messageImportance) {
			if(OutputExtractLogMsg != null) {
				OutputExtractLogMsg(msg, messageImportance);
			}
		}

		#region ExtractDescriptors
		public bool GetExtractDescriptors(
			out List<cExtractDescriptor> extractDescriptors) {

			bool bolRetVal = false;
			SqlParameter[] objParms;
			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_dimExtractDescriptor_Get";
			string strErrorMessage = string.Empty;
			List<cExtractDescriptor> arRetVal;
			DataTable dtExtractDescriptors;

			try {
				arParms = new List<SqlParameter>();
				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms, out dtExtractDescriptors);
				if(Database.lastException == null) {
					arRetVal = new List<cExtractDescriptor>();
					foreach(DataRow dr in dtExtractDescriptors.Rows) {
						arRetVal.Add(new cExtractDescriptor((int)dr["ExtractDescriptorKey"], dr["ExtractDescriptor"].ToString()));
					}
					bolRetVal = true;
				} else {
					arRetVal = null;
					strErrorMessage = Database.lastException.Message;
					bolRetVal = false;
				}
			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				arRetVal = null;
				bolRetVal = false;
			}

			//errorMessage = strErrorMessage;
			extractDescriptors = arRetVal;
			return (bolRetVal);

		}
		#endregion ExtractDescriptors

		#region ExtractTraces
		public bool GetPreviousRuns(
			string columnName, 
			out DataTable dt) {

			bool bolRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_factExtractTraces_Get";
			string strErrorMessage = string.Empty;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmTraceColumnName", SqlDbType.VarChar, columnName, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, parms, out dt);
				if(Database.lastException != null) {
					strErrorMessage = Database.lastException.Message;
				}
			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				//strErrorMessage = ex.Message;
				dt = null;
				bolRetVal = false;
			}

			//errorMessage = strErrorMessage;
			return (bolRetVal);
		}

		public bool CreateExtractTraceColumn(string newColumnName) {

			bool bolRetVal = false;
			SqlParameter[] objParms;
			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_dimExtractDescriptor_Ins";
			string strErrorMessage = string.Empty;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmExtractDescriptor", SqlDbType.VarChar, newColumnName, ParameterDirection.Input));
				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms);
				if(Database.lastException != null) {
					strErrorMessage = Database.lastException.Message;
				}
			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				bolRetVal = false;
			}

			//errorMessage = strErrorMessage;
			return (bolRetVal);
		}
		#endregion ExtractTraces

		#region Extract Execution Dynamic SQL
		public bool GetBanks(
			IExtractParms parms,
			IExtractDef extractDef, 
			Dictionary<string, IColumn> bankTableDef,
			out SqlDataReader drBanks) {

			SqlDataReader drRetVal;
			bool bolRetVal;
			string strSQL;

			strSQL = SQLExtractAPI.SQL_GetBanks(
				parms, 
				extractDef,
				bankTableDef);

			Console.WriteLine(string.Empty);
			OnOutputExtractLogMsg(strSQL, MessageImportance.Essential);

			OnOutputStatusMsg("Querying Database for Banks.");
			if(Database.CreateDataReader(strSQL, out drRetVal)) {
				drBanks = drRetVal;
				bolRetVal = true;
			} else {
				drBanks = null;
				bolRetVal = false;
			}

			return (bolRetVal);
		}

		

		public bool GetClientAccounts(
			IExtractParms parms,
			IExtractDef extractDef,
			Dictionary<string, IColumn> bankTableDef,
			Dictionary<string, IColumn> clientAccountTableDef,
			out DbDataReader drClientAccounts) {

			SqlDataReader drRetVal;
			bool bolRetVal;
			string strSQL;

			strSQL = SQLExtractAPI.SQL_GetLockboxes(
				parms,
				extractDef,
				bankTableDef,
				clientAccountTableDef);

			Console.WriteLine(string.Empty);
			OnOutputExtractLogMsg(strSQL, MessageImportance.Essential);

			OnOutputStatusMsg("Querying Database for Client Accounts.");
			if(Database.CreateDataReader(strSQL, out drRetVal)) {
				drClientAccounts = drRetVal;
				bolRetVal = true;
			} else {
				drClientAccounts = null;
				bolRetVal = false;
			}

			return (bolRetVal);
		}

		public bool GetDepositDates (
			IExtractParms parms,
			IExtractDef extractDef,
			string factTableName,
			Dictionary<string, IColumn> clientAccountTableDef,
			Dictionary<string, IColumn> extractTracesTableDef,
			Dictionary<string, IColumn> batchTableDef,
			out DataTable dtDepositDates) {

			DataTable dtRetVal;
			bool bolRetVal;
			string strSQL;

			strSQL = SQLExtractAPI.SQL_GetDepositDates(
				parms,
				extractDef,
				factTableName,
				clientAccountTableDef,
				extractTracesTableDef,
				batchTableDef);

			OnOutputExtractLogMsg(strSQL, MessageImportance.Essential);

			OnOutputStatusMsg("Querying Database for Deposit Dates.");
			if(Database.CreateDataTable(strSQL, out dtRetVal)) {
				dtDepositDates = dtRetVal;
				bolRetVal = true;
			} else {
				dtDepositDates = null;
				bolRetVal = false;
			}

			return (bolRetVal);
		}

		public bool GetBatches(
			IExtractParms parms,
			IExtractDef extractDef,
			int dteCurrent,
			Dictionary<string, IColumn> bankTableDef,
			Dictionary<string, IColumn> clientAccountTableDef,
			Dictionary<string, IColumn> extractTracesTableDef,
			Dictionary<string, IColumn> batchTableDef,            
			out SqlDataReader drBatches) {

			SqlDataReader drRetVal;
			bool bolRetVal;
			string strSQL;

			strSQL = SQLExtractAPI.SQL_GetBatches(
				parms,
				extractDef,
				dteCurrent,
				bankTableDef,
				clientAccountTableDef,
				extractTracesTableDef,
				batchTableDef);

			Console.WriteLine(string.Empty);
			OnOutputExtractLogMsg(strSQL, MessageImportance.Essential);


			OnOutputStatusMsg("Querying Database for Batches.");
			if(Database.CreateDataReader(strSQL, out drRetVal)) {
				drBatches = drRetVal;
				bolRetVal = true;
			} else {
				drBatches = null;
				bolRetVal = false;
			}

			return (bolRetVal);
		}

		public bool GetTransactions(
			IExtractParms parms,
			IExtractDef extractDef,
			int dteCurrent,
			Dictionary<string, IColumn> bankTableDef,
			Dictionary<string, IColumn> clientAccountTableDef,
			Dictionary<string, IColumn> extractTracesTableDef,
			Dictionary<string, IColumn> batchTableDef,
			Dictionary<string, IColumn> transactionsTableDef,
			out SqlDataReader drTransactions) {

			SqlDataReader drRetVal;
			bool bolRetVal;
			string strSQL;

			strSQL = SQLExtractAPI.SQL_GetTransactions(
				parms,
				extractDef,
				dteCurrent,
				bankTableDef,
				clientAccountTableDef,
				extractTracesTableDef,
				batchTableDef, 
				transactionsTableDef);

			Console.WriteLine(string.Empty);
			OnOutputExtractLogMsg(strSQL, MessageImportance.Essential);


			OnOutputStatusMsg("Querying Database for Transactions.");
			if(Database.CreateDataReader(strSQL, out drRetVal)) {
				drTransactions = drRetVal;
				bolRetVal = true;
			} else {
				drTransactions = null;
				bolRetVal = false;
			}

			return (bolRetVal);
		}

		public bool GetPayments(
			IExtractParms parms,
			IExtractDef extractDef,
			int dteCurrent,
			Dictionary<string, IColumn> bankTableDef,
			Dictionary<string, IColumn> clientAccountTableDef,
			Dictionary<string, IColumn> extractTracesTableDef,
			Dictionary<string, IColumn> batchTableDef,
			Dictionary<string, IColumn> transactionsTableDef,
			Dictionary<string, IColumn> paymentsTableDef,
			out SqlDataReader drPayments) {

			string strSQL;

			strSQL = SQLExtractAPI.SQL_GetPayments(
				parms,
				extractDef,
				dteCurrent,
				bankTableDef,
				clientAccountTableDef,
				extractTracesTableDef,
				batchTableDef,
				transactionsTableDef,
				paymentsTableDef);

			Console.WriteLine(string.Empty);
			OnOutputExtractLogMsg(strSQL, MessageImportance.Essential);

			OnOutputStatusMsg("Querying Database for Payments.");

			return(Database.CreateDataReader(strSQL, out drPayments));
		}

		public bool GetStubs(
			IExtractParms parms,
			IExtractDef extractDef,
			int dteCurrent,
			Dictionary<string, IColumn> bankTableDef,
			Dictionary<string, IColumn> clientAccountTableDef,
			Dictionary<string, IColumn> extractTracesTableDef,
			Dictionary<string, IColumn> batchTableDef, 
			Dictionary<string, IColumn> transactionsTableDef, 
			Dictionary<string, IColumn> stubsTableDef,
			out SqlDataReader drStubs) {

			SqlDataReader drRetVal;
			bool bolRetVal;
			string strSQL;

			strSQL = SQLExtractAPI.SQL_GetStubs(
				parms, 
				extractDef,
				dteCurrent,
				bankTableDef,
				clientAccountTableDef,
				extractTracesTableDef,
				batchTableDef, 
				transactionsTableDef, 
				stubsTableDef);

			Console.WriteLine(string.Empty);
			OnOutputExtractLogMsg(strSQL, MessageImportance.Essential);

			OnOutputStatusMsg("Querying Database for Stubs.");

			if(Database.CreateDataReader(strSQL, out drRetVal)) {
				drStubs = drRetVal;
				bolRetVal = true;
			} else {
				drStubs = null;
				bolRetVal = false;
			}

			return (bolRetVal);
		}


		public bool GetDocuments(
			IExtractParms parms,
			IExtractDef extractDef,
			int dteCurrent,
			Dictionary<string, IColumn> bankTableDef,
			Dictionary<string, IColumn> clientAccountTableDef,
			Dictionary<string, IColumn> extractTracesTableDef,
			Dictionary<string, IColumn> batchTableDef, 
			Dictionary<string, IColumn> transactionsTableDef,
			Dictionary<string, IColumn> documentsTableDef,
			out SqlDataReader drDocuments) {

			SqlDataReader drRetVal;
			bool bolRetVal;
			string strSQL;

			strSQL = SQLExtractAPI.SQL_GetDocuments(
				parms,
				extractDef,
				dteCurrent,
				bankTableDef,
				clientAccountTableDef,
				extractTracesTableDef,
				batchTableDef, 
				transactionsTableDef, 
				documentsTableDef);

			Console.WriteLine(string.Empty);
			OnOutputExtractLogMsg(strSQL, MessageImportance.Essential);
			Console.WriteLine(string.Empty);

			OnOutputStatusMsg("Querying Database for Documents.");


			if(Database.CreateDataReader(strSQL, out drRetVal)) {
				drDocuments = drRetVal;
				bolRetVal = true;
			} else {
				drDocuments = null;
				bolRetVal = false;
			}

			return (bolRetVal);
		}
		#endregion Extract Execution Dynamic SQL

        /// <summary>
        /// WI 146953 : Need a workgroup-level event log as well, for ExtractComplete.
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool WriteEventLog(
            string eventName,
            string message,
            int sitecodeid,
            int sitebankid,
            int siteworkgroupid
            )
        {
            SqlParameter[] objParms;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubAlert.usp_EventLog_Ins";

            var bolRetVal = true;

            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, eventName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmMessage", SqlDbType.VarChar, message, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, sitecodeid, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, sitebankid, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteworkgroupid, ParameterDirection.Input));
                objParms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, objParms);
            }
            catch (Exception ex)
            {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
                bolRetVal = false;
            }

            return (bolRetVal);

        }

		// WI 134896 : Adding a function to call an eventlog insert procedure. 
        // WI 139595 : Altered function for a System-Wide call.
		public bool WriteSystemLevelEventLog (
            string eventName,
            string message
			)
		{
			SqlParameter[] objParms;
			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubAlert.usp_EventLog_SystemLevel_Ins";

			var bolRetVal = true;

			try
			{
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, eventName, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmMessage", SqlDbType.VarChar, message, ParameterDirection.Input));
				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms);
			}
			catch (Exception ex)
			{
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				bolRetVal = false;
			}

			return (bolRetVal);

		}

        #region ExtractAudit
        public bool InsertExtractAudit(
            Guid extractAuditID,
            int bankID,
            int workgroupID,
            bool isComplete,
            int checkCount,
            int stubCount,
            decimal paymentTotal,
            decimal stubTotal,
            int recordCount,
            string fileName) {

            bool bolRetVal = false;
            SqlParameter[] objParms;

            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubAudit.usp_factExtractAudits_Ins";
            string strErrorMessage = string.Empty;
            try {
                arParms = new List<SqlParameter>();

                arParms.Add(BuildParameter("@parmExtractAuditID", SqlDbType.UniqueIdentifier, extractAuditID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmIsComplete", SqlDbType.Bit, isComplete, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractCount1", SqlDbType.Int, checkCount, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractCount2", SqlDbType.Int, stubCount, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractAmount1", SqlDbType.Decimal, paymentTotal, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractAmount2", SqlDbType.Decimal, stubTotal, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractRecordCount", SqlDbType.Int, recordCount.ToString(), ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractFileName", SqlDbType.VarChar, fileName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, workgroupID, ParameterDirection.Input));

                objParms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, objParms);
                if (Database.lastException != null) {
                    strErrorMessage = Database.lastException.Message;
                }
            } catch (Exception ex) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
                bolRetVal = false;
            }

            //errorMessage = strErrorMessage;
            return (bolRetVal);

        }

        #endregion ExtractAudit

        #region ExtractSequence
        /// <summary>
        /// WI 135786 : Implement the stored proc for when a batch (or transaction/payment/stub/document) 
        /// is pulled from the database.
        /// </summary>
        /// <param name="parms"></param>
        /// <param name="uniqueBatchKeys"></param>
        /// <returns></returns>
        public bool WriteExtractSequenceNumber(
			IExtractParms parms,
			cBatchKey[] uniqueBatchKeys) 
		{
			SqlParameter[] objParms;
			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_factBatchExtracts_UpSert";

			var bolRetVal = false;
			var strErrorMessage = string.Empty;
			
			foreach (var key in uniqueBatchKeys)
			{
				try
				{
					arParms = new List<SqlParameter>();
                    
					arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, key.BatchID, ParameterDirection.Input));
					arParms.Add(BuildParameter("@parmExtractSequenceNumber", SqlDbType.BigInt, parms.ExtractSequenceNumber, ParameterDirection.Input));

					objParms = arParms.ToArray();
                    
					bolRetVal = Database.executeProcedure(PROCNAME, objParms);
					if (Database.lastException != null)
					{
						strErrorMessage = Database.lastException.Message;
					}
				}
				catch (Exception ex)
				{
					// WI 131640 : Additional exception Logging.
					cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
					cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
					bolRetVal = false;
				}
			}

			return (bolRetVal);

		}
		#endregion ExtractSequence

		#region ExtractTrace
		public bool WriteTraceField(
			IExtractParms parms,
			cStandardFields standardFields,
			long batchID) {

			bool bolRetVal = false;
			SqlParameter[] objParms;
			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_factExtractTraces_UpSert";
			string strErrorMessage = string.Empty;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmExtractDateTime", SqlDbType.DateTime, standardFields.ExtractStarted.ToString("MM/dd/yyyy HH:mm:ss"), ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, batchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmExtractDescriptor", SqlDbType.VarChar, parms.TraceField, ParameterDirection.Input));
				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms);
				if(Database.lastException != null) {
					strErrorMessage = Database.lastException.Message;
				}
			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				bolRetVal = false;
			}

			//errorMessage = strErrorMessage;
			return (bolRetVal);

		}
		#endregion ExtractTrace

		#region EventAudit
		public bool WriteAuditFileData(
			IExtractParms parms,
			IExtractDef extractDef,
			cStandardFields standardFields,
			int workstationID) {

			bool bolRetVal = false;
			SqlParameter[] objParms;
			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubCommon.usp_WFS_EventAudit_Ins";
			string strErrorMessage = string.Empty;
			StringBuilder sbDescription;

			try {
				arParms = new List<SqlParameter>();

				arParms.Add(BuildParameter("@parmApplicationName", SqlDbType.VarChar, Support.AUDIT_NAME_FIELD, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, Support.AUDIT_NAME_FIELD, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, parms.UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmWorkstation", SqlDbType.Int, workstationID, ParameterDirection.Input));
				//arParms.Add(BuildParameter("@TimeStamp", SqlDbType.DateTime, standardFields.ExtractStarted.AddMilliseconds(-standardFields.ExtractStarted.Millisecond), ParameterDirection.Input));

				sbDescription = new StringBuilder();
				sbDescription.Append("Ran Extract " + parms.SetupFile + ".");

				if(extractDef.ExtractPath.ToLower() != parms.TargetFile.ToLower()) {
					sbDescription.Append(@"  Extract Path changed from " + extractDef.ExtractPath + " to " + parms.TargetFile + ".  ");
				}

				sbDescription.Append("  Filters applied: ");

				sbDescription.Append(@"Bank ");
				if(parms.BankID.Length > 0) {
					for(int i = 0;i < parms.BankID.Length;++i) {
						sbDescription.Append(parms.BankID[i].ToString());
						if(i < parms.BankID.Length - 1) {
							sbDescription.Append(", ");
						}
					}
				}
				sbDescription.Append(" , ");

				sbDescription.Append(@"Lockbox ");
				if(parms.LockboxID.Length > 0) {
					for(int i = 0;i < parms.LockboxID.Length;++i) {
						sbDescription.Append(parms.LockboxID[i].ToString());
						if(i < parms.LockboxID.Length - 1) {
							sbDescription.Append(", ");
						}
					}
				}
				sbDescription.Append(" , ");

				sbDescription.Append(@"Batch ");
				if(parms.BatchID.Length > 0) {
					for(int i = 0;i < parms.BatchID.Length;++i) {
						sbDescription.Append(parms.BatchID[i].ToString());
						if(i < parms.BatchID.Length - 1) {
							sbDescription.Append(", ");
						}
					}
				}
				sbDescription.Append(" , ");

				sbDescription.Append(@"ProcDate ");
				sbDescription.Append(parms.ProcessingDateFrom.ToString("MM/dd/yyyy"));
				if(parms.ProcessingDateFrom != parms.ProcessingDateTo) {
					sbDescription.Append(" - " + parms.ProcessingDateTo.ToString("MM/dd/yyyy"));
				}
				sbDescription.Append(" , ");

				sbDescription.Append(@"DepDateTime ");
				sbDescription.Append(parms.DepositDateFrom.ToString("MM/dd/yyyy"));
				if(parms.DepositDateFrom != parms.DepositDateTo) {
					sbDescription.Append(" - " + parms.DepositDateTo.ToString("MM/dd/yyyy"));
				}

				if(parms.TraceField.Length == 0) {
					sbDescription.Append("  No Batch Trace field is defined for this extract.");
				} else {
					sbDescription.Append("  Batch Trace field is " + parms.TraceField);
					if(parms.RerunList.Length > 0) {
						sbDescription.Append("Rerun of extracts:  ");
						for(int i = 0;i < parms.RerunList.Length;++i) {
                            if (parms.RerunList[i] != null)
							    sbDescription.Append(parms.RerunList[i].ToString("MM/dd/yyyy hh:mm:ss tt"));
							if(i < parms.RerunList.Length - 1) {
								sbDescription.Append(" ,  ");
							}
						}
					} else {
						sbDescription.Append(" First run of extract.");
					}
				}

				// We can exceed the length of the AuditFile.Description field if we are rerunning lots and 
				// lots of previous runs.
				if(sbDescription.Length > 8000) {
					arParms.Add(BuildParameter("@parmAuditMessage", SqlDbType.VarChar, sbDescription.ToString().Substring(0, 2048), ParameterDirection.Input));
				} else {
					arParms.Add(BuildParameter("@parmAuditMessage", SqlDbType.VarChar, sbDescription.ToString(), ParameterDirection.Input));
				}

				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms);
				if(Database.lastException != null) {
					strErrorMessage = Database.lastException.Message;
				}

			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				bolRetVal = false;
			}

			return (bolRetVal);
		}

		public bool WriteAuditFileData(
			string auditEvent,
			int workstationID,
			int userID,
			string description) {

			StringBuilder sbDescription = new StringBuilder();

			bool bolRetVal = false;
			SqlParameter[] objParms;
			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubCommon.usp_WFS_EventAudit_Ins";
			string strErrorMessage = string.Empty;

			try {

				arParms = new List<SqlParameter>();

				arParms.Add(BuildParameter("@parmApplicationName", SqlDbType.VarChar, Support.AUDIT_NAME_FIELD, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, auditEvent, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmWorkstation", SqlDbType.Int, workstationID.ToString(), ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmAuditMessage", SqlDbType.VarChar, SQLPublic.FormatValue(description), ParameterDirection.Input));

				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms);

			} catch(Exception e) {
				OnOutputError(e);
				bolRetVal = false;
			}
			return (bolRetVal);
		}
		#endregion EventAudit

		#region Describe Tables
		public bool GetTableDef(
			string tableName,
			out DataTable dtTableDef) {

			bool bolRetVal = false;
			SqlParameter[] objParms;

			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubCommon.usp_Get_TableDefinition";
			string strErrorMessage = string.Empty;
			try {
				arParms = new List<SqlParameter>();

				int iPos = tableName.IndexOf('.');
				string strSchema;
				string strTableName;
				if(iPos > -1) {
					strSchema = tableName.Substring(0, iPos);
					strTableName = tableName.Substring(iPos + 1);
				} else {
					strSchema = "RecHubData";
					strTableName = tableName;
				}

				arParms.Add(BuildParameter("@parmSchemaName", SqlDbType.VarChar, strSchema, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTableName", SqlDbType.VarChar, strTableName, ParameterDirection.Input));
				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms, out dtTableDef);
				if(Database.lastException != null) {
					strErrorMessage = Database.lastException.Message;
				}
			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				dtTableDef = null;
				bolRetVal = false;
			}

			//errorMessage = strErrorMessage;
			return (bolRetVal);
		}

		public bool GetTableColumns(
			DataEntryType dataEntryType,
			out DataTable dt) {

			bool bolRetVal = false;
			SqlParameter[] objParms;

			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_dimDataEntryColumns_Get_DataEntryDefinitions";
			string strErrorMessage = string.Empty;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmIsPayment", SqlDbType.Bit, (dataEntryType == DataEntryType.PaymentsDataEntry ? true : false), ParameterDirection.Input));
				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms, out dt);
				if(Database.lastException != null) {
					strErrorMessage = Database.lastException.Message;
				}
			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				dt = null;
				bolRetVal = false;
			}

			//errorMessage = strErrorMessage;
			return (bolRetVal);
		}

		#endregion Describe Tables

		#region Get Check/Document Info
		public bool GetCheckInfo(
			cItemKey checkKey, 
			out DataTable dtCheckInfo) {

			bool bolRetVal = false;
			SqlParameter[] objParms;

			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_factChecks_Get_ByImmutableDate";
			string strErrorMessage = string.Empty;

			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, checkKey.BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, checkKey.BatchSequence, ParameterDirection.Input));
				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms, out dtCheckInfo);
				if(Database.lastException != null) {
					strErrorMessage = Database.lastException.Message;
				}
			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				dtCheckInfo = null;
				bolRetVal = false;
			}

			//errorMessage = strErrorMessage;
			return (bolRetVal);
		}

		public bool GetDocumentInfo(
			cItemKey documentKey, 
			out DataTable dtDocumentInfo) {

			bool bolRetVal = false;
			SqlParameter[] objParms;

			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_factDocuments_Get_ByImmutableDate";
			string strErrorMessage = string.Empty;

			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, documentKey.BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, documentKey.BatchSequence, ParameterDirection.Input));
				objParms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, objParms, out dtDocumentInfo);
				if(Database.lastException != null) {
					strErrorMessage = Database.lastException.Message;
				}
			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				dtDocumentInfo = null;
				bolRetVal = false;
			}

			//errorMessage = strErrorMessage;
			return (bolRetVal);
		}
		#endregion Get Check/Document Info

		#region SystemSetup
		public string GetSystemSetupSetting(
			string section,
			string setupKey) {

			string sRet = string.Empty;

			SqlParameter[] objParms;

			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubConfig.usp_SystemSetup_Get_BySetupKey";
			string strErrorMessage = string.Empty;
			DataTable dtSystemSetup;

			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSection", SqlDbType.VarChar, section, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSetupKey", SqlDbType.VarChar, setupKey, ParameterDirection.Input));
				objParms = arParms.ToArray();
				if(Database.executeProcedure(PROCNAME, objParms, out dtSystemSetup)) {
					if(dtSystemSetup.Rows.Count > 0) {
						sRet = dtSystemSetup.Rows[0]["Value"].ToString();
					} else {
						sRet = "0";
					}
				} else {
					if(Database.lastException == null) {
						strErrorMessage = Database.lastException.Message;
					}
				}
			} catch(Exception ex) {
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				cCommonLogLib.LogMessageLTA("Unable to execute procedure: " + PROCNAME, LTAMessageImportance.Essential);
				sRet = string.Empty;
			}

			//errorMessage = strErrorMessage;
			return (sRet);

		}
		#endregion SystemSetup
		
		/// <summary>
		/// WI 130364 - Verify database connectivity providing 'friendly' messages to user upon failure.
		/// </summary>
		public bool TestConnection()
		{
			try { Database.beginTrans(); }
			catch (Exception ex){
				// WI 131640 : Additional exception Logging.
				cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				return false; 
			}
			finally { Database.rollbackTrans(); }
			return true;
		}

		// WI 114570 removed for v2.0
		//public bool GetCurrentProcessingDate(out DataTable dt) {

		//    DataTable dtRetVal;
		//    bool bolRetVal;

		//    if(Database.CreateDataTable(SQLExtractAPI.SQL_GetCurrentProcessingDate(), out dtRetVal)) {
		//        dt = dtRetVal;
		//        bolRetVal = true;
		//    } else {
		//        dt = null;
		//        //OnOutputError("Unable to retrieve CurrentProcessingDate: " + SQLExtractAPI.SQL_GetCurrentProcessingDate(), "cExtract.GetStandardFields");
		//        bolRetVal = false;
		//    }

		//    return (bolRetVal);
		//}


        
	}
}
