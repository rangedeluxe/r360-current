﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   -Initial Version 
* WI 130807 DLD 02/28/2014
*   -Encrypt/Decrypt UserID in database connection string 
* WI 131640 BLR 03/04/2014    
*   - Added in additional exception logging.
* WI 130364 DLD 03/06/2014
*   - Moved Encrypted UserId check to CommonLib to be referenced by Settings.Validation() 
******************************************************************************/
namespace WFS.RecHub.DAL.ExtractEngine {

    public abstract class _DALBase : _DMPObjectRoot {

        private cError _LastError = null;

        public _DALBase(string siteKey, string connectionString) : base(siteKey, connectionString) {
        ////public _DALBase(string vSiteKey, UserTypes vUserType) : base(vSiteKey) {
        ////    _UserType = vUserType;
        }

        public void BeginTrans() {
            Database.beginTrans();
        }
        
        public void CommitTrans() {
            Database.commitTrans();
        }
        
        public void RollbackTrans() {
            Database.rollbackTrans();
        }
        
        public bool SetDeadlockPriority {
            get {
                return(Database.SetDeadlockPriority);
            }
            set {
                Database.SetDeadlockPriority = value;
            }
        }

        protected bool ExecuteSQL(string sql, out DataTable dt) {

            DataTable dtRetVal = null;
            bool bolRetVal;

            try {
                bolRetVal = Database.CreateDataTable(sql, out dtRetVal);

                if(bolRetVal) {
                    dt = dtRetVal;
                    ////FilterDataTable(dt);
                } else {
                    if(dtRetVal != null) {
                        dtRetVal.Dispose();
                    }
                    dt = null;
                }
            } catch(Exception ex) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                if(dtRetVal != null) {
                    dtRetVal.Dispose();
                }
                dt = null;
                _LastError = new cError(-1, ex.Message);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        protected bool ExecuteSQL(string sql) {

            bool bolRetVal;

            try {
                bolRetVal = (Database.executeNonQuery(sql) >= 0);

            } catch(Exception ex) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                _LastError = new cError(-1, ex.Message);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        protected bool ExecuteNonScalar(string sql)
        {
            bool bolRetVal;

            try
            {
                bolRetVal = (Database.executeNonScalarQuery(sql) == -1);
            }
            catch (Exception ex)
            {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                _LastError = new cError(-1, ex.Message);
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        protected bool ExecuteSQL(string sql, out int rowsAffected) {

            bool bolRetVal;

            try {
                rowsAffected = Database.executeNonQuery(sql);
                bolRetVal = (rowsAffected >= 0);

            } catch(Exception ex) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                _LastError = new cError(-1, ex.Message);
                rowsAffected = -1;
                bolRetVal = false;
            }

            return(bolRetVal);
        }
        
        protected static SqlParameter BuildParameter(
            string parameterName, 
            SqlDbType sqlType, 
            object value, 
            ParameterDirection direction) {

            SqlParameter objParameter = new SqlParameter();
            int intSize = 0;
            int i;
            
            objParameter.ParameterName = parameterName;
            objParameter.SqlDbType = sqlType;
            objParameter.Value = value;
            objParameter.Direction = direction;
            
            if(sqlType == SqlDbType.Xml) {
                if(value == null) {
                    objParameter.Size = 4096;
                } else {
                    i = 10;
                    intSize = 2^i;
                    while(intSize < value.ToString().Length) {
                        ++i;
                        intSize = 2^i;                    
                    }
                    objParameter.Size = intSize;
                }
            }
           

            return(objParameter);
        }

        protected static SqlParameter BuildParameter(
            string parameterName, 
            SqlDbType sqlType, 
            int size, 
            string value) {

            SqlParameter objParameter = new SqlParameter();
            
            objParameter.ParameterName = parameterName;
            objParameter.SqlDbType = sqlType;
            objParameter.Size = size;
            objParameter.Value = value;

            return(objParameter);
        }

        protected static SqlParameter BuildReturnValueParameter(
            string parameterName, 
            SqlDbType sqlType) {

            SqlParameter objParameter = new SqlParameter();
            
            objParameter.ParameterName = parameterName;
            objParameter.SqlDbType = sqlType;
            objParameter.Direction = ParameterDirection.ReturnValue;

            return(objParameter);
        }

        /// <summary>
        /// 
        /// </summary>
        public cError GetLastError {
            get {
                if(_LastError == null) { 
                    _LastError = new cError(0, string.Empty);
                }
                return(_LastError);
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class cError {

        private int _Number = 0;
        private string _Description = string.Empty;

        public cError(int Number, string vDescription) {
            _Number = -1;
            _Description = vDescription;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Number {
            get {
                return(_Number);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Description {
            get {
                return(_Description);
            }
        }
    }
}
