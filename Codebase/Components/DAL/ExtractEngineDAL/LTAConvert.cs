﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Database;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DAL.ExtractEngine {

    internal static class LTAConvert {

        public static LTAMessageType ConvertMessageTypeToIpdLog(DbMessageType messageType) {
            switch(messageType) {
                case DbMessageType.Error:
                    return (LTAMessageType.Error);
                case DbMessageType.Information:
                    return (LTAMessageType.Information);
                case DbMessageType.Warning:
                    return (LTAMessageType.Warning);
                default:
                    return (LTAMessageType.Information);
            }
        }

        public static LTAMessageImportance ConvertMessageImportanceToIpdLog(DbMessageImportance messageImportance) {
            switch(messageImportance) {
                case DbMessageImportance.Debug:
                    return (LTAMessageImportance.Debug);
                case DbMessageImportance.Verbose:
                    return (LTAMessageImportance.Verbose);
                case DbMessageImportance.Essential:
                    return (LTAMessageImportance.Essential);
                default:
                    return (LTAMessageImportance.Essential);
            }
        }

        public static LTAMessageType ConvertMessageTypeToIpdLog(MessageType messageType) {
            switch(messageType) {
                case MessageType.Error:
                    return (LTAMessageType.Error);
                case MessageType.Information:
                    return (LTAMessageType.Information);
                case MessageType.Warning:
                    return (LTAMessageType.Warning);
                default:
                    return (LTAMessageType.Information);
            }
        }

        public static LTAMessageImportance ConvertMessageImportanceToIpdLog(MessageImportance messageImportance) {
            switch(messageImportance) {
                case MessageImportance.Debug:
                    return (LTAMessageImportance.Debug);
                case MessageImportance.Verbose:
                    return (LTAMessageImportance.Verbose);
                case MessageImportance.Essential:
                    return (LTAMessageImportance.Essential);
                default:
                    return (LTAMessageImportance.Essential);
            }
        }

        public static MessageType ConvertMessageType(DbMessageType messageType) {
            switch(messageType) {
                case DbMessageType.Error:
                    return (MessageType.Error);
                case DbMessageType.Information:
                    return (MessageType.Information);
                case DbMessageType.Warning:
                    return (MessageType.Warning);
                default:
                    return (MessageType.Information);
            }
        }

        public static MessageImportance ConvertMessageImportance(DbMessageImportance messageImportance) {
            switch(messageImportance) {
                case DbMessageImportance.Debug:
                    return (MessageImportance.Debug);
                case DbMessageImportance.Verbose:
                    return (MessageImportance.Verbose);
                case DbMessageImportance.Essential:
                    return (MessageImportance.Essential);
                default:
                    return (MessageImportance.Essential);
            }
        }
    }
}
