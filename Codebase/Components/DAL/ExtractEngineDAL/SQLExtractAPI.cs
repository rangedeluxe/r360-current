using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 114570 MLH 09/20/2013
*   - Removed CurrentProcessingDate from Standard Fields (for v2.0)
* WI 115319 MLH 09/27/2013
*   - Problem loading Client Accounts (wrong table name in WHERE clause)
* WI 129395�- DLD 20140218
*   - Fixed SQL_GetPayments and SQL_GetStubs. They were ignoring the 'depositDateKey' parameter 
*    causing them to return data for the entire date range selected on the UI 
*    instead of the single date required by referring methods.
*    Added 'WHERE ImmutableDateKey=depositDateKey' condition to each SQL statement
* WI 129986 BLR 02/24/2014
*   - Edited SQL to allow parent hierarchy fields.
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 132009 BLR 03/06/2014   
*   - Added in func for joined tables on batch summaries. 
* WI 132441 BLR 03/11/2014
*   - Added in func for Limited hierarchy fields.
* WI 115249 BLR 03/24/2014
*   - Added a sanitation of ' characters.   
* WI 135749 BLR 04/10/2014
*   - Revamped the way order-by statements are generated.
*   - Additionally, just cleaned up this large file a bit.
* WI 134896 BLR 04/14/2014
*   - Added SiteCodeID as a default select.
* WI 143039 BLR 05/22/2014
*   - Altered all INNER JOIN clauses dealing with the Composite Batch Key.
* WI 143506 BLR 05/22/2014
*   - Deposit Date keys now accept ProcessingDate as well as DepositDate.
*   - Removed where clauses on ImmutableDateKey.
* WI 133224 BLR 06/06/2014
*   - Added an ON clause for factBatchSummary.IsDeleted = 0 on each of the
*   - fact tables.  This can cause "duplicate" data to be returned and bugs
*   - on the PopulateTable methods.   
* WI 146886 BLR 06/10/2014
*   - Added a function for Limiting based on the parameters (parms). This was
*     mistakenly stripped out when we removed the batchfilterCTE. 
* WI 146610 BLR 06/16/2014
*   - Adding in a where clause for -1's.  These are just placeholder rows.     
* WI 115658 BLR 06/24/2014
*   - Added in BuildDataEntryInnerSelectClauses for Data Entry fields, altered
*     the get functions to call this when appropriate.   
* WI 153574 BLR 08/19/2014
*   - Joined Transaction Exception Status. 
* WI 162549 BLR 09/02/2014
*   - Added Immutable date back in, as we need this for requesting images from OLF. 
* WI 162851 BLR 09/03/2014
*   - Fixed up the limit SQL.
******************************************************************************/
namespace WFS.RecHub.DAL.ExtractEngine
{

    public static class SQLExtractAPI
    {

        public static string SQL_GetBanks(
            IExtractParms parms,
            IExtractDef extractDef,
            Dictionary<string, IColumn> bankTableDef)
        {

            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            string strSelectClause;
            string strLimitClause;
            string strOrderByClause;
            List<string> dicFieldNames;
            string strInClause;
            List<cQualifiedOrderByColumn> arOrderByClause = new List<cQualifiedOrderByColumn>();
            List<string> arTableNames = new List<string>();
            List<Dictionary<string, IColumn>> arTableDefs = new List<Dictionary<string, IColumn>>();

            //---------------------------------------------------------------------------
            // Add Table names to the TableDefs and TableNames collections so that 
            // the SELECT clause can be built to include data from higher-level entities.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBANKS);
            arTableDefs.Add(bankTableDef);
            //---------------------------------------------------------------------------

            sbSQL.Append(" SELECT ");
            dicFieldNames = new List<string>();
            dicFieldNames.Add("RecHubData.dimBanks.SiteBankID");
            if (FormatSelectClause(dicFieldNames,
                                  extractDef.AllLayouts,
                                  extractDef.JointDetailLayouts,
                                  extractDef.BankOrderByColumns,
                                  arTableNames.ToArray(),
                                  arTableDefs.ToArray(),
                                  out strSelectClause))
            {

                sbSQL.Append(strSelectClause);
            }

            sbSQL.Append(" FROM RecHubData.dimBanks");

            if (FormatValueList("RecHubData.dimBanks.SiteBankID", parms.BankID, 1, out strInClause))
            {
                sbWhere.Append(" WHERE RecHubData.dimBanks.MostRecent=1 AND " + strInClause);
            }
            else
            {
                sbWhere.Append(" WHERE RecHubData.dimBanks.MostRecent=1");
            }

            if (FormatLimitClause(extractDef.BankLimitItems,
                                 DatabaseConstants.LIMIT_TABLES_BANKS,
                                 bankTableDef,
                                 out strLimitClause))
            {

                sbWhere.Append(" AND " + strLimitClause);
            }

            BuildParametersLimitClauses(parms, sbWhere, DatabaseConstants.TABLE_NAME_DIMBANKS);
            sbSQL.Append(sbWhere.ToString());

            // ORDER BY Clause
            BuildOrderByArray(LayoutLevelEnum.Bank,
                              QualifyOrderByColumns(arTableNames.ToArray(), arTableDefs.ToArray(), extractDef.BankOrderByColumns),
                              new cQualifiedOrderByColumn[] { new cQualifiedOrderByColumn(DatabaseConstants.TABLE_NAME_DIMBANKS, "SiteBankID") },
                              ref arOrderByClause);

            if (FormatOrderByClause(arOrderByClause, out strOrderByClause))
            {
                sbSQL.Append(" ORDER BY " + strOrderByClause);
            }

            return (SQLPublic.UnformatSQL(sbSQL.ToString()));
        }

        public static string SQL_GetLockboxes(
            IExtractParms parms,
            IExtractDef extractDef,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef)
        {

            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            string strSelectClause;
            string strWhereClause;
            string strLimitClause;
            string strOrderByClause;
            List<string> dicFieldNames;
            List<cQualifiedOrderByColumn> arOrderByClause = new List<cQualifiedOrderByColumn>();
            List<string> arTableNames = new List<string>();
            List<Dictionary<string, IColumn>> arTableDefs = new List<Dictionary<string, IColumn>>();

            //---------------------------------------------------------------------------
            // Add Table names to the TableDefs and TableNames collections so that 
            // the SELECT clause can be built to include data from higher-level entities.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS);
            arTableDefs.Add(clientAccountTableDef);

            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBANKS);
            arTableDefs.Add(bankTableDef);
            //---------------------------------------------------------------------------

            // SELECT Clause
            sbSQL.Append(" SELECT ");
            dicFieldNames = new List<string>();
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteBankID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteClientAccountID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.LongName");
            // WI 134896 : Added SiteCodeID as a default select.  We need this for the event logs.
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteCodeID");
            if (FormatSelectClause(
                dicFieldNames,
                extractDef.AllLayouts,
                extractDef.JointDetailLayouts,
                extractDef.LockboxOrderByColumns,
                arTableNames.ToArray(),
                arTableDefs.ToArray(),
                out strSelectClause))
            {

                sbSQL.Append(strSelectClause);
            }

            sbSQL.Append(" FROM " + DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS
                                  + " "
                );

            sbSQL.Append("INNER JOIN RecHubData.dimBanks ");
            sbSQL.Append("ON RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimBanks.SiteBankID ");
            
            // WHERE Clause

            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
            sbWhere.Append(" RecHubData.dimClientAccounts.MostRecent = 1");

            if (FormatLockboxLimitClause(parms, extractDef.LockboxLimitItems, clientAccountTableDef, out strWhereClause))
            {
                sbWhere.Append(strWhereClause);
            }

            if (FormatLimitClause(
                extractDef.LockboxLimitItems,
                DatabaseConstants.LIMIT_TABLES_CLIENTACCOUNTS,
                clientAccountTableDef,
                out strLimitClause))
            {

                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(strLimitClause);
            }

            BuildParametersLimitClauses(parms, sbWhere, DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS);
            sbSQL.Append(sbWhere.ToString());

            // ORDER BY Clause
            BuildOrderByArray(LayoutLevelEnum.ClientAccount,
                              QualifyOrderByColumns(arTableNames.ToArray(), arTableDefs.ToArray(), extractDef.LockboxOrderByColumns),
                              new cQualifiedOrderByColumn[] { new cQualifiedOrderByColumn(DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS, "SiteClientAccountID") },
                              ref arOrderByClause);


            if (FormatOrderByClause(arOrderByClause, out strOrderByClause))
            {
                sbSQL.Append(" ORDER BY " + strOrderByClause);
            }

            return (SQLPublic.UnformatSQL(sbSQL.ToString()));
         }

        public static string SQL_GetDepositDates(
            IExtractParms parms,
            IExtractDef extractDef,
            string factTableName,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> factTableDef)
        {

            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            List<cQualifiedOrderByColumn> arOrderByClause = new List<cQualifiedOrderByColumn>();
            List<string> arTableNames = new List<string>();
            List<Dictionary<string, IColumn>> arTableDefs = new List<Dictionary<string, IColumn>>();

            // Deposit Date Key or Processing Date Key
            var column = ".DepositDateKey";
            if (parms.DepositDateFrom == DateTime.MinValue)
                column = ".SourceProcessingDateKey";

            sbSQL.Append(" SELECT ");
            sbSQL.Append(factTableName + column);

            sbSQL.Append(" FROM " + factTableName);

            sbWhere.Append(BuildDateWhereClause(parms, factTableName));

            sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
            sbWhere.Append(factTableName + ".IsDeleted=0");

            if (sbWhere.Length > 0)
            {
                sbSQL.Append(sbWhere.ToString());
            }

            sbSQL.Append(" GROUP BY " + factTableName + column);
            sbSQL.Append(" ORDER BY " + factTableName + column);

            return (SQLPublic.UnformatSQL(sbSQL.ToString()));
        }

        // WI 169054 - OLF Services (the method GetImage) now needs 2 more items:
        //  1. SourceBatchID
        //  2. ImportTypeShortName
        // So, we're going to be joining on the dimImportTypes table for all queries
        // for batches and below.

        public static string SQL_GetBatches(
            IExtractParms parms,
            IExtractDef extractDef,
            int depositDateKey,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef)
        {

            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            string strSelectClause;
            string strOrderByClause;
            List<string> dicFieldNames;
            List<cQualifiedOrderByColumn> arOrderByClause = new List<cQualifiedOrderByColumn>();
            List<string> arTableNames = new List<string>();
            List<Dictionary<string, IColumn>> arTableDefs = new List<Dictionary<string, IColumn>>();

            //---------------------------------------------------------------------------
            // Add Table names to the TableDefs and TableNames collections so that 
            // the SELECT clause can be built to include data from higher-level entities.
            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTBATCH);
            arTableDefs.Add(batchTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS);
            arTableDefs.Add(clientAccountTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBANKS);
            arTableDefs.Add(bankTableDef);

            // WI 132009 : Added in these other tables so we can expose LongName.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHPAYMENTTYPES);
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHSOURCES);

            //---------------------------------------------------------------------------

            // SELECT Clause
            sbSQL.Append(" SELECT ");
            dicFieldNames = new List<string>();
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteBankID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteClientAccountID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.LongName");
            // WI 134896 : Added SiteCodeID as a default select.  We need this for the event logs.
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteCodeID");
            dicFieldNames.Add("RecHubData.factBatchSummary.BatchID");
            dicFieldNames.Add("RecHubData.factBatchSummary.ImmutableDateKey");
            dicFieldNames.Add("RecHubData.factBatchSummary.DepositDateKey");
            dicFieldNames.Add("RecHubData.factBatchSummary.SourceBatchID");
            dicFieldNames.Add("RecHubData.dimImportTypes.ShortName");
            dicFieldNames.Add("RecHubData.dimBatchSources.ShortName");
            if (FormatSelectClause(dicFieldNames,
                                  extractDef.AllLayouts,
                                  extractDef.JointDetailLayouts,
                                  extractDef.BatchOrderByColumns,
                                  arTableNames.ToArray(),
                                  arTableDefs.ToArray(),
                                  out strSelectClause))
            {
                sbSQL.Append(strSelectClause);
            }
            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            sbSQL.Append(" FROM RecHubData.factBatchSummary ");
            sbSQL.Append("INNER JOIN RecHubData.dimBanks ");
            sbSQL.Append("ON RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimClientAccounts ");
            sbSQL.Append("ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey ");
            // WI 132009 : Added joins on the 2 other tables with 'LongName' we wish to expose.
            sbSQL.Append("INNER JOIN RecHubData.dimBatchPaymentTypes ");
            sbSQL.Append("ON RecHubData.factBatchSummary.BatchPaymentTypeKey = RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimBatchSources ");
            sbSQL.Append("ON RecHubData.factBatchSummary.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimImportTypes ");
            sbSQL.Append("ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimBatchExceptionStatuses ");
            sbSQL.Append("ON RecHubData.factBatchSummary.BatchExceptionStatusKey = RecHubData.dimBatchExceptionStatuses.BatchExceptionStatusKey ");

            sbSQL.Append(BuildTraceJoinClause(parms, "RecHubData.factBatchSummary"));

            sbWhere.Append(BuildDateWhereClause(parms, DatabaseConstants.TABLE_NAME_FACTBATCH));
            BuildTraceWhereClause(parms, sbWhere);

            sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
            sbWhere.Append(DatabaseConstants.TABLE_NAME_FACTBATCH + ".IsDeleted=0");

            string strLimitClause;
            
            if (FormatLimitClause(
                extractDef.BatchLimitItems,
                DatabaseConstants.LIMIT_TABLES_BATCHES,
                batchTableDef,
                out strLimitClause))
            {

                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(strLimitClause);
            }

            BuildParametersLimitClauses(parms, sbWhere, DatabaseConstants.TABLE_NAME_FACTBATCH);

            if (sbWhere.Length > 0)
            {
                sbSQL.Append(sbWhere.ToString());
            }

            // ORDER BY Clause
            // WI 135749 : Order-By reworking.
            BuildOrderByArray(LayoutLevelEnum.Batch,
                              QualifyOrderByColumns(arTableNames.ToArray(), arTableDefs.ToArray(), extractDef.BatchOrderByColumns),
                              new cQualifiedOrderByColumn[] { new cQualifiedOrderByColumn(DatabaseConstants.TABLE_NAME_FACTBATCH, "BatchID") },
                              ref arOrderByClause);

            if (FormatOrderByClause(arOrderByClause, out strOrderByClause))
            {
                sbSQL.Append(" ORDER BY " + strOrderByClause);
            }

            return (SQLPublic.UnformatSQL(sbSQL.ToString()));
        }

        public static string SQL_GetTransactions(
            IExtractParms parms,
            IExtractDef extractDef,
            int depositDateKey,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            Dictionary<string, IColumn> transactionsTableDef)
        {

            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            string strSelectClause;
            ////string strWhereClause;
            string strLimitClause;
            string strOrderByClause;
            List<string> dicFieldNames;
            List<cQualifiedOrderByColumn> arOrderByClause = new List<cQualifiedOrderByColumn>();
            List<string> arTableNames = new List<string>();
            List<Dictionary<string, IColumn>> arTableDefs = new List<Dictionary<string, IColumn>>();

            //---------------------------------------------------------------------------
            // Add Table names to the TableDefs and TableNames collections so that 
            // the SELECT clause can be built to include data from higher-level entities.
            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTTXN);
            arTableDefs.Add(transactionsTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTBATCH);
            arTableDefs.Add(batchTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS);
            arTableDefs.Add(clientAccountTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBANKS);
            arTableDefs.Add(bankTableDef);

            // WI 132009 : Added in these other tables so we can expose LongName.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHPAYMENTTYPES);
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHSOURCES);
            //---------------------------------------------------------------------------

            // SELECT Clause
            sbSQL.Append(" SELECT ");
            dicFieldNames = new List<string>();
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteBankID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteClientAccountID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.LongName");
            // WI 134896 : Added SiteCodeID as a default select.  We need this for the event logs.
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteCodeID");
            dicFieldNames.Add("RecHubData.factTransactionSummary.BatchID");
            dicFieldNames.Add("RecHubData.factTransactionSummary.TransactionID");
            dicFieldNames.Add("RecHubData.factTransactionSummary.ImmutableDateKey");
            dicFieldNames.Add("RecHubData.factTransactionSummary.SourceBatchID");

            if (FormatSelectClause(dicFieldNames,
                                  extractDef.AllLayouts,
                                  extractDef.JointDetailLayouts,
                                  extractDef.TransactionsOrderByColumns,
                                  arTableNames.ToArray(),
                                  arTableDefs.ToArray(),
                                  out strSelectClause))
            {
                sbSQL.Append(strSelectClause);
            }

            // FROM Clause
            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            sbSQL.Append(" FROM RecHubData.factTransactionSummary ");
            sbSQL.Append("INNER JOIN RecHubData.dimBanks ");
            sbSQL.Append("ON RecHubData.factTransactionSummary.BankKey = RecHubData.dimBanks.BankKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimClientAccounts ");
            sbSQL.Append("ON RecHubData.factTransactionSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimTransactionExceptionStatuses ");
            sbSQL.Append("ON RecHubData.factTransactionSummary.TransactionExceptionStatusKey = RecHubData.dimTransactionExceptionStatuses.TransactionExceptionStatusKey ");
            sbSQL.Append(BuildTraceJoinClause(parms, "RecHubData.factTransactionSummary"));

            // WHERE Clause
            sbWhere.Append(BuildDateWhereClause(parms, DatabaseConstants.TABLE_NAME_FACTTXN));
            BuildTraceWhereClause(parms, sbWhere);

            if (FormatLimitClause(extractDef.TransactionsLimitItems,
                                 DatabaseConstants.LIMIT_TABLES_TRANSACTIONS,
                                 transactionsTableDef,
                                 out strLimitClause))
            {

                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(strLimitClause);
            }

            sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
            sbWhere.Append(DatabaseConstants.TABLE_NAME_FACTTXN + ".IsDeleted=0");

            BuildParametersLimitClauses(parms, sbWhere, DatabaseConstants.TABLE_NAME_FACTTXN);

            if (sbWhere.Length > 0)
            {
                sbSQL.Append(sbWhere.ToString());
            }

            // ORDER BY Clause
            BuildOrderByArray(LayoutLevelEnum.Transaction,
                              QualifyOrderByColumns(arTableNames.ToArray(), arTableDefs.ToArray(), extractDef.TransactionsOrderByColumns),
                              new cQualifiedOrderByColumn[] { new cQualifiedOrderByColumn(DatabaseConstants.TABLE_NAME_FACTTXN, "TransactionID") },
                              ref arOrderByClause);

            if (FormatOrderByClause(arOrderByClause, out strOrderByClause))
            {
                sbSQL.Append(" ORDER BY " + strOrderByClause);
            }

            return (SQLPublic.UnformatSQL(sbSQL.ToString()));
        }

        public static string SQL_GetPayments(
            IExtractParms parms,
            IExtractDef extractDef,
            int depositDateKey,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            Dictionary<string, IColumn> transactionsTableDef,
            Dictionary<string, IColumn> paymentsTableDef)
        {

            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            string strSelectClause;
            string strLimitClause;
            string strOrderByClause;
            List<string> dicFieldNames;
            List<cQualifiedOrderByColumn> arOrderByClause = new List<cQualifiedOrderByColumn>();
            List<string> arTableNames = new List<string>();
            List<Dictionary<string, IColumn>> arTableDefs = new List<Dictionary<string, IColumn>>();

            //---------------------------------------------------------------------------
            // Add Table names to the TableDefs and TableNames collections so that 
            // the SELECT clause can be built to include data from higher-level entities.
            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTCHECKS);
            arTableDefs.Add(paymentsTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTTXN);
            arTableDefs.Add(transactionsTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTBATCH);
            arTableDefs.Add(batchTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS);
            arTableDefs.Add(clientAccountTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBANKS);
            arTableDefs.Add(bankTableDef);

            // WI 132009 : Added in these other tables so we can expose LongName.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHPAYMENTTYPES);
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHSOURCES);

            // WI 142972 : Added in these other tables so we can expose DDA and ABA.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMDDAS);

            // SELECT Clause
            sbSQL.Append(" SELECT ");
            dicFieldNames = new List<string>();
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteBankID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteClientAccountID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.LongName");
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteCodeID");
            dicFieldNames.Add("RecHubData.factChecks.BatchID");
            dicFieldNames.Add("RecHubData.factChecks.TransactionID");
            dicFieldNames.Add("RecHubData.factChecks.TxnSequence");
            dicFieldNames.Add("RecHubData.factChecks.BatchSequence");
            dicFieldNames.Add("RecHubData.factChecks.Amount");

            if (FormatSelectClause(
                dicFieldNames,
                extractDef.PaymentLayouts,
                extractDef.JointDetailLayouts,
                extractDef.PaymentsOrderByColumns,
                arTableNames.ToArray(),
                arTableDefs.ToArray(),
                out strSelectClause))
            {

                sbSQL.Append(strSelectClause);
            }

            if ((extractDef.PaymentLayouts != null && extractDef.PaymentLayouts.Any())
                || extractDef.JointDetailLayouts.Any())
            {
                var paymentfields = extractDef.PaymentLayouts
                    .SelectMany(x => x.LayoutFields)
                    .ToList();
                paymentfields.AddRange(extractDef.JointDetailLayouts.SelectMany(x => x.LayoutFields));
                sbSQL.Append(BuildDataEntryInnerSelectClauses(paymentfields, DataEntryTableType.Payments));
            }


            // FROM Clause
            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            sbSQL.Append(" FROM RecHubData.factChecks ");
            sbSQL.Append("INNER JOIN RecHubData.dimBanks ");
            sbSQL.Append("ON RecHubData.factChecks.BankKey = RecHubData.dimBanks.BankKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimClientAccounts ");
            sbSQL.Append("ON RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimDDAs ");
            sbSQL.Append("ON RecHubData.factChecks.DDAKey = RecHubData.dimDDAs.DDAKey ");
            sbSQL.Append(BuildTraceJoinClause(parms, "RecHubData.factChecks"));

            // WHERE Clause
            sbWhere.Append(BuildDateWhereClause(parms, DatabaseConstants.TABLE_NAME_FACTCHECKS));
            BuildTraceWhereClause(parms, sbWhere);

            if (FormatLimitClause(
                extractDef.PaymentsLimitItems,
                DatabaseConstants.LIMIT_TABLES_PAYMENTS,
                paymentsTableDef,
                out strLimitClause))
            {

                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(strLimitClause);
            }

            if (FormatLimitClauseDataEntryFields(DataEntryTableType.Payments,
                extractDef.PaymentsLimitItems,
                paymentsTableDef,
                out strLimitClause))
            {
                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(strLimitClause);
            }

            sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
            sbWhere.Append(DatabaseConstants.TABLE_NAME_FACTCHECKS + ".IsDeleted=0");

            BuildParametersLimitClauses(parms, sbWhere, DatabaseConstants.TABLE_NAME_FACTCHECKS);

            if (sbWhere.Length > 0)
            {
                sbSQL.Append(sbWhere.ToString());
            }

            // ORDER BY Clause
            // WI 135749 : Reworking of the order by.
            BuildOrderByArray(LayoutLevelEnum.Payment,
                              QualifyOrderByColumns(arTableNames.ToArray(), arTableDefs.ToArray(), extractDef.PaymentsOrderByColumns),
                              new cQualifiedOrderByColumn[] { new cQualifiedOrderByColumn(DatabaseConstants.TABLE_NAME_FACTCHECKS, "CheckSequence") },
                              ref arOrderByClause);



            if (FormatOrderByClause(arOrderByClause, out strOrderByClause))
            {
                sbSQL.Append(" ORDER BY " + strOrderByClause);
            }

            return (SQLPublic.UnformatSQL(sbSQL.ToString()));
        }

        /// <summary>
        /// Builds a series of inner-select clauses for DataEntry keys/values.
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        private static string BuildDataEntryInnerSelectClauses(IEnumerable<ILayoutField> fields, DataEntryTableType table)
        {
            if (fields == null)
                return null;

            var filtername = table == DataEntryTableType.Payments
                ? "RecHubData.ChecksDataEntry"
                : "RecHubData.StubsDataEntry";
            var ischeck = table == DataEntryTableType.Payments
                ? 1
                : 0;
            var fulltablename = table == DataEntryTableType.Payments
                ? "RecHubData.factChecks"
                : "RecHubData.factStubs";

            var decolumns = fields
                .Where(x => x.IsDataEntry && x.SimpleTableName == filtername);

            if (!decolumns.Any())
                return string.Empty;


            var sb = new StringBuilder();
            EnsureUniqueFieldNames(decolumns);
            foreach (var col in decolumns)
            {
                var datacolumn = string.Empty;

                // See LegacySupport.cs for conversions.
                if (col.DataType == 1)
                    datacolumn = "RecHubData.factDataEntryDetails.DataEntryValue";
                else if (col.DataType == 6)
                    datacolumn = "RecHubData.factDataEntryDetails.DataEntryValueFloat";
                else if (col.DataType == 7)
                    datacolumn = "RecHubData.factDataEntryDetails.DataEntryValueMoney";
                else if (col.DataType == 24)
                    datacolumn = "RecHubData.factDataEntryDetails.DataEntryValueDateTime";

                    // Todo: Convert to string interpolation when we have the ability to build it. (where clause)
                sb.Append(",");
                sb.Append(DataEntryColumnName(datacolumn,fulltablename,ischeck,
                                              col.SimpleFieldName,col.DisplayName,col.PaymentSource));
                sb.Append(string.Format(" AS '{0}'", col.UniqueFieldName));
            }

            return sb.ToString();
        }
        

        public static void EnsureUniqueFieldNames(IEnumerable<ILayoutField> fields) {
            var usedFieldNames = new List<string>();

            foreach (var field in fields) {
                int index = 1;
                string baseFieldName = field.FieldName;
                while (usedFieldNames.Contains(field.UniqueFieldName.ToLower())) {
                    field.UniqueFieldName = string.Format("{0}_{1}", baseFieldName, index++);
                }
                usedFieldNames.Add(field.UniqueFieldName.ToLower());
            }
        }

        public static string SQL_GetStubs(
            IExtractParms parms,
            IExtractDef extractDef,
            int depositDateKey, // DLD - WI 129395�- This parameter was not being used in constructing the SQL 'Where" clause.
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            Dictionary<string, IColumn> transactionsTableDef,
            Dictionary<string, IColumn> stubsTableDef/*,
            Dictionary<string, IColumn> StubsDataEntryTableDef*/)
        {

            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            string strSelectClause;
            ////string strWhereClause;
            string strLimitClause;
            string strOrderByClause;
            List<string> dicFieldNames;
            List<cQualifiedOrderByColumn> arOrderByClause = new List<cQualifiedOrderByColumn>();
            List<string> arTableNames = new List<string>();
            List<Dictionary<string, IColumn>> arTableDefs = new List<Dictionary<string, IColumn>>();

            //---------------------------------------------------------------------------
            // Add Table names to the TableDefs and TableNames collections so that 
            // the SELECT clause can be built to include data from higher-level entities.
            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTSTUBS);
            arTableDefs.Add(stubsTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTTXN);
            arTableDefs.Add(transactionsTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTBATCH);
            arTableDefs.Add(batchTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS);
            arTableDefs.Add(clientAccountTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBANKS);
            arTableDefs.Add(bankTableDef);

            // WI 132009 : Added in these other tables so we can expose LongName.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHPAYMENTTYPES);
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHSOURCES);
            //---------------------------------------------------------------------------

            // SELECT Clause
            sbSQL.Append(" SELECT ");
            dicFieldNames = new List<string>();
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteBankID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteClientAccountID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.LongName");
            // WI 134896 : Added SiteCodeID as a default select.  We need this for the event logs.
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteCodeID");
            dicFieldNames.Add("RecHubData.factStubs.BatchID");
            dicFieldNames.Add("RecHubData.factStubs.TransactionID");
            dicFieldNames.Add("RecHubData.factStubs.BatchSequence");
            dicFieldNames.Add("RecHubData.factStubs.Amount");
            if (FormatSelectClause(
                dicFieldNames,
                extractDef.AllLayouts,
                extractDef.JointDetailLayouts,
                extractDef.StubsOrderByColumns,
                arTableNames.ToArray(),
                arTableDefs.ToArray(),
                out strSelectClause))
            {

                sbSQL.Append(strSelectClause);
            }

            if (extractDef.StubLayouts != null && extractDef.StubLayouts.Any()
                || extractDef.JointDetailLayouts.Any())
            {
                var stubfields = extractDef.StubLayouts
                    .SelectMany(x => x.LayoutFields)
                    .ToList();
                stubfields.AddRange(extractDef.JointDetailLayouts.SelectMany(x => x.LayoutFields));
                sbSQL.Append(BuildDataEntryInnerSelectClauses(stubfields, DataEntryTableType.Stubs));
            }

            // FROM Clause
            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            sbSQL.Append(" FROM RecHubData.factStubs ");
            sbSQL.Append("INNER JOIN RecHubData.dimBanks ");
            sbSQL.Append("ON RecHubData.factStubs.BankKey = RecHubData.dimBanks.BankKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimClientAccounts ");
            sbSQL.Append("ON RecHubData.factStubs.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey ");
            sbSQL.Append(BuildTraceJoinClause(parms, "RecHubData.factStubs"));

            // WHERE Clause
            sbWhere.Append(BuildDateWhereClause(parms, DatabaseConstants.TABLE_NAME_FACTSTUBS));
            BuildTraceWhereClause(parms, sbWhere);

            if (FormatLimitClause(extractDef.StubsLimitItems,
                                 DatabaseConstants.LIMIT_TABLES_STUBS,
                                 stubsTableDef,
                                 out strLimitClause))
            {

                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(strLimitClause);
            }

            if (FormatLimitClauseDataEntryFields(DataEntryTableType.Stubs, 
                                                 extractDef.StubsLimitItems,
                                                 stubsTableDef,
                                                 out strLimitClause))
            {
                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(strLimitClause);
            }


            sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
            sbWhere.Append(DatabaseConstants.TABLE_NAME_FACTSTUBS + ".IsDeleted=0");

            BuildParametersLimitClauses(parms, sbWhere, DatabaseConstants.TABLE_NAME_FACTSTUBS);

            if (sbWhere.Length > 0)
            {
                sbSQL.Append(sbWhere.ToString());
            }

            // ORDER BY Clause
            // WI 135749 : Rework of the order-by clauses.

            BuildOrderByArray(LayoutLevelEnum.Stub,
                              QualifyOrderByColumns(arTableNames.ToArray(), arTableDefs.ToArray(), extractDef.StubsOrderByColumns),
                              new cQualifiedOrderByColumn[] { new cQualifiedOrderByColumn(DatabaseConstants.TABLE_NAME_FACTSTUBS, "StubSequence") },
                              ref arOrderByClause);

            if (FormatOrderByClause(arOrderByClause, out strOrderByClause))
            {
                sbSQL.Append(" ORDER BY " + strOrderByClause);
            }

            return (SQLPublic.UnformatSQL(sbSQL.ToString()));
        }

        private static string FormatLimitClause_DataEntryType(DataEntryType dataEntryType)
        {
            StringBuilder sbWHERE = new StringBuilder(" WHERE RecHubData.dimDataEntryColumns.TableType in (");
            try
            {
                if (dataEntryType == DataEntryType.PaymentsDataEntry)
                    sbWHERE.Append((int)DataEntryTableType.Payments + ", " + (int)DataEntryTableType.PaymentsDataEntry + ")");
                else
                    sbWHERE.Append((int)DataEntryTableType.Stubs + ", " + (int)DataEntryTableType.StubsDataEntry + ")");
            }
            catch (Exception ex)
            {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return sbWHERE.ToString();
        }

        public static string SQL_GetDocuments(
            IExtractParms parms,
            IExtractDef extractDef,
            int depositDateKey,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            Dictionary<string, IColumn> transactionsTableDef,
            Dictionary<string, IColumn> documentsTableDef/*,
            Dictionary<string, IColumn> DocumentsDataEntryTableDef*/)
        {

            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            string strSelectClause;
            string strLimitClause;
            string strOrderByClause;
            List<string> dicFieldNames;
            List<cQualifiedOrderByColumn> arOrderByClause = new List<cQualifiedOrderByColumn>();
            List<string> arTableNames = new List<string>();
            List<Dictionary<string, IColumn>> arTableDefs = new List<Dictionary<string, IColumn>>();

            //---------------------------------------------------------------------------
            // Add Table names to the TableDefs and TableNames collections so that 
            // the SELECT clause can be built to include data from higher-level entities.

            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTDOCUMENTS);
            arTableDefs.Add(documentsTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTTXN);
            arTableDefs.Add(transactionsTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_FACTBATCH);
            arTableDefs.Add(batchTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS);
            arTableDefs.Add(clientAccountTableDef);

            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBANKS);
            arTableDefs.Add(bankTableDef);

            // WI 132009 : Added in these other tables so we can expose LongName.
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHPAYMENTTYPES);
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMBATCHSOURCES);
            arTableNames.Add(DatabaseConstants.TABLE_NAME_DIMDOCUMENTTYPES);
            //---------------------------------------------------------------------------

            // SELECT Clause
            sbSQL.Append(" SELECT ");
            dicFieldNames = new List<string>();
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteBankID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteClientAccountID");
            dicFieldNames.Add("RecHubData.dimClientAccounts.LongName");
            // WI 134896 : Added SiteCodeID as a default select.  We need this for the event logs.
            dicFieldNames.Add("RecHubData.dimClientAccounts.SiteCodeID");
            dicFieldNames.Add("RecHubData.factDocuments.BatchID");
            dicFieldNames.Add("RecHubData.factDocuments.TransactionID");
            dicFieldNames.Add("RecHubData.factDocuments.BatchSequence");
            dicFieldNames.Add("RecHubData.factDocuments.DocumentSequence");
            dicFieldNames.Add("RecHubData.dimDocumentTypes.FileDescriptor");

            if (FormatSelectClause(dicFieldNames,
                                  extractDef.AllLayouts,
                                  extractDef.JointDetailLayouts,
                                  extractDef.DocumentsOrderByColumns,
                                  arTableNames.ToArray(),
                                  arTableDefs.ToArray(),
                                  out strSelectClause))
            {

                sbSQL.Append(strSelectClause);
            }

            ////// FROM Clause
            sbSQL.Append(" FROM " + DatabaseConstants.TABLE_NAME_FACTDOCUMENTS);

            // 129986 : Edited to allow multiple tables for parent hierarchy fields.
            sbSQL.Append(" INNER JOIN RecHubData.dimBanks ");
            sbSQL.Append("ON RecHubData.factDocuments.BankKey = RecHubData.dimBanks.BankKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimClientAccounts ");
            sbSQL.Append("ON RecHubData.factDocuments.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey ");
            sbSQL.Append("INNER JOIN RecHubData.dimDocumentTypes ");
            sbSQL.Append("ON RecHubData.factDocuments.DocumentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey ");
            sbSQL.Append(BuildTraceJoinClause(parms, "RecHubData.factDocuments"));

            sbWhere.Append(BuildDateWhereClause(parms, DatabaseConstants.TABLE_NAME_FACTDOCUMENTS));
            BuildTraceWhereClause(parms, sbWhere);

            if (FormatLimitClause(extractDef.DocumentsLimitItems,
                                 DatabaseConstants.LIMIT_TABLES_DOCUMENTS,
                                 documentsTableDef,
                                 out strLimitClause))
            {
                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(strLimitClause);
            }

            sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
            sbWhere.Append(DatabaseConstants.TABLE_NAME_FACTDOCUMENTS + ".IsDeleted=0");

            BuildParametersLimitClauses(parms, sbWhere, DatabaseConstants.TABLE_NAME_FACTDOCUMENTS);

            if (sbWhere.Length > 0)
            {
                sbSQL.Append(sbWhere.ToString());
            }

            // ORDER BY Clause
            // WI 135749 : Rework of the Order-By clauses.

            BuildOrderByArray(LayoutLevelEnum.Document,
                              QualifyOrderByColumns(arTableNames.ToArray(), arTableDefs.ToArray(), extractDef.DocumentsOrderByColumns),
                              new cQualifiedOrderByColumn[] { new cQualifiedOrderByColumn(DatabaseConstants.TABLE_NAME_FACTDOCUMENTS, "DocumentSequence") },
                              ref arOrderByClause);

            if (FormatOrderByClause(arOrderByClause, out strOrderByClause))
            {
                sbSQL.Append(" ORDER BY " + strOrderByClause);
            }

            return (SQLPublic.UnformatSQL(sbSQL.ToString()));
        }

        /// <summary>
        /// Function exists to form WHERE clauses for DepositDates and ProcessingDates.
        /// </summary>
        /// <param name="parms"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private static string BuildDateWhereClause(IExtractParms parms, string tableName)
        {
            // First validate our data.
            if (tableName == null)
                return null;

            var sb = new StringBuilder();

            // Check DepositDate, form a where clause.
            if (parms.DepositDateFrom != DateTime.MinValue || parms.DepositDateTo != DateTime.MinValue)
            {
                sb.Append(sb.Length == 0 ? " WHERE " : " AND ");
                sb.Append(tableName + ".DepositDateKey >= " + parms.DepositDateFrom.ToString("yyyyMMdd")
                    + " AND " + tableName + ".DepositDateKey <= " + parms.DepositDateTo.ToString("yyyyMMdd"));
            }


            // Check ProcessingDate, form a where clause.
            if (parms.ProcessingDateFrom != DateTime.MinValue || parms.ProcessingDateTo != DateTime.MinValue)
            {
                sb.Append(sb.Length == 0 ? " WHERE " : " AND ");
                sb.Append(tableName + ".SourceProcessingDateKey >= " + parms.ProcessingDateFrom.ToString("yyyyMMdd")
                    + " AND " + tableName + ".SourceProcessingDateKey <= " + parms.ProcessingDateTo.ToString("yyyyMMdd"));
            }

            return sb.ToString();
        }

        private static void BuildOrderByArray(
            LayoutLevelEnum layoutLevel,
            cQualifiedOrderByColumn[] orderByColumns,
            cQualifiedOrderByColumn[] defaultColumns,
            ref List<cQualifiedOrderByColumn> orderByClause)
        {
            orderByClause.AddRange(orderByColumns.Any() ? orderByColumns : defaultColumns);
        }

        private static bool FormatOrderByClause(
            IList<cQualifiedOrderByColumn> orderByFields,
            out string orderByClause)
        {

            StringBuilder sbOrderBy = new StringBuilder();
            Hashtable htOrderByColumns;

            if (orderByFields.Count > 0)
            {

                htOrderByColumns = new Hashtable();

                // WI 135749 : Looping through in the correct order now.
                foreach (var field in orderByFields)
                {
                    if (!htOrderByColumns.Contains(field.QualifiedColumnName))
                    {
                        if (field != orderByFields.First())
                            sbOrderBy.Append(", ");

                        sbOrderBy.Append(field);
                        htOrderByColumns.Add(field.QualifiedColumnName, null);
                    }
                }
            }

            orderByClause = sbOrderBy.ToString();

            return (orderByClause.Length > 0);
        }

        private static bool FormatBatchCTEJoin(
            string factTableName,
            out string batchCTEJoin)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append("    INNER JOIN BatchFilterCTE ON");
            sbSQL.Append("        " + factTableName + ".ClientAccountKey = BatchFilterCTE.ClientAccountKey");
            sbSQL.Append("           AND " + factTableName + ".BatchID = BatchFilterCTE.BatchID");
            sbSQL.Append("           AND " + factTableName + ".ImmutableDateKey = BatchFilterCTE.ImmutableDateKey");
            sbSQL.Append("           AND " + factTableName + ".DepositDateKey = BatchFilterCTE.DepositDateKey");

            batchCTEJoin = sbSQL.ToString();

            return (true);
        }

        private static bool FormatValueList(
            string fieldName,
            long[] values,
            long minValue,
            out string inClause)
        {

            string strInClause = string.Empty;
            long lngValue = 0;
            bool bolIsFirst = true;
            int intItemCount = 0;

            if (values.Length > 0)
            {
                for (int i = 0; i < values.Length; ++i)
                {
                    lngValue = values[i];
                    if (lngValue >= minValue)
                    {
                        if (bolIsFirst)
                        {
                            bolIsFirst = false;
                        }
                        else
                        {
                            strInClause += ", ";
                        }
                        strInClause += lngValue.ToString();
                        ++intItemCount;
                    }
                }
            }

            if (intItemCount == 1)
            {
                inClause = fieldName + " = " + strInClause;
            }
            else if (intItemCount > 1)
            {
                inClause = fieldName + " IN(" + strInClause + ")";
            }
            else
            {
                inClause = string.Empty;
            }

            return (intItemCount > 0);
        }

        private static bool FormatDateKeyRange(
            string fieldName,
            int dateFromKey,
            int dateToKey,
            out string dateRangeClause)
        {

            string strRetVal = string.Empty;

            if (dateFromKey > 0)
            {
                strRetVal = fieldName + " >= " + dateFromKey.ToString();
            }

            if (dateToKey > 0)
            {
                if (strRetVal.Length > 0)
                {
                    strRetVal += " AND ";
                }
                strRetVal += fieldName + " <= " + dateToKey.ToString();
            }

            dateRangeClause = strRetVal;

            return (strRetVal.Length > 0);
        }

        private static string[] GetJointDetailColumnNames(
            ILayout[] layouts,
            string tableName)
        {

            ArrayList arRetVal = new ArrayList();
            string strFieldName;
            int iPos;

            foreach (ILayout layout in layouts)
            {
                foreach (ILayoutField layoutfield in layout.LayoutFields)
                {
                    if (layoutfield.FieldName.ToLower().StartsWith(tableName.ToLower()))
                    {

                        iPos = layoutfield.FieldName.LastIndexOf('.');

                        if (iPos > -1)
                        {
                            strFieldName = layoutfield.FieldName.Substring(iPos + 1);

                            if (!arRetVal.Contains(strFieldName))
                            {
                                arRetVal.Add(strFieldName);
                            }   // if
                        }

                    }   // if
                }   // foreach
            }   // foreach

            return ((string[])arRetVal.ToArray(typeof(string)));

        }   // GetJointDetailColumns

        private static OrderedDictionary GetSQLFormattedColumns(ILayout[] Layouts)
        {

            OrderedDictionary columns = new OrderedDictionary();

            foreach (ILayout layout in Layouts)
            {
                foreach (ILayoutField layoutfield in layout.LayoutFields)
                {
                    if (!layoutfield.IsAggregate && !layoutfield.IsStatic && !layoutfield.IsFiller && layoutfield.FieldName.IndexOf('.') > -1)
                    {
                        columns.Add(layoutfield.FieldName, SQLPublic.SQLFormatColumnName(layoutfield.FieldName));
                    }
                }
            }

            return (columns);
        }

        private static bool FormatSelectClause(
            List<string> defaultFields,
            ILayout[] layouts,
            ILayout[] jointDetailLayouts,
            IOrderByColumn[] orderByColumns,
            string[] tableName,
            Dictionary<string, IColumn>[] tableDef,
            out string selectClause)
        {

            return (FormatSelectClause(
                defaultFields,
                layouts,
                jointDetailLayouts,
                orderByColumns,
                tableName,
                tableDef,
                true,
                out selectClause));
        }


        private static bool ColumnBelongsTo(ILayoutField column, string table)
        {
            var col = column.FieldName.ToLower();
            var tab = table.ToLower();

            // If 'RecHubData.factChecks' starts with ... 
            if (col.StartsWith(tab))
                return true;

            // Now we need to check for joined columns
            // See cJoinedTablesLib.cs, which we cannot access from this location.
            if (tab.ToLower() == "rechubdata.factbatchsummary"
                && (col == "rechubdata.dimbatchpaymenttypes.longname" ||
                    col == "rechubdata.dimbatchsources.longname" ||
                    col == "rechubdata.dimbatchexceptionstatuses.exceptionstatusname"))
                return true;

            if (tab.ToLower() == "rechubdata.factdocuments"
                && (col == "rechubdata.dimdocumenttypes.documenttypedescription"))
                return true;

            if (tab.ToLower() == "rechubdata.factchecks"
                && (col == "rechubdata.dimddas.dda" ||
                    col == "rechubdata.dimddas.aba"))
                return true;

            if (tab.ToLower() == "rechubdata.facttransactionsummary"
                && (col == "rechubdata.dimtransactionexceptionstatuses.exceptionstatusname"))
                return true;

            return false;
        }

        private static bool FormatSelectClause(
            List<string> defaultFields,
            ILayout[] layouts,
            ILayout[] jointDetailLayouts,
            IOrderByColumn[] orderByColumns,
            string[] tableName,
            Dictionary<string, IColumn>[] tableDef,
            bool aliasColumnNames,
            out string selectClause)
        {

            StringBuilder sbSelect = new StringBuilder();
            bool bolIsFirst = true;
            string strFieldName;

            foreach (string str in defaultFields)
            {

                if (bolIsFirst)
                {
                    bolIsFirst = false;
                }
                else
                {
                    sbSelect.Append(", ");
                }

                strFieldName = str;

                sbSelect.Append(strFieldName);
                if (aliasColumnNames)
                {
                    sbSelect.Append(" AS '" + strFieldName + "'");
                }
            }

            foreach (ILayout layout in layouts)
            {
                // Exclude fields NOT In our current data table.
                var table = tableName[0];
                var fields = layout.LayoutFields
                    .Where(x => ColumnBelongsTo(x, table));

                foreach (ILayoutField layoutfield in fields)
                {
                    if (!layoutfield.IsPredefField)
                    {
                        for (int i = 0; i < tableName.Length; ++i)
                        {
                            if (!defaultFields.Contains(tableName[i] + "." + layoutfield.SimpleFieldName))
                            {

                                if (bolIsFirst)
                                {
                                    bolIsFirst = false;
                                }
                                else
                                {
                                    sbSelect.Append(", ");
                                }
                                // WI 132009 : Using just the layoutfield tablename.
                                sbSelect.Append(layoutfield.SimpleTableName + "." + layoutfield.SimpleFieldName);
                                if (aliasColumnNames)
                                {
                                    sbSelect.Append(" AS '" + layoutfield.SimpleTableName + "." + layoutfield.SimpleFieldName + "'");
                                }
                                defaultFields.Add(layoutfield.SimpleTableName + "." + layoutfield.SimpleFieldName);

                                break;
                            }
                        }
                    }
                }
            }

            foreach (ILayout layout in jointDetailLayouts)
            {
                // Exclude fields NOT In our current data table.
                var table = tableName[0];
                var fields = layout.LayoutFields
                    .Where(x => ColumnBelongsTo(x, table));

                foreach (ILayoutField layoutfield in fields)
                {
                    if (!layoutfield.IsPredefField)
                    {
                        for (int i = 0; i < tableName.Length; ++i)
                        {
                            if ((layoutfield.SimpleTableName.ToLower() == tableName[i].ToLower()))
                            {

                                if (!defaultFields.Contains(tableName[i] + "." + layoutfield.SimpleFieldName))
                                {

                                    if (bolIsFirst)
                                    {
                                        bolIsFirst = false;
                                    }
                                    else
                                    {
                                        sbSelect.Append(", ");
                                    }
                                    // WI 132009 : Using just the layoutfield tablename.
                                    sbSelect.Append(layoutfield.SimpleTableName + "." + layoutfield.SimpleFieldName);
                                    if (aliasColumnNames)
                                    {
                                        sbSelect.Append(" AS '" + layoutfield.SimpleTableName + "." + layoutfield.SimpleFieldName + "'");
                                    }
                                    defaultFields.Add(layoutfield.SimpleTableName + "." + layoutfield.SimpleFieldName);

                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // WI 135749 : Overhaul on OrderBy columns.
            foreach (IOrderByColumn orderbycolumn in orderByColumns)
            {
                if (!defaultFields.Contains(orderbycolumn.FullName))
                {

                    if (bolIsFirst)
                        bolIsFirst = false;
                    else
                        sbSelect.Append(", ");

                    sbSelect.Append(orderbycolumn.FullName);
                    if (aliasColumnNames)
                        sbSelect.Append(" AS '" + orderbycolumn.FullName + "'");

                    defaultFields.Add(orderbycolumn.FullName);
                }
            }

            selectClause = sbSelect.ToString();

            return (sbSelect.Length > 0);
        }

        private static bool FormatLockboxLimitClause(
            IExtractParms parms,
            ILimitItem[] lockboxLimitItems,
            Dictionary<string, IColumn> clientAccountTableDef,
            out string whereClause)
        {

            string strInClause;
            string strLimitClause;

            StringBuilder sbWhere = new StringBuilder();

            if (FormatValueList("RecHubData.dimClientAccounts.SiteBankID", parms.BankID, long.MinValue, out strInClause))
            {
                sbWhere.Append(" AND " + strInClause);
            }

            if (FormatValueList("RecHubData.dimClientAccounts.SiteClientAccountID", parms.LockboxID, long.MinValue, out strInClause))
            {
                sbWhere.Append(" AND " + strInClause);
            }


            if (FormatLimitClause(lockboxLimitItems,
                                 new string[] { DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS },
                                 clientAccountTableDef,
                                 out strLimitClause))
            {

                sbWhere.Append(" AND " + strLimitClause);
            }

            whereClause = sbWhere.ToString();

            return (whereClause.Length > 0);
        }

        private static bool FormatBatchLimitClause(
            IExtractParms parms,
            ILimitItem[] batchLimitItems,
            int depositDateKey,
            string factTableName,
            Dictionary<string, IColumn> factTableDef,
            out string whereClause)
        {

            string strInClause;
            string strDateRangeClause;
            string strLimitClause;

            StringBuilder sbWhere = new StringBuilder();

            if (FormatValueList(factTableName + ".SourceBatchID", parms.BatchID, 1, out strInClause))
            {
                sbWhere.Append(strInClause);
            }

            if (FormatDateKeyRange(factTableName + ".ImmutableDateKey", parms.ProcessingDateFromKey, parms.ProcessingDateToKey, out strDateRangeClause))
            {
                sbWhere.Append(sbWhere.Length == 0 ? "" : " AND ");
                sbWhere.Append(strDateRangeClause);
            }

            if (depositDateKey > 0)
            {
                if (FormatDateKeyRange(factTableName + ".DepositDateKey", depositDateKey, depositDateKey, out strDateRangeClause))
                {
                    sbWhere.Append(sbWhere.Length == 0 ? "" : " AND ");
                    sbWhere.Append(strDateRangeClause);
                }
            }
            else if (FormatDateKeyRange(factTableName + ".DepositDateKey", parms.DepositDateFromKey, parms.DepositDateToKey, out strDateRangeClause))
            {
                sbWhere.Append(sbWhere.Length == 0 ? "" : " AND ");
                sbWhere.Append(strDateRangeClause);
            }

            if (FormatLimitClause(
                batchLimitItems,
                new string[] { factTableName },
                factTableDef,
                out strLimitClause))
            {

                sbWhere.Append(sbWhere.Length == 0 ? "" : " AND ");
                sbWhere.Append(strLimitClause);
            }

            whereClause = sbWhere.ToString();

            return (whereClause.Length > 0);
        }

        private static bool FormatLimitClause(
            ILimitItem[] limitItems,
            string[] tableName,
            Dictionary<string, IColumn> tableDef,
            out string limitClause)
        {

            StringBuilder sbRetVal = new StringBuilder();
            ILimitItem objLimitItem;

            for (int i = 0; i < limitItems.Length; ++i)
            {

                objLimitItem = limitItems[i];

                for (int j = 0; j < tableName.Length; ++j)
                {

                    if (tableDef.Any(x => x.Value.ColumnName == objLimitItem.FieldName))
                    {
                        sbRetVal.Append(CommonLib.repeatChar('(', objLimitItem.LeftParenCount) +
                                        objLimitItem.Table + "." +
                                        objLimitItem.FieldName + " " +
                                        LimitItemLib.FormatLogicalOperator(objLimitItem.LogicalOperator) + " ");


                        if (objLimitItem.LogicalOperator == LogicalOperatorEnum.Between ||
                           objLimitItem.LogicalOperator == LogicalOperatorEnum.In)
                        {
                            sbRetVal.Append(objLimitItem.Value);
                        }
                        else if (objLimitItem.LogicalOperator == LogicalOperatorEnum.IsNotNull ||
                                  objLimitItem.LogicalOperator == LogicalOperatorEnum.IsNull)
                        {
                            //Do Nothing
                        }
                        else
                        {
                            sbRetVal.Append(FormatSQLValue(objLimitItem.FieldName, objLimitItem.Value, tableDef));
                        }

                        sbRetVal.Append(CommonLib.repeatChar(')', objLimitItem.RightParenCount));

                        if (i < limitItems.Length - 1)
                        {
                            switch (objLimitItem.CompoundOperator)
                            {
                                case CompoundOperatorEnum.AndNot:
                                    sbRetVal.Append(" AND NOT ");
                                    break;
                                case CompoundOperatorEnum.Or:
                                    sbRetVal.Append(" OR ");
                                    break;
                                case CompoundOperatorEnum.OrNot:
                                    sbRetVal.Append(" OR NOT ");
                                    break;
                                default:
                                    sbRetVal.Append(" AND ");
                                    break;
                            }
                        }

                        break;
                    }
                }
            }

            limitClause = sbRetVal.ToString();

            if (limitClause.Length > 0)
            {
                limitClause = "(" + limitClause + ")";
            }

            return (limitClause.Length > 0);
        }

        private static bool FormatLimitClauseDataEntryFields(
            DataEntryTableType table,
            ILimitItem[] limitItems,
            Dictionary<string, IColumn> tableDef,
            out string limitClause)
        {

            StringBuilder sbRetVal = new StringBuilder();
            ILimitItem objLimitItem;

            var filterTableName = table == DataEntryTableType.Payments
                ? "ChecksDataEntry"
                : "StubsDataEntry";
            var isCheck = table == DataEntryTableType.Payments
                ? 1
                : 0;
            var fullTableName = table == DataEntryTableType.Payments
                ? "RecHubData.factChecks"
                : "RecHubData.factStubs";
            
            for (int i = 0; i < limitItems.Length; i++)
            {
                var datacolumn = string.Empty;
                objLimitItem = limitItems[i];
                if (objLimitItem.Table == filterTableName)
                {
                    // See LegacySupport.cs for conversions.
                    if (objLimitItem.DataType == 1)
                        datacolumn = "RecHubData.factDataEntryDetails.DataEntryValue";
                    else if (objLimitItem.DataType == 6)
                        datacolumn = "RecHubData.factDataEntryDetails.DataEntryValueFloat";
                    else if (objLimitItem.DataType == 7)
                        datacolumn = "RecHubData.factDataEntryDetails.DataEntryValueMoney";
                    else if (objLimitItem.DataType == 24)
                        datacolumn = "RecHubData.factDataEntryDetails.DataEntryValueDateTime";

                    sbRetVal.Append(CommonLib.repeatChar('(', objLimitItem.LeftParenCount) +
                                    DataEntryColumnName(datacolumn,
                                        fullTableName, isCheck, objLimitItem.FieldName, objLimitItem.DisplayName,
                                        objLimitItem.PaymentSource) +
                                    LimitItemLib.FormatLogicalOperator(objLimitItem.LogicalOperator) + " ");


                    if (objLimitItem.LogicalOperator == LogicalOperatorEnum.Between ||
                        objLimitItem.LogicalOperator == LogicalOperatorEnum.In)
                    {
                        sbRetVal.Append(objLimitItem.Value);
                    }
                    else if (objLimitItem.LogicalOperator == LogicalOperatorEnum.IsNotNull ||
                             objLimitItem.LogicalOperator == LogicalOperatorEnum.IsNull)
                    {
                        //Do Nothing
                    }
                    else
                    {
                        sbRetVal.Append(FormatSQLValue(objLimitItem.FieldName, objLimitItem.Value, tableDef));
                    }

                    sbRetVal.Append(CommonLib.repeatChar(')', objLimitItem.RightParenCount));

                    if (i < limitItems.Length - 1)
                    {
                        switch (objLimitItem.CompoundOperator)
                        {
                            case CompoundOperatorEnum.AndNot:
                                sbRetVal.Append(" AND NOT ");
                                break;
                            case CompoundOperatorEnum.Or:
                                sbRetVal.Append(" OR ");
                                break;
                            case CompoundOperatorEnum.OrNot:
                                sbRetVal.Append(" OR NOT ");
                                break;
                            default:
                                sbRetVal.Append(" AND ");
                                break;
                        }
                    }
                }

                //break;
            }

            limitClause = sbRetVal.ToString();

            if (limitClause.Length > 0)
            {
                limitClause = "(" + limitClause + ")";
            }

            return (limitClause.Length > 0);
        }

        private static string DataEntryColumnName(string dataColumn, string fullTablename, int isCheck,
            string simpleFieldName, string displayName, string paymentSource)
        {
            return " (SELECT TOP 1" +
                   string.Format(" {0} AS 'RecHub.factDataEntryDetails.DataEntryValue'", dataColumn) +
                   " FROM RecHubData.factDataEntryDetails" +
                   " INNER JOIN RecHubData.dimWorkgroupDataEntryColumns" +
                   " ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey" +
                   " INNER JOIN RecHubData.dimBatchSources" +
                   " ON RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey" +
                   string.Format(" WHERE RecHubData.factDataEntryDetails.BatchID = {0}.BatchID", fullTablename) +
                   string.Format(" AND RecHubData.factDataEntryDetails.BatchSequence = {0}.BatchSequence",
                       fullTablename) +
                   string.Format(" AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = {0}", isCheck) +
                   string.Format(" AND RecHubData.dimWorkgroupDataEntryColumns.FieldName = '{0}'",
                       simpleFieldName) +
                   string.Format(" AND RecHubData.dimWorkgroupDataEntryColumns.UILabel = '{0}'", displayName) +
                   string.Format(" AND RecHubData.dimBatchSources.LongName = '{0}'", paymentSource) +
                   string.Format(" AND RecHubData.factDataEntryDetails.DepositDateKey = {0}.DepositDateKey",
                       fullTablename) +
                   " AND RecHubData.factDataEntryDetails.IsDeleted = 0)";
        }

        private static string BuildTraceJoinClause(IExtractParms parms, string facttablename)
        {
            var sb = new StringBuilder();

            // If we're not using a trace field, kick out.
            if (parms.TraceField.Length <= 0)
                return string.Empty;

            // If we're not using any re-runs, then we need to select our batches and exclude what is in the trace table.
            // Else, if we're running on a re-run, then we need to join and include only the previous batches.
            var jointype = (parms.RerunList == null || parms.RerunList.Length == 0)
                ? " LEFT OUTER JOIN"
                : " INNER JOIN";

            sb.Append(jointype);
            sb.Append(" RecHubData.factExtractTraces");
            sb.Append(" ON RecHubData.factExtractTraces.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey");
            sb.Append(" AND RecHubData.factExtractTraces.BankKey = " + facttablename + ".BankKey");
            sb.Append(" AND RecHubData.factExtractTraces.BatchID = " + facttablename + ".BatchID");
            sb.Append(" AND RecHubData.factExtractTraces.ImmutableDateKey = " + facttablename + ".ImmutableDateKey");
            sb.Append(jointype);
            sb.Append(" RecHubData.dimExtractDescriptor");
            sb.Append(" ON RecHubData.factExtractTraces.ExtractDescriptorKey = RecHubData.dimExtractDescriptor.ExtractDescriptorKey");

            return sb.ToString();
        }

        private static void BuildParametersLimitClauses(IExtractParms parms, StringBuilder sb, string tablename)
        {
            string inclause;
            // BankID
            if (FormatValueList(DatabaseConstants.TABLE_NAME_DIMBANKS + ".SiteBankID", parms.BankID, 1, out inclause))
            {
                sb.Append(sb.Length == 0 ? " WHERE " : " AND ");
                sb.Append(inclause);
            }

            // Kick out early if we need to.
            if (tablename == DatabaseConstants.TABLE_NAME_DIMBANKS)
                return;

            // WorkgroupID
            if (FormatValueList(DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS + ".SiteClientAccountID", parms.LockboxID, 1, out inclause))
            {
                sb.Append(sb.Length == 0 ? " WHERE " : " AND ");
                sb.Append(inclause);
            }

            // Kick out early if we need to.
            if (tablename == DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS)
                return;

            // BatchID
            if (FormatValueList(tablename + ".SourceBatchID", parms.BatchID, 1, out inclause))
            {
                sb.Append(sb.Length == 0 ? " WHERE " : " AND ");
                sb.Append(inclause);
            }
        }

        private static void BuildTraceWhereClause(IExtractParms parms, StringBuilder sb)
        {
            // If we don't have any trace fields defined, kick out early.
            if (parms.TraceField.Length <= 0)
                return;

            // If we're re-running a previous extract.
            if (parms.RerunList.Length > 0)
            {
                sb.Append(sb.Length == 0 ? " WHERE " : " AND ");
                sb.Append(" RecHubData.factExtractTraces.ExtractDateTime IN(");
                for (int i = 0; i < parms.RerunList.Length; ++i)
                {
                    if (parms.RerunList[i] != null)
                        sb.Append("'" + parms.RerunList[i].ToString("MM/dd/yyyy hh:mm:ss tt") + "'");
                    if (i < parms.RerunList.Length - 1)
                        sb.Append(", ");
                }
                sb.Append(")");
                sb.Append(sb.Length == 0 ? " WHERE " : " AND ");
                sb.Append(" RecHubData.dimExtractDescriptor.ExtractDescriptor = '" + parms.TraceField + "'");
            }
            else
            {
                // If we're running on a new extract. (Time to exclude the previous batches)
                sb.Append(sb.Length == 0 ? " WHERE " : " AND ");
                sb.Append(" RecHubData.factExtractTraces.FactExtractKey IS NULL");
            }

        }

        private static bool FormatBatchFilterCTE(
            IExtractParms parms,
            ILimitItem[] lockboxLimitItems,
            ILimitItem[] batchLimitItems,
            string factTableName,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            out string traceClause)
        {

            StringBuilder sbBatchFilterCTE = new StringBuilder();
            StringBuilder sbWhere;
            string strWhereClause;

            sbBatchFilterCTE.Append(" WITH BatchFilterCTE (ClientAccountKey, BatchID, ImmutableDateKey, DepositDateKey)");
            sbBatchFilterCTE.Append(" AS");
            sbBatchFilterCTE.Append(" (");
            sbBatchFilterCTE.Append("     SELECT");
            sbBatchFilterCTE.Append("         RecHubData.factBatchSummary.ClientAccountKey,");
            sbBatchFilterCTE.Append("         RecHubData.factBatchSummary.BatchID,");
            sbBatchFilterCTE.Append("         RecHubData.factBatchSummary.ImmutableDateKey,");
            sbBatchFilterCTE.Append("         RecHubData.factBatchSummary.DepositDateKey");
            sbBatchFilterCTE.Append("     FROM RecHubData.factBatchSummary");
            sbBatchFilterCTE.Append("       INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey");

            // WHERE Clause
            sbWhere = new StringBuilder();
            if (FormatLockboxLimitClause(parms, lockboxLimitItems, clientAccountTableDef, out strWhereClause))
            {
                sbWhere.Append(" WHERE " + strWhereClause);
            }

            if (FormatBatchLimitClause(parms, batchLimitItems, 0, DatabaseConstants.TABLE_NAME_FACTBATCH, extractTracesTableDef, out strWhereClause))
            {
                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(strWhereClause);
            }

            sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
            sbWhere.Append(DatabaseConstants.TABLE_NAME_FACTBATCH + ".IsDeleted=0");

            sbBatchFilterCTE.Append(sbWhere);

            if (parms.TraceField.Length > 0)
            {

                if (parms.RerunList.Length > 0)
                {
                    sbBatchFilterCTE.Append(" INTERSECT");
                }
                else
                {
                    sbBatchFilterCTE.Append(" EXCEPT");
                }

                sbBatchFilterCTE.Append(" SELECT");
                sbBatchFilterCTE.Append("         RecHubData.factExtractTraces.ClientAccountKey,");
                sbBatchFilterCTE.Append("         RecHubData.factExtractTraces.BatchID,");
                sbBatchFilterCTE.Append("         RecHubData.factExtractTraces.ImmutableDateKey,");
                sbBatchFilterCTE.Append("         RecHubData.factExtractTraces.DepositDateKey");
                sbBatchFilterCTE.Append(" FROM RecHubData.factExtractTraces");
                sbBatchFilterCTE.Append("     INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factExtractTraces.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey");
                sbBatchFilterCTE.Append("     INNER JOIN RecHubData.dimExtractDescriptor ON RecHubData.factExtractTraces.ExtractDescriptorKey = RecHubData.dimExtractDescriptor.ExtractDescriptorKey");

                // WHERE Clause
                sbWhere = new StringBuilder();
                if (FormatLockboxLimitClause(parms, lockboxLimitItems, clientAccountTableDef, out strWhereClause))
                {
                    sbWhere.Append(" WHERE " + strWhereClause);
                }

                if (FormatBatchLimitClause(parms, batchLimitItems, 0, " RecHubData.factExtractTraces", extractTracesTableDef, out strWhereClause))
                {
                    sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                    sbWhere.Append(strWhereClause);
                }

                if (parms.RerunList.Length > 0)
                {
                    sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                    sbWhere.Append(" RecHubData.factExtractTraces.ExtractDateTime IN(");
                    for (int i = 0; i < parms.RerunList.Length; ++i)
                    {
                        if (parms.RerunList[i] != null)
                            sbWhere.Append("'" + parms.RerunList[i].ToString("MM/dd/yyyy hh:mm:ss tt") + "'");

                        if (i < parms.RerunList.Length - 1)
                            sbWhere.Append(", ");
                    }
                    sbWhere.Append(")");

                    sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                    sbWhere.Append(strWhereClause);
                }

                sbWhere.Append(sbWhere.Length == 0 ? " WHERE " : " AND ");
                sbWhere.Append(" RecHubData.dimExtractDescriptor.ExtractDescriptor = '" + parms.TraceField + "'");

                sbBatchFilterCTE.Append(sbWhere.ToString());
            }


            sbBatchFilterCTE.Append(")");

            traceClause = sbBatchFilterCTE.ToString();

            return (traceClause.Length > 0);
        }

        private static string FormatSQLValue(
            string fieldName,
            string fieldValue,
            Dictionary<string, IColumn> tableDef)
        {

            string strRetVal;

            string strFieldValue;
            DateTime dteTemp;

            if (tableDef.ContainsKey(fieldName))
            {
                switch (((IColumn)tableDef[fieldName]).Type)
                {
                    case SqlDbType.BigInt:
                        strRetVal = fieldValue;
                        break;
                    //case SqlDbType.Binary:
                    case SqlDbType.Bit:
                        strRetVal = fieldValue;
                        break;
                    //case SqlDbType.Char:
                    case SqlDbType.DateTime:
                        strRetVal = "'" + fieldValue + "'";
                        break;
                    case SqlDbType.Decimal:
                        strRetVal = fieldValue;
                        break;
                    case SqlDbType.Float:
                        strRetVal = fieldValue;
                        break;
                    //case SqlDbType.Image:
                    case SqlDbType.Int:
                        if (DateTime.TryParse(fieldValue, out dteTemp))
                        {
                            strFieldValue = dteTemp.ToString("yyyyMMdd");
                        }
                        else
                        {
                            strFieldValue = fieldValue;
                        }
                        strRetVal = strFieldValue;
                        break;
                    case SqlDbType.Money:
                        strRetVal = fieldValue;
                        break;
                    //case SqlDbType.NChar:
                    //case SqlDbType.NText:
                    //case SqlDbType.NVarChar:
                    //case SqlDbType.Real:
                    case SqlDbType.SmallDateTime:
                        strRetVal = "'" + fieldValue + "'";
                        break;
                    case SqlDbType.SmallInt:
                        strRetVal = fieldValue;
                        break;
                    case SqlDbType.SmallMoney:
                        strRetVal = fieldValue;
                        break;
                    //case SqlDbType.Text:
                    case SqlDbType.Timestamp:
                        strRetVal = "'" + fieldValue + "'";
                        break;
                    case SqlDbType.TinyInt:
                        strRetVal = fieldValue;
                        break;
                    //case SqlDbType.Udt:
                    //case SqlDbType.UniqueIdentifier:
                    //case SqlDbType.VarBinary:
                    //case SqlDbType.VarChar:
                    //case SqlDbType.Variant:
                    //case SqlDbType.Xml:
                    default:
                        // WI 115249 : Added a sanitization of ' characters. 
                        strRetVal = string.Format("'{0}'", ToSafeSqlLiteral(fieldValue));
                        break;
                }
            }
            else
            {
                // TODO: Log Error Condition
                strRetVal = "'" + fieldValue + "'";
            }

            return (strRetVal);
        }

        /// <summary>
        /// Found here:
        /// http://msdn.microsoft.com/en-us/library/ff648339.aspx
        /// WI 115249 : Added a sanitization of ' characters. 
        /// Function simply sanitizes "'" characters.
        /// </summary>
        /// <param name="sqlliteral"></param>
        /// <returns></returns>
        private static string ToSafeSqlLiteral(string sqlliteral)
        {
            return sqlliteral.Replace("'", "''");
        }

        private static string GetTableName(LayoutLevelEnum layoutLevel)
        {

            string strRetVal = string.Empty;

            switch (layoutLevel)
            {
                case LayoutLevelEnum.Bank:
                    strRetVal = DatabaseConstants.TABLE_NAME_DIMBANKS;
                    break;
                case LayoutLevelEnum.ClientAccount:
                    strRetVal = DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS;
                    break;
                case LayoutLevelEnum.Batch:
                    strRetVal = DatabaseConstants.TABLE_NAME_FACTBATCH;
                    break;
                case LayoutLevelEnum.Transaction:
                    strRetVal = DatabaseConstants.TABLE_NAME_FACTTXN;
                    break;
                case LayoutLevelEnum.Payment:
                    strRetVal = DatabaseConstants.TABLE_NAME_FACTCHECKS;
                    break;
                case LayoutLevelEnum.Stub:
                    strRetVal = DatabaseConstants.TABLE_NAME_FACTSTUBS;
                    break;
                case LayoutLevelEnum.Document:
                    strRetVal = DatabaseConstants.TABLE_NAME_FACTDOCUMENTS;
                    break;
            }

            return (strRetVal);
        }

        private static cQualifiedOrderByColumn[] QualifyOrderByColumns(
            string[] tableName,
            Dictionary<string, IColumn>[] tableDef,
            IOrderByColumn[] orderByColumns)
        {

            List<cQualifiedOrderByColumn> objRetVal = new List<cQualifiedOrderByColumn>();

            // loop goal: objRedVal.Add(new qualified column, with the tablename, columnname, and orderby direction)
            foreach (IOrderByColumn col in orderByColumns)
            {
                var tablename = col.FullName.Substring(0, col.FullName.LastIndexOf('.'));
                objRetVal.Add(new cQualifiedOrderByColumn(tablename, col.ColumnName, col.OrderByDir));
            }

            return ((cQualifiedOrderByColumn[])objRetVal.ToArray());
        }

        private class cQualifiedOrderByColumn
        {

            string _TableName;
            string _ColumnName;
            OrderByDirEnum _OrderByDir;

            public cQualifiedOrderByColumn(string vTableName, string vColumnName)
            {

                if (vTableName == null || vTableName.Length == 0)
                {
                    throw (new Exception("Table Name cannot be empty."));
                }

                if (vColumnName == null || vColumnName.Length == 0)
                {
                    throw (new Exception("Column Name cannot be empty."));
                }

                _TableName = vTableName;
                _ColumnName = vColumnName;
                _OrderByDir = OrderByDirEnum.Ascending;
            }

            public cQualifiedOrderByColumn(string vTableName, string vColumnName, OrderByDirEnum vOrderByDir)
            {

                if (vTableName == null || vTableName.Length == 0)
                {
                    throw (new Exception("Table Name cannot be empty."));
                }

                if (vColumnName == null || vColumnName.Length == 0)
                {
                    throw (new Exception("Column Name cannot be empty."));
                }

                _TableName = vTableName;
                _ColumnName = vColumnName;
                _OrderByDir = vOrderByDir;
            }

            public string TableName
            {
                get
                {
                    return (_TableName);
                }
            }

            public string ColumnName
            {
                get
                {
                    return (_ColumnName);
                }
            }

            public string QualifiedColumnName
            {
                get
                {
                    return (this.TableName + "." + this.ColumnName);
                }
            }

            public OrderByDirEnum OrderByDir
            {
                get
                {
                    return (_OrderByDir);
                }
            }

            public override string ToString()
            {
                return this.TableName + "." + this.ColumnName + (OrderByDir == OrderByDirEnum.Descending ? " DESC" : "");
            }
        }
    }
}
