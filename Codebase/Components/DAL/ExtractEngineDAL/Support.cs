﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   - Initial Version 
* WI 135744 BLR 04/08/2014
*   - Added a constant to represent the Name fields during audit inserts.
******************************************************************************/
namespace WFS.RecHub.DAL.ExtractEngine {

    /// <summary>
    /// 
    /// </summary>
    public delegate void OutputStatusMsgEventHandler(string msg);
    public delegate void OutputExtractLogMsgEventHandler(string msg, MessageImportance messageImportance);

    public static class Support
    {
        // WI 135744 : Migrated "DMPEXTRACT" to "Receivables360EXTRACT".  
        public const string AUDIT_NAME_FIELD = "Receivables360EXTRACT";
    }
}
