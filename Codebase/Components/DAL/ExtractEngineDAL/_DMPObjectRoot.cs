using System;
using System.Xml;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Database;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added logging for blackbox calls.
******************************************************************************/
namespace WFS.RecHub.DAL.ExtractEngine {

    /// <summary>
    /// ipo Image Service Object Root class.
    /// </summary>
	public abstract class _DMPObjectRoot : IDisposable {

        protected cSiteOptions _SiteOptions = null;
        protected cDatabase _Database = null;
        protected ltaLog _EventLog = null;
        protected string _SiteKey;

        protected string _ConnectionString = string.Empty;

        /// <summary>
        /// Event handler to output informational text.
        /// </summary>
        public event outputMessageEventHandler OutputMessage;

        /// <summary>
        /// EventHandler to output error information.
        /// </summary>
        public event outputErrorEventHandler OutputError;

        ///// <summary>
        ///// 
        ///// </summary>
        //public event ExceptionOccurredEventHandler ExceptionOccurred;

        // Track whether Dispose has been called.
        private bool disposed = false;        

        public _DMPObjectRoot(string siteKey, string connectionString) {
            this.SiteKey = siteKey;
            _ConnectionString = connectionString;
        }

        /// <summary>
        /// Determines the section of the local .ini file to be used for local 
        /// options.
        /// </summary>
        public string SiteKey {
            set { _SiteKey = value; }
            get { return _SiteKey; }
        }

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        internal cSiteOptions SiteOptions {
            get {
                if(_SiteOptions == null) { 
                    _SiteOptions = new cSiteOptions(this.SiteKey);
                }

                return _SiteOptions;
            }
        }

        protected void OnOutputMessage(
            string message,
            string src,
            MessageType messageType,
            MessageImportance messageImportance) {

            if(OutputMessage != null) {
                OutputMessage(
                    message,
                    src,
                    messageType,
                    messageImportance);
            } else {
                cCommonLogLib.LogMessageLTA(message, LTAMessageImportance.Essential);
            }
        }

        protected void OnOutputError(System.Exception e) {
            if(OutputError != null) {
                OutputError(e);
            } else {
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
        }

        /// <summary>
        /// Database component using settings from the local siteOptions object.
        /// </summary>
        internal cDatabase Database {
            get {
                if (_Database == null) {
                    _Database = new cDatabase(_ConnectionString); 

                    _Database.SetDeadlockPriority = SiteOptions.SetDeadlockPriority;
                    _Database.QueryRetryAttempts = SiteOptions.QueryRetryAttempts;

                    _Database.outputMessage += new OutputDbMessageEventHandler(DbOutputMessage);
                    _Database.outputError += new OutputDbErrorEventHandler(OnOutputError);
                }
                return _Database;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        protected void DbOutputMessage(
            string message,
            string src,
            DbMessageType messageType,
            DbMessageImportance messageImportance) {

            OnOutputMessage(
                message,
                src,
                LTAConvert.ConvertMessageType(messageType),
                LTAConvert.ConvertMessageImportance(messageImportance));
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose() {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing) {
            // Check to see if Dispose has already been called.
            if(!this.disposed) {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if(disposing) {
                    // Dispose managed resources.
                    if(_Database != null) {
                        _Database.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }

        // Use interop to call the method necessary
        // to clean up the unmanaged resource.
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~_DMPObjectRoot() {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }

    }
}
