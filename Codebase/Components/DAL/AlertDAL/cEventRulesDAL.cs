﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   EAS
* Date:     05/10/2013
*
* Purpose: Data Access Layer Class for Event Rules
*
* Modification History
* WI 99851 EAS 5/10/2013
*   - Initial
* WI 109747 EAS 07/30/2013
*   DAL components called with RecHubAdmin Connection Type 
******************************************************************************/

namespace WFS.RecHub.DAL
{
    public class cEventRulesDAL : _DALBase
    {
        #region Member Variables

        const string db_Schema = "RecHubAlert";

        const string eventRules_GetByFilter = "usp_EventRules_Get_ByFilters";
        const string eventRules_GetByID = "usp_EventRules_Get_ByEventRuleID";
        const string eventRules_Insert = "usp_EventRules_Ins";
        const string eventRules_Update = "usp_EventRules_Upd";
        const string alerts_Insert = "usp_Alerts_Ins";
        const string alerts_Delete_By_EventRuleID = "usp_Alerts_Del_ByEventRuleID";

        #endregion

        #region Constructors

        public cEventRulesDAL(string vSiteKey)
            : base(vSiteKey)
        {
        }

        public cEventRulesDAL(string vSiteKey, DAL._DMPObjectRoot.ConnectionType enmConnection)
            : base(vSiteKey, enmConnection) { }

        #endregion

        #region Methods

        #region Public

        #region Retrieval

        public bool GetEventRulesByFilter(int startRecord, int maxRows, string userName, int? eventID, int? siteCodeID, int? siteBankID, int? siteClientAccountID, out int totalRows, out DataTable dt)
        {
            dt = null;
            totalRows = 0;

            bool returnVal = false;

            SqlParameter[] parms;
            SqlParameter parmTotalRecords;

            List<SqlParameter> arParms = new List<SqlParameter>();
            arParms.Add(BuildParameter("@parmStartRecord", SqlDbType.Int, startRecord, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmRecordsPerPage", SqlDbType.Int, maxRows, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmUserName", SqlDbType.VarChar, userName == String.Empty ? null : userName, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmEventID", SqlDbType.Int, eventID, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountID, ParameterDirection.Input));

            parmTotalRecords = BuildParameter("@parmTotalRecords", SqlDbType.Int, 0, ParameterDirection.Output);
            arParms.Add(parmTotalRecords);

            parms = arParms.ToArray();

            try
            {
                returnVal = Database.executeProcedure(db_Schema + "." + eventRules_GetByFilter, parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetEventRulesByFilter");
            }

            if (returnVal)
            {
                int result;
                if (int.TryParse(parmTotalRecords.Value.ToString(), out result))
                    totalRows = result;
            }

            return returnVal;
        }


        public bool GetEventRulesByID(int eventRuleID, out DataTable dt)
        {
            dt = null;

            bool returnVal = false;

            SqlParameter[] parms;

            List<SqlParameter> arParms = new List<SqlParameter>();
            arParms.Add(BuildParameter("@parmEventRuleID", SqlDbType.Int, eventRuleID, ParameterDirection.Input));

            parms = arParms.ToArray();

            try
            {
                returnVal = Database.executeProcedure(db_Schema + "." + eventRules_GetByID, parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetEventRulesByID");
            }

            return returnVal;

        }

        #endregion

        #region Inserts

        public bool InsertEventRule(int eventID, int siteCodeID, int? siteBankID, int? siteClientAcctID, bool isActive, string description, string ruleOperator, string ruleValue, out int eventRuleID)
        {
            bool bolRetVal = false;
            eventRuleID = 0;
            SqlParameter[] parms;
            SqlParameter parmEventRuleID;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmEventID", SqlDbType.Int, eventID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankID == null ? 0 : siteBankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAcctID == null ? 0 : siteClientAcctID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmIsActive", SqlDbType.Bit, isActive == true ? 1 : 0, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDescription", SqlDbType.VarChar, description == String.Empty ? null : description, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmOperator", SqlDbType.VarChar, ruleOperator == String.Empty ? null : ruleOperator, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmValue", SqlDbType.VarChar, ruleValue == String.Empty ? null : ruleValue, ParameterDirection.Input));
                parmEventRuleID = BuildParameter("@parmEventRuleID", SqlDbType.Int, 0, ParameterDirection.Output);
                arParms.Add(parmEventRuleID);

                parms = arParms.ToArray();

                bolRetVal = Database.executeProcedure(db_Schema + "." + eventRules_Insert, parms);
                if (bolRetVal)
                {
                    eventRuleID = ((int)parmEventRuleID.Value);
                }
                else
                {
                    EventLog.logEvent("An error occurred while executing RecHubAlert.usp_EventRules_Ins"
                                    , this.GetType().Name
                                    , MessageType.Error
                                    , MessageImportance.Essential);
                    eventRuleID = -1;
                    bolRetVal = false;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertEventRule(int eventID, int eventLevel, int siteCodeID, int? siteBankID, int? siteClientAcctID, bool isActive, string description, string ruleOperator, string ruleValue, out int eventRuleID)");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        public bool InsertAlert(int? userID, int eventRuleID, out int alertID)
        {
            bool bolRetVal = false;
            alertID = 0;
            SqlParameter[] parms;
            SqlParameter parmAlertID;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEventRuleID", SqlDbType.Int, eventRuleID, ParameterDirection.Input));
                parmAlertID = BuildParameter("@parmAlertID", SqlDbType.Int, 0, ParameterDirection.Output);
                arParms.Add(parmAlertID);

                parms = arParms.ToArray();

                bolRetVal = Database.executeProcedure(db_Schema + "." + alerts_Insert, parms);
                if (bolRetVal)
                {
                    alertID = ((int)parmAlertID.Value);
                }
                else
                {
                    EventLog.logEvent("An error occurred while executing RecHubAlert.usp_Alerts_Ins"
                                    , this.GetType().Name
                                    , MessageType.Error
                                    , MessageImportance.Essential);
                    alertID = -1;
                    bolRetVal = false;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertAlert(int? userID, int eventRuleID, out int alertID)");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        #endregion

        #region Updates

        public bool UpdateEventRule(int eventRuleID, int eventID, int siteCodeID, int? siteBankID, int? siteClientAcctID, bool isActive, string description, string ruleOperator, string ruleValue)
        {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmEventRuleID", SqlDbType.Int, eventRuleID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEventID", SqlDbType.Int, eventID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankID == null ? 0 : siteBankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAcctID == null ? 0 : siteClientAcctID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmIsActive", SqlDbType.Bit, isActive == true ? 1 : 0, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDescription", SqlDbType.VarChar, description == String.Empty ? null : description, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmOperator", SqlDbType.VarChar, ruleOperator == String.Empty ? null : ruleOperator, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmValue", SqlDbType.VarChar, ruleValue == String.Empty ? null : ruleValue, ParameterDirection.Input));
                
                parms = arParms.ToArray();

                bolRetVal = Database.executeProcedure(db_Schema + "." + eventRules_Update, parms);
                
            }
            catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "UpdateEventRule(int eventID, int siteCodeID, int? siteBankID, int? siteClientAcctID, bool isActive, string description, string ruleOperator, string ruleValue, out int eventRuleID)");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        #endregion

        #region Deletes

        public bool DeleteAlertsByEventRuleID(int eventRuleID)
        {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmEventRuleID", SqlDbType.Int, eventRuleID, ParameterDirection.Input));

                parms = arParms.ToArray();

                bolRetVal = Database.executeProcedure(db_Schema + "." + alerts_Delete_By_EventRuleID, parms);

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DeleteAlertsByEventRuleID(int eventRuleID)");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        #endregion

        #endregion

        #endregion
    }
}
