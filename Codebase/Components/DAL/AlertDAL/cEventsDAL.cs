﻿using System;
using System.Data;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   EAS
* Date:     06/13/2013
*
* Purpose: Data Access Layer Class for Events
*
* Modification History
* WI 99851 EAS 6/13/2013
*   - Initial
* WI  99863  EAS  07/15/2013
*    Add Connection Type to Constructor
******************************************************************************/

namespace WFS.RecHub.DAL
{
    public class cEventsDAL : _DALBase
    {
        #region Member Variables

        const string db_Schema = "RecHubAlert";

        const string events_Get_Names = "usp_Events_Get_Names";
        const string events_Get = "usp_Events_Get";

        #endregion

        #region Constructors

        public cEventsDAL(string vSiteKey)
            : base(vSiteKey)
        {
        }

        public cEventsDAL(string vSiteKey, ConnectionType enmConnection)
            : base(vSiteKey, enmConnection) { }

        #endregion

        #region Methods

        #region Public

        public bool GetAllEvents(out DataTable dt)
        {
            dt = null;

            bool returnVal = false;

            try
            {
                returnVal = Database.executeProcedure(db_Schema + "." + events_Get, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetAllEvents");
            }

            return returnVal;

        }

        public bool GetEventsLookup(out DataTable dt)
        {
            dt = null;

            bool returnVal = false;

            try
            {
                returnVal = Database.executeProcedure(db_Schema + "." + events_Get_Names, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetEventsLookup");
            }

            return returnVal;

        }

        #endregion

        #endregion
    }
}
