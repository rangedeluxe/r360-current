﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Calvin Glomb
* Date: 01/15/2013
*
* Purpose: 
*
* Modification History
* WI 91173 CRG 02/21/2013 
*    Remove SQLAlert.cs
******************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// 
	/// </summary>
	public class cCENDSDAL : _DALBase {

		/// <summary>
		/// Initializes a new instance of the <see cref="cCENDSDAL"/> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
		public cCENDSDAL(string vSiteKey)
			: base(vSiteKey) {

		}

		#region Contacts

		/// <summary>
		/// Returns a Contact row associated with the specified ContactID.
		/// </summary>
		/// <param name="ContactID">The contact ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetContactByID(Guid ContactID, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetContactByID(ContactID), out dt));
		}

		/// <summary>
		/// Returns a Contact based on BankID/CustomerID/ClientAccountID/ContactName.  If Lockbox == -1
		/// then we are looking for a Customer-Level Contact, otherwise, it will return a Lockbox
		/// Level Contact.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="CustomerID">The customer ID.</param>
		/// <param name="LockboxID">The lockbox ID.</param>
		/// <param name="ContactName">Name of the contact.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetContactByName(int BankID, int CustomerID, int LockboxID, string ContactName, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetContactByName(BankID, CustomerID, LockboxID, ContactName), out dt));
		}

		/// <summary>
		/// Returns all Customer-Level Contacts associated with the Online User.
		/// </summary>
		/// <param name="UserCustomers">The user customers.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetOnlineContacts(List<cCustomer> UserCustomers, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetOnlineContacts(UserCustomers), out dt));
		}

		/// <summary>
		/// Returns all Customer-Level Contacts for the specified Customer.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="CustomerID">The customer ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetCustomerContacts(int BankID, int CustomerID, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetCustomerContacts(BankID, CustomerID), out dt));
		}

		/// <summary>
		/// Updates the contact.
		/// </summary>
		/// <param name="Contact">The contact.</param>
		/// <param name="IsActive">if set to <c>true</c> [is active].</param>
		/// <param name="RowsAffected">The rows affected.</param>
		/// <returns></returns>
		public bool UpdateContact(cContact Contact, bool IsActive, out int RowsAffected) {
			return (ExecuteSQL(SQLAlert.SQL_UpdateContact(Contact, IsActive), out RowsAffected));
		}

		#endregion Contacts

		#region Alerts
		/// <summary>
		/// Gets the online event rule by ID.
		/// </summary>
		/// <param name="EventRuleID">The event rule ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetOnlineEventRuleByID(Guid EventRuleID, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetOnlineEventRuleByID(EventRuleID), out dt));
		}

		/// <summary>
		/// Creates the alert.
		/// </summary>
		/// <param name="Alert">The alert.</param>
		/// <param name="LogonName">Name of the logon.</param>
		/// <param name="RowsAffected">The rows affected.</param>
		/// <returns></returns>
		public bool CreateAlert(cAlert Alert, string LogonName, out int RowsAffected) {
			return (ExecuteSQL(SQLAlert.SQL_CreateAlert(Alert, LogonName), out RowsAffected));
		}

		/// <summary>
		/// Gets the alert.
		/// </summary>
		/// <param name="ContactID">The contact ID.</param>
		/// <param name="DeliveryMethodID">The delivery method ID.</param>
		/// <param name="EventRuleID">The event rule ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetAlert(Guid ContactID, Guid DeliveryMethodID, Guid EventRuleID, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetAlert(ContactID, DeliveryMethodID, EventRuleID), out dt));
		}
		#endregion Alerts

		#region Delivery Methods
		/// <summary>
		/// Gets the delivery methods.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetDeliveryMethods(out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetDeliveryMethods(), out dt));
		}
		#endregion Delivery Methods

		#region Event Rules
		/// <summary>
		/// Gets the event rule by ID.
		/// </summary>
		/// <param name="EventRuleID">The event rule ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetEventRuleByID(Guid EventRuleID, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetEventRuleByID(EventRuleID), out dt));
		}

		/// <summary>
		/// Creates the event rule.
		/// </summary>
		/// <param name="EventRule">The event rule.</param>
		/// <param name="RowsAffected">The rows affected.</param>
		/// <returns></returns>
		public bool CreateEventRule(cEventRule EventRule, out int RowsAffected) {
			return (ExecuteSQL(SQLAlert.SQL_CreateEventRule(EventRule), out RowsAffected));
		}

		/// <summary>
		/// Updates the event rule.
		/// </summary>
		/// <param name="EventRule">The event rule.</param>
		/// <param name="LogonName">Name of the logon.</param>
		/// <param name="RowsAffected">The rows affected.</param>
		/// <returns></returns>
		public bool UpdateEventRule(cEventRule EventRule, string LogonName, out int RowsAffected) {
			return (ExecuteSQL(SQLAlert.SQL_UpdateEventRule(EventRule, LogonName), out RowsAffected));
		}

		/// <summary>
		/// Returns all available EventRules.    If SuperUser == false,
		/// </summary>
		/// <param name="Lockboxes">The lockboxes.</param>
		/// <param name="SuperUser">If SuperUser == true, all Customer-level and Lockbox-level EventRules are returned.
		/// If SuperUser == false, only Lockbox-level EventRules are returned.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetEventRules(cOLLockboxes Lockboxes, bool SuperUser, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetEventRules(Lockboxes, SuperUser), out dt));
		}

		#endregion Event Rules

		#region Events
		/// <summary>
		/// Gets the events.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetEvents(out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetEvents(), out dt));
		}

		/// <summary>
		/// Gets the active events.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetActiveEvents(out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetActiveEvents(), out dt));
		}

		/// <summary>
		/// Updates the event.
		/// </summary>
		/// <param name="EventID">The event ID.</param>
		/// <param name="iOnlineModifiable">The i online modifiable.</param>
		/// <returns></returns>
		public bool UpdateEvent(int EventID, int iOnlineModifiable) {
			return (ExecuteSQL(SQLAlert.SQL_UpdateEvent(EventID, iOnlineModifiable)));
		}
		#endregion Events

		#region Event Arguments
		/// <summary>
		/// Gets the event args.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetEventArgs(out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetEventArgs(), out dt));
		}
		#endregion Event Arguments

		#region Internet Deliverables
		/// <summary>
		/// Gets the deliverables.
		/// </summary>
		/// <param name="Deliverable">The deliverable.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetDeliverables(string Deliverable, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetDeliverables(Deliverable), out dt));
		}

		/// <summary>
		/// Gets the internet deliverables.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetInternetDeliverables(out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetInternetDeliverables(), out dt));
		}

		/// <summary>
		/// Gets the internet deliverable.
		/// </summary>
		/// <param name="InternetDeliverableID">The internet deliverable ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetInternetDeliverable(Guid InternetDeliverableID, out DataTable dt) {
			return (ExecuteSQL(SQLAlert.SQL_GetInternetDeliverable(InternetDeliverableID), out dt));
		}

		#endregion Internet Deliverables

		/// <summary>
		/// Adds the contact.
		/// </summary>
		/// <param name="Contact">The contact.</param>
		/// <returns></returns>
		public bool AddContact(cContact Contact) {
			bool bolRetVal = false;
			SqlParameter parmContactID;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@BankID", SqlDbType.Int, Contact.BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@CustomerID", SqlDbType.Int, Contact.CustomerID, System.Data.ParameterDirection.Input));
				arParms.Add(BuildParameter("@ClientAccountID", SqlDbType.Int, Contact.LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@ContactName", SqlDbType.VarChar, Contact.ContactName, ParameterDirection.Input));
				arParms.Add(BuildParameter("@ContactPin", SqlDbType.Int, null, ParameterDirection.Input));
				arParms.Add(BuildParameter("@ContactPassword", SqlDbType.VarChar, null, ParameterDirection.Input));
				arParms.Add(BuildParameter("@IsActive", SqlDbType.TinyInt, null, ParameterDirection.Input));
				arParms.Add(BuildParameter("@IsPrimary", SqlDbType.TinyInt, null, ParameterDirection.Input));
				arParms.Add(BuildParameter("@SiteName", SqlDbType.VarChar, null, ParameterDirection.Input));
				arParms.Add(BuildParameter("@Address", SqlDbType.VarChar, Contact.Address, ParameterDirection.Input));
				arParms.Add(BuildParameter("@City", SqlDbType.VarChar, Contact.City, ParameterDirection.Input));
				arParms.Add(BuildParameter("@State", SqlDbType.VarChar, Contact.State, ParameterDirection.Input));
				arParms.Add(BuildParameter("@Zip", SqlDbType.VarChar, Contact.Zip, ParameterDirection.Input));
				arParms.Add(BuildParameter("@Voice", SqlDbType.VarChar, Contact.Voice, ParameterDirection.Input));
				arParms.Add(BuildParameter("@Fax", SqlDbType.VarChar, Contact.Fax, ParameterDirection.Input));
				arParms.Add(BuildParameter("@Modem", SqlDbType.VarChar, null, ParameterDirection.Input));
				arParms.Add(BuildParameter("@Email", SqlDbType.VarChar, Contact.Email, ParameterDirection.Input));
				arParms.Add(BuildParameter("@CreatedBy", SqlDbType.VarChar, Contact.CreatedBy, ParameterDirection.Input));
				arParms.Add(BuildParameter("@ModifiedBy", SqlDbType.VarChar, Contact.ModifiedBy, ParameterDirection.Input));
				arParms.Add(BuildParameter("@TextPager", SqlDbType.VarChar, string.Empty, ParameterDirection.Input));
				arParms.Add(BuildParameter("@IsOnline", SqlDbType.Bit, 1, ParameterDirection.Input));
				arParms.Add(BuildParameter("@ContactID", SqlDbType.UniqueIdentifier, Guid.Empty, ParameterDirection.InputOutput));
				parms = arParms.ToArray();
				Database.executeProcedure("proc_ContactsInsert", parms);
				parmContactID = parms.Single(parm => parm.ParameterName == "@ContactID");
				if(parmContactID.Value != null && (Guid) parmContactID.Value != Guid.Empty) {
					Contact.ContactID = (Guid) parmContactID.Value;
					bolRetVal = true;
				}
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "AddContact");
				bolRetVal = false;
			}

			return (bolRetVal);
		}

		/// <summary>
		/// Adds the alert.
		/// </summary>
		/// <param name="Alert">The alert.</param>
		/// <param name="LogonName">Name of the logon.</param>
		/// <returns></returns>
		public bool AddAlert(cAlert Alert, string LogonName) {
			return (ExecuteSQL(SQLAlert.SQL_CreateAlert(Alert, LogonName)));
		}

		/// <summary>
		/// Deletes the alert.
		/// </summary>
		/// <param name="Alert">The alert.</param>
		/// <returns></returns>
		public bool DeleteAlert(cAlert Alert) {
			bool bolRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@iAlertID", SqlDbType.UniqueIdentifier, Alert.AlertID, ParameterDirection.Input));
				parms = arParms.ToArray();
				Database.executeProcedure("proc_AlertsDelete", parms);
				bolRetVal = true;
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "DeleteAlert");
				bolRetVal = false;
			}

			return (bolRetVal);
		}
	}
}
