﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.RecHub.DAL
{
    public interface ISessionDAL
    {
        bool InsertSessionActivityLog(Guid SessionID, string ModuleName, ActivityCodes ActivityCode, out int RowsAffected);

        bool InsertSessionActivityLog(Guid SessionID, string ModuleName, ActivityCodes ActivityCode, out int RowsAffected, out long ActivityID);

        bool InsertSessionActivityLog(Guid SessionID, string ModuleName, ActivityCodes ActivityCode, Guid OnlineImageQueueID, int itemCount, int BankID, int ClientAccountID, out int RowsReturned);

        bool SetWebDeliveredFlag(Guid OnlineImageQueueID, bool InternetDelivered);

        bool UpdateActivityLockbox(long ActivityLogID, int BankID, int ClientAccountID, out int RowsAffected);

        bool GetNextPageCounter(Guid SessionID, out DataTable dt);

        bool InsertSessionLog(Guid SessionID, int NextPage, string PageTitle, string ScriptName, string QueryString, out int RowsAffected);

        bool UpdateSession(int NextPage, Guid SessionID, out int RowsAffected);

        bool GetSession(Guid SessionID, out DataTable dt);

        //bool GetSession(Guid sessionID, int userType, out DataTable dt);

        IReadOnlyList<BreadCrumb> GetSessionBreadCrumbs(Guid SessionID);

        bool EndSession(Guid SessionID, out DataTable dt);
    }
}

