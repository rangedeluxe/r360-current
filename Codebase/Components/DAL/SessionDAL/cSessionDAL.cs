﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date:
*
* Purpose:
*
* Modification History
* CR 32562 JMC 03/10/2011
*    -Modified InsertSessionActivityLog() to add ActivityCode to parameter
*     list
*    -Changed InsertSessionActivityLog() parameter list order in overloaded
*     version to match primary call.
* CR 28866 JMC 07/26/2011
*    -Added GetSessionBreadCrumbs() function.
* CR 54329 CEJ 08/24/2012
*	Fix vulnerability of using a CSR Research cookie to access IP Admin pages
* WI 72100 CRG 01/11/2013
*	 Convert cSessionDAL.GetOLPreferences(int, out DataTable) to a stored procedure call
* WI 72101 CRG 01/11/2013
*	 Convert cSessionDAL.GetSession(Guid, out DataTable) to a stored procedure call
* WI 72102 CRG 01/11/2013
*	Convert cSessionDAL.GetSession(Guid, int, out DataTable) to a stored procedure call
* WI 72105 CRG 01/14/2013
*	Convert cSessionDAL.InsertSessionActivityLog(Guid, string,  ActivityCodes, out int) to a stored procedure call
* WI 72106 CRG 01/14/2013
*	Convert cSessionDAL.InsertSessionActivityLog(Guid, Guid, string,  ActivityCodes, out int) to a stored procedure call
* WI 72107 CRG 01/14/2013
*	Convert cSessionDAL.InsertSessionActivityLog(Guid, string,  ActivityCodes, Guid, int, int, int, out int) to a stored procedure call
* WI 72108 CRG 01/14/2013
*	Convert cSessionDAL.InsertSessionToken(Guid, Guid, Guid) to a stored procedure call
* WI 72109 CRG 01/14/2013
*	Convert cSessionDAL.InsertSessionToken(Guid, Guid) to a stored procedure call
* WI 72110 CRG 01/14/2013
*	Convert cSessionDAL.SetWebDeliveredFlag(Guid, bool) to a stored procedure call
* WI 72111 CRG 01/14/2013
*	Convert cSessionDAL.UpdateActivityLockbox(Guid, int, int, out int) to a stored procedure call
* WI 72112 CRG 01/14/2013
*	Convert cSessionDAL.UpdateSession(int, Guid, out int) to a stored procedure call
* WI 72113 CRG 01/14/2013
*	Convert cSessionDAL.UpdateSessionToken(Guid, DateTime, out int) to a stored procedure call
* WI 72114 CRG 01/14/2013
*	Convert cSessionDAL.UpdateSessionToken(Guid, DateTime) to a stored procedure call
* WI 72183 CRG 12/28/2012
*	Convert cLogonDAL.CreateSession(Guid, string, string, bool, int, out int) to a stored procedure call
* WI 72193 CRG 01/04/2013
*	Convert cLogonDAL.GetSession(Guid, out DataTable) to a stored procedure call
* WI 72194 CRG 01/04/2013
*	Convert cLogonDAL.GetSessionToken(Guid, out DataTable) to a stored procedure call
* WI 72195 CRG 01/09/2013
*	Convert cLogonDAL.GetSessionTokenBySessionID(Guid, out DataTable) to a stored procedure call
* WI 72205 CRG 01/09/2013
*	Convert cLogonDAL.InsertSessionToken(Guid, Guid, Guid) to a stored procedure call
* WI 72206 CRG 01/09/2013
*	Convert cLogonDAL.InsertSessionToken(Guid, Guid) to a stored procedure call
* WI 72207 CRG 01/09/2013
*	Convert cLogonDAL.RegisterSession(Guid, out int) to a stored procedure call
* WI 85338 CRG 01/11/2013
*	Convert cSessionDAL.GetSessionBreadCrumbs(Guid SessionID, out DataTable dt) to a stored procedure call
* WI 85490 CRG 01/14/2013
*	Convert cSessionDAL.GetNextPageCounter(Guid, out DataTable) to call a stored procedure call
* WI 91136 CRG 01/14/2013
*	Change Schema for SessionDAL
* WI 105022  WJS 06/11/2013
*    Add GetSession Method for GetSessionDataBySID(Guid SID, out DataTable results)
* WI 107687 TWE 07/02/2013
*    modify CreateSession to allow userAgent to pass in empty string if null
* WI 108223 TWE 07/08/2013
*    Change primary key of ActivityLog from Guid to Long (bigint)
* WI 109747 EAS 07/30/2013
*    Add Constructor with Connection Type
* WI 124123 TWE 12/03/2013
*    Eliminate Cross-Talk between DALs
* WI 155343 BLR 07/31/2014
*    Added 2 methods 'RegisterExternalSession' and 'EndSession'
* WI 157063 CMC 08/05/2014
*    Removed password param from RegisterExternalSession method.
******************************************************************************/
namespace WFS.RecHub.DAL
{

    /// <summary>
    /// Used to access Session details from database
    /// </summary>
    public class cSessionDAL : _DALBase, ISessionDAL
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="cSessionDAL" /> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        public cSessionDAL(string vSiteKey)
            : base(vSiteKey)
        {
        }

        public cSessionDAL(string vSiteKey, ConnectionType enmConnection)
            : base(vSiteKey, enmConnection) { }

        /// <summary>
        /// Inserts the session activity log.
        /// </summary>
        /// <param name="SessionID">The session ID.</param>
        /// <param name="ModuleName">Name of the module.</param>
        /// <param name="ActivityCode">The activity code.</param>
        /// <param name="RowsAffected">The rows affected.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool InsertSessionActivityLog(Guid SessionID, string ModuleName, ActivityCodes ActivityCode, out int RowsAffected)
        {
            bool bolRetVal = false;
            RowsAffected = 0;
            long lActivityID = -1;

            bolRetVal = InsertSessionActivityLog(SessionID, ModuleName, ActivityCode, out RowsAffected, out lActivityID);
            if (lActivityID < 0)
            {
                // make sure we actually inserted a record fail if we did not
                bolRetVal = false;
            }


            return bolRetVal;
        }

        /// <summary>
        /// Inserts the session activity log.
        /// </summary>
        /// <param name="ActivityLogID">The activity log ID.</param>
        /// <param name="SessionID">The session ID.</param>
        /// <param name="ModuleName">Name of the module.</param>
        /// <param name="ActivityCode">The activity code.</param>
        /// <param name="RowsAffected">The rows affected.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool InsertSessionActivityLog(Guid SessionID, string ModuleName, ActivityCodes ActivityCode, out int RowsAffected, out long ActivityID)
        {
            bool bolRetVal = false;
            RowsAffected = 0;
            ActivityID = -1;
            SqlParameter[] parms;
            SqlParameter parmRowsAffected;
            SqlParameter parmActivityID;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmModuleName", SqlDbType.VarChar, ModuleName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmActivityCode", SqlDbType.Int, (int)ActivityCode, ParameterDirection.Input));
                parmRowsAffected = (BuildParameter("@parmRowsAffected", SqlDbType.Int, RowsAffected, ParameterDirection.Output));
                arParms.Add(parmRowsAffected);
                parmActivityID = (BuildParameter("@parmActivityID", SqlDbType.BigInt, ActivityID, ParameterDirection.Output));
                arParms.Add(parmActivityID);
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_SessionActivityLog_Ins_ByActivityLogID", parms);
                if (bolRetVal)
                {
                    RowsAffected = (int)parmRowsAffected.Value;
                    ActivityID = (long)parmActivityID.Value;
                }
                else
                {
                    RowsAffected = 0;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertSessionActivityLog(Guid ActivityLogID, Guid SessionID, string ModuleName, ActivityCodes ActivityCode, out int RowsAffected)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Inserts the session activity log.
        /// </summary>
        /// <param name="SessionID">The session ID.</param>
        /// <param name="ModuleName">Name of the module.</param>
        /// <param name="ActivityCode">The activity code.</param>
        /// <param name="OnlineImageQueueID">The online image queue ID.</param>
        /// <param name="itemCount">The item count.</param>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="RowsReturned">The rows returned.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool InsertSessionActivityLog(Guid SessionID, string ModuleName, ActivityCodes ActivityCode, Guid OnlineImageQueueID, int itemCount, int BankID, int ClientAccountID, out int RowsReturned)
        {
            bool bolRetVal = false;
            RowsReturned = 0;
            SqlParameter[] parms;
            SqlParameter parmRowsAffected;
            try
            {
                List<SqlParameter> arParms;
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmModuleName", SqlDbType.VarChar, ModuleName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmActivityCode", SqlDbType.Int, (int)ActivityCode, ParameterDirection.Input));
                if (OnlineImageQueueID == Guid.Empty)
                {
                    arParms.Add(BuildParameter("@parmOnlineImageQueueID", SqlDbType.UniqueIdentifier, null, ParameterDirection.Input));
                }
                else
                {
                    arParms.Add(BuildParameter("@parmOnlineImageQueueID", SqlDbType.UniqueIdentifier, OnlineImageQueueID, ParameterDirection.Input));
                }

                arParms.Add(BuildParameter("@parmItemCount", SqlDbType.Int, itemCount, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                parmRowsAffected = (BuildParameter("@parmRowsReturned", SqlDbType.Int, RowsReturned, ParameterDirection.Output));
                arParms.Add(parmRowsAffected);
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_SessionActivityLog_Ins_BySessionID", parms);
                if (bolRetVal)
                {
                    RowsReturned = (int)parmRowsAffected.Value;
                }
                else
                {
                    RowsReturned = 0;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertSessionActivityLog(Guid SessionID, string ModuleName, ActivityCodes ActivityCode, Guid OnlineImageQueueID, int itemCount, int BankID, int ClientAccountID, out int RowsAffected)");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        /// <summary>
        /// Sets the web delivered flag.
        /// </summary>
        /// <param name="OnlineImageQueueID">The online image queue ID.</param>
        /// <param name="InternetDelivered">if set to <c>true</c> [internet delivered].</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool SetWebDeliveredFlag(Guid OnlineImageQueueID, bool InternetDelivered)
        {
            bool bolRetVal = false;
            int RowsAffected = 0;
            SqlParameter[] parms;
            SqlParameter parmRowsReturned;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmOnlineImageQueueID", SqlDbType.UniqueIdentifier, OnlineImageQueueID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmInternetDelivered", SqlDbType.Bit, InternetDelivered, ParameterDirection.Input));
                parmRowsReturned = (BuildParameter("@parmRowsReturned", SqlDbType.Int, RowsAffected, ParameterDirection.Output));
                arParms.Add(parmRowsReturned);
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_SessionActivityLog_Upd_SetWebDeliveredFlag", parms);
                if (bolRetVal)
                {
                    RowsAffected = (int)parmRowsReturned.Value;
                }
                else
                {
                    RowsAffected = 0;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "SetWebDeliveredFlag(Guid OnlineImageQueueID, bool InternetDelivered)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Updates the activity lockbox.
        /// </summary>
        /// <param name="ActivityLogID">The activity log ID.</param>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="RowsAffected">The rows affected.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool UpdateActivityLockbox(long ActivityLogID, int BankID, int ClientAccountID, out int RowsAffected)
        {
            bool bolRetVal = false;
            RowsAffected = 0;
            SqlParameter[] parms;
            SqlParameter parmRowsReturned;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmActivityLogID", SqlDbType.BigInt, ActivityLogID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                parmRowsReturned = (BuildParameter("@parmRowsReturned", SqlDbType.Int, RowsAffected, ParameterDirection.Output));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_SessionActivityLog_Upd_ActivityClientAccount", parms);
                if (bolRetVal)
                {
                    RowsAffected = (int)parmRowsReturned.Value;
                }
                else
                {
                    RowsAffected = 0;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateActivityLockbox(Guid ActivityLogID, int BankID, int ClientAccountID, out int RowsAffected)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the next page counter.
        /// </summary>
        /// <param name="SessionID">The session ID.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetNextPageCounter(Guid SessionID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_GetNextPageCounter", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetNextPageCounter(Guid SessionID, out DataTable dt)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Inserts the session log.
        /// </summary>
        /// <param name="SessionID">The session ID.</param>
        /// <param name="NextPage">The next page.</param>
        /// <param name="PageTitle">The page title.</param>
        /// <param name="ScriptName">Name of the script.</param>
        /// <param name="QueryString">The query string.</param>
        /// <param name="RowsAffected">The rows affected.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool InsertSessionLog(Guid SessionID, int NextPage, string PageTitle, string ScriptName, string QueryString, out int RowsAffected)
        {
            bool bolRetVal = false;
            RowsAffected = 0;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmNextPage", SqlDbType.Int, NextPage, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPageName", SqlDbType.VarChar, PageTitle, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmScriptName", SqlDbType.VarChar, ScriptName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmFormFields", SqlDbType.VarChar, QueryString, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_SessionLog_Ins", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertSessionLog(Guid SessionID, int NextPage, string PageTitle, string ScriptName, string QueryString, out int RowsAffected)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Updates the session.
        /// </summary>
        /// <param name="NextPage">The next page.</param>
        /// <param name="SessionID">The session ID.</param>
        /// <param name="RowsAffected">The rows affected.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool UpdateSession(int NextPage, Guid SessionID, out int RowsAffected)
        {
            bool bolRetVal = false;
            RowsAffected = 0;
            SqlParameter[] parms;
            SqlParameter parmRowsReturned;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmNextPage", SqlDbType.Int, NextPage, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                parmRowsReturned = (BuildParameter("@parmRowsReturned", SqlDbType.Int, RowsAffected, ParameterDirection.Output));
                arParms.Add(parmRowsReturned);
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_Session_UpdPageCounter", parms);
                if (bolRetVal)
                {
                    RowsAffected = (int)parmRowsReturned.Value;
                }
                else
                {
                    RowsAffected = 0;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateSession(int NextPage, Guid SessionID, out int RowsAffected)");
            }

            return bolRetVal;
        }

        //******************************
        // Session
        //******************************
        /// <summary>
        /// Gets the session.
        /// </summary>
        /// <param name="SessionID">The session ID.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetSession(Guid SessionID, out DataTable dt)
        {
            bool bolRetVal = false;
            DataTable tempdt;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_Session_Get_GetSession", parms, out tempdt);

                // remove after business layer is updated
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetSession(Guid SessionID, out DataTable dt)");
                dt = null;
            }

            return bolRetVal;
        }
        

        /// <summary>
        /// Gets the session bread crumbs, not including the end of the breadcrumb chain.
        /// Returns null on error.
        /// </summary>
        public IReadOnlyList<BreadCrumb> GetSessionBreadCrumbs(Guid SessionID)
        {
            try
            {
                var parms = new[] {
                    BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)
                };
                DataTable dataTable = null;
                var success = Database.executeProcedure("RecHubUser.usp_Session_getBreadcrumbs", parms, out dataTable);

                if (success && dataTable != null)
                {
                    var allBreadCrumbs =
                        from dataRow in dataTable.Rows.Cast<DataRow>()
                        let scriptName = dataRow["ScriptName"].ToString()
                        let pageCounter = (int)dataRow["PageCounter"]
                        select new BreadCrumb(scriptName, pageCounter);
                    var breadCrumbs = TakeUntilBottomOfBreadCrumbChain(allBreadCrumbs);

                    return breadCrumbs.ToList();
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetSessionBreadCrumbs(Guid SessionID, out DataTable dt)");
            }

            // Unsuccessful
            return null;
        }
        private bool IsBottomOfBreadCrumbChain(BreadCrumb breadCrumb)
        {
            var normalizedScriptName = breadCrumb.ScriptName.ToLower().Trim();
            return
                normalizedScriptName == "batchsummary.aspx" ||
                normalizedScriptName == "remitsearch.aspx" ||
                normalizedScriptName == "lockboxsearch.aspx";
        }
        private IEnumerable<BreadCrumb> TakeUntilBottomOfBreadCrumbChain(IEnumerable<BreadCrumb> breadCrumbs)
        {
            foreach (var breadCrumb in breadCrumbs)
            {
                yield return breadCrumb;
                if (IsBottomOfBreadCrumbChain(breadCrumb))
                {
                    yield break;
                }
            }
        }

        /// <summary>
        /// Ends a session in the database.
        /// (Sets the IsEnded flag to true)
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool EndSession(Guid SessionID, out DataTable dt)
        {
            bool bolRetVal = false;
            DataTable tempdt;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_Session_Upd_EndSession", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "EndSession(Guid SessionID, out DataTable dt)");
                dt = null;
            }

            return bolRetVal;
        }

        //private cSessionDAL GetSessionDAL()
        //{
        //    if (this.ConnType == ConnectionType.RecHubData)
        //        return new cSessionDAL(SiteKey);
        //    else
        //        return new cSessionDAL(SiteKey, this.ConnType);
        //}
    }
}
