﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     06/17/2013
*
* Purpose:  
*
* Modification History
* WI 105570 JMC 06/17/2013 
*   -New object used to translate new RecHub field names to legacy OLTA names.
* WI 149481 TWE 06/23/2014
*    Remove OLOrganizationID from user object
******************************************************************************/
namespace WFS.RecHub.Common {

    public static class LegacyDALConvert {

        /// <summary>
        /// Converts the data table, by replacing the .
        /// </summary>
        /// <param name="dt">The data table.</param>
        /// <returns></returns>
        public static DataTable ConvertDataTable(ref DataTable dt) {
            if(dt != null) {
                foreach(DataColumn column in dt.Columns) {
                    switch(column.ColumnName.ToLower()) {
                        case "olclientaccount":
                            column.ColumnName = "OLLockbox";
                            break;
                        case "olclientaccountid":
                            column.ColumnName = "OLLockboxID";
                            break;
                        case "siteclientaccountid":
                            column.ColumnName = "SiteLockboxID";
                            break;
                        case "siteclientaccountkey":
                            column.ColumnName = "SiteLockboxKey";
                            break;
                        case "clientaccountname":
                            column.ColumnName = "LockboxName";
                            break;
                        case "clientaccountcutoff":
                            column.ColumnName = "LockboxCutoff";
                            break;
                        case "olorganization":
                            column.ColumnName = "OLCustomer";
                            break;
                        //case "olorganizationid":
                        //    column.ColumnName = "OLCustomerID";
                        //    break;
                        case "olorganizationcode":
                            column.ColumnName = "OLCustomerCode";
                            break;
                        case "organizationid":
                            column.ColumnName = "CustomerID";
                            break;
                        case "clientaccountid":
                            column.ColumnName = "LockboxID";
                            break;
                        case "organizationcode":
                            column.ColumnName = "CustomerCode";
                            break;
                        case "immutabledatekey":
                            column.ColumnName = "ProcessingDateKey";
                            break;
                        case "immutabledate":
                            column.ColumnName = "ProcessingDate";
                            break;
                    }
                }
            }

            return dt;
        }
    }
}
