﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ItemProcDAL {
    public class WorkgroupDTO {
        public int BankID { get; set; }

        public int ClientAccountID { get; set; }
        public string FileGroup { get; set; }
    }
}
