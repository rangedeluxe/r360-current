using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.Common.Database;
using WFS.RecHub.ItemProcDAL;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     01/26/2009
*
* Purpose:  
*
* Modification History
* CR 28738 JMC 05/03/2010
*   -Added GetDestLockboxes function.
* CR 31140 JMC 10/07/2010
*   -Add status as a parameter to call to SQL_UpdateIMSInterfaceQueueStatus
*   -Add method to call SQL_UpdateIMSInterfaceQueueStatusInUse.
* CR 32576 JMC 02/01/2011
*   -Modified definition of UpdateFactCheckDocumentsXML and 
*    UpdateFactChecksXML.
* CR 32576 WJS 02/09/2011 
*   -Add methods GetKeysForBatch
* CR 31587 JMC 02/16/2011 
*   -Moved IMS specific methods to IMSIntegrationDAL
* CR 32562 JMC 03/02/2011 
*   -Removed old conditional compilation code.
*   -Added GetCheck() and GetDocument() methods.
* CR 31536 WJS 03/09/2011
*   -Added methods to handle factItemData, factBatchData, dimItemDataSetupFields and dimBatchDataSetupFields
* CR 33541 JMC 04/09/2011
*   -Added methods GetBatch(), GetCheck2(), and GetStub()
* CR 34080 WJS 05/02/2011
*   -Added ability to call xml via stored proc in InsertICONClientSetup, InsertICONBatchImport and InsertICONBatchImport rather then with string.
* CR 34099 JMC 09/08/2011
*   -Added GetLockboxSummary() method.
* CR 30305 WJS 10/4/2011
 *  -Added UpdateDocTypeDescripton() method, modified GetDocumentTypes() to called stored procedure
* CR 48016 WJS 11/8/2011
*	-Allow GetImageInfo to take DepositDate for support of GetAllImages
*	-Add method GetBatchChecks and GetBatchDocuments.
* CR 48098 WJS 11/29/2011
*   -Remove GetImageInfo without depositDateKey
* CR 50158 JMC 03/15/2012
*   -Added overloaded GetOLCustomer() method that will lookup an OLCustomer 
*    by CustomerCode.
* CR 52261 WJS 5/21/2012
*   -Remove GetImageRPSAlias Mapping call and  UpsertImageRPSAliasKeyword call
* CR 52276 JNE 03/15/2012
*   -Added Getlockbox() method  
* CR 52661 WJS 7/31/2012
 *  -Add back UpsertImageRPSAliasKeyword
 * CR 56144 CEJ 11/19/2012 
*   -Modify the Batch Details page to expect pagination within the stored 
*    procedure
* WI 70465 WJS 11/27/2012
*   -Remove usp_ICONBatchNotifyOLTAServiceBroker and usp_ICONBatchNotifyOLTAServiceBroker from the DAL
* WI 72171 TWE 12/31/2012
*   -FP 2.00 - Convert cItemProcDAL.GetImageDisplayModeForBatch(Guid, int, int, int, DateTime, out DataTable) 
*         to a  stored procedure call
* WI 72172 TWE 12/31/2012
*   -FP 2.00 - Convert cItemProcDAL.GetImageDisplayModeForLockbox(Guid, Guid, out DataTable) 
*         to a  stored procedure call
* WI 72181 CRG 03/08/2013
*   -Convert cItemProcDAL.UpdateJobStatusError(Guid, string) to a stored procedure call
* WI 104475 CRG 06/06/2013
*   -Update ItemProcDal for usp_GetCheckImageInfo and  usp_GetDocumentImageInfo
* WI 105872 JMC 06/17/2013
*   -Modified Online to call new LegacyDALConvert object so as to include new fields.
* WI 110341 CEJ 07/30/2013
*   -Remove usages of DALLib.SQLEncodeString in all the DALs
* WI 109747 EAS 07/30/2013
*   -DAL components called with RecHubAdmin Connection Type 
* WI 116721 EAS 10/10/2013
*   -Add Payment Type ID as a filter to Totals requests 
* WI 113598 MLH 10/14/2013
*   -Changed GetDocumentTypes to pass in MostRecent = 1
* WI 143262 SAS 05/21/2014
*   -Changes done to change the datatype of BatchID from int to long
* WI 148949 DJW 6/19/2013
*   -Added SessionID to methods for sprocs that needed it (related to RAAM)
* WI 149481 TWE 06/23/2014
*   -Remove OLOrganizationID from user object
* WI 150507 TWE 06/26/2014
*   -Fix Batch Summary and initial Payment Search page display
* WI 151989 TWE 07/07/2014
*   -Fix IPOnline to work with new Batch ID and RAAM
* WI 156739 SAS 08/01/2014
*   -Changes done to pass correct SqlDB type for BatchSequence
* WI 159407 SAS 08/18/2014
*   -Changes done to add new call to SP GetPaymentType 
* WI 157074 JMC 08/20/2014
*   -Modified call to usp_OLClientAccounts_Get to use int OLWorkgroupID
* WI 166038 SAS 09/16/2014
*   -New function added for calling ImageRPS Stored procedures
* WI 172885 SAS 10/17/2014
*   -Changes done to convert return data for Report Stored procedures
******************************************************************************/
namespace WFS.RecHub.DAL {

	public class cItemProcDAL : _DALBase {

		/// <summary>
		/// Initializes a new instance of the <see cref="cItemProcDAL"/> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
		public cItemProcDAL(string vSiteKey)
			: base(vSiteKey) {

		}

		public cItemProcDAL(string vSiteKey, ConnectionType enmConnection)
			: base(vSiteKey, enmConnection) { }

		/// <summary>
		/// Gets the image info.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="ImmutableDateKey">The immutable date key.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="DepositDateKey">The deposit date key.</param>
		/// <param name="TableType">Type of the table.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, TableTypes TableType, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			try {
				using(cItemProcDAL objDAL = new cItemProcDAL(this.SiteKey)) {
					switch(TableType) {
						case TableTypes.Checks:
							bolRetVal = objDAL.GetCheckImageInfo(SessionID, BankID, ClientAccountID, ImmutableDateKey, BatchID, BatchSequence, DepositDateKey, out dt);
							break;
						case TableTypes.Documents:
                            bolRetVal = objDAL.GetDocumentImageInfo(SessionID, BankID, ClientAccountID, ImmutableDateKey, BatchID, BatchSequence, DepositDateKey, out dt);
							break;
						default:
							break;
					}
				};
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, ex.Source);
				return false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the check image info.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="ImmutableDateKey">The immutable date key.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="DepositDateKey">The deposit date key.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetCheckImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, out DataTable dt) {
			bool bolRetVal = false;
			dt = new DataTable();
			try {
				SqlParameter[] parms;
				List<SqlParameter> arParms;
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDateKey, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factCheckImages_Get", parms, out dt);
			} catch(System.Exception ex) {
				EventLog.logEvent("An error occurred while executing GetImageInfo: " + ex.Message,
								  this.GetType().Name,
								  MessageType.Error,
								  MessageImportance.Essential);
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the document image info.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="ImmutableDateKey">The immutable date key.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="DepositDateKey">The deposit date key.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetDocumentImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, out DataTable dt) {
			bool bolRetVal = false;
			dt = new DataTable();
			try {
				SqlParameter[] parms;
				List<SqlParameter> arParms;
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDateKey, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factDocumentImages_Get", parms, out dt);
			} catch(System.Exception ex) {
				EventLog.logEvent("An error occurred while executing GetImageInfo: " + ex.Message,
								  this.GetType().Name,
								  MessageType.Error,
								  MessageImportance.Essential);
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the OL customer.
		/// </summary>
		/// <param name="OLOrganizationID">The OL organization ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetOLCustomer(Guid OLOrganizationID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmOLOrganizationID", SqlDbType.UniqueIdentifier, OLOrganizationID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_OLOrganizations_Get_ByOLOrganizationID", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetOLCustomer(Guid OLOrganizationID, out DataTable dt)");
			}

			return bolRetVal;
		}


		/// <summary>
		/// Gets the OL customer.
		/// </summary>
		/// <param name="OrganizationCode">The organization code.</param>
		/// <param name="dtOLOrganization">The dt OL organization.</param>
		/// <returns></returns>
		public bool GetOLCustomer(string OrganizationCode, out DataTable dtOLOrganization) {
			bool bolRetVal = false;
			dtOLOrganization = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			SqlParameter parmRowsReturned;
			SqlParameter parmErrorDescription;
			SqlParameter parmSQLErrorNumber;
			List<SqlParameter> arParms;
			const string PROCNAME = "RecHubUser.usp_API_OLOrganizations_Get_ByOrganizationCode";
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmOrganizationCode", SqlDbType.VarChar, 20, OrganizationCode));
				parmRowsReturned = BuildParameter("@parmRowsReturned", SqlDbType.Int, 0, ParameterDirection.Output);
				arParms.Add(parmRowsReturned);
				parmErrorDescription = BuildParameter("@parmErrorDescription", SqlDbType.VarChar, string.Empty, ParameterDirection.Output);
				arParms.Add(parmErrorDescription);
				parmSQLErrorNumber = BuildParameter("@parmSQLErrorNumber", SqlDbType.Int, 0, ParameterDirection.Output);
				arParms.Add(parmSQLErrorNumber);
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, parms, out tempdt);
				dtOLOrganization = DALLib.ConvertDataTable(ref tempdt);
				if(parmSQLErrorNumber == null || (int) parmSQLErrorNumber.Value != (int) 0) {
					if(parmErrorDescription == null) {
						EventLog.logEvent("An error occurred while executing " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
					} else {
						EventLog.logEvent("An error occurred while executing " + PROCNAME + ": " + parmErrorDescription.Value, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
					}

					dtOLOrganization = null;
				} else {
					switch(tempdt.Rows.Count) {
						case 0:
							EventLog.logEvent("An error occurred while executing " + PROCNAME + ": No rows found for " + OrganizationCode, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
							dtOLOrganization = null;
							break;
						case 1:
							bolRetVal = true;
							break;
						default:
							EventLog.logEvent("An error occurred while executing " + PROCNAME + ": Multiple rows found for " + OrganizationCode, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
							dtOLOrganization = null;
							break;
					}
				}
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetOLCustomer(string OrganizationCode, out DataTable dtOLOrganization)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the OL lockbox.
		/// </summary>
		/// <param name="OLClientAccountID">The OL client account ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetOLLockbox(int OLWorkgroupID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmOLWorkgroupID", SqlDbType.Int, OLWorkgroupID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_OLClientAccounts_Get", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "GetOLLockbox(int OLWorkgroupID, out DataTable dt)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Lookups the name of the customer.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="OrganizationID">The organization ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool LookupCustomerName(int BankID, int OrganizationID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteOrganizationID", SqlDbType.Int, OrganizationID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_dimOrganizationView_Get_Name", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "LookupCustomerName(int BankID, int OrganizationID, out DataTable dt)");
			}

			return bolRetVal;
		}

        /// <summary>
        /// Gets the lockbox by ID.
        /// </summary>
        /// <param name="SessionUser">The session user.</param>
        /// <param name="OLClientAccountID">The OL client account ID.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetLockboxByID(cUserRAAM SessionUser, Guid OLClientAccountID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                //arParms.Add(BuildParameter("@parmOLOrganizationID", SqlDbType.UniqueIdentifier, SessionUser.OLCustomerID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmOLClientAccountID", SqlDbType.UniqueIdentifier, OLClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, SessionUser.UserID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_dimClientAccountsView_Get_ByOLOrganization", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetLockboxByID(cUser SessionUser, Guid OLClientAccountID, out DataTable dt)");
            }

            return bolRetVal;
        }

		/// <summary>
		/// Gets the lockbox by lockbox ID.
		/// </summary>
		/// <param name="SessionUser">The session user.</param>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetLockboxByLockboxID(cUserRAAM SessionUser, int BankID, int ClientAccountID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				//arParms.Add(BuildParameter("@parmOLOrganizationID", SqlDbType.UniqueIdentifier, SessionUser.OLCustomerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, SessionUser.UserID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_BySiteBankIDClientAccountID", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetLockboxByLockboxID(cUser SessionUser, int BankID, int ClientAccountID, out DataTable dt)");
			}
			return bolRetVal;
		}

        /// <summary>
        /// Gets the lockbox by Bank ID and Client account ID.
        /// </summary>
        /// <param name="SessionID">The user's session.</param>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetLockBoxByAccountIDBankID(Guid sessionID, int BankID, int ClientAccountID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_SessionClientAccountEntitlements_Get_ByWorkgroup", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetLockBoxByAccountIDBankID(Guid sessionID, int BankID, int ClientAccountID, out DataTable dt)");
            }
            return bolRetVal;
        }

		/// <summary>
		/// Gets the dest lockboxes.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		//public bool GetDestLockboxes(int BankID, int ClientAccountID, out DataTable dt) {
		//	bool rtnval = false;
		//	dt = new DataTable();
		//	SqlParameter[] parms;
		//	List<SqlParameter> arParms;
		//	try {
		//		arParms = new List<SqlParameter>();
		//		arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
		//		arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
		//		parms = arParms.ToArray();
		//		rtnval = Database.executeProcedure("RecHubData.usp_CommingledLockbox_GetDestLockboxes", parms, out dt);
		//	} catch(Exception ex) {
		//		EventLog.logError(ex, this.GetType().Name, "GetDestLockboxes");
		//	}

		//	return rtnval;
		//}

		/// <summary>
		/// Gets the batch lockbox.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		//public bool GetBatchLockbox(int BankID, int LockboxID, long BatchID, DateTime DepositDate, out DataTable dt) {
		//	bool bolRetVal = false;
		//	dt = null;
		//	SqlParameter[] parms;
		//	List<SqlParameter> arParms;
		//	try {
		//		arParms = new List<SqlParameter>();
		//		arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
		//		arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
		//		arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
		//		arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
		//		parms = arParms.ToArray();
		//		bolRetVal = Database.executeProcedure("RecHubData.usp_GetLockboxByLockboxID", parms, out dt);
		//	} catch(Exception ex) {
		//		EventLog.logError(ex, this.GetType().Name, "GetBatchLockbox");
		//	}
		//	return bolRetVal;
		//}

		/// <summary>
		/// Gets the lockbox total.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetLockboxTotal(Guid SessionID, int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate, int? PaymentTypeID, int? PaymentSourceID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateStart", SqlDbType.DateTime, StartDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateEnd", SqlDbType.DateTime, EndDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentTypeKey", SqlDbType.Int, PaymentTypeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentSourceKey", SqlDbType.Int, PaymentSourceID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_ClientAccountTotal", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetLockboxTotal(Guid SessionID, int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate, out DataTable dt)");
			}
			return bolRetVal;
		}

		/// <summary>
		/// Gets the batch count.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetBatchCount(Guid SessionID, int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate, int? PaymentTypeID, int? PaymentSourceID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateStart", SqlDbType.DateTime, StartDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateEnd", SqlDbType.DateTime, EndDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentTypeKey", SqlDbType.Int, PaymentTypeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentSourceKey", SqlDbType.Int, PaymentSourceID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_GetBatchCount", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetBatchCount");
			}
			return bolRetVal;
		}


		/// <summary>
		/// Gets the batch.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="ImmutableDateKey">The immutable date key.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetBatch(int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_BySiteBankIDSiteClientAccountID", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetBatch");
				bolRetVal = false;
			}
			
			return bolRetVal;
		}

        /// <summary>
        /// Gets the batch summary.
        /// </summary>
        /// <param name="SessionID">The session ID.</param>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="StartDate">The start date.</param>
        /// <param name="EndDate">The end date.</param>
        /// <param name="paymentTypeID">The payment type ID.</param>
        /// <param name="DisplayScannedCheckOnline">if set to <c>true</c> [display scanned check online].</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetBatchSummary(Guid SessionID, int BankID, int ClientAccountID, DateTime StartDate,
            DateTime EndDate, int? paymentTypeID, int? paymentSourceID, bool DisplayScannedCheckOnline,
            int start, int length, string orderby, string orderdirection, string search, out int recordstotal,
            out int recordsfiltered, out DataTable dt, out int transactionRecords, out int checkRecords,
            out int documentRecords, out decimal checkTotal)
        {
			bool bolRetVal = false;
			dt = null;
            transactionRecords = 0;
            checkRecords = 0;
            documentRecords = 0;
            checkTotal = 0.0M;

            var totalrecords = 0;
            var filteredrecords = 0;

            if (search == null)
                search = string.Empty;

			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateStart", SqlDbType.DateTime, StartDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateEnd", SqlDbType.DateTime, EndDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentTypeID", SqlDbType.Int, paymentTypeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentSourceID", SqlDbType.Int, paymentSourceID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Int, (DisplayScannedCheckOnline ? 1 : 0), ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmStart", SqlDbType.Int, start, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmLength", SqlDbType.Int, length, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmOrderBy", SqlDbType.VarChar, orderby, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmOrderDirection", SqlDbType.VarChar, orderdirection, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSearch", SqlDbType.VarChar, search, ParameterDirection.Input));
                var records = BuildParameter("@parmRecordsTotal", SqlDbType.Int, totalrecords, ParameterDirection.Output);
                var filtered = BuildParameter("@parmRecordsFiltered", SqlDbType.Int, filteredrecords, ParameterDirection.Output);
                var transactions = BuildParameter("@parmTransactionRecords", SqlDbType.Int, transactionRecords, ParameterDirection.Output);
                var checks = BuildParameter("@parmCheckRecords", SqlDbType.Int, checkRecords, ParameterDirection.Output);
                var documents = BuildParameter("@parmDocumentRecords", SqlDbType.Int, documentRecords, ParameterDirection.Output);
                var checkAmount = BuildParameter("@parmCheckAmount", SqlDbType.Money, checkTotal, ParameterDirection.Output);
                arParms.Add(records);
                arParms.Add(filtered);
                arParms.Add(transactions);
                arParms.Add(checks);
                arParms.Add(documents);
                arParms.Add(checkAmount);
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get", parms, out dt);
                filteredrecords = dt.Rows.Count;
                totalrecords = (int)records.Value;
                filteredrecords = (int)filtered.Value;
                transactionRecords = (int) transactions.Value;
                checkRecords = (int) checks.Value;
                documentRecords = (int) documents.Value;
                checkTotal = (decimal) checkAmount.Value;

            } catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetBatchSummary");
				bolRetVal = false;
			}

            recordstotal = totalrecords;
            recordsfiltered = filteredrecords;
			return bolRetVal;
		}

		/// <summary>
		/// Gets the batch detail.
		/// </summary>
		/// <param name="OLClientAccountID">The OL client account ID.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="DisplayScannedCheckOnline">if set to <c>true</c> [display scanned check online].</param>
		/// <param name="StartRecord">The start record.</param>
		/// <param name="RecordsPerPage">The records per page.</param>
		/// <param name="TotalRecords">The total records.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetBatchDetail(Guid SessionID, long BatchID, DateTime DepositDate, bool DisplayScannedCheckOnline, int StartRecord, int RecordsPerPage, string OrderBy, string OrderDirection, string parmSearch, out int TotalRecords, out int FilteredRecords, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			TotalRecords = 0;
			FilteredRecords = 0;
			DataTable tempdt = null;
			SqlParameter parmTotalRecords;
			SqlParameter parmFilteredRecords;
			try {
				List<SqlParameter> lstParms = new List<SqlParameter>();
				SqlParameter[] arParms;
                lstParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				//lstParms.Add(BuildParameter("@parmOLClientAccountID", SqlDbType.UniqueIdentifier, OLClientAccountID, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, DisplayScannedCheckOnline, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmStartRecord", SqlDbType.Int, StartRecord, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmRecordsPerPage", SqlDbType.Int, RecordsPerPage, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmOrderBy", SqlDbType.VarChar, OrderBy, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmOrderDirection", SqlDbType.VarChar , OrderDirection, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmSearch", SqlDbType.VarChar, parmSearch, ParameterDirection.Input));
				parmTotalRecords = BuildParameter("@parmTotalRecords", SqlDbType.Int, TotalRecords, ParameterDirection.Output);
				lstParms.Add(parmTotalRecords);
				parmFilteredRecords = BuildParameter("@parmFilteredRecords", SqlDbType.Int, FilteredRecords, ParameterDirection.Output);
				lstParms.Add(parmFilteredRecords);
				arParms = lstParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_BatchDetail", arParms, out tempdt);
				if(bolRetVal || parmTotalRecords.Value != null) {
					dt = DALLib.ConvertDataTable(ref tempdt);
					TotalRecords = (int) parmTotalRecords.Value;
					FilteredRecords = (int)parmFilteredRecords.Value;
				} else {
					TotalRecords = 0;
				}
			} catch(System.Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetBatchDetail(Guid SessionID, Guid OLClientAccountID, long BatchID, DateTime DepositDate, bool DisplayScannedCheckOnline, int StartRecord, int RecordsPerPage, out int TotalRecords, out DataTable dt): {0}");
				dt = null;
				TotalRecords = 0;
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the data entry fields.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetDataEntryFields(Guid SessionID, int BankID, int ClientAccountID, long BatchID, DateTime DepositDate, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factDataEntryDetails_Get_DEColumns", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetDataEntryFields(int BankID, int ClientAccountID, long BatchID, DateTime DepositDate, out DataTable dt)");
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the transaction count.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetTransactionCount(Guid SessionID, int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate, int? PaymentTypeID, int? PaymentSourceID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateStart", SqlDbType.DateTime, StartDate, ParameterDirection.Input));
				if(EndDate > DateTime.MinValue) {
					arParms.Add(BuildParameter("@parmDepositDateEnd", SqlDbType.DateTime, EndDate, ParameterDirection.Input));
				} else {
					arParms.Add(BuildParameter("@parmDepositDateEnd", SqlDbType.DateTime, StartDate, ParameterDirection.Input));
				}
                arParms.Add(BuildParameter("@parmPaymentTypeKey", SqlDbType.Int, PaymentTypeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentSourceKey", SqlDbType.Int, PaymentSourceID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_TransactionCount", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionCount");
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the batch transactions.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
        public bool GetBatchTransactions(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, out DataTable dt)
        {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factTransactionSummary_Get", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetBatchTransactions(int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, out DataTable dt)");
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the transaction details.
		/// </summary>
		/// <param name="OLOrganizationID">The OL organization ID.</param>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="TransactionID">The transaction ID.</param>
		/// <param name="IncludeScannedCheck">if set to <c>true</c> [include scanned check].</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetTransactionDetails(Guid SessionID, int BankID, int ClientAccountID, long BatchID, DateTime DepositDate, int TransactionID, bool IncludeScannedCheck, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
                //arParms.Add(BuildParameter("@parmOLOrganizationID", SqlDbType.UniqueIdentifier, OLOrganizationID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));

				// This does not appear to be used and should be removed from use. It is not implemented in the database
				// arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, IncludeScannedCheck, ParameterDirection.Input));
				
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factTransactionSummary_Get_ByOLOrganizationID", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionDetails");
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the transaction details by sequence.
		/// </summary>
		/// <param name="OLOrganizationID">The OL organization ID.</param>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="Sequence">The sequence.</param>
		/// <param name="IncludeScannedCheck">if set to <c>true</c> [include scanned check].</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetTransactionDetailsBySequence(Guid SessionID, int BankID, int ClientAccountID, long BatchID, DateTime DepositDate, int Sequence, bool IncludeScannedCheck, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                //arParms.Add(BuildParameter("@parmOLOrganizationID", SqlDbType.UniqueIdentifier, OLOrganizationID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTxnSequence", SqlDbType.Int, Sequence, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, IncludeScannedCheck, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factTransactionSummary_Get_ByTxnSequence", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionDetailsBySequence(Guid SessionID, Guid OLOrganizationID, int BankID, int ClientAccountID, long BatchID, DateTime DepositDate, int Sequence, bool IncludeScannedCheck, out DataTable dt)");
			}
			return bolRetVal;
		}

		/// <summary>
		/// Gets the check count.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
        public bool GetCheckCount(Guid SessionID, int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate, int? PaymentTypeID, int? PaymentSourceID, out DataTable dt)
        {
			bool bolRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateStart", SqlDbType.DateTime, StartDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateEnd", SqlDbType.DateTime, EndDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentTypeKey", SqlDbType.Int, PaymentTypeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentSourceKey", SqlDbType.Int, PaymentSourceID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_GetCheckCount", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetCheckCount");
			}
			return bolRetVal;
		}

		/// <summary>
		/// Gets the transaction checks.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="TransactionID">The transaction ID.</param>
		/// <param name="dt">The data table.</param>
		/// <returns></returns>
		public bool GetTransactionChecks(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, int TransactionID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_ByTransactionID", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionChecks(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, int TransactionID, out DataTable dt)");
			}
			return bolRetVal;
		}
		/// <summary>
		/// Gets the batch checks.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="SiteClientAccountID">The site client account ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="dt">The data table.</param>
		/// <returns></returns>
		public bool GetBatchChecks(Guid SessionID,int BankID, int SiteClientAccountID, DateTime DepositDate, long BatchID, out DataTable dt) {
			bool bolRetVal;
			dt = null;
			DataTable tempdt = null;
			try {
				SqlParameter[] parms;
				List<SqlParameter> arParms;
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, SiteClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_BatchChecks", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(System.Exception ex) {
				EventLog.logEvent("An error occurred while executing RecHubData.usp_factChecks_Get_BatchChecks: " + ex.Message, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the batch documents.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="ScannedCheck">if set to <c>true</c> [scanned check].</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetBatchDocuments(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, bool ScannedCheck, out DataTable dt) {
			bool bolRetVal;
			dt = new DataTable();
			try {
				SqlParameter[] parms;
				List<SqlParameter> arParms;
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Int, ScannedCheck, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factDocuments_Get", parms, out dt);
			} catch(System.Exception ex) {
				EventLog.logEvent("An error occurred while executing GetBatchDocuments: " + ex.Message, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the transaction checks by sequence.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="CleintAccountID">The cleint account ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="Sequence">The sequence.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetTransactionChecksBySequence(Guid SessionID, int BankID, int CleintAccountID, DateTime DepositDate, long BatchID, int Sequence, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, CleintAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTxnSequence", SqlDbType.Int, Sequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_ByTxnSequence", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionChecksBySequence(Guid SessionID, int BankID, int CleintAccountID, DateTime DepositDate, long BatchID, int Sequence, out DataTable dt)");
			}
			return bolRetVal;
		}

		/// <summary>
		/// Gets the check.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="CleintAccountID">The cleint account ID.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="TransactionID">The transaction ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetCheck(Guid SessionID, int BankID, int CleintAccountID, long BatchID, DateTime DepositDate, int TransactionID, int BatchSequence, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, CleintAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_ByDepositDate", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetCheck");
			}
			 
			return bolRetVal;
		}

		/// <summary>
		/// Gets the transaction checks DE.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="TransactionID">The transaction ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetTransactionChecksDE(Guid SessionID, int BankID, int ClientAccountID, long BatchID, DateTime DepositDate, int TransactionID, int BatchSequence, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factDataEntryDetails_Get_ChecksDE", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionChecksDE(Guid SessionID, int BankID, int ClientAccountID, long BatchID, DateTime DepositDate, int TransactionID, int BatchSequence, string CheckDEFieldList, out DataTable dt)");
			}
		
			return bolRetVal;
		}

		/// <summary>
		/// Gets the transaction stubs.
		/// </summary>
		/// <param name="Transaction">The transaction.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetTransactionStubs(Guid SessionID, cTransaction Transaction, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, Transaction.BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, Transaction.LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, Transaction.DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, Transaction.BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, Transaction.TransactionID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factStubs_Get_ByTransactionID", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionStubs(cTransaction Transaction, out DataTable dt)");
			}

			return bolRetVal;
		}

        /// <summary>
		/// Gets the transaction stubs.
		/// </summary>
		/// <param name="Transaction">The transaction.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetTransactionStubsBySequence(Guid SessionID, cTransaction Transaction, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, Transaction.BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, Transaction.LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, Transaction.DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, Transaction.BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTxnSequence", SqlDbType.Int, Transaction.TxnSequence, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factStubs_Get_ByTxnSequence", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetTransactionStubsBySequence(cTransaction Transaction, out DataTable dt)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the transaction stubs DE.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="DepositDate">The deposit date.</param>
        /// <param name="TransactionID">The transaction ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetTransactionStubsDE(Guid SessionID, int BankID, int ClientAccountID, long BatchID, DateTime DepositDate, int TransactionID, int BatchSequence, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factDataEntryDetails_Get_StubsDE", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionStubsDE");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the document count.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="IncludeScannedCheck">if set to <c>true</c> [include scanned check].</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetDocumentCount(Guid SessionID, int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate, bool IncludeScannedCheck, int? PaymentTypeID, int? PaymentSourceID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateStart", SqlDbType.DateTime, StartDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateEnd", SqlDbType.DateTime, EndDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, IncludeScannedCheck, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentTypeKey", SqlDbType.Int, PaymentTypeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPaymentSourceKey", SqlDbType.Int, PaymentSourceID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_DocumentCount", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetDocumentCount");
			}
			return bolRetVal;
		}

		/// <summary>
		/// Gets the transaction documents.
		/// </summary>
		/// <param name="Transaction">The transaction.</param>
		/// <param name="IncludeScannedCheck">if set to <c>true</c> [include scanned check].</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetTransactionDocuments(Guid SessionID, cTransaction Transaction, bool IncludeScannedCheck, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, Transaction.BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, Transaction.LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, Transaction.BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, Transaction.DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.VarChar, Transaction.TransactionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, IncludeScannedCheck, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factDocuments_Get_ByTransactionID", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionDocuments");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the transaction documents by sequence.
		/// </summary>
		/// <param name="Transaction">The transaction.</param>
		/// <param name="IncludeScannedCheck">if set to <c>true</c> [include scanned check].</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetTransactionDocumentsBySequence(Guid SessionID, cTransaction Transaction, bool IncludeScannedCheck, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, Transaction.BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, Transaction.LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, Transaction.BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, Transaction.DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTxnSequence", SqlDbType.VarChar, Transaction.TxnSequence, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, IncludeScannedCheck, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factDocuments_Get_ByTxnSequence", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetTransactionDocumentsBySequence");
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the document.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="TransactionID">The transaction ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="DisplayScannedCheck">if set to <c>true</c> [display scanned check].</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetDocument(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, int TransactionID, int BatchSequence, bool DisplayScannedCheck, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, DisplayScannedCheck, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_Document", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetDocument(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, int TransactionID, int BatchSequence, bool DisplayScannedCheck, out DataTable dt)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Method used to get the keys to be used in later update of factChecks/factDocuments
		/// </summary>
		/// <param name="BankID"></param>
		/// <param name="ClientAccountID"></param>
		/// <param name="ImmutableDate"></param>
		/// <param name="BatchID"></param>
		/// <param name="BankKey"></param>
		/// <param name="LockboxKey"></param>
		/// <returns></returns>
		public bool GetKeysForBatch(Guid SessionID, int BankID, int ClientAccountKey, DateTime ImmutableDate, long BatchID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input)); 
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountKey", SqlDbType.Int, ClientAccountKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDate", SqlDbType.DateTime, ImmutableDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_Keys", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetKeysForBatch");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Does this fact item exist
		/// </summary>
		/// <param name="bankKey"></param>
		/// <param name="customerKey"></param>
		/// <param name="lockboxKey"></param>
		/// <param name="ImmutableDateKey"></param>
		/// <param name="depositDateKey"></param>
		/// <param name="batchID"></param>
		/// <param name="itemFound"></param>
		/// <returns></returns>
		public bool DoesFactItemDataExist(int bankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int batchSequence, out bool itemFound) {
			bool bolRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			SqlParameter parmItemFound;
			itemFound = false;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, batchSequence, ParameterDirection.Input));
				parmItemFound = BuildParameter("@parmItemFound", SqlDbType.Bit, itemFound, ParameterDirection.Output);
				arParms.Add(parmItemFound);
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factItemData_CheckExists", parms);
				if (bolRetVal) {
					if((bool) parmItemFound.Value) {
						itemFound = true;
					}
				}
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "DoesFactItemDataExist");
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the document types.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetDocumentTypes(out DataTable dt) {
			bool bolRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			dt = new DataTable();
			try {
				arParms = new List<SqlParameter>();
                // Always select MostRecent
				arParms.Add(BuildParameter("@parmMostRecent", SqlDbType.Bit, 1, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_dimDocumentTypes_Get", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetDocumentTypes(out DataTable dt)");
				bolRetVal = false;
			}

			return bolRetVal;
		}

		/// <summary>
		/// Updates the doc type description.
		/// </summary>
		/// <param name="strFileDescriptor">The STR file descriptor.</param>
		/// <param name="strDescription">The STR description.</param>
		/// <returns></returns>
		public bool UpdateDocTypeDescripton(string strFileDescriptor, string strDescription) {
			bool bolRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmFileDescriptor", SqlDbType.VarChar, strFileDescriptor, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDescription", SqlDbType.VarChar, strDescription, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_dimDocumentTypes_Upd_DocumentTypeDescription", parms);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "UpdateDocTypeDescripton");
				bolRetVal = false;
			}

			return bolRetVal;

		}

		/// <summary>
		/// Gets the type of the system.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="ImmutableDate">The processing date.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetSystemType(Guid SessionID, int BankID, int ClientAccountID, long BatchID, DateTime ImmutableDate, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDate", SqlDbType.DateTime, ImmutableDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_SystemType", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetSystemType");
				bolRetVal = false;
			}
			return bolRetVal;
		}


		/// <summary>
		/// Gets the online image queue.
		/// </summary>
		/// <param name="OnlineImageQueueID">The online image queue ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetOnlineImageQueue(Guid OnlineImageQueueID, out DataTable dt) {
			bool bolRetVal = false;
			dt = new DataTable();
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmOnlineImageQueueID", SqlDbType.UniqueIdentifier, OnlineImageQueueID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_OnlineImageQueue_Get", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetOnlineImageQueue(Guid OnlineImageQueueID, out DataTable dt)");
			}
			return bolRetVal;
		}

		/// <summary>
		/// Creates the online image queue.
		/// </summary>
		/// <param name="OnlineImageQueueID">The online image queue ID.</param>
		/// <param name="SessionUserID">The session user ID.</param>
		/// <param name="DisplayScannedCheck">if set to <c>true</c> [display scanned check].</param>
		/// <param name="RowsReturned">The rows affected.</param>
		/// <returns></returns>
		public bool CreateOnlineImageQueue(Guid OnlineImageQueueID, int SessionUserID, bool DisplayScannedCheck, out int RowsAffected) {
			bool bolRetVal = false;
			RowsAffected = 0;
			SqlParameter[] parms;
			SqlParameter[] parmsOutput;
			SqlParameter parmRowsAffected;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmOnlineImageQueueID", SqlDbType.UniqueIdentifier, OnlineImageQueueID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSessionUserID", SqlDbType.Int, SessionUserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserFileName", SqlDbType.VarChar, OnlineImageQueueID.ToString(), ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, DisplayScannedCheck, ParameterDirection.Input));
				parmRowsAffected = BuildParameter("@parmRowsAffected", SqlDbType.Int, 0, ParameterDirection.Output);
				arParms.Add(parmRowsAffected);
				parms = arParms.ToArray();
				parmsOutput = new List<SqlParameter>().ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_OnlineImageQueue_Ins", parms, out parmsOutput);

				if(bolRetVal) {
					RowsAffected = ((int) parmRowsAffected.Value);
					bolRetVal = true;
				} else {
					EventLog.logEvent("An error occurred while executing usp_OnlineImageQueue_Ins."
									, this.GetType().Name
									, MessageType.Error
									, MessageImportance.Essential);
				}
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "CreateOnlineImageQueue");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Creates the online image queue.
		/// </summary>
		/// <param name="OnlineImageQueueID">The online image queue ID.</param>
		/// <param name="SessionUserID">The session user ID.</param>
		/// <param name="UserFileName">Name of the user file.</param>
		/// <param name="DisplayScannedCheck">if set to <c>true</c> [display scanned check].</param>
		/// <param name="RowsReturned">The rows affected.</param>
		/// <returns></returns>
		public bool CreateOnlineImageQueue(Guid OnlineImageQueueID, int SessionUserID, string UserFileName, bool DisplayScannedCheck, out int RowsAffected) {
			bool bolRetVal = false;
			RowsAffected = 0;
			SqlParameter[] parms;
			SqlParameter[] parmsOutput;
			SqlParameter parmRowsAffected;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmOnlineImageQueueID", SqlDbType.UniqueIdentifier, OnlineImageQueueID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSessionUserID", SqlDbType.Int, SessionUserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserFileName", SqlDbType.VarChar, UserFileName, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, DisplayScannedCheck, ParameterDirection.Input));
				parmRowsAffected = BuildParameter("@parmRowsAffected", SqlDbType.Int, RowsAffected, ParameterDirection.Output);
				arParms.Add(parmRowsAffected);
				parms = arParms.ToArray();
				parmsOutput = new List<SqlParameter>().ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_OnlineImageQueue_Ins", parms, out parmsOutput);
				if(bolRetVal) {
					RowsAffected = ((int) parmRowsAffected.Value);
					bolRetVal = true;
				} else {
					EventLog.logEvent("An error occurred while executing usp_OnlineImageQueue_Ins.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				}
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "CreateOnlineImageQueue(Guid OnlineImageQueueID, int SessionUserID, string UserFileName, bool DisplayScannedCheck, out int RowsAffected)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the lockbox online color mode.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetLockboxOnlineColorMode(int BankID, int ClientAccountID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_dimClientAccountsView_Get_OnlineColorMode", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetLockboxOnlineColorMode");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Updates the job status.
		/// </summary>
		/// <param name="jobID">The job ID.</param>
		/// <param name="jobStatus">The job status.</param>
		/// <param name="fileSize">Size of the file.</param>
		/// <returns></returns>
		public bool UpdateJobStatus(Guid jobID, OLFJobStatus jobStatus, long fileSize) {
			bool bolRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmJobStatusID", SqlDbType.Int, (int) jobStatus, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmJobStatus", SqlDbType.VarChar, jobStatus.ToString(), ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmFileSize", SqlDbType.Int, fileSize, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmJobID", SqlDbType.UniqueIdentifier, jobID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_OnlineImageQueue_Upd", parms);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "UpdateJobStatus");
			}
			
			return bolRetVal;
		}

		/// <summary>
		/// Updates the job status error.
		/// </summary>
		/// <param name="jobID">The job ID.</param>
		/// <param name="errText">The err text.</param>
		/// <returns></returns>
		public bool UpdateJobStatusError(Guid jobID, string errText) {
			bool bolRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmJobStatusID", SqlDbType.Int, (int) OLFJobStatus.Error, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmJobStatus", SqlDbType.VarChar, errText, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmFileSize", SqlDbType.Int, 0, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmJobID", SqlDbType.UniqueIdentifier, jobID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_OnlineImageQueue_Upd", parms);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "UpdateJobStatusError");
				bolRetVal = false;
			}
			return bolRetVal;
		}


		/// <summary>
		/// Gets the image display mode for batch.
		/// </summary>
		/// <param name="OLOrganizationID">The OL customer ID.</param>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetImageDisplayModeForBatch(Guid SessionID, int BankID, int ClientAccountID,  out DataTable dt) {
			bool bolRetVal = false;
			dt = null; 
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				//arParms.Add(BuildParameter("@parmOrganizationID", SqlDbType.UniqueIdentifier, OLOrganizationID, ParameterDirection.Input));
				//arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				//arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				parms = arParms.ToArray();              
                bolRetVal = Database.executeProcedure("RecHubUser.usp_OLWorkgroups_Get_DisplayModeByBankWorkgroupEntity", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetImageDisplayModeForBatch");
			}


			return bolRetVal;
		}

		/// <summary>
		/// Gets the image display mode for lockbox.
		/// </summary>
		/// <param name="OLOrganizationID">The OL customer ID.</param>
		/// <param name="OLClientAccountID">The OL lockbox ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetImageDisplayModeForLockbox(Guid OLClientAccountID, out DataTable dt) {
			//NOTE: as of 1.05 this is no longer used
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				//arParms.Add(BuildParameter("@parmOrganizationID", SqlDbType.UniqueIdentifier, OLOrganizationID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.UniqueIdentifier, OLClientAccountID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_OLClientAccounts_Get_DisplayModeByClientAccountOrganization", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetImageDisplayModeForLockbox");
			}
			
			return bolRetVal;
		}

		/// <summary>
		/// Gets the OLTA document types.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetOLTADocumentTypes(out DataTable dt) {
			bool bolRetVal = false;
			dt = new DataTable();
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmMostRecent", SqlDbType.Bit, 0, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_dimDocumentTypes_Get", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetOLTADocumentTypes");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the check.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="ImmutableDateKey">The processing date key.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetCheck(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_CheckInfo", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetCheck(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the check2.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="ImmutableDateKey">The processing date key.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetCheck2(int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_ByImmutableDate", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetCheck2");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the document.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The client account ID.</param>
		/// <param name="ImmutableDateKey">The immutable date key.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="dt">The data table.</param>
		/// <returns></returns>
		public bool GetDocument(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factDocuments_Get_ByImmutableDate", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetDocument(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt)");
			}

			return bolRetVal;
		}


		/// <summary>
        /// Gets the check data for Report
		/// </summary>
		/// <param name="SessionID"></param>
		/// <param name="BankID"></param>
		/// <param name="ClientAccountID"></param>
		/// <param name="BatchID"></param>
		/// <param name="DepositDate"></param>
		/// <param name="TransactionID"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
		public bool GetCheckData(Guid SessionID,int BankID, int LockboxID, long BatchID, DateTime DepositDate, int TransactionID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
            DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));				
				parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubReport.usp_TranItemReport_GetCheckData", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetCheckData");
			}

			return bolRetVal;
		}

		/// <summary>
        /// Gets the stub data for Report
		/// </summary>
		/// <param name="SessionID"></param>
		/// <param name="BankID"></param>
		/// <param name="LockboxID"></param>
		/// <param name="BatchID"></param>
		/// <param name="DepositDate"></param>
		/// <param name="TransactionID"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
		public bool GetStubData(Guid SessionID,int BankID, int LockboxID, long BatchID, DateTime DepositDate, int TransactionID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
            DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));				
				parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubReport.usp_TranItemReport_GetStubData", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetStubData");
			}

			return bolRetVal;
		}

		/// <summary>
        /// Gets the data entry data.
		/// </summary>
		/// <param name="SessionID"></param>
		/// <param name="BankID"></param>
		/// <param name="LockboxID"></param>
		/// <param name="BatchID"></param>
		/// <param name="DepositDate"></param>
		/// <param name="TransactionID"></param>
		/// <param name="BatchSequence"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
        public bool GetDataEntryData(Guid SessionID, int BankID, int LockboxID, long BatchID, DateTime DepositDate, int TransactionID, int BatchSequence,out DataTable dt)
        {
			bool bolRetVal = false;
			dt = null;
            DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));				
				parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubReport.usp_IMSTranItemReport_GetDataEntryData", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "GetDataEntryData");
			}

			return bolRetVal;
		}


		/// <summary>
		/// Upserts the image RPS alias keyword.
		/// </summary>
		/// <param name="bankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="iExtractType">Type of the i extract.</param>
		/// <param name="sFieldType">Type of the s field.</param>
		/// <param name="iDocType">Type of the i doc.</param>
		/// <param name="sAlias">The s alias.</param>
		/// <returns></returns>
        public bool UpsertImageRPSAliasKeyword(int bankID, int lockboxID, int iExtractType, string sFieldType, int iDocType, string sAlias)
        {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, lockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractType", SqlDbType.Int, iExtractType, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDocType", SqlDbType.Int, iDocType, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmFieldType", SqlDbType.VarChar, sFieldType, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmAliasName", SqlDbType.VarChar, sAlias, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_dimImageRPSAliasMappingsUpsert", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpsertImageRPSAliasKeyword");
            }
            return bolRetVal;

        }


		/// <summary>
		/// Gets the stub.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="ImmutableDateKey">The processing date key.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetStub(int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factStubs_Get_ByBatchSequence", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetStub(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt)");
			}

			return bolRetVal;
		}

		/// <summary>
		/// Gets the document sequence IMS mapping.
		/// </summary>
		/// <param name="BankID">The bank ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="ImmutableDateKey">The processing date key.</param>
		/// <param name="BatchID">The batch ID.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetDocumentSequenceIMSMapping(int BankID, int LockboxID, int ProcessingDateKey, long BatchID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmProcessingDateKey", SqlDbType.Int, ProcessingDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubUser.usp_GetDocumentSequenceIMSMapping", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetDocumentSequenceIMSMapping(int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, out DataTable dt)");
			}
			return bolRetVal;

		}

		/// <summary>
		/// Gets the lockbox summary.
		/// </summary>
		/// <param name="UserID">The user ID.</param>
		/// <param name="DepositDateKey">The deposit date key.</param>
		/// <param name="UseCutoff">if set to <c>true</c> [use cutoff].</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetLockboxSummary(Guid SessionID, int UserID, int DepositDateKey, bool UseCutoff, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDateKey, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmRespectCutoff", SqlDbType.Bit, UseCutoff, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_ClientAccountSummary", parms, out tempdt);
				dt = LegacyDALConvert.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetLockboxSummary(Guid SessionID, int UserID, int DepositDateKey, bool UseCutoff, out DataTable dt)");
			}

			return bolRetVal;

		}

	    /// <summary>
	    /// Gets the workgroup in the WorkgroupDTO object.
	    /// </summary>
	    /// <param name="BankID">The bank ID.</param>
	    /// <param name="ClientAccountID">The lockbox ID.</param>
	    /// <returns>Workgroup data housed in the WorkgroupDTO object</returns>
	    public WorkgroupDTO GetWorkgroup(int BankID, int ClientAccountID) {
	        DataTable workgroupTable=null;
	        WorkgroupDTO result = null;

	        if (GetLockbox(BankID, ClientAccountID, out workgroupTable)) {
	            if (workgroupTable != null) {
	                if (workgroupTable.Rows.Count > 0 &&
                        !(workgroupTable.Rows[0]["FileGroup"] is DBNull)) {
	                    result = new WorkgroupDTO() {
	                        BankID = BankID,
	                        ClientAccountID = ClientAccountID,
	                        FileGroup = workgroupTable.Rows[0]["FileGroup"].ToString()
	                    };
	                }
	                workgroupTable.Dispose();
	            }
	        }
            return result;
	    }

        /// <summary>
        /// Gets the lockbox.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public virtual bool GetLockbox(int BankID, int ClientAccountID, out DataTable dt) {
			bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_BySiteBankIDSiteClientAccountID", parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetLockbox(int BankID, int ClientAccountID, out DataTable dt)");
				bolRetVal = false;
			}

			return bolRetVal;
		}

        /// <summary>
        /// Gets the PaymentTypeKey
        /// Note: This Stored procedure is also used in OLFServicesDAL which is utilized by ipoImages
        /// </summary>
        /// <param name="BankID"></param>
        /// <param name="ClientAccountID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="BatchID"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetPaymentTypeKey(int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_PaymentTypeKey", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetPaymentTypeKey(int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, out DataTable dt)");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the batch data  
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="BankID"></param>
        /// <param name="clientAccountID"></param>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetBatchData(int BankID, int clientAccountID, long BatchID, int DepositDate, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
               // arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, clientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factBatchData_Get", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchData(int BankID, int clientAccountID, long BatchID, DateTime DepositDate, out DataTable dt)");
            }

            return bRtnval;
        }

        /// <summary>
        /// Gets the item data by item
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="BankID"></param>
        /// <param name="clientAccountID"></param>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="BatchSequence"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetItemDataByItem(int BankID, int clientAccountID, long BatchID, DateTime DepositDate, int BatchSequence, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, clientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_FactItemData_Get", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetItemData(int BankID, int clientAccountID, long BatchID, DateTime DepositDate, int BatchSequence, out DataTable dt)");
            }

            return bRtnval;
        }
    }
}