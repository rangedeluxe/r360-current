﻿using System;

namespace WFS.RecHub.DAL.OLFServicesDAL
{
    public interface IOLFServicesDAL : IDisposable, IGetImageDisplayModesDal
    {
        bool GetCheck(Guid SessionID, int BankID, int CleintAccountID, long BatchID, DateTime DepositDate, int TransactionID, int BatchSequence, out System.Data.DataTable dt);
        bool GetCheck(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out System.Data.DataTable dt);
        bool GetCheckDetails(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out System.Data.DataTable dt);
        bool GetCheckImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, out System.Data.DataTable dt);
        bool GetCheckImagePathInfo(long BatchID, int BatchSequence, string FileDescriptor, out System.Data.DataTable dt);
        bool GetDataEntryDetails(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out System.Data.DataTable dt);
        bool GetDocument(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, int TransactionID, int BatchSequence, bool DisplayScannedCheck, out System.Data.DataTable dt);
        bool GetDocument(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out System.Data.DataTable dt);
        bool GetDocumentImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, out System.Data.DataTable dt);
        bool GetImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, WFS.RecHub.Common.TableTypes TableType, out System.Data.DataTable dt);
        bool GetLockbox(int BankID, int ClientAccountID, out System.Data.DataTable dt);
        bool GetLockboxOnlineColorMode(int BankID, int ClientAccountID, out System.Data.DataTable dt);
        bool GetPaymentTypeKey(int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, out System.Data.DataTable dt);
        bool GetRawPaymentData(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out System.Data.DataTable dt);
    }
}
