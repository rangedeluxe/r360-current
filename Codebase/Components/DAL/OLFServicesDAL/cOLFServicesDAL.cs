﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 105559 JMC 06/14/2013 
* WI 116231 CEJ 10/09/2013
*   Remove Deposit Date from the filter of RecHubData.usp_factDocuments_Get_DocumentInfo
* WI 143419 DJW 6/2/2013
*   BatchID to Long, and BatchId to SourceBatchID conversion
* WI 148949 DJW 6/19/2013
*   Added SessionID to methods for sprocs that needed it (related to RAAM)
* WI 150841 SAS 07/01/2014
*   Added new functions for Generating Image on a fly 
* WI 151519 BLR 07/02/2014
*   Added a new stored procedure to gather Batch Source Short Name.
* WI 156531 SAS 07/31/2014
*   Updated a call to method GetCheckImagePathInfo to add filedescriptor to gather Batch Source Short Name.
* WI 160236 SAS 08/21/2014
*   Added a new function to get check details.
* WI 162633 BLR 09/02/2014
*   Altered GetDataEntryDetails to match the new SP definition.
* WI 169848 SAS 10/07/2014
*   Altered GetDataEntryDetails,GetRawPaymentData and GetCheckDetails to match the new SP definition.
* WI 173872 SAS 10/23/2014
*   Altered GetCheckDetails to match the new SP definition.
* WI 174837 SAS 11/03/2014
*   Altered GetDataEntryDetails,GetRawPaymentData and GetCheckDetails to match the new SP definition.
*******************************************************************************/
namespace WFS.RecHub.DAL.OLFServicesDAL
{

    /// <summary>
    /// RecHub Common Data Access Layer
    /// </summary>
    public class cOLFServicesDAL : _DALBase, WFS.RecHub.DAL.OLFServicesDAL.IOLFServicesDAL
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="cOLFServicesDAL"/> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        public cOLFServicesDAL(string siteKey)
            : base(siteKey)
        {
        }

        /// <summary>
        /// Gets the lockbox online color mode.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetLockboxOnlineColorMode(int BankID, int ClientAccountID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_dimClientAccountsView_Get_OnlineColorMode", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetLockboxOnlineColorMode");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the image info.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="ImmutableDateKey">The immutable date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="DepositDateKey">The deposit date key.</param>
        /// <param name="TableType">Type of the table.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, TableTypes TableType, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            try
            {
                using (cOLFServicesDAL objDAL = new cOLFServicesDAL(this.SiteKey))
                {
                    switch (TableType)
                    {
                        case TableTypes.Checks:
                            bolRetVal = objDAL.GetCheckImageInfo(SessionID, BankID, ClientAccountID, ImmutableDateKey, BatchID, BatchSequence, DepositDateKey, out dt);
                            break;
                        case TableTypes.Documents:
                            bolRetVal = objDAL.GetDocumentImageInfo(SessionID, BankID, ClientAccountID, ImmutableDateKey, BatchID, BatchSequence, DepositDateKey, out dt);
                            break;
                        default:
                            break;
                    }
                };
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, ex.Source);
                return false;
            }

            return bolRetVal;
        }
        /// <summary>
        /// Gets the check image info.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="ImmutableDateKey">The immutable date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="DepositDateKey">The deposit date key.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetCheckImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = new DataTable();
            try
            {
                SqlParameter[] parms;
                List<SqlParameter> arParms;
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDateKey, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factCheckImages_Get", parms, out dt);
            }
            catch (System.Exception ex)
            {
                EventLog.logEvent("An error occurred while executing GetImageInfo: " + ex.Message,
                                  this.GetType().Name,
                                  MessageType.Error,
                                  MessageImportance.Essential);
            }

            return bolRetVal;
        }

        /// <summary>
        /// Function gathers information on the check as well as the BatchSource, including the 
        /// Batch Source Short name.
        /// WI 156531 SAS 07/31/2014 :Updated a  call to method GetCheckImagePathInfo to add file descriptor to gather Batch Source Short Name.
        /// </summary>
        /// <param name="BatchID"></param>
        /// <param name="BatchSequence"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetCheckImagePathInfo(long BatchID, int BatchSequence, string FileDescriptor, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = new DataTable();
            try
            {
                SqlParameter[] parms;
                List<SqlParameter> arParms;
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmFileDescriptor", SqlDbType.VarChar, FileDescriptor, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_GetCheckImagePathInfo", parms, out dt);
            }
            catch (System.Exception ex)
            {
                EventLog.logEvent("An error occurred while executing usp_GetCheckImagePathInfo: " + ex.Message,
                                  this.GetType().Name,
                                  MessageType.Error,
                                  MessageImportance.Essential);
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the document image info.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="ImmutableDateKey">The immutable date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="DepositDateKey">The deposit date key.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetDocumentImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = new DataTable();
            try
            {
                SqlParameter[] parms;
                List<SqlParameter> arParms;
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDateKey, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factDocumentImages_Get", parms, out dt);
            }
            catch (System.Exception ex)
            {
                EventLog.logEvent("An error occurred while executing GetImageInfo: " + ex.Message,
                                  this.GetType().Name,
                                  MessageType.Error,
                                  MessageImportance.Essential);
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the check.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="CleintAccountID">The cleint account ID.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="DepositDate">The deposit date.</param>
        /// <param name="TransactionID">The transaction ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetCheck(Guid SessionID, int BankID, int CleintAccountID, long BatchID, DateTime DepositDate, int TransactionID, int BatchSequence, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, CleintAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_ByDepositDate", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetCheck");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the check.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="ImmutableDateKey">The processing date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetCheck(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_CheckInfo", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetCheck(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, int BatchID, int BatchSequence, out DataTable dt)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the document.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="ImmutableDateKey">The immutable date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="dt">The data table.</param>
        /// <returns></returns>
        public bool GetDocument(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factDocuments_Get_DocumentInfo", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetDocument(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, int BatchID, int BatchSequence, out DataTable dt)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the document.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="DepositDate">The deposit date.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="TransactionID">The transaction ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="DisplayScannedCheck">if set to <c>true</c> [display scanned check].</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetDocument(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, int TransactionID, int BatchSequence, bool DisplayScannedCheck, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDisplayScannedCheck", SqlDbType.Bit, DisplayScannedCheck, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_Document", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetDocument(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, int BatchID, int TransactionID, int BatchSequence, bool DisplayScannedCheck, out DataTable dt)");
            }

            return bolRetVal;
        }


        /// <summary>
        /// Gets the PaymentTypeKey
        /// </summary>
        /// <param name="BankID"></param>
        /// <param name="ClientAccountID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="BatchID"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetPaymentTypeKey(int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_PaymentTypeKey", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetPaymentTypeKey(int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, out DataTable dt)");
                bolRetVal = false;
            }

            return bolRetVal;
        }


        /// <summary>
        /// Gets Data Entry Details.
        /// Note: This SP is getting used for ACH and Wire Images
        /// </summary>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="BatchSequence"></param>
        /// <param name="isCheck"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetDataEntryDetails(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmIsCheck", SqlDbType.Bit, isCheck, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factDataEntryDetails_Get", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetDataEntryDetails(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out DataTable dt)");
            }
            return bolRetVal;
        }


        /// <summary>
        ///  Used to pull ACH/ Wire invoice information
        /// </summary>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="BatchSequence"></param>
        /// <param name="isCheck"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetRawPaymentData(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmIsCheck", SqlDbType.Bit, isCheck, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factRawPaymentData_Get", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetRawPaymentData(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out DataTable dt)");
            }
            return bolRetVal;
        }


        /// <summary>
        /// Gets the lockbox.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetLockbox(int BankID, int ClientAccountID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_BySiteBankIDSiteClientAccountID", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetLockbox(int BankID, int ClientAccountID, out DataTable dt)");
                bolRetVal = false;
            }

            return bolRetVal;
        }


       /// <summary>
       /// Get Check Details
       /// Note: Used to pull ACH/Wire Transfer check information
       /// </summary>
       /// <param name="BatchID"></param>
       /// <param name="DepositDate"></param>
       /// <param name="BatchSequence"></param>
       /// <param name="isCheck"></param>
       /// <param name="dt"></param>
       /// <returns></returns>
        public bool GetCheckDetails(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmDepositDate", SqlDbType.DateTime, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmIsCheck", SqlDbType.Bit, isCheck, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_CheckDetails", parms, out tempdt);
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, " GetCheckDetails(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out DataTable dt)");
            }
            return bolRetVal;
        }

        /// <summary>
        /// Reads the check and document image display modes for a given workgroup, after
        /// applying the cascading rules for the workgroup, entity, and system settings.
        /// </summary>
        /// <param name="siteBankId">The site bank ID.</param>
        /// <param name="siteWorkgroupId">The site workgroup ID.</param>
        /// <returns>An object containing the image display modes. Never returns null.</returns>
        public ImageDisplayModes GetImageDisplayModes(int siteBankId, int siteWorkgroupId)
        {
            try
            {
                var parameters = new[]
                {
                    BuildParameter("@paramSiteBankId", SqlDbType.Int, siteBankId, ParameterDirection.Input),
                    BuildParameter("@paramSiteWorkgroupId", SqlDbType.Int, siteWorkgroupId, ParameterDirection.Input),
                };

                DataTable dataTable;
                var success = Database.executeProcedure(
                    "RecHubUser.usp_ImageDisplayModes_Get", parameters, out dataTable);
                if (success)
                {
                    var row = dataTable?.Rows.Cast<DataRow>().FirstOrDefault();
                    if (row != null)
                    {
                        return new ImageDisplayModes(
                            (OLFImageDisplayMode) row["CheckImageDisplayMode"],
                            (OLFImageDisplayMode) row["DocumentImageDisplayMode"]);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name, nameof(GetImageDisplayModes));
            }

            // Fallback result in case of failure
            return new ImageDisplayModes();
        }

        public bool GetImageData(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck,
            out DataTable dt)
        {
            var bolRetVal = false;
            var sp = isCheck? "RecHubData.usp_GetCheckImage" : "RecHubData.usp_GetDocImage";
            dt = null;
            try
            {
                var depDate = int.Parse(DepositDate.ToString("yyyyMMdd"));
                var arParms = new List<SqlParameter>
                {
                    BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input),
                    BuildParameter("@parmDepositDate", SqlDbType.Int, depDate, ParameterDirection.Input),
                    BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input),
                };
                var parms = arParms.ToArray();
                DataTable tempdt = null;
                bolRetVal = Database.executeProcedure(sp, parms, out tempdt);
                //dt = DALLib.ConvertDataTable(ref tempdt);
                dt = tempdt;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, " GetCheckImageData(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out DataTable dt)");
            }
            return bolRetVal;
        }
    }
}
