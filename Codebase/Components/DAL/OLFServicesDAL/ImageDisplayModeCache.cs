﻿using System;
using System.Collections.Generic;
using WFS.RecHub.Common;

namespace WFS.RecHub.DAL.OLFServicesDAL
{
    public class ImageDisplayModeCache
    {
        private readonly Dictionary<Tuple<int, int>, ImageDisplayModes> _cache =
            new Dictionary<Tuple<int, int>, ImageDisplayModes>();
        private readonly IGetImageDisplayModesDal _getter;

        public ImageDisplayModeCache(IGetImageDisplayModesDal getter)
        {
            _getter = getter;
        }

        public ImageDisplayModes GetImageDisplayModes(int siteBankId, int siteWorkgroupId)
        {
            var key = Tuple.Create(siteBankId, siteWorkgroupId);
            if (_cache.ContainsKey(key))
                return _cache[key];

            var result = _getter.GetImageDisplayModes(siteBankId, siteWorkgroupId);
            _cache[key] = result;
            return result;
        }
    }
}