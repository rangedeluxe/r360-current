﻿using WFS.RecHub.Common;

namespace WFS.RecHub.DAL.OLFServicesDAL
{
    public interface IGetImageDisplayModesDal
    {
        /// <summary>
        /// Reads the check and document image display modes for a given workgroup, after
        /// applying the cascading rules for the workgroup, entity, and system settings.
        /// </summary>
        /// <param name="siteBankId">The site bank ID.</param>
        /// <param name="siteWorkgroupId">The site workgroup ID.</param>
        /// <returns>An object containing the image display modes. Never returns null.</returns>
        ImageDisplayModes GetImageDisplayModes(int siteBankId, int siteWorkgroupId);
    }
}