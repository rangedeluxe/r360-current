﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WFS.RecHub.Common.Database;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date:   05/31/2013
*
* Purpose: RecHub SHA Hash Class Library
*
* Modification History
* WI 104019 JMC 05/31/2013
*	-Added file to project.
******************************************************************************/
namespace WFS.RecHub.DAL {
    public static partial class LTAConvert {

        public static MessageType ConvertMessageType(DbMessageType messageType) {
            switch(messageType) {
                case DbMessageType.Error:
                    return (MessageType.Error);
                case DbMessageType.Information:
                    return (MessageType.Information);
                case DbMessageType.Warning:
                    return (MessageType.Warning);
                default:
                    return (MessageType.Information);
            }
        }

        public static MessageImportance ConvertMessageImportance(DbMessageImportance messageImportance) {
            switch(messageImportance) {
                case DbMessageImportance.Debug:
                    return (MessageImportance.Debug);
                case DbMessageImportance.Verbose:
                    return (MessageImportance.Verbose);
                case DbMessageImportance.Essential:
                    return (MessageImportance.Essential);
                default:
                    return (MessageImportance.Essential);
            }
        }
    }
}
