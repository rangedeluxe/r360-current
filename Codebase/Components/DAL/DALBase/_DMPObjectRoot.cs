using System;
using System.Data;
using WFS.RecHub.Common;

using WFS.RecHub.Common.Database;
using WFS.RecHub.Common.Log;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     1/14/2009
*
* Purpose:  _DMPObjectRoot.cs
*
* Modification History
* 01/14/2009 CR 99999 XXX
*   -Initial release.
* WI 90244 CRG 03/07/2013
*	-Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
* WI 103941 WJS 5/31/2013
*   -Create base class change to handle different connection types
* WI 104019 JMC 05/31/2013
*	-Added method that will convert ipoDb delegate types to ipoLib delegate 
*	 types.
* WI 105869 JMC 06/17/2013
*	-Added clientaccountname and clientaccountcutoff to list of 
*	 ConvertDataTable fields.
* WI 107883 EAS 07/03/2013
*	-Added Admin and Extract database connection string/types.
* WI 136855 RDS 04/15/2014
*	-Added Billing database connection string/types
******************************************************************************/
namespace WFS.RecHub.DAL {

    /// <summary>
    /// ipo Image Service Object Root class.
    /// </summary>
	public abstract class _DMPObjectRoot : IDisposable {

        protected cSiteOptions _SiteOptions = null;
        protected cDatabase _Database = null;
        protected cEventLog _EventLog = null;
        protected string _SiteKey;
        private ConnectionType _ConnType = ConnectionType.RecHubData;

        // Track whether Dispose has been called.
        private bool disposed = false;

        public enum ConnectionType
        {
            RecHubData = 0,
            integraPay = 1,
            CommonException =2,
            ExtractWizard = 3,
            RecHubAdmin = 4,
			RecHubBilling = 5,
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="_DMPObjectRoot"/> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
        public _DMPObjectRoot(string vSiteKey) {
            this.SiteKey = vSiteKey;
        }

        public _DMPObjectRoot(string vSiteKey, ConnectionType connType)
        {
            this.SiteKey = vSiteKey;
            this.ConnType = connType;
        }

		/// <summary>
		/// Occurs when [log event].
		/// </summary>
		public event outputMessageEventHandler LogEvent;

		/// <summary>
		/// Determines the section of the local .ini file to be used for local
		/// options.
		/// </summary>
		/// <value>
		/// The site key.
		/// </value>
        public string SiteKey {
            set { _SiteKey = value; }
            get { return _SiteKey; }
        }

        public ConnectionType ConnType
        {
            set { _ConnType = value; }
            get { return _ConnType; }
        }

		/// <summary>
		/// Uses SiteKey to retrieve site options from the local .ini file.
		/// </summary>
		/// <value>
		/// The site options.
		/// </value>
        internal cSiteOptions SiteOptions {
            get {
                if(_SiteOptions == null) { 
                    _SiteOptions = new cSiteOptions(this.SiteKey);
                }

                return _SiteOptions;
            }
        }

		/// <summary>
		/// Logging component using settings from the local siteOptions object.
		/// </summary>
		/// <value>
		/// The event log.
		/// </value>
        protected cEventLog EventLog {
            get {
                if (_EventLog == null) { 
                    _EventLog = new cEventLog(SiteOptions.logFilePath, 
                                              SiteOptions.logFileMaxSize, 
                                              (MessageImportance)SiteOptions.loggingDepth); 
                }

                return _EventLog;
            }
        }

		/// <summary>
		/// Database component using settings from the local siteOptions object.
		/// </summary>
		/// <value>
		/// The database.
		/// </value>
        protected cDatabase Database {
            get {
                if (_Database == null) {

                    switch (this.ConnType)
                    {
                        case ConnectionType.RecHubData:
                            _Database = new cDatabase(SiteOptions.connectionString); 
                            break;
                        case ConnectionType.integraPay:
                            _Database = new cDatabase(SiteOptions.connectionStringIP); 
                            break;
                        case ConnectionType.CommonException:
                            _Database = new cDatabase(SiteOptions.connectionStringCommonException);
                            break;
                        case ConnectionType.ExtractWizard:
                            _Database = new cDatabase(SiteOptions.connectionStringExtracts);
                            break;
						case ConnectionType.RecHubBilling:
							_Database = new cDatabase(SiteOptions.connectionStringBilling);
							break;
                    }
                   

                    _Database.SetDeadlockPriority = SiteOptions.SetDeadlockPriority;
                    _Database.QueryRetryAttempts = SiteOptions.QueryRetryAttempts;

                    _Database.outputMessage += new OutputDbMessageEventHandler(Object_OutputMessage);
                    _Database.outputError += new OutputDbErrorEventHandler(Object_OutputError);
                }

                return _Database;
            }
        }

        /// <summary>
        /// Use the specified instance of <see cref="cDatabase"/>, rather than
        /// lazy-creating our own based on <see cref="SiteOptions"/>.
        /// </summary>
        public void UseDatabase(cDatabase database)
        {
            _Database = database;
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output messages.
        /// </summary>
        /// <param name="message">Test message to be logged.</param>
        /// <param name="src">Source of the event.</param>
        /// <param name="messageType">MessageType(Information, Warning, Error)</param>
        /// <param name="messageImportance">MessageImportance(Essential, Verbose, Debug)
        /// </param>
		protected void Object_OutputMessage(string message,
											string src,
											MessageType messageType,
											MessageImportance messageImportance) {
			OnLogEvent(message, src, messageType, messageImportance);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        protected void Object_OutputMessage(string message,
                                            string src,
                                            DbMessageType messageType,
                                            DbMessageImportance messageImportance) {
            OnLogEvent(
                message, 
                src,
                LTAConvert.ConvertMessageType(messageType),
                LTAConvert.ConvertMessageImportance(messageImportance));
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output errors.
        /// </summary>
        /// <param name="e"></param>
		protected void Object_OutputError(Exception e) {
			OnLogEvent(e.Message, e.TargetSite.ToString(), MessageType.Error, MessageImportance.Essential);
		}

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
        public void Dispose() {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
		/// <summary>
		/// Releases unmanaged and - optionally - managed resources.
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing) {
            // Check to see if Dispose has already been called.
            if(!this.disposed) {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if(disposing) {
                    // Dispose managed resources.
                    if(_Database != null) {
                        _Database.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }

		/// <summary>
		/// Called when [log event].
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="source">The source.</param>
		/// <param name="type">The type.</param>
		/// <param name="importance">The importance.</param>
		protected void OnLogEvent(string message, string source, MessageType type, MessageImportance importance) {
			if(LogEvent != null) {
				LogEvent(message, source, type, importance);
			} else {
				EventLog.logEvent(message, source, type, importance);
			}
		}

        // Use interop to call the method necessary
        // to clean up the unmanaged resource.
		/// <summary>
		/// Closes the handle.
		/// </summary>
		/// <param name="handle">The handle.</param>
		/// <returns></returns>
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~_DMPObjectRoot() {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
        /// <summary>
        /// Data Access Layer Library for data conversion 
        /// </summary>
        protected static class DALLib
        {

            /// <summary>
            /// SQLs the encode string.
            /// </summary>
            /// <param name="Value">The value.</param>
            /// <returns></returns>
            public static string SQLEncodeString(string Value)
            {
                return Value.Replace("'", "''");
            }

            /// <summary>
            /// Gets the data value varchar.
            /// </summary>
            /// <param name="DataValue">The data value.</param>
            /// <returns></returns>
            public static string GetDataValueVarchar(string DataValue)
            {
                string strRetVal;
                if (DataValue != null)
                {
                    strRetVal = "'" + SQLEncodeString(DataValue) + "'";
                }
                else
                {
                    strRetVal = "NULL";
                }

                return (strRetVal);
            }

            /// <summary>
            /// Gets the data value date time.
            /// </summary>
            /// <param name="DataValue">The data value.</param>
            /// <returns></returns>
            public static string GetDataValueDateTime(string DataValue)
            {
                DateTime dateTemp;
                string strRetVal;
                if (DataValue != null && DateTime.TryParse(DataValue, out dateTemp))
                {
                    strRetVal = "'" + dateTemp.ToString() + "'";
                }
                else
                {
                    //if not a dateTime put a NULL here
                    strRetVal = "NULL";
                }

                return (strRetVal);
            }

            /// <summary>
            /// Gets the data value double.
            /// </summary>
            /// <param name="DataValue">The data value.</param>
            /// <returns></returns>
            public static string GetDataValueDouble(string DataValue)
            {
                double doubleTemp;
                string strRetVal;
                if (DataValue != null && double.TryParse(DataValue, out doubleTemp))
                {
                    strRetVal = doubleTemp.ToString();
                }
                else
                {
                    //if not a Double put a NULL here
                    strRetVal = "NULL";
                }

                return (strRetVal);
            }

            /// <summary>
            /// Gets the data value int.
            /// </summary>
            /// <param name="DataValue">The data value.</param>
            /// <returns></returns>
            public static string GetDataValueInt(string DataValue)
            {
                int intTemp;
                string strRetVal;
                if (DataValue != null && int.TryParse(DataValue, out intTemp))
                {
                    strRetVal = intTemp.ToString();
                }
                else
                {

                    //if not an int put a NULL here
                    strRetVal = "NULL";
                }

                return (strRetVal);
            }

            /// <summary>
            /// Gets the data value bit.
            /// </summary>
            /// <param name="DataValue">The data value.</param>
            /// <returns></returns>
            public static string GetDataValueBit(string DataValue)
            {
                string strRetVal;
                if (DataValue != null && DataValue.Trim().Length > 0)
                {
                    if (DataValue.Trim() == "1" || DataValue.Trim().ToLower() == "true")
                    {
                        strRetVal = "1";
                    }
                    else if (DataValue.Trim() == "0" || DataValue.Trim().ToLower() == "false")
                    {
                        strRetVal = "0";
                    }
                    else
                    {

                        //if not a boolean put a NULL here
                        strRetVal = "NULL";
                    }
                }
                else
                {

                    //if not a boolean put a NULL here
                    strRetVal = "NULL";
                }

                return (strRetVal);
            }

            /// <summary>
            /// Converts the data table, by replacing the .
            /// </summary>
            /// <param name="dt">The data table.</param>
            /// <returns></returns>
            public static DataTable ConvertDataTable(ref DataTable dt)
            {
                if (dt != null)
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        switch (column.ColumnName.ToLower())
                        {
                            case "olclientaccount":
                                column.ColumnName = "OLLockbox";
                                break;
                            case "olclientaccountid":
                                column.ColumnName = "OLLockboxID";
                                break;
                            case "siteclientaccountid":
                                column.ColumnName = "SiteLockboxID";
                                break;
                            case "siteclientaccountkey":
                                column.ColumnName = "SiteLockboxKey";
                                break;
                            case "clientaccountname":
                                column.ColumnName = "LockboxName";
                                break;
                            case "clientaccountcutoff":
                                column.ColumnName = "LockboxCutoff";
                                break;
                            case "olorganization":
                                column.ColumnName = "OLCustomer";
                                break;
                            case "olorganizationid":
                                column.ColumnName = "OLCustomerID";
                                break;
                            case "olorganizationcode":
                                column.ColumnName = "OLCustomerCode";
                                break;
                            case "organizationid":
                                column.ColumnName = "CustomerID";
                                break;
                            case "clientaccountid":
                                column.ColumnName = "LockboxID";
                                break;
                            case "organizationcode":
                                column.ColumnName = "CustomerCode";
                                break;
                            case "immutabledatekey":
                                column.ColumnName = "ProcessingDateKey";
                                break;
                            case "immutabledate":
                                column.ColumnName = "ProcessingDate";
                                break;
                        }
                    }
                }

                return dt;
            }
        }
    }
   
}
