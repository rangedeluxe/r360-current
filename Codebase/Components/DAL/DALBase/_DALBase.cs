﻿using System;
using System.Data;
using System.Data.SqlClient;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     
*
* Purpose:  This file is a helper file the making database calls.
*
* Modification History
* CR 32576 JMC 02/01/2011
*    -Added new BuildParameter overload.
* CR 52307 WJS 04/25/2012
*   -Handle apostrophes when passed in to execute procedure via parameters
* CR 52365 WJS 5/3/2012
*   - Remove apostrophes handling in general
* WI 90244 CRG 03/07/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
* WI 92351 CRG 03/14/2013
*	When logging events use an output variable for the SQL error message, only 1 character of the message is being displayed
* WI 103941 WJS 5/31/2013
 *   Create base class change to handle different connection types
******************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// 
	/// </summary>
    public abstract class _DALBase : _DMPObjectRoot {

        private cError _LastError = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="_DALBase"/> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
        public _DALBase(string vSiteKey) : base(vSiteKey) {
        }

        public _DALBase(string vSiteKey, ConnectionType connType)
            : base(vSiteKey, connType)
        {
        }

		/// <summary>
		/// Begins the trans.
		/// </summary>
        public void BeginTrans() {
            Database.beginTrans();
        }

		/// <summary>
		/// Commits the trans.
		/// </summary>
        public void CommitTrans() {
            Database.commitTrans();
        }

		/// <summary>
		/// Rollbacks the trans.
		/// </summary>
        public void RollbackTrans() {
            Database.rollbackTrans();
        }

		/// <summary>
		/// Gets or sets a value indicating whether [set deadlock priority].
		/// </summary>
		/// <value>
		///   <c>true</c> if [set deadlock priority]; otherwise, <c>false</c>.
		/// </value>
        public bool SetDeadlockPriority {
            get {
                return(Database.SetDeadlockPriority);
            }
            set {
                Database.SetDeadlockPriority = value;
            }
        }

		/// <summary>
		/// Executes the SQL.
		/// </summary>
		/// <param name="SQL">The SQL.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
        protected bool ExecuteSQL(string SQL, out DataTable dt) {

            DataTable dtRetVal = null;
            bool bolRetVal;

            try {
                bolRetVal = Database.CreateDataTable(SQL, out dtRetVal);

                if(bolRetVal) {
                    dt = dtRetVal;
                    ////FilterDataTable(dt);
                } else {
                    if(dtRetVal != null) {
                        dtRetVal.Dispose();
                    }
                    dt = null;
                }
            } catch(Exception ex) {
                if(dtRetVal != null) {
                    dtRetVal.Dispose();
                }
                dt = null;
                _LastError = new cError(-1, ex.Message);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

		/// <summary>
		/// Executes the SQL.
		/// </summary>
		/// <param name="SQL">The SQL.</param>
		/// <returns></returns>
        protected bool ExecuteSQL(string SQL) {

            bool bolRetVal;

            try {
                bolRetVal = (Database.executeNonQuery(SQL) >= 0);

            } catch(Exception ex) {
                _LastError = new cError(-1, ex.Message);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

		/// <summary>
		/// Executes the non scalar.
		/// </summary>
		/// <param name="SQL">The SQL.</param>
		/// <returns></returns>
        protected bool ExecuteNonScalar(string SQL)
        {
            bool bolRetVal;

            try
            {
                bolRetVal = (Database.executeNonScalarQuery(SQL) == -1);
            }
            catch (Exception ex)
            {
                _LastError = new cError(-1, ex.Message);
                bolRetVal = false;
            }

            return (bolRetVal);
        }

		/// <summary>
		/// Executes the SQL.
		/// </summary>
		/// <param name="SQL">The SQL.</param>
		/// <param name="RowsAffected">The rows affected.</param>
		/// <returns></returns>
        protected bool ExecuteSQL(string SQL, out int RowsAffected) {

            bool bolRetVal;

            try {
                RowsAffected = Database.executeNonQuery(SQL);
                bolRetVal = (RowsAffected >= 0);

            } catch(Exception ex) {
                _LastError = new cError(-1, ex.Message);
                RowsAffected = -1;
                bolRetVal = false;
            }

            return(bolRetVal);
        }

		/// <summary>
		/// Builds the parameter.
		/// </summary>
		/// <param name="ParameterName">Name of the parameter.</param>
		/// <param name="Type">The type.</param>
		/// <param name="Value">The value.</param>
		/// <param name="Direction">The direction.</param>
		/// <returns></returns>
         protected static SqlParameter BuildParameter(string ParameterName, SqlDbType Type, object Value, ParameterDirection Direction) {

            SqlParameter objParameter = new SqlParameter();
            int intSize = 0;
            int i;
            
            objParameter.ParameterName = ParameterName;
            objParameter.SqlDbType = Type;
            objParameter.Value = Value;
            objParameter.Direction = Direction;
            
            if(Type == SqlDbType.Xml) {
                if(Value == null) {
                    objParameter.Size = 4096;
                } else {
                    i = 10;
                    intSize = 2^i;
                    while(intSize < Value.ToString().Length) {
                        ++i;
                        intSize = 2^i;                    
                    }
                    objParameter.Size = intSize;
                }
			} else if(Type == SqlDbType.VarChar) {
				objParameter.Size = 1024;
			}

            return(objParameter);
        }

		 /// <summary>
		 /// Builds the parameter.
		 /// </summary>
		 /// <param name="ParameterName">Name of the parameter.</param>
		 /// <param name="Type">The type.</param>
		 /// <param name="Size">The size.</param>
		 /// <param name="Value">The value.</param>
		 /// <returns></returns>
        protected static SqlParameter BuildParameter(string ParameterName, SqlDbType Type, int Size, string Value) {

            SqlParameter objParameter = new SqlParameter();
            
            objParameter.ParameterName = ParameterName;
            objParameter.SqlDbType = Type;
            objParameter.Size = Size;
            objParameter.Value = Value;

            return(objParameter);
        }

		/// <summary>
		/// Builds the return value parameter.
		/// </summary>
		/// <param name="ParameterName">Name of the parameter.</param>
		/// <param name="Type">The type.</param>
		/// <returns></returns>
        protected static SqlParameter BuildReturnValueParameter(string ParameterName, SqlDbType Type) {

            SqlParameter objParameter = new SqlParameter();
            
            objParameter.ParameterName = ParameterName;
            objParameter.SqlDbType = Type;
            objParameter.Direction = ParameterDirection.ReturnValue;

            return(objParameter);
        }

		/// <summary>
		/// Gets the get last error.
		/// </summary>
		/// <value>
		/// The get last error.
		/// </value>
        public cError GetLastError {
            get {
                if(_LastError == null) { 
                    _LastError = new cError(0, string.Empty);
                }
                return(_LastError);
            }
        }
    }

	/// <summary>
	/// 
	/// </summary>
    public class cError {

        private int _Number = 0;
        private string _Description = string.Empty;

		/// <summary>
		/// Initializes a new instance of the <see cref="cError"/> class.
		/// </summary>
		/// <param name="Number">The number.</param>
		/// <param name="vDescription">The v description.</param>
        public cError(int Number, string vDescription) {
            _Number = -1;
            _Description = vDescription;
        }

		/// <summary>
		/// Gets the number.
		/// </summary>
		/// <value>
		/// The number.
		/// </value>
        public int Number {
            get {
                return(_Number);
            }
        }

		/// <summary>
		/// Gets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
        public string Description {
            get {
                return(_Description);
            }
        }
    }
}
