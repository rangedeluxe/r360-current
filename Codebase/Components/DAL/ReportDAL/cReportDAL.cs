﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Jason Efken   
* Date: 1/16/2012
*
* Purpose: This class library provides a COM interface to the OLTA Report Module.
*
* Modification History
* CR 54054 JNE 07/09/2012
*   - Added intUserID parameter to GetHOAPMASummaryData 
* WI 87320 CRG 02/05/2013
*   Remove SQL_Reports.cs
*******************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// Report Data Access Layer
	/// </summary>
    public class cReportDAL : _DALBase {

		/// <summary>
		/// Initializes a new instance of the <see cref="cReportDAL" /> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
        public cReportDAL(string vSiteKey) : base(vSiteKey) {
        }

		/// <summary>
		/// Gets the HOAPMA summary data.
		/// </summary>
		/// <param name="CustomerID">The customer ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="UserID">The user ID.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Results of store procedure call
		/// </returns>
		public bool GetHOAPMASummaryData(int CustomerID, int LockboxID, int DepositDate, int UserID, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmCustomerID", SqlDbType.Int, CustomerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDate, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_GetHOAPMASummaryDetail", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetHOAPMASummaryData(int CustomerID, int ClientAccountID, int DepositDate , int UserID, out DataTable dt)");
			}

			return bRetVal;
        }

		/// <summary>
		/// Gets the HOAPMA summary header.
		/// </summary>
		/// <param name="CustomerID">The customer ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="UserID">The user ID.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Results of store procedure call
		/// </returns>
		public bool GetHOAPMASummaryHeader(int CustomerID, int LockboxID, int DepositDate, int UserID, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSiteCustomerID", SqlDbType.Int, CustomerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDate, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_GetHOAPMASummaryHeader", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetHOAPMASummaryHeader(int CustomerID, int ClientAccountID, int DepositDate, int UserID, out DataTable dt)");
			}

			return bRetVal;
		}

		/// <summary>
		/// Gets the HOAPMA detail header.
		/// </summary>
		/// <param name="CustomerID">The customer ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="UserID">The user ID.</param>
		/// <param name="HOA_Number">The HO a_ number.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Results of store procedure call
		/// </returns>
		public bool GetHOAPMADetailHeader(int CustomerID, int LockboxID, int DepositDate, int UserID, string HOA_Number, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmCustomerID", SqlDbType.Int, CustomerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmHOA_Number", SqlDbType.VarChar, HOA_Number, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_GetHOAPMADetailHeader", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetHOAPMADetailHeader(int CustomerID, int ClientAccountID, int DepositDate, int UserID, string HOA_Number, out DataTable dt)");
			}

			return bRetVal;
		}

		/// <summary>
		/// Gets the HOAPMA detail data.
		/// </summary>
		/// <param name="CustomerID">The customer ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="UserID">The user ID.</param>
		/// <param name="HOA_Number">The HO a_ number.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Results of store procedure call
		/// </returns>
		public bool GetHOAPMADetailData(int CustomerID, int LockboxID, int DepositDate, int UserID, string HOA_Number, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmCustomerID", SqlDbType.Int, CustomerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmHOA_Number", SqlDbType.VarChar, HOA_Number, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_GetHOAPMADetailData", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetHOAPMADetailData(int intCustomerID, int intLockboxID, int intDepositDate, int intUserID, string strHOA_Number, out DataTable dt)");
			}

			return bRetVal;
        }

		/// <summary>
		/// Gets the HOAPMA detail footer.
		/// </summary>
		/// <param name="CustomerID">The customer ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="UserID">The user ID.</param>
		/// <param name="HOA_Number">The HO a_ number.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Results of store procedure call
		/// </returns>
		public bool GetHOAPMADetailFooter(int CustomerID, int LockboxID, int DepositDate, int UserID, string HOA_Number, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmCustomerID", SqlDbType.Int, CustomerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmHOA_Number", SqlDbType.VarChar, HOA_Number, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_GetHOAPMADetailFooter", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetHOAPMADetailFooter(int CustomerID, int ClientAccountID, int DepositDate, int UserID, string HOA_Number, out DataTable dt)");
			}

			return bRetVal;
        }

		/// <summary>
		/// Gets the HOAPMA customers.
		/// </summary>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="UserID">The user ID.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Results of store procedure call
		/// </returns>
		public bool GetHOAPMACustomers(int DepositDate, int UserID, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDate, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_GetHOAPMACustomers", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetHOAPMACustomers(int DepositDate, int UserID, out DataTable dt)");
			}

			return bRetVal;
        }

		/// <summary>
		/// Gets the HOAPMAHOA names.
		/// </summary>
		/// <param name="DepositDate">The deposit date.</param>
		/// <param name="UserID">The user ID.</param>
		/// <param name="CustomerID">The customer ID.</param>
		/// <param name="ClientAccountID">The lockbox ID.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Results of store procedure call
		/// </returns>
		public bool GetHOAPMAHOANames(int DepositDate, int UserID, int CustomerID, int LockboxID, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, DepositDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmCustomerID", SqlDbType.Int, CustomerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_GetHOAPMAHOANames", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetHOAPMAHOANames(int DepositDate, int UserID, int CustomerID, int ClientAccountID, out DataTable dt)");
			}

			return bRetVal;
        }   
    }
}
