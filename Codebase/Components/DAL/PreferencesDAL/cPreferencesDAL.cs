﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin R Glomb
* Date:     12/12/2012
*
* Purpose: Provide data access to the preferences table
*
* Modification History
* WI 72211 CRG 12/17/2012
*	Convert cPreferencesDAL.DeleteOLUserPreference(int) to a stored procedure call
* WI 72212 CRG 12/17/2012
*   Convert cPreferencesDAL.GetOLPreferences(out DataTable) to a stored procedure call
* WI 72213 CRG 12/17/2012
*   Convert cPreferencesDAL.GetOLPreferences(int, out DataTable) to a stored procedure call	
* WI 72214 CRG 12/17/2012
*   Convert cPreferencesDAL.GetOLPreferencesWithDescriptions(out DataTable) to a stored procedure call
* WI 72215 CRG 12/17/2012
*   Convert cPreferencesDAL.GetOLPreferencesWithDescriptions(int, out DataTable) to a stored procedure call
* WI 72216 CRG 12/17/2012
*   Convert cPreferencesDAL.GetOLUserPreferenceCount(Guid, int, out DataTable) to a stored procedure call
* WI 72217 CRG 12/17/2012
*   Convert cPreferencesDAL.GetOLUserPreferenceIDByName(string, out DataTable) to a stored procedure call
* WI 72218 CRG 12/17/2012
*   Convert cPreferencesDAL.GetOLUserPreferences(int, out DataTable) to a stored procedure call
* WI 72219 CRG 12/17/2012
*   Convert cPreferencesDAL.InsertOLUserPreference(Guid, int, string, out int) to a stored procedure call
* WI 72220 CRG 12/17/2012
*   Convert cPreferencesDAL.UpdateOLPreferences(Guid, string, bool, out int) to a stored procedure call
* WI 72221 CRG 12/17/2012
*   Convert cPreferencesDAL.UpdateOLUserPreference(Guid, int, string, out int) to a stored procedure call 
* WI 88724 CRG 02/21/2013 
*    - Overload the LogError function for two Parameters (Exception and Class name)
* WI 90244 CRG 03/19/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
* WI 109747 EAS 07/30/2013
*   -Add Constructor with Connection Type 
* WI 124123 TWE 12/03/2013
*    Eliminate Cross-Talk between DALs
******************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// Preference Data Access Layer class
	/// </summary>
	public class cPreferencesDAL : _DALBase {

		/// <summary>
		/// Initializes a new instance of the <see cref="cPreferencesDAL" /> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
		public cPreferencesDAL(string vSiteKey)
			: base(vSiteKey) {
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="cPreferencesDAL"/> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        /// <param name="enmConnection">Connection type to use.</param>
        public cPreferencesDAL(string vSiteKey, ConnectionType enmConnection)
            : base(vSiteKey, enmConnection) { }

        /// <summary>
        /// Gets the OL preferences.
        /// </summary>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of database store procedure call
        /// </returns>
        public bool GetOLPreferences(out DataTable dt)
        {
            bool bRetVal = false;
            dt = null;
            try
            {
                bRetVal = Database.executeProcedure("RecHubUser.usp_OLPreferences_Get", out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLPreferences(out DataTable dt)");
            }

            return bRetVal;
        }

        /// <summary>
        /// Gets the OL preferences.
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of database store procedure call
        /// </returns>
        public bool GetOLPreferences(int UserID, out DataTable dt)
        {
            bool bRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRetVal = Database.executeProcedure("RecHubUser.usp_OLPreferences_Get_ByUserID", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLPreferences(int UserID, out DataTable dt)");
            }

            return bRetVal;
        }

        /// <summary>
        /// Gets the OL preferences with descriptions.
        /// </summary>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of database store procedure call
        /// </returns>
        public bool GetOLPreferencesWithDescriptions(out DataTable dt)
        {
            bool bRetVal = false;
            dt = null;
            try
            {
                bRetVal = Database.executeProcedure("RecHubUser.usp_OLPreferences_Get_WithDescriptions", out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLPreferencesWithDescriptions(out DataTable dt)");
            }

            return bRetVal;
        }

        /// <summary>
        /// Gets the OL preferences with descriptions.
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of database store procedure call
        /// </returns>
        public bool GetOLPreferencesWithDescriptions(int UserID, out DataTable dt)
        {
            bool bRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRetVal = Database.executeProcedure("RecHubUser.usp_OLPreferences_Get_WithDescriptionsByUserID", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLPreferencesWithDescriptions(int UserID, out DataTable dt)");
            }

            return bRetVal;
        }

	}
}