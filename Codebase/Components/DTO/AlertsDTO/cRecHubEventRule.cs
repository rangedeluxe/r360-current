﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: AlertsDTO 
*  Filename: cRecHubEventRule.cs
*    Author: Eric Smasal
*
* Revisions:
* ----------------------
* WI 99857 EAS 05/21/2013
*    Initial Creation
******************************************************************************/


namespace WFS.RecHub.Common
{
    [DataContract]
    public class cRecHubEventRule
    {
        #region Properties

        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int EventID { get; set; }
        [DataMember]
        public int EventLevel { get; set; }
        [DataMember]
        public int SiteCodeID { get; set; }
        [DataMember]
        public int? SiteBankID { get; set; }
        [DataMember]
        public int? SiteOrganizationID { get; set; }
        [DataMember]
        public int? SiteClientAccountID { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Operator { get; set; }
        [DataMember]
        public string Value { get; set; }

        public static Dictionary<string, string> FieldMapping
        {
            get
            {
                return new Dictionary<string, string>() {
                    {"ID", "EventRuleID"},
                    {"EventID", "EventID"},
                    {"EventLevel", "EventLevel"},
                    {"SiteCodeID", "SiteCodeID"},
                    {"SiteBankID", "SiteBankID"},
                    {"SiteOrganizationID", "SiteOrganizationID"},
                    {"SiteClientAccountID", "SiteClientAccountID"},
                    {"IsActive", "IsActive"},
                    {"Description", "EventRuleDescription"},
                    {"Operator", "EventRuleOperator"},
                    {"Value", "EventRuleValue"}
                };
            }
        }

        #endregion

        #region Constructors

        public cRecHubEventRule() { }

        public cRecHubEventRule(DataRow drData) : this()
        {
            ID = (int)drData["EventRuleID"];
            EventID = (int)drData["EventID"];
            EventLevel = (int)drData["EventLevel"];
            SiteCodeID = (int)drData["SiteCodeID"];

            if (drData["SiteBankID"] != DBNull.Value)
                SiteBankID = (int)drData["SiteBankID"];

            if (drData["SiteOrganizationID"] != DBNull.Value)
                SiteOrganizationID = (int)drData["SiteOrganizationID"];

            if (drData["SiteClientAccountID"] != DBNull.Value)
                SiteClientAccountID = (int)drData["SiteClientAccountID"];

            IsActive = (bool)drData["IsActive"];
            Description = drData["EventRuleDescription"].ToString();
            Operator = drData["EventRuleOperator"].ToString();
            Value = drData["EventRuleValue"].ToString();

        }

        public cRecHubEventRule(XElement xmeData) : this()
        {
            ID = int.Parse(xmeData.Attribute("EventRuleID").Value);
            EventID = int.Parse(xmeData.Attribute("EventID").Value);
            EventLevel = int.Parse(xmeData.Attribute("EventLevel").Value);
            SiteCodeID = int.Parse(xmeData.Attribute("SiteCodeID").Value);

            if (xmeData.Attribute("SiteBankID").Value != String.Empty)
                SiteBankID = int.Parse(xmeData.Attribute("SiteBankID").Value);

            if (xmeData.Attribute("SiteOrganizationID").Value != String.Empty)
                SiteOrganizationID = int.Parse(xmeData.Attribute("SiteOrganizationID").Value);

            if (xmeData.Attribute("SiteClientAccountID").Value != String.Empty)
                SiteClientAccountID = int.Parse(xmeData.Attribute("SiteClientAccountID").Value);

            IsActive = bool.Parse(xmeData.Attribute("IsActive").Value);
            Description = xmeData.Attribute("EventRuleDescription").Value;
            Operator = xmeData.Attribute("EventRuleOperator").Value;
            Value = xmeData.Attribute("EventRuleValue").Value;
            
        }

        #endregion

        #region Methods

        #region Public

        public XElement ToXml()
        {
            return new XElement("EventRule",
                    new XElement("EventRuleID", ID),
                    new XElement("EventID", EventID),
                    new XElement("EventLevel", EventLevel),
                    new XElement("SiteCodeID", SiteCodeID),
                    new XElement("SiteBankID", SiteBankID == null ? String.Empty : SiteBankID.ToString()),
                    new XElement("SiteOrganizationID", SiteOrganizationID == null ? String.Empty : SiteClientAccountID.ToString()),
                    new XElement("SiteClientAccountID", SiteClientAccountID == null ? String.Empty : SiteClientAccountID.ToString()),
                    new XElement("IsActive", IsActive),
                    new XElement("EventRuleDescription", Description),
                    new XElement("EventRuleOperator", Operator),
                    new XElement("EventRuleValue", Value));
        }

        #endregion

        #endregion




    }

}
