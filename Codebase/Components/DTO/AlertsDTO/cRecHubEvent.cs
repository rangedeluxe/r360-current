﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Xml.Linq;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: AlertsDTO 
*  Filename: cRecHubEvent.cs
*    Author: Eric Smasal
*
* Revisions:
* ----------------------
* WI 99857 EAS 06/21/2013
*    Initial Creation
******************************************************************************/

namespace WFS.RecHub.Common
{
    [DataContract]
    public class cRecHubEvent
    {
        #region Properties

        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string DefaultMessage { get; set; }
        [DataMember]
        public string Schema { get; set; }
        [DataMember]
        public string Table { get; set; }
        [DataMember]
        public string Column { get; set; }
        [DataMember]
        public string Operators { get; set; }

        public static Dictionary<string, string> FieldMapping
        {
            get
            {
                return new Dictionary<string, string>() {
                    {"ID", "EventID"},
                    {"Name", "EventName"},
                    {"Type", "EventType"},
                    {"IsActive", "IsActive"},
                    {"DefaultMessage", "MessageDefault"},
                    {"Schema", "EventTable"},
                    {"Table", "EventTable"},
                    {"Column", "EventColumn"},
                    {"Operators", "EventOperators"}
                };
            }
        }

        #endregion

        #region Constructors

        public cRecHubEvent() { }

        public cRecHubEvent(DataRow drData)
            : this()
        {
            ID = (int)drData["EventID"];
            Name = drData["EventName"].ToString();
            Type = (int)drData["EventType"];
            IsActive = (bool)drData["IsActive"];
            DefaultMessage = drData["MessageDefault"].ToString();
            Schema = drData["EventSchema"].ToString();
            Table = drData["EventTable"].ToString();
            Column = drData["EventColumn"].ToString();
            Operators = drData["EventOperators"].ToString();
        }

        public cRecHubEvent(XElement xmeData)
            : this()
        {
            ID = int.Parse(xmeData.Attribute("EventID").Value);
            Name = xmeData.Attribute("EventName").Value;
            Type = int.Parse(xmeData.Attribute("EventType").Value);
            IsActive = bool.Parse(xmeData.Attribute("IsActive").Value);
            DefaultMessage = xmeData.Attribute("MessageDefault").Value;
            Schema = xmeData.Attribute("EventSchema").Value;
            Table = xmeData.Attribute("EventTable").Value;
            Column = xmeData.Attribute("EventColumn").Value;
            Operators = xmeData.Attribute("EventOperators").Value;
        }

        #endregion

        #region Methods

        #region Public

        public XElement ToXML()
        {
            return new XElement("Event",
                    new XAttribute("EventID", ID.ToString()),
                    new XAttribute("EventName", Name),
                    new XAttribute("EventType", Type.ToString()),
                    new XAttribute("IsActive", IsActive.ToString()),
                    new XAttribute("MessageDefault", DefaultMessage),
                    new XAttribute("EventSchema", Schema),
                    new XAttribute("EventTable", Table),
                    new XAttribute("EventColumn", Column),
                    new XAttribute("EventOperators", Operators));
        }

        #endregion

        #endregion
    }
}
