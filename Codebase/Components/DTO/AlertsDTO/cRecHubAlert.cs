﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Xml.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: AlertsDTO 
*  Filename: cRecHubAlert.cs
*    Author: Eric Smasal
*
* Revisions:
* ----------------------
* WI 99857 EAS 06/21/2013
*    Initial Creation
******************************************************************************/

namespace WFS.RecHub.Common
{
    [DataContract]
    public class cRecHubAlert
    {
        #region Properties

        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int? UserID { get; set; }
        [DataMember]
        public int DeliveryMethodID { get; set; }
        [DataMember]
        public int EventRuleID { get; set; }

        public static Dictionary<string, string> FieldMapping
        {
            get
            {
                return new Dictionary<string, string>() {
                    {"ID", "AlertID"},
                    {"UserID", "UserID"},
                    {"DeliveryMethodID", "DeliveryMethodID"},
                    {"EventRuleID", "EventRuleID"}
                };
            }
        }

        #endregion

        #region Constructors

        public cRecHubAlert() { }

        public cRecHubAlert(DataRow drData)
            : this()
        {
            ID = (int)drData["AlertID"];

            if (drData["UserID"] != DBNull.Value)
                UserID = (int)drData["UserID"];

            DeliveryMethodID = (int)drData["DeliveryMethodID"];
            EventRuleID = (int)drData["EventRuleID"];
        }

        public cRecHubAlert(XElement xmeData)
            : this()
        {
            ID = int.Parse(xmeData.Attribute("AlertID").Value);

            if (xmeData.Attribute("UserID").Value != String.Empty)
                UserID = int.Parse(xmeData.Attribute("UserID").Value);

            DeliveryMethodID = int.Parse(xmeData.Attribute("DeliveryMethodID").Value);
            EventRuleID = int.Parse(xmeData.Attribute("EventRuleID").Value);
        }

        #endregion

        #region Methods

        #region Public

        public XElement ToXML()
        {
            return new XElement("Alert",
                    new XAttribute("AlertID", ID.ToString()),
                    new XAttribute("UserID", UserID == null ? String.Empty : UserID.ToString()),
                    new XAttribute("DeliveryMethodID", DeliveryMethodID),
                    new XAttribute("EventRuleID", EventRuleID));
        }

        #endregion

        #endregion
    }
}
