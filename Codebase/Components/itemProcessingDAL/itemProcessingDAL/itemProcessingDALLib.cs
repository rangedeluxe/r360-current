﻿
using WFS.LTA.Common;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53789 JMC 08/23/2012 
*   -Initial version.
* WI 72252 TWE 12/20/2012
*  - Move integraPAY DAL queries from cResearchDAL to itemProcessingDAL
*******************************************************************************/
namespace WFS.itemProcessing.DAL {

    internal static class itemProcessingDALLib {

        public static LTAMessageType ConvertIPOtoLTA(MessageType messageType) {

            LTAMessageType enmType;

            switch(messageType) {
                case MessageType.Information:
                    enmType = LTAMessageType.Information;
                    break;
                case MessageType.Warning:
                    enmType = LTAMessageType.Warning;
                    break;
                case MessageType.Error:
                    enmType = LTAMessageType.Error;
                    break;
                default:
                    enmType = LTAMessageType.Information;
                    break;
            }

            return(enmType);
        }

        public static LTAMessageImportance ConvertIPOtoLTA(MessageImportance messageImportance) {

            LTAMessageImportance enmImportance;

            switch(messageImportance) {
                case MessageImportance.Essential:
                    enmImportance = LTAMessageImportance.Essential;
                    break;
                case MessageImportance.Verbose:
                    enmImportance = LTAMessageImportance.Verbose;
                    break;
                case MessageImportance.Debug:
                    enmImportance = LTAMessageImportance.Debug;
                    break;
                default:
                    enmImportance = LTAMessageImportance.Essential;
                    break;
            }

            return(enmImportance);
        }
    }
}
