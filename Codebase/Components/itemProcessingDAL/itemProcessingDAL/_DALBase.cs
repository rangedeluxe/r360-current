﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53789 JMC 07/17/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.itemProcessing.DAL {

    public abstract class _DALBase : _DMPObjectRoot {

        private cError _LastError = null;

        public _DALBase(string siteKey) : base(siteKey) {
        }

        public void BeginTrans() {
            Database.beginTrans();
        }
        
        public void CommitTrans() {
            Database.commitTrans();
        }
        
        public void RollbackTrans() {
            Database.rollbackTrans();
        }
        
        public bool SetDeadlockPriority {
            get {
                return(Database.SetDeadlockPriority);
            }
            set {
                Database.SetDeadlockPriority = value;
            }
        }

        protected bool ExecuteSQL(string SQL, out DataTable dt) {

            DataTable dtRetVal = null;
            bool bolRetVal;

            try {
                bolRetVal = Database.CreateDataTable(SQL, out dtRetVal);

                if(bolRetVal) {
                    dt = dtRetVal;
                } else {
                    if(dtRetVal != null) {
                        dtRetVal.Dispose();
                    }
                    dt = null;
                }
            } catch(Exception ex) {
                if(dtRetVal != null) {
                    dtRetVal.Dispose();
                }
                dt = null;
                _LastError = new cError(-1, ex.Message);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        protected bool ExecuteSQL(string SQL) {

            bool bolRetVal;

            try {
                bolRetVal = (Database.executeNonQuery(SQL) >= 0);

            } catch(Exception ex) {
                _LastError = new cError(-1, ex.Message);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        protected bool ExecuteNonScalar(string SQL)
        {
            bool bolRetVal;

            try
            {
                bolRetVal = (Database.executeNonScalarQuery(SQL) == -1);
            }
            catch (Exception ex)
            {
                _LastError = new cError(-1, ex.Message);
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        protected bool ExecuteSQL(string SQL, out int RowsAffected) {

            bool bolRetVal;

            try {
                RowsAffected = Database.executeNonQuery(SQL);
                bolRetVal = (RowsAffected >= 0);

            } catch(Exception ex) {
                _LastError = new cError(-1, ex.Message);
                RowsAffected = -1;
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        protected static SqlParameter BuildParameter(string parameterName, SqlDbType sqlDbType, object Value, ParameterDirection direction) {

            SqlParameter objParameter = new SqlParameter();
            int intSize = 0;
            int i;
            
            objParameter.ParameterName = parameterName;
            objParameter.SqlDbType = sqlDbType;
            objParameter.Value = Value;
            objParameter.Direction = direction;
            
            if(sqlDbType == SqlDbType.Xml) {
                if(Value == null) {
                    objParameter.Size = 4096;
                } else {
                    i = 10;
                    intSize = 2^i;
                    while(intSize < Value.ToString().Length) {
                        ++i;
                        intSize = 2^i;                    
                    }
                    objParameter.Size = intSize;
                }
            }

            return(objParameter);
        }

        protected static SqlParameter BuildParameter(string parameterName, SqlDbType sqlDbType, int size, string value) {

            SqlParameter objParameter = new SqlParameter();
            
            objParameter.ParameterName = parameterName;
            objParameter.SqlDbType = sqlDbType;
            objParameter.Size = size;
            objParameter.Value = value;

            return(objParameter);
        }

        protected static SqlParameter BuildReturnValueParameter(string parameterName, SqlDbType sqlDbType) {

            SqlParameter objParameter = new SqlParameter();
            
            objParameter.ParameterName = parameterName;
            objParameter.SqlDbType = sqlDbType;
            objParameter.Direction = ParameterDirection.ReturnValue;

            return(objParameter);
        }

        /// <summary>
        /// 
        /// </summary>
        public cError GetLastError {
            get {
                if(_LastError == null) { 
                    _LastError = new cError(0, string.Empty);
                }
                return(_LastError);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class cError {

        private int _Number = 0;
        private string _Description = string.Empty;

        public cError(int Number, string vDescription) {
            _Number = -1;
            _Description = vDescription;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Number {
            get {
                return(_Number);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Description {
            get {
                return(_Description);
            }
        }
    }
}
