﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date:   
*
* Purpose: Notifications for integra pay
*
* Modification History
* WI 72249 TWE 03/26/2013
*    moved integraPay queries to itemProcessingDAL 
******************************************************************************/

namespace WFS.itemProcessing.DAL
{
    public class cIPCENDSDAL : _DALBase
    {

        public cIPCENDSDAL(string vSiteKey)
            : base(vSiteKey)
        {
        }

        #region Contacts

        /// <summary>
        /// Returns a Contact row associated with the specified ContactID.
        /// </summary>
        /// <param name="ContactID"></param>
        /// <returns></returns>
        public bool GetContactByID(Guid ContactID, out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetContactByID(ContactID), out dt));
        }

        /// <summary>
        /// Returns a Contact based on BankID/CustomerID/LockboxID/ContactName.  If Lockbox == -1
        /// then we are looking for a Customer-Level Contact, otherwise, it will return a Lockbox
        /// Level Contact.
        /// </summary>
        /// <param name="BankID"></param>
        /// <param name="CustomerID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="ContactName"></param>
        /// <returns></returns>
        public bool GetContactByName(int BankID,
                                     int CustomerID,
                                     int LockboxID,
                                     string ContactName,
                                     out DataTable dt)
        {

            return (ExecuteSQL(SQLAlert.SQL_GetContactByName(BankID,
                                                   CustomerID,
                                                   LockboxID,
                                                   ContactName), out dt));
        }

        /// <summary>
        /// Returns all Customer-Level Contacts associated with the Online User.
        /// </summary>
        /// <param name="UserCustomers"></param>
        /// <returns></returns>
        public bool GetOnlineContacts(List<cCustomer> UserCustomers, out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetOnlineContacts(UserCustomers), out dt));
        }

        /// <summary>
        /// Returns all Customer-Level Contacts for the specified Customer.
        /// </summary>
        /// <param name="BankID"></param>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        public bool GetCustomerContacts(int BankID, int CustomerID, out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetCustomerContacts(BankID, CustomerID), out dt));
        }

        public bool UpdateContact(cContact Contact, bool IsActive, out int RowsAffected)
        {
            return (ExecuteSQL(SQLAlert.SQL_UpdateContact(Contact, IsActive), out RowsAffected));
        }

        #endregion Contacts

        #region Alerts
        public bool GetOnlineEventRuleByID(Guid EventRuleID, out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetOnlineEventRuleByID(EventRuleID), out dt));
        }

        public bool CreateAlert(cAlert Alert, string LogonName, out int RowsAffected)
        {
            return (ExecuteSQL(SQLAlert.SQL_CreateAlert(Alert, LogonName), out RowsAffected));
        }

        public bool GetAlert(Guid ContactID, Guid DeliveryMethodID, Guid EventRuleID, out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetAlert(ContactID, DeliveryMethodID, EventRuleID), out dt));
        }
        #endregion Alerts

        #region Delivery Methods
        public bool GetDeliveryMethods(out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetDeliveryMethods(), out dt));
        }
        #endregion Delivery Methods

        #region Event Rules
        public bool GetEventRuleByID(Guid EventRuleID, out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetEventRuleByID(EventRuleID), out dt));
        }

        public bool CreateEventRule(cEventRule EventRule, out int RowsAffected)
        {
            return (ExecuteSQL(SQLAlert.SQL_CreateEventRule(EventRule), out RowsAffected));
        }

        public bool UpdateEventRule(cEventRule EventRule,
                                                 string LogonName, out int RowsAffected)
        {
            return (ExecuteSQL(SQLAlert.SQL_UpdateEventRule(EventRule, LogonName), out RowsAffected));
        }

        /// <summary>
        /// Returns all available EventRules.    If SuperUser == false, 
        /// </summary>
        /// <param name="Lockboxes"></param>
        /// <param name="SuperUser">
        ///     If SuperUser == true, all Customer-level and Lockbox-level EventRules are returned.
        ///     If SuperUser == false, only Lockbox-level EventRules are returned.
        /// </param>
        /// <returns></returns>
        public bool GetEventRules(cOLLockboxes Lockboxes, bool SuperUser, out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetEventRules(Lockboxes, SuperUser), out dt));
        }

        #endregion Event Rules

        #region Events
        public bool GetEvents(out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetEvents(), out dt));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetActiveEvents(out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetActiveEvents(), out dt));
        }

        public bool UpdateEvent(int EventID, int iOnlineModifiable)
        {
            return (ExecuteSQL(SQLAlert.SQL_UpdateEvent(EventID, iOnlineModifiable)));
        }
        #endregion Events

        #region Event Arguments
        public bool GetEventArgs(out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetEventArgs(), out dt));
        }
        #endregion Event Arguments

        #region Internet Deliverables
        public bool GetDeliverables(string Deliverable, out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetDeliverables(Deliverable), out dt));
        }

        public bool GetInternetDeliverables(out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetInternetDeliverables(), out dt));
        }

        public bool GetInternetDeliverable(Guid InternetDeliverableID, out DataTable dt)
        {
            return (ExecuteSQL(SQLAlert.SQL_GetInternetDeliverable(InternetDeliverableID), out dt));
        }

        #endregion Internet Deliverables

        public bool AddContact(cContact Contact)
        {

            bool bolRetVal = false;
            SqlParameter parmContactID;
            //int intParmIndex = 0;
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            try
            {

                arParms = new List<SqlParameter>();

                arParms.Add(BuildParameter("@BankID", SqlDbType.Int, Contact.BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@CustomerID", SqlDbType.Int, Contact.CustomerID, System.Data.ParameterDirection.Input));
                arParms.Add(BuildParameter("@LockboxID", SqlDbType.Int, Contact.LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@ContactName", SqlDbType.VarChar, Contact.ContactName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@ContactPin", SqlDbType.Int, null, ParameterDirection.Input));
                arParms.Add(BuildParameter("@ContactPassword", SqlDbType.VarChar, null, ParameterDirection.Input));
                arParms.Add(BuildParameter("@IsActive", SqlDbType.TinyInt, null, ParameterDirection.Input));
                arParms.Add(BuildParameter("@IsPrimary", SqlDbType.TinyInt, null, ParameterDirection.Input));
                arParms.Add(BuildParameter("@SiteName", SqlDbType.VarChar, null, ParameterDirection.Input));
                arParms.Add(BuildParameter("@Address", SqlDbType.VarChar, Contact.Address, ParameterDirection.Input));
                arParms.Add(BuildParameter("@City", SqlDbType.VarChar, Contact.City, ParameterDirection.Input));
                arParms.Add(BuildParameter("@State", SqlDbType.VarChar, Contact.State, ParameterDirection.Input));
                arParms.Add(BuildParameter("@Zip", SqlDbType.VarChar, Contact.Zip, ParameterDirection.Input));
                arParms.Add(BuildParameter("@Voice", SqlDbType.VarChar, Contact.Voice, ParameterDirection.Input));
                arParms.Add(BuildParameter("@Fax", SqlDbType.VarChar, Contact.Fax, ParameterDirection.Input));
                arParms.Add(BuildParameter("@Modem", SqlDbType.VarChar, null, ParameterDirection.Input));
                arParms.Add(BuildParameter("@Email", SqlDbType.VarChar, Contact.Email, ParameterDirection.Input));
                arParms.Add(BuildParameter("@CreatedBy", SqlDbType.VarChar, Contact.CreatedBy, ParameterDirection.Input));
                arParms.Add(BuildParameter("@ModifiedBy", SqlDbType.VarChar, Contact.ModifiedBy, ParameterDirection.Input));
                arParms.Add(BuildParameter("@TextPager", SqlDbType.VarChar, string.Empty, ParameterDirection.Input));
                arParms.Add(BuildParameter("@IsOnline", SqlDbType.Bit, 1, ParameterDirection.Input));
                arParms.Add(BuildParameter("@ContactID", SqlDbType.UniqueIdentifier, Guid.Empty, ParameterDirection.InputOutput));

                parms = arParms.ToArray();

                Database.executeProcedure("proc_ContactsInsert", parms);

                parmContactID = parms.Single(parm => parm.ParameterName == "@ContactID");

                if (parmContactID.Value != null && (Guid)parmContactID.Value != Guid.Empty)
                {
                    Contact.ContactID = (Guid)parmContactID.Value;
                    bolRetVal = true;
                }

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "AddContact");
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        public bool AddAlert(cAlert Alert, string LogonName)
        {
            return (ExecuteSQL(SQLAlert.SQL_CreateAlert(Alert, LogonName)));
        }

        public bool DeleteAlert(cAlert Alert)
        {

            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            try
            {

                arParms = new List<SqlParameter>();

                arParms.Add(BuildParameter("@iAlertID", SqlDbType.UniqueIdentifier, Alert.AlertID, ParameterDirection.Input));

                parms = arParms.ToArray();

                Database.executeProcedure("proc_AlertsDelete", parms);

                bolRetVal = true;

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DeleteAlert");
                bolRetVal = false;
            }

            return (bolRetVal);
        }

    }
}
