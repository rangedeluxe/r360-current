using System;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Database;
using WFS.RecHub.DAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53789 JMC 07/17/2012 
*   -Initial version.
* WI 72252 TWE 12/20/2012
*  - Move integraPAY DAL queries from cResearchDAL to itemProcessingDAL
* WI 94256 TWE 03/27/2013
*    Change ItemProcessing DAL to use its own database connection string
*******************************************************************************/
namespace WFS.itemProcessing.DAL {

    /// <summary>
    /// ipo Image Service Object Root class.
    /// </summary>
    public abstract class _DMPObjectRoot : IDisposable {

        protected cSiteOptions _SiteOptions = null;
        protected cDatabase _Database = null;
        protected ltaLog _EventLog = null;
        protected string _SiteKey;

        // Track whether Dispose has been called.
        private bool disposed = false;        

        public event LTAOutputLogEventHandler OutputLog;
        public event LTAOutputTraceEventHandler OutputTrace;
        public event LTAOutputLogAndTraceEventHandler OutputLogAndTrace;
        public event LTAOutputLogAndTraceExceptionEventHandler OutputLogAndTraceException;

        public _DMPObjectRoot(string vSiteKey) {
            this.SiteKey = vSiteKey;
        }

        /// <summary>
        /// Determines the section of the local .ini file to be used for local 
        /// options.
        /// </summary>
        public string SiteKey {
            set { _SiteKey = value; }
            get { return _SiteKey; }
        }

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        internal cSiteOptions SiteOptions {
            get {
                if(_SiteOptions == null) { 
                    _SiteOptions = new cSiteOptions(this.SiteKey);
                }

                return _SiteOptions;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        internal ltaLog EventLog {
            get {
                if (_EventLog == null) { 
                    _EventLog = new ltaLog(SiteOptions.logFilePath, 
                                           SiteOptions.logFileMaxSize, 
                                           (LTAMessageImportance)SiteOptions.loggingDepth); 
                }

                return _EventLog;
            }
        }

        /// <summary>
        /// Database component using settings from the local siteOptions object.
        /// </summary>
        internal cDatabase Database {
            get {
                if (SiteOptions.integraPayIsEnabled)
                {
                    if (_Database == null)
                    {
                        _Database = new cDatabase(SiteOptions.connectionStringIP);

                        _Database.SetDeadlockPriority = SiteOptions.SetDeadlockPriority;
                        _Database.QueryRetryAttempts = SiteOptions.QueryRetryAttempts;

                        _Database.outputMessage += new OutputDbMessageEventHandler(Object_OutputMessage);
                        _Database.outputError += new OutputDbErrorEventHandler(Object_OutputError);
                    }
                }
                else
                {
                    //IntegraPay (dbo schema) has not been enabled
                    EventLog.logEvent("IntegraPay access is Disabled:  IntegraPayEnabled=False in the INI file ",
                        this.GetType().ToString(), 
                        WFS.LTA.Common.LTAMessageImportance.Essential);
                }

                return _Database;
            }
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output messages.
        /// </summary>
        /// <param name="message">Test message to be logged.</param>
        /// <param name="src">Source of the event.</param>
        /// <param name="LTAMessageType">LTAMessageType(Information, Warning, Error)</param>
        /// <param name="LTAMessageImportance">LTAMessageImportance(Essential, Verbose, Debug)
        /// </param>
        protected void Object_OutputMessage(
            string msg, 
            string src, 
            DbMessageType messageType, 
            DbMessageImportance messageImportance) {

            OnOutputLogAndTrace(
                msg, 
                src,
                itemProcessingDALLib.ConvertIPOtoLTA(WFS.RecHub.DAL.LTAConvert.ConvertMessageType(messageType)),
                itemProcessingDALLib.ConvertIPOtoLTA(WFS.RecHub.DAL.LTAConvert.ConvertMessageImportance(messageImportance)));
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output errors.
        /// </summary>
        /// <param name="e"></param>
        protected void Object_OutputError(Exception ex) {
            OnOutputLogAndTraceException(
                "Exception Occurred", 
                this.GetType().Name, 
                ex);
        }

        protected void OnOutputLog(
            string msg, 
            string src, 
            LTAMessageType messageType, 
            LTAMessageImportance messageImportance) {

            if(OutputLog == null) {
                EventLog.logEvent(msg, src, messageImportance); 
            } else {
                OutputLog(msg, src, messageType, messageImportance);
            }
        }

        protected void OnOutputTrace(
            string msg, 
            LTAConsoleMessageType messageType) {

            if(OutputTrace == null) {
                LTAConsole.ConsoleWriteLine(msg);
            } else {
                OutputTrace(msg, messageType);
            }
        }

        protected void OnOutputLogAndTrace(
            string msg, 
            string src, 
            LTAMessageType messageType, 
            LTAMessageImportance messageImportance) 
        {
            if (OutputLogAndTrace == null)
            {
                OnOutputLog(msg, src, messageType, messageImportance);
            }
            else
            {
                OutputLogAndTrace(msg, src, messageType, messageImportance);
            }
        }

        protected void OnOutputLogAndTraceException(
            string msg, 
            string src, 
            Exception ex) {

            if(OutputLogAndTraceException == null) {
                LTAConsole.ConsoleWriteLine(msg);
            } else {
                OutputLogAndTraceException(msg, src, ex);
            }
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose() {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing) {
            // Check to see if Dispose has already been called.
            if(!this.disposed) {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if(disposing) {
                    // Dispose managed resources.
                    if(_Database != null) {
                        _Database.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }

        // Use interop to call the method necessary
        // to clean up the unmanaged resource.
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~_DMPObjectRoot() {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }

    }
}
