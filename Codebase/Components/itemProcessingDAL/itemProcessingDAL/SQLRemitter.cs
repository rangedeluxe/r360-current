﻿using System;
using System.Text;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose: 
*
* Modification History
* WI 90244 CRG 02/21/2013 
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
* WI 72251 TWE 03/26/2013
*    moved integraPay queries to itemProcessingDAL 
******************************************************************************/

namespace WFS.itemProcessing.DAL
{

    internal static class SQLRemitter {

        public static string SQL_GetAllRemitters(string OLCustomerID, cOLLockboxes objLockboxes) {
            
            string strLockboxes = string.Empty;
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" DECLARE @parmOLCustomerID uniqueidentifier");
            sbSQL.Append("");
            sbSQL.Append(" SET @parmOLCustomerID = '" + OLCustomerID.ToString() + "'");
            sbSQL.Append("");
            sbSQL.Append(" SELECT ");
            sbSQL.Append("      RTRIM(GlobalRemitter.RoutingNumber)        AS RoutingNumber");
            sbSQL.Append("     , RTRIM(GlobalRemitter.Account)             AS Account");
            sbSQL.Append("     , LockboxRemitter.LockboxRemitterID");
            sbSQL.Append("     , LockboxRemitter.LockboxID");
            sbSQL.Append("     , LockboxRemitter.BankID");
            sbSQL.Append("     , RTRIM(LockboxRemitter.RemitterName)       AS RemitterName");
            sbSQL.Append("     , LockboxRemitter.ModificationDate");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     GlobalRemitter");
            sbSQL.Append("     INNER JOIN LockboxRemitter");
            sbSQL.Append("         INNER JOIN OLLockboxes ON");
            sbSQL.Append("             LockboxRemitter.BankID = OLLockboxes.BankID");
            sbSQL.Append("             AND LockboxRemitter.LockboxID = OLLockboxes.LockboxID ON");
            sbSQL.Append("         GlobalRemitter.GlobalRemitterID = LockboxRemitter.GlobalRemitterID");
            sbSQL.Append(" WHERE");
            sbSQL.Append("     OLLockboxes.OLCustomerID=@parmOLCustomerID");

            if(objLockboxes != null) {

                if(objLockboxes.Count > 0) {

                    foreach(cLockbox objLockbox in objLockboxes.Values) {
                        strLockboxes = strLockboxes + objLockbox.LockboxID + ",";
                    }

                    strLockboxes = strLockboxes.Substring(0, strLockboxes.Length - 1);
                    sbSQL.Append (" AND LockboxRemitter.LockboxID IN(" + strLockboxes + ")");

                } else {
                    throw(new Exception("There are no Lockboxes in the system for you to view. Please check back later."));
                }
            }

            sbSQL.Append(" ORDER BY");
            sbSQL.Append("  LockboxRemitter.LockboxID,");
            sbSQL.Append("    LockboxRemitter.RemitterName ASC");

            return sbSQL.ToString();
        }

        public static string SQL_GetLockboxRemitter(int BankID, int CustomerID, int LockboxID, string RoutingNumber, string AccountNumber) {

            return("EXEC proc_LockboxRemitterSelect @BankID=" + BankID.ToString() + 
                   ", @CustomerID=" + CustomerID.ToString() + 
                   ", @LockboxID=" + LockboxID.ToString() + 
                   ", @RoutingNumber='" + RoutingNumber + "'" +
                   ", @Account='" + AccountNumber + "'");
        }
        
        public static string SQL_GetLockboxRemitter(Guid LockboxRemitterID) {

            string strSQL;

            strSQL = "EXEC proc_LockboxRemitterSelect @LockboxRemitterID='";
            strSQL += LockboxRemitterID.ToString();
            strSQL += "'";

            return(strSQL);
        }

        public static string SQL_GetGlobalRemitter(string RoutingNumber, 
                                                   string AccountNumber) {

            return("EXEC proc_GlobalRemitterSelect @RoutingNumber='" + RoutingNumber + 
                                               "', @Account='" + AccountNumber + "'");
        }

        //public static string SQL_UpdateChecksRemitter(string LockboxRemitterName, 
        //                                              int BankID,
        //                                              int LockboxID,
        //                                              int BatchID,
        //                                              DateTime DepositDate,
        //                                              int TransactionID, 
        //                                              int BatchSequence) {

        //   // #if USE_ITEMPROC
        //    //return(" UPDATE Checks" +
        //    //       " SET RemitterName = '" + SQLPublic.SQLEncodeString(LockboxRemitterName) + "'" +
        //    //       " WHERE GlobalBatchID = " + GlobalBatchID + 
        //    //       " AND TransactionID = " + TransactionID + 
        //    //       " AND GlobalCheckID = " + GlobalCheckID);
        //    //#else
        //    //return ("EXEC usp_UpdateChecksRemitter"
        //    //     + "    @parmSiteBankID = " + BankID.ToString()
        //    //     + "  , @parmSiteLockboxID = " + LockboxID.ToString()
        //    //     + "  , @parmBatchID = " + BatchID.ToString()
        //    //     + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
        //    //     + "  , @parmTransactionID = " + TransactionID.ToString()
        //    //     + "  , @parmBatchSequence = " + BatchSequence.ToString()
        //    //     + "  , @parmRemitterName = '" + SQLPublic.SQLEncodeString(LockboxRemitterName) + "'");
        //    //#endif
        //}
    }
}
