﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53789 JMC 07/17/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.itemProcessing.DAL
{
    public class cNotificationsDAL : _DALBase {

        public cNotificationsDAL(string vSiteKey) : base(vSiteKey) {
        }

        ////public bool GetNotifications(DateTime LastNotificationCheck, DateTime NotificationCheck, out DataTable dt) {
        ////    return(ExecuteSQL(SQLNotifications.SQL_GetNotifications(LastNotificationCheck, NotificationCheck), out dt));
        ////}

        public bool GetNotifications(
            int retentionDays,
            int maxRows,
            out DataTable dtNotifications) {

            SqlParameter[] parms;
            bool bolRetVal;
            List<SqlParameter> arParms;

            const string PROCNAME = "dbo.usp_FITGetPendingNotifications";

            try {
                
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmRetentionDays", SqlDbType.Int, 0, retentionDays.ToString()));
                arParms.Add(BuildParameter("@parmMaxRows", SqlDbType.Int, 0, maxRows.ToString()));

                parms = arParms.ToArray();
                
                bolRetVal = Database.executeProcedure(PROCNAME, parms, out dtNotifications);

            } catch(Exception ex) {
                
                OnOutputLogAndTraceException("Unable to execute procedure: " + PROCNAME, this.GetType().Name, ex);
                
                dtNotifications = null;
                
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        //public bool UpsertNotificationQueueStatus(Guid integraPAYNotificationID, int queueStatus, bool incrementRetryCount, out int rowsAffected) {
        //    return(ExecuteSQL(SQLNotifications.SQL_UpsertNotificationQueueStatus(integraPAYNotificationID, queueStatus, incrementRetryCount), out rowsAffected));
        //}

        public bool UpsertNotificationQueueStatus(
            Guid integraPAYNotificationID, 
            Guid sourceTrackingID,
            int currentQueueStatus, 
            int queueStatus, 
            bool incrementRetryCount, 
            out int rowsAffected) {

            SqlParameter[] parms;
            bool bolRetVal;
            List<SqlParameter> arParms;

            const string PROCNAME = "dbo.usp_FITUpsertNotificationQueueStatus";
            
            try {
                
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmNotificationID", SqlDbType.UniqueIdentifier, integraPAYNotificationID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSourceTrackingID", SqlDbType.UniqueIdentifier, sourceTrackingID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmCurrentQueueStatus", SqlDbType.Int, 0, currentQueueStatus.ToString()));
                arParms.Add(BuildParameter("@parmNewQueueStatus", SqlDbType.Int, 0, queueStatus.ToString()));
                arParms.Add(BuildParameter("@parmIncrementRetryCount", SqlDbType.Bit, (incrementRetryCount ? true : false), ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmRowsAffected", SqlDbType.Int, 0, ParameterDirection.Output));

                parms = arParms.ToArray();

                bolRetVal = Database.executeProcedure(PROCNAME, parms);

                rowsAffected = (int)parms[5].Value;


            } catch(Exception ex) {
                
                OnOutputLogAndTraceException("Unable to execute procedure: " + PROCNAME, this.GetType().Name, ex);
                
                rowsAffected = 0;
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        public bool UpdateNotificationQueueStatusComplete(
            Guid sourceTrackingID, 
            int currentQueueStatus, 
            int queueStatus, 
            bool incrementRetryCount, 
            out int rowsAffected) {

            SqlParameter[] parms;
            bool bolRetVal;
            List<SqlParameter> arParms;

            const string PROCNAME = "dbo.usp_FITUpdateNotificationQueueStatusComplete";
            
            try {
                
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSourceTrackingID", SqlDbType.UniqueIdentifier, sourceTrackingID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmCurrentQueueStatus", SqlDbType.Int, 0, currentQueueStatus.ToString()));
                arParms.Add(BuildParameter("@parmNewQueueStatus", SqlDbType.Int, 0, queueStatus.ToString()));
                arParms.Add(BuildParameter("@parmIncrementRetryCount", SqlDbType.Bit, (incrementRetryCount ? true : false), ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmRowsAffected", SqlDbType.Int, 0, ParameterDirection.Output));

                parms = arParms.ToArray();
                
                bolRetVal = Database.executeProcedure(PROCNAME, parms);

                rowsAffected = (int)parms[4].Value;


            } catch(Exception ex) {
                
                OnOutputLogAndTraceException("Unable to execute procedure: " + PROCNAME, this.GetType().Name, ex);
                
                rowsAffected = 0;
                bolRetVal = false;
            }

            return(bolRetVal);
        }
    }
}
