﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date:   March 26, 2013
*
* Purpose: Data Access Layer Class for Admin (integrapay schema)
*
* Modification History
* WI 72249 TWE 03/26/2013
*    moved integraPay queries to itemProcessingDAL 
******************************************************************************/

namespace WFS.itemProcessing.DAL
{
    internal static class SQLAlert
    {
        #region Contacts

        /// <summary>
        /// Returns a Contact row associated with the specified ContactID.
        /// </summary>
        /// <param name="ContactID"></param>
        /// <returns></returns>
        public static string SQL_GetContactByID(Guid ContactID)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" DECLARE @parmContactID uniqueidentifier");
            sbSQL.Append("");
            sbSQL.Append(" SET @parmContactID = '" + ContactID.ToString() + "'");
            sbSQL.Append("");
            sbSQL.Append(" SELECT");
            sbSQL.Append(SQL_ContactsColumns());
            sbSQL.Append(" FROM Contacts");
            sbSQL.Append(" 	INNER JOIN Customer ON");
            sbSQL.Append(" 		Contacts.BankID = Customer.BankID AND");
            sbSQL.Append(" 		Contacts.CustomerID = Customer.CustomerID");
            sbSQL.Append(" WHERE Contacts.ContactID = @parmContactID");

            return (sbSQL.ToString());
        }

        /// <summary>
        /// Returns a Contact row associated with the specified ContactID.
        /// </summary>
        /// <param name="ContactID"></param>
        /// <returns></returns>
        public static string SQL_GetContactByName(int BankID,
                                                  int CustomerID,
                                                  int LockboxID,
                                                  string ContactName)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" DECLARE @parmContactName varchar(41)");
            sbSQL.Append("");
            sbSQL.Append(" SET @parmContactName = '" + SQLPublic.SQLEncodeString(ContactName) + "'");
            sbSQL.Append("");
            sbSQL.Append(" SELECT");
            sbSQL.Append(SQL_ContactsColumns());
            sbSQL.Append(" FROM Contacts");
            sbSQL.Append(" 	INNER JOIN Customer ON");
            sbSQL.Append(" 		Contacts.BankID = Customer.BankID AND");
            sbSQL.Append(" 		Contacts.CustomerID = Customer.CustomerID");
            sbSQL.Append(" WHERE Contacts.ContactName = @parmContactName");

            return (sbSQL.ToString());
        }

        /// <summary>
        /// Returns all Customer-Level Contacts associated with the Online User.
        /// </summary>
        /// <param name="UserCustomers"></param>
        /// <returns></returns>
        public static string SQL_GetOnlineContacts(List<cCustomer> UserCustomers)
        {

            StringBuilder sbSQL = new StringBuilder();
            //StringBuilder SQL_Where = new StringBuilder();
            //bool bolIsFirst;
            string strFilter;

            // Get Customer Contacts for User Lockboxes
            sbSQL.Append(" SELECT");
            sbSQL.Append(SQL_ContactsColumns());
            sbSQL.Append(" FROM Contacts");
            sbSQL.Append(" 	INNER JOIN Customer ON");
            sbSQL.Append(" 		Contacts.BankID = Customer.BankID AND");
            sbSQL.Append(" 		Contacts.CustomerID = Customer.CustomerID");
            sbSQL.Append(" WHERE Contacts.IsActive = 1");
            sbSQL.Append("      AND Contacts.LockboxID = -1");

            // Add unique Bank/Customers to sql where clause
            sbSQL.Append(" AND(");
            if (BuildLockboxFilter(UserCustomers, "Contacts", out strFilter))
            {
                sbSQL.Append(strFilter);
            }
            else
            {
                throw (new Exception("Cannot query Online Contacts without filter criteria."));
            }

            /*bolIsFirst = true;
            foreach(cCustomer customer in UserCustomers) {
                if(bolIsFirst) {
                    bolIsFirst = false;
                } else {
                    SQL_Where.Append(" OR ");
                }

                SQL_Where.Append("(Contacts.BankID=" + customer.BankID.ToString() + " AND Contacts.CustomerID=" + customer.CustomerID.ToString() + ")");
            }*/
            sbSQL.Append(")");

            //sbSQL.Append(SQL_Where);
            sbSQL.Append(" ORDER BY");
            sbSQL.Append("      Contacts.BankID,");
            sbSQL.Append("      Contacts.CustomerID,");
            sbSQL.Append("      Contacts.LockboxID,");
            sbSQL.Append("      Contacts.ContactName");

            return (sbSQL.ToString());
        }

        /// <summary>
        /// Returns all Customer-Level Contacts for the specified Customer.
        /// </summary>
        /// <param name="BankID"></param>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        public static string SQL_GetCustomerContacts(int BankID, int CustomerID)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT");
            sbSQL.Append(SQL_ContactsColumns());
            sbSQL.Append(" FROM Contacts");
            sbSQL.Append(" 	INNER JOIN Customer ON");
            sbSQL.Append(" 		Contacts.BankID = Customer.BankID AND");
            sbSQL.Append(" 		Contacts.CustomerID = Customer.CustomerID");
            sbSQL.Append(" WHERE Contacts.BankID = " + BankID);
            sbSQL.Append("      AND Contacts.CustomerID = " + CustomerID);
            sbSQL.Append("      AND Contacts.LockboxID = -1");
            sbSQL.Append("      AND Contacts.IsActive = 1");
            sbSQL.Append(" ORDER BY Contacts.ContactName ASC");

            return (sbSQL.ToString());
        }

        private static StringBuilder SQL_ContactsColumns()
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" Contacts.BankID,");
            sbSQL.Append(" Contacts.CustomerID,");
            sbSQL.Append(" Contacts.LockboxID,");
            sbSQL.Append(" Contacts.ContactID,");
            sbSQL.Append(" RTrim(OLTA.dimCustomersView.Name)    AS CustomerName,");
            sbSQL.Append(" RTrim(Contacts.ContactName)      AS ContactName,");
            sbSQL.Append(" RTrim(Contacts.Address)          AS Address,");
            sbSQL.Append(" RTrim(Contacts.City)             AS City,");
            sbSQL.Append(" RTrim(Contacts.State)            AS State,");
            sbSQL.Append(" RTrim(Contacts.Zip)              AS Zip,");
            sbSQL.Append(" RTrim(Contacts.Voice)            AS Voice,");
            sbSQL.Append(" RTrim(Contacts.Fax)              AS Fax,");
            sbSQL.Append(" RTrim(Contacts.Email)            AS Email,");
            sbSQL.Append(" Contacts.IsActive,");
            sbSQL.Append(" Contacts.ModificationDate");

            return (sbSQL);
        }

        public static string SQL_UpdateContact(cContact Contact,
                                               bool IsActive)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" UPDATE Contacts ");
            sbSQL.Append(" SET");
            sbSQL.Append("     ContactName='" + SQLPublic.SQLEncodeString(Contact.ContactName) + "'");
            sbSQL.Append("     ,Address='" + SQLPublic.SQLEncodeString(Contact.Address) + "'");
            sbSQL.Append("     ,City='" + SQLPublic.SQLEncodeString(Contact.City) + "'");
            sbSQL.Append("     ,State='" + SQLPublic.SQLEncodeString(Contact.State) + "'");
            sbSQL.Append("     ,Zip='" + SQLPublic.SQLEncodeString(Contact.Zip) + "'");
            sbSQL.Append("     ,Voice='" + SQLPublic.SQLEncodeString(Contact.Voice) + "'");
            sbSQL.Append("     ,Fax='" + SQLPublic.SQLEncodeString(Contact.Fax) + "'");
            sbSQL.Append("     ,Email='" + SQLPublic.SQLEncodeString(Contact.Email) + "'");
            sbSQL.Append("     ,LockboxID=" + Contact.LockboxID + "");
            sbSQL.Append("     ,IsActive=" + (IsActive ? '1' : '0') + "");
            sbSQL.Append("     ,ModificationDate=getDate()");
            sbSQL.Append("     ,ModifiedBy='" + Contact.ModifiedBy + "'");
            sbSQL.Append(" WHERE");
            sbSQL.Append("     ContactID='" + Contact.ContactID + "'");

            return (sbSQL.ToString());
        }

        #endregion Contacts

        #region Alerts
        public static string SQL_GetOnlineEventRuleByID(Guid EventRuleID)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT ");
            sbSQL.Append("      Alerts.AlertID");
            sbSQL.Append("     ,Alerts.DeliveryMethodID");
            sbSQL.Append("     ,Alerts.ContactID");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     Alerts");
            sbSQL.Append(" WHERE ");
            sbSQL.Append("     Alerts.EventRuleID = '" + EventRuleID + "'");

            return (sbSQL.ToString());
        }

        public static string SQL_CreateAlert(cAlert Alert,
                                      string LogonName)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" INSERT INTO Alerts");
            sbSQL.Append("     (EventRuleID");
            sbSQL.Append("     ,ContactID");
            sbSQL.Append("     ,DeliveryMethodID");
            sbSQL.Append("     ,CreatedBy");
            sbSQL.Append("     ,ModifiedBy)");
            sbSQL.Append(" VALUES");
            sbSQL.Append("     ('" + Alert.EventRuleID + "'");
            sbSQL.Append("     ,'" + Alert.ContactID + "'");
            sbSQL.Append("     ,'" + Alert.DeliveryMethodID + "'");
            sbSQL.Append("     ,'" + LogonName + "'");
            sbSQL.Append("     ,'" + LogonName + "')");

            return (sbSQL.ToString());
        }

        public static string SQL_GetAlert(Guid ContactID, Guid DeliveryMethodID, Guid EventRuleID)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT ");
            sbSQL.Append("     AlertID ");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     Alerts");
            sbSQL.Append(" WHERE ");
            sbSQL.Append("         ContactID = '" + ContactID.ToString() + "'");
            sbSQL.Append("     AND DeliveryMethodID = '" + DeliveryMethodID.ToString() + "'");
            sbSQL.Append("     AND EventRuleID = '" + EventRuleID.ToString() + "'");

            return (sbSQL.ToString());
        }
        #endregion Alerts

        #region Delivery Methods
        public static string SQL_GetDeliveryMethods()
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT ");
            sbSQL.Append("     DeliveryMethodID");
            sbSQL.Append("     ,RTrim(DeliveryMethodName)      AS DeliveryMethodName");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     DeliveryMethods");
            sbSQL.Append(" WHERE ");
            sbSQL.Append("     DeliveryMethodName IN ('Email', 'Fax', 'Internet')");
            sbSQL.Append(" ORDER BY ");
            sbSQL.Append("     DeliveryMethodName ASC");

            return (sbSQL.ToString());
        }
        #endregion Delivery Methods

        #region Event Rules

        public static string SQL_GetEventRuleByID(Guid EventRuleID)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT");
            sbSQL.Append("      EventRules.EventRuleID,");
            sbSQL.Append("      EventRules.EventID,");
            sbSQL.Append("      EventRules.BankID,");
            sbSQL.Append("      EventRules.CustomerID,");
            sbSQL.Append("      RTRIM(OLTA.dimCustomersView.Name)                AS CustomerName,");
            sbSQL.Append("      EventRules.LockboxID,");
            sbSQL.Append("      RTRIM(OLTA.dimLockboxesView.LongName)            AS LockboxName,");
            sbSQL.Append("      EventRules.RuleIsActive,");
            sbSQL.Append("      RTRIM(EventRules.EventOperator)         AS EventOperator,");
            sbSQL.Append("      RTRIM(EventRules.EventArgumentCode)     AS EventArgumentCode,");
            sbSQL.Append("      RTRIM(EventRules.EventRuleDescription)  AS EventRuleDescription,");
            sbSQL.Append("      EventRules.EventRuleValue,");
            sbSQL.Append("      EventRules.OnlineViewable,");
            sbSQL.Append("      EventRules.OnlineModifiable,");
            sbSQL.Append("      EventRules.EventAction,");
            sbSQL.Append("      EventRules.ModificationDate");
            sbSQL.Append(" FROM EventRules");
            sbSQL.Append(" 	INNER JOIN Customer ON");
            sbSQL.Append(" 		EventRules.BankID = Customer.BankID AND");
            sbSQL.Append(" 		EventRules.CustomerID = Customer.CustomerID");
            sbSQL.Append("  LEFT OUTER JOIN Lockbox ON");
            sbSQL.Append("      EventRules.BankID = Lockbox.BankID");
            sbSQL.Append("      AND EventRules.CustomerID = Lockbox.CustomerID");
            sbSQL.Append("      AND EventRules.LockboxID = Lockbox.LockboxID");
            sbSQL.Append(" WHERE EventRules.EventRuleID = '" + EventRuleID + "'");

            return (sbSQL.ToString());
        }

        public static string SQL_CreateEventRule(cEventRule EventRule)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" INSERT INTO EventRules");
            sbSQL.Append("     (EventRuleID");
            sbSQL.Append("     ,EventID");
            sbSQL.Append("     ,BankID");
            sbSQL.Append("     ,CustomerID");
            sbSQL.Append("     ,LockboxID");
            sbSQL.Append("     ,RuleIsActive");
            sbSQL.Append("     ,EventOperator");
            sbSQL.Append("     ,EventArgumentCode");
            sbSQL.Append("     ,EventRuleDescription");
            sbSQL.Append("     ,EventRuleValue");
            sbSQL.Append("     ,EventAction");
            sbSQL.Append("     ,OnlineViewable");
            sbSQL.Append("     ,OnlineModifiable");
            sbSQL.Append("     ,CreatedBy");
            sbSQL.Append("     , ModifiedBy)");
            sbSQL.Append(" VALUES");
            sbSQL.Append("     ('" + EventRule.EventRuleID + "'");
            sbSQL.Append("     ," + EventRule.EventID);
            sbSQL.Append("     ," + EventRule.BankID);
            sbSQL.Append("     ," + EventRule.CustomerID);
            sbSQL.Append("     ," + EventRule.LockboxID);
            sbSQL.Append("     ,1");
            sbSQL.Append("     ,'" + EventRule.EventOperator + "'");
            sbSQL.Append("     ,'" + EventRule.EventArgumentCode + "'");
            sbSQL.Append("     ,'" + SQLPublic.SQLEncodeString(EventRule.EventRuleDescription) + "'");

            if (EventRule.EventRuleValue.Length > 0)
            {
                sbSQL.Append("     ,'" + SQLPublic.SQLEncodeString(EventRule.EventRuleValue) + "'");
            }
            else
            {
                sbSQL.Append("     , NULL");
            }

            sbSQL.Append("     ,'" + SQLPublic.SQLEncodeString(EventRule.EventAction) + "'");
            sbSQL.Append("     ,1");
            sbSQL.Append("     ,1");
            sbSQL.Append("     ,'" + EventRule.CreatedBy + "'");
            sbSQL.Append("     ,'" + EventRule.ModifiedBy + "')");

            return (sbSQL.ToString());
        }

        public static string SQL_UpdateEventRule(cEventRule EventRule,
                                                 string LogonName)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" UPDATE EventRules");
            sbSQL.Append(" SET ");
            sbSQL.Append("      EventID = " + EventRule.EventID);
            sbSQL.Append("     ,LockboxID = " + EventRule.LockboxID);
            sbSQL.Append("     ,RuleIsActive = " + (EventRule.RuleIsActive ? "1" : "0"));
            sbSQL.Append("     ,EventOperator = '" + SQLPublic.SQLEncodeString(EventRule.EventOperator) + "'");
            sbSQL.Append("     ,EventArgumentCode = '" + SQLPublic.SQLEncodeString(EventRule.EventArgumentCode) + "'");
            sbSQL.Append("     ,EventRuleDescription = '" + SQLPublic.SQLEncodeString(EventRule.EventRuleDescription) + "'");

            if (EventRule.EventRuleValue.Length == 0)
            {
                sbSQL.Append("     ,EventRuleValue = NULL");
            }
            else
            {
                sbSQL.Append("     ,EventRuleValue = '" + SQLPublic.SQLEncodeString(EventRule.EventRuleValue) + "'");
            }

            sbSQL.Append("     ,EventAction = '" + SQLPublic.SQLEncodeString(EventRule.EventAction) + "'");
            sbSQL.Append("     ,ModificationDate = getDate()");
            sbSQL.Append("     ,ModifiedBy = '" + SQLPublic.SQLEncodeString(LogonName) + "'");
            sbSQL.Append(" WHERE ");
            sbSQL.Append("      EventRuleID = '" + EventRule.EventRuleID + "'");

            return (sbSQL.ToString());
        }

        /// <summary>
        /// Returns all available EventRules.    If SuperUser == false, 
        /// </summary>
        /// <param name="Lockboxes"></param>
        /// <param name="SuperUser">
        ///     If SuperUser == true, all Customer-level and Lockbox-level EventRules are returned.
        ///     If SuperUser == false, only Lockbox-level EventRules are returned.
        /// </param>
        /// <returns></returns>
        public static string SQL_GetEventRules(cOLLockboxes Lockboxes, bool SuperUser)
        {

            StringBuilder sbSQL = new StringBuilder();
            //StringBuilder SQL_Where = new StringBuilder();
            //StringBuilder SQL_Order;
            //bool bolIsFirst;
            //List<string> arCustomers;
            string strFilter;

            sbSQL.Append(" SELECT EventRules.EventRuleID,");
            sbSQL.Append("      EventRules.CustomerID,");
            sbSQL.Append("      RTRIM(Customer.Name)               AS CustomerName,");
            sbSQL.Append("      EventRules.LockboxID,");
            sbSQL.Append("      RTRIM(Lockbox.LongName)           AS LockboxName,");
            sbSQL.Append("      RTRIM(EventRules.EventRuleDescription)          AS EventRuleDescription,");
            sbSQL.Append("      RTRIM(Events.EventName)                         AS EventName,");
            sbSQL.Append("      RTRIM(EventArguments.EventArgumentDescription)  AS EventArgumentDescription,");
            sbSQL.Append("      RTRIM(EventRules.EventOperator)                 AS EventOperator,");
            sbSQL.Append("      EventRules.EventRuleValue,");
            sbSQL.Append("      EventRules.EventAction,");
            sbSQL.Append("      EventRules.OnlineModifiable");
            sbSQL.Append(" FROM EventRules");
            sbSQL.Append("      INNER JOIN Events ON");
            sbSQL.Append("          EventRules.EventID = Events.EventID");
            sbSQL.Append("      INNER JOIN EventArguments ON");
            sbSQL.Append("          EventRules.EventArgumentCode = EventArguments.EventArgumentCode");
            sbSQL.Append("          AND Events.EventID = EventArguments.EventID");
            sbSQL.Append(" 	INNER JOIN Customer ON");
            sbSQL.Append(" 		EventRules.BankID = Customer.BankID AND");
            sbSQL.Append(" 		EventRules.CustomerID = Customer.CustomerID");
            sbSQL.Append("  LEFT OUTER JOIN Lockbox ON");
            sbSQL.Append("      EventRules.BankID = Lockbox.BankID");
            sbSQL.Append("      AND EventRules.CustomerID = Lockbox.CustomerID");
            sbSQL.Append("      AND EventRules.LockboxID = Lockbox.LockboxID");
            sbSQL.Append(" WHERE EventRules.RuleIsActive = 1");
            sbSQL.Append("      AND EventRules.OnlineViewable = 1");


            // construct dynamic where clause based on user type and lockbox access
            if (SuperUser)
            {

                // filter by customer - unique bankid/customerid                
                sbSQL.Append(" AND(");
                if (BuildLockboxFilter(Lockboxes, "EventRules", true, true, out strFilter))
                {
                    sbSQL.Append(strFilter);
                }
                else
                {
                    throw (new Exception("Cannot query Event Rules without filter criteria."));
                }

                // append the where clause to the statement
                sbSQL.Append(")");

            }
            else
            {

                // filter by user lockboxes
                sbSQL.Append(" AND(");
                if (BuildLockboxFilter(Lockboxes, "EventRules", false, true, out strFilter))
                {
                    sbSQL.Append(strFilter);
                }
                else
                {
                    throw (new Exception("Cannot query Event Rules without filter criteria."));
                }

                sbSQL.Append(")");
            }

            // construct order by clause
            sbSQL.Append(" ORDER BY");
            sbSQL.Append("      EventRules.LockboxID ASC,");
            sbSQL.Append("      Customer.Name ASC,");
            sbSQL.Append("      Customer.CustomerID ASC,");
            sbSQL.Append("      EventRules.EventRuleDescription ASC");

            return (sbSQL.ToString());
        }

        #endregion Event Rules

        #region Events
        public static string SQL_GetEvents()
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT");
            sbSQL.Append("      EventID");
            sbSQL.Append("     ,RTrim(EventName)           AS EventName");
            sbSQL.Append("     ,EventIsActive");
            sbSQL.Append("     ,RTrim(MessageDefault)      AS MessageDefault");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     Events");
            sbSQL.Append(" WHERE ");
            sbSQL.Append("         Events.EventIsActive = 1");
            sbSQL.Append("     AND Events.OnlineViewable = 1");
            sbSQL.Append(" ORDER BY ");
            sbSQL.Append("      Events.EventName");

            return (sbSQL.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string SQL_GetActiveEvents()
        {
            string strSQL;
            strSQL = "SELECT EventID, EventName, MessageDefault, OnlineViewable, OnlineModifiable "
                      + " FROM Events"
                      + " WHERE EventIsActive=1"
                      + " ORDER BY EventName ASC";

            return (strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EventID"></param>
        /// <param name="iOnlineModifiable"></param>
        /// <returns></returns>
        public static string SQL_UpdateEvent(int EventID, int iOnlineModifiable)
        {
            string strSQL;
            strSQL = "UPDATE Events " +
                     "SET OnlineViewable = " + iOnlineModifiable +
                     " WHERE EventID = " + EventID;
            return (strSQL);
        }
        #endregion Events

        #region Event Arguments
        public static string SQL_GetEventArgs()
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT");
            sbSQL.Append("      EventArgumentID");
            sbSQL.Append("     ,Events.EventID");
            sbSQL.Append("     ,RTrim(EventArgumentCode)           AS EventArgumentCode");
            sbSQL.Append("     ,RTrim(EventArgumentDescription)    AS EventArgumentDescription");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     Events INNER JOIN");
            sbSQL.Append("     EventArguments ON Events.EventID = EventArguments.EventID");
            sbSQL.Append(" WHERE ");
            sbSQL.Append("         Events.EventIsActive = 1");
            sbSQL.Append("     AND Events.OnlineViewable = 1");
            sbSQL.Append(" ORDER BY ");
            sbSQL.Append("      Events.EventName");
            sbSQL.Append("     ,EventArguments.EventArgumentDescription ASC");

            return (sbSQL.ToString());
        }
        #endregion Event Arguments

        #region Internet Deliverables
        public static string SQL_GetDeliverables(string Deliverable)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT ");
            sbSQL.Append("     InternetDeliverableID,");
            sbSQL.Append("     Type,");
            sbSQL.Append("     RTrim(DeliverableName)   AS DeliverableName,");
            sbSQL.Append("     RTrim(GroupName)        AS GroupName,");
            sbSQL.Append("     Description");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     InternetDeliverables ");

            switch (Deliverable.ToLower())
            {
                case "report":

                    sbSQL.Append(" WHERE ");
                    sbSQL.Append("     Type = 'report' ");
                    sbSQL.Append("     AND DeliverableName Is Not NULL ");
                    sbSQL.Append(" ORDER BY ");
                    sbSQL.Append("     DeliverableName ASC");

                    break;

                case "report group":

                    sbSQL.Append(" WHERE ");
                    sbSQL.Append("     Type = 'report' ");
                    sbSQL.Append("     AND DeliverableName Is NULL ");
                    sbSQL.Append(" ORDER BY ");
                    sbSQL.Append("     GroupName ASC");

                    break;

                case "extract":

                    sbSQL.Append(" WHERE ");
                    sbSQL.Append("     Type = 'extract' ");
                    sbSQL.Append(" ORDER BY ");
                    sbSQL.Append("     DeliverableName ASC");

                    break;
            }

            return (sbSQL.ToString());
        }

        public static string SQL_GetInternetDeliverables()
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT ");
            sbSQL.Append("     InternetDeliverableID,");
            sbSQL.Append("     Type,");
            sbSQL.Append("     RTrim(DeliverableName)     AS DeliverableName,");
            sbSQL.Append("     GroupName,");
            sbSQL.Append("     Description");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     InternetDeliverables ");
            sbSQL.Append(" ORDER BY ");
            sbSQL.Append("     Type, GroupName, DeliverableName, Description");

            return (sbSQL.ToString());
        }

        public static string SQL_GetInternetDeliverable(Guid InternetDeliverableID)
        {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT ");
            sbSQL.Append("     InternetDeliverableID,");
            sbSQL.Append("     ISNULL(DeliverableName, GroupName) AS Deliverable,");
            sbSQL.Append("     Description");
            sbSQL.Append(" FROM InternetDeliverables");
            sbSQL.Append(" WHERE InternetDeliverableID = '" + InternetDeliverableID.ToString() + "'");

            return (sbSQL.ToString());
        }

        #endregion Internet Deliverables

        #region BuildLockboxFilter

        /// <summary>
        /// Returns a portion of a SQL statement that can be appended to a WHERE clause and
        /// will restrict a query to return results that include data for all specified Customers.
        /// </summary>
        /// <param name="UserLockboxes"></param>
        /// <param name="TableName"></param>
        /// <param name="SQLFilter"></param>
        /// <returns></returns>
        private static bool BuildLockboxFilter(List<cCustomer> UserCustomers,
                                               string TableName,
                                               out string SQLFilter)
        {

            string strFilter;
            List<string> arFilters = new List<string>();

            foreach (cCustomer customer in UserCustomers)
            {
                strFilter = "(Contacts.BankID=" + customer.BankID.ToString() +
                            " AND Contacts.CustomerID=" + customer.CustomerID.ToString() + ")";
                if (!arFilters.Contains(strFilter))
                {
                    arFilters.Add(strFilter);
                }
            }

            return (CatSQLFilters(arFilters, out SQLFilter));
        }

        /// <summary>
        /// Returns a portion of a SQL statement that can be appended to a WHERE clause and
        /// will restrict a query to return results that include data for all Customers
        /// associated with the specified Lockboxes.
        /// </summary>
        /// <param name="UserLockboxes"></param>
        /// <param name="TableName"></param>
        /// <param name="SQLFilter"></param>
        /// <returns></returns>
        private static bool BuildLockboxFilter(cOLLockboxes UserLockboxes,
                                               string TableName,
                                               out string SQLFilter)
        {

            string strFilter;
            List<string> arFilters = new List<string>();

            foreach (cLockbox lbx in UserLockboxes.Values)
            {
                strFilter = "(" + TableName + ".BankID=" + lbx.BankID.ToString() +
                            " AND " + TableName + ".CustomerID=" + lbx.CustomerID.ToString() + ")";
                if (!arFilters.Contains(strFilter))
                {
                    arFilters.Add(strFilter);
                }
            }

            return (CatSQLFilters(arFilters, out SQLFilter));
        }

        /// <summary>
        /// Returns a portion of a SQL statement that can be appended to a WHERE clause and
        /// will restrict a query to return results.  When including customer level data, 
        /// the customer ID will be -1.  When including Lockbox data, the Lockbox ID will
        /// be specified explicitly.
        /// </summary>
        /// <param name="UserLockboxes"></param>
        /// <param name="TableName"></param>
        /// <param name="IncludeCustomerLevel"></param>
        /// <param name="IncludeLockboxLevel"></param>
        /// <param name="SQLFilter"></param>
        /// <returns></returns>
        private static bool BuildLockboxFilter(cOLLockboxes UserLockboxes,
                                               string TableName,
                                               bool IncludeCustomerLevel,
                                               bool IncludeLockboxLevel,
                                               out string SQLFilter)
        {

            string strFilter;
            List<string> arFilters = new List<string>();

            foreach (cLockbox lbx in UserLockboxes.Values)
            {

                if (IncludeLockboxLevel)
                {
                    strFilter = "(" + TableName + ".BankID=" + lbx.BankID.ToString() +
                                " AND " + TableName + ".CustomerID=" + lbx.CustomerID.ToString() +
                                " AND " + TableName + ".LockboxID=" + lbx.LockboxID.ToString() + ")";
                    if (!arFilters.Contains(strFilter))
                    {
                        arFilters.Add(strFilter);
                    }
                }

                if (IncludeCustomerLevel)
                {
                    strFilter = "(" + TableName + ".BankID=" + lbx.BankID.ToString() +
                                " AND " + TableName + ".CustomerID=" + lbx.CustomerID.ToString() +
                                " AND " + TableName + ".LockboxID=-1)";
                    if (!arFilters.Contains(strFilter))
                    {
                        arFilters.Add(strFilter);
                    }
                }
            }

            return (CatSQLFilters(arFilters, out SQLFilter));
        }

        private static bool CatSQLFilters(List<string> Filters, out string SQLFilter)
        {

            StringBuilder sbRetVal;
            bool bolIsFirst;

            if (Filters.Count > 0)
            {

                sbRetVal = new StringBuilder();
                bolIsFirst = true;
                sbRetVal.Append("(");

                foreach (string str in Filters)
                {
                    if (bolIsFirst)
                    {
                        bolIsFirst = false;
                    }
                    else
                    {
                        sbRetVal.Append(" OR ");
                    }
                    sbRetVal.Append(str);
                }

                sbRetVal.Append(")");

                SQLFilter = sbRetVal.ToString();

                return (true);
            }
            else
            {
                SQLFilter = string.Empty;
                return (false);
            }
        }

        public static string SQL_GetRenderedFilesFilePathName(Guid FileID)
        {
            return (" SELECT FilePathName FROM RenderedFiles WHERE FileID = '" + FileID.ToString() + "'");
        }

        #endregion BuildLockboxFilter
    }
}
