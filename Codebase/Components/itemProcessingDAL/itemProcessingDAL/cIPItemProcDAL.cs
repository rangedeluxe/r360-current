using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Thomas Emery
* Date:     04/19/2013
*
* Purpose:  
*
* Modification History
* WI 90594 TWE 04/19/2013
*    Move GetDestLockboxes to itemProcessingDAL 
*    change from stored procedure to embedded code.
******************************************************************************/
namespace WFS.itemProcessing.DAL
{

    public class cIPItemProcDAL : _DALBase {

        /// <summary>
        /// Initializes a new instance of the <see cref="cItemProcDAL"/> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        public cIPItemProcDAL(string vSiteKey)
            : base(vSiteKey) {

        }


        /// <summary>
        /// Gets the dest lockboxes.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetDestLockboxes(int BankID, int ClientAccountID, out DataTable dt)
        {
            return (ExecuteSQL(SQL_GetDestLockboxes(BankID, ClientAccountID), out dt));
        }

        private static string SQL_GetDestLockboxes(int BankID, int ClientAccountID)
        {

            string strLockboxes = string.Empty;
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" Declare @parmBankID int;");
            sbSQL.Append(" Declare	@parmLockboxID int;");
            sbSQL.Append(" ");
            sbSQL.Append(" SET @parmBankID = '" + BankID.ToString() + "';");
            sbSQL.Append(" SET @parmLockboxID = '" + ClientAccountID.ToString() + "';");
            sbSQL.Append(" ");
            sbSQL.Append(" SELECT ");
            sbSQL.Append("      DestLockbox.LockboxKey,");
            sbSQL.Append("      DestLockbox.SiteCode,");
            sbSQL.Append("      DestLockbox.BankID		AS BankID,");
            sbSQL.Append("      DestLockbox.CustomerID	AS CustomerID,");
            sbSQL.Append("      DestLockbox.LockboxID	AS LockboxID,");
            sbSQL.Append("      DestLockbox.ShortName,");
            sbSQL.Append("      DestLockbox.LongName,");
            sbSQL.Append("      DestLockbox.Cutoff,");
            sbSQL.Append("      DestLockbox.IsActive,");
            sbSQL.Append("      DestLockbox.IsCommingled,");
            sbSQL.Append("      LockboxCommingledSetups.DestLockboxIdentifierLabel,");
            sbSQL.Append("      LockboxCommingledSetups.DestLockboxIdentifier");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     Lockbox");
            sbSQL.Append("     INNER JOIN LockboxCommingledAssignment");
            sbSQL.Append("         INNER JOIN Lockbox AS DestLockbox");
            sbSQL.Append("             INNER JOIN LockboxCommingledSetups ON");
            sbSQL.Append("                 DestLockbox.LockboxKey = LockboxCommingledSetups.LockboxKey ON");
            sbSQL.Append("             LockboxCommingledAssignment.DestinationLockboxKey = DestLockbox.LockboxKey ON");
            sbSQL.Append("         Lockbox.LockboxKey = LockboxCommingledAssignment.LockboxKey");
            sbSQL.Append(" WHERE");
            sbSQL.Append("     Lockbox.BankID =	 @parmBankID");
            sbSQL.Append("     AND Lockbox.LockboxID = @parmLockboxID");          

            return sbSQL.ToString();
        }
    }
}