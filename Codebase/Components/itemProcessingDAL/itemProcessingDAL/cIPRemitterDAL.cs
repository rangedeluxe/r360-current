﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose: 
*
* Modification History
* WI 90244 CRG 02/21/2013 
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
* WI 72251 TWE 03/26/2013
*    moved integraPay queries to itemProcessingDAL 
******************************************************************************/
namespace WFS.itemProcessing.DAL
{

    /// <summary>
    /// 
    /// </summary>
    public class cIPRemitterDAL : _DALBase {

        /// <summary>
        /// Initializes a new instance of the <see cref="cIPRemitterDAL"/> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        public cIPRemitterDAL(string vSiteKey)
            : base(vSiteKey) {

        }

        /// <summary>
        /// Gets all remitters.
        /// </summary>
        /// <param name="OLCustomerID">The OL customer ID.</param>
        /// <param name="objLockboxes">The obj lockboxes.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetAllRemitters(string OLCustomerID, cOLLockboxes objLockboxes, out DataTable dt) {
            return (ExecuteSQL(SQLRemitter.SQL_GetAllRemitters(OLCustomerID, objLockboxes), out dt));
        }

        /// <summary>
        /// Gets the lockbox remitter.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="CustomerID">The customer ID.</param>
        /// <param name="LockboxID">The lockbox ID.</param>
        /// <param name="RoutingNumber">The routing number.</param>
        /// <param name="AccountNumber">The account number.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetLockboxRemitter(int BankID, int CustomerID, int LockboxID, string RoutingNumber, string AccountNumber, out DataTable dt) {
            return (ExecuteSQL(SQLRemitter.SQL_GetLockboxRemitter(BankID, CustomerID, LockboxID, RoutingNumber, AccountNumber), out dt));
        }

        /// <summary>
        /// Gets the lockbox remitter.
        /// </summary>
        /// <param name="LockboxRemitterID">The lockbox remitter ID.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetLockboxRemitter(Guid LockboxRemitterID, out DataTable dt) {
            return (ExecuteSQL(SQLRemitter.SQL_GetLockboxRemitter(LockboxRemitterID), out dt));
        }

        /// <summary>
        /// Gets the global remitter.
        /// </summary>
        /// <param name="RoutingNumber">The routing number.</param>
        /// <param name="AccountNumber">The account number.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetGlobalRemitter(string RoutingNumber, string AccountNumber, out DataTable dt) {
            return (ExecuteSQL(SQLRemitter.SQL_GetGlobalRemitter(RoutingNumber, AccountNumber), out dt));
        }

        /// <summary>
        /// Updates the checks remitter.
        /// </summary>
        /// <param name="LockboxRemitterName">Name of the lockbox remitter.</param>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="LockboxID">The lockbox ID.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="DepositDate">The deposit date.</param>
        /// <param name="TransactionID">The transaction ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="RowsAffected">The rows affected.</param>
        /// <returns></returns>
        public bool UpdateChecksRemitter(string LockboxRemitterName, int BankID, int LockboxID, int BatchID, DateTime DepositDate, int TransactionID, int BatchSequence, out int RowsAffected) {
           // return (ExecuteSQL(SQLRemitter.SQL_UpdateChecksRemitter(LockboxRemitterName, BankID, LockboxID, BatchID, DepositDate, TransactionID, BatchSequence), out RowsAffected));
           //  OK, this needs to work in addition to the call on the rechub side
            // an update needs to be made to global remitter and or possibly lockbox remitter
            // Currently the rechub stored procedure is set to start work in timebox 3.
            RowsAffected = 0;
            return false;

        }

        /// <summary>
        /// Execute proc_LockboxRemitterInsert to insert a new lockbox and or global remitter record
        /// </summary>
        /// <param name="Lockbox">The lockbox.</param>
        /// <param name="RoutingNumber">The routing number.</param>
        /// <param name="Account">The account.</param>
        /// <param name="OLLockboxID">The OL lockbox ID.</param>
        /// <param name="RemitterName">Name of the remitter.</param>
        /// <param name="SessionUser">The session user.</param>
        /// <returns></returns>
        public Guid InsertLockboxRemitter(cLockbox Lockbox, string RoutingNumber, string Account, Guid OLLockboxID, string RemitterName, cUser SessionUser) {
            Guid gidRetVal = Guid.Empty;
            //cLockbox objOLLockbox;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@BankID", SqlDbType.Int, Lockbox.BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@CustomerID", SqlDbType.Int, Lockbox.CustomerID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@LockboxID", SqlDbType.Int, Lockbox.LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@RoutingNumber", SqlDbType.VarChar, RoutingNumber, ParameterDirection.Input));
                arParms.Add(BuildParameter("@Account", SqlDbType.VarChar, Account, ParameterDirection.Input));
                arParms.Add(BuildParameter("@RemitterName", SqlDbType.VarChar, RemitterName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@CreatedBy", SqlDbType.VarChar, SessionUser.LogonName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@LockboxRemitterID", SqlDbType.UniqueIdentifier, Guid.Empty, ParameterDirection.Output));
                arParms.Add(BuildReturnValueParameter("@return_value", SqlDbType.Int));
                parms = arParms.ToArray();
                Database.executeProcedure("proc_LockboxRemitterInsert", parms);
                if((int) parms.Single(parm => parm.ParameterName == "@return_value").Value >= 0) {
                    gidRetVal = (Guid) parms.Single(parm => parm.ParameterName == "@LockboxRemitterID").Value;
                }

            } catch(Exception ex) {

                //log error
                EventLog.logError(ex, this.GetType().Name, "InsertLockboxRemitter");
            }

            return (gidRetVal);
        }

        /// <summary>
        /// Execute proc_LockboxRemitterUpdate to update a lockbox remitter record
        /// </summary>
        /// <param name="LockboxRemitterID">The lockbox remitter ID.</param>
        /// <param name="LockboxRemitterName">Name of the lockbox remitter.</param>
        /// <param name="SessionUser">The session user.</param>
        /// <returns></returns>
        public bool UpdateLockboxRemitter(Guid LockboxRemitterID, string LockboxRemitterName, cUser SessionUser) {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@NewRemitterName", SqlDbType.VarChar, LockboxRemitterName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@LockboxRemitterID", SqlDbType.UniqueIdentifier, LockboxRemitterID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@ModifiedBy", SqlDbType.VarChar, SessionUser.LogonName, ParameterDirection.Input));
                arParms.Add(BuildReturnValueParameter("@return_value", SqlDbType.Int));
                parms = arParms.ToArray();
                Database.executeProcedure("proc_LockboxRemitterUpdate", parms);
                bolRetVal = ((int) parms.Single(parm => parm.ParameterName == "@return_value").Value >= 0);
            } catch(Exception ex) {

                //log error
                EventLog.logError(ex, this.GetType().Name, "UpdateLockboxRemitter");

                //throw error to calling application
                throw (ex);
            }

            return bolRetVal;
        }

    }
}
