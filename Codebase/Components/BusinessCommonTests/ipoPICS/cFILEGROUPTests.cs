﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske  
* Date: 07/03/2014
*
* Purpose: Tests some of the new functionality for the FILEGROUP image 
*          storage provider. 
*
* Modification History
* WI 151405 BLR 07/02/2014
*   Initial Version
* WI 152230 BLR 07/07/2014
*   Added a test method to test the filtering of the integraPAY source.
******************************************************************************/

namespace WFS.RecHub.Common.Tests.ipoPICS
{
    /// <summary>
    /// TODO: In order to correctly test the cFILEGROUP object, we need to remove the dependency of
    /// the file system and the database from cFILEGROUP and mock it here.  For now, we'll test 
    /// against some hard-coded data.
    /// 
    /// Remember to change the app.config as your RecHub.ini may change locations.
    /// Also, 'getRelativeImagePath' will return String.Empty unless the file actually exists in your
    /// file system.
    /// </summary>
    [TestClass]
    public class cFILEGROUPTests : BaseTestFixture
    {
        private readonly cFILEGROUP _cFILEGROUP;

        public cFILEGROUPTests()
        {
            _cFILEGROUP = new cFILEGROUP(base.SiteKey);
        }

        /// <summary>
        /// Tests FILEGROUP relativePath.
        /// Expected {root}\1\4350\20060228\192_3_C_F.TIF
        /// Note: No payment source name due to integraPAY.
        /// </summary>
        [TestMethod]
        public void TestRelativeIntegraPAYImagePath()
        {
            // Arrange
            var testdata = BuildIntegraPAYTestPaymentData();
            var root = base.RootImagePath;
            var expected = @"1\4350\20060228\192_3_C_F.tif";

            // Act
            var actual = _cFILEGROUP.getRelativeImagePath(0x1, testdata, 0, root);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Tests FILEGROUP DIT relativePath.
        /// Expected {root}DIT\1\9043\20060228\438_1_C_F.TIF
        /// </summary>
        [TestMethod]
        public void TestRelativeDITImagePath()
        {
            // Arrange
            var testdata = BuildDITTestPaymentData();
            var root = base.RootImagePath;
            var expected = @"DIT\1\9043\20060228\438_1_C_F.tif";

            // Act
            var actual = _cFILEGROUP.getRelativeImagePath(0x1, testdata, 0, root);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Tests FILEGROUP relativePath for a non-existing file.
        /// Expected an Empty String
        /// </summary>
        [TestMethod]
        public void TestFileDoesNotExistRelativeImagePath()
        {
            // Arrange
            var testdata = BuildIntegraPAYTestPaymentData();
            testdata.ProcessingDateKey = 20060227;
            var root = base.RootImagePath;
            var notexpected = @"1\4350\20060227\192_3_C_F.tif";

            // Act
            var actual = _cFILEGROUP.getRelativeImagePath(0x1, testdata, 0, root);

            // Assert
            Assert.AreNotEqual(notexpected, actual);
            Assert.AreEqual(string.Empty, actual);
        }

        protected cCheck BuildIntegraPAYTestPaymentData()
        {
            return new cCheck()
            {
                ProcessingDateKey = 20060228,
                BankID = 1,
                LockboxID = 4350,
                BatchID = 39,
                BatchSequence = 3
            };
        }

        protected cCheck BuildDITTestPaymentData()
        {
            return new cCheck()
            {
                ProcessingDateKey = 20060228,
                BankID = 1,
                LockboxID = 9043,
                BatchID = 256,
                BatchSequence = 1
            };
        }
    }
}
