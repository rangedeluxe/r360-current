﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske  
* Date: 07/03/2014
*
* Purpose: Base class for the Text Classes for PICS and FILEGROUP.
*
* Modification History
* WI 151405 BLR 07/02/2014
*   Initial Version
******************************************************************************/

namespace WFS.RecHub.Common.Tests.ipoPICS
{
    public abstract class BaseTestFixture
    {
        protected string SiteKey { get; private set; }
        protected string RootImagePath { get; private set; }

        public BaseTestFixture()
        {
            SiteKey = ConfigurationManager.AppSettings["SiteKey"];
            RootImagePath = ipoINILib.IniReadValue(SiteKey, "ImagePath");
        }
    }
}
