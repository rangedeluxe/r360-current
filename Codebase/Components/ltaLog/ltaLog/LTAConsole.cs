﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     08/10/2012
*
* Purpose:  
*
* Modification History
* CR 54217 JMC 08/10/2012
*   -Initial release.
******************************************************************************/
namespace WFS.LTA.Common {

    public static class LTAConsole {

        private static ConsoleColor DefaultConsoleColor {
            get {
                return(ConsoleColor.White);
            }
        }

        public static void ConsoleWriteLine(string msg) {
            ConsoleWriteLine(msg, DefaultConsoleColor);
        }

        public static void ConsoleWriteLine(string msg, LTAConsoleMessageType msgType) {

            switch(msgType) {
                case(LTAConsoleMessageType.Error):
                    ConsoleWriteLine(msg, ConsoleColor.Red);
                    break;
                case(LTAConsoleMessageType.Warning):
                    ConsoleWriteLine(msg, ConsoleColor.Yellow);
                    break;
                case(LTAConsoleMessageType.Information):
                    ConsoleWriteLine(msg, ConsoleColor.Green);
                    break;
                case(LTAConsoleMessageType.TraceInfo):
                    ConsoleWriteLine(msg, ConsoleColor.Cyan);
                    break;
                default:
                    ConsoleWriteLine(msg, DefaultConsoleColor);
                    break;
            }
        }

        private static void ConsoleWriteLine(string msg, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.WriteLine(msg);
            Console.ResetColor();
        }
    }
}
