﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     08/10/2012
*
* Purpose:  
*
* Modification History
* CR 54217 JMC 08/10/2012
*   -Initial release.
******************************************************************************/
namespace WFS.LTA.Common {

    ///// <summary></summary>
    public delegate void LTAOutputLogEventHandler(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance);

    ///// <summary></summary>
    public delegate void LTAOutputTraceEventHandler(string msg, LTAConsoleMessageType messageType);

    ///// <summary></summary>
    public delegate void LTAOutputLogAndTraceEventHandler(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance);

    ///// <summary></summary>
    public delegate void LTAOutputLogAndTraceExceptionEventHandler(string msg, string src, Exception ex);

    /// <summary>
    /// MessageType enumeration
    /// </summary>
    public enum LTAMessageType {
        /// <summary></summary>
        Error = 0,
        /// <summary></summary>
        Information = 1,
        /// <summary></summary>
        Warning = 2
    }

    /// <summary>
    /// MessageImportance enumeration
    /// </summary>
    public enum LTAMessageImportance {
        /// <summary></summary>
        Essential = 0,
        /// <summary></summary>
        Verbose = 1,
        /// <summary></summary>
        Debug = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum LTAConsoleMessageType {
        /// <summary></summary>
        Error = 0,
        /// <summary></summary>
        Information = 1,
        /// <summary></summary>
        Warning = 2,
        /// <summary></summary>
        TraceInfo = 3
    }
}
