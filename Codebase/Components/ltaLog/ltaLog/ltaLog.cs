﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:     05/16/2012
*
* Purpose:  Lta Logging Component
*
* Modification History
* CR 52611 WJS 05/16/2012
*   -Initial release.
* CR 54217 JMC 07/17/2012
*   -Changed names of public enumerations so they do not conflict with values 
*    in ipoLib.
*   -Removed Console.WriteLine() call from outputText() method.
* CR 69359 WJS 11/16/2011
*   -FP:Fix Memory leaks
* CR 104682 JMC 06/06/2013
*   -Now Exposing LogFilePath.
*   -Added InsertBlankLine() methods.
******************************************************************************/
namespace WFS.LTA.Common {

    /// <summary>
    /// lta Logging component.
    /// </summary>
    public class ltaLog
    {  
        private const string CRLF = "\r\n";

        private const string cSpacer = " ";
        private const string cLogTimeFormat = "MM/dd/yyyy hh:mm:ss tt";

        private const int COLUMN_WIDTH_TYPE = 11;
        private const int COLUMN_WIDTH_DATE = 22;
        private const int COLUMN_WIDTH_SOURCE = 20;
        private const string LOGFILE_TIMESTAMP_TOKEN = @"\{0:[yMHhdms].*\}";

        private string _LogFilePath = "";
        private long _LogFileMaxSize = 0;
        private LTAMessageImportance _LoggingDepth;
        private bool _blnUseRollingLogFile;

     

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="logFilePath"></param>
        /// <param name="logFileMaxSize"></param>
        /// <param name="loggingDepth"></param>
        public ltaLog(string logFilePath
                       , long logFileMaxSize
                       , LTAMessageImportance loggingDepth) {
            _LogFileMaxSize = logFileMaxSize;
            _LoggingDepth = loggingDepth;
            _LogFilePath = logFilePath;

            // Check if logfile path contains composite date/time formatting for rolling log file.
            Regex objRegEx = new Regex (LOGFILE_TIMESTAMP_TOKEN, RegexOptions.IgnoreCase);
            _blnUseRollingLogFile = objRegEx.IsMatch(logFilePath);
        }

        public string LogFilePath {
            get {
                return (_LogFilePath);
            }
        }

        /// <summary>
        /// Log Entry struc.
        /// </summary>
        private struct sLogEntry {  
            public string LogDate;
            public string EventType;
            public string Source;
            public string LogText;
        }


        /// <summary>
        /// Constant header line string appended to the beginning of the LogFile.
        /// </summary>
        private string headerString {
            get {
                return string.Format ("{0,-" + COLUMN_WIDTH_TYPE + "}", "Type")
                     + cSpacer
                     + string.Format ("{0,-" + COLUMN_WIDTH_DATE + "}", "Date")
                     + cSpacer
                     + string.Format ("{0,-" + COLUMN_WIDTH_SOURCE + "}", "Source")
                     + cSpacer
                     + "Description"
                     + CRLF;
            }
        }


     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="LineCount"></param>
        /// <param name="LinesToKeep"></param>
        /// <param name="logEntriesToKeep"></param>
        private void getLogEntriesToKeep(long LineCount
                                       , long LinesToKeep
                                       , out string[] logEntriesToKeep) {

            long lngLinesToEliminate = (LineCount - LinesToKeep);

            logEntriesToKeep = new string[LinesToKeep];
            using (TextReader tr = System.IO.File.OpenText(_LogFilePath))
            {

                try {
                    // Skip lines to be eliminated.
                    for(int i=0;i<lngLinesToEliminate;++i)
                        { tr.ReadLine(); }

                    // Add lines to keep to the array.
                    for (int i=0;i<LinesToKeep;++i)
                        { logEntriesToKeep[i] = tr.ReadLine(); }
                }
                catch(Exception e)
                {
                    writeEventError(e);
                }
                tr.Close();
            }
            
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="logEntries"></param>
        private void writeNewLogFile(ref string[] logEntries) {
            if (File.Exists(_LogFilePath))
                { File.Delete(_LogFilePath); }

            System.IO.TextWriter tw;
            getLogFile(out tw);

            try
            {
                for (int i=0;i<logEntries.Length;++i)
                {
                    tw.WriteLine(logEntries[i]);
                }
            }
            catch(Exception e)
            {
                writeEventError(e);
            }
            finally
            {
                if (tw !=null)
                {
                    try { tw.Close(); } catch {}
                    tw.Dispose();
                }
            }
        }


        /// <summary>
        /// Retrieves the size in bytes of the LogFile.
        /// </summary>
        /// <returns></returns>
        private long logFileSize() {
            long lngFileSize = 0;
            string strLogFile = string.Empty;

            strLogFile = string.Format(_LogFilePath, DateTime.Now);
            if(File.Exists(strLogFile)) {
                using (FileStream fsOriginal = new FileStream(strLogFile, System.IO.FileMode.Open))
                {
                    lngFileSize = fsOriginal.Length;

                    fsOriginal.Close();
                }
            }

            return lngFileSize;
        }


        /// <summary>
        /// Multiplies the max number of Mb's as specified in the INI  file by 1024 
        /// to get the max number of bytes the Logfile can be.
        /// </summary>
        /// <returns></returns>
        private long maxFileSize() {
            return (_LogFileMaxSize * 1024);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="tw"></param>
        private void getLogFile(out System.IO.TextWriter tw) {
            string strLogFile = "";
            string strBasePath = "";

            if (_blnUseRollingLogFile) {
                strLogFile = string.Format(_LogFilePath, DateTime.Now);
            } else {
                strLogFile = _LogFilePath;
            }

            strBasePath = System.IO.Directory.GetParent(strLogFile).FullName;

            if (!System.IO.Directory.Exists(strBasePath))
                System.IO.Directory.CreateDirectory(strBasePath);

            if (System.IO.File.Exists(strLogFile)) {
                tw = new System.IO.StreamWriter(strLogFile, true); 
            } else {
                tw = new System.IO.StreamWriter(strLogFile, true);    
                tw.WriteLine(headerString);
                tw.Flush();
            }
        }

        /// <summary>
        /// Does the actual write to the output file.
        /// </summary>
        /// <param name="eventMsg"></param>
        private void outputText(string eventMsg) {

            System.IO.TextWriter tw = null;
            try
            {
                
                getLogFile(out tw);

                try
                {
                    tw.WriteLine(eventMsg);
                    tw.Flush();
                    tw.Close();
                }
                catch (Exception e)
                {
                    writeEventError(e);
                    Console.WriteLine("error:{0}", e);
                }
                finally
                {
                    if (tw!=null)
                    {
                        try { tw.Close(); } catch {}
                        tw.Dispose();
                        tw=null;
                    }
                }
            }
            catch(Exception e)
            {
                writeEventError(e);
            }
            finally 
            {
                if (tw != null)
                {
                    tw.Dispose();
                }
            }
        }

//*******************************************************************************************
//*******************************************************************************************

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Overloaded function.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="messageImportance"></param>
        //************************************************************************
        public void logEvent(string msg
                           , LTAMessageImportance messageImportance) {
            logEvent(msg
                   , ""
                   , LTAMessageType.Information
                   , messageImportance);
        }

        /// <summary>
        /// Overloaded function.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="src"></param>
        /// <param name="messageImportance"></param>
        public void logEvent(string msg
                           , string src
                           , LTAMessageImportance messageImportance) {
            logEvent(msg
                   , src
                   , LTAMessageType.Information
                   , messageImportance);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        public void logEvent(string msg
                           , string src
                           , LTAMessageType messageType) {

            logEvent(msg, src, messageType, LTAMessageImportance.Essential);
        }

        /// <summary>
        /// Adds the specified message to the Logfile.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        public void logEvent(string msg
                           , string src
                           , LTAMessageType messageType
                           , LTAMessageImportance messageImportance) {

            string strEventMsg;

            try
            {            
                if(isImportant(messageImportance)) {
                    sLogEntry tmpLog = new sLogEntry();

                    tmpLog.LogDate = System.DateTime.Now.ToString (cLogTimeFormat).PadRight (COLUMN_WIDTH_DATE);
                    tmpLog.LogText = msg;
                    tmpLog.Source = src.PadRight (COLUMN_WIDTH_SOURCE);
                    
                    switch(messageType)
                    {
                        case LTAMessageType.Error :
                            tmpLog.EventType = string.Format ("{0,-" + COLUMN_WIDTH_TYPE + "}", "Error");
                            break;
                        case LTAMessageType.Information :
                            tmpLog.EventType = string.Format ("{0,-" + COLUMN_WIDTH_TYPE + "}", "Information");
                            break;
                        case LTAMessageType.Warning :
                            tmpLog.EventType = string.Format ("{0,-" + COLUMN_WIDTH_TYPE + "}", "Warning");
                            break;
                    }

                    //if(messageType == MessageType.Error) {
                    //    if (tmpLog.Source.Trim().Length > 0) {
                    //        strEventMsg = tmpLog.EventType
                    //                    + cSpacer
                    //                    + tmpLog.LogDate
                    //                    + cSpacer
                    //                    + tmpLog.Source;

                    //        outputText(strEventMsg);
                    //    }

                    //    strEventMsg = tmpLog.EventType
                    //                + cSpacer
                    //                + tmpLog.LogDate
                    //                + cSpacer
                    //                + tmpLog.Source
                    //                + cSpacer
                    //                + tmpLog.LogText;

                    //    outputText(strEventMsg);
                    //} else {
                        strEventMsg=tmpLog.EventType
                                + cSpacer
                                + tmpLog.LogDate
                                + cSpacer
                                + tmpLog.Source
                                + cSpacer
                                + tmpLog.LogText;

                        outputText(strEventMsg);

                        createNewLogFileIfNecessary();
                  
                }
            }
            catch(Exception e)
            {
                writeEventError(e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageImportance"></param>
        /// <returns></returns>
        private bool isImportant(LTAMessageImportance messageImportance) {

            bool bolRetVal;
            LTAMessageImportance enmImportanceLevel;

            try{enmImportanceLevel = (LTAMessageImportance)_LoggingDepth; }
            catch{enmImportanceLevel = LTAMessageImportance.Essential; }

            if(enmImportanceLevel == LTAMessageImportance.Debug)
            {
                bolRetVal = true; 
            }
            else if((enmImportanceLevel == LTAMessageImportance.Verbose) &&
                    ((messageImportance == LTAMessageImportance.Essential)||
                     (messageImportance == LTAMessageImportance.Verbose)
                    )
                   )
            {
                bolRetVal = true;
            }
            else if((enmImportanceLevel == LTAMessageImportance.Essential) &&
                    (messageImportance == LTAMessageImportance.Essential))
            {
                bolRetVal = true;
            }
            else
            {
                bolRetVal = false;
            }

            return bolRetVal;
        }


        /// <summary>
        /// Overloaded function
        /// </summary>
        /// <param name="e"></param>
        public void logError(Exception e) {
            if(e.TargetSite == null) {
                logError(e
                       , e.Source
                       , string.Empty);
            } else {
                logError(e
                       , e.Source
                       , e.TargetSite.Name);
            }
        }


        /// <summary>
        /// Formats the Exception object in standard Error format, and calls the 
        /// logEvent method to write the error to the Logfile.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="src"></param>
        /// <param name="procName"></param>
        public void logError(Exception e
                           , string src
                           , string procName) {
            string strAns;

            try
            {
                
                if (procName != "")
                    { strAns = "Error occurred in " + procName + ".  " + e.Message + "."; }
                else
                    { strAns = e.Message + "."; }
                
                //logEvent(strAns
                //       , e.TargetSite + " " + e.TargetSite.ToString()
                //       , MessageType.Error
                //       , MessageImportance.Essential);
                logEvent (strAns + "  " + e.TargetSite + " " + e.TargetSite.ToString ()
                       , src
                       , LTAMessageType.Error
                       , LTAMessageImportance.Essential);
            }
            catch
            {
                writeEventError(e);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="messageImportance"></param>
        public void logWarning(string msg
                             , LTAMessageImportance messageImportance) {
            logEvent(msg
                   , ""
                   , LTAMessageType.Warning
                   , messageImportance);
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="src"></param>
        /// <param name="messageImportance"></param>
        public void logWarning(string msg
                             , string src
                             , LTAMessageImportance messageImportance) {
            logEvent(msg
                   , src
                   , LTAMessageType.Warning
                   , messageImportance);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineCount"></param>
        public void InsertBlankLine(int lineCount) {
            if(lineCount > 0 && lineCount < 100) {
                for(int i = 0;i < lineCount;++i) {
                    InsertBlankLine();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void InsertBlankLine() {
            outputText(string.Empty);
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// <remarks>
        /// This code was commented out because on a very fast machine, the Event 
        /// log will fill up.  The error that will normally be handled here is a 
        /// conflict when attempting to access the logfile.  This happens when 
        /// parallel threads compete for file access.  Commenting this out means 
        /// that some events will not be logged.  A better solution to handle 
        /// this would be to use Thread blocking with a Mutex.       
        /// </remarks>
        private void writeEventError(Exception e) {
////            System.Diagnostics.EventLog.WriteEntry(e.Source
////                                                 , "eventLog unable to write to logfile : " 
////                                                 + serverOptions.logFilePath 
////                                                 + ".  Error at : " + e.Source + "."
////                                                 + e.TargetSite.ToString() + e.Message + "."
////                                                 , System.Diagnostics.EventLogEntryType.Error);
        }


        /// <summary>
        /// Accepts the full path to a readable (presumably text) file.  returns 
        /// the number of lines contained in that file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static long countFileLines(string filePath) {
            string strTemp = "";
            long lngLineCount=0;

            using (System.IO.TextReader tr = System.IO.File.OpenText(filePath))
            {

                while ((strTemp = tr.ReadLine()) != null)   //(tr.Peek())
                {
                    ++lngLineCount;
                }

                tr.Close();
            }

            return lngLineCount;
        }

        private void createNewLogFileIfNecessary()
        {
            bool bolContinue; 
            int intTemp;
            string strLogFileName, strLogFile;
            try {

                strLogFile = string.Format(_LogFilePath, DateTime.Now);
                if(_LogFileMaxSize > 0) {
    
                    while(logFileSize() > maxFileSize()) {
                        bolContinue = true;
                        intTemp = 1;
                        while (bolContinue)
                        {
                            strLogFileName = strLogFile.Replace(".txt", "." + intTemp.ToString().PadLeft(6, '0') + ".txt");
                           
                            if (File.Exists(strLogFileName))
                            {
                                ++intTemp;
                                if (intTemp > 999999)
                                {
                                    //we are out of log files just go ahead and let it grow :)
                                    bolContinue = false;
                                    //throw (new Exception("Maximum number of log files exceeded."));
                                }
                            }
                            else
                            {
                                File.Move(strLogFile, strLogFileName);
                                
                                bolContinue = false;
                            }
                        }
                    }
                }
            }
            catch(Exception e) {
                writeEventError(e);
            }
        }
    }
}
