﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace DatabaseBuild
{
    class Program
    {

        private static List<string> ManifestFiles(CommandLineOptions commandLineOptions)
        {
            return Directory.GetFiles(commandLineOptions.WorkspaceFolder, $"*{commandLineOptions.VersionNumber}*.xml", SearchOption.AllDirectories).ToList();
        }

        private static void CopyFile(string sourceFile, string targetFile)
        {
            Console.WriteLine($"Copying file: {sourceFile}");
            if (!Directory.Exists(Path.GetDirectoryName(targetFile)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(targetFile));
            }
            File.Copy(sourceFile, targetFile, true);
        }

        private static List<string> GetFileNames(string workspaceFolder, string sourceFile)
        {
            List<string> fileNameList;
            if (sourceFile.Contains("*"))
            {
                var dir = Path.Combine(workspaceFolder, Path.GetDirectoryName(sourceFile));
                var file = Path.GetFileName(sourceFile);

                fileNameList = Directory.GetFiles(dir, file).ToList();
            }
            else
            {
                fileNameList = new List<string> { Path.Combine(workspaceFolder, sourceFile) };
            }

            return fileNameList;
        }

        private static void CopyFiles(string sourceFile, CommandLineOptions commandLineOptions)
        {
            var fileNameList = GetFileNames(commandLineOptions.WorkspaceFolder, sourceFile); foreach (var fileName in fileNameList)
            {
                CopyFile(fileName, Path.Combine(commandLineOptions.StagingFolder, Path.GetDirectoryName(sourceFile),Path.GetFileName(fileName)));
            }
        }

        private static string CreateSourceFileName(string manifestFile, string sourceFileName)
        {
            var subFolderList = manifestFile.Split('\\');

            var commandFileIndex = Array.FindIndex(subFolderList,
                s => string.Compare(s, "CommandFile", StringComparison.InvariantCultureIgnoreCase) == 0);

            var subFolder = string.Compare(subFolderList[commandFileIndex - 1], "Database") == 0
                ? string.Empty
                : subFolderList[commandFileIndex - 1];

            return Path.Combine(subFolder, sourceFileName);
        }

        private static void ProcessManifestFile(string manifestFile, CommandLineOptions commandLineOptions)
        {
            var manifestXml = new XmlDocument();
            if (File.Exists(manifestFile))
            {
                manifestXml.LoadXml(File.ReadAllText(manifestFile));

                var objectFiles = manifestXml?.DocumentElement?.SelectNodes("/SQLInstall/ProcessFile");
                if (objectFiles != null)
                {
                    foreach (XmlNode xmlNode in objectFiles)
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(commandLineOptions.StagingFolder)))
                        {
                            Directory.CreateDirectory(commandLineOptions.StagingFolder);
                        }

                        CopyFiles(CreateSourceFileName(manifestFile, xmlNode.LastChild.InnerText.Trim()), commandLineOptions);
                    }
                }
                
                var includeFiles = manifestXml?.DocumentElement?.SelectNodes("/SQLInstall/IncludeFile");
                if (includeFiles != null)
                {
                    foreach (XmlNode xmlNode in includeFiles)
                    {
                        CopyFiles(CreateSourceFileName(manifestFile, xmlNode.InnerText), commandLineOptions);
                    }
                }
            }
        }

        private static void CopyManifestFile(string manifestFile, CommandLineOptions commandLineOptions)
        {
            CopyFile(manifestFile, Path.Combine(commandLineOptions.StagingFolder, "CommandFile", Path.GetFileName(manifestFile)));
        }

        static void Main(string[] args)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo assemblyFileVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            Console.WriteLine($"BuildDatabase Version {assemblyFileVersion.ProductMajorPart}.{assemblyFileVersion.FileMinorPart}");

            var commandLineProcessor = new ProcessCommandLine();
            var commandLineOptions = commandLineProcessor.parseCommandLine(args);

            if (commandLineOptions.Success)
            {
                Console.WriteLine($"Staging   {commandLineOptions.StagingFolder}");
                Console.WriteLine($"Workspace {commandLineOptions.WorkspaceFolder}");
                Console.WriteLine($"Version   {commandLineOptions.VersionNumber}");

                var manifestFiles = ManifestFiles(commandLineOptions);
                Console.WriteLine($"Found {manifestFiles.Count} manifest files to process");
                if ( manifestFiles.Any() && !Directory.Exists(commandLineOptions.StagingFolder))
                {
                    Console.WriteLine("Creating Staging folder.");
                    Directory.CreateDirectory(commandLineOptions.StagingFolder);
                }

                foreach (var manifestFile in manifestFiles)
                {
                    Console.WriteLine("-------------------------------------------------------------------------------");
                    Console.WriteLine($"Processing manifest file {manifestFile}");
                    Console.WriteLine("===============================================================================");

                    ProcessManifestFile( manifestFile, commandLineOptions);

                    CopyManifestFile(manifestFile, commandLineOptions);
                    Console.WriteLine($"{manifestFile} complete.");
                }
            }
        }
    }
}
