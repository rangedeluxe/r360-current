﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseBuild
{
    public class ProcessCommandLine
    {
        private void Help()
        {
            Console.WriteLine("Use either - or / to start the command line option.");
            Console.WriteLine("StagingFolder - Folder where the files will be written to be pushed to Artifactory.");
            Console.WriteLine("WorkspaceFolder - Root folder where the database files are.");
            Console.WriteLine("VersionNumber - Version number to build.");
        }

        public CommandLineOptions parseCommandLine(string[] args)
        {
            var commandLineOptions = new CommandLineOptions();

            for (var i = 0; i < args.Length; i++)
            {
                if (args[i].StartsWith("/") || args[i].StartsWith("-"))
                {
                    switch (args[i].Remove(0, 1).ToUpper())
                    {
                        case "STAGINGFOLDER":
                            commandLineOptions.StagingFolder = args[++i];
                            break;
                        case "WORKSPACEFOLDER":
                            commandLineOptions.WorkspaceFolder = args[++i];
                            break;
                        case "VERSIONNUMBER":
                            commandLineOptions.VersionNumber = args[++i];
                            break;
                        case "HELP":
                        case "?":
                            Help();
                            break;
                        default:
                            commandLineOptions.Success = false;
                            Console.WriteLine("Invalid argument {0}", args[i]);
                            Help();
                            break;
                    }
                }
            }


            return commandLineOptions;
        }
    }
}
