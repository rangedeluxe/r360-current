﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseBuild
{
    public class CommandLineOptions
    {
        public bool Success { get; set; } = true;
        public string StagingFolder { get; set; }
        public string WorkspaceFolder { get; set; }
        public string VersionNumber { get; set; }
    }
}
