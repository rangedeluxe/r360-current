<%@ WebService Language="C#"  Class="Service" %>


using System.Collections.Specialized;

using System.Diagnostics;
using System.Web.Services;
using WFS.integraPAY.Online.ICONServicesAPI;
using WFS.RecHub.Common;

public class Service : System.Web.Services.WebService
{
    private string _SiteKey = string.Empty;

    [WebMethod]
    public string Execute(string strXML)
    {
        //EventLog myLog = new EventLog();

        //myLog.Source = "HttpListenerController";
            
        string strFileName;

        if(System.Configuration.ConfigurationManager.AppSettings["localIni"] != null && System.Configuration.ConfigurationManager.AppSettings["localIni"].Length > 0) {
            strFileName=System.Configuration.ConfigurationManager.AppSettings["localIni"];
        } else {
            strFileName=System.IO.Path.Combine(ipoLib.AppPath, ipoLib.INIFILE_NAME);
        }
        //myLog.WriteEntry("Filename is " + strFileName, EventLogEntryType.Information);
        ICONWebService ICONWebServiceProcessor = new ICONWebService(SiteKey);
        return (ICONWebServiceProcessor.ProcessXMLCommand(strXML));
    }


    private string GetSettingValue(string setting)
    {
        return System.Configuration.ConfigurationManager.AppSettings[setting];
    }
    
    protected string SiteKey
    {
        get
        {
            StringCollection arrSites;
            string[] arrEntry;

            _SiteKey = GetSettingValue("sitekey");//override site key from config
            
            if (string.IsNullOrEmpty(_SiteKey))
            {
                // Retrieve null separated list of key/site value pairs and separate into array
                arrSites = ipoINILib.GetINISection("Sites");

                foreach (string strSite in arrSites)
                {
                    if (strSite.Length > 0)
                    {
                        arrEntry = strSite.Split('=');    // Split site key/value pair into array
                        if (Context.Request.ServerVariables["SERVER_NAME"] == arrEntry[arrEntry.GetUpperBound(0)])
                        {
                            _SiteKey = arrEntry[arrEntry.GetLowerBound(0)];
                            break;
                        }
                    }
                }
            }

            return (_SiteKey);
        }
    }
}