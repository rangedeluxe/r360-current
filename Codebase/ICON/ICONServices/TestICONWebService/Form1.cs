using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

/******************************************************************************
** Wausau
** Copyright � 1997-2015 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 1997-2015.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:    
*
* Purpose:  Allow unit testing without imageRPS.
*
* Modification History
* WI 196657 TWE 03/20/2015
*     Require logon, password, and payment source from imageRPS
* WI 196683 TWE 03/20/2015
*     implement config file setting to allow legacy imageRPS to be used
******************************************************************************/


namespace WFS.integraPAY.Online.OLFICONServicesAPI
{
    public partial class frmMain : Form
    {
        private string _ACHReturnsAuditTrail = "ACH Returns Audit Trail";
        private string _BatchID = "710001";
        private string _BatchDate = "03/09/2010";
        private string _P1Sequence = "";
        private string _SiteID = "1";
        private string _ClientID = "20";
        private string _Logon = string.Empty;
        private string _Password = string.Empty;
        private string _PaymentSource = string.Empty;

        //private string _BatchDate = "03/09/2010";
        private string _BatchNumber = "85";
        private string _ClientName = "Bevent Insurance";
        private string _AbbrevClientName = "202020";
        private string _DocGrpID = "5";
        private string _ProcessDate = "03/09/2010";
        private string _ReceiveDate = "03/09/2010";
        private string _DepositDate = "06/10/2010";
        private string _DepositNumber = "0";
        private string _DepositTime = "0";
        private string _ConsolidationDate = "06/08/2010";
        private string _ConsolidationNumber = "1";
        private string _P1StationID = "5";
        private string _P1OperatorID = "SCOTT";
        private string _P2StationID = "5";
        private string _P2OperatorID = "SCOTT";
        private string _ProcessType = "";
        private string _LocationID = "1234";
        private string _LocationName = "Test Location 4 Digits";

        public frmMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ICONServicesClient objClient;
            XmlDocument docXml;
            string sRet;

            try {

                docXml = new XmlDocument();
                docXml.LoadXml(this.txtXml.Text);

                try {
                    objClient = new ICONServicesClient(ConfigurationManager.AppSettings["ServiceUrl"]);
                    sRet = objClient.Execute(docXml.OuterXml);
                    MessageBox.Show("Return: " + sRet);
                } catch(Exception ex) {
                    MessageBox.Show("Exception occurred executing service: " + ex.Message);
                }
            } catch(Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private string GetSampleXmlAppend() {

            StringBuilder sbXml = new StringBuilder();

            sbXml.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
            sbXml.Append("<RemitXMLCommand xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n");
            sbXml.Append(GetQueryKeywordsCredentials());
            sbXml.Append("  <CommandList>\r\n");
            sbXml.Append("    <Command CommandType=\"AppendKeywords\">\r\n");
            sbXml.Append("      <QueryKeywords>\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Batch ID\" Value=\"" + _BatchID + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Batch Date\" Value=\"" + _BatchDate + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"P1 Sequence\" Value=\"" + _P1Sequence + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Site ID\" Value=\"" + _SiteID + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Client ID\" Value=\"" + _ClientID + "\" />\r\n");
            sbXml.Append("      </QueryKeywords>\r\n");
            sbXml.Append("      <ActionKeywords>\r\n");
            sbXml.Append("        <ActionKeyword Name=\"ACH Returns Audit Trail\" Value=\"" + _ACHReturnsAuditTrail + "\" />\r\n");
            sbXml.Append("      </ActionKeywords>\r\n");
            sbXml.Append("    </Command>\r\n");
            sbXml.Append("  </CommandList>\r\n");
            sbXml.Append("</RemitXMLCommand>");

            return(sbXml.ToString());
        }

        private string GetSampleXmlUpdate() {

            StringBuilder sbXml = new StringBuilder();

            sbXml.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
            sbXml.Append("<RemitXMLCommand xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org.2001/XMLSchema\">\r\n");
            sbXml.Append(GetQueryKeywordsCredentials());
            sbXml.Append("  <CommandList>\r\n");
            sbXml.Append("    <Command CommandType=\"UpdateKeywords\">\r\n");
            sbXml.Append("      <QueryKeywords>\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Batch ID\" Value=\"" + _BatchID + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Batch Date\" Value=\"" + _BatchDate + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"P1 Sequence\" Value=\"" + _P1Sequence + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Site ID\" Value=\"" + _SiteID + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Client ID\" Value=\"" + _ClientID + "\" />\r\n");
            sbXml.Append("      </QueryKeywords>\r\n");
            sbXml.Append("      <ActionKeywords>\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Batch Date\" Value=\"" + _BatchDate + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Batch Number\" Value=\"" + _BatchNumber + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Client Name\" Value=\"" + _ClientName + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Abbrev Client Name\" Value=\"" + _AbbrevClientName + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Doc Grp ID\" Value=\"" + _DocGrpID + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Process Date\" Value=\"" + _ProcessDate + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Receive Date\" Value=\"" + _ReceiveDate + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Deposit Date\" Value=\"" + _DepositDate + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Deposit Number\" Value=\"" + _DepositNumber + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Deposit Time\" Value=\"" + _DepositTime + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Consolidation Date\" Value=\"" + _ConsolidationDate + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Consolidation Number\" Value=\"" + _ConsolidationNumber + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"P1 Station ID\" Value=\"" + _P1StationID + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"P1 Operator ID\" Value=\"" + _P1OperatorID + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"P2 Station ID\" Value=\"" + _P2StationID + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"P2 Operator ID\" Value=\"" + _P2OperatorID + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Process Type\" Value=\"" + _ProcessType + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Location ID\" Value=\"" + _LocationID + "\" />\r\n");
            sbXml.Append("        <ActionKeyword Name=\"Location Name\" Value=\"" + _LocationName + "\" />\r\n");
            sbXml.Append("      </ActionKeywords>\r\n");
            sbXml.Append("    </Command>\r\n");
            sbXml.Append("  </CommandList>\r\n");
            sbXml.Append("</RemitXMLCommand>");

            return(sbXml.ToString());
        }

        private string GetSampleXmlDelete() {

            StringBuilder sbXml = new StringBuilder();

            sbXml.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
            sbXml.Append("<RemitXMLCommand xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n");
            sbXml.Append(GetQueryKeywordsCredentials());
            sbXml.Append("  <CommandList>\r\n");
            sbXml.Append("    <Command CommandType=\"DeleteBatch\">\r\n");
            sbXml.Append("      <QueryKeywords>\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Batch ID\" Value=\"" + _BatchID + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Batch Date\" Value=\"" + _BatchDate + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Site ID\" Value=\"" + _SiteID + "\" />\r\n");
            sbXml.Append("        <QueryKeyword Name=\"Client ID\" Value=\"" + _ClientID + "\" />\r\n");
            sbXml.Append("      </QueryKeywords>\r\n");
            sbXml.Append("      <ActionKeywords />\r\n");
            sbXml.Append("    </Command>\r\n");
            sbXml.Append("  </CommandList>\r\n");
            sbXml.Append("</RemitXMLCommand>");

            return(sbXml.ToString());
        }

        private string GetQueryKeywordsCredentials()
        {
            StringBuilder sbXml = new StringBuilder();
            _Logon = ConfigurationManager.AppSettings["LogonName"];
            _Password = ConfigurationManager.AppSettings["Password"];
            _PaymentSource = ConfigurationManager.AppSettings["DefaultPaymentSource"];

            sbXml.Append("  <ICONInitialize Logon=\"" + _Logon + "\"  \r\n");
            sbXml.Append("                  Password=\"" + _Password + "\"  \r\n");
            sbXml.Append("                  PaymentSource=\"" + _PaymentSource + "\" />\r\n");
           
            return (sbXml.ToString());
        }


        private void button2_Click(object sender, EventArgs e)
        {
            switch(this.cboSampleXml.SelectedIndex) {
                case 0:
                    this.txtXml.Text = string.Empty;
                    break;
                case 1:
                    this.txtXml.Text = GetSampleXmlUpdate();
                    break;
                case 2:
                    this.txtXml.Text = GetSampleXmlAppend();
                    break;
                case 3:
                    this.txtXml.Text = GetSampleXmlDelete();
                    break;
            }
        }
    }
}
