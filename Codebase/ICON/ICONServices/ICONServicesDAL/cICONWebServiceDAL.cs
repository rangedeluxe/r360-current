using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Xml;
using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2015 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013-2015.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     MM/DD/YYYY
*
* Purpose:  Used to access database
*
* Modification History
* CR 33541 JMC 04/09/2011
*    -
* WI 87324 CRG 02/08/2013	
*    Remove SQLICONWebService.cs
* WI 145906 DJW 06/05/2014
*    - Modified to handle the int BatchID to Int64 BatchID 
* WI 192003 TWE 03/04/2015 
*     - Modify to remove ItemProcDal from solution
* WI 193808 TWE 03/11/2015
*     Handle the Item updates
******************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// Used to access database
	/// </summary>
    public class cICONWebServiceDAL : _DALBase {
		/// <summary>
		/// Initializes a new instance of the <see cref="cICONWebServiceDAL" /> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
        public cICONWebServiceDAL(string vSiteKey) : base(vSiteKey)
        {
        }

        /// <summary>
        /// Helper class to write Start / End Elements with "using" syntax...
        /// </summary>
        internal class DisposableElement : IDisposable
        {
            private readonly XmlWriter _xmlWriter;
            public DisposableElement(XmlWriter xmlWriter, string elementName)
            {
                _xmlWriter = xmlWriter;
                xmlWriter.WriteStartElement(elementName);
            }

            public void Dispose()
            {
                _xmlWriter.WriteEndElement();
            }
        }

		/// <summary>
		/// Gets the batch sequence numbers by batch sequence.
		/// </summary>
		/// <param name="processingDate">The processing date.</param>
		/// <param name="bankID">The bank ID.</param>
		/// <param name="customerID">The customer ID.</param>
		/// <param name="lockboxID">The lockbox ID.</param>
		/// <param name="batchID">The batch ID.</param>
		/// <param name="siteCodeID">The site code ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Returns the results of the store procedure call
		/// </returns>
		public bool GetBatchSequenceNumbersByBatchSequence(DateTime processingDate, int bankID, int customerID, int lockboxID, long batchID, int siteCodeID, int BatchSequence, string paymentSource, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmImmutableDate", SqlDbType.DateTime, processingDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteOrganizationID", SqlDbType.Int, customerID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, lockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, batchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSource", SqlDbType.Char, paymentSource, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubSystem.usp_ICONWebGetCheckDocumentBatchSequenceByBatchSequence", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetBatchSequenceNumbersByBatchSequence(DateTime processingDate, int bankID, int customerID, int lockboxID, long batchID, int siteCodeID, int iBatchSequence, out DataTable dt)");
			}

			return bRetVal;
		}

		/// <summary>
		/// Updates the OLTA batch.
		/// </summary>
		/// <param name="XML">The XML.</param>
		/// <param name="ActionCode">The action code.</param>
		/// <param name="NotifyIMS">The notify IMS.</param>
		/// <returns>
		/// Returns the results of the store procedure call
		/// </returns>
		public bool UpdateR360Batch(string XML)
        {
			bool bRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmXML", SqlDbType.Xml, XML, ParameterDirection.Input));
				parms = arParms.ToArray();
                bRetVal = Database.executeProcedure("RecHubSystem.usp_ICON_BatchUpdate", parms);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "UpdateOLTABatch(string XML, int ActionCode, int NotifyIMS)");
			}

			return bRetVal;
		}

        /// <summary>
        /// Data import get image RPS alias mappings.
        /// </summary>
        /// <param name="siteBankID">The site bank ID.</param>
        /// <param name="siteLockboxId">The site lockbox id.</param>
        /// <param name="modficiationDate">The modification date.</param>
        /// <param name="dt">The data table</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetImageRPSAliasMappings(Int32 siteBankID, Int32 siteLockboxId, DateTime? modficiationDate, out DataTable dt)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms = new List<SqlParameter>();
            const string PROCNAME = "RecHubSystem.usp_dimImageRPSAliasMappings_GetRecords";
            dt = null;
            try
            {
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, siteLockboxId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmModificationDate", SqlDbType.DateTime, modficiationDate, ParameterDirection.Input));

                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bRtnval = false;
            }
            if (bRtnval && dt != null)
            {
                string aliasName;
                string extractType;
                string docType;
                string fieldType;
                for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
                {
                    aliasName = dt.Rows[rowCount]["AliasName"].ToString();
                    extractType = dt.Rows[rowCount]["ExtractType"].ToString();
                    docType = dt.Rows[rowCount]["DocType"].ToString();
                    fieldType = dt.Rows[rowCount]["FieldType"].ToString();
                   
                    EventLog.logEvent("    keyword = (" + aliasName +
                                         ", " + extractType +
                                         ", " + fieldType + ")"
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);
                    
                }
            }
            return bRtnval;
        }

        /// <summary>
        /// GetBatchData Setup Fields. This will happen one time at startup
        /// </summary>
        /// <param name="sBatchSourceName">Name of the s batch source.</param>
        /// <param name="modificationDate">The modification date.</param>
        /// <param name="dt">The dt.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetBatchDataSetupField(string sBatchSourceName, DateTime? modificationDate, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmImportShortName", SqlDbType.VarChar, sBatchSourceName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmModificationDate", SqlDbType.DateTime, modificationDate, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_dimBatchDataSetupFields_Get_ByShortName", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "");
            }

            if (bRtnval)
            {
                EventLog.logEvent("GetBatchDataSetupFields Row Count = " + dt.Rows.Count.ToString()
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);

                for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
                {
                    string keyWord = dt.Rows[rowCount]["Keyword"].ToString();
                    int batchDataSetupFieldKey = 0;
                    int dataType = 0;
                    Int32.TryParse(dt.Rows[rowCount]["BatchDataSetupFieldKey"].ToString(), out batchDataSetupFieldKey);
                    Int32.TryParse(dt.Rows[rowCount]["DataType"].ToString(), out dataType);
                    // out put the keyword found to the log file
                    EventLog.logEvent("    keyword = (" + keyWord +
                                         ", " + batchDataSetupFieldKey.ToString() +
                                         ", " + dataType.ToString() + ")"
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);
                }
            }

            return bRtnval;
        }

        /// <summary>
        /// Gets the item data setup field.
        /// </summary>
        /// <param name="sBatchSourceName">Name of the s batch source.</param>
        /// <param name="modificationDate">The modification date.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetItemDataSetupField(string sBatchSourceName, DateTime? modificationDate, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmImportShortName", SqlDbType.VarChar, sBatchSourceName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmModificationDate", SqlDbType.DateTime, modificationDate, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_dimItemDataSetupFields_Get_ByShortName", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetItemDataSetupField(string sBatchSourceName, DateTime? modificationDate, out DataTable dt)");
            }

            if (bRtnval)
            {
                EventLog.logEvent("GetItemDataSetupField Row Count = " + dt.Rows.Count.ToString()
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);

                for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
                {
                    string keyWord = dt.Rows[rowCount]["Keyword"].ToString();
                    int itemDataSetupFieldKey = 0;
                    int dataType = 0;
                    Int32.TryParse(dt.Rows[rowCount]["ItemDataSetupFieldKey"].ToString(), out itemDataSetupFieldKey);
                    Int32.TryParse(dt.Rows[rowCount]["DataType"].ToString(), out dataType);
                    // out put the keyword found to the log file
                    EventLog.logEvent("    keyword = (" + keyWord +
                                         ", " + itemDataSetupFieldKey.ToString() +
                                         ", " + dataType.ToString() + ")"
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);
                }
            }

            return bRtnval;
        }

        public bool InsertFactItemData(int bankKey, int organizationKey, int clientAccountKey,
           int depositDateKey, int immutableDateKey, int sourceProcessingDateKey,
           int batchNumber, short batchSourceKey, long batchID, long sourceBatchID,
           int transactionId, int batchSequence, List<cItemData> listItemData)
        {
            string xml = BuildItemDataXml(listItemData);

            EventLog.logEvent("InsertFactItemData: XML = >>" + xml + "<<"
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Debug);

            bool bRtnval = false;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmBankKey", SqlDbType.Int, bankKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmOrganizationKey", SqlDbType.Int, organizationKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountKey", SqlDbType.Int, clientAccountKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, immutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSourceProcessingDateKey", SqlDbType.Int, sourceProcessingDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchNumber", SqlDbType.Int, batchNumber, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, batchSourceKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, batchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, sourceBatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, transactionId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, batchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmItemData", SqlDbType.Xml, xml, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factItemData_Ins", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertFactItemData(int BankID, int ClientAccountID, long BatchID, int ProcessingDateKey)");
            }

            return bRtnval;
        }

        private string BuildItemDataXml(List<cItemData> listData)
        {
            StringBuilder sbXml = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(sbXml, new XmlWriterSettings() { OmitXmlDeclaration = true });
            using (new DisposableElement(xmlWriter, "Root"))
            {
                using (new DisposableElement(xmlWriter, "ItemDataRows"))
                {
                    foreach (var data in listData)
                    {
                        using (new DisposableElement(xmlWriter, "ItemDataRow"))
                        {
                            xmlWriter.WriteAttributeString("DataSetupFieldKey", data.DataSetupFieldKey.ToString());
                            xmlWriter.WriteAttributeString("DataValue", data.DataValue);
                            switch (data.DataType)
                            {
                                case (int)WFSDataTypes.STRING:
                                    break;
                                case (int)WFSDataTypes.INT:
                                    xmlWriter.WriteAttributeString("DataValueInteger", data.DataValue);
                                    break;
                                case (int)WFSDataTypes.BIT:
                                    xmlWriter.WriteAttributeString("DataValueBit", data.DataValue);
                                    break;
                                case (int)WFSDataTypes.FLOAT:
                                    break;
                                case (int)WFSDataTypes.CURRENCY:
                                    xmlWriter.WriteAttributeString("DataValueMoney", data.DataValue);
                                    break;
                                case (int)WFSDataTypes.DATE:
                                    xmlWriter.WriteAttributeString("DataValueDateTime", data.DataValue);
                                    break;
                            }
                        }
                    }
                }
            }

            // Return the XML string
            xmlWriter.Flush();
            return sbXml.ToString();
        }

        public bool InsertFactBatchData(int bankKey, int organizationKey, int clientAccountKey,
            int immutableDateKey, int depositDateKey, int sourceProcessingDateKey,
            int batchNumber, short batchSourceKey, long batchID, long sourceBatchID, List<cBatchData> arBatchData)
        {
            string xml = BuildBatchDataXml(arBatchData);

            bool bRtnval = false;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmBankKey", SqlDbType.Int, bankKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmOrganizationKey", SqlDbType.Int, organizationKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountKey", SqlDbType.Int, clientAccountKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, immutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSourceProcessingDateKey", SqlDbType.Int, sourceProcessingDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchNumber", SqlDbType.Int, batchNumber, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, batchSourceKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, batchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, sourceBatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmItemData", SqlDbType.Xml, xml, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factBatchData_Ins", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertFactBatchData(int BankID, int ClientAccountID, long BatchID, int ProcessingDateKey)");
            }

            return bRtnval;
        }

        private string BuildBatchDataXml(List<cBatchData> listData)
        {
            StringBuilder sbXml = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(sbXml, new XmlWriterSettings() { OmitXmlDeclaration = true });
            using (new DisposableElement(xmlWriter, "Root"))
            {
                using (new DisposableElement(xmlWriter, "BatchDataRows"))
                {
                    foreach (var data in listData)
                    {
                        using (new DisposableElement(xmlWriter, "BatchDataRow"))
                        {
                            //xmlWriter.WriteAttributeString("BatchNumber", itemData..ID);
                            xmlWriter.WriteAttributeString("DataSetupFieldKey", data.DataSetupFieldKey.ToString());
                            xmlWriter.WriteAttributeString("DataValue", data.DataValue);
                            switch (data.DataType)
                            {
                                case (int)WFSDataTypes.STRING:
                                    break;
                                case (int)WFSDataTypes.INT:
                                    xmlWriter.WriteAttributeString("DataValueInteger", data.DataValue);
                                    break;
                                case (int)WFSDataTypes.BIT:
                                    xmlWriter.WriteAttributeString("DataValueBit", data.DataValue);
                                    break;
                                case (int)WFSDataTypes.FLOAT:
                                    break;
                                case (int)WFSDataTypes.CURRENCY:
                                    xmlWriter.WriteAttributeString("DataValueMoney", data.DataValue);
                                    break;
                                case (int)WFSDataTypes.DATE:
                                    xmlWriter.WriteAttributeString("DataValueDateTime", data.DataValue);
                                    break;
                            }
                        }
                    }
                }
            }

            // Return the XML string
            xmlWriter.Flush();
            return sbXml.ToString();
        }

        /// <summary>
        /// Updates the fact item data.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="ProcessingDateKey">The processing date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="ItemData">The item data.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool UpdateFactItemData(int BankID, int LockboxID, int ProcessingDateKey, long BatchID, int BatchSequence, cItemData ItemData)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ProcessingDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmItemDataSetupFieldKey", SqlDbType.Int, ItemData.DataSetupFieldKey.ToString(), ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDataValue", SqlDbType.VarChar, ItemData.DataValue, ParameterDirection.Input));

                // add datetime parm
                if (ItemData.DataType == (int)WFSDataTypes.DATE)
                {
                    arParms.Add(BuildParameter("@parmDataValueDateTime", SqlDbType.DateTime, ItemData.DataValue, ParameterDirection.Input));
                }
                else
                    // add Money parm
                    if (ItemData.DataType == (int)WFSDataTypes.FLOAT ||
                        ItemData.DataType == (int)WFSDataTypes.CURRENCY)
                    {
                        arParms.Add(BuildParameter("@parmDataValueMoney", SqlDbType.Money, ItemData.DataValue, ParameterDirection.Input));
                    }
                    else
                        // add Integer parm
                        if (ItemData.DataType == (int)WFSDataTypes.INT)
                        {
                            arParms.Add(BuildParameter("@parmDataValueInteger", SqlDbType.Int, ItemData.DataValue, ParameterDirection.Input));
                        }
                        else
                            // add bit parm
                            if (ItemData.DataType == (int)WFSDataTypes.BIT)
                            {
                                arParms.Add(BuildParameter("@parmDataValueBit", SqlDbType.Bit, ItemData.DataValue, ParameterDirection.Input));
                            }
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factItemData_Upd_DataValue", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateFactItemData(int BankID, int ClientAccountID, int ProcessingDateKey, long BatchID, int BatchSequence, cItemData ItemData)");
            }

            return bRtnval;
        }

        /// <summary>
        /// Appends the fact item data.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="ProcessingDateKey">The processing date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="ItemData">The item data.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool AppendFactItemData(int BankID, int LockboxID, int ProcessingDateKey, long BatchID, int BatchSequence, cItemData ItemData)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ProcessingDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmItemDataSetupFieldKey", SqlDbType.Int, ItemData.DataSetupFieldKey.ToString(), ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDataValue", SqlDbType.VarChar, ItemData.DataValue, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factItemData_Upd_AppendDataValue", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "AppendFactItemData(int BankID, int ClientAccountID, int ProcessingDateKey, long BatchID, int BatchSequence, cItemData ItemData)");
            }

            return bRtnval;
        }

        #region BatchInformation

        /// <summary>
        /// Gets the item data by item
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="BankID"></param>
        /// <param name="clientAccountID"></param>
        /// <param name="BatchID"></param>
        /// <param name="immutableDatekey"></param>
        /// <param name="BatchSequence"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetItemDataByItem(int BankID, int clientAccountID, long BatchID, int immutableDatekey, int BatchSequence, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, clientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, immutableDatekey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_FactItemData_Get", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetItemData(int BankID, int clientAccountID, long BatchID, DateTime DepositDate, int BatchSequence, out DataTable dt)");
            }

            return bRtnval;
        }

        /// <summary>
        /// Gets the batch data  
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="BankID"></param>
        /// <param name="clientAccountID"></param>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool GetBatchData(int BankID, int clientAccountID, long BatchID, int DepositDate, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, clientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, DepositDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factBatchData_Get", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchData(int BankID, int clientAccountID, long BatchID, DateTime DepositDate, out DataTable dt)");
            }

            return bRtnval;
        }

        /// <summary>
        /// Gets the check2.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="ImmutableDateKey">The processing date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetCheck2(int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factChecks_Get_ByImmutableDate", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetCheck2");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the stub.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="ImmutableDateKey">The processing date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchSequence">The batch sequence.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetStub(int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            //DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factStubs_Get_ByBatchSequence", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetStub(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out DataTable dt)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the batch.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="ImmutableDateKey">The immutable date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetBatch(int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ImmutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_BySiteBankIDSiteClientAccountID", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatch");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        /// <summary>
        /// Gets the batch.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The client account ID.</param>
        /// <param name="ImmutableDateKey">The immutable date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool UpdateBatchLevel(long batchID, int depositDateKey, 
            int sourceProcessingDateKey, int batchNumber, string batchPaymentType)
        {
            bool bolRetVal = false;
            
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                //keys
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, batchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input));
                //data to update
                arParms.Add(BuildParameter("@parmSourceProcessingDateKey", SqlDbType.Int, sourceProcessingDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchNumber", SqlDbType.Int, batchNumber, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchPaymentType", SqlDbType.Char, batchPaymentType, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubSystem.usp_ICON_UpdateBatchLevelInfo", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateBatchLevel");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        public bool GetBatchID(int immutableDateKey, int siteBankID, int siteOrganizationID, int siteClientAccountID, 
            int siteCodeID, long sourceBatchID, string batchSource, 
            out long batchID, out int depositDateKey, out int batchNumber, out short batchPaymentTypeKey, out int depositStatus)
        {
            bool bolRetVal = false;
            batchID = -1;
            depositDateKey = -1;
            batchNumber = -1;
            batchPaymentTypeKey = 0;
            depositStatus = -1;
            SqlParameter parmBatchID;
            SqlParameter parmDepositDate;
            SqlParameter parmBatchNumber;
            SqlParameter parmBatchPaymentTypeKey;
            SqlParameter parmDepositStatus;
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, immutableDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteOrganizationID", SqlDbType.Int, siteOrganizationID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, sourceBatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSource", SqlDbType.VarChar, batchSource, ParameterDirection.Input));
                parmBatchID = BuildParameter("@parmBatchID", SqlDbType.BigInt, 0, ParameterDirection.Output);
                arParms.Add(parmBatchID);
                parmDepositDate = BuildParameter("@parmDepositDateKey", SqlDbType.Int, 0, ParameterDirection.Output);
                arParms.Add(parmDepositDate);
                parmBatchNumber = BuildParameter("@parmBatchNumber", SqlDbType.Int, 0, ParameterDirection.Output);
                arParms.Add(parmBatchNumber);
                parmBatchPaymentTypeKey = BuildParameter("@parmBatchPaymentTypeKey", SqlDbType.TinyInt, 0, ParameterDirection.Output);
                arParms.Add(parmBatchPaymentTypeKey);
                parmDepositStatus = BuildParameter("@parmDepositStatus", SqlDbType.Int, 0, ParameterDirection.Output);
                arParms.Add(parmDepositStatus);
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_BatchID", parms);
                if (bolRetVal)
                {
                    batchID = (long)parmBatchID.Value;
                    depositDateKey = (int)parmDepositDate.Value;
                    batchNumber = (int)parmBatchNumber.Value;
                    batchPaymentTypeKey = (byte)parmBatchPaymentTypeKey.Value;
                    depositStatus = (int)parmDepositStatus.Value;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatch");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        /// <summary>
        /// Updates the fact batch data.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="ProcessingDateKey">The processing date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchData">The batch data.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool UpdateFactBatchData(int BankID, int LockboxID, int ProcessingDateKey, long BatchID, cBatchData BatchData)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ProcessingDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchDataSetupFieldKey", SqlDbType.Int, BatchData.DataSetupFieldKey.ToString(), ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDataValue", SqlDbType.VarChar, BatchData.DataValue, ParameterDirection.Input));
                // add datetime parm
                if (BatchData.DataType == (int)WFSDataTypes.DATE)
                {
                    arParms.Add(BuildParameter("@parmDataValueDateTime", SqlDbType.DateTime, BatchData.DataValue, ParameterDirection.Input));
                }
                else
                    // add Money parm
                    if (BatchData.DataType == (int)WFSDataTypes.FLOAT ||
                        BatchData.DataType == (int)WFSDataTypes.CURRENCY)
                    {
                        arParms.Add(BuildParameter("@parmDataValueMoney", SqlDbType.Money, BatchData.DataValue, ParameterDirection.Input));
                    }
                    else
                        // add Integer parm
                        if (BatchData.DataType == (int)WFSDataTypes.INT)
                        {
                            arParms.Add(BuildParameter("@parmDataValueInteger", SqlDbType.Int, BatchData.DataValue, ParameterDirection.Input));
                        }
                        else
                            // add bit parm
                            if (BatchData.DataType == (int)WFSDataTypes.BIT)
                            {
                                arParms.Add(BuildParameter("@parmDataValueBit", SqlDbType.Bit, BatchData.DataValue, ParameterDirection.Input));
                            }
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factBatchData_Upd_DataValue", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateFactBatchData(int BankID, int ClientAccountID, int ProcessingDateKey, long BatchID, cBatchData BatchData)");
            }

            return bRtnval;
        }

        /// <summary>
        /// Appends the fact batch data.
        /// </summary>
        /// <param name="BankID">The bank ID.</param>
        /// <param name="ClientAccountID">The lockbox ID.</param>
        /// <param name="ProcessingDateKey">The processing date key.</param>
        /// <param name="BatchID">The batch ID.</param>
        /// <param name="BatchData">The batch data.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool AppendFactBatchData(int BankID, int LockboxID, int ProcessingDateKey, long BatchID, cBatchData BatchData)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ProcessingDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchDataSetupFieldKey", SqlDbType.Int, BatchData.DataSetupFieldKey.ToString(), ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDataValue", SqlDbType.VarChar, BatchData.DataValue, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factBatchData_Upd_AppendDataValue", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "AppendFactBatchData(int BankID, int ClientAccountID, int ProcessingDateKey, long BatchID, cBatchData BatchData)");
            }

            return bRtnval;
        }


        #endregion

        /// <summary>
        /// Delete all fact batch info
        /// </summary>
        /// <param name="BankID">The unique bank identifier.</param>
        /// <param name="ClientAccountID">The unique client account identifier.</param>
        /// <param name="BatchID">The unique batch identifer.</param>
        /// <param name="ProcessingDateKey">The processing date key.</param>
        /// <returns>
        /// True/False whether the call was successful.
        /// </returns>
        public bool DeleteAllFactData(int BankId, int ClientAccountId, long BatchId, int ProcessingDateKey, int depositDateKey, string batchSource)
        {
            bool bRtnval = false;
            SqlParameter parmRowDeleted;
            SqlParameter[] parms;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, BankId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, ClientAccountId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, BatchId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, ProcessingDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchSource", SqlDbType.VarChar, batchSource, ParameterDirection.Input));
                parmRowDeleted = BuildParameter("@parmRowDeleted", SqlDbType.Bit, 0, ParameterDirection.Output);
                arParms.Add(parmRowDeleted);
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubData.usp_factBatch_Delete", parms);
                if (bRtnval && !(bool)parmRowDeleted.Value)
                {
                    EventLog.logWarning("Nothing deleted:  ", this.GetType().Name, MessageImportance.Debug);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DeleteAllFactData(int BankId, int ClientAccountId, long BatchId, int ProcessingDateKey, int depositDateKey, string batchSource)");
            }

            return bRtnval;
        }
    }

}
