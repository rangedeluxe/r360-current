﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.integraPAY.Online.ICONServicesAPI;
using WFS.integraPAY.Online.ICONServicesAPI.Fakes;
using WFS.RecHub.ExternalLogonService.Fakes;
using Microsoft.QualityTools.Testing.Fakes;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ExternalLogonService;


namespace UnitTests
{
    [TestClass]
    public class ICONServicesTests
    {
        [TestMethod]
        public void IconWebService_LogonWithInvalidCredentials_ShouldReturnFalse()
        {
            //arrange
            var expected = false;
            int user;
            Guid theGuid;
            string theMessage;

            using (ShimsContext.Create())
            {
                ShimExternalLogonClient.Constructor = (ExternalLogonClient client) => { return; };
                ShimExternalLogonClient.AllInstances.ExternalLogonExternalLogonRequest = (ExternalLogonClient client, ExternalLogonRequest request) =>
                    {
                        return new ExternalLogonResponse{Status = StatusCode.FAIL};
                    };

                //act
                var actual = ICONWebService.Logon("userName", "password", out user, out theGuid, out theMessage);
                
                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void IconWebService_LogonWithValidCredentials_ShouldReturnTrue()
        {
            //arrange
            var expected = true;
            int user;
            Guid theGuid;
            string theMessage;

            using (ShimsContext.Create())
            {
                ShimExternalLogonClient.Constructor = (client) => { return; };
                ShimExternalLogonClient.AllInstances.ExternalLogonExternalLogonRequest = (ExternalLogonClient client, ExternalLogonRequest  request) =>
                {
                    return new ExternalLogonResponse { Status = StatusCode.SUCCESS };
                };

                //act
                var actual = ICONWebService.Logon("userName", "password", out user, out theGuid, out theMessage);

                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void IconWebService_SendingInEmptyXmlStringToTheProcessCommand_ReturnsErrorMessage()
        {
            //arrange
            var expected = "False - No XML found to process";
            var service = new ICONWebService("hackingSiteKey");

            //act
            var actual = service.ProcessXMLCommand(string.Empty);
            
            //assert
            Assert.AreEqual(expected, actual, true);
        }

        [TestMethod]
        public void IconWebService_ErrorProcessingIncomingXmlString_ReturnsErrorMessage()
        {
            //arrange
            var expected = "False - error during processing";
            var service = new ICONWebService("hackingSiteKey");

            //act
            var actual = service.ProcessXMLCommand("This string should not work");

            //assert
            Assert.AreEqual(expected, actual, true);
        }

        [TestMethod]
        public void IconWebService_ProcessingValidXmlString_ReturnsSuccessMessage()
        {
            //arrange
            var expected = "True";
            var service = new ICONWebService("hackingSiteKey");
            
            using (ShimsContext.Create())
            {
                ShimICONWebService.AllInstances.ProcessXMLStringStringOut = (ICONWebService webService, string xml, out string returnString) => 
                { 
                    returnString = string.Empty; 
                    return true; 
                };
                //act
                var actual = service.ProcessXMLCommand("Pretend this is valid xml");

                //assert
                StringAssert.Contains(actual, expected);
            }
           
        }
              
    }
}
