﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Wayne Schwarz
* Date:     4/18/2013
*
* Purpose:  ICON Web Service Options
*
* Modification History
*   04/18/2013 WJS
*        - * WI 96342  Created class
*
******************************************************************************/
namespace WFS.integraPAY.Online.ICONServiceSvc
{
    public class cICONWebServiceSvcOptions : cSiteOptions
    {

        private const string INIKEY_HOST_WCF_ICONWEBSERVICES = "HostWCFICONWebServices";
        private const string INIKEY_ICONWEBSERVICE_URL = "ICONWebService_URL";
        private const string INIKEY_ICONASMX_PHYSICALPATH = "PhysicalPathICONWebServiceASMX";
       //************************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteKey"></param>
        //************************************************************************
        public cICONWebServiceSvcOptions(string siteKey): base(siteKey)
        {

            StringCollection colSiteOptions = ipoINILib.GetINISection(siteKey);
            string strKey;
            string strValue;

            

            System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {
                 strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                 strValue = myEnumerator.Current.ToString().Substring(strKey.Length + 1);
                 if (strKey.ToLower() == INIKEY_HOST_WCF_ICONWEBSERVICES.ToLower()) {
                    List<string> lstTrueValues = new List<string>(new string[] {
                            "true", "t", "yes", "1"});
                    Host_WCF_ICONWEBServices = lstTrueValues.Contains(strValue.ToLower());
                }else if (strKey.ToLower() == INIKEY_ICONWEBSERVICE_URL.ToLower()) {
                    ICONWebService_URL = strValue;
                } else if (strKey.ToLower() == INIKEY_ICONASMX_PHYSICALPATH.ToLower()){
                    PhysicalPathICONWebServiceASMX = strValue;
               } 
            }
        }

        public bool Host_WCF_ICONWEBServices
        {
            get;
            set;
        }
        public string ICONWebService_URL
        {
            get;
            set;
        }
        public string PhysicalPathICONWebServiceASMX
        {
            get;
            set;
        }
    }

}
