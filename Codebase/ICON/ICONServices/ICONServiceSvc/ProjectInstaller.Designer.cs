﻿namespace WFS.integraPAY.Online.ICONServiceSvc
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ICONWebServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ICONWebServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ICONWebServiceProcessInstaller
            // 
            this.ICONWebServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ICONWebServiceProcessInstaller.Password = null;
            this.ICONWebServiceProcessInstaller.Username = null;
            // 
            // ICONWebServiceInstaller
            // 
            this.ICONWebServiceInstaller.ServiceName = "WFS ICON Web Service";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ICONWebServiceProcessInstaller,
            this.ICONWebServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ICONWebServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ICONWebServiceInstaller;
    }
}