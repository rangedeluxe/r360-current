﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.ServiceModel;
using System.Net;
using System.IO;
using WFS.RecHub.Common.Log;
using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Wayne Schwarz
* Date:     4/9/2013
*
* Purpose:  ICON Web Service Service.
*
* Modification History
* 04/09/2013 WJS
*      - WI 96432  Created class
*
******************************************************************************/
namespace WFS.integraPAY.Online.ICONServiceSvc
{
    public partial class ICONWebServiceSvc : ServiceBase
    {
        //move this to ipoLib at some point
        public const string INISECTION_ICONWEBSERVICE_SERVICE = "ICONWebServiceService";
        private cICONWebServiceSvcOptions _ServerOptions = null;
        private cEventLog _EventLog = null;
        private HttpListenerController _controller;

        /// <summary>
        /// 
        /// </summary>
        private cEventLog eventLog
        {
            get
            {
                if (_EventLog == null)
                {
                    _EventLog = new cEventLog(serverOptions.logFilePath
                                                , serverOptions.logFileMaxSize
                                                , (MessageImportance)serverOptions.loggingDepth);
                }

                return _EventLog;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private cICONWebServiceSvcOptions serverOptions
        {
            get
            {
                try
                {
                    if (_ServerOptions == null)
                    {
                        _ServerOptions = new cICONWebServiceSvcOptions
                            (INISECTION_ICONWEBSERVICE_SERVICE);
                    }
                }
                catch { }

                return _ServerOptions;
            }
        }

        public ICONWebServiceSvc()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                eventLog.logEvent("Attempting to host WCF ICON Web Service. Value of HostWCFICONWebServices is:" + serverOptions.Host_WCF_ICONWEBServices.ToString(), "ICONWebServiceSvc",
                            MessageType.Information, MessageImportance.Debug);
                if (serverOptions.Host_WCF_ICONWEBServices)
                {

                    eventLog.logEvent("Attempting to host ICON Web Service", "ICONWebServiceSvc",
                            MessageType.Information, MessageImportance.Debug);
                    try
                    {
                        eventLog.logEvent("ICON Web Service URL to host: " + serverOptions.ICONWebService_URL, "ICONWebServiceSvc",
                            MessageType.Information, MessageImportance.Debug);
                        if (serverOptions.ICONWebService_URL != string.Empty)
                        {
                            string[] prefixs = new string[1];
                            string vdir = "/"; ;
                            Uri uri = new Uri(serverOptions.ICONWebService_URL);
                            string absolutPath = uri.AbsolutePath;
                            prefixs[0] = uri.Scheme + "://" + uri.Host;
                            for (int i = 0; i < uri.Segments.Length - 1; i++)
                            {
                                prefixs[0] += uri.Segments[i];
                                if (i > 0)
                                {
                                    vdir += uri.Segments[i];
                                }
                            }
                            eventLog.logEvent("ICON Physical Path for ASMX is: " + serverOptions.PhysicalPathICONWebServiceASMX, "ICONWebServiceSvc",
                                    MessageType.Information, MessageImportance.Debug);

                            if (!File.Exists(Path.Combine(serverOptions.PhysicalPathICONWebServiceASMX, "service.asmx")))
                            {
                                eventLog.logEvent("Could not find service.asmx for ICON Web Service in the specified location: " + serverOptions.PhysicalPathICONWebServiceASMX, "ICONWebServiceSvc",
                                    MessageType.Information, MessageImportance.Essential);
                            }
                            _controller = new HttpListenerController(prefixs, vdir, serverOptions.PhysicalPathICONWebServiceASMX);
                            _controller.Start();
                            //wait to see if error has occurred (wait for a pump of the thread;
                            System.Threading.Thread.Sleep(2000);
                            if (_controller.HasErrorOcurred())
                            {
                                eventLog.logEvent("Unable to start ICON Service." + serverOptions.ICONWebService_URL + "  Check event log for more information.", "ICONWebServiceSvc",
                                         MessageType.Information, MessageImportance.Essential);
                                throw new Exception();

                            }
                        }
                        else
                        {
                            eventLog.logEvent("URL to host: " + serverOptions.ICONWebService_URL + "  need to be filled out fo the hosting to work. Physical path needs to be filled out: " + serverOptions.PhysicalPathICONWebServiceASMX, "ICONWebServiceSvc",
                               MessageType.Information, MessageImportance.Essential);
                        }


                        eventLog.logEvent("URL to host: " + serverOptions.ICONWebService_URL, "ICONWebServiceSvc",
                                MessageType.Information, MessageImportance.Debug);


                    }
                    catch (Exception ex)
                    {
                        eventLog.logEvent(string.Format("Failed to host WCF ICON Web Service: {0}", ex.Message),
                                "ICONWebServiceSvc", MessageType.Error, MessageImportance.Essential);
                        throw new Exception();
                    }
                }

                eventLog.logEvent("ICON Web  Service started normally."
                                , this.GetType().Name
                                , MessageImportance.Essential);
            }
            catch (Exception e)
            {
                eventLog.logEvent("ICON Web Service started with errors : " + e.Message
                                , e.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
                throw new Exception("Service should not start");
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (_controller != null)
                {
                    _controller.Stop();
                }
            }
            catch (AppDomainUnloadedException unloadedEx)
            {
                try
                {
                    eventLog.logWarning("Underlying ASMX service has terminated sometime before this stop request.",
                        MessageImportance.Essential);
                }
                catch { }
            }
            catch (Exception ex)
            {
                try
                {
                    eventLog.logEvent(
                        string.Format("ICON Web failed to stop in a timely manner. Error: {0}", ex.Message)
                        , this.GetType().Name
                        , MessageImportance.Essential);
                }
                catch { }
            }
        }
        void _whbWebHost_LogEvent(string sMessage, string sSource, MessageType mstEventType, MessageImportance msiEventImportance)
        {
            eventLog.logEvent(sMessage, sSource, mstEventType, msiEventImportance);
        }
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "WFS ICON Web Service";
        }

    }
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ICONWebServiceSvc()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
