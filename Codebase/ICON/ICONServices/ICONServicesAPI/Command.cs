using System;
using System.Collections.Generic;
using System.Text;

using WFS.integraPAY.Online.ICONCommon;

/******************************************************************************
** Wausau
** Copyright � 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     02/09/2010
*
* Purpose:  integraPAY Online Services ImageRPS XML Command object.
*
* Modification History
* CR 28461 MEH 02/09/2010
*    -Initial release.
* CR 33541 JMC 04/09/2011
*    -Modified class to use IMSKeyword instead of Keyword object.  This was
*     done so that both the Windows and Web service can share business logic.
******************************************************************************/

namespace WFS.integraPAY.Online.ICONServicesAPI
{
    internal class Command
    {
        private string _sCommandType = string.Empty;
        public List<IMSKeywordPair> listQueryKeywords = null;
        public List<IMSKeywordPair> listActionKeywords = null;
       
        public Command(string sCommandType)
        {
            listQueryKeywords = new List<IMSKeywordPair>();
            listActionKeywords = new List<IMSKeywordPair>();

            _sCommandType = sCommandType;
        }

        public string CommandType
        {
            get { return (_sCommandType); }
            set { _sCommandType = value; }
        }

        public void AddQueryKeyword(IMSKeywordPair kwQueryKeyword)
        {
            listQueryKeywords.Add(kwQueryKeyword);
        }

        public void AddActionKeyword(IMSKeywordPair kwActionKeyword)
        {
            listActionKeywords.Add(kwActionKeyword);
        }
    }
}
