﻿using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     02/09/2010
*
* Purpose:  IntegraPAY Online Services Keyword object.
*
* Modification History
* 02/09/2010 CR 28461 MEH
*     - Initial release.
******************************************************************************/

namespace WFS.integraPAY.Online.OLFICONServicesAPI
{
    internal class Keyword
    {
        private string _sName = string.Empty;
        private string _sValue = string.Empty;

        public Keyword(string sName, string sValue)
        {
            this._sName = sName;
            this._sValue = sValue;
        }

        public string Name
        {
            get { return (_sName); }
            set { _sName = value; }
        }

        public string Value
        {
            get { return (_sValue); }
            set { _sValue = value; }
        }
    }
}
