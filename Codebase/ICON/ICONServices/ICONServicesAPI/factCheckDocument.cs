﻿using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2015 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008-2015.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     02/09/2010
*
* Purpose:  IntegraPAY Online Services factCheck/factDocument object.
*
* Modification History
* 02/09/2010 CR 28461 MEH
*     - Initial release.
* WI 193808 TWE 03/11/2015
*     Handle the Item updates
******************************************************************************/

namespace WFS.integraPAY.Online.ICONServicesAPI
{
    internal class factCheckDocument
    {
        private int _TransactionID = -1;
        private int _BatchSequence = -1;
        private int _TxnSequence = -1;
        private int _Sequence = -1;
        private bool _IsCheck = false;
        private bool _IsDocument = false;
        private bool _IsStub = false;
        private int _BatchNumber = -1;
        private short _BatchPaymentTypeKey = -1;
        private int _DepositStatus = -1;

        public factCheckDocument(int TransactionIDValue, int BatchSequenceValue, int TxnSequenceValue, int SequenceValue, 
            bool IsCheckValue, bool IsDocumentValue, bool IsStubValue, 
            int batchNumber, short batchPaymentTypeKey, int depositStatus)
        {
            _TransactionID = TransactionIDValue;
            _BatchSequence = BatchSequenceValue;
            _TxnSequence = TxnSequenceValue;
            _Sequence = SequenceValue;
            _IsCheck = IsCheckValue;
            _IsDocument = IsDocumentValue;
            _IsStub = IsStubValue;
            _BatchNumber = batchNumber;
            _BatchPaymentTypeKey = batchPaymentTypeKey;
            _DepositStatus = depositStatus;
        }

        public int BatchNumber
        {
            get { return _BatchNumber; }
            set { _BatchNumber = value; }
        }

        public short BatchPaymentTypeKey
        {
            get { return _BatchPaymentTypeKey; }
            set { _BatchPaymentTypeKey = value; }
        }

        public int DepositStatus
        {
            get { return _DepositStatus; }
            set { _DepositStatus = value; }
        }

        public int TransactionID
        {
            get { return (_TransactionID); }
            set { _TransactionID = value; }
        }

        public int BatchSequence
        {
            get { return (_BatchSequence); }
            set { _BatchSequence = value; }
        }

        public int TxnSequence
        {
            get { return (_TxnSequence); }
            set { _TxnSequence = value; }
        }

        public int Sequence
        {
            get { return (_Sequence); }
            set { _Sequence = value; }
        }

        public bool IsCheck
        {
            get { return (_IsCheck); }
            set { _IsCheck = value; }
        }

        public bool IsDocument
        {
            get { return (_IsDocument); }
            set { _IsDocument = value; }
        }

        public bool IsStub
        {
            get { return (_IsStub); }
            set { _IsStub = value; }
        }
    }
}
