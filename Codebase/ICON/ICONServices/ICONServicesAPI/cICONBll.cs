using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using WFS.RecHub.DAL;
using WFS.integraPAY.Online.ICONCommon;
using WFS.RecHub.Common.Log;
using WFS.RecHub.Common;
/******************************************************************************
** Wausau
** Copyright � 1997-2015 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008-2015.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    3/2/2011
*
* Purpose:  cICONBll.cs.
*
* Modification History
* CR 31536 WJS 03/02/2011 
*    -Initial release.
* CR 34395 JMC 05/19/2011
*    -Modified logic that determines whether an item a business or personal 
*     check.
* CR 45047 JMC 05/19/2011
*    -Modified ZipFile() method to include logic to rename new zip file if a
*     file already exists with the same name.
* CR 47269  WJS 10/10/2011 
*  - Change processing date to use the system date instead
* CR 49232 WJS 1/12/2012
 * - FP:Look for BEGIN,END, Self processing Tag DIP as exact match as opposed to contains
 * - Change calls to indicate by ref
 * CR 49675 WJS 1/30/2012
 *  - Pass in source processing date key
* WI 145906 DJW 06/05/2014
*    - Modified to handle the int BatchID to Int64 BatchID 
* WI 192197 TWE 02/25/2015
*    Pass the extra parms needed for the stored procedure delete
* WI 192003 TWE 03/04/2015 
*     - Modify to remove ItemProcDal from solution
* WI 193808 TWE 03/11/2015
*     Handle the Item updates
* WI 198682 TWE 03/31/2015
*     Correct logic to locate Batchid
******************************************************************************/
namespace WFS.integraPAY.Online.ICONServicesAPI 
{
    public class cICONBll: _WFSObjectRoot {

           private cMagicKeywords _MagicKeywords = null;
        
           private Hashtable _itemDataSetupFieldsHashTable = null;
           private Hashtable _batchDataSetupFieldsHashTable = null;
           private Hashtable       _imageRPSAliasKeywordHashTable      = null;
           private cEventLog EventLog = null;
           /// <summary>
           /// Public Constructor.  Sets the SiteKey for the class.
           /// </summary>
           public cICONBll(string siteKey, cEventLog log)
           {
               this.SiteKey=siteKey;
               this.EventLog = log;
               
           }

           public Hashtable  BatchDataSetupFieldsTable
           {
               get
               {
                   if (_batchDataSetupFieldsHashTable == null)
                   {
                       _batchDataSetupFieldsHashTable = new Hashtable();
                       GetBatchDataSetupFields();
                   }
                   return _batchDataSetupFieldsHashTable;
               }
           }

           public Hashtable ItemDataSetupFieldsTable
           {
               get
               {
                   if (_itemDataSetupFieldsHashTable == null)
                   {
                       _itemDataSetupFieldsHashTable = new Hashtable();
                       GetItemDataSetupFields();
                   }
                   return _itemDataSetupFieldsHashTable;
               }
           }

           public cMagicKeywords ImageRPSMagicKeywords {
               get {
                   if(_MagicKeywords == null) {
                       _MagicKeywords = new cMagicKeywords();
                   }
                   return(_MagicKeywords);
               }
           }

           public bool UpdateFactItemData(IICONItemIDFields ItemIDFields,
                                           List<WFS.RecHub.Common.cItemData> listItemData,
                                           bool IsCheck,
                                           UpdateType UpdateTypeValue)
           {

               bool bolRetVal = true;
               DataTable dt;
               Hashtable htDataValues;

               if (ICONDAL.GetItemDataByItem(ItemIDFields.BankID,
                                                 ItemIDFields.ClientAccountID,
                                                 ItemIDFields.BatchID,
                                                 ItemIDFields.ProcessingDateKey,
                                                 ItemIDFields.BatchSequence,
                                                 out dt))
               {

                   htDataValues = new Hashtable();
                   foreach (DataRow dr in dt.Rows)
                   {
                       if (!htDataValues.ContainsKey(dr["Keyword"].ToString().Trim().ToLower()))
                       {
                           htDataValues.Add(dr["Keyword"].ToString().Trim().ToLower(), dr["DataValue"].ToString());
                       }
                   }

                   foreach (WFS.RecHub.Common.cItemData itemdata in listItemData)
                   {

                       if (ItemDataSetupFieldsTable.ContainsKey(itemdata.Keyword))
                       {

                           if (htDataValues.ContainsKey(itemdata.Keyword.Trim().ToLower()))
                           {                               
                               if (UpdateTypeValue == UpdateType.Update)
                               {
                                   EventLog.logEvent("UpdateFactItemData: Update keyword = '" + itemdata.Keyword + "'"
                                      , this.GetType().Name
                                      , MessageType.Information
                                      , MessageImportance.Debug);

                                   if (!ICONDAL.UpdateFactItemData(ItemIDFields.BankID,
                                                                       ItemIDFields.ClientAccountID,
                                                                       ItemIDFields.ProcessingDateKey,
                                                                       ItemIDFields.BatchID,
                                                                       ItemIDFields.BatchSequence,
                                                                       itemdata))
                                   {
                                       bolRetVal = false;
                                   }
                               }
                               else
                               {
                                   EventLog.logEvent("UpdateFactItemData: Append keyword = '" + itemdata.Keyword + "'"
                                      , this.GetType().Name
                                      , MessageType.Information
                                      , MessageImportance.Debug);

                                   if (!ICONDAL.AppendFactItemData(ItemIDFields.BankID,
                                                                          ItemIDFields.ClientAccountID,
                                                                          ItemIDFields.ProcessingDateKey,
                                                                          ItemIDFields.BatchID,
                                                                          ItemIDFields.BatchSequence,
                                                                          itemdata))
                                   {
                                       bolRetVal = false;
                                   }
                               }
                           }
                           else
                           {
                               EventLog.logEvent("UpdateFactItemData: Insert keyword = '" + itemdata.Keyword + "'"
                                      , this.GetType().Name
                                      , MessageType.Information
                                      , MessageImportance.Debug);

                               if (!InsertFactItemData(ItemIDFields, itemdata, IsCheck))
                               {
                                   bolRetVal = false;
                               }
                           }
                       }
                       else
                       {
                           // Nothing to update
                       }
                   }
               }
               else
               {
                   // Exception
               }

               return (bolRetVal);
           }

           public bool UpdateFactBatchData(IICONBatchIDFields BatchIDFields,
                                            List<WFS.RecHub.Common.cBatchData> listBatchData,
                                            UpdateType UpdateTypeValue)
           {
               bool bolRetVal = true;
               DataTable dt;
               Hashtable htDataValues;

               if (ICONDAL.GetBatchData(BatchIDFields.BankID,
                                            BatchIDFields.ClientAccountID,
                                            BatchIDFields.BatchID,
                                            BatchIDFields.ProcessingDateKey,
                                            out dt))
               {

                   //locate current value special fields
                   int batchNumber = (int)dt.Rows[0]["BatchNumber"];
                   int sourceProcessingDateKey = (int)dt.Rows[0]["SourceProcessingDateKey"];
                   string batchPaymentType = (string)dt.Rows[0]["BatchPaymentType"];

                   EventLog.logEvent("UpdateFactBatchData: Original values  " +
                           " Source Processing Date Key = " + sourceProcessingDateKey.ToString() +
                           " Batch Number = " + batchNumber.ToString() +
                           " Batch Payment Type = " + batchPaymentType
                                  , this.GetType().Name
                                  , MessageType.Information
                                  , MessageImportance.Debug);
                   
                   htDataValues = new Hashtable();
                   foreach (DataRow dr in dt.Rows)
                   {
                       if (!htDataValues.ContainsKey(dr["Keyword"].ToString().Trim().ToLower()))
                       {
                           htDataValues.Add(dr["Keyword"].ToString().Trim().ToLower(), dr["DataValue"].ToString());
                       }
                   }            

                   foreach (WFS.RecHub.Common.cBatchData batchdata in listBatchData)
                   {

                       if (BatchDataSetupFieldsTable.ContainsKey(batchdata.Keyword))
                       {
                           //check for the special keywords
                           if (batchdata.Keyword.Trim().ToLower() == "batch number")
                           {
                               Int32.TryParse(batchdata.DataValue, out batchNumber);
                           }
                           if (batchdata.Keyword.Trim().ToLower() == "process date")
                           {
                               DateTime tempDate;
                               DateTime.TryParse(batchdata.DataValue, out tempDate);
                               int.TryParse(tempDate.ToString("yyyyMMdd"), out sourceProcessingDateKey);                              
                           }

                           if (htDataValues.ContainsKey(batchdata.Keyword.Trim().ToLower()))
                           {
                               if (UpdateTypeValue == UpdateType.Update)
                               {
                                   EventLog.logEvent("UpdateFactBatchData: Update keyword = '" + batchdata.Keyword + "'"
                                      , this.GetType().Name
                                      , MessageType.Information
                                      , MessageImportance.Debug);
                                   if (!ICONDAL.UpdateFactBatchData(BatchIDFields.BankID,
                                                                        BatchIDFields.ClientAccountID,
                                                                        BatchIDFields.ProcessingDateKey,
                                                                        BatchIDFields.BatchID,
                                                                        batchdata))
                                   {
                                       bolRetVal = false;
                                   }
                               }
                               else
                               {
                                   EventLog.logEvent("UpdateFactBatchData: Append keyword = '" + batchdata.Keyword + "'"
                                      , this.GetType().Name
                                      , MessageType.Information
                                      , MessageImportance.Debug);
                                   if (!ICONDAL.AppendFactBatchData(BatchIDFields.BankID,
                                                                        BatchIDFields.ClientAccountID,
                                                                        BatchIDFields.ProcessingDateKey,
                                                                        BatchIDFields.BatchID,
                                                                        batchdata))
                                   {
                                       bolRetVal = false;
                                   }
                               }
                           }
                           else
                           {
                               EventLog.logEvent("UpdateFactBatchData: Insert keyword = '" + batchdata.Keyword + "'"
                                  , this.GetType().Name
                                  , MessageType.Information
                                  , MessageImportance.Debug);
                               if (!InsertFactBatchData(BatchIDFields, batchdata))
                               {
                                   bolRetVal = false;
                               }
                           }
                       }
                       else
                       {
                           EventLog.logEvent("UpdateFactBatchData: keyword = '" + batchdata.Keyword + "' Not found in master list"
                                  , this.GetType().Name
                                  , MessageType.Information
                                  , MessageImportance.Debug);
                           // Nothing to update
                       }
                   }

                   //Now call update of special fields
                   if (ICONDAL.UpdateBatchLevel(BatchIDFields.BatchID, BatchIDFields.DepositDateKey,
                       sourceProcessingDateKey, batchNumber, batchPaymentType))
                   {
                       EventLog.logEvent("UpdateFactBatchData: perform batch Level update  " +
                           " Source Processing Date Key = " + sourceProcessingDateKey.ToString() +
                           " Batch Number = " + batchNumber.ToString() +
                           " Batch Payment Type = " + batchPaymentType
                                  , this.GetType().Name
                                  , MessageType.Information
                                  , MessageImportance.Debug);
                   }
                   else
                   {
                       EventLog.logEvent("UpdateFactBatchData: Batch Level update Failed " +
                           " Source Processing Date Key = " + sourceProcessingDateKey.ToString() +
                           " Batch Number = " + batchNumber.ToString() +
                           " Batch Payment Type = " + batchPaymentType
                                  , this.GetType().Name
                                  , MessageType.Error
                                  , MessageImportance.Verbose);
                   }
               }
               else
               {
                   // Exception
               }

               return (bolRetVal);
           }

           private bool InsertFactItemData(IICONItemIDFields ItemIDFields,
                                           WFS.RecHub.Common.cItemData ItemData,
                                           bool IsCheck)
           {
               bool bolRetVal;

               if (IsCheck)
               {
                   bolRetVal = InsertFactItemDataCheck(ItemIDFields, ItemData);
               }
               else
               {
                   bolRetVal = InsertFactItemDataOther(ItemIDFields, ItemData);
               }

               return (bolRetVal);
           }


           private bool InsertFactItemDataCheck(IICONItemIDFields ItemIDFields,
                                           WFS.RecHub.Common.cItemData ItemData)
           {
               EventLog.logEvent("InsertFactItemDataCheck: "
                                 , this.GetType().Name
                                 , MessageType.Information
                                 , MessageImportance.Debug);

               DataTable dt;
               List<WFS.RecHub.Common.cItemData> arItemData;
               bool bolRetVal = false;

               if (ICONDAL.GetCheck2(ItemIDFields.BankID,
                                               ItemIDFields.ClientAccountID,
                                               ItemIDFields.ProcessingDateKey,
                                               ItemIDFields.BatchID,
                                               ItemIDFields.BatchSequence,
                                               out dt))
               {                  
                   if (dt.Rows.Count > 0)
                   {

                       arItemData = new List<WFS.RecHub.Common.cItemData>();
                       arItemData.Add(ItemData);

                       bolRetVal = ICONDAL.InsertFactItemData((int)dt.Rows[0]["BankKey"],
                                                                   (int)dt.Rows[0]["OrganizationKey"],
                                                                   (int)dt.Rows[0]["ClientAccountKey"],
                                                                   (int)dt.Rows[0]["DepositDateKey"],
                                                                   (int)dt.Rows[0]["ImmutableDateKey"],
                                                                   (int)dt.Rows[0]["SourceProcessingDateKey"],
                                                                   (int)dt.Rows[0]["BatchNumber"],
                                                                   (short)dt.Rows[0]["BatchSourceKey"],
                                                                   (long)dt.Rows[0]["BatchID"],
                                                                   (long)dt.Rows[0]["SourceBatchID"],
                                                                   (int)dt.Rows[0]["TransactionID"],
                                                                   (int)dt.Rows[0]["BatchSequence"],
                                                                   arItemData);
                   }
                   else
                   {
                       EventLog.logEvent("InsertFactItemDataCheck GetCheck2:  Returned no records"
                              , this.GetType().Name
                              , MessageType.Information
                              , MessageImportance.Debug);

                       // Stub not found.
                       bolRetVal = false;
                   }
               }
               else
               {
                   // db error occurred.
                   bolRetVal = false;
               }
               
               return bolRetVal;
           }

           private bool InsertFactItemDataOther(IICONItemIDFields ItemIDFields,
                                              WFS.RecHub.Common.cItemData ItemData)
           {
               EventLog.logEvent("InsertFactItemDataOther: "
                                 , this.GetType().Name
                                 , MessageType.Information
                                 , MessageImportance.Debug);

               DataTable dt;
               List<WFS.RecHub.Common.cItemData> arItemData;
               bool bolRetVal;
               if (ICONDAL.GetStub(ItemIDFields.BankID,
                                            ItemIDFields.ClientAccountID,
                                            ItemIDFields.ProcessingDateKey,
                                            ItemIDFields.BatchID,
                                            ItemIDFields.BatchSequence,
                                            out dt))
               {
                   if (dt.Rows.Count > 0)
                   {

                       arItemData = new List<WFS.RecHub.Common.cItemData>();
                       arItemData.Add(ItemData);

                       bolRetVal = ICONDAL.InsertFactItemData((int)dt.Rows[0]["BankKey"],
                                                                   (int)dt.Rows[0]["OrganizationKey"],
                                                                   (int)dt.Rows[0]["ClientAccountKey"],
                                                                   (int)dt.Rows[0]["DepositDateKey"],
                                                                   (int)dt.Rows[0]["ImmutableDateKey"],
                                                                   (int)dt.Rows[0]["SourceProcessingDateKey"],
                                                                   (int)dt.Rows[0]["BatchNumber"],
                                                                   (short)dt.Rows[0]["BatchSourceKey"],
                                                                   (long)dt.Rows[0]["BatchID"],
                                                                   (long)dt.Rows[0]["SourceBatchID"],
                                                                   (int)dt.Rows[0]["TransactionID"],
                                                                   (int)dt.Rows[0]["BatchSequence"],
                                                                   arItemData);
                   }
                   else
                   {
                       EventLog.logEvent("InsertFactItemDataOther GetStub:  Returned no records"
                              , this.GetType().Name
                              , MessageType.Information
                              , MessageImportance.Debug);
                       // Stub not found.
                       bolRetVal = false;
                   }
               }
               else
               {
                   // db error occurred.
                   bolRetVal = false;
               }
               return bolRetVal;
           }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BatchIDFields"></param>
        /// <param name="BatchData"></param>
        /// <returns></returns>   
        private bool InsertFactBatchData(IICONBatchIDFields BatchIDFields,
                                            WFS.RecHub.Common.cBatchData BatchData)
           {

               EventLog.logEvent("InsertFactBatchData "
                              , this.GetType().Name
                              , MessageType.Information
                              , MessageImportance.Debug);

               DataTable dt;
               List<WFS.RecHub.Common.cBatchData> arBatchData;
               bool bolRetVal;

               if (ICONDAL.GetBatch(BatchIDFields.BankID,
                                       BatchIDFields.ClientAccountID,
                                       BatchIDFields.ProcessingDateKey,
                                       BatchIDFields.BatchID,
                                       out dt))
               {
                   if (dt.Rows.Count > 0)
                   {
                       arBatchData = new List<WFS.RecHub.Common.cBatchData>();
                       arBatchData.Add(BatchData);

                       bolRetVal = ICONDAL.InsertFactBatchData((int)dt.Rows[0]["BankKey"],
                                                                    (int)dt.Rows[0]["OrganizationKey"],
                                                                    (int)dt.Rows[0]["ClientAccountKey"],
                                                                    (int)dt.Rows[0]["ImmutableDateKey"],
                                                                    (int)dt.Rows[0]["DepositDateKey"],
                                                                    (int)dt.Rows[0]["SourceProcessingDateKey"],
                                                                    (int)dt.Rows[0]["BatchNumber"],
                                                                    (short)dt.Rows[0]["BatchSourceKey"],
                                                                    (long)dt.Rows[0]["BatchID"],
                                                                    (long)dt.Rows[0]["SourceBatchID"],
                                                                    arBatchData);
                   }
                   else
                   {
                       EventLog.logEvent("InsertFactBatchData GetBatch:  Returned no records"
                              , this.GetType().Name
                              , MessageType.Information
                              , MessageImportance.Debug);
                       // Stub not found.
                       bolRetVal = false;
                   }
               }
               else
               {
                   EventLog.logEvent("InsertFactBatchData GetBatch:  Call Failed"
                              , this.GetType().Name
                              , MessageType.Information
                              , MessageImportance.Debug);
                   // db error occurred.
                   bolRetVal = false;
               }
               return (bolRetVal);
           }       

           /// <summary>
           /// This will get batch data setup fields information
           /// </summary>
           private void GetBatchDataSetupFields()
           {
               DataTable dtBatchDataSetupTable = null;
               try
               {
                   dtBatchDataSetupTable = new DataTable();
                   EventLog.logEvent("Calling GetBatchDataSetupField()"
                           , this.GetType().Name
                           , MessageType.Information
                           , MessageImportance.Debug);



                   ICONDAL.GetBatchDataSetupField("ImageRPS", null, out dtBatchDataSetupTable);
                   if (dtBatchDataSetupTable != null)
                   {
                       //load this in hashtable and reuse it
                       for (int rowCount = 0; rowCount < dtBatchDataSetupTable.Rows.Count; rowCount++)
                       {
                           string keyWord = dtBatchDataSetupTable.Rows[rowCount]["Keyword"].ToString();
                           int batchDataSetupFieldKey = 0;
                           int dataType = 0;
                           bool bSuccess = true;
                           if (!Int32.TryParse(dtBatchDataSetupTable.Rows[rowCount]["BatchDataSetupFieldKey"].ToString(), out batchDataSetupFieldKey))
                           {
                               EventLog.logEvent("Unable to parse BatchDataSetupFieldKey in dimBatchDataSetupFields"
                                   , this.GetType().Name
                                   , MessageType.Error
                                   , MessageImportance.Essential);
                               bSuccess = false;
                           }
                           if (!Int32.TryParse(dtBatchDataSetupTable.Rows[rowCount]["DataType"].ToString(), out dataType))
                           {
                               EventLog.logEvent("Unable to parse dataType in dimBatchDataSetupFields"
                                    , this.GetType().Name
                                    , MessageType.Error
                                    , MessageImportance.Essential);
                               bSuccess = false;
                           }

                           if (bSuccess)
                           {
                               BatchDataSetupFieldsTable.Add(keyWord, new cDataSetupFieldInfo(batchDataSetupFieldKey, dataType));
                           }
                       }
                   }
                   else
                   {
                       EventLog.logEvent("Unable to load dimBatchDataSetupFields"
                            , this.GetType().Name
                            , MessageType.Error
                            , MessageImportance.Essential);
                   }
               }
               finally
               {
                   if (dtBatchDataSetupTable != null)
                   {
                       dtBatchDataSetupTable.Dispose();
                   }
               }
           }

           public bool GetItemData(IMSKeywordPair myPair, out WFS.RecHub.Common.cItemData ItemData)
           {
               cDataSetupFieldInfo dataSetupField = null;
               WFS.RecHub.Common.cItemData objItemData;
               bool bolRetVal = false;

               if ((ItemDataSetupFieldsTable.Contains(myPair.Keyword)))
               {
                   dataSetupField = (cDataSetupFieldInfo)ItemDataSetupFieldsTable[myPair.Keyword];
                   objItemData = new WFS.RecHub.Common.cItemData(dataSetupField.ItemDataSetupFieldKey, myPair.Keyword, dataSetupField.DataType, myPair.Value);
                   bolRetVal = true;
              
               } else {
                   objItemData = null;
                   bolRetVal = false;
               }

               ItemData = objItemData;
               return(bolRetVal);
           }


           public bool GetBatchData(IMSKeywordPair myPair, out WFS.RecHub.Common.cBatchData BatchData)
           {
               WFS.RecHub.Common.cBatchData objBatchData;
               bool bolRetVal = false;
               cDataSetupFieldInfo dataSetupField = null;

               if (BatchDataSetupFieldsTable.Contains(myPair.Keyword))
               {
                   dataSetupField = (cDataSetupFieldInfo)BatchDataSetupFieldsTable[myPair.Keyword];
                   objBatchData = new WFS.RecHub.Common.cBatchData(dataSetupField.ItemDataSetupFieldKey, myPair.Keyword, dataSetupField.DataType, myPair.Value);
                   bolRetVal = true;
                   EventLog.logEvent("GetBatchData: keyword = '" + myPair.Keyword + "' was found"
                           , this.GetType().Name
                           , MessageType.Information
                           , MessageImportance.Debug);
               }
               else
               {
                   EventLog.logEvent("GetBatchData: keyword = '" + myPair.Keyword + "' was NOT found"
                           , this.GetType().Name
                           , MessageType.Information
                           , MessageImportance.Debug);
                   objBatchData = null;
                   bolRetVal = false;
               }              

               BatchData = objBatchData;
               return(bolRetVal);
           }
  
           /// <summary>
           /// Get Item Data Setup Fields
           /// </summary>
           private void GetItemDataSetupFields()
           {
               DataTable dtItemDataSetupTable = null;
               bool bSuccess = true;
               string keyWord;
               int itemDataSetupFieldKey = 0;
               int dataType = 0;
                try
                {
                    dtItemDataSetupTable = new DataTable();
                    EventLog.logEvent("Calling GetItemDataSetupFields()"
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Debug);



                    ICONDAL.GetItemDataSetupField("imageRPS", null, out dtItemDataSetupTable);
                    if (dtItemDataSetupTable != null)
                    {
                        for (int rowCount = 0; rowCount < dtItemDataSetupTable.Rows.Count; rowCount++)
                        {
                            keyWord = dtItemDataSetupTable.Rows[rowCount]["Keyword"].ToString();
                            itemDataSetupFieldKey = 0;
                            dataType = 0;
                            if (!Int32.TryParse(dtItemDataSetupTable.Rows[rowCount]["ItemDataSetupFieldKey"].ToString(), out itemDataSetupFieldKey))
                            {
                                EventLog.logEvent("Unable to parse ItemDataSetupFieldKey in dimItemDataSetupFields"
                                  , this.GetType().Name
                                  , MessageType.Error
                                  , MessageImportance.Essential);
                                bSuccess = false;
                            }
                            if (!Int32.TryParse(dtItemDataSetupTable.Rows[rowCount]["DataType"].ToString(), out dataType))
                            {
                                EventLog.logEvent("Unable to parse DataType in dimItemDataSetupFields"
                                  , this.GetType().Name
                                  , MessageType.Error
                                  , MessageImportance.Essential);
                                bSuccess = false;
                            }
                            if (bSuccess)
                            {
                                ItemDataSetupFieldsTable.Add(keyWord, new cDataSetupFieldInfo(itemDataSetupFieldKey, dataType));
                            }
                        }
                    }
                    else
                    {
                        EventLog.logEvent("Unable to load dimItemDataSetupFields"
                             , this.GetType().Name
                             , MessageType.Error
                             , MessageImportance.Essential);
                    }
                }
                finally
                {
                    if (dtItemDataSetupTable != null)
                    {
                        dtItemDataSetupTable.Dispose();
                    }
                }
           }

           public bool DeleteAllFactData(int bankId, int clientAccountId, long batchId, int processingDateKey, int depositDateKey, string batchSource)
           {
               return ICONDAL.DeleteAllFactData(bankId, clientAccountId, batchId, processingDateKey, depositDateKey, batchSource);
           }

           public void GetImageRPSAliasKeywords(int iBankID, int iLockboxID)
           {
               DataTable dtImageRPSAliasDataSetupTable = null;
               string aliasName   = string.Empty;
               string fieldType   = string.Empty;
               string extractType = string.Empty;
               SetupDocType enmDoctype;
               string strAliasHashTableKey;

                try
                {
                    //always wipe out all the data in there first
                   if (_imageRPSAliasKeywordHashTable == null)
                   {
                       _imageRPSAliasKeywordHashTable = new Hashtable();
                   }
                   else
                   {
                       _imageRPSAliasKeywordHashTable.Clear();    
                   }

                   dtImageRPSAliasDataSetupTable = new DataTable();
                   EventLog.logEvent("Calling GetImageRPSAliasKeywords()"
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Debug);


                    
                   ICONDAL.GetImageRPSAliasMappings(iBankID, iLockboxID, null, out dtImageRPSAliasDataSetupTable);
                   if (dtImageRPSAliasDataSetupTable != null)
                   {
                       //load this in hashtable and reuse it
                       for (int rowCount = 0; rowCount < dtImageRPSAliasDataSetupTable.Rows.Count; rowCount++)
                       {
                           aliasName  = dtImageRPSAliasDataSetupTable.Rows[rowCount]["AliasName"].ToString();
                           extractType= dtImageRPSAliasDataSetupTable.Rows[rowCount]["ExtractType"].ToString();
                           fieldType  = dtImageRPSAliasDataSetupTable.Rows[rowCount]["FieldType"].ToString();
                           enmDoctype = AliasRPSKeywordLib.ParseDocType(dtImageRPSAliasDataSetupTable.Rows[rowCount]["DocType"].ToString());

                           strAliasHashTableKey = AliasRPSKeywordLib.BuildKeyword(aliasName, enmDoctype);
                           _imageRPSAliasKeywordHashTable.Add(strAliasHashTableKey, new cAliasRPSKeywordValue(extractType, fieldType));   
                       }
                   }
               }
               finally
               {
                   if (dtImageRPSAliasDataSetupTable != null)
                   {
                       dtImageRPSAliasDataSetupTable.Dispose();
                   }
               }
           }
      
           public bool GetMICRValues(int BankID, 
                                     int LockboxID, 
                                     SetupDocType DocType,
                                     ItemType ItemTypeValue,
                                     List<IMSKeywordPair> Keywords, 
                                     out List<IMSKeywordPair> CleanKeywords, 
                                     out MICRFields MICRFieldValues) {

               List<IMSKeywordPair> objCleanKeywords;
               MICRFields objMICRFields;
               string strAliasHashTableKey;
               SetupExtractType enmExtractType;

               try {

                   GetImageRPSAliasKeywords(BankID, LockboxID);
                   objCleanKeywords = new List<IMSKeywordPair>();
                   objMICRFields = new MICRFields();

                   foreach(IMSKeywordPair KeywordPair in Keywords) {

                       strAliasHashTableKey = AliasRPSKeywordLib.BuildKeyword(KeywordPair.Keyword, DocType);
                       if (_imageRPSAliasKeywordHashTable.ContainsKey(strAliasHashTableKey) && ItemTypeValue != ItemType.Undefined) {
                           enmExtractType = ((cAliasRPSKeywordValue)_imageRPSAliasKeywordHashTable[strAliasHashTableKey]).ExtractType;
                       } else {
                           enmExtractType = SetupExtractType.Unknown;
                       }

                       switch(enmExtractType) {
                           case SetupExtractType.DocTypeIdentifier:
                               break;
                           case SetupExtractType.Unknown:
                               if(ItemTypeValue == ItemType.Undefined) {
                                   if(!AliasRPSKeywordLib.IsMICRHardCodeValues(KeywordPair, (DocType == SetupDocType.Check), objMICRFields)) {
                                       objCleanKeywords.Add(KeywordPair);
                                   }
                               } else {
                                   objCleanKeywords.Add(KeywordPair);
                               }
                               break;
                           default:
                               AliasRPSKeywordLib.AssignMICRInfo(enmExtractType, KeywordPair, ItemTypeValue, objMICRFields);
                               break;
                       }

                   }

                   CleanKeywords = objCleanKeywords;
                   MICRFieldValues = objMICRFields;

                   return(true);

               } catch(Exception ex) {
                   EventLog.logError(ex);
                   CleanKeywords = null;
                   MICRFieldValues = null;
                   return(false);
               }
           }

           public bool GetBatchSequenceNumbersByBatchSequence(DateTime processingDate, int bankID, int customerID, int lockboxID, long batchID, int siteCodeID, int iBatchSequence, string paymentSource, out DataTable dt)
        {
            return (ICONDAL.GetBatchSequenceNumbersByBatchSequence(processingDate, bankID, customerID, lockboxID, batchID, siteCodeID, iBatchSequence, paymentSource, out dt));
        }

        public bool UpdateR360Batch(string sXML)
        {
            EventLog.logEvent("Call XML >>" + sXML + "<<"
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Debug);
            return (ICONDAL.UpdateR360Batch(sXML));
        }

        public bool GetBatchID(int immutableDateKey, int siteBankID, int siteOrganizationID, int siteClientAccountID, 
            int siteCodeID, long sourceBatchID, string batchSource,
            out long batchID, out int depositDateKey, out int batchNumber, out short batchPaymentTypeKey, out int depositStatus)
        {
            return (ICONDAL.GetBatchID(immutableDateKey, siteBankID, siteOrganizationID, siteClientAccountID,
                siteCodeID, sourceBatchID, batchSource, 
                out batchID, out depositDateKey, out batchNumber, out batchPaymentTypeKey, out depositStatus));
        }


    }
}
