using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using WFS.integraPAY.Online.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Mark Horwich
* Date:     09/22/2009
*
* Purpose:  IntegraPAY Online Image Service options class.
*
* Modification History
*   09/22/2009 MEH
*       - Created class
*  03/30/20211 CR 33547 WJS
*      - Remove ClientPollTimer since we do not have a second timer
*  06/14/2011 CR 33841 WJS
 *      - Added error folder path as ini setting.
* 05/09/2012 CR 31789 WJS
 *      - No longer used
******************************************************************************/
