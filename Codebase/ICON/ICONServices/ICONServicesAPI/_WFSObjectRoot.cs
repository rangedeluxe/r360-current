﻿using System;
using System.Collections.Generic;
using WFS.RecHub.DAL;

/******************************************************************************
** Wausau
** Copyright © 2011-2015 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2011-2015.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    3/2/2011
*
* Purpose:  _WFSObjectRoot.cs.
*
* Modification History
* 03/02/2011 CR 31536 WJS
*     - Initial release.
* WI 192003 TWE 03/04/2015 
*     - Modify to remove ItemProcDal from solution
******************************************************************************/

namespace WFS.integraPAY.Online.ICONServicesAPI 
{
    public abstract class _WFSObjectRoot : IDisposable
    {
        protected cICONWebServiceDAL _ICONWebServiceDAL = null;
        
         // Track whether Dispose has been called.
        private bool disposed = false;
        protected string _SiteKey;

        public _WFSObjectRoot()
        {            
        }

        /// <summary>
        /// Determines the section of the local .ini file to be used for local 
        /// options.
        /// </summary>
        public string SiteKey
        {
            set { _SiteKey = value; }
            get { return _SiteKey; }
        }               
        
        
        protected cICONWebServiceDAL ICONDAL
        {
            get
            {
                if (_ICONWebServiceDAL == null)
                {
                    _ICONWebServiceDAL = new cICONWebServiceDAL(this.SiteKey);
                }

                return (_ICONWebServiceDAL);
            }
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    if (_ICONWebServiceDAL != null)
                    {
                        _ICONWebServiceDAL.Dispose();
                        _ICONWebServiceDAL = null;
                    }       
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }

    }
}
