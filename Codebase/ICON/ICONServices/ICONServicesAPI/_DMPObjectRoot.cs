using System;
using System.Collections.Generic;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

/******************************************************************************
** Wausau
** Copyright � 1997-2008 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     01/22/2010
*
* Purpose:  integraPAY Online Services base Object.
*
* Modification History
* CR 28461 MEH 01/22/2010
*     - Initial release.
* CR 33541 JMC 04/05/2011
*    -Removed reference to cICONWebServiceDAL object.
******************************************************************************/
namespace WFS.integraPAY.Online.ICONServicesAPI
{
    /// <summary>
    /// ICON Web Service API Object Root class.
    /// </summary>
	public abstract class _DMPObjectRoot
    {
        private cICONWebServiceOptions _ServerOptions = null;
        private cEventLog _EventLog = null;
        private string _SiteKey = string.Empty;
        private cICONBll _ICONBLL = null;

        private List<cMsg> _Messages = new List<cMsg>();

        protected _DMPObjectRoot(string SiteKeyValue)
        {
            _SiteKey = SiteKeyValue;
        }

        protected string SiteKey
        {
            set { _SiteKey = value; }
            get { return _SiteKey; }
        }

        /// <summary>
        /// Object that contains ICON Web Service options from the local .ini file.
        /// </summary>
        protected cICONWebServiceOptions serverOptions
        {
            get
            {
                if (_ServerOptions == null)
                {
                    _ServerOptions = new cICONWebServiceOptions(_SiteKey); 
                }

                return _ServerOptions;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog eventLog
        {
            get
            {
                if (_EventLog == null)
                { 
                    _EventLog = new cEventLog(serverOptions.logFilePath, 
                                              serverOptions.logFileMaxSize, 
                                              (MessageImportance)serverOptions.loggingDepth); 
                }

                return _EventLog;
            }
        }

        protected cICONBll ICONBLL {
            get {
                if(_ICONBLL == null) {
                    _ICONBLL = new cICONBll(this.SiteKey, this.eventLog);
                }
                return(_ICONBLL);
            }
        }


        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output messages.
        /// </summary>
        /// <param name="message">Test message to be logged.</param>
        /// <param name="src">Source of the event.</param>
        /// <param name="messageType">MessageType(Information, Warning, Error)</param>
        /// <param name="messageImportance">MessageImportance(Essential, Verbose, Debug)
        /// </param>
        protected void object_outputMessage(string message
                                          , string src
                                          , MessageType messageType
                                          , MessageImportance messageImportance) {

            if(messageImportance == MessageImportance.Essential) {
                if(messageType == MessageType.Error) {
                    addMessage(MsgType.Error, MessageCode.UnexpectedError, message);
                } else {
                    addMessage(MsgType.Information, MessageCode.Information, message);
                }
            }

            eventLog.logEvent(message
                            , src
                            , messageType
                            , messageImportance);
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output errors.
        /// </summary>
        /// <param name="e"></param>
        protected void object_outputError(Exception e) {
            addMessage(MsgType.Error, MessageCode.UnexpectedError, e.Message);
            eventLog.logError(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msgType"></param>
        /// <param name="msgCode"></param>
        /// <param name="msgText"></param>
        internal void addMessage(MsgType msgType, MessageCode msgCode, string msgText) {
            _Messages.Add(new cMsg(msgType, ((int)msgCode).ToString(), msgText));
        }

        /// <summary>
        /// 
        /// </summary>
        internal List<cMsg> Errors {
            get {
                List<cMsg> arRetVal = new List<cMsg>();

                foreach(cMsg msg in _Messages) {
                    if(msg.type == MsgType.Error) {
                        arRetVal.Add(msg);
                    }
                }

                return(arRetVal);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        internal List<cMsg> Messages {
            get {
                return(_Messages);
            }
        }
    }
}
