using System;
using System.Collections;
using System.Collections.Specialized;
using WFS.RecHub.Common;
/******************************************************************************
** Wausau
** Copyright � 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     02/09/2010
*
* Purpose:  integraPAY Online Services ICON web service options object.
*
* Modification History
* 02/09/2010 CR 28461 MEH
*     - Initial release.
******************************************************************************/

namespace WFS.integraPAY.Online.ICONServicesAPI {

	/// <summary>
	/// Summary description for ICONWebServiceOptions.
	/// </summary>
    public class cICONWebServiceOptions : cSiteOptions
    {
       
        private int _DefaultBankID = -1;
        private int _DefaultCustomerID = -1;

        /// <summary>
        /// 
        /// </summary>
        ///        
        public cICONWebServiceOptions(string sSiteKey) : base(sSiteKey)
        {
            string strKey = string.Empty;
            string strValue = string.Empty;
                       
            const string INIKEY_ICONDEFAULTBANK = "DefaultBankID";
            const string INIKEY_ICONDEFAULTCUSTOMER = "DefaultCustomerID";
            
            // get the ICONSvc settings for default bankid and default customerid
            StringCollection colICONSvcOptions = ipoINILib.GetINISection("ICONSvc");

            IEnumerator myICONEnumerator = ((IEnumerable)colICONSvcOptions).GetEnumerator();
            while (myICONEnumerator.MoveNext())
            {
                strKey = myICONEnumerator.Current.ToString().Substring(0, myICONEnumerator.Current.ToString().IndexOf('='));
                strValue = myICONEnumerator.Current.ToString().Substring(strKey.Length + 1);

                if (strKey.ToLower() == INIKEY_ICONDEFAULTBANK.ToLower())
                {
                    _DefaultBankID = Convert.ToInt32(strValue);
                }
                else if (strKey.ToLower() == INIKEY_ICONDEFAULTCUSTOMER.ToLower())
                {
                    _DefaultCustomerID = Convert.ToInt32(strValue);
                }          
            }
		}

        public int DefaultBankID
        {
            get
            {
                return _DefaultBankID;
            }
        }

        public int DefaultCustomerID
        {
            get
            {
                return _DefaultCustomerID;
            }
        }
	}
}
