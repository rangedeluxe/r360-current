using System;

/******************************************************************************
** Wausau
** Copyright � 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     02/09/2010
*
* Purpose:  integraPAY Online Services message object.
*
* Modification History
* 02/09/2010 CR 28461 MEH
*     - Initial release.
******************************************************************************/

namespace WFS.integraPAY.Online.ICONServicesAPI
{
    /// <summary>
    /// 
    /// </summary>
    public enum MessageCode {
        /// <summary></summary>
        Information = 15000,
        /// <summary></summary>
        RequiredFieldMissing = 15001,
        /// <summary></summary>
        BatchDoesNotExist = 15101,
        /// <summary></summary>
        IncorrectStatusForOperation = 15102,
        /// <summary></summary>
        InvalidGlobalBatchIDRequested = 15003,
        /// <summary></summary>
        UnexpectedError = 15004,
        /// <summary></summary>
        DatabaseError = 15005,
        /// <summary></summary>
        InvalidRequestParms = 15006,
        /// <summary></summary>
        InvalidUserForUpdate = 15007,
        /// <summary></summary>
        InvalidDataForFieldDataType = 15008,
        /// <summary></summary>
        DecisioningNotAllowedForField = 15009,
        /// <summary></summary>
        CannotLockBatch = 15010
    }

    /// <summary>
    /// 
    /// </summary>
    internal enum MsgType {
        Error,
        Information
    }
        
    /// <summary>
    /// 
    /// </summary>
    internal class cMsg {

        /// <summary></summary>
        public MsgType type;
        /// <summary></summary>
        public string code;
        /// <summary></summary>
        public string text;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="Code"></param>
        /// <param name="Text"></param>
        public cMsg(MsgType Type, string Code, string Text) {
            this.type = Type;
            this.code = Code;
            this.text = Text;
        }
    }
}
