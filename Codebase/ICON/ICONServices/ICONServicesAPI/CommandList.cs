﻿using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     02/09/2010
*
* Purpose:  integraPAY Online Services ImageRPS XML CommandList object.
*
* Modification History
* 02/09/2010 CR 28461 MEH
*     - Initial release.
******************************************************************************/

namespace WFS.integraPAY.Online.ICONServicesAPI
{
    internal class CommandList
    {
        public List<Command> listCommands = null;

        public CommandList()
        {
            listCommands = new List<Command>();
        }

        public void AddCommand(Command cmdCommand)
        {
            listCommands.Add(cmdCommand);
        }
    }
}
