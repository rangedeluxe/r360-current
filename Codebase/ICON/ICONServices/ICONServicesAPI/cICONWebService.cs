using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Globalization;
using WFS.integraPAY.Online.ICONCommon;
using WFS.RecHub.DAL;
using WFS.RecHub.Common;
using WFS.RecHub.ExternalLogonService;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.OLFServices;
using WFS.RecHub.R360Shared;
using WFS.RecHub.Common.Crypto;
using WFS.RecHub.Common.ClientMapper;
using Wfs.Raam.Core;
using Wfs.Raam.Core.Services;

/******************************************************************************
** Wausau
** Copyright � 1997-2015 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 1997-2015.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     02/09/2010
*
* Purpose:  integraPAY Online Services base Object.
*
* Modification History
* CR 28461 MEH 02/09/2010
*    -Initial release.
* CR 32168 JNE 12/23/2010
*    -modify single quote (if any exist) to 2 single quotes for stored 
*     procedure call.     
* CR 33541 JMC 04/09/2011
*    -Modified object to use new BLL, and to handle the new factItemData 
*     and factBatchData tables.
* CR 30662 WJS 9/6/2011
*	- Removed thumbs.db to prevent directories being removed
* WI 145906 DJW 06/05/2014
*    - Modified to handle the int BatchID to Int64 BatchID 
* WI 187507 TWE 02/03/2015
*    Add call to External logon service
* WI 192197 TWE 02/25/2015
*    Pass the extra parms needed for the stored procedure delete
* WI 193808 TWE 03/11/2015
*     Handle the Item updates
* WI 196657 TWE 03/20/2015
*     Require logon, password, and payment source from imageRPS
* WI 196683 TWE 03/20/2015
*     implement config file setting to allow legacy imageRPS to be used
* WI 198682 TWE 03/31/2015
*     Correct logic to locate Batchid
* WI 199420 TWE 04/03/2015
*     pass in default Bank ID and deposit date correctly
******************************************************************************/

namespace WFS.integraPAY.Online.ICONServicesAPI {

    public class ICONWebService : _DMPObjectRoot  {

        private IExternalLogonService logonService;
        private ClientMapper _clientMapper;
        protected IExternalLogonService LogonService
        {
            get
            {
                if (logonService == null)
                    return new ExternalLogonClient();

                return logonService;
            }
            set
            {
                logonService = value;
            }
        }

        private OLFServiceClient olfService = null;
        protected OLFServiceClient OLFService
        {
            get
            {
                if (olfService == null)
                {
                    olfService = new OLFServiceClient(this.SiteKey);
                }
                return olfService;
            }
            set
            {
                olfService = value;
            }
        }

        private void LoadClientMapper()
        {
            string clientMappingFile = ConfigurationManager.AppSettings["ClientMappingFile"];

            if (!string.IsNullOrEmpty(clientMappingFile))
            {
                //load the client mapping file
                var clientMapperLoader = new ClientMapperLoader();
                var clientMappings = clientMapperLoader.LoadFromFile(clientMappingFile);
                //process any logging messages that exist
                if (clientMapperLoader.LogEntries.Any())
                {
                    foreach (var logEntry in clientMapperLoader.LogEntries)
                    {
                        switch (logEntry.Type)
                        {
                            case LogType.Error:
                                eventLog.logEvent(
                                    logEntry.Message,
                                    logEntry.Source,
                                    MessageType.Error,
                                    MessageImportance.Essential);
                                break;
                            case LogType.Warning:
                                eventLog.logEvent(
                                    logEntry.Message,
                                    logEntry.Source,
                                    MessageType.Warning,
                                    MessageImportance.Essential);
                                break;
                            case LogType.Information:
                                eventLog.logEvent(
                                    logEntry.Message,
                                    logEntry.Source,
                                    MessageType.Information,
                                    MessageImportance.Verbose);
                                break;
                        }
                    }
                }
                //load the client mapper returned from the mapper loader
                _clientMapper = new ClientMapper(serverOptions.DefaultBankID, clientMappings);
            }
            else
            {
                //file is defined, use the default bankId for all clients
                eventLog.logEvent(
                    "Client Mapping file not defined, using default bankID.",
                    this.GetType().Name,
                    MessageType.Information,
                    MessageImportance.Verbose);

                _clientMapper = new ClientMapper(serverOptions.DefaultBankID);
            }

            eventLog.logEvent("*****************************************************************************"
                , this.GetType().Name
                , MessageType.Information
                , MessageImportance.Verbose);
        }

        public ICONWebService(string sSiteKey) : base(sSiteKey) {

            
            eventLog.logEvent("ICON Web Service initializing..."
                            , this.GetType().Name
                           , MessageImportance.Essential);

            // log all the settings
            eventLog.logEvent("*****************************************************************************"
                        , this.GetType().Name
                        , MessageType.Information
                        , MessageImportance.Verbose);
            
            eventLog.logEvent("Online Settings"
                        , this.GetType().Name
                        , MessageType.Information
                        , MessageImportance.Verbose);

            eventLog.logEvent("ImageStorageType=" + serverOptions.imageStorageMode.ToString()
                        , this.GetType().Name
                        , MessageType.Information
                        , MessageImportance.Verbose);

            eventLog.logEvent("DBConnection2=" + serverOptions.connectionString
                        , this.GetType().Name
                        , MessageType.Information
                        , MessageImportance.Verbose);

            eventLog.logEvent("ImagePath=" + serverOptions.imagePath
                        , this.GetType().Name
                        , MessageType.Information
                        , MessageImportance.Verbose);

            eventLog.logEvent("ICON Settings"
                        , this.GetType().Name
                        , MessageType.Information
                        , MessageImportance.Verbose);

            eventLog.logEvent("DefaultBankID=" + serverOptions.DefaultBankID
                        , this.GetType().Name
                        , MessageType.Information
                        , MessageImportance.Verbose);

            eventLog.logEvent("DefaultCustomerID=" + serverOptions.DefaultCustomerID
                        , this.GetType().Name
                        , MessageType.Information
                        , MessageImportance.Verbose);

            eventLog.logEvent("*****************************************************************************"
                , this.GetType().Name
                , MessageType.Information
                , MessageImportance.Verbose);

            LoadClientMapper();
        }

        public string ProcessXMLCommand(string strXML) {

            string sRet = string.Empty;
            bool rtrnFlag = false;

            try
            {

                if (strXML.Length > 0)
                {                    
                    eventLog.logEvent("Processing XML: " + strXML
                                                    , this.GetType().Name
                                                    , MessageType.Information
                                                    , MessageImportance.Debug);

                    // Process incoming Xml
                    rtrnFlag = ProcessXML(strXML, out sRet);
                }
                else
                {

                    eventLog.logEvent("XML was empty, skipping web service request"
                                                    , this.GetType().Name
                                                    , MessageType.Information
                                                    , MessageImportance.Essential);
                    sRet = "No XML found to process";
                }

            }
            catch (Exception ex)
            {

                eventLog.logEvent("Exception in ProcessXMLCommand(): " + ex.Message
                                                , this.GetType().Name
                                                , MessageType.Error
                                                , MessageImportance.Essential);
               
                sRet = "Error during Processing";
            }

            eventLog.logEvent("ProcessXMLCommand() complete!  Return string: " + sRet
                                                , this.GetType().Name
                                                , MessageType.Information
                                                , MessageImportance.Debug);

            return ((rtrnFlag ? "True - " : "False - ") + sRet);
        }

        private bool ProcessXML(string sXML, out string sRetVal)
        {
            bool rtrnFlag = false;
            sRetVal = string.Empty;

            string logonName = string.Empty;
            string password = string.Empty;
            string paymentSource = string.Empty;
            string versionCheck = ConfigurationManager.AppSettings["PreviousVersionAllowed"];
            
            // parse the XML into objects
            RemitXMLCommand myRemitXMLCommand = new RemitXMLCommand(sXML);

            if (versionCheck != null && versionCheck.ToUpper() == "TRUE")
            {
                var _crypto = new cCrypto3DES();
                _crypto.Decrypt(ConfigurationManager.AppSettings["Password"], out password);
                _crypto.Decrypt(ConfigurationManager.AppSettings["LogonName"], out logonName);
                rtrnFlag = true;
                paymentSource = ConfigurationManager.AppSettings["DefaultPaymentSource"];
                eventLog.logEvent("Use Credentials from web.config Payment Source = >>" + paymentSource + "<<", this.GetType().Name, MessageType.Information, MessageImportance.Essential);
            }
            else
            {
                rtrnFlag = LocateCredentials(sXML, out logonName, out password, out paymentSource, out sRetVal);
            }

            if (rtrnFlag && 
                ProcessLogon(logonName, password, out sRetVal ))
            {                
                eventLog.logEvent("Current User Claims: \r\n" + WSFederatedAuthentication.ClaimsAccess.GetClaims().Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
                //Now go actually process the xml sent in
                rtrnFlag = ProcessXML2(myRemitXMLCommand, paymentSource, out sRetVal);
            }

            return rtrnFlag;
        }

        private bool ProcessLogon(string logonName,string password, out string sRetVal)
        {
            bool rtrnFlag = false;
            sRetVal = string.Empty;
            string applicatoinURI = ConfigurationManager.AppSettings["ApplicationURI"];
            string wcfConfigLocation = ConfigurationManager.AppSettings["WCFConfigLocation"];

            string entity = string.Empty;
            int userID = 0;
            string message = string.Empty;
            Guid sessionID;

           
            //go authenticate the logon

            try
            {
                rtrnFlag = Logon(logonName, password, out userID, out sessionID, out message);
                if (rtrnFlag == false)
                {
                    sRetVal = "RAAM Authentication Failed";
                    eventLog.logEvent(sRetVal
                                , this.GetType().Name
                                , MessageType.Warning
                                , MessageImportance.Essential);
                }
            }
            catch (Exception ex)
            {
                eventLog.logEvent("Exception in Logon: " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                sRetVal = "Unable to Log into RAAM";
            }

            if (rtrnFlag)
            {
                // Check the R360Claims, if we don't have any, we'll need to attach them.
                if (!WSFederatedAuthentication.IsAuthenticated || WSFederatedAuthentication.ClaimsAccess.GetClaim(WfsClaimTypes.SessionId) == null)
                {
                    rtrnFlag = WSFederatedAuthentication.AuthenticateUsingRichClient(applicatoinURI, entity, logonName, password, wcfConfigLocation);
                    if (!rtrnFlag)
                    {
                        sRetVal = "Unable to Attach RAAM Claims";
                        eventLog.logEvent(sRetVal
                                    , this.GetType().Name
                                    , MessageType.Information
                                    , MessageImportance.Essential);
                    }
                }
            }

            return rtrnFlag;
        }

        private bool LocateCredentials(string sXML, out string logon, out string password, out string paymentSource, out string sRetVal)
        { 
            bool bolRetVal = false;
            logon = string.Empty;
            password = string.Empty;
            paymentSource = string.Empty;
            sRetVal = string.Empty;
            //<ICONInitialize Logon="90e/rWooYpA3UxvqRzwcrg=="
            //                Password="T9AyWrk1+zU4gktpQh6viw=="
            //	              PaymentSource="ImageRPS-Michael" />

            try
            {
                XmlDocument xInput = new XmlDocument();
                xInput.InnerXml = sXML;

                if (xInput.SelectSingleNode("/RemitXMLCommand/ICONInitialize") != null)
                {
                    XmlNode iconParms = xInput.SelectSingleNode("/RemitXMLCommand/ICONInitialize");

                    if (LocateAttribute(iconParms, "Logon", out logon) &&
                        LocateAttribute(iconParms, "Password", out password) &&
                        LocateAttribute(iconParms, "PaymentSource", out paymentSource))
                    {
                        bolRetVal = true;
                    }
                    else
                    {
                        sRetVal = "Failed to locate ICONInititalize attribute: see log for more details";
                    }
                }
                else
                {
                    eventLog.logEvent("The required XML node 'ICONInitialize' was not found " + sXML
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
                    sRetVal = "Failed to locate ICONInititalize: see log for more details";
                }
            }
            catch (Exception ex)
            {
                eventLog.logEvent("Exception in ICONInitialize: retrieve ICON parameters " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
                bolRetVal = false;
                logon = string.Empty;
                password = string.Empty;
                paymentSource = string.Empty;
                sRetVal = "Failed locating ICONInititalize: see log for more details";
            }

            return bolRetVal;
        }

        private bool LocateAttribute(XmlNode iconParms, string locateAttribute, out string attributeValue)
        {
            bool bolRetVal = false;
            attributeValue = string.Empty;

            if (iconParms.Attributes[locateAttribute] != null &&
                iconParms.Attributes[locateAttribute].InnerText != null)
            {
                attributeValue = iconParms.Attributes[locateAttribute].InnerText;
                bolRetVal = true;
            }
            else
            {
                eventLog.logEvent("The required attribute '" + locateAttribute + "' for node 'ICONInitialize' was not found " + iconParms.OuterXml.ToString()
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }

            return bolRetVal;
        }

        private bool ProcessXML2(RemitXMLCommand myRemitXMLCommand, string paymentSource, out string sRetVal)
        {

            sRetVal = string.Empty;
            bool rtrnFlag = false;
            
            try
            {
                // now that we have our command list, loop through them
                foreach (CommandList cmdList in myRemitXMLCommand.listCommandList)
                {

                    foreach (Command myCommand in cmdList.listCommands)
                    {

                        switch (myCommand.CommandType.ToUpper())
                        {
                            case "UPDATEKEYWORDS":
                                rtrnFlag = UpdateKeywords(myCommand, paymentSource, out sRetVal);
                                break;
                            case "APPENDKEYWORDS":
                                rtrnFlag = AppendKeywords(myCommand, paymentSource, out sRetVal);
                                break;
                            case "DELETEBATCH":
                                rtrnFlag = DeleteBatch(myCommand, paymentSource, out sRetVal);
                                break;
                            default:
                                throw (new Exception("Invalid Command Type defined."));
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                eventLog.logEvent("Exception in ProcessXML2: " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                sRetVal = "Processing Exception: see Log files for more detail ";
            }

            return rtrnFlag;
        }



        private bool UpdateKeywords(Command myCommand,string paymentSource, out string sRetVal)
        {

            sRetVal = string.Empty;
            bool rtrnFlag;

            cICONBatchIDFields objOLTAKeys;
            DataTable dtmyTable;
            List<WFS.RecHub.Common.cBatchData> arBatchData;
            string strRemitterName = string.Empty;
            string strMsg;

            if (GetOLTAKeysFromKeywords(myCommand.listQueryKeywords, paymentSource, out objOLTAKeys))
            {

                if (objOLTAKeys.BatchSequence > -1)
                {

                    // - figure out of it is a batch update or just a keyword update for a document
                    //   based on sP1.  If it is empty, it is a batch update
                    // - get all the BatchSequence numbers from the factChecks/factDocuments for the batch
                    // - call a stored proc to get all the BatchSequence numbers from the factCheck/factDocument table
                    if (ICONBLL.GetBatchSequenceNumbersByBatchSequence(objOLTAKeys.ProcessingDate,
                                                                       _clientMapper.GetBankIdFromClientId(objOLTAKeys.ClientAccountID), 
                                                                       serverOptions.DefaultCustomerID, 
                                                                       objOLTAKeys.ClientAccountID, 
                                                                       objOLTAKeys.SourceBatchID, 
                                                                       objOLTAKeys.SiteCodeID, 
                                                                       objOLTAKeys.BatchSequence, 
                                                                       paymentSource,
                                                                       out dtmyTable))
                    {

                        // add the batch sequence numbers to the list
                        ////myChecksDocuments = new List<factCheckDocument>();
                        if (dtmyTable.Rows.Count > 0)
                        {
                            string sMesg;
                            rtrnFlag = AddBatchSequenceToList(myCommand, dtmyTable, objOLTAKeys, paymentSource, out sMesg);
                            sRetVal = AddToReturnMsg(sRetVal, sMesg);
                        }
                        else
                        {
                            strMsg = "Requested item not found: " + objOLTAKeys.ProcessingDateAsString + ", SiteID: " + objOLTAKeys.SiteCodeID.ToString() + ", Lockbox: " + objOLTAKeys.ClientAccountID.ToString() + ", BatchID: " + objOLTAKeys.SourceBatchID.ToString() + ", Sequence: " + objOLTAKeys.BatchSequence.ToString();
                            eventLog.logEvent(strMsg,
                                      this.GetType().Name,
                                      MessageType.Warning,
                                      MessageImportance.Essential);
                            sRetVal = AddToReturnMsg(sRetVal, strMsg);

                            // Item not found in database.
                            rtrnFlag = false;
                        }
                    }
                    else
                    {
                        strMsg = "System error occurred requesting item: " + objOLTAKeys.ProcessingDateAsString + ", SiteID: " + objOLTAKeys.SiteCodeID.ToString() + ", Lockbox: " + objOLTAKeys.ClientAccountID.ToString() + ", BatchID: " + objOLTAKeys.SourceBatchID.ToString() + ", Sequence: " + objOLTAKeys.BatchSequence.ToString();
                        eventLog.logEvent(strMsg,
                                  this.GetType().Name,
                                  MessageType.Error,
                                  MessageImportance.Verbose);
                        sRetVal = AddToReturnMsg(sRetVal, strMsg);

                        // Error on db call.
                        rtrnFlag = false;
                    }
                }
                else
                {
                    if (GetBatchDataList(myCommand, out arBatchData))
                    {
                        if (arBatchData.Count > 0)
                        {
                            if (ICONBLL.UpdateFactBatchData(objOLTAKeys, arBatchData, UpdateType.Update))
                            {
                                sRetVal = AddToReturnMsg(sRetVal, "Batch Data update completed successfully.");
                                rtrnFlag = true;
                            }
                            else
                            {
                                sRetVal = AddToReturnMsg(sRetVal, "Batch Data update failed.");
                                rtrnFlag = false;
                            }
                        }
                        else
                        {
                            strMsg = "Update request did not include any valid BatchData Keywords: " + objOLTAKeys.ProcessingDateAsString + ", SiteID: " + objOLTAKeys.SiteCodeID.ToString() + ", Lockbox: " + objOLTAKeys.ClientAccountID.ToString() + ", BatchID: " + objOLTAKeys.SourceBatchID.ToString();
                            eventLog.logEvent(strMsg,
                                      this.GetType().Name,
                                      MessageType.Warning,
                                      MessageImportance.Verbose);
                            sRetVal = AddToReturnMsg(sRetVal, strMsg);
                            rtrnFlag = true;
                        }
                    }
                    else
                    {
                        rtrnFlag = false;
                    }
                }
            }
            else
            {
                // Unable to get keys
                strMsg = "Invalid Key values specified.";
                eventLog.logEvent(strMsg,
                          this.GetType().Name,
                          MessageType.Warning,
                          MessageImportance.Verbose);
                sRetVal = AddToReturnMsg(sRetVal, strMsg);
                rtrnFlag = false;
            }

            return rtrnFlag;
        }

        private bool AddBatchSequenceToList(Command myCommand, 
                                            DataTable dtmyTable,
                                            cICONBatchIDFields objOLTAKeys,
                                            string paymentSource, 
                                            out string sRtrnMesg)
        {
            bool rtrnFlag = false;
            sRtrnMesg = string.Empty;
            List<IMSKeywordPair> myCleanIMSKeywordList;
            List<IMSKeywordPair> myIMSKeywordList;
            MICRFields objMICRFieldValues;
            IMSKeywordPair keyword;
            factCheckDocument myChecksDocument;           
            XmlDocument xOutput;
            List<WFS.RecHub.Common.cItemData> arItemData;
            ItemType enmItemType;
            int intTemp;
            bool bolRemitterIsDirty = false;
            string strRemitterName = string.Empty;

            DataRow dr = dtmyTable.Rows[0];

            myChecksDocument = new factCheckDocument(Convert.ToInt32(dr["TransactionID"]),
                                        Convert.ToInt32(dr["BatchSequence"]),
                                        Convert.ToInt32(dr["TxnSequence"]),
                                        Convert.ToInt32(dr["Sequence"]),
                                        Convert.ToBoolean(dr["IsCheck"]),
                                        Convert.ToBoolean(dr["IsDocument"]),
                                        Convert.ToBoolean(dr["IsStub"]),
                                        Convert.ToInt32(dr["BatchNumber"]),
                                        Convert.ToInt16(dr["BatchPaymentTypeKey"]),
                                        Convert.ToInt32(dr["DepositStatus"]));

            if (myChecksDocument.IsCheck)
            {
                enmItemType = ItemType.Undefined;
                foreach (IMSKeywordPair keypair in myCommand.listQueryKeywords)
                {
                    if (keypair.Keyword.ToLower().Trim() == "item type")
                    {
                        if (int.TryParse(keypair.Value, out intTemp))
                        {
                            switch (intTemp)
                            {
                                case (int)ItemType.BusinessCheck:
                                    enmItemType = ItemType.BusinessCheck;
                                    break;
                                case (int)ItemType.PersonalCheck:
                                    enmItemType = ItemType.PersonalCheck;
                                    break;
                                default:
                                    enmItemType = ItemType.Undefined;
                                    break;
                            }
                        }
                        break;
                    }
                }
            }
            else
            {
                enmItemType = ItemType.NonCheck;
            }

            // Initialize BLL MICR Keyword collection
            ICONBLL.GetImageRPSAliasKeywords(objOLTAKeys.BankID, objOLTAKeys.ClientAccountID);

            // Get MICR data / Parse MICR fields out of the field list.
            ICONBLL.GetMICRValues(objOLTAKeys.BankID,
                                  objOLTAKeys.ClientAccountID,
                                  (dr["IsCheck"].ToString() == "1" ? SetupDocType.Check : SetupDocType.NonCheck),
                                  enmItemType,
                                  myCommand.listActionKeywords,
                                  out myCleanIMSKeywordList,
                                  out objMICRFieldValues);

            // Parse through the MICR-less fields, and parse out the Magic values and the BatchData/ItemData fields.
            myIMSKeywordList = new List<IMSKeywordPair>();
            foreach (IMSKeywordPair myActionKeyword in myCleanIMSKeywordList)
            {

                switch (myActionKeyword.Keyword)
                {
                    case cMagicKeywords.RemitterName:
                    case cMagicKeywords.RemitterSpaceName:
                        strRemitterName = myActionKeyword.Value;
                        bolRemitterIsDirty = true;
                        break;
                    case cMagicKeywords.Amount:
                    case cMagicKeywords.AppliedAmount:
                        keyword = new IMSKeywordPair(cMagicKeywords.Amount, cMagicKeywords.Amount, myActionKeyword.Value);
                        myIMSKeywordList.Add(keyword);
                        break;
                    case cMagicKeywords.Account:
                    case cMagicKeywords.AccountNumber:
                        if (myChecksDocument.IsStub) {
                            keyword = new IMSKeywordPair(
                                cMagicKeywords.AccountNumber.Replace(" ", ""),
                                cMagicKeywords.AccountNumber,
                                myActionKeyword.Value);
                            myIMSKeywordList.Add(keyword);
                        }
                        break;
                    default:
                        if (!ICONBLL.ImageRPSMagicKeywords.IsMagic(myActionKeyword.Keyword) &&
                           !ICONBLL.BatchDataSetupFieldsTable.ContainsKey(myActionKeyword.Keyword) &&
                           !ICONBLL.ItemDataSetupFieldsTable.ContainsKey(myActionKeyword.Keyword))
                        {
                            keyword = new IMSKeywordPair(myActionKeyword.Keyword.Replace(" ", ""), myActionKeyword.Keyword, myActionKeyword.Value);
                            myIMSKeywordList.Add(keyword);
                        }
                        break;
                }
            }

            //----------------------------------------------
            if ((myIMSKeywordList != null && myIMSKeywordList.Count > 0) ||
               objMICRFieldValues.IsDirty ||
               bolRemitterIsDirty)
            {
                xOutput = CreateUpdateXML(objOLTAKeys, 
                                          myChecksDocument, 
                                          myIMSKeywordList, 
                                          objMICRFieldValues, 
                                          bolRemitterIsDirty, 
                                          strRemitterName,
                                          paymentSource);

                // call a stored proc to update the factDataEntryDetails 
                if (ICONBLL.UpdateR360Batch(GetXMLString(xOutput)))
                {
                    sRtrnMesg = AddToReturnMsg(sRtrnMesg, "Item update completed successfully.");
                    rtrnFlag = true;
                }
                else
                {
                    sRtrnMesg = AddToReturnMsg(sRtrnMesg, "Item update failed.");
                    rtrnFlag = false;
                }                
            }
            else
            {
                sRtrnMesg = AddToReturnMsg(sRtrnMesg, "No Data Entry or MICR data found for update.");
                rtrnFlag = true;
            }

            //------------------------------------------
            //  Handle any Item Data found
            if (rtrnFlag)
            {
                if (GetItemDataList(myCommand, out arItemData))
                {
                    if (arItemData.Count > 0)
                    {
                        sRtrnMesg = (string.IsNullOrEmpty(sRtrnMesg) ? sRtrnMesg : sRtrnMesg + "; ");
                        if (ICONBLL.UpdateFactItemData(objOLTAKeys, arItemData, myChecksDocument.IsCheck, UpdateType.Update))
                        {
                            sRtrnMesg = AddToReturnMsg(sRtrnMesg, "Item Data update completed successfully.");
                            rtrnFlag = true;
                        }
                        else
                        {
                            sRtrnMesg = AddToReturnMsg(sRtrnMesg, "Item Data update failed.");
                            rtrnFlag = false;
                        }
                    }
                    else
                    {
                        sRtrnMesg = AddToReturnMsg(sRtrnMesg, "No Item Data fields specified for update.");
                        rtrnFlag = true;
                    }
                }
                else
                {
                    sRtrnMesg = AddToReturnMsg(sRtrnMesg, "Unable to read Item Data Xml.");
                    rtrnFlag = false;
                }
            }
            return rtrnFlag;
        }

        /// <summary>
        /// -Will append to existing data, or insert if the data does not already exist.
        /// -Function not valid for Data Entry or standard factChecks/factStubs/factDocuments values
        /// </summary>
        /// <param name="myCommand"></param>
        /// <param name="strReturnMsg"></param>
        /// <returns></returns>
        private bool AppendKeywords(Command myCommand, string paymentSource, out string sRetVal) {

            cICONBatchIDFields objOLTAKeys;
            sRetVal = string.Empty;
            List<WFS.RecHub.Common.cItemData> arItemData;
            List<WFS.RecHub.Common.cBatchData> arBatchData;
            bool rtrnFlag = true;
            DataTable dt;

            arItemData = new List<WFS.RecHub.Common.cItemData>();
            arBatchData = new List<WFS.RecHub.Common.cBatchData>();

            GetOLTAKeysFromKeywords(myCommand.listQueryKeywords, paymentSource, out objOLTAKeys);

            if (GetBatchDataList(myCommand, out arBatchData))
            {
                if (arBatchData.Count > 0)
                {
                    if (ICONBLL.UpdateFactBatchData(objOLTAKeys, arBatchData, UpdateType.Append))
                    {
                        sRetVal = AddToReturnMsg(sRetVal, "Batch Data update completed successfully");
                        rtrnFlag = true;
                    }
                    else
                    {
                        sRetVal = AddToReturnMsg(sRetVal, "Batch Data update failed.");
                        rtrnFlag = false;
                    }
                }
                else
                {
                    sRetVal = AddToReturnMsg(sRetVal, "No Batch Data fields specified for update.");
                    rtrnFlag = true;
                }
            }
            else
            {
                sRetVal = AddToReturnMsg(sRetVal, "Unable to read Batch Data Xml.");
                rtrnFlag = false;
            }
            
            //-------------------------------------------------------------------------
            if (rtrnFlag)
            {
                if (objOLTAKeys.BatchSequence > -1)
                {
                    if (GetItemDataList(myCommand, out arItemData))
                    {
                        if (arItemData.Count > 0)
                        {

                            if (ICONBLL.GetBatchSequenceNumbersByBatchSequence(objOLTAKeys.ProcessingDate,
                                                                               _clientMapper.GetBankIdFromClientId(objOLTAKeys.ClientAccountID), 
                                                                               serverOptions.DefaultCustomerID, 
                                                                               objOLTAKeys.ClientAccountID, 
                                                                               objOLTAKeys.SourceBatchID, 
                                                                               objOLTAKeys.SiteCodeID, 
                                                                               objOLTAKeys.BatchSequence, 
                                                                               paymentSource,
                                                                               out dt))
                            {

                                // add the batch sequence numbers to the list
                                if (dt.Rows.Count > 0)
                                {
                                    if (ICONBLL.UpdateFactItemData(objOLTAKeys, arItemData, Convert.ToBoolean(dt.Rows[0]["IsCheck"]), UpdateType.Append))
                                    {
                                        sRetVal = AddToReturnMsg(sRetVal, "Item Data update completed successfully.");
                                        rtrnFlag = true;
                                    }
                                    else
                                    {
                                        sRetVal = AddToReturnMsg(sRetVal, "Item Data update failed.");
                                        rtrnFlag = false;
                                    }
                                }
                                else
                                {
                                    sRetVal = AddToReturnMsg(sRetVal, "Specified Item not found.");
                                    rtrnFlag = false;
                                }
                            }
                            else
                            {
                                sRetVal = AddToReturnMsg(sRetVal, "Unable to retrieve item from the database.");
                                rtrnFlag = false;
                            }
                        }
                        else
                        {
                            sRetVal = AddToReturnMsg(sRetVal, "No Item Data fields specified for update.");
                            rtrnFlag = true;
                        }
                    }
                    else
                    {
                        sRetVal = AddToReturnMsg(sRetVal, "Unable to read Batch Data Xml.");
                        rtrnFlag = false;
                    }
                }
            }
            

            return rtrnFlag; 
        }


        private bool DeleteBatch(Command myCommand, string paymentSource, out string sRetVal) 
        {
            cICONBatchIDFields objLockboxDetails;
            sRetVal = string.Empty;
            bool rtrnFlag = false;

            GetOLTAKeysFromKeywords(myCommand.listQueryKeywords, paymentSource, out objLockboxDetails);

            if (objLockboxDetails.SourceBatchID > -1 && objLockboxDetails.ProcessingDate > DateTime.MinValue)
            {
                // call the stored procedure to delete the batch
                if (ICONBLL.DeleteAllFactData(_clientMapper.GetBankIdFromClientId(objLockboxDetails.ClientAccountID), objLockboxDetails.ClientAccountID, objLockboxDetails.SourceBatchID, objLockboxDetails.ProcessingDateKey, objLockboxDetails.DepositDateKey, paymentSource))
                {

                    eventLog.logEvent("Deleted batch, Client: " + objLockboxDetails.ClientAccountID.ToString() + ", SiteID: " + objLockboxDetails.SiteCodeID.ToString() + ", ProcDate: " + objLockboxDetails.ProcessingDateAsString + ", BatchID: " + objLockboxDetails.SourceBatchID.ToString()
                                , this.GetType().Name
                                , MessageType.Information
                                , MessageImportance.Essential);

                    sRetVal = AddToReturnMsg(sRetVal, "Deleted batch, Client: " + objLockboxDetails.ClientAccountID.ToString() + ", SiteID: " + objLockboxDetails.SiteCodeID.ToString() + ", ProcDate: " + objLockboxDetails.ProcessingDateAsString + ", BatchID: " + objLockboxDetails.SourceBatchID.ToString());
                    rtrnFlag = true;

                }
                else
                {

                    eventLog.logEvent("Could not delete batch, Client: " + objLockboxDetails.ClientAccountID + ", SiteID: " + objLockboxDetails.SiteCodeID.ToString() + ", ProcDate: " + objLockboxDetails.ProcessingDateAsString + ", BatchID: " + objLockboxDetails.SourceBatchID.ToString()
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                    sRetVal = AddToReturnMsg(sRetVal, "Could not delete batch, Client: " + objLockboxDetails.ClientAccountID.ToString() + ", SiteID: " + objLockboxDetails.SiteCodeID.ToString() + ", ProcDate: " + objLockboxDetails.ProcessingDateAsString + ", BatchID: " + objLockboxDetails.SourceBatchID.ToString());
                    rtrnFlag = false;
                }

                if (rtrnFlag)
                {

                    // don't delete the documents using the web service since OLTA will take care of
                    // it when the batch is deleted


                    if (DeleteBatchImages(_clientMapper.GetBankIdFromClientId(objLockboxDetails.ClientAccountID),
                                          objLockboxDetails.ClientAccountID,
                                          objLockboxDetails.ProcessingDateKey,
                                          "imageRPS",
                                          paymentSource,
                                          objLockboxDetails.SourceBatchID))
                    {

                        eventLog.logEvent("Deleted batch images for batch " + objLockboxDetails.SourceBatchID.ToString()
                                , this.GetType().Name
                                , MessageType.Information
                                , MessageImportance.Essential);

                        sRetVal = AddToReturnMsg(sRetVal, "Deleted batch images for batch " + objLockboxDetails.SourceBatchID.ToString());

                        rtrnFlag = true;
                    }
                    else
                    {
                        eventLog.logEvent("Could not delete batch images for batch " + objLockboxDetails.SourceBatchID.ToString()
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                        sRetVal = AddToReturnMsg(sRetVal, "Could not delete batch images for batch " + objLockboxDetails.SourceBatchID.ToString());
                        rtrnFlag = false;
                    }
                   
                }
            }
            else
            {
                rtrnFlag = false;
            }

            return rtrnFlag;
        }

        public static bool Logon(
            string uername,
            string password,
            out int userID,
            out Guid sessionID,
            out string userMessage)
        {
            var client = new ExternalLogonClient();
            var request = new ExternalLogonRequest()
            {
                Username = uername,
                Password = password,
                Entity = string.Empty,
                IPAddress = Environment.MachineName
            };

            var response = client.ExternalLogon(request);

            // If we couldn't log in, we'll kick out early.
            if (response == null || response.Status == StatusCode.FAIL || response.Session == null)
            {
                userID = 0;
                sessionID = Guid.Empty;
                userMessage = "Logon Failed";
                return false;
            }

            userID = response.UserId;
            sessionID = response.Session;
            userMessage = "Logon Successful";
            return true;
        }

        private bool GetBatchDataList(Command myCommand, out List<WFS.RecHub.Common.cBatchData> BatchData)
        {

            IMSKeywordPair myPair;
            WFS.RecHub.Common.cBatchData objBatchData;
            List<WFS.RecHub.Common.cBatchData> arBatchData;
            bool bolRetVal;

            try {

                arBatchData = new List<WFS.RecHub.Common.cBatchData>();

                using(cICONBll BLL = new cICONBll(this.SiteKey, this.eventLog)) {

                    foreach (IMSKeywordPair myActionKeyword in myCommand.listActionKeywords) {

                        myPair = new IMSKeywordPair(myActionKeyword.Keyword.Trim(), myActionKeyword.Value.Trim());

                        if (BLL.GetBatchData(myPair, out objBatchData)) {
                            //it is batch data which is valid add it to collection to store
                            arBatchData.Add(objBatchData);
                        }
                    }
                }

                BatchData = arBatchData;
                bolRetVal = true;
            } catch(Exception ex) {
                eventLog.logError(ex);
                BatchData = null;
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        private bool GetItemDataList(Command myCommand, out List<WFS.RecHub.Common.cItemData> ItemData)
        {

            IMSKeywordPair myPair;
            WFS.RecHub.Common.cItemData objItemData;
            List<WFS.RecHub.Common.cItemData> arItemData;
            bool bolRetVal;

            try {

                arItemData = new List<WFS.RecHub.Common.cItemData>();

                using(cICONBll BLL = new cICONBll(this.SiteKey, this.eventLog)) {

                    foreach (IMSKeywordPair myActionKeyword in myCommand.listActionKeywords) {

                        myPair = new IMSKeywordPair(myActionKeyword.Keyword.Trim(), myActionKeyword.Value.Trim());

                        if (BLL.GetItemData(myPair, out objItemData)) {
                            //it is item data which is valid add it to collection to store
                            arItemData.Add(objItemData);
                        }
                    }
                }

                ItemData = arItemData;
                bolRetVal = true;
            } catch(Exception ex) {
                eventLog.logError(ex);
                ItemData = null;
                bolRetVal = false;
            }

            return(bolRetVal);
        }


        private XmlDocument CreateUpdateXML(cICONBatchIDFields LockboxDetails, 
                                            factCheckDocument myfact, 
                                            List<IMSKeywordPair> myIMSKeywordList,
                                            MICRFields MICRFieldValues,
                                            bool RemitterNameIsDirty,
                                            string RemitterName,
                                            string paymentSource) 
        {

            /* construct XML for the batch update
                <Batches>
                <Batch 
                        BankID="1" 
                        OrganizationID="0" 
                        ClientAccountID="99" 
                        SiteID="7000" 
                        ImmutableDate="2009-10-13 00:00:00" 
                        SourceBatchID="45">
                    <DataEntry>
                        <DataEntry TransactionID="1" DocumentBatchSequence="1" IsCheck="0" FieldName="AmountDue" SourceDisplayName="Amount Due" TableName="StubsDataEntry" DataEntryValue="973.99" />
                        <DataEntry TransactionID="2" DocumentBatchSequence="3" IsCheck="0" FieldName="COAMarkSense" SourceDisplayName="COA Mark Sense" TableName="StubsDataEntry" DataEntryValue="Updated" />
                    </DataEntry>
                    <Transaction  TransactionID="1" Sequence="1">
                        <Checks  GlobalCheckID="" TransactionID="1" TransactionSequence="1" BatchSequence="2" CheckSequence="1" Serial="" RT="271081528" Account="" TransactionCode="" RemitterName="" Amount="1.00" />
                        <Stubs  GlobalStubID="" TransactionID="1" TransactionSequence="3" BatchSequence="5" StubSequence="1" AccountNumber="" Amount="10.05" />
                    </Transaction>
                </Batch>
            </Batches>
            */

            XmlDocument xOutput = new XmlDocument();
            XmlNode BatchesNode;
            XmlNode BatchNode;
            XmlNode DataEntryNode;
            int iTransactionID;
            XmlNode TransactionNode;
            XmlNode CheckNode;
            XmlNode StubNode;

            string sTableName;
            string sKeyword_Value;
            XmlNode DataEntryChildNode;

            try
            {

                eventLog.logEvent("CreateUpdateXML:  TransactionID=" + myfact.TransactionID.ToString()
                                    , this.GetType().Name
                                    , MessageType.Information
                                    , MessageImportance.Debug);
                eventLog.logEvent("CreateUpdateXML:  IsCheck=" + myfact.IsCheck.ToString()
                                    , this.GetType().Name
                                    , MessageType.Information
                                    , MessageImportance.Debug);

                // create the Batches node
                BatchesNode = xOutput.CreateElement("Batches");
                xOutput.AppendChild(BatchesNode);

                // create the Batch node under the batches node
                BatchNode = xOutput.CreateElement("Batch");
                BatchesNode.AppendChild(BatchNode);

                // add the Batch attributes
                //AddAttribute(xOutput, BatchNode, "GlobalBatchID", "");
                AddAttribute(xOutput, BatchNode, "BankID", _clientMapper.GetBankIdFromClientId(LockboxDetails.ClientAccountID).ToString());
                AddAttribute(xOutput, BatchNode, "CustomerID", serverOptions.DefaultCustomerID.ToString());
                AddAttribute(xOutput, BatchNode, "ClientAccountID", LockboxDetails.ClientAccountID.ToString());
                AddAttribute(xOutput, BatchNode, "SiteID", LockboxDetails.SiteCodeID.ToString());
                AddAttribute(xOutput, BatchNode, "ImmutableDate", LockboxDetails.ProcessingDateAsString);
                AddAttribute(xOutput, BatchNode, "BatchID", LockboxDetails.BatchID.ToString());
                AddAttribute(xOutput, BatchNode, "SourceBatchID", LockboxDetails.SourceBatchID.ToString());
                AddAttribute(xOutput, BatchNode, "BatchSequence", LockboxDetails.BatchSequence.ToString());
                AddAttribute(xOutput, BatchNode, "BatchNumber", LockboxDetails.BatchNumber.ToString());
                AddAttribute(xOutput, BatchNode, "BatchPaymentTypeKey", LockboxDetails.BatchPaymentTypeKey.ToString());
                AddAttribute(xOutput, BatchNode, "DepositStatus", LockboxDetails.DepositStatus.ToString());
                AddAttribute(xOutput, BatchNode, "BatchSource", paymentSource);

                // add a DataEntry node
                DataEntryNode = xOutput.CreateElement("DataEntry");

                iTransactionID = -1;
                TransactionNode = null;
                CheckNode = null;
                StubNode = null;

                if (myfact.TransactionID != iTransactionID)
                {
                    iTransactionID = Convert.ToInt32(myfact.TransactionID);
                    TransactionNode = xOutput.CreateElement("Transaction");
                    eventLog.logEvent("CreateUpdateXML:  Creating Transaction Element"
                                    , this.GetType().Name
                                    , MessageType.Information
                                    , MessageImportance.Debug);

                    // add the transaction attributes
                    //AddAttribute(xOutput, TransactionNode, "GlobalBatchID", "");
                    AddAttribute(xOutput, TransactionNode, "BatchID", LockboxDetails.BatchID.ToString());
                    AddAttribute(xOutput, TransactionNode, "SourceBatchID", LockboxDetails.SourceBatchID.ToString());
                    AddAttribute(xOutput, TransactionNode, "TransactionID", myfact.TransactionID.ToString());
                    AddAttribute(xOutput, TransactionNode, "BatchSequence", LockboxDetails.BatchSequence.ToString());
                    AddAttribute(xOutput, TransactionNode, "Sequence", myfact.TxnSequence.ToString());
                    AddAttribute(xOutput, TransactionNode, "BatchNumber", myfact.BatchNumber.ToString());
                    AddAttribute(xOutput, TransactionNode, "BatchPaymentTypeKey", myfact.BatchPaymentTypeKey.ToString());
                    AddAttribute(xOutput, TransactionNode, "DepositStatus", myfact.DepositStatus.ToString());
                }

                if (myfact.IsCheck)
                {

                    CheckNode = xOutput.CreateElement("Checks");
                    eventLog.logEvent("CreateUpdateXML:  Creating Checks Element"
                                    , this.GetType().Name
                                    , MessageType.Information
                                    , MessageImportance.Debug);

                    // Add Checks MICR fields
                    if (MICRFieldValues.AccountIsDirty)
                    {
                        AddAttribute(xOutput, CheckNode, "Account", MICRFieldValues.Account);
                    }

                    if (MICRFieldValues.AmountIsDirty)
                    {
                        AddAttribute(xOutput, CheckNode, "Amount", MICRFieldValues.Amount);
                    }

                    if (MICRFieldValues.RTIsDirty)
                    {
                        AddAttribute(xOutput, CheckNode, "RT", MICRFieldValues.RT);
                    }

                    if (MICRFieldValues.SerialIsDirty)
                    {
                        AddAttribute(xOutput, CheckNode, "Serial", MICRFieldValues.Serial);
                    }

                    if (MICRFieldValues.TransactionCodeIsDirty)
                    {
                        AddAttribute(xOutput, CheckNode, "TransactionCode", MICRFieldValues.TransactionCode);
                    }

                    if (RemitterNameIsDirty)
                    {
                        AddAttribute(xOutput, CheckNode, "RemitterName", RemitterName);
                    }

                }
                else if (myfact.IsStub || myfact.IsDocument)
                {
                    StubNode = xOutput.CreateElement("Stubs");
                    eventLog.logEvent("CreateUpdateXML:  Creating Stubs Element"
                                    , this.GetType().Name
                                    , MessageType.Information
                                    , MessageImportance.Debug);

                    // Add Stubs MICR fields
                    if (MICRFieldValues.StubAccountIsDirty)
                    {
                        AddAttribute(xOutput, StubNode, "AccountNumber", MICRFieldValues.StubAccount);
                    }

                    if (MICRFieldValues.StubAmountIsDirty)
                    {
                        AddAttribute(xOutput, StubNode, "Amount", MICRFieldValues.StubAmount);
                    }

                    if (MICRFieldValues.TransactionCodeIsDirty)
                    {
                        AddAttribute(xOutput, StubNode, "TransactionCode", MICRFieldValues.TransactionCode);
                    }
                }

                eventLog.logEvent("CreateUpdateXML:   myIMSKeywordList count=" + myIMSKeywordList.Count.ToString()
                                    , this.GetType().Name
                                    , MessageType.Information
                                    , MessageImportance.Debug);

                // now that the DataEntry and Transaction nodes are created, add the DataEntry, Checks, and Stubs nodes
                foreach (IMSKeywordPair myKeyword in myIMSKeywordList)
                {

                    sTableName = GetDataEntryTableName(myfact.IsCheck, myKeyword.SourceDisplayName);
                    // CR 32168 12/23/2010 JNE - modify single quote (if any exist) to 2 single quotes for stored procedure call.
                    sKeyword_Value = myKeyword.Value.Replace("'", "''");

                    eventLog.logEvent("CreateUpdateXML:   sTableName=" + sTableName
                                     , this.GetType().Name
                                     , MessageType.Information
                                     , MessageImportance.Debug);

                    if (sTableName == "Checks")
                    {
                        // Check field value
                        AddAttribute(xOutput, CheckNode, myKeyword.Keyword, sKeyword_Value);
                    }

                    var shouldCreateDataEntry =
                        sTableName == "ChecksDataEntry" ||
                        sTableName == "StubsDataEntry" ||
                        (sTableName == "Stubs" && myfact.IsStub && IsSpecialStubFieldName(myKeyword.SourceDisplayName));
                    if (shouldCreateDataEntry)
                    {
                        DataEntryChildNode = xOutput.CreateElement("DataEntry");

                        AddAttribute(xOutput, DataEntryChildNode, "TransactionID", myfact.TransactionID.ToString());
                        AddAttribute(xOutput, DataEntryChildNode, "BatchSequence", myfact.BatchSequence.ToString());
                        AddAttribute(xOutput, DataEntryChildNode, "BatchNumber", myfact.BatchNumber.ToString());
                        AddAttribute(xOutput, DataEntryChildNode, "BatchPaymentTypeKey", myfact.BatchPaymentTypeKey.ToString());
                        AddAttribute(xOutput, DataEntryChildNode, "IsCheck", myfact.IsCheck.ToString());
                        AddAttribute(xOutput, DataEntryChildNode, "DepositStatus", myfact.DepositStatus.ToString());
                        AddAttribute(xOutput, DataEntryChildNode, "FieldName", myKeyword.Keyword);
                        AddAttribute(xOutput, DataEntryChildNode, "SourceDisplayName", myKeyword.SourceDisplayName);
                        AddAttribute(xOutput, DataEntryChildNode, "DataEntryValue", sKeyword_Value);
                        DataEntryNode.AppendChild(DataEntryChildNode);
                    }

                    if (sTableName == "Stubs" && myfact.IsStub)
                    { // if it's not a stub, don't add it 
                        // DataEntryValue
                        AddAttribute(xOutput, StubNode, myKeyword.Keyword, sKeyword_Value);
                    }
                }

                // add the Checks, Stubs, or DataEntry nodes as needed
                if (CheckNode != null && CheckNode.Attributes.Count > 0)
                {

                    // add the check attributes
                    AddAttribute(xOutput, CheckNode, "GlobalCheckID", "");
                    AddAttribute(xOutput, CheckNode, "BatchID", LockboxDetails.BatchID.ToString());
                    AddAttribute(xOutput, CheckNode, "SourceBatchID", LockboxDetails.SourceBatchID.ToString());
                    AddAttribute(xOutput, CheckNode, "TransactionID", myfact.TransactionID.ToString());
                    AddAttribute(xOutput, CheckNode, "TransactionSequence", myfact.TxnSequence.ToString());
                    AddAttribute(xOutput, CheckNode, "BatchSequence", myfact.BatchSequence.ToString());
                    AddAttribute(xOutput, CheckNode, "BatchNumber", myfact.BatchNumber.ToString());
                    AddAttribute(xOutput, CheckNode, "BatchPaymentTypeKey", myfact.BatchPaymentTypeKey.ToString());
                    AddAttribute(xOutput, CheckNode, "DepositStatus", myfact.DepositStatus.ToString());
                    AddAttribute(xOutput, CheckNode, "CheckSequence", myfact.Sequence.ToString());
                    TransactionNode.AppendChild(CheckNode);                   
                }

                if (StubNode != null && StubNode.Attributes.Count > 0)
                {

                    AddAttribute(xOutput, StubNode, "GlobalStubID", "");
                    AddAttribute(xOutput, StubNode, "BatchID", LockboxDetails.BatchID.ToString());
                    AddAttribute(xOutput, StubNode, "SourceBatchID", LockboxDetails.SourceBatchID.ToString());
                    AddAttribute(xOutput, StubNode, "TransactionID", myfact.TransactionID.ToString());
                    AddAttribute(xOutput, StubNode, "TransactionSequence", myfact.TxnSequence.ToString());
                    AddAttribute(xOutput, StubNode, "BatchSequence", myfact.BatchSequence.ToString());
                    AddAttribute(xOutput, StubNode, "BatchNumber", myfact.BatchNumber.ToString());
                    AddAttribute(xOutput, StubNode, "BatchPaymentTypeKey", myfact.BatchPaymentTypeKey.ToString());
                    AddAttribute(xOutput, StubNode, "DepositStatus", myfact.DepositStatus.ToString());
                    AddAttribute(xOutput, StubNode, "StubSequence", myfact.Sequence.ToString());
                    TransactionNode.AppendChild(StubNode);
                }

                if (DataEntryNode != null && 
                    DataEntryNode.ChildNodes.Count > 0)
                    BatchNode.AppendChild(DataEntryNode);   // don't add the DataEntry node if there are not data entry fields

                if (TransactionNode != null &&
                    TransactionNode.ChildNodes.Count > 0)
                    BatchNode.AppendChild(TransactionNode); // don't add the transaction node if there are no checks or stubs
            }

            catch (Exception ex)
            {
                eventLog.logEvent("Exception in CreateUpdateXML: " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }

            return xOutput;
        }


        private string GetDataEntryTableName(bool bIsCheck, string sKeyword) {

            string sRet = string.Empty;

            try {
                if (bIsCheck) {
                    // CR 29756 MEH 05-20-2010 ICON Service does not create check fields (Account, RT, and Serial, etc.) in the correct table
                    if (sKeyword.CompareTo("RemitterName") == 0 || sKeyword.CompareTo("Applied Amount") == 0 || sKeyword.CompareTo("RT") == 0 || sKeyword.CompareTo("Account") == 0 || sKeyword.CompareTo("Serial") == 0 || sKeyword.CompareTo("Transaction Code") == 0)
                        sRet = "Checks";
                    else
                        sRet = "ChecksDataEntry";
                } else {
                    if (IsSpecialStubFieldName(sKeyword))
                        sRet = "Stubs";
                    else
                        sRet = "StubsDataEntry";
                }
            } catch (Exception ex) {
                eventLog.logEvent("Exception in GetDataEntryTableName : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }

            return sRet;
        }

        /// <summary>
        /// Is this a stub field name that (1) should have "Stubs" as its table name to match
        /// what's defined in the database, and (2) should go into both the fact table and data entry?
        /// </summary>
        private bool IsSpecialStubFieldName(string fieldName)
        {
            switch (fieldName) {
                case "Account":
                case "Account Number":
                case "Amount":
                case "Applied Amount":
                    return true;

                default:
                    return false;
            }
        }

        private string GetXMLString(XmlDocument myDoc) {

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            myDoc.WriteTo(xw);
            return sw.ToString();
        }

        private void AddAttribute(XmlDocument doc, XmlNode node, string sAttributeName, string sAttributeValue) {

            XmlAttribute BatchSequence = doc.CreateAttribute(sAttributeName);
            BatchSequence.Value = sAttributeValue;
            node.Attributes.Append(BatchSequence);
        }

        private bool DeleteBatchImages(int siteBankID, int clientID, int processingDateKey, string fileType, string batchPaymentSourceName, long BatchID)
        {

            bool rtrnFlag = false;

            try
            {
                rtrnFlag = OLFService.DeleteBatchImages(
                    clientID,
                    processingDateKey,
                    siteBankID,
                    fileType,
                    batchPaymentSourceName,
                    BatchID);

       
            }
            catch (Exception ex)
            {
                eventLog.logEvent("Exception in DeleteBatchImages: " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
                rtrnFlag = false;
            }

            return rtrnFlag;
        }

        private bool GetOLTAKeysFromKeywords(List<IMSKeywordPair> listQueryKeywords, string paymentSource, out cICONBatchIDFields OLTAKeys) { 

            DateTime dteTemp;
            int intTemp;

            cICONBatchIDFields objLockboxDetails = new cICONBatchIDFields(serverOptions.DefaultBankID);
            char[] chrBinaryCompletionFlag = new char[] {'0','0','0','0','0'};
            bool bolInvalidData = false;
            bool bolRetVal;

            try {
                foreach (IMSKeywordPair queryKeyword in listQueryKeywords) {
                    switch (queryKeyword.Keyword.ToUpper().Trim()) {
                        case "P1 SEQUENCE":
                            if(queryKeyword.Value.Trim().Length > 0) {
                                if(int.TryParse(queryKeyword.Value, out intTemp)) {
                                    objLockboxDetails.BatchSequence = intTemp;
                                } else {
                                    eventLog.logEvent("Invalid value specified for 'P1 Sequence': [" + queryKeyword.Value + "].", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                                    bolInvalidData = true;
                                }
                            }
                            chrBinaryCompletionFlag[0] = '1';
                            break;
                        // Currently we are not sure if "Sequence Number" is a valid XML node??
                        //case "SEQUENCE NUMBER":
                        //    // Only do this if 'P1 Sequence' is not populated.
                        //    if(chrBinaryCompletionFlag[0] != '1') {
                        //        if(int.TryParse(queryKeyword.Value, out intTemp)) {
                        //            objLockboxDetails.BatchSequence = intTemp;
                        //        } else {
                        //            eventLog.logEvent("Invalid value specified for 'Sequence Number': [" + queryKeyword.Value + "].", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        //            // No need to set a false return flag at this point.
                        //        }
                        //    }
                        //    break;
                        case "BATCH DATE":
                            if(DateTime.TryParseExact(queryKeyword.Value, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dteTemp)) {
                                objLockboxDetails.ProcessingDate = dteTemp;
                            } else {
                                eventLog.logEvent("Invalid value specified for 'BATCH DATE': [" + queryKeyword.Value + "].", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                                bolInvalidData = true;
                            }
                                
                            chrBinaryCompletionFlag[1] = '1';
                            break;
                        case "BATCH ID":
                            if(int.TryParse(queryKeyword.Value, out intTemp)) {
                                objLockboxDetails.SourceBatchID = intTemp;
                            } else {
                                eventLog.logEvent("Invalid value specified for 'BATCH ID': [" + queryKeyword.Value + "].", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                                bolInvalidData = true;
                            }
                            chrBinaryCompletionFlag[2] = '1';
                            break;
                        case "SITE ID":
                            if(int.TryParse(queryKeyword.Value, out intTemp)) {
                                objLockboxDetails.SiteCodeID = intTemp;
                            } else {
                                eventLog.logEvent("Invalid value specified for 'SITE ID': [" + queryKeyword.Value + "].", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                                bolInvalidData = true;
                            }
                            chrBinaryCompletionFlag[3] = '1';
                            break;
                        case "CLIENT ID":
                            if(int.TryParse(queryKeyword.Value, out intTemp)) {
                                objLockboxDetails.ClientAccountID = intTemp;
                            } else {
                                eventLog.logEvent("Invalid value specified for 'CLIENT ID': [" + queryKeyword.Value + "].", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                                bolInvalidData = true;
                            }
                            chrBinaryCompletionFlag[4] = '1';
                            break;
                    }

                    // Exit when all values are found in the input array.
                    if(new string(chrBinaryCompletionFlag) == "11111") {
                        break;
                    }
                }

                //Set the BankID to the value from the mapping table. This was the default BankID from the INI file when the object was created.
                objLockboxDetails.BankID = _clientMapper.GetBankIdFromClientId(objLockboxDetails.ClientAccountID);

                OLTAKeys = objLockboxDetails;

                if (new string(chrBinaryCompletionFlag) == "11111" || !bolInvalidData)
                {                    
                    long batchID = -2;
                    int depositDate = -1;
                    int batchNumber = -2;
                    short batchPaymentTypeKey = -2;
                    int depositStatus = -2;
                    ICONBLL.GetBatchID(objLockboxDetails.ProcessingDateKey,
                                       _clientMapper.GetBankIdFromClientId(objLockboxDetails.ClientAccountID),
                                       serverOptions.DefaultCustomerID,                      
                                       objLockboxDetails.ClientAccountID,
                                       objLockboxDetails.SiteCodeID,
                                       objLockboxDetails.SourceBatchID,
                                       paymentSource,
                                       out batchID,
                                       out depositDate,
                                       out batchNumber,
                                       out batchPaymentTypeKey,
                                       out depositStatus);
                    OLTAKeys.BatchID = batchID;
                    OLTAKeys.DepositDateKey = depositDate;
                    OLTAKeys.BatchNumber = batchNumber;
                    OLTAKeys.BatchPaymentTypeKey = batchPaymentTypeKey;
                    OLTAKeys.DepositStatus = depositStatus;
                    if (batchID > 0)
                    {
                        bolRetVal = true;
                    }
                    else
                    {
                        eventLog.logEvent("GetBatchID failed: " +
                            " ProcessingDateKey= " + objLockboxDetails.ProcessingDateKey +
                            ", DefaultBankID= " + _clientMapper.GetBankIdFromClientId(objLockboxDetails.ClientAccountID) +
                            ", DefaultCustomerID= " + serverOptions.DefaultCustomerID +
                            ", ClientAccountID= " + objLockboxDetails.ClientAccountID +
                            ", SiteCodeID= " + objLockboxDetails.SiteCodeID +
                            ", SourceBatchID= " + objLockboxDetails.SourceBatchID,
                            this.GetType().Name, MessageType.Error, 
                            MessageImportance.Essential);
                        bolRetVal = false;
                    }
                }
                else
                {
                    bolRetVal = false;
                }       
            } catch (Exception ex) {
                eventLog.logEvent("Exception in GetQueryKeywords : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                OLTAKeys = null;
                bolRetVal = false;
            }            

            return(bolRetVal);
        }

        private static string AddToReturnMsg(string Msg, string NewText) {
            if(string.IsNullOrEmpty(Msg)) {
                return(NewText);
            } else {
                if(Msg.Length > 0) {
                    return(Msg + "; " + NewText);
                } else {
                    return(NewText);
                }
            }
        }        
    }
}
