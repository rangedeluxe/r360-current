using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;

using WFS.integraPAY.Online.ICONCommon;
/******************************************************************************
** Wausau
** Copyright � 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     02/09/2010
*
* Purpose:  integraPAY Online Services ImageRPS XML command object.
*
* Modification History
* CR 28461 MEH 02/09/2010
*    -Initial release.
* CR 33541 JMC 04/09/2011
*    -Modified class to use IMSKeyword instead of Keyword object.  This was
*     done so that both the Windows and Web service can share business logic.
******************************************************************************/

namespace WFS.integraPAY.Online.ICONServicesAPI
{
    internal class RemitXMLCommand
    {
        public List<CommandList> listCommandList = null;

        public RemitXMLCommand(string sXML)
        {
            listCommandList = new List<CommandList>();

            XmlDocument xInput = new XmlDocument();
            xInput.InnerXml = sXML;

            XmlNodeList nCommandList = xInput.SelectNodes("/RemitXMLCommand/CommandList");

            foreach (XmlNode nCommandListNode in nCommandList)
            {
                CommandList cmdlList = new CommandList();

                XmlNodeList listCommand = nCommandListNode.SelectNodes("Command");

                foreach (XmlNode nCommandNode in listCommand)
                {
                    Command cmdCommand = new Command(nCommandNode.Attributes["CommandType"].InnerText);

                    XmlNodeList listQueryKeywords = nCommandNode.SelectNodes("QueryKeywords/QueryKeyword");

                    foreach (XmlNode nQueryKeyword in listQueryKeywords)
                    {
                        IMSKeywordPair kwQueryKeyWord = new IMSKeywordPair(nQueryKeyword.Attributes["Name"].InnerText, nQueryKeyword.Attributes["Value"].InnerText);

                        cmdCommand.AddQueryKeyword(kwQueryKeyWord);
                    }

                    XmlNodeList listActionKeywords = nCommandNode.SelectNodes("ActionKeywords/ActionKeyword");

                    foreach (XmlNode nActionKeyword in listActionKeywords)
                    {
                        IMSKeywordPair kwActionKeyWord = new IMSKeywordPair(nActionKeyword.Attributes["Name"].InnerText, nActionKeyword.Attributes["Value"].InnerText);

                        cmdCommand.AddActionKeyword(kwActionKeyWord);
                    }

                    cmdlList.AddCommand(cmdCommand);
                }

                listCommandList.Add(cmdlList);
            }
        }
    }
}
