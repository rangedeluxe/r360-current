﻿namespace WFS.integraPAY.Online.ICONCommon
{
    /// <summary>
    /// ImageRPS data types, as seen in ImageRPS client-setup files' &lt;Keytype&gt; elements' datatype attributes.
    /// These are *not* the same numeric values that we store in the R360 database.
    /// </summary>
    /// <remarks>
    /// If the ICON XClient finds unknown values, it logs a warning (in Verbose mode) and treats the field as
    /// alphanumeric (equivalent to <see cref="String"/>).
    /// </remarks>
    public enum ImageRpsDataType
    {
        /// <summary>
        /// 1 = Numeric. Maps to a value of "6" in the canonical XML.
        /// </summary>
        /// <remarks>
        /// This data type was documented as "Numeric 20" in ClientXMLNode.GetOLTADataType.
        /// </remarks>
        Numeric = 1,

        /// <summary>
        /// 2 = String. Maps to a value of "1" in the canonical XML.
        /// </summary>
        /// <remarks>
        /// This data type was documented as "Alphanumeric" in ClientXMLNode.GetOLTADataType.
        /// </remarks>
        String = 2,

        /// <summary>
        /// 3 = Currency. Maps to a value of "7" in the canonical XML.
        /// </summary>
        Currency = 3,

        /// <summary>
        /// 4 = Date. Maps to a value of "11" in the canonical XML.
        /// </summary>
        Date = 4,

        /// <summary>
        /// This value is not documented in the DIT documentation, but is handled by ClientXMLNode.GetOLTADataType.
        /// Maps to a value of "6" in the canonical XML.
        /// </summary>
        /// <remarks>
        /// This data type was documented as "Numeric?" in ClientXMLNode.GetOLTADataType.
        /// </remarks>
        AlternateNumeric = 5,

        /// <summary>
        /// 6 = ID or Sequence Number. Maps to a value of "6" in the canonical XML.
        /// </summary>
        /// <remarks>
        /// This data type was documented as "Numeric 9" in ClientXMLNode.GetOLTADataType.
        /// </remarks>
        IdOrSequenceNumber = 6,

        /// <summary>
        /// This value is not documented in the DIT documentation, but is handled by ClientXMLNode.GetOLTADataType.
        /// Maps to a value of "11" in the canonical XML.
        /// </summary>
        /// <remarks>
        /// This data type was documented as "Date/Time" in ClientXMLNode.GetOLTADataType.
        /// </remarks>
        AlternateDateTime = 9,

        /// <summary>
        /// This value is not documented in the DIT documentation, but is handled by ClientXMLNode.GetOLTADataType.
        /// Maps to a value of "1" in the canonical XML.
        /// </summary>
        /// <remarks>
        /// This data type was documented as "AlphaNumeric" in ClientXMLNode.GetOLTADataType.
        /// </remarks>
        AlternateAlphanumeric = 10,
    }
}