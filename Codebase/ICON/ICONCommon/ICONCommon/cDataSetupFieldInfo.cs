﻿using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     MM/DD/YYYY
*
* Purpose:  
*
* Modification History
* CR 33541 JMC 04/09/2011
*    -
******************************************************************************/
namespace WFS.integraPAY.Online.ICONCommon
{
    public class cDataSetupFieldInfo
    {
        public cDataSetupFieldInfo(int iItemDataSetupFieldKey, int iDataType)
        {
            ItemDataSetupFieldKey = iItemDataSetupFieldKey;
            DataType = iDataType;
        }
        public int ItemDataSetupFieldKey
        {
            get;
            set;
        }
        public int DataType
        {
            get;
            set;
        }    
    }
}
