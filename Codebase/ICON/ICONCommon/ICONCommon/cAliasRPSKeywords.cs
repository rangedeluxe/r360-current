using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     MM/DD/YYYY
*
* Purpose:  
*
* Modification History
* CR 33541 JMC 04/09/2011
*    -
* CR 34394 JMC 05/20/2011
*    -Added a check to AssignMICRInfo() to validate that an item is a 
*     business check, and has length > 0 when assigning ExtractType 1 to the
*     Serial field.
* CR 51067 WJS 3/15/2012 
*    - For stubs you want to add the micr info to data entry details as well
******************************************************************************/
namespace WFS.integraPAY.Online.ICONCommon
{
    public static class AliasRPSKeywordLib {


        public static SetupDocType ParseDocType(string Value) {

            SetupDocType enmRetVal;

            switch(Value.Trim()) {
                case "1":
                    enmRetVal = SetupDocType.NonCheck;
                    break;
                case "2":
                    enmRetVal = SetupDocType.Check;
                    break;
                default:
                    enmRetVal= SetupDocType.Unknown;
                    break;
            }

            return(enmRetVal);
        }


        public static string ParseDocType(SetupDocType docType)
        {

            string enmRetVal;

            switch (docType)
            {
                case SetupDocType.NonCheck:
                    enmRetVal ="1";
                    break;
                case SetupDocType.Check:
                    enmRetVal = "2";
                    break;
                default:
                    enmRetVal = "-1";
                    break;
            }

            return (enmRetVal);
        }
        public static SetupExtractType ParseExtractType(string Value) {

            SetupExtractType enmRetVal;

            switch(Value.Trim()) {
                case "1":
                    enmRetVal = SetupExtractType.BusCheckNumber;
                    break;
                case "2":
                    enmRetVal = SetupExtractType.RoutingTransit;
                    break;
                case "3":
                    enmRetVal = SetupExtractType.Account;
                    break;
                case "4":
                    enmRetVal = SetupExtractType.PersonalCheckNum;
                    break;
                default:
                    enmRetVal = SetupExtractType.Unknown;
                    break;
            }

            return(enmRetVal);
        }

        public static string BuildKeyword(string AliasName, SetupDocType DocType) {
            StringBuilder keyWordBuilder = new StringBuilder();
            keyWordBuilder.Append(AliasName);
            keyWordBuilder.Append("|");
            keyWordBuilder.Append(ParseDocType(DocType));
            return keyWordBuilder.ToString();
        }

        //public static bool AssignMICRInfo(string ExtractType,
        //                                  IMSKeywordPair keyWordPair, 
        //                                  ItemType ItemTypeValue, 
        //                                  MICRFields field) {
        //    return(AssignMICRInfo(ParseExtractType(ExtractType),
        //                          keyWordPair,
        //                          ItemTypeValue,
        //                          field));
        //}

        public static bool AssignMICRInfo(SetupExtractType ExtractType,
                                          IMSKeywordPair keyWordPair, 
                                          ItemType ItemTypeValue, 
                                          MICRFields field) {
            bool bolRetVal = false;
            bool bolIsCheck;

            bolIsCheck = (ItemTypeValue == ItemType.BusinessCheck || ItemTypeValue == ItemType.PersonalCheck);

            switch(ExtractType)
            {
                case SetupExtractType.BusCheckNumber:       // 1
                    if(ItemTypeValue == ItemType.BusinessCheck && keyWordPair.Value.Trim().Length > 0) {
                        AssignSerial(keyWordPair.Value, ref field);
                    }
                    bolRetVal = true;
                    break;
                case SetupExtractType.RoutingTransit:       // 2
                    AssignRT(keyWordPair.Value, ref field);
                    bolRetVal = true;
                    break;
                case SetupExtractType.Account:              // 3
                    AssignAccount(keyWordPair.Value, bolIsCheck, ref field);
                    //CR 51067 WJS 3/15/2012 only if check remove it other wise you want to add it data entry as well
                    if (bolIsCheck) {
                        bolRetVal = true;
                    }
                    break;
                case SetupExtractType.PersonalCheckNum:     // 4
                    if(ItemTypeValue == ItemType.BusinessCheck) {
                        // If this is a Business Check, this is the TransactionCode
                        AssignTransCode(keyWordPair.Value, ref field);
                        bolRetVal = true;
                    } else {
                        // If this is NOT a Business Check, this is the Serial
                        AssignSerial(keyWordPair.Value, ref field);
                        bolRetVal = true;
                    }
                    break;
                default:
                    switch (keyWordPair.Keyword)
                    {
                        case cMagicKeywords.Amount:
                        case cMagicKeywords.AppliedAmount:
                            AssignAmount(keyWordPair.Value, bolIsCheck, ref field);
                             //CR 51067 WJS 3/15/2012 only if check remove it other wise you want to add it data entry as well
                            if (bolIsCheck) {
                                bolRetVal = true;
                             }
                            break;
                    }
                    break;
            }

            return(bolRetVal);
        }

        private static void AssignAccount(string account, bool bIsCheck, ref MICRFields field)
        {
             if (bIsCheck)
                 field.Account = account; 
             else
                 field.StubAccount = account; 
        }

        private static void AssignSerial(string serialValue, ref MICRFields field)
        {
            field.Serial = serialValue;
        }

        private static void AssignTransCode(string transCode, ref MICRFields field)
        {
            field.TransactionCode = transCode;
        }

        private static void AssignRT(string RTValue, ref MICRFields field)
        {
            field.RT = RTValue;
        }

        private static void AssignAmount(string amount, bool bIsCheck, ref MICRFields field)
        {
             if (bIsCheck)
                 field.Amount = amount; 
             else
                 field.StubAmount = amount; 
        }

        /// <summary>
        /// Uses the legacy logic for determining MICR values.  This approach relies on hardcoded 
        /// Keyword values to determine MICR values.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="keyWordPair"></param>
        /// <param name="bIsCheck"></param>
        /// <returns></returns>
        public static bool IsMICRHardCodeValues(IMSKeywordPair keyWordPair, bool bIsCheck, MICRFields field)
        {
            bool bFoundHardValue = true;
            switch (keyWordPair.Keyword)
            {
               case cMagicKeywords.RT:
                    AssignRT(keyWordPair.Value, ref field);
                   break;
               case cMagicKeywords.Account:
               case cMagicKeywords.AccountNumber:
                   AssignAccount(keyWordPair.Value, bIsCheck, ref field);
                   break;
               case cMagicKeywords.Serial:
                   AssignSerial(keyWordPair.Value, ref field);
                   break;
               case cMagicKeywords.TransactionCode:
                   AssignTransCode(keyWordPair.Value, ref field);
                   break;
               case cMagicKeywords.Amount:
               case cMagicKeywords.AppliedAmount:
                   AssignAmount(keyWordPair.Value, bIsCheck, ref field);
                   break;
                default:
                    bFoundHardValue = false;  
                    break;
            }
            return bFoundHardValue;
        }
    }
}
