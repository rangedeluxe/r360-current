using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Mark Horwich
* Date:     09/22/2009
*
* Purpose:  IntegraPAY Online Image Service options class.
*
* Modification History
*   09/22/2009 MEH
*       - Created class
* CR 31536 JMC 04/02/2011
*    -Ported class to the ICON BLL.
* WI 145906 DJW 06/05/2014
*    - Modified to handle the int BatchID to Int64 BatchID 
* WI 193808 TWE 03/11/2015
*     Handle the Item updates
******************************************************************************/
namespace WFS.integraPAY.Online.ICONCommon {

    public class cICONBatchIDFields : IICONBatchIDFields, IICONItemIDFields {

        private int _SiteCodeID = -1;
        private int _BankID = -1;
        private int _ClientAccountID = -1;
        private DateTime _ProcessingDate = DateTime.MinValue;
        private long _BatchID = -1;
        private long _SourceBatchID = -1;
        private int _BatchSequence = -1;
        private int _DepositDateKey = -1;
        private int _BatchNumber = -1;
        private short _BatchPaymentTypeKey = -1;
        private int _DepositStatus = -1;

        public cICONBatchIDFields(int BankIDValue) {
            _BankID = BankIDValue;
        }

        public int SiteCodeID {
            get { return(_SiteCodeID); }
            set { _SiteCodeID = value; }
        }

        public int BankID {
            get { return(_BankID); }
            set { _BankID = value; }
        }

        public int ClientAccountID {
            get { return(_ClientAccountID); }
            set { _ClientAccountID = value; }
        }

        public DateTime ProcessingDate {
            get { return(_ProcessingDate); }
            set { _ProcessingDate = value; }
        }

        public string ProcessingDateAsString {
            get { return(_ProcessingDate.ToString("MM/dd/yyyy")); }
        }                

        public int ProcessingDateKey {
            get { return(Convert.ToInt32(_ProcessingDate.ToString("yyyyMMdd"))); }
        }

        public int DepositDateKey
        {
            get { return (_DepositDateKey); }
            set {_DepositDateKey = value;}
        }

        public long BatchID
        {
            get { return (_BatchID); }
            set { _BatchID = value; }
        }

        public int BatchNumber
        {
            get { return (_BatchNumber); }
            set { _BatchNumber = value; }
        }

        public short BatchPaymentTypeKey
        {
            get { return (_BatchPaymentTypeKey); }
            set { _BatchPaymentTypeKey = value; }
        }

        public int DepositStatus
        {
            get { return (_DepositStatus); }
            set { _DepositStatus = value; }
        }

        public long SourceBatchID {
            get { return(_SourceBatchID); }
            set { _SourceBatchID = value; }
        }

        public int BatchSequence {
            get { return(_BatchSequence); }
            set { _BatchSequence = value; }
        }
    }

    public interface IICONBatchIDFields {
        int SiteCodeID { get; }
        int BankID { get; }
        int ClientAccountID { get; }
        DateTime ProcessingDate { get; }
        string ProcessingDateAsString { get; }
        int ProcessingDateKey { get; }
        int DepositDateKey { get; }
        long BatchID { get; }
        long SourceBatchID { get; }
        int BatchNumber { get; }
        short BatchPaymentTypeKey { get; }
        int DepositStatus { get; }
    }

    public interface IICONItemIDFields {
        int SiteCodeID { get; }
        int BankID { get; }
        int ClientAccountID { get; }
        DateTime ProcessingDate { get; }
        string ProcessingDateAsString { get; }
        int ProcessingDateKey { get; }
        int DepositDateKey { get; }
        long BatchID { get; }
        long SourceBatchID { get; }
        int BatchSequence { get; }
        int BatchNumber { get; }
        short BatchPaymentTypeKey { get; }
        int DepositStatus { get; }
    }
}
