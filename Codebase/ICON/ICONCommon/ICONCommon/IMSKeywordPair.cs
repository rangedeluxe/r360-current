﻿using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    3/2/2011
*
* Purpose:  IMSKeywordPair.cs.
*
* Modification History
* 03/02/2011 CR 31536 WJS
*     - Initial release.
******************************************************************************/
namespace WFS.integraPAY.Online.ICONCommon 
{
    public class IMSKeywordPair
    {
        private string _Keyword = string.Empty;
        private string _SourceDisplayName = string.Empty;
        private string _Value = string.Empty;

        public IMSKeywordPair()
        {
        }

        public IMSKeywordPair(string sKeyword, string sValue)
        {
            _Keyword = sKeyword;
            _Value = sValue;
        }

        public IMSKeywordPair(string sKeyword, string sReportTitle, string sValue)
        {
            _Keyword = sKeyword;
            _SourceDisplayName = sReportTitle;
            _Value = sValue;
        }

        public string Keyword
        {
            get { return (_Keyword); }
            set { _Keyword = value; }
        }

        public string SourceDisplayName
        {
            get { return (_SourceDisplayName); }
            set { _SourceDisplayName = value; }
        }

        public string Value
        {
            get { return (_Value); }
            set { _Value = value; }
        }
    }
}
