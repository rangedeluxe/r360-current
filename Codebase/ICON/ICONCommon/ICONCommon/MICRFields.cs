﻿using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    3/2/2011
*
* Purpose:  MICRFields.cs.
******************************************************************************/

namespace WFS.integraPAY.Online.ICONCommon
{
    public class MICRFields
    {
        private string _RT = string.Empty;
        private string _Account = string.Empty;
        private string _Serial = string.Empty;
        private string _TransactionCode = string.Empty;
        private string _Amount = string.Empty;
        private string _StubAccount = string.Empty;
        private string _StubAmount = string.Empty;

        private bool _RTIsDirty = false;
        private bool _AccountIsDirty = false;
        private bool _SerialIsDirty = false;
        private bool _TransactionCodeIsDirty = false;
        private bool _AmountIsDirty = false;
        private bool _StubAccountIsDirty = false;
        private bool _StubAmountIsDirty = false;

        public MICRFields()
        {
        }

        public string RT
        {
            get {
                return(_RT);
            }
            set {
                _RT = value;
                _RTIsDirty = true;
            }
        }

        public string Account
        {
            get {
                return(_Account);
            }
            set {
                _Account = value;
                _AccountIsDirty = true;
            }
        }

        public string Serial
        {
            get {
                return(_Serial);
            }
            set {
                _Serial = value;
                _SerialIsDirty = true;
            }
        }

        public string TransactionCode
        {
            get {
                return(_TransactionCode);
            }
            set {
                _TransactionCode = value;
                _TransactionCodeIsDirty = true;
            }
        }

        public string Amount
        {
            get {
                return(_Amount);
            }
            set {
                _Amount = value;
                _AmountIsDirty = true;
            }
        }
        public string StubAccount
        {
            get {
                return(_StubAccount);
            }
            set {
                _StubAccount = value;
                _StubAccountIsDirty = true;
            }
        }

        public string StubAmount
        {
            get {
                return(_StubAmount);
            }
            set {
                _StubAmount = value;
                _StubAmountIsDirty = true;
            }
        }

        public bool RTIsDirty {
            get {
                return(_RTIsDirty);
            }
        }

        public bool AccountIsDirty {
            get {
                return(_AccountIsDirty);
            }
        }

        public bool SerialIsDirty {
            get {
                return(_SerialIsDirty);
            }
        }

        public bool TransactionCodeIsDirty {
            get {
                return(_TransactionCodeIsDirty);
            }
        }

        public bool AmountIsDirty {
            get {
                return(_AmountIsDirty);
            }
        }

        public bool StubAccountIsDirty {
            get {
                return(_StubAccountIsDirty);
            }
        }

        public bool StubAmountIsDirty {
            get {
                return(_StubAmountIsDirty);
            }
        }

        public bool IsDirty {
            get {
                return(_RTIsDirty ||
                       _AccountIsDirty ||
                       _SerialIsDirty ||
                       _TransactionCodeIsDirty ||
                       _AmountIsDirty ||
                       _StubAccountIsDirty ||
                       _StubAmountIsDirty);
            }
        }
    }
}
