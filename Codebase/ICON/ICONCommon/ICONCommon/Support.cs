﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;


/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    3/2/2011
*
* Purpose:  Support.cs.
******************************************************************************/

namespace WFS.integraPAY.Online.ICONCommon {

    public enum SetupFieldType
    {
        [Description("A")]
        AccountField,
        [Description("D")]
        DollarField,
        [Description("O")]
        OtherDollarField,
        [Description("M")]
        MarkSenseField
    }

    public enum SetupDocType
    {
        Unknown = 0,
        NonCheck = 1,
        Check = 2
    }

    /// <summary>
    /// This is reprsented by BBBBBBBBBBBB RRRRRRRRR AAAAAAAAAAAAAAAA TTTTTTTTTTTT
    /// BBBBB is 1
    /// RRRR  is 2
    /// AAAA  is 3
    /// TTTT   is 4
    /// The rules for this are:
    /// 1)	If B is present then it is a serial
    /// 2)	If B is present and T is present then T is trans code
    /// 3)	If NO B is present then T is serial
    /// </summary>
    public enum SetupExtractType
    {
        Unknown       = 0,
        BusCheckNumber = 1,
        RoutingTransit = 2,
        Account = 3,
        PersonalCheckNum = 4,
        DocTypeIdentifier = 11
    }

    public enum UpdateType {
        Update,
        Append
    }

    public enum ItemType {
        Undefined = 0,
        PersonalCheck = 1,
        BusinessCheck = 2,
        NonCheck = 3,
    }
}
