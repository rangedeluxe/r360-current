using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Xml;
using WFS.integraPAY.Online.Common;
using System.Timers;
using System.Reflection;
using WFS.integraPAY.Online.ICONAPI;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Mark Horwich
* Date:     09/22/2009
*
* Purpose:  IntegraPAY Online Image Import Service.
* Modification History
* 09/22/2009 MEH
*    -Created class
* 01/11/2011 JNE
*    -In ProcessClientInputFiles - Added class SetupField and compared entries 
*    -against table name and field name when checking for duplicates.
* 01/13/2011 JNE 
*    -In AddDataEntryNode added check for single apostrophe in data value. 
*    -Converted it to two single apostrophe in xml
* CR 31536 WJS 03/02/2011
*    -Added support for factItemData and factBatchData
*    -Removed memory leaks
*    -Moved direct calls to DLL to BLL layer.
*    -Limit number of files which are retrived from directory
* CR 33547 WJS 03/30/2011
*    -Removed Client Poll Timer. Make it so we have only one timer and process client file first
* CR 45047 JMC 05/19/2011
*    -Added CreateZipArchiveFile() method to handle archiving of Batch and 
*     Client input files 
* CR 45049 JMC 05/19/2011
*    -Modified WriteImageRPSResponseFile() to no longer write to a SiteID subfolder
*    -Modified WriteImageRPSResponseFile() to initially write to a subfolder 
*     named 'InProcess',and be copied over to the root of the RPSResponseFileFolder
*     after it is completely written to the disk.
* CR 33841 WJS 6/14/2011
 *   - Moved DAT files which error out to error folder directory
 *   - If permission is denied to ICON directory then stop the service
 *   - Change procDate, depositDate to use native types
 * CR 47014 WJS 09/27/2011
*    - Fixed date info on pics to format correctly
* CR 45665 WJS 09/21/2011 
*    - Add support for shutdown service
* CR 47269 WJS 10/10/2011
 *  - Change processing date to use the system date instead
* CR 47665 WJS 10/24/2011	
*	- Look for any existance of that transaction ID first
* CR 48192 WJS 11/11/11
*  - Always write out image to PICS directory
* CR 49675 WJS 1/30/12
 *   - Passing in SourceProcessingDateKey
* CR 51046 WJS 3/19/2012
 *  - Added logging of query retry attempts
******************************************************************************/

namespace WFS.integraPAY.Online.ICONSvc
{
    public partial class ICONService : ServiceBase
    {
    
        private cICONServiceOptions _ServerOptions = null;
        private cIPOnlineOptions _IPOnlineOptions = null;
        private cEventLog _EventLog = null;
        private Timer m_tmBatchTimer = null;
        private cICONBll _ICONBll = null;
        private const int _limitDirectoryPath = 20;
        
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{                 
				new ICONService() 
			};
            ServiceBase.Run(ServicesToRun);
        }

        public ICONService()
        {
            InitializeComponent();
        }

        public void Go(bool DoProcessClientInputFiles, bool DoProcessBatchInputFiles) {

            _ICONBll = new cICONBll(IPOnlineOptions.siteKey);

            if(DoProcessClientInputFiles) {
                ProcessClientInputFiles();
            }

            if(DoProcessBatchInputFiles) {
                ProcessBatchInputFiles();
            }
        }

        #region Init

        

        private cICONServiceOptions serverOptions
        {
            get
            {
                try
                {
                    if (_ServerOptions == null)
                    {
                        _ServerOptions = new cICONServiceOptions
                            (ipoLib.INISECTION_OLFIMAGE_ICON_SERVICE);
                    }
                }
                catch (Exception ex)
                {
                    eventLog.logEvent("ICON Service Service started with errors. Reading Config Settings : " + ex.Message
                               , ex.Source
                               , MessageType.Error
                               , MessageImportance.Essential);
                }

                return _ServerOptions;
            }
        }

        private cIPOnlineOptions IPOnlineOptions
        {
            get
            {
                try
                {
                    if (_IPOnlineOptions == null)
                    {
                        // get "Sites" section
                        // look for X=ICONSvc
                        // get section for X
                        string sTargetSectionName = string.Empty;
                        string strKey;
                        string strValue;

                        StringCollection colSiteOptions = ipoINILib.GetINISection("Sites");

                        System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
                        while (myEnumerator.MoveNext())
                        {
                            strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                            strValue = myEnumerator.Current.ToString().Substring(strKey.Length + 1);

                            if (strValue.ToLower() == ipoLib.INISECTION_OLFIMAGE_ICON_SERVICE.ToLower())
                            {
                                sTargetSectionName = strKey;
                                break;
                            }
                        }

                        _IPOnlineOptions = new cIPOnlineOptions
                            (sTargetSectionName);
                    }
                }
                catch (Exception ex)
                {
                    eventLog.logEvent("ICON Service started with errors. Reading Config Settings : " + ex.Message
                               , ex.Source
                               , MessageType.Error
                               , MessageImportance.Essential);
                }

                return _IPOnlineOptions;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public cEventLog eventLog
        {
            get
            {
                if (_EventLog == null)
                {
                    _EventLog = new cEventLog(serverOptions.logFilePath
                                                , serverOptions.logFileMaxSize
                                                , (MessageImportance)serverOptions.loggingDepth);
                }

                return _EventLog;
            }
        }        

        #endregion

        #region Service Stop/Start

        protected override void OnStart(string[] args)
        {
            try
            {
                eventLog.logEvent("ICON Service started normally."
                            , this.GetType().Name
                           , MessageImportance.Essential);

                // log all the settings
                eventLog.logEvent("*****************************************************************************"
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("Service Options [ICON]"
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("DefaultBankID=" + serverOptions.defaultBankID.ToString()
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("DefaultCustomerID=" + serverOptions.defaultCustomerID.ToString()
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("ClientInputFolder=" + serverOptions.clientInputPath
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("ClientArchiveFolder=" + serverOptions.clientArchivePath
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("BatchInputFolder=" + serverOptions.batchInputPath
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("BatchArchiveFolder=" + serverOptions.batchArchivePath
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("RPSResponseFileFolder=" + serverOptions.responseFilePath
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("ErrorFolder=" + serverOptions.errorFolderPath
                           , this.GetType().Name
                           , MessageType.Information
                           , MessageImportance.Verbose);

                eventLog.logEvent("DBConnection2=" + IPOnlineOptions.connectionString
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("ImagePath=" + IPOnlineOptions.imagePath
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                eventLog.logEvent("Image Storage Mode: " + IPOnlineOptions.imageStorageMode.ToString()
                            , this.GetType().Name
                           , MessageImportance.Verbose);

                eventLog.logEvent("Query Retry Attempts: " + IPOnlineOptions.QueryRetryAttempts.ToString()
                           , this.GetType().Name
                          , MessageImportance.Verbose);

                eventLog.logEvent("*****************************************************************************"
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Verbose);

                _ICONBll = new cICONBll(IPOnlineOptions.siteKey);

                //Get these fields once on startup
                StartBatchTimerThread();

                eventLog.logEvent("Waiting for client setup files..."
                           , this.GetType().Name
                           , MessageType.Information
                           , MessageImportance.Essential);
                eventLog.logEvent("Waiting for batch files..."
                            , this.GetType().Name
                            , MessageType.Information
                            , MessageImportance.Essential);


            }
            catch (Exception e)
            {
                eventLog.logEvent("ICON Service Service started with errors : " + e.Message
                                , e.Source
                                , WFS.integraPAY.Online.Common.MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        protected override void OnStop()
        {
            if (m_tmBatchTimer != null)
            {
                m_tmBatchTimer.Dispose();
                m_tmBatchTimer = null;
            }

        }

        /// <summary>
        /// Start Timer Thread
        /// </summary>
        public void StartBatchTimerThread()
        {
            if (serverOptions.batchPollTimer > 0)
            {
                lock (this)
                {
                    if (m_tmBatchTimer != null)
                        return;
                    m_tmBatchTimer = new Timer();
                }
                m_tmBatchTimer.AutoReset = false;
                m_tmBatchTimer.Interval = serverOptions.batchPollTimer * 1000;
                m_tmBatchTimer.Enabled = true;
                m_tmBatchTimer.Elapsed += new ElapsedEventHandler(OnTimedBatchEvent);
            }
        }


        private void OnTimedBatchEvent(object sender, ElapsedEventArgs e)
        {
            bool bErrorExists = false;
            m_tmBatchTimer.Stop();
            //always process xml files first
            bErrorExists = ProcessClientInputFiles();
            if (!bErrorExists)
            {
                bErrorExists =  ProcessBatchInputFiles();
            }
            if (!bErrorExists)
            {
            
                if (m_tmBatchTimer != null)
                {
                    // now restart the timer
                    m_tmBatchTimer.Start();
                }
            }
            else
            {
                ShutDownService();
            }
        }



        #endregion

        private void ShutDownService() 
        {

            try
            {
                //shutting down service
                string exeName = Assembly.GetExecutingAssembly().Location;
                Assembly svcAssembly = Assembly.LoadFile(exeName);
                foreach (Type t in svcAssembly.GetTypes())
                {
                    if (t.BaseType == typeof(ServiceBase))
                    {
                        // Get default constructor
                        ConstructorInfo conInf = t.GetConstructor(new Type[] { });
                        if (conInf != null)
                        {
                            ServiceBase svcBase = conInf.Invoke(new object[] { }) as ServiceBase;
                            if (svcBase != null)
                            {
                                ServiceController sc = new ServiceController();
                                sc.ServiceName = svcBase.ServiceName;
                                sc.Stop();

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, "Shutdown ICON Service");
            }
        }

        #region ProcessClientInputFiles

        private bool ProcessClientInputFiles()
        {
            bool bErrorExists = false;
            string sClientFileBackup = string.Empty;
            try
            {

                StringBuilder sArchiveFile = new StringBuilder();
                string sClientFileShortName = string.Empty;
                string sShortenPathName = string.Empty;
                string sdatatype = string.Empty;
                string skeytypelen = string.Empty;
                string sClientID = string.Empty;
                string sShortName = string.Empty;
                string sSiteCode = string.Empty;
                string sFieldType = string.Empty;
                string skeytype = string.Empty;
                bool    bMarkSense = false;
                string sExtractType = string.Empty;
                bool    bImageRPSAliasMapping = false;
                int     iLockboxId  = 0;
                const int     iDocTypeIdentifier = (int)SetupExtractType.DocTypeIdentifier;
                bool bHasPermissionToDirectory = false;
                
                // look for XML files in the ClientInput folder
                List<string> directoryPath;
                directoryPath = DirectoryUtils.GetFiles(serverOptions.clientInputPath, "*.xml", _limitDirectoryPath, true, out bHasPermissionToDirectory);
                if (bHasPermissionToDirectory)
                {
                    foreach (string clientFile in directoryPath)
                    {
                        StringBuilder sbClientFile = new StringBuilder();
                        sClientFileBackup = clientFile;
                        sbClientFile.AppendFormat("{0}", Path.Combine(serverOptions.clientInputPath, clientFile));
                        eventLog.logEvent("Processing file " + sbClientFile.ToString()
                                        , this.GetType().Name
                                        , WFS.integraPAY.Online.Common.MessageType.Information
                                        , MessageImportance.Essential);

                        // get the client number, short name, and sitecode from the file name
                        sClientID = string.Empty;
                        sShortName = string.Empty;
                        sSiteCode = string.Empty;
                        sClientFileShortName = Path.GetFileName(sbClientFile.ToString());

                        if (GetClientInfo(sClientFileShortName, out sClientID, out sShortName, out sSiteCode))
                        {
                            // create Lockbox/DE XML and send to SSB via a stored procedure

                            // output file
                            XmlDocument xOutput = new XmlDocument();

                            // input file
                            XmlDocument xInput = new XmlDocument();
                            xInput.Load(sbClientFile.ToString());

                            // get the lcnum from the LifeCyle node
                            XmlNodeList LifeCycleNodeList = xInput.SelectNodes("/Report/LifeCycle");

                            string slcnum = string.Empty;
                            if (!Int32.TryParse(sClientID, out iLockboxId))
                            {
                                eventLog.logEvent("Error converting lockboxID in ProcessClientInputFiles. The lockbox ID is: " + sClientID.ToString()
                                    , this.GetType().Name
                                    , MessageType.Error
                                    , MessageImportance.Essential);
                            }          

                            foreach (XmlNode LifeCycleNode in LifeCycleNodeList)
                            {
                                slcnum = LifeCycleNode.Attributes["lcnum"].InnerText;

                                // create the Lockbox/DE XML file
                                /*
                                <ClientSetup>
                                    <Lockboxes>
                                        <Lockbox BankID="1" CustomerID="0" LockboxID="123" ShortName="Example" SiteCode="1000" /> 
                                    </Lockboxes>
                                    <DataEntryColumns>
                                        <DataEntryColumns FldName="RemitterName" ReportTitle="RemitterName" TableName="Checks" DataType="1" FldLength="60" ScreenOrder="0" /> 
                                        <DataEntryColumns FldName="InvoiceNumber" ReportTitle="InvoiceNumber" TableName="StubsDataEntry" DataType="1" FldLength="25" ScreenOrder="0" /> 
                                        <DataEntryColumns FldName="AuthorizationCode" ReportTitle="Authorization Code" TableName="StubsDataEntry" DataType="1" FldLength="2" ScreenOrder="1" /> 
                                    </DataEntryColumns>
                                </ClientSetup>
                                */

                                XmlNode ClientSetupNode = xOutput.CreateElement("ClientSetup");
                                xOutput.AppendChild(ClientSetupNode);

                                XmlNode LockboxesNode = xOutput.CreateElement("Lockboxes");
                                ClientSetupNode.AppendChild(LockboxesNode);

                                XmlNode LockboxNode = xOutput.CreateElement("Lockbox");
                                LockboxesNode.AppendChild(LockboxNode);

                                AddAttribute(xOutput, LockboxNode, "BankID", serverOptions.defaultBankID.ToString());

                                AddAttribute(xOutput, LockboxNode, "CustomerID", serverOptions.defaultCustomerID.ToString());

                                AddAttribute(xOutput, LockboxNode, "LockboxID", sClientID);

                                AddAttribute(xOutput, LockboxNode, "ShortName", sShortName.Length > 11 ? sShortName.Substring(0, 11) : sShortName); // Lockbox.ShortName is char[11]
                
                                //Add attribute to make long name the same as short name
                                AddAttribute(xOutput, LockboxNode, "LongName", sShortName); 
                               
                                AddAttribute(xOutput, LockboxNode, "SiteCode", sSiteCode);

                                // get all the AllItemtypeLCs for the lcnum
                                StringBuilder sSearchString = new StringBuilder();
                                sSearchString.AppendFormat("/Report/LifeCycle/AllItemtypeLCs/ItemTypeLC[@lcnum={0}]", slcnum);
                                XmlNodeList ItemTypeLCList = xInput.SelectNodes(sSearchString.ToString());

                                string sDocType = string.Empty;
                                string sitemtypenum = string.Empty;

                                // add the DataEntryColumns to the output XML
                                XmlNode DataEntryColumnsNode = xOutput.CreateElement("DataEntryColumns");
                                ClientSetupNode.AppendChild(DataEntryColumnsNode);

                                bool bIsCheck = false;

                                // create a list of keywords so they aren't duplicated
                                //List<string> listKeywords = new List<string>();
                                List<SetupField> listSetupFields = new List<SetupField>(); //CR 31663 1/6/2010 JNE

                                foreach (XmlNode ItemTypeLCNode in ItemTypeLCList)
                                {
                                    // get the itemtypenum
                                    sitemtypenum = ItemTypeLCNode.Attributes["itemtypenum"].InnerText;

                                    // get the itemtypename from the AllItemtypes node that match the itemtypenum
                                    sSearchString.Length = 0;
                                    sSearchString.AppendFormat("/Report/LifeCycle/AllItemtypes/ItemType[@itemtypenum={0}]", sitemtypenum);

                                    XmlNodeList ItemTypeList = xInput.SelectNodes(sSearchString.ToString());

                                    string sitemtypename = string.Empty;

                                    foreach (XmlNode ItemTypeNode in ItemTypeList)
                                    {
                                        sitemtypename = ItemTypeNode.Attributes["itemtypename"].InnerText;
                                        if (ItemTypeNode.Attributes["doctype"] != null)
                                        {
                                             sDocType = ItemTypeNode.Attributes["doctype"].InnerText;
                                        }
                                    }
                    
                                    if (String.IsNullOrEmpty(sDocType))
                                    {
                                        //else old logic replace when all setup file go to new way
                                        if (sitemtypename.Contains("Per/Bus Check"))
                                            bIsCheck = true;
                                        else
                                            bIsCheck = false;
                                    }
                                    else if (sDocType == "2")
                                    {
                                        bIsCheck = true;
                                        _ICONBll.InsertImageRPSAliasMapping(serverOptions.defaultBankID, iLockboxId,
                                                            iDocTypeIdentifier.ToString(), string.Empty, sDocType, sitemtypename);
                                    }
                                    else if (sDocType == "1")
                                    {
                                        bIsCheck = false;
                                        _ICONBll.InsertImageRPSAliasMapping(serverOptions.defaultBankID, iLockboxId,
                                                             iDocTypeIdentifier.ToString(), string.Empty, sDocType, sitemtypename);
                                    }
                                    else
                                    {
                                        eventLog.logEvent("Recevied a docType which is not expected type. DocType is: " + sDocType.ToString()
                                               , this.GetType().Name
                                               , MessageType.Error
                                               , MessageImportance.Essential);
                                    }
                                    // get all the ItemtypeexKeyword for this itemtype (keytype, seqnum)
                                    sSearchString.Length = 0;
                                    sSearchString.AppendFormat("/Report/LifeCycle/AllItemtypexKeywords/ItemtypexKeyword[@itemtype={0}]", sitemtypenum);

                                    XmlNodeList ItemtypeexKeywordList = xInput.SelectNodes(sSearchString.ToString());

                                    skeytype = string.Empty;

                                    foreach (XmlNode ItemtypexKeywordNode in ItemtypeexKeywordList)
                                    {
                                        skeytype = ItemtypexKeywordNode.Attributes["keytype"].InnerText;

                                        // get all the Keytype nodes for the keytype
                                        sSearchString.Length = 0;
                                        sSearchString.AppendFormat("/Report/LifeCycle/AllKeytypes/Keytype[@keytypenum={0}]", skeytype);

                                        XmlNodeList KeyTypeNodeList = xInput.SelectNodes(sSearchString.ToString());

                                        sdatatype = string.Empty;
                                        skeytypelen = string.Empty;                                

                                        foreach (XmlNode KeytypeNode in KeyTypeNodeList)
                                        {
                                            skeytype = KeytypeNode.Attributes["keytype"].InnerText;
                                            sdatatype = KeytypeNode.Attributes["datatype"].InnerText;
                                            skeytypelen = KeytypeNode.Attributes["keytypelen"].InnerText;
                                            

                                            ///need to add this check since the fieldType might or might not exists
                                            if (KeytypeNode.Attributes["fieldtype"] == null) {
                                                sFieldType = string.Empty;
                                                bMarkSense = false;
                                            } else {
                                                sFieldType = KeytypeNode.Attributes["fieldtype"].InnerText;
                                                bMarkSense = (sFieldType == cICONBll.GetDescription(SetupFieldType.MarkSenseField)) ? true : false;
                                            }
                                           
                                            if (KeytypeNode.Attributes["extracttype"] == null) {
                                                sExtractType = string.Empty;
                                            } else { 
                                                sExtractType = KeytypeNode.Attributes["extracttype"].InnerText;
                                            }


                                            if (!String.IsNullOrEmpty(sExtractType) || !String.IsNullOrEmpty(sFieldType)
                                                || !String.IsNullOrEmpty(sDocType) && !_ICONBll.IsStandardImageRPSKeyword(skeytype))
                                            {
                                                //we have a field 
                                                bImageRPSAliasMapping = _ICONBll.InsertImageRPSAliasMapping(serverOptions.defaultBankID, iLockboxId,
                                                        sExtractType, sFieldType, sDocType, skeytype);
                                            }
            
                                    
                                            // disregard the standard keywords
                                            // CR 29756 MEH 06-03-2010 don't send Checks.Amount ("Applied Amount")
                                            // "Applied Amount" was taken out of the list of Standard ImageRPS keywords
                                            if (!bImageRPSAliasMapping && !_ICONBll.IsStandardImageRPSKeyword(skeytype)
                                                    && !IsAmountField(bIsCheck, skeytype, sExtractType, sFieldType))
                                                    
                                            {
                                                SetupField myField = new SetupField(GetFieldName(bIsCheck,skeytype),GetDataEntryTableName(bIsCheck, skeytype)); //CR 31663 1/6/2010 JNE
                                                // check that the keyword isn't in the list before adding it to the XML
                                                //if (!listKeywords.Contains(GetFieldName(bIsCheck, skeytype)))
                                                if (!listSetupFields.Contains(myField)) //CR 31663 1/6/2010 JNE
                                                {
                                                    // add to the output XML
                                                    XmlNode DataEntryColumnNode = xOutput.CreateElement("DataEntryColumns");
                                                    DataEntryColumnsNode.AppendChild(DataEntryColumnNode);

                                                    AddAttribute(xOutput, DataEntryColumnNode, "FldName", GetFieldName(bIsCheck, skeytype));

                                                    AddAttribute(xOutput, DataEntryColumnNode, "ReportTitle", skeytype);

                                                    AddAttribute(xOutput, DataEntryColumnNode, "TableName", GetDataEntryTableName(bIsCheck, skeytype));

                                                    AddAttribute(xOutput, DataEntryColumnNode, "DataType", GetOLTADataType(Convert.ToInt32(sdatatype)));

                                                    AddAttribute(xOutput, DataEntryColumnNode, "FldLength", skeytypelen);

                                                    //WJS CR 33568 3/29/2011 always send a screen order of zero
                                                    AddAttribute(xOutput, DataEntryColumnNode, "ScreenOrder", "0");

                                                    AddAttribute(xOutput, DataEntryColumnNode, "MarkSense", bMarkSense ? "1" : "0");

                                                    //listKeywords.Add(GetFieldName(bIsCheck, skeytype));
                                                    // add it to the list of SetupFields
                                                    listSetupFields.Add(myField); //CR 31663 1/6/2010 JNE
                                                }
                                            }                                    
                                        }
                                    }
                                }
                            }

                            // call the stored procedure to make an entry in the ICON Client Setup queue
                            eventLog.logEvent("Calling stored procedure to insert ICON Client Setup XML in SSB queue"
                                       , this.GetType().Name
                                       , WFS.integraPAY.Online.Common.MessageType.Information
                                       , MessageImportance.Verbose);
                            _ICONBll.InsertICONClientSetup(GetXMLString(xOutput));
                                        
                            
                            // if debug, save the file
    #if (DEBUG)
                            // Save the document to a file and auto-indent the output.
                            StringBuilder sbTempXMLFile = new StringBuilder();
                            sbTempXMLFile.AppendFormat("Client_{0}_{1}_{2}.oxi", sClientID, DateTime.Now.ToString("M-d-yyy_HH_mm_ss"), DateTime.Now.Millisecond);
                            using (XmlTextWriter writer = new XmlTextWriter(Path.Combine(serverOptions.clientInputPath, sbTempXMLFile.ToString()), null))
                            {
                                writer.Formatting = Formatting.Indented;
                                xOutput.Save(writer);
                                writer.Close();
                            }
    #endif
                        }

                        if(serverOptions.clientArchivePath.Length > 0) {
                            // zip up the file
                            CreateZipArchiveFile(sbClientFile.ToString(), serverOptions.clientArchivePath, "Client Archive");
                        } else {
                            eventLog.logEvent("ICON Client Archive path option disabled.  Skipping Client Archive step.", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
                        }

                       
                        // remove the .DAT file
                        eventLog.logEvent("Deleting the file: " + sbClientFile.ToString()
                                    , this.GetType().Name
                                    , MessageType.Information
                                    , MessageImportance.Verbose);

                        File.Delete(sbClientFile.ToString());
                    }
                } //end of if
                else
                {
                    eventLog.logEvent("You do not have permission to the directory: " + serverOptions.clientInputPath + "."
                                   , this.GetType().Name
                                   , MessageType.Information
                                   , MessageImportance.Essential);
                    bErrorExists = true;
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in ProcessClientInputFiles : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
                _ICONBll.MoveToErrorFolder(sClientFileBackup, serverOptions.errorFolderPath);
            }

            return bErrorExists;
        }

        private void CreateZipArchiveFile(string FileNameToZip, string ArchiveFolderPath, string ArchiveDescription) {
 
            string strZipFolderPath;
            StringBuilder sbZipFilePath;
            bool bolContinue;
            string strFileNameWithoutExtension;

            try {
                if(Directory.Exists(ArchiveFolderPath)) {
                    bolContinue = true;
                } else {
                    try {
                        Directory.CreateDirectory(ArchiveFolderPath);
                        eventLog.logEvent("Created ICON " + ArchiveDescription + " folder: " + ArchiveFolderPath, this.GetType().Name, MessageType.Information, MessageImportance.Essential);
                        bolContinue = true;
                    } catch(Exception ex) {
                        eventLog.logEvent("ICON " + ArchiveDescription + " folder path does not exist.  Application was unable to create folder [" + serverOptions.clientArchivePath + "]: " + ex.Message + ";  Skipping Client Archive zip.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        bolContinue = false;
                        ShutDownService();
                    }
                }

                if(bolContinue) {
                    strZipFolderPath = Path.Combine(ArchiveFolderPath, DateTime.Now.ToString("yyyyMMdd"));
                    if(Directory.Exists(strZipFolderPath)) {
                        bolContinue = true;
                    } else {
                        try {
                            Directory.CreateDirectory(strZipFolderPath);
                            eventLog.logEvent("Created ICON " + ArchiveDescription + " folder: " + strZipFolderPath, this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
                            bolContinue = true;
                        } catch(Exception ex) {
                            eventLog.logEvent("Application was unable to create ICON " + ArchiveDescription + " folder path [" + strZipFolderPath + "]: " + ex.Message + ";  Skipping Client Archive zip.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                            bolContinue = false;
                        }
                    }

                    if(bolContinue) {
                        sbZipFilePath = new StringBuilder();
                        strFileNameWithoutExtension = Path.GetFileNameWithoutExtension(FileNameToZip) + ".zip";
                        sbZipFilePath.Append(Path.Combine(strZipFolderPath, strFileNameWithoutExtension));

                        _ICONBll.ZipFile(FileNameToZip, sbZipFilePath.ToString());
                    }
                }
            } catch(Exception ex) {
                eventLog.logEvent("Exception occurred attempting to create ICON " + ArchiveDescription + " file [" + FileNameToZip + "]: " + ex.Message + ";  Skipping Client Archive zip.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            }
        }

        private bool IsAmountField(bool bIsCheck, string skeytype, string sExtractType, string sFieldType)
        {
            bool bAmountField = false;
            if (bIsCheck && skeytype == "Applied Amount")
            {
                bAmountField = true;
            }
            else if (!bIsCheck && IsStubAccountOrAmount(skeytype, sFieldType))
            {
                bAmountField = true;
            }
            return bAmountField;
        }
        private string GetXMLString(XmlDocument myDoc)
        {
            string returnString = string.Empty;
            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter xw = new XmlTextWriter(sw))
                {
                    myDoc.WriteTo(xw);
                }
                returnString = sw.ToString();
            }
            return returnString;
        }

        private bool GetClientInfo(string sClientFileName, out string sClientID, out string sShortName, out string sSiteCode)
        {
            bool bRet = true;

            sClientID = string.Empty;
            sShortName = string.Empty;
            sSiteCode = string.Empty;

            try
            {
                // get the ClientID, ShortName, and SiteCode from the client file name
                // file name ="Client21_212121_7000_20091109_134545.XML" 
                // 21 = ClientID
                // 212121 = ShortName
                // 7000 = SiteCode

                string[] myTokens = sClientFileName.Split('_');

                if(myTokens.GetUpperBound(0) == 4)
                {
                    sClientID = myTokens[0].Remove(0, 6);
                    sShortName = myTokens[1];
                    sSiteCode = myTokens[2];
                }   
                else
                {
                    eventLog.logEvent("GetClientInfo file name error!" + "\nFile name: " + sClientFileName + "\nExpected format is: ClientXXX_SHORTNAME_SITECODE_YYYYMMDD_HHMMSS.XML"
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                    bRet = false;
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in GetClientInfo : " + ex.Message + "\nFile name: " + sClientFileName + "\nExpected format is: ClientXXX_SHORTNAME_SITECODE_YYYYMMDD_HHMMSS.XML"
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                bRet = false;
            }

            return bRet;            
        }

        private string GetFieldName(bool bIsCheck, string skeytype)
        {
            string sRet = skeytype;

            try
            {
                if (skeytype.CompareTo("Amount") == 0 || skeytype.CompareTo("Applied Amount") == 0)
                {
                    sRet = "Amount";
                }
                else if (skeytype.CompareTo("Account") == 0 || skeytype.CompareTo("Account Number") == 0)
                {
                    if (bIsCheck)
                    {
                        sRet = "Account";
                    }
                    else
                    {
                        sRet = "AccountNumber";
                    }
                   
                }
              
                //removal all spaces

                sRet= sRet.Replace(" ","");
            }
            catch (System.Exception ex)
            {
            	eventLog.logEvent("Exception in GetFieldName : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }            

            return sRet;
        }

        private string GetOLTADataType(int iDataType)
        {
            int iOLTADataType = -1;

            try
            {
                /*
                ImageRPS Enum	Data Type	    integraPAY Data Type
                1	            Numeric 20      6
                2	            Alphanumeric    1
                3	            Currency        7
                4	            Date            11
                5               Numeric?        6?
                6	            Numeric 9       6
                9	            Date/Time       11
                10	            AlphaNumeric    1
                */

                switch(iDataType)
                {
                    case 1:
                    case 5:
                    case 6:
                        iOLTADataType = 6;
                        break;
                    case 2:
                    case 10:
                        iOLTADataType = 1;
                        break;
                    case 3:
                        iOLTADataType = 7;
                        break;
                    case 4:
                    case 9:
                        iOLTADataType = 11;
                        break;
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in GetOLTADataType : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }

            return iOLTADataType.ToString();
        }

        private string GetDataEntryTableName(bool bIsCheck, string sKeyword)
        {
            string sRet = string.Empty;

            try
            {
                if(bIsCheck)
                {
                    // CR 29756 MEH 05-20-2010 ICON Service does not create check fields (Account, RT, and Serial, etc.) in the correct table
                    if (sKeyword.CompareTo("RemitterName") == 0 || sKeyword.CompareTo("Applied Amount") == 0 || sKeyword.CompareTo("RT") == 0 || sKeyword.CompareTo("Account") == 0 || sKeyword.CompareTo("Serial") == 0 || sKeyword.CompareTo("Transaction Code") == 0)
                        sRet = "Checks";
                    else
                        sRet = "ChecksDataEntry";
                }
                else
                {
                    if (sKeyword.CompareTo("Account") == 0 || sKeyword.CompareTo("Account Number") == 0 || sKeyword.CompareTo("Amount") == 0 || sKeyword.CompareTo("Applied Amount") == 0)
                        sRet = "Stubs";
                    else
                        sRet = "StubsDataEntry";
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in GetDataEntryTableName : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }

            return sRet;
        }

       
        #endregion

        #region ProcessBatchInputFiles

        private bool ProcessBatchInputFiles()
        {
            string sBatchFileBackup = string.Empty;
            bool bErrorExists = false;
            try
            {
                string sShortenPathName = string.Empty;
                StringBuilder sArchiveFile = new StringBuilder();
                StringBuilder sBatchFilePath = new StringBuilder();
                List<IMSDocument> listIMSDocuments =  null;
                List<string> directoryPath;
                bool bHasPermissionToDirectory = false;
                // look for DAT files in the ClientInput folder
                directoryPath = DirectoryUtils.GetFiles(serverOptions.batchInputPath, "*.dat", _limitDirectoryPath, true, out bHasPermissionToDirectory);
                if (bHasPermissionToDirectory)
                {
                    foreach (string batchFile in directoryPath)
                    {
                        //CR 33841 WJS - need to take a backup in case it errors out
                        sBatchFileBackup = batchFile;
                        sArchiveFile.Length = 0;
                        sBatchFilePath.Length = 0;
                        sBatchFilePath.AppendFormat("{0}", Path.Combine(serverOptions.batchInputPath, batchFile));


                        eventLog.logEvent("Processing file " + batchFile
                                        , this.GetType().Name
                                        , MessageType.Information
                                        , MessageImportance.Essential);

                        // read the .DAT file line by line
                        _ICONBll.ReadDATFile(batchFile, serverOptions.defaultBankID, out listIMSDocuments);


                        // now construct two XML documents with the list of IMSDocuments
                        BuildXMLDocuments(listIMSDocuments);


                        if (serverOptions.batchArchivePath.Length > 0)
                        {
                            // zip up the .DAT file and put it in the batch archive folder
                            CreateZipArchiveFile(sBatchFilePath.ToString(), serverOptions.batchArchivePath, "Batch Archive");
                        }
                        else
                        {
                            eventLog.logEvent("ICON Batch Archive path option disabled.  Skipping Batch Archive step.", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
                        }

                        // remove the .DAT file
                        File.Delete(batchFile);
                    }
                     
                }
                else
                {
                    eventLog.logEvent("You do not have permission to the directory: " + serverOptions.batchInputPath + "."
                                    , this.GetType().Name
                                    , MessageType.Information
                                    , MessageImportance.Essential);
                    bErrorExists = true;
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in ProcessBatchInputFiles : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
                _ICONBll.MoveToErrorFolder(sBatchFileBackup, serverOptions.errorFolderPath);
            }
            return bErrorExists;
        }

        
       
        // SSB Batch Import XML
        /*                    
        <Batches>
            <Batch GlobalBatchID="" BankID="7000" LockboxID="505" CustomerID="0" SiteID="1" BatchID="1000" DESetupID="" DepositStatus="850" DepositDDA="" DepositStatusDisplayName="Complete" SystemType="2" ProcessingDate="2005-12-02 00:00:00" DepositDate="2005-12-02 08:00:00">
                <Transaction GlobalBatchID="" TransactionID="1" Sequence="1">
                    <Checks GlobalBatchID="" GlobalCheckID="" TransactionID="1" TransactionSequence="1" BatchSequence="1" CheckSequence="1" Serial="7415" RT="053900225" Account="869052965" RemitterName="bill seymore" Amount="20.0000" /> 
                    <Documents GlobalBatchID="" GlobalDocumentID="" TransactionID="1" TransactionSequence="1" BatchSequence="3" DocumentSequence="1" FileDescriptor="MI" /> 
                    <Stubs GlobalBatchID="" GlobalStubID="" TransactionID="1" TransactionSequence="3" BatchSequence="5" StubSequence="1" AccountNumber="" Amount="10.05" />
                </Transaction>
                <DataEntry>
                    <DataEntry GlobalID="" GlobalBatchID="" TransactionID="1" BatchSequence="1" FldName="RemitterName" ReportTitle="RemitterName" FldDataTypeEnum="" FldLength="" ScreenOrder="" TableName="Checks" DataEntryValue="bill seymore" /> 
                    <DataEntry GlobalID="" GlobalBatchID="" TransactionID="1" BatchSequence="1" FldName="CheckDate" ReportTitle="Check Date" FldDataTypeEnum="" FldLength="" ScreenOrder="" TableName="ChecksDataEntry" DataEntryValue="10/1/2009" /> 
                    <DataEntry GlobalID="" GlobalBatchID="" TransactionID="2" BatchSequence="2" FldName="InvoiceNumber" ReportTitle="Invoice Number" FldDataTypeEnum="" FldLength="" ScreenOrder="" TableName="Stubs" DataEntryValue="123456" /> 
                    <DataEntry GlobalID="" GlobalBatchID="" TransactionID="2" BatchSequence="3" FldName="MiscAmount" ReportTitle="Misc Amount" FldDataTypeEnum="" FldLength="" ScreenOrder="" TableName="StubsDataEntry" DataEntryValue="100.00" /> 
                </DataEntry>
            </Batch>
        </Batches>

        // IMSInterfaceQueue XML
        <Batches>
            <Batch ProcessingDate="2009-12-02 00:00:00" DepositDate="2009-12-02 08:00:00" BankID="7000" LockboxID="505" BatchID="1000" SystemType="2" SiteID="1">
                <Transaction TransactionID="1">
                    <Checks TransactionID="1" BatchSequence="1" DocDate="04/17/2009" Serial="7415" RT="053900225" Account="869052965" Amount="20.0000" DocTypeName="Per/Bus Check" Pages="FB=4875,RB=2587" />
                        <Image Path="\\path\to\image\0000001.tif "/>
                    </Checks>
                    <Documents TransactionID="1" BatchSequence="3" DocDate="04/17/2009" DocTypeName="Invoice" Pages="FB=12365"/> 
                        <Image Path="\\path\to\image\0000002.tif "/>
                    </Documents>
                </Transaction>
            </Batch>
        </Batches>
        */
        private void BuildXMLDocuments(List<IMSDocument> listIMSDocuments)
        {
            try
            {
                bool bSuccess = true;
                int iBatchID = -1;
                int iLockboxID = -1;
                int iBatchSequence = -1;
                int iTransactionID = -1;
                int iTransactionSequence = 1;
                int iCheckSequence = 1;
                int iDocumentSequence = 1;
                int iStubSequence = 1;
                int iBatchSequenceForDocuments = GetNextBatchSequence(listIMSDocuments);

                XmlNode TransactionNode = null;
            
                XmlDocument xOutput = null;
                // for IMS
                XmlDocument xIMSOutput = null;
                int iIMSTransactionID = -1;
                XmlNode IMSTransactionNode = null;
                XmlNode IMSBatchNode = null;

                bool bolDocumentExists;
                bool bolDataEntryExists;
                bool bolDoCreateStub;

                Dictionary<int, cBatchData> dicBatchData;
                string strBatchKey1 = string.Empty;
                string strBatchKey2 = string.Empty;
                bool bolDoBatchDataInsert;

                // SSB Batch Import start
                // output file for SSB Batch Import
                xOutput = new XmlDocument();

                // create the Batches node
                XmlNode BatchesNode = xOutput.CreateElement("Batches");
                xOutput.AppendChild(BatchesNode);

                // create the Batch node under the batches node
                XmlNode BatchNode = xOutput.CreateElement("Batch");
                BatchesNode.AppendChild(BatchNode);

                // create the DataEntry node under the Batch node
                XmlNode DataEntryNode = xOutput.CreateElement("DataEntry");
                BatchNode.AppendChild(DataEntryNode);

                // add the Batch attributes

                // GlobalBatchID
                AddAttribute(xOutput, BatchNode, "GlobalBatchID", "");
                // BankID
                AddAttribute(xOutput, BatchNode, "BankID", serverOptions.defaultBankID.ToString());
                // CustomerID
                AddAttribute(xOutput, BatchNode, "CustomerID", serverOptions.defaultCustomerID.ToString());
                // LockboxID
                AddAttribute(xOutput, BatchNode, "LockboxID", _ICONBll.GetLockboxID(listIMSDocuments));
                // SiteID
                AddAttribute(xOutput, BatchNode, "SiteID", _ICONBll.GetSiteID(listIMSDocuments));
                // BatchID
                AddAttribute(xOutput, BatchNode, "BatchID", _ICONBll.GetBatchID(listIMSDocuments));
                // DESetupID
                AddAttribute(xOutput, BatchNode, "DESetupID", "");
                // DepositStatus
                AddAttribute(xOutput, BatchNode, "DepositStatus", "850");
                // DepositDDA
                AddAttribute(xOutput, BatchNode, "DepositDDA", "");
                // DepositStatusDisplayName
                AddAttribute(xOutput, BatchNode, "DepositStatusDisplayName", "Complete");
                // SystemType
                AddAttribute(xOutput, BatchNode, "SystemType", "2");
                // SystemDate
                AddAttribute(xOutput, BatchNode, "ProcessingDate", _ICONBll.GetSystemDate(listIMSDocuments).ToString( "yyyy-MM-dd 00:00:00"));

                // DepositDate
                AddAttribute(xOutput, BatchNode, "DepositDate", _ICONBll.GetDepositDate(listIMSDocuments).ToString("yyyy-MM-dd 00:00:00"));

                // SourceProcessingDate
                AddAttribute(xOutput, BatchNode, "SourceProcessingDate", _ICONBll.GetProcessingDate(listIMSDocuments).ToString("yyyy-MM-dd 00:00:00"));
                // Batch Source
                AddAttribute(xOutput, BatchNode, "BatchSource", "ICON");
    
                // SSB Batch Import end

                // IMSInterfaceQueue start
                if (IPOnlineOptions.imageStorageMode == ImageStorageMode.HYLAND)
                {
                    // create Image Import Service XML
                    xIMSOutput = new XmlDocument();

                    // create the Batches node
                    XmlNode IMSBatchesNode = xIMSOutput.CreateElement("Batches");
                    xIMSOutput.AppendChild(IMSBatchesNode);

                    // create the Batch node under the batches node
                    IMSBatchNode = xIMSOutput.CreateElement("Batch");
                    IMSBatchesNode.AppendChild(IMSBatchNode);

                    // add the batch attributes
                    // ProcessingDate
                    AddAttribute(xIMSOutput, IMSBatchNode, "ProcessingDate", _ICONBll.GetSystemDate(listIMSDocuments).ToString("yyyy-MM-dd 00:00:00"));

                    // DepositDate
                    AddAttribute(xIMSOutput, IMSBatchNode, "DepositDate", _ICONBll.GetDepositDate(listIMSDocuments).ToString("yyyy-MM-dd 00:00:00"));
                    // SourceProcessingDate
                    AddAttribute(xIMSOutput, IMSBatchNode, "SourceProcessingDate", _ICONBll.GetProcessingDate(listIMSDocuments).ToString("yyyy-MM-dd 00:00:00"));

                    // BankID
                    AddAttribute(xIMSOutput, IMSBatchNode, "BankID", serverOptions.defaultBankID.ToString());
                    // BankID
                    AddAttribute(xIMSOutput, IMSBatchNode, "CustomerID", serverOptions.defaultCustomerID.ToString());
                    // LockboxID
                    AddAttribute(xIMSOutput, IMSBatchNode, "LockboxID", _ICONBll.GetLockboxID(listIMSDocuments));
                    // BatchID
                    AddAttribute(xIMSOutput, IMSBatchNode, "BatchID", _ICONBll.GetBatchID(listIMSDocuments));
                    // SystemType
                    AddAttribute(xIMSOutput, IMSBatchNode, "SystemType", "2");
                    // SiteID
                    AddAttribute(xIMSOutput, IMSBatchNode, "SiteID", _ICONBll.GetSiteID(listIMSDocuments));
                }
                // IMSInterfaceQueue end

                dicBatchData = new Dictionary<int,cBatchData>();
                bolDoBatchDataInsert = false;

                // loop through the documents
                foreach (IMSDocument myOutputDocument in listIMSDocuments)
                {
                    bolDocumentExists = false;
                    bolDataEntryExists = false;
                    bolDoBatchDataInsert = false;

                    if (Int32.TryParse(myOutputDocument.BatchSequence, out iBatchSequence))
                    {
                        if (Convert.ToInt32(myOutputDocument.TransactionID) != iTransactionID)
                        {
                            // SSB Batch Import start
                            iTransactionID = Convert.ToInt32(myOutputDocument.TransactionID);
                          
                            XmlNodeList nlTransactions = xOutput.SelectNodes("Batches/Batch/Transaction[@TransactionID = '" + iTransactionID + "']");
                            if (nlTransactions.Count > 0)
                            {
                                TransactionNode = nlTransactions[0];
                            }
                            else
                            {

                                // create the Transaction node
                                TransactionNode = xOutput.CreateElement("Transaction");
                                BatchNode.AppendChild(TransactionNode);
                            }

                            // add the transaction attributes

                            // GlobalBatchID 
                            AddAttribute(xOutput, TransactionNode, "GlobalBatchID", "");
                            // TransactionID
                            AddAttribute(xOutput, TransactionNode, "TransactionID", myOutputDocument.TransactionID);
                            // Sequence (same as TransactionID)
                            AddAttribute(xOutput, TransactionNode, "Sequence", myOutputDocument.TransactionID);

                            iTransactionSequence = 1;
                            iCheckSequence = 1;
                            iDocumentSequence = 1;
                            // SSB Batch Import end

                            // IMSInterfaceQueue start
                            if (IPOnlineOptions.imageStorageMode == ImageStorageMode.HYLAND)
                            {
                                iIMSTransactionID = Convert.ToInt32(myOutputDocument.TransactionID);

                                // create the Transaction node
                                IMSTransactionNode = xIMSOutput.CreateElement("Transaction");
                                IMSBatchNode.AppendChild(IMSTransactionNode);

                                // add the transaction attributes
                                // TransactionID
                                AddAttribute(xIMSOutput, IMSTransactionNode, "TransactionID", myOutputDocument.TransactionID);
                            }
                            // IMSInterfaceQueue end
                        }

                        if (myOutputDocument.IsCheck)
                        {
                            // SSB Batch Import start
                            // add check node
                            XmlNode CheckNode = xOutput.CreateElement("Checks");
                            TransactionNode.AppendChild(CheckNode);

                            // add the check attributes

                            // GlobalBatchID
                            AddAttribute(xOutput, CheckNode, "GlobalBatchID", "");
                            // GlobalDocumentID
                            AddAttribute(xOutput, CheckNode, "GlobalCheckID", "");
                            // TransactionID
                            AddAttribute(xOutput, CheckNode, "TransactionID", myOutputDocument.TransactionID);
                            // TransactionSequence
                            AddAttribute(xOutput, CheckNode, "TransactionSequence", iTransactionSequence.ToString());

                            iTransactionSequence++;

                            // BatchSequence
                            AddAttribute(xOutput, CheckNode, "BatchSequence", myOutputDocument.BatchSequence);

                            // CheckSequence
                            AddAttribute(xOutput, CheckNode, "CheckSequence", iCheckSequence.ToString());

                            iCheckSequence++;


                            // RemitterName
                            AddAttribute(xOutput, CheckNode, "RemitterName", myOutputDocument.RemitterName);

                            AddCheckInfo(xOutput, myOutputDocument.MICRFields, CheckNode);

                            AddDataEntryInfo(myOutputDocument, xOutput, DataEntryNode, Convert.ToInt32(myOutputDocument.BatchSequence), true);
                           

                            // IMSInterfaceQueue start
                            if (IPOnlineOptions.imageStorageMode == ImageStorageMode.HYLAND)
                            {
                                // add check node
                                XmlNode IMSCheckNode = xIMSOutput.CreateElement("Checks");
                                IMSTransactionNode.AppendChild(IMSCheckNode);

                                // add the check attributes

                                // TransactionID
                                AddAttribute(xIMSOutput, IMSCheckNode, "TransactionID", myOutputDocument.TransactionID);
                                // BatchSequence
                                AddAttribute(xIMSOutput, IMSCheckNode, "BatchSequence", myOutputDocument.BatchSequence);
                                // DocDate
                                AddAttribute(xIMSOutput, IMSCheckNode, "DocDate", myOutputDocument.DocDate);

                                AddCheckInfo(xIMSOutput, myOutputDocument.MICRFields, IMSCheckNode);

                                
                                // DocTypeName
                                AddAttribute(xIMSOutput, IMSCheckNode, "DocTypeName", myOutputDocument.DocTypeName);
                                // SiteID
                                AddAttribute(xIMSOutput, IMSCheckNode, "SiteID", myOutputDocument.SiteID);
                                // Pages
                                AddAttribute(xIMSOutput, IMSCheckNode, "Pages", myOutputDocument.GetPages());

                                // add an Image node
                                XmlNode IMSImageNode = xIMSOutput.CreateElement("Image");
                                IMSCheckNode.AppendChild(IMSImageNode);

                                // add Image attributes

                                // Path
                                AddAttribute(xIMSOutput, IMSImageNode, "Path", myOutputDocument.FullPath);
                            }
                            // IMSInterfaceQueue end
                        }
                        else
                        {
                            if (myOutputDocument.FullPath != null && myOutputDocument.FullPath.Length > 0)
                            {
                                bolDocumentExists = true;
                            
                                // SSB Batch Import start
                                // add document nodes
                                XmlNode DocumentNode = xOutput.CreateElement("Documents");
                                TransactionNode.AppendChild(DocumentNode);

                                // GlobalBatchID
                                AddAttribute(xOutput, DocumentNode, "GlobalBatchID", "");
                                // GlobalDocumentID
                                AddAttribute(xOutput, DocumentNode, "GlobalDocumentID", "");
                                // TransactionID
                                AddAttribute(xOutput, DocumentNode, "TransactionID", myOutputDocument.TransactionID);
                                // TransactionSequence
                                AddAttribute(xOutput, DocumentNode, "TransactionSequence", iTransactionSequence.ToString());

                                iTransactionSequence++;

                                // BatchSequence
                                AddAttribute(xOutput, DocumentNode, "BatchSequence", iBatchSequenceForDocuments.ToString());
                                myOutputDocument.BatchSequenceForDocuments = iBatchSequenceForDocuments;

                                // DocumentSequence
                                AddAttribute(xOutput, DocumentNode, "DocumentSequence", iDocumentSequence.ToString());

                                iDocumentSequence++;
                                // FileDescriptor
                                AddAttribute(xOutput, DocumentNode, "FileDescriptor", _ICONBll.GetFileDescriptor(myOutputDocument.DocTypeName));

                                // check for data entry besides StubAccount and StubAmount

                                bolDataEntryExists = AddDataEntryInfo(myOutputDocument, xOutput, DataEntryNode, Convert.ToInt32(myOutputDocument.BatchSequence), false);

                            
                                // SSB Batch Import end

                                // IMSInterfaceQueue start
                                if (IPOnlineOptions.imageStorageMode == ImageStorageMode.HYLAND)
                                {
                                    // add document nodes
                                    XmlNode IMSDocumentNode = xIMSOutput.CreateElement("Documents");
                                    IMSTransactionNode.AppendChild(IMSDocumentNode);

                                    // add the document attributes
                                    // TransactionID
                                    AddAttribute(xIMSOutput, IMSDocumentNode, "TransactionID", myOutputDocument.TransactionID);
                                    // BatchSequence
                                    AddAttribute(xIMSOutput, IMSDocumentNode, "BatchSequence", iBatchSequenceForDocuments.ToString() );
                                    // DocDate
                                    AddAttribute(xIMSOutput, IMSDocumentNode, "DocDate", myOutputDocument.DocDate);
                                    // DocTypeName
                                    AddAttribute(xIMSOutput, IMSDocumentNode, "DocTypeName", myOutputDocument.DocTypeName);
                                    // FileDescriptor
                                    AddAttribute(xIMSOutput, IMSDocumentNode, "FileDescriptor", _ICONBll.GetFileDescriptor(myOutputDocument.DocTypeName));
                                    // SiteID
                                    AddAttribute(xIMSOutput, IMSDocumentNode, "SiteID", myOutputDocument.SiteID);
                                    // Pages
                                    AddAttribute(xIMSOutput, IMSDocumentNode, "Pages", myOutputDocument.GetPages());

                                    // add an Image node
                                    XmlNode IMSImageNode = xIMSOutput.CreateElement("Image");
                                    IMSDocumentNode.AppendChild(IMSImageNode);

                                    // add Image attributes

                                    // Path
                                    AddAttribute(xIMSOutput, IMSImageNode, "Path", myOutputDocument.FullPath);

                                  
                                }

                                // IMSInterfaceQueue end
                            }
                            else  // this has to be data entry
                            {
                                bolDataEntryExists = AddDataEntryInfo(myOutputDocument, xOutput, DataEntryNode, Convert.ToInt32(myOutputDocument.BatchSequence), false);
                            }

                            // add Stubs node if StubAccount is available
                            if(myOutputDocument.MICRFields.StubAccount != null) {
                                bolDoCreateStub = true;
                            // add Stubs node if StubAmount is available
                            } else if(myOutputDocument.MICRFields.StubAmount != null) {
                                bolDoCreateStub = true;
                            // add Stubs node if item contains both Data Entry and an associated Document.
                            } else if(bolDocumentExists && bolDataEntryExists){
                                bolDoCreateStub = true;
                            } else {
                                bolDoCreateStub = false;
                            }
                        
                            if (bolDoCreateStub)
                            {
                                AddStubNode(xOutput, TransactionNode, myOutputDocument, iTransactionSequence, iStubSequence, myOutputDocument.BatchSequence, iBatchSequenceForDocuments);
                               
                                iTransactionSequence++;
                                iStubSequence++;
                            }

                            iBatchSequenceForDocuments++;
                        }

                        //CR 31536 WJS 3/7/2011 Call insert factItemData and factBatchData directly
                        eventLog.logEvent(" Calling BLL to insert ICON factItemData and factBatchData"
                                 , this.GetType().Name
                                 , WFS.integraPAY.Online.Common.MessageType.Information
                                 , MessageImportance.Debug);

                        if (!Int32.TryParse(_ICONBll.GetLockboxID(listIMSDocuments), out iLockboxID))
                        {
                            bSuccess = false;
                            eventLog.logEvent("Failed to convert lockbox while BuildXMLDocuments. LockboxStr is " + _ICONBll.GetLockboxID(listIMSDocuments)
                                             , this.GetType().Name
                                             , MessageType.Error
                                             , MessageImportance.Essential);
                        }
                        if (!Int32.TryParse(_ICONBll.GetBatchID(listIMSDocuments), out iBatchID))
                        {
                            bSuccess = false;
                            eventLog.logEvent("Failed to convert batchID while inserting BuildXMLDocuments. BatchID String is " + _ICONBll.GetBatchID(listIMSDocuments)
                                             , this.GetType().Name
                                             , MessageType.Error
                                             , MessageImportance.Essential);
                        }
                        if (bSuccess)
                        {
                            // If this is the first time through, populate the reference key (strBatchKey1) 
                            // with the current Batch key values
                            if(strBatchKey1.Length == 0) {
                                strBatchKey1 = iLockboxID.ToString() + "~" + _ICONBll.GetSystemDate(listIMSDocuments).ToString("MM/dd/yyyy") + "~" + iBatchID.ToString();
                            }

                            // Store the current Batch key values in strBatchKey2

                            strBatchKey2 = iLockboxID.ToString() + "~" + _ICONBll.GetSystemDate(listIMSDocuments).ToString("MM/dd/yyyy") + "~" + iBatchID.ToString();


                            // If the current key is not equal to the reference key, write the Batch data
                            // to the database, and reset the values which have been collected.
                            if(strBatchKey1 != strBatchKey2) {
                                _ICONBll.InsertFactBatchData(
                                    serverOptions.defaultBankID,
                                    serverOptions.defaultCustomerID,
                                    iLockboxID, 
                                    iBatchID,
                                    _ICONBll.GetSystemDate(listIMSDocuments).ToString("MM/dd/yyyy"),
                                    _ICONBll.GetDepositDate(listIMSDocuments).ToString("MM/dd/yyyy"),
                                    _ICONBll.GetProcessingDate(listIMSDocuments).ToString("MM/dd/yyyy"),
                                    dicBatchData.Values.ToList<cBatchData>());

                                dicBatchData = new Dictionary<int,cBatchData>();
                                strBatchKey1 = strBatchKey2;
                            }

                            foreach(cBatchData batchdata in myOutputDocument.ListBatchData) {
                                if(dicBatchData.ContainsKey(batchdata.DataSetupFieldKey)) {
                                    dicBatchData[batchdata.DataSetupFieldKey] = batchdata;
                                } else {
                                    dicBatchData.Add(batchdata.DataSetupFieldKey, batchdata);
                                }
                            }

                            //now insert items
                            _ICONBll.InsertFactItemData(serverOptions.defaultBankID, 
                                                        serverOptions.defaultCustomerID,
                                                        iLockboxID,
                                                        iBatchID,
                                                        iTransactionID, 
                                                        iBatchSequence,
                                                        _ICONBll.GetSystemDate(listIMSDocuments).ToString("MM/dd/yyyy"),
                                                        _ICONBll.GetDepositDate(listIMSDocuments).ToString("MM/dd/yyyy"),
                                                        _ICONBll.GetProcessingDate(listIMSDocuments).ToString("MM/dd/yyyy"),
                                                        myOutputDocument.ListItemData);

                            bolDoBatchDataInsert = true;
                        }
                    }
                }

                if(bolDoBatchDataInsert) {
                    _ICONBll.InsertFactBatchData(
                        serverOptions.defaultBankID,
                        serverOptions.defaultCustomerID,
                        iLockboxID, 
                        iBatchID,
                        _ICONBll.GetSystemDate(listIMSDocuments).ToString("MM/dd/yyyy"),
                        _ICONBll.GetDepositDate(listIMSDocuments).ToString("MM/dd/yyyy"),
                        _ICONBll.GetProcessingDate(listIMSDocuments).ToString("MM/dd/yyyy"),
                        dicBatchData.Values.ToList<cBatchData>());
                }


                 // call the stored procedure to make an entry in the ICON Client Setup queue
              
                eventLog.logEvent("Calling stored procedure to insert ICON Batch XML in SSB queue"
                        , this.GetType().Name
                        , WFS.integraPAY.Online.Common.MessageType.Information
                        , MessageImportance.Debug);

              
                _ICONBll.InsertICONBatchImport(GetXMLString(xOutput), 1, IPOnlineOptions.imageStorageMode == ImageStorageMode.HYLAND ? 1 : 0);
          
              
                //WJS 48192 11/11/11 - Always write out image to PICS directory
                // if we are using PICS image storage mode, copy the images from the ImageRPS directory to the IMG folder
               
                // CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install
                int iNumImagesArchived = 0;
                int iNumErrorImages = 0;

                bool bWriteImagesSuccessful = WritePICSImages(listIMSDocuments, out iNumImagesArchived, out iNumErrorImages);

                WriteImageRPSResponseFile(_ICONBll.GetLockboxID(listIMSDocuments), _ICONBll.GetBatchID(listIMSDocuments),
                        iNumImagesArchived.ToString(), iNumErrorImages.ToString(), !bWriteImagesSuccessful || iNumErrorImages > 0);
                // END CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install

                if (IPOnlineOptions.imageStorageMode == ImageStorageMode.HYLAND)
                {
                    // call the stored procedure to make a row in the IMSInterfaceQueue table                        
                
                    eventLog.logEvent("Calling stored procedure to insert IMSInterfaceQueue table row"
                            , this.GetType().Name
                            , WFS.integraPAY.Online.Common.MessageType.Information
                            , MessageImportance.Debug);

                    _ICONBll.InsertICONIMSBatchImport(GetXMLString(xIMSOutput), 2, IPOnlineOptions.imageStorageMode == ImageStorageMode.HYLAND ? 1 : 0);
                   
                }

                // if debug, save the file(s)
#if (DEBUG)
                // Save the document to a file and auto-indent the output.
                StringBuilder sbTempXMLFile = new StringBuilder();
                sbTempXMLFile.AppendFormat("Batch_Lockbox{0}_Batch{1}_{2}_{3}.oxi", _ICONBll.GetLockboxID(listIMSDocuments),
                     _ICONBll.GetBatchID(listIMSDocuments), DateTime.Now.ToString("M-d-yyy_HH_mm_ss"), DateTime.Now.Millisecond);
                using (XmlTextWriter writer = new XmlTextWriter(Path.Combine(serverOptions.batchInputPath, sbTempXMLFile.ToString()), null))
                {
                    writer.Formatting = Formatting.Indented;
                    xOutput.Save(writer);
                    writer.Close();
                }

                if (IPOnlineOptions.imageStorageMode == ImageStorageMode.HYLAND)
                {
                    // Save the document to a file and auto-indent the output.
                    sbTempXMLFile.Length = 0;
                    sbTempXMLFile.AppendFormat("IMSInterfaceQueue_Lockbox{0}_Batch{1}_{2}_{3}.oxi", 
                        _ICONBll.GetLockboxID(listIMSDocuments),
                        _ICONBll.GetBatchID(listIMSDocuments), DateTime.Now.ToString("M-d-yyy_HH_mm_ss"), DateTime.Now.Millisecond);
                    using (XmlTextWriter writer1 = new XmlTextWriter(Path.Combine(serverOptions.batchInputPath, sbTempXMLFile.ToString()), null))
                    {
                        writer1.Formatting = Formatting.Indented;
                        xIMSOutput.Save(writer1);
                        writer1.Close();
                    }
                }
#endif
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in BuildXMLDocuments : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// common method for adding data entry info
        /// </summary>
        /// <param name="myOutputDocument"></param>
        /// <param name="bCheckStubAccountAmount"></param>
        private bool AddDataEntryInfo(IMSDocument myOutputDocument, XmlDocument xOutput,XmlNode dataEntryNode, int iSequenceStubs, bool bCheckStubAccountAmount)
        {
            bool bolRetVal = false;

            foreach (IMSKeywordPair myPair in myOutputDocument.listKeywords)
            {
                // check for any keyword that doesn't begin with ">>", that's data entry
                if (!myPair.Keyword.Contains(">"))
                {
                     bool bAddDataEntryNode = true;

                    // CR 29756 MEH 05-21-2010
                    // check for the StubAccount and StubAmount fields
                    if (bCheckStubAccountAmount && IsCheckField(myOutputDocument, myPair))
                    {
                        bAddDataEntryNode = false;
                    }
                    if (bAddDataEntryNode)
                    {
                        AddDataEntryNode(xOutput, dataEntryNode, myOutputDocument, iSequenceStubs, myPair);
                        bolRetVal = true;
                    }
                }
            }

            return(bolRetVal);
        }


        private void AddCheckInfo(XmlDocument xOutput, MICRFields micrFields, XmlNode xmlNode )
        {
            // Amount
            AddAttribute(xOutput, xmlNode, "Amount", micrFields.Amount);
            // Serial
            AddAttribute(xOutput, xmlNode, "Serial", micrFields.Serial);
            // RT
            AddAttribute(xOutput, xmlNode, "RT", micrFields.RT);
            // Account
            AddAttribute(xOutput, xmlNode, "Account", micrFields.Account);
            // TransactionCode
            AddAttribute(xOutput, xmlNode, "TransactionCode", micrFields.TransactionCode);


        }

        #region Support Functions

        // CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install
        private void WriteImageRPSResponseFile(string lockboxId, string batchId, string archiveBatches, string unarchivedBatches, bool bError)
        {
            StringBuilder filename;
            string sInProcessSubFolder;
            string strInProcessResponseFilePath;
            string strResponseFilePath;
            StringBuilder stringBuffer;

            string dateString;
            DateTime time;
            DateTime currentMidnight;
            double seconds;

            try
            {
                eventLog.logEvent("Write ImageRPS response file started."
                           , this.GetType().Name
                          , MessageImportance.Debug);

                filename = new StringBuilder();
                filename.AppendFormat("{0}_{1}.dat", lockboxId.PadLeft(4, '0'), batchId.PadLeft(6, '0'));

                // check that the InProcess subfolder exists
                sInProcessSubFolder = Path.Combine(serverOptions.responseFilePath, "InProcess");

                eventLog.logEvent("Checking for InProcess subfolder in " + serverOptions.responseFilePath
                          , this.GetType().Name
                         , MessageImportance.Debug);

                if (!Directory.Exists(serverOptions.responseFilePath))
                    Directory.CreateDirectory(serverOptions.responseFilePath);

                eventLog.logEvent("Checking for InProcess subfolder in " + sInProcessSubFolder
                          , this.GetType().Name
                         , MessageImportance.Debug);

                if (!Directory.Exists(sInProcessSubFolder))
                    Directory.CreateDirectory(sInProcessSubFolder);

                strInProcessResponseFilePath = Path.Combine(sInProcessSubFolder, filename.ToString());

                eventLog.logEvent("Deleting InProcess ImageRPS response file if it already exists,  filename: " + sInProcessSubFolder
                          , this.GetType().Name
                         , MessageImportance.Debug);

                if (File.Exists(strInProcessResponseFilePath))
                    File.Delete(strInProcessResponseFilePath);

                eventLog.logEvent("Begin writing ImageRPS response file,  filename: " + strInProcessResponseFilePath
                          , this.GetType().Name
                         , MessageImportance.Debug);

                using (StreamWriter sw = new StreamWriter(strInProcessResponseFilePath))
                {
                    stringBuffer = new StringBuilder();

                    // CR 29316 MEH 04-06-2010 The response file generated for the ImageRPS batches needs to be modified
                    stringBuffer.AppendFormat("\"{0}\" \"{1}\" \"{2}\" \"{3}\" ", batchId, batchId, archiveBatches, unarchivedBatches);

                    if (bError)
                        stringBuffer.AppendFormat("\"Resend\" ");
                    else
                        stringBuffer.AppendFormat("\"Good\" ");

                    dateString = string.Empty;
                    time = DateTime.Now;
                    currentMidnight = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
                    seconds = time.TimeOfDay.TotalSeconds - currentMidnight.TimeOfDay.TotalSeconds;

                    dateString = time.ToString("MM/dd/yyyy") + " " + Convert.ToInt32(seconds);
                    stringBuffer.AppendFormat("\"unattend/imsdip\" {0} \"Archived RPS Batch {1} as OLTA Batch {2} ", dateString, batchId, batchId);

                    if (bError)
                        stringBuffer.AppendFormat(" with Errors/Warnings/Invalids\"");
                    else
                        stringBuffer.AppendFormat(" Successfully\"");
                    // END CR 29316 MEH 04-06-2010 The response file generated for the ImageRPS batches needs to be modified

                    sw.WriteLine(stringBuffer);
                }

                strResponseFilePath = Path.Combine(serverOptions.responseFilePath, filename.ToString());

                eventLog.logEvent("Deleting ImageRPS response file if it already exists,  filename: " + strResponseFilePath
                          , this.GetType().Name
                         , MessageImportance.Debug);

                if (File.Exists(strResponseFilePath))
                    File.Delete(strResponseFilePath);

                File.Move(strInProcessResponseFilePath, strResponseFilePath);

                eventLog.logEvent("Finished writing ImageRPS response file"
                       , this.GetType().Name
                      , MessageImportance.Debug);
            }
            catch (Exception ex)
            {
                eventLog.logEvent("Exception thrown trying to write ImageRPS response file. Exception: " + ex.Message
                          , this.GetType().Name
                         , MessageImportance.Debug);
                throw ex;
            }
        }
        // END CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install

        private void AddStubNode(XmlDocument xOutput, XmlNode TransactionNode, IMSDocument myOutputDocument, int iTransactionSequence, int iStubSequence, string strBatchSequence, int iDocumentBatchSequence)
        {
            try
            {
                XmlNode StubsNodeChild = xOutput.CreateElement("Stubs");
                TransactionNode.AppendChild(StubsNodeChild);

                // add the data entry attributes
                // GlobalBatchID
                AddAttribute(xOutput, StubsNodeChild, "GlobalBatchID", "");
                // GlobalStubID
                AddAttribute(xOutput, StubsNodeChild, "GlobalStubID", "");
                // TransactionID
                AddAttribute(xOutput, StubsNodeChild, "TransactionID", myOutputDocument.TransactionID);
                // TransactionSequence
                AddAttribute(xOutput, StubsNodeChild, "TransactionSequence", iTransactionSequence.ToString());
                // BatchSequence
                AddAttribute(xOutput, StubsNodeChild, "BatchSequence", strBatchSequence);
                // StubSequence
                AddAttribute(xOutput, StubsNodeChild, "StubSequence", iStubSequence.ToString());
                // DocumentBatchSequence
                AddAttribute(xOutput, StubsNodeChild, "DocumentBatchSequence", iDocumentBatchSequence.ToString());
                // AccountNumber
                if(myOutputDocument.MICRFields.StubAccount != null)
                    AddAttribute(xOutput, StubsNodeChild, "AccountNumber", myOutputDocument.MICRFields.StubAccount);
                // Amount 
                if (myOutputDocument.MICRFields.StubAmount != null)
                    AddAttribute(xOutput, StubsNodeChild, "Amount", myOutputDocument.MICRFields.StubAmount);
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in AddStubNode : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        private void AddDataEntryNode(XmlDocument xOutput, XmlNode DENode, IMSDocument myOutputDocument, int iBatchSequenceForStubs, IMSKeywordPair myPair)
        {
            try
            {
                
                XmlNode DataEntryNodeChild = xOutput.CreateElement("DataEntry");
                DENode.AppendChild(DataEntryNodeChild);

                // add the data entry attributes

                // GlobalID
                AddAttribute(xOutput, DataEntryNodeChild, "GlobalID", "");
                // GlobalBatchID
                AddAttribute(xOutput, DataEntryNodeChild, "GlobalBatchID", "");
                // TransactionID
                AddAttribute(xOutput, DataEntryNodeChild, "TransactionID", myOutputDocument.TransactionID);
                // BatchSequence
                AddAttribute(xOutput, DataEntryNodeChild, "BatchSequence", iBatchSequenceForStubs.ToString());
                // FldName
                AddAttribute(xOutput, DataEntryNodeChild, "FldName", GetFieldName(myOutputDocument.IsCheck, myPair.Keyword));
                // ReportTitle
                AddAttribute(xOutput, DataEntryNodeChild, "ReportTitle", myPair.Keyword);
                // FldDataTypeEnum
                AddAttribute(xOutput, DataEntryNodeChild, "FldDataTypeEnum", "");
                // FldLength
                AddAttribute(xOutput, DataEntryNodeChild, "FldLength", "");
                // ScreenOrder
                AddAttribute(xOutput, DataEntryNodeChild, "ScreenOrder", "");
                // TableName
                AddAttribute(xOutput, DataEntryNodeChild, "TableName", GetDataEntryTableName(myOutputDocument.IsCheck, myPair.Keyword));

                // DataEntryValue
                // CR 32168 12/23/2010 JNE - modify single quote (if any exist) to 2 single quotes for stored procedure call.
                AddAttribute(xOutput, DataEntryNodeChild, "DataEntryValue", myPair.Value.Replace("'","''"));
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in AddDataEntryNode : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        private bool IsStubAccountOrAmount(string sFieldName, string sFieldType)
        {
            bool bRet = false;

            try
            {
                //WJS CR 45114 only check account, or applied amount if you do not have a field Type
                if (sFieldName.CompareTo("Amount") == 0 || 
                    (String.IsNullOrEmpty(sFieldType) &&  
                    (sFieldName.Contains("Account") ||  sFieldName.CompareTo("Applied Amount") == 0)))
                    bRet = true;
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in IsStubAccountOrAmount : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }

            return bRet;
        }

        // CR 29756 MEH 05-21-2010
        private bool IsCheckField(IMSDocument myOutputDocument, IMSKeywordPair myPair)
        {
            bool bRet = false;

            try
            {
                if (myOutputDocument.IsCheck)
                {
                    if (myPair.Keyword.Contains(cMagicKeywords.RemitterName) || myPair.Keyword.CompareTo(cMagicKeywords.Amount) == 0
                        || myPair.Keyword.CompareTo(cMagicKeywords.AppliedAmount) == 0 || myPair.Keyword.CompareTo(cMagicKeywords.RT) == 0
                    || myPair.Keyword.CompareTo(cMagicKeywords.Account) == 0 || myPair.Keyword.CompareTo(cMagicKeywords.Serial) == 0
                     || myPair.Keyword.CompareTo(cMagicKeywords.TransactionCode) == 0)
                        bRet = true;
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in IsCheckField : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }

            return bRet;
        }

       
        private int GetNextBatchSequence(List<IMSDocument> listIMSDocuments)
        {
            int iRet = -1;

            try
            {
                // get the largest BatchSequence of the list
                foreach (IMSDocument myDoc in listIMSDocuments)
                {
                    if (Convert.ToInt32(myDoc.BatchSequence) > iRet)
                        iRet = Convert.ToInt32(myDoc.BatchSequence);
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in GetNextBatchSequence : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }

            return iRet + 1;
        }

        


        // CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install
        private bool WritePICSImages(List<IMSDocument> listIMSDocuments, out int iNumImagesArchived, out int iNumErrorImages)
        {
            bool bRet = true;

            // CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install
            iNumImagesArchived = 0;
            iNumErrorImages = 0;

            try
            {
                // assemble the PICS folder names
                string sbFolder = IPOnlineOptions.imagePath;

                string sPICSDate = _ICONBll.GetPICSDate(listIMSDocuments).ToString("yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);

                // check the processing date folder in the PICS folder
                sbFolder = Path.Combine(sbFolder, sPICSDate);
                
                if(!Directory.Exists(sbFolder))
                    Directory.CreateDirectory(sbFolder);

                // check the bank folder under the processing date folder
                sbFolder = Path.Combine(sbFolder, serverOptions.defaultBankID.ToString());

                if(!Directory.Exists(sbFolder))
                    Directory.CreateDirectory(sbFolder);

                // check the lockbox folder under the bank folder
                sbFolder = Path.Combine(sbFolder.ToString(), _ICONBll.GetLockboxID(listIMSDocuments));

                if(!Directory.Exists(sbFolder.ToString()))
                    Directory.CreateDirectory(sbFolder);

                foreach (IMSDocument myDoc in listIMSDocuments)
                {
                    // CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install
                    bool bImageError = false;

                    foreach (IMSPage myPage in myDoc.listPages)
                    {
                        // construct file name
                        StringBuilder sbFileToWrite = new StringBuilder();
                        StringBuilder sbFileName = new StringBuilder();
                        string batchSequence = myDoc.IsCheck ? myDoc.BatchSequence : myDoc.BatchSequenceForDocuments.ToString();
                        sbFileName.AppendFormat("{0}_{1}_{2}_{3}.tif", _ICONBll.GetBatchID(listIMSDocuments), batchSequence, myPage.FileDescriptor, myPage.FrontBackIndicator);

                        sbFileToWrite.AppendFormat("{0}", Path.Combine(sbFolder.ToString(), sbFileName.ToString()));

                        try
                        {
                            // write the TIF, extracting it from the multipage TIF in FullPath
                            using (Image img = Bitmap.FromFile(myDoc.FullPath))
                            {
                                int pageCount = img.GetFrameCount(FrameDimension.Page);

                                Guid objGuid = img.FrameDimensionsList[0];
                                FrameDimension objDimension = new FrameDimension(objGuid);
                                img.SelectActiveFrame(objDimension, myPage.TIFPosition);

                                PixelFormat myformat = img.PixelFormat;

                                using (MemoryStream ms = new MemoryStream())
                                {

                                    
                                    if (myformat == PixelFormat.Format1bppIndexed)
                                    {
                                        EncoderParameters ep = new EncoderParameters(1);
                                        ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
                                        img.Save(ms, _ICONBll.CodecInfo, ep);
                                    }
                                    else
                                        img.Save(ms, ImageFormat.Jpeg);

                                    eventLog.logEvent("Writing file : " + sbFileToWrite.ToString()
                                        , this.GetType().Name
                                        , MessageType.Information
                                        , MessageImportance.Debug);

                                    byte[] data = ms.ToArray();
                                    using (FileStream fs = File.OpenWrite(sbFileToWrite.ToString()))
                                    {
                                        fs.Write(data, 0, data.Length);
                                        fs.Close();
                                    }
                                }
                            }
                        }
                        catch (System.IO.FileNotFoundException fnfex)
                        {
                            eventLog.logEvent("Image file not found: " + myDoc.FullPath
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                            eventLog.logEvent("FileNotFoundException: " + fnfex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                            // CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install
                            bImageError = true;
                        }
                        catch (System.Exception ex)
                        {
                            eventLog.logEvent("Error processing image file (writing): " + myDoc.FullPath + " Exception: " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                            // CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install
                            bImageError = true;
                        }                        
                    }

                    // CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install
                    if (!bImageError)
                        iNumImagesArchived++;
                    else
                        iNumErrorImages++;
                    // END CR 30265 07-20-2010 MEH Write response files for ImageRPS in a non-Hyland install
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in WritePICSImages : " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                bRet = false;
            }

            return bRet;
        }

    
        private void AddAttribute(XmlDocument doc, XmlNode node, string sAttributeName, string sAttributeValue)
        {
            XmlAttribute BatchSequence = doc.CreateAttribute(sAttributeName);
            BatchSequence.Value = sAttributeValue;
            node.Attributes.Append(BatchSequence);
        }

     
        
       

       

        #endregion
    }
}
