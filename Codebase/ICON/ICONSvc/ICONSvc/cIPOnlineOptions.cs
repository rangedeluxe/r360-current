﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using WFS.integraPAY.Online.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Mark Horwich
* Date:     10/16/2009
*
* Purpose:  IntegraPAY Online IPOnlie options class.
*
* Modification History
*   10/16/2009 MEH
*       - Created class
* CR 28522 JMC 01/06/2010
*     - Settings class now inherits from cSiteOptions.
******************************************************************************/

namespace WFS.integraPAY.Online.ICONSvc
{
    public class cIPOnlineOptions : cSiteOptions {

        private string _SectionName = "";

        //************************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteKey"></param>
        //************************************************************************
        public cIPOnlineOptions(string siteKey)
            : base(siteKey)
        {
		}

        public string sectionName
        {
            get
            {
                return _SectionName;
            }
        }
    }
}
