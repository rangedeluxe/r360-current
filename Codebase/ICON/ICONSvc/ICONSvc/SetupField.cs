﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFS.integraPAY.Online.ICONSvc {
    class SetupField {
        private string m_sFieldName = string.Empty;
        private string m_sTableName = string.Empty;

        public SetupField(string sFieldName, string sTableName){
            this.m_sFieldName = sFieldName;
            this.m_sTableName = sTableName;
        }

        public string FieldName{
            get { return (m_sFieldName); }
            set { m_sFieldName = value; }
        }

        public string TableName{
            get { return (m_sTableName); }
            set { m_sTableName = value; }
        }
    }
}
