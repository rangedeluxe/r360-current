﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFS.integraPAY.Online.ICONSvc
{
    class DataEntryColumn
    {
        private string sFldName = string.Empty;
        private string sReportTitle = string.Empty;
        private string sTableName = string.Empty;
        private int iDataType = -1;
        private int iFldLength = -1;
        private int iScreenOrder = -1;

        public DataEntryColumn(string sFldName, string sReportTitle, string sTableName, int iDataType, int iFldLength, int iScreenOrder)
        {
            this.sFldName = sFldName;
            this.sReportTitle = sReportTitle;
            this.sTableName = sTableName;
            this.iDataType = iDataType;
            this.iFldLength = iFldLength;
            this.iScreenOrder = iScreenOrder;
        }

        public string FldName
        {
            get { return (sFldName); }
            set { sFldName = value; }
        }

        public string ReportTitle
        {
            get { return (sReportTitle); }
            set { sReportTitle = value; }
        }

        public string TableName
        {
            get { return (sTableName); }
            set { sTableName = value; }
        }

        public int DataType
        {
            get { return (iDataType); }
            set { iDataType = value; }
        }

        public int FldLength
        {
            get { return (iFldLength); }
            set { iFldLength = value; }
        }

        public int ScreenOrder
        {
            get { return (iScreenOrder); }
            set { iScreenOrder = value; }
        }
    }
}
