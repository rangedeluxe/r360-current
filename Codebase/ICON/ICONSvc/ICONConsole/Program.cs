﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace WFS.integraPAY.Online.ICONConsole {

    class Program {

        static void Main(string[] args) {

            bool bolDoProcessClientInputFiles = false;
            bool bolDoProcessBatchInputFiles = false;

            try {

                if(args.Length > 0) {

                    if(args[0].Trim() == "-?" || args[0].Trim() == "/?") {
                        Usage();
                    } else {
                        foreach(string arg in args) {
                            if(arg.ToLower().StartsWith("-c")) {
                                bolDoProcessClientInputFiles = true;
                            } else if(arg.ToLower().StartsWith("-b")) {
                                bolDoProcessBatchInputFiles = true;
                            }
                        }
                    }
                } else {
                    bolDoProcessClientInputFiles = true;
                    bolDoProcessBatchInputFiles = true;
                }
            } catch(Exception ex) {
                Console.WriteLine("Exception occurred: " + ex.Message);
            }

            ICONService objService = new ICONService(); 

            objService.Go(bolDoProcessClientInputFiles, bolDoProcessBatchInputFiles);
        }

        private static void Usage() {
            Console.WriteLine("Manually executes the ICON import process");
            Console.WriteLine("");
            Console.WriteLine(" ICONClient [-c] [-b]");
            Console.WriteLine("");
            Console.WriteLine("  -c                   Process Client setup import.");
            Console.WriteLine("  -b                   Process Batch import.");
            Console.WriteLine("");
            Console.WriteLine("Executing the console with no parameters execute both the Client and Batch import processes.");
        }
    }
}
