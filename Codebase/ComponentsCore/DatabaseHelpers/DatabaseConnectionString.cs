﻿using DpapiCrypto;
using DpapiCrypto.Dto;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace DatabaseHelpers
{
    public class DatabaseConnectionString
    {
        //private const string DBSTRING = "database:connectionString";
        private readonly ILogger _logger;

        //public string ConnectionString { get; set; }

        public DatabaseConnectionString(ILogger logger)
        {
            //ConnectionString =  connectionString;
            _logger = logger;
            //ConnectionString = DecryptConnectionString(connectionString);
        }

        public string GetConnectionString(string connStringName, IConfiguration _configuration)
        {
            var connectionString = _configuration[connStringName];
            try
            {             
                connectionString = DecryptConnectionString(connectionString);
            }
            catch (Exception)
            {
                connectionString = string.Empty;
            }
            return connectionString;
        }

        private string DecryptConnectionString(string encryptedConnectionString)
        {
            CryptoDpapi cryptoDpapi = new CryptoDpapi();
            //just in case it is not really encrypted, return the string if the decrypt fails. 
            string strNewConnectionString = encryptedConnectionString;

            var response = cryptoDpapi.Decrypt(encryptedConnectionString);
            if (response.Status == DpapiStatusCode.SUCCESS)
            {
                strNewConnectionString = response.ReturnedString;
            }

            return strNewConnectionString;
        }
    }
}
