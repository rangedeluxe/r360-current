using System;
using DpapiCrypto;
using DpapiCrypto.Dto;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DpapiCryptoUnitTests
{
    [TestClass]
    public class CryptoDpapiUnitTest
    {
        [TestMethod]
        public void TestEncryptDycrptReturnEqual()
        {
            string orginalStringToEncrypt = "abcdef";
            string encryptedData;
            var crypto = new CryptoDpapi();
            var response = crypto.Encrypt(orginalStringToEncrypt);
            encryptedData = response.ReturnedString;
            string decryptedData;
            var response2 = crypto.Decrypt(encryptedData);
            decryptedData = response2.ReturnedString;
            Assert.AreEqual(response.Status, DpapiStatusCode.SUCCESS);
            Assert.AreEqual(orginalStringToEncrypt, decryptedData);
        }

        [TestMethod]
        public void TestEncryptInvalidString()
        {
            string orginalStringToEncrypt = "";
            string encryptedData;
            var crypto = new CryptoDpapi();
            var response = crypto.Encrypt(orginalStringToEncrypt);
            encryptedData = response.ReturnedString;
            Assert.AreEqual(response.Status, DpapiStatusCode.FAIL);
            Assert.IsNull(encryptedData);
        }

        [TestMethod]
        public void TestDycrptBadBase64StringReturnFalse()
        {
            //pass in bad base64 string
            string encryptedData = "abcdef";
            var crypto = new CryptoDpapi();
            string decryptedData;
            var response = crypto.Decrypt(encryptedData);
            decryptedData = response.ReturnedString;
            Assert.AreEqual(response.Status, DpapiStatusCode.FAIL);
            Assert.IsNull(decryptedData);
        }

        [TestMethod]
        public void TestDycrptBadEncryptedStringReturnFalse()
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.ASCII.GetBytes("abcdef");
            string encryptedData = Convert.ToBase64String(toEncodeAsBytes);
            var crypto = new CryptoDpapi();
            string decryptedData;
            var response = crypto.Decrypt(encryptedData);
            decryptedData = response.ReturnedString;
            Assert.AreEqual(response.Status, DpapiStatusCode.FAIL);
            Assert.IsNull(decryptedData);
        }
    }
}
