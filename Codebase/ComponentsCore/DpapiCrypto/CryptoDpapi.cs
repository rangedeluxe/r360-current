﻿using System;
using System.Security.Cryptography;
using System.Text;
using DpapiCrypto.Dto;

namespace DpapiCrypto
{
    public class CryptoDpapi
    {
        public DpapiResponse Encrypt(string data)
        {
            DpapiResponse response;
            if (Encrypt(data, out byte[] arCipherText))
            {
                response = new DpapiResponse()
                    { ReturnedString = Convert.ToBase64String(arCipherText) };
                return response;
            }

            response = new DpapiResponse()
            {
                ReturnedString = null,
                Status = DpapiStatusCode.FAIL
            };
            return response;
        }

        private bool Encrypt(string data, out byte[] encdata)
        {
            return EncryptWithDpapi(data, out encdata);
        }

        private static readonly byte[] DpapiEntropy = Convert.FromBase64String("+m5dGpOoAgfSo8P59rPLNSO3sMGK7wR7la/Qj0nKY3SYSJZJ8dpm6pRQupjbCujq5yg=");
        
        internal static bool EncryptWithDpapi(string value, out byte[] encBytes)
        {
            bool blnSuccess;
            try
            {
                encBytes = ProtectedData.Protect(Encoding.Unicode.GetBytes(value), DpapiEntropy, DataProtectionScope.LocalMachine);
                blnSuccess = true;
            }
            catch
            {
                encBytes = null;
                blnSuccess = false;
            }
            return blnSuccess;
        }

        public DpapiResponse Decrypt(string base64Data)
        {
            DpapiResponse response;
            byte[] encryptedData;
            try
            {
                encryptedData = Convert.FromBase64String(base64Data);
            }
            catch (Exception)
            {
                response = new DpapiResponse()
                {
                    ReturnedString = null,
                    Status = DpapiStatusCode.FAIL
                };
                response.ErrorMessages.Add("Decryption Failed:  Invalid base64 string");
                return response;
            }

            response = Decrypt(encryptedData);
            return response;
        }

        private DpapiResponse Decrypt(byte[] encData)
        {
            DpapiResponse response = DecryptWithDpapi(encData, out encData);
            if (response.Status == DpapiStatusCode.SUCCESS)
            {
                response.ReturnedString = new ASCIIEncoding().GetString(encData).Replace("\0", "");
            }

            return response;
        }

        internal static DpapiResponse DecryptWithDpapi(byte[] encData, out byte[] value)
        {
            DpapiResponse response = new DpapiResponse();
            try
            {
                value = ProtectedData.Unprotect(encData, DpapiEntropy, DataProtectionScope.LocalMachine);
                response.Status = DpapiStatusCode.SUCCESS;
            }
            catch
            {
                response.Status = DpapiStatusCode.FAIL;
                response.ErrorMessages.Add("Decryption Failed");
                response.ReturnedString = null;
                value = null;
            }
            return response;
        }
    }
}
