﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DpapiCrypto.Dto
{
    public enum DpapiStatusCode
    {
        SUCCESS,
        FAIL
    }
    public class DpapiResponse
    {
        public DpapiResponse()
        {
            Status = DpapiStatusCode.SUCCESS;
            ErrorMessages = new List<string>();
            ReturnedString = string.Empty;
        }

        public DpapiStatusCode Status { get; set; }
        public List<string> ErrorMessages { get; set; }
        public string ReturnedString { get; set; }
    }
}
