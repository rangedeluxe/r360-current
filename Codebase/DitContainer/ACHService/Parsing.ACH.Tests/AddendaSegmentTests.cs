﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Parsing.ACH.Tests
{
    [TestClass]
    public class AddendaSegmentTests
    {
        private string fileHeader = @"101 072000096 0720000961906190837A094101MI POST-C RED MICHIGAN Deluxe Corp            00001237";
        private string batchHeader = @"5220DELUXE CORP     DISCRESIONARYDATA   0720000961CTXPAYMENT   1906191906190001 07200000000001";
        private string batchDetail = @"632041902016123123000123     000055916207000096       0003FREDS BANK LLC XX1000000000000001   ";
        private string addenda1 = @"705ISA*00*          *00*          *ZZ* SVAUTONA       *01*048463400P*130510*17200010000001";
        private string addenda2 = @"7054*U*00400*076913354*0*P*>\GS* RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001";
        private string addenda3 = @"70540918*0\REF* VAlid-Q* ZWFDX*0\SE*19*000000268\REF* Valid_Q*76913354\IEA*1          00040000001";
        private string addenda4 = @"70540918*0\REF* Invalid_Q*ZWFDX*0\SE*19*000000268\GE*1*76913354\IEA*1*076913354\    00040000001";
        private string batchControl = @"822000000541902016  0000000000000000005591620720000961                          07200000000001";
        private string fileControl = @"9000001000009000000050041902016000000000000000000559162                                       ";
        private IList<string> rawDataLines;
        

        private IList<string> BuildFile()
        {
            return new List<string>
            {
                fileHeader,
                batchHeader,
                batchDetail,
                addenda1,
                addenda2,
                addenda3,
                addenda4,
                batchControl,
                fileControl
            };
        }
                                       
        [TestMethod]
        public void CanParseCTXAddendaSegments()
        {
            
            //arrange
            //var batchHeaderRawData = "5220DELUXE CORP     DISCRESIONARYDATA   0720000961CTXPAYMENT   1906111906110001 07200000000001";
            //var lineNumber = 1;
            var batchHeaderRecord = new BatchHeaderRecord(batchHeader, 1);
            var batchDetailRecord = new CTXBatchDetailRecord(batchDetail, 1, batchHeaderRecord);
            var rules = new List<AddendaRule>();
            var segmentDelimiter = "\\~";

            //act
            var parsedResult = new AddendaSegments(batchDetailRecord, rules, segmentDelimiter).Parse();

            //assert
            Assert.IsInstanceOfType(parsedResult.Result, typeof(AddendaSegments));
            //var addendaSegments = parsedResult.Result as AddendaSegments;
            //Assert.AreEqual(5, addendaSegments..AddendaTypeCode);
        }

        [TestMethod]
        public void CanDetermine820Loops()
        {
            //arrange

            //var batchHeaderRecord = new BatchHeaderRecord(batchHeader, 2);
            //var batchDetailRecord = new CTXBatchDetailRecord(batchDetail, 3, batchHeaderRecord);
            //var rules = new List<AddendaRule>
            //{
            //    //new AddendaRule { a}
            //};
            //var addendaSegments = new AddendaSegments(batchDetailRecord, rules, @"\~");
            
            //act
            //var actual = new AddendaLoopBuilder().Build(new BatchAddenda((, "rmr");
            //assert
        }
    }
}
