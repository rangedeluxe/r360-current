using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Parsing.ACH.Tests
{
    [TestClass]
    public class AddendaRecordTests
    {
        private const string rawAddenda1 = @"705ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001";
        private const string rawAddenda2 = @"7054*U*00400*076913354*0*P*>\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001";
        private const string rawAddenda3 = @"705010\ST*820*000000268\REF*2U*DDC2CRJC5G\SE*19*000000268\GE*1*76913354\IEA*1*0769100030000001";
        private const string rawAddenda4 = @"7053354\                                                                           00040000001";

        [TestMethod]
        public void CanParseAnAddendaRecord()
        {
            //arrange
            var lineNumber = 1;

            //act
            var parsedResult = new AddendaRecord(rawAddenda1, lineNumber).Parse();

            //assert
            Assert.IsInstanceOfType(parsedResult.Result, typeof(AddendaRecord));
            var addendaRecord = parsedResult.Result as AddendaRecord;
            Assert.AreEqual(5, addendaRecord.AddendaTypeCode);
        }
                
        [TestMethod]
        public void ShouldHaveSequence()
        {
            //arrange
            var lineNumber = 1;

            //act
            var addenda1 = new AddendaRecord(rawAddenda1, lineNumber).Parse().Result as AddendaRecord;
            var addenda2 = new AddendaRecord(rawAddenda2, lineNumber).Parse().Result as AddendaRecord;
            var addenda3 = new AddendaRecord(rawAddenda3, lineNumber).Parse().Result as AddendaRecord;
            var addenda4 = new AddendaRecord(rawAddenda4, lineNumber).Parse().Result as AddendaRecord;

            //assert
            Assert.AreEqual(addenda1.SequenceNumber, 1);
            Assert.AreEqual(addenda2.SequenceNumber, 2);
            Assert.AreEqual(addenda3.SequenceNumber, 3);
            Assert.AreEqual(addenda4.SequenceNumber, 4);
        }

        [TestMethod]
        public void WillReturnErrorsFromParsingAnAddendaRecord()
        {
            //arrange
            var rawData = "7xxISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*172ZAXC0UU0001";
            var lineNumber = 1;
            
            //act
            var addenda = new AddendaRecord(rawData, lineNumber).Parse();

            //assert
            Assert.AreEqual(addenda.Errors.Count, 3);
            Assert.AreEqual("Error parsing addenda record value:\tLine number: 1\tField: AddendaTypeCode\tValue: xx", addenda.Errors[0]);
            Assert.AreEqual("Error parsing addenda record value:\tLine number: 1\tField: SequenceNumber\tValue: ZAXC", addenda.Errors[1]);
            Assert.AreEqual("Error parsing addenda record value:\tLine number: 1\tField: DetailSequenceNumber\tValue: 0UU0001", addenda.Errors[2]);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
                public void WillThrowExceptionIfInvalidLength()
        {
            //arrange
            var rawData = "7xxISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*172ZAXC0UU000";
            var lineNumber = 1;

            //act
            var parsedResult = new AddendaRecord(rawData, lineNumber).Parse();

            //assert
            Assert.Fail("Did not catch exception for invalid length.");

           
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WillThrowExceptionAddendaRecordEmpty()
        {
            //arrange
            var rawData = "";
            var lineNumber = 1;

            //act
            var parsedResult = new AddendaRecord(rawData, lineNumber).Parse();

            //assert
            Assert.Fail("Did not catch exception for empty addenda record.");


        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WillThrowExceptionInvalidRecordType()
        {
            //arrange
            var rawData = "8xxISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*172ZAXC0UU0001";
            var lineNumber = 1;

            //act
            var parsedResult = new AddendaRecord(rawData, lineNumber).Parse();

            //assert
            Assert.Fail("Did not catch exception for invalid record type.");


        }
    }
}
