﻿using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parsing.ACH.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parsing.ACH.Tests
{
    [TestClass]
    public class CTXAddendaTests
    {
        [TestMethod]
        public void CanParseCTXAddenda()
        {
            //arrange
            var headerRecord = RecordBuilder.BuildCTXHeaderRecord(1);
            var detailRecord = RecordBuilder.BuildCTXBatchDetailRecord(headerRecord, 2);
            var controlRecord = RecordBuilder.BuildControlRecord(3);
            var addendaRules = RecordBuilder.BuildRules();

            //act
            var actual = new CTXAddenda(headerRecord, detailRecord, controlRecord, addendaRules).Parse();

            //assert
            Assert.AreEqual(15, actual.Result.PaymentAddendaFields.Count);
            //do more checks on the actual fields or whatever you wish to check
        }
    }
}
