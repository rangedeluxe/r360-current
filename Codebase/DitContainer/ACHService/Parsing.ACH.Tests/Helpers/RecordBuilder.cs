﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parsing.ACH.Tests.Helpers
{
    public static class RecordBuilder
    {
        
        public static BatchHeaderRecord BuildIATHeaderRecord(int lineNumber = 1)
        {
            return new BatchHeaderRecord(@"5220DELUXE CORP     DISCRESIONARYDATA   0720000961IATPAYMENT   1906191906190001 07200000000001", 1);
        }

        public static BatchHeaderRecord BuildCTXHeaderRecord(int lineNumber = 1)
        {
            return new BatchHeaderRecord(@"5220DELUXE CORP     DISCRESIONARYDATA   0720000961CTXPAYMENT   1906191906190001 07200000000001", lineNumber);
        }

        public static CTXBatchDetailRecord BuildCTXBatchDetailRecord(BatchHeaderRecord headerRecord, int lineNumber)
        {
            var data = @"632041902016123123000123     000055916207000096       0003FREDS BANK LLC XX1000000000000001   ";
            return new CTXBatchDetailRecord(data, lineNumber, headerRecord);
        }

        public static BatchControlRecord BuildControlRecord(int lineNumber)
        {
            return new BatchControlRecord(@"822000000541902016  0000000000000000005591620720000961                          07200000000001", lineNumber);
        }

        public static IList<AddendaRule> BuildRules()
        {
            return new List<AddendaRule>
            {
            };
        }
    }
}
