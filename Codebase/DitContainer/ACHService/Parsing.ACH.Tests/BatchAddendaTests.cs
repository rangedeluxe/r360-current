﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parsing.ACH.Tests
{
    [TestClass]
    public class BatchAddendaTests
    {
        [TestMethod]
        public void CanParseBatchAddenda()
        {
            //arrange
            var batch = new Batch();
            var rules = new List<AddendaRule>();
            //act
            var parsedResult = new BatchAddenda(batch, rules).Parse();

            //assert
            Assert.AreEqual(0, parsedResult.Errors.Count);
        }
    }
}
