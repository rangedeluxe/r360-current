﻿using CommonObjects.Abstractions;
using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace Parsing.ACH.Tests
{
    [TestClass]
    public class ACHFileTests
    {
        private Mock<IRulesProvider> _rulesProvider;

        [TestInitialize()]
        public void Setup()
        {
            _rulesProvider = new Mock<IRulesProvider>();
        }
        
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WillThrowExceptionIfFilePathEmpty()
        {
            //arrange
            //var lineNumber = 1;
            var fullPath = "";

            //act
            var fileName = new ACHFile(fullPath, _rulesProvider.Object).Parse();

            //assert
            Assert.Fail("Did not catch exception for empty path name.");
        }

        [TestMethod]
        public void FileIsValid()
        {
            //arrange
            //var lineNumber = 1;
            var fullPath = $"{AppContext.BaseDirectory}TestFiles\\step_2__brad_IAT_Standard.dat";

            //act
            var fileName = new ACHFile(fullPath, _rulesProvider.Object).Parse();

            //assert
            Assert.AreEqual(fileName.Errors.Count, 0);
            Assert.AreEqual(fileName.Result.Batches.Count, 1);
            Assert.AreEqual(fileName.Result.Batches[0].BatchDetail.Count, 1,"Batch Detail Count not = 1");
            Assert.AreEqual(fileName.Result.Batches[0].BatchDetail[0].Addenda.Count, 4);
            Assert.AreEqual(fileName.Result.Batches[0].BatchDetail[0].RecordType, 6);
            Assert.AreEqual(fileName.Result.RawDataLines.Count, 9);
            Assert.AreEqual(fileName.Result.Messages.Count, 0);
            Assert.AreEqual(fileName.Result.FileHeaderRecord.RecordType, 1);
            Assert.AreEqual(fileName.Result.FileFooterRecord.RecordType, 9);
            Assert.AreEqual(fileName.Result.Batches[0].Header.RecordType, 5);
            Assert.AreEqual(fileName.Result.Batches[0].Footer.RecordType, 8);
            Assert.AreEqual(fileName.Result.Batches[0].Header.EntryClassCode, "IAT");
        }
        [TestMethod]
        public void FooterIsMissingError()
        {
            //arrange
            //var lineNumber = 1;
            var rawDatalines = new List<string>
            {
                "101 720000964 7200009641905080538A094101MI POST-C RED MICHIGAN Deluxe Corp            00004783",
                "5220DELUXE CORP     FV3Discretionary  US6666666701IATPAYMENT   USDCHF1906190001 07200000000001"
                
            };

            //act
            var fileName = new ACHFile(rawDatalines, _rulesProvider.Object).Parse();

            //assert
            Assert.AreEqual(1, fileName.Errors.Count, "Did not get missing footer error as expected");
        }
        [TestMethod]
        public void BatchCountNotEqualFooterCount()
        {
            //arrange
            //var lineNumber = 1;
            var rawDatalines = new List<string>
            {
                "101 720000964 7200009641905080538A094101MI POST-C RED MICHIGAN Deluxe Corp            00004783",
                "5220DELUXE CORP     FV3Discretionary  US6666666701IATPAYMENT   USDCHF1906190001 07200000000001",
                "9000001000009000000050666666670000000000000000000600211                                       "
            };

            //act
            var fileName = new ACHFile(rawDatalines, _rulesProvider.Object).Parse();

            //assert
            Assert.AreEqual(1, fileName.Errors.Count, "Did not get missing footer error as expected");
        }
    }
}