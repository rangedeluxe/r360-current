﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parsing.ACH.Tests
{
    [TestClass]
    public class AddendaLoopTests
    {
        private string segment1 = @"ISA*00*          *00*          *ZZ* SVAUTONA       *01*048463400P*130510*172000100000014*U*00400*076913354*0*P*>";
        private string segment2 = @"GS* RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001010";
        private string segment3 = @"ST*820*000000268";
        private string segment4 = @"RMR* ZZ*20020519*20131101*0.08*90.11*0.10*0";
        private string segment5 = @"DTM*20150814*2010003000000140918*0";
        private string segment6 = @"REF*VAlid-Q* ZWFDX*0";
        private string segment7 = @"REF*Valid_Q*76913354";
        private string segment8 = @"RMR* IV*20020519*20131101*0.08*90.11*0.10*0";
        private string segment9 = @"DTM*20150814*2010003000000140918*0";
        private string segment10 = @"REF* VAlid-Q* ZWFDX*0";
        private string segment11 = @"REF* Valid_Q*76913354*0";
        private string segment12 = @"REF* Invalid_Q*ZWFDX*0";
        private string segment13 = @"SE*19*000000268";
        private string segment14 = @"GE*1*76913354";
        private string segment15 = @"IEA*1*076913354";
        private string segment16 = @"    00040000001";

        [TestMethod]
        public void CanCreatLoops()
        {
            //arrange
            var expectedLoops = 2;
            var loopBuilder = new AddendaLoopBuilder();
            var fieldDelimiter = "*";
            var parsedSegments = new List<ParseResult<Segment>>
            {
                new Segment(segment1, fieldDelimiter).Parse(),
                new Segment(segment2, fieldDelimiter).Parse(),
                new Segment(segment3, fieldDelimiter).Parse(),
                new Segment(segment4, fieldDelimiter).Parse(),
                new Segment(segment5, fieldDelimiter).Parse(),
                new Segment(segment6, fieldDelimiter).Parse(),
                new Segment(segment7, fieldDelimiter).Parse(),
                new Segment(segment8, fieldDelimiter).Parse(),
                new Segment(segment9, fieldDelimiter).Parse(),
                new Segment(segment10, fieldDelimiter).Parse(),
                new Segment(segment11, fieldDelimiter).Parse(),
                new Segment(segment12, fieldDelimiter).Parse(),
                new Segment(segment13, fieldDelimiter).Parse(),
                new Segment(segment14, fieldDelimiter).Parse(),
                new Segment(segment15, fieldDelimiter).Parse(),
                new Segment(segment16, fieldDelimiter).Parse(),
            };

            //act
            var actual = loopBuilder.Build(parsedSegments, "rmr");

            //assert
            Assert.AreEqual(expectedLoops, actual.Count());
        }

    }
}
