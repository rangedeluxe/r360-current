﻿using Deluxe.Payments.Parsing.ACH;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parsing.ACH.Tests
{
    [TestClass]
    public class ExtensionTests
    {
        [TestMethod]
        public void WillNot_StripLeadingZeros_OnAlphaNumericValue()
        {
            //arrange
            var alphaNumericValue = "00X1230  ";
            var expected = "00X1230";

            //act
            var actual = alphaNumericValue.TrimLeadingZeros();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Will_StripLeadingZeros_OnNumericValue()
        {
            //arrange
            var numericValue = "001230  ";
            var expected = "1230";

            //act
            var actual = numericValue.TrimLeadingZeros();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Will_StripLeadingZeros_OnLargestAccountValue()
        {
            //arrange
            var numericValue = "00591009345889999";
            var expected = "591009345889999";

            //act
            var actual = numericValue.TrimLeadingZeros();

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
