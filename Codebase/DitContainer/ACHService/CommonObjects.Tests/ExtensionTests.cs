using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using CommonObjects;
using CommonObjects.Extensions;


namespace CommonObjects.Tests
{
    [TestClass]
    public class ExtensionTests
    {
        [TestMethod]
        public void ValidateJulianDate()
        {

            var depositDate = Convert.ToDateTime("2019-06-20");
            var julianDate = "171";
            var message = "Invalid Julian Date";
            DateTime DTResult;
            IList<string> errors = new List<string>();

            DTResult = depositDate.TryParseJulianDate(julianDate, message, errors);

            Assert.AreEqual(depositDate, DTResult.Date);
        }
        [TestMethod]
        public void InvalidJulianDateReturnsErrorMessage()
        {

            var depositDate = Convert.ToDateTime("2019-06-20");
            var julianDate = "";
            var message = "Invalid Julian Date";
            DateTime DTResult;
            IList<string> errors = new List<string>();

            DTResult = depositDate.TryParseJulianDate(julianDate, message, errors);

            Assert.AreEqual(1, errors.Count);
            Assert.AreEqual(message, errors[0].Substring(0, message.Length));
            Assert.AreNotEqual(depositDate, DTResult.Date);
        }
    }
}
