﻿
using System.Collections.Generic;
using System.Text;
using ACHConverter.Models;
using ACHConverter.Utilities;
using CommonObjects;

namespace ACHConverter.Parsers
{
    public class CTXParser
    {
        private const string DEFAULT_ERROR = "Error parsing Batch Detail value:";
        public static ParseResult<BatchDetailCTX> Parse(string rawDetail, int lineNumber)
        {
            var errors = new List<string>();
            var result = new BatchDetailCTX
            (
                recordType: rawDetail.Substring(0, 1),
                transactionCode: rawDetail.Substring(1, 2),
                aba: rawDetail.Substring(3, 9).Trim(),
                dda: rawDetail.Substring(12, 17).Trim(),
                accountNumber: rawDetail.Substring(12, 17),
                amount: rawDetail.Substring(29, 10).TryParseDecimal($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: Amount", errors),
                traceNumber: rawDetail.Substring(79, 15),
                addenda: new List<Addenda>()
            )
            {
                DiscretionaryData = rawDetail.Substring(76, 2),
                IndividualId = rawDetail.Substring(39, 15),
                ReceivingCompany = rawDetail.Substring(58, 16)
            };

            return new ParseResult<BatchDetailCTX>
            {
                Result = result,
                Errors = errors
            };
        }
    }
}
