﻿using System;
using System.Collections.Generic;
using ACHConverter.Models;
using ACHConverter.Utilities;
using CommonObjects;

namespace ACHConverter.Parsers
{
	public class IatParser
	{
		private const string DEFAULT_ERROR = "Error parsing Batch Detail value:";

		public static ParseResult<BatchDetailIat> Parse(string rawDetail, int lineNumber)
		{
			var errors = new List<string>();
			var result = new BatchDetailIat
			(
				recordType: rawDetail.Substring(0, 1),
				transactionCode: rawDetail.Substring(1, 2),
				aba: rawDetail.Substring(3, 9).Trim(),
				dda: rawDetail.Substring(39, 35).Trim(),
				accountNumber: rawDetail.Substring(39, 35),
				amount: rawDetail.Substring(29, 10).TryParseDecimal($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: Amount", errors),
				traceNumber: rawDetail.Substring(79, 15),
				addenda: new List<Addenda>()
			)
			{
			    OfacIndicator1 = rawDetail.Substring(76, 1),
			    OfacIndicator2 = rawDetail.Substring(77, 1)
            };

			return new ParseResult<BatchDetailIat>
			{
				Result = result,
				Errors = errors
			};
		}
	}
}
