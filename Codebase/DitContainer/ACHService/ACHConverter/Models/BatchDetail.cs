﻿using System.Collections.Generic;
using System.Text;
using ACHConverter.Utilities;

namespace ACHConverter.Models
{
	public class BatchDetail
	{
		private const string DEFAULT_ERROR = "Error parsing Batch Detail value:";
		public string RecordType { get; set; }
		public string TransactionCode { get; set; }
		public string ABA { get; set; }
		public string DDA { get; set; }
		public string AccountNumber { get; set; }
		public decimal Amount { get; set; }
		public string TraceNumber { get; set; }
		public IList<Addenda> Addenda { get; set; }
		public string TransactionSignature { get; set; }
		public string TransactionHash => TransactionSignature.GetHash();

		public BatchDetail(string recordType, string transactionCode, string aba,
			string dda, string accountNumber, decimal amount, string traceNumber,
			IList<Addenda> addenda)
		{
			RecordType = recordType;
			TransactionCode = transactionCode;
			this.ABA = aba;
			this.DDA = dda;
			AccountNumber = accountNumber;
			Amount = amount;
			TraceNumber = traceNumber;
			var sb = new StringBuilder();
			sb.Append(RecordType).Append(TransactionCode).
				Append(Amount).Append(AccountNumber).
				Append(TraceNumber);
			TransactionSignature = sb.ToString();
		}
	}
}
