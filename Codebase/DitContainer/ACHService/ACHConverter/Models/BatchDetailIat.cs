﻿

using System.Collections.Generic;

namespace ACHConverter.Models
{
	public class BatchDetailIat : BatchDetail
	{
		public BatchDetailIat(string recordType, string transactionCode, string aba,
			string dda, string accountNumber, decimal amount, string traceNumber,
			IList<Addenda> addenda) :
			base(recordType, transactionCode, aba, dda, accountNumber, amount, traceNumber, addenda)
		{
		}

		public string OfacIndicator1 { get; set; }
		public string OfacIndicator2 { get; set; }
	}
}
