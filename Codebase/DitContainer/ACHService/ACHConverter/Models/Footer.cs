﻿using System.Collections.Generic;
using ACHConverter.Utilities;
using CommonObjects;

namespace ACHConverter.Models
{
    public class Footer
    {
        private const string DEFAULT_ERROR = "Error parsing footer value:";
        public int RecordTypeCode { get; set; }
        public int BatchCount { get; set; }
        public int BlockCount { get; set; }
        public int TotalCount { get; set; }
        public long TotalHash { get; set; }
        public string FileDebitAmount { get; set; }
        public string FileCreditAmount { get; set; }
        public string ReservedData { get; set; }

        public static ParseResult<Footer> Parse(string rawFooter, int lineNumber)
        {
            var errors = new List<string>();
            if (rawFooter.Length != ACHUtilities.FIXED_LINE_SIZE)
            {
                errors.Add($"{DEFAULT_ERROR}\tLine Number: {lineNumber} {ACHUtilities.ErrorCodes.DITACHIMPORT_INVALID_FILE_FOOTER_ERROR}");
                return new ParseResult<Footer>{Result = null, Errors = errors};
            }

            var footer = new Footer
            {
                RecordTypeCode = rawFooter.Substring(0, 1).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: RecordTypeCode", errors),
                BatchCount = rawFooter.Substring(1, 6).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: BatchCount", errors),
                BlockCount = rawFooter.Substring(7, 6).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: TotalCount", errors),
                TotalCount = rawFooter.Substring(13, 8).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: EntryCount", errors),
                TotalHash = rawFooter.Substring(21, 10).TryParseLong($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: TotalHash", errors),
                FileDebitAmount = rawFooter.Substring(31, 10) + "." + rawFooter.Substring(41, 2),
                FileCreditAmount = (rawFooter.Substring(43, 10) + "." + rawFooter.Substring(53, 2)),
            };
            return new ParseResult<Footer>
            {
                Result = footer,
                Errors = errors

            };
        }

    }
}