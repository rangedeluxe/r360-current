﻿using System;
using System.Collections.Generic;
using ACHConverter.Utilities;
using CommonObjects;

namespace ACHConverter.Models
{
    public class BatchHeader
    {
        private const string DEFAULT_ERROR = "Error Parsing Batch Header";
        public int ServiceClass { get; set; }
        public string CompanyName { get; set; }
        public string CompanyData { get; set; }
        public string CompanyId { get; set; }
        public string EntryClassCode { get; set; }
        public string EntryDescription { get; set; }
        public string DescriptiveDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime SettlementDate { get; set; }
        public string OriginatorStatus { get; set; }
        public int ACHBatchNumber { get; set; }

        //The following fields are part of the IAT batch header
        public string OriginatingCurrencyCode { get; set; }
        public string DestinationCurrencyCode { get; set; }
        public string ForeignExchangeIndicator { get; set; }
        public string ForeignExchangeRefIndicator { get; set; }
        public string ForeignExchangeReference { get; set; }
        public string IsoDestinationCountryCode { get; set; }

        public static ParseResult<BatchHeader> Parse(string rawBatchHeader, int lineNumber)
        {
            var errors = new List<string>();
            if (rawBatchHeader.Length != ACHUtilities.FIXED_LINE_SIZE)
            {
                errors.Add($"{DEFAULT_ERROR}\tLineNumber: {lineNumber} {ACHUtilities.ErrorCodes.DITACHIMPORT_INVALID_BATCH_DETAIL_ERROR} : Invalid length {rawBatchHeader.Length}");
                return new ParseResult<BatchHeader>{Result = null, Errors = errors};
            }
            var effectiveDate = rawBatchHeader.Substring(69, 6).TryParseFormmatedDate($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: EffectiveDate", errors);
            var settlementDate = rawBatchHeader.Substring(75, 3) == "000" ? effectiveDate :
	            effectiveDate.TryParseJulianDate(rawBatchHeader.Substring(75, 3), $"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: SettlementDate", errors);
            return new ParseResult<BatchHeader>
            {
                Result = new BatchHeader
                {
                    ServiceClass = rawBatchHeader.Substring(1, 3).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: ServiceClass", errors),
                    CompanyName = rawBatchHeader.Substring(4, 16),
                    CompanyData = rawBatchHeader.Substring(20, 20),
                    CompanyId = rawBatchHeader.Substring(40, 10),
                    EntryClassCode = rawBatchHeader.Substring(50, 3),
                    EntryDescription = rawBatchHeader.Substring(53, 10),
                    DescriptiveDate = rawBatchHeader.Substring(63, 6),
                    ForeignExchangeIndicator = rawBatchHeader.Substring(20, 2),
                    ForeignExchangeRefIndicator = rawBatchHeader.Substring(22, 1),
                    ForeignExchangeReference = rawBatchHeader.Substring(23, 15),
                    IsoDestinationCountryCode = rawBatchHeader.Substring(38, 2),
                    OriginatingCurrencyCode = rawBatchHeader.Substring(63, 3),
                    DestinationCurrencyCode = rawBatchHeader.Substring(66, 3),
                    EffectiveDate = effectiveDate,
                    SettlementDate = settlementDate,
                    OriginatorStatus = rawBatchHeader.Substring(78, 1),
                    ACHBatchNumber = rawBatchHeader.Substring(87, 7).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: ACHBatchNumber", errors),
                },
                Errors = errors
            };
        }

    }
}
