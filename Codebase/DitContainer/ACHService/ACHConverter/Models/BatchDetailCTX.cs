﻿

using System.Collections.Generic;
using System.Diagnostics;

namespace ACHConverter.Models
{
	public class BatchDetailCTX : BatchDetail
	{
		public BatchDetailCTX(string recordType, string transactionCode, string aba,
			string dda, string accountNumber, decimal amount, string traceNumber,
			IList<Addenda> addenda):
			base(recordType, transactionCode,aba, dda,accountNumber,amount,traceNumber,addenda)
		{
			
		}
		public string IndividualId { get; set; }
		public string ReceivingCompany { get; set; }
		public string DiscretionaryData { get; set; }
	}
}
