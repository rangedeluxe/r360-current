﻿using System.Collections.Generic;
using ACHConverter.Utilities;
using CommonObjects;

namespace ACHConverter.Models
{
    public class BatchFooter
    {
        private const string DEFAULT_ERROR = "Error parsing Batch footer value:";
        public string OriginatingDFI { get; set; }
        public long TotalHash { get; set; }
        public decimal TotalDebitAmount { get; set; }
        public decimal TotalCreditAmount { get; set; }

        public static ParseResult<BatchFooter> Parse(string rawBatchFooter, int lineNumber)
        {
            var errors = new List<string>();
            if (rawBatchFooter.Length != ACHUtilities.FIXED_LINE_SIZE)
            {
                errors.Add($"{DEFAULT_ERROR}\tLine number: {lineNumber} { ACHUtilities.ErrorCodes.DITACHIMPORT_INVALID_BATCH_FOOTER_ERROR}");
                return new ParseResult<BatchFooter>{Result = null, Errors = errors};
            }
            return new ParseResult<BatchFooter>
            {
                Result = new BatchFooter{
                    OriginatingDFI = rawBatchFooter.Substring(79, 8),
                    TotalHash = rawBatchFooter.Substring(10,10).TryParseLong($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: TotalHash", errors),
                    TotalDebitAmount = $"{rawBatchFooter.Substring(20,10)}.{rawBatchFooter.Substring(30,2)}".TryParseDecimal($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: TotalDebitAmount", errors),
                    TotalCreditAmount = $"{rawBatchFooter.Substring(32,10)}.{rawBatchFooter.Substring(42,2)}".TryParseDecimal($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: TotalCreditAmount", errors),
                },
                Errors = errors,
            };
        }

    }
}
