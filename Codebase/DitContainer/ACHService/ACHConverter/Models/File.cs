﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ACHConverter.Parsers;
using ACHConverter.Utilities;
using CommonObjects;

namespace ACHConverter.Models
{
    public class File
    {
        private const string PARSERERROR = "Error parsing document";
        public Header Header { get; set; }
        public IList<Batch> Batches { get; set; } = new List<Batch>();
        public Footer Footer { get; set; }
        public string FileSignature { get; set; }
        public string FileHash => FileSignature.GetHash();

        public ParseResult<bool> Validate()
        {
            var errors = new List<string>();
            if (Footer.BatchCount != Batches.Count)
                errors.Add("Footer count and Batches found are not equal");
            //checks for supported batch types
            errors.AddRange(Batches.Where(batch => !ACHUtilities.BatchTypes.Contains(batch.Header.EntryClassCode))
                .Select(batch => $"Batch Type:{batch.Header.EntryClassCode} not recognized"));
            
            return new ParseResult<bool>
            {
                Result = errors.Count == 0,
                Errors = errors
            };
        }

        public static ParseResult<File> Parse(StreamReader reader)
        {
            var File = new File();
            var line = string.Empty;
            var docerrors = new List<string>();
	        int lineNumber = 0;
            while ((line = reader.ReadLine()) != null)
            {
	            lineNumber++;
                var current = (ACHUtilities.RecordType)line.Substring(0, 1).TryParseInt(PARSERERROR, docerrors);
                if (current == ACHUtilities.RecordType.HEADER_RECORD_TYPE_CODE)
                {
                    var headerresult = Header.Parse(line, lineNumber);
                    File.Header = headerresult.Result;
                    docerrors.AddRange(headerresult.Errors);
                }
                else if (current == ACHUtilities.RecordType.BATCH_HEADER_RECORD_TYPE_CODE)
                {
                    var batchHeaderRes = BatchHeader.Parse(line, lineNumber);
                    docerrors.AddRange(batchHeaderRes.Errors);
                    File.Batches.Add(new Batch {Header = batchHeaderRes.Result});
                }
                else if (current == ACHUtilities.RecordType.DETAIL_RECORD_TYPE_CODE)
                {
                    var batch = File.Batches[File.Batches.Count - 1];
                    batch.BatchDetail = batch.BatchDetail ?? new List<BatchDetail>();
                    if (batch.Header.EntryClassCode == ACHUtilities.ACH_BATCH_TYPE_CTX)
                    {
                        var res = CTXParser.Parse(line, lineNumber);
                        docerrors.AddRange(res.Errors);
                        batch.BatchDetail.Add(res.Result); 
                    }
                    else if (batch.Header.EntryClassCode == ACHUtilities.ACH_BATCH_TYPE_CCD)
                    {
                        var res = CcdParser.Parse(line, lineNumber);
                        docerrors.AddRange(res.Errors);
                        batch.BatchDetail.Add(res.Result);
                    }
                    else if (batch.Header.EntryClassCode == ACHUtilities.ACH_BATCH_TYPE_CIE)
                    {
                        var res = CieParser.Parse(line, lineNumber);
                        docerrors.AddRange(res.Errors);
                        batch.BatchDetail.Add(res.Result);
                    }
                    else if (batch.Header.EntryClassCode == ACHUtilities.ACH_BATCH_TYPE_IAT)
                    {
                        var res = IatParser.Parse(line, lineNumber);
                        docerrors.AddRange(res.Errors);
                        batch.BatchDetail.Add(res.Result);
                    }
                    else if (batch.Header.EntryClassCode == ACHUtilities.ACH_BATCH_TYPE_PPD)
                    {
                        var res = PpdParser.Parse(line, lineNumber);
                        docerrors.AddRange(res.Errors);
                        batch.BatchDetail.Add(res.Result);
                    }
                }
                else if (current == ACHUtilities.RecordType.ADDENDA_RECORD_TYPE_CODE)
                {
                    var batch = File.Batches[File.Batches.Count - 1];
                    var batchDetail = batch.BatchDetail[batch.BatchDetail.Count - 1];
                    batchDetail.Addenda = batchDetail.Addenda ?? new List<Addenda>(); 
                    var res = Addenda.Parse(line, lineNumber);
                    docerrors.AddRange(res.Errors);
                    batchDetail.Addenda.Add(res.Result);
                }
                else if (current == ACHUtilities.RecordType.BATCH_FOOTER_RECORD_TYPE_CODE)
                {
                    var batchFooterResult = BatchFooter.Parse(line, lineNumber);
                    docerrors.AddRange(batchFooterResult.Errors);
                    File.Batches[File.Batches.Count - 1].Footer = batchFooterResult.Result;
                }
                else if (current == ACHUtilities.RecordType.FOOTER_RECORD_TYPE_CODE)
                {
                    var footerResults = Footer.Parse(line, lineNumber);
                    docerrors.AddRange(footerResults.Errors);
                    File.Footer = footerResults.Result;
                    var filesignature = new StringBuilder();
                    filesignature.Append(File.Footer.BatchCount);
                    filesignature.Append(File.Footer.BlockCount);
                    filesignature.Append(File.Footer.TotalCount );
                    filesignature.Append(File.Footer.TotalHash);
                    filesignature.Append(File.Footer.FileDebitAmount);
                    filesignature.Append(File.Footer.FileCreditAmount);
                    File.FileSignature = filesignature.ToString();
                }
            }
            return new ParseResult<File>
            {
                Result = File,
                Errors = docerrors
            };
        }
    }
}
