﻿

using System.Collections.Generic;

namespace ACHConverter.Models
{
    public class BatchDetailCcd : BatchDetail
    {
        public BatchDetailCcd(string recordType, string transactionCode, string aba,
            string dda, string accountNumber, decimal amount, string traceNumber,
            IList<Addenda> addenda):
            base(recordType, transactionCode,aba, dda,accountNumber,amount,traceNumber,addenda)
        {
        }
        public string IndividualId { get; set; }
        public string ReceivingCompany { get; set; }
        public string DiscretionaryData { get; set; }
    }
}
