﻿

using System.Collections.Generic;

namespace ACHConverter.Models
{
	public class BatchDetailCie : BatchDetail
	{
		public BatchDetailCie(string recordType, string transactionCode, string aba,
			string dda, string accountNumber, decimal amount, string traceNumber,
			IList<Addenda> addenda) :
			base(recordType, transactionCode, aba, dda, accountNumber, amount, traceNumber, addenda)
		{
		}
		public string IndividualId { get; set; }
		public string IndividualName { get; set; }
	}
}
