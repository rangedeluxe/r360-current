﻿using System.Collections.Generic;
using ACHConverter.Utilities;
using CommonObjects;

namespace ACHConverter
{
    public class Addenda
    {
        private const string DEFAULT_ERROR = "Error parsing Addenda value:";
        public int RecordTypeCode { get; set; }
        public int AddedaTypeCode { get; set; }
        public string Data { get; set; }
        public int SequenceNumber { get; set; }
        public int DetailSequenceNumber { get; set; }
        public string RawAddenda { get; set; }
        public static ParseResult<Addenda> Parse(string rawAddenda, int lineNumber)
        {
            var errors = new List<string>();
            return new ParseResult<Addenda>()
            {
                Result = new Addenda
                {
                    RecordTypeCode = rawAddenda.Substring(0, 1).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: RecordTypeCode", errors),

					AddedaTypeCode = rawAddenda.Substring(1,2).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: AddendaTypeCode", errors),

					Data = rawAddenda.Substring(3,80),
                    SequenceNumber = rawAddenda.Substring(83,4).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: SequenceNumber", errors),

					DetailSequenceNumber = rawAddenda.Substring(87,7).TryParseInt($"{DEFAULT_ERROR}\tLine number: {lineNumber}\tField: DetailSequenceNumber", errors),

					RawAddenda = rawAddenda
                },
                Errors = errors
            };
        }
    }
}