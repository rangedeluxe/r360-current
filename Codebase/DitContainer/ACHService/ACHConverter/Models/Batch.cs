﻿using System.Collections.Generic;

namespace ACHConverter.Models
{
    public class Batch
    {
        public BatchHeader Header { get; set; }
        public IList<BatchDetail> BatchDetail { get; set; }
        public BatchFooter Footer { get; set; }

    }
}