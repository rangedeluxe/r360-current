﻿using System.Collections.Generic;
using ACHConverter.Utilities;
using CommonObjects;

namespace ACHConverter
{
    public class Header
    {
        private const string PARSERERROR = "Error parsing header";
        public string RawHeader { get; set; }

        public static ParseResult<Header> Parse(string rawHeader, int lineNumber)
        {
            var errors = new List<string>();
	        if (rawHeader.Length != ACHUtilities.FIXED_LINE_SIZE)
	        {
				errors.Add(
			        $"{PARSERERROR} Line length {rawHeader.Length} is invalid.\tLine Number: {lineNumber}.\t{ACHUtilities.ErrorCodes.DITACHIMPORT_INVALID_FILE_HEADER_ERROR}");
	        }

	        return new ParseResult<Header>
            {
                Result = new Header
                {
                    RawHeader = rawHeader
                },
                Errors = errors
            };
        }
    }
}