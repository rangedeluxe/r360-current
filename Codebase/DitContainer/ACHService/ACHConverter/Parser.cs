﻿using System;
using System.Collections.Generic;
using System.IO;
using ACHConverter.Utilities;
using CommonObjects;
using File = ACHConverter.Models.File;

namespace ACHConverter
{
    public class Parser
    {
        
        public ParseResult<File> Parse(StreamReader reader)
        {
            var result = File.Parse(reader);
            return new ParseResult<File>
            {
                Result = result.Result,
                Errors = result.Errors
            };
        }
    }


}
