﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ACHConverter.Utilities
{
    public static class DateTimeMethods
    {
        public static DateTime TryParseJulianDate(this DateTime startDt, string value, string message, IList<string> errors)
        {
            try
            {
	            DateTime januaryOne = DateTime.Parse($"01/01/{startDt.Year}");
                return januaryOne.AddDays(Convert.ToInt32(value) - 1);
            }
            catch (Exception)
            {
                errors.Add(message + $"\tValue: {value}");
                return DateTime.MinValue;
            }
        }
    }
}
