﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ACHConverter.Utilities
{
    public static class ACHUtilities
    {
        /// <summary>
        /// Line size in file        
        ///<summary>
        public const int FIXED_LINE_SIZE = 94;
        /// <summary>
        /// Record Type Code of the file header 
        /// </summary>
        public const string HEADER_RECORD_TYPE_CODE = "1";
        /// <summary>
        /// Record Type Code of the batch header 
        /// </summary>
        public const string BATCH_HEADER_RECORD_TYPE_CODE = "5";
        /// <summary>
        /// Record Type Code of the entry detail 
        /// </summary>
        public const string DETAIL_RECORD_TYPE_CODE = "6";
        /// <summary>
        /// Record Type Code of the Payment-related information 
        /// </summary>
        public const string ADDENDA_RECORD_TYPE_CODE = "7";
        /// <summary>
        /// Record Type Code of the batch footer - Batch control record 
        /// </summary>        
        public const string BATCH_FOOTER_RECORD_TYPE_CODE = "8";
        /// <summary>
        /// Record Type Code of the file footer - File control record 
        /// </summary>
        public const string FOOTER_RECORD_TYPE_CODE = "9";


        /// <summary>
        /// Names of Batch Types
        /// </summary>
        public const string ACH_BATCH_TYPE_CCD = "CCD";
        public const string ACH_BATCH_TYPE_CTX = "CTX";
        public const string ACH_BATCH_TYPE_PPD = "PPD";
        public const string ACH_BATCH_TYPE_IAT = "IAT";
        public const string ACH_BATCH_TYPE_CIE = "CIE";

        public  static readonly IList<string> BatchTypes = new ReadOnlyCollection<string>
        (new List<String> { 
            ACH_BATCH_TYPE_CCD,
            ACH_BATCH_TYPE_CTX, 
            ACH_BATCH_TYPE_PPD,
            ACH_BATCH_TYPE_IAT,
            ACH_BATCH_TYPE_CIE
        });
        public enum RecordType
        {
            HEADER_RECORD_TYPE_CODE = 1,
            BATCH_HEADER_RECORD_TYPE_CODE = 5,
            DETAIL_RECORD_TYPE_CODE = 6,
            ADDENDA_RECORD_TYPE_CODE = 7,
            BATCH_FOOTER_RECORD_TYPE_CODE = 8,
            FOOTER_RECORD_TYPE_CODE = 9
        }

        /// <summary>
        /// Error codes
        /// </summary>
        public enum ErrorCodes
        {
            DITACHIMPORT_NOERROR = 0,
            DITACHIMPORT_INVALID_FILE_HEADER_ERROR = 1,
            DITACHIMPORT_INVALID_FILE_FOOTER_ERROR = 2,
            DITACHIMPORT_INVALID_BATCH_HEADER_ERROR = 3,
            DITACHIMPORT_INVALID_BATCH_FOOTER_ERROR = 4,
            DITACHIMPORT_INVALID_BATCH_DETAIL_ERROR = 5,
            DITACHIMPORT_INVALID_DETAIL_ADDENDA_ERROR = 6
        }

    }
}
