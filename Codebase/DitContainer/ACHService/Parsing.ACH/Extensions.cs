﻿using CommonObjects;
using CommonObjects.Abstractions;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using Deluxe.Payments.Parsing.ACH.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using CommonObjects.Extensions;

namespace Deluxe.Payments.Parsing.ACH
{
    public static class Extensions
    {
        public static bool Exist(this IList<AddendaRule> rules)
        {
            rules = rules ?? new List<AddendaRule>();
            return rules.Any();
        }

        public static PossibleResult<IList<AddendaRule>> TryFindMatchedRules(this IList<AddendaRule> rules, string[] data)
        {
            if (!rules.Exist()) return Result.None<IList<AddendaRule>>();

            if (data.Length < 2) return Result.None<IList<AddendaRule>>();

            var matchedRules = rules.Where(rule =>
                data.Length > rule.FieldNumber &&
                (rule.SegmentIdentifier == data[0] && rule.SegmentQualifier == data[1]) ||
                (rule.SegmentIdentifier == data[0] && string.IsNullOrEmpty(rule.SegmentQualifier))).ToList();

            return Result.Real(matchedRules as IList<AddendaRule>);
        }

        public static IList<IList<Field>> MapToPaymentFields(this IList<ParseResult<Addenda>> parsedAddendaList, BatchDetailRecord detail)
        {
            var parsedAddenda = parsedAddendaList.Where(addenda => addenda.Result.Detail == detail).FirstOrDefault();
            if (parsedAddenda == null) return new List<IList<Field>>();

            return parsedAddenda.Result.PaymentAddendaFields;
        }

        public static IList<IList<Field>> MapToDocumentFields(this IList<ParseResult<Addenda>> parsedAddendaList, BatchDetailRecord detail)
        {
            var parsedAddenda = parsedAddendaList.Where(addenda => addenda.Result.Detail == detail).FirstOrDefault();
            if (parsedAddenda == null) return new List<IList<Field>>();

            return parsedAddenda.Result.DocumentAddendaFields;
        }

        public static IList<Transaction> MapTo(this BatchAddenda batchAddenda, BatchDetailIdentifier account)
        {
            var transactions = new List<Transaction>();
            batchAddenda.Batch.BatchDetail
                .Where(d => d.ABA == account.ABA && d.DDA.TrimLeadingZeros() == account.DDA.TrimLeadingZeros())
                .ToList().ForEach(detail =>
                {
                    transactions.Add(detail.MapTo(batchAddenda));
                });

            return transactions;
        }

        public static Transaction MapTo(this BatchDetailRecord detail, BatchAddenda batchAddenda)
        {
            return new Transaction
            {
                TransactionSignature = detail.TransactionSignature,
                TransactionHash = detail.TransactionHash,
                Payments = new List<Payment>
                {
                    new Payment
                    {
                        Amount = detail.Amount,
                        TransactionCode = detail.TransactionCode,
                        Serial = detail.TraceNumber,
                        RemitterName = detail.Header.CompanyName,
                        ABA = detail.ABA,
                        DDA = detail.DDA,
                        RemittanceData = batchAddenda.ParsedAddenda.MapToPaymentFields(detail),
                        RawData = detail.Addenda?.Select(x => new RawData {Data = x.RawAddenda}).ToList()
                    }
                },
                Documents = new List<Document>
                {
                    new Document
                    {
                        DocumentDescriptor = "ELECTRONIC",
                        DocumentRemittanceData = batchAddenda.ParsedAddenda.MapToDocumentFields(detail)
                    }
                }
            };
        }

        public static bool Is820Addenda(this IList<ParseResult<Segment>> parsedSegments)
        {
            if (parsedSegments == null) return false;

            return parsedSegments
                .Any(segment => string.Equals(segment.Result.Identifier, "isa", StringComparison.InvariantCultureIgnoreCase));
        }

        public static PossibleResult<IAddendaFieldsBuilder> MapTo(this IList<ParseResult<Segment>> parsedSegments)
        {
            if (parsedSegments == null) return Result.None<IAddendaFieldsBuilder>();
            if (parsedSegments.Is820Addenda()) return Result.Real(new EDI820AddendaFieldsBuilder() as IAddendaFieldsBuilder);

            return Result.None<IAddendaFieldsBuilder>();
        }

        public static bool IsENTSegment(this Segment segment)
        {
            return segment.IsSegment("ent");
        }

        public static bool IsBPRSegment(this Segment segment)
        {
            return segment.IsSegment("bpr");
        }

        public static bool IsRMRSegment(this Segment segment)
        {
            return segment.IsSegment("rmr");
        }

        public static bool IsLXSegment(this Segment segment)
        {
            return segment.IsSegment("lx");
        }

        public static bool IsRYLSegment(this Segment segment)
        {
            return segment.IsSegment("ryl");
        }

        public static bool IsREFSegment(this Segment segment)
        {
            return segment.IsSegment("ref");
        }

        public static bool IsNM1Segment(this Segment segment)
        {
            return segment.IsSegment("nm1");
        }

        public static bool IsDTMSegment(this Segment segment)
        {
            return segment.IsSegment("dtm");
        }

        public static bool IsSegment(this Segment segment, string identifier)
        {
            if (segment == null) return false;
            if (string.IsNullOrEmpty(identifier)) return false;

            return string.Equals(segment.Identifier, identifier, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsClassCode(this BatchHeaderRecord batchHeaderRecord, string entryClassCode)
        {
            if (string.Equals(batchHeaderRecord.EntryClassCode, entryClassCode, StringComparison.InvariantCultureIgnoreCase)) return true;
            return false;
        }

        public static bool IsCtx(this BatchHeaderRecord batchHeaderRecord)
        {
            return IsClassCode(batchHeaderRecord, "ctx");
        }

        public static bool IsCcd(this BatchHeaderRecord batchHeaderRecord)
        {
            return IsClassCode(batchHeaderRecord, "ccd");
        }
        public static bool IsCie(this BatchHeaderRecord batchHeaderRecord)
        {
            return IsClassCode(batchHeaderRecord, "cie");
        }

        public static bool IsIat(this BatchHeaderRecord batchHeaderRecord)
        {
            return IsClassCode(batchHeaderRecord, "iat");
        }

        public static bool IsPpd(this BatchHeaderRecord batchHeaderRecord)
        {
            return IsClassCode(batchHeaderRecord, "ppd");
        }

        public static string TrimLeadingZeros(this string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;

            value = value.Trim();

            //make sure the value is numeric, if not, we don't want to strip zeros
            if (!value.All(char.IsDigit)) return value;

            return value.TrimStart('0');
        }

        public static IEnumerable<BatchDetailIdentifier> ToDistinctAccounts(this IEnumerable<BatchDetailRecord> detailRecords)
        {
            return detailRecords
                .Select(x => new BatchDetailIdentifier { ABA = x.ABA, DDA = x.DDA.TrimLeadingZeros() })
                .DistinctBy(x => new { x.ABA, x.DDA });
        }
    }
}
