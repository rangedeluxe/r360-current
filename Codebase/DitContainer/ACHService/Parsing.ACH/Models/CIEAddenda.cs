﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class CIEAddenda : Addenda
    {
        public CIEAddenda(BatchHeaderRecord batchHeaderRecord, BatchDetailRecord batchDetailRecord,
            BatchControlRecord batchControlRecord, IList<AddendaRule> rules) 
            : base(batchHeaderRecord, batchDetailRecord, batchControlRecord, rules)
        {
            ValidateDetailRecordType<CIEBatchDetailRecord>();
        }

        public override ParseResult<Addenda> Parse()
        {
            ParseAddendaSegments();

            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "CompanyData",
                FieldValue = Header.CompanyData, IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "DescriptiveDate",
                FieldValue = Header.DescriptiveDate, IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "IndividualID",
                FieldValue = (Detail as CIEBatchDetailRecord).IndividualId, IsCheck = true, SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "IndividualName",
                FieldValue = (Detail as CIEBatchDetailRecord).IndividualName, IsCheck = true, SequenceNumber = 0} });

            return new ParseResult<Addenda> { Errors = ParsedAddendaSegments.Errors, Result = this };
        }
    }
}
