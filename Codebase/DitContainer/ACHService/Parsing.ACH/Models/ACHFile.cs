﻿using CommonObjects;
using CommonObjects.Abstractions;
using CommonObjects.Extensions;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using Deluxe.Payments.Parsing.ACH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class ACHFile : IParsable<ParseResult<ACHFile>>
    {
        private const string PARSERERROR = "Error parsing document";
        private int _lineNumber;
        private Models.Batch _currentBatch;
        private BatchDetailRecord _currentBatchDetail;
        private IRulesProvider _rulesProvider;

        public ACHFile(string fullPathFileName, IRulesProvider rulesProvider)
        {
            if (string.IsNullOrEmpty(fullPathFileName)) throw new ArgumentException("Invalid ach file.");
            
            Setup(new List<string>(System.IO.File.ReadAllLines(fullPathFileName)), rulesProvider);
        }

        public ACHFile(IList<string> rawDatalines, IRulesProvider rulesProvider)
        {
            Setup(rawDatalines, rulesProvider);
        }

        public IList<string> RawDataLines { get; private set; }
        public List<string> Messages { get; private set; } = new List<string>();
        public FileHeaderRecord FileHeaderRecord { get; private set; }

        public IList<Models.Batch> Batches { get; private set; } = new List<Models.Batch>();
        public FileControlRecord FileFooterRecord { get; private set; }
        public string FileSignature { get; private set; }
        public string FileHash => FileSignature.GetHash();
        public IList<string> BlockFiller { get; private set; } = new List<string>();
        public IList<AddendaRule> Rules { get; private set; } = new List<AddendaRule>();
        public IList<ParseResult<BatchAddenda>> ParsedBatchAddenda { get; private set; } = new List<ParseResult<BatchAddenda>>();

        public ParseResult<ACHFile> Parse()
        {
            try
            {
                _lineNumber = 1;
                RawDataLines.ToList().ForEach(line =>
                {
                    var achFileRecord = TryToDetermineRecordType(line).GetResult(HandleInvalidRecord);
                    if (achFileRecord is FileHeaderRecord) HandleFileHeaderRecord(achFileRecord as FileHeaderRecord);
                    if (achFileRecord is BatchHeaderRecord) HandleBatchHeaderRecord(achFileRecord as BatchHeaderRecord);
                    if (achFileRecord is BatchDetailRecord) HandleBatchDetailRecord(achFileRecord as BatchDetailRecord);
                    if (achFileRecord is AddendaRecord) HandleAddendaRecord(achFileRecord as AddendaRecord);
                    if (achFileRecord is BatchControlRecord) HandleBatchControlRecord(achFileRecord as BatchControlRecord);
                    if (achFileRecord is FileControlRecord) HandleFileControlRecord(achFileRecord as FileControlRecord);

                    _lineNumber++;
                });

                ValidateParsedFile();

                if (Messages.Any()) return new ParseResult<ACHFile> { Errors = Messages, Result = this };

                ParseBatchAddenda();
                Messages.AddRange(ParsedBatchAddenda.SelectMany(addenda => addenda.Errors).Where(errors => errors.Length > 0).ToList());
            }
            catch(Exception ex)
            {
                Messages.Add(ex.Message);
            }

            return new ParseResult<ACHFile> { Errors = Messages, Result = this };
        }

        private void ParseBatchAddenda()
        {
            //get list of rules
            var rulesparm = Batches.SelectMany(batches => batches.BatchDetail).ToDistinctAccounts();

            Rules = _rulesProvider.GetRules(rulesparm.ToList());
            var parsedBatchAddendaList = new List<ParseResult<BatchAddenda>>();
            Batches.ToList().ForEach(batch =>
            {
                ParsedBatchAddenda.Add(new BatchAddenda(batch, Rules).Parse());
            });
        }

        private void Setup(IList<string> rawDataLines, IRulesProvider rulesProvider)
        {
            //what other checks??
            if (rawDataLines == null) throw new ArgumentException("Invalid rawDatalines.");
            if (rulesProvider == null) throw new ArgumentException("Invalid rulesProvider.");

            RawDataLines = rawDataLines;
            _rulesProvider = rulesProvider;
        }

        private void ValidateParsedFile()
        {
            if (FileFooterRecord == null)
            {
                Messages.Add("Footer record does not exist in file");
                return;
            }
            if (FileFooterRecord.BatchCount != Batches.Count) Messages.Add("Footer count and Batches found are not equal");
        }

        private PossibleResult<ACHFileRecord> TryToDetermineRecordType(string data)
        {
            if (string.IsNullOrEmpty(data)) return Result.None<ACHFileRecord>();
            if (data.Length == 0) return Result.None<ACHFileRecord>();

            int recordType;
            if (!int.TryParse(data.Substring(0, 1), out recordType)) return Result.None<ACHFileRecord>();

            if (recordType == 1) return Result.Real(new FileHeaderRecord(data, _lineNumber) as ACHFileRecord);
            if (recordType == 5) return Result.Real(new BatchHeaderRecord(data, _lineNumber) as ACHFileRecord);
            if (recordType == 6) return TryToDetermineBatchDetailRecordType(data);
            if (recordType == 7)
            {
                return _currentBatch.Header.IsIat()
                    ? Result.Real(new IATAddendaRecord(data, _lineNumber) as ACHFileRecord)
                    : Result.Real(new AddendaRecord(data, _lineNumber) as ACHFileRecord);
            }
            if (recordType == 8) return Result.Real(new BatchControlRecord(data, _lineNumber) as ACHFileRecord);
            if (recordType == 9) return Result.Real(new FileControlRecord(data, _lineNumber) as ACHFileRecord);

            Messages.Add($"Unrecognized record type: {recordType}.");
            return Result.None<ACHFileRecord>();
        }

        private PossibleResult<ACHFileRecord> TryToDetermineBatchDetailRecordType(string data)
        {
            if (string.IsNullOrEmpty(data)) return Result.None<ACHFileRecord>();
            if (data.Length == 0) return Result.None<ACHFileRecord>();
            if (_currentBatch == null || _currentBatch.Header == null)
            {
                Messages.Add("Trying to parse a batch detail record without a batch header.");
                return Result.None<ACHFileRecord>();
            }

            if (_currentBatch.Header.IsCtx())
                return Result.Real(new CTXBatchDetailRecord(data, _lineNumber, _currentBatch.Header) as ACHFileRecord);

            if (_currentBatch.Header.IsCcd())
                return Result.Real(new CCDBatchDetailRecord(data, _lineNumber, _currentBatch.Header) as ACHFileRecord);

            if (_currentBatch.Header.IsCie())
                return Result.Real(new CIEBatchDetailRecord(data, _lineNumber, _currentBatch.Header) as ACHFileRecord);

            if (_currentBatch.Header.IsIat())
                return Result.Real(new IATBatchDetailRecord(data, _lineNumber, _currentBatch.Header) as ACHFileRecord);

            if (_currentBatch.Header.IsPpd())
                return Result.Real(new PPDBatchDetailRecord(data, _lineNumber, _currentBatch.Header) as ACHFileRecord);

            Messages.Add($"Batch Type: {_currentBatch.Header.EntryClassCode} not recognized.");
            return Result.None<ACHFileRecord>();
        }

        private ACHFileRecord HandleInvalidRecord()
        {
            return new InvalidACHFileRecord(RawDataLines[_lineNumber - 1], _lineNumber);
        }

        private void HandleAddendaRecord(AddendaRecord addendaRecord)
        {
            AddendaRecord _addendaRecord = addendaRecord; 
            if (_currentBatch.Header.IsIat())
                _addendaRecord = addendaRecord as IATAddendaRecord;

            var parsed = _addendaRecord.Parse();
            Messages.AddRange(parsed.Errors);
            _currentBatchDetail.Addenda.Add(parsed.Result as AddendaRecord);
        }

        private void HandleFileControlRecord(FileControlRecord fileControlRecord)
        {
            var parsed = fileControlRecord.Parse();
            Messages.AddRange(parsed.Errors);
            if (FileFooterRecord != null)
            {
                BlockFiller.Add(fileControlRecord.RawData);
                return;
            }

            FileFooterRecord = parsed.Result as FileControlRecord;
            AddFileSignature();
        }

        private void AddFileSignature()
        {
            var filesignature = new StringBuilder();
            filesignature.Append(FileFooterRecord.BatchCount);
            filesignature.Append(FileFooterRecord.BlockCount);
            filesignature.Append(FileFooterRecord.TotalCount);
            filesignature.Append(FileFooterRecord.TotalHash);
            filesignature.Append(FileFooterRecord.FileDebitAmount);
            filesignature.Append(FileFooterRecord.FileCreditAmount);
            FileSignature = filesignature.ToString();
        }

        private void HandleBatchControlRecord(BatchControlRecord batchControlRecord)
        {
            var parsed = batchControlRecord.Parse();
            Messages.AddRange(parsed.Errors);
            _currentBatch.Footer = parsed.Result as BatchControlRecord;
            Batches.Add(_currentBatch);
        }

        private void HandleBatchDetailRecord(BatchDetailRecord batchDetailRecord)
        {
            _currentBatch.BatchDetail = _currentBatch.BatchDetail ?? new List<BatchDetailRecord>();
            var parsed = batchDetailRecord.Parse();
            Messages.AddRange(parsed.Errors);
            _currentBatchDetail = parsed.Result as BatchDetailRecord;
            _currentBatch.BatchDetail.Add(_currentBatchDetail);
        }

        private void HandleBatchHeaderRecord(BatchHeaderRecord batchHeaderRecord)
        {
            var parsed = batchHeaderRecord.Parse();
            Messages.AddRange(parsed.Errors);
            _currentBatch = new Models.Batch { Header = parsed.Result as BatchHeaderRecord };
        }

        private void HandleFileHeaderRecord(FileHeaderRecord fileHeaderRecord)
        {
            var parsed = fileHeaderRecord.Parse();
            FileHeaderRecord = parsed.Result as FileHeaderRecord;
            Messages.AddRange(parsed.Errors);
        }
    }
}
