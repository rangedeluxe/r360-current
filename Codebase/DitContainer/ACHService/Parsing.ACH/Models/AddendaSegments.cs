﻿using CommonObjects;
using CommonObjects.Abstractions;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class AddendaSegments : IParsable<ParseResult<AddendaSegments>>
    {
        private readonly ParseResult<AddendaSegments> _parsedAddendaSegments;

        public AddendaSegments(BatchDetailRecord batchDetailRecord, IList<AddendaRule> rules, string segmentDelimiter)
        {
            Detail = batchDetailRecord ?? throw new ArgumentNullException($"Failed to create addenda segments.  Batch detail record is invalid.");
            Rules = rules ?? new List<AddendaRule>();
            //make sure the rules are just for this ABA/DDA
            Rules =
                Rules.Where(rule => 
                    string.Equals(rule.ABA, Detail.ABA, StringComparison.InvariantCulture) &&
                    string.Equals(rule.DDA.TrimLeadingZeros(), Detail.DDA.TrimLeadingZeros(), StringComparison.InvariantCulture)).ToList();

            RawAddenda = Detail.Addenda?.Select(addenda => addenda.Data).ToList() ?? new List<string>();
            FullAddenda = string.Join(string.Empty, RawAddenda);
            SegmentDelimiter = segmentDelimiter;
            _parsedAddendaSegments = new ParseResult<AddendaSegments> { Errors = new List<string>(), Result = this };
        }

        public IList<Field> PaymentAddendaFields { get; private set; } = new List<Field>();
        public IList<Field> DocumentAddendaFields { get; private set; } = new List<Field>();
        public BatchDetailRecord Detail { get; private set; }
        public IList<AddendaRule> Rules { get; private set; }
        public IList<string> RawAddenda { get; private set; }
        public string FullAddenda { get; private set; }
        public string SegmentDelimiter { get; private set; }
        public string FieldDelimiter { get; private set; }
        public char MatchedDelimiter { get; private set; }
        public IList<ParseResult<Segment>> ParsedSegments { get; private set; } = new List<ParseResult<Segment>>();

        public ParseResult<AddendaSegments> Parse()
        {
            if (!Rules.Exist()) return _parsedAddendaSegments;
            if (RawAddenda.Count == 0) return _parsedAddendaSegments;

            ParsedSegments = TryToParseSegments().GetResult(new List<ParseResult<Segment>>());
            if (HaveParsingErrors()) return _parsedAddendaSegments;

            var parsedAddendaFields =
                ParsedSegments
                .MapTo()
                .GetResult(HandleUnknownAddendaFieldsBuilder)
                .Build(ParsedSegments, Rules);
            
            PaymentAddendaFields = parsedAddendaFields.PaymentAddendaFields;
            DocumentAddendaFields = parsedAddendaFields.DocumentAddendaFields;

            return _parsedAddendaSegments;
        }

        private IAddendaFieldsBuilder HandleUnknownAddendaFieldsBuilder()
        {
            return new GenericAddendaFieldsBuilder();
        }

        private bool HaveParsingErrors()
        {
            return ParsedSegments.Any(e => e.Errors.Count > 0) || _parsedAddendaSegments.Errors.Count > 0;
        }

        private PossibleResult<IList<ParseResult<Segment>>> TryToParseSegments()
        {
            //Make sure this is an 820 addenda
            if (!FullAddenda.ToUpper().StartsWith("ISA"))
            { //Not an 820 addenda, drop out without any errors
                return Result.None<IList<ParseResult<Segment>>>();
            }

            //try and find the segment delimiter in the full addenda
            var match = Regex.Match(FullAddenda, $"([{SegmentDelimiter}])GS", RegexOptions.IgnoreCase);
            if (string.IsNullOrEmpty(match.Value))//if the segment delimiter isn't found, kick out
            {
                _parsedAddendaSegments.Errors.Add($"Segment delimiter: '{SegmentDelimiter}' was not found in addenda: '{FullAddenda}'.");
                return Result.None<IList<ParseResult<Segment>>>();
            }

            MatchedDelimiter = match.Value[0];
            FieldDelimiter = Rules[0].FieldDelimiter;
            var segments = FullAddenda.Split(MatchedDelimiter);
            IList<ParseResult<Segment>> parsedSegments = new List<ParseResult<Segment>>();
            segments.ToList().ForEach(segmentRawData =>
            {
                parsedSegments.Add(new Segment(segmentRawData, FieldDelimiter).Parse());
            });

            return Result.Real(parsedSegments);
        }
    }
}
