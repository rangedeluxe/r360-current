﻿using CommonObjects;
using CommonObjects.Extensions;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class FileControlRecord : ACHFileRecord
    {
        private const string DEFAULT_ERROR = "Error parsing file control record value:";

        public FileControlRecord(string data, int lineNumber) : base(data, lineNumber)
        {
            RecordType = 9;
            ConfirmRecordType();
        }
       
        public int BatchCount { get; private set; }
        public int BlockCount { get; private set; }
        public int TotalCount { get; private set; }
        public long TotalHash { get; private set; }
        public string FileDebitAmount { get; private set; }
        public string FileCreditAmount { get; private set; }
        public string ReservedData { get; private set; }

        public override ParseResult<ACHFileRecord> Parse()
        {
            BatchCount = RawData.Substring(1, 6).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: BatchCount", Messages);
            BlockCount = RawData.Substring(7, 6).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: TotalCount", Messages);
            TotalCount = RawData.Substring(13, 8).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: EntryCount", Messages);
            TotalHash = RawData.Substring(21, 10).TryParseLong($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: TotalHash", Messages);
            FileDebitAmount = RawData.Substring(31, 10) + "." + RawData.Substring(41, 2);
            FileCreditAmount = (RawData.Substring(43, 10) + "." + RawData.Substring(53, 2));

            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }
    }
}
