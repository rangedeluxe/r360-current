﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    internal class GenericAddendaFieldsBuilder : IAddendaFieldsBuilder
    {
        public AddendaFields Build(IList<ParseResult<Segment>> parsedSegments, IList<AddendaRule> rules)
        {
            return new AddendaFields { PaymentAddendaFields = new List<Field>(), DocumentAddendaFields = new List<Field>()};
        }
    }
}
