﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class CCDAddenda : Addenda
    {
        public CCDAddenda(BatchHeaderRecord batchHeaderRecord, BatchDetailRecord batchDetailRecord, 
            BatchControlRecord batchControlRecord, IList<AddendaRule> rules) 
            : base(batchHeaderRecord, batchDetailRecord, batchControlRecord, rules)
        {
            ValidateDetailRecordType<CCDBatchDetailRecord>();
        }

        public override ParseResult<Addenda> Parse()
        {
            ParseAddendaSegments();

            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "CompanyData",
                FieldValue = Header.CompanyData, IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "DescriptiveDate",
                FieldValue = Header.DescriptiveDate, IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "IndividualID",
                FieldValue = (Detail as CCDBatchDetailRecord).IndividualId, IsCheck = true, SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "ReceivingCompany",
                FieldValue = (Detail as CCDBatchDetailRecord).ReceivingCompany, IsCheck = true, SequenceNumber = 0} });

            return new ParseResult<Addenda> { Errors = ParsedAddendaSegments.Errors, Result = this };
        }
    }
}
