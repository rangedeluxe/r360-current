﻿using CommonObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class AddendaFields
    {
        public IList<Field> PaymentAddendaFields { get; set; } = new List<Field>();
        public IList<Field> DocumentAddendaFields { get; set; } = new List<Field>();
    }
}
