﻿using CommonObjects;
using CommonObjects.Abstractions;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class BatchAddenda : IParsable<ParseResult<BatchAddenda>>
    {
        private readonly ParseResult<BatchAddenda> _parsedBatchAddenda;

        public BatchAddenda(Batch batch, IList<AddendaRule> rules)
        {
            if (batch == null) throw new ArgumentException("Must provide a valid batch.");
            Batch = batch;
            Rules = rules ?? new List<AddendaRule>();
            _parsedBatchAddenda = new ParseResult<BatchAddenda> { Errors = new List<string>(), Result = this };
            ParsedAddenda = new List<ParseResult<Addenda>>();
        }

        public Batch Batch { get; private set; }
        public IList<AddendaRule> Rules { get; set; }
        public IList<ParseResult<Addenda>> ParsedAddenda { get; private set; }

        public ParseResult<BatchAddenda> Parse()
        {
            
            if (!Rules.Exist()) return _parsedBatchAddenda;

            TryParse(Batch);

            _parsedBatchAddenda.Result = this;
            _parsedBatchAddenda.Errors = new List<string>(ParsedAddendaErrors());
            return _parsedBatchAddenda;
        }

        private IList<string> ParsedAddendaErrors()
        {
            var addendaErrors = ParsedAddenda.Select(x => x.Errors).FirstOrDefault();
            if (addendaErrors == null) return new List<string>();
            return addendaErrors;
        }

        private void Append(ParseResult<Addenda> parsedAddenda)
        {
            ParsedAddenda.Add(parsedAddenda);
        }

        private void TryParse(Batch batch)
        {
            batch.BatchDetail.ToList().ForEach(detail =>
            {
                TryParse(batch.Header, detail, batch.Footer);
            });
        }

        private void TryParse(BatchHeaderRecord header, BatchDetailRecord detail, BatchControlRecord control)
        {
            if (detail is CTXBatchDetailRecord) Append(new CTXAddenda(header, detail, control, Rules).Parse());
            if (detail is CCDBatchDetailRecord) Append(new CCDAddenda(header, detail, control, Rules).Parse());
            if (detail is PPDBatchDetailRecord) Append(new PPDAddenda(header, detail, control, Rules).Parse());
            if (detail is CIEBatchDetailRecord) Append(new CIEAddenda(header, detail, control, Rules).Parse());
            if (detail is IATBatchDetailRecord) Append(new IATAddenda(header, detail, control, Rules).Parse());
        }
    }
}
