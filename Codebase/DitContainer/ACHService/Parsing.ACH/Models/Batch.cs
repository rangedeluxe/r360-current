﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System.Collections.Generic;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class Batch 
    {
        public BatchHeaderRecord Header { get; set; }
        public IList<BatchDetailRecord> BatchDetail { get; set; } = new List<BatchDetailRecord>();
        public BatchControlRecord Footer { get; set; }
    }
}
