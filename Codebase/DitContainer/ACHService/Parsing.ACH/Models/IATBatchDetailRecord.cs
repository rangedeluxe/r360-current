﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class IATBatchDetailRecord : BatchDetailRecord
    {
        public IATBatchDetailRecord(string data, int lineNumber, BatchHeaderRecord batchHeaderRecord) : 
            base(data, lineNumber, batchHeaderRecord)
        {
        }

        public override ParseResult<ACHFileRecord> Parse()
        {
            DDA = RawData.Substring(39, 35).Trim();
            AccountNumber = RawData.Substring(39, 35);
            OfacIndicator1 = RawData.Substring(76, 1);
            OfacIndicator2 = RawData.Substring(77, 1);

            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }

        public string OfacIndicator1 { get; private set; }
        public string OfacIndicator2 { get; private set; }
    }
}
