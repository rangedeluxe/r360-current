﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class NoAddenda : Addenda
    {
        public NoAddenda(BatchHeaderRecord batchHeaderRecord, BatchDetailRecord batchDetailRecord,
            BatchControlRecord batchControlRecord, IList<AddendaRule> rules)
            : base(batchHeaderRecord, batchDetailRecord, batchControlRecord, rules)
        { }

        public override ParseResult<Addenda> Parse()
        {
            return new ParseResult<Addenda> { Errors = ParsedAddendaSegments.Errors, Result = this };
        }
    }
}
