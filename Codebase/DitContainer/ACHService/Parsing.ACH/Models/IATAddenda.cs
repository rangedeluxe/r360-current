﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System.Collections.Generic;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class IATAddenda : Addenda
    {
        public IATAddenda(BatchHeaderRecord batchHeaderRecord, BatchDetailRecord batchDetailRecord,
            BatchControlRecord batchControlRecord, IList<AddendaRule> rules) 
            : base(batchHeaderRecord, batchDetailRecord, batchControlRecord, rules)
        {
            ValidateDetailRecordType<IATBatchDetailRecord>();
        }

        public override ParseResult<Addenda> Parse()
        {
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "ForeignExchangeIndicator",
                FieldValue = Header.ForeignExchangeIndicator.ToString(), IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "ForeignExchangeRefIndicator",
                FieldValue = Header.ForeignExchangeRefIndicator.ToString(), IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "ForeignExchangeReference",
                FieldValue = Header.ForeignExchangeReference.ToString(), IsCheck = true,SequenceNumber = 0}});
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "IsoDestinationCountryCode",
                FieldValue = Header.IsoDestinationCountryCode.ToString(), IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "OriginatingCurrencyCode",
                FieldValue = Header.OriginatingCurrencyCode.ToString(), IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "DestinationCurrencyCode",
                FieldValue = Header.DestinationCurrencyCode.ToString(), IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "OFACIndicator1",
                FieldValue = (Detail as IATBatchDetailRecord).OfacIndicator1, IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "OFACIndicator2",
                FieldValue = (Detail as IATBatchDetailRecord).OfacIndicator2, IsCheck = true,SequenceNumber = 0}});

            return new ParseResult<Addenda> { Errors = ParsedAddendaSegments.Errors, Result = this };
        }
    }
}
