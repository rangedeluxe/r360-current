﻿using CommonObjects;
using CommonObjects.Extensions;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class BatchHeaderRecord : ACHFileRecord
    {
        private const string DEFAULT_ERROR = "Error Parsing Batch Header";

        public BatchHeaderRecord(string data, int lineNumber) : base(data, lineNumber)
        {
            RecordType = 5;
            ConfirmRecordType();
        }

        public override ParseResult<ACHFileRecord> Parse()
        {
            var effectiveDate = RawData.Substring(69, 6).TryParseFormattedDate($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: EffectiveDate", Messages);
            var settlementDate = RawData.Substring(75, 3) == "000" ? effectiveDate :
                effectiveDate.TryParseJulianDate(RawData.Substring(75, 3), $"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: SettlementDate", Messages);

            ServiceClass = RawData.Substring(1, 3).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: ServiceClass", Messages);
            CompanyName = RawData.Substring(4, 16);
            CompanyData = RawData.Substring(20, 20);
            CompanyId = RawData.Substring(40, 10);
            EntryClassCode = RawData.Substring(50, 3);
            EntryDescription = RawData.Substring(53, 10);
            DescriptiveDate = RawData.Substring(63, 6);
            ForeignExchangeIndicator = RawData.Substring(20, 2);
            ForeignExchangeRefIndicator = RawData.Substring(22, 1);
            ForeignExchangeReference = RawData.Substring(23, 15);
            IsoDestinationCountryCode = RawData.Substring(38, 2);
            OriginatingCurrencyCode = RawData.Substring(63, 3);
            DestinationCurrencyCode = RawData.Substring(66, 3);
            EffectiveDate = effectiveDate;
            SettlementDate = settlementDate;
            OriginatorStatus = RawData.Substring(78, 1);
            ACHBatchNumber = RawData.Substring(87, 7).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: ACHBatchNumber", Messages);

            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }

        public int ServiceClass { get; private set; }
        public string CompanyName { get; private set; }
        public string CompanyData { get; private set; }
        public string CompanyId { get; private set; }
        public string EntryClassCode { get; private set; }
        public string EntryDescription { get; private set; }
        public string DescriptiveDate { get; private set; }
        public DateTime EffectiveDate { get; private set; }
        public DateTime SettlementDate { get; private set; }
        public string OriginatorStatus { get; private set; }
        public int ACHBatchNumber { get; private set; }

        //The following fields are part of the IAT batch header
        public string OriginatingCurrencyCode { get; set; }
        public string DestinationCurrencyCode { get; set; }
        public string ForeignExchangeIndicator { get; set; }
        public string ForeignExchangeRefIndicator { get; set; }
        public string ForeignExchangeReference { get; set; }
        public string IsoDestinationCountryCode { get; set; }
    }
}
