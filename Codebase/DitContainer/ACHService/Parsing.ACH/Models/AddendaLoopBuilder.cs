﻿using CommonObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class AddendaLoopBuilder
    {
        private IList<ParseResult<Segment>> _parsedSegments;
        private IList<IList<ParseResult<Segment>>> _loops;
        private IList<ParseResult<Segment>> _currentLoop;
        private string _identifier;

        public IList<IList<ParseResult<Segment>>> Build(AddendaSegments addendaSegments, string identifier)
        {
            return Build(addendaSegments.ParsedSegments, identifier);
        }

        public IList<IList<ParseResult<Segment>>> Build(IList<ParseResult<Segment>> parsedSegments, string identifier)
        {
            ValidateIdentifier(identifier);

            _parsedSegments = parsedSegments ?? new List<ParseResult<Segment>>();
            _currentLoop = new List<ParseResult<Segment>>();
            _loops = new List<IList<ParseResult<Segment>>>();

            _parsedSegments.ToList().ForEach(parsedSegment =>
            {
                if (IsStartOfLoop(parsedSegment.Result?.Identifier))
                {
                    StartLoop(parsedSegment);
                    return;
                }

                AddToLoop(parsedSegment);

            });

            FinishCurrentLoop();

            return _loops;
        }

        private void ValidateIdentifier(string identifier)
        {
            if (string.IsNullOrEmpty(identifier))
                throw new ArgumentException($"Invalid segment identifier passed in to {nameof(AddendaLoopBuilder)}.");

            _identifier = identifier;
        }

        private void StartLoop(ParseResult<Segment> parsedSegment)
        {
            //finish current loop if there is one
            FinishCurrentLoop();
            _currentLoop = new List<ParseResult<Segment>>
            {
                parsedSegment
            };

        }

        private bool IsStartOfLoop(string identifier)
        {
            return string.Equals(identifier, _identifier, StringComparison.InvariantCultureIgnoreCase);
        }

        private void FinishCurrentLoop()
        {
            if (_currentLoop.Any()) _loops.Add(_currentLoop);
        }

        private void AddToLoop(ParseResult<Segment> parsedSegment)
        {
            if (!_currentLoop.Any()) return;

            _currentLoop.Add(parsedSegment);

        }
    }
}
