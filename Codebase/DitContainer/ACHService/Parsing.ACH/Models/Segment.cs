﻿using CommonObjects;
using CommonObjects.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class Segment : IParsable<ParseResult<Segment>>
    {

        public Segment(string rawSegmentData, string fieldDelimiter)
        {
            FieldDelimiter = fieldDelimiter;
            RawSegmentData = rawSegmentData;
            Messages = new List<string>();
            Fields = new List<string>();
            //ParsedSegmentFields = new List<Field>();
        }

        public string Identifier { get; private set; }
        public string Qualifier { get; private set; }
        public string FieldDelimiter { get; private set; }
        public string RawSegmentData { get; private set; }
        public IList<string> Messages { get; private set; }
        public IList<string> Fields { get; private set; }

        public virtual ParseResult<Segment> Parse()
        {
            return new ParseResult<Segment> { Errors = Messages, Result = TryParse().GetResult(HandleUnknownSegment) };
        }

        private Segment HandleUnknownSegment()
        {
            return new UnknownSegment(RawSegmentData, FieldDelimiter);
        }

        private PossibleResult<Segment> TryParse()
        {
            if (string.IsNullOrEmpty(RawSegmentData)) return Result.None<Segment>();

            Fields = RawSegmentData.Split(FieldDelimiter);

            if (Fields.Count() < 2) return Result.None<Segment>();

            Identifier = Fields[0];
            Qualifier = Fields[1];

            return Result.Real(this);
        }
    }
}
