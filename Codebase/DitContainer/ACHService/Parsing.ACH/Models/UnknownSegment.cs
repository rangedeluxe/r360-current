﻿using CommonObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class UnknownSegment : Segment
    {
        public UnknownSegment(string rawSegmentData, string fieldDelimiter)
            : base(rawSegmentData, fieldDelimiter)
        { }

        public override ParseResult<Segment> Parse()
        {
            return new ParseResult<Segment> { Errors = Messages, Result = this };
        }
    }
}
