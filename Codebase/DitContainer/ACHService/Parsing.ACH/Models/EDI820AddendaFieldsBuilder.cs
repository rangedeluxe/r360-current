﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class EDI820AddendaFieldsBuilder : IAddendaFieldsBuilder
    {
        private AddendaFields _addendaFields;
        private IList<AddendaRule> _rules;
        private bool _paymentLoopActive = false;
        private bool _stubLoopActive = false;
        private AddendaRule _currentREFHeaderSelected;
        private AddendaRule _currentREFDetailSelected;
        private bool _ENTLoopActive = false;
        private int _sequenceNumber = 0;
        private bool _firstENTLoop = false;
        private enum RuleType
        {
            Payment,
            Stub,
            All
        }

        public AddendaFields Build(IList<ParseResult<Segment>> parsedSegments, IList<AddendaRule> rules)
        {
            _addendaFields = new AddendaFields { PaymentAddendaFields = new List<Field>(), DocumentAddendaFields = new List<Field>() };
            _rules = rules ?? new List<AddendaRule>();

            if (parsedSegments == null) return _addendaFields;

            parsedSegments.ToList().ForEach(parsedSegment =>
            {
                ProcessSegment(parsedSegment.Result);
            });

            return _addendaFields;
        }

        private void ProcessSegment(Segment segment)
        {
            if (segment.IsBPRSegment()) StartPaymentLoop(segment);
            if (segment.IsENTSegment()) StartStubLoop(segment);
            if (segment.IsRMRSegment()) StartStubLoop(segment);
            if (segment.IsLXSegment()) StartStubLoop(segment);
            if (segment.IsRYLSegment()) StartStubLoop(segment);

            if (_paymentLoopActive) ProcessPaymentSegment(segment);

            if (_stubLoopActive) ProcessStubSegment(segment);
        }

        private void ProcessPaymentSegment(Segment segment)
        {
            if (_paymentLoopActive) HandlePaymentLoops(segment);
        }

        private void ProcessStubSegment(Segment segment)
        {
            if (_stubLoopActive) HandleStubLoops(segment);
        }

        private void HandlePaymentLoops(Segment segment)
        {
            if (segment.IsREFSegment()) { ProcessPaymentREFLoop(segment); return; }
            if (segment.IsDTMSegment()) { AppendFields(segment, RuleType.Payment); return; }

            AppendFields(segment, RuleType.All);
        }

        private void HandleStubLoops(Segment segment)
        {
            if (segment.IsNM1Segment()) { ProcessStubNM1Loop(segment);  return; }
            if (segment.IsREFSegment()) { ProcessStubREFLoop(segment);  return; }
            if (segment.IsDTMSegment()) { AppendFields(segment, RuleType.Stub); return; }

            AppendFields(segment, RuleType.All);
            if (segment.IsRMRSegment()) _ENTLoopActive = false;
        }

        private void ProcessStubNM1Loop(Segment segment)
        {
            var nm1DetailRules = LookupRules(segment, RuleType.Stub)
                .Where(rule => string.Equals(rule.FieldName, "nm1name", StringComparison.InvariantCultureIgnoreCase))
                .ToList();

            if (!nm1DetailRules.Any()) return;        
            
           
            AppendDocumentAddendaField(nm1DetailRules.First(), segment);    //add NM1
            //<<TO DO set _NM1Name = the value of NM1Name - so we can add to all RMR's in the loop >>

            

        }
        private void ProcessPaymentREFLoop(Segment segment)
        {
            if (HaveAlreadySelectedREFHeaderForThisLoop()) return;

            var refHeaderRules = LookupRules(segment, RuleType.Payment)
                .Where(rule => string.Equals(rule.FieldName, "refheaderqualifier", StringComparison.InvariantCultureIgnoreCase))
                .ToList();

            if (!refHeaderRules.Any()) return;

            var rules = LookupRules(segment, RuleType.Payment)
                .Where(rule => string.Equals(rule.SegmentQualifier, segment.Qualifier));

            if (!rules.Any()) return;//if no matching ref detail qualifier rules, just return

            AppendPaymentAddendaField(refHeaderRules.First(), segment);//add REFDetailQualifier
            AppendPaymentAddendaField(rules.First(), segment);//if there are take the first matching rule

            _currentREFHeaderSelected = rules.First();
        }

        private bool HaveAlreadySelectedREFHeaderForThisLoop()
        {
            return _currentREFHeaderSelected != null;
        }

        private bool HaveAlreadySelectedREFDetailForThisLoop()
        {
            return _currentREFDetailSelected != null;
        }

        private void ProcessStubREFLoop(Segment segment)
        {
            if (HaveAlreadySelectedREFDetailForThisLoop()) return;

            var refDetailRules = LookupRules(segment, RuleType.Stub)
                .Where(rule => string.Equals(rule.FieldName, "refdetailqualifier", StringComparison.InvariantCultureIgnoreCase))
                .ToList();

            if (!refDetailRules.Any()) return;

            var rules = LookupRules(segment, RuleType.Stub)
                .Where(rule => string.Equals(rule.SegmentQualifier, segment.Qualifier));

            if (!rules.Any()) return;//if no matching ref detail qualifier rules, just return

            AppendDocumentAddendaField(refDetailRules.First(), segment);//add REFDetailQualifier
            AppendDocumentAddendaField(rules.First(), segment);//if there are take the first matching rule

            _currentREFDetailSelected = rules.First();
        }

        private void StartPaymentLoop(Segment segment)
        {
            _stubLoopActive = false;
            _currentREFHeaderSelected = null;

            var paymentRules = LookupRules(segment, RuleType.All);

            if (paymentRules.Any())
                _paymentLoopActive = true;
            else
                _paymentLoopActive = false;
        }

        private void StartStubLoop(Segment segment)
        {
            _paymentLoopActive = false;
            _currentREFDetailSelected = null;

            if (segment.IsENTSegment()) { HandleENTStubLoop(segment); }
            if (segment.IsLXSegment()) { HandleLXStubLoop(segment);}
            if (segment.IsRYLSegment()) { HandleRYLStubLoop(segment); }
            if (segment.IsRMRSegment()) { HandleRMRStubLoop(segment); }
        }

        private void HandleRMRStubLoop(Segment segment)
        {
            var stubRules = SegmentRules(segment);
            if (stubRules.Any())
                _stubLoopActive = true;
            else
                _stubLoopActive = false;
        }

        private void HandleENTStubLoop(Segment segment)
        {
            _stubLoopActive = true;
            _ENTLoopActive = true;
            if (!_firstENTLoop)
            { _firstENTLoop = true; }
            else
            { ++_sequenceNumber; }
            
                
        }

        private void HandleLXStubLoop(Segment segment)
        {
            _stubLoopActive = false;
        }

        private void HandleRYLStubLoop(Segment segment)
        {
            _stubLoopActive = false;
        }

        private void AppendFields(Segment segment, RuleType ruleType)
        {
            var matchedRules = LookupRules(segment, ruleType);
            if (!matchedRules.Any()) return; //skip to next loop
            if (matchedRules.Any(m => m.CreateNewRecord) && (!_ENTLoopActive)) ++_sequenceNumber;

            matchedRules.ToList().ForEach(matchedSegmentRule =>
            {
                if (segment.Fields.Count() > matchedSegmentRule.FieldNumber)
                    AppendAddendaField(matchedSegmentRule, segment);
            });
        }
        
        private void AppendAddendaField(AddendaRule matchedRule, Segment segment)
        {
            if (matchedRule.IsCheck)
                AppendPaymentAddendaField(matchedRule, segment);
            else
                AppendDocumentAddendaField(matchedRule, segment);
        }

        private IList<AddendaRule> LookupRules(Segment segment, RuleType ruleType)
        {
            if (ruleType is RuleType.All) return SegmentRules(segment);

            return SegmentRules(segment).Where(rule => rule.IsCheck == ruleType is RuleType.Payment ? true : false).ToList();
        }

        private IList<AddendaRule> SegmentRules(Segment segment)
        {
            return _rules.TryFindMatchedRules(segment.Fields.ToArray()).GetResult(new List<AddendaRule>());
        }
       
        private void AppendPaymentAddendaField(AddendaRule rule, Segment segment)
        {
            _addendaFields.PaymentAddendaFields.Add(new Field
            {
                FieldName = rule.FieldName,
                FieldValue = segment.Fields[rule.FieldNumber],
                SequenceNumber = _sequenceNumber,
                IsCheck = rule.IsCheck
            });
        }

        private void AppendDocumentAddendaField(AddendaRule rule, Segment segment)
        {
            _addendaFields.DocumentAddendaFields.Add(new Field
            {
                FieldName = rule.FieldName,
                FieldValue = segment.Fields[rule.FieldNumber],
                SequenceNumber = _sequenceNumber,
                IsCheck = rule.IsCheck
            });
        }
    }
}
