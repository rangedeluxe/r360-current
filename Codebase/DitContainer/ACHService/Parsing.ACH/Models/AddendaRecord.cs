﻿using CommonObjects;
using CommonObjects.Extensions;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class AddendaRecord : ACHFileRecord
    {
        private const string DEFAULT_ERROR = "Error parsing addenda record value:";

        public AddendaRecord(string data, int lineNumber) : base(data, lineNumber)
        {
            RecordType = 7;
            ConfirmRecordType();
        }

        public int AddendaTypeCode { get; set; }
        public string Data { get; set; }
        public int SequenceNumber { get; private set; }
        public int DetailSequenceNumber { get; set; }
        public string RawAddenda { get; set; }

        public override ParseResult<ACHFileRecord> Parse()
        {
            AddendaTypeCode = RawData.Substring(1, 2).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: AddendaTypeCode", Messages);
            Data = RawData.Substring(3, 80);
            SequenceNumber = RawData.Substring(83, 4).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: SequenceNumber", Messages);
            DetailSequenceNumber = RawData.Substring(87, 7).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: DetailSequenceNumber", Messages);
            RawAddenda = RawData;

            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }
    }
}
