﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class CIEBatchDetailRecord : BatchDetailRecord
    {
        public CIEBatchDetailRecord(string data, int lineNumber, BatchHeaderRecord batchHeaderRecord) : 
            base(data, lineNumber, batchHeaderRecord)
        {
        }

        public override ParseResult<ACHFileRecord> Parse()
        {
            IndividualName = RawData.Substring(39, 15);
            IndividualId = RawData.Substring(54, 22);

            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }

        public string IndividualId { get; private set; }
        public string IndividualName { get; private set; }
    }
}
