﻿using CommonObjects;
using CommonObjects.Extensions;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class BatchControlRecord : ACHFileRecord
    {
        private const string DEFAULT_ERROR = "Error parsing batch control record value:";

        public BatchControlRecord(string data, int lineNumber) : base(data, lineNumber)
        {
            RecordType = 8;
            ConfirmRecordType();
        }

        public string OriginatingDFI { get; private set; }
        public long TotalHash { get; private set; }
        public decimal TotalDebitAmount { get; private set; }
        public decimal TotalCreditAmount { get; private set; }

        public override ParseResult<ACHFileRecord> Parse()
        {
            OriginatingDFI = RawData.Substring(79, 8);
            TotalHash = RawData.Substring(10, 10).TryParseLong($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: TotalHash", Messages);
            TotalDebitAmount = $"{RawData.Substring(20, 10)}.{RawData.Substring(30, 2)}".TryParseDecimal($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: TotalDebitAmount", Messages);
            TotalCreditAmount = $"{RawData.Substring(32, 10)}.{RawData.Substring(42, 2)}".TryParseDecimal($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: TotalCreditAmount", Messages);

            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }
    }
}
