﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class PPDBatchDetailRecord : BatchDetailRecord
    {
        public PPDBatchDetailRecord(string data, int lineNumber, BatchHeaderRecord batchHeaderRecord) : 
            base(data, lineNumber, batchHeaderRecord)
        {
        }

        public override ParseResult<ACHFileRecord> Parse()
        {
            IndividualId = RawData.Substring(39, 15);
            IndividualName = RawData.Substring(54, 22);

            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }

        public string IndividualId { get; private set; }
        public string IndividualName { get; private set; }
    }
}
