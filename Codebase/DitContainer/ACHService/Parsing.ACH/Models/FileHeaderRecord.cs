﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class FileHeaderRecord : ACHFileRecord
    {
        public FileHeaderRecord(string data, int lineNumber) : base(data, lineNumber)
        {
            RecordType = 1;
            ConfirmRecordType();
        }

        public override ParseResult<ACHFileRecord> Parse()
        {
            //what do we need to parse in this one
            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }

    }
}
