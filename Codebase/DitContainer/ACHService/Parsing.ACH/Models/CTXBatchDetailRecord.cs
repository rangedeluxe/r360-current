﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class CTXBatchDetailRecord : BatchDetailRecord
    {
        public CTXBatchDetailRecord(string data, int lineNumber, BatchHeaderRecord batchHeaderRecord) :
            base(data, lineNumber, batchHeaderRecord)
        {

        }

        public override ParseResult<ACHFileRecord> Parse()
        {
            DiscretionaryData = RawData.Substring(76, 2);
            IndividualId = RawData.Substring(39, 15);
            ReceivingCompany = RawData.Substring(58, 16);

            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }

        public string IndividualId { get; private set; }
        public string ReceivingCompany { get; private set; }
        public string DiscretionaryData { get; private set; }
    }
}
