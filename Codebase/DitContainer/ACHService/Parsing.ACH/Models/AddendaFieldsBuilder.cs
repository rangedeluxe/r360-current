﻿using CommonObjects;
using CommonObjects.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class AddendaFieldsBuilder : IParsable<ParseResult<AddendaFields>>
    {
        private readonly IList<ParseResult<Segment>> _parsedSegments;
        private readonly IList<AddendaRule> _rules;
        private readonly IList<string> _loopIdentifiers;
        private readonly ParseResult<AddendaFields>_parsedAddendaFields;
        private readonly AddendaLoopBuilder _loopBuilder;
        private IList<IList<IList<ParseResult<Segment>>>> _loops;
        private int _sequenceNumber = 0;

        public AddendaFieldsBuilder(IList<ParseResult<Segment>> parsedSegments, IList<AddendaRule> rules)
        {
            _parsedSegments = parsedSegments ?? new List<ParseResult<Segment>>();
            _rules = rules ?? new List<AddendaRule>();
            _parsedAddendaFields = new ParseResult<AddendaFields>();
            _loopBuilder = new AddendaLoopBuilder();
            _loopIdentifiers = new List<string> { "rmr", "ent" };
            _loops = new List<IList<IList<ParseResult<Segment>>>>();
        }

        
        public ParseResult<AddendaFields> Parse()
        {
            //if (InvalidBPRSegment(parsedSegment.Result)) return;
            
            //_loopIdentifiers.ToList().ForEach(identifier =>
            //{
            //    _loops.Add(_loopBuilder.Build(_parsedSegments, identifier));
            //});
            if (_parsedSegments.Is820Addenda())
            _parsedSegments.ToList().ForEach(parsedSegment =>
            {
                //if (string.Equals(parsedSegment.Result.Identifier, "rmr", StringComparison.InvariantCultureIgnoreCase))
                //    ProcessRMRLoop(parsedSegment.Result.TakeWhile(x => x.Result.Identifier != "RMR").ToList());
                
                AppendFields(parsedSegment.Result);
            });

            return _parsedAddendaFields;
        }

        
        private IList<AddendaRule> SegmentRules(Segment segment)
        {
            return _rules.TryFindMatchedRules(segment.Fields.ToArray()).GetResult(new List<AddendaRule>());
        }

        private void AppendFields(Segment segment)
        {
            var matchedSegmentRules = SegmentRules(segment);
            if (!matchedSegmentRules.Any()) return; //skip to next loop
            if (matchedSegmentRules.Any(m => m.CreateNewRecord)) ++_sequenceNumber;

            matchedSegmentRules.ToList().ForEach(matchedSegmentRule =>
            {
                AppendRemittanceField(matchedSegmentRule, segment);
            });
        }

        private void AppendRemittanceField(AddendaRule rule, Segment segment)
        {
            if (rule.IsCheck)
                AppendPaymentRemittanceField(rule, segment);
            else
                AppendDocumentRemittanceField(rule, segment);
        }

        private void AppendPaymentRemittanceField(AddendaRule rule, Segment segment)
        {
            _parsedAddendaFields.Result.PaymentAddendaFields.Add(new Field
            {
                FieldName = rule.FieldName,
                FieldValue = segment.Fields[rule.FieldNumber],
                SequenceNumber = _sequenceNumber,
                IsCheck = rule.IsCheck
            });
        }

        private void AppendDocumentRemittanceField(AddendaRule rule, Segment segment)
        {
            _parsedAddendaFields.Result.DocumentAddendaFields.Add(new Field
            {
                FieldName = rule.FieldName,
                FieldValue = segment.Fields[rule.FieldNumber],
                SequenceNumber = _sequenceNumber,
                IsCheck = rule.IsCheck
            });
        }
    }
}
