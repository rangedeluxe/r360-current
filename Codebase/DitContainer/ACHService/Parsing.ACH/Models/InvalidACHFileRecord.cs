﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    internal class InvalidACHFileRecord : ACHFileRecord
    {
        public InvalidACHFileRecord(string data, int lineNumber) : base(data, lineNumber)
        {
        }

        public override ParseResult<ACHFileRecord> Parse()
        {
            //Do nothing because there is nothing to parse.  
            //This object is just reflective of a bad ACH file record.
            //Just return this object.
            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }
    }
}
