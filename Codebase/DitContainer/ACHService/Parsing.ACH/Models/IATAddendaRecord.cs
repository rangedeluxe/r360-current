﻿using CommonObjects;
using CommonObjects.Extensions;
using Deluxe.Payments.Parsing.ACH.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Models
{
    public class IATAddendaRecord : AddendaRecord
    {
        private const string DEFAULT_ERROR = "Error parsing IAT addenda record value:";

        public IATAddendaRecord(string data, int lineNumber) : base(data, lineNumber)
        {
            RecordType = 7;
            ConfirmRecordType();
        }


        public override ParseResult<ACHFileRecord> Parse()
        {
            AddendaTypeCode = RawData.Substring(1, 2).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: AddendaTypeCode", Messages);
            Data = RawData.Substring(3, 80);
            DetailSequenceNumber = RawData.Substring(87, 7).TryParseInt($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: DetailSequenceNumber", Messages);
            RawAddenda = RawData;

            return new ParseResult<ACHFileRecord>
            {
                Errors = Messages,
                Result = this
            };
        }
    }
}
