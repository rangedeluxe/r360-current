﻿using CommonObjects;
using CommonObjects.Abstractions;
using System;
using System.Collections.Generic;

namespace Deluxe.Payments.Parsing.ACH.Abstractions
{
    public abstract class ACHFileRecord : IParsable<ParseResult<ACHFileRecord>>
    {
        protected ACHFileRecord(string data, int lineNumber)
        {
            LineNumber = lineNumber;
            RawData = data;
            Messages = new List<string>();
            Validate();
        }

        public abstract ParseResult<ACHFileRecord> Parse();
        
        public int LineNumber { get; private set; }
        public string RawData { get; private set; }
        public int RecordType { get; protected set; }
        public IList<string> Messages { get; private set; }

        private void Validate()
        {
            if (string.IsNullOrEmpty(RawData)) throw new ArgumentException("Data is invalid", nameof(RawData));
            if (RawData.Length != 94) throw new ArgumentException($"Data is invalid. Invalid length - Line Number {LineNumber} ", nameof(RawData));
        }

        protected void ConfirmRecordType()
        {
            int value;
            int.TryParse(RawData.Substring(0, 1), out value);
            if (RecordType != value)
                throw new ArgumentException($"Invalid record type.  Expected: {RecordType} - Actual: {value}");

        }
    }
}
