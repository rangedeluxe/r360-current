﻿using CommonObjects;
using CommonObjects.Abstractions;
using Deluxe.Payments.Parsing.ACH.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Deluxe.Payments.Parsing.ACH.Abstractions
{
    public abstract class Addenda : IParsable<ParseResult<Addenda>>
    {
        public Addenda(BatchHeaderRecord batchHeaderRecord, BatchDetailRecord batchDetailRecord, 
             BatchControlRecord batchControlRecord, IList<AddendaRule> rules)
        {
            Header = batchHeaderRecord ?? throw new ArgumentNullException($"Failed to create addenda.  Batch header record is invalid.");
            Detail = batchDetailRecord ?? throw new ArgumentNullException($"Failed to create addenda.  Batch detail record is invalid.");
            Control = batchControlRecord ?? throw new ArgumentNullException($"Failed to create addenda.  Batch control record is invalid.");
            Rules = rules ?? new List<AddendaRule>();
            SegmentDelimiter = rules.Any() ? rules[0].SegmentDelimiter.Replace("\\", "\\\\") : string.Empty;
            ParsedAddendaSegments = new ParseResult<AddendaSegments> { Errors = new List<string>() };
            PaymentAddendaFields = new List<IList<Field>>();
            DocumentAddendaFields = new List<IList<Field>>();

            
        }

        public IList<IList<Field>> PaymentAddendaFields { get; private set; }
        public IList<IList<Field>> DocumentAddendaFields { get; private set; }
        public ParseResult<AddendaSegments> ParsedAddendaSegments { get; protected set; }
        public string SegmentDelimiter { get; private set; }
        public IList<AddendaRule> Rules { get; private set; }
        public BatchHeaderRecord Header { get; private set; }
        public BatchDetailRecord Detail { get; private set; }
        public BatchControlRecord Control { get; private set; }

        public abstract ParseResult<Addenda> Parse();

        protected void ValidateDetailRecordType<T>()
        {
            if (!(Detail is T))
                throw new ArgumentException($"Invalid batch detail record.");
        }

        protected void ParseAddendaSegments()
        {
            ParsedAddendaSegments = new AddendaSegments(Detail, Rules, SegmentDelimiter).Parse();
            //need to do anything here if there are errors?

            AppendPaymentFields();
            AppendDocumentFields();
        }

        private void AppendPaymentFields()
        {
            var groupedBySequence = ParsedAddendaSegments.Result.PaymentAddendaFields.GroupBy(x => x.SequenceNumber);
            PaymentAddendaFields = 
                ParsedAddendaSegments.Result.PaymentAddendaFields
                .GroupBy(field => field.SequenceNumber)
                .Select(groupOfFields => groupOfFields.ToList() as IList<Field>)
                .ToList();

            AppendDefaultPaymentFields();
        }

        private void AppendDocumentFields()
        {
            DocumentAddendaFields = 
                ParsedAddendaSegments.Result.DocumentAddendaFields
                .GroupBy(field => field.SequenceNumber)
                .Select(groupOfFields => groupOfFields.ToList() as IList<Field>)
                .ToList();
        }

        private void AppendDefaultPaymentFields()
        {
            PaymentAddendaFields.Add(new List<Field>{ new Field{FieldName = "ServiceClassCode",
                FieldValue = Header.ServiceClass.ToString(), IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "CompanyName",
                FieldValue = Header.CompanyName, IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "CompanyID",
                FieldValue = Header.CompanyId, IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field{FieldName = "StandardEntryClass",
                FieldValue = Header.EntryClassCode,  IsCheck = true,SequenceNumber = 0}});
            PaymentAddendaFields.Add(new List<Field> { new Field{FieldName = "EntryDescription",
                FieldValue = Header.EntryDescription, IsCheck = true, SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field> { new Field{FieldName = "EffectiveDate",
                FieldValue = Header.EffectiveDate.ToString("MM/dd/yyyy"), IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field> { new Field{FieldName = "SettlementDate",
                FieldValue = Header.SettlementDate.ToString("MM/dd/yyyy"), IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field> { new Field {FieldName = "OriginatingDFI",
                FieldValue = Control.OriginatingDFI, IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field> { new Field {FieldName = "ACHBatchNumber",
                FieldValue = Header.ACHBatchNumber.ToString(), IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field { FieldName = "ElectronicTransactionCode",
                FieldValue = Detail.TransactionCode, IsCheck = true,SequenceNumber = 0} });
            PaymentAddendaFields.Add(new List<Field>{new Field {FieldName = "TraceNumber",
                FieldValue = Detail.TraceNumber, IsCheck = true,SequenceNumber = 0} });
        }

        private bool HaveParseErrors()
        {
            return ParsedAddendaSegments.Errors.Count() > 0;
        }
    }
}
