﻿using Deluxe.Payments.Parsing.ACH.Models;
using System.Collections.Generic;
using CommonObjects.Extensions;
using System.Text;
using CommonObjects;
using CommonObjects.Abstractions;

namespace Deluxe.Payments.Parsing.ACH.Abstractions
{
    public abstract class BatchDetailRecord : ACHFileRecord
    {
        private const string DEFAULT_ERROR = "Error parsing batch detail record value:";

        public BatchDetailRecord(string data, int lineNumber, BatchHeaderRecord batchHeaderRecord) 
            : base(data, lineNumber)
        {
            RecordType = 6;
            Header = batchHeaderRecord;
            ConfirmRecordType();
            Validate();
        }

        public BatchHeaderRecord Header { get; private set; }
        public string TransactionCode { get; private set; }
        public string ABA { get; private set; }
        public string DDA { get; protected set; }
        public string AccountNumber { get; protected set; }
        public decimal Amount { get; private set; }
        public string TraceNumber { get; private set; }
        public IList<AddendaRecord> Addenda { get; private set; }
        public string TransactionSignature { get; private set; }
        public string TransactionHash => TransactionSignature.GetHash();

        private void Validate()
        {
            TransactionCode = RawData.Substring(1, 2);
            ABA = RawData.Substring(3, 9).Trim();
            DDA = RawData.Substring(12, 17).Trim();
            AccountNumber = RawData.Substring(12, 17);
            Amount = RawData.Substring(29, 10).TryParseDecimal($"{DEFAULT_ERROR}\tLine number: {LineNumber}\tField: Amount", Messages);
            TraceNumber = RawData.Substring(79, 15);
            Addenda = new List<AddendaRecord>();
            var sb = new StringBuilder();
            sb.Append(RecordType).Append(TransactionCode).
                Append(Amount).Append(AccountNumber).
                Append(TraceNumber);
            TransactionSignature = sb.ToString();
        }

    }
}
