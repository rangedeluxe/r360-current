﻿using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Deluxe.Payments.Parsing.ACH.Abstractions
{
    public interface IAddendaFieldsBuilder 
    {
        AddendaFields Build(IList<ParseResult<Segment>> parsedSegments, IList<AddendaRule> rules);
    }
}
