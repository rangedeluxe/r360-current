﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ACHConverter;
using ACHConverter.Utilities;
using ACHService.DataProviders;
using ACHService.Models;
using HostingBackgroundQueue;
using CommonObjects.Exceptions;
using System.Net.Http;
using ACHService.Abstractions;
using ACHService.Helpers;
using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Models;
using CommonObjects.Abstractions;

namespace ACHService.Controllers
{
	[Produces("application/json")]
	[Route("api/ACH")]
	public class ACHController : Controller
	{
		private readonly ILogger _logger;
		private readonly IBackgroundQueue _backgroundQueue;
		private readonly IDataImportTracking _importTracking;
		private readonly IAlertDataProvider _alertDataProvider;
		private readonly IDataImportQueue _dataImportQueue;
		private readonly IFileSystem _fileSystem;
		private readonly IRulesProvider _rulesProvider;
        private readonly IDatabaseHelper _databaseHelper;
        private const string SERVICE = "DIT SERVICE";
		private const string PROCESSING_EXCEPTION_EVENT_NAME = "ProcessingException";

		public ACHController(ILogger<ACHController> logger, IBackgroundQueue backgroundQueue,
			IDataImportTracking importTracking, IAlertDataProvider alertDataProvider, IDataImportQueue dataImportQueue,
			IFileSystem fileSystem, IRulesProvider rulesProvider, IDatabaseHelper databaseHelper)
		{
			_logger = logger;
			_backgroundQueue = backgroundQueue;
			_importTracking = importTracking;
			_alertDataProvider = alertDataProvider;
			_dataImportQueue = dataImportQueue;
			_fileSystem = fileSystem;
			_rulesProvider = rulesProvider;
            _databaseHelper = databaseHelper;
        }

		//GET: api/
		/// <summary>
		/// Gets the current status for the provided file id
		/// </summary>
		/// <param name="id">a guid for a previously posted file</param>
		/// <returns></returns>
		[HttpGet("{id}")]
		[ProducesResponseType(typeof(ServiceResponse<DITProcessingStatus>), 200)]
		[ProducesResponseType(typeof(ServiceResponse<DITProcessingStatus>), 500)]
		public IActionResult Get(Guid id)
		{
			try
			{
				var result = _importTracking.GetTrackingInfo(id);
				var response = new ServiceResponse<DitTrackingResponse>
				{
					Errors = new List<string>(),
					Data = new DitTrackingResponse
                    {
                        FileStatus = result?.FileStatus ?? DITProcessingStatus.Notfound,
                        BatchTrackingIds = result?.BatchTrackingIds
                    }
				};
				return new JsonResult(response)
				{
					StatusCode = StatusCodes.Status200OK,
				};

			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Exception on Get", ex.Message);
				var response = new ServiceResponse<DITProcessingStatus>
				{
					Errors = new List<string> { "An issue was encountered when attempting to complete the request." },
					Data = DITProcessingStatus.Error
				};
				return new JsonResult(response)
				{
					StatusCode = StatusCodes.Status500InternalServerError
				};
			}

		}

		// POST: api/ACH
		/// <summary>
		/// Creates a new import job
		/// </summary>
		/// <param name="importFileContext"></param>
		/// <returns>A newly created job tracking id</returns>
		[HttpPost]
		[ProducesResponseType(typeof(ServiceResponse<FileTrack>), 201)]
		[ProducesResponseType(typeof(ServiceResponse<FileTrack>), 500)]
		public async Task<IActionResult> PostAsync([FromForm]DataImportFile importFileContext)
		{
			try
			{
				if (importFileContext.FormFile == null)
				{
					return new JsonResult(new ServiceResponse<DataImportTracking>
					{
						Errors = new List<string> { "A FormFile is required. Please include it with the request" }
					})
					{ StatusCode = StatusCodes.Status400BadRequest };
				}
				
				var fileId = Guid.NewGuid();
				_importTracking.InsertTrackingInfo(new DataImportTracking
				{
					FileStatus = DITProcessingStatus.Processing,
					SourceTrackingId = fileId,
					CreationDate = DateTime.Now,
					CreatedBy = SERVICE,
				});

                var temp = await _fileSystem.CreateTempFile(importFileContext.FormFile);

                //when .net core 3.0 gets released, used the native task queue.
                _backgroundQueue.Enqueue(cancellationToken => ParseFile(temp, fileId, importFileContext));

                return new JsonResult(new ServiceResponse<FileTrack>()
				{
					Data = new FileTrack
					{
						FileId = fileId,
						StatusCode = DITProcessingStatus.Processing,
						FileStatus = Enum.GetName(typeof(DITProcessingStatus), DITProcessingStatus.Processing)
					},
					Links = new List<Link>{new Link
						{
							Action = "GET",
							Href = $"ACH/{fileId}"
						}},
				})
				{ StatusCode = StatusCodes.Status201Created };
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Exception on Post", ex.Message);
				var response = new ServiceResponse<FileTrack>
				{
					Errors = new List<string> { "An issue was encountered when attempting to complete the request." },
					Data = new FileTrack
					{
						StatusCode = DITProcessingStatus.Error,
						FileStatus = Enum.GetName(typeof(DITProcessingStatus), DITProcessingStatus.Error),
						FileId = Guid.Empty
					}
				};
				return new JsonResult(response)
				{
					StatusCode = StatusCodes.Status500InternalServerError
				};
			}
		}
       
        private Task ParseFile(string file, Guid fileId, DataImportFile fileContext)
        {
            var work = new Task(() =>
            {
                try
                {
                    List<string> parseErrors = new List<string>();              

                    var parsedFile = new ACHFile(file, _rulesProvider).Parse();
                    parseErrors.AddRange(parsedFile.Errors);
                                                                             
                    //get list of rules & log them
                    foreach (var rule in parsedFile.Result.Rules) _logger.LogInformation(rule.ToString());

                    var parsedBatches = parsedFile.MapTo(fileContext);
                    var batchList = parsedBatches.Result;
                    parseErrors.AddRange(parsedBatches.Errors);

                    //Only insert the batches if there are no errors for all the batches in the file. (I.e. this is an all or nothing deal.)
                    if (parseErrors.Count == 0)
                        _dataImportQueue.Insert(new DataImportQueue
                        {
                            ClientProcessCode = fileContext.ClientProcessCode,
                            SourceTrackingId = fileId,
                            Batches = batchList

                        });

                    if (parseErrors.Count > 0)
                    {
                        _alertDataProvider.LogAlert(PROCESSING_EXCEPTION_EVENT_NAME,
                            $"There was an issue processing file {fileId}");
                        var joinederrors = string.Join(", ", parseErrors.ToArray());
                        _logger.LogError($"Errors found during parsing and validation: {joinederrors}");
                    }

                    _importTracking.UpdateTrackingInfo(new DataImportTracking
                    {
                        FileStatus = parseErrors.Count > 0
                            ? DITProcessingStatus.Error
                            : DITProcessingStatus.Completed,
                        ModificationDate = DateTime.Now,
                        ModifiedBy = SERVICE,
                        SourceTrackingId = fileId
                    });
                    LogFileInfo(parsedFile);

                }
                catch (Exception e)
                {
                    _logger.LogError($"Exception Processing file with message: {e.Message} and data {e.Data}");
                    throw;
                }
                finally
                {
                    _fileSystem.DeleteFile(file);//have to make sure the temp file always gets deleted
                }
            });
            work.Start();
            return work;

        }

        private void LogFileInfo(ParseResult<ACHFile> file)
        {
            _logger.LogInformation($"Batch Count = {file.Result.Batches.Count}");
            foreach (var batch in file.Result.Batches)
            {
                _logger.LogInformation($"Batch Record Type = {batch.Header.EntryClassCode}");
                _logger.LogInformation($"Company Name ={batch.Header.CompanyName}");
                _logger.LogInformation($"  Batch Detail Count= {batch.BatchDetail.Count} ");
                foreach (var batchDetail in batch.BatchDetail)
                {
                    _logger.LogInformation($"  Batch Amount= {batchDetail.Amount:C2} ");
                    _logger.LogInformation($"  Batch ABA= {batchDetail.ABA} ");
                    _logger.LogInformation($"  Batch DDA= {batchDetail.DDA} ");
                    if (batchDetail.Addenda != null)
                    {
                        _logger.LogInformation($"  Addenda Count= {batchDetail.Addenda.Count} ");
                    }
                }
            }
        }

        [Obsolete]
        private void LogFileInfo(ParseResult<ACHConverter.Models.File> file)
		{
			_logger.LogInformation($"Batch Count = {file.Result.Batches.Count}");
			foreach (var batch in file.Result.Batches)
			{
				_logger.LogInformation($"Batch Record Type = {batch.Header.EntryClassCode}");
				_logger.LogInformation($"Company Name ={batch.Header.CompanyName}");
				_logger.LogInformation($"  Batch Detail Count= {batch.BatchDetail.Count} ");
				foreach (var batchDetail in batch.BatchDetail)
				{
					_logger.LogInformation($"  Batch Amount= {batchDetail.Amount:C2} ");
					_logger.LogInformation($"  Batch ABA= {batchDetail.ABA} ");
					_logger.LogInformation($"  Batch DDA= {batchDetail.DDA} ");
					if (batchDetail.Addenda != null)
					{
						_logger.LogInformation($"  Addenda Count= {batchDetail.Addenda.Count} ");
					}
				}
			}
		}
	}
}
