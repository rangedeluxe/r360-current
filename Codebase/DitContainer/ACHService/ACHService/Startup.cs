﻿using System;
using System.IO;
using ACHService.DataProviders;
using ACHService.Helpers;
using HostingBackgroundQueue.DI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using ACHService.Abstractions;
using ACHService.Implementations;
using HostingBackgroundQueue;
using Microsoft.Extensions.Hosting;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;
using CommonObjects.Abstractions;

namespace ACHService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private readonly ILogger _logger;
        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddSingleton<IBackgroundQueue, BackgroundQueue>(sp =>
            {
                return new BackgroundQueue(exception => { _logger.LogError(exception.Message); }, 1, 1000);
            });
            services.AddSingleton<IHostedService, BackgroundQueueService>();

            services.AddTransient<IDatabaseHelper, DatabaseHelper>();
            services.AddTransient<IDataImportTracking, DataImportTrackingDataProvider>();
            services.AddTransient<IAlertDataProvider, AlertDataProvider>();
            services.AddTransient<IDataImportQueue, DataImportQueueDataProvider>();
            services.AddTransient<IFileSystem, WindowsFileSystem>();
            services.AddTransient<IRulesProvider, RulesDataProvider>();
            services.ConfigureSwaggerGen(c =>
            {
                var xmlCommentsPath = Path.Combine(AppContext.BaseDirectory, "ACHService.xml");
                c.IncludeXmlComments(xmlCommentsPath, true);
            });
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(api =>
            {
                api.SwaggerDoc("ACHService", new Info { Title = "ACHService" });
                api.DocumentFilter<KnownSubTypesDocumentFilter>();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/ACHService/swagger.json", "ACHService API");
            });

            app.UseMvc();
        }
    }
}
