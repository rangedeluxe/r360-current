﻿using ACHConverter.Models;
using CommonObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using ACHService.Implementations;
using ACHService.Models;
using Deluxe.Payments.Parsing.ACH.Models;
using CommonObjects.Abstractions;
using Deluxe.Payments.Parsing.ACH.Abstractions;
using Deluxe.Payments.Parsing.ACH;
using CommonObjects.Extensions;

namespace ACHService
{
	public static class Extensions
	{
        public static ParseResult<IList<CommonBatch>> MapTo(this ParseResult<ACHFile> parsedFile, DataImportFile fileContext)
        {
            var response = new ParseResult<IList<CommonBatch>>() { Result = new List<CommonBatch>(), Errors = new List<string>()};

            if (fileContext == null) response.Errors.Add("Must provide data import file context.");
            if (parsedFile == null || parsedFile.Result == null) response.Errors.Add("File was not parsed successfully.");
            if (parsedFile.Result.FileSignature == null || parsedFile.Result.FileHash == null)
                response.Errors.Add("Unable to determine file hash/signature.");

            if (response.Errors.Any()) return response; //any errors after parameter validation, get out

            var parseErrors = new List<string>();
            var batchResults = new List<CommonBatch>();
            var clientData = new ClientData
            {
                BatchSiteCode = fileContext.BatchSiteCode,
                BatchSource = fileContext.BatchSource,
                PaymentType = fileContext.PaymentType,
                FileSignature = parsedFile.Result.FileSignature,
                FileHash = parsedFile.Result.FileHash
            };

            parsedFile.Result.ParsedBatchAddenda.ToList().ForEach(parsedBatchAddenda =>
            {
                batchResults.AddRange(parsedBatchAddenda.Result.MapTo(clientData));
                parseErrors.AddRange(parsedBatchAddenda.Errors);
            });

            response.Result = new List<CommonBatch>(batchResults);
            response.Errors = new List<string>(parseErrors);
            return response;
        }

        public static IList<CommonBatch> MapTo(this BatchAddenda batchAddenda, ClientData clientData)
        {
            if (batchAddenda == null) throw new ArgumentException("Must provide batch addenda.");

            var uniqueAccounts = batchAddenda.Batch.BatchDetail.ToDistinctAccounts();

            var result = new List<CommonBatch>();
            uniqueAccounts.ToList().ForEach(account =>
            {
                var batch = batchAddenda.Batch.Header.MapTo(clientData, account);
                batch.Transactions = batchAddenda.MapTo(account);

                result.Add(batch);
            });

            return result;
        }

        public static CommonBatch MapTo(this BatchHeaderRecord batchHeader, ClientData clientData, BatchDetailIdentifier batchDetailIdentifier)
        {
            if (batchHeader == null) throw new ArgumentException("Must provide batch.");

            return new CommonBatch
            {
                DepositDate = batchHeader.SettlementDate,
                BatchDate = batchHeader.EffectiveDate,
                ProcessingDate = batchHeader.EffectiveDate,
                BankId = 0,
                ClientId = 0,
                BatchId = 0,
                BatchNumber = 0,
                BatchCueID = 0,
                BatchSiteCode = clientData.BatchSiteCode,
                BatchSource = clientData.BatchSource,
                PaymentType = clientData.PaymentType,
                FileHash = clientData.FileHash,
                FileSignature = clientData.FileSignature,
                BatchTrackingID = Guid.NewGuid(),
                Transactions = new List<Transaction>(),
                ABA = batchDetailIdentifier?.ABA,
                DDA = batchDetailIdentifier?.DDA
            };
        }
	}
}
