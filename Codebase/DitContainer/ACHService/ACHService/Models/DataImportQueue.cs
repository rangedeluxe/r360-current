﻿using ACHConverter.Models;
using CommonObjects;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace ACHService.Models
{
    [ExcludeFromCodeCoverage]
    public class DataImportQueue
    {
        public Guid SourceTrackingId { get; set; }
        public string ClientProcessCode { get; set; }
        public IList<CommonBatch> Batches { get; set; }
    }
}
