﻿namespace ACHService.Models
{
    public enum DITProcessingStatus
    {
        Error =-1,
        Notfound=0,
        Processing=1,
        Completed=2,
    }
}