﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace ACHService.Models
{
    /// <summary>
    /// A clients request to process an import file
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class DataImportFile
    {
        /// <summary>
        /// The client distinct processing code
        /// </summary>
        public string ClientProcessCode { get; set; }

        public int BatchSiteCode { get; set; }
        public string BatchSource { get; set; }
        public string PaymentType { get; set; }
        /// <summary>
        /// Import File
        /// </summary>
        public IFormFile FormFile { get; set; }
    }
}
