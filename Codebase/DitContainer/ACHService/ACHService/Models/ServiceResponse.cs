﻿
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ACHService.Models
{
    [ExcludeFromCodeCoverage]
    public class ServiceResponse<T>
    {
        public IList<string> Errors { get; set; }
        public T Data { get; set; }
        /// <summary>
        /// Links related to the response
        /// </summary>
        public IList<Link> Links { get; set; }
    }
}