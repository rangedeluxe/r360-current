﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace ACHService.Models
{
    [ExcludeFromCodeCoverage]
    public class DataImportTracking
    {
        public Guid SourceTrackingId { get; set; }
        public DITProcessingStatus FileStatus { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModificationDate { get; set; }
        public string ModifiedBy { get; set; }
        public string BatchTrackingIds { get; set; }
    }
}
