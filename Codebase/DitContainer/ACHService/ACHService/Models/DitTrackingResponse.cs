﻿namespace ACHService.Models
{
    public class DitTrackingResponse
    {
        public DITProcessingStatus FileStatus { get; set; }
        public string BatchTrackingIds { get; set; }
    }
}
