﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACHService.Models
{
    public class FileTrack
    {
        public string FileStatus { get;set; }
        public DITProcessingStatus StatusCode { get;set; }
        public Guid FileId { get; set; }
    }
}
