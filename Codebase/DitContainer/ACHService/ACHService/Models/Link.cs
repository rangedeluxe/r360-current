﻿using System.Diagnostics.CodeAnalysis;

namespace ACHService.Models
{
    [ExcludeFromCodeCoverage]
    public class Link
    {
        public string Rel { get; set; }
        public string Href { get; set; }
        public string Action { get; set; }
    }   
}