﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACHService.Models
{
	public class ClientData
	{
		public int BatchSiteCode { get; set; }
		public string BatchSource { get; set; }
		public string PaymentType { get; set; }
		public string FileSignature { get; set; }
		public string FileHash { get; set; }		
	}
}
