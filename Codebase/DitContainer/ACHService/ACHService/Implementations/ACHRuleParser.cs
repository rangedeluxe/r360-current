﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ACHConverter;
using ACHConverter.Models;
using ACHService.Abstractions;
using Deluxe.Payments.Parsing.ACH;
using CommonObjects;

namespace ACHService.Implementations
{
	public class ACHRuleParser : IACHRuleParser
	{
		public ParseResult<IList<Field>> ParseAddenda(IList<AddendaRule> rules, string fulladdenda, string entryClassCode)
		{
            var result = new ParseResult<IList<Field>> { Result = new List<Field>(), Errors = new List<string>() };
            if (rules == null || !rules.Any()) return result;

            if (string.IsNullOrEmpty(fulladdenda)) return result;

            if (!string.Equals(entryClassCode, "ctx", StringComparison.InvariantCultureIgnoreCase) &&
                !string.Equals(entryClassCode, "ccd", StringComparison.InvariantCultureIgnoreCase) &&
                !string.Equals(entryClassCode, "ppd", StringComparison.InvariantCultureIgnoreCase) &&
                !string.Equals(entryClassCode, "cie", StringComparison.InvariantCultureIgnoreCase))
                return result;

            var delimiter = rules[0].SegmentDelimiter.Replace("\\", "\\\\");
            var match = Regex.Match(fulladdenda, $"([{delimiter}])GS", RegexOptions.IgnoreCase);
            
            if (string.IsNullOrEmpty(match.Value))
            {
                result.Errors.Add($"Configuration Error: The file segment delimiter does not match segment delimiter list {rules[0].SegmentDelimiter} from the ACHAddenda table.");
                return result;
            }
            var segmentDelimiter = match.Value[0];
            var fieldDelimeter = rules[0].FieldDelimiter;
			var splitAddenda = fulladdenda.Split(segmentDelimiter);
			var parsedAddenda = new List<Field>();
			var sequenceNumber = 0;
		    var paymentRecord = true;
            IList<AddendaRule> matchedRules;
            var invalidSegmentEncountered = false;

            splitAddenda.ToList().ForEach(addenda =>
            {
                var data = addenda.Split(fieldDelimeter);

                if (paymentRecord && data[0].ToUpper() == "RMR")
                {
                    paymentRecord = false;
                    matchedRules = rules.TryFindMatchedRules(data).GetResult(new List<AddendaRule>());
                    if (!matchedRules.Any()) invalidSegmentEncountered = true; //done parsing any further
                }

                if (!invalidSegmentEncountered)
                {
                    matchedRules = rules.TryFindMatchedRules(data).GetResult(new List<AddendaRule>());

                    if (matchedRules.Count <= 0) return;
                    if (matchedRules.Any(m => m.CreateNewRecord)) ++sequenceNumber;
                    matchedRules.ToList().ForEach(matchedRule =>
                    {
                        if (data.Length > matchedRule.FieldNumber)
                        {
                            if (data[0].ToUpper() == "DTM" || data[0].ToUpper() == "REF")
                            {
                                if (paymentRecord && matchedRule.IsCheck
                                    || !paymentRecord && !matchedRule.IsCheck)
                                {
                                    parsedAddenda.Add(new Field
                                    {
                                        FieldName = matchedRule.FieldName,
                                        FieldValue = data[matchedRule.FieldNumber],
                                        SequenceNumber = sequenceNumber,
                                        IsCheck = matchedRule.IsCheck
                                    });
                                }
                            }
                            else
                            {
                                parsedAddenda.Add(new Field
                                {
                                    FieldName = matchedRule.FieldName,
                                    FieldValue = data[matchedRule.FieldNumber],
                                    SequenceNumber = sequenceNumber,
                                    IsCheck = matchedRule.IsCheck
                                });
                            }
                        }
                    });
                }
                

			});

			result.Result = parsedAddenda;
			return result;
		}
	}
}
