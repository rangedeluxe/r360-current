﻿using ACHService.Abstractions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ACHService.Implementations
{
    public class WindowsFileSystem : IFileSystem
    {
        public async Task<string> CreateTempFile(IFormFile formFile)
        {
            var temp = Path.GetTempFileName();
            using (var fileStream = new FileStream(temp, FileMode.Create))
            {
                await formFile.CopyToAsync(fileStream);
            }
            return temp;
        }

        public void DeleteFile(string fileName)
        {
            File.Delete(fileName);
        }

        public string GetFileNameWithoutExtension(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName);
        }
    }
}
