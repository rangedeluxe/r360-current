﻿using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using NLog.Web;

namespace ACHService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Debug("init main");
                var configuration = new ConfigurationBuilder()
                    .AddCommandLine(args).AddJsonFile("appsettings.json")
                    .Build();
                int port = int.TryParse(configuration["port"], out port) ? port : 8001;
                var thumb = configuration["thumbprint"];
                BuildWebHost(args, configuration, port, thumb).Run();
            }
            catch (Exception e)
            {
                //NLog: catch setup errors
                logger.Error(e, "Stopped program on exception");
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }
        }
        public static X509Certificate2 GetCertificate(string certificateThumbprint, StoreName storeName)
        {
            //certificateThumbprint = certificateThumbprint.Substring(0, 40);
            var store = new X509Store(storeName, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            var cert = store.Certificates.OfType<X509Certificate2>()
                .FirstOrDefault(x => x.Thumbprint.ToUpper().StartsWith(certificateThumbprint.ToUpper()));
            store.Close();
            return cert;
        }

        public static IWebHost BuildWebHost(string[] args, IConfiguration configuration, int port, string thumbprint) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel(o => o.Listen(IPAddress.Any, port, options => options.UseHttps(GetCertificate(thumbprint, StoreName.My))))
                .UseConfiguration(configuration)
                .UseNLog()
                .Build();
    }


}
