﻿namespace ACHService.Helpers
{
    public interface IDatabaseHelper
    {
        string ConnectionString { get;set; }
    }
}