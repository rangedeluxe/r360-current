﻿
using DatabaseHelpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ACHService.Helpers
{
    public class DatabaseHelper : IDatabaseHelper
    {
        private const string DBSTRING = "database:connectionString";

        public string ConnectionString { get;set; }

        public DatabaseHelper(IConfiguration configuration, ILogger<DatabaseHelper> logger)
        {
            ConnectionString =  configuration[DBSTRING];
            DatabaseConnectionString connectionStringHelper = new DatabaseConnectionString(logger);
            ConnectionString = connectionStringHelper.GetConnectionString(DBSTRING, configuration);
        }
    }
}
