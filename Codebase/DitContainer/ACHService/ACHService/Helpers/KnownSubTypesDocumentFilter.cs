﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using ACHService.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ACHService.Helpers
{
    /// <summary>
    /// Utility class to generate documentation for swagger's Model section.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class KnownSubTypesDocumentFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            context.SchemaRegistry.GetOrRegister(typeof(DataImportFile));
            context.SchemaRegistry.GetOrRegister(typeof(ServiceResponse<FileTrack>));

        }
    }
}
