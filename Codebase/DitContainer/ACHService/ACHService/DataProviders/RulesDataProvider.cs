﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ACHConverter.Models;
using ACHService.Abstractions;
using ACHService.Helpers;
using ACHService.Models;
using CommonObjects;
using CommonObjects.Abstractions;
using Dapper;
using Microsoft.Extensions.Logging;

namespace ACHService.DataProviders
{
    public class RulesDataProvider : IRulesProvider
    {
        private readonly ILogger _logger;
        private readonly IDatabaseHelper _dbHelper;
        private const string GETRULESP = "RecHubConfig.usp_ACHRules_Get";
        public RulesDataProvider(ILogger<RulesDataProvider> logger, IDatabaseHelper dbhelper)
        {
            _logger = logger;
            _dbHelper = dbhelper;
        }
        public IList<AddendaRule> GetRules(IList<BatchDetailIdentifier> batchDetailsDetailIdentifiers)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_dbHelper.ConnectionString))
                {
                    var columns = new List<string>
                    {
                        "ABA",
                        "DDA"
                    };
                    var dataTable = new DataTable();
                    dataTable.Columns.AddRange(columns.Select(c => new DataColumn(c)).ToArray());
                    batchDetailsDetailIdentifiers.ToList().ForEach(bd =>
                    {
                        var row = dataTable.NewRow();
                        row["DDA"] = bd.DDA.Trim();
                        row["ABA"] = bd.ABA.Trim();
                        dataTable.Rows.Add(row);
                    });
                    var procParams = new DynamicParameters();
                    procParams.Add("@parmDDARuleListTable", dataTable.AsTableValuedParameter());
                    
                    var dbres = db.Query<AddendaRule>(GETRULESP, procParams, commandType: CommandType.StoredProcedure);
                    return dbres.ToList();

                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{nameof(GetRules)} : Error: {e.Message}");
                throw;
            }
        }
    }
}
