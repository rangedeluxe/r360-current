﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ACHService.Abstractions;
using ACHService.Helpers;
using ACHService.Models;
using Dapper;
using Microsoft.Extensions.Logging;

namespace ACHService.DataProviders
{
    public class DataImportTrackingDataProvider : IDataImportTracking
    {
        private const string INS_SP = "RecHubSystem.usp_DataImportTracking_Ins";
        private const string UPD_SP = "RecHubSystem.usp_DataImportTracking_Upd";
        private const string SLCT_SP = "RecHubSystem.usp_DataImportTracking_Get";
        private readonly IDatabaseHelper _dbHelper;
        private readonly ILogger _logger;
        public DataImportTrackingDataProvider(ILogger<DataImportTrackingDataProvider> logger, IDatabaseHelper dbHelper)
        {
            _dbHelper = dbHelper;
            _logger = logger;
        }
        public DataImportTracking GetTrackingInfo(Guid id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_dbHelper.ConnectionString))
                {
                    var dataImportTracking = db.Query<DataImportTracking>(
                            SLCT_SP, 
                            new {@parmSourceTrackingId = id}, 
                            commandType:CommandType.StoredProcedure)
                        .FirstOrDefault();
                    return dataImportTracking;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{nameof(GetTrackingInfo)} : Error: {e.Message}");
                throw;
            }
        }

        public bool InsertTrackingInfo(DataImportTracking trackingInfo)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_dbHelper.ConnectionString))
                {
                    var procParams = new DynamicParameters();
                    procParams.Add("@parmSourceTrackingId", trackingInfo.SourceTrackingId);
                    procParams.Add("@parmFileStatus", (int)trackingInfo.FileStatus);
                    procParams.Add("@parmCreationDate", trackingInfo.CreationDate);
                    procParams.Add("@parmCreatedBy", trackingInfo.CreatedBy);
                    var dbres = db.Execute(INS_SP, procParams, commandType: CommandType.StoredProcedure);
                    return  dbres > 0;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{nameof(InsertTrackingInfo)} : Error: {e.Message}");
                throw;
            }
        }

        public bool UpdateTrackingInfo(DataImportTracking trackingInfo)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_dbHelper.ConnectionString))
                {
                    var procParams = new DynamicParameters();
                    procParams.Add("@parmSourceTrackingId", trackingInfo.SourceTrackingId);
                    procParams.Add("@parmFileStatus", (int)trackingInfo.FileStatus);
                    procParams.Add("@parmModificationDate", trackingInfo.ModificationDate);
                    procParams.Add("@parmModifiedBy", trackingInfo.ModifiedBy);
                    var dbres = db.Execute(UPD_SP, procParams, commandType: CommandType.StoredProcedure);
                    return dbres > 0;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{nameof(UpdateTrackingInfo)} : Error: {e.Message}");
                throw;
            }
        }
    }
}
