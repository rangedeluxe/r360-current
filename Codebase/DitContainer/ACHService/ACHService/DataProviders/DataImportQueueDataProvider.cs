﻿using ACHService.Abstractions;
using ACHService.Helpers;
using ACHService.Models;
using Dapper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ACHService.DataProviders
{
	public class DataImportQueueDataProvider : IDataImportQueue
    {
        private const string InsertStoredProcedure = "RecHubSystem.usp_DataImportQueue_Ins_JSONBatch";
        private readonly IDatabaseHelper _dbHelper;
        private readonly ILogger _logger;

        public DataImportQueueDataProvider(ILogger<DataImportQueueDataProvider> logger, IDatabaseHelper dbHelper)
        {
            _dbHelper = dbHelper;
            _logger = logger;
        }

        public bool Insert(DataImportQueue request)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_dbHelper.ConnectionString))
                {
                    var columns = new List<string>
                    {
                        "AuditDateKey",
                        "ClientProcessCode",
                        "SourceTrackingID",
                        "BatchTrackingID",
                        "JsonDataDocument"
                    };
                    var dataTable = new DataTable();
                    dataTable.Columns.AddRange(columns.Select(c => new DataColumn(c)).ToArray());

                    int auditDateKey;
                    int.TryParse(DateTime.Now.ToString("yyyyMMdd"), out auditDateKey);

                    request.Batches.ToList().ForEach(batch =>
                    {
                        var row = dataTable.NewRow();
                        row["AuditDateKey"] = auditDateKey;
                        row["ClientProcessCode"] = request.ClientProcessCode;
                        row["SourceTrackingID"] = request.SourceTrackingId;
                        row["BatchTrackingID"] = batch.BatchTrackingID;
                        row["JsonDataDocument"] = JsonConvert.SerializeObject(batch);
                        dataTable.Rows.Add(row);
                    });

                    var procParams = new DynamicParameters();
                    procParams.Add("@parmDataImportQueueInsertTable", dataTable.AsTableValuedParameter());

                    var dbres = db.Execute(InsertStoredProcedure, procParams, commandType: CommandType.StoredProcedure);
                    return dbres > 0;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{nameof(Insert)} : Error: {e.Message}");
                throw;
            }
        }
    }
}
