﻿using System;
using System.Data;
using System.Data.SqlClient;
using ACHService.Abstractions;
using ACHService.Helpers;
using Dapper;
using Microsoft.Extensions.Logging;

namespace ACHService.DataProviders
{
    class AlertDataProvider : IAlertDataProvider
    {
        private const string INS_SP = "RecHubAlert.usp_EventLog_SystemLevel_Ins";
        private readonly IDatabaseHelper _dbHelper;
        private readonly ILogger _logger;
        public AlertDataProvider(ILogger<AlertDataProvider> logger, IDatabaseHelper dbHelper)
        {
            _logger = logger;
            _dbHelper = dbHelper;
        }
        public bool LogAlert(string eventName, string message)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(_dbHelper.ConnectionString))
                {
                    var procParams = new DynamicParameters();
                    procParams.Add("@parmEventName", eventName);
                    procParams.Add("@parmMessage", message);

                    var dbres = db.Execute(INS_SP, procParams, commandType: CommandType.StoredProcedure);
                    return  dbres > 0;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{nameof(LogAlert)} : Error: {e.Message}");
                throw;
            }
        }
    }
}