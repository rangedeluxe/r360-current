﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACHService.Abstractions
{
    public interface IAlertDataProvider
    {
        bool LogAlert(string eventName, string message);
    }
}
