﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACHService.Abstractions
{
    public interface IFileSystem
    {
        Task<string> CreateTempFile(IFormFile formFile);
        string GetFileNameWithoutExtension(string fileName);
        void DeleteFile(string fileName);
    }
}
