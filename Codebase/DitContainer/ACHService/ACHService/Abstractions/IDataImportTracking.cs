﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACHService.Models;

namespace ACHService.Abstractions
{
    public interface IDataImportTracking
    {
        DataImportTracking GetTrackingInfo(Guid id);
        bool InsertTrackingInfo(DataImportTracking trackingInfo);
        bool UpdateTrackingInfo(DataImportTracking trackingInfo);

    }
}
