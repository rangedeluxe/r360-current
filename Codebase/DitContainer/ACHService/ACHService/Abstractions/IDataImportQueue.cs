﻿using ACHService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACHService.Abstractions
{
    public interface IDataImportQueue
    {
        bool Insert(DataImportQueue request);
    }
}
