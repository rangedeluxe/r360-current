﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACHConverter;
using ACHConverter.Models;
using CommonObjects;

namespace ACHService.Abstractions
{
    public interface IACHRuleParser
    {
        ParseResult<IList<Field>> ParseAddenda(IList<AddendaRule> rules, string addenda, string entryClassCode);
    }
}
