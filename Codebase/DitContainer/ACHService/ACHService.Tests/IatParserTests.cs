﻿
using ACHConverter.Parsers;
using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parsing.ACH.Tests;
using System.Collections.Generic;

namespace ACHService.Tests
{
    [TestClass]
    public class IatParserTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static MockRulesProvider _rulesProvider;

        private static readonly string _aba = "666666670";
        private static readonly string _dda = "666666671";

        private static List<string> _iatFile = new List<string>
        {
            @"101 666666670 6666666701910030424H094101TEST IAT IMPORT                                       ",
            @"5220                FF3               US123456TESTIATPAYMENT   USDUSD1910030001072000000000001",
            @"622666666670666666671        0001087594666666671                             01191003042400001",
            @"710BUS000000000001087594                      TEST IAT FILE                    191003042400001",
            @"711TEST IAT ADDRESS ROAD NAME                                                  191003042400001",
            @"712ACHTEST*ON\                        CA*N1L 1E9\                              191003042400001",
            @"713THE.BANK                           01125000024                         US   191003042400001",
            @"714DELUXE BANK                        011250000245                        US   191003042400001",
            @"715TEST1234567/8  PO BOX 1234 TESTADD                                          191003042400001",
            @"716WAUSAU*WISCONSIN\                  US*12345 1234\                           191003042400001",
            @"717123 456 7890\                                                               191003042400001",
            @"82200000090006100010000000000000000001087594123456TEST                         072000000000001",
            @"9000001000003000000170012200020000000000000000001087696                                       "
        };

        private static List<string> _iatFileNoSeqNumber = new List<string>
        {
            @"101 072000096 0720000961910180914A094101MI POST-C RED MICHIGAN Deluxe Corp            00002751",
            @"5220DELUXE CORP     FF3FOREIGNEXCHREF US0720000961IATPAYMENT   AUDUZS1910180001 07200000000001",
            @"622666666670666666671        0000323439980980983                            XY1000000000000001",
            @"710TEL000000000000323439                      WELLS FARGO                              0000001",
            @"711GOOGLE                             1600 PENNSYLVANIA AVE                            0000001",
            @"712WASHINGTON*DC                      US*99999                                         0000001",
            @"713SWISS BANK AG                      02UBSWCHZH12A                       CA           0000001",
            @"714FIRST NATIONAL BANK                01104000016                         US           0000001",
            @"71565049721       14818 REDMAN AVE                                                     0000001",
            @"716OMAHA*NE                           US*68154                                         0000001",
            @"822000000989089089  0000000000000000003234390720000961                          07200000000001",
            @"9000001000012000000090089089089000000000000000000323439                                       "
        };

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _rulesProvider = new MockRulesProvider();
            _rulesProvider.CreateExpandedTemplate(_aba, _dda, "\\~", "*");
        }

        [TestMethod]
        public void ValidIatPaymentRecord()
        {
            var payment = IatParser.Parse("6221234567890001             0000000962987654321                            101000000000000001", 1);

            Assert.AreEqual(0, payment.Errors.Count, "ErrorCount");
            Assert.AreEqual("6", payment.Result.RecordType, "RecordType");
            Assert.AreEqual("22", payment.Result.TransactionCode, "TransactionCode");
            Assert.AreEqual("987654321", payment.Result.DDA, "DDA");
            Assert.AreEqual("123456789", payment.Result.ABA, "ABA");
            Assert.AreEqual((decimal)9.62, payment.Result.Amount, "Amount");
            Assert.AreEqual("0", payment.Result.OfacIndicator2, "IndividualId");
            Assert.AreEqual("987654321                          ", payment.Result.AccountNumber, "AccountNumber");
            Assert.AreEqual("1", payment.Result.OfacIndicator1, "ReceivingCompany");
            Assert.AreEqual("000000000000001", payment.Result.TraceNumber, "TraceNumber");
        }

	    [TestMethod]
	    public void InvalidIatAmount()
	    {
		    var payment =
                IatParser.Parse("622072000096000000090123456711YYYY32646VISHAY AMERICAS00095954-2001931405     1091004447858009", 1);
		    Assert.AreEqual(1, payment.Errors.Count);
		    Assert.AreEqual("Error parsing Batch Detail value:\tLine number: 1\tField: Amount\tValue: 1YYYY32646", payment.Errors[0]);
	    }

        [TestMethod]
        public void ParseIatAddenda()
        {

            var achFile = new ACHFile(_iatFile, _rulesProvider).Parse();
            var achBatch = achFile.Result.Batches[0];
            var addendaRecord = new IATAddenda(achBatch.Header, achBatch.BatchDetail[0], achBatch.Footer,
                _rulesProvider.GetRules(new List<BatchDetailIdentifier>
                    {new BatchDetailIdentifier() {ABA = _aba, DDA = _dda}}));

            Assert.AreEqual(0, addendaRecord.Parse().Errors.Count);
            Assert.IsNull(addendaRecord.ParsedAddendaSegments.Result);
        }

        [TestMethod]
        public void ParseIatAddendaNoSeqNumber()
        {

            var achFile = new ACHFile(_iatFileNoSeqNumber, _rulesProvider).Parse();
            var achBatch = achFile.Result.Batches[0];
            var addendaRecord = new IATAddenda(achBatch.Header, achBatch.BatchDetail[0], achBatch.Footer,
                _rulesProvider.GetRules(new List<BatchDetailIdentifier>
                    {new BatchDetailIdentifier() {ABA = _aba, DDA = _dda}}));

            Assert.AreEqual(0, addendaRecord.Parse().Errors.Count);
            Assert.IsNull(addendaRecord.ParsedAddendaSegments.Result);
        }
    }
}
