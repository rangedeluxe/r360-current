﻿
using ACHConverter;
using ACHConverter.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ACHService.Tests
{
    [TestClass]
    public class AddendaTests
    {
		private const string testAddenda1 = @"705ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001";
		private const string testAddenda2 = @"7054*U*00400*076913354*0*P*>\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001";
		private const string testAddenda3 = @"705010\ST*820*000000268\REF*2U*DDC2CRJC5G\SE*19*000000268\GE*1*76913354\IEA*1*0769100030000001";
		private const string testAddenda4 = @"7053354\                                                                           00040000001";

		[TestMethod]
		public void ShouldHaveSequence()
		{
			var addenda1 = Addenda.Parse(testAddenda1, 1);
			var addenda2 = Addenda.Parse(testAddenda2, 1);
			var addenda3 = Addenda.Parse(testAddenda3, 1);
			var addenda4 = Addenda.Parse(testAddenda4, 1);
			Assert.AreEqual(addenda1.Result.SequenceNumber, 1);
			Assert.AreEqual(addenda2.Result.SequenceNumber, 2);
			Assert.AreEqual(addenda3.Result.SequenceNumber, 3);
			Assert.AreEqual(addenda4.Result.SequenceNumber, 4);
		}

        [TestMethod]
        public void InvalidAddendaTest()
        {
            var addenda = Addenda.Parse("7xxISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*172ZAXC0UU0001", 1);
			Assert.AreEqual(addenda.Errors.Count, 3);
	        Assert.AreEqual("Error parsing Addenda value:\tLine number: 1\tField: AddendaTypeCode\tValue: xx", addenda.Errors[0]);
	        Assert.AreEqual("Error parsing Addenda value:\tLine number: 1\tField: SequenceNumber\tValue: ZAXC", addenda.Errors[1]);
	        Assert.AreEqual("Error parsing Addenda value:\tLine number: 1\tField: DetailSequenceNumber\tValue: 0UU0001", addenda.Errors[2]);
		}
	}
}
