﻿using CommonObjects;
using CommonObjects.Abstractions;
using System.Collections.Generic;

namespace Parsing.ACH.Tests
{
    class MockRulesProvider : IRulesProvider
    {
        private List<AddendaRule> _ruleList = new List<AddendaRule>();
        private readonly List<AddendaRule> _expandedAddendaRules = new List<AddendaRule>();


        private List<AddendaRule> AddBprRules(string aba, string dda, string addendaName, string segmentDelimiter,
            string fieldDelimiter, string addendaSegmentName)
        {
            var bprRules = new List<AddendaRule>
            {
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "BPR",
                    CreateNewRecord = true,
                    IsCheck = true,
                    FieldNumber = 2,
                    FieldName = "BPRMonetaryAmount"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "BPR",
                    CreateNewRecord = true,
                    IsCheck = true,
                    FieldNumber = 3,
                    FieldName = "CreditIndicator"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "BPR",
                    CreateNewRecord = true,
                    IsCheck = true,
                    FieldNumber = 7,
                    FieldName = "OriginatingDFI"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "BPR",
                    CreateNewRecord = true,
                    IsCheck = true,
                    FieldNumber = 9,
                    FieldName = "BPRAccountNumber"
                }
            };

            return bprRules;
        }

        private List<AddendaRule> AddDtmRules(string aba, string dda, string addendaName, string segmentDelimiter,
            string fieldDelimiter, string addendaSegmentName, bool isCheck)
        {
            var dtmRules = new List<AddendaRule>
            {
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "DTM",
                    CreateNewRecord = false,
                    IsCheck = isCheck,
                    FieldNumber = 1,
                    FieldName = "DTMDateQualifier"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "DTM",
                    CreateNewRecord = false,
                    IsCheck = isCheck,
                    FieldNumber = 2,
                    FieldName = "DTMDate"
                }
            };

            return dtmRules;
        }

        private List<AddendaRule> AddPaymentNRules(string aba, string dda, string addendaName, string segmentDelimiter,
            string fieldDelimiter, string addendaSegmentName)
        {
            var nRules = new List<AddendaRule>
            {
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "N1",
                    CreateNewRecord = false,
                    SegmentQualifier = "PR",
                    IsCheck = true,
                    FieldNumber = 2,
                    FieldName = "Payer"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "N1",
                    CreateNewRecord = false,
                    SegmentQualifier = "PE",
                    IsCheck = true,
                    FieldNumber = 2,
                    FieldName = "Payee"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "N1",
                    CreateNewRecord = false,
                    SegmentQualifier = "BK",
                    IsCheck = true,
                    FieldNumber = 2,
                    FieldName = "BankName"
                }
            };

            return nRules;
        }

        private List<AddendaRule> AddPaymentTrnRules(string aba, string dda, string addendaName, string segmentDelimiter,
            string fieldDelimiter, string addendaSegmentName)
        {
            var trnRules = new List<AddendaRule>
            {
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "TRN",
                    CreateNewRecord = false,
                    IsCheck = true,
                    FieldNumber = 2,
                    FieldName = "ReassociationTraceNumber"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "TRN",
                    CreateNewRecord = false,
                    IsCheck = true,
                    FieldNumber = 4,
                    FieldName = "PayerName"
                }
            };

            return trnRules;
        }
        private List<AddendaRule> AddRefRules(string aba, string dda, string addendaName,
            string segmentDelimiter, string fieldDelimiter, string addendaSegmentName, bool isCheck)
        {
            var refRules = new List<AddendaRule>
            {
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "REF",
                    CreateNewRecord = false,
                    IsCheck = isCheck,
                    FieldNumber = 1,
                    FieldName = isCheck ? "REFHeaderQualifier" : "REFDetailQualifier"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "REF",
                    CreateNewRecord = false,
                    SegmentQualifier = "IV",
                    IsCheck = isCheck,
                    FieldNumber = 2,
                    FieldName = "REFID"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "REF",
                    CreateNewRecord = false,
                    SegmentQualifier = "2U",
                    IsCheck = isCheck,
                    FieldNumber = 2,
                    FieldName = "REFID"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "REF",
                    CreateNewRecord = false,
                    SegmentQualifier = "11",
                    IsCheck = isCheck,
                    FieldNumber = 2,
                    FieldName = "REFID"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "REF",
                    CreateNewRecord = false,
                    SegmentQualifier = "TN",
                    IsCheck = isCheck,
                    FieldNumber = 2,
                    FieldName = "REFID"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "REF",
                    CreateNewRecord = false,
                    SegmentQualifier = "PO",
                    IsCheck = isCheck,
                    FieldNumber = 2,
                    FieldName = "REFID"
                }
            };

            return refRules;
        }

        private List<AddendaRule> AddDocumentRmrRules(string aba, string dda, string addendaName,
            string segmentDelimiter, string fieldDelimiter, string addendaSegmentName, string segmentQualifier)
        {
            var rmrRules = new List<AddendaRule>
            {
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = addendaSegmentName,
                    SegmentIdentifier = "RMR",
                    CreateNewRecord = true,
                    SegmentQualifier = segmentQualifier,
                    IsCheck = false,
                    FieldNumber = 2,
                    FieldName = "RMRReferenceNumber"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = "ACH - Expanded Template RMR",
                    SegmentIdentifier = "RMR",
                    CreateNewRecord = true,
                    SegmentQualifier = segmentQualifier,
                    IsCheck = false,
                    FieldNumber = 2,
                    FieldName = "InvoiceNumber"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = "ACH - Expanded Template RMR",
                    SegmentIdentifier = "RMR",
                    CreateNewRecord = true,
                    SegmentQualifier = segmentQualifier,
                    IsCheck = false,
                    FieldNumber = 4,
                    FieldName = "RMRMonetaryAmount"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = "ACH - Expanded Template RMR",
                    SegmentIdentifier = "RMR",
                    CreateNewRecord = true,
                    SegmentQualifier = segmentQualifier,
                    IsCheck = false,
                    FieldNumber = 5,
                    FieldName = "RMRTotalInvoiceAmount"
                },
                new AddendaRule
                {
                    ABA = aba,
                    DDA = dda,
                    AddendaName = addendaName,
                    SegmentDelimiter = segmentDelimiter,
                    FieldDelimiter = fieldDelimiter,
                    AddendaSegmentName = "ACH - Expanded Template RMR",
                    SegmentIdentifier = "RMR",
                    CreateNewRecord = true,
                    SegmentQualifier = segmentQualifier,
                    IsCheck = false,
                    FieldNumber = 6,
                    FieldName = "RMRDiscountAmount"
                }
            };

            return rmrRules;
        }

        public IList<AddendaRule> GetRules(IList<BatchDetailIdentifier> batchDetailsDetailIdentifiers)
        {
            return _ruleList;
        }

        public void CreateExpandedTemplate(string aba, string dda, string segmentDelimiter, string fieldDelimiter)
        {
            var addendaName = "ACH - Expanded Template";

            _expandedAddendaRules.AddRange(AddBprRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template BPR"));
            _expandedAddendaRules.AddRange(AddDtmRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template DTM", true));
            _expandedAddendaRules.AddRange(AddPaymentNRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template N1"));
            _expandedAddendaRules.AddRange(AddRefRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template REF", true));
            _expandedAddendaRules.AddRange(AddPaymentTrnRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template TRN"));
            
            _expandedAddendaRules.AddRange(AddDocumentRmrRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template RMR", "AP"));
            _expandedAddendaRules.AddRange(AddDocumentRmrRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template RMR", "BM"));
            _expandedAddendaRules.AddRange(AddDocumentRmrRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template RMR", "CT"));
            _expandedAddendaRules.AddRange(AddDocumentRmrRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template RMR", "PO"));
            _expandedAddendaRules.AddRange(AddDocumentRmrRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template RMR", "R7"));
            _expandedAddendaRules.AddRange(AddDocumentRmrRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template RMR", "VV"));
            _expandedAddendaRules.AddRange(AddDocumentRmrRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template RMR", "12"));
            _expandedAddendaRules.AddRange(AddDocumentRmrRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template RMR", "IV"));
            _expandedAddendaRules.AddRange(AddDtmRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template DTM", false));
            _expandedAddendaRules.AddRange(AddRefRules(aba, dda, addendaName, segmentDelimiter, fieldDelimiter,
                "ACH - Expanded Template REF", false));

            _ruleList = _expandedAddendaRules;
        }
    }
}
