﻿using ACHService.Models;
using CommonObjects.Abstractions;
using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACHService.Tests
{
    [TestClass]
    public class ExtensionTests
    {
        private Mock<IRulesProvider> _rulesProvider;

        [TestInitialize()]
        public void Setup()
        {
            _rulesProvider = new Mock<IRulesProvider>();
        }

        [TestMethod]
        public void Will_ConvertParsedFile_ToR360Batches()
        {
            //arrange
            var expectedBatchCount = 2;
            var formFile = new Mock<IFormFile>();
            formFile.Setup(x => x.FileName).Returns($"{AppContext.BaseDirectory}TestFiles\\multipleabadda.dat");
            var fileContext = new DataImportFile
            {
                ClientProcessCode = "DITACHUnitTest", PaymentType = "DITACHPaymentType", FormFile = formFile.Object,
                BatchSiteCode = 1, BatchSource = "DITACHBatchSource"
            };

            //act
            var actual = new ACHFile(formFile.Object.FileName, _rulesProvider.Object).Parse().MapTo(fileContext);

            //assert
            Assert.AreEqual(expectedBatchCount, actual.Result.Count);
        }
    }
}
