
using System;
using ACHConverter.Parsers;
using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parsing.ACH.Tests;
using System.Collections.Generic;
using System.Linq;

namespace ACHService.Tests
{
    [TestClass]
    public class CTXParserTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static MockRulesProvider _rulesProvider;

        private static readonly string _aba = "666666670";
        private static readonly string _dda = "666666671";

        private static List<string> _ctxFile = new List<string>
        {
            @"101 072000096 0720000961810041445A094101MI POST-C RED MICHIGAN Deluxe Corp            00001175",
            @"5220DELUXE CORP     DISCRESIONARYDATA   0720000961CTXPAYMENT   1810041810040001 07200000000001",
            @"636666666670666666671        000088950107000096       0002FREDS BANK LLC    XX1000000000000001",
            @"705ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001",
            @"7054*U*00400*076913354*0*P*>\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001",
            @"705010\ST*820*000000268\BPR*C*0.01*C*ACH*CTX*01*65465465*DA*FWMAIC4XT*JTDGXWFWQ\TRN00030000001",
            @"705*1*7RHBG\REF*2U*0T9G86N0CI\N1*PE*UZLD6JC0RP6LKHM\N3*23434*Main Street\N4*SomeCit00040000001",
            @"705y*USA*55555\DTM*002*20111013\RMR*IV*KKFN0**0.02*57.31*0\DTM*003*20090507\REF*PO*00050000001",
            @"705ZN7LUXQ7MZLESFW\RMR*IV*SDKXV**127.99*781.36*0\DTM*003*20071104\REF*PO*HWRT8HRAMO00060000001",
            @"7050J021\RMR*IV*L7QDW**0.07*9.77*0\DTM*003*20050723\REF*PO*7CXXJEFR5NIUT2C\RMR*IV*Z00070000001",
            @"705YLEN**17.27*44.17*0\DTM*003*20050824\REF*PO*RE3ZMT87JH04E2G\RMR*IV*OVI53**735.2300080000001",
            @"705*0.03*0\DTM*003*20061022\REF*PO*MKNV6G6SP5BU30S\RMR*IV*PF1GL**925.72*0.02*0\DTM*00090000001",
            @"705003*20130103\REF*PO*BFGAIVSWYQZC09L\RMR*IV*32ZKM**0.18*963.04*0\DTM*003*2007012100100000001",
            @"705\REF*PO*MRPMKLV8X4PL7JG\RMR*IV*JCKAB**0.47*60.76*0\DTM*003*20100119\REF*PO*C0E2600110000001",
            @"705Z7OXYQPWMU\RMR*IV*QTN2L**0.02*0.02*0\DTM*003*20091217\REF*PO*E0AYTS519EZ63E0\RMR00120000001",
            @"705*IV*RZULQ**9.12*888.26*0\DTM*003*20040830\REF*PO*DW5E1I5XRMSDRA4\SE*19*00000026800130000001",
            @"705\GE*1*76913354\IEA*1*076913354\                                                 00140000001",
            @"636666666670666666671        000066536107000096       0001WELLS FARGO       XX1000000000000001",
            @"705ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001",
            @"7054*U*00400*076913354*0*P*>\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001",
            @"705010\ST*820*000000268\BPR*C*53.15*C*ACH*CTX*01*65465465*DA*NCSD44YMW*C20CVH20T\TR00030000001",
            @"705N*1*W8P1D\N1*PR*FDIIQFNGVN\N1*PE*HWZPRUNSFH\DTM*005*20181201\REF*2U*EWRERJFS\SE*00040000001",
            @"70519*000000268\GE*1*76913354\IEA*1*076913354\                                     00050000001",
            @"8220000015554974640 0000000000000000008895010720000961                          07200000000001",
            @"9000001000019000000150554974640000000000000000000889501                                       "
        };

        private void ValidateDocumentAddenda(List<Field> expectedResults, IList<Field> actualResults, string testName)
        {
            foreach (var expectedResult in expectedResults)
            {
                var actualResult = actualResults.FirstOrDefault(a => a.FieldName == expectedResult.FieldName);
                Assert.IsNotNull(actualResult, $"{testName} - Could not find field {expectedResult.FieldName}");
                Assert.AreEqual(expectedResult.IsCheck, actualResult?.IsCheck,
                    $"{testName} - {expectedResult.FieldName}:IsCheck");
                Assert.AreEqual(expectedResult.FieldValue, actualResult?.FieldValue,
                    $"{testName} - {expectedResult.FieldName}:FieldValue");
                Assert.AreEqual(expectedResult.SequenceNumber, actualResult?.SequenceNumber,
                    $"{testName} - {expectedResult.FieldName}:SequenceNumber");
            }
        }

        private void ValidateCheckAddendaField(string expectedResult, IList<Field> actualResult, string fieldName, string testName)
        {
            Assert.AreEqual(expectedResult, actualResult.FirstOrDefault(ap => string.Equals(ap.FieldName,fieldName, StringComparison.InvariantCultureIgnoreCase))?.FieldValue,
                $"{TestContext.TestName} - {fieldName}");
        }

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _rulesProvider = new MockRulesProvider();
            _rulesProvider.CreateExpandedTemplate(_aba, _dda, "\\~", "*");
        }

        [TestMethod]
        public void ValidCTXPaymentRecord()
        {
            var payment =
                CTXParser.Parse(
                    "622072000096000000090123456711000632646VISHAY AMERICAS00095954-2001931405     1091004447858009",
                    1);

            Assert.AreEqual(0, payment.Errors.Count, "ErrorCount");
            Assert.AreEqual("6", payment.Result.RecordType, "RecordType");
            Assert.AreEqual("22", payment.Result.TransactionCode, "TransactionCode");
            Assert.AreEqual("00000009012345671", payment.Result.DDA, "DDA");
            Assert.AreEqual("072000096", payment.Result.ABA, "ABA");
            Assert.AreEqual((decimal) 10006326.46, payment.Result.Amount, "Amount");
            Assert.AreEqual("VISHAY AMERICAS", payment.Result.IndividualId, "IndividualId");
            Assert.AreEqual("00000009012345671", payment.Result.AccountNumber, "AccountNumber");
            Assert.AreEqual("5954-2001931405 ", payment.Result.ReceivingCompany, "ReceivingCompany");
            Assert.AreEqual("  ", payment.Result.DiscretionaryData, "DiscretionaryData");
            Assert.AreEqual("091004447858009", payment.Result.TraceNumber, "TraceNumber");
        }

        [TestMethod]
        public void InvalidCTXAmount()
        {
            var batch =
                CTXParser.Parse(
                    "622072000096000000090123456711YYYY32646VISHAY AMERICAS00095954-2001931405     1091004447858009",
                    1);
            Assert.AreEqual(1, batch.Errors.Count);
            Assert.AreEqual("Error parsing Batch Detail value:\tLine number: 1\tField: Amount\tValue: 1YYYY32646",
                batch.Errors[0]);
        }

        [TestMethod]
        public void ParseCtxAddendaWithRmrDtmAndRef()
        {
            var achFile = new ACHFile(_ctxFile, _rulesProvider).Parse();

            var achBatch = achFile.Result.Batches[0];
            var addendaRecord = new CTXAddenda(achBatch.Header, achBatch.BatchDetail[0], achBatch.Footer,
                _rulesProvider.GetRules(new List<BatchDetailIdentifier>
                    {new BatchDetailIdentifier() {ABA = _aba, DDA = _dda}}));
            var addendaRecordResults = addendaRecord.Parse();
            var paymentAddendaFields = addendaRecordResults.Result.ParsedAddendaSegments.Result.PaymentAddendaFields;
            var documentAddendaFields = addendaRecordResults.Result.DocumentAddendaFields;
            Assert.AreEqual(0, addendaRecordResults.Errors.Count, $"{TestContext.TestName} - Addenda Error Count");

            //Validate the payment addenda records
            Assert.AreEqual(10, paymentAddendaFields.Count, $"{TestContext.TestName} - Payment Addenda Count");
            ValidateCheckAddendaField("0.01", paymentAddendaFields, "BPRMonetaryAmount", TestContext.TestName);
            ValidateCheckAddendaField("C", paymentAddendaFields, "CreditIndicator", TestContext.TestName);
            ValidateCheckAddendaField("65465465", paymentAddendaFields, "OriginatingDFI", TestContext.TestName);
            ValidateCheckAddendaField("FWMAIC4XT", paymentAddendaFields, "BPRAccountNumber", TestContext.TestName);
            ValidateCheckAddendaField("UZLD6JC0RP6LKHM", paymentAddendaFields, "Payee", TestContext.TestName);
            ValidateCheckAddendaField("2U", paymentAddendaFields, "REFHeaderQualifier", TestContext.TestName);
            ValidateCheckAddendaField("0T9G86N0CI", paymentAddendaFields, "REFID", TestContext.TestName);
            ValidateCheckAddendaField("002", paymentAddendaFields, "DTMDateQualifier", TestContext.TestName);
            ValidateCheckAddendaField("20111013", paymentAddendaFields, "DTMDate", TestContext.TestName);
            ValidateCheckAddendaField("7RHBG", paymentAddendaFields, "ReassociationTraceNumber", TestContext.TestName);

            //Setup expected results
            var expectedResults = new List<List<Field>>
            {
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "KKFN0", IsCheck = false, SequenceNumber = 2},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "0.02", IsCheck = false, SequenceNumber = 2},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "57.31", IsCheck = false, SequenceNumber = 2},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 2},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 2},
                    new Field {FieldName = "DTMDate", FieldValue = "20090507", IsCheck = false, SequenceNumber = 2},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 2},
                    new Field {FieldName = "REFID", FieldValue = "ZN7LUXQ7MZLESFW", IsCheck = false, SequenceNumber = 2}
                },
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "SDKXV", IsCheck = false, SequenceNumber = 3},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "127.99", IsCheck = false, SequenceNumber = 3},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "781.36", IsCheck = false, SequenceNumber = 3},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 3},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 3},
                    new Field {FieldName = "DTMDate", FieldValue = "20071104", IsCheck = false, SequenceNumber = 3},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 3},
                    new Field {FieldName = "REFID", FieldValue = "HWRT8HRAMO0J021", IsCheck = false, SequenceNumber = 3}
                },
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "L7QDW", IsCheck = false, SequenceNumber = 4},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "0.07", IsCheck = false, SequenceNumber = 4},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "9.77", IsCheck = false, SequenceNumber = 4},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 4},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 4},
                    new Field {FieldName = "DTMDate", FieldValue = "20050723", IsCheck = false, SequenceNumber = 4},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 4},
                    new Field {FieldName = "REFID", FieldValue = "7CXXJEFR5NIUT2C", IsCheck = false, SequenceNumber = 4}
                },
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "ZYLEN", IsCheck = false, SequenceNumber = 5},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "17.27", IsCheck = false, SequenceNumber = 5},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "44.17", IsCheck = false, SequenceNumber = 5},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 5},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 5},
                    new Field {FieldName = "DTMDate", FieldValue = "20050824", IsCheck = false, SequenceNumber = 5},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 5},
                    new Field {FieldName = "REFID", FieldValue = "RE3ZMT87JH04E2G", IsCheck = false, SequenceNumber = 5}
                },
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "OVI53", IsCheck = false, SequenceNumber = 6},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "735.23", IsCheck = false, SequenceNumber = 6},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "0.03", IsCheck = false, SequenceNumber = 6},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 6},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 6},
                    new Field {FieldName = "DTMDate", FieldValue = "20061022", IsCheck = false, SequenceNumber = 6},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 6},
                    new Field {FieldName = "REFID", FieldValue = "MKNV6G6SP5BU30S", IsCheck = false, SequenceNumber = 6}
                },
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "PF1GL", IsCheck = false, SequenceNumber = 7},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "925.72", IsCheck = false, SequenceNumber = 7},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "0.02", IsCheck = false, SequenceNumber = 7},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 7},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 7},
                    new Field {FieldName = "DTMDate", FieldValue = "20130103", IsCheck = false, SequenceNumber = 7},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 7},
                    new Field {FieldName = "REFID", FieldValue = "BFGAIVSWYQZC09L", IsCheck = false, SequenceNumber = 7}
                },
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "32ZKM", IsCheck = false, SequenceNumber = 8},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "0.18", IsCheck = false, SequenceNumber = 8},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "963.04", IsCheck = false, SequenceNumber = 8},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 8},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 8},
                    new Field {FieldName = "DTMDate", FieldValue = "20070121", IsCheck = false, SequenceNumber = 8},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 8},
                    new Field {FieldName = "REFID", FieldValue = "MRPMKLV8X4PL7JG", IsCheck = false, SequenceNumber = 8}
                },
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "JCKAB", IsCheck = false, SequenceNumber = 9},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "0.47", IsCheck = false, SequenceNumber = 9},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "60.76", IsCheck = false, SequenceNumber = 9},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 9},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 9},
                    new Field {FieldName = "DTMDate", FieldValue = "20100119", IsCheck = false, SequenceNumber = 9},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 9},
                    new Field {FieldName = "REFID", FieldValue = "C0E26Z7OXYQPWMU", IsCheck = false, SequenceNumber = 9}
                },
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "QTN2L", IsCheck = false, SequenceNumber = 10},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "0.02", IsCheck = false, SequenceNumber = 10},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "0.02", IsCheck = false, SequenceNumber = 10},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 10},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 10},
                    new Field {FieldName = "DTMDate", FieldValue = "20091217", IsCheck = false, SequenceNumber = 10},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 10},
                    new Field {FieldName = "REFID", FieldValue = "E0AYTS519EZ63E0", IsCheck = false, SequenceNumber = 10}
                },
                new List<Field>
                {
                    new Field {FieldName = "RMRReferenceNumber", FieldValue = "RZULQ", IsCheck = false, SequenceNumber = 11},
                    new Field {FieldName = "RMRMonetaryAmount", FieldValue = "9.12", IsCheck = false, SequenceNumber = 11},
                    new Field {FieldName = "RMRTotalInvoiceAmount", FieldValue = "888.26", IsCheck = false, SequenceNumber = 11},
                    new Field {FieldName = "RMRDiscountAmount", FieldValue = "0", IsCheck = false, SequenceNumber = 11},
                    new Field {FieldName = "DTMDateQualifier", FieldValue = "003", IsCheck = false, SequenceNumber = 11},
                    new Field {FieldName = "DTMDate", FieldValue = "20040830", IsCheck = false, SequenceNumber = 11},
                    new Field {FieldName = "REFDetailQualifier", FieldValue = "PO", IsCheck = false, SequenceNumber = 11},
                    new Field {FieldName = "REFID", FieldValue = "DW5E1I5XRMSDRA4", IsCheck = false, SequenceNumber = 11}
                }
            };

            //Loop through and validate results
            for (var i = 0; i < expectedResults.Count; i++)
            {
                ValidateDocumentAddenda(expectedResults[i], documentAddendaFields[i], _testContext.TestName);
            }
        }

        [TestMethod]
        public void ParseCtxAddendaWithBprDtmAndRef()
        {
            var achFile = new ACHFile(_ctxFile, _rulesProvider).Parse();

            var achBatch = achFile.Result.Batches[0];
            var addendaRecord = new CTXAddenda(achBatch.Header, achBatch.BatchDetail[1], achBatch.Footer,
                _rulesProvider.GetRules(new List<BatchDetailIdentifier>
                    {new BatchDetailIdentifier() {ABA = _aba, DDA = _dda}}));
            var addendaRecordResults = addendaRecord.Parse();
            var paymentAddendaFields = addendaRecordResults.Result.ParsedAddendaSegments.Result.PaymentAddendaFields;
            var documentAddendaFields = addendaRecordResults.Result.DocumentAddendaFields;

            Assert.AreEqual(0, addendaRecordResults.Errors.Count, $"{TestContext.TestName} - Addenda Error Count");

            //Validate the payment addenda records
            Assert.AreEqual(11, paymentAddendaFields.Count, $"{TestContext.TestName} - Payment Addenda Count");
            ValidateCheckAddendaField("53.15", paymentAddendaFields, "BPRMonetaryAmount", TestContext.TestName);
            ValidateCheckAddendaField("C", paymentAddendaFields, "CreditIndicator", TestContext.TestName);
            ValidateCheckAddendaField("65465465", paymentAddendaFields, "OriginatingDFI", TestContext.TestName);
            ValidateCheckAddendaField("NCSD44YMW", paymentAddendaFields, "BPRAccountNumber", TestContext.TestName);
            ValidateCheckAddendaField("FDIIQFNGVN", paymentAddendaFields, "Payer", TestContext.TestName);
            ValidateCheckAddendaField("HWZPRUNSFH", paymentAddendaFields, "Payee", TestContext.TestName);
            ValidateCheckAddendaField("005", paymentAddendaFields, "DTMDateQualifier", TestContext.TestName);
            ValidateCheckAddendaField("20181201", paymentAddendaFields, "DTMDate", TestContext.TestName);
            ValidateCheckAddendaField("2U", paymentAddendaFields, "REFHeaderQualifier", TestContext.TestName);
            ValidateCheckAddendaField("EWRERJFS", paymentAddendaFields, "REFID", TestContext.TestName);
            ValidateCheckAddendaField("W8P1D", paymentAddendaFields, "ReassociationTraceNumber", TestContext.TestName);

            //Validate there are no document addenda records
            Assert.AreEqual(0, documentAddendaFields.Count);
        }
    }
}
