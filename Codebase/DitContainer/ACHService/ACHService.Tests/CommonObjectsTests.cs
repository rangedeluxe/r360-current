﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonObjects;
using CommonObjects.Extensions;

namespace ACHService.Tests
{
    [TestClass]
    public class CommonObjectsTests
    {
            [TestMethod]
            public void ValidCommonBatchObject()
            {
                var batchDataRecord = CreateBatchDataRecord();
                var transactions = CreateTransaction();

                var theDate = DateTime.Today;
                var depositDate = Convert.ToDateTime("2019-10-01");
                var processingDate = Convert.ToDateTime("2019-01-20");
                var batchTracking = Guid.NewGuid();
                var newObject = new CommonBatch
                {
                    ABA = "1234567",
                    BankId = 99,
                    BatchCueID = 777,
                    BatchDate = theDate,
                    BatchId = 1492,
                    BatchNumber = 5,
                    BatchSiteCode = 24,
                    BatchSource = "check",
                    BatchTrackingID = batchTracking,
                    ClientId = 888,
                    DepositDate = depositDate,
                    DDA = "9876",
                    ProcessingDate = processingDate,
                    PaymentType = "paytype",
                    FileHash = "thehash",
                    FileSignature = "filesignature",
                    BatchRecords = new[] {batchDataRecord},
                    Transactions = transactions
                };
                Assert.AreEqual("1234567",newObject.ABA,"verify ABA");
                Assert.AreEqual(99, newObject.BankId, "verify BankId");
                Assert.AreEqual(777, newObject.BatchCueID, "verify BatchCueID");
                Assert.AreEqual(theDate, newObject.BatchDate, "verify BatchDate");
                Assert.AreEqual(1492, newObject.BatchId, "verify BatchId");
                Assert.AreEqual(5, newObject.BatchNumber, "verify BatchNumber");
                Assert.AreEqual(24, newObject.BatchSiteCode, "verify BatchSiteCode");
                Assert.AreEqual("check", newObject.BatchSource, "verify BatchSource");
                Assert.AreEqual(batchTracking, newObject.BatchTrackingID, "verify BatchTrackingID");
                Assert.AreEqual(888,newObject.ClientId,"verify ClientId");
                Assert.AreEqual(depositDate,newObject.DepositDate,"verify DepositDate");
                Assert.AreEqual("9876",newObject.DDA,"verify DDA");
                Assert.AreEqual(processingDate,newObject.ProcessingDate,"verify ProcessingDate");
                Assert.AreEqual("paytype",newObject.PaymentType,"verify PaymentType");
                Assert.AreEqual("thehash",newObject.FileHash,"verify FileHash");
                Assert.AreEqual("filesignature",newObject.FileSignature,"verify FileSignature");
                Assert.AreEqual(1,newObject.BatchRecords.Count,"verify BatchRecords");
                Assert.AreEqual(2,newObject.Transactions.Count,"verify transaction count");

        }

        [TestMethod]
        public void ValidatePayment()
        {
            var newObject = CreatePaymentData();
            
            Assert.AreEqual(Convert.ToDecimal(12.89), newObject.Amount,"verify the amount");
            Assert.AreEqual("thisisthert", newObject.RT,"verify the RT");
            Assert.AreEqual("thisistheaccount",newObject.Account,"verify the Account");
            Assert.AreEqual("thisistheserial", newObject.Serial, "verify the Serial");
            Assert.AreEqual("thisisthetransactioncode", newObject.TransactionCode, "verify the TransactionCode");
            Assert.AreEqual("thisistheremittername", newObject.RemitterName, "verify the RemitterName");
            Assert.AreEqual("thisistheaba", newObject.ABA, "verify the ABA");
            Assert.AreEqual("thisisthedda", newObject.DDA, "verify the DDA");
            Assert.AreEqual(2,newObject.RemittanceData.Count,"verify count of RemittanceData");
            Assert.AreEqual(2,newObject.RawData.Count,"verify Rawdata Count");

            var theRawData = newObject.RawData;
            Assert.AreEqual("thisistherawdata",theRawData[0].Data,"verify the RawData");

            var theRemittanceData = newObject.RemittanceData;
            Assert.AreEqual("ServiceClassCode", theRemittanceData[0][0].FieldName, "");
        }

        [TestMethod]
        public void ValidBatchDataRecord()
        {
            var batchDataRecord = CreateBatchDataRecord();
            var fieldsCount = batchDataRecord.fields.Count;

            Assert.AreEqual(2,fieldsCount,"valid count");
        }

        [TestMethod]
        public void ValidateDocument()
        {
            var newObject = CreateDocumentData();

            Assert.AreEqual("documentdescriptor",newObject.DocumentDescriptor,"verify DocumentDescriptor");
            Assert.AreEqual(57,newObject.DocumentSequence,"verify DocumentSequence");
            Assert.AreEqual(true,newObject.IsCorrespondence,"verify IsCorrespondence flag");
            Assert.AreEqual(45,newObject.SequenceWithinTransaction,"verify SequenceWithinTransaction");
            Assert.AreEqual(2,newObject.DocumentRemittanceData.Count,"verify count of DocuementRemittanceData");
        }

        

        [TestMethod]
        public void ValidateTransaction()
        {
            var newObject = CreateTransaction();

            Assert.AreEqual("transactionhash", newObject[0].TransactionHash,"verify TransactionHash");
            Assert.AreEqual("transactionsignature", newObject[0].TransactionSignature, "verify TransactionSignature");
            Assert.AreEqual(1,newObject[0].Documents.Count,"verify Documents Count");
            Assert.AreEqual(1,newObject[0].GhostDocuments.Count,"verify GhostDocuments Count");
            Assert.AreEqual(1,newObject[0].Payments.Count,"verify Payments Count");
        }

        [TestMethod]
        public void ValidateGhostDocument()
        {
            var newObject = CreateGhostDocument();

            Assert.AreEqual(true,newObject.IsCorrespondance,"verify IsCorrespondance");
            Assert.AreEqual(1,newObject.GhostFields.Count,"verify count of GhostFields");
        }

        [TestMethod]
        public void ValidatePaymentField()
        {
            var newObject = new PaymentField
            {
                PaymentId = 898
            };
            
            Assert.AreEqual(898,newObject.PaymentId,"verify PaymentId");
        }

        private Document CreateDocumentData()
        {
            var newObject = new Document
            {
                DocumentDescriptor = "documentdescriptor",
                DocumentSequence = 57,
                IsCorrespondence = true,
                SequenceWithinTransaction = 45,
                DocumentRemittanceData = new[] { CreateListOfFields(), CreateListOfFields() }
            };
            return newObject;
        }

        private BatchDataRecord CreateBatchDataRecord()
        {
            var listOfField = new List<Field>
            {
                new Field
                {
                    FieldName = "ServiceClassCode",
                    FieldValue = "TheServiceClassCode",
                    IsCheck = true, SequenceNumber = 0
                },
                new Field
                {
                    FieldName = "FieldName2",
                    FieldValue = "67",
                    IsCheck = true,
                    SequenceNumber = 1
                }
            };

            return new BatchDataRecord()
            {
                fields = listOfField
            };
        }

        private IList<Transaction> CreateTransaction()
        {
            var listOfTransactions = new List<Transaction>
            {
                new Transaction
                {
                    TransactionHash = "transactionhash",
                    TransactionSignature = "transactionsignature",
                    Payments = new[] {CreatePaymentData()},
                    Documents = new[] {CreateDocumentData()},
                    GhostDocuments = new[] {CreateGhostDocument()}
                },
                new Transaction
                {
                    TransactionHash = "thehash",
                    TransactionSignature = "thesignature"
                }
            };
            return listOfTransactions;
        }

        private IList<Field> CreateListOfFields()
        {
            var listOfField = new List<Field>
            {
                new Field
                {
                    FieldName = "ServiceClassCode",
                    FieldValue = "TheServiceClassCode",
                    IsCheck = true, SequenceNumber = 0
                },
                new Field
                {
                    FieldName = "FieldName2",
                    FieldValue = "67",
                    IsCheck = true,
                    SequenceNumber = 1
                }
            };
            return listOfField;
        }

        private IList<RawData> CreateRawData()
        {
            var rawData = new List<RawData>
            {
                new RawData
                {
                    Data = "thisistherawdata"
                } ,
                new RawData
                {
                    Data = "secondsetofrawdata"
                }
            };
            return rawData;
        }

        private Payment CreatePaymentData()
        {
            var payment = new Payment
            {
                Amount = Convert.ToDecimal(12.89),
                RT = "thisisthert",
                Account = "thisistheaccount",
                Serial = "thisistheserial",
                TransactionCode = "thisisthetransactioncode",
                RemitterName = "thisistheremittername",
                ABA = "thisistheaba",
                DDA = "thisisthedda",
                RemittanceData = new[] { CreateListOfFields(), CreateListOfFields() },
                RawData = CreateRawData()
            };

            return payment;
        }

        private GhostDocument CreateGhostDocument()
        {
            var ghostDocument = new GhostDocument
            {
                IsCorrespondance = true,
                GhostFields = new[] {CreateGhostFieldData()}
            };
            return ghostDocument;
        }

        private GhostField CreateGhostFieldData()
        {
            return new GhostField();
        }
    }
}
