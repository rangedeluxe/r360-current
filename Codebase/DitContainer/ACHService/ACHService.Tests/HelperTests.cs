﻿using System;
using System.Collections.Generic;
using System.Text;
using ACHService.Controllers;
using ACHService.Helpers;
using DpapiCrypto;
using DpapiCrypto.Dto;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NLog;
using Logger = Microsoft.VisualStudio.TestTools.UnitTesting.Logging.Logger;

namespace ACHService.Tests
{
    [TestClass]
    public class HelperTests
    {
        private Mock<Microsoft.Extensions.Logging.ILogger<DatabaseHelper>> logger;
        private Mock<IConfiguration> configuration;

        [TestInitialize]
        public void Setup()
        {
            logger = new Mock<Microsoft.Extensions.Logging.ILogger<DatabaseHelper>>();
            configuration = new Mock<IConfiguration>();
        }

        [TestMethod]
        public void ValidConnectionString()
        {
            string connectionString =
                "Server=AnyServer;Database=AnyDatabase;User ID=theuser;Password=thepassword;";
            configuration.SetupGet(x => x[It.IsAny<string>()]).Returns(BuildValidConnectionString(connectionString));
            var response = new DatabaseHelper(configuration.Object, logger.Object);
            Assert.AreEqual(connectionString, response.ConnectionString);
        }

        [TestMethod]
        public void InValidConnectionString()
        {
            string connectionString =
                "Server=AnyServer;Database=AnyDatabase;User ID=theuser;Password=thepassword;";
            configuration.SetupGet(x => x[It.IsAny<string>()]).Returns(BuildInValidUserConnectionString(connectionString));
            var response = new DatabaseHelper(configuration.Object, logger.Object);
            Assert.AreEqual(configuration.Object["database: connectionString"], response.ConnectionString);
        }

        [TestMethod]
        public void UnencryptedConnectionString()
        {
            string connectionString =
                "Server=AnyServer;Database=AnyDatabase;User ID=theuser;Password=thepassword;";
            configuration.SetupGet(x => x[It.IsAny<string>()]).Returns(connectionString);
            var response = new DatabaseHelper(configuration.Object, logger.Object);
            Assert.AreEqual(connectionString, response.ConnectionString);

        }

        [TestMethod]
        public void MissingConnectionString()
        {
            string connectionString = String.Empty;
               // "Server=AnyServer;Database=AnyDatabase;User ID=theuser;Password=thepassword;";
            configuration.SetupGet(x => x[It.IsAny<string>()]).Returns(BuildInValidPasswordConnectionString(connectionString));
            var response = new DatabaseHelper(configuration.Object, logger.Object);
            Assert.AreEqual(connectionString, response.ConnectionString);
            //Assert.AreEqual(connectionString.Replace("thepassword", ""), response.ConnectionString);
        }

        private string BuildValidConnectionString(string connectionString)
        {
            CryptoDpapi crypto = new CryptoDpapi();
            var encryptedConnectionString =  crypto.Encrypt(connectionString);
            return encryptedConnectionString.ReturnedString;
        }

        private string BuildInValidUserConnectionString(string connectionString)
        {
            CryptoDpapi crypto = new CryptoDpapi();
            var encryptedConnectionString = crypto.Encrypt(connectionString);
            return encryptedConnectionString.ReturnedString.Remove(0, 2);
        }

        private string BuildInValidPasswordConnectionString(string connectionString)
        {
            CryptoDpapi crypto = new CryptoDpapi();
            DpapiResponse userId = crypto.Encrypt("theuser");
            DpapiResponse password = crypto.Encrypt("thepassword");
            var invalidPassword = new StringBuilder(password.ReturnedString).Remove(0,2);
            return connectionString.Replace("theuser", userId.ReturnedString)
                .Replace("thepassword", invalidPassword.ToString());
        }
    }
}
