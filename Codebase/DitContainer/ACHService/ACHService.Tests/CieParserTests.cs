﻿
using ACHConverter.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ACHService.Tests
{
    [TestClass]
    public class CieParserTests
    {
        [TestMethod]
        public void ValidCiePaymentRecord()
        {
            var payment = CieParser.Parse("632666666670666666671        0000223755   IndividualIdFRODO BAGGINS         XX1000000000000001", 1);

            Assert.AreEqual(0, payment.Errors.Count, "ErrorCount");
            Assert.AreEqual("6", payment.Result.RecordType, "RecordType");
            Assert.AreEqual("32", payment.Result.TransactionCode, "TransactionCode");
            Assert.AreEqual("666666671", payment.Result.DDA, "DDA");
            Assert.AreEqual("666666670", payment.Result.ABA, "ABA");
            Assert.AreEqual((decimal)2237.55, payment.Result.Amount, "Amount");
            Assert.AreEqual("   IndividualId", payment.Result.IndividualName, "IndividualName");
            Assert.AreEqual("666666671        ", payment.Result.AccountNumber, "AccountNumber");
            Assert.AreEqual("FRODO BAGGINS         ", payment.Result.IndividualId, "IndividualId");
            Assert.AreEqual("000000000000001", payment.Result.TraceNumber, "TraceNumber");
        }

	    [TestMethod]
	    public void InvalidCieAmount()
	    {
		    var batch =
			    CieParser.Parse("622072000096000000090123456711YYYY32646VISHAY AMERICAS00095954-2001931405     1091004447858009", 1);
		    Assert.AreEqual(1, batch.Errors.Count);
		    Assert.AreEqual("Error parsing Batch Detail value:\tLine number: 1\tField: Amount\tValue: 1YYYY32646", batch.Errors[0]);
	    }

	}
}
