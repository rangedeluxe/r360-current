﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACHConverter;
using ACHConverter.Models;
using ACHService.Abstractions;
using ACHService.Implementations;
using CommonObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ACHService.Tests
{
    [TestClass]
    public class ACHRuleParserTests
    {
        private List<AddendaRule> _rules;
        private List<string> _theAddenda;

        private AddendaRule AddAddendaRule(string addendaSegmentName, string segmentIdentifier, string fieldName,
            int fieldNumber, string aba = "1", string dda = "1", string segmentQualifier = null, bool isCheck = false,
            string fieldDelimiter = "*", string segmentDelimiter = "\\", string addendaName = "Test",
            bool createNewRecord = false)
        {
            return new AddendaRule
            {
                AddendaSegmentName = addendaSegmentName,
                SegmentIdentifier = segmentIdentifier,
                FieldName = fieldName,
                FieldNumber = fieldNumber,
                ABA = aba,
                DDA = dda,
                SegmentQualifier = segmentQualifier,
                IsCheck = isCheck,
                FieldDelimiter = fieldDelimiter,
                SegmentDelimiter = segmentDelimiter,
                AddendaName = addendaName,
                CreateNewRecord = createNewRecord
            };
        }

        [TestInitialize]
        public void Setup()
        {
            _rules = new List<AddendaRule>
            {
                AddAddendaRule("Remittance Advice Accounts Receivable", "RMR", "RMRReferenceNumber", 2,
                    segmentQualifier: "IV", createNewRecord: true),
                AddAddendaRule("Remittance Advice Accounts Receivable", "RMR", "RMRMonetaryAmount", 4,
                    segmentQualifier: "IV", createNewRecord: true),
                AddAddendaRule("Remittance Advice Accounts Receivable", "RMR", "TotalInvoiceAmount", 5,
                    segmentQualifier: "IV", createNewRecord: true),
                AddAddendaRule("Remittance Advice Accounts Receivable", "RMR", "DiscountAmount", 6,
                    segmentQualifier: "IV", createNewRecord: true),
                AddAddendaRule("Remittance Advice Accounts Receivable", "BPR", "BPRMonetaryAmount", 2, isCheck: true,
                    createNewRecord: true),
                AddAddendaRule("Remittance Advice Accounts Receivable", "BPR", "CreditIndicator", 3, isCheck: true),
                AddAddendaRule("Remittance Advice Accounts Receivable", "BPR", "BPRAccountNumber", 9, isCheck: true),
                AddAddendaRule("Remittance Advice Accounts Receivable", "DTM", "InvoiceDate", 2),
                AddAddendaRule("Remittance Advice Accounts Receivable", "N1", "Payer", 2, segmentQualifier: "PR",
                    isCheck: true),
                AddAddendaRule("Remittance Advice Accounts Receivable", "N1", "Payee", 2, segmentQualifier: "PE",
                    isCheck: true)
            };

            _theAddenda = new List<string>
            {
                @"705ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010465021",
                @"7054*U*00400*076913354*0*P*>\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020465021",
                @"705010\ST*820*000000268\BPR*C*6326.46*C*ACH*CTX*01*053101561*DA*2079900567889*4133700030465021",
                @"70584645**01*072000096*DA*9012345678*20130514\TRN*1*5954-2001931405\CUR*PE*USD**PR*00040465021",
                @"705USD\N1*PR*ContinentalUSA\N1*BK*COMERICA BANK\N3*COMERICA BANK\N4****US\N1*PE*VIS00050465021",
                @"705HAY AMERICAS*ZZ*0000201077\N3*1122-23 RD. STREET\N4*COLUMBUS*NE*686020609\ENT*1\00060465021",
                @"705RMR*IV*0688147387**735*735*0\DTM*003*20130214\RMR*IV*0688153103**5160.96*5160.9600070465021",
                @"705*0\DTM*003*20130214\RMR*IV*0688155759**430.5*430.5*0\DTM*003*20130215\SE*19*000000080465021",
                @"70500268\GE*1*76913354\IEA*1*076913354\                                            00090465021"
            };
        }

        [TestMethod]
        public void BasicAddendaContainsSequenceNumbers()
        {
            var data = _theAddenda.Select(Addenda.Parse).Select(a => a.Result.Data);
            var ruleParser = new ACHRuleParser();
            var result = ruleParser.ParseAddenda(_rules, string.Join(string.Empty, data), "ctx").Result;
            var seq1 = result.Where(f => f.SequenceNumber == 1);
            Assert.AreEqual(5, seq1.Count());
            var seq2 = result.Where(f => f.SequenceNumber == 2);
            Assert.AreEqual(5, seq2.Count());
            var seq3 = result.Where(f => f.SequenceNumber == 3);
            Assert.AreEqual(5, seq3.Count());
            var seq4 = result.Where(f => f.SequenceNumber == 4);
            Assert.AreEqual(5, seq4.Count());
        }

        [TestMethod]
        public void BasicAddendaContainsCorrectData()
        {
            var data = _theAddenda.Select(Addenda.Parse).Select(a => a.Result.Data);
            var ruleParser = new ACHRuleParser();
            var result = ruleParser.ParseAddenda(_rules, string.Join(string.Empty, data), "ctx").Result;
            var seq1 = result.Where(f => f.SequenceNumber == 1);
            Assert.AreEqual("6326.46", seq1.First(f => f.FieldName == "BPRMonetaryAmount").FieldValue);
        }

        [TestMethod]
        public void BasicPaymentParsing()
        {
            var data = _theAddenda.Select(Addenda.Parse).Select(a => a.Result.Data);
            var ruleParser = new ACHRuleParser();
            var result = ruleParser.ParseAddenda(_rules, string.Join(string.Empty, data), "ctx").Result;

            Assert.AreEqual(1, result.Count(d => d.IsCheck && d.FieldName == "BPRMonetaryAmount"),
                "BPRMonetaryAmount Count");
            Assert.AreEqual("6326.46",
                result.First(d => d.IsCheck && d.FieldName == "BPRMonetaryAmount" && d.SequenceNumber == 1).FieldValue,
                "BPRMonetaryAmount");
            Assert.AreEqual("2079900567889",
                result.First(d => d.IsCheck && d.FieldName == "BPRAccountNumber" && d.SequenceNumber == 1).FieldValue,
                "BPRAccountNumber");
            Assert.AreEqual("C",
                result.First(d => d.IsCheck && d.FieldName == "CreditIndicator" && d.SequenceNumber == 1).FieldValue,
                "CreditIndicator");
            Assert.AreEqual("ContinentalUSA",
                result.First(d => d.IsCheck && d.FieldName == "Payer" && d.SequenceNumber == 1).FieldValue, "Payer");
            Assert.AreEqual("VISHAY AMERICAS",
                result.First(d => d.IsCheck && d.FieldName == "Payee" && d.SequenceNumber == 1).FieldValue, "Payee");
        }

        [TestMethod]
        public void BasicRemittanceParsing()
        {
            var data = _theAddenda.Select(Addenda.Parse).Select(a => a.Result.Data);
            var ruleParser = new ACHRuleParser();
            var result = ruleParser.ParseAddenda(_rules, string.Join(string.Empty, data), "ccd").Result;

            Assert.AreEqual(3, result.Count(d => !d.IsCheck && d.FieldName == "RMRReferenceNumber"),
                "RMRReferenceNumber Count");
            Assert.AreEqual(3, result.Count(d => !d.IsCheck && d.FieldName == "InvoiceDate"), "InvoiceDate Count");
            Assert.AreEqual("0688153103",
                result.First(d => !d.IsCheck && d.FieldName == "RMRReferenceNumber" && d.SequenceNumber == 3)
                    .FieldValue, "RMRReferenceNumber");
            Assert.AreEqual("735",
                result.First(d => !d.IsCheck && d.FieldName == "RMRMonetaryAmount" && d.SequenceNumber == 2).FieldValue,
                "RMRMonetaryAmount");
            Assert.AreEqual("430.5",
                result.First(d => !d.IsCheck && d.FieldName == "TotalInvoiceAmount" && d.SequenceNumber == 4)
                    .FieldValue, "TotalInvoiceAmount");
            Assert.AreEqual("0",
                result.First(d => !d.IsCheck && d.FieldName == "DiscountAmount" && d.SequenceNumber == 2).FieldValue,
                "DiscountAmount");
            Assert.AreEqual("20130215",
                result.First(d => !d.IsCheck && d.FieldName == "InvoiceDate" && d.SequenceNumber == 4).FieldValue,
                "InvoiceDate");
        }

        [TestMethod]
        public void AdvancedDtmRefParsing()
        {
            var addendaData = new List<string>
            {
                @"705ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001",
                @"7054*U*00400*076913354*0*P*>\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001",
                @"705010\ST*820*000000268\BPR*C*0.01*C*ACH*CTX*01*65465465*DA*0GHYK50KM*IU53S6X89\TRN00030000001",
                @"705*1*8PM4L\REF*2U*VFZ1U22QBE\N1*PE*ICHIDTVUTSXT6R2\N3*23434*Main Street\N4*SomeCit00040000001",
                @"705y*USA*55555\DTM*003*20000911\RMR*IV*IMBW0**0.33*36.62*0\DTM*001*20031028\REF*DS*00050000001",
                @"705TIXBEHNWNZRWEMV\RMR*IV*WE6YH**5.66*17.16*0\DTM*001*20030219\REF*DS*WVO5S1H2GTKFP00060000001",
                @"7050O\RMR*IV*Q6RWQ**810.77*0.01*0\DTM*001*20110813\REF*DS*9GG12J8ZR2C0M8Z\RMR*IV*J200070000001",
                @"705UII**0.44*0.03*0\DTM*001*20180601\REF*DS*PSDNIY7LNRFJYVW\SE*19*000000268\GE*1*7600080000001",
                @"705913354\IEA*1*076913354\                                                         00090000001",
            };

            var parsingRules = new List<AddendaRule>
            {
                AddAddendaRule("ACH - Expanded Template BPR", "BPR", "BPRMonetaryAmount", 2, isCheck: true,
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template TRN", "TRN", "ReassociationTraceNumber", 2),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDateQualifier", 1, isCheck: true),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDate", 2, segmentQualifier: "003",
                    isCheck: true),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFHeaderQualifier", 1, isCheck: true),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFID", 2, segmentQualifier: "2U", isCheck: true),
                AddAddendaRule("ACH - Expanded Template RMR", "RMR", "RMRReferenceNumber", 2, segmentQualifier: "IV",
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template RMR", "RMR", "RMRMonetaryAmount", 4, segmentQualifier: "IV",
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template RMR", "RMR", "TotalInvoiceAmount", 5, segmentQualifier: "IV",
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDateQualifier", 1),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDate", 2),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFDetailQualifier", 1),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFID", 2, segmentQualifier: "DS")
            };

            var ruleParser = new ACHRuleParser();
            var data = addendaData.Select(Addenda.Parse).Select(a => a.Result.Data);

            var result = ruleParser.ParseAddenda(parsingRules, string.Join(string.Empty, data), "ccd").Result;

            // verify all the counts match expected results.
            Assert.AreEqual(1,
                result.Count(d => d.IsCheck && d.FieldName == "DTMDateQualifier" && d.FieldValue == "003"),
                "Check DTM");
            Assert.AreEqual(0,
                result.Count(d => !d.IsCheck && d.FieldName == "DTMDateQualifier" && d.FieldValue == "003"),
                "Check DTM not returned as stub");
            Assert.AreEqual(1, result.Count(d => d.IsCheck && d.FieldName == "REFHeaderQualifier"),
                "Check REFHeaderQualifier");
            Assert.AreEqual(0, result.Count(d => d.IsCheck && d.FieldName == "REFDetailQualifier"),
                "Check REFDetailQualifier not returned as stub");
            Assert.AreEqual(4,
                result.Count(d => !d.IsCheck && d.FieldName == "DTMDateQualifier" && d.FieldValue == "001"),
                "Stub DTM");
            Assert.AreEqual(0,
                result.Count(d => d.IsCheck && d.FieldName == "DTMDateQualifier" && d.FieldValue == "001"),
                "Stub DTM not returned as check");
            Assert.AreEqual(4, result.Count(d => !d.IsCheck && d.FieldName == "REFDetailQualifier"), "Stub REF");
            Assert.AreEqual(0, result.Count(d => !d.IsCheck && d.FieldName == "REFHeaderQualifier"),
                "Stub REF not returned as check");

            // detail check of the BPR/payment data
            Assert.AreEqual("0.01",
                result.First(d => d.IsCheck && d.FieldName == "BPRMonetaryAmount" && d.SequenceNumber == 1).FieldValue,
                "BPRMonetaryAmount");
            // TRN is defined as a stub item even though it is part of the payment data, double check that it is parsed as stub data
            Assert.AreEqual("8PM4L",
                result.First(d => !d.IsCheck && d.FieldName == "ReassociationTraceNumber" && d.SequenceNumber == 1)
                    .FieldValue, "ReassociationTraceNumber");

            Assert.AreEqual("003",
                result.First(d => d.IsCheck && d.FieldName == "DTMDateQualifier" && d.SequenceNumber == 1).FieldValue,
                "Check DTMDateQualifier");
            Assert.AreEqual("20000911",
                result.First(d => d.IsCheck && d.FieldName == "DTMDate" && d.SequenceNumber == 1).FieldValue,
                "Check DTMDate");

            Assert.AreEqual("2U",
                result.First(d => d.IsCheck && d.FieldName == "REFHeaderQualifier" && d.SequenceNumber == 1).FieldValue,
                "Check DTMDateQualifier");
            Assert.AreEqual("VFZ1U22QBE",
                result.First(d => d.IsCheck && d.FieldName == "REFID" && d.SequenceNumber == 1).FieldValue,
                "Check REFID");

            // detail check of the RMR/remittance data
            // sequence 2, i.e. stub sequence 1
            Assert.AreEqual("001",
                result.First(d => !d.IsCheck && d.FieldName == "DTMDateQualifier" && d.SequenceNumber == 2).FieldValue,
                "Stub Seq 1 DTMDateQualifier");
            Assert.AreEqual("DS",
                result.First(d => !d.IsCheck && d.FieldName == "REFDetailQualifier" && d.SequenceNumber == 2)
                    .FieldValue, "Stub Seq 1 REFDetailQualifier");
            Assert.AreEqual("IMBW0",
                result.First(d => !d.IsCheck && d.FieldName == "RMRReferenceNumber" && d.SequenceNumber == 2)
                    .FieldValue, "Stub Seq 1 RMRReferenceNumber");
            Assert.AreEqual("0.33",
                result.First(d => !d.IsCheck && d.FieldName == "RMRMonetaryAmount" && d.SequenceNumber == 2).FieldValue,
                "Stub Seq 1 RMRMonetaryAmount");
            Assert.AreEqual("36.62",
                result.First(d => !d.IsCheck && d.FieldName == "TotalInvoiceAmount" && d.SequenceNumber == 2)
                    .FieldValue, "Stub Seq 1 TotalInvoiceAmount");
            Assert.AreEqual("20031028",
                result.First(d => !d.IsCheck && d.FieldName == "DTMDate" && d.SequenceNumber == 2).FieldValue,
                "Stub Seq 1 DTMDate");
            Assert.AreEqual("TIXBEHNWNZRWEMV",
                result.First(d => !d.IsCheck && d.FieldName == "REFID" && d.SequenceNumber == 2).FieldValue,
                "Stub Seq 1 REFID");

            // sequence 3, i.e. stub sequence 2
            Assert.AreEqual("001",
                result.First(d => !d.IsCheck && d.FieldName == "DTMDateQualifier" && d.SequenceNumber == 3).FieldValue,
                "Stub Seq 2 DTMDateQualifier");
            Assert.AreEqual("DS",
                result.First(d => !d.IsCheck && d.FieldName == "REFDetailQualifier" && d.SequenceNumber == 3)
                    .FieldValue, "Stub Seq 2 REFDetailQualifier");
            Assert.AreEqual("WE6YH",
                result.First(d => !d.IsCheck && d.FieldName == "RMRReferenceNumber" && d.SequenceNumber == 3)
                    .FieldValue, "Stub Seq 2 RMRReferenceNumber");
            Assert.AreEqual("5.66",
                result.First(d => !d.IsCheck && d.FieldName == "RMRMonetaryAmount" && d.SequenceNumber == 3).FieldValue,
                "Stub Seq 2 RMRMonetaryAmount");
            Assert.AreEqual("17.16",
                result.First(d => !d.IsCheck && d.FieldName == "TotalInvoiceAmount" && d.SequenceNumber == 3)
                    .FieldValue, "Stub Seq 2 TotalInvoiceAmount");
            Assert.AreEqual("20030219",
                result.First(d => !d.IsCheck && d.FieldName == "DTMDate" && d.SequenceNumber == 3).FieldValue,
                "Stub Seq 2 DTMDate");
            Assert.AreEqual("WVO5S1H2GTKFP0O",
                result.First(d => !d.IsCheck && d.FieldName == "REFID" && d.SequenceNumber == 3).FieldValue,
                "Stub Seq 2 REFID");

            // sequence 4, i.e. stub sequence 3
            Assert.AreEqual("001",
                result.First(d => !d.IsCheck && d.FieldName == "DTMDateQualifier" && d.SequenceNumber == 4).FieldValue,
                "Stub Seq 3 DTMDateQualifier");
            Assert.AreEqual("DS",
                result.First(d => !d.IsCheck && d.FieldName == "REFDetailQualifier" && d.SequenceNumber == 4)
                    .FieldValue, "Stub Seq 3 REFDetailQualifier");
            Assert.AreEqual("Q6RWQ",
                result.First(d => !d.IsCheck && d.FieldName == "RMRReferenceNumber" && d.SequenceNumber == 4)
                    .FieldValue, "Stub Seq 3 RMRReferenceNumber");
            Assert.AreEqual("810.77",
                result.First(d => !d.IsCheck && d.FieldName == "RMRMonetaryAmount" && d.SequenceNumber == 4).FieldValue,
                "Stub Seq 2 RMRMonetaryAmount");
            Assert.AreEqual("0.01",
                result.First(d => !d.IsCheck && d.FieldName == "TotalInvoiceAmount" && d.SequenceNumber == 4)
                    .FieldValue, "Stub Seq 3 TotalInvoiceAmount");
            Assert.AreEqual("20110813",
                result.First(d => !d.IsCheck && d.FieldName == "DTMDate" && d.SequenceNumber == 4).FieldValue,
                "Stub Seq 3 DTMDate");
            Assert.AreEqual("9GG12J8ZR2C0M8Z",
                result.First(d => !d.IsCheck && d.FieldName == "REFID" && d.SequenceNumber == 4).FieldValue,
                "Stub Seq 3 REFID");

            // sequence 5, i.e. stub sequence 4
            Assert.AreEqual("001",
                result.First(d => !d.IsCheck && d.FieldName == "DTMDateQualifier" && d.SequenceNumber == 5).FieldValue,
                "Stub Seq 4 DTMDateQualifier");
            Assert.AreEqual("DS",
                result.First(d => !d.IsCheck && d.FieldName == "REFDetailQualifier" && d.SequenceNumber == 5)
                    .FieldValue, "Stub Seq 4 REFDetailQualifier");
            Assert.AreEqual("J2UII",
                result.First(d => !d.IsCheck && d.FieldName == "RMRReferenceNumber" && d.SequenceNumber == 5)
                    .FieldValue, "Stub Seq 4 RMRReferenceNumber");
            Assert.AreEqual("0.44",
                result.First(d => !d.IsCheck && d.FieldName == "RMRMonetaryAmount" && d.SequenceNumber == 5).FieldValue,
                "Stub Seq 4 RMRMonetaryAmount");
            Assert.AreEqual("0.03",
                result.First(d => !d.IsCheck && d.FieldName == "TotalInvoiceAmount" && d.SequenceNumber == 5)
                    .FieldValue, "Stub Seq 4 TotalInvoiceAmount");
            Assert.AreEqual("20180601",
                result.First(d => !d.IsCheck && d.FieldName == "DTMDate" && d.SequenceNumber == 5).FieldValue,
                "Stub Seq 4 DTMDate");
            Assert.AreEqual("PSDNIY7LNRFJYVW",
                result.First(d => !d.IsCheck && d.FieldName == "REFID" && d.SequenceNumber == 5).FieldValue,
                "Stub Seq 4 REFID");
        }


        [TestMethod]
        public void PerParsing()
        {
            var addendaData = new List<string>
            {
                @"705ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001",
                @"7054*U*00400*076913354*0*P*>\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001",
                @"705010\ST*820*000000268\BPR*C*0.09*C*ACH*CTX*01*65465465*DA*GBJ6HDLYB*0E10C1F1L\TRN00030000001",
                @"705*1*YGGFE\REF*80*ZHML2GGG59\N1*PE*FS2GA3HYWYGV261\N3*23434*Main Street\N4*SomeCit00040000001",
                @"705y*USA*55555\PER*CN*CompanyName*EM*emailaddress@something.com\N1*O1*Wells Fargo\D00050000001",
                @"705TM*003*20150122\RMR*IV*GYNLF**0.09*0.05*0\DTM*003*20060702\REF*DS*7DF5T49YATW7YU00060000001",
                @"7056\RMR*IV*YXFC5**3.32*642.89*0\DTM*003*20100710\REF*DS*J7WVXCPD6F5FLL0\SE*19*000000070000001",
                @"70500268\GE*1*76913354\IEA*1*076913354\                                            00080000001"
            };

            var parsingRules = new List<AddendaRule>
            {
                AddAddendaRule("ACH - Expanded Template BPR", "BPR", "BPRMonetaryAmount", 2, isCheck: true,
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template TRN", "TRN", "ReassociationTraceNumber", 2),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDateQualifier", 1, isCheck: true),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDate", 2, segmentQualifier: "003",
                    isCheck: true),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFHeaderQualifier", 1, isCheck: true),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFID", 2, segmentQualifier: "2U", isCheck: true),
                AddAddendaRule("ACH - Expanded Template RMR", "RMR", "RMRReferenceNumber", 2, segmentQualifier: "IV",
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template RMR", "RMR", "RMRMonetaryAmount", 4, segmentQualifier: "IV",
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template RMR", "RMR", "TotalInvoiceAmount", 5, segmentQualifier: "IV",
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDateQualifier", 1),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDate", 2),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFDetailQualifier", 1),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFID", 2, segmentQualifier: "DS"),
                AddAddendaRule("ACH - Expanded Template REF", "PER", "Email", 4, isCheck: true)
            };

            var ruleParser = new ACHRuleParser();
            var data = addendaData.Select(Addenda.Parse).Select(a => a.Result.Data);

            var result = ruleParser.ParseAddenda(parsingRules, string.Join(string.Empty, data), "ccd").Result;

            Assert.AreEqual(1,
                result.Count(d => d.IsCheck && d.FieldName == "Email" && d.FieldValue == "emailaddress@something.com"),
                "Email Address");

        }

        [TestMethod]
        public void CanParseMultiSegmentDelimiter()
        {
            var addendaData = new List<string>
            {
                @"710BUS00000000000707825309227536              VISTEON CORPORATION                      0603412",
                @"711FORD MOTOR COMPANY SA DE CV        CTO H. HERNANEZ CAMARENA 1500                    0603412",
                @"712MEXICO*DF\                         MX*\                                             0603412",
                @"713THE BANK OF NOVA SCOTIA            01000247696                         CA           0603412",
                @"714COMERICA BANK                      01888888888                         US           0603412",
                @"715RFC            P.O. BOX 70386                                                       0603412",
                @"716CHICAGO*IL\                        US*60673\                                        0603412"
            };

            var parsingRules = new List<AddendaRule>
            {
                AddAddendaRule("ACH - Expanded Template BPR", "BPR", "BPRMonetaryAmount", 2, isCheck: true,
                    createNewRecord: true, segmentDelimiter:"\\~"),
                AddAddendaRule("ACH - Expanded Template TRN", "TRN", "ReassociationTraceNumber", 2),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDateQualifier", 1, isCheck: true),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDate", 2, segmentQualifier: "003",
                    isCheck: true),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFHeaderQualifier", 1, isCheck: true),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFID", 2, segmentQualifier: "2U", isCheck: true),
                AddAddendaRule("ACH - Expanded Template RMR", "RMR", "RMRReferenceNumber", 2, segmentQualifier: "IV",
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template RMR", "RMR", "RMRMonetaryAmount", 4, segmentQualifier: "IV",
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template RMR", "RMR", "TotalInvoiceAmount", 5, segmentQualifier: "IV",
                    createNewRecord: true),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDateQualifier", 1),
                AddAddendaRule("ACH - Expanded Template DTM", "DTM", "DTMDate", 2),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFDetailQualifier", 1),
                AddAddendaRule("ACH - Expanded Template REF", "REF", "REFID", 2, segmentQualifier: "DS"),
                AddAddendaRule("ACH - Expanded Template REF", "PER", "Email", 4, isCheck: true)
            };

            var ruleParser = new ACHRuleParser();
            var data = addendaData.Select(Addenda.Parse).Select(a => a.Result.Data);

            var result = ruleParser.ParseAddenda(parsingRules, string.Join(string.Empty, data), "iat");

            Assert.AreEqual(0, result.Errors.Count);
            Assert.AreEqual(0, result.Result.Count);

        }
    }
}
