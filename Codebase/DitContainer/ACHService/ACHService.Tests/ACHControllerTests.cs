﻿using ACHService.Controllers;
using ACHService.DataProviders;
using ACHService.Models;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Reflection;
using ACHService.Abstractions;
using System.Threading;
using System.Threading.Tasks;
using ACHService.Helpers;
using CommonObjects.Abstractions;

namespace ACHService.Tests
{
    [TestClass]
    public class ACHControllerTests
    {
        private Mock<ILogger<ACHController>> logger;
        private Mock<HostingBackgroundQueue.IBackgroundQueue> backgroundQueue;
        private Mock<IDataImportTracking> dataImportTracking;
        private Mock<IDataImportQueue> dataImportQueue;
        private Mock<IAlertDataProvider> alertProvider;
        private Mock<IFileSystem> fileSystem;
        private ACHController controller;
        private Guid sourceTrackingId;
        private Guid batchTrackingId;
        private Mock<IFormFile> formFile;
        private Mock<IRulesProvider> rulesProvider;
        private Mock<IDatabaseHelper> databaseHelper;

        [TestInitialize]
        public void Setup()
        {
            logger = new Mock<ILogger<ACHController>>();
            backgroundQueue = new Mock<HostingBackgroundQueue.IBackgroundQueue>();
            dataImportTracking = new Mock<IDataImportTracking>();
            alertProvider = new Mock<IAlertDataProvider>();
            dataImportQueue = new Mock<IDataImportQueue>();
            fileSystem = new Mock<IFileSystem>();
            rulesProvider = new Mock<IRulesProvider>();
            databaseHelper = new Mock<IDatabaseHelper>();
            controller = new ACHController(logger.Object, backgroundQueue.Object, dataImportTracking.Object, alertProvider.Object, 
                dataImportQueue.Object, fileSystem.Object, rulesProvider.Object, databaseHelper.Object);
            sourceTrackingId = Guid.NewGuid();
            batchTrackingId = Guid.NewGuid();
            formFile = new Mock<IFormFile>();
        }
        
        [TestMethod]
        public void Can_GetStatus_WhenFileIsProcessing()
        {
            //arrange
            dataImportTracking
                .Setup(m => m.GetTrackingInfo(It.IsAny<Guid>()))
                .Returns(new DataImportTracking { SourceTrackingId = sourceTrackingId, FileStatus = DITProcessingStatus.Processing });

            //act
            var result = controller.Get(sourceTrackingId);
            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(JsonResult));
            var jsonResult = result as JsonResult;
            Assert.AreEqual(jsonResult.StatusCode, 200);
            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOfType(jsonResult.Value, typeof(ServiceResponse<DitTrackingResponse>));
            var jsonBody = jsonResult.Value as ServiceResponse<DitTrackingResponse>;
            Assert.IsNotNull(jsonBody.Data);
            Assert.AreEqual(jsonBody.Data.FileStatus, DITProcessingStatus.Processing);
        }
        
        [TestMethod]
        public void Can_CheckForCompleteFile()
        {
            //arrange
            dataImportTracking
                .Setup(m => m.GetTrackingInfo(It.IsAny<Guid>()))
                .Returns(new DataImportTracking { SourceTrackingId = sourceTrackingId, FileStatus = DITProcessingStatus.Completed, BatchTrackingIds = $"<Batch BatchTrackingID=\"{batchTrackingId}\"/>" });

            //act
            var result = controller.Get(sourceTrackingId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(JsonResult));

            var jsonResult = result as JsonResult;
            Assert.AreEqual(jsonResult.StatusCode, 200);
            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOfType(jsonResult.Value, typeof(ServiceResponse<DitTrackingResponse>));

            var jsonBody = jsonResult.Value as ServiceResponse<DitTrackingResponse>; 
            Assert.IsNotNull(jsonBody);
            Assert.IsNotNull(jsonBody.Data);
            Assert.AreEqual(jsonBody.Data.FileStatus, DITProcessingStatus.Completed);
            Assert.AreEqual($"<Batch BatchTrackingID=\"{batchTrackingId}\"/>", jsonBody.Data.BatchTrackingIds);
        }

        [TestMethod]
        public void Can_HandleServerException_WhenCheckingForCompleteFile()
        {
            //arrange
            dataImportTracking
                .Setup(m => m.GetTrackingInfo(It.IsAny<Guid>()))
                .Throws(new NotImplementedException("This functionality ain't happening!"));

            //act
            var result = controller.Get(sourceTrackingId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(JsonResult));

            var jsonResult = result as JsonResult;
            Assert.AreEqual(StatusCodes.Status500InternalServerError, jsonResult.StatusCode, "This should have returned a 500 http status code for internal server error.");
            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOfType(jsonResult.Value, typeof(ServiceResponse<DITProcessingStatus>));

            var jsonBody = jsonResult.Value as ServiceResponse<DITProcessingStatus>;
            Assert.IsNotNull(jsonBody);
            Assert.IsNotNull(jsonBody.Errors);
            Assert.AreEqual(1, jsonBody.Errors.Count);
            Assert.AreEqual("An issue was encountered when attempting to complete the request.", jsonBody.Errors.First());
            Assert.IsNotNull(jsonBody.Data);
            Assert.AreEqual(jsonBody.Data, DITProcessingStatus.Error);
        }

        [TestMethod]
        public void Can_HandleMissingTrackingInformation_WhenCheckingForCompleteFile()
        {
            //arrange
            DataImportTracking trackingInfo = null;
            dataImportTracking
                .Setup(m => m.GetTrackingInfo(It.IsAny<Guid>()))
                .Returns(trackingInfo);

            //act
            var result = controller.Get(sourceTrackingId);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(JsonResult));

            var jsonResult = result as JsonResult;
            Assert.AreEqual(jsonResult.StatusCode, 200);
            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOfType(jsonResult.Value, typeof(ServiceResponse<DitTrackingResponse>));

            var jsonBody = jsonResult.Value as ServiceResponse<DitTrackingResponse>;
            Assert.IsNotNull(jsonBody);
            Assert.IsNotNull(jsonBody.Data);
            Assert.AreEqual(jsonBody.Data.FileStatus, DITProcessingStatus.Notfound);
        }

        [TestMethod]
        public void Can_PostFile()
        {
            //arrange
            var dataImportTrackingCalled = false;
            var dataImportQueuedUp = false;

            formFile.Setup(p => p.FileName).Returns($"{sourceTrackingId}.txt");
            dataImportTracking.Setup(m => m.InsertTrackingInfo(It.IsAny<DataImportTracking>())).Returns(true).Callback(() => dataImportTrackingCalled = true);
            fileSystem
                .Setup(m => m.GetFileNameWithoutExtension(It.IsAny<string>()))
                .Returns(Path.GetFileNameWithoutExtension(formFile.Object.FileName));
            fileSystem.Setup(f => f.CreateTempFile(It.IsAny<IFormFile>())).ReturnsAsync("achFile.txt");
            backgroundQueue.Setup(m => m.Enqueue(It.IsAny<Func<CancellationToken, Task>>())).Callback(() => dataImportQueuedUp = true);
            
            //act
            var result = controller.PostAsync(new DataImportFile { ClientProcessCode = "DITACHUnitTest", FormFile = formFile.Object });

            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(dataImportTrackingCalled);
            Assert.IsTrue(dataImportQueuedUp);
            Assert.IsNotNull(result.Result);
            var jsonRes = result.Result as JsonResult;
            Assert.IsNotNull(jsonRes);
            Assert.AreEqual(StatusCodes.Status201Created, jsonRes.StatusCode);
            Assert.IsInstanceOfType(jsonRes.Value, typeof(ServiceResponse<FileTrack>));
            var jsonBody = jsonRes.Value as ServiceResponse<FileTrack>;
            Assert.IsNotNull(jsonBody.Data);
            Assert.AreEqual(jsonBody.Data.StatusCode, DITProcessingStatus.Processing);
        }
    }
}
