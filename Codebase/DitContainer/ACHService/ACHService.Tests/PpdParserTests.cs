﻿
using ACHConverter.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ACHService.Tests
{
    [TestClass]
    public class PpdParserTests
    {
        [TestMethod]
        public void ValidPpdPaymentRecord()
        {
            var payment = PpdParser.Parse("632123456789987654321        0000042650000000004534445SAMWISE               XX1000000000000001", 1);

            Assert.AreEqual(0, payment.Errors.Count, "ErrorCount");
            Assert.AreEqual("6", payment.Result.RecordType, "RecordType");
            Assert.AreEqual("32", payment.Result.TransactionCode, "TransactionCode");
            Assert.AreEqual("987654321", payment.Result.DDA, "DDA");
            Assert.AreEqual("123456789", payment.Result.ABA, "ABA");
            Assert.AreEqual((decimal)426.50, payment.Result.Amount, "Amount");
            Assert.AreEqual("000000004534445", payment.Result.IndividualId, "IndividualId");
            Assert.AreEqual("987654321        ", payment.Result.AccountNumber, "AccountNumber");
            Assert.AreEqual("SAMWISE               ", payment.Result.IndividualName, "IndividualName");
            Assert.AreEqual("000000000000001", payment.Result.TraceNumber, "TraceNumber");
        }

	    [TestMethod]
	    public void InvalidPPDAmount()
	    {
		    var batch =
			    CTXParser.Parse("622072000096000000090123456711YYYY32646VISHAY AMERICAS00095954-2001931405     1091004447858009", 1);
		    Assert.AreEqual(1, batch.Errors.Count);
		    Assert.AreEqual("Error parsing Batch Detail value:\tLine number: 1\tField: Amount\tValue: 1YYYY32646", batch.Errors[0]);
	    }
    }
}
