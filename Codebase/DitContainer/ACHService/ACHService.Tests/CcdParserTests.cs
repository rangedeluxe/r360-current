﻿
using ACHConverter.Parsers;
using CommonObjects;
using Deluxe.Payments.Parsing.ACH.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parsing.ACH.Tests;
using System.Collections.Generic;
using System.Linq;

namespace ACHService.Tests
{
    [TestClass]
    public class CcdParserTests
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static MockRulesProvider _rulesProvider;

        private static readonly string _aba = "666666670";
        private static readonly string _dda = "666666671";

        private static List<string> _ccdFile = new List<string>
        {
            @"101 072000096 0720000961810041128A094101MI POST-C RED MICHIGAN Deluxe Corp            00001473",
            @"5220DELUXE CORP     DISCRESIONARYDATA   0720000961CCDPAYMENT   1810041810040001 07200000000001",
            @"622666666670666666671        0000341085000000004534445APPLE                 XX1000000000000001",
            @"705ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001",
            @"7054*U*00400*076913354*0*P*>\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001",
            @"705010\ST*820*000000268\DTM*009*20021009*0\REF*2U*YH413*0\RMR*IV*TYZEU*C*J2VAG*462P00030000001",
            @"705G*2284Y*\REF*2U*YWNEO*0\SE*19*000000268\GE*1*76913354\IEA*1*076913354\          00040000001",
            @"8220000005554974640 0000000000000000003410850720000961                          07200000000001",
            @"9000001000009000000050554974640000000000000000000341085                                       "
        };

        private void ValidateDocumentAddenda(List<Field> expectedResults, IList<Field> actualResults, string testName)
        {
            foreach (var expectedResult in expectedResults)
            {
                Assert.AreEqual(expectedResult.IsCheck, actualResults.FirstOrDefault(a => a.FieldName == expectedResult.FieldName)?.IsCheck, $"{testName} - {expectedResult.FieldName}:IsCheck");
                Assert.AreEqual(expectedResult.FieldValue, actualResults.FirstOrDefault(a => a.FieldName == expectedResult.FieldName)?.FieldValue, $"{testName} - {expectedResult.FieldName}:FieldValue");
                Assert.AreEqual(expectedResult.SequenceNumber, actualResults.FirstOrDefault(a => a.FieldName == expectedResult.FieldName)?.SequenceNumber, $"{testName} - {expectedResult.FieldName}:SequenceNumber");
            }
        }

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _testContext = testContext;
            _rulesProvider = new MockRulesProvider();
            _rulesProvider.CreateExpandedTemplate("666666670", "666666671", "\\~", "*");
        }

        [TestMethod]
        public void ValidCcdPaymentRecord()
        {
            var payment = CcdParser.Parse("622072000096000000090123456711000632646VISHAY AMERICAS00095954-2001931405     1091004447858009", 1);

            Assert.AreEqual(0, payment.Errors.Count, "ErrorCount");
            Assert.AreEqual("6", payment.Result.RecordType, "RecordType");
            Assert.AreEqual("22", payment.Result.TransactionCode, "TransactionCode");
            Assert.AreEqual("00000009012345671", payment.Result.DDA, "DDA");
            Assert.AreEqual("072000096", payment.Result.ABA, "ABA");
            Assert.AreEqual((decimal)10006326.46, payment.Result.Amount, "Amount");
            Assert.AreEqual("VISHAY AMERICAS", payment.Result.IndividualId, "IndividualId");
            Assert.AreEqual("00000009012345671", payment.Result.AccountNumber, "AccountNumber");
            Assert.AreEqual("00095954-2001931405   ", payment.Result.ReceivingCompany, "ReceivingCompany");
            Assert.AreEqual("  ", payment.Result.DiscretionaryData, "DiscretionaryData");
            Assert.AreEqual("091004447858009", payment.Result.TraceNumber, "TraceNumber");
        }

	    [TestMethod]
	    public void InvalidCddAmount()
	    {
		    var batch =
			    CTXParser.Parse("622072000096000000090123456711YYYY32646VISHAY AMERICAS00095954-2001931405     1091004447858009", 1);
		    Assert.AreEqual(1, batch.Errors.Count);
		    Assert.AreEqual("Error parsing Batch Detail value:\tLine number: 1\tField: Amount\tValue: 1YYYY32646", batch.Errors[0]);
	    }

        [TestMethod]
        public void ParseCcdAddendaWithRmrDtmAndRef()
        {
            var achFile = new ACHFile(_ccdFile, _rulesProvider).Parse();

            var achBatch = achFile.Result.Batches[0];
            var addendaRecord = new CCDAddenda(achBatch.Header, achBatch.BatchDetail[0], achBatch.Footer,
                _rulesProvider.GetRules(new List<BatchDetailIdentifier>
                    {new BatchDetailIdentifier() {ABA = _aba, DDA = _dda}}));
            var addendaRecordResults = addendaRecord.Parse();
            var paymentAddendaFields = addendaRecordResults.Result.ParsedAddendaSegments.Result.PaymentAddendaFields;
            var documentAddendaFields = addendaRecordResults.Result.DocumentAddendaFields;

            Assert.AreEqual(0, addendaRecordResults.Errors.Count, $"{TestContext.TestName} - Addenda Error Count");

            Assert.AreEqual(0, paymentAddendaFields.Count, $"{TestContext.TestName} - Addenda Payment Segments Count");
            Assert.AreEqual(1, documentAddendaFields.Count, $"{TestContext.TestName} - Addenda Document Segments Count");
            //Setup expected results
            var expectedResults = new List<List<Field>>
            {
                new List<Field>
                {
                    new Field { FieldName = "RMRReferenceNumber", FieldValue = "TYZEU", IsCheck = false, SequenceNumber = 1 },
                    new Field { FieldName = "RMRMonetaryAmount", FieldValue = "J2VAG", IsCheck = false, SequenceNumber = 1 },
                    new Field { FieldName = "RMRTotalInvoiceAmount", FieldValue = "462PG", IsCheck = false, SequenceNumber = 1 },
                    new Field { FieldName = "RMRDiscountAmount", FieldValue = "2284Y", IsCheck = false, SequenceNumber = 1 },
                    new Field { FieldName = "REFDetailQualifier", FieldValue = "2U", IsCheck = false, SequenceNumber = 1 },
                    new Field { FieldName = "REFID", FieldValue = "YWNEO", IsCheck = false, SequenceNumber = 1 }
                }
            };

            //Loop through and validate results
            for (var i = 0; i < expectedResults.Count; i++)
            {
                ValidateDocumentAddenda(expectedResults[i], documentAddendaFields[i], _testContext.TestName);
            }
        }
    }
}
