using System;
using ACHConverter;
using ACHConverter.Models;
using ACHConverter.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACHConverter.Utilities;
using CommonObjects;
using Microsoft.Azure.Services.AppAuthentication;
using File = ACHConverter.Models.File;

namespace ACHService.Tests
{
    [TestClass]
    public class FileParserTests
    {
        [TestMethod]
        public void ValidFileTrailerRecord()
        {
            var footer = Footer.Parse("9000009002356000002450079200099000000000000000012268064                                       ", 99);

            Assert.AreEqual(0, footer.Errors.Count, "ErrorCount");
            Assert.AreEqual(9, footer.Result.RecordTypeCode, "RecordTypeCode");
            Assert.AreEqual(9, footer.Result.BatchCount, "BatchCount");
            Assert.AreEqual(2356, footer.Result.BlockCount, "BlockCount");
            Assert.AreEqual(245, footer.Result.TotalCount, "TotalCount");
            Assert.AreEqual(79200099, footer.Result.TotalHash, "TotalHash");
            Assert.AreEqual("0000000000.00", footer.Result.FileDebitAmount, "FileDebitAmount");
            Assert.AreEqual("0000122680.64", footer.Result.FileCreditAmount, "FileCreditAmount");
        }

        [TestMethod]
        public void InvalidFileHeaderLength()
        {
            var header = Header.Parse("101 072000096 0720000961305140505G094101MI POST-C RED MICHIGAN COMERICA BANK", 1);

            Assert.AreEqual(1, header.Errors.Count, "ErrorCount");
            Assert.AreEqual("Error parsing header Line length 76 is invalid.\tLine Number: 1.\tDITACHIMPORT_INVALID_FILE_HEADER_ERROR", header.Errors[0],
                "ErrorMessage");
        }

        [TestMethod]
        public void InvalidFileTrailerLength()
        {
            var footer = Footer.Parse("9000009002356000002450079200099000000000000000012268064", 99);

            Assert.AreEqual(1, footer.Errors.Count, "ErrorCount");
            Assert.AreEqual("Error parsing footer value:\tLine Number: 99 DITACHIMPORT_INVALID_FILE_FOOTER_ERROR", footer.Errors[0],
                "ErrorMessage");
        }
	    [TestMethod]
	    public void InvalidFileTrailerRecord()
	    {
		    var footer = Footer.Parse("9Z00002X00018XXXX0012GGGG178178XX0000940049CCC000971181                                       ", 99);

		    Assert.AreEqual(4, footer.Errors.Count, "ErrorCount");
		    Assert.AreEqual("Error parsing footer value:\tLine number: 99\tField: BatchCount\tValue: Z00002", footer.Errors[0],
			    "ErrorMessage");
		    Assert.AreEqual("Error parsing footer value:\tLine number: 99\tField: TotalCount\tValue: X00018", footer.Errors[1],
			    "ErrorMessage");
		    Assert.AreEqual("Error parsing footer value:\tLine number: 99\tField: EntryCount\tValue: XXXX0012", footer.Errors[2],
			    "ErrorMessage");
		    Assert.AreEqual("Error parsing footer value:\tLine number: 99\tField: TotalHash\tValue: GGGG178178", footer.Errors[3],
			    "ErrorMessage");
		}
	}
}
