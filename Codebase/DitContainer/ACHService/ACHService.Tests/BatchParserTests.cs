using System;
using ACHConverter.Models;
using ACHConverter.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACHConverter.Utilities;
using CommonObjects;
using Microsoft.Azure.Services.AppAuthentication;
using File = ACHConverter.Models.File;

namespace ACHService.Tests
{
	[TestClass]
	public class BatchParserTests
	{
		[TestMethod]
		public void ValidCTXBatchRecord_WillUseSettlementDate()
		{
		    //                                       111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999
		    //                              123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
            var batch = BatchHeader.Parse("5220ContinentalUSA                      4133784645CTXVENDOR PAY      1501210181091000010003351", 2);

			Assert.AreEqual(0, batch.Errors.Count, "ErrorCount");
			Assert.AreEqual(220, batch.Result.ServiceClass, "ServiceClass");
			Assert.AreEqual(3351, batch.Result.ACHBatchNumber, "ACHBatchNumber");
			Assert.AreEqual("      ", batch.Result.DescriptiveDate, "DescriptiveDate");
			Assert.AreEqual(new DateTime(2015, 1, 21), batch.Result.EffectiveDate, "EffectiveDate");
			Assert.AreEqual(new DateTime(2015, 1, 18), batch.Result.SettlementDate, "SettlementDate");
			Assert.AreEqual("                    ", batch.Result.CompanyData, "CompanyData");
			Assert.AreEqual("4133784645", batch.Result.CompanyId, "CompanyId");
			Assert.AreEqual("ContinentalUSA  ", batch.Result.CompanyName, "CompanyName");
			Assert.AreEqual("CTX", batch.Result.EntryClassCode, "EntryClassCode");
			Assert.AreEqual("VENDOR PAY", batch.Result.EntryDescription, "");
			Assert.AreEqual("1", batch.Result.OriginatorStatus, "OriginatorStatus");
		}

	    [TestMethod]
	    public void ValidIatBatchRecord()
	    {
            //                                      111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999
            //                             123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
	      //var batch = BatchHeader.Parse("5220ContinentalUSA                      4133784645CTXVENDOR PAY      1501210181091000010003351", 2);
            var batch = BatchHeader.Parse("5220ContinentalUSA  aabcccccccccccccccdd4133784645CTXVENDOR PAYggghhh1501210181091000010003351", 2);

	        Assert.AreEqual(0, batch.Errors.Count, "ErrorCount");
            Assert.AreEqual("aa",batch.Result.ForeignExchangeIndicator, "ForeignExchangeIndicator");
	        Assert.AreEqual("b", batch.Result.ForeignExchangeRefIndicator, "ForeignExchangeRefIndicator");
	        Assert.AreEqual("ccccccccccccccc", batch.Result.ForeignExchangeReference, "ForeignExchangeReference");
	        Assert.AreEqual("dd", batch.Result.IsoDestinationCountryCode, "IsoDestinationCountryCode");
	        Assert.AreEqual("ggg", batch.Result.OriginatingCurrencyCode, "OriginatingCurrencyCode");
	        Assert.AreEqual("hhh", batch.Result.DestinationCurrencyCode, "DestinationCurrencyCode");
        }

        [TestMethod]
		public void ValidCTXBatchRecord_WillUseEffectiveDateForSettlementDate()
		{
		    //                                      111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999
		    //                             123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
            var batch = BatchHeader.Parse("5220ContinentalUSA                      4133784645CTXVENDOR PAY      1501210001091000010003351", 2);

			Assert.AreEqual(0, batch.Errors.Count, "ErrorCount");
			Assert.AreEqual(220, batch.Result.ServiceClass, "ServiceClass");
			Assert.AreEqual(3351, batch.Result.ACHBatchNumber, "ACHBatchNumber");
			Assert.AreEqual("      ", batch.Result.DescriptiveDate, "DescriptiveDate");
			Assert.AreEqual(new DateTime(2015, 1, 21), batch.Result.EffectiveDate, "EffectiveDate");
			Assert.AreEqual(new DateTime(2015, 1, 21), batch.Result.SettlementDate, "SettlementDate");
			Assert.AreEqual("                    ", batch.Result.CompanyData, "CompanyData");
			Assert.AreEqual("4133784645", batch.Result.CompanyId, "CompanyId");
			Assert.AreEqual("ContinentalUSA  ", batch.Result.CompanyName, "CompanyName");
			Assert.AreEqual("CTX", batch.Result.EntryClassCode, "EntryClassCode");
			Assert.AreEqual("VENDOR PAY", batch.Result.EntryDescription, "");
			Assert.AreEqual("1", batch.Result.OriginatorStatus, "OriginatorStatus");
		}

		[TestMethod]
		public void InvalidCTXBatchRecordLength()
		{
			var batch = BatchHeader.Parse("5220ContinentalUSA                      4133784645CTXVENDOR PAY    1501210211091000010003351", 2);

			Assert.AreEqual(1, batch.Errors.Count, "ErrorCount");
			Assert.AreEqual("Error Parsing Batch Header\tLineNumber: 2 DITACHIMPORT_INVALID_BATCH_DETAIL_ERROR : Invalid length 92", batch.Errors[0],
				"ErrorMessage");
		}

		[TestMethod]
		public void InValidCTXBatchRecord_InvalidServiceClass()
		{
			var batch = BatchHeader.Parse("5xx0ContinentalUSA                      4133784645CTXVENDOR PAY      1501210001091000010003351", 2);
			Assert.AreEqual(1, batch.Errors.Count);
			Assert.AreEqual("Error Parsing Batch Header\tLine number: 2\tField: ServiceClass\tValue: xx0", batch.Errors[0]);
		}

		[TestMethod]
		public void InValidCTXBatchRecord_InvalidEffectiveDate()
		{
			var batch = BatchHeader.Parse("5220ContinentalUSA                      4133784645CTXVENDOR PAY      xxx1210001091000010003351", 2);
			Assert.AreEqual(1, batch.Errors.Count);
			Assert.AreEqual("Error Parsing Batch Header\tLine number: 2\tField: EffectiveDate\tValue: xxx121", batch.Errors[0]);
		}

		[TestMethod]
		public void InValidCTXBatchRecord_InvalidSettlementDate()
		{
			var batch = BatchHeader.Parse("5220ContinentalUSA                      4133784645CTXVENDOR PAY      150121xzy1091000010003351", 2);
			Assert.AreEqual(1, batch.Errors.Count);
			Assert.AreEqual("Error Parsing Batch Header\tLine number: 2\tField: SettlementDate\tValue: xzy", batch.Errors[0]);
		}

		[TestMethod]
		public void InValidCTXBatchRecord_InvalidACHBatchNumber()
		{
			var batch = BatchHeader.Parse("5220ContinentalUSA                      4133784645CTXVENDOR PAY      150121000109100001000xxx1", 2);
			Assert.AreEqual(1, batch.Errors.Count);
			Assert.AreEqual("Error Parsing Batch Header\tLine number: 2\tField: ACHBatchNumber\tValue: 000xxx1", batch.Errors[0]);
		}

		[TestMethod]
		public void InvalidBatchFooterTest()
		{
			var batch = BatchFooter.Parse("82200000068VVV9089  kkkkkkk40049XYZZY00000000720000961                          07200000000001", 2);
			Assert.AreEqual(3, batch.Errors.Count);
			Assert.AreEqual("Error parsing Batch footer value:\tLine number: 2\tField: TotalHash\tValue: 8VVV9089  ", batch.Errors[0]);
			Assert.AreEqual("Error parsing Batch footer value:\tLine number: 2\tField: TotalDebitAmount\tValue: kkkkkkk400.49", batch.Errors[1]);
			Assert.AreEqual("Error parsing Batch footer value:\tLine number: 2\tField: TotalCreditAmount\tValue: XYZZY00000.00", batch.Errors[2]);
		}

		[TestMethod]
		public void BatchFooterInvalidLengthTest()
		{
			var batch = BatchFooter.Parse("9000002000018000000120178178178000000940049000000971181                                   ", 101);
			Assert.AreEqual(1, batch.Errors.Count);
			Assert.AreEqual("Error parsing Batch footer value:\tLine number: 101 DITACHIMPORT_INVALID_BATCH_FOOTER_ERROR", batch.Errors[0]);
		}
	}
}
