﻿using System.Collections.Generic;

namespace CommonObjects
{
    public class Document
    {
        
        public int DocumentSequence { get; set; }
        public int SequenceWithinTransaction { get; set; }
        public string DocumentDescriptor { get; set; }
        public bool IsCorrespondence { get; set; }
        public IList<IList<Field>> DocumentRemittanceData { get; set; }
    }
}