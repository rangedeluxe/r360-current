﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonObjects
{
    public class BatchDetailIdentifier
    {
        public string ABA { get; set; }
        public string DDA { get; set; }
    }
}
