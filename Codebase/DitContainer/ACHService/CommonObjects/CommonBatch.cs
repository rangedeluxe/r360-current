﻿using System;
using System.Collections.Generic;

namespace CommonObjects
{
    public class CommonBatch
    {
        public DateTime DepositDate { get; set; }
        public DateTime BatchDate { get; set; }
        public DateTime ProcessingDate { get; set; }
        public int BankId { get; set; }
        public int ClientId { get; set; }
        public int BatchId { get; set; }
        public int BatchNumber { get; set; }
        public int BatchSiteCode { get; set; }
        public string BatchSource { get; set; }
        public string PaymentType { get; set; }
        public int BatchCueID { get; set; }
        public Guid BatchTrackingID { get; set; }
        public string ABA { get; set; }
        public string DDA { get; set; }
        public string FileHash { get; set; }
        public string FileSignature { get; set; }
        public IList<BatchDataRecord> BatchRecords { get; set; }
        public IList<Transaction> Transactions { get; set; } = new List<Transaction>();

    }
}
