﻿using System.Collections.Generic;

namespace CommonObjects
{
    public class BatchDataRecord
    {
        public IList<Field> fields { get; set; }
    }
}