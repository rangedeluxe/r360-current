﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace CommonObjects
{
    /// <summary>
	/// Defines a rule for ACH addenda parsing
	/// </summary>
    [ExcludeFromCodeCoverage]
	public class AddendaRule
    {
        public string AddendaSegmentName { get; set; }
        public string SegmentIdentifier { get; set; }
        public string FieldName { get; set; }
        public int FieldNumber { get; set; }
        public string SegmentQualifier { get; set; }
        public bool IsCheck { get; set; }
        public string SegmentDelimiter { get; set; }
        public string FieldDelimiter { get; set; }
        public string AddendaName { get; set; }
        public bool CreateNewRecord { get; set; }
        public string ABA { get; set; }
        public string DDA { get; set; }

        public override string ToString()
        {
            return
                $"Addenda Rule:" +
                $"AddendaSegmentName: {AddendaSegmentName}, " +
                $"FieldName: {FieldName}," +
                $"SegmentDelimiter:{SegmentDelimiter} " +
                $"FieldDelimiter:{FieldDelimiter}, " +
                $"AddendaName:{AddendaName}, " +
                $"ABA:{ABA}, " +
                $"DDA:{DDA}\n";

        }
    }
}
