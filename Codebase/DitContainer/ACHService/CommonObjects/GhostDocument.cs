﻿using System.Collections.Generic;

namespace CommonObjects
{
    public class GhostDocument
    {
        public bool IsCorrespondance { get; set; }
        public IList<GhostField> GhostFields { get; set; }
    }
}