﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text;

namespace CommonObjects.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class BadDataException : Exception
    {
        public BadDataException()
        {
        }

        public BadDataException(string message) : base(message)
        {
        }

        public BadDataException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BadDataException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
