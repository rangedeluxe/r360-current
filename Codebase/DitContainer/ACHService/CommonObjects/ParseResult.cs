﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonObjects
{
    public class ParseResult<T>
    {
        public IList<string> Errors { get; set; }
        public T Result { get; set; }
    }
}
