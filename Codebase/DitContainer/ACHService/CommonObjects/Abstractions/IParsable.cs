﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonObjects.Abstractions
{
    public interface IParsable<TParseTo>
    {
        TParseTo Parse();
    }
}
