﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonObjects.Abstractions
{
    public interface IRulesProvider
    {
        IList<AddendaRule> GetRules(IList<BatchDetailIdentifier> batchDetailsDetailIdentifiers);
    }
}
