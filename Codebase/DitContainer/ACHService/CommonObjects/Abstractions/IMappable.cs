﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonObjects.Abstractions
{
    public interface IMappable<TResult>
    {
        TResult MapTo();
    }
}
