﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonObjects.Abstractions
{
    public abstract class PossibleResult<T>
    {
        public abstract T GetResult(T whenNoResult);
        public abstract T GetResult(Func<T> whenNoResult);
    }
}
