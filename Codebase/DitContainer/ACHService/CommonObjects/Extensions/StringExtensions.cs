﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CommonObjects.Extensions
{
    public static class StringExtensions
    {
        public static int TryParseInt(this string value, string message, IList<string> errors)
        {
            if (!int.TryParse(value, out var val)) errors.Add(message + $"\tValue: {value}");
            return val;
        }

        public static long TryParseLong(this string value, string message, IList<string> errors)
        {
            if (!long.TryParse(value, out var val)) errors.Add(message + $"\tValue: {value}");
            return val;
        }

        public static decimal TryParseDecimal(this string value, string message, IList<string> errors)
        {
            if (!decimal.TryParse(value, out var val)) errors.Add(message + $"\tValue: {value}");
            return val / 100;
        }

        public static DateTime TryParseDate(this string value, string message, IList<string> errors)
        {
            if (!DateTime.TryParse(value, out var val)) errors.Add(message + $"\tValue: {value}");
            return val;
        }

        public static DateTime TryParseFormattedDate(this string value, string message, IList<string> errors)
        {
            try
            {
                return DateTime.ParseExact(value, "yyMMdd", null);
            }
            catch
            {
                errors.Add(message + $"\tValue: {value}");
            }

            return DateTime.MinValue;
        }

        public static string GetHash(this string value)
        {
            var result = string.Empty;
            var data = Encoding.ASCII.GetBytes(value);
            var hashData = new SHA1Managed().ComputeHash(data);
            return hashData.Aggregate(result, (current, b) => current + b.ToString("X2"));
        }
    }
}
