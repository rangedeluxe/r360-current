﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonObjects.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime TryParseJulianDate(this DateTime startDt, string value, string message, IList<string> errors)
        {
            try
            {
                if (value.Equals(null) || (value.Length == 0) || (Convert.ToInt16(value) > 366))
                {
                    errors.Add($"{ message} Value: {value} ");
                    return DateTime.MinValue;
                }
            
                DateTime januaryOne = DateTime.Parse($"01/01/{startDt.Year}");
                return januaryOne.AddDays(Convert.ToInt32(value) - 1);
            }
            catch (Exception)
            {
                //errors.Add(message + $"\\tValue: {value}");
                errors.Add($"{ message} Value: {value} ");
                return DateTime.MinValue;
            }
        }
    }
}
