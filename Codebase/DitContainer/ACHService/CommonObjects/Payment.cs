﻿using System;
using System.Collections.Generic;

namespace CommonObjects
{
    public class Payment
    {
        public decimal Amount { get; set; }
        public string RT { get; set; }
        public string Account { get; set; }
        public string Serial { get; set; }
        public string TransactionCode { get;set; }
        public string RemitterName { get; set; }
        public string ABA { get; set; }
        public string DDA { get; set; }
        public IList<IList<Field>> RemittanceData { get; set; }
        public IList<RawData> RawData { get; set; }
    }
}