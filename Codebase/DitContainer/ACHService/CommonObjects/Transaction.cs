﻿using System.Collections.Generic;

namespace CommonObjects
{
    public class Transaction
    {
        public string TransactionHash { get; set; }
        public string TransactionSignature{get; set; }
        public IList<Payment> Payments { get; set; } = new List<Payment>();
        public IList<GhostDocument> GhostDocuments { get; set; }        
        public IList<Document> Documents { get; set; }

    }
}