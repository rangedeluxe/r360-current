﻿namespace CommonObjects
{
	public class Field
	{
		public string FieldName { get; set; }
		public string FieldValue { get; set; }
		public int SequenceNumber { get; set; }
		public bool IsCheck { get; set; }
	}
}