﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace HostingBackgroundQueue
{
    public class BackgroundQueue : IBackgroundQueue
    {
        private readonly Action<Exception> _onException;

        public ConcurrentQueue<Func<CancellationToken, Task>> TaskQueue { get; } = new ConcurrentQueue<Func<CancellationToken, Task>>();
        public int MaxConcurrentCount { get; }
        public int MillisecondsToWaitBeforePickingUpTask { get; }
        public int ConcurrentCount => _concurrentCount;
        private int _concurrentCount;

        public BackgroundQueue(Action<Exception> onException, int maxConcurrentCount, int millisecondsToWaitBeforePickingUpTask)
        {
            if (maxConcurrentCount < 1) throw new ArgumentException("maxConcurrentCount must be at least 1", nameof(maxConcurrentCount));

            _onException = onException ?? (exception => { });
            MaxConcurrentCount = maxConcurrentCount;
            MillisecondsToWaitBeforePickingUpTask = millisecondsToWaitBeforePickingUpTask;
        }

        public void Enqueue(Func<CancellationToken, Task> task)
        {
            TaskQueue.Enqueue(task);
        }

        public async Task Dequeue(CancellationToken cancellationToken)
        {
            if (TaskQueue.TryDequeue(out var nextTaskAction))
            {
                Interlocked.Increment(ref _concurrentCount);
                try
                {
                    await nextTaskAction(cancellationToken);
                }
                catch (Exception e)
                {
                    _onException(e);
                }
                finally
                {
                    Interlocked.Decrement(ref _concurrentCount);
                }
            }

            await Task.CompletedTask;
        }
    }
}
