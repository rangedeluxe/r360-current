﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HostingBackgroundQueue
{
    public interface IBackgroundQueue
    {
        ConcurrentQueue<Func<CancellationToken, Task>> TaskQueue { get; }
        int MaxConcurrentCount { get;}
        int MillisecondsToWaitBeforePickingUpTask { get; }
        int ConcurrentCount { get; }
        void Enqueue(Func<CancellationToken, Task> task);
        Task Dequeue(CancellationToken cancellationToken);
    }
}
