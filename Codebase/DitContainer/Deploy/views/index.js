const installer = require('../installer/installer');
const exec = require('child_process').exec;
var remote = require('electron').remote;
console.log(`running with params: ${remote.process.argv}`);
var params = require('minimist')(remote.process.argv);
var silentMode = params.silentMode;
const timeout = silentMode ? 0 : 5000;

// create our installer and subscribe the the logging event.
const inst = new installer();
inst.onLog((log) => logs.unshift(log));
if(silentMode){console.log('running in silent mode');}
// Logic for the HTML.
let logs = [];
let app = new Vue({
    el: '#app',
    data: {
        name: 'R360 DIT Container',
        logs: logs,
        activity: '',
        state: 0,
        configuration: {
            port: "443",
            cert: "",
            dbserver: "",
            dbname: "",
            dbid: "",
            dbpwd: ""
        },
        certs: [{
            Subject: 'Loading Certificates...'
        }]
    },
    updated: () => {
        $('select').material_select();
        $('select').unbind('change');
        $('select').on('change', function () {
            app.configuration.cert = this.value;
        });
        $('select').val(app.configuration.cert);
    },
    async beforeMount() {
        // load the config from file
        let config = await inst.loadConfig();
        this.configuration = config ? config : this.configuration;

        // load the certificates
        this.certs = await findCerts();
        if (!this.configuration.cert)
            this.configuration.cert = this.certs[0]['Cert Hash(sha1)'];
        if (silentMode) {             
            await install();               
        }
    },  
    methods: {
        async install() {
            await install();
        },
        async uninstall() {
            await uninstall();
        }
    },
});

let findCerts = async () => {
    return new Promise((res) => {
        exec('certutil -store My', (err, out, stderr) => {
            let lines = out.split('\n');
            let certs = [];
            let valuereg = /\s?([^:]*):\s(.*)/;
            for (let line of lines) {
                if (/===========/.test(line)) {
                    certs.push({});
                } else if (valuereg.test(line)) {
                    let m = valuereg.exec(line);
                    certs[certs.length - 1][m[1]] = (m[1] == 'Cert Hash(sha1)') ?
                        m[2].replace(/ /g, '') :
                        m[2];
                }
            }
            res(certs);
        });
    });
}

let install = async () => {
    app.activity = 'Installing';
    app.state = 2;

    await inst.saveConfig(app.configuration);

    await inst.install();
    app.state = 3;

    // quit when we're all done.
    setTimeout(() => {
        const remote = require('electron').remote;
        remote.getCurrentWindow().close();
    }, timeout);
};

let uninstall = async () => {
    app.activity = 'Uninstalling';
    app.state = 2;

    await inst.uninstall();
    app.state = 3;

    // quit when we're all done.
    setTimeout(() => {
        const remote = require('electron').remote;
        remote.getCurrentWindow().close();
    }, timeout);
};