const replace = require('replace-in-file');
const copy = require('nexe-deploy-utils').copy;
const unpack = require('nexe-deploy-utils').unpack;
const remote = require('electron').remote;
const exec = require('nexe-deploy-utils').exec;
const path = require('path');
const Engine = require('nexe-deploy-utils').Engine;
const fs = require('fs');
const eol = require('os').EOL;
const winston = require('winston');
const moment = require('moment');
const timeStamp = moment().format('YYYYDDMM_HHmmss');

// Electron-Builder sets the application path as an environment varable, and resets the defautl __dirname vars.
// So we can use the builder path if it exists, or we can use the __dirname path if we're running debug mode.
const resfrompath = process.env.PORTABLE_EXECUTABLE_DIR
    ? path.resolve('resources/res')
    : path.join(__dirname, '../res');
const stagingpath = process.env.PORTABLE_EXECUTABLE_DIR
    ? path.join(process.env.PORTABLE_EXECUTABLE_DIR, 'staging/res')
    : path.join(__dirname, '../dist/staging/res');
const deploypath = process.env.PORTABLE_EXECUTABLE_DIR
    ? path.join(process.env.PORTABLE_EXECUTABLE_DIR, 'deploy/res')
    : path.join(__dirname, '../dist/deploy/res');
const workingdir = process.env.PORTABLE_EXECUTABLE_DIR
    ? process.env.PORTABLE_EXECUTABLE_DIR
    : path.join(__dirname, '../');
const configlocation=path.join(workingdir, 'config.json');

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.simple(),
    transports: [
      new winston.transports.File({ filename: path.join(workingdir, `DetailInstallLog_${timeStamp}.txt`) })
    ]
  });

logger.info(`PATHS:`);
logger.info(`  resfrompath:${resfrompath}`);
logger.info(`  stagingpath:${stagingpath}`);
logger.info(`  deploypath:${deploypath}`);
logger.info(`  workingdir:${workingdir}`);
logger.info(`  configlocation:${configlocation}`);

module.exports = class {

    constructor() {
        this.logFunction = (log) => { console.log(log); }
    }

    async saveConfig(config) {
        return new Promise((res) => {
            fs.writeFile(configlocation, JSON.stringify(config), 
                (err) => {
                    res(err ? false : true);
                });
        });
    };
    
    async loadConfig() {
        return new Promise((res) => {
            fs.readFile(configlocation, 
                (err, data) => {
                    res(err ? null : JSON.parse(data.toString()));
                });
        });
    };

    onLog(callback) {
        this.logFunction = callback;
    }

    log(message) {
        return new Promise((res) => {
            this.logFunction(message);
            res(true);
        })
    }

    async replaceConfiguration() {
        return new Promise(async (res) => {
            let config = await this.loadConfig();
            let glob = `${stagingpath}\\**\\*.template`;
            let files = [];
            
            // Loop through and change all of template files.
            for (let c in config) {
                let reg = new RegExp('\\$\\{' + c + '\\}', 'g');
                let options = {
                    files: glob,
                    from: reg,
                    to: config[c]
                };

                let changes = await replace(options);
                files = files.concat(changes);
            }

            // Copy the files to basically remove their .template extension.
            files = files.filter((o, i) => files.indexOf(o) === i);
            for (let f of files) {
                let destfilename = path.parse(f).name;
                let destpath = path.join(path.dirname(f), destfilename);
                await copy(f, destpath);
            }

            res(true);
        });
    }

    async uninstall() {
        return new Promise(async (res) => {
            let result = await new Engine().describe('R360 Dit Container Uninstallation')
                .step(async() => await this.log('Removing Services...'))
                .step(async() => await logger.log('info','Removing Services.'))
                .step(async() => await exec(path.join(stagingpath, `Dependencies/nssm.exe stop achservice`), true, false))
                .step(async() => await exec(path.join(stagingpath, `Dependencies/nssm.exe remove achservice confirm`), true, false))
                .exec();
            await logger.info('Uninstall Complete.');
            res(result);
        });
    }

    async install() {
        return new Promise(async (res, rej) => {
            let result = await new Engine().describe('R360 Dit Container Installation')
                .step(async() => await this.log('Staging Resources...'))
                .step(async() => await logger.info('Staging Resources.'))
                .step(async() => await this.log(`Copying from ${resfrompath} to ${stagingpath}`))
                .step(async() => await logger.info(`Copying from ${resfrompath} to ${stagingpath}`))
                .step(async() => await copy(`${resfrompath}`, `${stagingpath}`))
                .step(async() => await this.log('Applying Configuration...'))
                .step(async() => await logger.info('Applying Configuration.'))
                .step(async() => await this.replaceConfiguration())
                .step(async() => await this.log('Stopping Services...'))
                .step(async() => await logger.info('Stopping Services.'))
                .step(async() => await exec(path.join(stagingpath, `Dependencies/nssm.exe stop achservice`), true, false))
                .step(async() => await exec(path.join(stagingpath, `Dependencies/nssm.exe remove achservice confirm`), true, false))
                .step(async() => await this.log('Deploying Resources...'))
                .step(async() => await logger.info('Deploying Resources.'))
                .step(async() => await copy(`${stagingpath}`, `${deploypath}`))
                .exec();
            await logger.info('Install Complete.');
            res(result);
        });
    }
}