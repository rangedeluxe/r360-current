using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;

using WFS.Rechub.FileCleanup.FileCleanupAPI;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     12/12/2003
*
* Purpose:  IntegraPAY Online Cleanup Service.
*
* Modification History
* 12/12/2003 JMC
*   -Created class
* WI 70275 JMC 12/05/2012
*   -Added ILMerge post-build event to the project so FileCleanupSvc can be
*    released as a single executable.
******************************************************************************/
namespace WFS.Rechub.FileCleanup.FileCleanupSvc {

    /// <author>Joel Caples</author>
    /// <summary>
    /// ipo Cleanup Service
    /// </summary>
	public class ipoCleanupService : System.ServiceProcess.ServiceBase {

		/// <summary>Required designer variable.</summary>
		private System.ComponentModel.Container components = null;

        /// <summary>
        /// Private collection of cleanup objects.  One object exists in the 
        /// collection for every service section in the .ini file.
        /// </summary>
        private System.Collections.SortedList _Cleanups;

        /// <summary>
        /// Public constructor
        /// </summary>
		public ipoCleanupService() {
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();
		}

        /// <summary>
        /// Starts service, and creates a cleanup process for all cleanup sections 
        /// in the local config file.
        /// </summary>
        /// <param name="args"></param>
		protected override void OnStart(string[] args) {

            string strServiceOptionsKey;
            string strServiceName;

            const string INISECTION_IPO_CLEANUP_SECTIONS = "ipoCleanupSections";

            if(_Cleanups==null) {

                _Cleanups = new SortedList();

                StringCollection colSiteOptions = ipoINILib.GetINISection(INISECTION_IPO_CLEANUP_SECTIONS);   
                System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();

                while ( myEnumerator.MoveNext() ) {
                    strServiceOptionsKey = myEnumerator.Current.ToString().Substring(0
                        , myEnumerator.Current.ToString().IndexOf('='));

                    strServiceName = myEnumerator.Current.ToString().Substring(strServiceOptionsKey.Length+1);

                    cCleanupProcess objCleanup = new cCleanupProcess(strServiceName
                                                                   , strServiceOptionsKey);

                    _Cleanups.Add(strServiceOptionsKey, objCleanup);
                }
            }

            if(_Cleanups.Count > 0) {
                foreach(cCleanupProcess objTemp in _Cleanups.Values) {
                    objTemp.start();
                    //Ideally, I would like to remove it from the collection if this 
                    //fails, but I don't have a good way of getting to it's index 
                    //without setting a bunch of variables.
                }
            }       
		}
 
        /// <summary>
        /// Stop service and all living cleanup processes.
        /// </summary>
		protected override void OnStop() {

            cCleanupProcess objCleanup;

            if(_Cleanups.Count > 0) {
                for(int i=_Cleanups.Count-1;i>=0;--i) {
                    objCleanup = (cCleanupProcess)_Cleanups.GetByIndex(i);
                    objCleanup.stop();
                    _Cleanups.RemoveAt(i);
                }
            }

            _Cleanups = null;
		}

        /// <summary>
        /// Pauses service and all living cleanup processes.
        /// </summary>
        protected override void OnPause()  {
            if(_Cleanups.Count > 0) {
                foreach(cCleanupProcess objCleanup in _Cleanups.Values) {
                    objCleanup.pause();
                }
            }
        }

        /// <summary>
        /// Resumes service and all living cleanup processes.
        /// </summary>
        protected override void OnContinue() {
            if(_Cleanups.Count > 0) {
                foreach(cCleanupProcess objCleanup in _Cleanups.Values) {
                    objCleanup.resume();
                }
            }
        }

        /// <summary>
        /// The main entry point for the process
        /// </summary>
		static void Main() {
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = New System.ServiceProcess.ServiceBase[] 
            //      {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] 
                { new ipoCleanupService() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            this.CanPauseAndContinue = true;
            this.ServiceName = "ipoCleanupService";
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing"></param>
		protected override void Dispose(bool disposing) {
			if(disposing) {
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
