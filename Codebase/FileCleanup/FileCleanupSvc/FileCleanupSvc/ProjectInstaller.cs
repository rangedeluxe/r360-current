using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

namespace WFS.Rechub.FileCleanup.FileCleanupSvc {

	/// <summary>
	/// Summary description for ProjectInstaller.
	/// </summary>
	[RunInstaller(true)]
	public class ProjectInstaller : System.Configuration.Install.Installer {

        private System.ServiceProcess.ServiceProcessInstaller ipoCleanupServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ipoCleanupServiceInstaller;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        /// <summary>
        /// 
        /// </summary>
		public ProjectInstaller() {
			// This call is required by the Designer.
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing) {
			if(disposing) {
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ipoCleanupServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ipoCleanupServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ipoCleanupServiceProcessInstaller
            // 
            this.ipoCleanupServiceProcessInstaller.Password = null;
            this.ipoCleanupServiceProcessInstaller.Username = null;
            // 
            // ipoCleanupServiceInstaller
            // 
            this.ipoCleanupServiceInstaller.DisplayName = "WFS IPOnline Cleanup Service";
            this.ipoCleanupServiceInstaller.ServiceName = "WFS IPOnline Cleanup Service";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ipoCleanupServiceProcessInstaller,
            this.ipoCleanupServiceInstaller});

        }
		#endregion
	}
}
