using System;
using System.Collections;
using System.Collections.Specialized;

using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     12/12/2003
*
* Purpose:  IntegraPAY Online Cleanup Service Options.
*
* Modification History
*   12/12/2003 JMC
*       -Created class
*   08/29/2005 CR 13687 JMC
*       -Added CleanupMode property and CleanupMode enumeration.
*   08/29/2005 CR 13763 JMC
*       -Added FileExpression property.
* CR 28522 JMC 01/06/2010
*    - Class now inherits from cSiteOptions
******************************************************************************/
namespace WFS.Rechub.FileCleanup.FileCleanupAPI {

    /// <summary>
    /// 
    /// </summary>
    public enum CleanupMode {
        /// <summary></summary>
        File = 0,
        /// <summary></summary>
        Folder = 1
    }

    /// <author>Joel Caples</author>
    /// <summary>
    /// ipo Cleanup service options
    /// </summary>
	public class cCleanupServiceOptions : cSiteOptions {

        private long _CleanupInterval=10;               // 10 Seconds
        private long _FileExpiration=3600;              // 1 Hour
        private string _FileExtensions="";
        private string _CleanupDirectory="";
        private bool _DescendSubdirectories=false;
        private CleanupMode _CleanupMode = CleanupMode.File;
        private string _FileExpression="";

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Public Constructor.
        /// </summary>
        /// <param name="cleanupKey">
        /// ini key that contains the current cleanup options.
        /// </param>
        //************************************************************************
		public cCleanupServiceOptions(string cleanupKey) : base(cleanupKey) {
            loadCleanupOptions(cleanupKey);
		}

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Reads the specified section from the local .ini file to retrieve 
        /// processing options.
        /// </summary>
        /// <param name="cleanupKey"></param>
        //************************************************************************
        private void loadCleanupOptions(string cleanupKey) {

            string strKey;
            string strValue;

            const string INIKEY_CLEANUP_INTERVAL = "CleanupInterval";
            const string INIKEY_FILE_EXPIRATION = "FileExpiration";
            const string INIKEY_FILE_EXTENSIONS = "FileExtensions";
            const string INIKEY_CLEANUP_DIRECTORY = "CleanupDirectory";
            const string INIKEY_DESCEND_SUBDIRECTORIES = "DescendSubdirectories";
            const string INIKEY_CLEANUP_MODE = "CleanupMode";
            const string INIKEY_FILE_EXPRESSION = "FileExpression";

            StringCollection colSiteOptions = ipoINILib.GetINISection(cleanupKey);

            System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {

                strKey = myEnumerator.Current.ToString().Substring(0
                            , myEnumerator.Current.ToString().IndexOf('='));

                strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                if(strKey.ToLower() == INIKEY_CLEANUP_INTERVAL.ToLower()) {
                    try { _CleanupInterval = System.Convert.ToInt64(strValue); }
                    catch {_CleanupInterval = 10; } // 10 minutes.
                }
                else if(strKey.ToLower() == INIKEY_FILE_EXPIRATION.ToLower()) {
                    try { _FileExpiration = System.Convert.ToInt64(strValue); }
                    catch {_FileExpiration = 3600; }    //1 hour.
                }
                else if(strKey.ToLower() == INIKEY_FILE_EXTENSIONS.ToLower()) {
                    _FileExtensions = strValue;
                }
                else if(strKey.ToLower() == INIKEY_CLEANUP_DIRECTORY.ToLower()) {
                    _CleanupDirectory = ipoLib.cleanPath(strValue);
                }
                else if(strKey.ToLower() == INIKEY_DESCEND_SUBDIRECTORIES.ToLower()) {
                    try { _DescendSubdirectories=System.Convert.ToBoolean(strValue); }
                    catch { _DescendSubdirectories = false; }
                }
                else if(strKey.ToLower() == INIKEY_CLEANUP_MODE.ToLower()) {
                    try { _CleanupMode=(CleanupMode)System.Convert.ToInt32(strValue); }
                    catch { _CleanupMode = CleanupMode.File; }
                }
                else if(strKey.ToLower() == INIKEY_FILE_EXPRESSION.ToLower()) {
                    _FileExpression=strValue;
                }
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Returns the number of seconds that should elapse for the Image service 
        /// should pause before scanning the directory specified by the 
        /// OnlineArchive setting for files that require deletion.
        /// </summary>
        //************************************************************************
        public long cleanupInterval {
            get {
                return(_CleanupInterval);
            }
        }


        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Specifies the length(in hours) that a file in the directory specified 
        /// by the OnlineArchive Path setting should be allowed to exist before 
        /// the image service should delete it.
        /// </summary>
        //************************************************************************
        public long fileExpiration {
            get {
                return(_FileExpiration);
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// List of file extensions that the service should delete.  If this 
        /// property is null, the service will delete all files from the specified
        /// directories.
        /// </summary>
        //************************************************************************
        public string fileExtensions {
            get { 
                return(_FileExtensions);
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Root directory to be cleaned.
        /// </summary>
        //************************************************************************
        public string cleanupDirectory {
            get {
                return(ipoLib.cleanPath(_CleanupDirectory));
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// If this property is set to True, the service will delete all specified
        /// files, and delete all subdirectories(if empty) under the given root
        /// path.  If the property is set to False, the service will clean only
        /// the root directory.
        /// </summary>
        //************************************************************************
        public bool descendSubdirectories {
            get {
                return(_DescendSubdirectories);
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public CleanupMode cleanupMode {
            get { 
                return(_CleanupMode);
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public string fileExpression {
            get { 
                return(_FileExpression);
            }
        }
	}
}
