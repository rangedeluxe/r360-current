using System;
using System.Timers;
using System.IO;
using System.Text.RegularExpressions;

using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     12/12/2003
*
* Purpose:  IntegraPAY Online Cleanup Service.
*
* Modification History
*   12/12/2003 JMC
*       - Initial release.
*   10/06/2004 CR 10127 JMC
*       - Fixed compiler warning.
*   08/29/2005 CR 13687 JMC
*       - Added CleanupFolder mode functionality.
*   08/29/2005 CR 13763 JMC
*       - Added FileExpression setting to use regular expressions when evaluating
*         file names.
******************************************************************************/
namespace WFS.Rechub.FileCleanup.FileCleanupAPI {

    /// <author>Joel Caples</author>
    /// <summary>
    /// ipo Cleanup process object.
    /// </summary>
	public class cCleanupProcess : IDisposable
    {
        // Track whether Dispose has been called.
        private bool disposed = false;

        private Timer _ProcessTimer;
        private cEventLog _EventLog =null;
        private cCleanupServiceOptions _ServiceOptions = null;
        private string _ServiceName;
        private string _ServiceOptionsKey;

        private ServiceState _ServiceState = ServiceState.Stopped;

        private enum ServiceState {
             Started
            ,Paused
            ,Stopped
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Public constructor, tells the application which ini section to use
        /// for it's processing instructions.
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="serviceOptionsKey"></param>
        //************************************************************************
        public cCleanupProcess(string serviceName, 
                               string serviceOptionsKey) {
            _ServiceName = serviceName;
            _ServiceOptionsKey = serviceOptionsKey;
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Application options for the current instance.
        /// </summary>
        //************************************************************************
        private cCleanupServiceOptions serviceOptions
        {
            get
            {
                try
                {
                    if (_ServiceOptions == null) {
                        _ServiceOptions = new cCleanupServiceOptions(_ServiceOptionsKey);
                    }
                }
                catch(Exception e)
                {
                    throw;
                }

                return _ServiceOptions;
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Logging object for the current instance.
        /// </summary>
        //************************************************************************
        private cEventLog eventLog
        {
            get
            {
                if (_EventLog == null) { 
                    _EventLog = new cEventLog(serviceOptions.logFilePath
                                                , serviceOptions.logFileMaxSize
                                                , (MessageImportance) serviceOptions.loggingDepth); 
                }

                return _EventLog;
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Starts the internal timer, and outputs processing options to the log.
        /// </summary>
        //************************************************************************
        public bool start()
        {
            bool bolRetVal=true;

            try {
                if(ipoLib.cleanPath(serviceOptions.cleanupDirectory) == ipoLib.cleanPath(""))
                    {throw(new Exception("Cleanup folder is a required value.")); }

                if(!(Directory.Exists(serviceOptions.cleanupDirectory)))
                    {throw(new Exception("Specified Cleanup folder: "
                        + serviceOptions.cleanupDirectory
                        + " is not accessible.")); }

                _ProcessTimer = new System.Timers.Timer(serviceOptions.cleanupInterval*1000);
                _ProcessTimer.Elapsed += new ElapsedEventHandler( this.ProcessTimer_Tick );

                _ProcessTimer.AutoReset = true;
                _ProcessTimer.Enabled=true;
                _ProcessTimer.Start();
                _ServiceState = ServiceState.Started;

                try{
                    eventLog.logEvent (_ServiceName + " cleanup process started."
                                    , this.GetType().Name
                                    , MessageImportance.Essential);

                    eventLog.logEvent("    Cleanup Directory: "
                                    + serviceOptions.cleanupDirectory
                                    , this.GetType().Name
                                    , MessageImportance.Essential);

                    eventLog.logEvent("    Cleanup Interval: "
                                    + serviceOptions.cleanupInterval.ToString()
                                    + " seconds"
                                    , this.GetType().Name
                                    , MessageImportance.Essential);

                    eventLog.logEvent("    File Expiration: "
                                    + serviceOptions.fileExpiration.ToString()
                                    + " seconds"
                                    , this.GetType().Name
                                    , MessageImportance.Essential);

                    eventLog.logEvent("    Cleanup Mode: "
                                    + serviceOptions.cleanupMode.ToString()
                                    , this.GetType().Name
                                    , MessageImportance.Essential);

                    eventLog.logEvent("    File Expression: "
                                    + serviceOptions.fileExpression
                                    , this.GetType().Name
                                    , MessageImportance.Essential);

                    if(serviceOptions.cleanupMode == CleanupMode.File) {

                        eventLog.logEvent("    Descend Subdirectories?: "
                                        + serviceOptions.descendSubdirectories.ToString()
                                        , this.GetType().Name
                                        , MessageImportance.Essential);

                        eventLog.logEvent("    File Extensions: "
                                        + serviceOptions.fileExtensions
                                        , this.GetType().Name
                                        , MessageImportance.Essential);
                    }
                }
                catch(Exception ex) {
                    //This error should not halt processing.
                    eventLog.logError(ex);
                }
            }
            catch(Exception e) {
                eventLog.logEvent("An error occurred while starting " 
                                + _ServiceName + " cleanup process: " 
                                + e.Message
                                , e.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
                bolRetVal=false;
            }

            return bolRetVal;
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Stops the internal timer.
        /// </summary>
        //************************************************************************
        public void pause()
        {
            try {

                if(_ProcessTimer != null) {
                    _ProcessTimer.Stop();
                    _ServiceState = ServiceState.Paused;

                    eventLog.logEvent(_ServiceName + " cleanup process paused."
                                    , this.GetType().Name
                                    , MessageImportance.Essential);
                }
            }
            catch(Exception e) {
                eventLog.logEvent("An error occurred while pausing " + _ServiceName + " cleanup process: " 
                                        + e.Message
                                , e.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Re-starts the internal timer.
        /// </summary>
        //************************************************************************
        public void resume()
        {
            try {
                if(_ProcessTimer != null) {
                    _ProcessTimer.Start();
                    _ServiceState = ServiceState.Started;

                    eventLog.logEvent(_ServiceName + " cleanup process resumed."
                                    , this.GetType().Name
                                    , MessageImportance.Essential);
                }
            }
            catch(Exception e) {
                eventLog.logEvent("An error occurred while resuming " + _ServiceName + " cleanup process: " 
                                            + e.Message
                                , e.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Stops the internal timer.
        /// </summary>
        //************************************************************************
        public void stop()
        {
            try {
                if(_ProcessTimer != null) {
                    _ProcessTimer.Stop();
                    _ProcessTimer.AutoReset = false;
                    _ProcessTimer.Enabled = false;
                    _ProcessTimer=null;

                    _ServiceState = ServiceState.Stopped;

                    eventLog.logEvent(_ServiceName + " cleanup process stopped."
                                    , this.GetType().Name
                                    , MessageImportance.Essential);
                }
            }
            catch(Exception e) {
                eventLog.logEvent("An error occurred while stopping " + _ServiceName + " cleanup process: " 
                                        + e.Message
                                , e.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Runs the cleanup process when the timer ticks.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //************************************************************************
        private void ProcessTimer_Tick(object sender
                                , System.Timers.ElapsedEventArgs e) {
            try {
                _ProcessTimer.Stop();

                eventLog.logEvent(_ServiceName + " cleanup process starting."
                                , this.GetType().Name
                                , MessageImportance.Verbose);

                if(serviceOptions.cleanupMode == CleanupMode.File) {
                    cleanFiles(serviceOptions.cleanupDirectory);
                } else {
                    cleanFolder(serviceOptions.cleanupDirectory);
                }

                eventLog.logEvent(_ServiceName + " cleanup process completed."
                                , this.GetType().Name
                                , MessageImportance.Verbose);
            }
            catch(Exception ex)
            {
                eventLog.logEvent(_ServiceName + " cleanup process terminated abmormally : " + ex.Message
                                , ex.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
            }

            if(_ServiceState == ServiceState.Started) {
                _ProcessTimer.Start();
            }
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Runs the cleanup process a single time.  This method can be used to
        /// call the application from an external assembly without using the
        /// service features.
        /// </summary>
        //************************************************************************
        public void runOnce()
        {
            _ServiceState = ServiceState.Started;

            try {
                if(serviceOptions.cleanupMode == CleanupMode.File) {
                    cleanFiles(serviceOptions.cleanupDirectory);
                } else {
                    cleanFolder(serviceOptions.cleanupDirectory);
                }
            } catch {}

            _ServiceState = ServiceState.Stopped;
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Deletes expired files from the specified directory.
        /// </summary>
        /// <param name="directory">Path to the directory to be cleaned.</param>
        //************************************************************************
        private void cleanFiles(string directory)
        {
            int intDotIndex;
            bool bolDeleteFile;

            try
            {
                if(_ServiceState != ServiceState.Started) {
                    return;
                }

                eventLog.logEvent(_ServiceName + " cleanup process purging files from folder '" 
                                + directory + "'."
                                , this.GetType().Name
                                , MessageImportance.Verbose);

                string[] strFiles = System.IO.Directory.GetFiles(directory
                                                               , "*.*");

                if (strFiles.Length > 0) {

                    for(int i=0;i<strFiles.Length;++i) {

                        bolDeleteFile = true;

                        System.IO.FileInfo fi = new System.IO.FileInfo(strFiles[i]);

                        if (fi.LastWriteTime >= System.DateTime.Now.AddSeconds(-(serviceOptions.fileExpiration))) {
                            bolDeleteFile = false;
                        }

                        if(bolDeleteFile) {
                            if(serviceOptions.fileExtensions.Trim().Length > 0) {
                                intDotIndex=strFiles[i].LastIndexOf('.');
                                if(intDotIndex > 0) {
                                    if(serviceOptions.fileExtensions.ToLower().IndexOf(strFiles[i].ToLower().Substring(intDotIndex+1))<0) {
                                        bolDeleteFile = false;
                                    }
                                }
                            }
                        }

                        if(bolDeleteFile) {
                            if(serviceOptions.fileExpression.Length > 0) {
                                if(!Regex.IsMatch(strFiles[i], serviceOptions.fileExpression)) {
                                    bolDeleteFile = false;
                                }
                            }
                        }

                        if(bolDeleteFile) {
                            File.Delete(strFiles[i]);
                        }
                    }
                }        

                eventLog.logEvent(_ServiceName + " cleanup process done purging files from folder " 
                                + directory 
                                + "."
                                , this.GetType().Name
                                , MessageImportance.Verbose);

                if(serviceOptions.descendSubdirectories) {
                    string[] strSubDirectories = Directory.GetDirectories(directory);
                    if(strSubDirectories.Length > 0) {
                        for(int i=0;i<strSubDirectories.Length;++i) {
                            cleanFiles(strSubDirectories[i]);
                            try { 
                                Directory.Delete(strSubDirectories[i]); 
                            }
                            catch(System.IO.IOException) {
                                //Do Nothing, 'cause the directory is not empty.
                                eventLog.logEvent("Not removing folder containing files."
                                                , this.GetType().Name
                                                , MessageImportance.Debug);
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                eventLog.logEvent(_ServiceName + " cleanup process errored while purging files : " 
                                + e.Message
                                , e.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        private void cleanFolder(string directory)
        {

            bool bolDeleteFolder;

            try
            {
                if(_ServiceState != ServiceState.Started) {
                    return;
                }

                eventLog.logEvent(_ServiceName + " cleanup process purging subdirectories from folder '" 
                                + directory + "'."
                                , this.GetType().Name
                                , MessageImportance.Verbose);

                string[] strFolders = Directory.GetDirectories(directory);


                if (strFolders.Length > 0) {

                    for(int i=0;i<strFolders.Length;++i) {

                        bolDeleteFolder = true;

                        DirectoryInfo di = new DirectoryInfo(strFolders[i]);

                        if (di.LastWriteTime >= System.DateTime.Now.AddSeconds(-(serviceOptions.fileExpiration))) {
                            bolDeleteFolder = false;
                        }

                        if(bolDeleteFolder) {
                            if(serviceOptions.fileExpression.Length > 0) {
                                if(!Regex.IsMatch(strFolders[i], serviceOptions.fileExpression)) {
                                    bolDeleteFolder = false;
                                }
                            }
                        }

                        if(bolDeleteFolder) {
                            deleteFolder(strFolders[i]);
                        }
                    }
                }        

                eventLog.logEvent(_ServiceName + " cleanup process done purging subdirectories from folder " 
                                + directory 
                                + "."
                                , this.GetType().Name
                                , MessageImportance.Verbose);

                if(serviceOptions.descendSubdirectories) {
                    string[] strSubDirectories = Directory.GetDirectories(directory);
                    if(strSubDirectories.Length > 0) {
                        for(int i=0;i<strSubDirectories.Length;++i) {
                            cleanFolder(strSubDirectories[i]);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                eventLog.logEvent(_ServiceName + " cleanup process errored while purging subdirectories : " 
                                + e.Message
                                , e.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderName"></param>
        private void deleteFolder(string folderName) {
            try {
                Directory.Delete(folderName, true);
            } catch(Exception e) {
                eventLog.logEvent(_ServiceName + " cannot remove folder '" + folderName + "': " 
                                + e.Message
                                , e.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        #region Dispose
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.      
                    if (_ProcessTimer != null)
                    {
                        _ProcessTimer.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }
        #endregion
	}
}
