# This runs the tests. This powershell file is expected to exist right where it is, to make things simple.
# This can be executed on a windows task.
$WEBSERVER = 'https://rechubintweb01.qalabs.nwk/'
$APPSERVER = 'https://rechubintapp01.qalabs.nwk/'

write-host 'Deleting old test results...'
$currentdir = (get-item -path ".\" -verbose)
$rootpath = join-path $currentdir '..\'
$results = join-path $rootpath 'results'
remove-item $results -force -recurse

# First we're hitting some endpoints to make sure the server is awake.
write-host 'Hitting web services to wake up servers...'
invoke-webrequest ($WEBSERVER + 'RecHubViews')
invoke-webrequest ($APPSERVER + 'RecHubServices/R360Services.svc')
invoke-webrequest ($APPSERVER + 'RecHubExtractScheduleService/ExtractScheduleService.svc')
invoke-webrequest ($APPSERVER + 'RecHubConfigService/HubConfig.svc')

# Now we can run the tests.
write-host 'Executing tests...'
set-location $rootpath
invoke-expression 'npm run test'

set-location $currentdir