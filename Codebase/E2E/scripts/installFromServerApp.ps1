# This script expects to exist in the /WFSStaging folder.
# The purpose is to pull the latest code from another server (like QA) and run the 
# installation steps in silent mode.

$serverlocation = '\\rechubqaapp02.qalabs.nwk\D$\WFSStaging'
$includes = 
    'APP', 
    'App Deploy Framework.ps1',
    'App Deploy RAAM.ps1',
    'App Deploy Rechub.ps1',
    'Wfs.R360.DeploymentConfigurationUtilities.dll',
    'WFS.RaamFramework.StagingUtilities.dll',
    'Wfs.RaamFrameworkDeploymentConfigUtilities.dll'
$scripts = 
    'App Deploy Framework.ps1',
    'App Deploy RAAM.ps1',
    'App Deploy Rechub.ps1'

$currentdir = (get-item -path ".\" -verbose)

foreach ($include in $includes) {
    $copyfrom = join-path $serverlocation $include
    write-host "Copying '$copyfrom'..."
	copy-item $copyfrom $currentdir -force -recurse
}

foreach ($script in $scripts) {
	$command = '.\' + '"' + "$script" + '"' + ' -Silent'
	write-host "Running command '$command'..."
	invoke-expression "& $command"
}