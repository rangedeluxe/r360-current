# This script expects to exist in the /WFSStaging folder.
# The purpose is to pull the latest code from another server (like QA) and run the 
# installation steps in silent mode.

$serverlocation = '\\rechubqadb02.qalabs.nwk\D$\WFSStaging'
$includes = 'DB'
$replacements = 
    ('^\$SQLVersion.*', '$SQLVersion="2012"'),
    ('^\$DBSERVER.*', '$DBSERVER="rechubintdb01.qalabs.nwk"'),
    ('^\$R360DB.*', '$R360DB="CURRENT_R360_NP"'),
    ('^\$RAAMDB.*', '$RAAMDB="CURRENT_RAAM"'),
	('^\$FrameworkDB.*', '$FrameworkDB="CURRENT_FRAMEWORK"'),
	('^Read-Host.*any\skey.*', '')
$scripts = 
    'DB\RecHub2.03\2.03.00.00\SupportScripts\Install R3602.03.00.00Patch.ps1',
	'DB\Framework2.3\Framework2.03.00.00\SupportScripts\Framework2.03.00.00Patch.ps1',
	'DB\RAAM2.03\2.03.00.00\SupportScripts\RAAM2.03.00.00Patch.ps1'

$currentdir = (get-item -path ".\" -verbose)

foreach ($include in $includes) {
    $copyfrom = join-path $serverlocation $include
    write-host "Copying '$copyfrom'..."
	copy-item $copyfrom $currentdir -force -recurse
}

foreach ($script in $scripts) {
    $file = join-path $currentdir $script
	set-location (split-path $file -parent)
    foreach($replace in $replacements) {
        (get-content $file) -replace $replace[0], $replace[1] | out-file $file
    }
	$command = '"' + "$file" + '"' + ' -Silent'
	write-host "Running command '$command'..."
	invoke-expression "& $command"
}
set-location $currentdir