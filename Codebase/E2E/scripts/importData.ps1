# Designed to be ran on the client tier, where imports are performed.
# This script is responsible for populating data that can be imported each day.
# Currently, it just takes some files and replaces the deposit date to today.
# This can be executed on a windows task.

# It uses the executable located here: 
#  http://bitbucket.deluxe.com/projects/WFS/repos/r360-utilities/browse/DataImportDataTransformer/DataImportDataTransformer

# Expects to sit in your data import folder:
#  C:\Wfsapps\rechub\data\dataimport
#  Note: The executable noted above is expected to exist in the same folder.

# Expects folder structure to look like this (example):
#  C:\Wfsapps\rechub\data\dataimport\ach\000_sample\Automation-IAT-PaymentDEWithDocuments.dat

$currentdir = (get-item -path ".\" -verbose)

$ach = join-path $currentdir 'ach\000_sample'
set-location $ach
invoke-expression '..\..\DataImportDataTransformer.exe -t ach'
copy-item * '..\00_input'
write-host 'Imported ACH.'

$icon = join-path $currentdir 'icon\000_sample'
set-location $icon
invoke-expression '..\..\DataImportDataTransformer.exe -t imagerps'
copy-item * '..\00_input'
write-host 'Imported ImageRPS.'

$generic = join-path $currentdir 'generic\000_sample'
set-location $generic
invoke-expression '..\..\DataImportDataTransformer.exe -t generic'
copy-item * '..\00_input'
write-host 'Imported Generic.'

set-location $currentdir