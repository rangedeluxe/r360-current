# This script expects to exist in the /WFSStaging folder.
# The purpose is to pull the latest code from another server (like QA) and run the 
# installation steps in silent mode.

$serverlocation = '\\rechubqaclnt02.qalabs.nwk\E$\WFSStaging'
$includes = 
    'CLIENT-DIT',
    'CLIENT-EXTRACT',
    'CLIENT-FIT', 
    'Client-DIT Deploy RecHub.ps1',
    'Client-EXTRACT Deploy RecHub.ps1',
    'Client-FIT Deploy RecHub.ps1',
    'Wfs.R360.DeploymentConfigurationUtilities.dll',
    'WFS.R360.StagingUtilities.dll'
$scripts = 
    'Client-DIT Deploy RecHub.ps1',
    'Client-EXTRACT Deploy RecHub.ps1',
    'Client-FIT Deploy RecHub.ps1'

$currentdir = (get-item -path ".\" -verbose)

foreach ($include in $includes) {
    $copyfrom = join-path $serverlocation $include
    write-host "Copying '$copyfrom'..."
	copy-item $copyfrom $currentdir -force -recurse
}

foreach ($script in $scripts) {
	$command = '.\' + '"' + "$script" + '"' + ' -Silent'
	write-host "Running command '$command'..."
	invoke-expression "& $command"
}