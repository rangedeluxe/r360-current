# End-to-End Tests

## Preparation

1. Make sure the Node version on your computer is new enough to support async/await (at least v7.4).
2. `cd e2e` and `npm install`.

### Choosing which server to point to

Edit protractor.release.config.js and change `baseUrl`.

### Test data

Some tests assume that data has already been imported for the current date. (E.g. post-deposit exceptions)

RecHubIntClnt01 has a scheduled task that imports data every morning. It's currently set up to run under Brandon's credentials and Brandon has to be logged into the VM for it to run.

To manually run the import tests on RecHubIntClnt01, run E:\WFSApps\RecHub\Data\DataImport\importData.ps1.

If you want to import data into another environment, you'll have to set that up manually at this point.

## Running the tests

These tests run really slowly over the VPN (including in the Omaha office). If you're going to run them all, consider copying them to one of the web servers in QALabs (e.g. RecHubIntWeb01) and running them from there.

To run the tests, just run: `npm test`

### Running a subset of tests

Jasmine (and therefore Protractor) has a feature that lets you "focus" on one test or one suite - which makes Jasmine ignore everything else, and run only the focused test(s). You do this by editing the test file, and renaming the `it` to `fit` (focused it) or the `describe` to `fdescribe` (focused describe) for the tests you want to focus on. Obviously, only do this for debugging; don't check it in!

to run just an individual test
npm run test -- --specs tests/userpreferences.e2e.js
