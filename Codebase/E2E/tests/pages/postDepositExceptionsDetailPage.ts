import {browser, element, by} from 'protractor/built';
import {BrowserUtilities}  from '../utilities/browserutilities.e2e';
import {WebElement} from 'selenium-webdriver';

export class PostDepositExceptionsDetailPage {
    // Wait functions
    public async waitForPageLoadAsync() {
        await BrowserUtilities.waitForCSSTextAsync('.page-title', 'Exceptions Transaction Detail', 30000, 'Post Deposit Exceptions Detail took too long to load.');
        await this.waitForSpinnerAsync('initial page load');
    }
    public async waitForSpinnerAsync(description: string): Promise<void> {
        await BrowserUtilities.waitForCSSAsync('.spinDiv', 5000, `Post-Deposit Exceptions Detail: waiting for spinner to appear: ${description}`);
        await BrowserUtilities.waitForCSSRemovedAsync('.spinDiv', 30000, `Post-Deposit Exceptions Detail: waiting for spinner to disappear: ${description}`);
    }
    public async waitForModalAsync(expectedTitle: string): Promise<void> {
        await BrowserUtilities.waitForCSSTextAsync('#frameworkModalContent h4', expectedTitle, 30000,
            `Modal '${expectedTitle}' took too long to appear.`);
    }

    // Stub-field functions
    public async getStubFieldsAsync(): Promise<WebElement[]> {
        const finder = element.all(by.css('#stubs-table .stub-editor'));
        const fields = await finder.getWebElements();
        expect(fields.length).toBeGreaterThan(0, "No stub fields found");
        return fields;
    }
    public async getStubFieldAsync(locator: {index: number}): Promise<WebElement> {
        const fields = await this.getStubFieldsAsync();
        expect(locator.index).toBeLessThan(fields.length, `Requested stub field ${locator.index} but length was ${fields.length}`);
        return fields[locator.index];
    }
    public async clearStubFieldAsync(locator: {index: number}): Promise<void> {
        const field = await this.getStubFieldAsync(locator);
        await field.clear();
    }
    public async getStubFieldClassAsync(locator: {index: number}): Promise<string> {
        const field = await this.getStubFieldAsync(locator);
        const classValue = await field.getAttribute('class');
        return classValue;
    }
    public async populateAllFieldsAsync(): Promise<void> {
        const editors = await this.getStubFieldsAsync();
        for (const editor of editors) {
            await browser.executeScript('arguments[0].scrollIntoView();', editor);

            const dataType = await editor.getAttribute('data-type');
            editor.clear();
            if (dataType === 'numeric' || dataType === 'currency') {
                editor.sendKeys('123');
            } else if (dataType === 'date') {
                editor.sendKeys('1/1/2017');
            } else {
                editor.sendKeys('abc');
            }
        }
    }

    // Save/accept/navigate functions
    public async saveAsync(): Promise<void> {
        const saveButton = element(by.id('button-save'));
        await saveButton.click();
        await this.waitForSpinnerAsync('save');
    }
    public async acceptAsync(): Promise<void> {
        const saveButton = element(by.id('button-accept'));
        await saveButton.click();
        await this.waitForSpinnerAsync('accept');
    }
}
