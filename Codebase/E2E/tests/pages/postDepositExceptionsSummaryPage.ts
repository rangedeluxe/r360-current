import {browser, element, by, protractor} from 'protractor/built';
import {BrowserUtilities}  from '../utilities/browserutilities.e2e';
import {DataTableUtilities} from '../utilities/dataTableUtilities.e2e';
import {KeyValuePair} from '../objects/keyValuePair';
import {PostDepositExceptionsDetailPage} from './postDepositExceptionsDetailPage';

export class PostDepositExceptionsSummaryPage {
    // Wait functions
    public async waitForSpinnerAsync(): Promise<void> {
        await BrowserUtilities.waitForCSSAsync('.spinDiv', 5000, 'Post Deposit Exceptions took too long to search for a date.');
        await BrowserUtilities.waitForCSSRemovedAsync('.spinDiv', 30000, 'Post Deposit Exceptions took too long to retrieve search data.');
    }

    // Search functions
    public async searchAsync(text): Promise<void> {
        const searchelement = element(by.css('#postDepositTable_filter input'));
        await searchelement.sendKeys(text, protractor.Key.ENTER);
        await this.waitForSpinnerAsync();

        // If we have enough data that the grid is paginated, then select "Show 100 entries"
        const gridPageCount = await this.getGridPageCountAsync();
        if (gridPageCount >= 2) {
            await this.setGridPageSizeToMaxValueAsync();
        }
    }

    // Grid-pagination functions
    public async getGridPageCountAsync(): Promise<number> {
        const lastPageButtonText = await browser.element(by.css('.paginate_button:nth-last-child(2)')).getText();
        return parseInt(lastPageButtonText);
    }
    public async setGridPageSizeToMaxValueAsync(): Promise<void> {
        const showEntriesElement = element(by.name('postDepositTable_length')).element(by.css('option:last-child'));
        await showEntriesElement.click();
        await this.waitForSpinnerAsync();
    }

    // Grid-drilldown functions
    public async clickRowByDataAsync(data: object) {
        let values: KeyValuePair[] = Object.keys(data).map(key => <KeyValuePair> {key: key, value: data[key]});
        let rows = await DataTableUtilities.findRowsByData('postDepositTable_wrapper', values, true);
        // We need at one transaction, but if there are more (typically if the import was run
        // multiple times to re-run tests), who cares? We just need one we can use.
        expect(rows.length).toBeGreaterThanOrEqual(1);
        let row = rows[0];

        BrowserUtilities.clickUsingScript(row);

        const detail = new PostDepositExceptionsDetailPage();
        await detail.waitForPageLoadAsync();
    }
}
