import {browser, element, by, ExpectedConditions} from 'protractor/built';
import {RaamUtilities} from './utilities/raamUtilities.e2e';
import {UrlUtilities} from './utilities/urlUtilities.e2e';
import {NavigationUtilities} from './utilities/navigationUtilities.e2e';
import {BrowserUtilities} from './utilities/browserutilities.e2e';

describe('User Preferences E2E Test', () => {

    let DEFAULT_WAIT_TIME = 10000;
    
    beforeEach(() => {
        // This means we don't have an 'angular' page, which is true for the login pages.
        browser.ignoreSynchronization = true;
        // Sets the browser width.  Sometimes clicks can go a bit haywire if there is not enough space.
        browser.manage().window().setSize(1024, 1024);
    });

    it('should load up user preferences page', () => {
        RaamUtilities.login();
        NavigationUtilities.loadUserPreferencesPage();
    });

    it('should restore default preferences', () => {
        RaamUtilities.login();
        NavigationUtilities.loadUserPreferencesPage();

        // Restore the defaults.
        BrowserUtilities.click(element(by.id('restoreDefaults')));
        BrowserUtilities.waitForCSS('.toast-success', DEFAULT_WAIT_TIME, 'Restoring preferences took too long to respond.');
        BrowserUtilities.waitForCSSRemoved('.toast-success', DEFAULT_WAIT_TIME, 'Toast took too long to hide.');

        // Verify the results persist after a page refresh.
        NavigationUtilities.refresh();
        NavigationUtilities.loadUserPreferencesPage();
        expect(element(by.id('displaypayer')).isSelected()).toBeFalsy();
    });

    it('should save preferences', () => {
        RaamUtilities.login();
        NavigationUtilities.loadUserPreferencesPage();

        // Restore the defaults to make sure we have a blank test.
        BrowserUtilities.click(element(by.id('restoreDefaults')));
        BrowserUtilities.waitForCSS('.toast-success', DEFAULT_WAIT_TIME, 'Restoring preferences took too long to respond.');
        BrowserUtilities.waitForCSSRemoved('.toast-success', DEFAULT_WAIT_TIME, 'Toast took too long to hide.');

        // Update the pref to 'true'.
        BrowserUtilities.click(element(by.css('label[for="displaypayer"]')));
        // Save the prefs.
        BrowserUtilities.click(element(by.id('submit')));
        BrowserUtilities.waitForCSS('.toast-success', DEFAULT_WAIT_TIME, 'Saving preferences took too long to respond.');
        BrowserUtilities.waitForCSSRemoved('.toast-success', DEFAULT_WAIT_TIME, 'Toast took too long to hide.');

        // Verify the results persist after a page refresh.
        NavigationUtilities.refresh();
        NavigationUtilities.loadUserPreferencesPage();
        expect(element(by.id('displaypayer')).isSelected()).toBeTruthy();
    });

});