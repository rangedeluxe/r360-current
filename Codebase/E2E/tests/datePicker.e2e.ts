import {browser, element, by, ExpectedConditions, protractor} from 'protractor/built';
import {RaamUtilities} from './utilities/raamUtilities.e2e';
import {UrlUtilities} from './utilities/urlUtilities.e2e';
import {NavigationUtilities} from './utilities/navigationUtilities.e2e';
import {BrowserUtilities} from './utilities/browserUtilities.e2e';
import {WorkgroupSelectorUtilities} from './utilities/workgroupSelectorUtilities.e2e';
import {DatePickerUtilities} from './utilities/datepickerUtilities.e2e';

describe('DatePicker E2E tests', () => {
    let DEFAULT_WAIT_TIME = 10000;
    
    beforeEach(() => {
        // This means we don't have an 'angular' page, which is true for the login pages.
        browser.ignoreSynchronization = true;
        // Sets the browser width.  Sometimes clicks can go a bit haywire if there is not enough space.
        browser.manage().window().setSize(1024, 1024);
    });
    
    it('Load up the dashboard page', () => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();
        BrowserUtilities.waitForCSS('datepicker input', DEFAULT_WAIT_TIME, 'Dashboard datepicker took too long to load.');
        BrowserUtilities.waitForCSSRemoved('r360-dashboard loading', DEFAULT_WAIT_TIME, 'Dashboard took too long to remove loading spinner.');
    });

    it('datepicker should allow mm/dd/yyyy', async () => {
        DatePickerUtilities.selectDate('datepicker input', '08/21/2017');
        let val = await element(by.css('datepicker input')).getAttribute('value');
        expect(val).toBe('8/21/2017');
        BrowserUtilities.waitForCSSRemoved('r360-dashboard loading', DEFAULT_WAIT_TIME, 'Dashboard took too long to remove loading spinner.');
    });

    it('datepicker should allow mmddyyyy', async () => {
        DatePickerUtilities.selectDate('datepicker input', '08212017');
        let val = await element(by.css('datepicker input')).getAttribute('value');
        expect(val).toBe('8/21/2017');
        BrowserUtilities.waitForCSSRemoved('r360-dashboard loading', DEFAULT_WAIT_TIME, 'Dashboard took too long to remove loading spinner.');
    });

    it('datepicker should allow mmddyy', async () => {
        DatePickerUtilities.selectDate('datepicker input', '082117');
        let val = await element(by.css('datepicker input')).getAttribute('value');
        expect(val).toBe('8/21/2017');
        BrowserUtilities.waitForCSSRemoved('r360-dashboard loading', DEFAULT_WAIT_TIME, 'Dashboard took too long to remove loading spinner.');
    });

    it('datepicker should not allow 000000', async () => {
        let date = new Date();
        let today = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
        DatePickerUtilities.selectDate('datepicker input', '000000');
        let val = await element(by.css('datepicker input')).getAttribute('value');
        expect(val).toBe(today);
        BrowserUtilities.waitForCSSRemoved('r360-dashboard loading', DEFAULT_WAIT_TIME, 'Dashboard took too long to remove loading spinner.');
    });

    it('datepicker should not allow 222222', async () => {
        let date = new Date();
        let today = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
        DatePickerUtilities.selectDate('datepicker input', '222222');
        let val = await element(by.css('datepicker input')).getAttribute('value');
        expect(val).toBe(today);
        BrowserUtilities.waitForCSSRemoved('r360-dashboard loading', DEFAULT_WAIT_TIME, 'Dashboard took too long to remove loading spinner.');
    });

    it('should logout correctly.', async () => {
        RaamUtilities.logout();
    });

});