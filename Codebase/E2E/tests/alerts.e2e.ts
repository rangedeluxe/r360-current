import {browser, element, by, ExpectedConditions, protractor} from 'protractor/built';
import {RaamUtilities} from './utilities/raamUtilities.e2e';
import {UrlUtilities} from './utilities/urlUtilities.e2e';
import {NavigationUtilities} from './utilities/navigationUtilities.e2e';
import {BrowserUtilities} from './utilities/browserUtilities.e2e';
import {WorkgroupSelectorUtilities} from './utilities/workgroupSelectorUtilities.e2e';

describe('Alerts E2E Test', () => {
    let DEFAULT_WAIT_TIME = 10000;
    
    beforeEach(() => {
        // This means we don't have an 'angular' page, which is true for the login pages.
        browser.ignoreSynchronization = true;
        // Sets the browser width.  Sometimes clicks can go a bit haywire if there is not enough space.
        browser.manage().window().setSize(1024, 1024);
    });

	it('should load up alerts page', () => {
        RaamUtilities.login();
        NavigationUtilities.loadAlertsPage();
    });
	
    it('should add a new alert', () => {
        RaamUtilities.login();
        NavigationUtilities.loadAlertsPage();
        BrowserUtilities.click(element(by.id('addButton')));
        BrowserUtilities.waitForCSS('.alertInput .dropdown', DEFAULT_WAIT_TIME, 'Alerts took too long to load the dialog.');
        
        WorkgroupSelectorUtilities.waitToLoad('wgs-alertextract');

        WorkgroupSelectorUtilities.open('wgs-alertextract');
        WorkgroupSelectorUtilities.select('wgs-alertextract', '666');
    });

    it('should hide and show inactive event rules', () => {
        RaamUtilities.login();
        NavigationUtilities.loadAlertsPage();
        BrowserUtilities.click(element(by.css('label[for="hideInactiveAlerts"]')));
        BrowserUtilities.waitForCSSText('tr.datarow td.alignRight', '', DEFAULT_WAIT_TIME, 'Alerts took too long to load inactive event rules.');
    });
	
    it('should refresh when button is pressed', () => {
        RaamUtilities.login();
        NavigationUtilities.loadAlertsPage();
        BrowserUtilities.click(element(by.id('refreshPageButton')));
    });
	
});