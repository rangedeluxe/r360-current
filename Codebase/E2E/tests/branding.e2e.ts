import {browser, element, by, ExpectedConditions, protractor} from 'protractor/built';
import {RaamUtilities} from './utilities/raamUtilities.e2e';
import {NavigationUtilities} from './utilities/navigationUtilities.e2e';
import {BrowserUtilities} from './utilities/browserUtilities.e2e';

describe('Branding E2E Test', () => {
    let DEFAULT_WAIT_TIME = 30000;
    
    beforeEach(() => {
        // This means we don't have an 'angular' page, which is true for the login pages.
        browser.ignoreSynchronization = true;
        // Sets the browser width.  Sometimes clicks can go a bit haywire if there is not enough space.
        browser.manage().window().setSize(1024, 1024);
    });

    it('should load branding', () => {
        RaamUtilities.login();
        NavigationUtilities.getNavigationDropdown('Admin');
        //NavigationUtilities.getNavigationTab('Branding');
        //BrowserUtilities.waitForCSSText('.portlet-text', 'Branding Manager', DEFAULT_WAIT_TIME, 'Branding Manager took too long to load.');
    });
});