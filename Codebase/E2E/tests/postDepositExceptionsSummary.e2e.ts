import {browser, element, by, ExpectedConditions} from 'protractor/built';
import {RaamUtilities} from './utilities/raamUtilities.e2e';
import {UrlUtilities} from './utilities/urlUtilities.e2e';
import {NavigationUtilities} from './utilities/navigationUtilities.e2e';
import {BrowserUtilities} from './utilities/browserutilities.e2e';

describe('Post Deposit Exceptions E2E', () => {

    let DEFAULT_WAIT_TIME = 10000;
    
    beforeEach(() => {
        // This means we don't have an 'angular' page, which is true for the login pages.
        browser.ignoreSynchronization = true;
        // Sets the browser width.  Sometimes clicks can go a bit haywire if there is not enough space.
        browser.manage().window().setSize(1024, 1024);
    });

    it('should load up the post deposit summary page', () => {
        RaamUtilities.login();
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
    });

});