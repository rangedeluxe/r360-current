import {browser, element, by, ExpectedConditions, protractor} from 'protractor/built';
import {RaamUtilities} from './utilities/raamUtilities.e2e';
import {UrlUtilities} from './utilities/urlUtilities.e2e';
import {NavigationUtilities} from './utilities/navigationUtilities.e2e';
import {BrowserUtilities} from './utilities/browserUtilities.e2e';
import {WorkgroupSelectorUtilities} from './utilities/workgroupSelectorUtilities.e2e';
import {DatePickerUtilities} from './utilities/datepickerUtilities.e2e';
import {DataTableUtilities} from './utilities/dataTableUtilities.e2e';
import {KeyValuePair} from "./objects/keyValuePair";

describe('Dashboard E2E Test', () => {
    let DEFAULT_WAIT_TIME = 20000;
    
    beforeEach(() => {
        // This means we don't have an 'angular' page, which is true for the login pages.
        browser.ignoreSynchronization = true;
        // Sets the browser width.  Sometimes clicks can go a bit haywire if there is not enough space.
        browser.manage().window().setSize(1024, 1024);
    });

    it('should load the dashboard page', () => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();
    });

    it('should view dashboard chart data with workgroup selection', () => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();

        // AUTOMATED DATA: Using workgroup 303.
        BrowserUtilities.waitForCSS('datepicker input', DEFAULT_WAIT_TIME, 'Dashboard datepicker took too long to load.');
        BrowserUtilities.waitForCSSRemoved('r360-dashboard loading', DEFAULT_WAIT_TIME, 'Dashboard took too long to remove loading spinner.');

        // Open the WGS and select the WG.
        WorkgroupSelectorUtilities.waitToLoad('dashboard-wgs');
        WorkgroupSelectorUtilities.open('dashboard-wgs');
        WorkgroupSelectorUtilities.select('dashboard-wgs', 'AUTOMATION');
        
        // Wait for the data to load for this workgroup.
        BrowserUtilities.waitForCSS('r360-dashboard loading', DEFAULT_WAIT_TIME, 'Dashboard took too long to request data after workgroup selection.');
        browser.actions().click(element(by.css('r360-dashboard loading')));
        BrowserUtilities.waitForCSS('.legendlist', DEFAULT_WAIT_TIME, 'Dashboard did not load up the charts.');
    });
    
    it('should modify data based on legend interaction', async () => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();
        // AUTOMATED DATA: Using workgroup 303, assuming ACH data.
        BrowserUtilities.waitForCSS('.legendlist', DEFAULT_WAIT_TIME, 'Dashboard did not load up the charts.');
        let amount = await element.all(by.css('.chart-total')).get(0).getText();
        let ach = element.all(by.cssContainingText('.legendlist li span', 'ACH')).get(0);
        BrowserUtilities.click(ach);
        let newamount = await element.all(by.css('.chart-total')).get(0).getText();
        expect(amount).not.toEqual(newamount);
    });

    it('should show the rec summary grid', async () => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();
        BrowserUtilities.waitForCSSText('label', 'Receivables Summary', 30000, 'Receivables summary grid took too long to load.');
    });

    it('should show imageRPS and ACH data', async () => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();
        // AUTOMATED DATA: Using workgroup 303, assuming ACH & ImageRPS data.
        BrowserUtilities.waitForCSSText('label', 'Receivables Summary', 30000, 'Receivables summary grid took too long to load.');
        let search = [{ key: "Payment Type", value: "ACH" }] as Array<KeyValuePair>;
        let rows = await DataTableUtilities.findRowsByData('rec-grid', search);
        expect(rows.length).toBe(1);
        search = [{ key: "Payment Type", value: "Check" }] as Array<KeyValuePair>;
        rows = await DataTableUtilities.findRowsByData('rec-grid', search);
        expect(rows.length).toBe(1);
    });

    it('should minimize the summary charts', async() => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();
        BrowserUtilities.waitForCSS('.legendlist', DEFAULT_WAIT_TIME, 'Dashboard did not load up the charts.');
        let buttons = await element.all((by.css('.panel-heading button.collapser'))).getWebElements();
        expect(buttons.length).toBe(2);
        BrowserUtilities.click(buttons[0]);
        BrowserUtilities.waitForCSSRemoved('.legendlist', 2000, 'The dashboard panels did not close after minimizing.');
    });

    it('should minimize the summary grid', async() => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();
        BrowserUtilities.waitForCSSText('label', 'Receivables Summary', 30000, 'Receivables summary grid took too long to load.');
        let buttons = await element.all((by.css('.panel-heading button.collapser'))).getWebElements();
        expect(buttons.length).toBe(2);
        BrowserUtilities.click(buttons[1]);
        BrowserUtilities.waitForCSSRemoved('#rec-grid_filter input', 2000, 'The dashboard panels did not close after minimizing.');
    });

    it('should maximize the panels', async() => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();
        BrowserUtilities.waitForCSSText('label', 'Receivables Summary', 30000, 'Receivables summary grid took too long to load.');
        let buttons = await element.all((by.css('.panel-heading button.collapser'))).getWebElements();
        expect(buttons.length).toBe(2);
        BrowserUtilities.click(buttons[0]);
        BrowserUtilities.click(buttons[1]);
        BrowserUtilities.waitForCSS('#rec-grid_filter input', 2000, 'The dashboard panels did not open after maximizing.');
    });

    it('should select different groupings', async() => {
        RaamUtilities.login();
        NavigationUtilities.loadDashboardPage();
        BrowserUtilities.waitForCSSText('label', 'Receivables Summary', 30000, 'Receivables summary grid took too long to load.');
        
        // expect there to be 5 grouping options.
        let options = await element.all((by.css('#selectbox-grouping option'))).getWebElements();
        expect(options.length).toBe(5);

        // expect 3 different ddas on this screen.
        options[0].click();
        let list = element.all(by.css('.legendlist')).get(0);
        let legends = await list.all(by.css('li')).getWebElements();
        expect(legends.length).toBe(3);

        // expect 1 Entity group.
        options[1].click();
        list = element.all(by.css('.legendlist')).get(0);
        legends = await list.all(by.css('li')).getWebElements();
        expect(legends.length).toBe(1);

        // expect 1 payment source group.
        options[2].click();
        list = element.all(by.css('.legendlist')).get(0);
        legends = await list.all(by.css('li')).getWebElements();
        expect(legends.length).toBe(1);

        // expect 3 payment type groups.
        options[3].click();
        list = element.all(by.css('.legendlist')).get(0);
        legends = await list.all(by.css('li')).getWebElements();
        expect(legends.length).toBe(3);

        // expect 1 workgroup group.
        options[4].click();
        list = element.all(by.css('.legendlist')).get(0);
        legends = await list.all(by.css('li')).getWebElements();
        expect(legends.length).toBe(1);
    });

});