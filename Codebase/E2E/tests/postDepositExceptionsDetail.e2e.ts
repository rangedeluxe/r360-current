import {browser, element, by, ExpectedConditions, protractor} from 'protractor/built';
import {RaamUtilities} from './utilities/raamUtilities.e2e';
import {UrlUtilities} from './utilities/urlUtilities.e2e';
import {NavigationUtilities} from './utilities/navigationUtilities.e2e';
import {BrowserUtilities} from './utilities/browserutilities.e2e';
import {DataTableUtilities} from './utilities/dataTableUtilities.e2e';
import {KeyValuePair} from './objects/keyValuePair';
import {PostDepositExceptionsSummaryPage} from './pages/postDepositExceptionsSummaryPage';
import {PostDepositExceptionsDetailPage} from './pages/postDepositExceptionsDetailPage';

describe('Post Deposit Exceptions E2E', () => {

    let DEFAULT_WAIT_TIME = 10000;
    
    beforeEach(() => {
        // This means we don't have an 'angular' page, which is true for the login pages.
        browser.ignoreSynchronization = true;
        // Sets the browser width.  Sometimes clicks can go a bit haywire if there is not enough space.
        // 1440 is enough for the full width of the table, which we need to grab the text from the headers.
        browser.manage().window().setSize(1440, 1440);
    });

    let getToday = () => {
        let today = new Date();
        let dd = today.getDate().toString();
        let mm = (today.getMonth()+1).toString(); //January is 0!
        let yyyy = today.getFullYear().toString();
        if(dd.length == 1)
            dd='0'+dd;
        if(mm.length == 1)
            mm='0'+mm;
        return mm+'/'+dd+'/'+yyyy;
    };

    it('should load up the post deposit detail page', () => {
        // Nondestructive - can be run repeatedly in isolation.
        // Isolated - no other tests modify data that interferes with this test.
        // Resilient to re-import - will not fail if data is imported multiple times.
        RaamUtilities.login();
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        BrowserUtilities.clickUsingScript(element.all(by.css('#postDepositTable tbody tr')).get(0));
        BrowserUtilities.waitForCSSText('.page-title', 'Exceptions Transaction Detail', 30000, 'Post Deposit Exceptions Detail took too long to load.');
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post Deposit Exceptions Detail took too long to request initial data.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post Deposit Exceptions Detail took too long to load initial data.');
    });

    // AUTOMATION - assumes data batchID 100 and transaction 1. Should be data with 1 stub.
    it('new stubs should have a delete button', async () => {
        // Nondestructive - can be run repeatedly in isolation.
        // Not isolated - other tests interfere with this test's data.
        // Resilient to re-import - will not fail if data is imported multiple times,
        //   because ImageRPS re-import will overwrite the existing batch.
        RaamUtilities.login();
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        const summary = new PostDepositExceptionsSummaryPage();

        let today = getToday();
        let search = [ 
            {key: "Workgroup", value: "303-AUTOMATION"}, 
            {key: "Batch ID", value: "100"}, 
            {key: "Transaction ID", value: "1"} ] as Array<KeyValuePair>;

        await summary.searchAsync(today);

        let rows = await DataTableUtilities.findRowsByData('postDepositTable_wrapper', search, true);
        expect(rows.length).toBe(1);
        BrowserUtilities.clickUsingScript(rows[0]);
        BrowserUtilities.waitForCSSText('.page-title', 'Exceptions Transaction Detail', 30000, 'Post Deposit Exceptions Detail took too long to load.');
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post Deposit Exceptions Detail took too long to request initial data.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post Deposit Exceptions Detail took too long to load initial data.');
        
        BrowserUtilities.clickUsingScript(element(by.css('.btn-add-related-item')));
        BrowserUtilities.waitForCSS('#stubs-table .btn-remove', 2000, 'Delete button not found in post deposit exceptions.');
        
        // Click the delete icon, remove the new row.
        BrowserUtilities.clickUsingScript(element(by.css('#stubs-table .btn-remove')));
        BrowserUtilities.waitForCSSText('#frameworkModalContent h4', 'Confirm Delete', 2000, 'Delete confirm dialog took too long to appear.');
        BrowserUtilities.waitForCSS('#frameworkModalContent .btn-primary', 2000, 'Delete button took too long to appear.');
        BrowserUtilities.clickUsingScript(element(by.css('#frameworkModalContent .btn-primary')));
    });

    it('transactions should have an accept button', async () => {
        // Nondestructive - can be run repeatedly in isolation.
        // Not isolated - other tests interfere with this test's data.
        // Resilient to re-import - will not fail if data is imported multiple times,
        //   because ImageRPS re-import will overwrite the existing batch.
        RaamUtilities.login();
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        const summary = new PostDepositExceptionsSummaryPage();

        let today = getToday();
        let search = [ 
            {key: "Workgroup", value: "303-AUTOMATION"}, 
            {key: "Batch ID", value: "100"}, 
            {key: "Transaction ID", value: "1"} ] as Array<KeyValuePair>;

        await summary.searchAsync(today);

        let rows = await DataTableUtilities.findRowsByData('postDepositTable_wrapper', search, true);
        expect(rows.length).toBe(1);
        BrowserUtilities.clickUsingScript(rows[0]);
        BrowserUtilities.waitForCSSText('.page-title', 'Exceptions Transaction Detail', 30000, 'Post Deposit Exceptions Detail took too long to load.');
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post Deposit Exceptions Detail took too long to request initial data.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post Deposit Exceptions Detail took too long to load initial data.');
        BrowserUtilities.waitForCSS('#button-accept', 2000, 'Post Deposit Exceptions Detail does not show an accept button.');
        BrowserUtilities.clickUsingScript(element(by.css('#button-accept')));
        BrowserUtilities.waitForCSSText('#frameworkModalContent h4', 'Confirm Accept', 30000, 'Accept confirm dialog took too long to appear.');
        BrowserUtilities.click(element(by.css('#frameworkModalContent .btn-inverse')));
    });

    it('transactions should save my data', async () => {
        // Nondestructive - can be run repeatedly in isolation.
        // Not isolated - other tests interfere with this test's data.
        // Resilient to re-import - will not fail if data is imported multiple times,
        //   because ImageRPS re-import will overwrite the existing batch.
        RaamUtilities.login();
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        const summary = new PostDepositExceptionsSummaryPage();

        let today = getToday();
        let search = [ 
            {key: "Workgroup", value: "303-AUTOMATION"}, 
            {key: "Batch ID", value: "100"}, 
            {key: "Transaction ID", value: "1"} ] as Array<KeyValuePair>;

        await summary.searchAsync(today);

        let rows = await DataTableUtilities.findRowsByData('postDepositTable_wrapper', search, true);
        expect(rows.length).toBe(1);
        BrowserUtilities.clickUsingScript(rows[0]);
        BrowserUtilities.waitForCSSText('.page-title', 'Exceptions Transaction Detail', 30000, 'Post Deposit Exceptions Detail took too long to load.');
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post Deposit Exceptions Detail took too long to request initial data.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post Deposit Exceptions Detail took too long to load initial data.');

        // Edit the one column that has an exception. (should be 'Sailu1')
        const newValue = 'automation-test' + Math.floor(Math.random() * 1000);
        const editor = element(by.css('#stubs-table .stub-editor'));
        editor.clear();
        editor.sendKeys(newValue);
        element(by.css('#button-next-transaction')).click();
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post Deposit Exceptions Detail did not request to save the data.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post Deposit Exceptions Detail took too long to load next transaction.');

        // Reload the detail page, and check our data still exists.
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        await summary.searchAsync(today);

        rows = await DataTableUtilities.findRowsByData('postDepositTable_wrapper', search, true);
        expect(rows.length).toBe(1);
        BrowserUtilities.clickUsingScript(rows[0]);
        BrowserUtilities.waitForCSSText('.page-title', 'Exceptions Transaction Detail', 30000, 'Post Deposit Exceptions Detail took too long to load.');
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post Deposit Exceptions Detail took too long to request initial data.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post Deposit Exceptions Detail took too long to load initial data.');
        let text = await element(by.css('#stubs-table .stub-editor')).getAttribute('value');
        expect(text).toBe(newValue);
    });

    it('accept button should complete the exception', async () => {
        // Destructive - cannot be run repeatedly in isolation. Accepts the transaction, removing it from the list.
        // Resilient to re-import - will not fail if data is imported multiple times,
        //   because ImageRPS re-import will overwrite the existing batch.
        RaamUtilities.login();
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        const summary = new PostDepositExceptionsSummaryPage();

        let today = getToday();
        let search = [
            {key: "Workgroup", value: "303-AUTOMATION"},
            {key: "Batch ID", value: "100"},
            {key: "Transaction ID", value: "1"}
        ] as Array<KeyValuePair>;

        await summary.searchAsync(today);

        let rows = await DataTableUtilities.findRowsByData('postDepositTable_wrapper', search, true);
        expect(rows.length).toBe(1);
        BrowserUtilities.clickUsingScript(rows[0]);
        BrowserUtilities.waitForCSSText('.page-title', 'Exceptions Transaction Detail', 30000, 'Post Deposit Exceptions Detail took too long to load.');
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post Deposit Exceptions Detail took too long to request initial data.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post Deposit Exceptions Detail took too long to load initial data.');
        BrowserUtilities.waitForCSS('#button-accept', 2000, 'Post Deposit Exceptions Detail does not show an accept button.');
        BrowserUtilities.clickUsingScript(element(by.css('#button-accept')));
        BrowserUtilities.waitForCSSText('#frameworkModalContent h4', 'Confirm Accept', 30000, 'Accept confirm dialog took too long to appear.');
        BrowserUtilities.click(element(by.css('#frameworkModalContent .btn-primary')));
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post Deposit Exceptions Detail took too long to request the save.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post Deposit Exceptions Detail took too long to complete the transaction.');

        // Go back and verify the row is gone on the summary page.
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        await summary.searchAsync(today);

        rows = await DataTableUtilities.findRowsByData('postDepositTable_wrapper', search, true);
        expect(rows.length).toBe(0);
    });

    it('saving an empty row should ignore the row', async () => {
        // AUTOMATION Data - Assumes ACH batch with amount 139.96.
        // Nondestructive - can be run repeatedly in isolation.
        // Isolated - no other tests modify data that interferes with this test.
        // Resilient to re-import - will not fail if data is imported multiple times.
        RaamUtilities.login();
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        const summary = new PostDepositExceptionsSummaryPage();

        let today = getToday();

        await summary.searchAsync(today);

        // Find our ACH row.  (ACH rows have dynamic batch IDs, so we're going to search for the amount.)
        const search = {"Payment Type": "ACH", "Amount": "$139.96"};
        await summary.clickRowByDataAsync(search);

        BrowserUtilities.waitForCSS('.btn-add-related-item', 30000, 'Post deposit exceptions detail took too long to show the add related item button.');
        BrowserUtilities.clickUsingScript(element(by.css('.btn-add-related-item')));
        BrowserUtilities.waitForCSS('.btn-remove', 2000, 'Post deposit exceptions did not show the delete button after clicking add new related item.');
        BrowserUtilities.clickUsingScript(element(by.css('#button-next-transaction')));
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post deposit exceptions took too long to request to save the transaction.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post deposit exceptions took too long to save the transaction.');

        // Head back and find the transaction again.
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        await summary.searchAsync(today);
        await summary.clickRowByDataAsync(search);

        // Verify we only have the 1 auto-created row, not 2 rows.
        let stubrows = await element.all(by.css('#stubs-table tbody tr')).getWebElements();
        expect(stubrows.length).toBe(1);
    });

    it('refreshes data-type errors when you try to accept', async () => {
        // AUTOMATION Data - Assumes ACH batch with amount 215.74.
        // Nondestructive - can be run repeatedly in isolation.
        // Isolated - no other tests modify data that interferes with this test.
        // Resilient to re-import - will not fail if data is imported multiple times.
        const today = getToday();

        RaamUtilities.login();
        NavigationUtilities.loadPostDepositExceptionsSummaryPage();
        const summary = new PostDepositExceptionsSummaryPage();

        await summary.searchAsync(today);
        await summary.clickRowByDataAsync({"Payment Type": "ACH", "Amount": "$215.74"});

        const detail = new PostDepositExceptionsDetailPage();

        // Arrange: Make the transaction valid.
        await detail.populateAllFieldsAsync();
        await detail.saveAsync();

        // Act: Clear out one field and accept. Should get "are you sure?" modal.
        await detail.clearStubFieldAsync({index: 0});
        await detail.acceptAsync();
        await detail.waitForModalAsync('Confirm Accept');

        // Assert: The field we cleared should be red.
        expect(await detail.getStubFieldClassAsync({index: 0})).toMatch(/\bhas-exception\b/);
    });
});