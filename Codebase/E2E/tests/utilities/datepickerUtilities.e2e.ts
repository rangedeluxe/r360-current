import {browser, element, by, ExpectedConditions, protractor, WebElement} from 'protractor/built';
// This class simply attempts to cut down on code and to make tests more readable.
export class DatePickerUtilities {
    public static selectDate(selector:string, date:string) {
        let datepicker = element(by.css(selector));
        datepicker.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        datepicker.sendKeys(protractor.Key.DELETE);
        datepicker.sendKeys(date);
        datepicker.sendKeys(protractor.Key.ENTER);
    }
}