import {browser, element, by, ExpectedConditions, protractor, WebElement, ElementFinder, promise} from 'protractor/built';
import { KeyValuePair } from "../objects/keyValuePair";

export class DataTableUtilities {

    public static findColumnIndex(tableid:string, columnname:string, isScrollTable: boolean = false): promise.Promise<number> {
        let byquery = isScrollTable
            ? by.css(`#${tableid} .dataTables_scrollHead thead th`)
            : by.css(`#${tableid} thead th`);
        let cols = element.all(byquery);
        return cols.getWebElements()
            .then(elements => {
                return protractor.promise.all(elements.map((e) => e.getText()))
                    .then(columns => {
                        return columns.indexOf(columnname);
                    });
            });
    }

    // Function finds rows with data mapped by the key (columnname) and expected column value (text).
    public static findRowsByData(tableid:string, values:Array<KeyValuePair>, isScrollTable: boolean = false) : promise.Promise<WebElement[]> {
        let byquery = isScrollTable
            ? by.css(`#${tableid} .dataTables_scrollBody tbody tr`)
            : by.css(`#${tableid} tbody tr`);
        let rows = element.all(byquery);
        rows = rows.filter((e) => {
            return e.all(by.css('td'))
                .getWebElements()
                .then((tds:WebElement[]) => {
                    return promise.all(values.map(v => this.findColumnIndex(tableid, v.key, isScrollTable)))
                        .then(indexes => {
                            let maps = values.map( (v, i) => ({ expected: v.value, index: indexes[i], actual: null }) );
                            let promises = tds.map((td) => td.getText());
                            return protractor.promise.all(promises)
                                .then(vals => {
                                    for (let m of maps) {
                                        m.actual = vals[m.index];
                                    }
                                    return !maps.some((v) => v.expected !== v.actual);
                                });
                        });
                });
        });
        return rows.getWebElements();
    }
}