import {browser, element, by, ExpectedConditions, protractor} from 'protractor/built';
import * as chalk from 'chalk';
import * as fs from 'fs';
import * as path from 'path';
import * as repl from 'repl';
import * as util from 'util';

export class InteractiveExplorer {
    public static async exploreAsync() {
        if (!process.stdin.isTTY || !process.stdout.isTTY) {
            console.error('WARNING: Cannot run interactive explorer in non-interactive session');
            return;
        }

        // We have some test code that doesn't properly await promises. If such code gets run
        // before we get called, we could show our prompt right away, but then the user would have
        // to wait while stuff finishes happening in the browser window, which is undesirable.
        // To mitigate this, let's force a wait until all previous operations are done.
        await browser.element(by.className('body')).isPresent();

        console.log(
            chalk.cyan.bold('Starting interactive explorer. Enter JavaScript expressions, or type "') +
            chalk.blue.bold('.exit') +
            chalk.cyan.bold('" to exit.')
        );
        const replPromise = new Promise(resolve => {
            const prompt = chalk.cyan.bold('Explore>> ');
            const replServer = repl.start({
                ignoreUndefined: true,
                prompt: prompt,
                writer: output => {
                    if (output.then) {
                        output.then(output => {
                            // The cursor is probably on our 'Explore>>' line right now. Start a new line.
                            console.log('');
                            if (output) {
                                console.log(chalk.cyan('Promise result:'), output);
                            } else {
                                console.log("Promise completed");
                            }
                            // Hopefully the promise didn't take long enough for the user to start
                            // typing another expression. If they did, we can't re-echo the thing
                            // they were typing. (Maybe worth noting: the readline call *will*
                            // re-echo the user's entry if the user hits 'Backspace' or something.)
                            process.stdout.write(prompt);
                        });
                        return chalk.cyan('Evaluating promise...');
                    } else {
                        return util.inspect(output);
                    }
                }
            });
            for (const dir of ['../utilities', '../pages']) {
                for (const fileName of fs.readdirSync(path.join(__dirname, dir))) {
                    // Look for .ts files, so we don't accidentally pick up a stale .js file
                    // on a dev machine whose .ts has been renamed
                    if (fileName.endsWith('.ts')) {
                        const jsFileName = fileName.substr(0, fileName.length - 3) + '.js';
                        const filePath = path.join(__dirname, dir, jsFileName);
                        const module = require(filePath);
                        for (const key in module) {
                            Object.defineProperty(replServer.context, key, {
                                configurable: false,
                                enumerable: true,
                                value: module[key]
                            });
                        }
                    }
                }
            }
            replServer.on('exit', () => {
                console.log(chalk.cyan.bold('Received "exit" request'));
                resolve();
            });
        });
        await replPromise;
        console.log(chalk.cyan.bold('Exiting interactive explorer'));
    }
}
