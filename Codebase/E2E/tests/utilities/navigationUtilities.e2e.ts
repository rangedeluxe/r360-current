import {browser, element, by, ExpectedConditions, protractor} from 'protractor/built';
import {UrlUtilities} from './urlutilities.e2e';
import {BrowserUtilities}  from './browserutilities.e2e';
// Contains utility functions to load each page and to wait until the page is ready.
export class NavigationUtilities {
    public static getNavigationDropdown(tabtext:string) {
        BrowserUtilities.waitForCSSText('#recTabs a span', tabtext, 15000, `Navigation dropdown '${tabtext}' took too long to load.`);
        return element(by.cssContainingText('#recTabs a span', tabtext)).element(by.xpath('..'));
    }

    public static getNavigationTab(tabtext:string) {
        BrowserUtilities.waitForCSSText('#recTabs a', tabtext, 15000, `Navigation tab '${tabtext}' took too long to load.`);
        return element(by.cssContainingText('#recTabs a', tabtext));
    }

    public static loadUserPreferencesPage() {
        BrowserUtilities.click(this.getNavigationDropdown('Admin'));
        BrowserUtilities.click(this.getNavigationTab('User Preferences'));
        BrowserUtilities.waitForCSS('label[for="displaypayer"]', 30000, 'User preferences page took too long to load.');
    }

    public static loadReceivablesSummaryPage() {
        BrowserUtilities.clickUsingScript(this.getNavigationTab('Receivables Summary'));
        BrowserUtilities.waitForCSS('input.hasDatepicker', 30000, 'Receivables Summary took too long to load.');
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Receivables Summary took too long to request initial data.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Receivables Summary took too long to load initial data.');
    }

    public static loadPostDepositExceptionsSummaryPage() {
        BrowserUtilities.click(this.getNavigationDropdown('Exceptions'));
        BrowserUtilities.click(this.getNavigationTab('Post-Deposit Exceptions'));
        BrowserUtilities.waitForCSSText('.page-title', 'Post-Deposit Exceptions Summary', 30000, 'Post Deposit Exceptions Summary took too long to load.');
        BrowserUtilities.waitForCSS('.spinDiv', 5000, 'Post Deposit Exceptions Summary took too long to request initial data.');
        BrowserUtilities.waitForCSSRemoved('.spinDiv', 30000, 'Post Deposit Exceptions Summary took too long to load initial data.');
    }

    public static loadExtractConfigurationManagerPage() {
        BrowserUtilities.click(this.getNavigationDropdown('Admin'));
        BrowserUtilities.click(this.getNavigationTab('Extract Config Manager'));
        BrowserUtilities.waitForCSSText('.portlet-text', 'Extract Definitions', 30000, 'Extract config manager took too long to load.');
        BrowserUtilities.waitForCSS('#definitionsTable tbody tr[role="row"]', 30000, 'Extract Definitions took too long to load initial data.');
    }

    public static loadAlertsPage() {
        // ToDo - update when the navigation tab changes to the new alerts page.
        browser.get(UrlUtilities.formBaseUrl('/AlertManager'));
        BrowserUtilities.waitForCSSText('h4', 'Alert Manager', 30000, 'Alerts took too long to load.');
    }

    public static loadDashboardPage() {
        // ToDo - update when the navigation tab changes to the new alerts page.
        browser.get(UrlUtilities.formBaseUrl('/Dashboard'));
        BrowserUtilities.waitForCSSText('h4', 'Dashboard', 30000, 'Dashboard took too long to load.');
    }
    
    public static refresh() {
        // Refreshes the page, waits for 'dashboard' to show up.
        browser.get(UrlUtilities.formBaseUrl('/'));
        this.getNavigationTab('Dashboard');
    }
}