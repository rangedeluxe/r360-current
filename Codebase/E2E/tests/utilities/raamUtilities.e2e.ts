import {browser, element, by, ExpectedConditions} from 'protractor/built';
import {BrowserUtilities} from './browserutilities.e2e';
import {UrlUtilities} from './urlUtilities.e2e';
import { RaamCredential } from '../objects/raamCredential';

export class RaamUtilities {
    public static login(cred?:RaamCredential) {
        if (!cred)
            cred = require('../staticdata/raamCredentials.json').admin as RaamCredential;

        browser.get(UrlUtilities.formBaseUrl('/Logout'));
        browser.get(UrlUtilities.formBaseUrl('/'));
        BrowserUtilities.waitForID('Entity', 30000, 'Login page took too long to load.');
        element(by.id('Entity')).sendKeys(cred.entity);
        element(by.id('UserName')).sendKeys(cred.username);
        element(by.id('Password')).sendKeys(cred.password);
        BrowserUtilities.click(element.all(by.css('.saveButton')).get(0));
        BrowserUtilities.waitForCSSText('a', 'sign out', 30000, 'R360UI took too long to respond.');
    };

    public static logout() {
        browser.get(UrlUtilities.formBaseUrl('/Logout'));
        BrowserUtilities.waitForCSSText('h2', 'Sign Out', 30000, 'Raam took too long to log out.');
    }
}