import {browser} from 'protractor/built';
export class UrlUtilities {
    // Forms a URL based on the protractor configuration loaded.
    public static formBaseUrl(url:string) {
        return `${browser.baseUrl}${url}`;
    }
}