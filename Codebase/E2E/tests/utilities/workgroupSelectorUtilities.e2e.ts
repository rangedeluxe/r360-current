import {browser, element, by} from 'protractor/built';
import {BrowserUtilities} from './browserUtilities.e2e';
export class WorkgroupSelectorUtilities {
    public static WGS_WAIT_TIME:number = 20000;
    public static waitToLoad(id:string) {
        // Waiting for 'Loading...' to turn into ''.
        BrowserUtilities.waitForCSSText(`#${id}.workgroup-selector>div`, '', this.WGS_WAIT_TIME, `WGS '${id}' took too long to load.`);
    }

    public static open(id:string) {
        BrowserUtilities.click(element(by.css(`#${id} .jqx-widget-header.jqx-expander-header`)));
        BrowserUtilities.waitForCSS(`#${id} .jqx-expander-content`, this.WGS_WAIT_TIME, `WGS '${id}' took too long to open.`);
    }

    public static select(id:string, name:string) {
        BrowserUtilities.click(element(by.css(`#${id} a.select2-choice.select2-default`)));
        // select2 is an absolute-positioned dropdown, so it's directly under body and we cant scope by ID.
        BrowserUtilities.waitForCSS(`body>.select2-drop>.select2-search>.select2-input`, this.WGS_WAIT_TIME, `WGS '${id}' took too long to open search panel.`);
        element(by.css(`body>.select2-drop>.select2-search>.select2-input`)).sendKeys(name);
        BrowserUtilities.waitForCSSText(`body>.select2-drop>.select2-results b`, name, this.WGS_WAIT_TIME, `WGS '${id}' took too long to search for '${name}'.`);
        BrowserUtilities.click(element(by.css(`body>.select2-drop>.select2-results li`)));
    }
}