import {browser, element, by, ExpectedConditions, protractor, WebElement} from 'protractor/built';
// This class simply attempts to cut down on code and to make tests more readable.
export class BrowserUtilities {
    public static waitForCSS(css:string, timeout:number, error:string) {
        browser.wait(ExpectedConditions.visibilityOf(element(by.css(css))), timeout, error);
    }
    public static async waitForCSSAsync(css:string, timeout:number, error:string): Promise<void> {
        await browser.wait(ExpectedConditions.visibilityOf(element(by.css(css))), timeout, error);
    }
    public static waitForCSSText(css:string, text:string, timeout:number, error:string) {
        browser.wait(ExpectedConditions.textToBePresentInElement(
            element(by.cssContainingText(css, text)), text), timeout, error);
        browser.wait(ExpectedConditions.visibilityOf(element(by.cssContainingText(css, text))), timeout, error);
    }
    public static async waitForCSSTextAsync(css:string, text:string, timeout:number, error:string): Promise<void> {
        await browser.wait(ExpectedConditions.textToBePresentInElement(
            element(by.cssContainingText(css, text)), text), timeout, error);
        await browser.wait(ExpectedConditions.visibilityOf(element(by.cssContainingText(css, text))), timeout, error);
    }
    public static waitForID(id:string, timeout:number, error:string) {
        browser.wait(ExpectedConditions.visibilityOf(element(by.id(id))), timeout, error);
    }
    public static waitForCSSRemoved(css:string, timeout:number, error:string) {
        browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(element(by.css(css)))), timeout, error);
    }
    public static async waitForCSSRemovedAsync(css:string, timeout:number, error:string): Promise<void> {
        await browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(element(by.css(css)))), timeout, error);
    }
    public static waitForIDRemoved(id:string, timeout:number, error:string) {
        browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(element(by.id(id)))), timeout, error);
    }

    // See this link for the reasons why we're not using 'element.click()'.
    // http://www.blaiseliu.com/protractor-error-element-is-not-clickable-at-point-xx-xx/
    public static click(element:WebElement) {
        // Use this method as much as we can, but JQuery-Clicks might be required for certain reasons.
        browser.actions().click(element).perform();
    }

    public static clickUsingScript(element:WebElement) {
        // Launches script to handle the click. This forces a click on the element, even if it might not recieve it.
        browser.executeScript('arguments[0].click()', element);
    }

}