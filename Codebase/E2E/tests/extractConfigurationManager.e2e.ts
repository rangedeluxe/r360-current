import {browser, element, by, ExpectedConditions, protractor} from 'protractor/built';
import {RaamUtilities} from './utilities/raamUtilities.e2e';
import { NavigationUtilities } from './utilities/navigationUtilities.e2e';
import { RaamCredential } from './objects/raamCredential';
import { BrowserUtilities } from './utilities/browserutilities.e2e';

describe('Extract config manager E2E Test', () => {
    let DEFAULT_WAIT_TIME = 20000;
    let credentials = require('./staticdata/raamCredentials.json');
    let viewOnlyCredential = credentials.extractView as RaamCredential;
    let adminCredential = credentials.admin as RaamCredential;
    
    beforeEach(() => {
        browser.ignoreSynchronization = true;
        browser.manage().window().setSize(1024, 1024);
    });

    it('should load the extract page', () => {
        RaamUtilities.login(viewOnlyCredential);
        NavigationUtilities.loadExtractConfigurationManagerPage();
    });

    it('should not display the edit buttons when user only has view permission', async () => {
        // DATA: Assumes we have at least 1 definition uploaded.
        RaamUtilities.login(viewOnlyCredential);
        NavigationUtilities.loadExtractConfigurationManagerPage();

        BrowserUtilities.waitForIDRemoved('addextractbutton', 1, 'Extract definition page did not hide the add button.');
        BrowserUtilities.waitForCSSRemoved('i.update', 1, 'Extract definition page did not hide the edit buttons.');
        BrowserUtilities.waitForCSSRemoved('i.delete', 1, 'Extract definition page did not hide the delete buttons.');
    });

    it('should display the edit buttons when user has manage permission', async () => {
        // DATA: Assumes we have at least 1 definition uploaded.
        RaamUtilities.login(adminCredential);
        NavigationUtilities.loadExtractConfigurationManagerPage();

        BrowserUtilities.waitForID('addextractbutton', 1, 'Extract definition page did not show the add button.');
        BrowserUtilities.waitForCSS('i.update', 1, 'Extract definition page did not show the edit buttons.');
        BrowserUtilities.waitForCSS('i.delete', 1, 'Extract definition page did not show the delete buttons.');
    });

});