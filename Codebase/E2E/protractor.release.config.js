var HtmlReporter = require('protractor-beautiful-reporter');
exports.config = {
  chromeDriver: 'node_modules/webdriver-manager/selenium/chromedriver_2.38.exe',
  firefoxPath: 'node_modules/webdriver-manager/selenium/geckodriver-v0.19.1.exe',
  directConnect: true,
  multiCapabilities: [
    { browserName: 'chrome' },
    //{ browserName: 'firefox' },
  ],
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 2500000
  },
  baseUrl: 'https://localhost/R360UI',
  specs: [ './tests/**/*.e2e.js' ],
  onPrepare: function() {
    // Add a nice HTML reporter that outputs to /results directory.
    jasmine.getEnv().addReporter(new HtmlReporter({
      baseDirectory: 'results'
    }).getJasmine2Reporter());
  }
};