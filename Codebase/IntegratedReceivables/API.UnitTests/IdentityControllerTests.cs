using Api.Controllers;
using Common.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace API.UnitTests
{
    [TestClass]
    public class IdentityControllerTests
    {
        private Mock<ILogger<IdentityController>> logger;
        private Mock<IConfiguration> configuration;
        private IdentityController controller;
        private Credentials credentials;

        [TestInitialize]
        public void Setup()
        {
            logger = new Mock<ILogger<IdentityController>>();
            configuration = new Mock<IConfiguration>();
            controller = new IdentityController(logger.Object, configuration.Object);
            credentials = new Credentials { Entity = "test", UserName = "UnitTestUser", Password = "Un1tTestP@$$w0rd" };
        }

        [TestMethod]
        public void Can_GetToken_WhenRequestIsRight()
        {
            //arrange
            configuration.Setup(c => c[It.IsAny<string>()]).Returns("hereliesmyunittestsecret");
            
                //act
            var result = controller.GetToken(credentials);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            var response = result as OkObjectResult;
            Assert.IsTrue(response.StatusCode == StatusCodes.Status200OK);
            Assert.IsInstanceOfType(response.Value, typeof(IdentityResponse));

            var identityResponse = response.Value as IdentityResponse;
            Assert.IsNotNull(identityResponse.Token);
        }

        [TestMethod]
        public void Will_ReturnErrors_WhenRequestIsWrong()
        {
            //arrange
            configuration.Setup(c => c[It.IsAny<string>()]).Returns("hereliesmyunittestsecret");

            //act
            var result = controller.GetToken(null);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var response = result as BadRequestObjectResult;
            Assert.IsTrue(response.StatusCode == StatusCodes.Status400BadRequest);

            Assert.IsInstanceOfType(response.Value, typeof(ErrorResponse));

            var errorResponse = response.Value as ErrorResponse;
            Assert.IsNotNull(errorResponse.Errors);

        }

        [TestMethod]
        public void Will_ReturnErrors_WhenServerSideIssueIsEncountered()
        {
            //arrange
            configuration.Setup(c => c[It.IsAny<string>()]).Throws(new System.ArgumentException());

            //act
            var result = controller.GetToken(credentials);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ObjectResult));

            var response = result as ObjectResult;
            Assert.IsTrue(response.StatusCode == StatusCodes.Status500InternalServerError);

            Assert.IsInstanceOfType(response.Value, typeof(ErrorResponse));

            var errorResponse = response.Value as ErrorResponse;
            Assert.IsNotNull(errorResponse.Errors);

        }
    }
}
