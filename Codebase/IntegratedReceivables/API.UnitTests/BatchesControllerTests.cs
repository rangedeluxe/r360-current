﻿using Api.Controllers;
using Api.DataProviders;
using Common.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace API.UnitTests
{
    [TestClass]
    public class BatchesControllerTests
    {
        private Mock<ILogger<BatchesController>> logger;
        private Mock<IBatchDataProvider> batchDataProvider;
        private BatchesController controller;
        private BatchRequest batchRequest;
        private Mock<IUrlHelper> urlHelper;
        private string baseUrl = "https://unittests/integratedreceivables/api/batches";

        [TestInitialize]
        public void Setup()
        {
            logger = new Mock<ILogger<BatchesController>>();
            batchDataProvider = new Mock<IBatchDataProvider>();
            urlHelper = new Mock<IUrlHelper>();
            
            controller = new BatchesController(logger.Object, batchDataProvider.Object);
            controller.Url = urlHelper.Object;
            batchRequest = new BatchRequest { BankId = 123, WorkGroupId = 987, StartDate = new DateTime(2018, 07, 17), EndDate = new DateTime(2018, 07, 18) };
        }

        [TestMethod]
        public void Can_SearchForBatches_WhenRequestIsRight()
        {
            //arrange
            var dataProviderCalled = false;
            var batchSearchUrl = $"{baseUrl}/search";
            urlHelper.Setup(x => x.Link("BatchSearch", null)).Returns(batchSearchUrl);
            batchDataProvider.Setup(x => x.GetBatches(It.IsAny<BatchRequest>())).Returns(GetBatchResults()).Callback(() => { dataProviderCalled = true; });

            //act
            var result = controller.Post(batchRequest);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            var response = result as OkObjectResult;
            Assert.IsTrue(response.StatusCode == StatusCodes.Status200OK);
            Assert.IsInstanceOfType(response.Value, typeof(BatchSearchResult));

            var batchSearchResult = response.Value as BatchSearchResult;
            Assert.IsNotNull(batchSearchResult);
            Assert.IsNotNull(batchSearchResult.Batches);
            Assert.IsNotNull(batchSearchResult.Metadata);
            Assert.AreEqual(batchSearchUrl, batchSearchResult.Metadata.Resource);
            Assert.AreEqual(1, batchSearchResult.SearchResults);
            Assert.IsNotNull(batchSearchResult.Batches.First().Metadata);
            Assert.IsTrue(dataProviderCalled);
        }

        [TestMethod]
        public void Will_ReturnErrors_WhenTheBatchSearchRequestIsWrong()
        {
            //arrange

            //act
            var result = controller.Post(null);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var response = result as BadRequestObjectResult;
            Assert.IsTrue(response.StatusCode == StatusCodes.Status400BadRequest);

            Assert.IsInstanceOfType(response.Value, typeof(ErrorResponse));

            var errorResponse = response.Value as ErrorResponse;
            Assert.IsNotNull(errorResponse.Errors);
        }

        [TestMethod]
        public void Will_ReturnBadRequest_WhenCallingBatchSearchWithInvalidDateRange()
        {
            //arrange
            var expectedErrorMessage = "Must provide valid date range in search";
            batchRequest.StartDate = new DateTime(2018, 07, 18);//start date is greater than the end date
            batchRequest.EndDate = new DateTime(2018, 07, 17);

            //act
            var result = controller.Post(batchRequest);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var response = result as BadRequestObjectResult;
            Assert.IsTrue(response.StatusCode == StatusCodes.Status400BadRequest);

            Assert.IsInstanceOfType(response.Value, typeof(ErrorResponse));

            var errorResponse = response.Value as ErrorResponse;
            Assert.IsNotNull(errorResponse.Errors);
            Assert.IsNotNull(errorResponse.Errors.First());
            Assert.AreEqual(expectedErrorMessage, errorResponse.Errors.First());
        }

        [TestMethod]
        public void Will_ReturnErrors_WhenBatchSearchEncountersServerSideIssues()
        {
            //arrange
            batchDataProvider.Setup(x => x.GetBatches(It.IsAny<BatchRequest>())).Throws(new UnauthorizedAccessException());

            //act
            var result = controller.Post(batchRequest);

            //assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ObjectResult));

            var response = result as ObjectResult;
            Assert.IsTrue(response.StatusCode == StatusCodes.Status500InternalServerError);

            Assert.IsInstanceOfType(response.Value, typeof(ErrorResponse));

            var errorResponse = response.Value as ErrorResponse;
            Assert.IsNotNull(errorResponse.Errors);

        }

        internal IList<BatchResult> GetBatchResults()
        {
            return new List<BatchResult>
            {
                new BatchResult { BankId = 1, WorkgroupId = 1, BankName = "Unit Test Bank", WorkgroupName = "Unit Test Client", BatchId = 1}
            };
        }
    }
}
