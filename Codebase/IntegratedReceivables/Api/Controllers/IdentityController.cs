﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Common.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdentityController : ControllerBase
    {
        private readonly ILogger<IdentityController> logger;
        private readonly IConfiguration configuration;
        public IdentityController(ILogger<IdentityController> loggerContext, IConfiguration configurationContext)
        {
            logger = loggerContext;
            configuration = configurationContext;
        }

        [HttpPost ("token", Name = "GetToken")]
        public IActionResult GetToken([FromForm] Credentials credentials)
        {
            var errors = new List<string>();
            if (credentials == null)
                credentials = new Credentials { };

            if (string.IsNullOrEmpty(credentials.UserName))
                errors.Add("Must provide user name");

            if (string.IsNullOrEmpty(credentials.Password))
                errors.Add("Must provide password");

            if (errors.Any())
                return BadRequest(new ErrorResponse { Errors = errors });

            try
            {
                var token = GenerateToken(credentials);
                return Ok(new IdentityResponse { Token = token } );
            }
            catch(Exception ex)
            {
                var message = $"{nameof(IdentityController)} encountered an error";
                logger.LogError(message, ex.Message);
                return new ObjectResult(new ErrorResponse { Errors = new List<string> { message } }) { StatusCode = StatusCodes.Status500InternalServerError };
            }
            
        }

        private string GenerateToken(Credentials credentials)
        {
            var signingKey = configuration["identity:signingKey"];
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signingKey));

            var signing = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, credentials.UserName),
            };

            var token = new JwtSecurityToken
            (
                issuer: "Deluxe",
                audience: "http://www.deluxe.com",
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(30),
                signingCredentials: signing
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}