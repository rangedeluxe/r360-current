﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.DataProviders;
using Common.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BatchesController : ControllerBase
    {
        private readonly ILogger<BatchesController> logger;
        private readonly IBatchDataProvider batchDataProvider;

        public BatchesController(ILogger<BatchesController> loggerContext, IBatchDataProvider batchDataProviderContext)
        {
            logger = loggerContext;
            batchDataProvider = batchDataProviderContext;
        }

        [HttpGet("{batchId:int}", Name = "GetBatch")]
        public IActionResult GetBatch(int batchId)
        {
            return new JsonResult("Your call was successful.  Unfortunately, this endpoint is still under construction.")
            {
                StatusCode = StatusCodes.Status501NotImplemented
            };
        }

        //[HttpGet(Name = "BatchSearch")]
        //[Route("search")]
        //public IActionResult Get([FromQuery] BatchRequest requestContext)
        //{
        //    return BatchSearch(requestContext);
        //}

       
        [HttpPost("search", Name = "BatchSearch")]
        public IActionResult Post([FromForm]BatchRequest requestContext)
        {
            return BatchSearch(requestContext);
        }

        [NonAction]
        protected IActionResult BatchSearch(BatchRequest requestContext)
        {
            var errors = new List<string>();

            if (requestContext == null)
                requestContext = new BatchRequest { };

            if (requestContext.BankId == null)
                errors.Add("Must provide bank in search");

            if (requestContext.WorkGroupId == null)
                errors.Add("Must provide workgroup in search");

            if (requestContext.StartDate == null)
                errors.Add("Must provide start date in search");

            if (requestContext.EndDate == null)
                errors.Add("Must provide end date in search");

            if (requestContext.StartDate > requestContext.EndDate)
                errors.Add("Must provide valid date range in search");

            if (errors.Any())
                return BadRequest(new ErrorResponse { Errors = errors });

            try
            {
                var batches = batchDataProvider.GetBatches(requestContext);
               
                batches.ToList().ForEach(batch =>
                {
                    batch.Metadata = new MetaData
                    {
                        Links = new List<Link>
                        {
                            new Link{ Action = "GET", Relationship = "get-batch", Resource = this.Url.Link("GetBatch", new { batchId = batch.BatchId}) },
                        }
                    };
                });

                var response = new BatchSearchResult
                {
                    SearchResults = batches.Count(),
                    Metadata = new Link
                    {
                        Action = "POST",
                        Relationship = "self",
                        Resource = this.Url.Link("BatchSearch", null)
                    },
                    Batches = batches
                };
                return Ok(response);
            }
            catch (Exception exception)
            {
                var message = $"{nameof(BatchSearch)} encountered an error";
                logger.LogError(message, exception.Message);
                return new ObjectResult(new ErrorResponse { Errors = new List<string> { message } }) { StatusCode = StatusCodes.Status500InternalServerError };
            }

        }
    }
}