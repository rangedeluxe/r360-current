﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.DataProviders
{
    public interface IBatchDataProvider
    {
        IEnumerable<BatchResult> GetBatches(BatchRequest requestContext);
    }
}
