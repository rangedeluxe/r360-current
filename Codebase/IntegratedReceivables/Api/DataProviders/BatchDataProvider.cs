﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Common.Models;
using Common;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Api.DataProviders
{
    public class BatchDataProvider : IBatchDataProvider
    {
        private readonly ILogger<BatchDataProvider> logger;
        private readonly IConfiguration configuration;

        public BatchDataProvider(ILogger<BatchDataProvider> loggerContext, IConfiguration configurationContext)
        {
            logger = loggerContext;
            configuration = configurationContext;
        }
        public IEnumerable<BatchResult> GetBatches(BatchRequest requestContext)
        {
            try
            {
                var connectionString = configuration["database:connectionstring"].ToDecryptedString();
                using (IDbConnection db = new SqlConnection(connectionString))
                {
                    int startDepositDate;
                    int endDepositDate;

                    int.TryParse(requestContext.StartDate.Value.ToString("yyyyMMdd"), out startDepositDate);
                    int.TryParse(requestContext.EndDate.Value.ToString("yyyyMMdd"), out endDepositDate);

                    var batches = db.Query<Batch>(
                            "RecHubAPI.usp_factBatchSummary_Get",
                            new
                            {
                                @parmBankId = requestContext.BankId,
                                @parmWorkgroupId = requestContext.WorkGroupId,
                                @parmStartDepositDate = startDepositDate,
                                @parmEndDepositDate = endDepositDate
                            },
                            commandType: CommandType.StoredProcedure);
                    return batches.ToBatchResults();
                }
            }
            catch (Exception e)
            {
                logger.LogError($"{nameof(GetBatches)} : Error: {e.Message}");
                throw;
            }
        }
    }
}
