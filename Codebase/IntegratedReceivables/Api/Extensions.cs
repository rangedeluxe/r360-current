﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api
{
    public static class Extensions
    {
        public static string ToDecryptedString(this string value)
        {
            var crypto = new DpapiCrypto.CryptoDpapi();
            var response = crypto.Decrypt(value);
            if (response.Status == DpapiCrypto.Dto.DpapiStatusCode.SUCCESS)
                return response.ReturnedString;

            return value;
        }
    }
}
