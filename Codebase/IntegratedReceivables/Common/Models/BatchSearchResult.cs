﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class BatchSearchResult
    {
        public int SearchResults { get; set; }
        public Link Metadata { get; set; }
        public IEnumerable<BatchResult> Batches { get; set; }
    }
}
