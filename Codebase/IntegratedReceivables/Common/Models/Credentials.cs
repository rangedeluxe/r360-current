﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class Credentials
    {
        public string Entity { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
