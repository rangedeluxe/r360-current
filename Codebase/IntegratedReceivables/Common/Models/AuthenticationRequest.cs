﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class AuthenticationRequest
    {
        public Credentials Credentials { get; set; }
    }
}
