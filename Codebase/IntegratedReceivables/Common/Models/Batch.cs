﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class Batch
    {
        public DateTime DepositDate { get; set; }
        public DateTime ProcessingDate { get; set; }
        public int BatchId { get; set; }
        public int SourceBatchId { get; set; }
        public string BatchPaymentType { get; set; }
        public string BatchPaymentSource { get; set; }
        public int BankId { get; set; }
        public string BankName { get; set; }
        public int BatchNumber { get; set; }
        public int ClientAccountId { get; set; }
        public string ClientLongName { get; set; }
        public string ClientShortName { get; set; }
        public int CheckCount { get; set; }
        public int StubCount { get; set; }
        public int ScannedCheckCount { get; set; }
        public int DocumentCount { get; set; }
        public int BatchSourceKey { get; set; }
    }
}
