﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class MetaData
    {
        public IEnumerable<Link> Links { get; set; }
    }
}
