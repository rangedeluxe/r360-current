﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class Link
    {
        [JsonProperty(PropertyName = "rel")]
        public string Relationship { get; set; }

        [JsonProperty(PropertyName = "href")]
        public string Resource { get; set; }

        public string Action { get; set; }
    }
}
