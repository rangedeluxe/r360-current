﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Common.Models
{
    [ExcludeFromCodeCoverage]
    public class IdentityResponse
    {
        public string Token { get; set; }
    }
}
