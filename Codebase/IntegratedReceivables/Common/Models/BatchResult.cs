﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class BatchResult
    {
        public DateTime DepositDate { get; set; }
        public DateTime ProcessingDate { get; set; }
        public int BatchId { get; set; }
        public int SourceBatchId { get; set; }
        public string BatchPaymentType { get; set; }
        public string BatchPaymentSource { get; set; }
        public int BankId { get; set; }
        public string BankName { get; set; }
        public int BatchNumber { get; set; }
        public int WorkgroupId { get; set; }
        public string WorkgroupName { get; set; }
        public int PaymentCount { get; set; }
        public int DocumentCount { get; set; }
        public int StubCount { get; set; }
        public MetaData Metadata { get; set; }
    }
}
