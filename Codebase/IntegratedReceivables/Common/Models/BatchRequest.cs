﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class BatchRequest
    {
        public int? BankId { get; set; }
        public int? WorkGroupId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
