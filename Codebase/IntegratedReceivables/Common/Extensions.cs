﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Common
{
    public static class Extensions
    {
        public static IEnumerable<BatchResult> ToBatchResults(this IEnumerable<Batch> batchList)
        {
            var batchResults = new List<BatchResult>();

            batchList?.ToList().ForEach(batch =>
            {
                batchResults.Add(ToBatchResult(batch));
            });

            return batchResults;
        }

        public static BatchResult ToBatchResult(this Batch batch)
        {
            if (batch == null)
                throw new ArgumentNullException();

            return new BatchResult
            {
                BankId = batch.BankId,
                BatchId = batch.BatchId,
                BatchNumber = batch.BatchNumber,
                BatchPaymentSource = batch.BatchPaymentSource,
                BatchPaymentType = batch.BatchPaymentType,
                DepositDate = batch.DepositDate,
                DocumentCount = batch.DocumentCount,
                PaymentCount = batch.CheckCount,
                ProcessingDate = batch.ProcessingDate,
                WorkgroupId = batch.ClientAccountId,
                SourceBatchId = batch.SourceBatchId,
                BankName = batch.BankName,
                StubCount = batch.StubCount,
                WorkgroupName = batch.ClientLongName
            };
        }
    }
}
