﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseIntegrationTestHelpers;

namespace SSISLoggingDatabaseIntegrationTestHelpers
{
    public class Dbo : SsisLoggingDatabaseSchema
    {
        public Dbo(IntegrationDatabase database, DateTime runDate) : base(database, runDate)
        {
            MakeTable("BatchLog", IgnorePartitions);
            MakeTable("Package", IgnorePartitions);
            MakeTable("PackageVersion", IgnorePartitions);
            MakeTable("PackageLog", IgnorePartitions);
            MakeTable("PackageErrorLog", IgnorePartitions);
            MakeTable("PackageTaskLog", IgnorePartitions);
            MakeTable("PackageVariableLog", IgnorePartitions);
            MakeStoredProcedure("LogPackageEnd");
            MakeStoredProcedure("LogPackageError");
            MakeStoredProcedure("LogPackageStart");
            MakeStoredProcedure("LogTaskPostExecute");
            MakeStoredProcedure("LogTaskPreExecute");
            MakeStoredProcedure("LogVariableValueChanged");
        }
    }
}
