﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseIntegrationTestHelpers;

namespace SSISLoggingDatabaseIntegrationTestHelpers
{
    public class SsisLoggingDatabaseSchema
    {
        protected string SchemaName { get; }

        protected IntegrationDatabase Database { get; }

        protected DateTime RunDate { get; }

        public SsisLoggingDatabaseSchema(IntegrationDatabase database, DateTime runDate)
        {
            //The class names match the schema name
            SchemaName = GetType().Name;
            Database = database;
            RunDate = runDate;
        }

        public void MakeTable(string tableName, Func<string, string> filterFileContent = null)
        {
            Database.MakeTable(SchemaName, tableName, filterFileContent);
        }

        public void MakeStoredProcedure(string storedProcedureName)
        {
            Database.MakeStoredProcedure(SchemaName, storedProcedureName);
        }

        public string IgnorePartitions(string sql)
        {
            return sql.Replace("ON WFSSSISLogging(CreationDateKey)", "");
        }
    }
}
