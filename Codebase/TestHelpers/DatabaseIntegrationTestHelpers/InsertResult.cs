﻿using System;

namespace DatabaseIntegrationTestHelpers
{
    public struct InsertResult
    {
        public InsertResult(int? insertedId, string origin)
        {
            InsertedId = insertedId;
            Origin = origin;
        }

        private int? InsertedId { get; }
        private string Origin { get; }

        /// <summary>
        /// Returns the ID of the record that was inserted by the query.
        /// If no record was inserted, throws a descriptive exception.
        /// </summary>
        /// <returns>The ID that was inserted into the database (SCOPE_IDENTITY).</returns>
        public int GetInsertedId()
        {
            if (InsertedId.HasValue)
                return InsertedId.Value;
            throw new InvalidOperationException($"{Origin} did not return an inserted ID");
        }
    }
}