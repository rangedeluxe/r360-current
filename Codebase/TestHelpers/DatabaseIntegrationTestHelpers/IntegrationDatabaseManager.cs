﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Dapper;

namespace DatabaseIntegrationTestHelpers
{
    /// <summary>
    /// Creates and drops integration databases.
    /// </summary>
    public static class IntegrationDatabaseManager
    {
        /// <summary>
        /// Databases older than this will be dropped (though only by the client
        /// machine that created them).
        /// </summary>
#if DEBUG
        private static readonly TimeSpan DatabaseCleanupAge = TimeSpan.FromMinutes(1);
#else
        private static readonly TimeSpan DatabaseCleanupAge = TimeSpan.FromHours(1);
#endif

        public static void CreateDatabase(IntegrationDatabaseIdentifier databaseIdentifier)
        {
            using (var connection = IntegrationDatabaseConnection.OpenMasterDatabase())
            {
                connection.Execute($"CREATE DATABASE {databaseIdentifier.QuotedName}");
            }
        }
        private static void DropDatabase(IntegrationDatabaseIdentifier databaseIdentifier)
        {
            using (var connection = IntegrationDatabaseConnection.OpenMasterDatabase())
            {
                // Close other users' existing connections before dropping the database.
                connection.Execute(
                    $"ALTER DATABASE {databaseIdentifier.QuotedName} SET SINGLE_USER WITH ROLLBACK IMMEDIATE;" +
                    $"DROP DATABASE {databaseIdentifier.QuotedName};");
            }
        }
        public static void DropMyOldDatabases()
        {
            var myOldDatabases =
                from databaseName in ListIntegrationDatabases()
                where databaseName.IsMine && databaseName.Age >= DatabaseCleanupAge
                select databaseName;

            foreach (var databaseName in myOldDatabases)
            {
                try
                {
                    DropDatabase(databaseName);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine($"Error dropping database {databaseName}: {ex.Message}");
                    // Continue trying to clean up other databases
                }
            }
        }
        private static IEnumerable<IntegrationDatabaseIdentifier> ListIntegrationDatabases()
        {
            using (var connection = IntegrationDatabaseConnection.OpenMasterDatabase())
            {
                var unquotedNames = connection.Query<string>(
                    "SELECT name FROM sys.databases WHERE name LIKE @Pattern",
                    new {Pattern = IntegrationDatabaseIdentifier.LikePattern});

                return
                    from unquotedName in unquotedNames
                    let databaseName = IntegrationDatabaseIdentifier.TryParseUnquotedName(unquotedName)
                    where databaseName != null
                    select databaseName;
            }
        }
    }
}