﻿using System;
using System.Data.SqlClient;
using WFS.RecHub.Common.Crypto;

namespace DatabaseIntegrationTestHelpers
{
    public static class IntegrationDatabaseConnection
    {
        private const string Server = "RecHubDevDb02.qalabs.nwk";
        /// <summary>
        /// <para>
        ///     SQL Server user name. This user needs the "Create any database" permission
        ///     (SSMS > right-click the server > Permissions page, select the login, find
        ///     the permission in the bottom list, and check "Grant").
        /// </para>
        /// <para>
        ///     The user could instead be a member of the "sysadmin" or "dbcreator" roles,
        ///     but both roles allow the user to drop any databases as well, so that's not
        ///     recommended for credentials you'll be checking into version control.
        /// </para>
        /// </summary>
        private const string UserName = "IntegrationTests";
        private const string Password = "IntegrationTests";

        private static string Encrypt(string value)
        {
            var crypto = new cCrypto3DES();
            string result;
            var success = crypto.Encrypt(value, out result);
            if (!success)
                throw new Exception("Unable to encrypt value");
            return result;
        }
        private static string GetConnectionString(string databaseName, string userName, string password)
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = Server,
                InitialCatalog = databaseName,
                UserID = userName,
                Password = password,
            };
            return connectionStringBuilder.ConnectionString;
        }
        public static string GetEncryptedConnectionString(IntegrationDatabaseIdentifier databaseIdentifier)
        {
            var connectionString = GetConnectionString(databaseIdentifier.UnquotedName,
                Encrypt(UserName), Encrypt(Password));
            // ConnectionStringBuilder "helpfully" quotes our Base64-encoded username
            // and password. Strip out those quotes, because cDatabase will choke on them.
            return connectionString.Replace("\"", "");
        }
        private static SqlConnection Open(string databaseName)
        {
            var connectionString = GetConnectionString(databaseName, UserName, Password);
            var connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }
        public static SqlConnection Open(IntegrationDatabaseIdentifier databaseIdentifier)
        {
            return Open(databaseIdentifier.UnquotedName);
        }
        public static SqlConnection OpenMasterDatabase()
        {
            return Open("master");
        }
    }
}