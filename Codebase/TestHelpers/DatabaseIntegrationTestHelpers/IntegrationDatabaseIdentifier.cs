﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace DatabaseIntegrationTestHelpers
{
    /// <summary>
    /// Integration-test database names contain information like the computer name that
    /// created the database, and when it was created. This class holds that information
    /// and can convert to and from the database name string.
    /// </summary>
    [DebuggerDisplay("{" + nameof(QuotedName) + "}")]
    public class IntegrationDatabaseIdentifier
    {
        public const string LikePattern = "IntegrationTests_%";

        private IntegrationDatabaseIdentifier(string machineName, DateTime dateTime, string uniquifier)
        {
            MachineName = machineName;
            DateTime = dateTime;
            Uniquifier = uniquifier;
        }

        public TimeSpan Age => DateTime.Now - DateTime;
        public DateTime DateTime { get; }
        public bool IsMine => MachineName.Equals(Environment.MachineName, StringComparison.InvariantCultureIgnoreCase);
        public string MachineName { get; }
        public string QuotedName => SqlUtilities.QuoteObjectName(UnquotedName);
        public string Uniquifier { get; }
        public string UnquotedName => $"IntegrationTests_{MachineName}_{DateTime:yyyyMMdd_HHmmss}_{Uniquifier}";

        public static IntegrationDatabaseIdentifier CreateNew()
        {
            return new IntegrationDatabaseIdentifier(
                machineName: Environment.MachineName,
                dateTime: DateTime.Now,
                uniquifier: Path.GetRandomFileName().Replace(".", ""));
        }
        public static IntegrationDatabaseIdentifier TryParseUnquotedName(string unquotedName)
        {
            const string pattern =
                @"^IntegrationTests_" +
                @"(?<machineName>[^_]+)_" +
                @"(?<year>\d{4})(?<month>\d{2})(?<day>\d{2})_(?<hour>\d{2})(?<minute>\d{2})(?<second>\d{2})_" +
                @"(?<uniquifier>.*)";

            var match = Regex.Match(unquotedName, pattern);
            if (match.Success)
            {
                var machineName = match.Groups["machineName"].Value;
                var dateTime = new DateTime(
                    year: int.Parse(match.Groups["year"].Value),
                    month: int.Parse(match.Groups["month"].Value),
                    day: int.Parse(match.Groups["day"].Value),
                    hour: int.Parse(match.Groups["hour"].Value),
                    minute: int.Parse(match.Groups["minute"].Value),
                    second: int.Parse(match.Groups["second"].Value));
                var uniquifier = match.Groups["uniquifier"].Value;

                return new IntegrationDatabaseIdentifier(machineName, dateTime, uniquifier);
            }

            return null;
        }
    }
}