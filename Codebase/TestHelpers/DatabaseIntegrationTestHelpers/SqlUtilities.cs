﻿namespace DatabaseIntegrationTestHelpers
{
    public static class SqlUtilities
    {
        /// <summary>
        /// Quotes an object name (such as a database name, table name, etc.) by
        /// wrapping it in square brackets, and escaping as needed.
        /// </summary>
        /// <param name="unquotedName"></param>
        /// <returns></returns>
        public static string QuoteObjectName(string unquotedName)
        {
            return $"[{unquotedName.Replace("]", "]]")}]";
        }
    }
}