﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseIntegrationTestHelpers
{
    public class IntegrationDatabaseSsis : IDisposable
    {
        private readonly string _diskSsisPackagePath;
        private readonly string _scriptFolder;
        private readonly string _databaseName;

        public string DiskSsisPackagePath => _diskSsisPackagePath;
        private void EmptyFolder(string folderName)
        {
            var folder = new DirectoryInfo(folderName);

            foreach (FileInfo file in folder.GetFiles())
                file.Delete();

            foreach (DirectoryInfo subFolder in folder.GetDirectories())
                subFolder.Delete(true);
        }

        private void RemoveOldSsisFolders()
        {
            var packagePath = Path.GetDirectoryName(_diskSsisPackagePath);
            var search = _databaseName.Substring(0, _databaseName.IndexOf('_', _databaseName.IndexOf('_') + 1));
            search += "*";
            var folderList = Directory.GetDirectories(packagePath, search);
            foreach (var folderName in folderList)
            {
                var subFolder = new DirectoryInfo(folderName);
                if (subFolder.CreationTime < DateTime.Now.AddDays(-1))
                {
                    EmptyFolder(folderName);
                    subFolder.Delete(true);
                }
            }
        }

        public IntegrationDatabaseSsis(string DataSource, string Database, string scriptFolder)
        {
            _scriptFolder = scriptFolder;
            _diskSsisPackagePath = Path.Combine($"\\\\{DataSource}", "d$", "SSISPackages", Database);
            _databaseName = Database;
            Directory.CreateDirectory(_diskSsisPackagePath);
            RemoveOldSsisFolders();
        }

        public void Dispose()
        {
            EmptyFolder(_diskSsisPackagePath);
            RemoveOldSsisFolders();
        }

        public void RemoveDiskSsisPackageFolder(string package)
        {
            EmptyFolder(Path.Combine(_diskSsisPackagePath, package));
        }

        public void CreateDiskSsisPackageFolder(string package)
        {
            var folder = Path.Combine(_diskSsisPackagePath, package);

            Directory.CreateDirectory(folder);
        }

        public void CopyDiskSsisPackage(string packageFolder, string packageName)
        {
            var source = Path.Combine(_scriptFolder, "SSIS", packageName.Split('_')[0], packageName.Split('_')[0],
                $"{packageName}.dtsx");
            var target = Path.Combine(_diskSsisPackagePath, packageFolder, $"{packageName}.dtsx");

            File.Copy(source,target);
        }
    }
}
