﻿using System.Diagnostics;

namespace DatabaseIntegrationTestHelpers
{
    [DebuggerDisplay("{" + nameof(QuotedName) + "}")]
    public class DatabaseObjectName
    {
        public DatabaseObjectName(string schemaName, string objectName)
        {
            ObjectName = objectName;
            SchemaName = schemaName;
        }

        public string SchemaName { get; }
        public string ObjectName { get; }
        public string QuotedName =>
            $"{SqlUtilities.QuoteObjectName(SchemaName)}.{SqlUtilities.QuoteObjectName(ObjectName)}";
    }
}