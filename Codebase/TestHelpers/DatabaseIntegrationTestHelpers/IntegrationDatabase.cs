﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using Dapper;
using WFS.RecHub.Common.Database;

namespace DatabaseIntegrationTestHelpers
{
    public class IntegrationDatabase : IDisposable
    {
        private readonly Lazy<cDatabase> _lazyCDatabase;
        private readonly string _rootDatabaseScriptsDirectory;
        private readonly HashSet<string> _schemasCreated = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
        private readonly IList<DatabaseObjectName> _tablesCreated = new List<DatabaseObjectName>();

        public string DatabaseScriptsFolder => _rootDatabaseScriptsDirectory;

        private IntegrationDatabase(SqlConnection connection, IntegrationDatabaseIdentifier databaseIdentifier,
            string rootDatabaseScriptsDirectory)
        {
            _rootDatabaseScriptsDirectory = rootDatabaseScriptsDirectory;
            Connection = connection;

            _lazyCDatabase = new Lazy<cDatabase>(() =>
            {
                var connectionString =
                    IntegrationDatabaseConnection.GetEncryptedConnectionString(databaseIdentifier);
                var cDatabase = new cDatabase(connectionString, debugLevel: 2);
                cDatabase.outputMessage += (message, src, messageType, messageImportance) => Console.WriteLine(message);
                return cDatabase;
            });
            _schemasCreated.Add("dbo");
        }

        public cDatabase CDatabase => _lazyCDatabase.Value;
        public SqlConnection Connection { get; }

        public static IntegrationDatabase CreateNew(string databaseSubFolder = null)
        {
            IntegrationDatabaseManager.DropMyOldDatabases();

            var databaseIdentifier = IntegrationDatabaseIdentifier.CreateNew();
            IntegrationDatabaseManager.CreateDatabase(databaseIdentifier);
            var connection = IntegrationDatabaseConnection.Open(databaseIdentifier);

            var rootDatabaseScriptsDirectory = DatabaseDirectories.GetRootDatabaseScriptsDirectory();

            if (databaseSubFolder != null)
                rootDatabaseScriptsDirectory = Path.Combine(rootDatabaseScriptsDirectory, databaseSubFolder);

            return new IntegrationDatabase(connection, databaseIdentifier, rootDatabaseScriptsDirectory);
        }

        public void ClearTables()
        {
            if (!_tablesCreated.Any())
                return;

            // Clear the last-added tables first, in hopes of avoiding foreign-key errors.
            var deleteStatements = _tablesCreated.Reverse().Select(table => $"DELETE FROM {table.QuotedName};");
            var deleteScript = string.Join("\r\n", deleteStatements);
            Connection.Execute(deleteScript);
        }
        private void CreateSchema(string schemaName)
        {
            Connection.Execute($"CREATE SCHEMA {SqlUtilities.QuoteObjectName(schemaName)}");
            _schemasCreated.Add(schemaName);
        }
        private void CreateSchemaIfNeeded(string schemaName)
        {
            if (!_schemasCreated.Contains(schemaName))
                CreateSchema(schemaName);
        }
        public void Dispose()
        {
            Connection.Dispose();
            if (_lazyCDatabase.IsValueCreated)
                _lazyCDatabase.Value.Dispose();
        }
        private void ExecuteScriptFile(string schemaName, string scriptType, string name,
            Func<string, string> filterFileContent = null)
        {
            CreateSchemaIfNeeded(schemaName);
            var path = Path.Combine(_rootDatabaseScriptsDirectory, schemaName, scriptType, $"{name}.sql");
            var sqlScript = File.ReadAllText(path);
            if (filterFileContent != null)
                sqlScript = filterFileContent(sqlScript);

            try
            {
                Connection.ExecuteSqlScript(sqlScript);
            }
            catch (Exception ex)
            {
                throw new Exception($"Error running SQL script {path}: {ex.Message}", ex);
            }
        }
        public InsertResult Insert(string tableName, object values, bool setIdentityInsert = false)
        {
            if (values == null)
                values = new object();

            var properties = values.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var propertyNames = properties.Select(property => property.Name).ToList();
            if (!propertyNames.Any())
            {
                if (setIdentityInsert)
                    throw new InvalidOperationException($"{nameof(setIdentityInsert)} requires columns to insert");

                var id = Connection.ExecuteScalar<int?>(
                    $"INSERT INTO {tableName} DEFAULT VALUES; SELECT SCOPE_IDENTITY();");
                return new InsertResult(id, $"Insert into {tableName}");
            }
            else
            {
                var fieldList = string.Join(", ", propertyNames);
                var valueList = string.Join(", ", propertyNames.Select(name => $"@{name}"));
                var sql = $"INSERT INTO {tableName} ({fieldList}) VALUES ({valueList}); SELECT SCOPE_IDENTITY();";
                if (setIdentityInsert)
                {
                    var quotedDatabaseName = SqlUtilities.QuoteObjectName(Connection.Database);
                    var prefix = $"SET IDENTITY_INSERT {quotedDatabaseName}.{tableName} ON;";
                    var suffix = $"SET IDENTITY_INSERT {quotedDatabaseName}.{tableName} OFF;";
                    sql = $"{prefix}\r\n{sql}\r\n{suffix}";
                }

                var id = Connection.ExecuteScalar<int?>(sql, values);
                return new InsertResult(id, $"Insert into {tableName}");
            }
        }

        public SqlParameter BuildParameter(string parameterName, SqlDbType type, object value, ParameterDirection direction = ParameterDirection.Input)
        {

            SqlParameter objParameter = new SqlParameter();
            int intSize = 0;
            int i;

            objParameter.ParameterName = parameterName;
            objParameter.SqlDbType = type;
            objParameter.Value = value;
            objParameter.Direction = direction;

            if (type == SqlDbType.Xml)
            {
                if (value == null)
                {
                    objParameter.Size = 4096;
                }
                else
                {
                    i = 10;
                    intSize = 2 ^ i;
                    while (intSize < value.ToString().Length)
                    {
                        ++i;
                        intSize = 2 ^ i;
                    }
                    objParameter.Size = intSize;
                }
            }
            else if (type == SqlDbType.VarChar)
            {
                objParameter.Size = 1024;
            }

            return (objParameter);
        }

        public bool ExecuteProcedure(string procName, SqlParameter[] parms)
        {
            //SqlParameter[] objOutParms;
            return (ExecuteProcedure(procName, parms, out SqlParameter[] objOutParms));
        }

        public bool ExecuteProcedure(string procName, SqlParameter[] parms, out SqlParameter[] outParms)
        {

            SqlCommand cmd = null;
            SqlParameter[] returnParms;
            int intTries = 0;
            bool bolContinue;
            bool bolRetVal;

            //_LastException = null;

            try
            {

                cmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = procName,
                    Connection = Connection
                };

                //if (_SQLTrans != null)
                //{
                //    cmd.Transaction = _SQLTrans;
                //}

                foreach (SqlParameter parm in parms)
                {
                    cmd.Parameters.Add(parm);
                }

                //OutputDatabaseMessage(cmd, "Executing Procedure : EXEC ");
                bolContinue = true;
                bolRetVal = false;

                while (bolContinue)
                {
                    ++intTries;
                    try
                    {
                        cmd.ExecuteNonQuery();
                        bolContinue = false;
                        bolRetVal = true;
                    }
                    catch (Exception ex)
                    {
                        //if (intTries > this.QueryRetryAttempts)
                        //{
                        //    onOutputMessage("An Error occurred while executing Procedure: " + procName, ""
                        //                    , DbMessageType.Error
                        //                    , DbMessageImportance.Essential);
                        //    onOutputError(ex);
                        throw new Exception($"An Error occurred while executing Procedure {procName}: {ex.Message}", ex);
                        bolContinue = false;
                        //}
                        //else
                        //{
                        //    onOutputMessage("An Error occurred while executing Procedure.  Attempting retry(" + intTries.ToString() + ")...", ""
                        //                    , DbMessageType.Warning
                        //                    , DbMessageImportance.Essential);
                        //}
                    }
                }

                returnParms = new SqlParameter[cmd.Parameters.Count];
                cmd.Parameters.CopyTo(returnParms, 0);
            }
            catch (Exception ex)
            {
                //onOutputMessage("An Error occurred while running stored procedure: " + procName,
                //    this.GetType().Name,
                //    DbMessageType.Error,
                //    DbMessageImportance.Essential);
                //onOutputError(ex);
                returnParms = null;
                bolRetVal = false;
                throw new Exception($"An Error occurred while executing Procedure {procName}: {ex.Message}", ex);
            }
            finally
            {

                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }

                //if (_SQLTrans == null && _Connection != null)
                //{
                //    TerminateConnection();
                //}
            }

            outParms = returnParms;

            return (bolRetVal);
        }

        public bool ExecuteProcedure(string procName, SqlParameter[] parms, out SqlParameter[] outParms,
            out DataTable datatable)
        {
            SqlCommand cmd = null;
            SqlParameter[] returnParms;
            var ds = new DataSet(); 

            int intTries = 0;
            bool bolContinue;
            bool bolRetVal;

            datatable = null;

            cmd = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = procName,
                Connection = Connection
            };

            var da = new SqlDataAdapter
            {
                SelectCommand = cmd
            };

            try
            {

                //if (_SQLTrans != null)
                //{
                //    cmd.Transaction = _SQLTrans;
                //}

                foreach (SqlParameter parm in parms)
                {
                    cmd.Parameters.Add(parm);
                }

                bolContinue = true;
                bolRetVal = false;

                while (bolContinue)
                {
                    ++intTries;
                    try
                    {
                        da.Fill(ds);
                        bolContinue = false;
                        bolRetVal = true;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"An Error occurred while executing Procedure {procName}: {ex.Message}", ex);
                    }
                }
                da.Dispose();
                da = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    datatable = ds.Tables[0];
                }
                else
                {
                    datatable = null;
                }
                returnParms = new SqlParameter[cmd.Parameters.Count];
                cmd.Parameters.CopyTo(returnParms, 0);

            }
            catch (Exception ex)
            {
                if (datatable != null)
                {
                    datatable.Dispose();
                    datatable = null;
                }
                returnParms = null;
                bolRetVal = false;
                throw new Exception($"An Error occurred while executing Procedure {procName}: {ex.Message}", ex);
            }
            finally
            {

                if (da != null)
                {
                    da.Dispose();
                    da = null;
                }

                if (ds != null)
                {
                    ds.Dispose();
                    ds = null;
                }

                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }
            }

            outParms = returnParms;

            return (bolRetVal);
        }

        public void MakeDataType(string schemaName, string dataTypeName)
        {
            ExecuteScriptFile(schemaName, "DataType", dataTypeName);
        }
        public void MakeStoredProcedure(string schemaName, string storedProcedureName)
        {
            ExecuteScriptFile(schemaName, "StoredProcedure", storedProcedureName);
        }
        public void MakeTable(string schemaName, string tableName, Func<string, string> filterFileContent = null)
        {
            ExecuteScriptFile(schemaName, "Table", tableName, filterFileContent);
            _tablesCreated.Add(new DatabaseObjectName(schemaName, tableName));
        }
        public void MakeView(string schemaName, string tableName)
        {
            ExecuteScriptFile(schemaName, "View", tableName);
        }
        public void MakeFunction(string schemaName, string functionName)
        {
            ExecuteScriptFile(schemaName, "Function", functionName);
        }

        public void MakeSequence(string schemaName, string sequenceName)
        {
            ExecuteScriptFile(schemaName, "Sequence", sequenceName);
        }
    }
}