﻿using System;
using System.Data;
using System.Globalization;
using Dapper;

namespace DatabaseIntegrationTestHelpers.R360
{
    public static class RecHubData
    {
        public static readonly DateTime DefaultCreationDate = new DateTime(2016, 1, 1);

        public static InsertResult InsertBatchSource(IntegrationDatabase database,
            string shortName = null, string longName = null)
        {
            return database.Insert("RecHubData.dimBatchSources",
                new
                {
                    ImportTypeKey = 0,
                    LongName = longName ?? "Batch source",
                    ShortName = shortName ?? "Batch source",
                });
        }
        public static void InsertDimBank(IntegrationDatabase database, int siteBankId, int entityId,
            DateTime? creationDate = null, string bankName = null, bool mostRecent = true)
        {
            database.Insert("RecHubData.dimBanks",
                new
                {
                    BankName = bankName ?? $"Bank {siteBankId}",
                    CreationDate = creationDate ?? DefaultCreationDate,
                    EntityId = entityId,
                    MostRecent = mostRecent,
                    SiteBankID = siteBankId,
                });
        }
        public static InsertResult InsertDimClientAccount(IntegrationDatabase database, int siteClientAccountId,
            int siteBankId, int siteCodeId, string shortName = null, string longName = null,
            int dataRetentionDays = 999, int imageRetentionDays = 999,
            DateTime? creationDate = null, int cutOff = 0, bool isActive = true, bool mostRecent = true)
        {
            return database.Insert("RecHubData.dimClientAccounts",
                new
                {
                    CutOff = cutOff,
                    CreationDate = creationDate ?? DefaultCreationDate,
                    DataRetentionDays = dataRetentionDays,
                    ImageRetentionDays = imageRetentionDays,
                    IsActive = isActive,
                    LongName = longName ?? $"Workgroup {siteClientAccountId} - Long Name",
                    MostRecent = mostRecent,
                    ShortName = shortName ?? $"Workgroup {siteClientAccountId}",
                    SiteBankID = siteBankId,
                    SiteClientAccountID = siteClientAccountId,
                    SiteCodeID = siteCodeId,
                    SiteOrganizationID = -1,
                });
        }
        public static void InsertDimDates(IntegrationDatabase database, DateTime date)
        {
            var calendar = new GregorianCalendar();
            var quarter = (date.Month - 1) / 3 + 1;
            database.Insert("RecHubData.dimDates",
                new
                {
                    DateKey = date.Year * 10000 + date.Month * 100 + date.Day,
                    CalendarDate = date.Date,
                    CalendarDayMonth = date.Day,
                    CalendarDayYear = date.DayOfYear,
                    CalendarDayWeek = (int) date.DayOfWeek + 1,
                    CalendarDayName = date.DayOfWeek.ToString(),
                    CalendarWeek = calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Sunday),
                    CalendarMonth = date.Month,
                    CalendarMonthName = date.Month.ToString(),
                    CalendarQuarter = quarter,
                    CalendarQuarterName = $"Q{quarter} {date.Year}",
                    CalendarYear = date.Year,
                });
        }
        public static void InsertDimSiteCode(IntegrationDatabase database, int siteCodeId,
            string shortName = null, string longName = null)
        {
            database.Insert("RecHubData.dimSiteCodes",
                new
                {
                    LongName = longName ?? $"Site Code {siteCodeId} - Long Name",
                    ShortName = shortName ?? $"Site Code {siteCodeId}",
                    SiteCodeID = siteCodeId,
                });
        }

        public static InsertResult InsertDimWorkgroupDataEntryColumn(IntegrationDatabase database,
            int batchSourceKey, string fieldName, string uiLabel = null, bool isRequired = false)
        {
            return database.Insert("RecHubData.dimWorkgroupDataEntryColumns",
                values: new
                {
                    SourceDisplayName = "",
                    SiteBankID = 0,
                    SiteClientAccountID = 0,
                    BatchSourceKey = batchSourceKey,
                    DataType = 0,
                    ScreenOrder = 0,
                    CreationDate = DefaultCreationDate,
                    FieldName = fieldName,
                    UILabel = uiLabel ?? $"{fieldName} - UI Label",
                    IsRequired = isRequired,
                });
        }
        public static InsertResult UspDimClientAccountsIns(IntegrationDatabase database,
            int siteClientAccountId, int siteBankId, int entityId, int userId, string userDisplayName = null,
            string shortName = null, string longName = null,
            string dataEntryFieldsXml = null, int? expectedErrorCode = null)
        {
            const string procName = "RecHubData.usp_DimClientAccounts_Ins";

            var p = new DynamicParameters(new
            {
                parmSiteBankId = siteBankId,
                parmSiteClientAccountId = siteClientAccountId,
                parmIsActive = true,
                parmDataRetentionDays = 999,
                parmImageRetentionDays = 999,
                parmShortName = shortName ?? $"Workgroup {siteClientAccountId}",
                parmLongName = longName ?? $"Workgroup {siteClientAccountId} - Long Name",
                parmFileGroup = (string) null,
                parmEntityId = entityId,
                parmViewingDays = 999,
                parmMaximumSearchDays = 999,
                parmCheckImageDisplayMode = 0,
                parmDocumentImageDisplayMode = 0,
                parmHoa = false,
                parmDisplayBatchId = true,
                parmInvoiceBalancingOption = 0,
                parmUserId = userId,
                parmUserDisplayName = userDisplayName,
                parmDdas = (string) null,
                parmDataEntryColumns = dataEntryFieldsXml,
                parmDeadLineDay = 0,
                parmRequiresInvoice = false,
                parmBusinessRules = (string) null,
                parmFIs = $"<FIs><ID>{entityId}</ID></FIs>",
            });
            // Out parameters:
            p.Add("parmClientAccountKey", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("parmErrorCode", dbType: DbType.Int32, direction: ParameterDirection.Output);

            database.Connection.Execute(procName, p, commandType: CommandType.StoredProcedure);

            var errorCode = p.Get<int?>("parmErrorCode");
            if (errorCode != expectedErrorCode)
            {
                var errorDescription = errorCode == 1 ? "Workgroup already exists" : "Unknown error";
                throw new InvalidOperationException(
                    $"{procName} returned error code {errorCode} ({errorDescription})");
            }

            return new InsertResult(p.Get<int>("parmClientAccountKey"), procName);
        }
        public static void UspDimClientAccountsUpd(IntegrationDatabase database, int clientAccountKey,
            int userId, string userDisplayName, int entityId, string shortName = null, string longName = null,
            string dataEntryFieldsXml = null)
        {
            const string procName = "RecHubData.usp_DimClientAccounts_Upd";

            var p = new DynamicParameters(new
            {
                parmClientAccountKey = clientAccountKey,
                parmIsActive = true,
                parmDataRetentionDays = 999,
                parmImageRetentionDays = 999,
                parmShortName = shortName ?? $"Workgroup {clientAccountKey}",
                parmLongName = longName ?? $"Workgroup {clientAccountKey} - Long Name",
                parmFileGroup = (string) null,
                parmEntityID = entityId,
                parmViewingDays = 999,
                parmMaximumSearchDays = 999,
                parmCheckImageDisplayMode = 0,
                parmDocumentImageDisplayMode = 0,
                parmHOA = false,
                parmDisplayBatchID = false,
                parmInvoiceBalancingOption = 0,
                parmUserID = userId,
                parmUserDisplayName = userDisplayName,
                parmDDAs = (string) null,
                parmDataEntryColumns = dataEntryFieldsXml,
                parmDeadLineDay = 1,
                parmRequiresInvoice = false,
                parmBusinessRules = (string) null,
                parmFIs = $"<FIs><ID>{entityId}</ID></FIs>",
                parmNewClientAccountKey = 0,
                parmRAAMNotificationRequried = false,
            });

            database.Connection.Execute(procName, p, commandType: CommandType.StoredProcedure);
        }
    }
}