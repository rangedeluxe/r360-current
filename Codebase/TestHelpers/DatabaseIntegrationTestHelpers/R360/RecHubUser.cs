﻿using System;

namespace DatabaseIntegrationTestHelpers.R360
{
    public static class RecHubUser
    {
        public static void InsertOlEntity(IntegrationDatabase database, int entityId)
        {
            database.Insert("RecHubUser.OLEntities",
                new
                {
                    EntityId = entityId,
                });
        }
        public static InsertResult InsertUser(IntegrationDatabase database,
            string logonName = null, Guid? raamSid = null)
        {
            return database.Insert("RecHubUser.Users",
                new
                {
                    LogonName = logonName ?? "User",
                    RA3MSID = (raamSid ?? Guid.NewGuid()).ToString(),
                });
        }
    }
}