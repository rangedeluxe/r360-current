﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace DatabaseIntegrationTestHelpers.R360
{
    public static class RecHubAudit
    {
        private class AuditMessageRow
        {
            public Guid AuditKey { get; set; }
            public string AuditMessage { get; set; }
        }

        public static IEnumerable<string> GetAuditMessages(IntegrationDatabase database)
        {
            const string sql =
                "SELECT TOP 50 AuditKey, AuditMessage FROM RecHubAudit.factDataAuditMessages ORDER BY factDataAuditMessageKey";
            var results = database.Connection.Query<AuditMessageRow>(sql);
            var messages =
                from row in results
                group row.AuditMessage by row.AuditKey
                into messageGroup
                select string.Join("", messageGroup);
            return messages;
        }
        public static void InsertAuditColumn(IntegrationDatabase database, int auditColumnKey,
            string schemaName, string tableName, string columnName)
        {
            database.Insert("RecHubAudit.dimAuditColumns",
                new
                {
                    AuditColumnKey = auditColumnKey,
                    SchemaName = schemaName,
                    TableName = tableName,
                    ColumnName = columnName,
                },
                setIdentityInsert: true);
        }
    }
}