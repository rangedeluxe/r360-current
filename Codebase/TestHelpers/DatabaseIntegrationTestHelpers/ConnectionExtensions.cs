﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Dapper;

namespace DatabaseIntegrationTestHelpers
{
    public static class ConnectionExtensions
    {
        private static void Execute(IDbConnection connection, IEnumerable<string> sqlLines)
        {
            connection.Execute(string.Join("\r\n", sqlLines));
        }
        /// <summary>
        /// Executes a SQL script that may have GOs embedded in it.
        /// </summary>
        public static void ExecuteSqlScript(this IDbConnection connection, string sqlScript)
        {
            var accumulatedLines = new List<string>();
            var reader = new StringReader(sqlScript);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (!line.Equals("GO", StringComparison.InvariantCultureIgnoreCase))
                    accumulatedLines.Add(line);
                else
                {
                    Execute(connection, accumulatedLines);
                    accumulatedLines.Clear();
                }
            }
            if (accumulatedLines.Any())
                Execute(connection, accumulatedLines);
        }
    }
}