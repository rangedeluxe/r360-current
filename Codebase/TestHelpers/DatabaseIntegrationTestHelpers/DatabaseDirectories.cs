﻿using System;
using System.IO;
using System.Linq;

namespace DatabaseIntegrationTestHelpers
{
    public static class DatabaseDirectories
    {
        private static string GetGitDatabaseScriptsDirectory()
        {
            var result = Path.Combine(GetGitRoot(), "Database");
            if (!Directory.Exists(result))
                throw new InvalidOperationException($"Could not find database directory at {result}");
            return result;
        }
        private static string GetGitRoot()
        {
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var directory = baseDirectory;
            while (directory != null)
            {
                if (Directory.Exists(Path.Combine(directory, ".git")))
                    return directory;
                directory = Path.GetDirectoryName(directory);
            }

            throw new InvalidOperationException($"Directory {baseDirectory} is not in a Git workspace");
        }
        private static string GetJenkinsDatabaseScriptsDirectory(string jenkinsWorkspace)
        {
            // In Jenkins builds that have been updated to use multiple
            // Git repositories (one for R360 and one for build scripts),
            // the R360 code gets checked out to an "R360" folder under
            // jenkinsWorkspace. In older builds, R360 code gets checked
            // out to the jenkinsWorkspace folder.
            var r360Workspace = new[]
            {
                Path.Combine(jenkinsWorkspace, "R360"),
                jenkinsWorkspace
            }.First(Directory.Exists);

            var result = Path.Combine(r360Workspace, "Database");
            if (!Directory.Exists(result))
            {
                throw new InvalidOperationException(
                    $"Could not find database directory at '{result}'. " +
                    "Do you need to adjust the Jenkins sparse-checkout settings?");
            }
            return result;
        }

        public static string GetRootDatabaseScriptsDirectory()
        {
            var jenkinsWorkspace = Environment.GetEnvironmentVariable("WORKSPACE");
            return string.IsNullOrEmpty(jenkinsWorkspace)
                ? GetGitDatabaseScriptsDirectory()
                : GetJenkinsDatabaseScriptsDirectory(jenkinsWorkspace);
        }
    }
}