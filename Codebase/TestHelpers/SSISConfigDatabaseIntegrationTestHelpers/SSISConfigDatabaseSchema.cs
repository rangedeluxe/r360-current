﻿using System;
using DatabaseIntegrationTestHelpers;

namespace SSISConfigDatabaseIntegrationTestHelpers
{
    public class SsisConfigDatabaseSchema
    {
        protected string SchemaName { get; }

        protected IntegrationDatabase Database { get; }

        protected DateTime RunDate { get; }

        public SsisConfigDatabaseSchema(IntegrationDatabase database, DateTime runDate)
        {
            //The class name matches the schema name
            SchemaName = GetType().Name;
            Database = database;
            RunDate = runDate;
        }

        public void MakeTable(string tableName, Func<string, string> filterFileContent = null)
        {
            Database.MakeTable(SchemaName, tableName, filterFileContent);
        }

        public void MakeStoredProcedure(string storedProcedureName)
        {
            Database.MakeStoredProcedure(SchemaName, storedProcedureName);
        }
    }
}
