﻿using System;
using DatabaseIntegrationTestHelpers;

namespace SSISConfigDatabaseIntegrationTestHelpers
{
    public class Dbo : SsisConfigDatabaseSchema
    {
        public Dbo(IntegrationDatabase database, DateTime runDate) : base(database,runDate)
        {
            MakeTable("SSISConfigurations");
        }

        public void InsertConfig(string path, string value, string filter = "CommonConfigurations", string type = "string")
        {
            Database.Insert($"{SchemaName}.SSISConfigurations",
                new
                {
                    ConfigurationFilter = filter,
                    ConfiguredValue = value,
                    PackagePath = $"\\Package.Variables[User::{path}].Properties[Value]",
                    ConfiguredValueType = type
                });
        }
    }
}
