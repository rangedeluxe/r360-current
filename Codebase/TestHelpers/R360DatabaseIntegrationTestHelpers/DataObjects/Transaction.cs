﻿using System.Collections.Generic;
using System.Linq;

namespace R360DatabaseIntegrationTestHelpers.DataObjects
{
    public class Transaction
    {
        public int TransactionExceptionStatusKey { get; set; }
        public int? TransactionId { get; set; } = null;
        public int? TxnSequence { get; set; } = null;
        public int ScannedCheckCount { get; set; }
        public int StubCount => Stubs?.Count() ?? 0;
        public int DocumentCount { get; set; }
        public int OmrCount { get; set; }
        public int CheckCount => Checks?.Count() ?? 0;
        public decimal CheckTotal => Checks?.Sum(c => c.Amount) ?? 0;
        public decimal StubTotal => Stubs?.Sum(s => s.Amount) ?? 0;
        public string TransactionExceptionStatus { get; set; }
        public List<Check> Checks { get; set; }
        public List<Stub> Stubs { get; set; }
    }
}
