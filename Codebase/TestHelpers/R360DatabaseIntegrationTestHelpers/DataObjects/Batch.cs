﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace R360DatabaseIntegrationTestHelpers.DataObjects
{
    public class Batch
    {
        public Int64 BatchId { get; set; }
        public bool IsDeleted { get; set; }
        public int BankKey { get; set; }
        public int ClientAccountKey { get; set; }
        public DateTime? DepositDate { get; set; }
        public DateTime? ImmutableDate { get; set; }
        public DateTime? SourceProcessingDate { get; set; }
        public Int64 SourceBatchId { get; set; }
        public Int64 BatchNumber { get; set; }
        public int BatchSourceKey { get; set; }
        public int BatchPaymentTypeKey { get; set; }
        public int BatchCueId { get; set; }
        public int DepositStatus { get; set; }
        public int SystemType { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModificationDate { get; set; }
        public int TransactionCount => Transactions?.Count() ?? 0;
        public int CheckCount => Transactions?.Sum(t => t.CheckCount) ?? 0;
        public decimal CheckTotal => Transactions?.Sum(t => t.CheckTotal) ?? 0;
        public int StubCount => Transactions?.Sum(t => t.StubCount) ?? 0;
        public decimal StubTotal => Transactions?.Sum(t => t.StubTotal) ?? 0;
        public int DocumentCount => Transactions?.Sum(t => t.DocumentCount) ?? 0;
        public int ScannedCheckCount => Transactions?.Sum(t => t.ScannedCheckCount) ?? 0;
        public int? BatchSiteCode { get; set; }
        public string BatchSource { get; set; }
        public string PaymentType { get; set; }
        public string BatchExceptionStatus { get; set; }
        public string DepositStatusName { get; set; }
        public string DepositDda { get; set; }

        public List<Transaction> Transactions;

        public Batch(DateTime runDate, long batchId, Int64 sourceBatchId, int batchPaymentTypeKey,
            bool isDeleted = false, int bankKey = -1, int clientAccountKey = -1,
            DateTime? depositDate = null, DateTime? immutableDate = null, DateTime? sourceProcessingDate = null,
            int? batchNumber = null, int batchSourceKey = 0, int batchCueId = 0, int depositStatusKey = -1,
            int depositStatus = 850, int systemType = 0, int? batchSiteCode = null, DateTime? creationDate = null,
            DateTime? modificationDate = null,List<Transaction> transactions = null)
        {
            BatchId = batchId;
            IsDeleted = isDeleted;
            BankKey = bankKey;
            ClientAccountKey = clientAccountKey;
            SourceBatchId = sourceBatchId;
            DepositDate = (depositDate ?? runDate);
            ImmutableDate = (immutableDate ?? runDate);
            SourceProcessingDate = (sourceProcessingDate ?? runDate);
            BatchNumber = batchNumber ?? sourceBatchId;
            BatchSourceKey = batchSourceKey;
            BatchPaymentTypeKey = batchPaymentTypeKey;
            BatchCueId = batchCueId;
            DepositStatus = depositStatus;
            SystemType = systemType;
            BatchSiteCode = batchSiteCode;
            CreationDate = creationDate ?? DateTime.Now;
            ModificationDate = modificationDate ?? DateTime.Now;
            Transactions = transactions;
        }
    }
}
