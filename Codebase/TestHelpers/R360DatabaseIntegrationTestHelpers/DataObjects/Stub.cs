﻿using System;
using System.Collections.Generic;

namespace R360DatabaseIntegrationTestHelpers.DataObjects
{
    public class Stub
    {
        public int? TxnSequence { get; set; } = null;
        public int? SequenceWithinTransaction { get; set; } = null;
        public int? BatchSequence { get; set; } = null;
        public int? StubSequence { get; set; } = null;
        public bool IsCorrespondence { get; set; } = false;
        public int? DocumentBatchSequence { get; set; } = null;
        public bool IsOmrDetected { get; set; } = false;
        public decimal? Amount { get; set; } = 0;
        public string AccountNumber { get; set; } = null;
        public List<DataEntryRow> DataEntryRows { get; set; }
    }
}
