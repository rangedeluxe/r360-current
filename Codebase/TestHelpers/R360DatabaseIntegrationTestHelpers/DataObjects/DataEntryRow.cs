﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R360DatabaseIntegrationTestHelpers.DataObjects
{
    public class DataEntryRow
    {
        public int? BatchSequence { get; set; } = null;
        public List<DataEntryValue> DataEntryValues { get; set; }
    }
}
