﻿using System;
using System.Collections.Generic;

namespace R360DatabaseIntegrationTestHelpers.DataObjects
{
    public class Check
    {
        public int DdaKey { get; set; }
        public int? TxnSequence { get; set; } = null;
        public int? SequenceWithinTransaction { get; set; } = null;
        public int? BatchSequence { get; set; } = null;
        public int? CheckSequence { get; set; } = null;
        public Int64 NumericRoutingNumber { get; set; } = 0L;
        public Int64 NumericSerial { get; set; } = 0L;
        public decimal Amount { get; set; } = 0;
        public int? BatchSiteCode { get; set; } = null;
        public string RoutingNumber { get; set; } = "0";
        public string Account { get; set; } = "0";
        public string TransactionCode { get; set; } = null;
        public string Serial { get; set; } = null;
        public string RemitterName { get; set; } = null;
        public List<DataEntryRow> DataEntryRows { get; set; }
    }
}
