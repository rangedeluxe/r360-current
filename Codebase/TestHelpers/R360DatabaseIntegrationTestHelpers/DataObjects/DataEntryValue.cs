﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R360DatabaseIntegrationTestHelpers.DataObjects
{
    public class DataEntryValue
    {
        public Int64 WorkgroupDataEntryColumnKey { get; set; }
        public string Value { get; set; }
        public DateTime? ValueDateTime { get; set; } = null;
        public float? ValueFloat { get; set; } = null;
        public decimal? ValueMoney { get; set; } = null;
        public string FieldName { get; set; }
        public string DataType { get; set; }
    }
}
