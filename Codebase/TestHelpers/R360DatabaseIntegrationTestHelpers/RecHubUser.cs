﻿using System;
using DatabaseIntegrationTestHelpers;

namespace R360DatabaseIntegrationTestHelpers
{
    public class RecHubUser : R360DatabaseSchema
    {
        public RecHubUser(IntegrationDatabase database, DateTime runDate) : base(database, runDate)
        {
        }

        public void InsertUser(int userId, Guid raamSid = new Guid(), string logonName = null, int? logonEntityId = null,
            string logonEntityName = null, DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.Users",
                new
                {
                    UserID = userId,
                    RA3MSID = raamSid,
                    LogonName = logonName ?? "TestLogonName",
                    LogonEntityID = logonEntityId,
                    LogonEntityName = logonEntityName,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now
                }, setIdentityInsert: true);

        }

        public void InsertSessionClientAccountEntitlements(Guid sessionId, DateTime? startDateKey = null, DateTime? endDateKey = null,
            int entityId = 1, int siteBankId = 9999, int siteClientAccountId = 99, int clientAccountKey = 1,
            bool viewAhead = true, int? viewingDays = null, int? maxSearchDays = null, string entityName = "TestEntity")
        {
            Database.Insert($"{SchemaName}.SessionClientAccountEntitlements",
                new
                {
                    SessionID = sessionId,
                    EntityID = entityId,
                    SiteBankID = siteBankId,
                    SiteClientAccountID = siteClientAccountId,
                    ClientAccountKey = clientAccountKey,
                    ViewingDays = viewingDays,
                    MaximumSearchDays = maxSearchDays,
                    StartDateKey = (startDateKey ?? RunDate).ToDateKey(),
                    EndDateKey = (endDateKey ?? RunDate).ToDateKey(),
                    ViewAhead = viewAhead,
                    EntityName = entityName
                });
        }
    }
}
