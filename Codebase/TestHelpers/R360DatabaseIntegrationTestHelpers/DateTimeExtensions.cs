﻿using System;

namespace R360DatabaseIntegrationTestHelpers
{
    public static class DateTimeExtensions
    {
        public static int ToDateKey(this DateTime date)
        {
            return date.Year * 10000 + date.Month * 100 + date.Day;
        }
    }
}