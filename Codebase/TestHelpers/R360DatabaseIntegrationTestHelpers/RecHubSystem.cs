﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using DatabaseIntegrationTestHelpers;

namespace R360DatabaseIntegrationTestHelpers
{
    public class RecHubSystem : R360DatabaseSchema
    {
        private enum SqlAgentAction { QuitSuccess = 1, QuitFailure = 2, NextStep = 3, NextStepId = 4}

        private string _diskSsisPackagePath;

        public RecHubSystem(IntegrationDatabase database, DateTime runDate) : base(database, runDate)
        {
            _diskSsisPackagePath = Path.Combine($"\\\\{Database.Connection.DataSource}", "d$", "SSISPackages", Database.Connection.Database);
        }

        private Guid CreateAgentJob(string agentJobName, string description, string sqlServer)
        {
            var parms = new SqlParameter[]
            {
                Database.BuildParameter("@job_name", SqlDbType.NVarChar, agentJobName),
                Database.BuildParameter("@enabled", SqlDbType.Bit, false),
                Database.BuildParameter("@notify_level_eventlog", SqlDbType.Bit, false),
                Database.BuildParameter("@notify_level_email", SqlDbType.Bit, false),
                Database.BuildParameter("@notify_level_netsend", SqlDbType.Bit, false),
                Database.BuildParameter("@notify_level_page", SqlDbType.Bit, false),
                Database.BuildParameter("@delete_level", SqlDbType.Bit, false),
                Database.BuildParameter("@description", SqlDbType.NVarChar, description),
                Database.BuildParameter("@category_name", SqlDbType.NVarChar, ""),
                Database.BuildParameter("@owner_login_name", SqlDbType.NVarChar, "IntegrationTests"),
                Database.BuildParameter("@job_id", SqlDbType.UniqueIdentifier, new Guid(), ParameterDirection.Output)
            };

            Database.ExecuteProcedure("msdb.dbo.sp_add_job", parms);
            return (Guid)parms[10].Value;
        }

        private void DeleteAgentJob(string agentJobName)
        {
            var parms = new SqlParameter[]
            {
                Database.BuildParameter("@job_name", SqlDbType.NVarChar, agentJobName)
            };

            Database.ExecuteProcedure("msdb.dbo.sp_delete_job", parms);
        }

        public XElement CreateBatchesElement(string xsdVersion = "0.00.00.00",
            string clientProcessCode = "DatabaseTest", Guid sourceTrackingId = new Guid(),
            Guid entityTrackingId = new Guid())
        {
            return new XElement(
                "Batches", 
                    new XAttribute("XSDVersion", xsdVersion),
                    new XAttribute("ClientProcessCode", clientProcessCode), 
                    new XAttribute("SourceTrackingID", sourceTrackingId == Guid.Empty ? Guid.NewGuid() : sourceTrackingId),
                    new XAttribute("EntityTrackingID", entityTrackingId == Guid.Empty ? Guid.NewGuid() : entityTrackingId
                    )
                );
        }

        public void CreateBatchElement(XElement xBatchElement, int batchId, string batchSource, string paymentType,
            DateTime? depositDate = null, DateTime? batchDate = null, DateTime? processingDate = null, int bankId = -1,
            int clientId = -1, int batchNumber = 0, int batchSiteCode = -1, int batchCueId = 0,
            Guid batchTrackingId = new Guid())
        {
            var runDate = RunDate.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);

            xBatchElement.Add(
                new XElement("Batch", 
                    new XAttribute("DepositDate", depositDate?.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture) ?? runDate), 
                    new XAttribute("BatchDate", batchDate?.ToString("MM-dd-yyyy") ?? runDate),
                    new XAttribute("ProcessingDate", processingDate?.ToString("MM-dd-yyyy") ?? runDate),
                    new XAttribute("BankID", bankId),
                    new XAttribute("ClientID", clientId),
                    new XAttribute("BatchID", batchId),
                    new XAttribute("BatchNumber", batchNumber),
                    new XAttribute("BatchSiteCode", batchSiteCode),
                    new XAttribute("BatchSource", batchSource),
                    new XAttribute("PaymentType", paymentType),
                    new XAttribute("BatchCueID", batchCueId),
                    new XAttribute("BatchTrackingID", batchTrackingId == Guid.Empty ? Guid.NewGuid() : batchTrackingId)
                    )
                );
        }

        public void CreateDocumentElement(XElement xTransactionElement, int batchSequence, int documentId, int documentSequence,
            int sequenceWithinTransaction, string documentDescriptor, bool isCorrespondence = false)
        {
            xTransactionElement.Add(
                new XElement("Document",
                    new XAttribute("Transaction_Id", xTransactionElement.Attribute("Transaction_Id").Value),
                    new XAttribute("Document_Id", documentId),
                    new XAttribute("BatchSequence", batchSequence),
                    new XAttribute("DocumentSequence", documentSequence),
                    new XAttribute("SequenceWithinTransaction", sequenceWithinTransaction),
                    new XAttribute("DocumentDescriptor", documentDescriptor),
                    new XAttribute("IsCorrespondence", isCorrespondence ? "1" : "0")
                    )
                );
        }

        public void CreateDocumentRemittanceDataElement(XElement xDocumentElement, int batchSequence, string fieldName,
            string fieldValue)
        {
            xDocumentElement.Add(
                new XElement("DocumentRemittanceData", 
                    new XAttribute("Document_Id", xDocumentElement.Attribute("Document_Id").Value),
                    new XAttribute("RemittanceDataRecord_Id", xDocumentElement.Attribute("Document_Id").Value),
                    new XAttribute("BatchSequence", batchSequence),
                    new XAttribute("FieldName", fieldName),
                    new XAttribute("FieldValue", fieldValue)
                    )
                );
        }

        public void CreateTransactionElement(XElement xBatchElement, int transactionId, int transactionSequence)
        {
            xBatchElement.Add(
                new XElement("Transaction",
                    new XAttribute("Transaction_Id", transactionId),
                    new XAttribute("TransactionID", transactionId),
                    new XAttribute("TransactionSequence", transactionSequence)
                    )
                );
        }

        public void InsertDataImportQueueRecord_Batch(XElement xDataDocument, int? auditDateKey = null)
        {
            var columns = new string[]
            {
                "XSDVersion",
                "ClientProcessCode",
                "SourceTrackingID",
                "BatchTrackingID",
                "XMLDataDocument"
            };
            var currenttable = new DataTable();
            currenttable.Columns.AddRange(columns.Select(c => new DataColumn(c)).ToArray());
            // Grab the common data from the 'batches' row.
            var sourceTrackingId = Guid.Parse(xDataDocument.Attribute("SourceTrackingID").Value);
            var xsdVersion = xDataDocument.Attribute("XSDVersion").Value;
            var clientCode = xDataDocument.Attribute("ClientProcessCode").Value;
            // add each 'batch' to the data table
            foreach (var batch in xDataDocument.Descendants("Batch"))
            {
                var row = currenttable.NewRow();
                row["XSDVersion"] = xsdVersion;
                row["ClientProcessCode"] = clientCode;
                row["SourceTrackingID"] = sourceTrackingId;
                row["BatchTrackingID"] = Guid.Parse(batch.Attribute("BatchTrackingID").Value);
                row["XMLDataDocument"] = batch;
                currenttable.Rows.Add(row);
            }

            var parms = new SqlParameter[]
            {
                Database.BuildParameter("@parmDataImportQueueInsertTable", SqlDbType.Structured, currenttable, ParameterDirection.Input),
                Database.BuildParameter("@parmAuditDateKey", SqlDbType.Int, auditDateKey ?? RunDate.ToDateKey(), ParameterDirection.Input)
            };
            Database.ExecuteProcedure($"{SchemaName}.usp_DataImportQueue_Ins_Batch", parms);
        }

        private void AddAgentJobToServer(Guid jobId, string serverName)
        {
            var parms = new SqlParameter[]
            {
                Database.BuildParameter("@job_id", SqlDbType.UniqueIdentifier, jobId),
                Database.BuildParameter("@server_name", SqlDbType.NVarChar, serverName)
            };

            Database.ExecuteProcedure("msdb.dbo.sp_add_jobserver", parms);

        }

        private void CreateAgentJobStep(Guid jobId, int stepId, string stepName, string subSystem, string command, int onSucessStepId, int onFailStepId, string databseName = "master", int successCode = 0, SqlAgentAction onSuccessAction = SqlAgentAction.NextStep, SqlAgentAction onFailAction = SqlAgentAction.QuitFailure, int retryAttempts = 0, int retryInterval = 0)
        {
            var parms = new SqlParameter[]
            {
                Database.BuildParameter("@job_id", SqlDbType.UniqueIdentifier, jobId),
                Database.BuildParameter("@step_id", SqlDbType.Int, stepId),
                Database.BuildParameter("@step_name", SqlDbType.NVarChar, stepName),
                Database.BuildParameter("@subsystem", SqlDbType.NVarChar, subSystem),
                Database.BuildParameter("@database_name", SqlDbType.NVarChar, databseName),
                Database.BuildParameter("@cmdexec_success_code", SqlDbType.Int, successCode),
                Database.BuildParameter("@on_success_action", SqlDbType.Int, onSuccessAction),
                Database.BuildParameter("@on_success_step_id", SqlDbType.Int, onSucessStepId),
                Database.BuildParameter("@on_fail_action", SqlDbType.Int, onFailAction),
                Database.BuildParameter("@on_fail_step_id", SqlDbType.Int, onFailStepId),
                Database.BuildParameter("@retry_attempts", SqlDbType.Int, retryAttempts),
                Database.BuildParameter("@retry_interval", SqlDbType.Int, retryInterval),
                Database.BuildParameter("@command", SqlDbType.NVarChar, command)

            };

            Database.ExecuteProcedure("msdb.dbo.sp_add_jobstep", parms);
        }

        public void CreateDitAgentJob(string r360ServerName,string configServerName, string configDatabaseName)
        {
            var jobId = CreateAgentJob($"({Database.Connection.Database}) DataImportIntegrationServices", "Data Import Integration Services Setup and Data", r360ServerName);
            CreateAgentJobStep(jobId, 1, "Import Integration Data", "Dts", $"/DTS \"DataImportIntegrationServices_BatchData\" /SERVER {r360ServerName} /CHECKPOINTING OFF /SET \"\\Package.Variables[User::WFSConfigurationServerName].Properties[Value]\";{configServerName} /SET \"\\Package.Variables[User::WFSConfigurationInitialCatalog].Properties[Value]\";\"{configDatabaseName}\" /REPORTING E", 0, 0);
            AddAgentJobToServer(jobId, r360ServerName);
        }

        public void DeleteDitAgentJob()
        {
            DeleteAgentJob($"({Database.Connection.Database}) DataImportIntegrationServices");
        }

        public void RemoveDiskSsisPackageFolder(string package)
        {
            var folder = Path.Combine(_diskSsisPackagePath, package);

            if (Directory.Exists(folder))
                Directory.Delete(folder);
        }

        public void CreateDiskSsisPackageFolder(string package)
        {
            var folder = Path.Combine(_diskSsisPackagePath, package);

            Directory.CreateDirectory(folder);
        }
    }
}
