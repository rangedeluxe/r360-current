﻿using System;
using DatabaseIntegrationTestHelpers;


namespace R360DatabaseIntegrationTestHelpers
{
    public class R360DatabaseSchema
    {
        protected string SchemaName { get; }

        protected IntegrationDatabase Database { get; }

        protected DateTime RunDate { get; }

        protected R360DatabaseSchema(IntegrationDatabase database, DateTime runDate)
        {
            //The class names match the schema name
            SchemaName = GetType().Name;
            Database = database;
            RunDate = runDate;
        }

        public void MakeTable(string tableName)
        {
            Database.MakeTable(SchemaName, tableName, IgnorePartitions);
        }

        public void MakeStoredProcedure(string storedProcedureName)
        {
            Database.MakeStoredProcedure(SchemaName, storedProcedureName);
        }

        public void MakeSequence(string sequenceName)
        {
            Database.MakeSequence(SchemaName, sequenceName);
        }

        public void MakeDataType(string dataType)
        {
            Database.MakeDataType(SchemaName, dataType);
        }

        private string IgnorePartitions(string sql)
        {
            return sql.Replace("$(OnDataPartition)", "").Replace("$(OnAuditPartition)", "").Replace("ON DataImport (AuditDateKey)","").Replace("ON Sessions (CreationDateKey)", "");
        }
    }
}
