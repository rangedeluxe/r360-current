﻿using System;
using DatabaseIntegrationTestHelpers;

namespace R360DatabaseIntegrationTestHelpers
{
    public class RecHubException : R360DatabaseSchema
    {
        public RecHubException(IntegrationDatabase database, DateTime runDate) : base(database, runDate)
        {
        }

        public void InsertPostDepositTransactionException(long batchId, int transactionId, int lockedByUserId,
            DateTime? depositDate = null)
        {
            Database.Insert("RecHubException.PostDepositTransactionExceptions", new
            {
                BatchId = batchId,
                DepositDateKey = (depositDate ?? RunDate).ToDateKey(),
                TransactionId = transactionId,
                LockedByUserId = lockedByUserId,
            });
        }
    }
}
