﻿using System;
using DatabaseIntegrationTestHelpers;

namespace R360DatabaseIntegrationTestHelpers
{
    public class RecHubCommon : R360DatabaseSchema
    {
        public RecHubCommon(IntegrationDatabase database, DateTime runDate) : base(database, runDate)
        {
            MakeStoredProcedure("usp_WfsRethrowException");
        }
    }
}
