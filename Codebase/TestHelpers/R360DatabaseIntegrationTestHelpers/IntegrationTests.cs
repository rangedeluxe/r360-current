﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseIntegrationTestHelpers;
using R360DatabaseIntegrationTestHelpers.DataObjects;
using WFS.RecHub.R360Services.Common.DTO;

namespace R360DatabaseIntegrationTestHelpers
{
    public class IntegrationTests : R360DatabaseSchema
    {
        public IntegrationTests(IntegrationDatabase database, DateTime runDate) : base(database, runDate)
        {
            MakeStoredProcedure("usp_dimBanks_GetSert");
            MakeStoredProcedure("usp_dimClientAccounts_GetSert");
            MakeStoredProcedure("usp_dimBatchSources_Ins");
            MakeStoredProcedure("usp_dimBatchPaymentTypes_Ins");
            MakeStoredProcedure("usp_dimImportTypes_Ins");
            MakeStoredProcedure("usp_WorkgroupDataEntryColumns_Ins");
            MakeStoredProcedure("usp_factBatchSummary_Ins");
            MakeStoredProcedure("usp_factChecks_Ins");
            MakeStoredProcedure("usp_factStubs_Ins");
            MakeStoredProcedure("usp_factDataEntryDetails_Ins");
            MakeStoredProcedure("usp_factTransactionSummary_Ins");
        }

        public int GetBankKey(int siteBankId, int? entityId = null, string bankName = null, string aba = null)
        {
            var bankKey = -1;
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId),
                    Database.BuildParameter("@parmBankName", SqlDbType.VarChar, bankName ?? $"Bank {siteBankId}"),
                    Database.BuildParameter("@parmFIEntityID", SqlDbType.Int, entityId),
                    Database.BuildParameter("@parmABA", SqlDbType.VarChar, aba),
                    Database.BuildParameter("@parmBankKey", SqlDbType.Int, null, ParameterDirection.Output)
                };

                Database.ExecuteProcedure("IntegrationTests.usp_dimBanks_GetSert", parms, out var returnParms);
                if (returnParms?[4]?.Value != null)
                {
                    int.TryParse(returnParms[4].Value.ToString(), out bankKey);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing GetBankKey: {ex.Message}", ex);
            }
            return bankKey;
        }

        public int GetClientAccountKey(int siteBankId, int siteClientAccountId, string shortName = null,
            string longName = null, int dataRetentionDays = 999, int imageRetentionDays = 999)
        {
            var clientAccountKey = -1;
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId),
                    Database.BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountId),
                    Database.BuildParameter("@parmDataRetentionDays", SqlDbType.Int, dataRetentionDays),
                    Database.BuildParameter("@parmImageRetentionDays", SqlDbType.Int, imageRetentionDays),
                    Database.BuildParameter("@parmShortName", SqlDbType.VarChar, shortName ?? $"Workgroup {siteClientAccountId}"),
                    Database.BuildParameter("@parmLongName", SqlDbType.VarChar, longName ?? $"Workgroup Long {siteClientAccountId}"),
                    Database.BuildParameter("@parmClientAccountKey", SqlDbType.Int, null, ParameterDirection.Output)
                };

                Database.ExecuteProcedure("IntegrationTests.usp_dimClientAccounts_GetSert", parms, out var returnParms);
                if (returnParms?[6]?.Value != null)
                {
                    int.TryParse(returnParms[6].Value.ToString(), out clientAccountKey);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing GetClientAccountKey: {ex.Message}", ex);
            }

            return clientAccountKey;
        }

        public void InsertBatchSources(string shortName, string longName, string importType)
        {
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmShortName", SqlDbType.VarChar, shortName),
                    Database.BuildParameter("@parmLongName", SqlDbType.VarChar, longName ?? shortName),
                    Database.BuildParameter("@parmImportType", SqlDbType.VarChar, importType)
                };

                Database.ExecuteProcedure("IntegrationTests.usp_dimBatchSources_Ins", parms);
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing InsertBatchSources: {ex.Message}", ex);
            }
        }

        public void InsertBatchPaymentTypes(string shortName, string longName)
        {
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmShortName", SqlDbType.VarChar, shortName),
                    Database.BuildParameter("@parmLongName", SqlDbType.VarChar, longName ?? shortName)
                };

                Database.ExecuteProcedure("IntegrationTests.usp_dimBatchPaymentTypes_Ins", parms);
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing InsertBatchSources: {ex.Message}", ex);
            }
        }

        public void InsertImportTypes(string shortName)
        {
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmShortName", SqlDbType.VarChar, shortName)
                };

                Database.ExecuteProcedure("IntegrationTests.usp_dimImportTypes_Ins", parms);
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing InsertImportTypes: {ex.Message}", ex);
            }
        }

        public void InsertWorkgroupDataEntryColumns(int siteBankId, int siteClientAccountId, string batchSource, 
                string fieldName, string uiLabel = null, string displayName = null, int dataType = 1, bool isCheck = true,
                bool isRequired = true, bool markSense = false, int screenOrder = 1)
        {
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId),
                    Database.BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountId),
                    Database.BuildParameter("@parmBatchSource", SqlDbType.VarChar, batchSource),
                    Database.BuildParameter("@parmIsCheck", SqlDbType.Bit, isCheck),
                    Database.BuildParameter("@parmDataType", SqlDbType.TinyInt, dataType),
                    Database.BuildParameter("@parmIsRequired", SqlDbType.Bit, isRequired),
                    Database.BuildParameter("@parmMarkSense", SqlDbType.Bit, markSense),
                    Database.BuildParameter("@parmScreenOrder", SqlDbType.Int, screenOrder),
                    Database.BuildParameter("@parmUILabel", SqlDbType.VarChar, uiLabel ?? fieldName),
                    Database.BuildParameter("@parmFieldName", SqlDbType.VarChar, fieldName),
                    Database.BuildParameter("@parmSourceDisplayName", SqlDbType.VarChar, displayName ?? "")
                };

                Database.ExecuteProcedure("IntegrationTests.usp_WorkgroupDataEntryColumns_Ins", parms);
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing InsertWorkgroupDataEntryColumns: {ex.Message}", ex);
            }
        }

        public void InsertFactBatchSummary(Batch batch)
        {
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmBankKey", SqlDbType.Int, batch.BankKey),
                    Database.BuildParameter("@parmClientAccountKey", SqlDbType.Int, batch.ClientAccountKey),
                    Database.BuildParameter("@parmDepositDateKey", SqlDbType.Int, (batch.DepositDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmImmutableDateKey", SqlDbType.Int, (batch.ImmutableDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceProcessingDateKey", SqlDbType.Int, (batch.SourceProcessingDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, batch.SourceBatchId),
                    Database.BuildParameter("@parmBatchNumber", SqlDbType.BigInt, batch.BatchNumber),
                    Database.BuildParameter("@parmBatchSource", SqlDbType.VarChar, batch.BatchSource),
                    Database.BuildParameter("@parmPaymentType", SqlDbType.VarChar, batch.PaymentType),
                    Database.BuildParameter("@parmBatchExceptionStatus", SqlDbType.VarChar, batch.BatchExceptionStatus ?? "NoException"),
                    Database.BuildParameter("@parmBatchCueID", SqlDbType.Int, batch.BatchCueId),
                    Database.BuildParameter("@parmDepositStatusName", SqlDbType.VarChar, batch.DepositStatusName ?? "Deposit Complete"),
                    Database.BuildParameter("@parmSystemType", SqlDbType.SmallInt, batch.SystemType),
                    Database.BuildParameter("@parmTransactionCount", SqlDbType.Int, batch.TransactionCount),
                    Database.BuildParameter("@parmCheckCount", SqlDbType.Int, batch.CheckCount),
                    Database.BuildParameter("@parmScannedCheckCount", SqlDbType.Int, batch.ScannedCheckCount),
                    Database.BuildParameter("@parmStubCount", SqlDbType.Int, batch.StubCount),
                    Database.BuildParameter("@parmDocumentCount", SqlDbType.Int, batch.DocumentCount),
                    Database.BuildParameter("@parmCheckTotal", SqlDbType.Money, batch.CheckTotal),
                    Database.BuildParameter("@parmStubTotal", SqlDbType.Money, batch.CheckTotal),
                    Database.BuildParameter("@parmBatchSiteCode", SqlDbType.Int, batch.BatchSiteCode ?? 0),
                    Database.BuildParameter("@parmDepositDDA", SqlDbType.VarChar, batch.DepositDda ?? ""),
                    Database.BuildParameter("@parmBatchID", SqlDbType.BigInt, null,ParameterDirection.Output),
                    Database.BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, null,ParameterDirection.Output),
                    Database.BuildParameter("@parmPaymentTypeKey", SqlDbType.TinyInt, null,ParameterDirection.Output),
                    Database.BuildParameter("@parmDepositStatus", SqlDbType.Int, null,ParameterDirection.Output),
                };

                Database.ExecuteProcedure("IntegrationTests.usp_factBatchSummary_Ins", parms, out var returnParms);
                if (returnParms?[22]?.Value != null)
                {
                    if (long.TryParse(returnParms[22].Value.ToString(), out var batchId))
                    {
                        batch.BatchId = batchId;
                    }
                }
                if (returnParms?[23]?.Value != null)
                {
                    if (int.TryParse(returnParms[23].Value.ToString(), out var batchSourceKey))
                    {
                        batch.BatchSourceKey = batchSourceKey;
                    }
                }
                if (returnParms?[24]?.Value != null)
                {
                    if (int.TryParse(returnParms[24].Value.ToString(), out var paymentTypeKey))
                    {
                        batch.BatchPaymentTypeKey = paymentTypeKey;
                    }
                }
                if (returnParms?[25]?.Value != null)
                {
                    if (int.TryParse(returnParms[25].Value.ToString(), out var depositStatus))
                    {
                        batch.DepositStatus = depositStatus;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing InsertFactBatchSummary: {ex.Message}", ex);
            }
        }

        public void InsertFactChecks(Batch batch, Transaction transaction, Check check)
        {
            Int64.TryParse(check.RoutingNumber, out var numericRoutingNumber);
            Int64.TryParse(check.Serial, out var numericSerial);
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmBankKey", SqlDbType.Int, batch.BankKey),
                    Database.BuildParameter("@parmClientAccountKey", SqlDbType.Int, batch.ClientAccountKey),
                    Database.BuildParameter("@parmDepositDateKey", SqlDbType.Int, (batch.DepositDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmImmutableDateKey", SqlDbType.Int, (batch.ImmutableDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceProcessingDateKey", SqlDbType.Int, (batch.SourceProcessingDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, batch.SourceBatchId),
                    Database.BuildParameter("@parmBatchID", SqlDbType.BigInt, batch.BatchId),
                    Database.BuildParameter("@parmBatchNumber", SqlDbType.BigInt, batch.BatchNumber),
                    Database.BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, batch.BatchSourceKey),
                    Database.BuildParameter("@parmPaymentTypeKey", SqlDbType.TinyInt, batch.BatchPaymentTypeKey),
                    Database.BuildParameter("@parmBatchCueID", SqlDbType.Int, batch.BatchCueId),
                    Database.BuildParameter("@parmDepositStatus", SqlDbType.Int, batch.DepositStatus),
                    Database.BuildParameter("@parmTransactionID", SqlDbType.Int, transaction.TransactionId),
                    Database.BuildParameter("@parmTxnSequence", SqlDbType.Int, transaction.TxnSequence),
                    Database.BuildParameter("@parmSequenceWithinTransaction", SqlDbType.Int, check.SequenceWithinTransaction),
                    Database.BuildParameter("@parmBatchSequence", SqlDbType.Int, check.BatchSequence),
                    Database.BuildParameter("@parmCheckSequence", SqlDbType.Int, check.CheckSequence),
                    Database.BuildParameter("@parmAmount", SqlDbType.Money, check.Amount),
                    Database.BuildParameter("@parmNumericRoutingNumber", SqlDbType.BigInt, numericRoutingNumber),
                    Database.BuildParameter("@parmNumericSerial", SqlDbType.BigInt, numericSerial),
                    Database.BuildParameter("@parmRoutingNumber", SqlDbType.VarChar, check.RoutingNumber),
                    Database.BuildParameter("@parmAccount", SqlDbType.VarChar, check.Account),
                    Database.BuildParameter("@parmSerial", SqlDbType.VarChar, check.Serial),
                    Database.BuildParameter("@parmTransactionCode", SqlDbType.VarChar, check.TransactionCode),
                    Database.BuildParameter("@parmRemitterName", SqlDbType.VarChar, check.RemitterName),
                    Database.BuildParameter("@parmBatchSiteCode", SqlDbType.VarChar, batch.BatchSiteCode ?? 0)
                };

                Database.ExecuteProcedure("IntegrationTests.usp_factChecks_Ins", parms);
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing InsertFactChecks: {ex.Message}", ex);
            }
        }

        public void InsertFactDataEntryDetails(Batch batch, Transaction transaction, DataEntryValue dataentryValue, int siteBankId, int siteClientAccountID, int batchSequence, bool isCheck)
        {
            var dataType = AdvancedSearchWhereClauseDataType.String;
            DateTime? datetimeValue = null;
            float? floatValue = null;
            decimal? moneyValue = null;

            switch (dataentryValue.DataType.ToUpper())
            {
                case "DATETIME":
                    dataType = AdvancedSearchWhereClauseDataType.DateTime;
                    if (DateTime.TryParse(dataentryValue.Value, out var dtValue))
                        datetimeValue = dtValue;
                    break;
                case "FLOAT":
                    dataType = AdvancedSearchWhereClauseDataType.Float;
                    if (float.TryParse(dataentryValue.Value, out var fValue))
                        floatValue = fValue;
                    break;
                case "DECIMAL":
                    dataType = AdvancedSearchWhereClauseDataType.Decimal;
                    if (decimal.TryParse(dataentryValue.Value, out var dValue))
                        moneyValue = dValue;
                    break;
                case "STRING":
                    dataType = AdvancedSearchWhereClauseDataType.String;
                    break;
            }
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmBankKey", SqlDbType.Int, batch.BankKey),
                    Database.BuildParameter("@parmClientAccountKey", SqlDbType.Int, batch.ClientAccountKey),
                    Database.BuildParameter("@parmDepositDateKey", SqlDbType.Int, (batch.DepositDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmImmutableDateKey", SqlDbType.Int, (batch.ImmutableDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceProcessingDateKey", SqlDbType.Int, (batch.SourceProcessingDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, batch.SourceBatchId),
                    Database.BuildParameter("@parmBatchID", SqlDbType.BigInt, batch.BatchId),
                    Database.BuildParameter("@parmBatchNumber", SqlDbType.BigInt, batch.BatchNumber),
                    Database.BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, batch.BatchSourceKey),
                    Database.BuildParameter("@parmPaymentTypeKey", SqlDbType.TinyInt, batch.BatchPaymentTypeKey),
                    Database.BuildParameter("@parmDepositStatus", SqlDbType.Int, batch.DepositStatus),
                    Database.BuildParameter("@parmTransactionID", SqlDbType.Int, transaction.TransactionId),
                    Database.BuildParameter("@parmBatchSequence", SqlDbType.Int, batchSequence),
                    Database.BuildParameter("@parmIsCheck", SqlDbType.Bit, isCheck),
                    Database.BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId),
                    Database.BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountID),
                    Database.BuildParameter("@parmFieldName", SqlDbType.VarChar, dataentryValue.FieldName),
                    Database.BuildParameter("@parmDataType", SqlDbType.TinyInt, dataType),
                    Database.BuildParameter("@parmValueDateTime", SqlDbType.DateTime, datetimeValue),
                    Database.BuildParameter("@parmValueFloat", SqlDbType.Float, floatValue),
                    Database.BuildParameter("@parmValueMoney", SqlDbType.Money, moneyValue),
                    Database.BuildParameter("@parmValue", SqlDbType.VarChar, dataentryValue.Value),
                };

                Database.ExecuteProcedure("IntegrationTests.usp_factDataEntryDetails_Ins", parms);
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing InsertFactDataEntryDetails: {ex.Message}", ex);
            }
        }

        public void InsertFacStubs(Batch batch, Transaction transaction, Stub stub)
        {
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmBankKey", SqlDbType.Int, batch.BankKey),
                    Database.BuildParameter("@parmClientAccountKey", SqlDbType.Int, batch.ClientAccountKey),
                    Database.BuildParameter("@parmDepositDateKey", SqlDbType.Int, (batch.DepositDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmImmutableDateKey", SqlDbType.Int, (batch.ImmutableDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceProcessingDateKey", SqlDbType.Int, (batch.SourceProcessingDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, batch.SourceBatchId),
                    Database.BuildParameter("@parmBatchID", SqlDbType.BigInt, batch.BatchId),
                    Database.BuildParameter("@parmBatchNumber", SqlDbType.BigInt, batch.BatchNumber),
                    Database.BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, batch.BatchSourceKey),
                    Database.BuildParameter("@parmPaymentTypeKey", SqlDbType.TinyInt, batch.BatchPaymentTypeKey),
                    Database.BuildParameter("@parmBatchCueID", SqlDbType.Int, batch.BatchCueId),
                    Database.BuildParameter("@parmDepositStatus", SqlDbType.Int, batch.DepositStatus),
                    Database.BuildParameter("@parmSystemType", SqlDbType.SmallInt, batch.SystemType),
                    Database.BuildParameter("@parmTransactionID", SqlDbType.Int, transaction.TransactionId),
                    Database.BuildParameter("@parmTxnSequence", SqlDbType.Int, transaction.TxnSequence),
                    Database.BuildParameter("@parmSequenceWithinTransaction", SqlDbType.Int, stub.SequenceWithinTransaction),
                    Database.BuildParameter("@parmBatchSequence", SqlDbType.Int, stub.BatchSequence),
                    Database.BuildParameter("@parmStubSequence", SqlDbType.Int, stub.StubSequence),
                    Database.BuildParameter("@parmAmount", SqlDbType.Money, stub.Amount),
                    Database.BuildParameter("@parmAccountNumber", SqlDbType.VarChar, stub.AccountNumber),
                    Database.BuildParameter("@parmIsCorrespondence", SqlDbType.Bit, stub.IsCorrespondence),
                    Database.BuildParameter("@parmDocumentBatchSequence", SqlDbType.Int, stub.DocumentBatchSequence),
                    Database.BuildParameter("@parmIsOMRDetected", SqlDbType.Bit, stub.IsOmrDetected),
                    Database.BuildParameter("@parmBatchSiteCode", SqlDbType.VarChar, batch.BatchSiteCode ?? 0)
                };

                Database.ExecuteProcedure("IntegrationTests.usp_factStubs_Ins", parms);
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing InsertFacStubs: {ex.Message}", ex);
            }
        }

        public void InsertFactTransactionSummary(Batch batch,Transaction transaction)
        {
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmBankKey", SqlDbType.Int, batch.BankKey),
                    Database.BuildParameter("@parmClientAccountKey", SqlDbType.Int, batch.ClientAccountKey),
                    Database.BuildParameter("@parmDepositDateKey", SqlDbType.Int, (batch.DepositDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmImmutableDateKey", SqlDbType.Int, (batch.ImmutableDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceProcessingDateKey", SqlDbType.Int, (batch.SourceProcessingDate ?? RunDate).ToDateKey()),
                    Database.BuildParameter("@parmSourceBatchID", SqlDbType.BigInt, batch.SourceBatchId),
                    Database.BuildParameter("@parmBatchID", SqlDbType.BigInt, batch.BatchId),
                    Database.BuildParameter("@parmBatchNumber", SqlDbType.BigInt, batch.BatchNumber),
                    Database.BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, batch.BatchSourceKey),
                    Database.BuildParameter("@parmPaymentTypeKey", SqlDbType.TinyInt, batch.BatchPaymentTypeKey),
                    Database.BuildParameter("@parmTransactionExceptionStatus", SqlDbType.VarChar, transaction.TransactionExceptionStatus ?? "NoException"),
                    Database.BuildParameter("@parmBatchCueID", SqlDbType.Int, batch.BatchCueId),
                    Database.BuildParameter("@parmDepositStatus", SqlDbType.Int, batch.DepositStatus),
                    Database.BuildParameter("@parmSystemType", SqlDbType.SmallInt, batch.SystemType),
                    Database.BuildParameter("@parmTransactionID", SqlDbType.Int, transaction.TransactionId),
                    Database.BuildParameter("@parmTxnSequence", SqlDbType.Int, transaction.TxnSequence),
                    Database.BuildParameter("@parmOMRCount", SqlDbType.Int,transaction.OmrCount),
                    Database.BuildParameter("@parmCheckCount", SqlDbType.Int, transaction.CheckCount),
                    Database.BuildParameter("@parmScannedCheckCount", SqlDbType.Int, transaction.ScannedCheckCount),
                    Database.BuildParameter("@parmStubCount", SqlDbType.Int, transaction.StubCount),
                    Database.BuildParameter("@parmDocumentCount", SqlDbType.Int, transaction.DocumentCount), 
                    Database.BuildParameter("@parmCheckTotal", SqlDbType.Money, transaction.CheckTotal),
                    Database.BuildParameter("@parmStubTotal", SqlDbType.Money, transaction.CheckTotal),
                    Database.BuildParameter("@parmBatchSiteCode", SqlDbType.Int, batch.BatchSiteCode ?? 0)
                };

                Database.ExecuteProcedure("IntegrationTests.usp_factTransactionSummary_Ins", parms);
            }
            catch (Exception ex)
            {
                throw new Exception($"An Error occurred while executing InsertFactTransactionSummary: {ex.Message}", ex);
            }
        }
    }
}
