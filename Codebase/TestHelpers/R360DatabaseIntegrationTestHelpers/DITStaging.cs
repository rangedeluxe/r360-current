﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseIntegrationTestHelpers;

namespace R360DatabaseIntegrationTestHelpers
{
    public class DitStaging : R360DatabaseSchema
    {
        public DitStaging(IntegrationDatabase database, DateTime runDate) : base(database, runDate)
        {
        }

        public void CreateBatchDataObjects()
        {
            MakeTable("DataImportWorkBatch");
            MakeTable("DataImportWorkBatchKeys");
            MakeTable("DataImportWorkBatchResponses");
            MakeTable("DataImportWorkBatchStartup");
            MakeTable("DataImportWorkDuplicateTransactions");
            MakeTable("DataImportWorkgroupDataEntryColumnKeys");
            MakeTable("DataImportWorkMarkSense");
            MakeTable("DuplicateFileDetection");
            MakeTable("DuplicateTransactionDetection");
            MakeTable("ExceptionTransactions");
            MakeTable("factBatchData");
            MakeTable("factBatchSummary");
            MakeTable("factChecks");
            MakeTable("factDataEntryDetails");
            MakeTable("factDocuments");
            MakeTable("factItemData");
            MakeTable("factRawPaymentData");
            MakeTable("factStubs");
            MakeTable("factTransactionSummary");

            MakeTable("XMLBatch");
            MakeTable("XMLBatchData");
            MakeTable("XMLBatches");
            MakeTable("XMLDocument");
            MakeTable("XMLDocumentData");
            MakeTable("XMLDocumentItemData");
            MakeTable("XMLElectronicBatch");
            MakeTable("XMLGhostDocument");
            MakeTable("XMLGhostDocumentData");
            MakeTable("XMLGhostDocumentItemData");
            MakeTable("XMLPayment");
            MakeTable("XMLPaymentData");
            MakeTable("XMLPaymentItemData");
            MakeTable("XMLRawPaymentData");
            MakeTable("XMLTransaction");

            MakeStoredProcedure("usp_DataImportQueue_Get_PendingBatches");
            MakeStoredProcedure("usp_DataImportWorkBatch_Upd_Response");
            MakeStoredProcedure("usp_TruncateBatchDataWorkTables");

        }
    }
}
