﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Xml;
using DatabaseIntegrationTestHelpers;
using R360DatabaseIntegrationTestHelpers.DataObjects;
using WFS.RecHub.R360Services.Common.DTO.AdvancedSearch;

namespace R360DatabaseIntegrationTestHelpers
{
    public class RecHubData : R360DatabaseSchema
    {
        public bool ExecuteAdvancedSearch(TableParams tableParams, out XmlDocument xmlSearchResults, out DataTable dtSearchResults)
        {
            SqlParameter[] returnParms;
            DataTable dt;
            bool bolRetVal = false;

            xmlSearchResults = null;
            try
            {
                var parms = new SqlParameter[]
                {
                    Database.BuildParameter("@parmAdvancedSearchBaseKeysTable", SqlDbType.Structured, tableParams.BaseRequest),
                    Database.BuildParameter("@parmAdvancedSearchWhereClauseTable", SqlDbType.Structured, tableParams.WhereClause),
                    Database.BuildParameter("@parmAdvancedSearchSelectFieldsTable", SqlDbType.Structured, tableParams.SelectFields),
                    Database.BuildParameter("@parmSearchTotals", SqlDbType.Xml, null, ParameterDirection.Output)
                };

                Database.ExecuteProcedure("RecHubData.usp_ClientAccount_Search", parms, out returnParms, out dt);
                if(returnParms != null && returnParms[3] != null && returnParms[3].Value != null)
                {
                    try
                    {
                        xmlSearchResults = new XmlDocument();
                        if (returnParms[3] != null && returnParms[3].Value != null && returnParms[3].Value.ToString().Length > 0)
                        {
                            xmlSearchResults.LoadXml(returnParms[3].Value.ToString());
                            bolRetVal = true;
                        }
                        else
                        {
                            bolRetVal = false; //(Database.lastException == null);
                        }
                    }
                    catch (XmlException)
                    {
                        xmlSearchResults = new XmlDocument();
                    }
                    catch(Exception ex)
                    {
                        throw new Exception($"An Error occurred while executing ExecuteAdvancedSearch: {ex.Message}", ex);
                    }

                    dtSearchResults = dt;

                }
                else
                {
                    //xmlSearchResults = ipoXmlLib.GetXMLBase();
                    dtSearchResults = null;
                }
            }
            catch (Exception ex) 
            {
                throw new Exception($"An Error occurred while executing ExecuteAdvancedSearch: {ex.Message}", ex);
            }
            return bolRetVal;
        }

        public RecHubData(IntegrationDatabase database, DateTime runDate) : base(database, runDate)
        {
            //These are required fo all fact tables - some additional tables are needed for specific tables
            MakeTable("dimSiteCodes");
            MakeTable("dimBanks");
            MakeTable("dimOrganizations");
            MakeTable("dimClientAccounts");
            MakeTable("dimDates");
            MakeTable("dimDepositStatuses");
            MakeTable("dimBatchSources");
            MakeTable("dimBatchPaymentTypes");
            MakeTable("dimBatchExceptionStatuses");
            MakeTable("dimTransactionExceptionStatuses");

            //Populate defaults into the dim tables, these mostly match the static data XML defaults
            InsertDimDate(RunDate);
            InsertDimSiteCode(-1, shortName: "Default", longName: "Default");
            InsertDimBank(-1, bankName: "Undefined");
            InsertDimOrganizations(-1, name: "Undefined", description: "Undefined");
            InsertDimClientAccount(-1, dataRetentionDays: 0, imageRetentionDays: 0, shortName: "Undefined",
                longName: "Undefined");
            InsertDimDepositStatuses(1, -1, "Not Set");
            InsertDimDepositStatuses(2, 240, "Pending Decisioning");
            InsertDimDepositStatuses(3, 241, "In Decisioning");
            InsertDimDepositStatuses(5, 850, "Deposit Complete");
            InsertDimBatchExceptionStatuses(0, "NoException", "No Exception");
            InsertDimBatchSources(0,shortName: "Unknown", longName: "Unknown");
            InsertDimTransactionExceptionStatuses(0, "NoException", "No Exception");
        }

        public void InsertDimBank(int bankKey, int siteBankId = -1, int entityId = -1, string bankName = null,
            bool mostRecent = true, DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimBanks",
                new
                {
                    BankKey = bankKey,
                    SiteBankID = siteBankId,
                    MostRecent = mostRecent,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now,
                    BankName = bankName ?? $"Bank {siteBankId}"
                }, setIdentityInsert: true);
        }

        public void InsertDimBatchExceptionStatuses(int batchExceptionStatusKey, string exceptionStatusName = null,
            string exceptionStatusDescripion = null, DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimBatchExceptionStatuses",
                new
                {
                    BatchExceptionStatusKey = batchExceptionStatusKey,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now,
                    ExceptionStatusName = exceptionStatusName,
                    ExceptionStatusDescripion = exceptionStatusDescripion
                });
        }

        public void InsertDimBatchPaymentTypes(int batchPaymentTypeKey, string shortName = null, string longName = null)
        {
            Database.Insert($"{SchemaName}.dimBatchPaymentTypes",
                new
                {
                    BatchPaymentTypeKey = batchPaymentTypeKey,
                    ShortName = shortName ?? $"PaymentType {batchPaymentTypeKey}",
                    LongName = longName ?? $"Payment Type Long {batchPaymentTypeKey}"
                });
        }

        public void InsertDimBatchSources(int batchSourceKey, int importTypeKey = 0, int? entityId = null,
            string shortName = null, string longName = null, DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimBatchSources",
                new
                {
                    BatchSourceKey = batchSourceKey,
                    ImportTypeKey = importTypeKey,
                    entityId,
                    ShortName = shortName,
                    LongName = longName,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now
                }, setIdentityInsert: true);
        }

        public void InsertDimClientAccount(int clientAccountKey, int siteCodeId = -1, int siteBankId = -1,
            int siteOrganizationId = -1, int siteClientAccountId = -1, bool mostRecent = true, bool isActive = true,
            bool cutoff = false, int onlineColorMode = 0, bool isCommingled = false, int dataRetentionDays = 999,
            int imageRetentionDays = 999, Guid siteClientAccountKey = new Guid(), string shortName = null, string longName = null,
            DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimClientAccounts",
                new
                {
                    ClientAccountKey = clientAccountKey,
                    SiteCodeID = siteCodeId,
                    SiteBankID = siteBankId,
                    SiteOrganizationID = siteOrganizationId,
                    SiteClientAccountID = siteClientAccountId,
                    MostRecent = mostRecent,
                    IsActive = isActive,
                    Cutoff = cutoff,
                    OnlineColorMode = onlineColorMode,
                    IsCommingled = isCommingled,
                    DataRetentionDays = dataRetentionDays,
                    ImageRetentionDays = imageRetentionDays,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now,
                    SiteClientAccountKey = siteClientAccountKey == Guid.Empty ? Guid.NewGuid() : siteClientAccountKey,
                    ShortName = shortName ?? $"Workgroup {siteClientAccountId}",
                    LongName = longName ?? $"Workgroup Long {siteClientAccountId}"
                }, setIdentityInsert: true);
        }

        public void InsertDimDate(DateTime date)
        {
            var calendar = new GregorianCalendar();
            var quarter = (date.Month - 1) / 3 + 1;
            Database.Insert($"{SchemaName}.dimDates",
                new
                {
                    DateKey = date.ToDateKey(),
                    CalendarDate = date.Date,
                    CalendarDayMonth = date.Day,
                    CalendarDayYear = date.DayOfYear,
                    CalendarDayWeek = (int)date.DayOfWeek + 1,
                    CalendarDayName = date.DayOfWeek.ToString(),
                    CalendarWeek = calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Sunday),
                    CalendarMonth = date.Month,
                    CalendarMonthName = date.Month.ToString(),
                    CalendarQuarter = quarter,
                    CalendarQuarterName = $"Q{quarter} {date.Year}",
                    CalendarYear = date.Year,
                });
        }

        public void InsertDimDda(int ddaKey, string aba, string dda, DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimDDAs",
                new
                {
                    DDAKey = ddaKey,
                    ABA = aba,
                    DDA = dda,
                    CreationDate = creationDate ?? DateTime.Now
                }, setIdentityInsert: true);
        }

        public void InsertDimDepositStatuses(int depositStatusKey, int depositStatus, string despositDisplayName = null,
            DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimDepositStatuses",
                new
                {
                    DepositStatusKey = depositStatusKey,
                    DepositStatus = depositStatus,
                    DespositDisplayName = despositDisplayName ?? $"Deposit Status {depositStatus}",
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now
                });
        }

        public void InsertDimImportTypes(int importTypeKey, int isActive = 1, string shortName = null,
            DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimImportTypes",
                new
                {
                    ImportTypeKey = importTypeKey,
                    IsActive = isActive,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now,
                    ShortName = shortName
                });
        }

        public void InsertDimOrganizations(int organizationKey, int siteOrganizationId = -1, int siteBankId = -1,
            string name = null, string description = null, bool mostRecent = true, DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimOrganizations",
                new
                {
                    OrganizationKey = organizationKey,
                    SiteOrganizationID = siteOrganizationId,
                    SiteBankID = siteBankId,
                    MostRecent = mostRecent,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now,
                    Name = name ?? $"Organization {siteOrganizationId}",
                    Description = description ?? $"Description {siteOrganizationId}"
                }, setIdentityInsert: true);
        }

        public void InsertDimSiteCode(int siteCodeId, int cwDbActiveSite = 0, int localTimeZoneBias = 0,
            bool autoUpdateCurrentProcessingDate = true, string shortName = null, string longName = null,
            DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimSiteCodes",
                new
                {
                    SiteCodeID = siteCodeId,
                    CWDBActiveSite = cwDbActiveSite,
                    LocalTimeZoneBias = localTimeZoneBias,
                    AutoUpdateCurrentProcessingDate = autoUpdateCurrentProcessingDate,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now,
                    ShortName = shortName ?? $"Site {siteCodeId}",
                    LongName = longName ?? $"LongSite {siteCodeId}"
                });
        }

        public void InsertDimTransactionExceptionStatuses(int transactionExceptionStatusKey,
            string exceptionStatusName = null, string exceptionStatusDescripion = null, DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimTransactionExceptionStatuses",
                new
                {
                    TransactionExceptionStatusKey = transactionExceptionStatusKey,
                    ExceptionStatusName = exceptionStatusName ?? $"StatusName {transactionExceptionStatusKey}",
                    ExceptionStatusDescripion = exceptionStatusDescripion ?? $"StatusDescripion {transactionExceptionStatusKey}",
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now
                });
        }

        public void InsertDimWorkgroupDataEntryColumns(int workgroupDataEntryColumnKey, int siteBankId,
            int siteClientAccountId, int batchSourceKey, string fieldName, int dataType, bool isCheck = true,
            bool isActive = true, bool isRequired = true, bool markSense = false, int screenOrder = 1,
            string uiLabel = null, string sourceDisplayName = null, DateTime? creationDate = null)
        {
            Database.Insert($"{SchemaName}.dimWorkgroupDataEntryColumns",
                new
                {
                    WorkgroupDataEntryColumnKey = workgroupDataEntryColumnKey,
                    SiteBankID = siteBankId,
                    SiteClientAccountID = siteClientAccountId,
                    BatchSourceKey = batchSourceKey,
                    FieldName = fieldName,
                    DataType = dataType,
                    IsCheck = isCheck,
                    IsActive = isActive,
                    IsRequired = isRequired,
                    MarkSense = markSense,
                    ScreenOrder = screenOrder,
                    UILabel = uiLabel ?? fieldName,
                    SourceDisplayName = sourceDisplayName ?? "",
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = creationDate ?? DateTime.Now
                }, setIdentityInsert: true);
        }

        public void InsertBatch(Batch batch)
        {
            int autoBatchSequence = 1;
            int autoCheckSequence = 1;
            int autoStubSequence = 1;
            int autoTransactionId = 1;
            int autoTxnSequence = 1;

            foreach (var transaction in batch.Transactions)
            {
                var transactionId = transaction.TransactionId ?? autoTransactionId++;
                var txnSequence = transaction.TxnSequence ?? autoTxnSequence++;

                InsertFactTransactionSummary(batch.BatchId, batch.SourceBatchId, batch.BatchPaymentTypeKey,
                    transaction.TransactionExceptionStatusKey, transactionId, txnSequence,
                    batch.IsDeleted, batch.BankKey, batch.ClientAccountKey, batch.DepositDate,
                    batch.ImmutableDate, batch.SourceProcessingDate, batch.BatchNumber, batch.BatchSourceKey,
                    batch.BatchCueId, batch.DepositStatus, batch.SystemType, transaction.CheckCount,
                    transaction.ScannedCheckCount, transaction.StubCount, transaction.DocumentCount,
                    transaction.OmrCount, transaction.CheckTotal, transaction.StubTotal, batch.CreationDate,
                    batch.ModificationDate);

                if(transaction.Checks != null && transaction.Checks.Any())
                    foreach (var check in transaction.Checks)
                    {
                        var autoSequenceWithinTransaction = 1;

                        InsertFactCheck(batch.BatchId, batch.SourceBatchId, batch.BatchPaymentTypeKey, transactionId,
                            txnSequence, batch.IsDeleted, batch.BankKey, batch.ClientAccountKey,
                            batch.DepositDate, batch.ImmutableDate, batch.SourceProcessingDate, batch.BatchNumber,
                            batch.BatchSourceKey, batch.BatchCueId, batch.DepositStatus, check.DdaKey,
                            check.SequenceWithinTransaction ?? autoSequenceWithinTransaction++,
                            check.BatchSequence ?? autoBatchSequence, check.CheckSequence ?? autoCheckSequence++,
                            check.NumericRoutingNumber, check.NumericSerial, check.Amount, batch.BatchSiteCode,
                            check.RoutingNumber, check.Account, check.TransactionCode, check.Serial, check.RemitterName,
                            batch.CreationDate, batch.ModificationDate);

                        if (check.DataEntryRows != null && check.DataEntryRows.Any())
                        {
                            foreach (var dataEntryRow in check.DataEntryRows)
                            {
                                if (dataEntryRow.DataEntryValues != null && dataEntryRow.DataEntryValues.Any())
                                {
                                    foreach (var dataEntryValue in dataEntryRow.DataEntryValues)
                                    {
                                        InsertFactDataEntryDetails(batch.BatchId, batch.SourceBatchId, batch.BatchPaymentTypeKey,
                                            transactionId, dataEntryValue.WorkgroupDataEntryColumnKey, dataEntryValue.Value,
                                            batch.IsDeleted, batch.BankKey, batch.ClientAccountKey,
                                            batch.DepositDate, batch.ImmutableDate, batch.SourceProcessingDate, batch.BatchNumber,
                                            batch.BatchSourceKey, batch.DepositStatus, check.BatchSequence ?? autoBatchSequence,
                                            dataEntryValue.ValueDateTime, dataEntryValue.ValueFloat,
                                            dataEntryValue.ValueMoney, batch.CreationDate, batch.ModificationDate);
                                    }
                                }
                            }
                        }

                        autoBatchSequence++;
                    }

                if ( transaction.Stubs != null && transaction.Stubs.Any())
                    foreach (var stub in transaction.Stubs)
                    {
                        var autoSequenceWithinTransaction = 1;

                        InsertFactStub(batch.BatchId, batch.SourceBatchId, batch.BatchPaymentTypeKey, transactionId,
                            txnSequence, batch.IsDeleted, batch.BankKey, batch.ClientAccountKey,
                            batch.DepositDate, batch.ImmutableDate, batch.SourceProcessingDate, batch.BatchNumber,
                            batch.BatchSourceKey, batch.BatchCueId, batch.DepositStatus, batch.SystemType,
                            stub.SequenceWithinTransaction ?? autoSequenceWithinTransaction,
                            stub.BatchSequence ?? autoBatchSequence, stub.StubSequence ?? autoStubSequence++,
                            stub.IsCorrespondence, stub.DocumentBatchSequence, batch.BatchSiteCode, stub.IsOmrDetected,
                            stub.Amount, stub.AccountNumber, batch.CreationDate, batch.ModificationDate);

                        if (stub.DataEntryRows != null && stub.DataEntryRows.Any())
                        {
                            foreach (var dataEntryRow in stub.DataEntryRows)
                            {
                                if (dataEntryRow.DataEntryValues != null && dataEntryRow.DataEntryValues.Any())
                                {
                                    foreach (var dataEntryValue in dataEntryRow.DataEntryValues)
                                    {
                                        InsertFactDataEntryDetails(batch.BatchId, batch.SourceBatchId,
                                            batch.BatchPaymentTypeKey, transactionId,
                                            dataEntryValue.WorkgroupDataEntryColumnKey, dataEntryValue.Value,
                                            batch.IsDeleted, batch.BankKey, 
                                            batch.ClientAccountKey, batch.DepositDate, batch.ImmutableDate,
                                            batch.SourceProcessingDate, batch.BatchNumber, batch.BatchSourceKey,
                                            batch.DepositStatus, stub.BatchSequence ?? autoBatchSequence,
                                            dataEntryValue.ValueDateTime, dataEntryValue.ValueFloat,
                                            dataEntryValue.ValueMoney, batch.CreationDate, batch.ModificationDate);
                                    }
                                }
                            }

                            autoBatchSequence++;
                        }
                    }
            }
        }

        public void InsertFactBatchSummary(Int64 batchId, Int64 sourceBatchId, int batchPaymentTypeKey,
            int batchExceptionStatusKey, bool isDeleted = false, int bankKey = -1, 
            int clientAccountKey = -1, DateTime? depositDate = null, DateTime? immutableDate = null,
            DateTime? sourceProcessingDate = null, int? batchNumber = null, int batchSourceKey = 0, int batchCueId = 0,
            int depositStatusKey = -1, int depositStatus = 850, int systemType = 0, int transactionCount = 0,
            int checkCount = 0, int scannedCheckCount = 0, int stubCount = 0, int documentCount = 0,
            decimal checkTotal = 0, decimal stubTotal = 0, DateTime? creationDate = null,
            DateTime? modificationDate = null)
        {
            Database.Insert($"{SchemaName}.factBatchSummary",
                new
                {
                    IsDeleted = isDeleted,
                    BankKey = bankKey,
                    OrganizationKey = -1,
                    ClientAccountKey = clientAccountKey,
                    DepositDateKey = (depositDate ?? RunDate).ToDateKey(),
                    ImmutableDateKey = (immutableDate ?? RunDate).ToDateKey(),
                    SourceProcessingDateKey = (sourceProcessingDate ?? RunDate).ToDateKey(),
                    BatchID = batchId,
                    SourceBatchID = sourceBatchId,
                    BatchNumber = batchNumber ?? sourceBatchId,
                    BatchSourceKey = batchSourceKey,
                    BatchPaymentTypeKey = batchPaymentTypeKey,
                    BatchExceptionStatusKey = batchExceptionStatusKey,
                    BatchCueID = batchCueId,
                    DepositStatus = depositStatus,
                    DepositStatusKey = depositStatusKey,
                    SystemType = systemType,
                    TransactionCount = transactionCount,
                    CheckCount = checkCount,
                    ScannedCheckCount = scannedCheckCount,
                    StubCount = stubCount,
                    DocumentCount = documentCount,
                    CheckTotal = checkTotal,
                    StubTotal = stubCount,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = modificationDate ?? DateTime.Now
                });
        }

        public void InsertFactCheck(Int64 batchId, Int64 sourceBatchId, int batchPaymentTypeKey, int transactionId,
            int txnSequence, bool isDeleted = false, int bankKey = -1, 
            int clientAccountKey = -1, DateTime? depositDate = null, DateTime? immutableDate = null,
            DateTime? sourceProcessingDate = null, Int64? batchNumber = null, int batchSourceKey = 0, int batchCueId = 0,
            int depositStatus = 850, int ddaKey = 0, int sequenceWithinTransaction = 1, int batchSequence = 1,
            int checkSequence = 1, long numericRoutingNumber = 0L, long numericSerial = 0L, decimal amount = 0,
            int? batchSiteCode = null, string routingNumber = "0", string account = "0",
            string transactionCode = null, string serial = null, string remitterName = null,
            DateTime? creationDate = null, DateTime? modificationDate = null)
        {
            Database.Insert($"{SchemaName}.factChecks",
                new
                {
                    IsDeleted = isDeleted,
                    BankKey = bankKey,
                    OrganizationKey = -1,
                    ClientAccountKey = clientAccountKey,
                    DepositDateKey = (depositDate ?? RunDate).ToDateKey(),
                    ImmutableDateKey = (immutableDate ?? RunDate).ToDateKey(),
                    SourceProcessingDateKey = (sourceProcessingDate ?? RunDate).ToDateKey(),
                    BatchID = batchId,
                    SourceBatchID = sourceBatchId,
                    BatchNumber = batchNumber ?? sourceBatchId,
                    BatchSourceKey = batchSourceKey,
                    BatchPaymentTypeKey = batchPaymentTypeKey,
                    DDAKey = ddaKey,
                    BatchCueID = batchCueId,
                    DepositStatus = depositStatus,
                    TransactionID = transactionId,
                    TxnSequence = txnSequence,
                    SequenceWithinTransaction = sequenceWithinTransaction,
                    BatchSequence = batchSequence,
                    CheckSequence = checkSequence,
                    NumericRoutingNumber = numericRoutingNumber,
                    NumericSerial = numericSerial,
                    Amount = amount,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = modificationDate ?? DateTime.Now,
                    BatchSiteCode = batchSiteCode,
                    RoutingNumber = routingNumber,
                    Account = account,
                    TransactionCode = transactionCode,
                    Serial = serial,
                    RemitterName = remitterName
                });
        }

        public void InsertFactDataEntryDetails(Int64 batchId, Int64 sourceBatchId, int batchPaymentTypeKey,
            int transactionId, Int64 workgroupDataEntryColumnKey, string dataEntryValue, bool isDeleted = false,
            int bankKey = -1, int clientAccountKey = -1, DateTime? depositDate = null,
            DateTime? immutableDate = null, DateTime? sourceProcessingDate = null, Int64? batchNumber = null,
            int batchSourceKey = 0, int depositStatus = 850, int batchSequence = 1,
            DateTime? dataEntryValueDateTime = null, float? dataEntryValueFloat = null,
            decimal? dataEntryValueMoney = null, DateTime? creationDate = null, DateTime? modificationDate = null)
        {
            Database.Insert($"{SchemaName}.factDataEntryDetails",
                new
                {
                    IsDeleted = isDeleted,
                    BankKey = bankKey,
                    OrganizationKey = -1,
                    ClientAccountKey = clientAccountKey,
                    DepositDateKey = (depositDate ?? RunDate).ToDateKey(),
                    ImmutableDateKey = (immutableDate ?? RunDate).ToDateKey(),
                    SourceProcessingDateKey = (sourceProcessingDate ?? RunDate).ToDateKey(),
                    BatchID = batchId,
                    SourceBatchID = sourceBatchId,
                    BatchNumber = batchNumber ?? sourceBatchId,
                    BatchSourceKey = batchSourceKey,
                    BatchPaymentTypeKey = batchPaymentTypeKey,
                    DepositStatus = depositStatus,
                    WorkgroupDataEntryColumnKey = workgroupDataEntryColumnKey,
                    TransactionID = transactionId,
                    BatchSequence = batchSequence,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = modificationDate ?? DateTime.Now,
                    DataEntryValueDateTime = dataEntryValueDateTime,
                    DataEntryValueFloat = dataEntryValueFloat,
                    DataEntryValueMoney = dataEntryValueMoney,
                    DataEntryValue = dataEntryValue
                });
        }

        public void InsertFactStub(Int64 batchId, Int64 sourceBatchId, int batchPaymentTypeKey, int transactionId,
            int txnSequence, bool isDeleted = false, int bankKey = -1, 
            int clientAccountKey = -1, DateTime? depositDate = null, DateTime? immutableDate = null,
            DateTime? sourceProcessingDate = null, Int64? batchNumber = null, int batchSourceKey = 0,
            int batchCueId = 0, int depositStatus = 850, int systemType = 0, int sequenceWithinTransaction = 1,
            int batchSequence = 1, int stubSequence = 1,
            bool isCorrespondence = false, int? documentBatchSequence = null, int? batchSiteCode = null,
            bool? isOmrDetected = null, decimal? amount = null, string accountNumber = null,
            DateTime? creationDate = null, DateTime? modificationDate = null)
        {
            Database.Insert($"{SchemaName}.factStubs",
                new
                {
                    IsDeleted = isDeleted,
                    BankKey = bankKey,
                    OrganizationKey = -1,
                    ClientAccountKey = clientAccountKey,
                    DepositDateKey = (depositDate ?? RunDate).ToDateKey(),
                    ImmutableDateKey = (immutableDate ?? RunDate).ToDateKey(),
                    SourceProcessingDateKey = (sourceProcessingDate ?? RunDate).ToDateKey(),
                    BatchID = batchId,
                    SourceBatchID = sourceBatchId,
                    BatchNumber = batchNumber ?? sourceBatchId,
                    BatchSourceKey = batchSourceKey,
                    BatchPaymentTypeKey = batchPaymentTypeKey,
                    BatchCueID = batchCueId,
                    DepositStatus = depositStatus,
                    SystemType = systemType,
                    TransactionID = transactionId,
                    TxnSequence = txnSequence,
                    SequenceWithinTransaction = sequenceWithinTransaction,
                    BatchSequence = batchSequence,
                    StubSequence = stubSequence,
                    IsCorrespondence = isCorrespondence,
                    DocumentBatchSequence = documentBatchSequence,
                    BatchSiteCode = batchSiteCode,
                    IsOMRDetected = isOmrDetected,
                    Amount = amount,
                    AccountNumber = accountNumber,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = modificationDate ?? DateTime.Now
                });
        }

        public void InsertFactTransactionSummary(Int64 batchId, Int64 sourceBatchId, int batchPaymentTypeKey,
            int transactionExceptionStatusKey, int transactionId, int txnSequence, bool isDeleted = false,
            int bankKey = -1, int clientAccountKey = -1, DateTime? depositDate = null,
            DateTime? immutableDate = null, DateTime? sourceProcessingDate = null, Int64? batchNumber = null,
            int batchSourceKey = 0, int batchCueId = 0, int depositStatus = 850, int systemType = 0, int checkCount = 0,
            int scannedCheckCount = 0, int stubCount = 0, int documentCount = 0, int omrCount = 0,
            decimal checkTotal = 0, decimal stubTotal = 0, DateTime? creationDate = null,
            DateTime? modificationDate = null)
        {
            Database.Insert($"{SchemaName}.factTransactionSummary",
                new
                {
                    IsDeleted = isDeleted,
                    BankKey = bankKey,
                    OrganizationKey = -1,
                    ClientAccountKey = clientAccountKey,
                    DepositDateKey = (depositDate ?? RunDate).ToDateKey(),
                    ImmutableDateKey = (immutableDate ?? RunDate).ToDateKey(),
                    SourceProcessingDateKey = (sourceProcessingDate ?? RunDate).ToDateKey(),
                    BatchID = batchId,
                    SourceBatchID = sourceBatchId,
                    BatchNumber = batchNumber ?? sourceBatchId,
                    BatchSourceKey = batchSourceKey,
                    BatchPaymentTypeKey = batchPaymentTypeKey,
                    TransactionExceptionStatusKey = transactionExceptionStatusKey,
                    BatchCueID = batchCueId,
                    DepositStatus = depositStatus,
                    SystemType = systemType,
                    TransactionID = transactionId,
                    TxnSequence = txnSequence,
                    CheckCount = checkCount,
                    ScannedCheckCount = scannedCheckCount,
                    StubCount = stubCount,
                    DocumentCount = documentCount,
                    OMRCount = omrCount,
                    CheckTotal = checkTotal,
                    StubTotal = stubCount,
                    CreationDate = creationDate ?? DateTime.Now,
                    ModificationDate = modificationDate ?? DateTime.Now
                });
         }
    }
}
