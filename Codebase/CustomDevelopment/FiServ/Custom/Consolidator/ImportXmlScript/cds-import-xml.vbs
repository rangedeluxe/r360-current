Option Explicit

CONST APP_NAME   = "Consolidator Load Script"
CONST LOG_FILE   = "importlog.txt"

Dim SiteCode
Dim m_objCWDBOptions
Dim m_objEventLog
Dim m_objCWDBParser
Dim m_objCWDBServer
Dim m_objShell
Dim mobjFSO
Dim i

'initialize variables
Set m_objCWDBOptions = Nothing
Set m_objEventLog = Nothing
Set m_objCWDBParser = Nothing
Set m_objCWDBServer = Nothing
Set m_objShell = Nothing
Set mobjFSO = Nothing

Dim LogFile

forceCScriptExecution

EventLog.FileName = fso.BuildPath(Shell.CurrentDirectory,LOG_FILE)

Main()

'clean up
Set m_objCWDBOptions = Nothing
Set m_objEventLog = Nothing
Set m_objCWDBParser = Nothing
Set m_objCWDBServer = Nothing
Set m_objShell = Nothing
Set mobjFSO = Nothing


Private Sub Main()

	Dim strFileName
	Dim strPrompt
	Dim xmlSource
	Dim nodeSiteCode
	Dim blnError

	RecordEvent "Consolidator Import Script started."
	RecordEvent "Source parameter: " & GetInputFilename()

	strFileName = VerifiedFilename(GetInputFilename())

	If Len(strFileName) > 0  then

		strPrompt = "Importing the following file into the consolidation database:" _
			& vbcrlf & vbcrlf _
			& strFileName _
			& vbcrlf & vbcrlf _
			& "Do you want to continue?"

		If MsgBox (strPrompt, vbYesNoCancel+vbQuestion, APP_NAME) = vbYes Then

			'load XML document
			If (GetXMLDocument (strFilename, xmlSource)) Then
				If xmlSource is Nothing Then
					blnError = True
					RecordEvent "Import cancelled. An error occurred while attempting to load Xml file " & strFilename & "."
					msgbox "Unable to load Xml file.", vbOKOnly+vbCritical, APP_NAME
				End If
			Else
				blnError = True
				RecordEvent "Import cancelled. An error occurred while attempting to load Xml file " & strFilename & "."
				msgbox "Unable to load Xml file.", vbOKOnly+vbCritical, APP_NAME
			End If

			If not blnError Then
				'retrieve SiteCode from document
				Set nodeSiteCode = xmlSource.documentElement.selectSingleNode("@SiteCode")
				SiteCode = nodeSiteCode.nodeTypedValue

				if (SiteCode > 0) Then
					'kick off import process
					ImportXML xmlSource, strFilename
				else
					RecordEvent "Import cancelled. Xml source file does not contain a valid SiteCode."
					msgbox "Import cancelled.  Xml source document does not contain a valid SiteCode", vbOKOnly+vbCritical, APP_NAME
				end if

				RecordEvent "Import complete."
				msgbox "Import complete.", vbOKOnly+vbInformation, APP_NAME
			End If
		Else
			RecordEvent "Import cancelled by user."
			msgbox "Import cancelled.", vbOKOnly+vbCritical, APP_NAME
		End If
	Else
		RecordEvent "Import cancelled. Xml source file parameter missing or invalid."
		msgbox "Import cancelled.  No XML document provided.", vbOKOnly+vbCritical, APP_NAME
	End If

	RecordEvent "Consolidator Import Script completed."
End Sub


Private Function ImportXML (ByRef xmlSource, ByVal Filename)

	Dim xmlSrc
	Dim xmlRtn
	Dim xmlReturn
	Dim nodeBankID, nodeLockboxID, nodeProcessingDate, nodeBatchID
	Dim blnError
	Dim strTargetFile
	Dim strFile

	If Not CWDBServer.InitializeService (SiteCode) Then
		blnError = true
	End If

	InitializeCWDB()

	RecordEvent "Source File: " & Filename
	RecordEvent "SiteCode: " & SiteCode
	'RecordEvent "Database: " & CWDBOptions.DBConnection

	'identify if batch Xml. If yes then delete existing batch before sending
	Set nodeBankID = xmlSource.documentElement.selectSingleNode("Batch_Data/Batch/BankID")
	Set nodeLockboxID = xmlSource.documentElement.selectSingleNode("Batch_Data/Batch/LockboxID")
	Set nodeProcessingDate = xmlSource.documentElement.selectSingleNode("Batch_Data/Batch/ProcessingDate")
	Set nodeBatchID = xmlSource.documentElement.selectSingleNode("Batch_Data/Batch/BatchID")

	If Not nodeBankID is Nothing Then
		RecordEvent "Deleting previous instance of batch " _
					& " [BankID=" & cstr(nodeBankID.nodeTypedValue) _
					& ", LockboxID=" & cstr(nodeLockboxID.nodeTypedValue) _
					& ", ProcessingDate=" & cstr(nodeProcessingDate.nodeTypedValue) _
					& ", BatchID=" & cstr(nodeBatchID.nodeTypedValue) & "]."

		If Not CWDBServer.DeleteBatch (CLng(nodeBankID.nodeTypedValue),CLng(nodeLockboxID.nodeTypedValue), cdate(nodeProcessingDate.nodeTypedValue), clng(nodeBatchID.nodeTypedValue)) Then
			blnError = True
			RecordEvent "Delete of batch failed."
		End If
	End If

	If Not blnError Then

		RecordEvent "Sending source Xml..."

		Set xmlSrc = xmlSource.DocumentAsAttachment

		On Error Resume Next
		Set xmlRtn = CWDBServer.SendXMLDoc (xmlSrc)
		If Err.Number <> 0 Then
			RecordEvent "Error " & Err.Number & " - " & Err.Description
		End If
		On Error Goto 0

		RecordEvent "Retreiving Xml response..."

		' load return Xml
		Set xmlReturn = CWDBTransferDoc
		xmlReturn.Document.validateOnParse = False
		xmlReturn.Document.async = False

		xmlReturn.Document.Load xmlRtn

		Dim datNow
		Dim strNow

		datNow = CDate(Now())
		strNow = Year(datNow) & Right("0" & Month(datNow), 2) & Right("00" & Day(datNow), 2) & Right("00" & Hour(datNow), 2) & Right("00" & Minute(datNow), 2) & Right("00" & Second(datNow), 2)

		strFile = fso.GetBaseName(FileName) & "-results-" & strNow & ".xml"

		strTargetFile = fso.BuildPath(GetCurrentDirectory(FileName), strFile)

		If fso.FileExists(strTargetFile) Then
			fso.DeleteFile strTargetFile, True
		End If

		xmlReturn.Document.save strTargetFile
		RecordEvent "Import completed. Results saved to file '" & strTargetFile & "'"
	Else
		RecordEvent "Error occurred during import."
	End If

End Function


Private Function GetXMLDocument (ByVal Filename, ByRef xmlDoc)
	Set xmlDoc = CWDBTransferDoc
	xmlDoc.Document.validateOnParse = 0
	xmlDoc.Document.async = False
	xmlDoc.Document.Load Filename

	GetXMLDocument = true
End Function


Private Function InitializeCWDB()
	'initialize server
	'With CWDBServer
		'.DBConnection = CWDBOptions.DBConnection
		'.Logfile = EventLog.FileName
		'.ImagePath = CWDBOptions.ImagePath
		'.DisableGMTAdjustment = CWDBOptions.DisableGMTAdjustment
		'.TimeZoneBias = CWDBOptions.TimeZoneBias
		'.CENDSPath = CWDBOptions.CENDSPath
		'.SiteCode = CWDBOptions.SiteCode
	'End With

	'initialize parser
	With CWDBParser
		.DBConnection = CWDBOptions.DBConnection
		.Logfile = EventLog.FileName
		.ImagePath = CWDBOptions.ImagePath
		.DisableGMTAdjustment = CWDBOptions.DisableGMTAdjustment
		.TimeZoneBias = CWDBOptions.TimeZoneBias
		.CENDSPath = CWDBOptions.CENDSPath
		.SiteCode = CWDBOptions.SiteCode
	End With
End Function


Private Function GetInputFilename()
	Dim objArgs

	Set objArgs = WScript.Arguments

	If objArgs.Count <> 1 Then
		DisplayUsageInfo
	Else
		If InStr(objArgs(0),"?") Then
			DisplayUsageInfo()
		Else
			GetInputFilename = Trim(objArgs(0))
		End If
	End If
End Function


Private Function VerifiedFilename(ByVal Filename)
	'does file exist
	If fso.FileExists (Filename) Then
		VerifiedFilename = Filename
		VerifiedFilename = fso.GetAbsolutePathName(filename)
	Else
		'check if file exists in current directory
		If fso.FileExists (fso.BuildPath(Shell.CurrentDirectory,Filename)) Then
			VerifiedFilename = fso.BuildPath(Shell.CurrentDirectory,Filename)
		Else
			VerifiedFilename = ""
			RecordEvent "Xml import file: " & "<error: file not found>"
		End If
	End If
End Function


Private Function GetCurrentDirectory (ByVal Filename)
	If fso.FileExists (Filename) Then
		GetCurrentDirectory = fso.GetParentFolderName(filename)
	Else
		GetCurrentDirectory = Shell.CurrentDirectory
	End If
End Function


Private Sub DisplayUsageInfo()
	dim strUsage

	strUsage = "Imports a Consolidator source Xml file directly into the CDS." _
		& vbcrlf & vbcrlf _
		& "CScript.exe " & WScript.ScriptName & " [source.xml]" _
		& vbcrlf & vbcrlf _
		& "    [source.xml]" & vbtab & "Specifies Consolidator Xml source file to import."

	msgbox strUsage, vbOKOnly+vbInformation, APP_NAME
End Sub


Private Function LoadSiteCode(ByVal SiteCode)
	If CWDBOptions.RestoreService (SiteCode) Then
		LoadSiteCode = True
	Else
		LoadSiteCode = False
	End If
End Function


Private Function CWDBOptions()
    If m_objCWDBOptions Is Nothing Then
		Set m_objCWDBOptions = CreateObject ("CBXCWDB.cOptions")
		m_objCWDBOptions.RestoreService (SiteCode)
    End If
    Set CWDBOptions = m_objCWDBOptions
End Function

Private Function EventLog()
    If m_objEventLog Is Nothing Then
        Set m_objEventLog = CreateObject ("CBXLog.EventLog")
        'm_objEventLog.FileName = SystemOption.Logfile
    End If
    Set EventLog = m_objEventLog
End Function

Private Function CWDBParser()
    If m_objCWDBParser Is Nothing Then
        Set m_objCWDBParser = CreateObject ("CBXCWDB.cCWDBParser")
    End If
    Set CWDBParser = m_objCWDBParser
End Function


Private Function CWDBServer()
    If m_objCWDBServer Is Nothing Then
        Set m_objCWDBServer = CreateObject ("CBXCWDB.cCWDBServer")
    End If
    Set CWDBServer = m_objCWDBServer
End Function


Private Function Shell()
    If m_objShell Is Nothing Then
        Set m_objShell = CreateObject ("WScript.Shell")
    End If
    Set Shell = m_objShell
End Function


Private Function fso()
	If mobjFSO Is Nothing Then
		Set mobjFSO = CreateObject("Scripting.FileSystemObject")
	End If
	Set fso = mobjFSO
End Function


Private Function CWDBTransferDoc()
	Set CWDBTransferDoc = CreateObject ("CBXCWDB.cTransferDoc")
End Function


Private Sub RecordEvent (ByVal EventText)
	On Error Resume Next
	WScript.Echo FormatDateTime(Now()) & " " & EventText
	EventLog.LogEvent cstr(EventText), "CDSImport", 1
	On Error Goto 0
End Sub

Private Sub forceCScriptExecution
	Dim Arg, Str
	If Not LCase( Right( WScript.FullName, 12 ) ) = "\cscript.exe" Then
		For Each Arg In WScript.Arguments
			If InStr( Arg, " " ) Then Arg = """" & Arg & """"
			Str = Str & " " & Arg
		Next
		CreateObject( "WScript.Shell" ).Run "cscript //nologo """ & WScript.ScriptFullName & """" & Str
		WScript.Quit
	End If
End Sub

