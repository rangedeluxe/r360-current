Usage of CDS Import Xml Script
==============================

1.  Execute the script cds-import-xml.vbs from a command prompt directly on the Consolidation server.

2.  Usage of the script is

    d:\>cds-import-xml.vbs <source.xml>

3.  After the script completes check the logfile and results-<timestamp>.xml file to verify that the
    script was successfully able to process the source Xml.

    If the script failed any errors should be logged to indicate what the issues encountered were.
	These issues will need to be checked and resolved before the source Xml will successfully import.

4.  If the script was successfully able to process the source Xml there may be additional steps to
    take at the Consolidator client to perform the same actions Consolidator would after a successful
    attempt.   These steps tell Consolidator that basically the action is now complete and Consolidator
    will continue to check, but not perform the same action until data changes require it.

    With tables like Instructions, for example, the <sourcefile>-return-<timestamp>.xml needs to be
    copied back to the Consolidation client and placed in the IPOnline\Data\LogFiles directory with
	the file renamed to cache_Instructions.xml.

    With other tables like Contacts or Products, the Consolidation client CBXTransfer.INI would need
    to be updated so that the corresponding timestamp is updated to the timestamp embedded in the
	results Xml filename.

	Instructions for all additional steps to address the specific issue would be provided along with
	the CDS Xml import script.


