﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.integraPAY.Online.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR  45986 WJS 10/14/2011 Initial Release
******************************************************************************/
namespace WFS.CustomImage
{
    public class _ServiceBase
    {
        protected cEventLog _EventLog = null;
        private string _logFilePath = string.Empty;
        private int _logFileMaxSize = 1024;
        private int _logDepth = 2;
        
        public _ServiceBase()
        {
            int tempInt  = 0;
            _logFilePath = ltaConfig.ReadAppConfig("/FiservCustomWebService", "LogFile");
             if (Int32.TryParse(ltaConfig.ReadAppConfig("/FiservCustomWebService", "LogFileMaxSize"), out tempInt))
            {
                _logFileMaxSize = tempInt;
            }
            if (Int32.TryParse(ltaConfig.ReadAppConfig("/FiservCustomWebService", "LoggingDepth"), out tempInt))
            {
                _logDepth = tempInt;
            }
       
        }

        protected cEventLog EventLog
        {
            get
            {
                if (_EventLog == null)
                {

                    _EventLog = new cEventLog(_logFilePath,
                                              _logFileMaxSize,
                                              (MessageImportance)_logDepth);
                }
                return _EventLog;
            }
        }


        protected ServerFaultException BuildGeneralException(Exception ex, string procName, string Msg)
        {
            EventLog.logEvent("A general exception occurred", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return (BuildServerFaultException(OLFServiceErrorCodes.UndefinedError, Msg, Msg));
        }

        

        protected ServerFaultException BuildCommunicationsChannelException(Exception ex, string procName, string Msg)
        {
            EventLog.logEvent("Unable to establish communications channel - " + Msg, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return (BuildServerFaultException(OLFServiceErrorCodes.CommunicationError,
                                             "A communications channel occurred at the server",
                                             "A communications channel occurred at the server"));
        }

        private static ServerFaultException BuildServerFaultException(OLFServiceErrorCodes ErrorCode, string Msg, string Details)
        {

            ServerFaultException objError;

            objError = new ServerFaultException();
            objError.errorcode = "Error";
            objError.message = ((int)ErrorCode).ToString() + " - " + Msg;
            objError.details = Details;

            return (objError);
        }

        protected ServerFaultException BuildAppConfigException(Exception ex, string procName,  string key)
        {
            EventLog.logEvent("Unable to read app setting:" + key, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return (BuildServerFaultException(OLFServiceErrorCodes.ConfigurationError,
                                             "A configuration error occurred at the server",
                                             "A configuration error occurred at the server"));
        }

    }
}