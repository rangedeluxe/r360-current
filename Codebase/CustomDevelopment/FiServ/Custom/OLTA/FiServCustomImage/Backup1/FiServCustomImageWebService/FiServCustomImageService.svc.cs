﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WFS.integraPAY.Online.Common;
using OLTAImageRetrieval;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR  45986 WJS 10/14/2011 Initial Release
******************************************************************************/
namespace WFS.CustomImage
{
    public class FiServCustomImageService : _ServiceBase, IFiServCustomImage, IDisposable
    {
        private IFiServCustomImage _CustomImageService = null;
        private string _url = string.Empty;

        public FiServCustomImageService()
        {
            _url = OperationContext.Current.RequestContext.RequestMessage.Headers.To.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        public FiServCustomImageService(string url)
        {
            _url = url;
           
        }
        /// <summary>
        /// 
        /// </summary>
        ~FiServCustomImageService() 
        {
           
            // Finalizer calls Dispose(false)
            Dispose(false);
        }
        // Dispose() calls Dispose(true)
        public void Dispose()
        {
           
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
           
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
       

       

        // Opens the communication channel to service
        private void OpenCommunication()
        {
            ServerFaultException error;
            // Setup Binding
            BasicHttpBinding basicHTTPBinding = new BasicHttpBinding();
            basicHTTPBinding.MaxReceivedMessageSize = 67108864;
            basicHTTPBinding.MessageEncoding = WSMessageEncoding.Mtom;
            basicHTTPBinding.TransferMode = TransferMode.Streamed;
            EventLog.logEvent("Open Communication", MessageImportance.Debug);
            // Retrieve URL & set endpoint
            String strWebServiceLocation = _url;
            EndpointAddress endpointaddress = new EndpointAddress(strWebServiceLocation);
            EventLog.logEvent("EndpointAddress:" + strWebServiceLocation, MessageImportance.Debug);
            // Open channel
            try
            {
                _CustomImageService = new ChannelFactory<IFiServCustomImage>(basicHTTPBinding, endpointaddress).CreateChannel();
            }
            catch (Exception ex)
            {
                error = BuildCommunicationsChannelException(ex, "OpenCommunication", "Unable to construct Channel Factory: [" + strWebServiceLocation + "].");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
            EventLog.logEvent("OpenCommunication complete", MessageImportance.Debug);
        }

        // Ping
        public string Ping()
        {
            ServerFaultException error;
            try
            {
                // Setup Communication Channel
                OpenCommunication();
                // Call method
                return "Success";
                
            }
            catch (Exception ex)
            {
                error = BuildGeneralException(ex, "Ping", "A general exception occurred while executing Ping() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }
        private RetrievalObjectClass.ImageFormatVal GetImageFormat(string imageFormatVal)
        {
            bool bSuccess = false;
            RetrievalObjectClass.ImageFormatVal imageFormat = RetrievalObjectClass.ImageFormatVal.UNKNOWN;
            int imageFormatInt = 0;
            if (Int32.TryParse(imageFormatVal, out imageFormatInt))
            {
                if (typeof(RetrievalObjectClass.ImageFormatVal).IsEnumDefined(imageFormatInt))
                {
                    imageFormat = (RetrievalObjectClass.ImageFormatVal)imageFormatInt;
                    bSuccess = true;
                }
            }

            if (!bSuccess)
            {
                EventLog.logEvent("Unable to determine image format", MessageImportance.Essential);
            }
            return imageFormat;
        }
        private RetrievalObjectClass.ImageTypeVal GetImageType(string imageTypeVal)
        {
            bool bSuccess = false;
            RetrievalObjectClass.ImageTypeVal imageType = RetrievalObjectClass.ImageTypeVal.UNKNOWN;
            int imageTypeInt = 0;
            if (Int32.TryParse(imageTypeVal, out imageTypeInt))
            {
                if (typeof(RetrievalObjectClass.ImageTypeVal).IsEnumDefined(imageTypeInt))
                {
                    imageType = (RetrievalObjectClass.ImageTypeVal)imageTypeInt;
                    bSuccess = true;
                }
            }
            if (!bSuccess)
            {
                EventLog.logEvent("Unable to determine image type", MessageImportance.Essential);
            }
            return imageType;
        }
        public RetrievalObjectClass[] RetrieveImage(DateTime pProcessingDate, int pLbxNo, int pBatchNo, int pBankId, int pLocId, int pTransaction, int pBatchSeqNo)
        {
            RetrievalObjectClass[] retImages = new OLTAImageRetrieval.RetrievalObjectClass[1];

            RetrievalObjectClass image = new OLTAImageRetrieval.RetrievalObjectClass();
            int batchSeq =0;

            EventLog.logEvent("Retrive Image starting ", MessageImportance.Verbose);
           
            if (!Int32.TryParse(ltaConfig.ReadAppConfig("/FiservCustomWebService", "BatchSeqNo"), out batchSeq))
            {
                EventLog.logEvent("Failed to convert batchSeq to number", MessageImportance.Essential);
            }
            else
            {
                image.BatchSeqNo = batchSeq;
            }
            image.ImageFormat = GetImageFormat(ltaConfig.ReadAppConfig("/FiservCustomWebService", "imageFormat"));
            image.ImageType = GetImageType(ltaConfig.ReadAppConfig("/FiservCustomWebService", "ImageType"));
            image.ExtraData1 = ltaConfig.ReadAppConfig("/FiservCustomWebService", "ExtraData1");
            
            image.ImageFilePath = ltaConfig.ReadAppConfig("/FiservCustomWebService", "imageFilename");
            EventLog.logEvent("Found  Image with name: " + image.ImageFilePath, MessageImportance.Verbose);
            retImages[0] = image;
            return retImages;
        }
       
    }
}
