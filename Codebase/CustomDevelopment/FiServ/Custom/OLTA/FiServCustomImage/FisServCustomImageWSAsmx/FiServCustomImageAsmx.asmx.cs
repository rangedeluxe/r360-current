﻿using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using OLTAImageRetrieval;
using WFS.RecHub.Common.Log;
using WFS.RecHub.Common;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR  45986 WJS 10/14/2011 Initial Release

* CR 50543 WJS 2/23/2012
*	  - Added some logging
* CR 50982 WJS 3/8/2012
*	  - Remove enum types
* CR 53508 WJS 6/16/2012
*	- Add ability to retrieve 10 circular image
******************************************************************************/
namespace OLTAImageRetrievalWebService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class FiServCustomImageAsmx : System.Web.Services.WebService
    {

        protected cEventLog _EventLog = null;
        private string _logFilePath = string.Empty;
        private int _logFileMaxSize = 1024;
        private int _logDepth = 2;
        private IConfigurationProvider configurationProvider;
        private int _imageType = 0;
        private int _imageFormat = 0;
        private string _extraData;
        private string _imagePath;

        public FiServCustomImageAsmx()
        {
            int tempInt  = 0;
            configurationProvider = new ConfigurationManagerProvider();
            _logFilePath = configurationProvider.GetSetting("LogFile");
             if (Int32.TryParse(configurationProvider.GetSetting("LogFileMaxSize"), out tempInt))
            {
                _logFileMaxSize = tempInt;
            }
            if (Int32.TryParse(configurationProvider.GetSetting("LoggingDepth"), out tempInt))
            {
                _logDepth = tempInt;
            }
            Int32.TryParse(configurationProvider.GetSetting("imageFormat"), out _imageFormat);
            Int32.TryParse(configurationProvider.GetSetting("ImageType"), out _imageType);
            _extraData = configurationProvider.GetSetting("ExtraData1");
            _imagePath = configurationProvider.GetSetting("ImagePath");
            if (_imagePath == null) throw new ConfigurationErrorsException("Image path not configured correctly.");
            if (!_imagePath.EndsWith("\\")) { _imagePath = $"{_imagePath}\\"; }
        }

        protected cEventLog EventLog
        {
            get
            {
                if (_EventLog == null)
                {

                    _EventLog = new cEventLog(_logFilePath,
                                              _logFileMaxSize,
                                              (MessageImportance)_logDepth);
                }
                return _EventLog;
            }
        }

        [WebMethod]
        public OLTAImageRetrieval.RetrievalObjectClass[] RetrieveImage(DateTime pProcessingDate, int pLbxNo, int pBatchNo, int pBankId, int pLocId, int pTransaction, int pBatchSeqNo)
        {
            var retImages = new List<RetrievalObjectClass>();
            var processingDateKey = pProcessingDate.ToString("yyyyMMdd", CultureInfo.CurrentCulture);
            
            EventLog.logEvent($"Starting image retrieval from Viewpointe mock service for Processing Date: " +
                $"{pProcessingDate.ToString("yyyyMMdd", CultureInfo.CurrentCulture)} " +
                $"LockboxId: {pLbxNo.ToString()} BatchId: {pBatchNo.ToString()} " +
                $"SiteID: {pLocId.ToString()} TransactionId: {pTransaction.ToString()} " +
                $"BatchSequence: {pBatchSeqNo.ToString()}", MessageImportance.Debug);

            string imageDirectory = $"{_imagePath}{processingDateKey}\\{pBankId}\\{pLocId}\\{pLbxNo}\\{pBatchNo}";
            EventLog.logEvent($"Looking for files in directory: {imageDirectory}", MessageImportance.Verbose);

            if (!Directory.Exists(imageDirectory)) return retImages.ToArray();

            var imageFiles = Directory.GetFiles(imageDirectory);
            var images = imageFiles.Where(x => x.Contains($"_{pBatchSeqNo}_"));
            var frontImages = images.Where(x => x.Contains("_f") || x.Contains("_F"));
            var backImages = images.Where(x => x.Contains("_b") || x.Contains("_B"));
            var frontImage = CreateRetrievalObject(pBatchSeqNo,  frontImages == null ? string.Empty : frontImages.FirstOrDefault());
            var backImage = CreateRetrievalObject(pBatchSeqNo,  backImages == null ? string.Empty : backImages.FirstOrDefault());
            
            EventLog.logEvent($"Front image: {frontImage.ImageFilePath}", MessageImportance.Verbose);
            EventLog.logEvent($"Back image: {backImage.ImageFilePath}", MessageImportance.Verbose);
            retImages.Add(frontImage);
            retImages.Add(backImage);
            return retImages.ToArray();
        }

        private RetrievalObjectClass CreateRetrievalObject(int batchSequenceNumber, string imageFilePath)
        {
            return new RetrievalObjectClass
            {
                BatchSeqNo = batchSequenceNumber,
                ImageFormat = _imageFormat,
                ImageType = _imageType,
                ExtraData1 = _extraData,
                ImageFilePath = imageFilePath ?? string.Empty
            };
        }
    }
}
