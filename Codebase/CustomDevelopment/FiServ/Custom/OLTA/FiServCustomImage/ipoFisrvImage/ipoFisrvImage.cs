﻿using System;
using System.Collections.Generic;
using System.Text;
using WFS.integraPAY.Online.Common;
using System.IO;
using System.Globalization;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:  Wayne Schwarz
* Date:     10/14/2011
*
* Purpose: 
*
* Modification History
* CR  45986 WJS 10/14/2011 Initial Release
******************************************************************************/
namespace WFS.Custom.image
{
    public class ipoFisrvImage : _DMPObjectRoot
    {

        public ipoFisrvImage(string sImageRootPath)
        {
        }

        public void Dispose()
        {
        }

        public int SiteID
        {
            get;
            set;
        }

        public bool GetImage(DateTime pProcessingDate, int pLbxNo, int BankId, int pBatchNo, 
                             int TransactionId, int BatchSequence, int iPage,
                             out byte[] ImageBytes, out string FileExtension)
        //public bool GetImage(IItem irRequest, int iPage, out byte[] ImageBytes, out string FileExtension)
        {
            string sFileName = string.Empty;
            byte[] byteData;
            bool bRet = false ;
            bool bGotImageServiceUrl = true;
           // byte[]  ImageBytes = null;
            //string FileExtension = string.Empty;
            string imageServiceURL= string.Empty;
            ImageBytes = null;
            FileExtension = string.Empty;
            try
            {
                RetrievalObjectClass[] retImageItems;
                FiServCustomImageService customImageService = null;
                try
                {
                    imageServiceURL = ipoINILib.IniReadValue("CustomImageService", "ServerLocation");
                    if (imageServiceURL.Length == 0)
                    {
                        bGotImageServiceUrl = false;
                        eventLog.logEvent("Failed to get custom image service url." ,MessageImportance.Essential);
                    }
                }
                catch (Exception ex)
                {
                    bGotImageServiceUrl = false;
                    eventLog.logError(ex, this.GetType().Name, "GetImage (CustomImageService)");
                }
                if (bGotImageServiceUrl)
                {
                    customImageService = new FiServCustomImageService(imageServiceURL);

                    retImageItems = customImageService.RetrieveImage(pProcessingDate,
                                    pLbxNo,
                                    pBatchNo,
                                    BankId,
                                    SiteID,
                                    TransactionId,
                                    BatchSequence);
                    
                    foreach (RetrievalObjectClass item in retImageItems)
                    {
                        sFileName = item.ImageFilePath;

                        if (item.ImageFormat == RetrievalObjectClassImageFormatVal.TIF)
                        {
                            sFileName= Path.ChangeExtension(sFileName, ".TIF");
                        }
                        else if (item.ImageFormat == RetrievalObjectClassImageFormatVal.JPEG)
                        {
                            sFileName = Path.ChangeExtension(sFileName, ".JPEG");
                        }

                        if (sFileName != null && sFileName.Length > 0 && File.Exists(sFileName))
                        {
                            byteData = null;
                            byteData = ReadFromFile(sFileName);
                            ImageBytes = byteData;
                            FileExtension = Path.GetExtension(sFileName);
                            bRet = true;
                        }
                        else
                        {
                            ImageBytes = null;
                            FileExtension = string.Empty;
                            bRet = false;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                ImageBytes = null;
                FileExtension = string.Empty;
                eventLog.logError(ex);
                bRet = false;
            }

            return bRet;
        }   
      
        private byte[] ReadFromFile(string FilePath)
        {
            byte[] pageData = null;

            try
            {
                using (FileStream fStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader binReader = new BinaryReader(fStream))
                    {
                        pageData = binReader.ReadBytes((int)fStream.Length);
                        binReader.Close();
                    }

                    fStream.Close();
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logError(ex);
            }

            return pageData;
        }
        
        public void SetSiteID(int iSiteID){
            SiteID = iSiteID;
        }
    }
}
