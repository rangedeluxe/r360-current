﻿using CustomImageAPI.Interfaces;
using CustomImageAPI.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.Common;

namespace WFS.Custom.image
{
    public class FiServCustomImageProvider : ICustomImageProvider
    {
        public PossibleResult<CustomImage> GetImage(IItem requestContext)
        {
            if (requestContext == null) return Result.None<CustomImage>();
            DateTime processingDate;

            if (!DateTime.TryParseExact(requestContext.ProcessingDateKey.ToString(), "yyyyMMdd",
                new CultureInfo("en-US"), DateTimeStyles.None, out processingDate))
                return Result.None<CustomImage>();

            //downcast batchid from int64 to int32
            int batchId;
            if (!int.TryParse(requestContext.SourceBatchID.ToString(), out batchId))
                return Result.None<CustomImage>();

            return new ipoFisrvImage().GetImage(processingDate, requestContext.LockboxID,
                requestContext.BankID, requestContext.BatchSiteCode, batchId,
                requestContext.TransactionID, requestContext.BatchSequence, 0);
        }
    }
}
