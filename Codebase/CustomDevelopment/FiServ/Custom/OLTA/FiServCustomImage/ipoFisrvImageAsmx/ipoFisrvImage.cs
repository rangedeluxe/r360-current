using System;
using System.Text;
using System.IO;
using System.Globalization;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using System.Configuration;
using WFS.RecHub.ApplicationBlocks.Common;
using CustomImageAPI.Models;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:  Wayne Schwarz
* Date:     10/14/2011
*
* Purpose: 
*
* Modification History
* CR 45986 WJS 10/14/2011
*    -Initial Release
* CR 50050 WJS 02/20/2011
*    -Support for asmx support
* CR 50543 JMC 02/23/2012
*    -Removed 'SiteID' property.
*    -GetImage() now expects SiteCode as an input parameter.
*    -GetRelativeImagePath() now expects SiteCode as an input parameter.
* CR 50543 WJS 2/23/2012
*	 - Added Some logging in debug mode
* CR 50982 WJS 3/8/2012
*	 - Remove enum types, switch JPG extension
* CR 52087 WJS 4/12/2012
*	- Add logging for relative image path
* CR 53508 WJS 6/16/2012
*	- Add logging
* CR 53731 WJS 6/22/2012
 *  -Pass transactionID into get relative image path
* CR 53837 WJS 6/27/2012
 *  -Stored back/previous so on a request for another page it looks item up in the array to prevent round trip to web service
* WI 112433 WJS 8/21/2013
 *   -Added logging and possible bug fix for when page is equal to length
******************************************************************************/
namespace WFS.Custom.image
{
    public class ipoFisrvImage
    {

        private cSiteOptions _SiteOptions = null;
        private cEventLog _EventLog = null;
        private string _SiteKey;
        private const int _TIFF = 1;
        private const int _JPEG = 2;

        //Store previous item which was looked up it case it repeats
        private static long _currentProcessingBankID = -1;
        private static long _currentProcessingLockboxID = -1;
        private static long  _currentProcessingBatchNo = -1;
        private static int _currentProcessingTransID = -1;
        private static int _currentProcessingSiteID = -1;
        private static DateTime _currentProcessingDate = DateTime.MinValue;

        private static RetrievalObjectClass[] _currentRetrivalObjectArray = null;

        public ipoFisrvImage() : this(string.Empty) {}
        public ipoFisrvImage(string sImageRootPath) {}

        public void Dispose()
        {
        }

        public PossibleResult<CustomImage> GetImage(DateTime pProcessingDate, int pLbxNo, int BankId, int SiteCode, int pBatchNo, 
            int TransactionId, int BatchSequence, int iPage)
        {
            var customImage = new CustomImage();
           
            try
            {
                eventLog.logEvent("Start of Get Image", MessageImportance.Debug);
               
                customImage.Details = 
                    new ImageDetails(GetFileName( pProcessingDate, pLbxNo, BankId, SiteCode, pBatchNo, TransactionId, BatchSequence, iPage));
                eventLog.logEvent($"ReadFileName {customImage.Details.RelativePath}", MessageImportance.Debug);
                if (customImage.Details.RelativePath != null && customImage.Details.RelativePath.Length > 0 && File.Exists(customImage.Details.RelativePath))
                {
                    customImage.Data = ReadFromFile(customImage.Details.RelativePath);
                    eventLog.logEvent($"ByteData {customImage.Details.RelativePath} ", MessageImportance.Debug);
                    return Result.Real(customImage);
                }

                return Result.None<CustomImage>();
            }
            catch (System.Exception ex)
            {
                eventLog.logError(ex);
                return Result.None<CustomImage>();
            }
        }

        private string GetFileName(DateTime pProcessingDate, 
                             int pLbxNo, 
                             int BankId, 
                             int SiteCode,
                             int pBatchNo, 
                             int TransactionId, 
                             int BatchSequence, 
                             int iPage)
        {
            bool bGotImageServiceUrl = true;
            RetrievalObjectClass item = null;
            RetrievalObjectClass[] retImageItems;
            FiServCustomImageAsmx customImageService = null;
            StringBuilder message = new StringBuilder();
            string imageServiceURL = string.Empty;
            string sFileName = string.Empty;
            try
            {
                imageServiceURL = ConfigurationManager.AppSettings.Get("customimageserviceurl");
                if (string.IsNullOrEmpty(imageServiceURL))
                {
                    bGotImageServiceUrl = false;
                    eventLog.logEvent("Failed to get custom image service url." ,MessageImportance.Essential);
                }
            }
            catch (Exception ex)
            {
                bGotImageServiceUrl = false;
                eventLog.logError(ex, this.GetType().Name, "GetImage (CustomImageService)");
            }
            if (bGotImageServiceUrl)
            {
                customImageService = new FiServCustomImageAsmx(imageServiceURL);

                eventLog.logEvent("Processing Date: " + pProcessingDate.ToLongDateString() + " LockboxId:" + pLbxNo.ToString() + " BatchId: " + pBatchNo.ToString() + " SiteID: " + SiteCode.ToString() + "TransactionId:" + TransactionId.ToString() + " BatchSequence " + BatchSequence.ToString(), MessageImportance.Debug);

                if (_currentRetrivalObjectArray != null && _currentRetrivalObjectArray.Length > 0) 
                {
                    RetrievalObjectClass obj = _currentRetrivalObjectArray[0];
                    eventLog.logEvent("Comparing batchSequence stored: " + obj.BatchSeqNo.ToString() + " to passed in batchSequence " + BatchSequence.ToString(), MessageImportance.Debug);
                    if (obj.BatchSeqNo == BatchSequence &&
                        _currentProcessingDate == pProcessingDate &&
                        _currentProcessingLockboxID == pLbxNo &&
                        _currentProcessingBatchNo == pBatchNo &&
                        _currentProcessingBankID == BankId &&
                        _currentProcessingSiteID == SiteCode &&
                        _currentProcessingTransID == TransactionId)
                    {
                        if (iPage <= _currentRetrivalObjectArray.Length)
                        {
                            eventLog.logEvent("Found Item which previous was retrived. Getting page: " + iPage.ToString(), MessageImportance.Debug);
                            item = _currentRetrivalObjectArray[iPage];
                        }
                        else
                        {
                            message.AppendFormat("Requested a page {0} which is greater then current length {1}: ", iPage, _currentRetrivalObjectArray.Length);
                            eventLog.logEvent(message.ToString(), MessageImportance.Debug);
                        }
                    }
                }
                if (item == null)
                {

                    retImageItems = customImageService.RetrieveImage(pProcessingDate,
                                    pLbxNo,
                                    pBatchNo,
                                    BankId,
                                    SiteCode,
                                    TransactionId,
                                    BatchSequence);
                    eventLog.logEvent("Got images Returned count. " + retImageItems.Length.ToString(), MessageImportance.Debug);
                    AssignStoredMembers(retImageItems, pProcessingDate, BankId,
                                            SiteCode, pLbxNo, pBatchNo, TransactionId);

                  
                  if (retImageItems != null) {
                      eventLog.logEvent("RetImages count exist. Page is  " + iPage.ToString() + " Length is " + retImageItems.Length.ToString(), MessageImportance.Debug);
                   
                        if (iPage >= retImageItems.Length) {
                            message.AppendFormat("Requested a page {0} which is greater then or equal to current length {1}: ", iPage, retImageItems.Length);
                            eventLog.logEvent(message.ToString(), MessageImportance.Debug);


                        } else if (retImageItems.Length > 0) {
                            message.AppendFormat("Requesting page {0} for a length of {1}: ", iPage, retImageItems.Length);
                            eventLog.logEvent(message.ToString(), MessageImportance.Debug);

                            item = retImageItems[iPage];
                        } else {
                            message.AppendFormat("Did not a item returned length is zero  ");
                            eventLog.logEvent(message.ToString(), MessageImportance.Debug);
                        }
                    } else {
                        message.AppendFormat("Return Image Items is null.  ");
                        eventLog.logEvent(message.ToString(), MessageImportance.Debug);
                    }
                }
                if (item != null) {
                    message.AppendFormat("Got a image back with an imageName {0}. A imageformat of {1}. A Batch Sequence of {2}. A imageType of {3}", item.ImageFilePath, item.ImageFormat, item.BatchSeqNo, item.ImageType);
                    eventLog.logEvent(message.ToString(), MessageImportance.Debug);
                    sFileName = item.ImageFilePath;
                    eventLog.logEvent("FileName " + sFileName, MessageImportance.Debug);

                    if (item.ImageFormat == _TIFF) {
                        sFileName = Path.ChangeExtension(sFileName, ".TIF");
                    } else if (item.ImageFormat == _JPEG) {
                        sFileName = Path.ChangeExtension(sFileName, ".JPG");
                    } else {
                        message.AppendFormat("Item does not match specified formats.  ");
                        eventLog.logEvent(message.ToString(), MessageImportance.Debug);
                    }
                } else {
                    message.AppendFormat("Item is null.  ");
                    eventLog.logEvent(message.ToString(), MessageImportance.Debug);
                }
            }
            return sFileName;
        }
        private void AssignStoredMembers(RetrievalObjectClass[] retImageItems,
                                            DateTime pProcessingDate,
                                           long BankID,
                                           int SiteCode,
                                           long LockboxID,
                                           long BatchID,
                                           int TransactionId )
        {
            _currentRetrivalObjectArray = retImageItems;
            _currentProcessingDate = pProcessingDate;
            _currentProcessingLockboxID = LockboxID;
            _currentProcessingBatchNo = BatchID;
            _currentProcessingBankID = BankID;
            _currentProcessingSiteID = SiteCode;
            _currentProcessingTransID = TransactionId;
        }


        public string GetRelativeImagePath(DateTime PICSDate,
                                           long BankID,
                                           int SiteCode,
                                           long LockboxID,
                                           long BatchID,
                                           int TransactionId, 
                                           long BatchSequence,
                                           string FileDescriptor,
                                           WorkgroupColorMode lockboxColorMode,
                                           int iPage)
        {
            string sFileName = string.Empty;
            try
            {
                eventLog.logEvent("Get Relative Image path", MessageImportance.Debug);
               
                eventLog.logEvent("Relative Image Path: Processing Date: " + PICSDate.ToLongDateString() + " LockboxId:" + LockboxID.ToString() + " BatchId: " + BatchID.ToString() + " SiteID: " + SiteCode.ToString() + " TransactionId:" + TransactionId.ToString() + " BankID:" + BankID.ToString() + " BatchSequence " + BatchSequence.ToString(), MessageImportance.Debug);
                sFileName = GetFileName(PICSDate,
                            (int)LockboxID,
                            (int)BankID,
                            SiteCode,
                            (int)BatchID,
                            TransactionId,
                            (int)BatchSequence,
                            iPage);
            }
            catch (System.Exception ex)
            {
                eventLog.logError(ex);
                sFileName = string.Empty;
            }

            return sFileName;
        }
      
        private byte[] ReadFromFile(string FilePath)
        {
            byte[] pageData = null;

            try
            {
                using (FileStream fStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader binReader = new BinaryReader(fStream))
                    {
                        pageData = binReader.ReadBytes((int)fStream.Length);
                        binReader.Close();
                    }

                    fStream.Close();
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logError(ex);
            }

            return pageData;
        }

        /// <summary>
        /// Determines the section of the local .ini file to be used for local 
        /// options.
        /// </summary>
        public string siteKey
        {
            set { _SiteKey = value; }
            get { return _SiteKey; }
        }

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        protected cSiteOptions siteOptions
        {
            get
            {
                if (_SiteOptions == null)
                { _SiteOptions = new cSiteOptions(this.siteKey); }

                return _SiteOptions;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog eventLog
        {
            get
            {
                if (_EventLog == null)
                {
                    _EventLog = new cEventLog(siteOptions.logFilePath
                                                , siteOptions.logFileMaxSize
                                                , (MessageImportance)siteOptions.loggingDepth);
                }

                return _EventLog;
            }
        }


        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output messages.
        /// </summary>
        /// <param name="message">Test message to be logged.</param>
        /// <param name="src">Source of the event.</param>
        /// <param name="messageType">MessageType(Information, Warning, Error)</param>
        /// <param name="messageImportance">MessageImportance(Essential, Verbose, Debug)
        /// </param>
        protected void object_outputMessage(string message
                                          , string src
                                          , MessageType messageType
                                          , MessageImportance messageImportance)
        {
            eventLog.logEvent(message
                            , src
                            , messageType
                            , messageImportance);
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output errors.
        /// </summary>
        /// <param name="e">Exception to be logged.</param>
        protected void object_outputError(Exception e)
        {
            eventLog.logError(e);
        }
    }
}
