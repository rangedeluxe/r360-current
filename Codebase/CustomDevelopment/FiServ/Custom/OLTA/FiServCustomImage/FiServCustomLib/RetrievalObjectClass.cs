﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR  45986 WJS 10/14/2011 Initial Release
* CR  50982 WJS 3/8/2012 - Remove enum types
******************************************************************************/
namespace OLTAImageRetrieval
{
    public class RetrievalObjectClass
    {
   
        public string ImageFilePath
        {
            get;
            set;
        }
        public int ImageFormat
        {
            get;
            set;
        }
        public int BatchSeqNo
        {
            get;
            set;
        }
        public int ImageType
        {
            get;
            set;
        }
        public string ExtraData1
        {
            get;
            set;
        }
    }
}
