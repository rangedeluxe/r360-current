﻿using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using WFS.integraPAY.Online.Common;
using OLTAImageRetrieval;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR  45986 WJS 10/14/2011 Initial Release

* CR 50543 WJS 2/23/2012
*	  - Added some logging
* CR 50982 WJS 3/8/2012
*	  - Remove enum types
* CR 53508 WJS 6/16/2012
*	- Add ability to retrieve 10 circular image
******************************************************************************/
namespace OLTAImageRetrievalWebService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class FiServCustomImageAsmx : System.Web.Services.WebService
    {

        protected cEventLog _EventLog = null;
        private string _logFilePath = string.Empty;
        private int _logFileMaxSize = 1024;
        private int _logDepth = 2;

        public FiServCustomImageAsmx()
        {
            int tempInt  = 0;
            _logFilePath = ltaConfig.ReadAppConfig("/FiservCustomWebService", "LogFile");
             if (Int32.TryParse(ltaConfig.ReadAppConfig("/FiservCustomWebService", "LogFileMaxSize"), out tempInt))
            {
                _logFileMaxSize = tempInt;
            }
            if (Int32.TryParse(ltaConfig.ReadAppConfig("/FiservCustomWebService", "LoggingDepth"), out tempInt))
            {
                _logDepth = tempInt;
            }
       
        }

        protected cEventLog EventLog
        {
            get
            {
                if (_EventLog == null)
                {

                    _EventLog = new cEventLog(_logFilePath,
                                              _logFileMaxSize,
                                              (MessageImportance)_logDepth);
                }
                return _EventLog;
            }
        }


        [WebMethod]
        public OLTAImageRetrieval.RetrievalObjectClass[] RetrieveImage(DateTime pProcessingDate, int pLbxNo, int pBatchNo, int pBankId, int pLocId, int pTransaction, int pBatchSeqNo)
        {
            RetrievalObjectClass[] retImages = new OLTAImageRetrieval.RetrievalObjectClass[2];
            RetrievalObjectClass backImage = new OLTAImageRetrieval.RetrievalObjectClass();
            RetrievalObjectClass image = new OLTAImageRetrieval.RetrievalObjectClass();
            int batchSeq =0;
            int numberOfFileNamesInImages = 20;

            EventLog.logEvent("Retrive Image starting ", MessageImportance.Verbose);
            EventLog.logEvent("Processing Date: " + pProcessingDate.ToLongDateString() + " LockboxId:" + pLbxNo.ToString() + " BatchId: " + pBatchNo.ToString() + " SiteID: " + pLocId.ToString() + "TransactionId:" + pTransaction.ToString() + " BatchSequence " + pBatchSeqNo.ToString(), MessageImportance.Debug);

            image.BatchSeqNo = pBatchSeqNo;
           
            image.ImageFormat = Int32.Parse(ltaConfig.ReadAppConfig("/FiservCustomWebService", "imageFormat"));
            image.ImageType = Int32.Parse(ltaConfig.ReadAppConfig("/FiservCustomWebService", "ImageType"));
            image.ExtraData1 = ltaConfig.ReadAppConfig("/FiservCustomWebService", "ExtraData1");

            backImage.BatchSeqNo = image.BatchSeqNo;
            backImage.ImageFormat = image.ImageFormat;
            backImage.ImageType = image.ImageType;
            backImage.ExtraData1 = image.ExtraData1;

            int batchToUse = pBatchSeqNo % numberOfFileNamesInImages;
            EventLog.logEvent("BatchSeq " + pBatchSeqNo.ToString() + " BatchToUse " + batchToUse.ToString(), MessageImportance.Verbose);
             string imageFileName = String.Format("imageFilename" + batchToUse.ToString());
             string backimageFileName = String.Format("backimageFilename" + batchToUse.ToString());
   
            EventLog.logEvent("Looking for imageFileName: " + imageFileName, MessageImportance.Verbose);
            EventLog.logEvent("Looking for backimageFileName: " + backimageFileName, MessageImportance.Verbose);
            image.ImageFilePath = ltaConfig.ReadAppConfig("/FiservCustomWebService", imageFileName);
            EventLog.logEvent("Found  Image with name: " + image.ImageFilePath, MessageImportance.Verbose);
            backImage.ImageFilePath = ltaConfig.ReadAppConfig("/FiservCustomWebService", backimageFileName);
            EventLog.logEvent("Found  Image with back image name: " + backImage.ImageFilePath, MessageImportance.Verbose);
            retImages[0] = image;
            retImages[1] = backImage;
            return retImages;
           // return imageRetrieval.RetrieveImage(pProcessingDate, pLbxNo, pBatchNo, pBankId, pLocId, pTransaction, pBatchSeqNo);
        }
     
    }
}
