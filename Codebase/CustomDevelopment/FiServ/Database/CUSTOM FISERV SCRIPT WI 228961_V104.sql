/*********** FISERV CUSTOM FIX SCRIPT - WI 228961 - MGE - 08/18/2015 **********************************/
DECLARE	@BeginDate	DATETIME = '04/04/2015';			--Fiserv will put 6/13/2015 here
DECLARE @EndDate	DATETIME = '08/17/2015';			--Fiserv will put 6/17/2015 here

SELECT 'Re-queueing batches Between ' + CAST(@BeginDate  AS VARCHAR(11)) + ' AND ' + CAST(@EndDate  AS VARCHAR(11));

Update Batch SET Batch.SiteCode = Batch_SiteCodeFix20150724.SiteCode
FROM Batch 
INNER JOIN Batch_SiteCodeFix20150724 
	ON Batch.ProcessingDate = Batch_SiteCodeFix20150724.ProcessingDate
	AND Batch.BankID = Batch_SiteCodeFix20150724.BankID
	AND Batch.LockboxID = Batch_SiteCodeFix20150724.LockboxID
	AND Batch.BatchID = Batch_SiteCodeFix20150724.BatchID
	AND Batch.GlobalBatchID = Batch_SiteCodeFix20150724.GlobalBatchID
WHERE Batch_SiteCodeFix20150724.ProcessingDate BETWEEN @BeginDate AND @EndDate
	--AND Batch_SiteCodeFix20150724.SiteCode in (2,3,4,5);			--FISERV SITE CODES
	AND Batch_SiteCodeFix20150724.SiteCode in (1234567890);			--TESTING SITE CODES  (returns 7 rows)  Fiserv will comment this row out and uncomment the previous row.


-- Requeuing of the batches to OTISQueue.

DECLARE	@Loop					INT,
		@BatchCount				INT,
		@NotifyIMS				BIT,
		@BankID					INT,
		@LockboxID				INT,
		@ProcessingDate			DATETIME,
		@BatchID				INT,
		@ErrorMessage    NVARCHAR(4000),
		@ErrorSeverity			INT,
		@ErrorState				INT,
		@ProcessMessage			VARCHAR(512);


IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchList')) 
	DROP TABLE #tmpBatchList;

CREATE TABLE #tmpBatchList 
(
	RowID INT IDENTITY(1,1) NOT NULL,
	BankID INT NOT NULL,
	LockboxID INT NOT NULL,
	ProcessingDate DATETIME NOT NULL,
	BatchID INT NOT NULL,
	GlobalBatchID INT NOT NULL
);
	
SELECT @NotifyIMS = 0;

INSERT INTO #tmpBatchList(BankID,LockboxID,ProcessingDate,BatchID, GlobalBatchID)
SELECT BankID,LockboxID,ProcessingDate,BatchID, GlobalBatchID
FROM Batch_SiteCodeFix20150724 
WHERE	
Batch_SiteCodeFix20150724.ProcessingDate BETWEEN @BeginDate AND @EndDate
--AND Batch_SiteCodeFix20150724.SiteCode in (2,3,4,5);			--FISERV SITE CODES
AND Batch_SiteCodeFix20150724.SiteCode in (1234567890);			--TESTING SITE CODES  (returns 7 rows)  Fiserv will comment this row out and uncomment the previous row.

Select * from #tmpBatchList;									--DEBUG ONLY (can be removed for Fiserv)

SELECT @BatchCount = MAX(RowID) FROM #tmpBatchList;
SET @Loop = 1;
WHILE( @Loop <= @BatchCount )
BEGIN
	SELECT	@BankID = BankID,
			@LockboxID = LockboxID,
			@ProcessingDate = ProcessingDate,
			@BatchID = BatchID
	FROM	#tmpBatchList
	WHERE	RowID = @Loop;
		
	SET @ProcessMessage = 'Processing Bank ' + CAST(@BankID AS VARCHAR) + ', Lockbox ' + CAST(@LockboxID AS VARCHAR) + ', Batch ' + CAST(@BatchID AS VARCHAR) + ', ProcessingDate ' + CAST(@ProcessingDate AS VARCHAR);
	RAISERROR(@ProcessMessage,10,1) WITH NOWAIT;
	/* Queue Batch Record */
	EXEC usp_CDSBatchNotifyOLTAServiceBroker @ProcessingDate,@BankID,@LockboxID,@BatchID,3,@NotifyIMS;
	/* Queue Image Record */
	--EXEC usp_CDSBatchNotifyOLTAServiceBroker @ProcessingDate,@BankID,@LockboxID,@BatchID,2,@NotifyIMS;
	SET @Loop = @Loop + 1;
END

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchList')) 
	DROP TABLE #tmpBatchList
