/************* CREATE A TABLE LIKE THE ONE FISERV HAS *********************************************/
Create Table Batch_SiteCodeFix20150724
(
ProcessingDate DateTime,
SiteCode INT,
BankID INT,
LockboxID INT,
BatchID INT,
GlobalBatchID INT
)

/**************************** INSERT SOME ROWS FROM OUR TEST DATABASE TO USE FOR THE TEST ***********/
INSERT INTO Batch_SiteCodeFix20150724
select ProcessingDate, SiteCode, BankID, LockboxID, BatchID, GlobalBatchID 
from Batch
Where ProcessingDate > '04/04/2015'
AND ProcessingDate < '08/17/2015'

select * from Batch_SiteCodeFix20150724								-- use to see what rows will be processed FOR OUR TESTING.
/**************** USE THIS STATEMENT TO VERIFY ROWS WERE ADDED TO THE OTISQUEUE TABLE ***************/
select top 10 * from OtisQueue order by ModificationDate Desc		-- use to verify that batches were inserted into OtisQueue

select top 100 * from Batch order by processingdate desc