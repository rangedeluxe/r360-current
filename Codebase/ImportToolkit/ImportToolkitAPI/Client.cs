﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.API
{
    public class Client : ConfigurationElement
    {
        [ConfigurationProperty("UserName", IsRequired = true, IsKey = true)]
        public string UserName
        {
            get { return this["UserName"].ToString(); }
        }

        [ConfigurationProperty("Password", IsRequired = true)]
        public string Password
        {
            get { return this["Password"].ToString(); }
        }

        [ConfigurationProperty("Sid", IsRequired = true)]
        public Guid Sid
        {
            get
            {
                Guid sid;
                Guid.TryParse(this["Sid"].ToString(), out sid);
                return sid;
            }
        }
    }
}
