﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;

namespace WFS.RecHub.ImportToolkit.API
{
    public class ConfigurationSettings : BaseConfigurationSettings
    {
        private ClientConfigurationSection clientConfigurationSection = null;

        public ConfigurationSettings(IConfigurationProvider provider)
            : base(provider)
        {
        }

        public ClientConfigurationSection ClientConfigurations
        {
            get
            {
                return clientConfigurationSection ?? (clientConfigurationSection = ConfigurationManager.GetSection(ClientConfigurationSection.Name) as ClientConfigurationSection);
            }
        }

        public override void ValidateSettings()
        {
            throw new NotImplementedException();
        }
    }
}
