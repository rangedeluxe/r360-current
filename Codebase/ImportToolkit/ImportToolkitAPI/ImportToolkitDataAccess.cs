﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

namespace WFS.RecHub.ImportToolkit.API
{
    internal class ImportToolkitDataAccess : _DALBase
    {
        public ImportToolkitDataAccess(string paramSiteKey)
            : base(paramSiteKey) 
        {
        }

        public bool LogAlert(string eventName, string message)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms = new List<SqlParameter>();
            const string PROCNAME = "RecHubAlert.usp_EventLog_SystemLevel_Ins";
            try
            {
                arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, eventName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmMessage", SqlDbType.VarChar, message, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(PROCNAME, parms);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bRtnval = false;
            }
            return bRtnval;
        }

        public bool LogAuditEvent(AuditRequest requestContext)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms = new List<SqlParameter>();
            const string PROCNAME = "RecHubCommon.usp_WFS_EventAudit_Ins";
            try
            {
                arParms.Add(BuildParameter("@parmApplicationName", SqlDbType.VarChar, requestContext.ApplicationName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, requestContext.EventName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEventType", SqlDbType.VarChar, requestContext.EventType, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, requestContext.UserId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmWorkstation", SqlDbType.Int, requestContext.WorkstationId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmAuditMessage", SqlDbType.VarChar, requestContext.Message, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(PROCNAME, parms);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bRtnval = false;
            }
            return bRtnval;
        }

        public DataTable GetWorkgroupInfo(WorkgroupRequest requestContext)
        {
            DataTable results = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms = new List<SqlParameter>();
            const string PROCNAME = "RecHubData.usp_dimClientAccounts_Get_BySiteBankIDSiteClientAccountID";
            try
            {
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, requestContext.BankId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, requestContext.ClientAccountId, ParameterDirection.Input));
                parms = arParms.ToArray();
                if (Database.executeProcedure(PROCNAME, parms, out results))
                    return results;
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
            }
            return results;
        }
    }
}
