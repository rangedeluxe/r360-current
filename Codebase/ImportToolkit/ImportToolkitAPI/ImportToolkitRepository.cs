﻿
using System.Collections.Generic;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.RecHub.ImportToolkit.Common.Interfaces;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using System.Data;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.R360BaseResponse;

namespace WFS.RecHub.ImportToolkit.API
{
    public class ImportToolkitRepository : IAlertable, IAuditable, IWorkgroupRetrievable
    {
        private readonly string siteKey;

        public ImportToolkitRepository(string paramSiteKey)
        {
            siteKey = paramSiteKey;
        }

        public AlertResponse Log(AlertRequest requestContext)
        {
            return new ImportToolkitDataAccess(siteKey).Use(dal =>
            {
                var response = new AlertResponse();

                if (!dal.LogAlert(requestContext.EventName, requestContext.Message))
                    return new AlertResponse
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string> { "Unable to log alert. Please check log files for further details." }
                    };

                return new AlertResponse
                {
                    Status = StatusCode.SUCCESS,
                    Errors = new List<string> { "Alert successfully processed." }
                };
            });
        }

        public AuditResponse LogAuditEvent(AuditRequest requestContext)
        {
            return new ImportToolkitDataAccess(siteKey).Use(dal =>
            {
                var response = new AuditResponse();

                if (!dal.LogAuditEvent(requestContext))
                    return new AuditResponse
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string> { "Unable to log audit event. Please check log files for further details." }
                    };

                return new AuditResponse
                {
                    Status = StatusCode.SUCCESS,
                    Errors = new List<string> { "Audit event successfully processed." }
                };
            });
        }

        public WorkgroupResponse GetWorkgroupInfo(WorkgroupRequest requestContext)
        {
            return new ImportToolkitDataAccess(siteKey).Use(dal =>
            {
                DataTable results = dal.GetWorkgroupInfo(requestContext);
                if (!results.HasData())
                    return new WorkgroupResponse
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string> {"Unable to process workgroup request." }
                    };

                return new WorkgroupResponse
                {
                    WorkgroupList = results.Map(ToWorkgroupList),
                    Status = StatusCode.SUCCESS,
                    Errors = new List<string> { "Workgroup request successfully processed." }
                };
                
            });
        }

        private List<WorkgroupInfo> ToWorkgroupList(DataTable dataTable)
        {
            var workgroupList = new List<WorkgroupInfo>();
            foreach (DataRow row in dataTable.Rows)
            {
                workgroupList.Add(row.Map(ToWorkgroup));
            }

            return workgroupList;
        }

        private WorkgroupInfo ToWorkgroup(DataRow dataRow)
        {
            return new WorkgroupInfo
                {
                    ClientAccountKey = dataRow.GetInteger("clientaccountkey"),
                    IsActive = dataRow.GetInteger("isactive") == 1 ? true : false,
                    MostRecent = dataRow.GetBoolean("mostrecent"),
                    FileGroup = dataRow.GetString("filegroup"),
                    LongName = dataRow.GetString("longname"),
                    ShortName = dataRow.GetString("shortname"),
                    ClientAccountId = dataRow.GetInteger("clientaccountid"),
                    BankId = dataRow.GetInteger("bankid"),
                    OrganizationId = dataRow.GetInteger("organizationid"),
                    PaymentSource = dataRow.GetString("batchsourcename"),
                    SiteCodeId = dataRow.GetInteger("sitecodeid")
                };
        }
    }
}
