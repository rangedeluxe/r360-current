﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.API
{
    public class ClientConfigurationSection : ConfigurationSection
    {
        public const string Name = "ClientConfigurations";

        [ConfigurationProperty(ClientList.Name)]
        [ConfigurationCollection(typeof(Client))]
        public ClientList Clients
        {
            get { return this[ClientList.Name] as ClientList; }
        }
    }
}
