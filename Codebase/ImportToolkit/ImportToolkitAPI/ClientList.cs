﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.API
{
    public class ClientList : ConfigurationElementCollection
    {
        public const string Name = "Clients";

        protected override ConfigurationElement CreateNewElement()
        {
            return new Client();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Client)element).UserName;
        }
    }
}
