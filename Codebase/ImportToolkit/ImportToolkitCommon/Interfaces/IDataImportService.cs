﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.RecHub.ImportToolkit.Common.Interfaces
{
    [ServiceContract(Namespace = "urn:wausaufs.com:services:DataImportServices")]
    public interface IDataImportService : IAlertable, IAuditable, IWorkgroupRetrievable
    {
        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "GetSession")]
        bool GetSession(string LogonName, string Password, out Guid SessionID);

        [OperationContract(Action = "EndSession")]
        bool EndSession();

        [OperationContract(Action = "GetSchemaDefinition")]
        bool GetSchemaDefinition(string type, string XSDVersion, out string setupDataXsd, out string respXml);

        [OperationContract(Action = "SendClientSetup")]
        bool SendClientSetup(string clientSetupXml, int auditDateKey, out string respXml);

        [OperationContract(Action = "SendBatchData")]
        bool SendBatchData(string batchDataXml, int auditDateKey, out string respXml);

        [OperationContract(Action = "SendBatchMeasureData")]
        bool SendBatchMeasureData(BatchFileImportMeasures batchFileImportMeasures);

        [OperationContract(Action = "SendBatchImage")]
        void SendBatchImages(IList<UploadRequest> item);

        [OperationContract(Action = "GetClientSetupResponses")]
        ClientSetupResponses GetClientSetupResponses(string clientProcessCode);

        [OperationContract(Action = "GetBatchDataResponses")]
        DataSet GetBatchDataResponses(string clientProcessCode, out bool success);


        [OperationContract(Action = "SendResponseComplete")]
        bool SendResponseComplete(Guid responseIdentifier);

        [OperationContract(Action = "SendResponseCompleteClientUnknown")]
        bool SendResponseCompleteClientUnknown(Guid responseIdentifier);

        //[OperationContract(Action = "RequestClientSetup")]
        //DataSet RequestClientSetup(Int32 siteBankID, Int32 siteClientID, out bool success);

        //[OperationContract(Action = "DeleteBatch")]
        //bool DeleteBatch(DateTime processingDate, DateTime depositDate, DateTime batchDate, Int32 siteBankID, Int32 siteLockboxId, Int32 batchID, out bool bRowsDeleted);

        [OperationContract(Action = "GetDocumentTypes")]
        DataSet GetDocumentTypes(DateTime? loadDate, out bool success);

        [OperationContract(Action = "GetImageRPSAliasMappings")]
        DataSet GetImageRPSAliasMappings(Int32 siteBankID, Int32 siteLockboxId, DateTime? modificationDate, out bool success);

        [OperationContract(Action = "GetBatchDataSetupField")]
        DataSet GetBatchDataSetupField(string batchSourceName, DateTime? modificationDate, out bool success);

        [OperationContract(Action = "GetItemDataSetupField")]
        DataSet GetItemDataSetupField(string batchSourceName, DateTime? modificationDate, out bool success);

        [OperationContract(Action = "MultiImageUpload")]
        MultiImageResponse MultiImageUpload(MultiImageRequest request);

        [OperationContract(Action = "UpdateStatus")]
        bool UpdateStatus(UpdateStatusContext context);

    }
}
