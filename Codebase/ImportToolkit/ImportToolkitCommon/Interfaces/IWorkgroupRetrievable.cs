﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

namespace WFS.RecHub.ImportToolkit.Common.Interfaces
{
    [ServiceContract]
    public interface IWorkgroupRetrievable
    {
        [OperationContract(Action = "GetWorkgroupInfo")]
        WorkgroupResponse GetWorkgroupInfo(WorkgroupRequest requestContext);
    }
}
