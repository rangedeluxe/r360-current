﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.Interfaces
{
    [ServiceContract]
    public interface IFileDataService : IAlertable, IAuditable
    {
        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "GetSession")]
        bool GetSession(string LogonName, string Password, out int UserID, out Guid SessionID);

        [OperationContract(Action = "EndSession")]
        bool EndSession();

        [OperationContract(Action = "GetSchemaDefinition")]
        bool GetSchemaDefinition(string sType, string XSDVersion, out string SetupDataXsd, out string RespXml);

        [OperationContract(Action = "SendNotificationData")]
        bool SendNotificationData(string NotificationDataXml, out string RespXml);

        [OperationContract(Action = "GetNotificationResponses")]
        bool GetNotificationResponses(string clientProcessCode, out string notificationRespXml, out string respXml);

        [OperationContract(Action = "SendResponseComplete")]
        bool SendResponseComplete(Guid responseTrackingID, Guid notificationTrackingID);
    }
}
