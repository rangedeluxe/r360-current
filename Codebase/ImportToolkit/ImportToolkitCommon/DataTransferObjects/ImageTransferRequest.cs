﻿using System;
using System.Runtime.Serialization;
using WFS.RecHub.R360BaseResponse;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class ImageTransferRequest : BaseResponse
    {
        [DataMember]
        public DateTime ProcessingDate { get; set; }

        [DataMember]
        public int SiteBankId { get; set; }

        [DataMember]
        public int SiteAccountId { get; set; }

        [DataMember]
        public long BatchId { get; set; }

        [DataMember]
        public string ClientProcessCode { get; set; }

        [DataMember]
        public int ArchivedImageCount { get; set; }

        [DataMember]
        public int UnArchiveImageCount { get; set; }
    }
}
