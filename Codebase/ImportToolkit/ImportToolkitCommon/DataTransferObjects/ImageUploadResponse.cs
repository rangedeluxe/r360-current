﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class ImageUploadResponse
    {
        [DataMember]
        public bool IsSuccessfullyWritten { get; set; }
        [DataMember]
        public string ImageFileName { get; set; }
        [DataMember]
        public ImageInfoContract ImageInfo { get; set; }
    }
}
