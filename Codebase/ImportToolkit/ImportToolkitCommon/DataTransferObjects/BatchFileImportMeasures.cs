﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class BatchFileImportMeasures
    {
        [DataMember]
        public bool IsSuccessful { get; set; }
        [DataMember]
        public long Duration { get; set; }
        [DataMember]
        public string MeasurementTypeName { get; set; }
        [DataMember]
        public string ClientProcessingCode { get; set; }
        [DataMember]
        public string SourceFileName { get; set; }
        [DataMember]
        public Guid SourceTrackingId { get; set; }

        [DataMember]
        public IList<BatchFileImportMeasure> BatchFileImportMeasure { get; set; } = new List<BatchFileImportMeasure>();
    }
}