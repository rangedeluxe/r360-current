﻿using System.Runtime.Serialization;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose: 
*
* Modification History
* WI 175548  SAS 10/31/2014
*   Update ImportTypeShortName field to BatchSourceName        
*********************************************************************************/

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class ImageInfoContract : ILTAImageInfo
    {
        [DataMember]
        public int ProcessingDateKey { get; set; }

        [DataMember]
        public int BankID { get; set; }

        [DataMember]
        public int LockboxID { get; set; }

        [DataMember]
        public long SourceBatchID { get; set; }

        [DataMember]
        public int BatchID { get; set; }

        [DataMember]
        public int BatchSequence { get; set; }

        [DataMember]
        public string FileDescriptor { get; set; }

        [DataMember]
        public bool IsCheck { get; set; }

        [DataMember]
        public bool IsBack { get; set; }

        [DataMember]
        public short ColorMode { get; set; }

        [DataMember]
        public string ImportTypeShortName { get; set; }

        [DataMember]
        public string BatchSourceShortName { get; set; }

        [DataMember]
        public string ImagePath { get; set; }

        [DataMember]
        public string BatchNumber { get; set; }

        [DataMember]
        public bool AllowMultiPageTiff { get; set; }

        public ImageInfoContract Clone()
        {
            return new ImageInfoContract
            {
                BankID = BankID,
                BatchID = BatchID,
                BatchSequence = BatchSequence,
                ColorMode = ColorMode,
                FileDescriptor = FileDescriptor,
                IsBack = IsBack,
                IsCheck = IsCheck,
                LockboxID = LockboxID,
                ProcessingDateKey = ProcessingDateKey,
                BatchSourceShortName = BatchSourceShortName,
                BatchNumber = BatchNumber,
                ImagePath = ImagePath,
                ImportTypeShortName = ImportTypeShortName,
                SourceBatchID = SourceBatchID,
                AllowMultiPageTiff = AllowMultiPageTiff,
            };
        }
        public override string ToString()
        {
            return string.Format(
                "ProcessingDateKey: {0}, BankID: {1}, LockboxID: {2}, SourceBatchId: {3}, " +
                "BatchID: {4}, BatchSequence: {5}, FileDescriptor: {6}, IsCheck: {7}, " +
                "IsBack: {8}, ColorMode: {9}, ImportTypeShortName: {10}, " +
                "BatchSourceShortName: {11}, ImagePath: {12}, BatchNumber: {13}",
                ProcessingDateKey, BankID, LockboxID, SourceBatchID, BatchID, BatchSequence,
                FileDescriptor, IsCheck, IsBack, ColorMode, ImportTypeShortName,
                BatchSourceShortName, ImagePath, BatchNumber);
        }
    }
}
