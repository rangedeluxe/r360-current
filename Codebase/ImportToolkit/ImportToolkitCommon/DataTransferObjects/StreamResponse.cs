﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [MessageContract]
    public class StreamResponse
    {
        public enum StatusCode
        {
            SUCCESS,
            FAIL
        }
       
        public StreamResponse()
        {
            Errors = new List<string>();
            Status = StatusCode.SUCCESS;
        }

        [DataMember]
        public StatusCode Status { get; set; }

        [DataMember]
        public List<string> Errors { get; set; }

        [DataMember]
        public string ResponseXml { get; set; }
    }
}
