﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class WorkgroupInfo
    {
        [DataMember]
        public int ClientAccountKey { get; set; }
        
        [DataMember]
        public string FileGroup { get; set; }
        
        [DataMember]
        public string ShortName { get; set; }
        
        [DataMember]
        public string LongName { get; set; }
        
        [DataMember]
        public bool IsActive { get; set; }
        
        [DataMember]
        public bool MostRecent { get; set; }

        [DataMember]
        public int OrganizationId { get; set; }

        [DataMember]
        public int BankId { get; set; }

        [DataMember]
        public int ClientAccountId { get; set; }

        [DataMember]
        public int SiteCodeId { get; set; }

        [DataMember]
        public string PaymentSource { get; set; }
    }
}
