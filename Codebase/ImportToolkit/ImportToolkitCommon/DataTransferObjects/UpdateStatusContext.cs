﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class UpdateStatusContext
    {
        [DataMember]
        public Nullable<Guid> SourceTrackingId { get; set; }

        [DataMember]
        public Guid ResponseTrackingId { get; set; }

        [DataMember]
        public Nullable<Guid> EntityTrackingId { get; set; }

        [DataMember]
        public bool ClientKnowsAboutRecord { get; set; }
    }
}
