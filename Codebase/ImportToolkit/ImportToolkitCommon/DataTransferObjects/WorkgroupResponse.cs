﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WFS.RecHub.R360BaseResponse;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class WorkgroupResponse : BaseResponse
    {
        [DataMember]
        public IEnumerable<WorkgroupInfo> WorkgroupList { get; set; }
    }
}
