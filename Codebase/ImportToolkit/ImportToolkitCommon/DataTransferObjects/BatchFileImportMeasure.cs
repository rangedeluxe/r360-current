﻿using System;
using System.Runtime.Serialization;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class BatchFileImportMeasure
    {
        [DataMember]
        public long SourceBatchId { get; set; }
        [DataMember]
        public int DepositDateKey { get; set; }
        [DataMember]
        public int ImmutableDateKey { get; set; }
        [DataMember]
        public int SiteBankId { get; set; }
        [DataMember]
        public int SiteWorkgroupId { get; set; }
        [DataMember]
        public int TransactionCount { get; set; }
        [DataMember]
        public int PaymentCount { get; set; }
        [DataMember]
        public int DocumentCount { get; set; }
        [DataMember]
        public int StubCount { get; set; }
        [DataMember]
        public Guid BatchTrackingId { get; set; }
    }
}