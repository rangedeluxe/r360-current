﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class AuditRequest
    {
        [DataMember]
        public string ApplicationName { get; set; }

        [DataMember]
        public string EventName { get; set; }

        [DataMember]
        public string EventType { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public Nullable<int> UserId { get; set; }

        [DataMember]
        public Nullable<int> WorkstationId { get; set; }
    }
}
