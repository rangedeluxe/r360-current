﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class MultiImageRequest
    {
        [DataMember]
        public int BankId { get; set; }
        [DataMember]
        public Int64 BatchId { get; set; }
        [DataMember]
        public IEnumerable<ImageUploadRequest> Images { get; set; }

    }
}
