﻿using System;
using System.Runtime.Serialization;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class ClientSetupResponse
    {
        [DataMember]
        public int AuditDateKey { get; set; }
        [DataMember]
        public bool IsSuccessful { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public Guid ResponseTrackingId { get; set; }
        [DataMember]
        public Guid SourceTrackingId { get; set; }
    }
}