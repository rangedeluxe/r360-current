﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class UploadRequest
    {
        [DataMember]
        public ImageInfoContract ImageInfo { get; set; }
        
        [DataMember]
        public byte[] ImageData { get; set; } 
    }
}
