﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class WorkgroupRequest
    {
        [DataMember]
        public Nullable<int> BankId { get; set; }

        [DataMember]
        public int ClientAccountId { get; set; }

        [DataMember]
        public Nullable<int> SiteCodeId { get; set; }

        [DataMember]
        public Nullable<int> OrganizationId { get; set; }

    }
}
