﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class NotificationFileInfoContract : INotificationFile
    {

        [DataMember]
        public int NotificationDateKey
        {
            get;
            set;
        }

        [DataMember]
        public int BankID
        {
            get;
            set;
        }

        [DataMember]
        public int CustomerID
        {
            get;
            set;
        }

        [DataMember]
        public int LockboxID
        {
            get;
            set;
        }

        [DataMember]
        public Guid FileIdentifier
        {
            get;
            set;
        }

        [DataMember]
        public string FileType
        {
            get;
            set;
        }

        [DataMember]
        public string UserFileName
        {
            get;
            set;
        }

        [DataMember]
        public string FileExtension
        {
            get;
            set;
        }

        [DataMember]
        public DateTime NotificationDateUTC 
        { 
            get;
            set; 
        }
    }
}
