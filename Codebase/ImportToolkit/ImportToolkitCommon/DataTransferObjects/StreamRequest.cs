﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    [MessageContract]
    public class StreamRequest
    {
        [MessageBodyMember]
        public Stream DataStream { get; set; }

        [DataMember]
        public NotificationFileInfoContract NotificationFileInfo { get; set; }

    }
}
