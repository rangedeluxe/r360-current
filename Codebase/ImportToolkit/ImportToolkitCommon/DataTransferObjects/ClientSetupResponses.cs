﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class ClientSetupResponses
    {
        [DataMember]
        public IList<ClientSetupResponse> Responses { get; set; }

        public static ClientSetupResponses CreateFromDataTable(DataTable dataTable, bool wasDatabaseCallSuccessful)
        {
            return new ClientSetupResponses
            {
                Responses = ParseResponses(dataTable, wasDatabaseCallSuccessful).ToList(),
            };
        }
        private static IEnumerable<ClientSetupResponse> ParseResponses(DataTable dataTable,
            bool wasDatabaseCallSuccessful)
        {
            if (dataTable == null || !wasDatabaseCallSuccessful)
                yield break;

            foreach (DataRow row in dataTable.Rows)
            {
                var responseTrackingId = (Guid) row["ResponseTrackingID"];
                var sourceTrackingId = (Guid) row["SourceTrackingID"];
                var auditDateKey = (int) row["AuditDateKey"];
                var responseDocument = row["XMLResponseDocument"].ToString();

                var xmlResponse = XElement.Parse(responseDocument);
                var results = xmlResponse.DescendantsAndSelf("Results");
                foreach (var result in results)
                {
                    var errorMessageElement = result?.Parent?.Element("ErrorMessage");

                    var message = errorMessageElement?.Value ?? string.Empty;
                    var isSuccessful = string.Equals(result?.Value, "success",
                        StringComparison.InvariantCultureIgnoreCase);

                    yield return new ClientSetupResponse
                    {
                        AuditDateKey = auditDateKey,
                        SourceTrackingId = sourceTrackingId,
                        ResponseTrackingId = responseTrackingId,
                        Message = message,
                        IsSuccessful = isSuccessful,
                    };
                }
            }
        }
    }
}