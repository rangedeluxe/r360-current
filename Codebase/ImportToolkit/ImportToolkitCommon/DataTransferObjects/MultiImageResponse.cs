﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ImportToolkit.Common.DataTransferObjects
{
    [DataContract]
    public class MultiImageResponse
    {
        [DataMember]
        public IEnumerable<ImageUploadResponse> ImageUploadResults { get; set; }
    }
}
