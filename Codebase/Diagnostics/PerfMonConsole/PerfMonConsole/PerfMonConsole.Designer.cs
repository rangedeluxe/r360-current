﻿namespace WindowsFormsApp1
{
    partial class PerfMonConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.collectorCreate = new System.Windows.Forms.Button();
            this.collectorName = new System.Windows.Forms.TextBox();
            this.configFileName = new System.Windows.Forms.TextBox();
            this.outputPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CollectionInterval = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.viewCheatSheetButton = new System.Windows.Forms.Button();
            this.QueryButton = new System.Windows.Forms.Button();
            this.stopCollectingButton = new System.Windows.Forms.Button();
            this.startCollectingButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.OutputBox = new System.Windows.Forms.TextBox();
            this.listCollectors = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // collectorCreate
            // 
            this.collectorCreate.Location = new System.Drawing.Point(28, 231);
            this.collectorCreate.Name = "collectorCreate";
            this.collectorCreate.Size = new System.Drawing.Size(147, 23);
            this.collectorCreate.TabIndex = 0;
            this.collectorCreate.Text = "Create/Save Collection";
            this.collectorCreate.UseVisualStyleBackColor = true;
            this.collectorCreate.Click += new System.EventHandler(this.collectorCreate_Click);
            // 
            // collectorName
            // 
            this.collectorName.Location = new System.Drawing.Point(125, 25);
            this.collectorName.Name = "collectorName";
            this.collectorName.Size = new System.Drawing.Size(155, 20);
            this.collectorName.TabIndex = 1;
            this.collectorName.Text = "defaultTrace";
            // 
            // configFileName
            // 
            this.configFileName.Location = new System.Drawing.Point(125, 57);
            this.configFileName.Name = "configFileName";
            this.configFileName.Size = new System.Drawing.Size(332, 20);
            this.configFileName.TabIndex = 2;
            this.configFileName.Text = "C:\\perflogs\\PerfMonTraceTemplate.txt";
            // 
            // outputPath
            // 
            this.outputPath.Location = new System.Drawing.Point(125, 88);
            this.outputPath.Name = "outputPath";
            this.outputPath.Size = new System.Drawing.Size(332, 20);
            this.outputPath.TabIndex = 3;
            this.outputPath.Text = "C:\\perflogs\\";
            this.outputPath.TextChanged += new System.EventHandler(this.outputPath_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Collector Set";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Config File";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Output Folder";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listCollectors);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.CollectionInterval);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.collectorName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.configFileName);
            this.groupBox1.Controls.Add(this.outputPath);
            this.groupBox1.Location = new System.Drawing.Point(5, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(687, 177);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Collector Details";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(176, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "(minutes between samples)";
            // 
            // CollectionInterval
            // 
            this.CollectionInterval.Location = new System.Drawing.Point(125, 121);
            this.CollectionInterval.MaxLength = 2;
            this.CollectionInterval.Name = "CollectionInterval";
            this.CollectionInterval.Size = new System.Drawing.Size(45, 20);
            this.CollectionInterval.TabIndex = 8;
            this.CollectionInterval.Text = "15";
            this.CollectionInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.CollectionInterval.WordWrap = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Collection Interval";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.viewCheatSheetButton);
            this.groupBox2.Controls.Add(this.QueryButton);
            this.groupBox2.Controls.Add(this.stopCollectingButton);
            this.groupBox2.Controls.Add(this.startCollectingButton);
            this.groupBox2.Location = new System.Drawing.Point(5, 276);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(687, 107);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Actions";
            // 
            // viewCheatSheetButton
            // 
            this.viewCheatSheetButton.Location = new System.Drawing.Point(466, 66);
            this.viewCheatSheetButton.Name = "viewCheatSheetButton";
            this.viewCheatSheetButton.Size = new System.Drawing.Size(157, 23);
            this.viewCheatSheetButton.TabIndex = 3;
            this.viewCheatSheetButton.Text = "View Collection Cheat Sheet";
            this.viewCheatSheetButton.UseVisualStyleBackColor = true;
            this.viewCheatSheetButton.Click += new System.EventHandler(this.viewCheatSheetButton_Click);
            // 
            // QueryButton
            // 
            this.QueryButton.Location = new System.Drawing.Point(466, 19);
            this.QueryButton.Name = "QueryButton";
            this.QueryButton.Size = new System.Drawing.Size(131, 23);
            this.QueryButton.TabIndex = 2;
            this.QueryButton.Text = "Query Collection Set";
            this.QueryButton.UseVisualStyleBackColor = true;
            this.QueryButton.Click += new System.EventHandler(this.QueryButton_Click);
            // 
            // stopCollectingButton
            // 
            this.stopCollectingButton.Location = new System.Drawing.Point(23, 66);
            this.stopCollectingButton.Name = "stopCollectingButton";
            this.stopCollectingButton.Size = new System.Drawing.Size(104, 23);
            this.stopCollectingButton.TabIndex = 1;
            this.stopCollectingButton.Text = "Stop Collecting";
            this.stopCollectingButton.UseVisualStyleBackColor = true;
            this.stopCollectingButton.Click += new System.EventHandler(this.stopCollectingButton_Click);
            // 
            // startCollectingButton
            // 
            this.startCollectingButton.Location = new System.Drawing.Point(23, 20);
            this.startCollectingButton.Name = "startCollectingButton";
            this.startCollectingButton.Size = new System.Drawing.Size(104, 23);
            this.startCollectingButton.TabIndex = 0;
            this.startCollectingButton.Text = "Start Collecting";
            this.startCollectingButton.UseVisualStyleBackColor = true;
            this.startCollectingButton.Click += new System.EventHandler(this.startCollectingButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // OutputBox
            // 
            this.OutputBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputBox.Location = new System.Drawing.Point(5, 399);
            this.OutputBox.Multiline = true;
            this.OutputBox.Name = "OutputBox";
            this.OutputBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.OutputBox.Size = new System.Drawing.Size(1246, 296);
            this.OutputBox.TabIndex = 9;
            this.OutputBox.WordWrap = false;
            // 
            // listCollectors
            // 
            this.listCollectors.Location = new System.Drawing.Point(545, 25);
            this.listCollectors.Name = "listCollectors";
            this.listCollectors.Size = new System.Drawing.Size(101, 23);
            this.listCollectors.TabIndex = 10;
            this.listCollectors.Text = "List Collectors";
            this.listCollectors.UseVisualStyleBackColor = true;
            this.listCollectors.Click += new System.EventHandler(this.listCollectors_Click);
            // 
            // PerfMonConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1263, 707);
            this.Controls.Add(this.OutputBox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.collectorCreate);
            this.Controls.Add(this.groupBox1);
            this.Name = "PerfMonConsole";
            this.Text = "PerfMonConsole";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button collectorCreate;
        private System.Windows.Forms.TextBox collectorName;
        private System.Windows.Forms.TextBox configFileName;
        private System.Windows.Forms.TextBox outputPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button startCollectingButton;
        private System.Windows.Forms.Button viewCheatSheetButton;
        private System.Windows.Forms.Button QueryButton;
        private System.Windows.Forms.Button stopCollectingButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CollectionInterval;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox OutputBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button listCollectors;
    }
}

