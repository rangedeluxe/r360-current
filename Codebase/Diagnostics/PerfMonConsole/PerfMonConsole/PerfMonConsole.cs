﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class PerfMonConsole : Form
    {
        public PerfMonConsole()
        {
            InitializeComponent();
            
        }


        private void collectorCreate_Click(object sender, EventArgs e)
        {
            
            var minutes = 0;
            int.TryParse(CollectionInterval.Text, out minutes);
            if (minutes == 0)
            {
                MessageBox.Show("CollectionInterval must be numeric value greater than 0");
                return; 
            }

            var configFile = configFileName.Text;
            var result = File.Exists($"{configFile}");
            if (!result)
            {
                MessageBox.Show($"{configFile} does not exist.");
                return;
            }

            var outputFolder = outputPath.Text;
            var folderResult = Directory.Exists($"{outputFolder}");
            if (!folderResult)
            {
                MessageBox.Show($"{outputFolder} folder does not exist.");
                return;
            }


            var quotedCollectorName = @"" + collectorName.Text + "";

            executeLogman($"delete \"{quotedCollectorName}\"", $"Deleting {collectorName.Text} ... ");
            if (!OutputBox.Text.Contains("Data Collector Set was not found"))
            {
                if (OutputBox.Text.Contains("Access is denied"))
                {
                    OutputBox.Text = OutputBox.Text + "Try running as Administrator.";
                    return;
                }
                if (OutputBox.Text.Contains("Error"))
                {
                    return;
                }
            }
            var combinedPath = Path.Combine(outputFolder, quotedCollectorName);
            executeLogman($"create counter \"{quotedCollectorName}\" -f csv -si {CollectionInterval.Text}:00 -v mmddhhmm -o \"{combinedPath}\" -cf {configFileName.Text} -ow", $"Creating {collectorName.Text } ... ");
            

        }
        private void executeLogman(string arguments, string callerText)
        {

            using (Process process = new Process())
            {
                process.StartInfo.FileName = "logman.exe";
                process.StartInfo.Arguments = arguments;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                
                process.Start();

                // Synchronously read the standard output of the spawned process. 
                StreamReader reader = process.StandardOutput;
                if (callerText == "Listing Collector Sets ...")
                {
                    var OutputBoxText = callerText + reader.ReadToEnd();
                    OutputBox.Text = OutputBoxText.Replace("-\n", "-\r\n");
                }
                else
                { 

                    // Write the redirected output to this application's window.

                    OutputBox.Text = callerText + reader.ReadToEnd();
                }
                process.WaitForExit();
            }
        }

        private void QueryButton_Click(object sender, EventArgs e)
        {
            var quotedCollectorName = @"" + collectorName.Text + "";
            executeLogman($"query \"{quotedCollectorName}\"", $"Querying {collectorName.Text} ...");
        }

        private void startCollectingButton_Click(object sender, EventArgs e)
        {
            var quotedCollectorName = @"" + collectorName.Text + "";
            executeLogman($"start \"{quotedCollectorName}\"", $"Starting {collectorName.Text} ...");
            collectorCreate.Enabled = false;
            startCollectingButton.Enabled = false;
            stopCollectingButton.Enabled = true;
        }

        private void stopCollectingButton_Click(object sender, EventArgs e)
        {
            var quotedCollectorName = @"" + collectorName.Text + "";
            executeLogman($"stop \"{quotedCollectorName}\"", $"Stopping {collectorName.Text} ...");
            collectorCreate.Enabled = true;
            startCollectingButton.Enabled = true;
            stopCollectingButton.Enabled = false;
        }

        private void viewCheatSheetButton_Click(object sender, EventArgs e)
        {
            var result = File.Exists("C:\\perflogs\\PerfMonCheatSheet.txt");
            if (!result)
            {
                MessageBox.Show($"C:\\perflogs\\PerfMonCheatSheet.txt does not exist.");
                return;
            }
            var lines = File.ReadAllLines("C:\\perflogs\\PerfMonCheatSheet.txt").ToList();

            var builder = new StringBuilder();
            foreach (var line in lines)
            {
                builder.AppendLine(line);
            }
            OutputBox.Text = builder.ToString();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void outputPath_TextChanged(object sender, EventArgs e)
        {

        }

        private void listCollectors_Click(object sender, EventArgs e)
        {
            executeLogman($"query", "Listing Collector Sets ...");
        }
    }
}