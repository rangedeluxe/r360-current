﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.DataImport.Common.DTO;

namespace WFS.DataImport.Common.Interfaces
{
    public interface ISessionSustainable<TRequest, TResponse, TSessionToken> 
    {
        TResponse BeginSession(TRequest request);
        bool EndSession(TSessionToken session);
        bool IsValidSession(TSessionToken session);
        
    }
}
