﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012-2104 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Chris Colombo
* Date: 07/25/2014
*
* Purpose: Provide a common image transfer request object for all DIT 
*          modules to use.
*
* Modification History

* WI 152407 CMC 07/25/2014
*          - Batch collision support
* WI 156262 CMC 07/29/2014
*          - Add Serialization attributes
******************************************************************************/
namespace WFS.DataImport.Common.DTO
{
    [DataContract]
    public class ImageTransferRequestDto
    {
        [DataMember]
        public DateTime ProcessingDate { get; set; }

        [DataMember]
        public int SiteBankId { get; set; }

        [DataMember]
        public int SiteAccountId { get; set; }

        [DataMember]
        public long BatchId { get; set; }

        [DataMember]
        public string ClientProcessCode { get; set; }

        [DataMember]
        public int ArchivedImageCount { get; set; }

        [DataMember]
        public int UnArchiveImageCount { get; set; }
    }
}
