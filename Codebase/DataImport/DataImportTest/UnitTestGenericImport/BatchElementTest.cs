﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMP.Generic.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportXClientBase;

namespace UnitTestGenericImport
{
    [TestClass]
    public class BatchElementTest : cDataImportXClientBase
    {
        private const string DefaultDate = "2017-01-01";

        private DataRow CreateBatchDataRow(int uniqueBatchId = 1, string workgroupId = "123",
            string batchId = "1", string processingDate = DefaultDate,
            string depositDate = DefaultDate, string batchDate = DefaultDate,
            string batchNumber = "100", string batchPaymentType = "Check", string batchSource = "Generic")
        {
            DataTable batchDataTable = new DataTable("Batch");
            batchDataTable.Columns.Add(new DataColumn(DITGenericCommon.UNIQUE_BATCH_ID, typeof(int)));
            batchDataTable.Columns.Add(new DataColumn(DITGenericCommon.BATCH_PROCESS_DATE, typeof(string)));
            batchDataTable.Columns.Add(new DataColumn(DITGenericCommon.BATCH_DEPOSIT_DATE, typeof(string)));
            batchDataTable.Columns.Add(new DataColumn(DITGenericCommon.BATCH_DATE, typeof(string)));
            batchDataTable.Columns.Add(new DataColumn("bankid", typeof(int)));
            batchDataTable.Columns.Add(new DataColumn(DITGenericCommon.BATCH_WORK_GRP_ID, typeof(string)));
            batchDataTable.Columns.Add(new DataColumn(DITGenericCommon.BATCH_ID, typeof(string)));
            batchDataTable.Columns.Add(new DataColumn(DITGenericCommon.BATCH_NUMBER, typeof(string)));
            batchDataTable.Columns.Add(new DataColumn(DITGenericCommon.BATCH_PAYMENT_TYPE, typeof(string)));
            batchDataTable.Columns.Add(new DataColumn(DITGenericCommon.BATCH_BATCH_SRC, typeof(string)));

            DataRow batchDataRow = batchDataTable.NewRow();
            batchDataRow[DITGenericCommon.UNIQUE_BATCH_ID] = uniqueBatchId;
            batchDataRow[DITGenericCommon.BATCH_PROCESS_DATE] = processingDate;
            batchDataRow[DITGenericCommon.BATCH_DEPOSIT_DATE] = depositDate;
            batchDataRow[DITGenericCommon.BATCH_DATE] = batchDate;
            batchDataRow["bankid"] = 0;
            batchDataRow[DITGenericCommon.BATCH_WORK_GRP_ID] = workgroupId;
            batchDataRow[DITGenericCommon.BATCH_ID] = batchId;
            batchDataRow[DITGenericCommon.BATCH_NUMBER] = batchNumber;
            batchDataRow[DITGenericCommon.BATCH_PAYMENT_TYPE] = batchPaymentType;
            batchDataRow[DITGenericCommon.BATCH_BATCH_SRC] = batchSource;

            batchDataTable.Rows.Add(batchDataRow);

            return batchDataRow;
        }

        private bool ValidateBatch_MissingBatchElement(string columnName)
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow();

            batchDataRow.Table.Columns.Remove(columnName);
            return batch.DoValidate(batchDataRow);

        }

        private DITGenericBatch CreateBatchElement(int uniqueBatchId = 1, string batchId = "1", string workgroupId = "123",
            string processingDate = DefaultDate, string depositDate = DefaultDate, string batchDate = DefaultDate,
            string batchNumber = "100", string batchPaymentType = "Check", string batchSource = "Generic")
        {
            DataRow batchDataRow = CreateBatchDataRow(uniqueBatchId: uniqueBatchId, batchId: batchId, workgroupId: workgroupId,
                processingDate: processingDate, depositDate: depositDate, batchDate: batchDate,
                batchNumber: batchNumber, batchPaymentType: batchPaymentType, batchSource: batchSource);

            DITGenericBatch batch = new DITGenericBatch(this);
            batch.SetData(batchDataRow);

            return batch;
        }

        [TestMethod]
        public void SetData_WorkgroupId_EqualToZero_PerformsDda()
        {
            DITGenericBatch batch = CreateBatchElement(workgroupId: "0");
            Assert.AreEqual(0, batch.BankID, "BankID");
            Assert.AreEqual(0, batch.ClientID, "ClientID");
        }

        [TestMethod]
        public void SetData_WorkgroupId_GreaterThanZero_UsesWorkgroup()
        {
            DITGenericBatch batch = CreateBatchElement(workgroupId: "1");
            Assert.AreEqual(0, batch.BankID, "BankID");
            Assert.AreEqual(1, batch.ClientID, "ClientID");
        }

        [TestMethod]
        public void SetData_WorkgroupId_LessThanZero_PerformsDda()
        {
            DITGenericBatch batch = CreateBatchElement(workgroupId: "-1");
            Assert.AreEqual(0, batch.BankID, "BankID");
            Assert.AreEqual(0, batch.ClientID, "ClientID");
        }

        [TestMethod]
        public void SetData_BankId_IsIgnored()
        {
            DITGenericBatch batch = CreateBatchElement();
            Assert.AreEqual(0, batch.BankID, "BankID");
        }

        [TestMethod]
        public void SetData_ClientId()
        {
            DITGenericBatch batch = CreateBatchElement(workgroupId: "987");
            Assert.AreEqual(987, batch.ClientID, "ClientID");
        }

        [TestMethod]
        public void SetData_BatchDate()
        {
            DITGenericBatch batch = CreateBatchElement(batchDate: "12/30/2016");
            Assert.AreEqual(Convert.ToDateTime("12/30/2016"), batch.BatchDate, "BatchDate");
        }

        [TestMethod]
        public void SetData_DepositDate()
        {
            DITGenericBatch batch = CreateBatchElement(depositDate: "12/31/2016");
            Assert.AreEqual(Convert.ToDateTime("12/31/2016"), batch.DepositDate, "DepositDate");
        }

        [TestMethod]
        public void SetData_ProcessingtDate()
        {
            DITGenericBatch batch = CreateBatchElement(processingDate: "12/30/2016");
            Assert.AreEqual(Convert.ToDateTime("12/30/2016"), batch.ProcessingDate, "ProcessingtDate");
        }

        [TestMethod]
        public void SetData_BatchID()
        {
            DITGenericBatch batch = CreateBatchElement(batchId: "100");
            Assert.AreEqual(100, batch.BatchID, "BatchID");
        }

        [TestMethod]
        public void SetData_BatchNumber()
        {
            DITGenericBatch batch = CreateBatchElement(batchNumber: "1000");
            Assert.AreEqual(1000, batch.BatchNumber, "BatchNumber");
        }

        [TestMethod]
        public void SetData_PaymentType()
        {
            DITGenericBatch batch = CreateBatchElement(batchPaymentType: "Lockbox");
            Assert.AreEqual("Lockbox", batch.PaymentType, "PaymentType");
        }

        [TestMethod]
        public void SetData_BatchSource()
        {
            DITGenericBatch batch = CreateBatchElement(batchSource: "ACH");
            Assert.AreEqual("ACH", batch.BatchSource, "BatchSource");
        }

        [TestMethod]
        public void SetData_UniqueBatchId()
        {
            DITGenericBatch batch = CreateBatchElement(uniqueBatchId: 5);
            Assert.AreEqual(5,batch.UniqueBatchID, "UniqueBatchID");
        }

        [TestMethod]
        public void ValidateBatch()
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow(batchDate: "2014-6-07", depositDate: "2014-6-07", processingDate: "2014-6-07", 
                workgroupId: "214", batchId: "354", batchNumber: "0", batchSource: "Generic", batchPaymentType: "SWIFT");

            bool validBatch = batch.DoValidate(batchDataRow);
            Assert.AreEqual(true, validBatch, "validBatch");
        }

        [TestMethod]
        public void ValidateBatch_PerformsDda()
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow(workgroupId: "-1");

            bool validBatch = batch.DoValidate(batchDataRow);
            Assert.AreEqual(true, validBatch, "validBatch");
        }

        [TestMethod]
        public void ValidateBatch_MissingBatchDate()
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow(batchDate: null);

            bool validBatch = batch.DoValidate(batchDataRow);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingBatchDate");
        }

        [TestMethod]
        public void ValidateBatch_MissingBatchDateElement()
        {
            bool validBatch = ValidateBatch_MissingBatchElement(DITGenericCommon.BATCH_DATE);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingBatchDateElement");

        }

        [TestMethod]
        public void ValidateBatch_MissingDepositDate()
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow(depositDate: null);

            bool validBatch = batch.DoValidate(batchDataRow);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingDepositDate");
        }

        [TestMethod]
        public void ValidateBatch_MissingDepositDateElement()
        {
            bool validBatch = ValidateBatch_MissingBatchElement(DITGenericCommon.BATCH_DEPOSIT_DATE);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingDepositDateElement");
        }

        [TestMethod]
        public void ValidateBatch_MissingProcessingDate()
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow(processingDate: null);

            bool validBatch = batch.DoValidate(batchDataRow);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingProcessingDate");
        }

        [TestMethod]
        public void ValidateBatch_MissingProcessingDateElement()
        {
            bool validBatch = ValidateBatch_MissingBatchElement(DITGenericCommon.BATCH_PROCESS_DATE);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingProcessingDateElement");
        }

        [TestMethod]
        public void ValidateBatch_InvalidBatchId()
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow(batchId: "-1");

            bool validBatch = batch.DoValidate(batchDataRow);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingBatchId");
        }

        [TestMethod]
        public void ValidateBatch_MissingBatchIdElement()
        {
            bool validBatch = ValidateBatch_MissingBatchElement(DITGenericCommon.BATCH_ID);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingBatchIdElement");
        }

        [TestMethod]
        public void ValidateBatch_InvalidBatchNumber()
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow(batchNumber: "-1");

            bool validBatch = batch.DoValidate(batchDataRow);
            Assert.AreEqual(false, validBatch, "ValidateBatch_InvalidBatchNumber");
        }

        [TestMethod]
        public void ValidateBatch_MissingClientId()
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow(workgroupId: null);

            bool validBatch = batch.DoValidate(batchDataRow);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingClientId");
        }

        [TestMethod]
        public void ValidateBatch_MissingClientIdElement()
        {
            bool validBatch = ValidateBatch_MissingBatchElement(DITGenericCommon.BATCH_WORK_GRP_ID);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingClientIdElement");
        }

        [TestMethod]
        public void ValidateBatch_MissingBatchSourceElement()
        {
            bool validBatch = ValidateBatch_MissingBatchElement(DITGenericCommon.BATCH_BATCH_SRC);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingBatchSourceElement");
        }

        [TestMethod]
        public void ValidateBatch_MissingBatchPaymentElement()
        {
            bool validBatch = ValidateBatch_MissingBatchElement(DITGenericCommon.BATCH_PAYMENT_TYPE);
            Assert.AreEqual(false, validBatch, "ValidateBatch_MissingBatchSourceElement");
        }

        [TestMethod]
        public void ValidateBatch_BadSetData()
        {
            DITGenericBatch batch = new DITGenericBatch(this);
            DataRow batchDataRow = CreateBatchDataRow();

            batchDataRow.Table.Columns.Remove(DITGenericCommon.BATCH_ID);
            bool validSet = batch.SetData(batchDataRow);
            Assert.AreEqual(false, validSet, "ValidateBatch_BadSetData");
        }
    }
}
