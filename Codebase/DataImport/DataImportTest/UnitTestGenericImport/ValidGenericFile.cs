﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 12/15/2014
*
* Purpose: 
*
* Modification History
* WI 180107 SAS 12/15/2014
*   -Initial Version 
******************************************************************************/
namespace UnitTestGenericImport.Generic
{
    [TestClass]
    public class ValidGenericFile : BaseGenericImport
    {

        /// <summary>
        /// The canonical XML should be generated.
        /// </summary>
        [TestMethod]
        public void GenerateCanonicalXMLForGenericBatchWithMultipleTransactions()
        {
            var actualOutput = StreamToXML("Resources\\GenericBatchWithMultipleTransactions.xml");
            // Assert
            Assert.IsNotNull(actualOutput);
        }
    }
}
