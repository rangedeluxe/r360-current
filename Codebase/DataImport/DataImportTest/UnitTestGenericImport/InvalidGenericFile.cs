﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 12/15/2014
*
* Purpose: 
*
* Modification History
* WI 180107 SAS 12/15/2014
*   -Initial Version 
******************************************************************************/
namespace UnitTestGenericImport.Generic
{
    [TestClass]
    public class InvalidGenericFile : BaseGenericImport
    {

        /// <summary>
        /// Tests the generic batch when it is no in xml format. No Canonical XML should be generated.
        /// </summary>
        [TestMethod]
        public void GenericBatchNotInXMLFormat()
        {
            InvalidFile("Resources\\GenericBatchNotInXMLFormat.xml");
        }


        /// <summary>
        /// Tests the canonical XML when Generic Batch is valid but the XSD format is not correct.
        /// </summary>
        [TestMethod]
        public void GenericBatchInXMLWithInValidXSDFormat()
        {
            InvalidFile("Resources\\GenericBatchInXMLButXSDInvalid.xml");
        }
        
        /// <summary>
        /// Tests the canonical XML when Batch Element is missing in generic.
        /// </summary>
        [TestMethod]
        public void GenericWithoutBatchElement()
        {
            InvalidFile("Resources\\GenericWithoutBatchElements.xml");
        }

        /// <summary>
        /// Tests the canonical XML when Transaction Element is missing in generic.
        /// </summary>
        [TestMethod]
        public void GenericWithoutTransactionElement()
        {
            InvalidFile("Resources\\GenericWithoutTransactionElement.xml");
        }
        
        

        

        /// <summary>
        /// The output returns for the invalid file should be null;
        /// </summary>
        /// <param name="inputfile"></param>
        public void InvalidFile(string inputfile)
        {
            // Act
            var actualoutput = StreamToXML(inputfile);
            // Assert
            Assert.AreEqual(null, actualoutput);
        }
    }
}
