﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 12/04/2014
*
* Purpose: 
*
* Modification History
* WI 180104 SAS 12/04/2014
*   -Initial Version 
******************************************************************************/
namespace ACHUnitTestProject.ACH
{
    [TestClass]
    public class InvalidACHFile : BaseACHImport
    {
        /// <summary>
        /// Tests the number of charters per line if it is not 94 then the canonical XML will be null.
        /// Verify the length of all ACH Record Types in the file 94 bytes
        /// </summary>
        [TestMethod]
        public void InvalidFileWithLessCharacters()
        {
            InvalidFile("Resources\\ACH_Less_than_94_Char.txt");
        }

        /// <summary>
        /// Tests for invalid batch count.
        /// Verify the Batch Count in the File Control record is equal to the total number of batches in the file:
        /// a.Add up all of the Batch Headers (record type 5)
        /// b.The total should equal the Batch Count (position 2-7) in the File Control (record type 9)
        /// </summary>
        [TestMethod]
        public void InvalidBatchCount()
        {
            InvalidFile("Resources\\ACH_Invalid_Batch_Count.txt");
        }

        /// <summary>
        /// Tests for invalid Entry Detail or Addenda Count batch count.
        /// Verify the Entry Detail / Addenda Count (position 14-21) in the File Control (record type 9) is equal to the total number of Entry Detail (record type 6) 
        /// and Addenda (record type 7) in the file:
        /// </summary>
        [TestMethod]
        public void InvalidEntryDetailOrAddendaCount()
        {
            InvalidFile("Resources\\ACH_Invalid_EntryDet_Or_AddendaCount.txt");
        }

        /// <summary>
        /// Validate Entry Hash
        /// a.The Entry Hash is the sum of the Entry Hash fields (position 11-20) contained within the Batch Control records (record type 8) in the file. 
        /// If the sum exceeds 10 characters, the field must be populated with the rightmost 10 characters. Example: If the sum is 123456789012, 
        /// the Entry Hash field would be populated with 3456789012. 
        /// b.The calculated Entry Hash should match the value in the Entry Hash (position 22-31) in the File Control (record type 9).
        /// </summary>
        [TestMethod]
        public void InvalidEntryHash()
        {
            InvalidFile("Resources\\ACH_Invalid_EntryHash.txt");
        }

        /// <summary>
        /// Verify the Credit Amount (position 44-55) in the File Control (record type 9) is equal to the total of the Credit Amounts (position 33-44) 
        /// in the Batch Control records (record type 8).
        /// </summary>
        [TestMethod]
        public void InvalidCreditAmount()
        {
            InvalidFile("Resources\\ACH_Invalid_Credit_Amount.txt");
        }

        /// <summary>
        /// Verify the Debit Amount (position 32-43) in the File Control (record type 9) is equal to the total of the Debit Amounts (position 21-32) 
        /// in the Batch Control records (record type 8).
        /// </summary>
        [TestMethod]        
        public void InvalidDebitAmount()
        {
            InvalidFile("Resources\\ACH_Invalid_Debit_Amount.txt");
        }

        /// <summary>
        ///Verify the Standard Entry Class Code (position 51-53) in the Batch Header (record type 5) is valid:
        ///It is valid if it contains one of the following values:
        ///i.	CCD
        ///ii.	CTX
        ///iii.	CIE
        ///iv.	PPD
        ///v.	IAT
        /// </summary>
        [TestMethod]
        public void InvalidStandardEntryClass()
        {
            InvalidFile("Resources\\ACH_Invalid_Standard_Entry_Class.txt");
        }
        
        /// <summary>
        /// Verify the Service Class Code (position 2-4) in the Batch Header (record type 5) is valid:
        ///  It is valid if it contains a value of ‘220’, which indicates credits only.
        /// </summary>
        [TestMethod]
        public void InvalidServiceClass()
        {
            InvalidFile("Resources\\ACH_Invalid_Service_Class_Code.txt");
        }

        /// <summary>
        /// Validate Settlement Date
        /// </summary>
        [TestMethod]
        public void InvalidSettlementDate()
        {
            InvalidFile("Resources\\ACH_Invalid_Settlement_Date.txt");
        }

        /// <summary>
        /// Validate Effective Date
        /// Verify the Effective Date (position 70-75) in the Batch Header (record type 5) is present and is a valid date 
        /// (this date is in YYMMDD format). 
        /// </summary>
        [TestMethod]
        public void InvalidEffectiveDate()
        {
            InvalidFile("Resources\\ACH_Invalid_Effective_Date.txt");
        }

        /// <summary>
        /// The output returns for the invalid file should be null;
        /// </summary>
        /// <param name="inputfile"></param>
        public void InvalidFile(string inputfile)
        {
            // Act
            var actualoutput = StreamToXML(inputfile);
            // Assert
            Assert.AreEqual(null, actualoutput);
        }
    }
}
