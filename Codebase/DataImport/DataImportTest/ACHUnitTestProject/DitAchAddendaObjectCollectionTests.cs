﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using DMP.ACH.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ACHUnitTestProject
{
    [TestClass]
    public class DitAchAddendaObjectCollectionTests
    {
        private DITACHAddendaObjectCollection CreateAddendaCollection(params string[] rawDataStrings)
        {
            var collection = new DITACHAddendaObjectCollection();
            foreach (var rawData in rawDataStrings)
                collection.Add(new DITACHAddendaObject {RawACHData = rawData});
            return collection;
        }
        private string CreateAddendaRecord(string data)
        {
            return "705" + data.PadRight(80) + "00010000001";
        }
        private XDocument BuildRemittanceDataNode(string batchType, DITACHAddendaObjectCollection collection)
        {
            var document = new XmlDocument();
            document.AppendChild(document.CreateElement("Root"));
            var documentElement = document.DocumentElement;
            collection.BuildRemittanceDataElementNode(batchType, document, ref documentElement);
            return XDocument.Parse(document.OuterXml);
        }
        private void CheckAllRemittanceFields(XDocument document, params string[] expectedFieldsAndValues)
        {
            var actualFieldsAndValues = GetRemittanceFields(document)
                .Select(pair => $"{pair.Key}={pair.Value}");

            Assert.AreEqual(ListToString(expectedFieldsAndValues), ListToString(actualFieldsAndValues));
        }
        private void CheckRemittanceField(XDocument document, string fieldName, string expectedFieldValue)
        {
            var dictionary = GetRemittanceFields(document);
            Assert.AreEqual(expectedFieldValue, dictionary[fieldName], fieldName);
        }
        private IDictionary<string, string> GetRemittanceFields(XDocument document)
        {
            return document.Descendants("RemittanceData").ToDictionary(
                element => element.Attribute("FieldName")?.Value,
                element => element.Attribute("FieldValue")?.Value);
        }
        private string ListToString(IEnumerable<string> values)
        {
            var sortedValues = values.OrderBy(value => value, StringComparer.InvariantCultureIgnoreCase);
            return string.Join("; ", sortedValues);
        }

        // BPR data
        [TestMethod]
        public void ParsedBprData()
        {
            var collection = CreateAddendaCollection(
                CreateAddendaRecord(@"BPR*C*2723.93*C*ACH*CTX*01*103088819*ZZ*11111111***01*10108881\"));

            var document = BuildRemittanceDataNode(DITACHCommon.ACH_BATCH_TYPE_CCD, collection);

            CheckAllRemittanceFields(document,
                "AccountNumber=11111111",
                "BPRAccountNumber=11111111",
                "BPRMonetaryAmount=2723.93");
        }
        [TestMethod]
        public void ParsedBprData_AddsCentsIfMissing()
        {
            var collection = CreateAddendaCollection(
                CreateAddendaRecord(@"BPR*C*2723*C*ACH*CTX*01*103088819*ZZ*11111111***01*10108881\"));

            var document = BuildRemittanceDataNode(DITACHCommon.ACH_BATCH_TYPE_CCD, collection);

            CheckRemittanceField(document, "BPRMonetaryAmount", "2723.00");
        }
        [TestMethod]
        public void ParsedBprData_LeadingNegativeSign()
        {
            var collection = CreateAddendaCollection(
                CreateAddendaRecord(@"BPR*C*-2723.93*C*ACH*CTX*01*103088819*ZZ*11111111***01*10108881\"));

            var document = BuildRemittanceDataNode(DITACHCommon.ACH_BATCH_TYPE_CCD, collection);

            CheckRemittanceField(document, "BPRMonetaryAmount", "-2723.93");
        }
        [TestMethod]
        public void ParsedBprData_TrailingNegativeSign_IsChangedToLeadingNegativeSignInCanonicalXml()
        {
            var collection = CreateAddendaCollection(
                CreateAddendaRecord(@"BPR*C*2723.93-*C*ACH*CTX*01*103088819*ZZ*11111111***01*10108881\"));

            var document = BuildRemittanceDataNode(DITACHCommon.ACH_BATCH_TYPE_CCD, collection);

            CheckRemittanceField(document, "BPRMonetaryAmount", "-2723.93");
        }

        // RMR data
        [TestMethod]
        public void ParsedRmrData()
        {
            var collection = CreateAddendaCollection(
                CreateAddendaRecord(@"RMR*IV*PM266666**40.93*93.40*0\"));

            var document = BuildRemittanceDataNode(DITACHCommon.ACH_BATCH_TYPE_CCD, collection);

            CheckAllRemittanceFields(document,
                "Amount=40.93",
                "InvoiceNumber=PM266666",
                "RMRReferenceNumber=PM266666",
                "RMRMonetaryAmount=40.93",
                "RMRTotalInvoiceAmount=93.40",
                "RMRDiscountAmount=0.00");
        }
        [TestMethod]
        public void ParsedRmrData_LeadingNegativeSigns()
        {
            var collection = CreateAddendaCollection(
                CreateAddendaRecord(@"RMR*IV*PM266666**-40.93*-93.40*0\"));

            var document = BuildRemittanceDataNode(DITACHCommon.ACH_BATCH_TYPE_CCD, collection);

            CheckRemittanceField(document, "Amount", "-40.93");
            CheckRemittanceField(document, "RMRMonetaryAmount", "-40.93");
            CheckRemittanceField(document, "RMRTotalInvoiceAmount", "-93.40");
        }
        [TestMethod]
        public void ParsedRmrData_TrailingNegativeSigns_AreChangedToLeadingNegativeSignsInCanonicalXml()
        {
            var collection = CreateAddendaCollection(
                CreateAddendaRecord(@"RMR*IV*PM266666**40.93-*93.40-*0\"));

            var document = BuildRemittanceDataNode(DITACHCommon.ACH_BATCH_TYPE_CCD, collection);

            CheckRemittanceField(document, "Amount", "-40.93");
            CheckRemittanceField(document, "RMRMonetaryAmount", "-40.93");
            CheckRemittanceField(document, "RMRTotalInvoiceAmount", "-93.40");
        }

        // TRN data
        [TestMethod]
        public void ParsedTrnData()
        {
            var collection = CreateAddendaCollection(
                CreateAddendaRecord(@"TRN*1*051036621690138\"));

            var document = BuildRemittanceDataNode(DITACHCommon.ACH_BATCH_TYPE_CCD, collection);

            CheckAllRemittanceFields(document, "ReassociationTraceNumber=051036621690138");
        }
    }
}