﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using DMP.ACH.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportXClientBase;
// ReSharper disable RedundantExplicitParamsArrayCreation

namespace ACHUnitTestProject
{
	[TestClass]
	public class DitAchImportTests
	{
		private static XDocument DatToCanonical(params string[] inputLines)
		{
			var xclient = new DITACHImport();
			var errors = new List<string>();
			xclient.LogEvent += (sender, message, source, messageType, importance) =>
			{
				if (messageType == LogEventType.Error)
					errors.Add(message);
			};

			var configData = new cConfigData(iDefaultBankID: 99, iDefaultLockboxID: 999);
			var inputText = string.Join(Environment.NewLine, inputLines);
			var inputStream = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(inputText)));

			XmlDocument xmlDocument;
			var success = xclient.StreamToXML(inputStream, enmFileType.Batch, configData, out xmlDocument);
			if (errors.Any())
				Assert.Fail("Error(s) were logged:" + Environment.NewLine + string.Join(Environment.NewLine, errors));
			Assert.IsTrue(success, "Result");
			Assert.IsNotNull(xmlDocument, "xmlDocument");

			return XDocument.Parse(xmlDocument.OuterXml);
		}
		private static void AssertRawAddendaValues(XDocument canonicalXml, params string[] expectedLines)
		{
			var actualLines = canonicalXml
				.Descendants("RawData")
				.Select(element => element.Value)
				.ToList();

			var expectedText = string.Join(Environment.NewLine, expectedLines);
			var actualText = string.Join(Environment.NewLine, actualLines);
			Assert.AreEqual(expectedText, actualText, "Raw addenda values");
		}

		[TestMethod]
		public void SimpleCcdFile()
		{
			var xDocument = DatToCanonical(
				"101 072001999 0720019991612060021A094101MI POST-C MOSINEE CCD BANK FILL IN THE SPACES ON FILEY",
				"5220CMA1            GROUP 348820 PAPER M1641060124CCDBNKCDDDEP 1611011611010005202000090000099",
				"6220720019990000000321917999900004911981089295134878  HELM AUTOMOTIVE MANUALMD0007938000000006",
				"705First                                                                       line00012957573",
				"822000000800576000720000000000000000129621371641060124                         072000090000099",
				"9000001007938000000020057600072000000000000000012962137                                       ");

			AssertRawAddendaValues(xDocument,
				"First                                                                       line");
		}
		[TestMethod]
		public void SkipsBlankAddendaRecords()
		{
			var xDocument = DatToCanonical(
				"101 072001999 0720019991612060021A094101MI POST-C MOSINEE CCD BANK FILL IN THE SPACES ON FILEY",
				"5220CMA1            GROUP 348820 PAPER M1641060124CCDBNKCDDDEP 1611011611010005202000090000099",
				"6220720019990000000321917999900004911981089295134878  HELM AUTOMOTIVE MANUALMD0007938000000006",
				"705First                                                                       line00012957573",
				"705Hello                                                                      world00032957573",
				"705                                                                                00032957573",
				"705Last                                                                        line00012957573",
				"822000000800576000720000000000000000129621371641060124                         072000090000099",
				"9000001007938000000050057600072000000000000000012962137                                       ");

			AssertRawAddendaValues(xDocument,
				"First                                                                       line",
				"Hello                                                                      world",
				"Last                                                                        line");
		}


		[TestMethod]
		public void ParseIATOFACIndicatorTest()
		{
			string iatFile =
				"101 072000096 0720000961810110734A094101MI POST-C RED MICHIGAN Deluxe Corp            00004972\r\n" +
				"5220DELUXE CORP     DISCRESIONARYDATA   0720000961IATPAYMENT   1810111810060001 07200000000001\r\n" +
				"622089089089980980980        0000399770980980980                            XY1000000000000001\r\n" +
				"702ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001\r\n" +
				"7024*U*00400*076913354*0*P*>\\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001\r\n" +
				"702010\\ST*820*000000268\\BPR*C*0.08*C*ACH*CTX*01*65465465*DA*WC1RB7IUR*TFMQ2G185\\TRN00030000001\r\n" +
				"702*1*H7SKO\\N1*PR*SIHCP6IJ6B\\N1*PE*VAWEYF96DY\\BPR*C*0.55*C*ACH*CTX*01*65465465*DA*X00040000001\r\n" +
				"702CTP7UJC7*XO7I98OM5\\TRN*1*C3BJN\\N1*PE*TZ727G1GML\\N1*PE*CTTVV36S5I\\SE*19*00000026800050000001\r\n" +
				"702\\GE*1*76913354\\IEA*1*076913354\\                                                 00060000001\r\n" +
				"822000000789089089  0000000000000000003997700720000961                          07200000000001\r\n" +
				"9000001000011000000070089089089000000000000000000399770                                       \r\n";
			var doc = DatToCanonical(iatFile);
			int cnt = 0;
			foreach (var element in doc.Descendants("RemittanceData"))
			{
				if (element.FirstAttribute.Value == "OFACIndicator1")
				{
					Assert.IsTrue(element.LastAttribute.Value == "X");
					cnt++;
				}

				if (element.FirstAttribute.Value == "OFACIndicator2")
				{
					Assert.IsTrue(element.LastAttribute.Value == "Y");
					cnt++;
				}
			}
			Assert.AreEqual(2, cnt);
		}

		[TestMethod]
		public void ParseIATCurrencyCodeTest()
		{
			string iatFile =
				"101 072000096 0720000961810111138A094101MI POST-C RED MICHIGAN Deluxe Corp            00000654\r\n" +
				"5220DELUXE CORP     DISCRESIONARYDATA   0720000961IATPAYMENT   EURGHS1810090001 07200000000001\r\n" +
				"632089089089980980980        0000976228980980980                            XY1000000000000001\r\n" +
				"702ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001\r\n" +
				"7024*U*00400*076913354*0*P*>\\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001\r\n" +
				"702010\\ST*820*000000268\\BPR*C*62.19*C*ACH*CTX*01*65465465*DA*4W3N87FBG*4TUXD38YB\\TR00030000001\r\n" +
				"702N*1*6ME4W\\N1*PR*W7M6K01ZLN\\N1*PE*UW9925TPC9\\BPR*C*7.47*C*ACH*CTX*01*65465465*DA*00040000001\r\n" +
				"70286TRLHVDS*PUJQ0OCQQ\\TRN*1*41AEN\\N1*PE*U2443GIMP6\\N1*PE*1HRJZYS7B7\\SE*19*0000002600050000001\r\n" +
				"7028\\GE*1*76913354\\IEA*1*076913354\\                                                00060000001\r\n" +
				"822000000789089089  0000000000000000009762280720000961                          07200000000001\r\n" +
				"9000001000011000000070089089089000000000000000000976228                                       \r\n";
			var doc = DatToCanonical(iatFile);
			int cnt = 0;
			foreach (var element in doc.Descendants("RemittanceData"))
			{
				if (element.FirstAttribute.Value == "ISOOriginatingCurrencyCode")
				{
					Assert.IsTrue(element.LastAttribute.Value == "EUR");
					cnt++;
				}

				if (element.FirstAttribute.Value == "ISODestinationCurrencyCode")
				{
					Assert.IsTrue(element.LastAttribute.Value == "GHS");
					cnt++;
				}
			}
			Assert.AreEqual(2, cnt);
		}

		[TestMethod]
		public void ParseIATForeignExchangeCodeTest()
		{
			string iatFile = "101 072000096 0720000961810120741A094101MI POST-C RED MICHIGAN Deluxe Corp            00001022\r\n" +
							 "5220DELUXE CORP     FF3FOREIGNEXCHREF US0720000961IATPAYMENT   EURPGK1810140001 07200000000001\r\n" +
							 "627089089089980980980        0000697176980980980                            XY1000000000000001\r\n" +
							 "702ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001\r\n" +
							 "7024*U*00400*076913354*0*P*>\\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001\r\n" +
							 "702010\\ST*820*000000268\\BPR*C*13.20*C*ACH*CTX*01*65465465*DA*0UOR6UHAU*ZQ20LQNF1\\TR00030000001\r\n" +
							 "702N*1*IDIMI\\N1*PE*Q7I1287YKA\\N1*PE*D1IA6RKTYJ\\SE*19*000000268\\GE*1*76913354\\IEA*1*00040000001\r\n" +
							 "702076913354\\                                                                      00050000001\r\n" +
							 "822000000689089089  0000006971760000000000000720000961                          07200000000001\r\n" +
							 "9000001000010000000060089089089000000697176000000000000                                       \r\n";
			var doc = DatToCanonical(iatFile);
			int cnt = 0;
			foreach (var element in doc.Descendants("RemittanceData"))
			{
				if (element.FirstAttribute.Value == "ForeignExchangeIndicator")
				{
					Assert.IsTrue(element.LastAttribute.Value == "FF");
					cnt++;
				}

				if (element.FirstAttribute.Value == "ForeignExchangeRefIndicator")
				{
					Assert.IsTrue(element.LastAttribute.Value == "3");
					cnt++;
				}

				if (element.FirstAttribute.Value == "ForeignExchangeReference")
				{
					Assert.IsTrue(element.LastAttribute.Value == "FOREIGNEXCHREF");
					cnt++;
				}
				if (element.FirstAttribute.Value == "ISODestinationCountryCode")
				{
					Assert.IsTrue(element.LastAttribute.Value == "US");
					cnt++;
				}
			}
			Assert.AreEqual(4, cnt);

		}

		[TestMethod]
		public void ParseIATForeignExchangeCodeBlankTest()
		{
			string iatFile = "101 072000096 0720000961810120741A094101MI POST-C RED MICHIGAN Deluxe Corp            00001022\r\n" +
							 "5220DELUXE CORP                         0720000961IATPAYMENT   EURPGK1810140001 07200000000001\r\n" +
							 "627089089089980980980        0000697176980980980                            XY1000000000000001\r\n" +
							 "702ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001\r\n" +
							 "7024*U*00400*076913354*0*P*>\\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001\r\n" +
							 "702010\\ST*820*000000268\\BPR*C*13.20*C*ACH*CTX*01*65465465*DA*0UOR6UHAU*ZQ20LQNF1\\TR00030000001\r\n" +
							 "702N*1*IDIMI\\N1*PE*Q7I1287YKA\\N1*PE*D1IA6RKTYJ\\SE*19*000000268\\GE*1*76913354\\IEA*1*00040000001\r\n" +
							 "702076913354\\                                                                      00050000001\r\n" +
							 "822000000689089089  0000006971760000000000000720000961                          07200000000001\r\n" +
							 "9000001000010000000060089089089000000697176000000000000                                       \r\n";
			var doc = DatToCanonical(iatFile);
			int cnt = 0;
			foreach (var element in doc.Descendants("RemittanceData"))
			{
				if (element.FirstAttribute.Value.Trim() == "ForeignExchangeIndicator")
				{
					cnt++;
				}

				if (element.FirstAttribute.Value == "ForeignExchangeRefIndicator")
				{
					cnt++;
				}

				if (element.FirstAttribute.Value == "ForeignExchangeReference")
				{
					cnt++;
				}
				if (element.FirstAttribute.Value == "ISODestinationCountryCode")
				{
					cnt++;
				}
			}
			Assert.AreEqual(0, cnt);

		}
		[TestMethod]
		public void ParseCTXNoIATFieldsTest()
		{
			string iatFile = "101 072000096 0720000961810120807A094101MI POST-C RED MICHIGAN Deluxe Corp            00000182\r\n" +
			                 "5220DELUXE CORP     DISCRESIONARYDATA   0720000961CTXPAYMENT   1810121810150001 07200000000001\r\n" +
			                 "632089089089980980980        000098758107000096       0001DAKOTA BANK       XX1000000000000001\r\n" +
			                 "705ISA*00*          *00*          *ZZ*SVAUTONA       *01*048463400P     *130510*17200010000001\r\n" +
			                 "7054*U*00400*076913354*0*P*>\\GS*RA*SVAUTONA*048463400P*20130510*1724*76913354*X*00400020000001\r\n" +
			                 "705010\\ST*820*000000268\\BPR*C*0.23*C*ACH*CTX*01*65465465*DA*LWPU7RFHN*RCLQEOSJV\\TRN00030000001\r\n" +
			                 "705*1*J5584\\N1*PE*M33VSANUG7\\N1*PE*70IJYYCL8J\\SE*19*000000268\\GE*1*76913354\\IEA*1*000040000001\r\n" +
			                 "70576913354\\                                                                       00050000001\r\n" +
			                 "822000000689089089  0000000000000000009875810720000961                          07200000000001\r\n" +
			                 "9000001000010000000060089089089000000000000000000987581                                       \r\n";
			var doc = DatToCanonical(iatFile);
			int cnt = 0;
			foreach (var element in doc.Descendants("RemittanceData"))
			{
				Assert.AreNotEqual(element.FirstAttribute.Value, "ForeignExchangeIndicator");
				Assert.AreNotEqual(element.FirstAttribute.Value, "ForeignExchangeReference");
				Assert.AreNotEqual(element.FirstAttribute.Value, "ISODestinationCountryCode");
				Assert.AreNotEqual(element.FirstAttribute.Value, "ForeignExchangeRefIndicator");
				Assert.AreNotEqual(element.FirstAttribute.Value, "ISOOriginatingCurrencyCode");
				Assert.AreNotEqual(element.FirstAttribute.Value, "ISODestinationCurrencyCode");
				Assert.AreNotEqual(element.FirstAttribute.Value, "OFACIndicator1");
				Assert.AreNotEqual(element.FirstAttribute.Value, "OFACIndicator2");
				if (element.FirstAttribute.Value == "CompanyData")
				{
					Assert.IsTrue(element.LastAttribute.Value == "DISCRESIONARYDATA");
					cnt++;
				}
				if (element.FirstAttribute.Value == "DescriptiveDate")
				{
					Assert.IsTrue(element.LastAttribute.Value == "181012");
					cnt++;
				}
			}
			Assert.AreEqual(2, cnt);
		}
	}
}