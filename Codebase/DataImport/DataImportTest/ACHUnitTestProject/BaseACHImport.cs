﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System.IO;
using DMP.ACH.Components;
using WFS.LTA.DataImport.DataImportXClientBase;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 12/04/2014
*
* Purpose:
*
* Modification History
* WI 180104 SAS 12/04/2014
*   -Initial Version
******************************************************************************/
namespace ACHUnitTestProject.ACH
{
    [TestClass]
    public abstract class BaseACHImport
    {
        /// <summary>
        /// Run the StreamToXML for ACH and return the resulted Canonical XML
        /// </summary>
        /// <param name="strInputFile">File location of the input XML.</param>
        /// <returns>The generated canonical XML</returns>
        public System.Xml.XmlDocument StreamToXML(string strInputFile)
        {
            StreamReader stmInputFile = new StreamReader(strInputFile);
            cConfigData cConfigData = new cConfigData(1, 1);
            XmlDocument xOutPut = new XmlDocument();
            DITACHImport objACH = new DITACHImport();
            objACH.StreamToXML(stmInputFile, enmFileType.Batch, cConfigData, out xOutPut);
            return xOutPut;
        }
    }
}
