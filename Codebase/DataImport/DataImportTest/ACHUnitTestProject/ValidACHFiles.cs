﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 12/04/2014
*
* Purpose: 
*
* Modification History
* WI 180104 SAS 12/04/2014
*   -Initial Version 
******************************************************************************/
namespace ACHUnitTestProject.ACH
{
    [TestClass]
    public class ValidACHFile : BaseACHImport
    {
        /// <summary>
        /// Tests the document element should be there in the canonical XML.
        /// </summary>
        [TestMethod]
        public void GenerateCanonicalXMLWithDocumentElement()
        {   
            var actualOutput = StreamToXML("Resources\\ACH_Without_ValidAddenda.txt");
            // Assert
            Assert.IsTrue(actualOutput.InnerXml.Contains("Document"));
        }

        /// <summary>
        /// Tests the ACH canonical XML is generated and value of BatchID should be zero.
        /// </summary>
        [TestMethod]
        public void GenerateCanonicalXMLWithBatchIDSetToZero()
        {
            bool bResult=false;
            var actualOutput = StreamToXML("Resources\\ACH_Without_ValidAddenda.txt");
            XmlNode nodeBatches,nodeBatch;
            nodeBatches=actualOutput.SelectSingleNode("Batches");
            int iTemp;
            if (!(nodeBatches == null) && (nodeBatches.ChildNodes.Count > 0))
            {
                for (int intIndex = 0; intIndex < nodeBatches.ChildNodes.Count; ++intIndex)
                {
                    nodeBatch = nodeBatches.ChildNodes[intIndex];
                    if (nodeBatch.Name.ToLower() == "batch")
                    {
                        if (nodeBatch.Attributes["BatchID"] != null && int.TryParse(nodeBatch.Attributes["BankID"].Value, out iTemp))
                        {
                            if (iTemp == 0)
                                bResult = true;
                            else
                            {
                                bResult = false;
                                break;
                            }
                        }
                    }
                }
            }
            // Assert
            Assert.IsTrue(bResult);
        }
    }
}
