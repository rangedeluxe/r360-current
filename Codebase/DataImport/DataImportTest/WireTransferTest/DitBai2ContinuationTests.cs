﻿using DMP.BAI2.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WireTransferTest
{
    [TestClass]
    public class DitBai2ContinuationTests
    {
        [TestMethod]
        public void AddendaRawData_IsParsedFromRawContinuationData()
        {
            var continuation = new DITBAI2Continuation {RawContinuationData = "88,Continuation Data"};
            Assert.AreEqual("Continuation Data", continuation.AddendaRawData);
        }
        [TestMethod]
        public void AddendaRawData_CanIncludeCommas()
        {
            var continuation = new DITBAI2Continuation {RawContinuationData = "88,abc,def"};
            Assert.AreEqual("abc,def", continuation.AddendaRawData);
        }
    }
}