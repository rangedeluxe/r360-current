﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 12/04/2014
*
* Purpose:
*
* Modification History
* WI 180105 SAS 12/04/2014
*   -Initial Version
******************************************************************************/

namespace WireTransferTest
{
    [TestClass]
    public class InvalidWireTransferFile : BaseWireTransferImport
    {
        private void AssertFileIsInvalid(string inputfile)
        {
            var xmlDocument = FileToCanonicalXml(inputfile);
            Assert.IsNull(xmlDocument);
        }

        /// <summary>
        /// Verify the Number of Records (field 4) in the File Trailer (record 99):
        /// This value should equal the total number of records of all codes in the file,
        /// including continuation records, headers and trailers (and including this 99 record).
        /// </summary>
        [TestMethod]
        public void InvalidFileTrailerRecordCount()
        {
            AssertFileIsInvalid("Resources\\Wire_Invalid_File_Trailer_Record_Count.txt");
        }

        /// <summary>
        /// Verify the file Trailer Group Count Number of Records (field 3) in the File Trailer (record 99):
        /// This value should equal the total number of records of code (02) in the file.
        /// </summary>
        [TestMethod]
        public void InvalidFileTrailerGroupRecordCount()
        {
            AssertFileIsInvalid("Resources\\Wire_Invalid_File_Trailer_Group_Record_Count.txt");
        }

        /// <summary>
        /// Verify the File Control Total (Field 2)  in File Trailer Record (record 99) matches
        /// the algebraic sum of all group control totals in this file.
        /// The Group Control Total (Field 2) in the Group Trailer Record (record 98)
        /// </summary>
        [TestMethod]
        public void InvalidFileTrailerControlTotal()
        {
            AssertFileIsInvalid("Resources\\Wire_Invalid_File_Trailer_Control_Total.txt");
        }

        /// <summary>
        /// Verify the Number of Records (field 4) in the Group Trailer (record 98)
        /// This value should equal the total number of all records in this group.
        /// Include the 02, all 03, 16, 49, and 88 records, and this 98 record.
        /// </summary>
        [TestMethod]
        public void InvalidGroupTrailerRecordCount()
        {
            AssertFileIsInvalid("Resources\\Wire_Invalid_Group_Trailer_Record_Count.txt");
        }

        /// <summary>
        /// Verify the Number of Accounts (Field 3) in Group Trailer Record (Record 98) matches
        /// with the number of records (Field 3) in Account Trailer Records (record 49) in the file.
        /// </summary>
        [TestMethod]
        public void InvalidGroupTrailerAccountRecordCount()
        {
            AssertFileIsInvalid("Resources\\Wire_Invalid_Group_Trailer_Account_Record_Count.txt");
        }

        /// <summary>
        /// Verify the Group Control Total (Field 2)  in Group Trailer Record (record 98) matches
        /// the algebraic sum of all account control totals in this file.
        /// The Account Control Total (Field 2) in the Account Trailer Record (record 49)
        /// </summary>
        [TestMethod]
        public void InvalidGroupTrailerControlTotal()
        {
            AssertFileIsInvalid("Resources\\Wire_Invalid_Group_Trailer_Control_Total.txt");
        }

        /// <summary>
        /// Verify the Number of Records (field 3) in the Account Trailer (record 49):
        /// This value should equal the total number of records in the account, including the
        /// 03 record and all 16 and 88 records, and including this account trailer 49 record.
        /// </summary>
        [TestMethod]
        public void InvalidAccountTrailerRecordCount()
        {
            AssertFileIsInvalid("Resources\\Wire_Invalid_Accont_Trailer_Record_Count.txt");
        }
    }
}
