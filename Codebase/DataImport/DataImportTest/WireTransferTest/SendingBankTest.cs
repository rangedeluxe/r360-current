﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WireTransferTest
{
    [TestClass]
    public class SendingBankTest : BaseWireTransferImport
    {
        [TestMethod]
        public void CanonicalXml_ContainsSendingBankElement()
        {
            // Wire_With_SendingBank_88Record.txt has 88,Sending Bank: 084000026 - Sending Bank Reference: FT170008006001\
            // The inner xml should have FieldValue="084000026" FieldName="SendingBank"


            string SendingBankFile = "Resources\\Wire_With_SendingBank_88Record.txt";
            var xmlDocument = FileToCanonicalXml(SendingBankFile);
            Assert.IsNotNull(xmlDocument);
            Assert.IsNotNull(xmlDocument.InnerXml);
            StringAssert.Contains(xmlDocument.InnerXml, "FieldName=\"SendingBank\"");
            StringAssert.Contains(xmlDocument.InnerXml, "FieldValue=\"084000026\"");
        }


        [TestMethod]
        public void CanonicalXml_NotContainsSendingBankElement()
        {
            //  Resources\\Wire_Without_SendingBank_88Record.txt does not have 88,Sending Bank: 084000026 - Sending Bank Reference: FT170008006001 \
            // should generate xmlDocument with our errors.


            string SendingBankFile = "Resources\\Wire_Without_SendingBank_88Record.txt";
            var xmlDocument = FileToCanonicalXml(SendingBankFile);
            Assert.IsNotNull(xmlDocument);
            Assert.IsNotNull(xmlDocument.InnerXml);

        }

        [TestMethod]
        public void CanonicalXml_Contains_Structured_SendingBankElement()
        {
            //Structured Wire_With_SendingBank_88Record.txt has 88,Sending Bank: 084000026 - Sending Bank Reference: FT170008006001\
            // The inner xml should have FieldValue="084000026" FieldName="SendingBank"


            string SendingBankFile = "Resources\\Wire_With_SendingBank_88Record_Structured.txt";
            var xmlDocument = FileToCanonicalXml(SendingBankFile);
            Assert.IsNotNull(xmlDocument);
            Assert.IsNotNull(xmlDocument.InnerXml);
            StringAssert.Contains(xmlDocument.InnerXml, "FieldName=\"SendingBank\"");
            StringAssert.Contains(xmlDocument.InnerXml, "FieldValue=\"094000026\"");
        }

        [TestMethod]
        public void CanonicalXml_Contains_SendingBank_without_HyphenAtEnd()
        {
            // Wire_With_SendingBank_88Record.txt has 88,Sending Bank: 084000026 
            // The inner xml should have FieldValue starting with "084000026" FieldName="SendingBank"(without hyphen at the end)


            string SendingBankFile = "Resources\\Wire_With_SendingBank_88Record_without_HyphenAtEnd.txt";
            var xmlDocument = FileToCanonicalXml(SendingBankFile);
            Assert.IsNotNull(xmlDocument);
            Assert.IsNotNull(xmlDocument.InnerXml);
            StringAssert.Contains(xmlDocument.InnerXml, "FieldName=\"SendingBank\"");

            //  Everything after 'SendingBank:' is copied!!; FieldValue="094000026/6*[875]**:ESA:NOTCONVERTED*\"
            StringAssert.Contains(xmlDocument.InnerXml, "FieldValue=\"094000026");
        }

    }
}
