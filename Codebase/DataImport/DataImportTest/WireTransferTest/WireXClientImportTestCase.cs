using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using DMP.BAI2.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportXClientBase;

namespace WireTransferTest
{
    public abstract class WireXClientImportTestCase
    {
        private const int DefaultBankId = 1;
        private const int DefaultWorkgroupId = 2;

        [TestInitialize]
        public void SetUp()
        {
            CanonicalXml = null;
        }

        private XDocument CanonicalXml { get; set; }
        protected XElement PaymentElement => PaymentElements.First();
        protected IEnumerable<XElement> PaymentElements => CanonicalXml.Descendants("Payment");
        private IEnumerable<XElement> RawDataElements => CanonicalXml.Descendants("RawData");
        protected IEnumerable<string> RawDataValues => RawDataElements.Select(e => e.Value);
        private IEnumerable<XElement> RemittanceDataElements => CanonicalXml.Descendants("RemittanceData");
        protected IEnumerable<XElement> TransactionElements => CanonicalXml.Descendants("Transaction");

        private static Stream CreateStream(IEnumerable<string> lines)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            foreach (var line in lines)
                writer.WriteLine(line);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        private static cDataImportXClientBase CreateXClient()
        {
            var xClient = new DITBAI2Import();
            xClient.LogEvent += (sender, message, source, eventType, eventImportance) =>
            {
                if (eventType != LogEventType.Information)
                    throw new Exception($"{eventType}: {message}");
            };
            return xClient;
        }
        protected string GetRemittanceDataValue(string fieldName)
        {
            var element = RemittanceDataElements.FirstOrDefault(e => e.Attribute("FieldName")?.Value == fieldName);
            Assert.IsNotNull(element, $"RemittanceData element with FieldName='{fieldName}'");

            var fieldValueAttribute = element.Attribute("FieldValue");
            Assert.IsNotNull(fieldValueAttribute, "RemittanceData element's FieldValue attribute");

            return fieldValueAttribute.Value;
        }
        protected void ImportToCanonicalXml(IEnumerable<string> inputLines)
        {
            var stream = CreateStream(inputLines);
            using (var reader = new StreamReader(stream))
            {
                var xClient = CreateXClient();
                var configData = new cConfigData(DefaultBankId, DefaultWorkgroupId);

                XmlDocument xmlDocument;
                xClient.StreamToXML(reader, enmFileType.Batch, configData, out xmlDocument);

                Assert.IsNotNull(xmlDocument, $"Canonical XML returned from {nameof(xClient.StreamToXML)}");
                CanonicalXml = xmlDocument.ToXDocument();
            }
        }
    }
}