﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using DMP.BAI2.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportXClientBase;

namespace WireTransferTest
{
	/// <summary>
	/// Summary description for DITBAI2ImportTest
	/// </summary>
	[TestClass]
	public class DITBAI2ImportTests
	{
		private DITBAI2Import importer = null;
		private StreamReader reader = null;
		private MemoryStream ms = null;
		private string actual = string.Empty;
		private string wireData = @"01,192000422,112233445,170928,0803,1505,80,,2/
02,112233445,112233445,1,180921,0803,,3/
03,112233445,,,,,,,,,/
16,195,229056455,S,000326352,ROCTENCOM/
88,ADDENDA={8300}OIPROPGREEN POWER SOLUTIONS, LLC*1154300************507 ACADEMY
88, AVE. DUBLIN GA 12312*{8350}WESTROCK RKT*OI*PROP*8800372087***********504
88, THRASHER ST. NORCROSS, GA 12321*{8450}USD2259026.44*/
16,195,705000,S,000324562,ROCTENCOM/
88,ADDENDA={8300}OIPROPIRNSTENGEL INVESTMENTS,
88, INC*000009532153222************608 PLYMOUTH RD MINNETONKA           MN
88, 55503-1074*{8350}WESTROCK*OI*PROP*8800372087************{8450}USD7500*{8750}
88,ATTN: LISA SHECRER DEER PARK NY*/
49,225924454,11/
03,112233445,USD,,,,/
16,159,20020452,S,000356709,UNDARM/
88,ADENDA={8300}OIPROPKVANTUM SPORT
88, DOO*RS31570003003814102076************BULEVAR MILUTINA MIIANLOVCIA 11A
88, BEOGRAD-NOBI BOEGRAD SBRIJA*{8350}UNDER ARMOUR EUROPE
88, BV*OI*PROP*1000014017631***********OLYMPICSH STADION 8 AMSTERDEM, 08 1067 DE
88, HOLANDIJA*{8450}USD200504.24*{8750}/INV/RSHK180011B+12+13+14,CN1600000*/
49,20050424,8/
98,245974878,2,21/
99,245974878,1,23/";

		private System.Xml.XmlDocument xml = null;
		public DITBAI2ImportTests()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		[TestInitialize]
		public void TestInitialize()
		{
			ms = new MemoryStream(Encoding.ASCII.GetBytes(wireData));
			reader = new StreamReader(ms);
			importer = new DITBAI2Import();
			xml = new XmlDocument();
			importer.StreamToXML(reader, enmFileType.Batch,
				new cConfigData_DebitSwitch(-1, 0)
				{
					BankName = "",
					DataRetentionDays = 0,
					ImageRetentionDays = 0,
					SkipDebit = true
				}, out xml);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			ms.Close();
			reader.Close();
		}

		[TestMethod]
		public void XMLRemittanceFreeTextTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Document/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "RemittanceFreeText")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					if (idx == 0)
					{
						Assert.AreEqual(result, "ATTN: LISA SHECRER DEER PARK NY*");
						idx++;
					}
					else
					{
						Assert.AreEqual(result, "INVRSHK180011B+12+13+14,CN1600000*");
						idx++;
					}
				}
			}
			Assert.AreEqual(idx, 2);
		}

		[TestMethod]
		public void XMLBAIFileCreationDateTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "BAIFileCreationDate")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					Assert.AreEqual(result, "09/28/2017");
					idx++;
				}
			}
			Assert.AreEqual(idx, 3);
		}

		[TestMethod]
		public void XMLBAIFileIDTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "BAIFileID")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					Assert.AreEqual(result, "1505");
					idx++;
				}
			}
			Assert.AreEqual(idx, 3);
		}
		[TestMethod]
		public void XMLRemittanceOriginatorTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Document/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			Dictionary<string, string> d = new Dictionary<string, string>();
			d.Add("GREEN POWER SOLUTIONS, LLC", "");
			d.Add("IRNSTENGEL INVESTMENTS, INC", "");
			d.Add("KVANTUM SPORT DOO", "");
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "RemittanceOriginator")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					if (d.ContainsKey(result))
					{
						idx++;
					}
				}
			}
			Assert.AreEqual(idx, 3);
		}

		[TestMethod]
		public void XMLCustomerAccountNumberTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "CustomerAccountNumber")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					Assert.AreEqual(result, "112233445");
					idx++;
				}
			}
			Assert.AreEqual(idx, 3);
		}

		[TestMethod]
		public void XMLTypeCodeTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			Dictionary<string, string> d = new Dictionary<string, string>();
			d.Add("1", "195");
			d.Add("2", "195");
			d.Add("3", "159");
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "TypeCode")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					if (d.ContainsValue(result))
					{
						idx++;
					}
				}
			}

			Assert.AreEqual(idx, 3);
		}

		[TestMethod]
		public void XMLBIABankReferenceTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			Dictionary<string, string> d = new Dictionary<string, string>();
			d.Add("1", "000326352");
			d.Add("2", "000324562");
			d.Add("3", "000356709");
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "BAIBankReference")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					if (d.ContainsValue(result))
					{
						idx++;
					}
				}
			}

			Assert.AreEqual(idx, 3);
		}

		[TestMethod]
		public void XMLBIACustomerReferenceTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			Dictionary<string, string> d = new Dictionary<string, string>();
			d.Add("1", "ROCTENCOM");
			d.Add("2", "ROCTENCOM");
			d.Add("3", "UNDARM");
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "BAICustomerReference")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					if (d.ContainsValue(result))
					{
						idx++;
					}
				}
			}

			Assert.AreEqual(idx, 3);
		}
		[TestMethod]
		public void XMLRemittanceBeneficiaryTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Document/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			Dictionary<string, string> d = new Dictionary<string, string>();
			d.Add("WESTROCK RKT", "");
			d.Add("WESTROCK", "");
			d.Add("UNDER ARMOUR EUROPE BV", "");
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "RemittanceBeneficiary")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					if (d.ContainsKey(result))
					{
						idx++;
					}
				}
			}
			Assert.AreEqual(idx, 3);
		}


		[TestMethod]
		public void XMActualAmountPaidTest()
		{
			XmlNodeList userNodes = xml.SelectNodes("//Batches/Batch/Transaction/Document/RemittanceDataRecord/RemittanceData");
			int idx = 0;
			Dictionary<string, string> d = new Dictionary<string, string>();
			d.Add("2259026.44", "");
			d.Add("7500", "");
			d.Add("200504.24", "");
			foreach (XmlNode userNode in userNodes)
			{
				var s = userNode.Attributes["FieldName"].Value;
				if (s == "ActualAmountPaid")
				{
					var result = userNode.Attributes["FieldValue"].Value;
					if (d.ContainsKey(result))
					{
						idx++;
					}
				}
			}
			Assert.AreEqual(idx, 3);
		}
	}
}
