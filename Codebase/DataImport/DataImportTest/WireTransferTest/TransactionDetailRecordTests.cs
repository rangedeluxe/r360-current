﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WireTransferTest
{
    /// <summary>
    /// Tests for the behavior of the Transaction Detail (16) records, and their addenda (88), in the input file.
    /// </summary>
    [TestClass]
    public class TransactionDetailRecordTests : WireXClientImportTestCase
    {
        private const string TypeCodeIncomingMoneyTransfer = "195";
        private const string TypeCodeOutgoingMoneyTransfer = "495";

        private static IEnumerable<string> FileWithAccountDetails(IReadOnlyCollection<string> accountDetails)
        {
            yield return "01,623929681,072000096,141020,1503,0116,80,,2/";
            yield return "02,072000096,121137522,1,031020,1503,,3/";
            yield return "03,62301,,,,,,,,,/";

            foreach (var accountDetailLine in accountDetails)
                yield return accountDetailLine;

            // The BAI format does not require an account to list all transactions that contribute
            // to the account's totals, and therefore doesn't require the account totals to match
            // the sum of the Transaction Detail (16) records. So our tests can get away with
            // hard-coded totals for the Account Trailer, Group Trailer, and File Trailer records,
            // as long as those totals are all consistent with each other.
            yield return $"49,+00000000002000,{accountDetails.Count + 2}/";
            yield return $"98,2000,000001,{accountDetails.Count + 4}/";
            yield return $"99,2000,000001,{accountDetails.Count + 6}/";
        }
        private void ImportAccountDetails(params string[] accountDetails)
        {
            ImportToCanonicalXml(FileWithAccountDetails(accountDetails));
        }

        [TestMethod]
        public void SingleTransactionDetailRecord_OutputsSingleTransactionElement()
        {
            ImportAccountDetails("16,495,000000000002183,0,623,001201/");
            Assert.AreEqual(1, TransactionElements.Count());
        }
        [TestMethod]
        public void TwoTransactionDetailRecords_OutputsTwoTransactionElements()
        {
            ImportAccountDetails(
                "16,495,000000000002183,0,623,001201/",
                "16,495,000000000002183,0,623,001201/"
            );
            Assert.AreEqual(2, TransactionElements.Count());
        }
        [TestMethod]
        public void SingleTransactionDetailRecord_OutputsSinglePaymentElement()
        {
            ImportAccountDetails("16,495,000000000002183,0,623,001201/");
            Assert.AreEqual(1, PaymentElements.Count());
        }
        [TestMethod]
        public void TwoTransactionDetailRecords_OutputsTwoPaymentElements()
        {
            ImportAccountDetails(
                "16,495,000000000002183,0,623,001201/",
                "16,495,000000000002183,0,623,001201/"
            );
            Assert.AreEqual(2, PaymentElements.Count());
        }
        [TestMethod]
        public void ForIncomingMoneyTransfer_PaymentElement_HasPositivePaymentAmount()
        {
            ImportAccountDetails($"16,{TypeCodeIncomingMoneyTransfer},000000000002183,0,623,001201/");
            Assert.AreEqual("21.83", PaymentElement.Attribute("Amount")?.Value);
        }
        [TestMethod]
        public void ForOutgoingMoneyTransfer_PaymentElement_HasNegativePaymentAmount()
        {
            ImportAccountDetails($"16,{TypeCodeOutgoingMoneyTransfer},000000000002183,0,623,001201/");
            Assert.AreEqual("-21.83", PaymentElement.Attribute("Amount")?.Value);
        }
        [TestMethod]
        public void BankReferenceNumber_ImportedIntoRemittanceData()
        {
            const string bankReferenceNumber = "12345";
            ImportAccountDetails($"16,495,000000000002183,0,{bankReferenceNumber},001201/");
            Assert.AreEqual(bankReferenceNumber, GetRemittanceDataValue("BAIBankReference"));
        }
        [TestMethod]
        public void CustomerReferenceNumber_ImportedIntoRemittanceData()
        {
            const string customerReferenceNumber = "12345";
            ImportAccountDetails($"16,495,000000000002183,0,623,{customerReferenceNumber}/");
            Assert.AreEqual(customerReferenceNumber, GetRemittanceDataValue("BAICustomerReference"));
        }
        [TestMethod]
        public void RawData_ImportedIntoRawDataElements()
        {
            ImportAccountDetails(
                "16,495,000000000002183,0,623,001201/",
                "88,Addendum 1",
                "88,Addendum 2");
            Assert.AreEqual("Addendum 1|Addendum 2", string.Join("|", RawDataValues));
        }
        [TestMethod]
        public void RawData_CanContainCommas()
        {
            ImportAccountDetails(
                "16,495,000000000002183,0,623,001201/",
                "88,Addendum 1,000",
                "88,Addendum 2,000");
            Assert.AreEqual("Addendum 1,000|Addendum 2,000", string.Join("|", RawDataValues));
        }
    }
}