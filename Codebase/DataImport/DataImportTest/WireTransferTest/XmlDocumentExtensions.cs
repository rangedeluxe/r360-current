using System.Xml;
using System.Xml.Linq;

namespace WireTransferTest
{
    public static class XmlDocumentExtensions
    {
        public static XDocument ToXDocument(this XmlDocument xmlDocument)
        {
            using (var nodeReader = new XmlNodeReader(xmlDocument))
            {
                return XDocument.Load(nodeReader);
            }
        }
    }
}