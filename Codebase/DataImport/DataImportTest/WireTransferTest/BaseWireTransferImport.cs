﻿using System.IO;
using System.Xml;
using DMP.BAI2.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportXClientBase;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 12/04/2014
*
* Purpose:
*
* Modification History
* WI 180105 SAS 12/04/2014
*   -Initial Version
******************************************************************************/

namespace WireTransferTest
{
    [TestClass]
    public abstract class BaseWireTransferImport
    {
        /// <summary>
        /// Runs the StreamToXML for Wire and returns the resulting canonical XML.
        /// </summary>
        /// <param name="inputFilePath">File path of the input XML.</param>
        /// <param name="skipDebit">Ignore debit on wire imports.</param>
        /// <returns>The generated canonical XML.</returns>
        protected static XmlDocument FileToCanonicalXml(string inputFilePath, bool skipDebit = false)
        {
            using (var stmInputFile = new StreamReader(inputFilePath))
            {
                var cConfigData = new cConfigData_DebitSwitch(1, 1) {SkipDebit = skipDebit};
                var wireImport = new DITBAI2Import();

                XmlDocument xmlOutput;
                wireImport.StreamToXML(stmInputFile, enmFileType.Batch, cConfigData, out xmlOutput);
                return xmlOutput;
            }
        }
    }
}
