﻿using DMP.BAI2.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace WireTransferTest
{
    [TestClass]
    public class DitBai2FileObjectTests
    {
        [TestMethod]
        public void FileControlTotal_EmptyString()
        {
            var obj = new DITBAI2FileObject(null) {FileFooter = "99,,1,9/"};
            Assert.AreEqual("0", obj.FileControlTotal);
        }
        [TestMethod]
        public void FileControlTotal_NonNumeric()
        {
            var obj = new DITBAI2FileObject(null) {FileFooter = "99,abc,1,9/"};
            Assert.AreEqual("0", obj.FileControlTotal);
        }
        [TestMethod]
        public void FileControlTotal_DigitsOnly()
        {
            var obj = new DITBAI2FileObject(null) {FileFooter = "99,1000,1,9/"};
            Assert.AreEqual("1000", obj.FileControlTotal);
        }
        [TestMethod]
        public void FileControlTotal_LeadingPlus()
        {
            var obj = new DITBAI2FileObject(null) {FileFooter = "99,+1000,1,9/"};
            Assert.AreEqual("+1000", obj.FileControlTotal);
        }
        [TestMethod]
        public void FileControlTotal_LeadingMinus()
        {
            var obj = new DITBAI2FileObject(null) {FileFooter = "99,-1000,1,9/"};
            Assert.AreEqual("-1000", obj.FileControlTotal);
        }

        [TestMethod]
        public void FileControlTotal_LargeAmount()
        {
            var obj = new DITBAI2FileObject(null) { FileFooter = "99,+2147483647,9,32/" };
            Assert.AreEqual("+2147483647", obj.FileControlTotal);
        }

        [TestMethod]
        public void FileControlTotal_TotalAmount_exceed_int32()
        {
            Mock<IWireLogger> logMock = new Mock<IWireLogger>();
            
            var obj = new DITBAI2FileObject(logMock.Object) { FileFooter = "99,+2147483648,0,32/" };
            obj.TotalRecords = 32;
            var obj2 = obj.DoValidate();
            Assert.IsFalse(obj2);
        }
    }
}