﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using DMP.BAI2.Components;
using DMP.BAI2.Components.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
// ReSharper disable RedundantExplicitParamsArrayCreation

namespace WireTransferTest
{
    [TestClass]
    public class DitBai2TransactionTests
    {
        private void AddAddendaRecord(DITBAI2Transaction transaction, string rawContinuation)
        {
            transaction.Continuations.Add(new DITBAI2Continuation {RawContinuationData = rawContinuation});
        }
        private void AssertNoPaymentDataEntryFields(XElement paymentNode)
        {
            var emptyArray = new KeyValuePair<string, string>[] { };
            AssertPaymentDataEntryFields(paymentNode, emptyArray);
        }
        private void AssertPaymentDataEntryFields(XElement paymentNode,
            params KeyValuePair<string, string>[] expectedPaymentDataEntryFields)
        {
            var remittanceDataNodes = paymentNode.DescendantsAndSelf("RemittanceData").ToList();
            Assert.AreEqual(expectedPaymentDataEntryFields.Length, remittanceDataNodes.Count, "Count");
            for (var i = 0; i < expectedPaymentDataEntryFields.Length; ++i)
            {
                var expectedKeyValuePair = expectedPaymentDataEntryFields[i];
                var actualElement = remittanceDataNodes[i];
                Assert.AreEqual(expectedKeyValuePair.Key, actualElement.Attribute("FieldName")?.Value,
                    $"FieldName {i}");
                Assert.AreEqual(expectedKeyValuePair.Value, actualElement.Attribute("FieldValue")?.Value,
                    $"FieldValue {i}");
            }
        }
        private XElement BuildPaymentNode(DITBAI2Transaction transaction)
        {
            var xmlDocument = new XmlDocument();
            return ToXElement(transaction.BuildPaymentNode(xmlDocument));
        }
        private KeyValuePair<string, string> CreateKeyValuePair(string key, string value)
        {
            return new KeyValuePair<string, string>(key, value);
        }
        private DITBAI2Transaction CreateTransaction()
        {
            var logger = new StubIWireLogger();
            return new DITBAI2Transaction(logger);
        }
        private DITBAI2Transaction CreateTransactionWithAddenda(params string[] rawAddendaRecords)
        {
            var transaction = CreateTransaction();
            foreach (var rawAddendaRecord in rawAddendaRecords)
            {
                StringAssert.StartsWith(rawAddendaRecord, "88,", "Raw addenda record should start with '88'");
                AddAddendaRecord(transaction, rawAddendaRecord);
            }
            return transaction;
        }
        private XElement ToXElement(XmlNode xmlDocument)
        {
            using (var reader = new XmlNodeReader(xmlDocument))
                return XElement.Load(reader);
        }

        [TestMethod]
        public void NoAddenda_ResultsInNoPaymentDataEntryFields()
        {
            var transaction = CreateTransaction();

            var paymentNode = BuildPaymentNode(transaction);

            AssertNoPaymentDataEntryFields(paymentNode);
        }
        [TestMethod]
        public void OneContinuation_WithSendingBank_CreatesAPaymentDataEntryFieldForSendingBank()
        {
            var transaction = CreateTransactionWithAddenda(
                "88,Sending Bank: Bank of Bob");

            var paymentNode = BuildPaymentNode(transaction);

            AssertPaymentDataEntryFields(paymentNode,
                CreateKeyValuePair("SendingBank", "Bank of Bob"));
        }
    }
}