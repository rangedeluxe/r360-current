﻿using System.Linq;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 12/04/2014
*
* Purpose:
*
* Modification History
* WI 180105 SAS 12/15/2014
*   -Initial Version
******************************************************************************/

namespace WireTransferTest
{
    [TestClass]
    public class ValidWireTransferFile : BaseWireTransferImport
    {
        private const string FilePathWithOnlyDebits = "Resources\\Wire_Without_Addenda_Records.txt";

        [TestMethod]
        public void CanonicalXml_ContainsDocumentElement()
        {
            var xmlDocument = FileToCanonicalXml(FilePathWithOnlyDebits);
            StringAssert.Contains(xmlDocument.InnerXml, "Document");
        }

        [TestMethod]
        public void FileWithOnlyDebits_WhenSkipDebits_ReturnsTrue_ButReturnsNullXmlDocument()
        {
            var xmlDocument = FileToCanonicalXml(FilePathWithOnlyDebits, skipDebit: true);
            Assert.IsNull(xmlDocument);
        }

        [TestMethod]
        public void CanonicalXml_ContainsExpectedRemittanceXml()
        {
            var xmlDocument = FileToCanonicalXml(FilePathWithOnlyDebits);
            StringAssert.Contains(xmlDocument.InnerXml,
                "<RemittanceDataRecord BatchSequence=\"1\">" +
                "<RemittanceData FieldName=\"BAIFileCreationDate\" FieldValue=\"10/20/2014\" />" +
                "<RemittanceData FieldName=\"BAIFileID\" FieldValue=\"0116\" />" +
                "<RemittanceData FieldName=\"CustomerAccountNumber\" FieldValue=\"62301\" />" +
                "<RemittanceData FieldName=\"TypeCode\" FieldValue=\"495\" />" +
                "<RemittanceData FieldName=\"BAIBankReference\" FieldValue=\"623\" />" +
                "<RemittanceData FieldName=\"BAICustomerReference\" FieldValue=\"001201\" />" +
                "</RemittanceDataRecord>");
        }

        [TestMethod]
        public void CanonicalXml_BatchIdsAreZero()
        {
            var xmlDocument = FileToCanonicalXml("Resources\\Wire_Valid_Records.txt");
            var batchElements = xmlDocument.ToXDocument()
                .Elements("Batches")
                .Elements("Batch")
                .ToList();
            Assert.AreNotEqual(0, batchElements.Count, "Count of <Batch> elements");

            foreach (var batchElement in batchElements)
            {
                var batchIdString = batchElement.Attribute("BatchID")?.Value ?? "";
                Assert.AreEqual("0", batchIdString);
            }
        }
    }
}
