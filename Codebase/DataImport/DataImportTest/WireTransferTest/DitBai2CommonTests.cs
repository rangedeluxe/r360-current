﻿using DMP.BAI2.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WireTransferTest
{
    [TestClass]
    public class DitBai2CommonTests
    {
        [TestMethod]
        public void GetParsedData_GettingFirstValue()
        {
            var result = DITBAI2Common.GetParsedData("88,xyz", oneBasedIndex: 1);
            Assert.AreEqual("88", result);
        }
        [TestMethod]
        public void GetParsedData_GettingSecondValue()
        {
            var result = DITBAI2Common.GetParsedData("88,xyz", oneBasedIndex: 2);
            Assert.AreEqual("xyz", result);
        }
        [TestMethod]
        public void GetParsedData_GettingOutOfRangeValue()
        {
            var result = DITBAI2Common.GetParsedData("88,xyz", oneBasedIndex: 3);
            Assert.AreEqual("", result);
        }
        [TestMethod]
        public void GetParsedData_TrimsTrailingSlash()
        {
            var result = DITBAI2Common.GetParsedData("88,xyz/", oneBasedIndex: 2);
            Assert.AreEqual("xyz", result);
        }

	    [TestMethod]
	    public void GetParsedData_RemittanceFreeText()
	    {
		    string sValue =
			    "ADDENDA=OIPROPIRNSTENGEL INVESTMENTS, INC*000009532153222************608 PLYMOUTH RD MINNETONKA           MN 55503-1074*WESTROCK*OI*PROP*8800372087************USD7500*{8750}ATTN: LISA SHECRER DEER PARK NY*";
		    int iStartindex = 167;
		    string sStripTag = "{8750}";
		    string delimiter = "";
		    int iStripOffPos = 0;
		    var result = DITBAI2Common.GetParsedAddendaData(ref sValue, iStartindex,  sStripTag, delimiter, iStripOffPos);

		    Assert.AreEqual("ATTN: LISA SHECRER DEER PARK NY*", result);
	    }
    }
}