﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.DataImport.DataImportXClientBase
{
    public class cConfigData_DebitSwitch : cConfigData
    {
        public cConfigData_DebitSwitch(int iDefaultBankID, int iDefaultLockboxID) : base( iDefaultBankID, iDefaultLockboxID){}
        public bool SkipDebit { get; set; }
    }
}
