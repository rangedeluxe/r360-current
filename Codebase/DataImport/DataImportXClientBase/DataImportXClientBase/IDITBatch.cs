﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     08/02/2012
*
* Purpose:  
*
* Modification History
* Created
* CR 55551 CEJ 08/02/2012
*   -Initial Version
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportXClientBase
{
    public interface IDITBatch
    {
        bool BatchStreamToXML(StreamReader stmInputFile, cConfigData cfgSettings, out XmlDocument xmdResult);
    }
}
