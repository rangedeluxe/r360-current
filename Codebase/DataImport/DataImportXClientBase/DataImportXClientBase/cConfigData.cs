﻿using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:     08/23/2012
*
* Purpose:  
*
* Modification History
* Created
*  CR 55551: WJS 8/23/2012
*   - DIT Client Base
*
* WI 129745 CMC 02/14/2014 
*   - Adding additional config attributes 
*   (BankName, DataRetentionDays, and ImageRetentionDays)
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportXClientBase
{
    public class cConfigData
    {
        public cConfigData(int iDefaultBankID, int iDefaultLockboxID)
        {
            DefaultBankID = iDefaultBankID;
            DefaultLockboxID = iDefaultLockboxID;
        }

        public int DefaultBankID
            { get; set; }

        public int DefaultLockboxID
            { get; set; }

        public int DataRetentionDays { get; set; }

        public int ImageRetentionDays { get; set; }

        public string BankName { get; set; }
    }
}
