﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     08/02/2012
*
* Purpose:  This class provides the basis for a dll that translates a client input file to xml for the Data Import Client Service
*
* Modification History
* CR 55551 CEJ 08/02/2012
*   -Initial Version
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportXClientBase
{

    public  class cDataImportXClientBase
    {
        public event dlgLogEventHandler LogEvent;

        public cDataImportXClientBase()
        {
        }

        public virtual bool StreamToXML(StreamReader stmInputFile, enmFileType fltFileType, cConfigData cfgSettings,
                out XmlDocument xmdResult)
        {
            bool bAns = false;
            xmdResult = null;

            switch (fltFileType)
            {
                case enmFileType.Batch:
                    bAns = BatchStreamToXML(stmInputFile, cfgSettings, out xmdResult);
                    break;

                case enmFileType.ClientSetup:
                    bAns = ClientStreamToXML(stmInputFile, cfgSettings, out xmdResult);
                    break;

            }
            return bAns;
        }

        public virtual bool ClientStreamToXML(StreamReader stmInputFile, cConfigData cfgSettings, out XmlDocument xmdResult)
        {
            throw new Exception("This DLL does not support the ClientStreamToXML method");
        }

        public virtual bool BatchStreamToXML(StreamReader stmInputFile, cConfigData cfgSettings, out XmlDocument xmdResult)
        {
            throw new Exception("This DLL does not support the BatchStreamToXML method");
        }


       public void DoLogEvent(string sMsg, string sSrc = "", LogEventType mstType = LogEventType.Information,
                LogEventImportance msiImportance = LogEventImportance.Essential)
        {
            if(LogEvent!=null)
                LogEvent(this, sMsg, sSrc, mstType, msiImportance);
        }
    }
}

