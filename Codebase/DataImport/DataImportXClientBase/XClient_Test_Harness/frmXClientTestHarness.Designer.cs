﻿namespace XClient_Test_Harness {
    partial class frmXClientTestHarness {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmXClientTestHarness));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblDisplay = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuConfirmSchema = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCopyXML = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.lstClientDLLs = new System.Windows.Forms.ListBox();
            this.txtError = new System.Windows.Forms.TextBox();
            this.tmrCheckIt = new System.Windows.Forms.Timer(this.components);
            this.transformXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AllowDrop = true;
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.AutoScrollMargin = new System.Drawing.Size(30, 30);
            this.splitContainer1.Panel1.AutoScrollMinSize = new System.Drawing.Size(100, 100);
            this.splitContainer1.Panel1.Controls.Add(this.lblDisplay);
            this.splitContainer1.Panel1.Controls.Add(this.lbl);
            this.splitContainer1.Panel1.Controls.Add(this.menuStrip1);
            this.splitContainer1.Panel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.lblDisplay_DragDrop);
            this.splitContainer1.Panel1.DragEnter += new System.Windows.Forms.DragEventHandler(this.splitContainer1_Panel1_DragEnter);
            this.splitContainer1.Panel1.DragOver += new System.Windows.Forms.DragEventHandler(this.splitContainer1_Panel1_DragEnter);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(806, 493);
            this.splitContainer1.SplitterDistance = 513;
            this.splitContainer1.TabIndex = 0;
            // 
            // lblDisplay
            // 
            this.lblDisplay.AutoSize = true;
            this.lblDisplay.Location = new System.Drawing.Point(0, 25);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(0, 13);
            this.lblDisplay.TabIndex = 1;
            this.lblDisplay.DragDrop += new System.Windows.Forms.DragEventHandler(this.lblDisplay_DragDrop);
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(4, 4);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(0, 13);
            this.lbl.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuConfirmSchema,
            this.mnuCopyXML,
            this.transformXMLToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(511, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuConfirmSchema
            // 
            this.mnuConfirmSchema.Name = "mnuConfirmSchema";
            this.mnuConfirmSchema.Size = new System.Drawing.Size(108, 20);
            this.mnuConfirmSchema.Text = "Confirm Schema";
            this.mnuConfirmSchema.Click += new System.EventHandler(this.mnuConfirmSchema_Click);
            // 
            // mnuCopyXML
            // 
            this.mnuCopyXML.Name = "mnuCopyXML";
            this.mnuCopyXML.Size = new System.Drawing.Size(74, 20);
            this.mnuCopyXML.Text = "Copy XML";
            this.mnuCopyXML.Click += new System.EventHandler(this.mnuCopyXML_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Panel1.Controls.Add(this.lstClientDLLs);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.txtError);
            this.splitContainer2.Size = new System.Drawing.Size(289, 493);
            this.splitContainer2.SplitterDistance = 234;
            this.splitContainer2.TabIndex = 0;
            // 
            // lstClientDLLs
            // 
            this.lstClientDLLs.AllowDrop = true;
            this.lstClientDLLs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstClientDLLs.FormattingEnabled = true;
            this.lstClientDLLs.IntegralHeight = false;
            this.lstClientDLLs.Location = new System.Drawing.Point(0, 0);
            this.lstClientDLLs.Name = "lstClientDLLs";
            this.lstClientDLLs.Size = new System.Drawing.Size(287, 232);
            this.lstClientDLLs.TabIndex = 0;
            this.lstClientDLLs.DragDrop += new System.Windows.Forms.DragEventHandler(this.lstClientDLLs_DragDrop);
            this.lstClientDLLs.DragEnter += new System.Windows.Forms.DragEventHandler(this.lstClientDLLs_DragEnter);
            // 
            // txtError
            // 
            this.txtError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtError.Location = new System.Drawing.Point(0, 0);
            this.txtError.Multiline = true;
            this.txtError.Name = "txtError";
            this.txtError.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtError.Size = new System.Drawing.Size(287, 253);
            this.txtError.TabIndex = 0;
            // 
            // tmrCheckIt
            // 
            this.tmrCheckIt.Enabled = true;
            this.tmrCheckIt.Interval = 1000;
            this.tmrCheckIt.Tick += new System.EventHandler(this.tmrCheckIt_Tick);
            // 
            // transformXMLToolStripMenuItem
            // 
            this.transformXMLToolStripMenuItem.Name = "transformXMLToolStripMenuItem";
            this.transformXMLToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.transformXMLToolStripMenuItem.Text = "Transform XML";
            this.transformXMLToolStripMenuItem.Click += new System.EventHandler(this.transformXMLToolStripMenuItem_Click);
            // 
            // frmXClientTestHarness
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 493);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmXClientTestHarness";
            this.Text = "XClient Test Harness";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.ListBox lstClientDLLs;
        private System.Windows.Forms.TextBox txtError;
        private System.Windows.Forms.Timer tmrCheckIt;
        private System.Windows.Forms.Label lblDisplay;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuConfirmSchema;
        private System.Windows.Forms.ToolStripMenuItem mnuCopyXML;
        private System.Windows.Forms.ToolStripMenuItem transformXMLToolStripMenuItem;
    }
}

