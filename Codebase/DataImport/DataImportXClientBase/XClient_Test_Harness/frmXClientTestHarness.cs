﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

using WFS.LTA.DataImport.DataImportXClientBase;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Charlie Johnson
* Date: 02/10/2014
*
* Purpose: 
*
* Modification History
* WI 129143 CEJ 02/10/2014
*   Create a XClient test stub application
******************************************************************************/

namespace XClient_Test_Harness {
    public partial class frmXClientTestHarness : Form {
        public Dictionary<int, cDataImportXClientBase> _CrossLink {
            get;
            set;
        }

        public frmXClientTestHarness() {
            InitializeComponent();
            _CrossLink = new Dictionary<int, cDataImportXClientBase>();
        }

        private string SchemaFile {
            get {
                return Properties.Settings.Default.SchemaFile;
            }
        }

        private void lstClientDLLs_DragEnter(object sender, DragEventArgs e) {
            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) ? DragDropEffects.Link : DragDropEffects.None;
        }

        private void lstClientDLLs_DragDrop(object sender, DragEventArgs e) {
            try {
                if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
                    foreach (string filename in (string[])(e.Data.GetData(DataFormats.FileDrop))) {
                        cDataImportXClientBase dixCurType = Util.LoadStreamToXMLDLL(filename);
                        int iIndex = lstClientDLLs.Items.Add(string.Format("{0}.{1}", Path.GetFileNameWithoutExtension(filename), dixCurType.GetType().Name));
                        if (_CrossLink.ContainsKey(iIndex)) {
                            _CrossLink[iIndex] = dixCurType;
                        }
                        else {
                            _CrossLink.Add(iIndex, dixCurType);
                        }
                        if (lstClientDLLs.SelectedIndex < 0) {
                            if (lstClientDLLs.Items.Count > 0) {
                                lstClientDLLs.SelectedIndex = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void splitContainer1_Panel1_DragEnter(object sender, DragEventArgs e) {
            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) ? DragDropEffects.Link : DragDropEffects.None;
        }

        private void lblDisplay_DragDrop(object sender, DragEventArgs e) {
            cDataImportXClientBase dixCurTool = null;

            try {
                if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                    if (lstClientDLLs.SelectedIndex > -1) {
                        dixCurTool = _CrossLink[lstClientDLLs.SelectedIndex];
                        XmlDocument xmdDoc;
                        StreamReader stmInput = new StreamReader(files.First());
                        stmInput.BaseStream.Seek(0, SeekOrigin.Begin);
                        if (dixCurTool.StreamToXML(stmInput, enmFileType.Batch, null, out xmdDoc)) {
                            lblDisplay.Text = FormattedXML(xmdDoc);
                        }
                        else {
                            lblDisplay.Text = "Error";
                        }
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private string FormattedXML(XmlDocument xmdValue) {
            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            string sResults = string.Empty;

            writer.Formatting = Formatting.Indented;
            xmdValue.WriteContentTo(writer);
            writer.Flush();
            mStream.Flush();
            mStream.Seek(0, SeekOrigin.Begin);
            sResults = new StreamReader(mStream).ReadToEnd();
            mStream.Close();
            writer.Close();

            return sResults;
        }

        private void tmrCheckIt_Tick(object sender, EventArgs e) {
            while(Util._ErrorMessages.Count > 0) {
                txtError.Text = string.Join("\n", new string[] { txtError.Text, Util._ErrorMessages.Pop()});
            }
        }

        private void mnuConfirmSchema_Click(object sender, EventArgs e) {
            try {
                string message = string.Format("{0}: {1}",
                        XML_XSD_Validator.Validate(lblDisplay.Text, GetSchema(SchemaFile)) ? "Success" : "Fail", 
                        XML_XSD_Validator.GetError()); 
                MessageBox.Show(message);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private string GetSchema(string XSDSchemaFile) {
            string Contents=string.Empty;

            if (File.Exists(XSDSchemaFile)) {
                StreamReader Reader = new StreamReader(SchemaFile);
                Contents = Reader.ReadToEnd();
                Reader.Close();
                Reader.Dispose();
            }
            else {
                throw new Exception(string.Format("Schema File {0} could not be found", XSDSchemaFile));
            }
            return Contents;
        }

        private void mnuCopyXML_Click(object sender, EventArgs e)
        {
            Clipboard.Clear();
            Clipboard.SetText(lblDisplay.Text);
        }

        private void transformXMLToolStripMenuItem_Click(object sender, EventArgs e) {
            lblDisplay.Text = Util.TransformXML(Properties.Resources.Batch_XSL, lblDisplay.Text);
        }
    }
}
