﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;

using WFS.LTA.DataImport.DataImportXClientBase;

namespace XClient_Test_Harness {
    public class Util {
        public static Stack<string> _ErrorMessages = new Stack<string>();

        public static cDataImportXClientBase LoadStreamToXMLDLL(string sFilename)
        {
            cDataImportXClientBase stxAns = null;
            
            try
            {
                Assembly asmDLLsAssembly = Assembly.LoadFile(sFilename);

                foreach (Type typCurType in asmDLLsAssembly.GetTypes())
                {
                    if (typCurType.IsSubclassOf(typeof(cDataImportXClientBase)))
                    {
                        stxAns = (cDataImportXClientBase)typCurType.GetConstructor(new Type[] { }).Invoke(null);
                        stxAns.LogEvent += stxAns_LogEvent;
                    }
                }
                if (stxAns == null)
                {
                    throw new Exception(string.Format(
                            "DLL File ({0}) does not contain a class that properly inherit from cDataImportXClientBase",
                            sFilename));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("LoadStreamToXMLDLL failed to load ({0}), Error: {1}",
                        sFilename, ex.Message), ex);
            }
            return stxAns;
         }

        public static void stxAns_LogEvent(object sender, string sMsg, string sSrc, LogEventType mstMsgType, LogEventImportance msiImportance) {
            _ErrorMessages.Push(string.Format("{0}:  [{1}] - {2}", msiImportance, sSrc, sMsg));
        }

        public static string TransformXML(string xslTransformString, string inputXML) {
            string returnString = string.Empty;
            try {
                XmlDocument docXml = new XmlDocument();
                XmlDocument docXsl = new XmlDocument();
                XPathNavigator navXsl = docXsl.CreateNavigator();
                docXsl.LoadXml(xslTransformString);
                docXml.LoadXml(inputXML);
                XslCompiledTransform transXsl = new XslCompiledTransform();
                transXsl.Load(navXsl, null, null);
                XPathNavigator navXml = docXml.CreateNavigator();

                //CR 51697 Created PathDocument from Document for speedier transformations.
                XPathDocument pathDocXml = new XPathDocument(new XmlNodeReader(docXml));
                navXml = pathDocXml.CreateNavigator();
                using (StringWriter sw = new StringWriter()) {
                    transXsl.Transform(pathDocXml, null, sw);
                    returnString = sw.ToString();
                }
            }
            catch (Exception ex) {
                throw (new Exception(string.Format("System Error in TransformXML: {0}", ex.Message), ex));
            }
            return returnString;
        }
    }
}
