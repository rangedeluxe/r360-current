﻿using WFS.LTA.DataImport.DataImportXClientBase;

namespace WFS.integraPAY.Online.DITICONXClient
{
    public interface IIconLogTarget
    {
        void DoLogEvent(string message, string source = "", LogEventType eventType = LogEventType.Information,
            LogEventImportance importance = LogEventImportance.Essential);
    }
}