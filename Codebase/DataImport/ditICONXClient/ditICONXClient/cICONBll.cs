﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using System.Reflection;
using System.ComponentModel;
using WFS.integraPAY.Online.DITICONXClient.Items;
using WFS.integraPAY.Online.ICONCommon;
using WFS.LTA.DataImport.DataImportXClientBase;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportIconClient;

/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2011.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    5/8/2012
*
* Purpose: 
*
* Modification History
* 05/08/2012 CR 31789 WJS
*     - Initial release.
* 03/15/2013  WI 92492 WJS
 *    - Trim spaces from fieldnames
* 05/03/2013 WI 100925 WJS
 *     - Trim spaces from docTypeNames
* 02/14/2014 WI 129753 CMC  
*   - Adding additional config attributes 
*   (BankName, DataRetentionDays, and ImageRetentionDays)
********************************************************************************/

namespace WFS.integraPAY.Online.DITICONXClient
{
    public class cICONBll : IFileDescriptorSource
    {
        private readonly IIconLogTarget _ditXClient;

        private List<IMSDocumentType> _listDocumentTypes = null;
        private List<string> _ImageRPSStandardKeywords = null;
        private cBatchXMLNode _batchXmlNode = null;
        private cClientXMLNode _clientXMLNode = null;

        private Hashtable _imageRPSAliasKeywordHashTable = null;
        private cDimData  _ddaDimData = null;

        public cICONBll(ditICONXClient xClient)
        {
            _ditXClient = xClient;
            _batchXmlNode = new cBatchXMLNode(this, new GuidSource(), xClient);
            _clientXMLNode = new cClientXMLNode(this, xClient);
        }
        public cICONBll(ditICONXClient xClient, cDimData dimData)
            : this(xClient)
        {
            _ddaDimData = dimData;
        }

        public cICONBll(ditICONXClient xClient, cDimData dimData, cConfigData configurationContext)
        {
            _ditXClient = xClient;
            _ddaDimData = dimData;
            _batchXmlNode = new cBatchXMLNode(this, new GuidSource(), xClient);
            _clientXMLNode = new cClientXMLNode(this, xClient, configurationContext);
        }

        public List<IMSDocumentType> IMSDocumentTypes
        {
            get
            {
                if (_listDocumentTypes == null)
                {
                    _listDocumentTypes = new List<IMSDocumentType>();

                    foreach (KeyValuePair<string, string> docType in _ddaDimData.DocumentTypes)
                    {
                        IMSDocumentType imdocType = new IMSDocumentType(docType.Key, docType.Value);
                        _listDocumentTypes.Add(imdocType);
                    }
                }
                return _listDocumentTypes;
            }
        }

        public List<string> ImageRPSStandardKeywords
        {
            get
            {
                if (_ImageRPSStandardKeywords == null)
                {
                    GetImageRPSStandardKeywords(out _ImageRPSStandardKeywords);
                }
                return (_ImageRPSStandardKeywords);
            }
        }
        private void LogDebug(string message, string source = null)
        {
            _ditXClient.DoLogEvent(message, source ?? GetType().Name,
                LogEventType.Information, LogEventImportance.Debug);
        }
        private void LogError(string message)
        {
            _ditXClient.DoLogEvent(message, GetType().Name, LogEventType.Error);
        }
        private void LogInformation(string message, string source)
        {
            _ditXClient.DoLogEvent(message, source);
        }
        private void LogWarning(string message)
        {
            _ditXClient.DoLogEvent(message, GetType().Name, LogEventType.Warning, LogEventImportance.Verbose);
        }
        /// <summary>
        /// Int Image RPS Standard Keywords
        /// </summary>
        private void GetImageRPSStandardKeywords(out List<string> ImageRPSStandardKeywords)
        {
            List<string> arImageRPSStandardKeywords = new List<string>();

            try
            {
                // CR 29756 MEH 05-21-2010
                //listImageRPSStandardKeywords.Add("Applied Amount");
                ////arImageRPSStandardKeywords.Add("ACH Returns Audit Trail");
                arImageRPSStandardKeywords.Add(cMagicKeywords.BatchID);
                arImageRPSStandardKeywords.Add(cMagicKeywords.ClientID);
                arImageRPSStandardKeywords.Add(cMagicKeywords.TransactionNumber);
                arImageRPSStandardKeywords.Add(cMagicKeywords.SequenceNumber);
                arImageRPSStandardKeywords.Add(cMagicKeywords.ProcessDate);
                arImageRPSStandardKeywords.Add(cMagicKeywords.SystemDate);
                arImageRPSStandardKeywords.Add(cMagicKeywords.DepositDate);
                arImageRPSStandardKeywords.Sort();
            }
            catch (Exception ex)
            {
                LogError($"Exception in InitImageRPSStandardKeywords : {ex.Message}");
                arImageRPSStandardKeywords = null;
            }

            ImageRPSStandardKeywords = arImageRPSStandardKeywords;
        }
        public string GetFileDescriptor(string sDocTypeName)
        {
            string sRet = string.Empty;
            foreach (IMSDocumentType myDocType in IMSDocumentTypes)
            {
                if (sDocTypeName == myDocType.DocumentType)
                {
                    sRet = myDocType.FileDescriptor;
                    break;
                }
            }

            if (sRet.Length == 0)
                sRet = "IN";
            return sRet;
        }

        /// <summary>
        /// use to get enum description
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string GetDescription(SetupFieldType value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute),
                false);

            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }

        public bool ProcessClientFile(TextReader reader, cClientInfo cinClientInfo, cICONConfigData cfgSettings, string strClientProcessCode, out XmlDocument xmdResult)
        {
            bool bRetVal = false;
            XmlElement topLevelElement, clientGroupElement, bankElement;
            XmlElement dataEntryElement = null;
            XmlElement clientElement = null;
            XmlElement imageRPSAliasElement = null;
            xmdResult = new XmlDocument();
            try
            {
                bool bIsCheck = false;
                string skeytype = string.Empty;
                string sdatatype = string.Empty;
                string sDocType = string.Empty;
                string sitemtypenum = string.Empty;
                string sFieldType = string.Empty;
                string sClientFileShortName = string.Empty;
                string skeytypelen = string.Empty;
                const int iDocTypeIdentifier = (int)SetupExtractType.DocTypeIdentifier;
                bool bMarkSense = false;
                bool bImageRPSAliasMapping = false;
                bool bDataEntryColumnsExist = false;

                // output file
                XmlDocument xOutput = new XmlDocument();
                List<SetupField> listSetupFields = new List<SetupField>();
                List<string> imageRPSSetupFields = new List<string>();
                StringBuilder sSearchString = new StringBuilder();
                string sExtractType = string.Empty;

                // create Lockbox/DE XML and send to SSB via a stored procedure
                topLevelElement = _clientXMLNode.BuildClientXML(xmdResult, strClientProcessCode);
                bankElement = _clientXMLNode.BuildBankNode(xmdResult, cfgSettings.DefaultBankID);
                clientElement = _clientXMLNode.BuildClientNode(xmdResult, cinClientInfo);
                int iLockboxId = cinClientInfo.ClientID;

                // input file
                XmlDocument xInput = new XmlDocument();
                xInput.Load(reader);

                // get the lcnum from the LifeCyle node
                XmlNodeList LifeCycleNodeList = xInput.SelectNodes("/Report/LifeCycle");

                string slcnum = string.Empty;

                foreach (XmlNode LifeCycleNode in LifeCycleNodeList)
                {
                    slcnum = LifeCycleNode.Attributes["lcnum"].InnerText;
                    sSearchString.AppendFormat("/Report/LifeCycle/AllItemtypeLCs/ItemTypeLC[@lcnum={0}]", slcnum);
                    XmlNodeList ItemTypeLCList = xInput.SelectNodes(sSearchString.ToString());
                    foreach (XmlNode ItemTypeLCNode in ItemTypeLCList)
                    {
                        //-------------------------------------------------
                        // This portion is for Documents. (NOT DE Columns)
                        //-------------------------------------------------

                        // get the itemtypenum
                        sitemtypenum = ItemTypeLCNode.Attributes["itemtypenum"].InnerText;

                        // get the itemtypename from the AllItemtypes node that match the itemtypenum
                        sSearchString.Length = 0;
                        sSearchString.AppendFormat("/Report/LifeCycle/AllItemtypes/ItemType[@itemtypenum={0}]", sitemtypenum);

                        XmlNodeList ItemTypeList = xInput.SelectNodes(sSearchString.ToString());

                        string sitemtypename = string.Empty;

                        if (ItemTypeList.Count != 1)
                        {
                            throw new Exception("Item Type Number '{0}' is not unique.");
                        }

                        var item = ItemTypeList[0];
                        sitemtypename = item.Attributes["itemtypename"].InnerText;

                        // Now we're going to be searching for our payment source.
                        // Each ID can have multiple sources, and we're going to be creating a new 
                        // entry for each payment source.
                        sSearchString.Length = 0;
                        sSearchString.AppendFormat("/Report/LifeCycle/AllDocGrpCL/DocGrpCL/DocumentCL[@itemtypenum={0}]", sitemtypenum);
                        var documentCLList = xInput.SelectNodes(sSearchString.ToString());
                        var sources = new List<string>();
                        foreach (XmlNode doc in documentCLList)
                        {
                            var src = doc.ParentNode.Attributes["paymentsource"].Value;
                            if (!sources.Contains(src))
                                sources.Add(src);
                        }
                        if (sources.Count == 0)
                        {
                            // Add the default if we have none.
                            sources.Add(cfgSettings.DefaultPaymentSource);
                        }

                        if (item.Attributes["doctype"] != null)
                        {
                            sDocType = item.Attributes["doctype"].InnerText;
                        }

                        if (String.IsNullOrEmpty(sDocType))
                        {
                            //else old logic replace when all setup file go to new way
                            if (sitemtypename.Contains("Per/Bus Check"))
                                bIsCheck = true;
                            else
                                bIsCheck = false;
                        }
                        else if (sDocType == "2")
                        {
                            bIsCheck = true;

                            foreach (var src in sources)
                            {
                                InsertImageRPSAliasMapping(
                                xmdResult,
                                new ClientSetupInfo
                                {
                                    BankId = cfgSettings.DefaultBankID,
                                    WorkGroupId = iLockboxId,
                                    ExtractType = iDocTypeIdentifier.ToString(),
                                    FieldType = string.Empty,
                                    DocType = sDocType,
                                    KeyType = sitemtypename,
                                    DataType = ((int) ImageRpsDataType.String).ToString(),
                                    FieldLength = "0",
                                    BatchSource = src
                                },
                                out imageRPSAliasElement);

                                clientElement.AppendChild(imageRPSAliasElement);
                            }
                        }
                        else if (sDocType == "1")
                        {
                            bIsCheck = false;

                            foreach (var src in sources)
                            {
                                InsertImageRPSAliasMapping(
                                xmdResult,
                                new ClientSetupInfo
                                {
                                    BankId = cfgSettings.DefaultBankID,
                                    WorkGroupId = iLockboxId,
                                    ExtractType = iDocTypeIdentifier.ToString(),
                                    FieldType = string.Empty,
                                    DocType = sDocType,
                                    KeyType = sitemtypename,
                                    DataType = ((int) ImageRpsDataType.String).ToString(),
                                    FieldLength = "0",
                                    BatchSource = src
                                },
                                out imageRPSAliasElement);

                                clientElement.AppendChild(imageRPSAliasElement);
                            }
                        }
                        else
                        {
                            LogError($"Recevied a docType which is not expected type. DocType is: {sDocType}");
                        }

                        //-------------------------------------------------
                        // This portion is for DE Columns.
                        //-------------------------------------------------

                        // get all the ItemtypeexKeyword for this itemtype (keytype, seqnum)
                        sSearchString.Length = 0;
                        sSearchString.AppendFormat("/Report/LifeCycle/AllItemtypexKeywords/ItemtypexKeyword[@itemtype={0}]", sitemtypenum);

                        XmlNodeList ItemtypeexKeywordList = xInput.SelectNodes(sSearchString.ToString());

                        skeytype = string.Empty;

                        foreach (XmlNode ItemtypexKeywordNode in ItemtypeexKeywordList)
                        {
                            skeytype = ItemtypexKeywordNode.Attributes["keytype"].InnerText;
                            sitemtypenum = ItemtypexKeywordNode.Attributes["itemtype"].InnerText;

                            // get all the Keytype nodes for the keytype
                            sSearchString.Length = 0;
                            sSearchString.AppendFormat("/Report/LifeCycle/AllKeytypes/Keytype[@keytypenum={0}]", skeytype);

                            XmlNodeList KeyTypeNodeList = xInput.SelectNodes(sSearchString.ToString());

                            sdatatype = string.Empty;
                            skeytypelen = string.Empty;

                            foreach (XmlNode KeytypeNode in KeyTypeNodeList)
                            {
                                skeytype = KeytypeNode.Attributes["keytype"].InnerText;
                                sdatatype = KeytypeNode.Attributes["datatype"].InnerText;
                                skeytypelen = KeytypeNode.Attributes["keytypelen"].InnerText;
                                int keyTypeLength = 0;
                                int.TryParse(skeytypelen, out keyTypeLength);

                                // Now we're checking for our payment sources.
                                sSearchString.Length = 0;
                                sSearchString.AppendFormat("/Report/LifeCycle/AllDocGrpCL/DocGrpCL/DocumentCL[@itemtypenum={0}]", sitemtypenum);
                                documentCLList = xInput.SelectNodes(sSearchString.ToString());
                                sources = new List<string>();
                                foreach (XmlNode doc in documentCLList)
                                {
                                    var src = doc.ParentNode.Attributes["paymentsource"].Value;
                                    if (!sources.Contains(src))
                                        sources.Add(src);
                                }
                                if (sources.Count == 0)
                                {
                                    // Add the default if we have none.
                                    sources.Add(cfgSettings.DefaultPaymentSource);
                                }

                                // need to add this check since the fieldType might or might not exists
                                if (KeytypeNode.Attributes["fieldtype"] == null)
                                {
                                    sFieldType = string.Empty;
                                    bMarkSense = false;
                                }
                                else
                                {
                                    sFieldType = KeytypeNode.Attributes["fieldtype"].InnerText;

                                    bMarkSense = (sFieldType == cICONBll.GetDescription(SetupFieldType.MarkSenseField)) ? true : false;
                                }

                                if (KeytypeNode.Attributes["extracttype"] == null)
                                {
                                    sExtractType = string.Empty;
                                }
                                else
                                {
                                    sExtractType = KeytypeNode.Attributes["extracttype"].InnerText;
                                }

                                if (!String.IsNullOrEmpty(sExtractType) || !String.IsNullOrEmpty(sFieldType)
                                    || !String.IsNullOrEmpty(sDocType) && !IsStandardImageRPSKeyword(skeytype))
                                {
                                    foreach (var src in sources)
                                    {
                                        bImageRPSAliasMapping = InsertImageRPSAliasMapping(
                                        xmdResult,
                                        new ClientSetupInfo
                                        {
                                            BankId = cfgSettings.DefaultBankID,
                                            WorkGroupId = iLockboxId,
                                            ExtractType = sExtractType,
                                            FieldType = sFieldType,
                                            DocType = sDocType,
                                            KeyType = skeytype,
                                            DataType = sdatatype,
                                            FieldLength = keyTypeLength.ToString(),
                                            BatchSource = src
                                        },
                                        out imageRPSAliasElement);

                                        if (bImageRPSAliasMapping && imageRPSAliasElement != null)
                                        {
                                            string strRpsAlias = String.Format("{0}#{1}#{2}#{3}#{4}", sExtractType, sFieldType, sDocType, skeytype, src);
                                            if (!imageRPSSetupFields.Contains(strRpsAlias))
                                            {
                                                clientElement.AppendChild(imageRPSAliasElement);
                                                imageRPSSetupFields.Add(strRpsAlias);
                                            }
                                        }
                                    }
                                }
                                if (!bImageRPSAliasMapping && !IsStandardImageRPSKeyword(skeytype)
                                    && !IsAmountField(bIsCheck, skeytype, sExtractType, sFieldType))
                                {
                                    SetupField myField = new SetupField(GetFieldName(bIsCheck, skeytype), GetDataEntryTableName(bIsCheck, skeytype)); //CR 31663 1/6/2010 JNE
                                    // check that the keyword isn't in the list before adding it to the XML
                                    if (!listSetupFields.Contains(myField)) //CR 31663 1/6/2010 JNE
                                    {
                                        DataEntryColumn deColumn = new DataEntryColumn();
                                        deColumn.FieldName = GetFieldName(bIsCheck, skeytype);
                                        deColumn.DisplayName = skeytype;
                                        //WJS CR 33568 3/29/2011 always send a screen order of zero
                                        deColumn.ScreenOrder = 0;
                                        deColumn.DataType = GetOLTADataType(Convert.ToInt32(sdatatype));
                                        deColumn.DisplayGroup = GetDataEntryTableName(bIsCheck, skeytype);
                                        deColumn.FieldLength = Convert.ToInt32(skeytypelen);
                                        deColumn.MarkSense = bMarkSense ? "1" : "0";
                                        // add it to the list of SetupFields
                                        listSetupFields.Add(myField);
                                        if (!bDataEntryColumnsExist)
                                        {
                                            bDataEntryColumnsExist = true;
                                            DataEntryColumns dataEntryColumns = new DataEntryColumns();
                                            dataEntryColumns.DataEntryColumnsID = 1;
                                            dataEntryElement = dataEntryColumns.BuildXMLNode(xmdResult);
                                        }
                                        dataEntryElement.AppendChild(deColumn.BuildXMLNode(xmdResult));
                                    }
                                }
                            }
                        }
                    }
                }
                if (bDataEntryColumnsExist)
                {
                    clientElement.AppendChild(dataEntryElement);
                }
                clientGroupElement = _clientXMLNode.BuildClientGroupNode(xmdResult, cfgSettings.DefaultLockboxID);
                clientGroupElement.AppendChild(bankElement);
                clientGroupElement.AppendChild(clientElement);
                topLevelElement.AppendChild(clientGroupElement);

                if (topLevelElement != null)
                {
                    xmdResult.InnerXml = topLevelElement.OuterXml;
                    bRetVal = true;
                }
            }
            catch (Exception ex)
            {
                LogError($"Exception in ProcessClientInputFiles : {ex.Message}");
            }
            return bRetVal;
        }

        private bool IsAmountField(bool bIsCheck, string skeytype, string sExtractType, string sFieldType)
        {
            bool bAmountField = false;
            if (bIsCheck && skeytype == "Applied Amount")
            {
                bAmountField = true;
            }
            else if (!bIsCheck && IsStubAccountOrAmount(skeytype, sFieldType))
            {
                bAmountField = true;
            }
            return bAmountField;
        }
        private bool IsStubAccountOrAmount(string sFieldName, string sFieldType)
        {
            bool bRet = false;

            try
            {
                //WJS CR 45114 only check account, or applied amount if you do not have a field Type
                if (sFieldName.CompareTo("Amount") == 0 ||
                    (String.IsNullOrEmpty(sFieldType) &&
                    (sFieldName.Contains("Account") || sFieldName.CompareTo("Applied Amount") == 0)))
                    bRet = true;
            }
            catch (Exception ex)
            {
                LogError($"Exception in IsStubAccountOrAmount : {ex.Message}");
            }

            return bRet;
        }

        private bool InsertImageRPSAliasMapping(XmlDocument xmlDoc, ClientSetupInfo clientSetupInfo, out XmlElement imageRPSAliasElement)
        {
            if (clientSetupInfo == null)
                throw new ArgumentNullException();

            bool bRetVal = false;
            int iDocType = 0;
            int iExtractType = 0;
            bool bError = false;
            imageRPSAliasElement = null;

            if (!String.IsNullOrEmpty(clientSetupInfo.ExtractType))
            {
                //we have a extractType make sure it is valid
                if (Int32.TryParse(clientSetupInfo.ExtractType, out iExtractType))
                {
                    if (iExtractType != (int)SetupExtractType.Account && iExtractType != (int)SetupExtractType.BusCheckNumber
                         && iExtractType != (int)SetupExtractType.PersonalCheckNum && iExtractType != (int)SetupExtractType.RoutingTransit
                         && iExtractType != (int)SetupExtractType.DocTypeIdentifier)
                    {
                        LogError(
                            $"ExtractType is not valid in InsertImageRPSAlisMapping. ExtractType is  {iExtractType}");
                        bError = true;
                    }
                }
                else
                {
                    LogError(
                        $"Failed to convert extracttype to integer in InsertImageRPSAlisMapping. ExtractType is  {clientSetupInfo.ExtractType}");
                    bError = true;
                }
            }

            if (!String.IsNullOrEmpty(clientSetupInfo.DocType))
            {
                //we have a docType make sure it is valid
                if (Int32.TryParse(clientSetupInfo.DocType, out  iDocType))
                {
                    if (iDocType != (int)SetupDocType.Check && iDocType != (int)SetupDocType.NonCheck)
                    {
                        LogError($"DocType is not valid in InsertImageRPSAlisMapping. DocType is {iDocType} ");
                        bError = true;
                    }
                }
                else
                {
                    LogError(
                        $"Failed to convert docType to integer in InsertImageRPSAlisMapping. DocType is {clientSetupInfo.DocType} ");
                    bError = true;
                }
            }
            if (IsStandardImageRPSKeyword(clientSetupInfo.KeyType))
            {
                bError = true;
                LogWarning(
                    $"Failed to assign keyword, since it assigned as a standard image rps keyword. Keyword is {clientSetupInfo.KeyType} ");
            }

            if (!bError)
            {
                bool bIsCheck = (iDocType == (int)SetupDocType.Check ? true : false);

                clientSetupInfo.KeyType = GetFieldName(bIsCheck, clientSetupInfo.KeyType);
                clientSetupInfo.DocType = iDocType.ToString();
                clientSetupInfo.ExtractType = iExtractType.ToString();
                imageRPSAliasElement = _clientXMLNode.BuildImageRPSAlias(clientSetupInfo, xmlDoc);

                bRetVal = true;

            }

            return bRetVal;
        }

        private static string ToStubField(string keyType)
        {
            if (string.Equals(keyType, "amount", StringComparison.InvariantCultureIgnoreCase))
                return "Amount";

            if (string.Equals(keyType, "applied amount", StringComparison.InvariantCultureIgnoreCase))
                return "Amount";

            return keyType;
        }

        private static string ToCheckField(string keyType)
        {
            if (string.Equals(keyType, "amount", StringComparison.InvariantCultureIgnoreCase))
                return "Amount";

            if (string.Equals(keyType, "applied amount", StringComparison.InvariantCultureIgnoreCase))
                return "Amount";

            if (string.Equals(keyType, "account", StringComparison.InvariantCultureIgnoreCase))
                return "Account";

            if (string.Equals(keyType, "account number", StringComparison.InvariantCultureIgnoreCase))
                return "Account";

            return keyType;
        }

        public static string GetFieldName(bool bIsCheck, string sKeyType)
        {
            return bIsCheck
                ? ToCheckField(sKeyType)
                : ToStubField(sKeyType);
        }

        private int GetOLTADataType(int iDataType)
        {
            int iOLTADataType = 1;

            try
            {
                /*
                ImageRPS Enum	Data Type	    integraPAY Data Type
                1	            Numeric 20      6
                2	            Alphanumeric    1
                3	            Currency        7
                4	            Date            11
                5               Numeric?        6?
                6	            Numeric 9       6
                9	            Date/Time       11
                10	            AlphaNumeric    1
                */

                switch (iDataType)
                {
                    case 1:
                    case 5:
                    case 6:
                        iOLTADataType = 6;
                        break;
                    case 2:
                    case 10:
                        iOLTADataType = 1;
                        break;
                    case 3:
                        iOLTADataType = 7;
                        break;
                    case 4:
                    case 9:
                        iOLTADataType = 11;
                        break;
                }
            }
            catch (Exception ex)
            {
                LogError($"Exception in GetOLTADataType : {ex.Message}");
            }

            return iOLTADataType;
        }

        private string GetDataEntryTableName(bool bIsCheck, string sKeyword)
        {
            string sRet = string.Empty;

            try
            {
                if (bIsCheck)
                {
                    // CR 29756 MEH 05-20-2010 ICON Service does not create check fields (Account, RT, and Serial, etc.) in the correct table
                    if (sKeyword.CompareTo("RemitterName") == 0 || sKeyword.CompareTo("Applied Amount") == 0 || sKeyword.CompareTo("RT") == 0 || sKeyword.CompareTo("Account") == 0 || sKeyword.CompareTo("Serial") == 0 || sKeyword.CompareTo("Transaction Code") == 0)
                        sRet = "Checks";
                    else
                        sRet = "ChecksDataEntry";
                }
                else
                {
                    if (sKeyword.CompareTo("Account") == 0 || sKeyword.CompareTo("Account Number") == 0 || sKeyword.CompareTo("Amount") == 0 || sKeyword.CompareTo("Applied Amount") == 0)
                        sRet = "Stubs";
                    else
                        sRet = "StubsDataEntry";
                }
            }
            catch (Exception ex)
            {
                LogError($"Exception in GetDataEntryTableName : {ex.Message}");
            }

            return sRet;
        }

        public  bool GetClientInfo(string sClientFileName, out string sClientID, out string sShortName, out string sSiteCode)
        {
            bool bRet = true;

            sClientID = string.Empty;
            sShortName = string.Empty;
            sSiteCode = string.Empty;

            try
            {
                // get the ClientID, ShortName, and SiteCode from the client file name
                // file name ="Client21_212121_7000_20091109_134545.XML" 
                // 21 = ClientID
                // 212121 = ShortName
                // 7000 = SiteCode

                string[] myTokens = sClientFileName.Split('_');

                if (myTokens.GetUpperBound(0) == 4)
                {
                    sClientID = myTokens[0].Remove(0, 6);
                    sShortName = myTokens[1];
                    sSiteCode = myTokens[2];
                }
                else
                {
                    LogError(
                        "GetClientInfo file name error!\n" +
                        $"File name: {sClientFileName}\n" +
                        "Expected format is: ClientXXX_SHORTNAME_SITECODE_YYYYMMDD_HHMMSS.XML");

                    bRet = false;
                }
            }
            catch (Exception ex)
            {
                LogError(
                    $"Exception in GetClientInfo : {ex.Message}\n" +
                    $"File name: {sClientFileName}\n" +
                    "Expected format is: ClientXXX_SHORTNAME_SITECODE_YYYYMMDD_HHMMSS.XML");

                bRet = false;
            }

            return bRet;
        }

        public bool ReadDATFile(TextReader file, int bankID, string strClientProcessCode, out XmlDocument xmdResult)
        {
            bool bRetVal = false;
            IMSDocument myDoc = new IMSDocument();
            string line = string.Empty;
            List<IMSDocument> listIMSDocuments = new List<IMSDocument>();
            // Read the file and display it line by line.
            while ((line = file.ReadLine()) != null)
            {
                LogDebug($"Read line from file : {line}");
                if (line.Length > 0)
                {
                    IMSKeywordPair myPair = GetKeywordPair(line);

                    if (myPair.Keyword == cMagicKeywords.TaggedDIP)
                        continue;

                    if (myPair.Keyword == cMagicKeywords.Begin)
                    {
                        // add the IMSDocument to the list 
                        if (myDoc.ClientID != null)
                        {

                            EndOfImage(bankID, myDoc, ref listIMSDocuments);
                        }
                        else
                        {
                            LogDebug($"Begin reached: {line} no client ID");
                        }
                        // create a new IMSDocument
                        myDoc = new IMSDocument();
                    }
                    else if (myPair.Keyword == cMagicKeywords.End)
                    {
                        EndOfImage(bankID, myDoc, ref listIMSDocuments);
                        //clear all the lockbox info
                    }
                    else {
                        //Standardize KeyWord to match with Client Setup process
                        myPair.Keyword = GetFieldName(myDoc.IsCheck, myPair.Keyword);
                        // assign values like BatchID, TransactionID, etc.
                        myDoc.AssignValues(myPair);

                        if (IMSDocument.IncludePairAsSetupField(myPair))
                        {
                            //CR 31536 WJS 3/7/2011 All the data here is additonal data
                            //which was not initial captured for example AuditTrails
                            AddItemToDoc( ref myDoc, myPair);
                        }
                    }
                } //  end of ifline length=0
            }

            bRetVal = _batchXmlNode.BuildBatchXml(
                bankID, listIMSDocuments, strClientProcessCode, out xmdResult);
            return bRetVal;
        }

        /// <summary>
        /// Check Data Setup Fields
        /// </summary>
        /// <param name="myDoc"></param>
        /// <param name="myPair"></param>
        private void AddItemToDoc(ref IMSDocument myDoc, IMSKeywordPair myPair)
        {
            string keyWord = myPair.Keyword;

            if (GetItemData(myPair.Keyword))
            {
                //it is item data which is valid add it to the collection to store
                if (!String.IsNullOrEmpty(myPair.Value))
                {
                    //add item data data
                    //We don't care about the type or the key since it will be taken care of in SSIS
                    cItemData itemData = new cItemData(-1, myPair.Keyword, -1, myPair.Value);
                    myDoc.AddItemDataItem(itemData);
                }
            }
            else if (GetBatchData(myPair.Keyword))
            {
                //it is batch data which is valid add it to collection to store
                if (!String.IsNullOrEmpty(myPair.Value))
                {
                    //add batch Node data
                    //We don't care about the type or the key since it will be taken care of in SSIS
                    cBatchData batchData = new cBatchData(-1, myPair.Keyword, -1, myPair.Value);
                    myDoc.AddBatchDataItem(batchData);
                }
            }
            else if (!IsStandardImageRPSKeyword(myPair.Keyword))
            {
                myDoc.AddKeyword(myPair);
            }
            else
            {
                LogError($"Keyword not found in setup fields table: {keyWord}");
            }
        }
        public bool GetItemData(string strKeyword)
        {
            bool bolRetVal = false;

            if (_ddaDimData.ItemFieldList.Contains(strKeyword))
            {
                bolRetVal = true;

            }
            else
            {
                bolRetVal = false;
            }
            return (bolRetVal);
        }

        public bool GetBatchData(string strKeyword)
        {
            bool bolRetVal = false;

            if (_ddaDimData.BatchFieldList.Contains(strKeyword))
            {
                bolRetVal = true;
            }
            else
            {
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        /// <summary>
        /// Is Standard Image RPS Keyword Method
        /// </summary>
        /// <param name="sKeyword"></param>
        /// <returns></returns>
        public bool IsStandardImageRPSKeyword( string sKeyword)
        {
            bool bRet = false;

            try
            {
                int searchIndex = ImageRPSStandardKeywords.BinarySearch(sKeyword);

                if (searchIndex >= 0 || GetBatchData(sKeyword)
                    || GetItemData(sKeyword))
                {
                    bRet = true;
                }
            }
            catch (Exception ex)
            {
                LogError($"Exception in IsStandardImageRPSKeyword : {ex.Message}");
            }

            return bRet;
        }

        private void EndOfImage(int bankID, IMSDocument myDoc, ref List<IMSDocument> listIMSDocuments)
        {
            int clientID = 0;

            if (Int32.TryParse(myDoc.ClientID, out clientID))
            {
                LogDebug($"End of Image: {clientID}");
                LogDebug($"ImageRPS Alias List {_ddaDimData.ImageRPSAliasList.ContainsKey(clientID)}");

                if (_ddaDimData.ImageRPSAliasList.ContainsKey(clientID))
                {
                    _imageRPSAliasKeywordHashTable = (Hashtable) _ddaDimData.ImageRPSAliasList[clientID];
                }

                ProcessAnyImageAliasRPSKeywords(myDoc);
                listIMSDocuments.Add(myDoc);
            }
            else
            {
                LogError("Could not convert clientID to integer in EndOfImage");
            }
        }

        public IMSKeywordPair GetKeywordPair(string sLine)
        {
            // a line looks like:
            // Client Name:Mylan Technology
            // break the line up and add to the list

            string sKeyword = string.Empty;
            string sValue = string.Empty;
            int idx = -1;

            idx = sLine.IndexOf(':');
            if (idx > 0)
            {
                sKeyword = sLine.Substring(0, sLine.IndexOf(':'));
                sValue = sLine.Substring(idx + 1);
            }
            else
            {
                sKeyword = sLine;
            }

            IMSKeywordPair myPair = new IMSKeywordPair(sKeyword.Trim(), sValue.Trim());

            return myPair;
        }

        private cAliasRPSKeywordValue GetAliasRPSKeyWordValue(string strAliasHashTableKey)
        {
            cAliasRPSKeywordValue aliasRPSValue;
            string[] aliasRPSArray;
            aliasRPSArray = (string[]) _imageRPSAliasKeywordHashTable[strAliasHashTableKey];

            aliasRPSValue = new cAliasRPSKeywordValue(aliasRPSArray[0], aliasRPSArray[1]);

            return aliasRPSValue;
        }

        /// <summary>
        /// If a key defined as extractType = ?1? is contained within a check record in a Dat file
        /// AND the value for that field has a length > 0,
        /// then that check is treated as a business check for purposes of importing into OLTA,
        /// and we look for the field defined as extractType = ?4? to import as TransCode.
        /// Otherwise, that check is a personal check, and we look for the field defined as
        /// extractType = ?4? to import as Serial.
        /// </summary>
        /// <param name="KeywordsList"></param>
        /// <param name="ItemTypeValue"></param>
        private void GetItemType(List<IMSKeywordPair> KeywordsList, out ItemType ItemTypeValue)
        {
            ItemTypeValue = ItemType.Undefined;
            ItemType enmItemTypeValue = ItemType.Undefined;
            try
            {
                string strAliasHashTableKey;
                cAliasRPSKeywordValue aliasRPSValue;

                // Determine if the check is Business or Personal
                enmItemTypeValue = ItemType.PersonalCheck;
                foreach (IMSKeywordPair KeyWordPair in KeywordsList)
                {
                    // CR 34934 JMC 05/19/2011
                    // It's only a business check if the keyword exists for the Item AND its value is populated.
                    if (KeyWordPair.Value.Trim().Length > 0)
                    {
                        strAliasHashTableKey = AliasRPSKeywordLib.BuildKeyword(KeyWordPair.Keyword, SetupDocType.Check).Replace(" ", "");
                        if (_imageRPSAliasKeywordHashTable != null && _imageRPSAliasKeywordHashTable.ContainsKey(strAliasHashTableKey))
                        {
                            aliasRPSValue = GetAliasRPSKeyWordValue(strAliasHashTableKey);

                            if (aliasRPSValue.ExtractType == SetupExtractType.BusCheckNumber)
                            {
                                enmItemTypeValue = ItemType.BusinessCheck;
                                break;
                            }
                        }
                    }
                }

                ItemTypeValue = enmItemTypeValue;
            }
            catch (Exception ex)
            {
                LogError($"Exception in GetItemType : {ex.Message}");
            }
        }
        /// <summary>
        /// -Assigns MICR Keywords to myDoc.MICRFields object
        /// -Removes found MICR Keywords from myDoc.listKeywords collection.
        /// -Calls FigureOutDocTypeName to append IsCheck, IsStub, and DocTypeName to myDoc
        /// </summary>
        /// <param name="myDoc"></param>
        public void ProcessAnyImageAliasRPSKeywords(IMSDocument myDoc)
        {
            string strAliasHashTableKey;
            ItemType enmItemTypeValue;

            try
            {
                bool bValueFound = false;
                IMSKeywordPair keyWordPair;
                cAliasRPSKeywordValue aliasRPSValue = new cAliasRPSKeywordValue();

                // Determine if the check is Business or Personal
                GetItemType(myDoc.listKeywords, out enmItemTypeValue);

                LogDebug("Calling Figure out docType Name", nameof(ProcessAnyImageAliasRPSKeywords));
                // Append IsCheck, IsStub, and DocTypeName to myDoc
                if (FigureOutDocTypeName(myDoc))
                {
                    for (int j = myDoc.listKeywords.Count - 1; j >= 0; j--)
                    {
                        bValueFound = false;
                        keyWordPair = (IMSKeywordPair)myDoc.listKeywords[j];

                        if (myDoc.IsCheck)
                        {
                            // Determine if the the keyword is an Aliased Check field.
                            strAliasHashTableKey = AliasRPSKeywordLib.BuildKeyword(keyWordPair.Keyword, SetupDocType.Check).Replace(" ", "");
                            if (_imageRPSAliasKeywordHashTable.ContainsKey(strAliasHashTableKey))
                            {
                                aliasRPSValue = GetAliasRPSKeyWordValue(strAliasHashTableKey);

                                bValueFound = AliasRPSKeywordLib.AssignMICRInfo(aliasRPSValue.ExtractType, keyWordPair, enmItemTypeValue, myDoc.MICRFields);
                            }
                            else
                            {
                                bValueFound = AliasRPSKeywordLib.IsMICRHardCodeValues(keyWordPair, myDoc.IsCheck, myDoc.MICRFields);
                            }
                        }
                        else
                        {
                            // Determine if the the keyword is an Aliased Stub field.
                            strAliasHashTableKey = AliasRPSKeywordLib.BuildKeyword(keyWordPair.Keyword, SetupDocType.NonCheck).Replace(" ", "");
                            if (_imageRPSAliasKeywordHashTable.ContainsKey(strAliasHashTableKey))
                            {
                                aliasRPSValue = GetAliasRPSKeyWordValue(strAliasHashTableKey);
                                bValueFound = AliasRPSKeywordLib.AssignMICRInfo(aliasRPSValue.ExtractType, keyWordPair, ItemType.NonCheck, myDoc.MICRFields);
                            }
                            else
                            {
                                bValueFound = AliasRPSKeywordLib.IsMICRHardCodeValues(keyWordPair, myDoc.IsCheck, myDoc.MICRFields);
                            }
                        }

                        if (bValueFound)
                        {
                            //now remove it so it does not get passed to data entry
                            myDoc.listKeywords.Remove(myDoc.listKeywords[j]);
                        }
                    }
                }
                else
                {
                    LogInformation("Unable to figure out doc type name", nameof(ProcessAnyImageAliasRPSKeywords));
                }
            }
            catch (Exception ex)
            {
                LogError($"Exception in ProcessAnyImageAliasRPSKeywords : {ex.Message}");
            }
        }

        /// <summary>
        /// Append IsCheck, IsStub, and DocTypeName to myDoc
        /// </summary>
        private bool FigureOutDocTypeName(IMSDocument myDoc)
        {
            try
            {
                LogDebug("Find Keyword", nameof(FigureOutDocTypeName));
                int index = myDoc.listKeywords.FindIndex(pair => pair.Keyword == cMagicKeywords.AADocTypeName);

                if (index != -1)
                {
                    if (_imageRPSAliasKeywordHashTable == null)
                    {
                        LogInformation("Image RPS Alias Keyword Hash Table is empty", nameof(FigureOutDocTypeName));
                        return false;
                    }

                    LogDebug("Build Keyword", nameof(FigureOutDocTypeName));

                    var documentType = myDoc.DocumentType;
                    var docTypeName = myDoc.listKeywords[index].Value;

                    if (documentType != null)
                    {
                        if (documentType?.ToLowerInvariant() == "check")
                        {
                            myDoc.IsCheck = true;
                            LogDebug($"Item is a check (based on Document Type '{documentType}')",
                                nameof(FigureOutDocTypeName));
                        }
                        else
                        {
                            myDoc.IsStub = true;
                            LogDebug($"Item is a stub (based on Document Type '{documentType}')",
                                nameof(FigureOutDocTypeName));
                        }
                    }
                    else
                    {
                        if (docTypeName.ToLowerInvariant().Contains("check"))
                        {
                            myDoc.IsCheck = true;
                            LogDebug($"Item is a check (based on >>DocTypeName '{docTypeName}')",
                                nameof(FigureOutDocTypeName));
                        }
                        else
                        {
                            myDoc.IsStub = true;
                            LogDebug($"Item is a stub (based on >>DocTypeName '{docTypeName}')",
                                nameof(FigureOutDocTypeName));
                        }
                    }

                    if (myDoc.IsCheck)
                    {
                        myDoc.DocTypeName = "Per/Bus Check";
                    }
                    else
                    {
                        if (docTypeName.Contains("Correspondence"))
                        {
                            myDoc.DocTypeName = "Correspondence";
                        }
                        else if (docTypeName.Contains("Envelope"))
                        {
                            myDoc.DocTypeName = "Envelope";
                        }
                        else if (docTypeName.Contains("Invoice"))
                        {
                            myDoc.DocTypeName = "Invoice";
                        }
                        else if (docTypeName.Contains("Miscellaneous"))
                        {
                            myDoc.DocTypeName = "Miscellaneous";
                        }
                        else if (docTypeName.Contains("Stub"))
                        {
                            myDoc.DocTypeName = "Stub";
                        }
                        else
                        {
                            myDoc.DocTypeName = "Unknown";
                        }
                    }

                    //now remove it so it does not get passed to data entry
                    myDoc.listKeywords.Remove(myDoc.listKeywords[index]);
                }

                return true;
            }
            catch (Exception ex)
            {
                LogError($"Exception in FigureOutDocTypeName : {ex.Message}");
                return false;
            }
        }
    }
}
