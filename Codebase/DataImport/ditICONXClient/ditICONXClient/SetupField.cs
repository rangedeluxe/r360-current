﻿using System;
using System.Collections.Generic;
using System.Text;
/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2011.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    5/8/2012
*
* Purpose: 
*
* Modification History
* 05/08/2012 CR 31789 WJS
*     - Initial release.
* 
********************************************************************************/
namespace WFS.integraPAY.Online.DITICONXClient  {
    class SetupField: IEquatable<SetupField> {
        private string m_sFieldName = string.Empty;
        private string m_sTableName = string.Empty;

        public SetupField(string sFieldName, string sTableName){
            this.m_sFieldName = sFieldName;
            this.m_sTableName = sTableName;
        }

        public string FieldName{
            get { return (m_sFieldName); }
            set { m_sFieldName = value; }
        }

        public string TableName{
            get { return (m_sTableName); }
            set { m_sTableName = value; }
        }

        public bool Equals(SetupField other)
        {
            if (this.TableName == other.TableName
                && this.FieldName == other.FieldName)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
