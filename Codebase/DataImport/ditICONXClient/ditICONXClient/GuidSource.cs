﻿using System;

namespace WFS.integraPAY.Online.DITICONXClient
{
    public class GuidSource : IGuidSource
    {
        public Guid CreateBatchSourceTrackingId()
        {
            return Guid.NewGuid();
        }
        public Guid CreateBatchTrackingId()
        {
            return Guid.NewGuid();
        }
    }
}