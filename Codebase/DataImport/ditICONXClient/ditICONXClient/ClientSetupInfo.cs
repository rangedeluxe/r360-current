﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.integraPAY.Online.DITICONXClient
{
    public class ClientSetupInfo
    {
        public string KeyType { get; set; }
        public string DocType { get; set; }
        public string DataType { get; set; }
        public string ExtractType { get; set; }
        public string FieldType { get; set; }
        public string FieldLength { get; set; }
        public int BankId { get; set; }
        public int WorkGroupId { get; set; }
        public string BatchSource { get; set; }
    }
}
