﻿namespace WFS.integraPAY.Online.DITICONXClient
{
    public interface IFileDescriptorSource
    {
        string GetFileDescriptor(string sDocTypeName);
    }
}