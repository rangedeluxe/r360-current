﻿

/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2011.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    3/2/2011
*
* Purpose:  DocumentType.cs.
*
* Modification History
* 03/02/2011 CR 31536 WJS
*     - Initial release.
******************************************************************************/

namespace WFS.integraPAY.Online.DITICONXClient.Items
{
    public class IMSDocumentType
    {
        private string sFileDescriptor = string.Empty;
        private string sIMSDocumentType = string.Empty;

        public IMSDocumentType(string sFileDescriptor, string sIMSDocumentType)
        {
            this.sFileDescriptor = sFileDescriptor;
            this.sIMSDocumentType = sIMSDocumentType;
        }

        public string FileDescriptor
        {
            get { return (sFileDescriptor); }
            set { sFileDescriptor = value; }
        }

        public string DocumentType
        {
            get { return (sIMSDocumentType); }
            set { sIMSDocumentType = value; }
        }
    }
}
