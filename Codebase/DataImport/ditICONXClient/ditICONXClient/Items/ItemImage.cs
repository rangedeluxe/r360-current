﻿using WFS.RecHub.Common;

namespace WFS.integraPAY.Online.DITICONXClient.Items
{
    public class ItemImage
    {
        public ItemImage(string path, WorkgroupColorMode colorMode, bool isBack, bool allowMultiPageTiff)
        {
            Path = path;
            ColorMode = colorMode;
            IsBack = isBack;
            AllowMultiPageTiff = allowMultiPageTiff;
        }

        public WorkgroupColorMode ColorMode { get; }
        public bool IsBack { get; }
        public string Path { get; }
        public bool AllowMultiPageTiff { get; }
    }
}