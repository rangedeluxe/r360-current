﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    3/2/2011
*
* Purpose:  cBatchData.cs.
*
* Modification History
* 03/02/2011 CR 31536 WJS
*     - Initial release.
******************************************************************************/
namespace WFS.integraPAY.Online.DITICONXClient.Items
{
    /// <summary>
    /// Summary description for cBatchData.
    /// </summary>
    public class cBatchData: cData
    {
        /// <summary>
        /// CBatchData constructor
        /// </summary>
        /// <param name="iDataSetupFieldKey"></param>
        /// <param name="sKeyword"></param>
        /// <param name="iDataType"></param>
        /// <param name="sDataValue"></param>
        public cBatchData(int iDataSetupFieldKey, string sKeyword, int iDataType, string sDataValue)
            : base(iDataSetupFieldKey, sKeyword, iDataType, sDataValue)
        {
		}
    }
}
