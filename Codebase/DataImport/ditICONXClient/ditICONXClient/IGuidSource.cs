﻿using System;

namespace WFS.integraPAY.Online.DITICONXClient
{
    public interface IGuidSource
    {
        Guid CreateBatchSourceTrackingId();
        Guid CreateBatchTrackingId();
    }
}