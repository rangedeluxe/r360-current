﻿using System;
using System.Xml;
using WFS.integraPAY.Online.ICONCommon;
using WFS.LTA.DataImport.DataImportXClientBase;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportIconClient;

/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2011.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    5/24/2012
*
* Purpose:
*
* Modification History
* 05/24/2012 CR 31789 WJS
*     - Initial release.
*
* 02/13/2014 WI 128746 CMC Updating xsd version
* 02/14/2014 WI 129753 CMC
*   - Adding additional config attributes
*   (BankName, DataRetentionDays, and ImageRetentionDays)
* 05/02/2014 WI 139703 CMC
* - Handle Data Type Logging In ICON XClient When Building ImageRPSAliasMappings Element
********************************************************************************/

namespace WFS.integraPAY.Online.DITICONXClient
{
    public class cClientXMLNode
    {

        private const string CLIENTXSDVERSION = "2.01.02.00";
        private ditICONXClient _ditXClient = null;
        private cICONBll _iconBll = null;
        private cConfigData configurationContext = null;

        public cClientXMLNode(cICONBll iconBLL, ditICONXClient xClient) 
            : this(iconBLL, xClient, new cICONConfigData(-1, -1, string.Empty, string.Empty))
        {

        }

        public cClientXMLNode(cICONBll iconBLL, ditICONXClient xClient, cConfigData configurationContext) 
        {
            _ditXClient = xClient;
            _iconBll = iconBLL;
            this.configurationContext = configurationContext;
        }
       
        public XmlElement BuildClientXML(XmlDocument xmlDoc, string strClientProcessCode)
        {
            Guid clientUniqueID = Guid.NewGuid();
            XmlElement topLevelElement = null;
            try
            {
                topLevelElement = (XmlElement)xmlDoc.CreateElement("ClientGroups");
                topLevelElement.SetAttribute("SourceTrackingID", clientUniqueID.ToString());
                topLevelElement.SetAttribute("XSDVersion", CLIENTXSDVERSION);
                topLevelElement.SetAttribute("ClientProcessCode", strClientProcessCode.Trim());
            }
            catch (Exception ex)
            {
                _ditXClient.DoLogEvent("Exception in BuildClientXML : " + ex.Message
                                 , this.GetType().Name
                                 , LogEventType.Error
                                  , LogEventImportance.Essential);
            }
            return topLevelElement;
        }

        public XmlElement BuildBankNode(XmlDocument xmlDoc, int bnkId)
        {
            Bank bk = new Bank();
            bk.BankID = bnkId;
            bk.BankName = configurationContext.BankName;
            return (bk.BuildXMLNode(xmlDoc));
        }

        public XmlElement BuildClientGroupNode(XmlDocument xmlDoc, int customerID)
        {
            ClientGroup cg = new ClientGroup();
            cg.ClientTrackingID = Guid.NewGuid();
            cg.ClientGroupID = customerID;
            return (cg.BuildXMLNode(xmlDoc));
        }
        public XmlElement BuildClientNode(XmlDocument xmlDoc, cClientInfo cinClientInfo)
        {
            XmlElement clientElement;
            Client client = new Client();
            client.ClientID = cinClientInfo.ClientID;

            client.ShortName = cinClientInfo.ShortName.Length > 20 ? cinClientInfo.ShortName.Substring(0, 20) : cinClientInfo.ShortName; // Lockbox.ShortName is char[11]

            client.LongName = cinClientInfo.ShortName;

            client.SiteCode = cinClientInfo.SiteCode;
            client.ImageRetentionDays = configurationContext.ImageRetentionDays;
            client.DataRetentionDays = configurationContext.DataRetentionDays;
           
            clientElement = client.BuildXMLNode(xmlDoc);

            return clientElement;
        }


        public XmlElement BuildImageRPSAlias(ClientSetupInfo requestContext, XmlDocument xmlDoc)
        {
            if (requestContext == null)
                throw new ArgumentNullException();

            XmlElement imageRPSAliasElement;
            ImageRPSAliasMappings aliasMapping = new ImageRPSAliasMappings
                {
                    AliasName = requestContext.KeyType,
                    DocType = requestContext.DocType,
                    ExtractType = requestContext.ExtractType,
                    FieldType = requestContext.FieldType,
                    DataType = GetOLTADataType(requestContext.DataType),
                    FieldLength = requestContext.FieldLength,
                    BatchSource = requestContext.BatchSource
                };

            imageRPSAliasElement = aliasMapping.BuildXMLNode(xmlDoc);
            return imageRPSAliasElement;
        }

        public XmlElement BuildImageRPSAlias(string skeytype, string sDocType, string sExtractType, string sFieldType, string sFieldLength, string sDataType, string sBatchSource, XmlDocument xmlDoc)
        {
            return BuildImageRPSAlias
                (new ClientSetupInfo
                    {
                        KeyType = skeytype,
                        DocType = sDocType,
                        ExtractType = sExtractType,
                        FieldType = sFieldType,
                        FieldLength = sFieldLength,
                        DataType = sDataType,
                        BatchSource = sBatchSource
                    },
                    xmlDoc
                );
        }

        private string GetOLTADataType(string sDataType)
        {
            int iOLTADataType = 1;
            int iDataType = 1;
            try
            {
                if (Int32.TryParse(sDataType, out iDataType))
                {
                    switch ((ImageRpsDataType) iDataType)
                    {
                        case ImageRpsDataType.Numeric:
                        case ImageRpsDataType.AlternateNumeric:
                        case ImageRpsDataType.IdOrSequenceNumber:
                            iOLTADataType = 6;
                            break;
                        case ImageRpsDataType.String:
                        case ImageRpsDataType.AlternateAlphanumeric:
                            iOLTADataType = 1;
                            break;
                        case ImageRpsDataType.Currency:
                            iOLTADataType = 7;
                            break;
                        case ImageRpsDataType.Date:
                        case ImageRpsDataType.AlternateDateTime:
                            iOLTADataType = 11;
                            break;
                        default:
                            iOLTADataType = 1;
                            _ditXClient.DoLogEvent(string.Format("Unknown data type value ‘{0}’.  Defaulting to ‘1’ for alphanumeric.", sDataType)
                               , this.GetType().Name
                               , LogEventType.Warning
                                 , LogEventImportance.Verbose);
                            break;
                    }
                }
                else
                {
                    _ditXClient.DoLogEvent(string.Format("Failed to convert data type value ‘{0}’.  Defaulting to ‘1’ for alphanumeric.",  sDataType)
                               , this.GetType().Name
                               , LogEventType.Warning
                                 , LogEventImportance.Verbose);
                }
            }
            catch (System.Exception ex)
            {
                _ditXClient.DoLogEvent("Exception in GetOLTADataType : " + ex.Message
                                , this.GetType().Name
                                , LogEventType.Error
                                  , LogEventImportance.Essential);
            }

            return iOLTADataType.ToString();
        }                 
    }   
}
