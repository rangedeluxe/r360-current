﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using WFS.LTA.DataImport.DataImportIconClient;
using WFS.integraPAY.Online.ICONCommon;
using WFS.LTA.DataImport.DataImportXClientBase;

/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2011.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    5/8/2012
*
* Purpose: 
*
* Modification History
* 05/08/2012 CR 31789 WJS
*     - Initial release.
* 
* 02/14/2014 WI 129753 CMC  
*   - Adding additional config attributes 
*   (BankName, DataRetentionDays, and ImageRetentionDays)
********************************************************************************/

namespace WFS.integraPAY.Online.DITICONXClient
{
    public class ditICONXClient : cDataImportXClientBase, IDataImportIconClient, IIconLogTarget
    {
        public ditICONXClient() : base()
        {
           DoLogEvent( "foobar", "DITXClientBase", LogEventType.Warning, LogEventImportance.Verbose);
        }

        public bool GetClientInfo(string sFilename, out cClientInfo cinClientInfo, out string sErrorMessage)
        {
            bool bRetVal = false;
            string sClientID = string.Empty;
            string sShortName = string.Empty;
            string sSiteCode = string.Empty;
            int tempInt = -1;
            cinClientInfo = new cClientInfo();
            sErrorMessage = string.Empty;
            try
            {
                cICONBll iconBll = new cICONBll(this);
                if (iconBll.GetClientInfo(sFilename, out sClientID, out sShortName, out sSiteCode))
                {
                    cinClientInfo.ShortName = sShortName;
                    if (Int32.TryParse(sClientID,out  tempInt))
                    {
                         cinClientInfo.ClientID = tempInt;
                         bRetVal = true; 
                    }
                    else
                    {
                        sErrorMessage= "Failed to convert clientID";
                        throw new Exception(sErrorMessage);
                    }
                    if (bRetVal && (Int32.TryParse(sSiteCode,out  tempInt)))
                    {
                         cinClientInfo.SiteCode = tempInt;
                    }
                    else
                    {
                        bRetVal = false; 
                        sErrorMessage= "Failed to convert site code";
                        throw new Exception(sErrorMessage);
                    }
                }
            }
            catch (System.Exception ex)
            {
                DoLogEvent("Exception in ClientStreamToXML : " + ex.Message
                                , this.GetType().Name
                                , LogEventType.Error
                                , LogEventImportance.Essential);

            }
            return bRetVal;

        }
        public bool GetClientIDs(StreamReader stmInputFile, out int[] aiClientIDs, out string sErrorMessage)
        {
            bool bRetVal = false;
            sErrorMessage = string.Empty;
            string line = string.Empty;
            int tempInt = 0;
            List<int> listClientID = new List<int>();
            cICONBll iconBll = new cICONBll(this);
            // Read the file and display it line by line.
            while ((line = stmInputFile.ReadLine()) != null)
            {
                DoLogEvent("GetClientID: Read line from file : " + line
                        , this.GetType().Name
                        , LogEventType.Information
                        , LogEventImportance.Debug);
                if (line.Length > 0)
                {
                    IMSKeywordPair myPair = iconBll.GetKeywordPair(line);

                    if (myPair.Keyword == cMagicKeywords.ClientID)
                    {
                        if (Int32.TryParse(myPair.Value, out tempInt))
                        {
                            if (!listClientID.Contains(tempInt))
                            {
                                listClientID.Add(tempInt);
                                bRetVal = true;
                            }
                        }
                        else
                        {
                            sErrorMessage = "GetClientID: Failed to parse clientID into int : " + line;
                            DoLogEvent(sErrorMessage
                                       , this.GetType().Name
                                       , LogEventType.Information
                                       , LogEventImportance.Essential);
                            
                        }
                    }
                  
                }
            }
            aiClientIDs = listClientID.ToArray();
            return bRetVal;
        }

        public bool StreamToBatchXML(StreamReader stmInputFile, cICONConfigData cfgSettings,  cDimData ddaDimData, string strClientProcessCode, out XmlDocument xmdResult)
        {
            bool bRetVal = false;
            xmdResult = new XmlDocument();
            try
            {
                cICONBll iconBll = new cICONBll(this, ddaDimData);

                bRetVal = iconBll.ReadDATFile(stmInputFile, cfgSettings.DefaultBankID, strClientProcessCode, out xmdResult);
               
            }
            catch (System.Exception ex)
            {
                DoLogEvent("Exception in BatchStreamToXML : " + ex.Message
                                , this.GetType().Name
                                , LogEventType.Error
                                , LogEventImportance.Essential);

            }
            return bRetVal;
        }
        public  bool StreamToClientSetupXML(StreamReader stmInputFile, cClientInfo cinClientInfo, cDimData ddaDimData,
                                cICONConfigData cfgSettings, string strClientProcessCode, out XmlDocument xmdResult)
        {
            bool bRetVal = false;
            xmdResult = new XmlDocument();

            cICONBll iconBll = new cICONBll(this, ddaDimData, cfgSettings);
            try
            {
                bRetVal =iconBll.ProcessClientFile(stmInputFile,cinClientInfo, cfgSettings, strClientProcessCode, out xmdResult);
                
            }
            catch (System.Exception ex)
            {
                DoLogEvent("Exception in ClientStreamToXML : " + ex.Message
                                , this.GetType().Name
                                , LogEventType.Error
                                , LogEventImportance.Essential);

            }
            return bRetVal;
        }
        public bool WriteBatchResponse(string sBankID, string sLockboxID, string sBatchID, DateTime datProcessDate,
                    bool bSuccessfullyUploaded, cICONConfigData cfgSettings, out string sErrorMessage)
        {
            bool bRetVal = true;
            sErrorMessage = string.Empty;
            return bRetVal;
        }
 
        /*
        public  override bool ClientStreamToXML(StreamReader stmInputFile, string fileName, out XmlDocument xmdResult)
        {
            bool bRetVal = false;
            xmdResult = new XmlDocument();
           
            try
            {
                cICONBll iconBll = new cICONBll(this);
                iconBll.ProcessClientFile(stmInputFile, fileName, out xmdResult);
                bRetVal = true;
            }
            catch (System.Exception ex)
            {
                DoLogEvent("Exception in ClientStreamToXML : " + ex.Message
                                , this.GetType().Name
                                , LogEventType.Error
                                , LogEventImportance.Essential);

            }
            return bRetVal;
        }*/
        /*
        public override bool BatchStreamToXML(StreamReader stmInputFile, 
                            string fileName, out XmlDocument xmdResult,
                             out string[] imageFileNames)
        {
            bool bRetVal = false;
            xmdResult = new XmlDocument();
            imageFileNames = null;
           
            try
            {
                ArrayList imageFilesArrayList = new ArrayList();
                cICONBll iconBll = new cICONBll(this);

                iconBll.ReadDATFile(stmInputFile, base.DefaultBankID, out xmdResult, out imageFilesArrayList);
                imageFileNames = (String[])imageFilesArrayList.ToArray(typeof(string));
                DoLogEvent("test", "DITXClientBase", LogEventType.Warning, LogEventImportance.Verbose);
                bRetVal = true;
            }
            catch (System.Exception ex)
            {
                DoLogEvent("Exception in BatchStreamToXML : " + ex.Message
                                , this.GetType().Name
                                , LogEventType.Error
                                , LogEventImportance.Essential);
              
            }
            return bRetVal;
        }

        public override bool WriteBatchResponse(string sBankID, string sLockboxID, string sBatchID, DateTime datProcessDate, bool bSuccessfullyUploaded, out string sErrorMessage)
        {
            return base.WriteBatchResponse(sBankID, sLockboxID, sBatchID, datProcessDate, bSuccessfullyUploaded, out sErrorMessage);
        }*/

        
       
    }
}

