﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.integraPAY.Online.DITICONXClient;
using WFS.integraPAY.Online.ICONCommon;

namespace DITICONClientTest
{
    [TestClass]
    public class IconCanonicalXmlFieldNameTests
    {
        [TestInitialize]
        public void SetUp()
        {
            SetupDocType = SetupDocType.Unknown;
        }

        private SetupDocType SetupDocType { get; set; }

        private void AssertFieldNameMapsTo(string inputFieldName, string expected)
        {
            var actual = cICONBll.GetFieldName(GetIsCheck(), inputFieldName);
            Assert.AreEqual(expected, actual);
        }
        private bool GetIsCheck()
        {
            switch (SetupDocType)
            {
                case SetupDocType.Check:
                    return true;
                case SetupDocType.NonCheck:
                    return false;
                default:
                    throw new InvalidOperationException("SetupDocType was not specified");
            }
        }

        [TestMethod]
        public void CheckFieldName_WhenNull_ReturnsNull()
        {
            SetupDocType = SetupDocType.Check;
            AssertFieldNameMapsTo(null, null);
        }
        [TestMethod]
        public void CheckFieldName_WhenAmountOrVariantThereof_ReturnsAmount()
        {
            SetupDocType = SetupDocType.Check;
            AssertFieldNameMapsTo("Amount", "Amount");
            AssertFieldNameMapsTo("AMOUNT", "Amount");
            AssertFieldNameMapsTo("Applied Amount", "Amount");
            AssertFieldNameMapsTo("APPLIED AMOUNT", "Amount");
        }
        [TestMethod]
        public void CheckFieldName_WhenAccountOrVariantThereof_ReturnsAccount()
        {
            SetupDocType = SetupDocType.Check;
            AssertFieldNameMapsTo("Account", "Account");
            AssertFieldNameMapsTo("ACCOUNT", "Account");
            AssertFieldNameMapsTo("Account Number", "Account");
            AssertFieldNameMapsTo("ACCOUNT NUMBER", "Account");
        }
        [TestMethod]
        public void CheckFieldName_OtherValues_ReturnsInput()
        {
            SetupDocType = SetupDocType.Check;
            AssertFieldNameMapsTo("AMOUNTx", "AMOUNTx");
            AssertFieldNameMapsTo("APPLIED AMOUNTx", "APPLIED AMOUNTx");
            AssertFieldNameMapsTo("ACCOUNTx", "ACCOUNTx");
            AssertFieldNameMapsTo("ACCOUNT NUMBERx", "ACCOUNT NUMBERx");
            AssertFieldNameMapsTo("Client ID", "Client ID");
            AssertFieldNameMapsTo("CLIENT ID", "CLIENT ID");
        }
        [TestMethod]
        public void StubFieldName_WhenNull_ReturnsNull()
        {
            SetupDocType = SetupDocType.NonCheck;
            AssertFieldNameMapsTo(null, null);
        }
        [TestMethod]
        public void StubFieldName_WhenAmountOrVariantThereof_ReturnsAmount()
        {
            SetupDocType = SetupDocType.NonCheck;
            AssertFieldNameMapsTo("Amount", "Amount");
            AssertFieldNameMapsTo("AMOUNT", "Amount");
            AssertFieldNameMapsTo("Applied Amount", "Amount");
            AssertFieldNameMapsTo("APPLIED AMOUNT", "Amount");
        }
        [TestMethod]
        public void StubFieldName_WhenAccountOrVariantThereof_ReturnsInput()
        {
            SetupDocType = SetupDocType.NonCheck;
            AssertFieldNameMapsTo("Account", "Account");
            AssertFieldNameMapsTo("ACCOUNT", "ACCOUNT");
            AssertFieldNameMapsTo("Account Number", "Account Number");
            AssertFieldNameMapsTo("ACCOUNT NUMBER", "ACCOUNT NUMBER");
            AssertFieldNameMapsTo("AccountNumber", "AccountNumber");
            AssertFieldNameMapsTo("ACCOUNTNUMBER", "ACCOUNTNUMBER");
        }
        [TestMethod]
        public void StubFieldName_OtherValues_ReturnsInput()
        {
            SetupDocType = SetupDocType.NonCheck;
            AssertFieldNameMapsTo("AMOUNTx", "AMOUNTx");
            AssertFieldNameMapsTo("APPLIED AMOUNTx", "APPLIED AMOUNTx");
            AssertFieldNameMapsTo("ACCOUNTx", "ACCOUNTx");
            AssertFieldNameMapsTo("ACCOUNT NUMBERx", "ACCOUNT NUMBERx");
            AssertFieldNameMapsTo("Client ID", "Client ID");
            AssertFieldNameMapsTo("CLIENT ID", "CLIENT ID");
        }
    }
}
