﻿using System.Collections;
using System.Collections.Generic;
using WFS.integraPAY.Online.ICONCommon;
using WFS.LTA.DataImport.DataImportIconClient;

namespace DITICONClientTest
{
    public class FakeDimData : cDimData
    {
        public FakeDimData()
            : base(new string[] {}, new string[] {}, new KeyValuePair<string, string>[] {}, new Hashtable())
        {
        }

        public void AddDocTypeName(int workgroupId, string docTypeName, SetupDocType setupDocType)
        {
            var key = AliasRPSKeywordLib.BuildKeyword(docTypeName, setupDocType).Replace(" ", "");
            var value = "Should be a two-element string array but the code won't actually go that deep";

            EnsureHashtableExistsForWorkgroupId(workgroupId);
            var innerHashtable = (Hashtable) ImageRPSAliasList[workgroupId];
            innerHashtable.Add(key, value);
        }
        /// <summary>Make sure dimensional data exists for a given workgroup. See Remarks.</summary>
        /// <remarks>
        /// <para>
        ///     The ICON XClient has some weird code around how it keeps track of what state
        ///     it's in as it reads the batch file. That code relies on its "current workgroup's
        ///     Hashtable" field starting off null, and staying null until it reads a row that
        ///     corresponds to an actual workgroup (represented as a Hashtable containing the
        ///     ImageRPS aliases).
        /// </para>
        /// <para>
        ///     Here we force it to be willing to handle data for a given workgroup, by pretending
        ///     we have ImageRPS aliases for that workgroup (but without actually creating those
        ///     ImageRPS aliases for tests that don't actually depend on them).
        /// </para>
        /// </remarks>
        /// <param name="workgroupId"></param>
        public void EnsureHashtableExistsForWorkgroupId(int workgroupId)
        {
            if (!ImageRPSAliasList.ContainsKey(workgroupId))
                ImageRPSAliasList.Add(workgroupId, new Hashtable());
        }
    }
}