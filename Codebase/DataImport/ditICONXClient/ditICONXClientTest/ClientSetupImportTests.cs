﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DITICONClientTest
{
    [TestClass]
    public class ClientSetupImportTests : BaseClientSetupImportTestCase
    {
        [TestMethod]
        public void CanonicalXml_RootElementIsClientGroups()
        {
            var canonicalXml = ParseClientSetup(new XElement("Empty"));
            Assert.AreEqual("ClientGroups", canonicalXml.Name);
        }
        [TestMethod]
        public void CanonicalXml_ContainsClientProcessCode()
        {
            var canonicalXml = ParseClientSetup(new XElement("Empty"));
            Assert.AreEqual(ClientProcessCode, canonicalXml.Attribute("ClientProcessCode")?.Value);
        }
        [TestMethod]
        public void CanonicalXml_ContainsWorkgroupId()
        {
            var canonicalXml = ParseClientSetup(new XElement("Empty"));
            Assert.AreEqual(WorkgroupId.ToString(),
                canonicalXml.Element("ClientGroup")?.Attribute("ClientGroupID")?.Value);
        }
        [TestMethod]
        public void CanonicalXml_ContainsBankId()
        {
            var canonicalXml = ParseClientSetup(new XElement("Empty"));
            Assert.AreEqual(BankId.ToString(),
                canonicalXml.Element("ClientGroup")?.Element("Bank")?.Attribute("BankID")?.Value);
        }
        [TestMethod]
        public void CanonicalXml_ContainsBankName()
        {
            const string testBankName = "MyBankName123";
            BankName = testBankName;
            var canonicalXml = ParseClientSetup(new XElement("Empty"));
            Assert.AreEqual(testBankName,
                canonicalXml.Element("ClientGroup")?.Element("Bank")?.Attribute("BankName")?.Value);
        }
        [TestMethod]
        public void BankName_CorrectlyEncodesXmlSpecialCharacters()
        {
            const string testBankName = "<>&'\"";
            BankName = testBankName;
            var canonicalXml = ParseClientSetup(new XElement("Empty"));
            Assert.AreEqual(testBankName,
                canonicalXml.Element("ClientGroup")?.Element("Bank")?.Attribute("BankName")?.Value);
        }
        [TestMethod]
        public void CanonicalXml_ContainsClientId()
        {
            var canonicalXml = ParseClientSetup(new XElement("Empty"));
            Assert.AreEqual(ClientId.ToString(),
                canonicalXml.Element("ClientGroup")?.Element("Client")?.Attribute("ClientID")?.Value);
        }
        [TestMethod]
        public void CanonicalXml_ContainsSiteCode()
        {
            var canonicalXml = ParseClientSetup(new XElement("Empty"));
            Assert.AreEqual(SiteCode.ToString(),
                canonicalXml.Element("ClientGroup")?.Element("Client")?.Attribute("SiteCode")?.Value);
        }
        [TestMethod]
        public void CanonicalXml_ContainsClientShortName()
        {
            var canonicalXml = ParseClientSetup(new XElement("Empty"));
            Assert.AreEqual(ClientShortName,
                canonicalXml.Element("ClientGroup")?.Element("Client")?.Attribute("ShortName")?.Value,
                "ShortName attribute");
            Assert.AreEqual(ClientShortName,
                canonicalXml.Element("ClientGroup")?.Element("Client")?.Attribute("LongName")?.Value,
                "LongName attribute");
        }
    }
}