using System.IO;
using System.Xml;
using System.Xml.Linq;
using WFS.LTA.DataImport.DataImportIconClient;

namespace DITICONClientTest
{
    public class BaseClientSetupImportTestCase : BaseIconBllTestCase
    {
        protected const int BankId = 11;
        protected const int ClientId = 12;
        protected const int SiteCode = 13;
        protected const int WorkgroupId = 14;
        protected const string ClientProcessCode = "ClientProcessCode";
        protected const string ClientShortName = "ClientShortName";
        private const string PaymentSource = "PaymentSource";
        private const string ResponseDirectory = @"C:\xyz";

        private XElement ParseClientSetup(TextReader reader)
        {
            XmlDocument result;
            var clientInfo = new cClientInfo(ClientId, ClientShortName, SiteCode);
            var configData = new cICONConfigData(BankId, WorkgroupId, ResponseDirectory, PaymentSource);
            CreateIconBll().ProcessClientFile(reader, clientInfo, configData, ClientProcessCode, out result);
            AssertNoWarningsOrErrors();
            return XElement.Parse(result.OuterXml);
        }
        protected XElement ParseClientSetup(XElement element)
        {
            using (var reader = new StringReader(element.ToString()))
            {
                return ParseClientSetup(reader);
            }
        }
    }
}