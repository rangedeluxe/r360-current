﻿using System.IO;
using System.Reflection;

namespace DITICONClientTest
{
    public static class TestData
    {
        public static Stream GetStream(string path)
        {
            var baseNamespace = typeof(TestData).Namespace;
            var relativeResourceName = path
                .Replace('/', '.')
                .Replace('\\', '.');
            var resourceName = $"{baseNamespace}.{relativeResourceName}";
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
            if (stream == null)
            {
                throw new FileNotFoundException(
                    $"Could not find embedded resource '{path}'. Make sure the file's Build Action is set to 'Embedded Resource'.");
            }
            return stream;
        }
        public static StreamReader GetStreamReader(string path)
        {
            var stream = GetStream(path);
            // When the returned StreamReader is disposed, it will dispose the stream.
            return new StreamReader(stream);
        }
        public static string GetText(string path)
        {
            using (var reader = GetStreamReader(path))
                return reader.ReadToEnd();
        }
    }
}