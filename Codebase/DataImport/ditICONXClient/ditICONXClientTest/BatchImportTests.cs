﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.integraPAY.Online.ICONCommon;

namespace DITICONClientTest
{
    /// <summary>
    /// Tests that import dynamically-generated "batch file" content (without actually
    /// dealing with files on disk).
    /// </summary>
    [TestClass]
    public class BatchImportTests : BaseBatchImportTestCase
    {
        private const int WorkgroupId = 303030;

        [TestInitialize]
        public void SetUp()
        {
            DimData.EnsureHashtableExistsForWorkgroupId(WorkgroupId);
        }

        [TestMethod]
        public void IfDocumentTypeIsCheck_ThenItemIsAPayment()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE", documentType: null, extraInputLines: new[]
            {
                "Document Type: Check",
            });
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, paymentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsCheck_CaseInsensitive_ThenItemIsAPayment()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE", documentType: null, extraInputLines: new[]
            {
                "Document Type: cHeCk",
            });
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, paymentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsAnythingButCheck_ThenItemIsAStub()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE", documentType: null, extraInputLines: new[]
            {
                "Document Type: Check1",
            });
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, ghostDocumentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsBlank_ThenItemIsAStub()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE", documentType: null, extraInputLines: new[]
            {
                "Document Type:",
            });
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, ghostDocumentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsNotPresent_AndᐳᐳDocTypeNameContainsCheck_ThenItemIsACheck()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, docTypeName: "MyCheck", documentType: null);
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, paymentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsNotPresent_AndᐳᐳDocTypeNameContainsCheck_CaseInsensitive_ThenItemIsACheck()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, docTypeName: "mYcHeCk", documentType: null);
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, paymentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsNotPresent_AndᐳᐳDocTypeNameDoesNotContainCheck_ThenItemIsAStub()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, docTypeName: "chec", documentType: null);
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, ghostDocumentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsCheck_AndᐳᐳDocTypeNameIsStub_ThenDocumentTypeWinsAndItemIsAPayment()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, docTypeName: "Stub", documentType: SetupDocType.Check);
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, paymentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsStub_AndᐳᐳDocTypeNameIsCheck_ThenDocumentTypeWinsAndItemIsAStub()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, docTypeName: "Check", documentType: SetupDocType.NonCheck);
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, ghostDocumentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsUnexpectedValue_AndᐳᐳDocTypeNameIsCheck_ThenDocumentTypeWinsAndItemIsAStub()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, docTypeName: "Check", documentType: null,
                extraInputLines: new[]
                {
                    "Document Type: asdf",
                });
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, ghostDocumentCount: 1);
        }
        [TestMethod]
        public void IfDocumentTypeIsPresentButBlank_AndᐳᐳDocTypeNameIsCheck_ThenDocumentTypeWinsAndItemIsAStub()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, docTypeName: "Check", documentType: null,
                extraInputLines: new[]
                {
                    "Document Type:",
                });
            var canonicalXml = ParseDatLines(lines);
            CheckItemCounts(canonicalXml, ghostDocumentCount: 1);
        }
        [TestMethod]
        public void Check_WithNoAmount_HasNoIsCorrespondenceAttribute()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE",
                documentType: SetupDocType.Check);

            var canonicalXml = ParseDatLines(lines);
            AssertIsCorrespondenceAttributeIsNotPresent(canonicalXml, "Payment");
        }
        [TestMethod]
        public void Stub_WithNoAmount_HasIsCorrespondenceSet()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE",
                documentType: SetupDocType.NonCheck,
                extraInputLines: new[]
                {
                    @">>FullPath:C:\Image.tif",
                });

            var canonicalXml = ParseDatLines(lines);
            AssertIsCorrespondenceAttributeHasValue(canonicalXml, "Document", "1");
        }
        [TestMethod]
        public void Stub_WithAmount_HasIsCorrespondenceUnset()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE",
                documentType: SetupDocType.NonCheck,
                extraInputLines: new[]
                {
                    @">>FullPath:C:\Image.tif",
                    "Amount:0.00",
                });

            var canonicalXml = ParseDatLines(lines);
            AssertIsCorrespondenceAttributeHasValue(canonicalXml, "Document", "0");
        }
        [TestMethod]
        public void Stub_WithAppliedAmount_HasIsCorrespondenceUnset()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE",
                documentType: SetupDocType.NonCheck,
                extraInputLines: new[]
                {
                    @">>FullPath:C:\Image.tif",
                    "Applied Amount:0.00",
                });

            var canonicalXml = ParseDatLines(lines);
            AssertIsCorrespondenceAttributeHasValue(canonicalXml, "Document", "0");
        }
        [TestMethod]
        public void GhostStub_WithNoAmount_HasIsCorrespondenceSet()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE",
                documentType: SetupDocType.NonCheck);

            var canonicalXml = ParseDatLines(lines);
            AssertIsCorrespondenceAttributeHasValue(canonicalXml, "GhostDocument", "1");
        }
        [TestMethod]
        public void GhostStub_WithAmount_HasIsCorrespondenceUnset()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE",
                documentType: SetupDocType.NonCheck,
                extraInputLines: new[]
                {
                    "Amount:0.00",
                });

            var canonicalXml = ParseDatLines(lines);
            AssertIsCorrespondenceAttributeHasValue(canonicalXml, "GhostDocument", "0");
        }
        [TestMethod]
        public void GhostStub_WithAppliedAmount_HasIsCorrespondenceUnset()
        {
            var lines = CreateMinimalDatLines(WorkgroupId, "MYDOCTYPE",
                documentType: SetupDocType.NonCheck,
                extraInputLines: new[]
                {
                    "Applied Amount:0.00",
                });

            var canonicalXml = ParseDatLines(lines);
            AssertIsCorrespondenceAttributeHasValue(canonicalXml, "GhostDocument", "0");
        }
    }
}