﻿using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.integraPAY.Online.DITICONXClient;
using WFS.LTA.DataImport.DataImportIconClient;
using WFS.LTA.DataImport.DataImportXClientBase;

namespace DITICONClientTest
{
    [TestClass()]
    public class cClientXMLNodeTests
    {
       [TestMethod()]
        public void BuildClientXMLTest()
        {
            var expected = "ClientGroups";
            var config = new cConfigData(1, 1);
            var cClient = new cClientXMLNode(new cICONBll(new ditICONXClient()), new ditICONXClient(), config);
            var xmlElement = cClient.BuildClientXML(new XmlDocument(), "PCode");

            Assert.AreEqual(xmlElement.Name, expected);
        }

        [TestMethod()]
        public void BuildBankNodeTest()
        {
            var expected = "Bank";
            var config = new cConfigData(1, 1);
            var cClient = new cClientXMLNode(new cICONBll(new ditICONXClient()), new ditICONXClient(), config);
            var xmlElement = cClient.BuildBankNode(new XmlDocument(), 1);

            Assert.AreEqual(xmlElement.Name, expected);
        }

        [TestMethod()]
        public void BuildClientGroupNodeTest()
        {
            var expected = "ClientGroup";
            var config = new cConfigData(1, 1);
            var cClient = new cClientXMLNode(new cICONBll(new ditICONXClient()), new ditICONXClient(), config);
            var xmlElement = cClient.BuildClientGroupNode(new XmlDocument(), 1);

            Assert.AreEqual(xmlElement.Name, expected);
        }

        [TestMethod()]
        public void BuildClientNodeTest()
        {
            var expected = "Client";
            var config = new cConfigData(1, 1);
            var client = new cClientInfo {
                ClientID = 1, ShortName = "Client", SiteCode = 100
            };
            var cClient = new cClientXMLNode(new cICONBll(new ditICONXClient()), new ditICONXClient(), config);
            var xmlElement = cClient.BuildClientNode(new XmlDocument(), client);

            Assert.AreEqual(xmlElement.Name, expected);
        }

        [TestMethod()]
        public void BuildImageRPSAliasTest()
        {
            var expected = "ImageRPSAliasMappings";
            var config = new cConfigData(1, 1);
            var clientSetup = new ClientSetupInfo() {
                BankId = 1, 
                DataType = "string",
                DocType = "string",
                ExtractType = "string",
                FieldLength = "100",
                FieldType = "string",
                KeyType = "string",
                WorkGroupId  = 1,
                BatchSource = "ImageRPS"
            };
            var cClient = new cClientXMLNode(new cICONBll(new ditICONXClient()), new ditICONXClient(), config);
            var xmlElement = cClient.BuildImageRPSAlias(clientSetup, new XmlDocument());

            Assert.AreEqual(xmlElement.Name, expected);
        }

        [TestMethod()]
        public void BuildImageRPSAliasTest1()
        {
            var expected = "ImageRPSAliasMappings";
            var config = new cConfigData(1, 1);
            var cClient = new cClientXMLNode(new cICONBll(new ditICONXClient()), new ditICONXClient(), config);
            var xmlElement = cClient.BuildImageRPSAlias("skeytype", "sDocType", "sExtractType", "sFieldType", "100",  "sDataType", "ImageRPS", new XmlDocument());

            Assert.AreEqual(xmlElement.Name, expected);
        }

        [TestMethod()]
        public void BuildImageRPSAliasTestWithBatchSource()
        {
            var expected = "<ImageRPSAliasMappings ExtractType=\"string\" DocType=\"string\" AliasName=\"string\" FieldType=\"string\" DataType=\"1\" FieldLength=\"100\" BatchSource=\"ImageRPS\" />";
            var config = new cConfigData(1, 1);
            var clientSetup = new ClientSetupInfo()
            {
                BankId = 1,
                DataType = "string",
                DocType = "string",
                ExtractType = "string",
                FieldLength = "100",
                FieldType = "string",
                KeyType = "string",
                WorkGroupId = 1,
                BatchSource = "ImageRPS"
            };
            var cClient = new cClientXMLNode(new cICONBll(new ditICONXClient()), new ditICONXClient(), config);
            var xmlElement = cClient.BuildImageRPSAlias(clientSetup, new XmlDocument());

            Assert.AreEqual(xmlElement.OuterXml, expected);
        }
    }
}