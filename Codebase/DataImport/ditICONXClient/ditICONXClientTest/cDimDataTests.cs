﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportIconClient;

namespace DITICONClientTest
{
    [TestClass()]
    public class cDimDataTests
    {
        [TestMethod()]
        public void cDimDataTest()
        {
            var sBatchFields = new string[] {"one", "two"};
            var sItemFields = new string[] {"one", "two"};
            var keyVal = new KeyValuePair<string, string>[] {
                  new KeyValuePair<string, string>("key1", "val1"),
                  new KeyValuePair<string, string>("key2", "val2")
            };
            var hashT = new Hashtable() {
                {"key1", "val1"},
                {"key2", "val2"}
            };
            var data = new cDimData(sBatchFields, sItemFields, keyVal, hashT);

            Assert.IsTrue(data.BatchFieldList.Count == 2);
            Assert.AreEqual(data.BatchFieldList[0], "one" );
            Assert.IsTrue(data.ItemFieldList.Count == 2);
            Assert.AreEqual(data.ItemFieldList[0], "one");
            Assert.IsTrue(data.DocumentTypes.Count == 2);
            Assert.AreEqual(data.DocumentTypes["key1"], "val1");
            Assert.IsTrue(data.ImageRPSAliasList.Count == 2);
            Assert.AreEqual(data.ImageRPSAliasList["key1"], "val1");
        }
    }
}
