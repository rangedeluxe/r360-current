﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using WFS.integraPAY.Online.DITICONXClient;
using WFS.LTA.DataImport.DataImportIconClient;

namespace DITICONClientTest
{
    /// <summary>
    /// Logic of PBI 242421:
    ///   Currently the item is only a "Check" if the DocTypeName contains "Per/Bus Check"
    ///   Alter to:
    ///     If "Document Type" is "Check", then it's a check.
    ///     If no "Document Type" is found, then if "DocTypeName" contains "Check", then it's a check.
    ///
    ///   So we have 12 tests. See the TestData/DocumentTypes folder.
    ///
    ///   Test case names are "XClient_ShouldFind[Check or Stub]_[DocTypeName][Document Type Value]"
    ///
    ///   Asserts check to make sure we either have populated checks or stubs.
    /// </summary>
    [TestClass]
    public class XClientDocumentTypeTests : DITICONXClientTest
    {
        private void TestBatchFile(string filename, bool expectedcheck)
        {
            // Arrange
            var xclient = new ditICONXClient();
            var settings = new cICONConfigData(1, 1, @"C:\temp", "PaymentSource");
            using (var stream = TestData.GetStreamReader(filename))
            {
                int[] clients;
                string error;
                xclient.GetClientIDs(stream, out clients, out error);

                List<string> batchfields;
                List<string> itemfields;
                List<KeyValuePair<string, string>> doctypes;
                Hashtable mappings;
                PopulateDataTypes(settings.DefaultBankID, clients, out batchfields, out itemfields, out doctypes, out mappings);

                var dimdata = new cDimData(batchfields.ToArray(), itemfields.ToArray(), doctypes.ToArray(), mappings);
                stream.DiscardBufferedData();
                stream.BaseStream.Seek(0, SeekOrigin.Begin);

                // Act
                XmlDocument document;
                var success = xclient.StreamToBatchXML(stream, settings, dimdata, "TestBatchClient", out document);

                // Assert
                if (!success || document == null)
                    Assert.Fail();

                // Count up the Payment nodes vs the Document nodes.
                var expected = expectedcheck ? "Payment" : "Document";
                var notexpected = expectedcheck ? "Document" : "Payment";

                var expectedcount = document.SelectNodes("Batches/Batch/Transaction/" + expected)?.Count;
                var notexpectedcount = document.SelectNodes("Batches/Batch/Transaction/" + notexpected)?.Count;

                // Assert we have exactly 1 of the expected type (because only 1 exists in the dat files)
                // and 0 of the unexpected type.
                Assert.AreEqual(1, expectedcount);
                Assert.AreEqual(0, notexpectedcount);
            }
        }


        [TestMethod]
        public void WhenDocumentTypeIsCheck_AndᐳᐳDocTypeNameIsCheck_ShouldFindCheck()
        {
            TestBatchFile("TestData/DocumentTypes/Check-Check.dat", true);
        }

        [TestMethod]
        public void WhenDocumentTypeIsMissing_AndᐳᐳDocTypeNameIsCheck_ShouldFindCheck()
        {
            TestBatchFile("TestData/DocumentTypes/Check-NoDocumentType.dat", true);
        }

        [TestMethod]
        public void WhenDocumentTypeIsSquirrel_AndᐳᐳDocTypeNameIsCheck_ShouldFindStub()
        {
            TestBatchFile("TestData/DocumentTypes/Check-Squirrel.dat", expectedcheck: false);
        }

        [TestMethod]
        public void WhenDocumentTypeIsCheck_AndᐳᐳDocTypeNameIsCheck1_ShouldFindCheck()
        {
            TestBatchFile("TestData/DocumentTypes/Check1-Check.dat", true);
        }

        [TestMethod]
        public void WhenDocumentTypeIsMissing_AndᐳᐳDocTypeNameIsCheck1_ShouldFindCheck()
        {
            TestBatchFile("TestData/DocumentTypes/Check1-NoDocumentType.dat", true);
        }

        [TestMethod]
        public void WhenDocumentTypeIsSquirrel_AndᐳᐳDocTypeNameIsCheck1_ShouldFindStub()
        {
            TestBatchFile("TestData/DocumentTypes/Check1-Squirrel.dat", expectedcheck: false);
        }

        [TestMethod]
        public void WhenDocumentTypeIsCheck_AndᐳᐳDocTypeNameIsPerBusCheck_ShouldFindCheck()
        {
            TestBatchFile("TestData/DocumentTypes/PerBusCheck-Check.dat", true);
        }

        [TestMethod]
        public void WhenDocumentTypeIsMissing_AndᐳᐳDocTypeNameIsPerBusCheck_ShouldFindCheck()
        {
            TestBatchFile("TestData/DocumentTypes/PerBusCheck-NoDocumentType.dat", true);
        }

        [TestMethod]
        public void WhenDocumentTypeIsSquirrel_AndᐳᐳDocTypeNameIsPerBusCheck_ShouldFindStub()
        {
            TestBatchFile("TestData/DocumentTypes/PerBusCheck-Squirrel.dat", expectedcheck: false);
        }

        [TestMethod]
        public void WhenDocumentTypeIsCheck_AndᐳᐳDocTypeNameIsSquirrel_ShouldFindCheck()
        {
            TestBatchFile("TestData/DocumentTypes/Squirrel-Check.dat", true);
        }

        [TestMethod]
        public void WhenDocumentTypeIsMissing_AndᐳᐳDocTypeNameIsSquirrel_ShouldFindStub()
        {
            TestBatchFile("TestData/DocumentTypes/Squirrel-NoDocumentType.dat", false);
        }

        [TestMethod]
        public void WhenDocumentTypeIsSquirrel_AndᐳᐳDocTypeNameIsSquirrel_ShouldFindStub()
        {
            TestBatchFile("TestData/DocumentTypes/Squirrel-Squirrel.dat", false);
        }

    }
}
