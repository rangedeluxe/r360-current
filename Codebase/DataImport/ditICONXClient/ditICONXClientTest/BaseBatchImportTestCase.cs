﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using JetBrains.Annotations;
using WFS.integraPAY.Online.ICONCommon;

namespace DITICONClientTest
{
    public class BaseBatchImportTestCase : BaseIconBllTestCase
    {
        protected void AssertGhostDocumentElementCount(XElement canonicalXml, int expectedElementCount)
        {
            var ghostDocumentElements = canonicalXml.DescendantsAndSelf("GhostDocument").ToList();

            Assert.IsTrue(ghostDocumentElements.FirstOrDefault() != null, "Should contain at least one 'GhostDocument' element.");
            Assert.AreEqual(expectedElementCount, ghostDocumentElements.Count);
        }
        protected void AssertIsCorrespondenceAttributeHasValue(XElement canonicalXml, string elementType,
            string expectedValue)
        {
            var attribute = TryGetIsCorrespondenceAttribute(canonicalXml, elementType);
            Assert.IsNotNull(attribute, "IsCorrespondence attribute");
            Assert.AreEqual(expectedValue, attribute.Value, "Value of IsCorrespondence attribute");
        }
        protected void AssertIsCorrespondenceAttributeIsNotPresent(XElement canonicalXml, string elementType)
        {
            var attribute = TryGetIsCorrespondenceAttribute(canonicalXml, elementType);
            Assert.IsNull(attribute, "IsCorrespondence attribute");
        }
        protected void CheckItemCounts(XElement canonicalXml, int paymentCount = 0,
            int documentCount = 0, int ghostDocumentCount = 0)
        {
            var expected = FormatItemCounts(
                paymentCount: paymentCount,
                documentCount: documentCount,
                ghostDocumentCount: ghostDocumentCount);

            var actual = FormatItemCounts(
                paymentCount: canonicalXml.DescendantsAndSelf("Payment").Count(),
                documentCount: canonicalXml.DescendantsAndSelf("Document").Count(),
                ghostDocumentCount: canonicalXml.DescendantsAndSelf("GhostDocument").Count());

            Assert.AreEqual(expected, actual);
        }
        protected IReadOnlyList<string> CreateMinimalDatLines(int workgroupId, string docTypeName,
            SetupDocType? documentType, IEnumerable<string> extraInputLines = null)
        {
            var lines = new List<string>
            {
                ">>DocTypeName:" + docTypeName,
                "Client ID:" + workgroupId,
                "Transaction Number:1",
                "Sequence Number:2",
                "Batch ID:70182",
                "Batch Number:70182",
                "Site ID:1",
            };
            switch (documentType)
            {
                case SetupDocType.Check:
                    // ImageRPS really does output with no space for Check, and with a space for Stub.
                    lines.Add("Document Type:Check");
                    break;
                case SetupDocType.NonCheck:
                    lines.Add("Document Type: Stub");
                    break;
            }
            if (extraInputLines != null)
                lines.AddRange(extraInputLines);
            lines.Add("END:");
            return lines;
        }
        private string FormatItemCounts(int paymentCount, int documentCount, int ghostDocumentCount)
        {
            return $"Payments:{paymentCount} Documents:{documentCount} GhostDocuments:{ghostDocumentCount}";
        }
        protected XElement ParseDat(TextReader reader)
        {
            XmlDocument canonicalXml;
            CreateIconBll().ReadDATFile(reader, 99, "batch", out canonicalXml);

            AssertNoWarningsOrErrors();
            Assert.IsNotNull(canonicalXml, "Expected ReadDATFile call to succeed but it failed");

            return XElement.Parse(canonicalXml.OuterXml);
        }
        protected XElement ParseDatLines(IEnumerable<string> lines)
        {
            using (var reader = new StringReader(string.Join("\r\n", lines)))
                return ParseDat(reader);
        }
        [CanBeNull]
        private XAttribute TryGetIsCorrespondenceAttribute(XElement canonicalXml, string elementType)
        {
            var elements = canonicalXml.DescendantsAndSelf(elementType).ToList();
            Assert.AreEqual(1, elements.Count, $"Expected to find a single <{elementType}> element");

            var element = elements.Single();
            return element.Attribute("IsCorrespondence");
        }
    }
}
