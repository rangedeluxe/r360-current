using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.integraPAY.Online.DITICONXClient;
using WFS.LTA.DataImport.DataImportIconClient;
using WFS.LTA.DataImport.DataImportXClientBase;

namespace DITICONClientTest
{
    [TestClass]
    public class BaseIconBllTestCase
    {
        protected const int DefaultBankId = 100001;
        protected const int DefaultWorkgroupId = 100002;
        protected const string DefaultResponseDirectory = @"X:\DefaultResponseDirectory";
        protected const string DefaultPaymentSource = "DefaultPaymentSource";

        private List<string> _loggedWarningsAndErrors;

        [TestInitialize]
        public void Setup()
        {
            _loggedWarningsAndErrors = new List<string>();
            BankName = $"BankName{Guid.NewGuid()}";
            XClient = new ditICONXClient();

            // There are several (regrettable) layers of code that catch and log exceptions,
            // and then continue processing as if nothing was wrong. Display those messages
            // to make it at least *possible* to diagnose why a test is behaving strangely.
            XClient.LogEvent += (sender, message, source, messageType, importance) =>
            {
                Console.WriteLine("LOG: " + message);
                if (messageType == LogEventType.Warning || messageType == LogEventType.Error)
                    _loggedWarningsAndErrors.Add(message);
            };

            DimData = new FakeDimData();
        }

        protected string BankName { get; set; }
        protected FakeDimData DimData { get; private set; }
        protected IReadOnlyList<string> LoggedWarningsAndErrors => _loggedWarningsAndErrors;
        private ditICONXClient XClient { get; set; }

        protected void AssertNoWarningsOrErrors()
        {
            if (LoggedWarningsAndErrors.Any())
            {
                var messageLines = new[] {"Warnings and/or errors were encountered during parsing:"}
                    .Concat(LoggedWarningsAndErrors);
                Assert.Fail(string.Join("\r\n", messageLines));
            }
        }
        protected cICONBll CreateIconBll()
        {
            var configData = new cICONConfigData(
                DefaultBankId, DefaultWorkgroupId, DefaultResponseDirectory, DefaultPaymentSource)
            {
                BankName = BankName,
            };
            return new cICONBll(XClient, DimData, configData);
        }
    }
}