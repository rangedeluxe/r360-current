﻿using System;
using System.Linq;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DITICONClientTest
{
    [TestClass]
    public class ClientSetupImportFileRegressionTests : BaseClientSetupImportTestCase
    {
        private const string TestDataDirectoryName = "TestData";

        // The "Client1002_Madison" file is an older version of ImageRPS client setup
        // that doesn't have "doctype" attributes on the <ItemType> elements.
        private const string MadisonXmlPath =
            TestDataDirectoryName + @"\Client1002_Madison_1_20110930_150850.XML";
        private const string MadisonMinimalXmlPath =
            TestDataDirectoryName + @"\Client1002_Madison_1_20110930_150850.Minimal.xml";
        private const string MadisonExpectedCanonicalXmlPath =
            TestDataDirectoryName + @"\Client1002_Madison_1_20110930_150850.ExpectedCanonical.xml";

        // The "Client4400" file is a newer version of ImageRPS client setup that does
        // have "doctype" attributes on <ItemType> elements. It doesn't include all
        // client-setup features; for example, it doesn't include payment sources.
        private const string Client4400XmlPath =
            TestDataDirectoryName + @"\Client4400_Workgroup 4400_1_20140210_082951.xml";
        private const string Client4400MinimalXmlPath =
            TestDataDirectoryName + @"\Client4400_Workgroup 4400_1_20140210_082951.Minimal.xml";
        private const string Client4400ExpectedCanonicalXmlPath =
            TestDataDirectoryName + @"\Client4400_Workgroup 4400_1_20140210_082951.ExpectedCanonical.xml";

        private void AssertMultiLineValuesAreEqual(string expected, string actual)
        {
            var expectedLines = expected.Split(new[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);
            var actualLines = actual.Split(new[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);

            // Select matching pairs from each list. The length will be that of the shorter input list.
            var pairs = expectedLines.Zip(actualLines, Tuple.Create)
                .Select((pair, index) => new {Index = index, Expected = pair.Item1, Actual = pair.Item2});
            foreach (var pair in pairs)
            {
                Assert.AreEqual(pair.Expected, pair.Actual, $"Index {pair.Index}");
            }

            // If the inputs had different numbers of lines, we've probably failed already.
            // But just in case, assert the line counts too.
            Assert.AreEqual(expectedLines.Length, actualLines.Length, "Line count");
        }
        private void AssertXElementsAreEqual(XElement expected, XElement actual)
        {
            AssertMultiLineValuesAreEqual(expected.ToString(), actual.ToString());
        }
        private static XElement LoadExpectedCanonical(string expectedCanonicalXmlPath)
        {
            return XElement.Parse(TestData.GetText(expectedCanonicalXmlPath));
        }
        private XElement ParseClientSetupFile(string relativePath)
        {
            var xml = XElement.Parse(TestData.GetText(relativePath));
            return ParseClientSetup(xml);
        }
        private XElement ScrubGuids(XElement canonicalXml)
        {
            var sourceTrackingIdAttribute = canonicalXml.Attribute("SourceTrackingID");
            Assert.IsNotNull(sourceTrackingIdAttribute, nameof(sourceTrackingIdAttribute));
            sourceTrackingIdAttribute.Value = "11111111-1111-1111-1111-111111111111";

            var clientTrackingIdAttribute = canonicalXml.Element("ClientGroup")?.Attribute("ClientTrackingID");
            Assert.IsNotNull(clientTrackingIdAttribute, nameof(clientTrackingIdAttribute));
            clientTrackingIdAttribute.Value = "22222222-2222-2222-2222-222222222222";

            return canonicalXml;
        }

        [TestMethod]
        [DeploymentItem(MadisonXmlPath, TestDataDirectoryName)]
        [DeploymentItem(MadisonExpectedCanonicalXmlPath, TestDataDirectoryName)]
        public void Madison_ImportGeneratesExpectedCanonicalXml()
        {
            BankName = "123rd National Bank";

            var actualClientSetup = ScrubGuids(ParseClientSetupFile(MadisonXmlPath));
            var expectedClientSetup = LoadExpectedCanonical(MadisonExpectedCanonicalXmlPath);
            AssertXElementsAreEqual(expectedClientSetup, actualClientSetup);
        }
        [TestMethod]
        [DeploymentItem(MadisonMinimalXmlPath, TestDataDirectoryName)]
        [DeploymentItem(MadisonExpectedCanonicalXmlPath, TestDataDirectoryName)]
        public void Madison_MinimalCanonicalFile_ImportGeneratesExpectedCanonicalXml()
        {
            BankName = "123rd National Bank";

            var actualClientSetup = ScrubGuids(ParseClientSetupFile(MadisonMinimalXmlPath));
            var expectedClientSetup = LoadExpectedCanonical(MadisonExpectedCanonicalXmlPath);
            AssertXElementsAreEqual(expectedClientSetup, actualClientSetup);
        }
        [TestMethod]
        [DeploymentItem(Client4400XmlPath, TestDataDirectoryName)]
        [DeploymentItem(Client4400ExpectedCanonicalXmlPath, TestDataDirectoryName)]
        public void Client4400_ImportGeneratesExpectedCanonicalXml()
        {
            BankName = "123rd National Bank";

            var actualClientSetup = ScrubGuids(ParseClientSetupFile(Client4400XmlPath));
            var expectedClientSetup = LoadExpectedCanonical(Client4400ExpectedCanonicalXmlPath);
            AssertXElementsAreEqual(expectedClientSetup, actualClientSetup);
        }
        [TestMethod]
        [DeploymentItem(Client4400MinimalXmlPath, TestDataDirectoryName)]
        [DeploymentItem(Client4400ExpectedCanonicalXmlPath, TestDataDirectoryName)]
        public void Client4400_MinimalCanonicalFile_ImportGeneratesExpectedCanonicalXml()
        {
            BankName = "123rd National Bank";

            var actualClientSetup = ScrubGuids(ParseClientSetupFile(Client4400MinimalXmlPath));
            var expectedClientSetup = LoadExpectedCanonical(Client4400ExpectedCanonicalXmlPath);
            AssertXElementsAreEqual(expectedClientSetup, actualClientSetup);
        }
    }
}