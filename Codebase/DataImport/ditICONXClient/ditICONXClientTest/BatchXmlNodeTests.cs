﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.integraPAY.Online.DITICONXClient;
using WFS.integraPAY.Online.DITICONXClient.Fakes;
using WFS.integraPAY.Online.DITICONXClient.Items;
using WFS.RecHub.Common;

namespace DITICONClientTest
{
    [TestClass]
    public class BatchXmlNodeTests
    {
        private enum CheckOrStub
        {
            Check,
            Stub,
        }

        private enum ColorMode
        {
            Bitonal = 1,
            Color = 2,
            Grayscale = 4,
        }

        private enum Page
        {
            Front = 0,
            Back = 1,
        }

        // Integer constants
        private const int DefaultBankId = 1111;

        // String constants that are parsed as integers
        private const string DefaultBatchId = "201";
        private const string DefaultBatchNumber = "202";
        private const string DefaultBatchSequence = "203";
        private const string DefaultClientId = "204";
        private const string DefaultSiteId = "205";
        private const string DefaultTransactionId = "206";

        // String constants that are really string constants
        private const string DefaultAba = "MyAba";
        private const string DefaultClientProcessCode = "MyClientProcessCode";
        private const string DefaultDda = "MyDda";
        private const string DefaultPaymentSource = "MyPaymentSource";
        private const string DefaultPaymentType = "MyPaymentType";
        private const string DefaultRemitterName = "MyRemitterName";

        // Constants that won't really be used (or tested) until we write tests for documents
        private const int DefaultBatchSequenceForDocuments = 2222;
        private const string DefaultDocTypeName = "MyDocTypeName";
        private const string DefaultFileDescriptorFormatString = "MyFileDescriptor:{0}";

        // Date constants
        private static readonly DateTime DefaultDepositDate = new DateTime(2000, 1, 1);
        private static readonly DateTime DefaultProcessDate = new DateTime(2000, 1, 2);
        private static readonly DateTime DefaultSystemDate = new DateTime(2000, 1, 3);

        private List<IMSDocument> _imsDocuments;
        private XmlDocument _resultXmlDocument;

        [TestInitialize]
        public void SetUp()
        {
            _imsDocuments = new List<IMSDocument>();
            _resultXmlDocument = null;
        }

        private void AddImsDocument(CheckOrStub checkOrStub, Action<IMSDocument> extraSetup = null,
            string stubAmount = null)
        {
            //TODO: Add tests for DocumentType

            var imsDocument = new IMSDocument
            {
                ABA = DefaultAba,
                BatchID = DefaultBatchId,
                BatchNumber = DefaultBatchNumber,
                BatchSequence = DefaultBatchSequence,
                BatchSequenceForDocuments = DefaultBatchSequenceForDocuments,
                ClientID = DefaultClientId,
                DDA = DefaultDda,
                DepositDate = DefaultDepositDate,
                DocTypeName = DefaultDocTypeName,
                IsCheck = checkOrStub == CheckOrStub.Check,
                IsStub = checkOrStub == CheckOrStub.Stub,
                // ListBatchData will be populated by individual tests that need it
                // ListItemData will be populated by individual tests that need it
                // listKeywords will be populated by individual tests that need it
                // MICRFields will be populated by individual tests that need it
                MICRFields =
                {
                    StubAmount = stubAmount ?? "",
                },
                PaymentSource = DefaultPaymentSource,
                PaymentType = DefaultPaymentType,
                ProcessDate = DefaultProcessDate,
                RemitterName = DefaultRemitterName,
                SiteID = DefaultSiteId,
                SystemDate = DefaultSystemDate,
                TransactionID = DefaultTransactionId,
                // Not used by the ICON XClient: DocDate, Pages, listPages
            };

            extraSetup?.Invoke(imsDocument);

            _imsDocuments.Add(imsDocument);
        }
        private void AddItemData(string keyword, string value)
        {
            const int notUsedByIconXClient = -1;
            var itemData = new cItemData(notUsedByIconXClient, keyword, notUsedByIconXClient, value);
            _imsDocuments.Last().ListItemData.Add(itemData);
        }
        private void AssertThrows<TException>(Action action, string expectedMessage)
        {
            try
            {
                action();
                Assert.Fail($"Expected an exception of type {typeof(TException).Name} to be thrown");
            }
            catch (Exception ex) when (ex.GetType() == typeof(TException))
            {
                Assert.AreEqual(expectedMessage, ex.Message);
            }
        }
        private string BatchDataRecordElement()
        {
            //TODO: Write tests that put something inside BatchDataRecord
            return "<BatchDataRecord />";
        }
        private string BatchesAndBatchElements(string content)
        {
            // The ICON XClient never sets BatchCueID, so it will always be 0.
            return
                "<Batches SourceTrackingID=\"ffffffff-ffff-ffff-ffff-ffffffffffff\"" +
                " ClientProcessCode=\"MyClientProcessCode\" XSDVersion=\"2.02.07.00\">" +
                "<Batch ProcessingDate=\"2000-01-02\" DepositDate=\"2000-01-01\"" +
                " BatchDate=\"2000-01-03\" BankID=\"1111\" ClientID=\"204\" BatchID=\"201\"" +
                " BatchCueID=\"0\" BatchNumber=\"202\" BatchSiteCode=\"205\"" +
                " BatchSource=\"MyPaymentSource\" PaymentType=\"MyPaymentType\"" +
                " BatchTrackingID=\"00000000-0000-0000-0000-000000000001\">" +
                content +
                "</Batch>" +
                "</Batches>";
        }
        private void CheckResultXml(string expected)
        {
            Assert.IsNotNull(_resultXmlDocument, "Execute must be called before CheckResultXml");
            var actual = _resultXmlDocument.OuterXml;
            Assert.AreEqual(expected, actual);
        }
        private static StubIFileDescriptorSource CreateStubFileDescriptorSource()
        {
            var stubFileDescriptorSource = new StubIFileDescriptorSource();
            stubFileDescriptorSource.GetFileDescriptorString +=
                docTypeName => string.Format(DefaultFileDescriptorFormatString, docTypeName);
            return stubFileDescriptorSource;
        }
        private static StubIGuidSource CreateStubGuidSource()
        {
            var stubGuidSource = new StubIGuidSource();
            stubGuidSource.CreateBatchSourceTrackingId += () => new Guid(new byte[]
            {
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            });
            var nextBatchTrackingId = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 };
            stubGuidSource.CreateBatchTrackingId += () =>
            {
                var guid = new Guid(nextBatchTrackingId);
                nextBatchTrackingId[15]++;
                return guid;
            };
            return stubGuidSource;
        }
        private static StubIIconLogTarget CreateStubLogTarget()
        {
            return new StubIIconLogTarget();
        }
        private string DocumentElement(string content, bool? isCorrespondence = null)
        {
            return
                "<Document BatchSequence=\"203\" DocumentDescriptor=\"MyFileDescriptor:MyDocTypeName\"" +
                " SequenceWithinTransaction=\"1\" DocumentSequence=\"1\"" +
                GetIsCorrespondenceAttribute(isCorrespondence) + ">" +
                content +
                "</Document>";
        }
        private void Execute()
        {
            var batchXmlNode = new cBatchXMLNode(
                CreateStubFileDescriptorSource(), CreateStubGuidSource(), CreateStubLogTarget());
            _resultXmlDocument = batchXmlNode.BuildBatchXmlInternal(DefaultBankId, _imsDocuments, DefaultClientProcessCode);
            Assert.IsNotNull(_resultXmlDocument, "BuildBatchXmlInternal return value");
        }
        private static string GetIsCorrespondenceAttribute(bool? isCorrespondence)
        {
            return isCorrespondence.HasValue
                ? " IsCorrespondence=\"" + (isCorrespondence.Value ? "1" : "0") + "\""
                : "";
        }
        private string GhostDocumentElement(string content, bool? isCorrespondence = null)
        {
            return
                "<GhostDocument" + GetIsCorrespondenceAttribute(isCorrespondence) + ">" +
                content +
                "</GhostDocument>";
        }
        private string ImagesElement(string content)
        {
            return "<Images>" + content + "</Images>";
        }
        private string ImgElement(string clientFilePath, ColorMode colorMode, Page page, string allowMultiPageTiff)
        {
            return
                $"<img ClientFilePath=\"{clientFilePath}\" Page=\"{(int) page}\" " +
                $"ColorMode=\"{(int) colorMode}\" AllowMultiPageTiff=\"{allowMultiPageTiff}\" />";
        }
        private string ItemDataElement(string fieldName, string fieldValue)
        {
            return string.Format("<ItemData FieldName=\"{0}\" FieldValue=\"{1}\" />", fieldName, fieldValue);
        }
        private string ItemDataRecordElement(string content = "")
        {
            const string tag = "ItemDataRecord";
            const string attributes = "BatchSequence=\"203\"";
            return string.IsNullOrEmpty(content)
                ? string.Format("<{0} {1} />", tag, attributes)
                : string.Format("<{0} {1}>{2}</{0}>", tag, attributes, content);
        }
        private string PaymentElement(string content)
        {
            //TODO: Add a test that fills in values for Amount, Serial, etc.
            return
                "<Payment BatchSequence=\"203\" Amount=\"\" Serial=\"\"" +
                " Account=\"\" RT=\"\" TransactionCode=\"\" DDA=\"MyDda\" ABA=\"MyAba\"" +
                " RemitterName=\"MyRemitterName\">" +
                content +
                "</Payment>";
        }
        private string RemittanceDataElement(string fieldName, string fieldValue)
        {
            return string.Format("<RemittanceData FieldName=\"{0}\" FieldValue=\"{1}\" />", fieldName, fieldValue);
        }
        private string RemittanceDataRecordElement(string content = "")
        {
            const string tag = "RemittanceDataRecord";
            const string attributes = "BatchSequence=\"203\"";
            return string.IsNullOrEmpty(content)
                ? string.Format("<{0} {1} />", tag, attributes)
                : string.Format("<{0} {1}>{2}</{0}>", tag, attributes, content);
        }
        private string TransactionElement(string content)
        {
            return
                "<Transaction TransactionID=\"206\" TransactionSequence=\"206\">" +
                content +
                "</Transaction>";
        }

        //TODO: Add tests that populate IMSDocument.ListBatchData
        //TODO: Add tests that populate IMSDocument.listKeywords
        //TODO: Add tests that populate IMSDocument.MICRFields

        [TestMethod]
        public void CheckWithNoImageKeywords()
        {
            AddImsDocument(CheckOrStub.Check);
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        PaymentElement(
                            ItemDataRecordElement()))));
        }
        [TestMethod]
        public void Check_WithImageFullPath_ButPathIsEmptyString()
        {
            AddImsDocument(CheckOrStub.Check, item => item.AddPotentiallyMultiPageImage(fullPath: ""));
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        PaymentElement(
                            ItemDataRecordElement()))));
        }
        [TestMethod]
        public void Check_WithImageFullPath()
        {
            const string imagePath = @"C:\ImageFullPath";
            AddImsDocument(CheckOrStub.Check, item => item.AddPotentiallyMultiPageImage(fullPath: imagePath));
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        PaymentElement(
                            ItemDataRecordElement() +
                            ImagesElement(
                                ImgElement(imagePath, ColorMode.Bitonal, Page.Front, allowMultiPageTiff: "true"))))));
            //TODO: Add a test with check RemittanceData (via IMSDocument.MICRFields)
        }
        [TestMethod]
        public void Check_WithImageColorFrontPath()
        {
            const string imagePath = @"C:\ColorFrontPath";
            AddImsDocument(CheckOrStub.Check, item =>
                item.AddSinglePageImage(imagePath, WorkgroupColorMode.COLOR_MODE_COLOR, isBack: false));
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        PaymentElement(
                            ItemDataRecordElement() +
                            ImagesElement(
                                ImgElement(imagePath, ColorMode.Color, Page.Front, allowMultiPageTiff: "false"))))));
        }
        [TestMethod]
        public void Check_WithImageGrayBackPath()
        {
            const string imagePath = @"C:\GrayBackPath";
            AddImsDocument(CheckOrStub.Check, item =>
                item.AddSinglePageImage(imagePath, WorkgroupColorMode.COLOR_MODE_GRAYSCALE, isBack: true));
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        PaymentElement(
                            ItemDataRecordElement() +
                            ImagesElement(
                                ImgElement(imagePath, ColorMode.Grayscale, Page.Back, allowMultiPageTiff: "false"))))));
        }
        [TestMethod]
        public void CheckWithItemData()
        {
            const string imagePath = @"C:\ImageFullPath";
            AddImsDocument(CheckOrStub.Check, item => item.AddPotentiallyMultiPageImage(fullPath: imagePath));
            AddItemData("Keyword1", "Value1");
            AddItemData("Keyword2", "Value2");
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        PaymentElement(
                            ItemDataRecordElement(
                                ItemDataElement("Keyword1", "Value1") +
                                ItemDataElement("Keyword2", "Value2")) +
                            ImagesElement(
                                ImgElement(imagePath, ColorMode.Bitonal, Page.Front, allowMultiPageTiff: "true"))))));
        }
        [TestMethod]
        public void StubWithImagePath_WrittenAsDocument_IsCorrespondence()
        {
            var imagePath = @"C:\ImageFullPath";
            AddImsDocument(CheckOrStub.Stub, item => item.AddPotentiallyMultiPageImage(fullPath: imagePath),
                stubAmount: "");
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        DocumentElement(
                            isCorrespondence: true, content:
                            ImagesElement(
                                ImgElement(imagePath, ColorMode.Bitonal, Page.Front, allowMultiPageTiff: "true")) +
                            RemittanceDataRecordElement(
                                RemittanceDataElement("Amount", "")) +
                            ItemDataRecordElement()))));
        }
        [TestMethod]
        public void StubWithImagePath_WrittenAsDocument_NotCorrespondence()
        {
            const string imagePath = @"C:\ImageFullPath";
            AddImsDocument(CheckOrStub.Stub, item => item.AddPotentiallyMultiPageImage(imagePath),
                stubAmount: "123");
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        DocumentElement(
                            isCorrespondence: false, content:
                            ImagesElement(
                                ImgElement(imagePath, ColorMode.Bitonal, Page.Front, allowMultiPageTiff: "true")) +
                            RemittanceDataRecordElement(
                                RemittanceDataElement("Amount", "123")) +
                            ItemDataRecordElement()))));
        }
        [TestMethod]
        public void StubWithNoImagePath_WrittenAsGhostDocument_IsCorrespondence()
        {
            AddImsDocument(CheckOrStub.Stub, stubAmount: "");
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        GhostDocumentElement(
                            isCorrespondence: true, content:
                            RemittanceDataRecordElement() +
                            ItemDataRecordElement()))));
        }
        [TestMethod]
        public void StubWithNoImagePath_WrittenAsGhostDocument_NotCorrespondence()
        {
            AddImsDocument(CheckOrStub.Stub, stubAmount: "123");
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        GhostDocumentElement(
                            isCorrespondence: false, content:
                            RemittanceDataRecordElement() +
                            ItemDataRecordElement()))));
        }
        [TestMethod]
        public void GhostDocumentWithItemData_IsCorrespondence()
        {
            AddImsDocument(CheckOrStub.Stub, stubAmount: "");
            AddItemData("Keyword1", "Value1");
            AddItemData("Keyword2", "Value2");
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        GhostDocumentElement(
                            isCorrespondence: true, content:
                            RemittanceDataRecordElement() +
                            ItemDataRecordElement(
                                ItemDataElement("Keyword1", "Value1") +
                                ItemDataElement("Keyword2", "Value2"))))));
        }
        [TestMethod]
        public void GhostDocumentWithItemData_NotCorrespondence()
        {
            AddImsDocument(CheckOrStub.Stub, stubAmount: "123");
            AddItemData("Keyword1", "Value1");
            AddItemData("Keyword2", "Value2");
            Execute();
            CheckResultXml(
                BatchesAndBatchElements(
                    BatchDataRecordElement() +
                    TransactionElement(
                        GhostDocumentElement(
                            isCorrespondence: false, content:
                            RemittanceDataRecordElement() +
                            ItemDataRecordElement(
                                ItemDataElement("Keyword1", "Value1") +
                                ItemDataElement("Keyword2", "Value2"))))));
        }
        [TestMethod]
        public void WithNonNumericClientId_ShouldThrow()
        {
            AddImsDocument(CheckOrStub.Check, item => item.ClientID = "");
            // Exact exception message isn't contractual - just shows that we're failing for the right reason
            AssertThrows<Exception>(Execute, "Failed to parse ClientID");
        }
        [TestMethod]
        public void WithNonNumericBatchId_ShouldThrow()
        {
            AddImsDocument(CheckOrStub.Check, item => item.BatchID = "");
            // Exact exception message isn't contractual - just shows that we're failing for the right reason
            AssertThrows<Exception>(Execute, "Failed to parse batchID");
        }
        [TestMethod]
        public void WithNonNumericBatchNumber_ShouldThrow()
        {
            AddImsDocument(CheckOrStub.Check, item => item.BatchNumber = "");
            // Exact exception message isn't contractual - just shows that we're failing for the right reason
            AssertThrows<Exception>(Execute, "Failed to parse BatchNumber");
        }
        [TestMethod]
        public void WithNonNumericSiteId_ShouldThrow()
        {
            AddImsDocument(CheckOrStub.Check, item => item.SiteID = "");
            // Exact exception message isn't contractual - just shows that we're failing for the right reason
            AssertThrows<Exception>(Execute, "Failed to parse SiteID");
        }
    }
}