using System.IO;
using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DITICONClientTest
{
    /// <summary>
    /// Tests based on entire files (that have been added to the test project).
    /// </summary>
    [TestClass]
    public class BatchImportFileRegressionTests : BaseBatchImportTestCase
    {
        private XElement ParseDatFile(string fileName)
        {
            using (var reader = TestData.GetStreamReader(fileName))
                return ParseDat(reader);
        }

        [TestMethod]
        public void BatchNode_ProcessCheckAndListFile_ShouldContainGhostDocumentElements()
        {
            //arrange
            const string fileName = "TestData/Check & List.dat";
            const int expectedGhostDocumentElements = 2;

            //act
            var canonicalXml = ParseDatFile(fileName);

            //assert
            AssertGhostDocumentElementCount(canonicalXml, expectedGhostDocumentElements);
        }

        [TestMethod]
        public void BatchNode_ProcessStubsOnlyFile_ShouldContainGhostDocumentElements()
        {
            //arrange
            const string fileName = "TestData/Stubs Only.dat";
            const int expectedGhostDocumentElements = 2;

            //act
            var canonicalXml = ParseDatFile(fileName);

            //assert
            AssertGhostDocumentElementCount(canonicalXml, expectedGhostDocumentElements);
        }

        [TestMethod]
        public void BatchNode_ProcessSinglesFile_ShouldContainGhostDocumentElements()
        {
            //arrange
            const string fileName = "TestData/Singles.dat";
            const int expectedGhostDocumentElements = 2;

            //act
            var canonicalXml = ParseDatFile(fileName);

            //assert
            AssertGhostDocumentElementCount(canonicalXml, expectedGhostDocumentElements);
        }

        [TestMethod]
        public void BatchNode_ProcessMultisFile_ShouldContainGhostDocumentElements()
        {
            //arrange
            const string fileName = "TestData/Multis.dat";
            const int expectedGhostDocumentElements = 1;

            //act
            var canonicalXml = ParseDatFile(fileName);

            //assert
            AssertGhostDocumentElementCount(canonicalXml, expectedGhostDocumentElements);
        }

        [TestMethod]
        public void BatchNode_ProcessChecksOnlyFile_ShouldContainGhostDocumentElements()
        {
            //arrange
            const string fileName = "TestData/Checks Only.dat";
            const int expectedGhostDocumentElements = 3;

            //act
            var canonicalXml = ParseDatFile(fileName);

            //assert
            AssertGhostDocumentElementCount(canonicalXml, expectedGhostDocumentElements);
        }
    }
}