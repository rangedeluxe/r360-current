﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.integraPAY.Online.DITICONXClient.Items;
using WFS.integraPAY.Online.ICONCommon;
using WFS.RecHub.Common;

namespace DITICONClientTest
{
    /// <summary>
    /// Summary description for IMSDocumentTests
    /// </summary>
    [TestClass]
    public class IMSDocumentTests
    {
        private void AssertNoImages(IMSDocument document)
        {
            Assert.AreEqual(0, document.Images.Count, "Images.Count");
        }
        private void AssertOneImage(IMSDocument document, string expectedPath, WorkgroupColorMode expectedColorMode,
            bool expectedIsBack, bool expectedAllowMultiPageTiff)
        {
            Assert.AreEqual(1, document.Images.Count, "Images.Count");
            var image = document.Images[0];
            Assert.AreEqual(expectedPath, image.Path, "Path");
            Assert.AreEqual(expectedColorMode, image.ColorMode, "ColorMode");
            Assert.AreEqual(expectedIsBack, image.IsBack, "IsBack");
            Assert.AreEqual(expectedAllowMultiPageTiff, image.AllowMultiPageTiff, "AllowMultiPageTiff");
        }

        [TestMethod]
        public void IncludePairAsSetupFieldShouldReturnTrueForDocumentTypeAsKey()
        {
            var keywordPair = new IMSKeywordPair("Document Type", "Testing Doc");

            Assert.IsTrue(IMSDocument.IncludePairAsSetupField(keywordPair));
        }

        [TestMethod]
        public void IncludePairAsSetupFieldShouldReturnFalseForEndpointABAAsKey()
        {
            var keywordPair = new IMSKeywordPair("Endpoint ABA", "1123581321");

            Assert.IsFalse(IMSDocument.IncludePairAsSetupField(keywordPair));
        }

        [TestMethod]
        public void AssignValuesShouldPopulateDocumentTypeWhenKeyIsDocumentType()
        {
            var imsDocument = new IMSDocument();
            var docTypeValue = "Testing Doc";
            var keywordPair = new IMSKeywordPair("Document Type", docTypeValue);

            imsDocument.AssignValues(keywordPair);
            Assert.AreEqual(imsDocument.DocumentType, docTypeValue);
        }

        [TestMethod]
        public void ImageFullPath()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AAFullPath, @"C:\ImageFullPath"));
            AssertOneImage(imsDocument, @"C:\ImageFullPath", WorkgroupColorMode.COLOR_MODE_BITONAL,
                expectedIsBack: false, expectedAllowMultiPageTiff: true);
        }
        [TestMethod]
        public void ImageFullPath_WhenBlank_DoesNotAddImage()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AAFullPath, ""));
            AssertNoImages(imsDocument);
        }
        [TestMethod]
        public void ColorFrontPath()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AAColorFrontPath, @"C:\ColorFrontPath"));
            AssertOneImage(imsDocument, @"C:\ColorFrontPath", WorkgroupColorMode.COLOR_MODE_COLOR,
                expectedIsBack: false, expectedAllowMultiPageTiff: false);
        }
        [TestMethod]
        public void ColorFrontPath_WhenBlank_DoesNotAddImage()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AAColorFrontPath, ""));
            AssertNoImages(imsDocument);
        }
        [TestMethod]
        public void GrayFrontPath()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AAGrayFrontPath, @"C:\GrayFrontPath"));
            AssertOneImage(imsDocument, @"C:\GrayFrontPath", WorkgroupColorMode.COLOR_MODE_GRAYSCALE,
                expectedIsBack: false, expectedAllowMultiPageTiff: false);
        }
        [TestMethod]
        public void BitonalFrontPath()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AABitonalFrontPath, @"C:\BitonalFrontPath"));
            AssertOneImage(imsDocument, @"C:\BitonalFrontPath", WorkgroupColorMode.COLOR_MODE_BITONAL,
                expectedIsBack: false, expectedAllowMultiPageTiff: false);
        }
        [TestMethod]
        public void ColorBackPath()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AAColorBackPath, @"C:\ColorBackPath"));
            AssertOneImage(imsDocument, @"C:\ColorBackPath", WorkgroupColorMode.COLOR_MODE_COLOR,
                expectedIsBack: true, expectedAllowMultiPageTiff: false);
        }
        [TestMethod]
        public void GrayBackPath()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AAGrayBackPath, @"C:\GrayBackPath"));
            AssertOneImage(imsDocument, @"C:\GrayBackPath", WorkgroupColorMode.COLOR_MODE_GRAYSCALE,
                expectedIsBack: true, expectedAllowMultiPageTiff: false);
        }
        [TestMethod]
        public void BitonalBackPath()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AABitonalBackPath, @"C:\BitonalBackPath"));
            AssertOneImage(imsDocument, @"C:\BitonalBackPath", WorkgroupColorMode.COLOR_MODE_BITONAL,
                expectedIsBack: true, expectedAllowMultiPageTiff: false);
        }
        [TestMethod]
        public void MultipleImages()
        {
            var imsDocument = new IMSDocument();
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AABitonalFrontPath, @"C:\BitonalFrontPath"));
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AAGrayFrontPath, @"C:\GrayFrontPath"));
            imsDocument.AssignValues(new IMSKeywordPair(cMagicKeywords.AAColorBackPath, @"C:\ColorBackPath"));

            Assert.AreEqual(3, imsDocument.Images.Count, "Images.Count");
            Assert.AreEqual(WorkgroupColorMode.COLOR_MODE_BITONAL, imsDocument.Images[0].ColorMode, "[0].ColorMode");
            Assert.AreEqual(WorkgroupColorMode.COLOR_MODE_GRAYSCALE, imsDocument.Images[1].ColorMode, "[1].ColorMode");
            Assert.AreEqual(WorkgroupColorMode.COLOR_MODE_COLOR, imsDocument.Images[2].ColorMode, "[2].ColorMode");
            Assert.AreEqual(false, imsDocument.Images[0].IsBack, "[0].IsBack");
            Assert.AreEqual(false, imsDocument.Images[1].IsBack, "[1].IsBack");
            Assert.AreEqual(true, imsDocument.Images[2].IsBack, "[2].IsBack");
        }
    }
}
