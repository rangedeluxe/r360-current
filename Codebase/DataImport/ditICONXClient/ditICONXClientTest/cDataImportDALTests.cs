﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.DAL;

namespace DITICONClientTest
{
    [TestClass()]
    public class cDataImportDALTests
    {
        [TestMethod()]
        public void cDataImportDALTest()
        {
            var dal = new cDataImportDAL("IPOnline");
            Assert.AreEqual(dal.SiteKey, "IPOnline");
        }
    }
}
