﻿using WFS.integraPAY.Online.DITICONXClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using WFS.LTA.DataImport.DataImportIconClient;
using WFS.RecHub.DAL;
using System.Data;
using System.Collections;

namespace DITICONClientTest
{
    /// <summary>
    ///This is a test class for DITICONXClientTest and is intended
    ///to contain all DITICONXClientTest Unit Tests
    ///</summary>
    [TestClass]
    public class DITICONXClientTest
    {
        private string TestDataPath(string fileName)
        {
            return Path.Combine("TestData", fileName);
        }
        private void LoadBatchFile(string filename)
        {
            bool expected = true;
            string sErrorMessage = string.Empty;
            string sResponseDir = "c:\\work1\\";
            int defaultBankID = 1;
            int defaultClientID = 14;
            string strClientProcessCode = "BatchTestCode";
            cICONConfigData cfgSettings = new cICONConfigData(defaultBankID, defaultClientID, sResponseDir, "ImageRPS");
            ditICONXClient target = new ditICONXClient();
            List<string> lstBatchFields = new List<string>();
            List<string> lstItemFields = new List<string>();
            List<KeyValuePair<string, string>> akvpDocTypes = new List<KeyValuePair<string, string>>();
            Hashtable hshImageRPSAliasMapping = new Hashtable();
            using (StreamReader stmInputFile = TestData.GetStreamReader(filename))
            {
                XmlDocument xmdResult = null;
                bool actual;

                int[] aiClientIDs;
                target.GetClientIDs(stmInputFile, out aiClientIDs, out sErrorMessage);
                PopulateDataTypes(defaultBankID, aiClientIDs, out lstBatchFields, out lstItemFields, out akvpDocTypes, out hshImageRPSAliasMapping);
                cDimData ddaDimData = new cDimData(lstBatchFields.ToArray(), lstItemFields.ToArray(), akvpDocTypes.ToArray(), hshImageRPSAliasMapping);
                //reset the stream buffer to beginning so we can start over
                stmInputFile.DiscardBufferedData();
                stmInputFile.BaseStream.Seek(0, SeekOrigin.Begin);
                actual = target.StreamToBatchXML(stmInputFile, cfgSettings, ddaDimData, strClientProcessCode, out xmdResult);

                Assert.AreEqual(expected, actual);
            }
        }
        private void UpsertHashTable(Hashtable hshBase, object objKey, object objValue)
        {
            UpsertHashTable(hshBase, new DictionaryEntry(objKey, objValue));
        }
        private void UpsertHashTable(Hashtable hshBase, DictionaryEntry dceUpdate)
        {
            if (hshBase.ContainsKey(dceUpdate.Key))
                hshBase[dceUpdate.Key] = dceUpdate.Value;
            else
                hshBase.Add(dceUpdate.Key, dceUpdate.Value);
        }
        protected void PopulateDataTypes(int siteBankID, int[] aiRequiredClientIDs, out List<string> lstBatchFields,
            out List<string> lstItemFields, out List<KeyValuePair<string, string>> akvpDocTypes, out Hashtable hshImageRPSAliasMapping)
        {
            string sBatchSourceKeyName = "ICON";
            string sResponseDirectory = string.Empty;

            lstBatchFields = new List<string>();
            lstItemFields = new List<string>();
            akvpDocTypes = new List<KeyValuePair<string, string>>();
            hshImageRPSAliasMapping = new Hashtable();

            foreach (int iCurClientID in aiRequiredClientIDs)
            {
                if (!hshImageRPSAliasMapping.ContainsKey(iCurClientID))
                    hshImageRPSAliasMapping.Add(iCurClientID, new Hashtable());

                using (cDataImportDAL dataImportDAL = new cDataImportDAL("IPOnline"))
                {
                    DataTable dtGetDocumentTypes = null;
                    DataTable dtBatchData = null;
                    DataTable dtItemData = null;
                    DataTable dtGetImageRPSAliasDT = null;
                    dataImportDAL.DataImportRequestDocumentTypes(null, out dtGetDocumentTypes);
                    if (dtGetDocumentTypes != null)
                    {
                        foreach (DataRow dtDocumentRow in dtGetDocumentTypes.Rows)
                        {
                            string fileDescriptor = dtDocumentRow["FileDescriptor"].ToString();
                            string documentType = dtDocumentRow["IMSDocumentType"].ToString();
                            KeyValuePair<string, string> doc = new KeyValuePair<string, string>(fileDescriptor, documentType);

                            akvpDocTypes.Add(doc);
                        }
                    }
                    dataImportDAL.DataImportGetImageRPSAliasMappings(siteBankID, iCurClientID, null, out dtGetImageRPSAliasDT);
                    if (dtGetImageRPSAliasDT != null)
                    {
                        foreach (DataRow rowCurRow in dtGetImageRPSAliasDT.Rows)
                        {
                            UpsertHashTable((Hashtable) hshImageRPSAliasMapping[iCurClientID],
                                string.Format("{0}|{1}", rowCurRow["AliasName"].ToString(), rowCurRow["DocType"].ToString()),
                                new string[] {rowCurRow["ExtractType"].ToString(), rowCurRow["FieldType"].ToString()});
                        }
                    }
                    using (cBatchDataDAL batchDataDAL = new cBatchDataDAL("IPOnline"))
                    {
                        batchDataDAL.GetBatchDataSetupField(sBatchSourceKeyName, null, out dtBatchData);
                        if (dtBatchData != null)
                        {
                            foreach (DataRow dtBatchDataRow in dtBatchData.Rows)
                            {
                                string keyWord = dtBatchDataRow["Keyword"].ToString();
                                lstBatchFields.Add(keyWord);
                            }
                        }
                        batchDataDAL.GetItemDataSetupField(sBatchSourceKeyName, null, out dtItemData);
                        if (dtItemData != null)
                        {
                            foreach (DataRow dtItemDataRow in dtItemData.Rows)
                            {
                                string keyWord = dtItemDataRow["Keyword"].ToString();
                                lstItemFields.Add(keyWord);
                            }
                        }
                    }
                }
            }
        }
        private void TestClientFile(string filename, string expectedResult)
        {
            int defaultBankID = 1;
            int defaultClientID = 14;

            string sResponseDir = "c:\\work1\\";

            string strClientProcessCode = "ClientTestCode";
            XmlDocument xmdResult = null; // TODO: Initialize to an appropriate value
            cICONConfigData cfgSettings = new cICONConfigData(defaultBankID, defaultClientID, sResponseDir, "ImageRPS");

            List<string> lstBatchFields = new List<string>();
            List<string> lstItemFields = new List<string>();
            List<KeyValuePair<string, string>> akvpDocTypes = new List<KeyValuePair<string, string>>();
            Hashtable hshImageRPSAliasMapping = new Hashtable();
            bool actual;
            ditICONXClient target = new ditICONXClient();
            using (StreamReader stmInputFile = TestData.GetStreamReader(filename))
            {
                cClientInfo cinClientInfo = new cClientInfo();
                string sErrorMessage = string.Empty;
                bool bGetClientInfo = target.GetClientInfo(Path.GetFileName(filename), out cinClientInfo, out sErrorMessage);

                List<int> aiClientList = new List<int>();
                aiClientList.Add(defaultClientID);
                PopulateDataTypes(defaultBankID, aiClientList.ToArray(), out lstBatchFields, out lstItemFields, out akvpDocTypes, out hshImageRPSAliasMapping);
                cDimData ddaDimData = new cDimData(lstBatchFields.ToArray(), lstItemFields.ToArray(), akvpDocTypes.ToArray(), hshImageRPSAliasMapping);

                actual = target.StreamToClientSetupXML(stmInputFile, cinClientInfo, ddaDimData, cfgSettings, strClientProcessCode, out xmdResult);
                if (!actual)
                    Assert.Fail();
                XmlNode clientNode = xmdResult.SelectSingleNode("ClientGroups/ClientGroup");
                Assert.AreEqual(expectedResult, clientNode.InnerXml);
            }
        }

        [TestMethod]
        public void WriteBatchResponseTest()
        {
            ditICONXClient client = new ditICONXClient();
            string outStr = "";
            var configData = new cICONConfigData(1, 1, "test", "ImageRPS");
            var date = DateTime.Now;

            var success = client.WriteBatchResponse("bank1", "lb1", "batch1", date, true, configData, out outStr);
        }
        [TestMethod]
        public void ClientStreamToXMLTest()
        {
            string fileName = TestDataPath("Client22_22_1_20110411_153317.XML");
            string fileName2 = TestDataPath("Client1002_Madison_1_20110930_150850.XML");
            string filename3 = TestDataPath("Client22_Renamed 22_1_20110726_133117.XML");
            string filename4 = TestDataPath("Client20_202020_1_20110331_172345.XML");
            string result4 =
                "<Bank BankID=\"1\" BankName=\"\" />" +
                "<Client ClientID=\"20\" SiteCode=\"1\" ShortName=\"202020\" LongName=\"202020\" DataRetentionDays=\"0\" ImageRetentionDays=\"0\">" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"20 - Per/Bus Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Amount\" FieldType=\"$\" DataType=\"7\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Batch Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"30\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Doc Grp ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Document ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Batch Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Applied OpID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Sequence\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Pocket Cut ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Pocket Seq Num\" FieldType=\"A\" DataType=\"1\" FieldLength=\"3\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Receive Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Deposit Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Deposit Time\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Consolidation Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Consolidation Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P1 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P1 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"128\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Process Type\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"ARC Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"ARC Tracer\" FieldType=\"A\" DataType=\"1\" FieldLength=\"50\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Reject Job\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Reject Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Location ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Location Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Orig Batch ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Orig P1 Seq Num\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Abbrev Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Site ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"ACH Returns Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"250\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"2\" DocType=\"2\" AliasName=\"RT\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"3\" DocType=\"2\" AliasName=\"Account\" FieldType=\"A\" DataType=\"1\" FieldLength=\"16\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"2\" AliasName=\"Per Check Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"20 - Foreign Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Cash Ticket\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Amount\" FieldType=\"$\" DataType=\"7\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Batch Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"30\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Doc Grp ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Document ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Batch Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Applied OpID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Sequence\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Pocket Cut ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Pocket Seq Num\" FieldType=\"A\" DataType=\"1\" FieldLength=\"3\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Receive Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Deposit Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Deposit Time\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Consolidation Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Consolidation Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P1 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P1 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"128\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Process Type\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"ARC Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"ARC Tracer\" FieldType=\"A\" DataType=\"1\" FieldLength=\"50\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Reject Job\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Reject Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Location ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Location Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Orig Batch ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Orig P1 Seq Num\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Abbrev Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Site ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"ACH Returns Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"250\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"2\" DocType=\"1\" AliasName=\"RT\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"3\" DocType=\"1\" AliasName=\"Account Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"16\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"1\" AliasName=\"Per Check Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - WFS Remit Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Reference Page\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - WFS Remit Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Bevent Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Policy Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Line of Business\" FieldType=\"A\" DataType=\"1\" FieldLength=\"2\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Due Date\" FieldType=\"A\" DataType=\"1\" FieldLength=\"6\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Amount Due\" FieldType=\"O\" DataType=\"7\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Change of Address\" FieldType=\"A\" DataType=\"1\" FieldLength=\"1\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Bevent Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - AEM Test Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFLDPolicyTest\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFldAddressTest\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFLDCityTest\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFLDNameTest\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - AEM Test Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Bevent Stub 2\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Bevent Stub 2 Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Bevent Stub 3\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Bevent Stub 3 Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - WFS Remit Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - WFS Remit Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - WFS Remit Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - WFS Remit Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Ghost Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Ghost Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Ghost Stub - Inline IMS\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Ghost Stub - Inline IMS Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - EOB\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - EOB Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - CHP stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Dash Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"20 - NSF Personal Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"20 - NSF Business Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - NSF Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - AEM Test Escort Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - AEM Test Escort Stub2\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Correspondence\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Envelope\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Postnet Barcode\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - 3 of 9 Barcode\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"20 - Planet Barcode\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "</Client>";
            string result3 =
                "<Bank BankID=\"1\" BankName=\"\" />" +
                "<Client ClientID=\"22\" SiteCode=\"1\" ShortName=\"Renamed 22\" LongName=\"Renamed 22\" DataRetentionDays=\"0\" ImageRetentionDays=\"0\">" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"22 - QA Test Business Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Amount\" FieldType=\"D\" DataType=\"7\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Batch Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"30\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Doc Grp ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Document ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Batch Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Applied OpID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Sequence\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Pocket Cut ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Pocket Seq Num\" FieldType=\"A\" DataType=\"1\" FieldLength=\"3\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Receive Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Deposit Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Deposit Time\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Consolidation Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Consolidation Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P1 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P1 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"128\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Process Type\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"ARC Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"ARC Tracer\" FieldType=\"A\" DataType=\"1\" FieldLength=\"50\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Reject Job\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Reject Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Location ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Location Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Orig Batch ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Orig P1 Seq Num\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Abbrev Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Site ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"ACH Returns Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"250\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"2\" DocType=\"2\" AliasName=\"RT\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"3\" DocType=\"2\" AliasName=\"Account\" FieldType=\"A\" DataType=\"1\" FieldLength=\"16\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"2\" AliasName=\"Per Check Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"1\" DocType=\"2\" AliasName=\"Serial\" FieldType=\"\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"2\" AliasName=\"Transaction Code\" FieldType=\"\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Change of Address\" FieldType=\"M\" DataType=\"1\" FieldLength=\"1\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"22 - QA Test Per or Bus Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Cash Ticket\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Amount\" FieldType=\"D\" DataType=\"7\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Batch Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"30\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Doc Grp ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Document ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Batch Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Applied OpID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Sequence\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Pocket Cut ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Pocket Seq Num\" FieldType=\"A\" DataType=\"1\" FieldLength=\"3\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Receive Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Deposit Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Deposit Time\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Consolidation Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Consolidation Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P1 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P1 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"128\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Process Type\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"ARC Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"ARC Tracer\" FieldType=\"A\" DataType=\"1\" FieldLength=\"50\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Reject Job\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Reject Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Location ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Location Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Orig Batch ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Orig P1 Seq Num\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Abbrev Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Site ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"ACH Returns Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"250\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"2\" DocType=\"1\" AliasName=\"RT\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"3\" DocType=\"1\" AliasName=\"Account\" FieldType=\"A\" DataType=\"1\" FieldLength=\"16\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"1\" AliasName=\"Per Check Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Reference Page\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - QA Test Stb\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Policy Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Line of Business\" FieldType=\"A\" DataType=\"1\" FieldLength=\"2\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Due Date\" FieldType=\"A\" DataType=\"1\" FieldLength=\"6\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Amount Due\" FieldType=\"O\" DataType=\"7\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Change of Address\" FieldType=\"M\" DataType=\"1\" FieldLength=\"1\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 1 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 10 Field\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 5 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 6 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 4 Field\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Change Type 9 Field\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 2 Field\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 3 Field\" FieldType=\"O\" DataType=\"7\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFLDPolicyTest\" FieldType=\"M\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFldAddressTest\" FieldType=\"M\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFLDCityTest\" FieldType=\"M\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFLDNameTest\" FieldType=\"M\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - QA Test Stb Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - AEM Test Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - AEM Test Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Bevent Stub 2\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Bevent Stub 2 Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Bevent Stub 3\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Bevent Stub 3 Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Ghost Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Ghost Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Ghost Stub - Inline IMS\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Ghost Stub - Inline IMS Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - EOB\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - EOB Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - CHP stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Dash Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"22 - NSF Personal Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"22 - QA Bus Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"2\" DocType=\"2\" AliasName=\"Rout Num\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"3\" DocType=\"2\" AliasName=\"Acct\" FieldType=\"A\" DataType=\"1\" FieldLength=\"16\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"2\" AliasName=\"Txn Code\" FieldType=\"\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"1\" DocType=\"2\" AliasName=\"Bus Serial\" FieldType=\"\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 1 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 10 Field\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 5 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 6 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 4 Field\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Change Type 9 Field\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 2 Field\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 3 Field\" FieldType=\"O\" DataType=\"7\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - NSF Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - AEM Test Escort Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - AEM Test Escort Stub2\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Correspondence\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Envelope\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Postnet Barcode\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - 3 of 9 Barcode\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Planet Barcode\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "</Client>";
            string result2 =
                "<Bank BankID=\"1\" BankName=\"\" />" +
                "<Client ClientID=\"1002\" SiteCode=\"1\" ShortName=\"Madison\" LongName=\"Madison\" DataRetentionDays=\"0\" ImageRetentionDays=\"0\">" +
                "<DataEntryColumns DataEntryColumnsID=\"1\">" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Batch Number\" DisplayName=\"Batch Number\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"30\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Client Name\" DisplayName=\"Client Name\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Doc Grp ID\" DisplayName=\"Doc Grp ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Document ID\" DisplayName=\"Document ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"11\" ScreenOrder=\"0\" FieldName=\"Batch Date\" DisplayName=\"Batch Date\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"12\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Applied OpID\" DisplayName=\"Applied OpID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"P2 Sequence\" DisplayName=\"P2 Sequence\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Pocket Cut ID\" DisplayName=\"Pocket Cut ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"3\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"P2 Pocket Seq Num\" DisplayName=\"P2 Pocket Seq Num\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"11\" ScreenOrder=\"0\" FieldName=\"Receive Date\" DisplayName=\"Receive Date\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Deposit Number\" DisplayName=\"Deposit Number\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Deposit Time\" DisplayName=\"Deposit Time\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"11\" ScreenOrder=\"0\" FieldName=\"Consolidation Date\" DisplayName=\"Consolidation Date\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Consolidation Number\" DisplayName=\"Consolidation Number\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"P1 Station ID\" DisplayName=\"P1 Station ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"8\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"P1 Operator ID\" DisplayName=\"P1 Operator ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"P2 Station ID\" DisplayName=\"P2 Station ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"8\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"P2 Operator ID\" DisplayName=\"P2 Operator ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"128\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Audit Trail\" DisplayName=\"Audit Trail\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"20\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Process Type\" DisplayName=\"Process Type\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"20\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"ARC Reason\" DisplayName=\"ARC Reason\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"50\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"ARC Tracer\" DisplayName=\"ARC Tracer\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"12\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Reject Job\" DisplayName=\"Reject Job\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"40\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Reject Reason\" DisplayName=\"Reject Reason\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Location ID\" DisplayName=\"Location ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"40\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Location Name\" DisplayName=\"Location Name\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Orig Batch ID\" DisplayName=\"Orig Batch ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Orig P1 Seq Num\" DisplayName=\"Orig P1 Seq Num\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"10\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Abbrev Client Name\" DisplayName=\"Abbrev Client Name\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Site ID\" DisplayName=\"Site ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"9\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Routing and Transit Number\" DisplayName=\"Routing and Transit Number\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"16\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"DDA Number\" DisplayName=\"DDA Number\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"10\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Personal Check Number\" DisplayName=\"Personal Check Number\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"10\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Business Check Number\" DisplayName=\"Business Check Number\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"4\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Source ID\" DisplayName=\"Source ID\" DisplayGroup=\"ChecksDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Batch Number\" DisplayName=\"Batch Number\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"30\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Client Name\" DisplayName=\"Client Name\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Doc Grp ID\" DisplayName=\"Doc Grp ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Document ID\" DisplayName=\"Document ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"11\" ScreenOrder=\"0\" FieldName=\"Batch Date\" DisplayName=\"Batch Date\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"12\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Applied OpID\" DisplayName=\"Applied OpID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"P2 Sequence\" DisplayName=\"P2 Sequence\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Pocket Cut ID\" DisplayName=\"Pocket Cut ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"3\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"P2 Pocket Seq Num\" DisplayName=\"P2 Pocket Seq Num\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"11\" ScreenOrder=\"0\" FieldName=\"Receive Date\" DisplayName=\"Receive Date\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Deposit Number\" DisplayName=\"Deposit Number\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Deposit Time\" DisplayName=\"Deposit Time\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"11\" ScreenOrder=\"0\" FieldName=\"Consolidation Date\" DisplayName=\"Consolidation Date\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Consolidation Number\" DisplayName=\"Consolidation Number\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"P1 Station ID\" DisplayName=\"P1 Station ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"8\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"P1 Operator ID\" DisplayName=\"P1 Operator ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"P2 Station ID\" DisplayName=\"P2 Station ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"8\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"P2 Operator ID\" DisplayName=\"P2 Operator ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"128\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Audit Trail\" DisplayName=\"Audit Trail\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"20\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Process Type\" DisplayName=\"Process Type\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"20\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"ARC Reason\" DisplayName=\"ARC Reason\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"50\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"ARC Tracer\" DisplayName=\"ARC Tracer\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"12\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Reject Job\" DisplayName=\"Reject Job\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"40\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Reject Reason\" DisplayName=\"Reject Reason\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Location ID\" DisplayName=\"Location ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"40\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Location Name\" DisplayName=\"Location Name\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Orig Batch ID\" DisplayName=\"Orig Batch ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Orig P1 Seq Num\" DisplayName=\"Orig P1 Seq Num\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"10\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Abbrev Client Name\" DisplayName=\"Abbrev Client Name\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Site ID\" DisplayName=\"Site ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"18\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Parcel ID\" DisplayName=\"Parcel ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Amount Paid\" DisplayName=\"Amount Paid\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"4\" DataType=\"1\" ScreenOrder=\"0\" FieldName=\"Source ID\" DisplayName=\"Source ID\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "<DataEntryColumn DataEntryColumnID=\"0\" FieldLength=\"0\" DataType=\"6\" ScreenOrder=\"0\" FieldName=\"Reference Page\" DisplayName=\"Reference Page\" DisplayGroup=\"StubsDataEntry\" MarkSense=\"0\" />" +
                "</DataEntryColumns>" +
                "</Client>";
            string result1 =
                "<Bank BankID=\"1\" BankName=\"\" />" +
                "<Client ClientID=\"22\" SiteCode=\"1\" ShortName=\"22\" LongName=\"22\" DataRetentionDays=\"0\" ImageRetentionDays=\"0\">" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"22 - QA Test Business Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Amount\" FieldType=\"D\" DataType=\"7\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Batch Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"30\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Doc Grp ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Document ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Batch Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Applied OpID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Sequence\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Pocket Cut ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Pocket Seq Num\" FieldType=\"A\" DataType=\"1\" FieldLength=\"3\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Receive Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Deposit Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Deposit Time\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Consolidation Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Consolidation Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P1 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P1 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"P2 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"128\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Process Type\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"ARC Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"ARC Tracer\" FieldType=\"A\" DataType=\"1\" FieldLength=\"50\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Reject Job\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Reject Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Location ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Location Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Orig Batch ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Orig P1 Seq Num\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Abbrev Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Site ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"ACH Returns Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"250\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"2\" DocType=\"2\" AliasName=\"RT\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"3\" DocType=\"2\" AliasName=\"Account\" FieldType=\"A\" DataType=\"1\" FieldLength=\"16\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"2\" AliasName=\"Per Check Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"1\" DocType=\"2\" AliasName=\"Serial\" FieldType=\"\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"2\" AliasName=\"Transaction Code\" FieldType=\"\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Change of Address\" FieldType=\"M\" DataType=\"1\" FieldLength=\"1\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"22 - QA Test Per or Bus Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Cash Ticket\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Amount\" FieldType=\"D\" DataType=\"7\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Batch Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"30\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Doc Grp ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Document ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Batch Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Applied OpID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Sequence\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Pocket Cut ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Pocket Seq Num\" FieldType=\"A\" DataType=\"1\" FieldLength=\"3\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Receive Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Deposit Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Deposit Time\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Consolidation Date\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Consolidation Number\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P1 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P1 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Station ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"P2 Operator ID\" FieldType=\"A\" DataType=\"1\" FieldLength=\"8\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"128\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Process Type\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"ARC Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"20\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"ARC Tracer\" FieldType=\"A\" DataType=\"1\" FieldLength=\"50\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Reject Job\" FieldType=\"A\" DataType=\"1\" FieldLength=\"12\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Reject Reason\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Location ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Location Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Orig Batch ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Orig P1 Seq Num\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Abbrev Client Name\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Site ID\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"ACH Returns Audit Trail\" FieldType=\"A\" DataType=\"1\" FieldLength=\"250\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"2\" DocType=\"1\" AliasName=\"RT\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"3\" DocType=\"1\" AliasName=\"Account\" FieldType=\"A\" DataType=\"1\" FieldLength=\"16\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"1\" AliasName=\"Per Check Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Reference Page\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - QA Test Stb\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Policy Number\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Line of Business\" FieldType=\"A\" DataType=\"1\" FieldLength=\"2\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Due Date\" FieldType=\"A\" DataType=\"1\" FieldLength=\"6\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Amount Due\" FieldType=\"O\" DataType=\"7\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Change of Address\" FieldType=\"M\" DataType=\"1\" FieldLength=\"1\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 1 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 10 Field\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 5 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 6 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 4 Field\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 9 Field\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 2 Field\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"Type 3 Field\" FieldType=\"O\" DataType=\"7\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFLDPolicyTest\" FieldType=\"M\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFldAddressTest\" FieldType=\"M\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFLDCityTest\" FieldType=\"M\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"1\" AliasName=\"MFLDNameTest\" FieldType=\"M\" DataType=\"1\" FieldLength=\"40\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - QA Test Stb Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - AEM Test Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - AEM Test Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Bevent Stub 2\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Bevent Stub 2 Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Bevent Stub 3\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Bevent Stub 3 Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - WFS Remit Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Ghost Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Ghost Stub Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Ghost Stub - Inline IMS\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Ghost Stub - Inline IMS Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - EOB\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - EOB Data Only\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - CHP stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Dash Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"22 - NSF Personal Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"2\" AliasName=\"22 - QA Bus Check\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"2\" DocType=\"2\" AliasName=\"Rout Num\" FieldType=\"A\" DataType=\"1\" FieldLength=\"9\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"3\" DocType=\"2\" AliasName=\"Acct\" FieldType=\"A\" DataType=\"1\" FieldLength=\"16\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"4\" DocType=\"2\" AliasName=\"Txn Code\" FieldType=\"\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"1\" DocType=\"2\" AliasName=\"Bus Serial\" FieldType=\"\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 1 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 10 Field\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 5 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 6 Field\" FieldType=\"A\" DataType=\"6\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 4 Field\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 9 Field\" FieldType=\"A\" DataType=\"11\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 2 Field\" FieldType=\"A\" DataType=\"1\" FieldLength=\"10\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"0\" DocType=\"2\" AliasName=\"Type 3 Field\" FieldType=\"O\" DataType=\"7\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - NSF Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - AEM Test Escort Stub\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - AEM Test Escort Stub2\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Correspondence\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Envelope\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Postnet Barcode\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - 3 of 9 Barcode\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "<ImageRPSAliasMappings ExtractType=\"11\" DocType=\"1\" AliasName=\"22 - Planet Barcode\" FieldType=\"\" DataType=\"1\" FieldLength=\"0\" BatchSource=\"ImageRPS\" />" +
                "</Client>";

            TestClientFile(filename4, result4);
            TestClientFile(filename3, result3);
            TestClientFile(fileName2, result2);
            TestClientFile(fileName, result1);
        }
        [TestMethod]
        public void BatchStreamToXMLTest()
        {
            string fileName = TestDataPath("01202011_0022_0000911_05202011.dat");
            string filename2 = TestDataPath("04112011_0022_000005_04152011.dat");
            string filename3 = TestDataPath("Processed_Import_20110604_106602.DAT");
            string filename4 = TestDataPath("08052010_0020_004603_01312011.dat");
            LoadBatchFile(fileName);
            LoadBatchFile(filename4);
            LoadBatchFile(filename2);
            LoadBatchFile(filename3);
        }
    }
}
