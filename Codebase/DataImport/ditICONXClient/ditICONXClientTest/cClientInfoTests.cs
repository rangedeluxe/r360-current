﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportIconClient;

namespace DITICONClientTest
{
    [TestClass()]
    public class cClientInfoTests
    {
        [TestMethod()]
        public void cClientInfoTest()
        {
            var info = new cClientInfo(1, "ShortName", 1);
            Assert.AreEqual(info.ClientID, 1);
            Assert.AreEqual(info.ShortName, "ShortName");
            Assert.AreEqual(info.SiteCode, 1);

        }
    }
}
