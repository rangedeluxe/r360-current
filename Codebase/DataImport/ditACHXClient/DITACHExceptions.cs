﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose: 
*
* Modification History
******************************************************************************/

namespace DMP.ACH.Components
    {
    class DITACHExceptions  
    {   
		/// <summary>
		/// Return codes
		/// </summary>
        public const int DITACHIMPORT_NOERROR = 0000;
       // public const int DITACHIMPORT_FAILURE = 0000;        		
        public const int DITACHIMPORT_VALIDATION_ERROR = 0700;
        public const int DITACHIMPORT_VALIDATION_SUCCESS = 0700;
        public const int DITACHIMPORT_VALIDATION_WARNING = 4000;

		/// <summary>
		/// Error codes
		/// </summary>
        public const int DITACHIMPORT_INVALID_FILE_HEADER = 1000;
        public const int DITACHIMPORT_INVALID_FILE_FOOTER = 1100;
        public const int DITACHIMPORT_INVALID_FILE_NUM_BATCHES = 1200;
        public const int DITACHIMPORT_INVALID_FILE_NUM_RECORDS = 1300;
        

		public const int DITACHIMPORT_INVALID_FILE_HEADER_ERROR		= 5000;
        public const int DITACHIMPORT_INVALID_FILE_FOOTER_ERROR = 5100;
        public const int DITACHIMPORT_INVALID_BATCH_HEADER_ERROR = 5200;
        public const int DITACHIMPORT_INVALID_BATCH_FOOTER_ERROR = 5300;
        public const int DITACHIMPORT_INVALID_BATCH_DETAIL_ERROR = 5400;
        public const int DITACHIMPORT_INVALID_DETAIL_ADDENDA_ERROR = 5500; 


		/// <summary>
		/// Unified error handler, logs exceptions.
		/// </summary>
		/// <param name="sender">Event originator instance</param>
		/// <param name="e">Event arguments</param>
		public static void HandleException(object sender, Exception e)
        {
        
        }
		
		public static string GetErrorMessage(int iErrorId)
		{
			return GetErrorMessage(iErrorId, new string[0]);
		}
		/// <summary>
		/// Retrieve error message by error code
		/// </summary>
		/// <param name="errorID">Error ID</param>
		/// <returns>Error message</returns>
		public static string GetErrorMessage(int iErrorId, params object[] args)
		{
			string sMessage = "Unexpected Error";			
			switch (iErrorId)
			{
				case DITACHIMPORT_INVALID_FILE_HEADER:
				{
					sMessage = string.Format("Invalid file format. First record must be a File Header ('1') Record. File import aborted and file '{0}' moved to the error folder.",args);
					break; 
				}
				case DITACHIMPORT_INVALID_FILE_FOOTER:
				{
					sMessage = string.Format("Invalid file format. Last record must be a File Footer ('9') Record. File import aborted and file '{0}' moved to the error folder.",args);
					break; 
				}				
                //case DITACHIMPORT_SUCCESS:
                //{
                //    sMessage = "Import complete."; 
                //    break; 
                //}
                //case DITACHIMPORT_FAILURE:
                //{
                //    sMessage = string.Format("Import failed and file '{0}' moved to the error folder.", args);
                //    break;
                //}				
				case DITACHIMPORT_INVALID_FILE_HEADER_ERROR:
				{
					sMessage = string.Format("Invalid file format. File Header record is longer/shorter than 94 bytes. Line Number {1} in File: '{0}'", args);
					break;
				}
				case DITACHIMPORT_INVALID_FILE_FOOTER_ERROR:
				{
					sMessage = string.Format("Invalid file format. File Trailer record is longer/shorter than 94 bytes. Line Number {1} in File: '{0}'", args);
					break;
				}
				case DITACHIMPORT_INVALID_BATCH_HEADER_ERROR:
				{
					sMessage = string.Format("Invalid file format. Batch Header record is longer/shorter than 94 bytes. Line Number {1} in File: '{0}'", args);
					break;
				}
				case DITACHIMPORT_INVALID_BATCH_FOOTER_ERROR:
				{
					sMessage = string.Format("Invalid file format. Batch Trailer record is longer/shorter than 94 bytes. Line Number {1} in File: '{0}'", args);
					break;
				}
				case DITACHIMPORT_INVALID_BATCH_DETAIL_ERROR:
				{
					sMessage = string.Format("Invalid file format. Batch Detail record is longer/shorter than 94 bytes. Line Number {1} in File: '{0}'", args);
					break;
				}

                case DITACHIMPORT_INVALID_DETAIL_ADDENDA_ERROR:
                {
                    sMessage = string.Format("Invalid file format. Detail Addenda record is longer/shorter than 94 bytes. Line Number {1} in File: '{0}'", args);
                    break;
                }
				case DITACHIMPORT_VALIDATION_ERROR:
				{
					sMessage = string.Format("File import aborted with errors: {0}. {1}", args);
					break;
				}
				case DITACHIMPORT_VALIDATION_WARNING:
				{
					sMessage = string.Format("File import completed with warnings: {0}. {1}", args);
					break;
				}				
			}
			
            return sMessage;
        }

    }
}