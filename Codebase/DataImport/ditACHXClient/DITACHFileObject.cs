﻿using System;
using System.IO;
using System.Xml;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
*
* Purpose: This class acts as a parent class and all the remaining records types 
* will be underneath.
*
* Modification History
* WI 136683 SAS 04/16/2014 
* Changes done to generate canonical xml
* 
* WI 139933 SAS 05/12/2014 
* Changes done to replace ItemDataRecord element with RemittanceData record and 
* GhostDocument element added

* WI 139934 SAS 05/12/2014 
* Changes done for Duplicate File and Payment detection.
*
* WI 143080 SAS 05/26/2014 
* Changes done to add RawDataRecord elements
*
* WI 169843 SAS 10/06/2014 
* Changes done to replace GhostDocument with Document Element
* WI 172990 SAS 10/17/2014 
* Changes done to set proper batch sequence numbers
* 
* WI 180186 SAS 12/16/2014 
* Changes done to discontinue to pass value for BatchID
*******************************************************************************/

namespace DMP.ACH.Components
{
    internal sealed class DITACHFileObject
    {
        #region Class Fields
        private string m_FileHeader = string.Empty;
        /// <summary>
        /// File Trailer
        /// </summary>
        private string m_FileFooter = string.Empty;
        /// <summary>
        /// Batches Collection
        /// </summary>
        private DITACHBatchObjectCollection m_Batches;
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;

        #endregion Class Fields

        #region Class Properties

        /// <summary>
        /// m_FileHeader public property
        /// </summary>
        public string FileHeader
        {
            get { return m_FileHeader; }
            set { m_FileHeader = value; }
        }

        /// <summary>
        /// m_FileFooter public property
        /// </summary>
        public string FileFooter
        {
            get { return m_FileFooter; }
            set { m_FileFooter = value; }
        }

        public string FooterRecordType
        {
            get { return DITACHCommon.GetSubString(m_FileFooter, 0, 1); }
        }

        /// <summary>
        /// BatchCount public property
        /// </summary>
        public string BatchCount
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_FileFooter, 1, 6);
                if (DITACHCommon.IsNumeric(sResult))
                    return sResult;
                else
                    return "0";
            }
        }

        /// <summary>
        /// Block Count
        /// </summary>
        public string BlockCount
        {
            get{return DITACHCommon.GetSubString(m_FileFooter, 7, 6);}
        }

        /// <summary>
        /// Entry Detail and Addenda Count public property
        /// </summary>
        public string TotalCount
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_FileFooter, 13, 8);
                if (DITACHCommon.IsNumeric(sResult))
                    return sResult;
                else
                    return "0";
            }
        }

        /// <summary>
        /// Total Entry Hash public property
        /// </summary>
        public string TotalHash
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_FileFooter, 21, 10);
                if (DITACHCommon.IsNumeric(sResult))
                    return sResult;
                else
                    return "0";
            }
        }

        /// <summary>
        /// TotalDebitAmount public property
        /// </summary>
        public string FileDebitAmount
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_FileFooter, 31, 12);
                if (DITACHCommon.IsNumeric(sResult))
                    return sResult;
                else
                    return "0";
            }
        }

        /// <summary>
        /// TotalCreditAmount public property
        /// </summary>
        public string FileCreditAmount
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_FileFooter, 43, 12);
                if (DITACHCommon.IsNumeric(sResult))
                    return sResult;
                else
                    return"0";
            }
        }

        /// <summary>
        /// m_Batches public property
        /// </summary>
        public DITACHBatchObjectCollection Batches
        {
            get { return m_Batches; }
        }

        /// <summary>
        /// Get the file signature
        /// </summary>
        public string FileSignature
        {
            get
            {
                StringBuilder sbFileSignature = new StringBuilder();
                sbFileSignature.Append(this.FooterRecordType);
                sbFileSignature.Append(this.BatchCount);
                sbFileSignature.Append(this.BlockCount);
                sbFileSignature.Append(this.TotalCount );
                sbFileSignature.Append(this.TotalHash);
                sbFileSignature.Append(this.FileDebitAmount);
                sbFileSignature.Append(this.FileCreditAmount);
                return sbFileSignature.ToString(); ;
            }
        }

        /// <summary>
        /// Returns the file hash
        /// </summary>
        public string FileHash
        {
            get { return DITACHCommon.GetHash(FileSignature); }
        }


        #endregion Class Properties

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="name">Full File Name</param>
        public DITACHFileObject(cDataImportXClientBase oDataImportXClient)
        {
            m_Batches = new DITACHBatchObjectCollection();
            m_DataImportXClient = oDataImportXClient;
        }

        /// <summary>
        /// Generate the required Canonical XML
        /// </summary>
        /// <returns></returns>
        public XmlDocument GetXML()
        {
            int iTransactionID;
            int iBatchSequenceNo;
            int iDocumentSequence;
            int iDocSeqWithinTransaction;

            XmlDocument xDoc = new XmlDocument();
            XmlElement xRootElement = xDoc.CreateElement("Batches");
            xRootElement.SetAttribute("SourceTrackingID", Guid.NewGuid().ToString());
            xRootElement.SetAttribute("ClientProcessCode", DITACHCommon.CLIENTPROCESSCODE);
            xRootElement.SetAttribute("XSDVersion", DITACHCommon.BATCHXSDVERSION);
            xRootElement.SetAttribute("FileHash", this.FileHash);
            xRootElement.SetAttribute("FileSignature", this.FileSignature);
            try
            {
                foreach (DITACHBatchObject objDITACHBatchObject in this.Batches)
                {
                    foreach (DITACHABADDAValue objACHDDAValue in objDITACHBatchObject.ABADDAValueCollection)
                    {
                        XmlElement xBatchElement = null;                        
                        iTransactionID = 0;
                        iBatchSequenceNo = 0;                                                
                        iDocumentSequence = 0;
                        DITACHDetailObjectCollectionFilter objDITACHDetailObjectCollectionFilter = new DITACHDetailObjectCollectionFilter();
                        //Filter the ACH Detail object for the ABA and DDA
                        foreach (DITACHDetailObject objDITACHDetailObject in objDITACHDetailObjectCollectionFilter.Filter(objDITACHBatchObject.BatchDetails, objACHDDAValue.ABA, objACHDDAValue.DDA, false))
                        {
                            //Batch Element: Add batch element for each unique ABA and DDA combination within an ACH Batch                           
                            if (xBatchElement == null)
                            {
                                xBatchElement = objDITACHDetailObject.BuildBatchNode(xDoc, Convert.ToDateTime(objDITACHBatchObject.EffectiveDate), Convert.ToDateTime(objDITACHBatchObject.SettlementDate));
                            }

                            //Transaction Element
                            XmlElement xTransactionElement;
                            objDITACHDetailObject.RowAdded = true;
                            objDITACHDetailObject.TransactionID = ++iTransactionID;
                            xTransactionElement = objDITACHDetailObject.BuildTransactionNode(xDoc);
                            iDocSeqWithinTransaction = 0;

                            //Payment Element with Remittance Data
                            objDITACHDetailObject.PaymentBatchSequenceNo = ++iBatchSequenceNo;
                            XmlElement xPaymentElement = objDITACHDetailObject.BuildPaymentNode(xDoc);

                            //RawData Record Element
                            XmlElement xRawDataRecordElement;
                            objDITACHDetailObject.RawDataRecordBatchSequence = iBatchSequenceNo;
                            xRawDataRecordElement=objDITACHDetailObject.BuildRawDataRecordNode(xDoc);
                            if (xRawDataRecordElement != null)
                                xPaymentElement.AppendChild(xRawDataRecordElement);

                            //Payment Remittance Data Record for Payment Data Elements
                            RemittanceDataRecord objRemittanceDataRecord = new RemittanceDataRecord();
                            objRemittanceDataRecord.BatchSequence = iBatchSequenceNo;
                            XmlElement xRemittanceDataRecord = objRemittanceDataRecord.BuildXMLNode(xDoc);
                            objDITACHBatchObject.BuildPaymentRemittanceDataNode(xDoc, ref xRemittanceDataRecord);
                            objDITACHDetailObject.BuildPaymentRemittanceDataNode(xDoc, ref xRemittanceDataRecord);
                            xPaymentElement.AppendChild(xRemittanceDataRecord);
                            xTransactionElement.AppendChild(xPaymentElement);

                            //Document Element
                            objDITACHDetailObject.DocumentBatchSequence = ++iBatchSequenceNo;
                            objDITACHDetailObject.DocumentSequence = ++iDocumentSequence;
                            objDITACHDetailObject.DocSeqWithinTransaction = ++iDocSeqWithinTransaction;
                            XmlElement xDocumentElement = objDITACHDetailObject.BuildDocumentNode(xDoc);
                            
                            xTransactionElement.AppendChild(xDocumentElement);
                            //Reset to the latest Batch Sequence , that will be used for the next payment within the same batch
                            iBatchSequenceNo = objDITACHDetailObject.DocumentBatchSequence;                                
                            
                            //If xTransactionElement is null means it was already added up in the loop or there is no data in the node
                            if (xTransactionElement != null)
                                xBatchElement.AppendChild(xTransactionElement);
                        }
                        if (xBatchElement != null)
                           xRootElement.AppendChild(xBatchElement);
                    }
                }
                xDoc.AppendChild(xRootElement);
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetXML : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xDoc = null;
            }

            return xDoc;
        }

    }
}
