﻿using System;
using System.IO;  
using WFS.LTA.DataImport.DataImportXClientBase;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
* Purpose: This is the main class which will generate the canonical XML
*
* Modification History
* WI 136683 SAS 04/16/2014 
* Changes done to generate canonical xml
*
* WI 138453 SAS 04/24/2014 
* Changes done to remove hard coded method name and resetting all the totals to zero.
  
* WI 138456 SAS 04/24/2014 
* Changes done to reset all the control variables to zero so that correct hash, credit 
* and debit amount gets calculated.

* WI 139934 SAS 05/12/2014 
* Changes done for Duplicate File and Payment detection.
******************************************************************************/

namespace DMP.ACH.Components
{
    /// <summary>
    /// FileProcessor exposes core functions of DIT ACH File Import.
    /// </summary>    
    public class DITACHImport: cDataImportXClientBase 
    {
        #region Class Fields
        /// <summary>
        /// Line size in file        
        ///<summary>
        public const int FIXED_LINE_SIZE = 94;                
        /// Running Hash Total
        /// </summary>
        private long m_TotalHash;
        /// <summary>
        /// Running Debit Amount Total
        /// </summary>
        private decimal m_TotalDebitAmount;
        /// <summary>
        /// Running Credit Amount Total
        /// </summary>
        private decimal m_TotalCreditAmount;
        /// <summary>
        /// Running File Records Count 
        /// </summary>
        private int m_TotalRecords;
        #endregion Class Fields
        #region Class Methods        

        /// Main processing routine.
        public override bool StreamToXML(StreamReader stmInputFile, enmFileType fltFileType, cConfigData cfgSettings, out System.Xml.XmlDocument xmdResult)
        {
            bool bResult = true;
            try
            {
                DITACHFileObject fileObject;
                fileObject = ValidateFile(stmInputFile);
                if (!(fileObject == null))
                    xmdResult = fileObject.GetXML(); 
                else
                    xmdResult = null;
            }
            catch(Exception ex)
            {
                DoLogEvent("Exception in StreamToXML : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bResult = false;
                xmdResult = null;                 
            }         
            return bResult;            
        }

        /// <summary>
		/// Process Import File
		/// </summary>
		/// <param name="fileName">File name for import</param>
		/// <returns></returns>
        private DITACHFileObject ValidateFile(StreamReader stmInputFile)
        {
            DITACHCommon.ErrorCodes enumErrorCode = DITACHCommon.ErrorCodes.DITACHIMPORT_NOERROR;
            DITACHFileObject fileObject;
            try
            {
                fileObject = new DITACHFileObject(this);
                string sLine;
                DITACHBatchObject batchObject = null;
                DITACHDetailObject objectDetail = null;
                DITACHAddendaObject objectAddenda = null;
                bool isValid = true;

                m_TotalHash = 0;
                m_TotalDebitAmount = 0;
                m_TotalCreditAmount = 0;
                m_TotalRecords = 0;

                DoLogEvent("Reading file...", "ValidateFile()", LogEventType.Information);
                while ((sLine = stmInputFile.ReadLine()) != null)
                {
                    enumErrorCode = DITACHCommon.ErrorCodes.DITACHIMPORT_NOERROR;
                    if (DITACHCommon.IsNumeric(DITACHCommon.GetSubString(sLine, 0, 1)))
                    {
                        switch ((DITACHCommon.RecordType)Convert.ToInt16(DITACHCommon.GetSubString(sLine, 0, 1)))
                        {
                            case DITACHCommon.RecordType.HEADER_RECORD_TYPE_CODE:
                                if (sLine.Length != FIXED_LINE_SIZE)
                                    enumErrorCode = DITACHCommon.ErrorCodes.DITACHIMPORT_INVALID_FILE_HEADER_ERROR;
                                else
                                    fileObject.FileHeader = sLine;
                                break;

                            case DITACHCommon.RecordType.BATCH_HEADER_RECORD_TYPE_CODE:
                                if (sLine.Length != FIXED_LINE_SIZE)
                                    enumErrorCode = DITACHCommon.ErrorCodes.DITACHIMPORT_INVALID_BATCH_HEADER_ERROR;
                                else
                                {
                                    batchObject = new DITACHBatchObject(this);
                                    batchObject.BatchHeader = sLine;
                                }
                                break;
                            case DITACHCommon.RecordType.DETAIL_RECORD_TYPE_CODE:
                                if (batchObject != null)
                                {
                                    if (sLine.Length != FIXED_LINE_SIZE)
                                        enumErrorCode = DITACHCommon.ErrorCodes.DITACHIMPORT_INVALID_BATCH_DETAIL_ERROR;
                                    else
                                    {
                                        
                                        objectDetail = new DITACHDetailObject(batchObject.EntryClassCode,this);
                                        objectDetail.RawDetailData = sLine;
                                        objectDetail.RemitterName = batchObject.CompanyName;
                                        batchObject.BatchDetails.Add(objectDetail);
                                        m_TotalRecords++;
                                        //Populate the distinct values of ABA and DDA collection 
                                        DITACHABADDAValue objABADDAValue = new DITACHABADDAValue(objectDetail.ABA,objectDetail.DDA);
                                        //check value already populated then no need to add it again
                                        if (!batchObject.ABADDAValueCollection.ValueExists(objABADDAValue))
                                            batchObject.ABADDAValueCollection.Add(objABADDAValue);
                                    }
                                }
                                break;
                            case DITACHCommon.RecordType.ADDENDA_RECORD_TYPE_CODE:
                                if (batchObject != null && batchObject.BatchDetails.Count > 0)
                                {
                                    if (sLine.Length != FIXED_LINE_SIZE)
                                        enumErrorCode = DITACHCommon.ErrorCodes.DITACHIMPORT_INVALID_DETAIL_ADDENDA_ERROR;
                                    else
                                    {
                                        objectAddenda = new DITACHAddendaObject();
                                        objectAddenda.RawACHData = sLine; 
                                        objectDetail = batchObject.BatchDetails[batchObject.BatchDetails.Count - 1]; 
                                        objectDetail.Addendas.Add(objectAddenda);
                                        m_TotalRecords++;
                                    }
                                }
                                break;
                            case DITACHCommon.RecordType.BATCH_FOOTER_RECORD_TYPE_CODE:
                                if (batchObject != null)
                                {
                                    if (sLine.Length != FIXED_LINE_SIZE)
                                        enumErrorCode = DITACHCommon.ErrorCodes.DITACHIMPORT_INVALID_BATCH_FOOTER_ERROR;
                                    else
                                    {
                                        batchObject.BatchFooter = sLine;
                                        fileObject.Batches.Add(batchObject);
                                        if (batchObject.TotalDebitAmount != DBNull.Value)
                                            m_TotalDebitAmount += Convert.ToDecimal(batchObject.TotalDebitAmount);
                                        if (batchObject.TotalCreditAmount != DBNull.Value)
                                            m_TotalCreditAmount += Convert.ToDecimal(batchObject.TotalCreditAmount);
                                        m_TotalHash += batchObject.TotalHash;
                                        batchObject = null;
                                    }
                                }
                                break;
                            case DITACHCommon.RecordType.FOOTER_RECORD_TYPE_CODE:
                                if (sLine.Length != FIXED_LINE_SIZE)
                                    enumErrorCode = DITACHCommon.ErrorCodes.DITACHIMPORT_INVALID_FILE_FOOTER_ERROR;
                                else
                                    fileObject.FileFooter = sLine;
                                break;
                        }
                        if (enumErrorCode != DITACHCommon.ErrorCodes.DITACHIMPORT_NOERROR)
                        {
                            DoLogEvent(DITACHCommon.GetErrorMessage(enumErrorCode), this.GetType().Name, LogEventType.Error);
                            isValid = false;
                        }
                    }
                }
                if (!isValid
                   || (!ValidateObject(fileObject)))
                {
                    fileObject = null;
                }
            }
            catch (Exception ex)
            {                
                DoLogEvent("Exception in ValidateFile : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                fileObject = null;
            }
            return fileObject;
        }


        /// <summary>
        /// Process DITACHFile object
        /// </summary>
        /// <param name="fileObject">DITACHFile object instance</param>
        /// <returns></returns>        
        private bool ValidateObject(DITACHFileObject fileObject)
        {
            bool bResult = true;
            try
            {

                if (Convert.ToInt32(fileObject.BatchCount) != fileObject.Batches.Count)
                {
                    DoLogEvent(string.Format("Error: Incorrect batch count, File Control: {0}. Calculated: {1}.", fileObject.BatchCount, fileObject.Batches.Count), this.GetType().Name, LogEventType.Error);
                    bResult = false;
                }

                if (Convert.ToInt32(fileObject.TotalCount) != m_TotalRecords)
                {
                    DoLogEvent(string.Format("Error: Incorrect Entry Detail/ Addenda Count,  File Control: {0}. Calculated: {1}.", fileObject.TotalCount, m_TotalRecords), this.GetType().Name, LogEventType.Error);
                    bResult = false;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                //The hash totals are only 10 bytes wide, therefore only take the right most 10 bytes when figuring 
                //the hash. I.E. when you are done calculating the hash, if the amount is greater then 10 bytes, 
                //take only the right most 10 bytes. (15503605915 should be 5503605915) 
                //If the value is less then 10 bytes, pad the left side of the results with 0. 
                //(46543 should be 0000046543).
                string sTotalHash = m_TotalHash.ToString();
                if (sTotalHash.Length > 10)
                {
                    sTotalHash = sTotalHash.Remove(0, sTotalHash.Length - 10);
                    m_TotalHash = Convert.ToInt64(sTotalHash);
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                if (Convert.ToInt64(fileObject.TotalHash) != m_TotalHash)
                {
                    DoLogEvent(string.Format("Error: Incorrect Entry Hash, File Control: {0}. Calculated: {1}.", fileObject.TotalHash, m_TotalHash), this.GetType().Name, LogEventType.Error);
                    bResult = false;
                }

                if ((Convert.ToDecimal(fileObject.FileCreditAmount)/100 != m_TotalCreditAmount))
                {
                    DoLogEvent(string.Format("Error: Incorrect Credit Amount, File Control: {0}. Calculated: {1}.", fileObject.FileCreditAmount, m_TotalCreditAmount), this.GetType().Name, LogEventType.Error);
                    bResult = false;
                }

                if ((Convert.ToDecimal(fileObject.FileDebitAmount)/100 != m_TotalDebitAmount))
                {
                    DoLogEvent(string.Format("Error: Incorrect Debit Amount, File Control: {0}. Calculated: {1}.", fileObject.FileDebitAmount, m_TotalDebitAmount), this.GetType().Name, LogEventType.Error);
                    bResult = false;
                }

                string sErrMsg = string.Empty;
                foreach (DITACHBatchObject batchObject in fileObject.Batches)
                {
                    if (batchObject.DoValidate(out sErrMsg) == false)
                    {
                        DoLogEvent(sErrMsg, this.GetType().Name, LogEventType.Error);
                        bResult = false;
                    }
                }

            }
            catch (Exception ex)
            {   
                DoLogEvent("Exception in ValidateObject : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bResult = false;
            }
            return bResult;
        }
        #endregion Class Methods       
    }
}
