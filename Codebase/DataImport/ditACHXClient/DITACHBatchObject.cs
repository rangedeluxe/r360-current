﻿using System;
using System.Collections;
using System.Text;
using System.Xml;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
*
* Purpose: This class will hold the data for a batch header and batch header collection
*
* Modification History
* WI 136683 SAS 04/16/2014 
* Changes done to generate canonical xml
*
* WI 139933 SAS 05/12/2014 
* Changes done to replace ItemDataRecord element with RemittanceData record and 
* GhostDocument element added
******************************************************************************/

namespace DMP.ACH.Components
{
    internal sealed class DITACHBatchObject
    {
                
        #region Class Fields
        /// <summary>
        /// Stores "Batch header" value of the file structure
        /// </summary>
        private string m_BatchHeader;
        /// <summary>
        /// Stores "Batch trailer" value of the file structure
        /// </summary>
        private string m_BatchFooter;
        /// <summary>
        /// Batch Detail Collection 
        /// </summary>
        private DITACHDetailObjectCollection m_DITACHBatchDetail;
        /// <summary>
        /// Batches Collection
        /// </summary>
        private DITACHABADDAValueCollection m_ABADDAValueCollection;
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        #endregion Class Fields

        /// <summary>
        /// Class constructor 
        /// </summary>
        public DITACHBatchObject(cDataImportXClientBase oDataImportXClient)
        {
            m_DITACHBatchDetail = new DITACHDetailObjectCollection(this);
            m_ABADDAValueCollection = new DITACHABADDAValueCollection();
            m_DataImportXClient = oDataImportXClient;
        }

        #region Class Properties
       
        /// <summary>
        /// m_BatchHeader public property
        /// </summary>
        public string BatchHeader
        {
            get { return m_BatchHeader; }
            set { m_BatchHeader = value; }
        }

        /// <summary>
        /// m_BatchFooter public property 
        /// </summary>
        public string BatchFooter
        {
            get { return m_BatchFooter; }
            set { m_BatchFooter = value; }
        }

        /// <summary>
        /// m_ABADDAValueCollection public property
        /// </summary>
        public DITACHABADDAValueCollection ABADDAValueCollection
        {
            get { return m_ABADDAValueCollection; }
        }
        
        /// <summary>
        /// Service Class
        /// </summary>
        public int ServiceClass
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_BatchHeader, 1, 3);                
                if (DITACHCommon.IsNumeric(sResult))
                    return Convert.ToInt32(sResult);
                else
                    return 0;
            }
        }
       
        /// <summary>
        /// Company Name
        /// </summary>
        public string CompanyName
        {
            get { return DITACHCommon.GetSubString(m_BatchHeader, 4, 16); }
        }

        /// <summary>
        /// Company Data
        /// </summary>
        public string CompanyData
        {
	        get
	        {
		        switch (EntryClassCode)
		        {
			        case DITACHCommon.ACH_BATCH_TYPE_IAT:
				        return string.Empty;
			        default:
				        return DITACHCommon.GetSubString(m_BatchHeader, 20, 20);
		        }
	        }
        }

		/// <summary>
		/// Foreign Exchange Indicator
		/// </summary>
	    public string ForeignExchangeIndicator
	    {
		    get
		    {
			    switch (EntryClassCode)
			    {
				    case DITACHCommon.ACH_BATCH_TYPE_IAT:
					    return DITACHCommon.GetSubString(m_BatchHeader, 20, 2);
					default:
						return string.Empty;
			    }
			}
	    }

	    /// <summary>
	    /// Foreign Exchange Ref Indicator
	    /// </summary>
		public string ForeignExchangeRefIndicator
	    {
		    get
		    {
			    switch (EntryClassCode)
			    {
				    case DITACHCommon.ACH_BATCH_TYPE_IAT:
					    return DITACHCommon.GetSubString(m_BatchHeader, 22, 1);
				    default:
					    return string.Empty;
			    }
		    }
		}

	    /// <summary>
	    /// Foreign Exchange Reference
	    /// </summary>
		public string ForeignExchangeReference
		{
			get
			{
				switch (EntryClassCode)
				{
					case DITACHCommon.ACH_BATCH_TYPE_IAT:
						return DITACHCommon.GetSubString(m_BatchHeader, 23, 15);
					default:
						return string.Empty;
				}
			}
		}

	    /// <summary>
	    /// ISO DestinationCountryCode
	    /// </summary>
		public string ISODestinationCountryCode
	    {
		    get
		    {
			    switch (EntryClassCode)
			    {
				    case DITACHCommon.ACH_BATCH_TYPE_IAT:
					    return DITACHCommon.GetSubString(m_BatchHeader, 38, 2);
				    default:
					    return string.Empty;
			    }
		    }
	    }
		/// <summary>
		/// Company Id
		/// </summary>
		public string CompanyId
        {
            get { return DITACHCommon.GetSubString(m_BatchHeader, 40, 10); }
        }

        /// <summary>
        /// Entry Class Code
        /// </summary>
        public string EntryClassCode
        {
            get { return DITACHCommon.GetSubString(m_BatchHeader, 50, 3); }
        }

        /// <summary>
        /// Entry Description
        /// </summary>
        public string EntryDescription
        {
            get { return DITACHCommon.GetSubString(m_BatchHeader, 53, 10); }
        }

        /// <summary>
        /// Descriptive Date
        /// </summary>
        public string DescriptiveDate
		{
	        get
	        {
		        switch (EntryClassCode)
		        {
			        case DITACHCommon.ACH_BATCH_TYPE_IAT:
				        return string.Empty;
			        default:
				        return DITACHCommon.GetSubString(m_BatchHeader, 63, 6);
		        }
	        }
        }

		/// <summary>
	    /// ISOOriginating Currency Code
	    /// </summary>
	    public string ISOOriginatingCurrencyCode
	    {
		    get
		    {
			    switch (EntryClassCode)
			    {
				    case DITACHCommon.ACH_BATCH_TYPE_IAT:
				    {
					    return DITACHCommon.GetSubString(m_BatchHeader, 63, 3);
				    }
				    default:
				    {
					    return string.Empty;
				    }
			    }
		    }
	    }

	    /// <summary>
	    /// ISODestinationCurrencyCode
	    /// </summary>
	    public string ISODestinationCurrencyCode
	    {
		    get
		    {
			    switch (EntryClassCode)
			    {
				    case DITACHCommon.ACH_BATCH_TYPE_IAT:
				    {
					    return DITACHCommon.GetSubString(m_BatchHeader, 66, 3);
				    }
				    default:
				    {
					    return string.Empty;
				    }
			    }
		    }
	    }

		/// <summary>
		/// Effective Date
		/// </summary>
		public object EffectiveDate
        {
            get
            {
                DateTime dt;
                try
                {
                    dt = DateTime.ParseExact(DITACHCommon.GetSubString(m_BatchHeader, 69, 6), "yyMMdd", null);
                }
                catch
                {
                    return DBNull.Value;
                }
                return dt;
            }
        }
        
        /// <summary>
        /// Settlement Date
        /// </summary>
        public object SettlementDate
        {
            get 
            {
                object odt;
                try
                {
                    // When settlement date is zeros, use
                    // Effective date as settlement date.
                    if (DITACHCommon.GetSubString(m_BatchHeader, 75, 3) == "000")
                        odt = EffectiveDate;
                    else
                    {
                        odt = DITACHCommon.GetDateFromJulianDate(DITACHCommon.GetSubString(m_BatchHeader, 69, 6),
                                                              DITACHCommon.GetSubString(m_BatchHeader, 75, 3));                       
                    }
                }
                catch
                {
                    return DBNull.Value;
                }
                return odt;
            }
        }

        /// <summary>
        /// Originator Status
        /// </summary>
        public string OriginatorStatus
        {
            get { return DITACHCommon.GetSubString(m_BatchHeader, 78, 1); }
        }

        /// <summary>
        /// Originating DFI
        /// </summary>
        public string OriginatingDFI
        {
            get { return DITACHCommon.GetSubString(m_BatchFooter, 79, 8); }
        }

        /// <summary>
        /// ACH Batch Number
        /// </summary>
        public int ACHBatchNumber
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_BatchHeader, 87, 7);
                if (DITACHCommon.IsNumeric(sResult))
                    return Convert.ToInt32(sResult);
                else
                    return 0;
            }
        }

        /// <summary>
        /// Total Hash
        /// </summary>
        public long TotalHash
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_BatchFooter, 10, 10);
                if (DITACHCommon.IsNumeric(sResult))
                    return Convert.ToInt64(sResult);
                else
                    return 0;
            }
        }
        
        /// <summary>
        /// Total Debit Amount
        /// </summary>
        public object TotalDebitAmount
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_BatchFooter, 20, 12);
                if (DITACHCommon.IsNumeric(sResult))
                    return Convert.ToDecimal(sResult)/100;
                else
                    return DBNull.Value;
            }
        }
        
        /// <summary>
        /// Total Credit Amount
        /// </summary>
        public object TotalCreditAmount
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_BatchFooter, 32, 12);                
                if (DITACHCommon.IsNumeric(sResult))
                    return Convert.ToDecimal(sResult)/100;
                else
                  return DBNull.Value;
            }
        }
        
        /// <summary>
        /// Read only m_BatchDetails public property
        /// </summary>
        public DITACHDetailObjectCollection BatchDetails
        {
            get { return m_DITACHBatchDetail; }
        }      

        #endregion Class Properties

        /// <summary>
        /// Validate Data
        /// </summary>
        /// <param name="sErrorMsg"></param>
        /// <returns></returns>
        public bool DoValidate(out string sErrorMsg)
        {
            
            bool bResult=true;
            StringBuilder sbErrorMsg=new StringBuilder();
            sErrorMsg = string.Empty;
            try
            {

                //Entry Class Type
                if (!(DITACHCommon.EntryClassList().Contains(this.EntryClassCode)))
                    sbErrorMsg.AppendLine(string.Format("Error: Invalid Standard Entry Class Code {0} found.", this.EntryClassCode));

                //Service Class Code
                if (this.ServiceClass != 220)
                    sbErrorMsg.AppendLine(string.Format("Error: Invalid Service Class Code {0} found.", this.ServiceClass));

                //Settlement Date
                if (this.SettlementDate == DBNull.Value)
                    sbErrorMsg.AppendLine(string.Format("Error: Invalid Settlement Date {0} found.", DITACHCommon.GetSubString(this.BatchHeader, 75, 3)));

                //Effective Date
                if (this.EffectiveDate == DBNull.Value)
                    sbErrorMsg.AppendLine(string.Format("Error: Invalid Effective Date {0} found.", DITACHCommon.GetSubString(this.BatchHeader, 69, 6)));

                sErrorMsg = sbErrorMsg.ToString();
                if (!(string.IsNullOrEmpty(sbErrorMsg.ToString())))
                    bResult = false;                
            }
            catch(Exception ex)
            {
                bResult = false;
                m_DataImportXClient.DoLogEvent("Exception in DoValidate : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
            }
            return bResult;
        }

        /// <summary>
        /// Buildup the Payment Data node
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecord"></param>
        public void BuildPaymentRemittanceDataNode(XmlDocument xDoc, ref XmlElement xRemittanceDataRecord)
        {
            try
            {
                this.BuildNode("ServiceClassCode", this.ServiceClass.ToString(), xDoc, ref xRemittanceDataRecord);
                this.BuildNode("CompanyName", this.CompanyName.ToString(), xDoc, ref xRemittanceDataRecord);
                this.BuildNode("CompanyID", this.CompanyId.ToString(), xDoc, ref xRemittanceDataRecord);
                this.BuildNode("StandardEntryClass", this.EntryClassCode.ToString(), xDoc, ref xRemittanceDataRecord);
                this.BuildNode("EntryDescription", this.EntryDescription.ToString(), xDoc, ref xRemittanceDataRecord);
	            if (EntryClassCode == DITACHCommon.ACH_BATCH_TYPE_IAT)
	            {
		            this.BuildNode("ISOOriginatingCurrencyCode", this.ISOOriginatingCurrencyCode.ToString(), xDoc, ref xRemittanceDataRecord);
		            this.BuildNode("ISODestinationCurrencyCode", this.ISODestinationCurrencyCode.ToString(), xDoc, ref xRemittanceDataRecord);
		            this.BuildNode("ForeignExchangeIndicator", this.ForeignExchangeIndicator.ToString(), xDoc, ref xRemittanceDataRecord);
		            this.BuildNode("ForeignExchangeRefIndicator", this.ForeignExchangeRefIndicator.ToString(), xDoc, ref xRemittanceDataRecord);
		            this.BuildNode("ForeignExchangeReference", this.ForeignExchangeReference.ToString(), xDoc, ref xRemittanceDataRecord);
		            this.BuildNode("ISODestinationCountryCode", this.ISODestinationCountryCode.ToString(), xDoc, ref xRemittanceDataRecord);

				}
				else
	            {
		            this.BuildNode("CompanyData", this.CompanyData.ToString(), xDoc, ref xRemittanceDataRecord);
		            this.BuildNode("DescriptiveDate", this.DescriptiveDate.ToString(), xDoc, ref xRemittanceDataRecord);
				}
				this.BuildNode("EffectiveDate", this.EffectiveDate.ToString(), xDoc, ref xRemittanceDataRecord);
                this.BuildNode("SettlementDate", this.SettlementDate.ToString(), xDoc, ref xRemittanceDataRecord);
                this.BuildNode("OriginatingDFI", this.OriginatingDFI.ToString(), xDoc, ref xRemittanceDataRecord);
                this.BuildNode("ACHBatchNumber ", this.ACHBatchNumber.ToString(), xDoc, ref xRemittanceDataRecord);
            }
            catch(Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildPaymentRemittanceDataNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
            }           
        }

       /// <summary>
        /// Buildup the required nodes
       /// </summary>
       /// <param name="sFieldName"></param>
       /// <param name="sFieldValue"></param>
       /// <param name="xDoc"></param>
       /// <param name="xRemittanceDataRecord"></param>
        private void BuildNode(string sFieldName, string sFieldValue, XmlDocument xDoc, ref XmlElement xRemittanceDataRecord)
        {
            try
            {
                if (!string.IsNullOrEmpty(sFieldValue))
                {
                    RemittanceData objRemittanceData;
                    objRemittanceData = new RemittanceData();
                    objRemittanceData.FieldName = sFieldName;
                    objRemittanceData.FieldValue = sFieldValue.ToString();
                    xRemittanceDataRecord.AppendChild(objRemittanceData.BuildXMLNode(xDoc));
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
            }       
            
        }

    }

    /// <summary>
    /// ACH Batch Collection class
    /// </summary>
    internal class DITACHBatchObjectCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITACHBatchObject this[int index]
        {
            get
            {
                return ((DITACHBatchObject)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }
        
        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITACHBatchObject value)
        {
            return (List.Add(value));
        }
        
        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITACHBatchObject value)
        {
            return (List.IndexOf(value));
        }
        
        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITACHBatchObject value)
        {
            List.Insert(index, value);
        }
        
        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITACHBatchObject value)
        {
            List.Remove(value);
        }
        
        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITACHBatchObject value)
        {
            // If value is not of type ACHBatchObject, this will return false.
            return (List.Contains(value));
        }
        
        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.ACH.Components.DITACHBatchObject"))
                throw new ArgumentException("value must be of type DITACHBatchObject.", "value");
        }
    }
}
