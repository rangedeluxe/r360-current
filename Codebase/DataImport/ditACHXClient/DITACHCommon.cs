﻿using System;
using System.Collections;
using System.Globalization;
using System.Text;
using System.Security.Cryptography;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
*
* Purpose: This class holds the constants and the common methods used by other classes.
*
* Modification History
* WI 136683 SAS 04/16/2014 
* Changes done to generate canonical xml
*
* WI 139933 SAS 05/12/2014 
* Changes done to replace ItemDataRecord element with RemittanceData record and 
* GhostDocument element added
* WI 169843 SAS 10/06/2014 
* Added a constant for Document Descriptor
******************************************************************************/

namespace DMP.ACH.Components
{
    public static class DITACHCommon
    {
        public const string BATCHXSDVERSION = "2.01.02.00";
        public const string CLIENTPROCESSCODE = "";

        #region RECORD_TYPE_CONSTANTS
        /// <summary>
        /// Record Type Code of the file header 
        /// </summary>
        public const string HEADER_RECORD_TYPE_CODE = "1";
        /// <summary>
        /// Record Type Code of the batch header 
        /// </summary>
        public const string BATCH_HEADER_RECORD_TYPE_CODE = "5";
        /// <summary>
        /// Record Type Code of the entry detail 
        /// </summary>
        public const string DETAIL_RECORD_TYPE_CODE = "6";
        /// <summary>
        /// Record Type Code of the Payment-related information 
        /// </summary>
        public const string ADDENDA_RECORD_TYPE_CODE = "7";
        /// <summary>
        /// Record Type Code of the batch footer - Batch control record 
        /// </summary>        
        public const string BATCH_FOOTER_RECORD_TYPE_CODE = "8";
        /// <summary>
        /// Record Type Code of the file footer - File control record 
        /// </summary>
        public const string FOOTER_RECORD_TYPE_CODE = "9";
        #endregion RECORD_TYPE_CONSTANTS
        /// <summary>
        /// Document Descriptor
        /// </summary>
        public const string DOCUMENT_DESCRIPTOR = "ELECTRONIC";

        #region ADDENDA DELIMITERS
        /// <summary>
        /// Addenda Segment Delimiter
        /// </summary>
        public const string ADDENDA_SEG_DELIMITER = "\\";
        /// <summary>
        /// Addenda Data Element Delimiter
        /// </summary>
        public const string ADDENDA_DATA_DELIMITER = "*";

        /// <summary>
        /// Addenda type code
        /// </summary>
        public const int ADDENDA_TYPE_CODE = 17;

        #endregion ADDENDA DELIMITERS

        #region BATCH_TYPE_CONSTANTS
        /// <summary>
        /// Names of Batch Types
        /// </summary>
        public const string ACH_BATCH_TYPE_CCD = "CCD";
        public const string ACH_BATCH_TYPE_CTX = "CTX";
        public const string ACH_BATCH_TYPE_PPD = "PPD";
        public const string ACH_BATCH_TYPE_IAT = "IAT";
        public const string ACH_BATCH_TYPE_CIE = "CIE";
        #endregion BATCH_TYPE_CONSTANTS

        #region ENUMERATION__CONSTANTS
        /// <summary>
        /// Records type in file
        /// </summary>
        public enum RecordType
        {
            HEADER_RECORD_TYPE_CODE = 1,
            BATCH_HEADER_RECORD_TYPE_CODE = 5,
            DETAIL_RECORD_TYPE_CODE = 6,
            ADDENDA_RECORD_TYPE_CODE = 7,
            BATCH_FOOTER_RECORD_TYPE_CODE = 8,
            FOOTER_RECORD_TYPE_CODE = 9
        }

        /// <summary>
        /// Error codes
        /// </summary>
        public enum ErrorCodes
        {
            DITACHIMPORT_NOERROR = 0,
            DITACHIMPORT_INVALID_FILE_HEADER_ERROR = 1,
            DITACHIMPORT_INVALID_FILE_FOOTER_ERROR = 2,
            DITACHIMPORT_INVALID_BATCH_HEADER_ERROR = 3,
            DITACHIMPORT_INVALID_BATCH_FOOTER_ERROR = 4,
            DITACHIMPORT_INVALID_BATCH_DETAIL_ERROR = 5,
            DITACHIMPORT_INVALID_DETAIL_ADDENDA_ERROR = 6
        }
        #endregion ENUMERATION__CONSTANTS

        #region COMMON_FUNCTIONS
        /// <summary>
        /// This function will return a substring
        /// </summary>
        /// <param name="sstring"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetSubString(string sstring, int startIndex, int length)
        {
            if (sstring.Length >= (startIndex + length))
                return sstring.Substring(startIndex, length).Trim();
            else
                return string.Empty;
        }

        /// <summary>
        /// Check if the value is numeric
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNumeric(string value)
        {
            bool  retvalue = true;
            foreach (Char c in value.ToCharArray())
            {
                retvalue = retvalue && Char.IsDigit(c);
            }
            return retvalue;
        }

        /// <summary>
        /// This function will calculate the date based on the Julian date provided
        /// </summary>
        /// <param name="val1"></param>
        /// <param name="val2"></param>
        /// <returns></returns>
        public static object GetDateFromJulianDate(string val1, string val2)
        {            
            try
            {                
                DateTime startDt = new DateTime( DateTime.ParseExact(val1, "yyMMdd", null).Year, 1, 1);
                return startDt.AddDays(Convert.ToInt32(val2) - 1); 
            }
            catch
            {
                return DBNull.Value;
            }            
        }

        /// <summary>
        /// If the input is a valid currency string, then strip any unexpected characters
        /// (like commas or currency symbols) and change trailing signs to leading.
        /// If the input isn't a valid currency string, return null.
        /// </summary>
        /// <param name="inputValue">
        /// The input string. This may have a leading or trailing sign (+ or -).
        /// </param>
        /// <param name="negate">Set this to true to negate the value before returning.</param>
        /// <returns>A string with only digits and possible decimal places, and possible leading minus.</returns>
        public static string NormalizeMoneyString(string inputValue, bool negate = false)
        {
            decimal moneyValue;
            if (decimal.TryParse(inputValue, NumberStyles.Currency, CultureInfo.InvariantCulture, out moneyValue))
            {
                if (negate)
                    moneyValue = -moneyValue;
                return moneyValue.ToString(CultureInfo.InvariantCulture);
            }
            return null;
        }

        /// <summary>
        /// Valid Entry Class
        /// </summary>
        /// <returns></returns>
        public static ArrayList EntryClassList()
        {
            ArrayList arrLstEntryClass = new ArrayList();
            arrLstEntryClass.Add(ACH_BATCH_TYPE_CCD);
            arrLstEntryClass.Add(ACH_BATCH_TYPE_CTX);
            arrLstEntryClass.Add(ACH_BATCH_TYPE_PPD);
            arrLstEntryClass.Add(ACH_BATCH_TYPE_IAT);
            arrLstEntryClass.Add(ACH_BATCH_TYPE_CIE);
            return arrLstEntryClass;
        }

        /// <summary>
        /// Retrieve error message by error code
        /// </summary>
        /// <param name="iErrorId"></param>
        /// <returns>sMessage</returns>
        public static string GetErrorMessage(ErrorCodes eErrorCode)
        {
            string sMessage = "Unexpected Error";
            switch (eErrorCode)
            {
                case ErrorCodes.DITACHIMPORT_INVALID_FILE_HEADER_ERROR:
                    {
                        sMessage = "Invalid file format. File Header record is longer/shorter than 94 bytes.";
                        break;
                    }
                case ErrorCodes.DITACHIMPORT_INVALID_FILE_FOOTER_ERROR:
                    {
                        sMessage = "Invalid file format. File Trailer record is longer/shorter than 94 bytes.";
                        break;
                    }
                case ErrorCodes.DITACHIMPORT_INVALID_BATCH_HEADER_ERROR:
                    {
                        sMessage = "Invalid file format. Batch Header record is longer/shorter than 94 bytes.";
                        break;
                    }
                case ErrorCodes.DITACHIMPORT_INVALID_BATCH_FOOTER_ERROR:
                    {
                        sMessage = "Invalid file format. Batch Trailer record is longer/shorter than 94 bytes.";
                        break;
                    }
                case ErrorCodes.DITACHIMPORT_INVALID_BATCH_DETAIL_ERROR:
                    {
                        sMessage = "Invalid file format. Batch Detail record is longer/shorter than 94 bytes.";
                        break;
                    }
                case ErrorCodes.DITACHIMPORT_INVALID_DETAIL_ADDENDA_ERROR:
                    {
                        sMessage = "Invalid file format. Detail Addenda record is longer/shorter than 94 bytes.";
                        break;
                    }
            }

            return sMessage;
        }

        /// <summary>
        /// Get hash
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static string GetHash(string sValue)
        {
            string sResult = string.Empty;
            var data = Encoding.ASCII.GetBytes(sValue);
            var hashData = new SHA1Managed().ComputeHash(data);
            foreach (var b in hashData)
                sResult += b.ToString("X2");
            return sResult;
        }
        #endregion COMMON_FUNCTIONS
    }
}
