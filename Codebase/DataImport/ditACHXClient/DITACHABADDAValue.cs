﻿using System;
using System.Collections;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/31/2104
*
*
* Purpose: This class will hold the distinct ABA and DDA value
*
* Modification History
* WI 136683 SAS 04/16/2014 
* Changes done to generate canonical xml
******************************************************************************/

namespace DMP.ACH.Components
{
    /// <summary>
    /// Holds the ABA as DDA values
    /// </summary>
    internal sealed class DITACHABADDAValue
    {

         /// <summary>
        /// Stores ABA value 
        /// </summary>
        private string m_ABAValue;
        /// <summary>
        /// Stores DDA Value
        /// </summary>
        private string m_DDAValue;    

        /// <summary>
        /// ABA value
        /// </summary>
        public string ABA 
        { 
            get { return m_ABAValue; }
        }
        /// <summary>
        /// DDA 
        /// </summary>
        public string DDA  
        { 
            get { return m_DDAValue; }
        }

        public DITACHABADDAValue(string sABA, string sDDA)
        {
            this.m_ABAValue = sABA;
            this.m_DDAValue = sDDA;
        }
    }
     /// <summary>
    /// ACHABADDAValue Batch Collection class
    /// </summary>
    internal class DITACHABADDAValueCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITACHABADDAValue this[int index]
        {
            get
            {
                return ((DITACHABADDAValue)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }
        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITACHABADDAValue value)
        {
            return (List.Add(value));
        }
        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITACHABADDAValue value)
        {
            return (List.IndexOf(value));
        }
        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITACHABADDAValue value)
        {
            List.Insert(index, value);
        }
        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITACHABADDAValue value)
        {
            List.Remove(value);
        }
        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITACHABADDAValue value)
        {
            // If value is not of type ACHBatchObject, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// Check if the object already exists in the collection.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool ValueExists(DITACHABADDAValue value)
        {
            foreach(DITACHABADDAValue objACHDDAValue in this)
            {
                if ((objACHDDAValue.ABA==value.ABA) && (objACHDDAValue.DDA==value.DDA))
                    return true; 
            }
            return false;
        }

        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }


        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.ACH.Components.DITACHABADDAValue"))
                throw new ArgumentException("value must be of type DITACHABADDAValue.", "value");
        }
    }
}
