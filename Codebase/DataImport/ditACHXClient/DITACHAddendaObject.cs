﻿using System;
using System.Collections;
using System.Xml;
using WFS.LTA.DataImport.DataImportClientAPI;
using System.Text;



/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
*
* Purpose: This class will hold the addenda data for a batch.
*
* Modification History
* WI 136683 SAS 04/16/2014 
* Changes done to parse addenda data.
*
* WI 139933 SAS 05/12/2014 
* Changes done to replace ItemDataRecord element with RemittanceData record and 
* GhostDocument element added
* WI 169843 SAS 10/06/2014 
* Changes done to replace GhostDocument with Document Element 
* WI 171744 SAS 10/13/2014 
* Changes done to sent an empty RemittanceDataRecord in document node.
* WI 172990 SAS 10/17/2014 
* Changes done to set proper batch sequence numbers
* WI 178369 SAS 11/17/2014 
* Changes done to link RMR, BPR and TRN records 
******************************************************************************/

namespace DMP.ACH.Components
{
    /// <summary>
    /// ACH Addenda Collection Item class
    /// </summary>
    public sealed class DITACHAddendaObject
    {
        #region Class Fields
        /// <summary>
        /// Addenda Raw ACH Data
        /// </summary>
        private string m_RawACHData = string.Empty;

        #endregion Class Fields

        #region Class Properties
        /// <summary>
        /// m_RawACHData public property
        /// </summary>
        public string RawACHData
        {
            get { return m_RawACHData; }
            set { m_RawACHData = value; }
        }
        /// <summary>
        /// Addenda Data
        /// </summary>
        public string AddendaData
        {
            get { return DITACHCommon.GetSubString(m_RawACHData, 3, 80); }
        }

        /// <summary>
        /// Addenda Type
        /// </summary>
        public int AddendaType
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_RawACHData, 1, 2);
                if (DITACHCommon.IsNumeric(sResult))
                    return Convert.ToInt32(sResult);
                else
                    return 0;
            }
        }
        #endregion Class Properties
    }


    /// <summary>
    /// ACH Addenda Collection class
    /// </summary>
    public sealed class DITACHAddendaObjectCollection : CollectionBase
    {
        #region Class Fields
        /// <summary>
        /// m_RemittanceBatchSequenceNo
        /// </summary>
        private int m_RemittanceBatchSequenceNo = 0;
        #endregion Class Fields

        #region Class Properties
        /// <summary>
        /// Remittance BatchSequenceNo
        /// </summary>
        public int RemittanceBatchSequenceNo
        {
            get { return m_RemittanceBatchSequenceNo; }
            set { m_RemittanceBatchSequenceNo = value; }
        }
        #endregion Class Properties
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITACHAddendaObjectCollection this[int index]
        {
            get
            {
                return ((DITACHAddendaObjectCollection)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }
        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITACHAddendaObject value)
        {
            return (List.Add(value));
        }
        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITACHAddendaObject value)
        {
            return (List.IndexOf(value));
        }
        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITACHAddendaObject value)
        {
            List.Insert(index, value);
        }
        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITACHAddendaObject value)
        {
            List.Remove(value);
        }
        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITACHAddendaObject value)
        {
            // If value is not of type ACHBatch, this will return false.
            return (List.Contains(value));
        }
        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.ACH.Components.DITACHAddendaObject"))
                throw new ArgumentException("value must be of type DITACHAddendaObject.", "value");
        }

        /// <summary>
        /// Parse Addenda data from multiple lines of 80 bytes
        /// </summary>
        /// <param name="sBatchType"></param>
        /// <param name="xDoc"></param>
        /// <param name="xGhostDocumentNode"></param>
        /// <returns></returns>
        public bool BuildRemittanceDataElementNode(string sBatchType, XmlDocument xDoc, ref XmlElement xDocumentNode)
        {
            
            bool bDataAdded = false;
            bool bRMRAdded = false;
            bool bBPRStillExists=false;
            bool bTRNStillExists=false;

            StringBuilder sbAddendaRawData = new StringBuilder();
            string sBPRAmount = string.Empty;
            string sBPRAccount = string.Empty;
            string sTraceNo = string.Empty;

            XmlElement xRemittanceDataRecordElement;
            foreach (DITACHAddendaObject objAddendaObject in this)
            {
                //Concatenate all the addenda data
                if ((string.Compare(sBatchType, DITACHCommon.ACH_BATCH_TYPE_CCD, true) == 0) ||
                    (string.Compare(sBatchType, DITACHCommon.ACH_BATCH_TYPE_CTX, true) == 0) ||
                    (string.Compare(sBatchType, DITACHCommon.ACH_BATCH_TYPE_PPD, true) == 0) ||
                    (string.Compare(sBatchType, DITACHCommon.ACH_BATCH_TYPE_CIE, true) == 0) ||
                    ((string.Compare(sBatchType, DITACHCommon.ACH_BATCH_TYPE_IAT, true) == 0) && (objAddendaObject.AddendaType == 17))
                   )
                    sbAddendaRawData.Append(objAddendaObject.AddendaData);
            }

            if (sbAddendaRawData.Length > 0)
            {
                int iIndex = 0;
                string[] sParsedAddenda = sbAddendaRawData.ToString().Split(DITACHCommon.ADDENDA_SEG_DELIMITER.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
               
                foreach (string sLine in sParsedAddenda)
                {
                    string sSegment = sLine.Trim();
                    if (sSegment.Length > 0)
                    {
                        //Set Remittance Data Record
                        RemittanceDataRecord objRemittanceDataRecord = new RemittanceDataRecord();
                        objRemittanceDataRecord.BatchSequence = ++this.RemittanceBatchSequenceNo;
                        xRemittanceDataRecordElement = objRemittanceDataRecord.BuildXMLNode(xDoc);

                        // Determine if the Addenda Record contains BPR                    
                        if ((iIndex = sSegment.ToUpper().IndexOf("BPR")) > -1)
                        {
                            // Remove any extra data before the BPR string. This is not needed
                            sSegment = sSegment.Remove(0, iIndex);
                            if (GetBPRRemittanceData(sSegment, out sBPRAmount, out sBPRAccount))
                                bBPRStillExists = true;  //Indicator to detected the BPR record has not yet added.
                        }

                        // Determine if the Addenda Record contains RMR
                        else if ((iIndex = sSegment.ToUpper().IndexOf("RMR")) > -1)
                        {
                            // Remove any extra data before the RMR string. This is not needed
                            sSegment = sSegment.Remove(0, iIndex);
                            if (CreateRMRRemittanceData(sSegment, sBPRAmount, sBPRAccount, xDoc, ref xRemittanceDataRecordElement))
                            {
                                bRMRAdded = true;  //Indicator states the RMR Records has been added.
                                bBPRStillExists = false; //Indicator to detected the BPR record has been added.
                            }
                        }

                        // Determine if the Addenda Record contains TRN
                        else if ((iIndex = sSegment.ToUpper().IndexOf("TRN")) > -1)
                        {
                            // Remove any extra data before the TRN string. This is not needed
                            sSegment = sSegment.Remove(0, iIndex);
                            sTraceNo = GetTRNRemittanceData(sSegment);
                            bTRNStillExists =true;
                        }


                        //Check if the RMR record are added then add a TRN records and add the entire data in Document Element
                        if (bRMRAdded)
                        {
                            if(!string.IsNullOrEmpty(sTraceNo))
                            {
                                CreateTransactionRemittanceData(sTraceNo,xDoc, ref xRemittanceDataRecordElement);
                                bTRNStillExists = false;
                            }
                            bRMRAdded = false;
                            xDocumentNode.AppendChild(xRemittanceDataRecordElement);
                            bDataAdded = true;
                        }
                        else
                            this.RemittanceBatchSequenceNo -= 1;
                    }
                }

                //If no BPR or TRN records were added then add them here, since no corresponding RMR records exists in Addenda
                if ((bBPRStillExists) || (bTRNStillExists))
                {
                    RemittanceDataRecord objRemittanceDataRecord = new RemittanceDataRecord();
                    objRemittanceDataRecord.BatchSequence = ++this.RemittanceBatchSequenceNo;
                    xRemittanceDataRecordElement = objRemittanceDataRecord.BuildXMLNode(xDoc);

                    if (CreateBPRRemittanceData(string.Empty, sBPRAmount, sBPRAccount, xDoc, ref xRemittanceDataRecordElement) ||
                       (CreateTransactionRemittanceData(sTraceNo, xDoc, ref xRemittanceDataRecordElement)))
                        xDocumentNode.AppendChild(xRemittanceDataRecordElement);
                    else
                        this.RemittanceBatchSequenceNo -= 1;
                }
            }
            return bDataAdded;
        }

        /// <summary>
        /// Get BPR Remittance Data
        /// </summary>
        /// <param name="sSegment"></param>
        /// <param name="sBPRAmount"></param>
        /// <param name="sBPRAccountNo"></param>
        private bool GetBPRRemittanceData(string sSegment, out string sBPRAmount, out string sBPRAccountNo)
        {

            bool bRetVal = false;
            // Remove any extra data before the BPR string. This is not needed
            string[] sParsedSegment = sSegment.Split(DITACHCommon.ADDENDA_DATA_DELIMITER.ToCharArray(), StringSplitOptions.None);
            sBPRAmount = string.Empty;
            sBPRAccountNo = string.Empty;
            if (sParsedSegment.Length > 0)
                ParseBPR(sParsedSegment, out sBPRAmount, out sBPRAccountNo);
            if(!(string.IsNullOrEmpty(sBPRAmount)) || !(string.IsNullOrEmpty(sBPRAccountNo)))
                bRetVal = true;
            return bRetVal;
        }



        /// <summary>
        /// Create Transaction Remittance Data
        /// </summary>
        /// <param name="sTraceNo"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        /// <returns></returns>
        private bool CreateTransactionRemittanceData(string sTraceNo, XmlDocument xDoc, ref XmlElement xRemittanceDataRecordElement)
        {
            bool bRetValue = false;
            //Remittance Data for BPR
            if (AddChildNodes("ReassociationTraceNumber", sTraceNo, xDoc, ref xRemittanceDataRecordElement))
                bRetValue = true;
            return bRetValue;
        }


        /// <summary>
        /// Create CreateBPRRemittanceData
        /// </summary>
        /// <param name="sRMRAmt"></param>
        /// <param name="sBPRAmount"></param>
        /// <param name="sBPRAccountNo"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        /// <returns></returns>
        private bool CreateBPRRemittanceData(string sRMRAmt, string sBPRAmount, string sBPRAccountNo, XmlDocument xDoc, ref XmlElement xRemittanceDataRecordElement)
        {
            bool bRetValue = false;
            //Remittance Data for BPR
            if (AddChildNodes("Amount", sRMRAmt, xDoc, ref xRemittanceDataRecordElement))
                bRetValue = true;
            if (AddChildNodes("AccountNumber", sBPRAccountNo, xDoc, ref xRemittanceDataRecordElement))
                bRetValue = true;
            if (AddChildNodes("BPRMonetaryAmount", sBPRAmount, xDoc, ref xRemittanceDataRecordElement))
                bRetValue = true;
            if (AddChildNodes("BPRAccountNumber", sBPRAccountNo, xDoc, ref xRemittanceDataRecordElement))
                bRetValue = true;
            return bRetValue;
        }

        /// <summary>
        /// Create RMR RemittanceData record
        /// </summary>
        /// <param name="sSegment"></param>
        /// <param name="sBPRAmount"></param>
        /// <param name="sBPRAccountNo"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        /// <returns></returns>
        private bool CreateRMRRemittanceData(string sSegment, string sBPRAmount, string sBPRAccountNo, XmlDocument xDoc, ref XmlElement xRemittanceDataRecordElement)
        {
            bool bRetVal = false;
            string[] sParsedSegment = sSegment.Split(DITACHCommon.ADDENDA_DATA_DELIMITER.ToCharArray(), StringSplitOptions.None);
            if (sParsedSegment.Length > 0)
            {
                string sRMRRefNo;
                string sRMRAmt;
                string sRMRTotInvoivceAmt;
                string sRMRDiscountAmt;
                ParseRMR(sParsedSegment, out sRMRRefNo, out sRMRAmt, out sRMRTotInvoivceAmt, out sRMRDiscountAmt);

                if (CreateBPRRemittanceData(sRMRAmt, sBPRAmount, sBPRAccountNo, xDoc, ref xRemittanceDataRecordElement))
                    bRetVal = true;

                //Remittance Data for RMR
                if (AddChildNodes("InvoiceNumber", sRMRRefNo, xDoc, ref xRemittanceDataRecordElement))
                    bRetVal = true;

                if (AddChildNodes("RMRReferenceNumber", sRMRRefNo, xDoc, ref xRemittanceDataRecordElement))
                    bRetVal = true;

                if (AddChildNodes("RMRMonetaryAmount", sRMRAmt, xDoc, ref xRemittanceDataRecordElement))
                    bRetVal = true;

                if (AddChildNodes("RMRTotalInvoiceAmount", sRMRTotInvoivceAmt, xDoc, ref xRemittanceDataRecordElement))
                    bRetVal = true;

                if (AddChildNodes("RMRDiscountAmount", sRMRDiscountAmt, xDoc, ref xRemittanceDataRecordElement))
                    bRetVal = true;
            }
            return bRetVal;
        }

        /// <summary>
        /// Get TRN RemittanceData record
        /// </summary>
        /// <param name="sSegment"></param>
        /// <returns></returns>
        private string GetTRNRemittanceData(string sSegment)
        {
            string sTraceNo = string.Empty;
            string[] sParsedSegment = sSegment.Split(DITACHCommon.ADDENDA_DATA_DELIMITER.ToCharArray(), StringSplitOptions.None);
            if (sParsedSegment.Length > 0)
            {
                ParseTRN(sParsedSegment, out sTraceNo);
            }
            return sTraceNo;
        }

        /// <summary>
        /// Adds the child nodes into the XML RemittanceDataRecordElement 
        /// </summary>
        /// <param name="sFieldName"></param>
        /// <param name="sFieldValue"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        /// <returns></returns>
        private bool AddChildNodes(string sFieldName, string sFieldValue, XmlDocument xDoc, ref XmlElement xRemittanceDataRecordElement)
        {
            bool bRetVal = false;
            if (!string.IsNullOrEmpty(sFieldValue))
            {
                RemittanceData objRemittanceData;
                objRemittanceData = new RemittanceData();
                objRemittanceData.FieldName = sFieldName;
                objRemittanceData.FieldValue = sFieldValue.ToString();
                xRemittanceDataRecordElement.AppendChild(objRemittanceData.BuildXMLNode(xDoc));
                bRetVal = true;
            }
            return bRetVal;
        }


        /// <summary>
        ///  Parse the BPR Segment of the Addenda Record
        /// </summary>
        /// <param name="strParsedSegment"></param>
        /// <param name="dBPRAmount"></param>
        /// <param name="sBPRAccountNo"></param>
        private void ParseBPR(string[] sParsedSegment, out string sBPRAmt, out string sBPRAccountNo)
        {
            bool bIsDebit = false;
            decimal dAmount = 0.00m;
            sBPRAmt = string.Empty;
            sBPRAccountNo = string.Empty;

            // The BPR Segment will have the format
            //
            //    BPR*C*2723.93*C*ACH*CTX*01*103088819*ZZ*11111111***01*10108881 
            //
            // Need to parse out the following fields:
            //
            //    Field 03 = BPR Monetary Amount
            //    Field 04 = Credit or Debit indicator
            //    Field 10 = BPR Account Number

            if (sParsedSegment.Length > 2)
                sBPRAmt = sParsedSegment[2];

            if (sParsedSegment.Length > 3)
                if (string.Compare(sParsedSegment[3], "D", true) == 0)
                    bIsDebit = true;

            if (sParsedSegment.Length > 9)
                sBPRAccountNo = sParsedSegment[9];

            // Add the cents if it is missing
            if (sBPRAmt.Length > 0 && sBPRAmt.IndexOf('.') < 0)
                sBPRAmt += ".00";

            sBPRAmt = DITACHCommon.NormalizeMoneyString(sBPRAmt, negate: bIsDebit) ?? sBPRAmt;
        }



        /// <summary>
        /// Parse the RMR Segment of the Addenda Record
        /// </summary>
        /// <param name="sParsedSegment"></param>
        /// <param name="sRMRRefNo"></param>
        /// <param name="sRMRAmt"></param>
        /// <param name="sRMRTotInvoivceAmt"></param>
        /// <param name="sRMRDiscountAmt"></param>
        private static void ParseRMR(string[] sParsedSegment, out string sRMRRefNo, out  string sRMRAmt, out  string sRMRTotInvoivceAmt, out  string sRMRDiscountAmt)
        {
            sRMRRefNo = string.Empty;
            sRMRAmt = string.Empty;
            sRMRTotInvoivceAmt = string.Empty;
            sRMRDiscountAmt = string.Empty;

            // The RMR Segment will have the format
            //
            //    RMR*IV*PM267039**2660*0\
            //
            // Parse out the fields that are available
            //
            //    Field 03 = RMR Reference Number
            //    Field 05 = RMR Monetary Amount
            //    Field 06 = RMR Total Invoice Amount
            //    Field 07 = RMR Discount Amount
            //
            if (sParsedSegment.Length > 2)
                sRMRRefNo = sParsedSegment[2];

            if (sParsedSegment.Length > 4)
                sRMRAmt = sParsedSegment[4];

            if (sParsedSegment.Length > 5)
                sRMRTotInvoivceAmt = sParsedSegment[5];

            if (sParsedSegment.Length > 6)
                sRMRDiscountAmt = sParsedSegment[6];

            // Add the cents if it is missing
            if (sRMRAmt.Length > 0 && sRMRAmt.IndexOf('.') < 0)
                sRMRAmt += ".00";

            if (sRMRTotInvoivceAmt.Length > 0 && sRMRTotInvoivceAmt.IndexOf('.') < 0)
                sRMRTotInvoivceAmt += ".00";

            if (sRMRDiscountAmt.Length > 0 && sRMRDiscountAmt.IndexOf('.') < 0)
                sRMRDiscountAmt += ".00";

            // Begin Validating the Currency fields
            sRMRAmt = DITACHCommon.NormalizeMoneyString(sRMRAmt) ?? "";
            sRMRTotInvoivceAmt = DITACHCommon.NormalizeMoneyString(sRMRTotInvoivceAmt) ?? "";
            sRMRDiscountAmt = DITACHCommon.NormalizeMoneyString(sRMRDiscountAmt) ?? "";
        }


        /// <summary>
        /// Parse the BPR Segment of the Addenda Record
        /// </summary>
        /// <param name="sParsedSegment"></param>
        /// <param name="sTraceNo"></param>
        private void ParseTRN(string[] sParsedSegment, out string sTraceNo)
        {
            sTraceNo = string.Empty;
            // The TRN Segment will have the format            
            //    TRN*1*051036621690138\            
            // Need to parse out the following fields:            
            //    Field 03 =ReassociationTraceNumber             
            if (sParsedSegment.Length > 2)
                sTraceNo = sParsedSegment[2];
        }


    }
}
