﻿using System;
using System.Xml;
using System.Collections;
using System.Text;
using System.Xml.Linq;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
*
* Purpose: This class will hold the data for a batch detail and batch detail collection
*
* Modification History
* WI 136683 SAS 04/16/2014 
* Changes done to generate canonical xml
*
* WI 139933 SAS 05/12/2014 
* Changes done to replace ItemDataRecord element with RemittanceData record and 
* GhostDocument element added.
*
* WI 139934 SAS 05/12/2014 
* Changes done for Duplicate File and Payment detection.
*
* WI 143080 SAS 05/26/2014 
* Changes done to add RawDataRecord elements
* 
* WI 144073, 144074 CMC 5/28/2014
* Handle payment source and payment type short name
* 
* WI 148220 SAS 06/17/2014 
* Changes done for renaming TransactionCode to ElectronicTransactionCode and 
* removing Account number and Amount attribute from Payment element
*
* WI 148104 SAS 06/17/2014 
* Changes done to pass Addenda Data row by row to the RawDataRecord Constructor
* WI 169843 SAS 10/06/2014 
* Changes done to replace GhostDocument with Document Element
* WI 172990 SAS 10/17/2014 
* Changes done to set proper batch sequence numbers
*******************************************************************************/


namespace DMP.ACH.Components
{
    /// <summary>
    /// ACH Detail Collection Item class
    /// </summary>
    internal sealed class DITACHDetailObject
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        /// <summary>
        /// ACH Batch Type
        /// </summary>
        private string m_BatchType = string.Empty;
        /// <summary>
        /// Stores "Entry detail for CCD or CTX entry" value of the file structure
        /// </summary>
        private string m_RawDetailData = string.Empty;
        /// <summary>
        ///  ACH Batch Id
        /// </summary>
        private int m_ACHBatchID;
        /// <summary>
        ///  Payment Batch Sequence Number
        /// </summary>
        private int m_PaymentBatchSequenceNo;
        /// <summary>
        /// Document Batch Sequence
        /// </summary>
        private int m_DocumentBatchSequence;
        /// <summary>
        /// Document Sequence
        /// </summary>
        private int m_DocumentSequence;
        /// <summary>
        /// Document Sequence within transaction;
        /// </summary>
        private int m_DocSeqWithinTransaction;
        /// <summary>
        /// Raw Data Record Batch Sequence no;
        /// </summary>
        private int m_RawDataRecordBatchSequence;
        /// <summary>
        ///  Transaction ID
        /// </summary>
        private int m_TransactionID;
        /// <summary>
        /// Remitter Name 
        /// </summary>
        private string m_RemitterName;
        /// <summary>
        /// Is the row scanned
        /// </summary>
        private bool m_RowAdded;
        /// <summary>
        /// ACH Addenda Collection 
        /// </summary>
        private DITACHAddendaObjectCollection m_Addendas = new DITACHAddendaObjectCollection();

        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="BatchType"></param>
        public DITACHDetailObject(string BatchType, cDataImportXClientBase oDataImportXClient)
        {
            this.m_BatchType = BatchType;
            this.m_DataImportXClient = oDataImportXClient;
            this.m_TransactionID = 0;
            this.m_PaymentBatchSequenceNo = 0;
            this.m_ACHBatchID = 0;
            this.m_DocumentBatchSequence = 0;
            this.m_DocumentSequence = 0;
            this.m_DocSeqWithinTransaction = 0;
            this.m_RawDataRecordBatchSequence = 0;
        }

        #endregion Class constructors

        #region Class Properties
        /// <summary>
        /// m_RawDetailData public property
        /// </summary>
        public string RawDetailData
        {
            get { return m_RawDetailData; }
            set { m_RawDetailData = value; }
        }

        /// <summary>
        /// m_RowAdded
        /// </summary>
        public bool RowAdded
        {
            get { return m_RowAdded; }
            set { m_RowAdded = value; }
        }

        /// <summary>
        /// m_ACHBatchID public property
        /// </summary>
        public int ACHBatchID
        {
            get { return m_ACHBatchID; }
            set { m_ACHBatchID = value; }
        }


        /// <summary>
        /// Record Type
        /// </summary>
        public string RecordType
        {
            get { return DITACHCommon.GetSubString(m_RawDetailData, 0, 1); }
        }

        /// <summary>
        /// Transaction Code
        /// </summary>
        public string TransactionCode
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_RawDetailData, 1, 2);
                if (DITACHCommon.IsNumeric(sResult))
                    return sResult;
                else
                    return "5";
            }
        }

        /// <summary>
        /// Receiving ABA
        /// </summary>
        public string ABA
        {
            get { return DITACHCommon.GetSubString(m_RawDetailData, 3, 9); }
        }

        /// <summary>
        /// DDA
        /// </summary>
        public string DDA
        {
            get
            {
                switch (m_BatchType)
                {

                    case DITACHCommon.ACH_BATCH_TYPE_IAT:
                        return DITACHCommon.GetSubString(m_RawDetailData, 39, 35);
                    default:
                        return DITACHCommon.GetSubString(m_RawDetailData, 12, 17);
                }
            }
        }

        /// <summary>
        /// Account Number
        /// </summary>
        public string AccountNumber
        {
            get
            {
                switch (m_BatchType)
                {
                    case DITACHCommon.ACH_BATCH_TYPE_IAT:
                        {
                            return DITACHCommon.GetSubString(m_RawDetailData, 39, 35);
                        }
                    default:
                        {
                            return DITACHCommon.GetSubString(m_RawDetailData, 12, 17);
                        }
                }
            }

        }

        /// <summary>
        /// Amount
        /// </summary>
        public string Amount
        {
            get
            {
                string sResult = DITACHCommon.GetSubString(m_RawDetailData, 29, 10);
                if (DITACHCommon.IsNumeric(sResult))
                    return sResult;
                else
                    return "0";
            }
        }

        /// <summary>
        /// IndividualID
        /// </summary>
        public string IndividualID
        {
            get
            {
                switch (m_BatchType)
                {
                    case DITACHCommon.ACH_BATCH_TYPE_CCD:
                    case DITACHCommon.ACH_BATCH_TYPE_CTX:
                    case DITACHCommon.ACH_BATCH_TYPE_PPD:
                        {
                            return DITACHCommon.GetSubString(m_RawDetailData, 39, 15);
                        }
                    case DITACHCommon.ACH_BATCH_TYPE_CIE:
                        {
                            return DITACHCommon.GetSubString(m_RawDetailData, 54, 22);
                        }
                    default:
                        {
                            return string.Empty;
                        }
                }
            }
        }

        /// <summary>
        /// Receiving Company
        /// </summary>
        public string ReceivingCompany
        {
            get
            {
                switch (m_BatchType)
                {
                    case DITACHCommon.ACH_BATCH_TYPE_CCD:
                        {
                            return DITACHCommon.GetSubString(m_RawDetailData, 54, 22);
                        }
                    case DITACHCommon.ACH_BATCH_TYPE_CTX:
                        {
                            return DITACHCommon.GetSubString(m_RawDetailData, 58, 16);
                        }
                    default:
                        {
                            return string.Empty;
                        }
                }

            }

        }

        /// <summary>
        /// Trace Number
        /// </summary>
        public string TraceNumber
        {
            get { return DITACHCommon.GetSubString(m_RawDetailData, 79, 15); }
        }


        /// <summary>
        /// Receiving Individual (IndividualName)
        /// </summary>
        public string ReceivingIndividual
        {
            get
            {
                switch (m_BatchType)
                {
                    case DITACHCommon.ACH_BATCH_TYPE_PPD:
                        return DITACHCommon.GetSubString(m_RawDetailData, 54, 22);
                    case DITACHCommon.ACH_BATCH_TYPE_CIE:
                        return DITACHCommon.GetSubString(m_RawDetailData, 39, 15);
                    default:
                        return string.Empty;
                }
            }
        }

        /// <summary>
        /// OFAC Screening Indicator 
        /// </summary>
        public string OFACIndicator1
        {
            get
            {
                switch (m_BatchType)
                {
                    case DITACHCommon.ACH_BATCH_TYPE_IAT:
                        return DITACHCommon.GetSubString(m_RawDetailData, 76, 1);
                    default:
                        return string.Empty;
                }
            }
        }

        /// <summary>
        /// Secondary OFAC Screening Indicator
        /// </summary>
        public string OFACIndicator2
        {
            get
            {
                switch (m_BatchType)
                {
                    case DITACHCommon.ACH_BATCH_TYPE_IAT:
                        return DITACHCommon.GetSubString(m_RawDetailData, 77, 1);
                    default:
                        return string.Empty;
                }
            }

        }

        /// <summary>
        /// Payment Type
        /// </summary>
        public string PaymentType
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Remitter Name
        /// </summary>        
        public string RemitterName
        {
            get { return m_RemitterName.Trim(); }
            set { m_RemitterName = value; }
        }

        /// <summary>
        /// RT
        /// </summary>        
        public string RT
        {
            get { return string.Empty; }
        }

        /// <summary>
        ///Account
        /// </summary>        
        public string Account
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Discretionary Data
        /// </summary>
        public string DiscretionaryData
        {
            get
            {
                switch (m_BatchType)
                {
                    case DITACHCommon.ACH_BATCH_TYPE_IAT:
                        return string.Empty;
                    default:
                        return DITACHCommon.GetSubString(m_RawDetailData, 76, 2);
                }
            }
        }

        /// <summary>
        /// Bank ID
        /// </summary>
        public int BankID
        {

            get
            {
                //this.ReceivingABA  //Use this for a look up
                return 0;  //To DO need to write code to pull bank Id based on ABA,Need a web service call
            }
        }

        /// <summary>
        /// WorkGroupID 
        /// </summary>
        public int WorkGroupID
        {
            get
            {
                int iWorkGroupID = 0;
                return iWorkGroupID;
            }
        }

        /// <summary>
        /// BatchCueID 
        /// </summary>
        public int BatchCueID
        {
            get { return 0; }
        }

        /// <summary>
        /// Batch Site Code 
        /// </summary>
        public int BatchSiteCode
        {
            get
            {
                int iBatchSiteCode = 0;
                return iBatchSiteCode;
            }
        }

        /// <summary>
        /// Batch number
        /// </summary>
        public int BatchNumber
        {
            get
            {
                int iBatchNumber = 0;
                return iBatchNumber;
            }
        }

        /// <summary>
        /// Batch Track ID
        /// </summary>
        public Guid BatchTrackID
        {
            get { return Guid.NewGuid(); }
        }


        /// <summary>
        /// Batch Source 
        /// </summary>
        public string BatchSource
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Payment Batch Sequence Number
        /// </summary>
        public int PaymentBatchSequenceNo
        {
            get { return m_PaymentBatchSequenceNo; }
            set { m_PaymentBatchSequenceNo = value; }
        }

        /// <summary>
        /// Document Batch Sequence no
        /// </summary>
        public int DocumentBatchSequence
        {
            get { return m_DocumentBatchSequence; }
            set { m_DocumentBatchSequence = value; }
        }

        /// <summary>
        /// Document Sequence
        /// </summary>
        public int DocumentSequence
        {
            get { return m_DocumentSequence; }
            set { m_DocumentSequence = value; }
        }

        /// <summary>
        /// Document Sequence
        /// </summary>
        public int DocSeqWithinTransaction
        {
            get { return m_DocSeqWithinTransaction; }
            set { m_DocSeqWithinTransaction = value; }
        }

        /// <summary>
        /// RawDataRecord Batch Sequence 
        /// </summary>
        public int RawDataRecordBatchSequence
        {
            get { return m_RawDataRecordBatchSequence; }
            set { m_RawDataRecordBatchSequence = value; }
        }
        
        
        /// <summary>
        /// Check Sequence Number
        /// </summary>
        public int CheckSequence
        {
            get { return 1; }
        }


        /// <summary>
        /// Transaction ID
        /// </summary>
        public int TransactionID
        {
            get { return m_TransactionID; }
            set { m_TransactionID = value; }
        }


        /// <summary>
        /// Transaction Signature
        /// </summary>
        public string TransactionSignature
        {
            get
            {
                StringBuilder sbResult = new StringBuilder();
                sbResult.Append(this.RecordType);
                sbResult.Append(this.TransactionCode);
                sbResult.Append(this.Amount);
                sbResult.Append(this.AccountNumber);
                sbResult.Append(this.TraceNumber);
                return sbResult.ToString();
            }
        }

        /// <summary>
        /// Returns the Transaction hash
        /// </summary>
        public string TransactionHash
        {
            get { return DITACHCommon.GetHash(TransactionSignature); }
        }


        /// <summary>
        /// ReadOnly m_ACHAddendas public property
        /// </summary>
        public DITACHAddendaObjectCollection Addendas
        {
            get { return m_Addendas; }
        }

        #endregion Class Properties

        #region Class Methods

        /// <summary>
        /// Build the Batch nodes
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="dtEffectiveDate"></param>
        /// <param name="dtSettlementDate"></param>
        /// <returns></returns>
        public XmlElement BuildBatchNode(XmlDocument xDoc, DateTime dtEffectiveDate, DateTime dtSettlementDate)
        {
            XmlElement xBatchElement;
            try
            {
                Batch objBatch = new Batch();
                objBatch.ProcessingDate = dtEffectiveDate;
                objBatch.DepositDate = dtSettlementDate;
                objBatch.BankID = this.BankID;
                objBatch.ClientID = this.WorkGroupID;
                objBatch.BatchCueID = this.BatchCueID;
                objBatch.BatchID = this.ACHBatchID;
                objBatch.BatchDate = dtSettlementDate;
                objBatch.BatchSiteCode = this.BatchSiteCode; ;
                objBatch.BatchNumber = this.BatchNumber;
                objBatch.PaymentType = this.PaymentType;
                objBatch.BatchSource = this.BatchSource;
                objBatch.BatchTrackID = this.BatchTrackID;
                xBatchElement = objBatch.BuildXMLNode(xDoc);
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildBatchNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xBatchElement = null;
            }
            return xBatchElement;
        }


        /// <summary>
        /// Build nodes for Transaction
        /// 
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement BuildTransactionNode(XmlDocument xDoc)
        {
            XmlElement xTransactionElement = (XmlElement)xDoc.CreateElement("Transaction");
            try
            {
                xTransactionElement.SetAttribute("TransactionID", this.TransactionID.ToString());
                xTransactionElement.SetAttribute("TransactionSequence", this.TransactionID.ToString());
                xTransactionElement.SetAttribute("TransactionHash", TransactionHash);
                xTransactionElement.SetAttribute("TransactionSignature", this.TransactionSignature);
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildTransactionNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xTransactionElement = null;
            }
            return xTransactionElement;
        }


        /// <summary>
        /// Build Payment nodes
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement BuildPaymentNode(XmlDocument xDoc)
        {
            XmlElement xPayment = (XmlElement)xDoc.CreateElement("Payment");
            try
            {
                string sAmount = this.Amount;
                if ((Convert.ToDecimal(sAmount) != 0) && sAmount.Length > 8)
                    sAmount = sAmount.Insert(8, ".");
                xPayment.SetAttribute("BatchSequence", this.PaymentBatchSequenceNo.ToString());
                xPayment.SetAttribute("Amount", Convert.ToDecimal(sAmount).ToString());
                xPayment.SetAttribute("CheckSequence", this.CheckSequence.ToString());
                xPayment.SetAttribute("Serial", this.TraceNumber);
                xPayment.SetAttribute("RemitterName", this.RemitterName);
                xPayment.SetAttribute("RT", this.RT);
                xPayment.SetAttribute("Account", this.Account);
                xPayment.SetAttribute("TransactionCode", "");
                xPayment.SetAttribute("ABA", this.ABA.ToString());
                xPayment.SetAttribute("DDA", this.DDA.ToString());

            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildPaymentNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xPayment = null;
            }
            return xPayment;
        }

        /// <summary>
        /// Build Document Element
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement BuildDocumentNode(XmlDocument xDoc)
        {
            XmlElement xDocumentNode;
            Document objDocument = new Document();
            objDocument.BatchSequence = this.DocumentBatchSequence;
            objDocument.DocumentDescriptor = DITACHCommon.DOCUMENT_DESCRIPTOR;
            objDocument.DocumentSequence = this.DocumentSequence.ToString();
            objDocument.SequenceWithinTransaction = this.DocSeqWithinTransaction.ToString();            
            xDocumentNode = objDocument.BuildXMLNode(xDoc);
            this.Addendas.RemittanceBatchSequenceNo= this.DocumentBatchSequence;
            this.Addendas.BuildRemittanceDataElementNode(this.m_BatchType,xDoc, ref xDocumentNode);
            //Reset to the latest Batch Sequence , that will be used for the next payment within the same Batch
            this.DocumentBatchSequence= this.Addendas.RemittanceBatchSequenceNo;
            return xDocumentNode;
        }


        /// <summary>
        /// Build Raw DataRecord Element
        /// WI 148104 Changes done to add proper RawDate Record
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement BuildRawDataRecordNode(XmlDocument xDoc)
        {
            int iRawDataSequence = 0;
            XmlElement xRawDataRecordElement;

            try
            {
                RawDataRecord objRawDataRecord = new RawDataRecord();
                objRawDataRecord.BatchSequence = this.RawDataRecordBatchSequence;
                xDoc.Load(objRawDataRecord.ToRawDataRecordElement().CreateReader());
                xRawDataRecordElement = xDoc.DocumentElement;

                foreach (DITACHAddendaObject objAddendaObject in this.Addendas)
                {
                    // Skip blank addenda records, since these aren't currently handled correctly by SSIS
                    if (!string.IsNullOrEmpty(objAddendaObject.AddendaData))
                    {
                        RawData objRawData = new RawData(objAddendaObject.AddendaData, ++iRawDataSequence);
                        foreach (XElement xRawDataElement in objRawData.ToRawDataElements())
                        {
                            xDoc.Load(xRawDataElement.CreateReader());
                            xRawDataRecordElement.AppendChild(xDoc.DocumentElement);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildRawDataRecordNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xRawDataRecordElement = null;
            }
            return xRawDataRecordElement;
        }

        /// <summary>
        /// Generate Payment data elements node
        /// WI 148220 Changes done for renaming TransactionCode to ElectronicTransactionCode and 
        /// removing Account number and Amount attribute from Payment element
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecord"></param>
        public void BuildPaymentRemittanceDataNode(XmlDocument xDoc, ref XmlElement xRemittanceDataRecord)
        {
            try
            {
                this.BuildNode("ElectronicTransactionCode", this.TransactionCode, xDoc, ref xRemittanceDataRecord);                
                this.BuildNode("IndividualID", this.IndividualID, xDoc, ref xRemittanceDataRecord);
                this.BuildNode("ReceivingCompany", this.ReceivingCompany, xDoc, ref xRemittanceDataRecord);
                this.BuildNode("TraceNumber", this.TraceNumber, xDoc, ref xRemittanceDataRecord);
                this.BuildNode("IndividualName", this.ReceivingIndividual, xDoc, ref xRemittanceDataRecord);
                this.BuildNode("OFACIndicator1", this.OFACIndicator1, xDoc, ref xRemittanceDataRecord);
                this.BuildNode("OFACIndicator2", this.OFACIndicator2, xDoc, ref xRemittanceDataRecord);
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildPaymentRemittanceDataNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
            }

        }

        /// <summary>
        /// Generate Data Element for the field name and Field value passed
        /// </summary>
        /// <param name="sFieldName"></param>
        /// <param name="sFieldValue"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecord"></param>
        private void BuildNode(string sFieldName, string sFieldValue, XmlDocument xDoc, ref XmlElement xRemittanceDataRecord)
        {
            try
            {
                if (!string.IsNullOrEmpty(sFieldValue))
                {
                    RemittanceData objRemittanceData;
                    objRemittanceData = new RemittanceData();
                    objRemittanceData.FieldName = sFieldName;
                    objRemittanceData.FieldValue = sFieldValue.ToString();
                    xRemittanceDataRecord.AppendChild(objRemittanceData.BuildXMLNode(xDoc));
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
            }

        }

        #endregion Class Methods
    }

    /// <summary>
    /// ACH Detail Collection class
    /// </summary>
    internal class DITACHDetailObjectCollection : CollectionBase
    {
        /// <summary>
        /// Collection owner 
        /// </summary>
        private DITACHBatchObject m_Owner;
        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="owner">ACH Batch Object</param>
        public DITACHDetailObjectCollection(DITACHBatchObject owner)
        {
            m_Owner = owner;
        }

        /// <summary>
        /// Default Empty constructor
        /// </summary>
        public DITACHDetailObjectCollection()
        {
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITACHDetailObject this[int index]
        {
            get
            {
                return ((DITACHDetailObject)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITACHDetailObject value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITACHDetailObject value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITACHDetailObject value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITACHDetailObject value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITACHDetailObject value)
        {
            // If value is not of type ACHDetailObject, this will return false.
            return (List.Contains(value));
        }


        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.ACH.Components.DITACHDetailObject"))
                throw new ArgumentException("value must be of type ACHDetailObject.", "value");
        }



    }

    /// <summary>
    /// ACH Detail Collection filter class
    /// This class is used for filtering the  DITACHDetailObjectCollection class based on the passed values
    /// </summary>
    internal class DITACHDetailObjectCollectionFilter
    {

        /// <summary>
        /// DITACHDetailObjectCollection object
        /// </summary>
        private DITACHDetailObjectCollection m_FilteredDITACHDetailObjectCollection;

        /// <summary>
        ///class constructor
        /// </summary>
        public DITACHDetailObjectCollectionFilter()
        {
            m_FilteredDITACHDetailObjectCollection = new DITACHDetailObjectCollection();
        }

        /// <summary>
        /// Filters the collections
        /// </summary>
        /// <param name="objDITACHDetailObjectCollection"></param>
        /// <param name="iABA"></param>
        /// <param name="iDDA"></param>
        /// <param name="bRowAdded"></param>
        /// <returns></returns>
        public DITACHDetailObjectCollection Filter(DITACHDetailObjectCollection objDITACHDetailObjectCollection, string sABA, string sDDA, bool bRowAdded)
        {
            foreach (DITACHDetailObject objDITACHDetailObject in objDITACHDetailObjectCollection)
            {
                if (objDITACHDetailObject.ABA == sABA && objDITACHDetailObject.DDA == sDDA && objDITACHDetailObject.RowAdded == bRowAdded)
                    m_FilteredDITACHDetailObjectCollection.Add(objDITACHDetailObject);
            }
            return m_FilteredDITACHDetailObjectCollection;
        }
    }
}