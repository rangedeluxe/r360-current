﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using WFS.LTA.DataImport.DataImportClientSvcAPI;
using WFS.LTA.Common;
using WFS.integraPAY.Online.Common;
using Timers=System.Timers;

namespace WFS.LTA.DataImport.DITLoader
    {
    public partial class frmConfig : Form
        {
        private cDITFileProcessor mdfpLoader;
        private ltaLog _logLogger;
        private Timers.Timer _tmrLogUpdater;
        private bool bShowConfigOnStartup = false;

        private cClientOptions _ClientOptions = null;

        protected cClientOptions ClientOptions {
            get {
                if(_ClientOptions == null) {
                    _ClientOptions = new cClientOptions();
                }
                return(_ClientOptions);
            }
        }

        public frmConfig()
            {

            InitializeComponent();

            /*_mmlLogger = new cMemoryLog(sttSettings.LogFile.FullName, sttSettings.LogFileMaxSize, 
                    sttSettings.LoggingLevel);
            */
            _logLogger = new ltaLog(
                ClientOptions.logFilePath, 
                ClientOptions.logFileMaxSize, 
                ClientOptions.LTALoggingDepth);
            try
                {
                string sErrorMessage;

                mdfpLoader = new cDITFileProcessor();
                if (!mdfpLoader.Initialize(_logLogger, out sErrorMessage))
                    {
                    MessageBox.Show(sErrorMessage);
                    bShowConfigOnStartup = true;
                    }
                }
            catch (Exception ex)
                {
                MessageBox.Show(ex.Message);
                bShowConfigOnStartup = true;
                }
            _tmrLogUpdater = new Timers.Timer(500.0);
            _tmrLogUpdater.Enabled = true;

            ////prgSettings.SelectedObject = mdfpLoader.Settings;
            chkTimer.Checked = mdfpLoader.ProcessFiles;            
            }

        /*
        void _tmrLogUpdater_Elapsed(object sender, Timers.ElapsedEventArgs e)
            {
            UpdateList(_logLogger.LogEntries, lstLog);
            }
        */

        private void mnuConfig_Click(object sender, EventArgs e)
            {
            this.Visible = true;
            this.Focus();
            }

        private void frmConfig_Shown(object sender, EventArgs e)
            {
            this.Visible = bShowConfigOnStartup;
            }

        private void cmdOK_Click(object sender, EventArgs e)
            {
            this.Visible = false;
            }

        private void cmsIconMenu_MouseClick(object sender, MouseEventArgs e)
            {
            this.Visible = true;
            this.Focus();
            }

        private void frmConfig_FormClosing(object sender, FormClosingEventArgs e)
            {
            ntiTray.Dispose();
            mdfpLoader.Dispose();
            }

        private void cmdEndLoader_Click(object sender, EventArgs e)
            {
            this.Close();
            }

        private void chkTimer_CheckedChanged(object sender, EventArgs e)
            {
            mdfpLoader.ProcessFiles = chkTimer.Checked;
            }

        private void mnuEnd_Click(object sender, EventArgs e)
            {
            this.Close();
            }

        private void UpdateList(List<string> lstData, ListBox lstControl)
            {
            if(!this.IsDisposed)
                if (InvokeRequired)
                    {
                    this.Invoke(new MethodInvoker(delegate { UpdateList(lstData, lstControl); }));
                    }
                else
                    {
                    lstControl.Items.Clear();
                    lstControl.Items.AddRange(lstData.ToArray());
                    //if (lstLog.Items.Count > 0)
                    //    lstLog.SelectedItem = lstLog.Items[lstLog.Items.Count - 1];
                    }
            }
        }
    }
