﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WFS.LTA.DataImport.DITLoader
    {
    public interface IDITConverter
        {
        string TranslateFileContent(string sFileContent);
        }
    }
