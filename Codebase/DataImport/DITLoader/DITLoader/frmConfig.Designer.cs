﻿namespace WFS.LTA.DataImport.DITLoader
{
    partial class frmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfig));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cmdEndLoader = new System.Windows.Forms.Button();
            this.imlPics = new System.Windows.Forms.ImageList(this.components);
            this.chkTimer = new System.Windows.Forms.CheckBox();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.prgSettings = new System.Windows.Forms.PropertyGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ntiTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.cmsIconMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEnd = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdOK = new System.Windows.Forms.Button();
            this.tmrLogUpdater = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.cmsIconMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(9, 10);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cmdEndLoader);
            this.splitContainer1.Panel1.Controls.Add(this.chkTimer);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabMain);
            this.splitContainer1.Size = new System.Drawing.Size(473, 205);
            this.splitContainer1.SplitterDistance = 141;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 0;
            // 
            // cmdEndLoader
            // 
            this.cmdEndLoader.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdEndLoader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdEndLoader.ImageIndex = 0;
            this.cmdEndLoader.ImageList = this.imlPics;
            this.cmdEndLoader.Location = new System.Drawing.Point(3, 161);
            this.cmdEndLoader.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmdEndLoader.Name = "cmdEndLoader";
            this.cmdEndLoader.Size = new System.Drawing.Size(100, 41);
            this.cmdEndLoader.TabIndex = 1;
            this.cmdEndLoader.Text = "End Loader";
            this.cmdEndLoader.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdEndLoader.UseVisualStyleBackColor = true;
            this.cmdEndLoader.Click += new System.EventHandler(this.cmdEndLoader_Click);
            // 
            // imlPics
            // 
            this.imlPics.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlPics.ImageStream")));
            this.imlPics.TransparentColor = System.Drawing.Color.Transparent;
            this.imlPics.Images.SetKeyName(0, "exit.png");
            // 
            // chkTimer
            // 
            this.chkTimer.AutoSize = true;
            this.chkTimer.Checked = true;
            this.chkTimer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTimer.Location = new System.Drawing.Point(2, 2);
            this.chkTimer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkTimer.Name = "chkTimer";
            this.chkTimer.Size = new System.Drawing.Size(97, 17);
            this.chkTimer.TabIndex = 0;
            this.chkTimer.Text = "Watch for Files";
            this.chkTimer.UseVisualStyleBackColor = true;
            this.chkTimer.CheckedChanged += new System.EventHandler(this.chkTimer_CheckedChanged);
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPage1);
            this.tabMain.Controls.Add(this.tabPage2);
            this.tabMain.Controls.Add(this.tabPage3);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(329, 205);
            this.tabMain.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.prgSettings);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Size = new System.Drawing.Size(321, 179);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Config";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // prgSettings
            // 
            this.prgSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prgSettings.HelpVisible = false;
            this.prgSettings.Location = new System.Drawing.Point(2, 2);
            this.prgSettings.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.prgSettings.Name = "prgSettings";
            this.prgSettings.Size = new System.Drawing.Size(317, 175);
            this.prgSettings.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Size = new System.Drawing.Size(321, 179);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "File Queue";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Size = new System.Drawing.Size(321, 179);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Log";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ntiTray
            // 
            this.ntiTray.BalloonTipText = "DIT Loader";
            this.ntiTray.BalloonTipTitle = "DIT Loader";
            this.ntiTray.ContextMenuStrip = this.cmsIconMenu;
            this.ntiTray.Icon = ((System.Drawing.Icon)(resources.GetObject("ntiTray.Icon")));
            this.ntiTray.Text = "DIT Loader";
            this.ntiTray.Visible = true;
            // 
            // cmsIconMenu
            // 
            this.cmsIconMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuConfig,
            this.mnuEnd});
            this.cmsIconMenu.Name = "cmsIconMenu";
            this.cmsIconMenu.Size = new System.Drawing.Size(154, 52);
            this.cmsIconMenu.Click += new System.EventHandler(this.mnuConfig_Click);
            this.cmsIconMenu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmsIconMenu_MouseClick);
            // 
            // mnuConfig
            // 
            this.mnuConfig.Image = global::DITLoader.Properties.Resources.preferences_system;
            this.mnuConfig.Name = "mnuConfig";
            this.mnuConfig.Size = new System.Drawing.Size(153, 24);
            this.mnuConfig.Text = "Configure";
            this.mnuConfig.Click += new System.EventHandler(this.mnuConfig_Click);
            // 
            // mnuEnd
            // 
            this.mnuEnd.Image = global::DITLoader.Properties.Resources.exit;
            this.mnuEnd.Name = "mnuEnd";
            this.mnuEnd.Size = new System.Drawing.Size(153, 24);
            this.mnuEnd.Text = "End Loader";
            this.mnuEnd.Click += new System.EventHandler(this.mnuEnd_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdOK.Location = new System.Drawing.Point(425, 223);
            this.cmdOK.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(56, 19);
            this.cmdOK.TabIndex = 1;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = true;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // tmrLogUpdater
            // 
            this.tmrLogUpdater.Enabled = true;
            this.tmrLogUpdater.Interval = 1000;
            // 
            // frmConfig
            // 
            this.AcceptButton = this.cmdOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdOK;
            this.ClientSize = new System.Drawing.Size(491, 252);
            this.ControlBox = false;
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfig";
            this.ShowInTaskbar = false;
            this.Text = "DIT Loader Config";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmConfig_FormClosing);
            this.Shown += new System.EventHandler(this.frmConfig_Shown);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.cmsIconMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PropertyGrid prgSettings;
        private System.Windows.Forms.NotifyIcon ntiTray;
        private System.Windows.Forms.ContextMenuStrip cmsIconMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuConfig;
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.CheckBox chkTimer;
        private System.Windows.Forms.Button cmdEndLoader;
        private System.Windows.Forms.ImageList imlPics;
        private System.Windows.Forms.ToolStripMenuItem mnuEnd;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Timer tmrLogUpdater;
    }
}

