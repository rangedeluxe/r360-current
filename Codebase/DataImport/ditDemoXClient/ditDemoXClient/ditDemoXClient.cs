﻿using System;
using System.Collections.Generic;
using System.Text;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.IO;
using System.Xml;

/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2011.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    8/23/2012
*
* Purpose: 
*
* Modification History
* 8/23/2012 CR 53274  WJS
*     - Initial release.
* 
********************************************************************************/
namespace WFS.integraPAY.Online.ditDemoXClient
{
    public class ditDemoXClient : cDataImportXClientBase, IDITBatch 
    {

        public ditDemoXClient() : base()
        {
            DoLogEvent("Constructor started ditDemoXClient", "DITXClientBase", LogEventType.Information, LogEventImportance.Verbose);
        }
        public override bool BatchStreamToXML(StreamReader stmInputFile, cConfigData cfgSettings, out XmlDocument xmdResult)
        {
            bool bRetval = false;
            xmdResult = new XmlDocument();
            try
            {

                DoLogEvent("Batch StreamTO XML Called started", "DITXClientBase", LogEventType.Information, LogEventImportance.Essential);
                string xmlString = stmInputFile.ReadToEnd();
                xmdResult.LoadXml(xmlString);
                bRetval = true;
            }
            catch (Exception ex)
            {
                DoLogEvent("Failed to load xml. Exception is:" + ex.Message);
                throw ex;
            }
            return bRetval;
        }
     
    }
}
