﻿using WFS.integraPAY.Online.ditDemoXClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Xml;
/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2011.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    8/23/2012
*
* Purpose: 
*
* Modification History
* 8/23/2012 CR 53274  WJS
*     - Initial release.
* 
********************************************************************************/
namespace DitDemoXClientTest
{
    
    
    /// <summary>
    ///This is a test class for ditDemoXClientTest and is intended
    ///to contain all ditDemoXClientTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ditDemoXClientTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BatchStreamToXML
        ///</summary>
        [TestMethod()]
        public void BatchStreamToXMLTest()
        {
            ditDemoXClient target = new ditDemoXClient();
          
            cConfigData cfgSettings = null; 
            XmlDocument xmdResult = null;  
            bool expected = true; 
            bool actual;
            using (StreamReader stmInputFile = new StreamReader("..\\..\\..\\DitDemoXClientTest\\DITDemoXClientTest.xml"))
            {
                actual = target.BatchStreamToXML(stmInputFile, cfgSettings, out xmdResult);
                Assert.AreEqual(expected, actual);
            }
        }
    }
}
