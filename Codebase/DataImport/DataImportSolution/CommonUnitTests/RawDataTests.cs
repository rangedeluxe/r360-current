﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportClientAPI;
using System.Linq;
using System.Xml.Linq;

namespace CommonUnitTests
{
    [TestClass]
    public class RawDataTests
    {
        private const int ChunkSize = RawData.ChunkSize;
        private const int InputSequence = 99;

        private void CheckElementPropertyValues<TValue>(IEnumerable<XElement> elements,
            IEnumerable<TValue> expectedPropertyValues, Func<XElement, TValue> getPropertyValue)
        {
            Assert.IsNotNull(elements, "elements");
            Assert.IsNotNull(expectedPropertyValues, "expectedPropertyValues");

            var actualPropertyValues = elements.Select(getPropertyValue);

            var expected = string.Join(",", expectedPropertyValues);
            var actual = string.Join(",", actualPropertyValues);
            Assert.AreEqual(expected, actual);
        }
        private void CheckElementRawDataParts(IEnumerable<XElement> elements, IEnumerable<string> expectedRawDataParts)
        {
            CheckElementPropertyValues(elements, expectedRawDataParts, element => element.Attribute("RawDataPart").Value);
        }
        private void CheckElementRawSequences(IEnumerable<XElement> elements, IEnumerable<int> expectedRawSequences)
        {
            CheckElementPropertyValues(elements,
                expectedRawSequences.Select(rawSequence => rawSequence.ToString()),
                element => element.Attribute("RawSequence").Value);
        }
        private void CheckElementValueLengths(IEnumerable<XElement> elements, IEnumerable<int> expectedValueLengths)
        {
            CheckElementPropertyValues(elements, expectedValueLengths, element => element.Value.Length);
        }
        private void CheckElementValues(IEnumerable<XElement> elements, IEnumerable<string> expectedValues)
        {
            CheckElementPropertyValues(elements, expectedValues, element => element.Value);
        }
        private RawData CreateRawData(string unchunkedData)
        {
            return new RawData(unchunkedData, InputSequence);
        }

        [TestMethod]
        public void RawData_ContainsLessThan128Characters_ShouldCreateOnlyOneRawDataElement()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize - 1));

            var elements = rawData.ToRawDataElements();

            CheckElementValueLengths(elements, new[] {ChunkSize - 1});
        }

        [TestMethod]
        public void RawData_ContainsExactly128Characters_ShouldCreateOnlyOneRawDataElement()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize));

            var elements = rawData.ToRawDataElements();

            CheckElementValueLengths(elements, new[] {ChunkSize});
        }

        [TestMethod]
        public void RawData_ContainsExactly128Characters_RawDataPartsShouldCountFromOne()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize));

            var elements = rawData.ToRawDataElements();

            CheckElementRawDataParts(elements, new[] {"1"});
        }

        [TestMethod]
        public void RawData_ContainsExactly128Characters_RawSequenceShouldBeSameAsInputSequence()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize));

            var elements = rawData.ToRawDataElements();

            CheckElementRawSequences(elements, new[] {InputSequence});
        }

        [TestMethod]
        public void RawData_ContainsAmpersandCharacter_ShouldNotEscapeTheAmpersandInTheXmlElement()
        {
            var rawData = CreateRawData("&");

            var elements = rawData.ToRawDataElements();

            CheckElementValues(elements, new[] {"&"});
        }

        [TestMethod]
        public void RawData_ContainsLessThanCharacter_ShouldNotEscapeTheLessThanCharacterInTheXmlElement()
        {
            var rawData = CreateRawData("<");

            var elements = rawData.ToRawDataElements();

            CheckElementValues(elements, new[] {"<"});
        }

        [TestMethod]
        public void RawData_ContainsGreaterThanCharacter_ShouldNotEscapeTheGreaterThanCharacterInTheXmlElement()
        {
            var rawData = CreateRawData(">");

            var elements = rawData.ToRawDataElements();

            CheckElementValues(elements, new[] {">"});
        }

        [TestMethod]
        public void RawData_ContainsDoubleQuoteCharacter_ShouldNotEscapeTheQuoteCharacterInTheXmlElement()
        {
            var rawData = CreateRawData("\"");

            var elements = rawData.ToRawDataElements();

            CheckElementValues(elements, new[] {"\""});
        }

        [TestMethod]
        public void RawData_ContainsApostropheCharacter_ShouldNotEscapeTheApostropheCharacterInTheXmlElement()
        {
            var rawData = CreateRawData("'");

            var elements = rawData.ToRawDataElements();

            CheckElementValues(elements, new[] {"'"});
        }

        [TestMethod]
        public void RawData_ContainsBetween128And256Characters_ShouldCreateTwoRawDataElements()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize) +
                new string('b', ChunkSize - 1));

            var elements = rawData.ToRawDataElements();

            CheckElementValueLengths(elements, new[] {ChunkSize, ChunkSize - 1});
        }

        [TestMethod]
        public void RawData_ContainsBetween128And256Characters_RawDataPartsShouldCountFromOne()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize) +
                new string('b', ChunkSize - 1));

            var elements = rawData.ToRawDataElements();

            CheckElementRawDataParts(elements, new[] {"1", "2"});
        }

        [TestMethod]
        public void RawData_ContainsBetween128And256Characters_RawSequenceShouldBeSameAsInputSequence()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize) +
                new string('b', ChunkSize - 1));

            var elements = rawData.ToRawDataElements();

            CheckElementRawSequences(elements, new[] {InputSequence, InputSequence});
        }

        [TestMethod]
        public void RawData_ContainsBetween256And384Characters_ShouldCreateThreeRawDataElements()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize) +
                new string('b', ChunkSize) +
                new string('c', ChunkSize - 1));

            var elements = rawData.ToRawDataElements();

            CheckElementValueLengths(elements, new[] {ChunkSize, ChunkSize, ChunkSize - 1});
        }

        [TestMethod]
        public void RawData_ContainsBetween256And384Characters_RawDataPartsShouldCountFromOne()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize) +
                new string('b', ChunkSize) +
                new string('c', ChunkSize - 1));

            var elements = rawData.ToRawDataElements();

            CheckElementRawDataParts(elements, new[] {"1", "2", "3"});
        }

        [TestMethod]
        public void RawData_ContainsBetween256And384Characters_RawSequenceShouldBeSameAsInputSequence()
        {
            var rawData = CreateRawData(
                new string('a', ChunkSize) +
                new string('b', ChunkSize) +
                new string('c', ChunkSize - 1));

            var elements = rawData.ToRawDataElements();

            CheckElementRawSequences(elements, new[] {InputSequence, InputSequence, InputSequence});
        }

        [TestMethod]
        public void RawData_ContainsAnEmptyString_ShouldReturnASingleEmptyChunk()
        {
            var rawData = CreateRawData("");

            var elements = rawData.ToRawDataElements();

            CheckElementValueLengths(elements, new[] {0});
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RawData_ContainsNullString_ShouldThrowAnArgumentNullException()
        {
            var rawData = CreateRawData(null);

            rawData.ToRawDataElements();

            Assert.Fail("Should have failed with an ArgumentNullException");
        }
    }
}
