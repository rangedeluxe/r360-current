﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonUnitTests
{
    public static class Helper
    {
        public static string BuildStringValue(int size, char characterValue)
        {
            StringBuilder stringValue = new StringBuilder();

            for (int i = 0; i < size; i++)
            {
                stringValue.Append(characterValue);
            }

            return stringValue.ToString();
        }
    }
}
