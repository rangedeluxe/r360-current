﻿using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportClientAPI;

namespace CommonUnitTests
{
    [TestClass]
    public class ImgTests
    {
        [TestMethod]
        public void AllowMultiPageTiff_WhenFalse_IsOutputAsLowercaseAttributeValue()
        {
            var xmlDocument = new XmlDocument();
            var img = new Img {AllowMultiPageTiff = false};
            var xmlNode = img.BuildXMLNode(xmlDocument);
            Assert.AreEqual("false", xmlNode.Attributes["AllowMultiPageTiff"].Value,
                "Lowercase 'false' is required to pass XSD validation");
        }
        [TestMethod]
        public void AllowMultiPageTiff_WhenTrue_IsOutputAsLowercaseAttributeValue()
        {
            var img = new Img {AllowMultiPageTiff = true};
            var xmlDocument = new XmlDocument();
            var xmlNode = img.BuildXMLNode(xmlDocument);
            Assert.AreEqual("true", xmlNode.Attributes["AllowMultiPageTiff"].Value,
                "Lowercase 'true' is required to pass XSD validation");
        }
    }
}
