﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using WFS.LTA.DataImport.DataImportClientAPI;
using System.Collections.Generic;
using System.Linq;

namespace CommonUnitTests
{
    [TestClass]
    public class ChunkerTests
    {
        private int chunkSize = 128;

        [TestMethod]
        public void UnchunkedData_IsEmpty_ShouldCreateOneEmptyItemInTheList()
        {
            var chunkedData = Chunker.ChunkData("", chunkSize).ToList();

            Assert.AreEqual(1, chunkedData.Count, "Count");
            Assert.AreEqual("", chunkedData[0], "First item");
        }
        [TestMethod]
        public void UnchunkedData_ContainsLessCharactersThanCharacterChunkSize_ShouldCreateOnlyOneItemInTheList()
        {
            //arrange
            StringBuilder unchunkedData =
                new StringBuilder()
                    .Append(Helper.BuildStringValue(chunkSize - 1, 'a'));

            //act
            var chunkedData = new List<string>(Chunker.ChunkData(unchunkedData.ToString(), chunkSize));

            //assert
            Assert.IsTrue(chunkedData.Count == 1);
            Assert.AreEqual(chunkedData[0].Length, unchunkedData.Length);
        }

        [TestMethod]
        public void UnchunkedData_WithZeroChunkSize_ShouldCreateOneItemInTheListThatMatchesTheLengthOfTheUnchunkedData()
        {
            //arrange
            StringBuilder unchunkedData =
                new StringBuilder()
                    .Append(Helper.BuildStringValue(chunkSize * 2, 'a'));

            //act
            var chunkedData = new List<string>(Chunker.ChunkData(unchunkedData.ToString(), 0));

            //assert
            Assert.IsTrue(chunkedData.Count == 1);
            Assert.AreEqual(chunkedData[0].Length, unchunkedData.Length);
        }

        [TestMethod]
        public void UnchunkedData_ContainsSameAmountOfCharactersAsChunkSize_ShouldCreateOnlyOneItemInTheList()
        {
            //arrange
            StringBuilder unchunkedData =
                new StringBuilder()
                    .Append(Helper.BuildStringValue(chunkSize, 'a'));


            //act
            var chunkedData = new List<string>(Chunker.ChunkData(unchunkedData.ToString(), 0));
            
            //assert
            Assert.IsTrue(chunkedData.Count == 1);
            Assert.AreEqual(chunkedData[0].Length, chunkSize);
        }

        [TestMethod]
        public void UnchunkedData_ContainsBetweenOneAndTwoChunksWorthOfCharacters_ShouldCreateTwoItemsInTheList()
        {
            //arrange
            StringBuilder unchunkedData =
                new StringBuilder()
                    .Append(Helper.BuildStringValue(chunkSize, 'a'))
                    .Append(Helper.BuildStringValue(chunkSize - 1, 'b'));

            int lengthCheck = 0;

            //act
            var chunkedData = new List<string>(Chunker.ChunkData(unchunkedData.ToString(), chunkSize));
            chunkedData.ForEach(chunk => lengthCheck += chunk.Length);

            //assert
            Assert.IsTrue(chunkedData.Count == 2);
            Assert.AreEqual(lengthCheck, unchunkedData.Length);
        }

        [TestMethod]
        public void UnchunkedData_ContainsBetweenTwoAndThreeChunksWorthOfCharacters_ShouldCreateThreeItemsInTheList()
        {
            //arrange
            StringBuilder unchunkedData =
                new StringBuilder()
                    .Append(Helper.BuildStringValue(chunkSize, 'a'))
                    .Append(Helper.BuildStringValue(chunkSize, 'b'))
                    .Append(Helper.BuildStringValue(chunkSize - 1, 'c'));

            int lengthCheck = 0;

            //act
            var chunkedData = new List<string>(Chunker.ChunkData(unchunkedData.ToString(), chunkSize));
            chunkedData.ForEach(chunk => lengthCheck += chunk.Length);

            //assert
            Assert.IsTrue(chunkedData.Count == 3);
            Assert.AreEqual(lengthCheck, unchunkedData.Length);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UnchunkedData_ContainsNullString_ShouldThrowAnArgumentNullException()
        {
            //arrange

            //act
            var chunkedData = new List<string>(Chunker.ChunkData(null, chunkSize));

            //assert
            Assert.Fail("Should have failed with an ArgumentNullException");
        }
    }
}
