The projects in this folder must be built as .NET 4.5.1. No upgrading!

On our WEB, APP, FILE, and CLIENT-* tiers, our system requirements are for .NET
4.6.x. But the XML-shredder projects will run from SSIS on the database server,
where we have no such prerequisites. We know .NET 4.5.1 will be available
(preinstalled with Windows Server 2012 R2, and also required for our PowerShell
install scripts), so that's what these projects can safely rely on.