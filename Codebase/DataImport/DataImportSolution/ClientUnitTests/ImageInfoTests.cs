﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportClientSvcAPI;
using WFS.LTA.Common;
using System.IO;
using System.Net.Configuration;

namespace ClientUnitTests
{
    public class LogInfo
    {
        public string LogMessage { get; }
        public string LogSource { get; }
        public LTAMessageType MessageType { get; }
        public LTAMessageImportance MessageImportance { get; }
        public LogInfo(string logMessage, string logSource, LTAMessageType messageType, LTAMessageImportance messageImportance)
        {
            this.LogMessage = logMessage;
            this.LogSource = logSource;
            this.MessageType = messageType;
            this.MessageImportance = messageImportance;
        }
    }
    [TestClass]
    public class ImageInfoTests
    {
        private string unknownImageFile = string.Format(@"{0}\Supporting\unknown.tif", AppDomain.CurrentDomain.BaseDirectory);
        private string emptyImageFile = string.Format(@"{0}\Supporting\empty image.tif", AppDomain.CurrentDomain.BaseDirectory);
        private string singlePageFrontImageFile = string.Format(@"{0}\Supporting\single-page front image.tif", AppDomain.CurrentDomain.BaseDirectory);
        private string singlePageBackImageFile = string.Format(@"{0}\Supporting\single-page back image.tif", AppDomain.CurrentDomain.BaseDirectory);
        private string multiPageImageFile = string.Format(@"{0}\Supporting\multi-page image.tif", AppDomain.CurrentDomain.BaseDirectory);
        private string renamedJpegImageFile = string.Format(@"{0}\Supporting\renamed jpeg.tif", AppDomain.CurrentDomain.BaseDirectory);
        private string onePagePlusOldStyleGrayscaleImageFile = string.Format(@"{0}\Supporting\One-Page plus Old-Style Grayscale.tif", AppDomain.CurrentDomain.BaseDirectory);
        private string twoPagePlusOldStyleGrayscaleImageFile = string.Format(@"{0}\Supporting\Two-Page plus Old-Style Grayscale.tif", AppDomain.CurrentDomain.BaseDirectory);

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreatingInstance_WithNullImagePath_ShouldFail()
        {
            //arrange
            IImageInfo imageInfo;

            //act
            imageInfo = ImageInfo.Create(string.Empty);
            imageInfo.Dispose();

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void ImageInfo_CreatingInstanceWithFileThatDoesNotExist_ShouldFailWithFileNotFoundException()
        {
            //arrange
            IImageInfo imageInfo;

            //act
            imageInfo = ImageInfo.Create(unknownImageFile);
            imageInfo.Dispose();

            //assert
        }

        [TestMethod]
        public void ImageInfo_GetNumberOfPages_ShouldHaveNumberOfPagesEqualToTwo()
        {
            //arrange

            //act
            using (var imageInfo = ImageInfo.Create(multiPageImageFile))
            {
                Assert.IsTrue(imageInfo.NumberOfPages > 0);
                Assert.IsTrue(imageInfo.NumberOfPages == 2);
            }
        }

        [TestMethod]
        public void ImageInfo_CreateFrontPageWhenThereAreTwoPages_ShouldCreateFrontPage()
        {
            //arrange
            var expected = 9316;

            //act
            using (var imageInfo = ImageInfo.Create(multiPageImageFile))
            using (MemoryStream stream = imageInfo.GetFrontPageStream() as MemoryStream)
            {
                byte[] data = stream.ToArray();

                //assert
                Assert.AreEqual(expected, data.Length);
            }
        }

        [TestMethod]
        public void ImageInfo_CreateBackPageWhenThereAreTwoPages_ShouldCreateBackPage()
        {
            //arrange
            var expected = 2028;

            //act
            using (var imageInfo = ImageInfo.Create(multiPageImageFile))
            using (MemoryStream stream = imageInfo.GetBackPageStream() as MemoryStream)
            {
                byte[] data = stream.ToArray();

                //assert
                Assert.AreEqual(expected, data.Length);
            }
        }

        [TestMethod]
        public void ImageInfo_CreateBackPageWhenThereIsOnlyOnePage_ShouldCreateBackPageFromPageOne()
        {
            // If an XClient returns XML that has an image with Page="1" (back),
            // DIT will call GetBackPageStream. So if the image is single-page,
            // GetBackPageStream had better return the image, not an empty stream.

            //arrange
            var expected = 14928;

            //act
            using (var imageInfo = ImageInfo.Create(singlePageBackImageFile))
            using (MemoryStream stream = imageInfo.GetBackPageStream() as MemoryStream)
            {
                var actual = stream.Length;

                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void ImageInfo_CreateFrontPageWhenThereAreNoPages_ShouldBeEmpty()
        {
            //arrange

            //act
            using (var imageInfo = ImageInfo.Create(emptyImageFile))
            using (MemoryStream stream = imageInfo.GetFrontPageStream() as MemoryStream)
            {
                byte[] data = stream.ToArray();

                //assert
                Assert.IsTrue(data.Length == 0);
            }
        }
        [TestMethod]
        public void ImageInfo_JpegRenamedToTif_NumberOfPages()
        {
            using (var imageInfo = ImageInfo.Create(renamedJpegImageFile))
            {
                Assert.AreEqual(1, imageInfo.NumberOfPages);
            }
        }
        [TestMethod]
        public void ImageInfo_JpegRenamedToTif_FrontPageStream()
        {
            const int expectedSize = 1326;
            using (var imageInfo = ImageInfo.Create(renamedJpegImageFile))
            using (var stream = imageInfo.GetFrontPageStream())
            {
                Assert.AreEqual(expectedSize, stream.Length);
            }
        }
        [TestMethod]
        public void ImageInfo_JpegRenamedToTif_BackPageStream_IsSameAsFrontPageStream()
        {
            const int expectedSize = 1326;
            using (var imageInfo = ImageInfo.Create(renamedJpegImageFile))
            using (var stream = imageInfo.GetBackPageStream())
            {
                Assert.AreEqual(expectedSize, stream.Length);
            }
        }
        [TestMethod]
        public void ImageInfo_OnePagePlusOldStyleGrayscale()
        {
            // The ImageRPS operator had to view the grayscale image to make out details,
            // so ImageRPS added the grayscale to the stacked TIFF as an additional page.
            // The grayscale uses deprecated TIFF compression format 6 (old-style JPEG),
            // which isn't supported by the WPF image library, but is by System.Drawing.
            // In this test we have 1 page bitonal + 1 page grayscale.
            using (var imageInfo = ImageInfo.Create(onePagePlusOldStyleGrayscaleImageFile))
            {
                using (var stream = imageInfo.GetFrontPageStream())
                {
                    Assert.AreEqual(4386, stream.Length, "Front page stream size");
                }
                using (var stream = imageInfo.GetBackPageStream())
                {
                    Assert.AreEqual(0, stream.Length, "Back page (grayscale) stream size");
                }
            }
        }
        [TestMethod]
        public void ImageInfo_TwoPagePlusOldStyleGrayscale()
        {
            // The ImageRPS operator had to view the grayscale image to make out details,
            // so ImageRPS added the grayscale to the stacked TIFF as an additional page.
            // The grayscale uses deprecated TIFF compression format 6 (old-style JPEG),
            // which isn't supported by the WPF image library, but is by System.Drawing.
            // In this test we have 2 pages bitonal (front and back) + 1 page grayscale
            // (which R360 will ignore, as long as it can load the image in the first place).
            using (var imageInfo = ImageInfo.Create(twoPagePlusOldStyleGrayscaleImageFile))
            {
                using (var stream = imageInfo.GetFrontPageStream())
                {
                    Assert.AreEqual(4386, stream.Length, "Front page stream size");
                }
                using (var stream = imageInfo.GetBackPageStream())
                {
                    Assert.AreEqual(9592, stream.Length, "Back page stream size");
                }
            }
        }
        [TestMethod]
        public void ImageInfo_TestLogOnePagePlusOldStyleGrayscale()
        {
            List<LogInfo> logInfo = new List<LogInfo>();
            // setup OutputLog to our test logger
            Action<string, string, LTAMessageType, LTAMessageImportance> logger = (msg, src, messageType, messageImportance) =>
            {
                logInfo.Add(new LogInfo(msg, src, messageType, messageImportance));
            };
            try
            {
                GblUtil.OutputLog += logger;

                var imageInfo = ImageInfo.Create(onePagePlusOldStyleGrayscaleImageFile);

                StringAssert.StartsWith(logInfo[0].LogMessage, "Image load: Unable to load image", "Image load");
                StringAssert.EndsWith(logInfo[0].LogMessage, "The image might be non-standard.. Retrying with System.Drawing.", "Retry Info");
                Assert.AreEqual(LTAMessageType.Information, logInfo[0].MessageType, "Message Type");
            }
            finally
            {
                // remove the test logger
                GblUtil.OutputLog -= logger;
            }
        }
        [TestMethod]
        public void ImageInfo_TestLogTwoPagePlusOldStyleGrayscale()
        {
            List<LogInfo> logInfo = new List<LogInfo>();
            // setup OutputLog to our test logger
            Action < string, string, LTAMessageType, LTAMessageImportance> logger = (msg,src,messageType,messageImportance) =>
            {
                logInfo.Add(new LogInfo(msg, src, messageType, messageImportance));
            };
            try
            {
                GblUtil.OutputLog += logger;

                var imageInfo = ImageInfo.Create(twoPagePlusOldStyleGrayscaleImageFile);

                StringAssert.StartsWith(logInfo[0].LogMessage, "Image load: Unable to load image", "Image load");
                StringAssert.EndsWith(logInfo[0].LogMessage, "The image might be non-standard.. Retrying with System.Drawing.", "Retry Info");
                Assert.AreEqual(LTAMessageType.Information, logInfo[0].MessageType, "Message Type");
            }
            finally
            {
                // remove the test logger
                GblUtil.OutputLog -= logger;
            }
        }
    }
}
