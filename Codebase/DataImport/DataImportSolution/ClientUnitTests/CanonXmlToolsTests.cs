﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.IO;
using System.Linq;
using WFS.LTA.DataImport.DataImportClientSvcAPI;

namespace ClientUnitTests
{
    [TestClass]
    public class CanonXmlToolsTests
    {
        private string rpsCanonical = string.Format(@"{0}\Supporting\rps canonical.xml", AppDomain.CurrentDomain.BaseDirectory);
        private string genericCanonical = string.Format(@"{0}\Supporting\generic canonical.xml", AppDomain.CurrentDomain.BaseDirectory);
        private string genericDdaLookup = string.Format(@"{0}\Supporting\GenericDDALookup.xml", AppDomain.CurrentDomain.BaseDirectory);

        [TestMethod]
        public void CanonTools_WhenNoImagesElementExists_ToImageElementListFunctionShouldReturnEmptyList()
        {
            //arrange
            var expected = 0;
            
            //act
            using (Stream stream = new FileStream(genericCanonical, FileMode.Open))
            {
                var batches = XElement.Load(stream);
                var imageInfoList = cCanonXMLTools.ToImageElementList(batches.Elements().FirstOrDefault());
                var actual = imageInfoList.Count();
                
                //assert
                Assert.AreEqual(expected, actual);
            }
            

        }

        [TestMethod]
        public void CanonTools_WhenBatchElementIsNotSentIn_ToImageInfoContractListFunctionShouldReturnEmptyList()
        {
            //arrange
            var expected = 0;

            //act
            using (Stream stream = new FileStream(genericCanonical, FileMode.Open))
            {
                var batches = XElement.Load(stream);
                var imageInfoList = cCanonXMLTools.ToImageInfoContractList(batches.Elements("Batch").Elements().FirstOrDefault(),0,0);
                var actual = imageInfoList.Count();

                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void CanonTools_WhenBatchElementIsSentIn_ToImageInfoContractListFunctionShouldReturnItemsInTheList()
        {
            //arrange
            var expected = 4;

            //act
            using (Stream stream = new FileStream(genericCanonical, FileMode.Open))
            {
                var batches = XElement.Load(stream);
                var imageInfoList = cCanonXMLTools.ToImageInfoContractList(batches.Elements().FirstOrDefault(),0,0);
                var actual = imageInfoList.Count();

                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void CanonTools_WhenBatchesElementIsSentIn_CollectImageDataFunctionShouldReturnItemsInTheList()
        {
            //arrange
            var expected = 4;

            //act
            using (Stream stream = new FileStream(genericCanonical, FileMode.Open))
            {
                var batches = XElement.Load(stream);
                var responseItem = new ResponseItem
                {
                    bankId = 0, clientId = 0,
                    BatchTrackingId = new Guid("86c83686-f42b-4fd6-972d-1d19752e76f2")
                };
                var imageInfoList = cCanonXMLTools.CollectImageData(batches.ToString(), responseItem);
                var actual = imageInfoList.Count;

                //assert
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void CanonTools_GenericDdaLookup()
        {
            //arrange
            var expected = 4;

            //act
            using (Stream stream = new FileStream(genericDdaLookup, FileMode.Open))
            {
                var batches = XElement.Load(stream);
                var responseItem = new ResponseItem
                {
                    bankId = 100, clientId = 123,
                    BatchTrackingId = new Guid("86C83686-F42B-4Fd6-972d-1d19752e76f2")
                };
                var imageInfoList = cCanonXMLTools.CollectImageData(batches.ToString(), responseItem);
                var actual = imageInfoList.Count;

                //assert
                Assert.AreEqual(expected, actual,"image list count");

                Assert.AreEqual(100, imageInfoList[0].BankID,"BankID does not match");
                Assert.AreEqual(123, imageInfoList[0].LockboxID,"LockboxID does not match");
            }
        }
    }
}
