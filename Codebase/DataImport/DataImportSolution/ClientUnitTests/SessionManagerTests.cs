﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportServiceClient;

namespace ClientUnitTests
{
    [TestClass]
    public class SessionManagerTests
    {
        private static readonly IReadOnlyList<Guid> SessionIdsToGenerate = new[]
        {
            new Guid("C80A15B6-2217-418A-A0A7-E9DDC1AC1D11"),
            new Guid("212DCC14-CCA8-4711-BFBA-43AD6893CF1C"),
            new Guid("0272AD83-3899-4BB2-9A6C-70FDE1CED0E0"),
        };

        private TimeSpan GetSessionLatency { get; set; }
        private object Lock { get; set; }
        private List<string> LogItems { get; set; }
        private SessionManager Manager { get; set; }
        private DateTime Now { get; set; }

        [TestInitialize]
        public void SetUp()
        {
            GetSessionLatency = TimeSpan.Zero;
            Now = new DateTime(2016, 5, 1);
            Lock = new object();
            LogItems = new List<string>();
            var sessionIdGenerator = new Queue<Guid>(SessionIdsToGenerate);
            Manager = new SessionManager(() => Now, () =>
            {
                if (GetSessionLatency != TimeSpan.Zero)
                    Thread.Sleep(GetSessionLatency);
                var newGuid = sessionIdGenerator.Dequeue();
                lock (Lock)
                {
                    Log("Generated " + newGuid.ToString().ToUpperInvariant());
                }
                return newGuid;
            }, guid =>
            {
                Log("Ended session " + guid.ToString().ToUpperInvariant());
            });
        }

        private void ExpectLog(params string[] expectedLogItems)
        {
            var expected = string.Join("; ", expectedLogItems);
            var actual = string.Join("; ", LogItems);
            Assert.AreEqual(expected, actual);
        }
        private void GetSessionId()
        {
            var sessionId = Manager.GetSessionId();
            Log("Returned " + sessionId.ToString().ToUpperInvariant());
        }
        private void Log(string message)
        {
            lock (Lock)
            {
                LogItems.Add(Now.ToString("MM-dd HH:mm") + ": " + message);
            }
        }

        [TestMethod]
        public void Constructor_DoesNotRequestSession()
        {
            ExpectLog("");
        }
        [TestMethod]
        public void GetSessionId_AtNoon_RequestsAndReturnsSession()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 0, 0);
            GetSessionId();
            ExpectLog(
                "05-01 12:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11");
        }
        [TestMethod]
        public void GetSessionId_AtNoonThenEleven_RequestsOneSessionAndReturnsItTwice()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 0, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 23, 0, 0);
            GetSessionId();
            ExpectLog(
                "05-01 12:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 23:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11");
        }
        [TestMethod]
        public void GetSessionId_AtElevenThenMidnight_RequestsAndReturnsTwoSessions()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 23, 0, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day + 1, 0, 0, 0);
            GetSessionId();
            ExpectLog(
                "05-01 23:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 23:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-02 00:00: Generated 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:00: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C");
        }
        [TestMethod]
        public void GetSessionId_AtElevenThenMidnightThenFiveAfter_RequestsAndReturnsTwoSessions()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 23, 0, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day + 1, 0, 0, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 0, 5, 0);
            GetSessionId();
            ExpectLog(
                "05-01 23:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 23:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-02 00:00: Generated 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:00: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:05: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C");
        }
        [TestMethod]
        public void GetSessionId_AtElevenThenMidnightThenTenAfter_RequestsTwoSessionsAndEndsOne()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 23, 0, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day + 1, 0, 0, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 0, 10, 0);
            GetSessionId();
            ExpectLog(
                "05-01 23:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 23:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-02 00:00: Generated 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:00: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:10: Ended session C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-02 00:10: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C");
        }
        [TestMethod]
        public void GetSessionId_AtElevenThenTwoThreadsAtMidnight_RequestsThreeSessions()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 23, 0, 0);
            GetSessionId();

            Now = new DateTime(Now.Year, Now.Month, Now.Day + 1, 0, 0, 0);
            GetSessionLatency = TimeSpan.FromSeconds(1);
            var task1 = Task.Run(() =>
            {
                GetSessionId();
            });
            var task2 = Task.Run(() =>
            {
                Thread.Sleep(TimeSpan.FromSeconds(0.5));
                GetSessionId();
            });
            Task.WhenAll(task1, task2).Wait();

            ExpectLog(
                "05-01 23:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 23:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-02 00:00: Generated 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:00: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:00: Generated 0272AD83-3899-4BB2-9A6C-70FDE1CED0E0",
                "05-02 00:00: Returned 0272AD83-3899-4BB2-9A6C-70FDE1CED0E0");
        }
        [TestMethod]
        public void GetSessionId_AtElevenThenTwoThreadsAtMidnightThenTenAfter_EndsTwoSessions()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 23, 0, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day + 1, 0, 0, 0);

            GetSessionLatency = TimeSpan.FromSeconds(1);
            var task1 = Task.Run(() =>
            {
                GetSessionId();
            });
            var task2 = Task.Run(() =>
            {
                Thread.Sleep(TimeSpan.FromSeconds(0.5));
                GetSessionId();
            });
            Task.WhenAll(task1, task2).Wait();

            GetSessionLatency = TimeSpan.Zero;
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 0, 10, 0);
            GetSessionId();

            ExpectLog(
                "05-01 23:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 23:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-02 00:00: Generated 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:00: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:00: Generated 0272AD83-3899-4BB2-9A6C-70FDE1CED0E0",
                "05-02 00:00: Returned 0272AD83-3899-4BB2-9A6C-70FDE1CED0E0",
                "05-02 00:10: Ended session C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-02 00:10: Ended session 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-02 00:10: Returned 0272AD83-3899-4BB2-9A6C-70FDE1CED0E0");
        }
        [TestMethod]
        public void GetSessionId_TwoThreadsAtNoon_CleansUpOrphanedSessionTenMinutesLater()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 0, 0);
            GetSessionLatency = TimeSpan.FromSeconds(1);
            var task1 = Task.Run(() =>
            {
                GetSessionId();
            });
            var task2 = Task.Run(() =>
            {
                Thread.Sleep(TimeSpan.FromSeconds(0.5));
                GetSessionId();
            });
            Task.WhenAll(task1, task2).Wait();

            GetSessionLatency = TimeSpan.Zero;
            Now = Now + TimeSpan.FromMinutes(10);
            GetSessionId();

            ExpectLog(
                "05-01 12:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:00: Generated 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-01 12:00: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-01 12:10: Ended session C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:10: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C");
        }
        [TestMethod]
        public void GetSessionId_ThenClear()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 0, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 1, 0);
            Manager.Clear();
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 2, 0);
            GetSessionId();
            ExpectLog(
                "05-01 12:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:02: Generated 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-01 12:02: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C");
        }
        [TestMethod]
        public void GetSessionId_ThenClearAndGetNewSession_EndsPreviousSessionTenMinutesAfterClear()
        {
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 0, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 1, 0);
            Manager.Clear();
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 2, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 10, 0);
            GetSessionId();
            Now = new DateTime(Now.Year, Now.Month, Now.Day, 12, 11, 0);
            GetSessionId();
            ExpectLog(
                "05-01 12:00: Generated C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:00: Returned C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:02: Generated 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-01 12:02: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-01 12:10: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C",
                "05-01 12:11: Ended session C80A15B6-2217-418A-A0A7-E9DDC1AC1D11",
                "05-01 12:11: Returned 212DCC14-CCA8-4711-BFBA-43AD6893CF1C");
        }
    }
}