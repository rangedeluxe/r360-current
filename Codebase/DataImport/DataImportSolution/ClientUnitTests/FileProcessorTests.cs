﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportClientSvcAPI;

namespace ClientUnitTests
{
    [TestClass]
    public class FileProcessorTests
    {
        private cDITFileProcessor fileProcessor;
        private ResponseContext responseContext;

        [TestInitialize]
        public void Setup()
        {
            fileProcessor = new cDITFileProcessor();
            responseContext = new ResponseContext();
        }
        [TestMethod]
        public void FileProcessor_InvalidSettings_ReturnsFalseBooleanAndErrorMessages()
        {
            //arrange
            var processor = new cDITFileProcessor();
            bool expected = false;
            string errorMessages = string.Empty;

            //act
            bool actual = processor.ValidateSettings(out errorMessages);

            //assert
            Assert.AreEqual(expected, actual, "Invalid settings should return a false response");
            Assert.AreNotEqual(string.Empty, errorMessages, "Valid settings should return error messages.");

        }
    }
}
