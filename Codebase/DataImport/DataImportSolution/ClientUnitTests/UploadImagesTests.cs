using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using WFS.LTA.Common.FileCollector;
using WFS.LTA.DataImport.DataImportClientSvcAPI;
using WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps;
using WFS.RecHub.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

namespace ClientUnitTests
{
    [TestClass]
    public class UploadImagesTests
    {
        // ReSharper disable InconsistentNaming
        /// <summary>
        /// The ColorMode that the ICON XClient passes when it gets an item with the old-style single TIFF filename
        /// (potentially a multi-page file).
        /// </summary>
        /// <remarks>
        /// Technically the ICON XClient passes a blank ColorMode when it gets an old-style single TIFF filename,
        /// but we parse that as zero, which this is a symbolic constant for.
        /// </remarks>
        private const WorkgroupColorMode ColorMode_ImageRps_SingleImageFileName = 0;
        private const byte RawImage_Byte = (byte) 'r';
        private const byte CookedImage_SinglePageTiff_Byte = (byte) 'S';
        private const byte CookedImage_MultiPageTiff_FirstPageByte = (byte) 'F';
        private const byte CookedImage_MultiPageTiff_SecondPageByte = (byte) 'B';
        // ReSharper restore InconsistentNaming

        private ImageInfoContract CreateImageInfoContract(string path, WorkgroupColorMode colorMode, bool isBack,
            bool allowMultiPageTiff)
        {
            return new ImageInfoContract
            {
                ImagePath = path,
                ColorMode = (short) colorMode,
                IsBack = isBack,
                AllowMultiPageTiff = allowMultiPageTiff,
            };
        }
        private ImageInfoContract CreateImageInfoContract_Generic(string path, WorkgroupColorMode colorMode,
            bool isBack)
        {
            return CreateImageInfoContract(path, colorMode, isBack, allowMultiPageTiff: false);
        }
        private ImageInfoContract CreateImageInfoContract_ImageRps_SingleImageFileName(string path)
        {
            return CreateImageInfoContract(path, ColorMode_ImageRps_SingleImageFileName, isBack: false,
                allowMultiPageTiff: true);
        }
        private IImageInfo CreateImageInfo(byte[] frontPageBytes, byte[] backPageBytes = null)
        {
            var imageInfo = Substitute.For<IImageInfo>();
            imageInfo.NumberOfPages.Returns(backPageBytes != null ? 2 : 1);
            imageInfo.GetFrontPageStream().Returns(new MemoryStream(frontPageBytes));
            // If generic uploads a back image, DIT calls GetBackPageStream. For single-page TIFFs,
            // GetBackPageStream returns the same thing as GetFrontPageStream.
            imageInfo.GetBackPageStream().Returns(new MemoryStream(backPageBytes ?? frontPageBytes));
            return imageInfo;
        }
        private IImageInfo CreateImageInfo_CookedMultiPageTiff()
        {
            // Anything to do with IImageInfo, by definition, deals with "cooked" (i.e., post-image-manipulation,
            // split into front and back and re-saved) images)
            return CreateImageInfo(
                new[] {CookedImage_MultiPageTiff_FirstPageByte},
                new[] {CookedImage_MultiPageTiff_SecondPageByte});
        }
        private IImageInfo CreateImageInfo_CookedSinglePageTiff()
        {
            // Anything to do with IImageInfo, by definition, deals with "cooked" (i.e., post-image-manipulation,
            // split into front and back and re-saved) images)
            return CreateImageInfo(new[] {CookedImage_SinglePageTiff_Byte});
        }
        private IImageLoader CreateImageInfoImageLoader(IImageInfo imageInfo)
        {
            var imageLoader = Substitute.For<IImageLoader>();
            imageLoader.LoadImageInfo(Arg.Any<ImageInfoContract>()).Returns(imageInfo);
            return imageLoader;
        }
        private IImageLoader CreateRawBytesImageLoader()
        {
            var imageLoader = Substitute.For<IImageLoader>();
            imageLoader.LoadImageRawBytes(Arg.Any<string>()).Returns(new[] {RawImage_Byte});
            return imageLoader;
        }
        private void UploadImage(ImageInfoContract imageInfoContract, IImageConsumer imageConsumer,
            IImageLoader imageLoader, IInputFileResultLogger resultLogger = null)
        {
            UploadImages.UploadImage(imageInfoContract, imageConsumer,
                resultLogger ?? Substitute.For<IInputFileResultLogger>(), imageLoader);
        }

        [TestMethod]
        public void ImageRps_SingleImageFileName_SinglePageTiff_UploadsOneImage()
        {
            var imageInfoContract = CreateImageInfoContract_ImageRps_SingleImageFileName("file.tif");
            var imageLoader = CreateImageInfoImageLoader(CreateImageInfo_CookedSinglePageTiff());
            var imageConsumer = Substitute.For<IImageConsumer>();

            UploadImage(imageInfoContract, imageConsumer, imageLoader);

            imageLoader.Received().LoadImageInfo(imageInfoContract);
            imageLoader.DidNotReceive().LoadImageRawBytes(Arg.Any<string>());

            imageConsumer.Received().Add("file.tif", Arg.Is<UploadRequest>(r =>
                r.ImageInfo.ColorMode == 0 &&
                r.ImageInfo.IsBack == false &&
                r.ImageData[0] == CookedImage_SinglePageTiff_Byte));
            imageConsumer.Received(1).Add(Arg.Any<string>(), Arg.Any<UploadRequest>());
        }
        [TestMethod]
        public void ImageRps_SingleImageFileName_MultiPageTiff_UploadsTwoImages()
        {
            var imageInfoContract = CreateImageInfoContract_ImageRps_SingleImageFileName("file.tif");
            var imageLoader = CreateImageInfoImageLoader(CreateImageInfo_CookedMultiPageTiff());
            var imageConsumer = Substitute.For<IImageConsumer>();

            UploadImage(imageInfoContract, imageConsumer, imageLoader);

            imageLoader.Received().LoadImageInfo(imageInfoContract);
            imageLoader.DidNotReceive().LoadImageRawBytes(Arg.Any<string>());

            imageConsumer.Received().Add("file.tif", Arg.Is<UploadRequest>(r =>
                r.ImageInfo.ColorMode == 0 &&
                r.ImageInfo.IsBack == false &&
                r.ImageData[0] == CookedImage_MultiPageTiff_FirstPageByte));
            imageConsumer.Received().Add("file.tif", Arg.Is<UploadRequest>(r =>
                r.ImageInfo.ColorMode == 0 &&
                r.ImageInfo.IsBack &&
                r.ImageData[0] == CookedImage_MultiPageTiff_SecondPageByte));
            imageConsumer.Received(2).Add(Arg.Any<string>(), Arg.Any<UploadRequest>());
        }
        [TestMethod]
        public void Generic_Color_Front_SinglePageTiff_UploadsOneImage()
        {
            var imageInfoContract =
                CreateImageInfoContract_Generic("file.tif", WorkgroupColorMode.COLOR_MODE_COLOR, isBack: false);
            var imageLoader = CreateRawBytesImageLoader();
            var imageConsumer = Substitute.For<IImageConsumer>();

            UploadImage(imageInfoContract, imageConsumer, imageLoader);

            imageLoader.Received().LoadImageRawBytes("file.tif");
            imageLoader.DidNotReceive().LoadImageInfo(Arg.Any<ImageInfoContract>());

            imageConsumer.Received().Add("file.tif", Arg.Is<UploadRequest>(r =>
                r.ImageInfo.ColorMode == (int) WorkgroupColorMode.COLOR_MODE_COLOR &&
                r.ImageInfo.IsBack == false &&
                r.ImageData[0] == RawImage_Byte));
            imageConsumer.Received(1).Add(Arg.Any<string>(), Arg.Any<UploadRequest>());
        }
        [TestMethod]
        public void Generic_Grayscale_Back_SinglePageTiff_UploadsOneImage()
        {
            var imageInfoContract =
                CreateImageInfoContract_Generic("file.tif", WorkgroupColorMode.COLOR_MODE_GRAYSCALE, isBack: true);
            var imageLoader = CreateRawBytesImageLoader();
            var imageConsumer = Substitute.For<IImageConsumer>();

            UploadImage(imageInfoContract, imageConsumer, imageLoader);

            imageLoader.Received().LoadImageRawBytes("file.tif");
            imageLoader.DidNotReceive().LoadImageInfo(Arg.Any<ImageInfoContract>());

            imageConsumer.Received().Add("file.tif", Arg.Is<UploadRequest>(r =>
                r.ImageInfo.ColorMode == (int) WorkgroupColorMode.COLOR_MODE_GRAYSCALE &&
                r.ImageInfo.IsBack &&
                r.ImageData[0] == RawImage_Byte));
            imageConsumer.Received(1).Add(Arg.Any<string>(), Arg.Any<UploadRequest>());
        }
        [TestMethod]
        public void Generic_NoColorMode_Front_SinglePageTiff_UploadsOneImage()
        {
            // User left the ColorMode attribute blank, or set it to zero
            const WorkgroupColorMode noColorMode = 0;
            var imageInfoContract = CreateImageInfoContract_Generic("file.tif", noColorMode, isBack: false);
            var imageLoader = CreateRawBytesImageLoader();
            var imageConsumer = Substitute.For<IImageConsumer>();

            UploadImage(imageInfoContract, imageConsumer, imageLoader);

            imageLoader.Received().LoadImageRawBytes("file.tif");
            imageLoader.DidNotReceive().LoadImageInfo(Arg.Any<ImageInfoContract>());

            imageConsumer.Received().Add("file.tif", Arg.Is<UploadRequest>(r =>
                r.ImageInfo.ColorMode == 0 &&
                r.ImageInfo.IsBack == false &&
                r.ImageData[0] == RawImage_Byte));
            imageConsumer.Received(1).Add(Arg.Any<string>(), Arg.Any<UploadRequest>());
        }
    }
}
