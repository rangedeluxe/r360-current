using System;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportClientSvcAPI;
using WFS.RecHub.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
// ReSharper disable PossibleNullReferenceException

namespace ClientUnitTests
{
    [TestClass]
    public class CanonXmlToolsTestsForToImageInfoContract
    {
        [TestInitialize]
        public void SetUp()
        {
            var random = new Random();
            BatchElement = new XElement("Batch",
                new XAttribute("BankID", random.Next(1000)),
                new XAttribute("BatchID", random.Next(1000)),
                new XAttribute("ClientID", random.Next(1000)),
                new XAttribute("BatchDate", "2017-1-" + (random.Next(31) + 1)));
            DocumentElement = new XElement("Payment",
                new XAttribute("BatchSequence", random.Next(1000)));
            ImgElement = new XElement("img",
                new XAttribute("Page", random.Next(2)),
                new XAttribute("ColorMode", random.Next(5)));
        }

        private XElement BatchElement { get; set; }
        private XElement DocumentElement { get; set; }
        private XElement ImgElement { get; set; }

        private ImageInfoContract ToImageInfoContract()
        {
            return cCanonXMLTools.ToImageInfoContract(BatchElement, DocumentElement.Name == "Payment",
                DocumentElement, ImgElement,0,0);
        }

        [TestMethod]
        public void SetupCreatesValidInputs()
        {
            // The test setup should set up a valid, parseable baseline for individual tests to deviate from.
            ToImageInfoContract();
        }
        [TestMethod]
        public void Page_Blank_DefaultsToFront()
        {
            ImgElement.Attribute("Page").Value = "";
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual(false, imageInfo.IsBack, "IsBack");
        }
        [TestMethod]
        public void Page_Front()
        {
            ImgElement.Attribute("Page").Value = "0";
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual(false, imageInfo.IsBack, "IsBack");
        }
        [TestMethod]
        public void Page_Back()
        {
            ImgElement.Attribute("Page").Value = "1";
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual(true, imageInfo.IsBack, "IsBack");
        }
        [TestMethod]
        public void ColorMode_Blank_DefaultsToZero()
        {
            ImgElement.Attribute("ColorMode").Value = "";
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual(0, imageInfo.ColorMode);
        }
        [TestMethod]
        public void ColorMode_Bitonal()
        {
            ImgElement.Attribute("ColorMode").Value = ((int) WorkgroupColorMode.COLOR_MODE_BITONAL).ToString();
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual((short) WorkgroupColorMode.COLOR_MODE_BITONAL, imageInfo.ColorMode);
        }
        [TestMethod]
        public void ColorMode_Grayscale()
        {
            ImgElement.Attribute("ColorMode").Value = ((int) WorkgroupColorMode.COLOR_MODE_GRAYSCALE).ToString();
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual((short) WorkgroupColorMode.COLOR_MODE_GRAYSCALE, imageInfo.ColorMode);
        }
        [TestMethod]
        public void ColorMode_Color()
        {
            ImgElement.Attribute("ColorMode").Value = ((int) WorkgroupColorMode.COLOR_MODE_COLOR).ToString();
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual((short) WorkgroupColorMode.COLOR_MODE_COLOR, imageInfo.ColorMode);
        }
        [TestMethod]
        public void AllowMultiPageTiff_AttributeNotPresent_DefaultsToFalse()
        {
            Assert.IsNull(ImgElement.Attribute("AllowMultiPageTiff"));
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual(false, imageInfo.AllowMultiPageTiff);
        }
        [TestMethod]
        public void AllowMultiPageTiff_False()
        {
            ImgElement.Add(new XAttribute("AllowMultiPageTiff", false));
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual(false, imageInfo.AllowMultiPageTiff);
        }
        [TestMethod]
        public void AllowMultiPageTiff_True()
        {
            ImgElement.Add(new XAttribute("AllowMultiPageTiff", true));
            var imageInfo = ToImageInfoContract();
            Assert.AreEqual(true, imageInfo.AllowMultiPageTiff);
        }
    }
}
