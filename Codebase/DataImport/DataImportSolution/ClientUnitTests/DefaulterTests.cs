﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportClientSvcAPI;
using System.Xml.Linq;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace ClientUnitTests
{
    [TestClass]
    public class DefaulterTests
    {
        private static string aliasMappingsXml;
        private static string dataEntryColumnXml;
        private static string batchXml;
        private Defaulter defaulter;
        private DefaultContext context;
        private string paymentSourceValue = "The Payment Source";
        private string paymentTypeValue = "The Payment Type";
        private string bankIdValue = "12345";
        private string siteCodeValue = "5678";
        
        [ClassInitialize]
        public static void ClassSetup(TestContext context)
        {
           aliasMappingsXml = File.ReadAllText("./Supporting/ClientSetupWithAliasMappings.xml");
           batchXml = File.ReadAllText("./Supporting/BatchData.xml");
           dataEntryColumnXml = File.ReadAllText("./Supporting/ClientSetupWithDataEntryColumns.xml");
        }

        [TestInitialize]
        public void Setup()
        {
            context = new DefaultContext
            {
                BatchDefaults = new List<DefaultItem>()
                {
                    new DefaultItem{Setting = "BatchSource", Value = paymentSourceValue},
                    new DefaultItem{Setting = "PaymentType", Value = paymentTypeValue},
                    new DefaultItem{Setting = "BankID", Value = bankIdValue, DefaultIfZero = true},
                    new DefaultItem{Setting = "BatchSiteCode", Value = siteCodeValue, DefaultIfZero = true}

                },
                ClientSetupDefaults = new List<DefaultItem>()
                {
                    new DefaultItem{Setting = "BatchSource", Value = "The Payment Source"},
                } 
            };
            defaulter = new Defaulter(context);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Defaulter_CreatingInstanceWithNullData_ThrowsArgumentNullException()
        {
            //arrange
            defaulter = new Defaulter(null);

            //act

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Defaulter_AttemptingToApplyDefaultsWithNullImportElement_ThrowsArgumentNullException()
        {
            //arrange
            
            //act
            defaulter.ApplyDefaults(null);

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Defaulter_ApplyDefaultsWithNullImportElementNullDefaultListAndNullElementToFind_ThrowsArgumentNullException()
        {
            //arrange

            //act
            defaulter.ApplyDefaults(null, null, null);

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Defaulter_ApplyDefaultsWithValidImportElementValidDefaultListAndNullElementToFind_ThrowsArgumentNullException()
        {
            //arrange
            var importElement = XElement.Parse(batchXml);
            var defaultList = new List<DefaultItem>();

            //act
            defaulter.ApplyDefaults(importElement, defaultList, null);

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Defaulter_ApplyDefaultsWithValidImportElementNullDefaultListAndValidElementToFind_ThrowsArgumentNullException()
        {
            //arrange
            var importElement = XElement.Parse(batchXml);
            List<DefaultItem> defaultList = null;

            //act
            defaulter.ApplyDefaults(importElement, defaultList, "batch");

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Defaulter_ApplyDefaultsWithValidImportElementNullDefaultList_ThrowsArgumentNullException()
        {
            //arrange
            var importElement = XElement.Parse(batchXml);
            List<DefaultItem> defaultList = null;

            //act
            defaulter.ApplyDefaults(importElement, defaultList);

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Defaulter_ApplyDefaultsWithNullImportElementAndValidDefaultList_ThrowsArgumentNullException()
        {
            //arrange

            //act
            defaulter.ApplyDefaults(null, context.BatchDefaults);

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Defaulter_ShouldDefaultCheckWithNullImportElementAndValidDefaultItem_ThrowsArgumentNullException()
        {
            //arrange

            //act
            defaulter.ShouldDefault(null, context.BatchDefaults.FirstOrDefault());

            //assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Defaulter_ShouldDefaultCheckWithValidImportElementAndNullDefaultItem_ThrowsArgumentNullException()
        {
            //arrange
            var importElement = XElement.Parse(batchXml);

            //act
            defaulter.ShouldDefault(importElement, null);

            //assert
        }

        [TestMethod]
        public void Defaulter_ShouldDefaultCheckWithMissingAttribute_ReturnsTrue()
        {
            //arrange
            var importElement = XElement.Parse(batchXml);
            var defaultItem = new DefaultItem
            {
                Setting = "MyCustomSetting",
                Value = "Inserted"
            };
            var expected = true;

            //act
            var actual = defaulter.ShouldDefault(importElement, defaultItem);

            //assert
            Assert.AreEqual(expected, actual, "Missing attribute should return true.");
        }

        [TestMethod]
        public void Defaulter_ShouldDefaultCheckWithMissingAttributeValue_ReturnsTrue()
        {
            //arrange
            var importElement = GetBatchElementWithBatchSourceEmpty().Elements().FirstOrDefault();
            var defaultItem = context.BatchDefaults.Where(item => string.Equals(item.Setting, "batchsource", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            var expected = true;

            //act
            var actual = defaulter.ShouldDefault(importElement, defaultItem);

            //assert
            Assert.AreEqual(expected, actual, "Missing attribute value should return true.");
        }

        [TestMethod]
        public void Defaulter_ShouldDefaultCheckWithValueOfZeroAndDefaultIfZeroFlagOn_ReturnsTrue()
        {
            //arrange
            var importElement = GetBatchElementWithBankIdSetToZero().Elements().FirstOrDefault();
            var defaultItem = context.BatchDefaults.Where(item => string.Equals(item.Setting, "bankid", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            var expected = true;

            //act
            var actual = defaulter.ShouldDefault(importElement, defaultItem);

            //assert
            Assert.AreEqual(expected, actual, "Zero value with 'DefaultIfZero' flag on should return true.");
        }

        [TestMethod]
        public void Defaulter_ShouldDefaultCheckWithValueOfZeroAndDefaultIfZeroFlagOff_ReturnsFalse()
        {
            //arrange
            var importElement = GetBatchElementWithBankIdSetToZero().Elements().FirstOrDefault();
            var defaultItem = context.BatchDefaults.Where(item => string.Equals(item.Setting, "bankid", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            defaultItem.DefaultIfZero = false;
            var expected = false;

            //act
            var actual = defaulter.ShouldDefault(importElement, defaultItem);

            //assert
            Assert.AreEqual(expected, actual, "Zero value with 'DefaultIfZero' flag off should return false.");
        }

        [TestMethod]
        public void Defaulter_PaymentSourceIsNotSetOnBatchElements_DefaultIsApplied()
        {

            //arrange
            var importElement = XElement.Parse(batchXml);

            //act
            var actual = defaulter.ApplyDefaults(importElement);
            foreach (var descendantElement in actual
                .Descendants()
                .Where(descendant => string.Equals(descendant.Name.ToString(), "batch", StringComparison.InvariantCultureIgnoreCase)))
            {
                //assert
                Assert.AreEqual(paymentSourceValue, descendantElement.Attribute("BatchSource").Value, "PaymentSource attribute should have been defaulted");
            }
        }

        [TestMethod]
        public void Defaulter_PaymentTypeIsNotSet_DefaultIsApplied()
        {

            //arrange
            var importElement = XElement.Parse(batchXml);

            //act
            var actual = defaulter.ApplyDefaults(importElement);
            foreach (var descendantElement in actual
                .Descendants()
                .Where(descendant => string.Equals(descendant.Name.ToString(), "batch", StringComparison.InvariantCultureIgnoreCase)))
            {
                //assert
                Assert.AreEqual(paymentTypeValue, descendantElement.Attribute("PaymentType").Value, "PaymentType attribute should have been defaulted");
            }
        }

        private XElement GetBatchElementWithBankIdSetToZero()
        {
            var importElement = XElement.Parse(batchXml);
            importElement.Descendants("Batch").Attributes("BankID").FirstOrDefault().SetValue("0");
            return importElement;
        }

        private XElement GetBatchElementWithBatchSourceEmpty()
        {
            var importElement = XElement.Parse(batchXml);
            importElement.Descendants("Batch").Attributes("BatchSource").FirstOrDefault().SetValue(string.Empty);
            return importElement;
        }
    }
}
