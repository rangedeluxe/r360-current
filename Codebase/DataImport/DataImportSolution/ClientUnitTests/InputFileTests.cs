﻿using System;
using System.IO;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.Common.FileCollector;

namespace ClientUnitTests
{
    [TestClass]
    public class InputFileTests
    {
        private cInputFile file;

        [TestInitialize]
        public void Setup()
        {
            file = new cInputFile();
            file.SubmittedXML = File.ReadAllText("./Supporting/Generic Multi Batch Canonical.xml");
        }

        [TestMethod]
        public void Can_Determine_WhenAllBatchesAreProcessed()
        {
            //arrange
            var expected = true;

            //act
            file.TrackCompletedBatch(new Guid("acc12828-d2b4-4332-8896-54249f16f6ac"));
            file.TrackCompletedBatch(new Guid("c6d256be-de08-4209-95f6-0856e6da533d"));

            var actual = file.AllBatchesAreCompleted();

            //assert
            Assert.AreEqual(expected, actual,
                $"If all of the batches in the file have been completed, then the '{nameof(cInputFile.AllBatchesAreCompleted)}' method should return true");
        }

        [TestMethod]
        public void Can_Determine_WhenBatchesAreStillPending()
        {
            //arrange
            var expected = false;

            //act
            file.TrackCompletedBatch(new Guid("acc12828-d2b4-4332-8896-54249f16f6ac"));
            var actual = file.AllBatchesAreCompleted();

            //assert
            Assert.AreEqual(expected, actual,
                $"If there are still batches pending, then the '{nameof(cInputFile.AllBatchesAreCompleted)}' method should return false");
        }

        [TestMethod]
        public void Can_DetermineThatAllBatchesAreComplete_WhenFileHasNoBatchElements()
        {
            //arrange
            var expected = true;
            file.SubmittedXML = "<client></client>";

            //act
            var actual = file.AllBatchesAreCompleted();

            //assert
            Assert.AreEqual(expected, actual, 
                $"If the file doesn't contain any batches, then the '{nameof(cInputFile.AllBatchesAreCompleted)}' method should return true");
        }

        [TestMethod]
        public void Can_DetermineThatAllBatchesAreComplete_IfFileContainsNoSubmittedXml()
        {
            //arrange
            var expected = true;
            file.SubmittedXML = null;

            //act
            var actual = file.AllBatchesAreCompleted();

            //assert
            Assert.AreEqual(expected, actual,
                $"If the file doesn't contain any submitted xml, then the '{nameof(cInputFile.AllBatchesAreCompleted)}' method should return true");
        }
    }
}
