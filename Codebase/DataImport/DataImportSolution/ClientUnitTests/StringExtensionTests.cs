﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportClientSvcAPI;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Extensions;

namespace ClientUnitTests
{
    [TestClass]
    public class StringExtensionTests
    {
        XElement xml;

        [TestMethod]
        public void Can_ConvertToResponseItemList_WhenResponseIsSuccess()
        {
            //arrange
            Setup();

            //act
            var actual = xml.ToString().ToResponseItemList();

            //assert
            AssertResponseItemList(actual);
        }

        [TestMethod]
        public void Can_ConvertToResponseItemList_WhenResponseIsFailure()
        {
            //arrange
            Setup(false);

            //act
            var actual = xml.ToString().ToResponseItemList();

            //assert
            AssertResponseItemList(actual, false);
        }

        [TestMethod]
        public void Can_DefaultValues_WhenAttributesAreMissing()
        {
            //arrange
            Setup(false);
            xml.Attributes("ResponseTrackingID").Remove();
            xml.Elements("Batches").Attributes("SourceTrackingID").Remove();
            xml.Elements("Batches").Elements("Batch").Attributes("BatchTrackingID").Remove();
            xml.Elements("Batches").Elements("Batch").Attributes("AuditDateKey").Remove();
            xml.Elements("Batches").Elements("Batch").Attributes("BankID").Remove();
            xml.Elements("Batches").Elements("Batch").Attributes("ClientID").Remove();

            //act
            var actual = xml.ToString().ToResponseItemList();

            //assert
            AssertResponseItemList(actual, false, true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Will_ThrowArgumentNullException_WhenXmlIsNull()
        {
            //arrange

            //act
            var actual = string.Empty.ToResponseItemList();

            //assert
            Assert.Fail($"Should have received an exception of type {nameof(ArgumentNullException)}");
        }

        private void Setup(bool useSuccessfulResponseFile = true)
        {
            string submittedXml;

            if (useSuccessfulResponseFile)
                submittedXml = File.ReadAllText("./Supporting/BatchResponseSuccess.xml");
            else
                submittedXml = File.ReadAllText("./Supporting/BatchResponseFailure.xml");

            xml = XElement.Parse(submittedXml);
        }

        private void AssertResponseItemList(IEnumerable<ResponseItem> actual, bool successfulResponse = true, 
            bool assertDefaults = false)
        {
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Count());
            var responseItem = actual.FirstOrDefault();

            if (successfulResponse)
                Assert.AreEqual(true, responseItem.IsSuccessful);
            else
                Assert.AreEqual(false, responseItem.IsSuccessful);

            if (assertDefaults)
            {
                Assert.AreEqual(Guid.Empty, responseItem.ResponseTrackingId);
                Assert.AreEqual(Guid.Empty, responseItem.BatchTrackingId);
                Assert.AreEqual(Guid.Empty, responseItem.SourceTrackingId);
                Assert.AreEqual(0, responseItem.bankId);
                Assert.AreEqual(0, responseItem.clientId);
                Assert.AreEqual(0, responseItem.AuditDateKey);
            }
            else
            {
                Assert.AreEqual(new Guid("833C35A0-CC58-45BC-8C51-73FF7FBE5B85"), responseItem.ResponseTrackingId);
                Assert.AreEqual(new Guid("3AC6C92B-9F76-4D43-B54B-EF6EFFBCE3AE"), responseItem.BatchTrackingId);
                Assert.AreEqual(new Guid("b473c5bc-6bca-4405-831f-d71ce02dc738"), responseItem.SourceTrackingId);
                Assert.AreEqual(9999, responseItem.bankId);
                Assert.AreEqual(303, responseItem.clientId);
                Assert.AreEqual(20180718, responseItem.AuditDateKey);
            }
            
        }
    }
}
