﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportClientSvcAPI;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Extensions;

namespace ClientUnitTests
{
    [TestClass]
    public class XmlExtensionTests
    {
        XElement xml;

        [TestInitialize]
        public void Setup()
        {
            var submittedXml = File.ReadAllText("./Supporting/Generic Multi Batch Canonical.xml");
            xml = XElement.Parse(submittedXml);
        }

        [TestMethod]
        public void Can_Determine_IfXmlElementIsSameBatchAsResponseItem()
        {
            //arrange
            var expected = true;
            var responseItem = new ResponseItem
            {
                BatchTrackingId = new Guid("c6d256be-de08-4209-95f6-0856e6da533d")
            };

            var batch = GetBatchElement("c6d256be-de08-4209-95f6-0856e6da533d");

            //act
            var actual = batch.IsSameBatch(responseItem);

            //assert
            Assert.AreEqual(expected, actual, 
                "The batch element is supposed to match the batch from the response.");
        }

        [TestMethod]
        public void Can_Determine_IfXmlElementIsNotSameBatchAsResponseItem()
        {
            //arrange
            var expected = false;
            var responseItem = new ResponseItem
            {
                BatchTrackingId = new Guid("acc12828-d2b4-4332-8896-54249f16f6ac")
            };

            var batch = GetBatchElement("c6d256be-de08-4209-95f6-0856e6da533d");

            //act
            var actual = batch.IsSameBatch(responseItem);

            //assert
            Assert.AreEqual(expected, actual, 
                "The batch element is not supposed to match the batch from the response.");
        }

        [TestMethod]
        public void Can_DetermineItIsNotSameBatch_WhenBatchTrackingIdIsMissingFromXml()
        {
            //arrange
            var expected = false;
            var responseItem = new ResponseItem
            {
                BatchTrackingId = new Guid("acc12828-d2b4-4332-8896-54249f16f6ac")
            };

            var batch = GetBatchElement("acc12828-d2b4-4332-8896-54249f16f6ac");
            batch.Attribute("BatchTrackingID").Remove();

            //act
            var actual = batch.IsSameBatch(responseItem);

            //assert
            Assert.AreEqual(expected, actual, 
                "Should have returned false if the batch element is missing the 'BatchTrackingID' attribute.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Will_ThrowArgumentNullException_WhenBatchElementIsNull()
        {
            //arrange
            var batch = GetBatchElement("this is a batch tracking id that doesn't exist");

            //act
            var actual = batch.IsSameBatch(null);

            //assert
            Assert.Fail($"Should have received an exception of type {nameof(ArgumentNullException)}");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Will_ThrowArgumentNullException_WhenResponseItemIsNull()
        {
            //arrange
            var batch = GetBatchElement("c6d256be-de08-4209-95f6-0856e6da533d");

            //act
            var actual = batch.IsSameBatch(null);

            //assert
            Assert.Fail($"Should have received an exception of type {nameof(ArgumentNullException)}");
        }

        private XElement GetBatchElement(string batchTrackingId)
        {
            var batch = xml.Elements("Batch")
                .Where(b => string.Equals(b.Attribute("BatchTrackingID").Value.ToString(),
                    batchTrackingId, StringComparison.InvariantCultureIgnoreCase));

            return batch.FirstOrDefault();
        }
    }
}
