﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportServicesAPI;
using WFS.LTA.DataImport.DataImportServicesAPI.DTO;
using WFS.RecHub.ApplicationBlocks.Common;

namespace RepositoryTests
{
    //these are integration tests
    //   used to test the changes to eliminate the DataImportDal
    //
    [Ignore]
    [TestClass]
    public class DataImportRepositoryTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            string batchXSDVersion = "2.02.07.01";
            var repository = new DataImportRepository(new R360DBConnectionSettings());
            var xsd = repository.GetBatchDataXsd(batchXSDVersion).GetResult(new List<ImportSchemasDto>());
            var xyz = xsd.ToList()[0].ImportSchema;

            Assert.IsNotNull(xsd);
            Assert.IsTrue(xsd.Count() > 0);
        }

        [TestMethod]
        public void TestMethod2()
        {
            XDocument doc = new XDocument(
                new XElement("ClientGroups", new XAttribute("SourceTrackingID", "00000000-0000-0000-0000-000000000000"), new XAttribute("ClientProcessCode","MyProcess"),new XAttribute("XSDVersion","2.01.00.00"),
                new XElement("ClientGroup",new XAttribute("ClientTrackingID", "a016ef0e-ff94-4460-91b4-867189d188ce"), new XAttribute("ClientGroupID","0"))
                ));
            
            int newDate = 20190101;
            var repository = new DataImportRepository(new R360DBConnectionSettings());
            var xsd = repository.DataImportInsertClientSetup(doc.ToString(),newDate);
            Assert.IsTrue(xsd);
        }

        [TestMethod]
        public void TestMethod3()
        {
            var repository = new DataImportRepository(new R360DBConnectionSettings());
            var xsd = repository.DataImportResponseBatch( "").GetResult(new DataTable());

            //Assert.IsTrue(xsd);
            //Assert.IsTrue(xsd.Count() > 0);
        }

        [TestMethod]
        public void TestMethod4()
        {
            var repository = new DataImportRepository(new R360DBConnectionSettings());
            var xsd = repository.DataImportResponseClient("My"); //.GetResult(new List<DataImportQueueDto>());

            //Assert.IsTrue(xsd);
            //Assert.IsTrue(xsd.Count() > 0);
            //var response = repository.DataImportRequestClientSetup(siteBankID, siteLockboxID);
        }

        [TestMethod]
        public void TestMethod5()
        {
            var repository = new DataImportRepository(new R360DBConnectionSettings());
            var xsd = repository.DataImportGetImageRPSAliasMappings(2,7032, null); //.GetResult(new List<ImageRpsAliasMappingsDto>());

            //Assert.IsTrue(xsd);
            //Assert.IsTrue(xsd.Count() > 0);
        }

        [TestMethod]
        public void TestMethod6()
        {
            var repository = new DataImportRepository(new R360DBConnectionSettings());
            var xsd = repository.DataImportRequestDocumentTypes( null);  //.GetResult(new List<DocumentTypesDto>());

            //Assert.IsTrue(xsd);
            //Assert.IsTrue(xsd.Count() > 0);
        }

        [TestMethod]
        public void TestMethod7()
        {
            var repository = new DataImportRepository(new R360DBConnectionSettings());
            var xsd = repository.DataImportResponseClient("My"); //.GetResult(new List<DataImportQueueDto>());

            //Assert.IsTrue(xsd);
            //Assert.IsTrue(xsd.Count() > 0);
            //var response = repository.DataImportRequestClientSetup(2, 7032);
        }
    }
}
