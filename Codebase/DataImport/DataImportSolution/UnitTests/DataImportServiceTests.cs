﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.QualityTools.Testing.Fakes;
using WFS.LTA.DataImport.DataImportServicesAPI;
using WFS.LTA.DataImport.DataImportServicesAPI.Fakes;
using WFS.RecHub.OLLogonServicesAPI.Fakes;
using WFS.RecHub.DAL;
using WFS.RecHub.DAL.Fakes;
using System.Data;
using System.Xml.Linq;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Fakes;

namespace UnitTests
{
    [TestClass]
    public class DataImportServiceTests
    {
        private ISecurityValidation securityValidation;
        private cSiteOptions siteOptions;

        [TestInitialize]
        public void SetUp()
        {
            securityValidation = new StubISecurityValidation() { IsValid = () => { return true; } };
            siteOptions = new StubcSiteOptions("1");
        }

        [TestCleanup]
        public void TearDown()
        {
            securityValidation = null;
            siteOptions = null;
        }

        /// <summary>
        /// This test method utilizes the following guidance point(s):
        /// 1) Descriptive Naming Convention (ClassName_Purpose_ExpectedResult)
        /// 2) Arrange/Act/Assert pattern
        /// 3) Have one logical assert
        /// 4) When to create unit tests (at a minimum on all public functions)
        /// 5) Utilizes the MS Fakes "stubs" to perform the security validation,
        /// 6) Keep it simple and focused (it is focused on the smallest unit of code, the initialization of the service class, and it's tested in isolation)
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(SystemErrorException))]
        public void DataImportServices_InvalidSecurityValidationDuringSetup_ThrowsSystemErrorException()
        {
            //arrange
            securityValidation = new StubISecurityValidation()
                {
                    IsValid = () => { return false; }
                };


            //act
            var service = new StubDataImportServices(securityValidation, siteOptions, new StubIDataImportRepository());


            //assert
            Assert.Fail("Should have thrown an SystemErrorException");

        }

        [TestMethod]
        public void DataImportServices_ValidClientSetupResponse_ReturnsTrue()
        {
            //arrange
            var repository = new StubIDataImportRepository()
            {
                DataImportResponseClientStringDataTableOut = (string processCode, out DataTable returnData) =>
                {
                    returnData = new DataTable();
                    return true;
                }
            };

            var services = new StubDataImportServices(securityValidation, siteOptions, repository);
            DataTable data = new DataTable();
            bool expected = true;


            //act
            bool actual = services.GetClientSetupResponses("TestProcess", out data);

            //assert
            Assert.AreEqual(expected, actual, "A valid client setup response should return true.");

        }

        [TestMethod]
        public void DataImportServices_ValidXmlNodeBuilt_ReturnsProperAttributes()
        {

            //arrange
            var services = new StubDataImportServices(securityValidation, siteOptions, new StubIDataImportRepository());

            //act
            var actual = services.BuildXMLNode(new System.Xml.XmlDocument(), DateTime.Now, DateTime.Now, DateTime.Now, 1, 1, 1);

            //assert
            Assert.AreEqual("batch", actual.Name, true, "Element Name should be 'Batch'");
            Assert.IsTrue(actual.HasAttributes, "XML Element should contain attributes.");
            Assert.IsTrue(actual.HasAttribute("ProcessingDate"), "XML Element should contain an attribute called 'ProcessingDate'.");
            Assert.IsTrue(actual.HasAttribute("DepositDate"), "XML Element should contain an attribute called 'DepositDate'.");
            Assert.IsTrue(actual.HasAttribute("SourceProcesingDate"), "XML Element should contain an attribute called 'SourceProcesingDate'.");
            Assert.IsTrue(actual.HasAttribute("BankID"), "XML Element should contain an attribute called 'BankID'.");
            Assert.IsTrue(actual.HasAttribute("LockboxID"), "XML Element should contain an attribute called 'LockBoxID'.");
            Assert.IsTrue(actual.HasAttribute("BatchID"), "XML Element should contain an attribute called 'BatchID'.");
            Assert.IsTrue(actual.HasAttribute("ActionCode"), "XML Element should contain an attribute called 'ActionCode'.");
            Assert.IsTrue(actual.HasAttribute("NotifyIMS"), "XML Element should contain an attribute called 'NotifyIMS'.");
            Assert.IsTrue(actual.HasAttribute("KeepStats"), "XML Element should contain an attribute called 'KeepStats'.");
            Assert.IsTrue(actual.HasAttribute("QueuedTime"), "XML Element should contain an attribute called 'QueuedTime'.");
        }

        [TestMethod]
        public void DataImportServices_ValidSetResponseComplete_ReturnsTrue()
        {
            //arrange
            bool expected = true;

            var repository = new StubIDataImportRepository()
            {
                DataImportSetResponseCompleteGuidBoolean = (Guid sessionId, bool clientKnowsAboutRecord) =>
                {
                    return true;
                }
            };

            var services = new StubDataImportServices(securityValidation, siteOptions, repository);

            //act
            bool actual = services.DataImportSetResponseComplete(Guid.NewGuid(), true);

            //assert
            Assert.AreEqual(expected, actual, "A valid set response complete should return true.");

        }
    }
}
