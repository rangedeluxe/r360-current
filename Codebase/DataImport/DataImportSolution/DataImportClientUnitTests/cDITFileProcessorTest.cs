﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.Common.FileCollector;
using WFS.LTA.DataImport.DataImportClientSvcAPI;

namespace DataImportClientUnitTests
{
    [TestClass]
    public class cDITFileProcessorTest
    {
        private const string FOLDER_PATH = @".\TestFolder";

        [TestMethod]
        [Ignore]
        public void ProcessWorkItem_XClientTranslationResultsInNull_ShouldAuditAFailure()
        {
            var fileProcessor = new cDITFileProcessor_Wrapper();
            string inputPath = Path.Combine(FOLDER_PATH, "trouble.dat");
            object status = null;
            var config = new ProcessElement_Wrapper(new Dictionary<string, string>() {
                {"ClientProcessCode", ""},
                {"ToCanonicalXSLFile", ""},
                {"FileType", ""},
                {"InputFolder", ""},
                {"InProcessFolder", ""},
                {"PendingResponseFolder", ""},
                {"ResponseFolder", ""},
                {"InputErrorFolder", ""},
                {"ErrorResponseFolder", ""},
                {"ArchiveFolder", ""},
                {"FilePattern", ""},
                {"XClientDllFilePath", ""},
                {"IniSectionKey", ""},
                {"BankName", ""},
                {"DefaultBankID", ""},
                {"DefaultOrganizationID", ""},
                {"DataRetentionDays", ""},
                {"ImageRetentionDays", ""},
                {"ImageRPSResponseFolder", ""},
                {"DefaultPaymentType", ""},
                {"DefaultPaymentSource", ""},
                {"DefaultSiteCode", ""},
                {"PreviousRPSQueueFileAllowed", ""},
                {"SkipDebit", "True" },
	            {"SendEndPoint", "" },
	            {"StatusEndPoint", "" }
            });

            fileProcessor.AuditLoggingMethod = (key, success) => status = success;
            StartWithCleanFolder();
            File.WriteAllText(inputPath, Encoding.Default.GetString(Properties.Resources.trouble__08142014_DROP_ALl_02_txt));
            var inputFile = new cInputFile(new FileInfo(inputPath), new cInputSourceFolderData(config));
            fileProcessor.Expose_ProcessWorkItem(inputFile, new TrackingKeys());
            StartWithCleanFolder();
            Assert.IsFalse(status == null);
        }

        private void StartWithCleanFolder()
        {
            if (Directory.Exists(FOLDER_PATH))
            {
                Directory.Delete(FOLDER_PATH, true);
            }
            Directory.CreateDirectory(FOLDER_PATH);
        }
    }

    public class cInputSourceFolderData : cInboxPathData
    {
        public cInputSourceFolderData(_ProcessElement processElement) : base(processElement) { }

    }

    public class ProcessElement_Wrapper : _ProcessElement
    {
        private Dictionary<string, string> _settingsTable;

        public ProcessElement_Wrapper(Dictionary<string, string> settingsTable)
        {
            _settingsTable = settingsTable;
        }

        public string ClientProcessCode
        {
            get { return _settingsTable["ClientProcessCode"]; }
        }
        public string ToCanonicalXSLFile
        {
            get { return _settingsTable["ToCanonicalXSLFile"]; }
        }
        public string FileType
        {
            get { return _settingsTable["FileType"]; }
        }
        public string InputFolder
        {
            get { return _settingsTable["InputFolder"]; }
        }
        public string InProcessFolder
        {
            get { return _settingsTable["InProcessFolder"]; }
        }
        public string PendingResponseFolder
        {
            get { return _settingsTable["PendingResponseFolder"]; }
        }
        public string ResponseFolder
        {
            get { return _settingsTable["ResponseFolder"]; }
        }
        public string InputErrorFolder
        {
            get { return _settingsTable["InputErrorFolder"]; }
        }
        public string ResponseErrorFolder
        {
            get { return _settingsTable["ErrorResponseFolder"]; }
        }
        public string ArchiveFolder
        {
            get { return _settingsTable["ArchiveFolder"]; }
        }
        public string FilePattern
        {
            get { return _settingsTable["FilePattern"]; }
        }
        public string XClientDllFilePath
        {
            get { return _settingsTable["XClientDllFilePath"]; }
        }
        public string IniSectionKey
        {
            get { return _settingsTable["IniSectionKey"]; }
        }
        public string BankName
        {
            get { return _settingsTable["BankName"]; }
        }
        public string DefaultBankId
        {
            get { return _settingsTable["DefaultBankID"]; }
        }
        public string DefaultOrganizationId
        {
            get { return _settingsTable["DefaultOrganizationID"]; }
        }
        public string DataRetentionDays
        {
            get { return _settingsTable["DataRetentionDays"]; }
        }
        public string ImageRetentionDays
        {
            get { return _settingsTable["ImageRetentionDays"]; }
        }
        public string ImageRPSResponseFolder
        {
            get { return _settingsTable["ImageRPSResponseFolder"]; }
        }
        public string DefaultPaymentType
        {
            get { return _settingsTable["DefaultPaymentType"]; }
        }
        public string DefaultBatchSource
        {
            get { return _settingsTable["DefaultPaymentSource"]; }
        }
        public string DefaultSiteCode
        {
            get { return _settingsTable["DefaultSiteCode"]; }
        }
        public string PreviousRpsQueueFileVersionAllowed
        {
            get { return _settingsTable["PreviousRPSQueueFileAllowed"]; }
        }

        public bool SkipDebit
        {
            get { return _settingsTable["SkipDebit"].Equals("True", StringComparison.CurrentCultureIgnoreCase); }
        }

        public string SendEndPoint
        {
            get { return _settingsTable["SendEndPoint"]; }
        }

        public string StatusEndPoint
        {
            get { return _settingsTable["StatusEndPoint"]; }
        }
    }

    public class cDITFileProcessor_Wrapper : cDITFileProcessor
    {
        public Action<TrackingKeys, bool> AuditLoggingMethod = null;

        protected override void SetEndProcessingAuditingRecord(TrackingKeys recordTrackingKeys, bool isDataSuccessful)
        {
            AuditLoggingMethod?.Invoke(recordTrackingKeys, isDataSuccessful);
        }

        protected override XmlDocument BuildCanonicalXml(cInputFile inputFile, cInboxPathData ipdSourceData, StreamReader smrReader,
            out bool bTranslateXMLDocument)
        {

            bTranslateXMLDocument = false;
            return null;
        }

        protected override void MeasureWork(string measurementTypeName, TrackingKeys recordTrackingKeys,
            string sourceFileName, string clientProcessingCode, XmlDocument xmlDocument, bool isSuccessful = true)
        {
            AuditLoggingMethod?.Invoke(recordTrackingKeys, isSuccessful);
        }

        public bool Expose_ProcessWorkItem(cInputFile inputFile, TrackingKeys recordTrackingKeys)
        {
            return ProcessWorkItem(inputFile, recordTrackingKeys);
        }
    }
}
