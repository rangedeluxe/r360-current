﻿using System;
using System.Globalization;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class AttributeParsingTests
    {
        private const string AttributeName = "Attribute";

        private void AssertThrows<T>(Action action) where T : Exception
        {
            bool gotException;
            try
            {
                action();
                gotException = false;
            }
            catch (Exception ex)
            {
                if (ex.GetType() != typeof(T))
                {
                    Assert.Fail(
                        $"Expected exception of type {typeof(T).FullName}, but got exception of type {ex.GetType().FullName}. " +
                        $"Message: {ex.Message}");
                }
                gotException = true;
            }

            if (!gotException)
                Assert.Fail($"Expected exception of type {typeof(T).Name} but no exception was thrown");
        }
        private BaseShredder ShredAttributeValue(string attributeValue)
        {
            var element = new XElement("Foo", new XAttribute(AttributeName, attributeValue));
            return ShredElement(element);
        }
        private BaseShredder ShredElement(XElement element)
        {
            var reader = element.CreateReader();
            // Advance to the first element (i.e., our 'element' parameter) in the XmlReader
            reader.Read();
            return new BaseShredder(reader);
        }
        private BaseShredder ShredWithNoAttributes()
        {
            var element = new XElement("Foo");
            return ShredElement(element);
        }

        [TestMethod]
        public void GetDate()
        {
            var shredder = ShredAttributeValue("2016-12-31");
            Assert.AreEqual(new DateTime(2016, 12, 31), shredder.GetDate(AttributeName));
        }
        [TestMethod]
        public void GetDate_EmptyString_Returns1January1900()
        {
            var shredder = ShredAttributeValue("");
            Assert.AreEqual(new DateTime(1900, 1, 1), shredder.GetDate(AttributeName));
        }
        [TestMethod]
        public void GetDate_Whitespace_Returns1January1900()
        {
            var shredder = ShredAttributeValue("  ");
            Assert.AreEqual(new DateTime(1900, 1, 1), shredder.GetDate(AttributeName));
        }
        [TestMethod]
        public void GetDate_Alpha_Throws()
        {
            var shredder = ShredAttributeValue("abc");
            AssertThrows<FormatException>(() => shredder.GetDate(AttributeName));
        }
        [TestMethod]
        public void GetDate_AttributeMissing_Throws()
        {
            var shredder = ShredWithNoAttributes();
            AssertThrows<InvalidOperationException>(() => shredder.GetDate(AttributeName));
        }
        [TestMethod]
        public void GetInt16()
        {
            var shredder = ShredAttributeValue("123");
            Assert.AreEqual(123, shredder.GetInt16(AttributeName));
        }
        [TestMethod]
        public void GetInt16_Negative()
        {
            var shredder = ShredAttributeValue("-123");
            Assert.AreEqual(-123, shredder.GetInt16(AttributeName));
        }
        [TestMethod]
        public void GetInt16_EmptyString_ReturnsZero()
        {
            var shredder = ShredAttributeValue("");
            Assert.AreEqual(0, shredder.GetInt16(AttributeName));
        }
        [TestMethod]
        public void GetInt16_Whitespace_ReturnsZero()
        {
            var shredder = ShredAttributeValue("  ");
            Assert.AreEqual(0, shredder.GetInt16(AttributeName));
        }
        [TestMethod]
        public void GetInt16_ValueOutOfRange_Throws()
        {
            var shredder = ShredAttributeValue("32768");
            AssertThrows<OverflowException>(() => shredder.GetInt16(AttributeName));
        }
        [TestMethod]
        public void GetInt16_WithDecimalPoint_Throws()
        {
            var shredder = ShredAttributeValue("111.0");
            AssertThrows<FormatException>(() => shredder.GetInt16(AttributeName));
        }
        [TestMethod]
        public void GetInt16_Alpha_Throws()
        {
            var shredder = ShredAttributeValue("abc");
            AssertThrows<FormatException>(() => shredder.GetInt16(AttributeName));
        }
        [TestMethod]
        public void GetInt16_AttributeMissing_Throws()
        {
            var shredder = ShredWithNoAttributes();
            AssertThrows<InvalidOperationException>(() => shredder.GetInt16(AttributeName));
        }
        [TestMethod]
        public void GetInt32()
        {
            var shredder = ShredAttributeValue("32768");
            Assert.AreEqual(32768, shredder.GetInt32(AttributeName));
        }
        [TestMethod]
        public void GetInt32_Negative()
        {
            var shredder = ShredAttributeValue("-32769");
            Assert.AreEqual(-32769, shredder.GetInt32(AttributeName));
        }
        [TestMethod]
        public void GetInt32_EmptyString_ReturnsZero()
        {
            var shredder = ShredAttributeValue("");
            Assert.AreEqual(0, shredder.GetInt32(AttributeName));
        }
        [TestMethod]
        public void GetInt32_Whitespace_ReturnsZero()
        {
            var shredder = ShredAttributeValue("  ");
            Assert.AreEqual(0, shredder.GetInt32(AttributeName));
        }
        [TestMethod]
        public void GetInt32_ValueOutOfRange_Throws()
        {
            var shredder = ShredAttributeValue("9876543210");
            AssertThrows<OverflowException>(() => shredder.GetInt32(AttributeName));
        }
        [TestMethod]
        public void GetInt32_WithDecimalPoint_Throws()
        {
            var shredder = ShredAttributeValue("111.0");
            AssertThrows<FormatException>(() => shredder.GetInt32(AttributeName));
        }
        [TestMethod]
        public void GetInt32_Alpha_Throws()
        {
            var shredder = ShredAttributeValue("abc");
            AssertThrows<FormatException>(() => shredder.GetInt32(AttributeName));
        }
        [TestMethod]
        public void GetInt32_AttributeMissing_Throws()
        {
            var shredder = ShredWithNoAttributes();
            AssertThrows<InvalidOperationException>(() => shredder.GetInt32(AttributeName));
        }
        [TestMethod]
        public void GetInt64()
        {
            var shredder = ShredAttributeValue("9876543210");
            Assert.AreEqual(9876543210, shredder.GetInt64(AttributeName));
        }
        [TestMethod]
        public void GetInt64_Negative()
        {
            var shredder = ShredAttributeValue("-9876543210");
            Assert.AreEqual(-9876543210, shredder.GetInt64(AttributeName));
        }
        [TestMethod]
        public void GetInt64_EmptyString_ReturnsZero()
        {
            var shredder = ShredAttributeValue("");
            Assert.AreEqual(0, shredder.GetInt64(AttributeName));
        }
        [TestMethod]
        public void GetInt64_Whitespace_ReturnsZero()
        {
            var shredder = ShredAttributeValue("  ");
            Assert.AreEqual(0, shredder.GetInt64(AttributeName));
        }
        [TestMethod]
        public void GetInt64_ValueOutOfRange_Throws()
        {
            var shredder = ShredAttributeValue((long.MaxValue + 1M).ToString(CultureInfo.InvariantCulture));
            AssertThrows<OverflowException>(() => shredder.GetInt64(AttributeName));
        }
        [TestMethod]
        public void GetInt64_WithDecimalPoint_Throws()
        {
            var shredder = ShredAttributeValue("111.0");
            AssertThrows<FormatException>(() => shredder.GetInt64(AttributeName));
        }
        [TestMethod]
        public void GetInt64_Alpha_Throws()
        {
            var shredder = ShredAttributeValue("abc");
            AssertThrows<FormatException>(() => shredder.GetInt64(AttributeName));
        }
        [TestMethod]
        public void GetInt64_AttributeMissing_Throws()
        {
            var shredder = ShredWithNoAttributes();
            AssertThrows<InvalidOperationException>(() => shredder.GetInt64(AttributeName));
        }
        [TestMethod]
        public void GetString()
        {
            var shredder = ShredAttributeValue("abcd");
            Assert.AreEqual("abcd", shredder.GetString(AttributeName, 5));
        }
        [TestMethod]
        public void GetString_InputIsExactlyMaximumLength()
        {
            var shredder = ShredAttributeValue("abcde");
            Assert.AreEqual("abcde", shredder.GetString(AttributeName, 5));
        }
        [TestMethod]
        public void GetString_InputIsLongerThanMaximumLength_SilentlyTruncates()
        {
            var shredder = ShredAttributeValue("abcdef");
            Assert.AreEqual("abcde", shredder.GetString(AttributeName, 5));
        }
        [TestMethod]
        public void GetString_EmptyString()
        {
            var shredder = ShredAttributeValue("");
            Assert.AreEqual("", shredder.GetString(AttributeName, 5));
        }
        [TestMethod]
        public void GetString_Whitespace()
        {
            var shredder = ShredAttributeValue("  ");
            Assert.AreEqual("  ", shredder.GetString(AttributeName, 5));
        }
        [TestMethod]
        public void GetString_AttributeMissing_Throws()
        {
            var shredder = ShredWithNoAttributes();
            AssertThrows<InvalidOperationException>(() => shredder.GetString(AttributeName, 5));
        }
        [TestMethod]
        public void GetOptionalString()
        {
            var shredder = ShredAttributeValue("abcd");
            Assert.AreEqual("abcd", shredder.GetOptionalString(AttributeName, 5));
        }
        [TestMethod]
        public void GetOptionalString_InputIsExactlyMaximumLength()
        {
            var shredder = ShredAttributeValue("abcde");
            Assert.AreEqual("abcde", shredder.GetOptionalString(AttributeName, 5));
        }
        [TestMethod]
        public void GetOptionalString_InputIsLongerThanMaximumLength_SilentlyTruncates()
        {
            var shredder = ShredAttributeValue("abcdef");
            Assert.AreEqual("abcde", shredder.GetOptionalString(AttributeName, 5));
        }
        [TestMethod]
        public void GetOptionalString_EmptyString_ReturnsNull()
        {
            var shredder = ShredAttributeValue("");
            Assert.AreEqual(null, shredder.GetOptionalString(AttributeName, 5));
        }
        [TestMethod]
        public void GetOptionalString_Whitespace_ReturnsWhitespace()
        {
            var shredder = ShredAttributeValue("  ");
            Assert.AreEqual("  ", shredder.GetOptionalString(AttributeName, 5));
        }
        [TestMethod]
        public void GetOptionalString_AttributeMissing_ReturnsNull()
        {
            var shredder = ShredWithNoAttributes();
            Assert.AreEqual(null, shredder.GetOptionalString(AttributeName, 5));
        }
        [TestMethod]
        public void GetGuid_AcceptsLowercase()
        {
            var shredder = ShredAttributeValue("d2d18cbb-08ae-4931-9fb9-afc303ae1563");
            Assert.AreEqual(new Guid("d2d18cbb-08ae-4931-9fb9-afc303ae1563"), shredder.GetGuid(AttributeName));
        }
        [TestMethod]
        public void GetGuid_AcceptsUppercase()
        {
            var shredder = ShredAttributeValue("D2D18CBB-08AE-4931-9FB9-AFC303AE1563");
            Assert.AreEqual(new Guid("d2d18cbb-08ae-4931-9fb9-afc303ae1563"), shredder.GetGuid(AttributeName));
        }
        [TestMethod]
        public void GetGuid_EmptyString_Throws()
        {
            var shredder = ShredAttributeValue("");
            AssertThrows<FormatException>(() => shredder.GetGuid(AttributeName));
        }
        [TestMethod]
        public void GetGuid_AttributeMissing_Throws()
        {
            var shredder = ShredWithNoAttributes();
            AssertThrows<InvalidOperationException>(() => shredder.GetGuid(AttributeName));
        }
        [TestMethod]
        public void GetDecimal()
        {
            var shredder = ShredAttributeValue("1");
            Assert.AreEqual(1.00m, shredder.GetDecimal(AttributeName));
        }
        [TestMethod]
        public void GetDecimal_Negative()
        {
            var shredder = ShredAttributeValue("-1");
            Assert.AreEqual(-1.00m, shredder.GetDecimal(AttributeName));
        }
        [TestMethod]
        public void GetDecimal_SupportsAtLeastFourDecimalPlaces()
        {
            var shredder = ShredAttributeValue("1.2345");
            Assert.AreEqual(1.2345m, shredder.GetDecimal(AttributeName));
        }
        [TestMethod]
        public void GetDecimal_EmptyString_ReturnsZero()
        {
            var shredder = ShredAttributeValue("");
            Assert.AreEqual(0.00m, shredder.GetDecimal(AttributeName));
        }
        [TestMethod]
        public void GetDecimal_Whitespace_ReturnsZero()
        {
            var shredder = ShredAttributeValue("  ");
            Assert.AreEqual(0.00m, shredder.GetDecimal(AttributeName));
        }
        [TestMethod]
        public void GetDecimal_Alpha_Throws()
        {
            var shredder = ShredAttributeValue("abc");
            AssertThrows<FormatException>(() => shredder.GetDecimal(AttributeName));
        }
        [TestMethod]
        public void GetDecimal_AttributeMissing_Throws()
        {
            var shredder = ShredWithNoAttributes();
            AssertThrows<InvalidOperationException>(() => shredder.GetDecimal(AttributeName));
        }
        [TestMethod]
        public void GetOptionalInt32()
        {
            var shredder = ShredAttributeValue("123");
            Assert.AreEqual(123, shredder.GetOptionalInt32(AttributeName));
        }
        [TestMethod]
        public void GetOptionalInt32_Negative()
        {
            var shredder = ShredAttributeValue("-123");
            Assert.AreEqual(-123, shredder.GetOptionalInt32(AttributeName));
        }
        [TestMethod]
        public void GetOptionalInt32_EmptyString_ReturnsNull()
        {
            var shredder = ShredAttributeValue("");
            Assert.AreEqual(null, shredder.GetOptionalInt32(AttributeName));
        }
        [TestMethod]
        public void GetOptionalInt32_ValueOutOfRange_Throws()
        {
            var shredder = ShredAttributeValue("9876543210");
            AssertThrows<OverflowException>(() => shredder.GetOptionalInt32(AttributeName));
        }
        [TestMethod]
        public void GetOptionalInt32_WithDecimalPoint_Throws()
        {
            var shredder = ShredAttributeValue("111.0");
            AssertThrows<FormatException>(() => shredder.GetOptionalInt32(AttributeName));
        }
        [TestMethod]
        public void GetOptionalInt32_Alpha_Throws()
        {
            var shredder = ShredAttributeValue("abc");
            AssertThrows<FormatException>(() => shredder.GetOptionalInt32(AttributeName));
        }
        [TestMethod]
        public void GetOptionalInt32_AttributeMissing_ReturnsNull()
        {
            var shredder = ShredWithNoAttributes();
            Assert.AreEqual(null, shredder.GetOptionalInt32(AttributeName));
        }
        [TestMethod]
        public void GetBoolean_ValueIs0_ReturnsFalse()
        {
            var shredder = ShredAttributeValue("0");
            Assert.AreEqual(false, shredder.GetBoolean(AttributeName));
        }
        [TestMethod]
        public void GetBoolean_ValueIs1_ReturnsTrue()
        {
            var shredder = ShredAttributeValue("1");
            Assert.AreEqual(true, shredder.GetBoolean(AttributeName));
        }
        [TestMethod]
        public void GetBoolean_Alpha_Throws()
        {
            var shredder = ShredAttributeValue("abc");
            AssertThrows<FormatException>(() => shredder.GetBoolean(AttributeName));
        }
        [TestMethod]
        public void GetBoolean_EmptyString_ReturnsFalse()
        {
            var shredder = ShredAttributeValue("");
            Assert.AreEqual(false, shredder.GetBoolean(AttributeName));
        }
        [TestMethod]
        public void GetBoolean_AttributeMissing_Throws()
        {
            var shredder = ShredWithNoAttributes();
            AssertThrows<InvalidOperationException>(() => shredder.GetBoolean(AttributeName));
        }
    }
}