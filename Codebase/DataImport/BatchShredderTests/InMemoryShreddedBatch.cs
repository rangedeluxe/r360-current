﻿using System.Collections.Generic;

namespace WFS.LTA.DataImport
{
    public class InMemoryShreddedBatch : IShreddedBatchWriter
    {
        public IList<BatchData> BatchData { get; } = new List<BatchData>();
        public IList<Batch> Batches { get; } = new List<Batch>();
        public IList<DataImportWorkBatchResponse> DataImportWorkBatchResponses { get; } =
            new List<DataImportWorkBatchResponse>();
        public IList<DocumentItemData> DocumentItemData { get; } = new List<DocumentItemData>();
        public IList<DocumentRemittanceData> DocumentRemittanceData { get; } = new List<DocumentRemittanceData>();
        public IList<Document> Documents { get; } = new List<Document>();
        public IList<GhostDocumentItemData> GhostDocumentItemData { get; } = new List<GhostDocumentItemData>();
        public IList<GhostDocumentRemittanceData> GhostDocumentRemittanceData { get; } =
            new List<GhostDocumentRemittanceData>();
        public IList<GhostDocument> GhostDocuments { get; } = new List<GhostDocument>();
        public IList<PaymentItemData> PaymentItemData { get; } = new List<PaymentItemData>();
        public IList<PaymentRawData> PaymentRawData { get; } = new List<PaymentRawData>();
        public IList<PaymentRemittanceData> PaymentRemittanceData { get; } = new List<PaymentRemittanceData>();
        public IList<Payment> Payments { get; } = new List<Payment>();
        public IList<Transaction> Transactions { get; } = new List<Transaction>();

        public void WriteBatch(Batch batch)
        {
            Batches.Add(batch);
        }
        public void WriteBatchData(BatchData batchData)
        {
            BatchData.Add(batchData);
        }
        public void WriteDataImportWorkBatchResponse(DataImportWorkBatchResponse response)
        {
            DataImportWorkBatchResponses.Add(response);
        }
        public void WriteDocument(Document document)
        {
            Documents.Add(document);
        }
        public void WriteDocumentItemData(DocumentItemData documentItemData)
        {
            DocumentItemData.Add(documentItemData);
        }
        public void WriteDocumentRemittanceData(DocumentRemittanceData documentRemittanceData)
        {
            DocumentRemittanceData.Add(documentRemittanceData);
        }
        public void WriteGhostDocument(GhostDocument ghostDocument)
        {
            GhostDocuments.Add(ghostDocument);
        }
        public void WriteGhostDocumentItemData(GhostDocumentItemData ghostDocumentItemData)
        {
            GhostDocumentItemData.Add(ghostDocumentItemData);
        }
        public void WriteGhostDocumentRemittanceData(GhostDocumentRemittanceData ghostDocumentRemittanceData)
        {
            GhostDocumentRemittanceData.Add(ghostDocumentRemittanceData);
        }
        public void WritePayment(Payment payment)
        {
            Payments.Add(payment);
        }
        public void WritePaymentItemData(PaymentItemData paymentItemData)
        {
            PaymentItemData.Add(paymentItemData);
        }
        public void WritePaymentRawData(PaymentRawData paymentRawData)
        {
            PaymentRawData.Add(paymentRawData);
        }
        public void WritePaymentRemittanceData(PaymentRemittanceData paymentRemittanceData)
        {
            PaymentRemittanceData.Add(paymentRemittanceData);
        }
        public void WriteTransaction(Transaction transaction)
        {
            Transactions.Add(transaction);
        }
    }
}