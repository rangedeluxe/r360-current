﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class TransactionTests : ShredderTestBase
    {
        private static Transaction ShredSingleTransaction(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.Transactions);
        }

        [TestMethod]
        public void BatchWithTwoTransactions()
        {
            var batch = CreateBatchElement();
            batch.Add(CreateTransactionElement());
            batch.Add(CreateTransactionElement());
            var transactions = ShredAll(batch).Transactions;
            Assert.AreEqual(2, transactions.Count, "Transaction count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement());
            Assert.AreEqual(DataImportQueueId, transaction.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromʽTransaction_IdʼXmlAttribute()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transaction_id: "9876543210"));
            Assert.AreEqual(9876543210, transaction.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionId: "123"));
            Assert.AreEqual(123, transaction.TransactionId);
        }
        [TestMethod]
        public void TransactionSequence()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionSequence: "123"));
            Assert.AreEqual(123, transaction.TransactionSequence);
        }
        [TestMethod]
        public void TransactionHash()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionHash: "abc"));
            Assert.AreEqual("abc", transaction.TransactionHash);
        }
        [TestMethod]
        public void TransactionHash_MaximumLengthIs40()
        {
            var transaction = ShredSingleTransaction(
                CreateTransactionElement(transactionHash: "12345678901234567890123456789012345678901"));
            Assert.AreEqual("1234567890123456789012345678901234567890", transaction.TransactionHash);
        }
        [TestMethod]
        public void TransactionHash_EmptyString_SetsPropertyToNull()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionHash: ""));
            Assert.AreEqual(null, transaction.TransactionHash);
        }
        [TestMethod]
        public void TransactionHash_AttributeMissing_SetsPropertyToNull()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement());
            Assert.AreEqual(null, transaction.TransactionHash);
        }
        [TestMethod]
        public void TransactionSignature()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionSignature: "abc"));
            Assert.AreEqual("abc", transaction.TransactionSignature);
        }
        [TestMethod]
        public void TransactionSignature_MaximumLengthIs65()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(
                transactionSignature: "123456789012345678901234567890123456789012345678901234567890123456"));
            Assert.AreEqual("12345678901234567890123456789012345678901234567890123456789012345",
                transaction.TransactionSignature);
        }
        [TestMethod]
        public void TransactionSignature_EmptyString_SetsPropertyToNull()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionSignature: ""));
            Assert.AreEqual(null, transaction.TransactionSignature);
        }
        [TestMethod]
        public void TransactionSignature_AttributeMissing_SetsPropertyToNull()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement());
            Assert.AreEqual(null, transaction.TransactionSignature);
        }
        [TestMethod]
        public void TransactionHashChecker_TransactionHashIsShorterThan10Characters_IsTransactionHashParsedAsHex()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionHash: "123"));
            Assert.AreEqual(0x123, transaction.TransactionHashChecker);
        }
        [TestMethod]
        public void TransactionHashChecker_TransactionHashIsExactly10Characters_IsTransactionHashParsedAsHex()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionHash: "9876543210"));
            Assert.AreEqual(0x9876543210, transaction.TransactionHashChecker);
        }
        [TestMethod]
        public void TransactionHashChecker_TransactionHashIsLongerThan10Characters_IsFirst10CharactersParsedAsHex()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionHash: "1234567890123"));
            Assert.AreEqual(0x1234567890, transaction.TransactionHashChecker);
        }
        [TestMethod]
        public void TransactionHashChecker_TransactionHashIsEmptyString_IsNull()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement(transactionHash: ""));
            Assert.AreEqual(null, transaction.TransactionHashChecker);
        }
        [TestMethod]
        public void TransactionHashChecker_TransactionHashIsMissingAttribute_IsNull()
        {
            var transaction = ShredSingleTransaction(CreateTransactionElement());
            Assert.AreEqual(null, transaction.TransactionHashChecker);
        }
    }
}