﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class DocumentTests : ShredderTestBase
    {
        private static Document ShredSingleDocument(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.Documents);
        }

        [TestMethod]
        public void BatchWithTwoDocuments()
        {
            var batch = CreateBatchElement();

            batch.Add(CreateDocumentElement());
            batch.Add(CreateDocumentElement());

            var documents = ShredAll(batch).Documents;
            Assert.AreEqual(2, documents.Count, "Document count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var document = ShredSingleDocument(CreateDocumentElement());
            Assert.AreEqual(DataImportQueueId, document.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromʽTransaction_IdʼXmlAttribute()
        {
            var document = ShredSingleDocument(CreateDocumentElement(transaction_id: "9876543210"));
            Assert.AreEqual(9876543210, document.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreateDocumentElement());
            var document = ShredSingleDocument(transaction);
            Assert.AreEqual(123, document.TransactionId);
        }
        [TestMethod]
        public void TransactionSequence_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionSequence: "123");
            transaction.Add(CreateDocumentElement());
            var document = ShredSingleDocument(transaction);
            Assert.AreEqual(123, document.TransactionSequence);
        }
        [TestMethod]
        public void GeneratedDocumentId_ComesFromʽDocument_IdʼXmlAttribute()
        {
            var document = ShredSingleDocument(CreateDocumentElement(document_id: "9876543210"));
            Assert.AreEqual(9876543210, document.GeneratedDocumentId);
        }
        [TestMethod]
        public void BatchSequence()
        {
            var document = ShredSingleDocument(CreateDocumentElement(batchSequence: "123"));
            Assert.AreEqual(123, document.BatchSequence);
        }
        [TestMethod]
        public void DocumentSequence()
        {
            var document = ShredSingleDocument(CreateDocumentElement(documentSequence: "123"));
            Assert.AreEqual(123, document.DocumentSequence);
        }
        [TestMethod]
        public void SequenceWithinTransaction()
        {
            var document = ShredSingleDocument(CreateDocumentElement(sequenceWithinTransaction: "123"));
            Assert.AreEqual(123, document.SequenceWithinTransaction);
        }
        [TestMethod]
        public void DocumentDescriptor()
        {
            var document = ShredSingleDocument(CreateDocumentElement(documentDescriptor: "abc"));
            Assert.AreEqual("abc", document.DocumentDescriptor);
        }
        [TestMethod]
        public void DocumentDescriptor_MaximumLengthIs30()
        {
            var document = ShredSingleDocument(CreateDocumentElement(
                documentDescriptor: "1234567890123456789012345678901"));
            Assert.AreEqual("123456789012345678901234567890", document.DocumentDescriptor);
        }
        [TestMethod]
        public void IsCorrespondence()
        {
            var document = ShredSingleDocument(CreateDocumentElement(isCorrespondence: "1"));
            Assert.AreEqual(true, document.IsCorrespondence);
        }
    }
}