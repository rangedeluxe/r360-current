﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class GhostDocumentTests : ShredderTestBase
    {
        private static GhostDocument ShredSingleGhostDocument(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.GhostDocuments);
        }

        [TestMethod]
        public void BatchWithTwoGhostDocuments()
        {
            var batch = CreateBatchElement();

            batch.Add(CreateGhostDocumentElement());
            batch.Add(CreateGhostDocumentElement());

            var ghostDocuments = ShredAll(batch).GhostDocuments;
            Assert.AreEqual(2, ghostDocuments.Count, "Ghost document count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var ghostDocument = ShredSingleGhostDocument(CreateGhostDocumentElement());
            Assert.AreEqual(DataImportQueueId, ghostDocument.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId()
        {
            var ghostDocument = ShredSingleGhostDocument(CreateGhostDocumentElement(transaction_id: "9876543210"));
            Assert.AreEqual(9876543210, ghostDocument.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreateGhostDocumentElement());
            var ghostDocument = ShredSingleGhostDocument(transaction);
            Assert.AreEqual(123, ghostDocument.TransactionId);
        }
        [TestMethod]
        public void TransactionSequence_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionSequence: "123");
            transaction.Add(CreateGhostDocumentElement());
            var ghostDocument = ShredSingleGhostDocument(transaction);
            Assert.AreEqual(123, ghostDocument.TransactionSequence);
        }
        [TestMethod]
        public void GeneratedGhostDocumentId_ComesFromʽGhostDocument_IdʼXmlAttribute()
        {
            var ghostDocument = ShredSingleGhostDocument(CreateGhostDocumentElement(ghostDocument_id: "9876543210"));
            Assert.AreEqual(9876543210, ghostDocument.GeneratedGhostDocumentId);
        }
        [TestMethod]
        public void BatchSequence()
        {
            var ghostDocument = ShredSingleGhostDocument(CreateGhostDocumentElement(batchSequence: "123"));
            Assert.AreEqual(123, ghostDocument.BatchSequence);
        }
        [TestMethod]
        public void IsCorrespondence()
        {
            var ghostDocument = ShredSingleGhostDocument(CreateGhostDocumentElement(isCorrespondence: "1"));
            Assert.AreEqual(true, ghostDocument.IsCorrespondence);
        }
    }
}