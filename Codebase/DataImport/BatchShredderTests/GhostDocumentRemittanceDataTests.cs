﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class GhostDocumentRemittanceDataTests : ShredderTestBase
    {
        private static GhostDocumentRemittanceData ShredSingleGhostDocumentRemittanceData(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.GhostDocumentRemittanceData);
        }

        [TestMethod]
        public void BatchWithTwoGhostDocumentRemittanceDataElements()
        {
            var batch = CreateBatchElement();

            batch.Add(CreateGhostDocumentRemittanceDataElement());
            batch.Add(CreateGhostDocumentRemittanceDataElement());

            var ghostDocumentRemittanceData = ShredAll(batch).GhostDocumentRemittanceData;
            Assert.AreEqual(2, ghostDocumentRemittanceData.Count, "Ghost document remittance data count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var ghostDocumentRemittanceData =
                ShredSingleGhostDocumentRemittanceData(CreateGhostDocumentRemittanceDataElement());
            Assert.AreEqual(DataImportQueueId, ghostDocumentRemittanceData.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transaction_id: "9876543210");
            transaction.Add(CreateGhostDocumentRemittanceDataElement());
            var ghostDocumentRemittanceData = ShredSingleGhostDocumentRemittanceData(transaction);
            Assert.AreEqual(9876543210, ghostDocumentRemittanceData.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreateGhostDocumentRemittanceDataElement());
            var ghostDocumentRemittanceData = ShredSingleGhostDocumentRemittanceData(transaction);
            Assert.AreEqual(123, ghostDocumentRemittanceData.TransactionId);
        }
        [TestMethod]
        public void GeneratedGhostDocumentId_ComesFromʽGhostDocument_IdʼXmlAttribute()
        {
            var ghostDocumentRemittanceData = ShredSingleGhostDocumentRemittanceData(
                CreateGhostDocumentRemittanceDataElement(ghostDocument_id: "9876543210"));
            Assert.AreEqual(9876543210, ghostDocumentRemittanceData.GeneratedGhostDocumentId);
        }
        [TestMethod]
        public void GeneratedRemittanceDataRecordId_ComesFromʽRemittanceDataRecord_IdʼXmlAttribute()
        {
            var ghostDocumentRemittanceData = ShredSingleGhostDocumentRemittanceData(
                CreateGhostDocumentRemittanceDataElement(remittanceDataRecord_id: "9876543210"));
            Assert.AreEqual(9876543210, ghostDocumentRemittanceData.GeneratedRemittanceDataRecordId);
        }
        [TestMethod]
        public void BatchSequence()
        {
            var ghostDocumentRemittanceData = ShredSingleGhostDocumentRemittanceData(
                CreateGhostDocumentRemittanceDataElement(batchSequence: "123"));
            Assert.AreEqual(123, ghostDocumentRemittanceData.BatchSequence);
        }
        [TestMethod]
        public void FieldName()
        {
            var ghostDocumentRemittanceData = ShredSingleGhostDocumentRemittanceData(
                CreateGhostDocumentRemittanceDataElement(fieldName: "abc"));
            Assert.AreEqual("abc", ghostDocumentRemittanceData.FieldName);
        }
        [TestMethod]
        public void FieldName_MaximumLengthIs32()
        {
            var ghostDocumentRemittanceData = ShredSingleGhostDocumentRemittanceData(
                CreateGhostDocumentRemittanceDataElement(fieldName: "123456789012345678901234567890123"));
            Assert.AreEqual("12345678901234567890123456789012", ghostDocumentRemittanceData.FieldName);
        }
        [TestMethod]
        public void FieldValue()
        {
            var ghostDocumentRemittanceData = ShredSingleGhostDocumentRemittanceData(
                CreateGhostDocumentRemittanceDataElement(fieldValue: "abc"));
            Assert.AreEqual("abc", ghostDocumentRemittanceData.FieldValue);
        }
        [TestMethod]
        public void FieldName_MaximumLengthIs256()
        {
            var ghostDocumentRemittanceData = ShredSingleGhostDocumentRemittanceData(
                CreateGhostDocumentRemittanceDataElement(fieldValue: new string('*', 257)));
            Assert.AreEqual(new string('*', 256), ghostDocumentRemittanceData.FieldValue);
        }
    }
}