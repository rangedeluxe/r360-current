﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class PaymentTests : ShredderTestBase
    {
        private static Payment ShredSinglePayment(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.Payments);
        }

        [TestMethod]
        public void BatchWithTwoPayments()
        {
            var batch = CreateBatchElement();

            var transaction = CreateTransactionElement();
            batch.Add(transaction);

            transaction.Add(CreatePaymentElement());
            transaction.Add(CreatePaymentElement());

            var payments = ShredAll(batch).Payments;
            Assert.AreEqual(2, payments.Count, "Payment count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var payment = ShredSinglePayment(CreatePaymentElement());
            Assert.AreEqual(DataImportQueueId, payment.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromʽTransaction_IdʼXmlAttribute()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(transaction_id: "9876543210"));
            Assert.AreEqual(9876543210, payment.GeneratedTransactionId);
        }
        [TestMethod]
        public void GeneratedPaymentId_ComesFromʽPayment_IdʼXmlAttribute()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(payment_id: "9876543210"));
            Assert.AreEqual(9876543210, payment.GeneratedPaymentId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreatePaymentElement());
            var payment = ShredSinglePayment(transaction);
            Assert.AreEqual(123, payment.TransactionId);
        }
        [TestMethod]
        public void TransactionSequence_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionSequence: "123");
            transaction.Add(CreatePaymentElement());
            var payment = ShredSinglePayment(transaction);
            Assert.AreEqual(123, payment.TransactionSequence);
        }
        [TestMethod]
        public void BatchSequence()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(batchSequence: "123"));
            Assert.AreEqual(123, payment.BatchSequence);
        }
        [TestMethod]
        public void Amount()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(amount: "123.45"));
            Assert.AreEqual(123.45m, payment.Amount);
        }
        [TestMethod]
        public void Rt()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(rt: "abc"));
            Assert.AreEqual("abc", payment.Rt);
        }
        [TestMethod]
        public void Rt_MaximumLengthIs30()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(rt: "1234567890123456789012345678901"));
            Assert.AreEqual("123456789012345678901234567890", payment.Rt);
        }
        [TestMethod]
        public void Account()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(account: "abc"));
            Assert.AreEqual("abc", payment.Account);
        }
        [TestMethod]
        public void Account_MaximumLengthIs30()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(account: "1234567890123456789012345678901"));
            Assert.AreEqual("123456789012345678901234567890", payment.Account);
        }
        [TestMethod]
        public void Serial()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(serial: "abc"));
            Assert.AreEqual("abc", payment.Serial);
        }
        [TestMethod]
        public void Serial_MaximumLengthIs30()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(serial: "1234567890123456789012345678901"));
            Assert.AreEqual("123456789012345678901234567890", payment.Serial);
        }
        [TestMethod]
        public void TransactionCode()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(transactionCode: "abc"));
            Assert.AreEqual("abc", payment.TransactionCode);
        }
        [TestMethod]
        public void TransactionCode_MaximumLengthIs30()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(transactionCode: "1234567890123456789012345678901"));
            Assert.AreEqual("123456789012345678901234567890", payment.TransactionCode);
        }
        [TestMethod]
        public void RemitterName()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(remitterName: "abc"));
            Assert.AreEqual("abc", payment.RemitterName);
        }
        [TestMethod]
        public void RemitterName_MaximumLengthIs60()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(
                remitterName: "1234567890123456789012345678901234567890123456789012345678901"));
            Assert.AreEqual("123456789012345678901234567890123456789012345678901234567890", payment.RemitterName);
        }
        [TestMethod]
        public void Aba()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(aba: "abc"));
            Assert.AreEqual("abc", payment.Aba);
        }
        [TestMethod]
        public void Aba_MaximumLengthIs10()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(aba: "12345678901"));
            Assert.AreEqual("1234567890", payment.Aba);
        }
        [TestMethod]
        public void Dda()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(dda: "abc"));
            Assert.AreEqual("abc", payment.Dda);
        }
        [TestMethod]
        public void Dda_MaximumLengthIs35()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(dda: "123456789012345678901234567890123456"));
            Assert.AreEqual("12345678901234567890123456789012345", payment.Dda);
        }
        [TestMethod]
        public void CheckSequence()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(checkSequence: "123"));
            Assert.AreEqual(123, payment.CheckSequence);
        }
        [TestMethod]
        public void CheckSequence_IfZero_WritesZero()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(checkSequence: "0"));
            Assert.AreEqual(0, payment.CheckSequence);
        }
        [TestMethod]
        public void CheckSequence_IfEmptyString_WritesNull()
        {
            var payment = ShredSinglePayment(CreatePaymentElement(checkSequence: ""));
            Assert.AreEqual(null, payment.CheckSequence);
        }
        [TestMethod]
        public void CheckSequence_AttributeMissing_WritesNull()
        {
            var payment = ShredSinglePayment(CreatePaymentElement());
            Assert.AreEqual(null, payment.CheckSequence);
        }
    }
}