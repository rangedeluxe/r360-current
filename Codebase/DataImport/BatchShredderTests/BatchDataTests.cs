﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class BatchDataTests : ShredderTestBase
    {
        private static BatchData ShredSingleBatchData(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.BatchData);
        }

        [TestMethod]
        public void BatchWithTwoBatchDataElements()
        {
            var batch = CreateBatchElement();

            batch.Add(CreateBatchDataElement());
            batch.Add(CreateBatchDataElement());

            var batchData = ShredAll(batch).BatchData;
            Assert.AreEqual(2, batchData.Count, "Batch data count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var batchData = ShredSingleBatchData(CreateBatchDataElement());
            Assert.AreEqual(DataImportQueueId, batchData.GeneratedBatchId);
        }
        [TestMethod]
        public void FieldName()
        {
            var batchData = ShredSingleBatchData(CreateBatchDataElement(fieldName: "abc"));
            Assert.AreEqual("abc", batchData.FieldName);
        }
        [TestMethod]
        public void FieldName_MaximumLengthIs32()
        {
            var batchData = ShredSingleBatchData(CreateBatchDataElement(fieldName: "123456789012345678901234567890123"));
            Assert.AreEqual("12345678901234567890123456789012", batchData.FieldName);
        }
        [TestMethod]
        public void FieldValue()
        {
            var batchData = ShredSingleBatchData(CreateBatchDataElement(fieldValue: "abc"));
            Assert.AreEqual("abc", batchData.FieldValue);
        }
        [TestMethod]
        public void FieldValue_MaximumLengthIs256()
        {
            var batchData = ShredSingleBatchData(CreateBatchDataElement(fieldValue: new string('*', 257)));
            Assert.AreEqual(new string('*', 256), batchData.FieldValue);
        }
    }
}