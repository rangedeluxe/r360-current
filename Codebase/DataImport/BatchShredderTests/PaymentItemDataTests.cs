﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class PaymentItemDataTests : ShredderTestBase
    {
        private static PaymentItemData ShredSinglePaymentItemData(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.PaymentItemData);
        }

        [TestMethod]
        public void BatchWithTwoPaymentItemDataElements()
        {
            var batch = CreateBatchElement();

            batch.Add(CreatePaymentItemDataElement());
            batch.Add(CreatePaymentItemDataElement());

            var payments = ShredAll(batch).PaymentItemData;
            Assert.AreEqual(2, payments.Count, "Payment item data count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var paymentItemData = ShredSinglePaymentItemData(CreatePaymentItemDataElement());
            Assert.AreEqual(DataImportQueueId, paymentItemData.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transaction_id: "9876543210");
            transaction.Add(CreatePaymentItemDataElement());
            var paymentItemData = ShredSinglePaymentItemData(transaction);
            Assert.AreEqual(9876543210, paymentItemData.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreatePaymentItemDataElement());
            var paymentItemData = ShredSinglePaymentItemData(transaction);
            Assert.AreEqual(123, paymentItemData.TransactionId);
        }
        [TestMethod]
        public void GeneratedPaymentId_ComesFromʽPayment_IdʼXmlAttribute()
        {
            var paymentItemData = ShredSinglePaymentItemData(CreatePaymentItemDataElement(payment_id: "9876543210"));
            Assert.AreEqual(9876543210, paymentItemData.GeneratedPaymentId);
        }
        [TestMethod]
        public void BatchSequence_ComesFromParentPayment()
        {
            var payment = CreatePaymentElement(batchSequence: "123");
            payment.Add(CreatePaymentItemDataElement());
            var paymentItemData = ShredSinglePaymentItemData(payment);
            Assert.AreEqual(123, paymentItemData.BatchSequence);
        }
        [TestMethod]
        public void FieldName()
        {
            var paymentItemData = ShredSinglePaymentItemData(CreatePaymentItemDataElement(fieldName: "abc"));
            Assert.AreEqual("abc", paymentItemData.FieldName);
        }
        [TestMethod]
        public void FieldName_MaximumLengthIs32()
        {
            var paymentItemData = ShredSinglePaymentItemData(CreatePaymentItemDataElement(
                fieldName: "123456789012345678901234567890123"));
            Assert.AreEqual("12345678901234567890123456789012", paymentItemData.FieldName);
        }
        [TestMethod]
        public void FieldValue()
        {
            var paymentItemData = ShredSinglePaymentItemData(CreatePaymentItemDataElement(fieldValue: "abc"));
            Assert.AreEqual("abc", paymentItemData.FieldValue);
        }
        [TestMethod]
        public void FieldValue_MaximumLengthIs256()
        {
            var paymentItemData = ShredSinglePaymentItemData(CreatePaymentItemDataElement(
                fieldValue: new string('*', 257)));
            Assert.AreEqual(new string('*', 256), paymentItemData.FieldValue);
        }
    }
}