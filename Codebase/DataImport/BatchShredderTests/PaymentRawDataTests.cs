﻿using System;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class PaymentRawDataTests : ShredderTestBase
    {
        private static DataImportWorkBatchResponse ShredSingleDataImportWorkBatchResponse(XElement xml)
        {
            var shreddedBatch = ShredAll(xml);
            Assert.AreEqual(0, shreddedBatch.PaymentRawData.Count, "Raw data count");
            Assert.AreEqual(1, shreddedBatch.DataImportWorkBatchResponses.Count, "Error count");
            return shreddedBatch.DataImportWorkBatchResponses[0];
        }
        private static PaymentRawData ShredSinglePaymentRawData(XElement xml)
        {
            var shreddedBatch = ShredAll(xml);
            Assert.AreEqual(0, shreddedBatch.DataImportWorkBatchResponses.Count, "Error count");
            Assert.AreEqual(1, shreddedBatch.PaymentRawData.Count, "Raw data count");
            return shreddedBatch.PaymentRawData[0];
        }

        [TestMethod]
        public void BatchWithTwoPaymentRawDataElements_BothWithInnerText()
        {
            var batch = CreateBatchElement();

            batch.Add(CreatePaymentRawDataElement(innerText: "abc"));
            batch.Add(CreatePaymentRawDataElement(innerText: "def"));

            var payments = ShredAll(batch).PaymentRawData;
            Assert.AreEqual(2, payments.Count, "Payment raw data count");
        }
        [TestMethod]
        public void BatchWithTwoPaymentRawDataElements_BothSelfClosingElements()
        {
            // This test is important because of the way we have to read InnerText from XmlReader.
            // If we don't pay attention to self-closing elements, it's easy to read ahead too far
            // looking for InnerText, and end up skipping over some of the XML elements in the input.

            var batch = CreateBatchElement();

            batch.Add(CreatePaymentRawDataElement(innerText: null));
            batch.Add(CreatePaymentRawDataElement(innerText: null));

            var payments = ShredAll(batch).PaymentRawData;
            Assert.AreEqual(2, payments.Count, "Payment raw data count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var paymentRawData = ShredSinglePaymentRawData(CreatePaymentRawDataElement());
            Assert.AreEqual(DataImportQueueId, paymentRawData.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transaction_id: "9876543210");
            transaction.Add(CreatePaymentRawDataElement());
            var paymentRawData = ShredSinglePaymentRawData(transaction);
            Assert.AreEqual(9876543210, paymentRawData.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreatePaymentRawDataElement());
            var paymentRawData = ShredSinglePaymentRawData(transaction);
            Assert.AreEqual(123, paymentRawData.TransactionId);
        }
        [TestMethod]
        public void GeneratedPaymentId_ComesFromʽPayment_IdʼXmlAttribute()
        {
            var paymentRawData = ShredSinglePaymentRawData(CreatePaymentRawDataElement(payment_id: "9876543210"));
            Assert.AreEqual(9876543210, paymentRawData.GeneratedPaymentId);
        }
        [TestMethod]
        public void BatchSequence_ComesFromParentPayment()
        {
            var payment = CreatePaymentElement(batchSequence: "123");
            payment.Add(CreatePaymentRawDataElement());
            var paymentRawData = ShredSinglePaymentRawData(payment);
            Assert.AreEqual(123, paymentRawData.BatchSequence);
        }
        [TestMethod]
        public void RawDataPart()
        {
            var paymentRawData = ShredSinglePaymentRawData(CreatePaymentRawDataElement(rawDataPart: "123"));
            Assert.AreEqual(123, paymentRawData.RawDataPart);
        }
        [TestMethod]
        public void RawDataSequence_ComesFromRawSequenceXmlAttribute()
        {
            var paymentRawData = ShredSinglePaymentRawData(CreatePaymentRawDataElement(rawSequence: "123"));
            Assert.AreEqual(123, paymentRawData.RawDataSequence);
        }
        [TestMethod]
        public void RawData_ComesFromInnerText()
        {
            var paymentRawData = ShredSinglePaymentRawData(CreatePaymentRawDataElement(innerText: "abcdefg"));
            Assert.AreEqual("abcdefg", paymentRawData.RawData);
        }
        [TestMethod]
        public void RawData_WhenElementHasNoInnerText()
        {
            var paymentRawData = ShredSinglePaymentRawData(CreatePaymentRawDataElement(innerText: ""));
            Assert.AreEqual("", paymentRawData.RawData);
        }
        [TestMethod]
        public void RawData_WhenElementIsSelfClosing()
        {
            var paymentRawData = ShredSinglePaymentRawData(CreatePaymentRawDataElement(innerText: null));
            Assert.AreEqual("", paymentRawData.RawData);
        }
        [TestMethod]
        public void HappyPath_WhenRawDataLengthIs128OrLess()
        {
            var string128 = new string('*', 128);
            var shreddedBatch = ShredAll(CreatePaymentRawDataElement(innerText: string128));
            Assert.AreEqual(0, shreddedBatch.DataImportWorkBatchResponses.Count, "Error count");
            Assert.AreEqual(1, shreddedBatch.PaymentRawData.Count, "Raw data count");
        }
        [TestMethod]
        public void UnhappyPath_WhenRawDataLengthIs129OrGreater()
        {
            var string129 = new string('*', 129);
            var shreddedBatch = ShredAll(CreatePaymentRawDataElement(innerText: string129));
            Assert.AreEqual(1, shreddedBatch.DataImportWorkBatchResponses.Count, "Error count");
            Assert.AreEqual(0, shreddedBatch.PaymentRawData.Count, "Raw data count");
        }
        [TestMethod]
        public void UnhappyPath_GeneratedBatchId()
        {
            var response = ShredSingleDataImportWorkBatchResponse(CreatePaymentRawDataElement(
                innerText: new string('*', 129)));
            Assert.AreEqual(DataImportQueueId, response.GeneratedBatchId);
        }
        [TestMethod]
        public void UnhappyPath_ResponseStatus_Is1()
        {
            var response = ShredSingleDataImportWorkBatchResponse(CreatePaymentRawDataElement(
                innerText: new string('*', 129)));
            Assert.AreEqual(1, response.ResponseStatus);
        }
        [TestMethod]
        public void UnhappyPath_ResultsMessage()
        {
            var response = ShredSingleDataImportWorkBatchResponse(CreatePaymentRawDataElement(
                rawSequence: "123",
                innerText: new string('*', 129)));
            Assert.AreEqual(
                "Raw data sequence 123 is greater than 128 characters. (Actual length is 129.) Correct and resubmit.",
                response.ResultsMessage);
        }
        [TestMethod]
        public void UnhappyPath_BatchTrackingId_ComesFromParentBatch()
        {
            var batch = CreateBatchElement(batchTrackingId: "047f7107-ac53-460f-972a-840fde689bb0");
            batch.Add(CreatePaymentRawDataElement(innerText: new string('*', 129)));
            var response = ShredSingleDataImportWorkBatchResponse(batch);
            Assert.AreEqual(new Guid("047f7107-ac53-460f-972a-840fde689bb0"), response.BatchTrackingId);
        }
    }
}