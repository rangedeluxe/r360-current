﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class GhostDocumentItemDataTests : ShredderTestBase
    {
        private static GhostDocumentItemData ShredSingleGhostDocumentItemData(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.GhostDocumentItemData);
        }

        [TestMethod]
        public void BatchWithTwoDocuments()
        {
            var batch = CreateBatchElement();

            batch.Add(CreateGhostDocumentItemDataElement());
            batch.Add(CreateGhostDocumentItemDataElement());

            var ghostDocumentItemData = ShredAll(batch).GhostDocumentItemData;
            Assert.AreEqual(2, ghostDocumentItemData.Count, "Ghost document item data count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var ghostDocumentItemData = ShredSingleGhostDocumentItemData(CreateGhostDocumentItemDataElement());
            Assert.AreEqual(DataImportQueueId, ghostDocumentItemData.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transaction_id: "9876543210");
            transaction.Add(CreateGhostDocumentItemDataElement());
            var ghostDocumentItemData = ShredSingleGhostDocumentItemData(transaction);
            Assert.AreEqual(9876543210, ghostDocumentItemData.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreateGhostDocumentItemDataElement());
            var ghostDocumentItemData = ShredSingleGhostDocumentItemData(transaction);
            Assert.AreEqual(123, ghostDocumentItemData.TransactionId);
        }
        [TestMethod]
        public void GeneratedGhostDocumentId_ComesFromʽGhostDocument_IdʼXmlAttribute()
        {
            var ghostDocumentItemData = ShredSingleGhostDocumentItemData(CreateGhostDocumentItemDataElement(
                ghostDocument_id: "9876543210"));
            Assert.AreEqual(9876543210, ghostDocumentItemData.GeneratedGhostDocumentId);
        }
        [TestMethod]
        public void BatchSequence()
        {
            var ghostDocumentItemData = ShredSingleGhostDocumentItemData(CreateGhostDocumentItemDataElement(
                batchSequence: "123"));
            Assert.AreEqual(123, ghostDocumentItemData.BatchSequence);
        }
        [TestMethod]
        public void FieldName()
        {
            var ghostDocumentItemData = ShredSingleGhostDocumentItemData(CreateGhostDocumentItemDataElement(
                fieldName: "abc"));
            Assert.AreEqual("abc", ghostDocumentItemData.FieldName);
        }
        [TestMethod]
        public void FieldName_MaximumLengthIs32()
        {
            var ghostDocumentItemData = ShredSingleGhostDocumentItemData(CreateGhostDocumentItemDataElement(
                fieldName: "123456789012345678901234567890123"));
            Assert.AreEqual("12345678901234567890123456789012", ghostDocumentItemData.FieldName);
        }
        [TestMethod]
        public void FieldValue()
        {
            var ghostDocumentItemData = ShredSingleGhostDocumentItemData(CreateGhostDocumentItemDataElement(
                fieldValue: "abc"));
            Assert.AreEqual("abc", ghostDocumentItemData.FieldValue);
        }
        [TestMethod]
        public void FieldValue_MaximumLengthIs256()
        {
            var ghostDocumentItemData = ShredSingleGhostDocumentItemData(CreateGhostDocumentItemDataElement(
                fieldValue: new string('*', 257)));
            Assert.AreEqual(new string('*', 256), ghostDocumentItemData.FieldValue);
        }
    }
}