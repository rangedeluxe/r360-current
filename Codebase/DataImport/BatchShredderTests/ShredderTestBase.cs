﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
// ReSharper disable InconsistentNaming

namespace WFS.LTA.DataImport
{
    public class ShredderTestBase
    {
        protected const long DataImportQueueId = 0x123456789ABC;
        private const string DefaultDate = "2016-01-01";

        protected static XElement CreateBatchDataElement(string fieldName = "1", string fieldValue = "1")
        {
            var batchData = new XElement("BatchData",
                new XAttribute("FieldName", fieldName),
                new XAttribute("FieldValue", fieldValue));
            return batchData;
        }
        protected static XElement CreateBatchElement(string depositDate = DefaultDate, string batchDate = DefaultDate,
            string processingDate = DefaultDate, string bankId = "1", string clientId = "1", string batchId = "1",
            string batchNumber = "1", string batchSiteCode = "1", string batchSource = "defaultBatchSource",
            string paymentType = "defaultPaymentType", string batchCueId = "1",
            string batchTrackingId = "11111111-1111-1111-1111-111111111111", string aba = null, string dda = null,
            string fileHash = null, string fileSignature = null)
        {
            var batch = new XElement("Batch",
                new XAttribute("DepositDate", depositDate),
                new XAttribute("BatchDate", batchDate),
                new XAttribute("ProcessingDate", processingDate),
                new XAttribute("BankID", bankId),
                new XAttribute("ClientID", clientId),
                new XAttribute("BatchID", batchId),
                new XAttribute("BatchNumber", batchNumber),
                new XAttribute("BatchSiteCode", batchSiteCode),
                new XAttribute("BatchSource", batchSource),
                new XAttribute("PaymentType", paymentType),
                new XAttribute("BatchCueID", batchCueId),
                new XAttribute("BatchTrackingID", batchTrackingId));

            if (aba != null)
                batch.Add(new XAttribute("ABA", aba));
            if (dda != null)
                batch.Add(new XAttribute("DDA", dda));
            if (fileHash != null)
                batch.Add(new XAttribute("FileHash", fileHash));
            if (fileSignature != null)
                batch.Add(new XAttribute("FileSignature", fileSignature));

            return batch;
        }
        protected static XElement CreateDocumentElement(string transaction_id = "1", string document_id = "1",
            string batchSequence = "1", string documentSequence = "1", string sequenceWithinTransaction = "1",
            string documentDescriptor = "1", string isCorrespondence = "0")
        {
            var document = new XElement("Document",
                new XAttribute("Transaction_Id", transaction_id),
                new XAttribute("Document_Id", document_id),
                new XAttribute("BatchSequence", batchSequence),
                new XAttribute("DocumentSequence", documentSequence),
                new XAttribute("SequenceWithinTransaction", sequenceWithinTransaction),
                new XAttribute("DocumentDescriptor", documentDescriptor),
                new XAttribute("IsCorrespondence", isCorrespondence));
            return document;
        }
        protected static XElement CreateDocumentItemDataElement(string document_id = "1",
            string fieldName = "1", string fieldValue = "1")
        {
            var documentItemData = new XElement("DocumentItemData",
                new XAttribute("Document_Id", document_id),
                new XAttribute("FieldName", fieldName),
                new XAttribute("FieldValue", fieldValue));
            return documentItemData;
        }
        protected static XElement CreateDocumentRemittanceDataElement(string document_id = "1",
            string remittanceDataRecord_id = "1", string batchSequence = "1",
            string fieldName = "1", string fieldValue = "1")
        {
            var documentRemittanceData = new XElement("DocumentRemittanceData",
                new XAttribute("Document_Id", document_id),
                new XAttribute("RemittanceDataRecord_Id", remittanceDataRecord_id),
                new XAttribute("BatchSequence", batchSequence),
                new XAttribute("FieldName", fieldName),
                new XAttribute("FieldValue", fieldValue));
            return documentRemittanceData;
        }
        protected static XElement CreateGhostDocumentElement(string transaction_id = "1", string ghostDocument_id = "1",
            string batchSequence = "1", string isCorrespondence = "0")
        {
            var ghostDocument = new XElement("GhostDocument",
                new XAttribute("Transaction_Id", transaction_id),
                new XAttribute("GhostDocument_Id", ghostDocument_id),
                new XAttribute("BatchSequence", batchSequence),
                new XAttribute("IsCorrespondence", isCorrespondence));
            return ghostDocument;
        }
        protected static XElement CreateGhostDocumentItemDataElement(string ghostDocument_id = "1",
            string batchSequence = "1", string fieldName = "1", string fieldValue = "1")
        {
            var ghostDocumentItemData = new XElement("GhostDocumentItemData",
                new XAttribute("GhostDocument_Id", ghostDocument_id),
                new XAttribute("BatchSequence", batchSequence),
                new XAttribute("FieldName", fieldName),
                new XAttribute("FieldValue", fieldValue));
            return ghostDocumentItemData;
        }
        protected static XElement CreateGhostDocumentRemittanceDataElement(string ghostDocument_id = "1",
            string remittanceDataRecord_id = "1", string batchSequence = "1",
            string fieldName = "1", string fieldValue = "1")
        {
            var ghostDocumentRemittanceData = new XElement("GhostDocumentRemittanceData",
                new XAttribute("GhostDocument_Id", ghostDocument_id),
                new XAttribute("RemittanceDataRecord_Id", remittanceDataRecord_id),
                new XAttribute("BatchSequence", batchSequence),
                new XAttribute("FieldName", fieldName),
                new XAttribute("FieldValue", fieldValue));
            return ghostDocumentRemittanceData;
        }
        protected static XElement CreatePaymentElement(string transaction_id = "1", string payment_id = "1",
            string batchSequence = "1", string amount = "1", string rt = "1", string account = "1", string serial = "1",
            string transactionCode = "1", string remitterName = "1", string aba = "1", string dda = "1",
            string checkSequence = null)
        {
            var payment = new XElement("Payment",
                new XAttribute("Transaction_Id", transaction_id),
                new XAttribute("Payment_Id", payment_id),
                new XAttribute("BatchSequence", batchSequence),
                new XAttribute("Amount", amount),
                new XAttribute("RT", rt),
                new XAttribute("Account", account),
                new XAttribute("Serial", serial),
                new XAttribute("TransactionCode", transactionCode),
                new XAttribute("RemitterName", remitterName),
                new XAttribute("ABA", aba),
                new XAttribute("DDA", dda));

            if (checkSequence != null)
                payment.Add(new XAttribute("CheckSequence", checkSequence));

            return payment;
        }
        protected static XElement CreatePaymentItemDataElement(string payment_id = "1",
            string fieldName = "1", string fieldValue = "1")
        {
            var paymentItemData = new XElement("PaymentItemData",
                new XAttribute("Payment_Id", payment_id),
                new XAttribute("FieldName", fieldName),
                new XAttribute("FieldValue", fieldValue));
            return paymentItemData;
        }
        protected static XElement CreatePaymentRawDataElement(string payment_id = "1", string rawDataPart = "1",
            string rawSequence = "1", string innerText = "1")
        {
            var paymentRawData = new XElement("PaymentRawData",
                new XAttribute("Payment_Id", payment_id),
                new XAttribute("RawDataPart", rawDataPart),
                new XAttribute("RawSequence", rawSequence));

            if (innerText != null)
                paymentRawData.Add(innerText);

            return paymentRawData;
        }
        protected static XElement CreatePaymentRemittanceDataElement(string payment_id = "1",
            string fieldName = "1", string fieldValue = "1")
        {
            var paymentRemittanceData = new XElement("PaymentRemittanceData",
                new XAttribute("Payment_Id", payment_id),
                new XAttribute("FieldName", fieldName),
                new XAttribute("FieldValue", fieldValue));
            return paymentRemittanceData;
        }
        protected static XElement CreateTransactionElement(string transaction_id = "1", string transactionId = "1",
            string transactionSequence = "1", string transactionHash = null, string transactionSignature = null)
        {
            var transaction = new XElement("Transaction",
                new XAttribute("Transaction_Id", transaction_id),
                new XAttribute("TransactionID", transactionId),
                new XAttribute("TransactionSequence", transactionSequence));

            if (transactionHash != null)
                transaction.Add(new XAttribute("TransactionHash", transactionHash));
            if (transactionSignature != null)
                transaction.Add(new XAttribute("TransactionSignature", transactionSignature));

            return transaction;
        }
        protected static InMemoryShreddedBatch ShredAll(XElement xml)
        {
            var shreddedBatch = new InMemoryShreddedBatch();
            var shredder = new BatchShredder(xml.CreateReader(), DataImportQueueId, shreddedBatch);
            shredder.ProcessAll();
            return shreddedBatch;
        }
        protected static T ShredSingle<T>(XElement xml, Func<InMemoryShreddedBatch, IList<T>> getCollection)
        {
            var all = getCollection(ShredAll(xml));
            if (all.Count != 1)
                Assert.Fail($"Expected exactly 1 {typeof(T).Name} but found {all.Count}");
            return all[0];
        }
    }
}