﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class PaymentRemittanceDataTests : ShredderTestBase
    {
        private static PaymentRemittanceData ShredSinglePaymentRemittanceData(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.PaymentRemittanceData);
        }

        [TestMethod]
        public void BatchWithTwoPaymentRemittanceDataElements()
        {
            var batch = CreateBatchElement();

            batch.Add(CreatePaymentRemittanceDataElement());
            batch.Add(CreatePaymentRemittanceDataElement());

            var paymentRemittanceData = ShredAll(batch).PaymentRemittanceData;
            Assert.AreEqual(2, paymentRemittanceData.Count, "Payment remittance data count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var paymentRemittanceData = ShredSinglePaymentRemittanceData(CreatePaymentRemittanceDataElement());
            Assert.AreEqual(DataImportQueueId, paymentRemittanceData.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transaction_id: "9876543210");
            transaction.Add(CreatePaymentRemittanceDataElement());
            var paymentRemittanceData = ShredSinglePaymentRemittanceData(transaction);
            Assert.AreEqual(9876543210, paymentRemittanceData.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreatePaymentRemittanceDataElement());
            var paymentRemittanceData = ShredSinglePaymentRemittanceData(transaction);
            Assert.AreEqual(123, paymentRemittanceData.TransactionId);
        }
        [TestMethod]
        public void GeneratedPaymentId_ComesFromʽPayment_IdʼXmlAttribute()
        {
            var paymentRemittanceData = ShredSinglePaymentRemittanceData(CreatePaymentRemittanceDataElement(
                payment_id: "9876543210"));
            Assert.AreEqual(9876543210, paymentRemittanceData.GeneratedPaymentId);
        }
        [TestMethod]
        public void BatchSequence_ComesFromParentPayment()
        {
            var payment = CreatePaymentElement(batchSequence: "123");
            payment.Add(CreatePaymentRemittanceDataElement());
            var paymentRemittanceData = ShredSinglePaymentRemittanceData(payment);
            Assert.AreEqual(123, paymentRemittanceData.BatchSequence);
        }
        [TestMethod]
        public void FieldName()
        {
            var paymentRemittanceData = ShredSinglePaymentRemittanceData(CreatePaymentRemittanceDataElement(
                fieldName: "abc"));
            Assert.AreEqual("abc", paymentRemittanceData.FieldName);
        }
        [TestMethod]
        public void FieldName_MaximumLengthIs32()
        {
            var paymentRemittanceData = ShredSinglePaymentRemittanceData(CreatePaymentRemittanceDataElement(
                fieldName: "123456789012345678901234567890123"));
            Assert.AreEqual("12345678901234567890123456789012", paymentRemittanceData.FieldName);
        }
        [TestMethod]
        public void FieldValue()
        {
            var paymentRemittanceData = ShredSinglePaymentRemittanceData(CreatePaymentRemittanceDataElement(
                fieldValue: "abc"));
            Assert.AreEqual("abc", paymentRemittanceData.FieldValue);
        }
        [TestMethod]
        public void FieldValue_MaximumLengthIs256()
        {
            var paymentRemittanceData = ShredSinglePaymentRemittanceData(CreatePaymentRemittanceDataElement(
                fieldValue: new string('*', 257)));
            Assert.AreEqual(new string('*', 256), paymentRemittanceData.FieldValue);
        }
    }
}