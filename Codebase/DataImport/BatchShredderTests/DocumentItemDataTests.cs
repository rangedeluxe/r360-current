﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class DocumentItemDataTests : ShredderTestBase
    {
        private static DocumentItemData ShredSingleDocumentItemData(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.DocumentItemData);
        }

        [TestMethod]
        public void BatchWithTwoDocumentItemDataElements()
        {
            var batch = CreateBatchElement();

            batch.Add(CreateDocumentItemDataElement());
            batch.Add(CreateDocumentItemDataElement());

            var documentItemData = ShredAll(batch).DocumentItemData;
            Assert.AreEqual(2, documentItemData.Count, "Document item data count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var documentItemData = ShredSingleDocumentItemData(CreateDocumentItemDataElement());
            Assert.AreEqual(DataImportQueueId, documentItemData.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transaction_id: "9876543210");
            transaction.Add(CreateDocumentItemDataElement());
            var documentItemData = ShredSingleDocumentItemData(transaction);
            Assert.AreEqual(9876543210, documentItemData.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreateDocumentItemDataElement());
            var documentItemData = ShredSingleDocumentItemData(transaction);
            Assert.AreEqual(123, documentItemData.TransactionId);
        }
        [TestMethod]
        public void BatchSequence_ComesFromParentDocument()
        {
            var document = CreateDocumentElement(batchSequence: "123");
            document.Add(CreateDocumentItemDataElement());
            var documentItemData = ShredSingleDocumentItemData(document);
            Assert.AreEqual(123, documentItemData.BatchSequence);
        }
        [TestMethod]
        public void GeneratedDocumentId_ComesFromʽDocument_IdʼXmlAttribute()
        {
            var documentItemData = ShredSingleDocumentItemData(CreateDocumentItemDataElement(document_id: "9876543210"));
            Assert.AreEqual(9876543210, documentItemData.GeneratedDocumentId);
        }
        [TestMethod]
        public void FieldName()
        {
            var documentItemData = ShredSingleDocumentItemData(CreateDocumentItemDataElement(fieldName: "abc"));
            Assert.AreEqual("abc", documentItemData.FieldName);
        }
        [TestMethod]
        public void FieldName_MaximumLengthIs32()
        {
            var documentItemData = ShredSingleDocumentItemData(CreateDocumentItemDataElement(
                fieldName: "123456789012345678901234567890123"));
            Assert.AreEqual("12345678901234567890123456789012", documentItemData.FieldName);
        }
        [TestMethod]
        public void FieldValue()
        {
            var documentItemData = ShredSingleDocumentItemData(CreateDocumentItemDataElement(fieldValue: "abc"));
            Assert.AreEqual("abc", documentItemData.FieldValue);
        }
        [TestMethod]
        public void FieldValue_MaximumLengthIs256()
        {
            var documentItemData = ShredSingleDocumentItemData(CreateDocumentItemDataElement(
                fieldValue: new string('*', 257)));
            Assert.AreEqual(new string('*', 256), documentItemData.FieldValue);
        }
    }
}