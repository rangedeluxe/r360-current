﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class DocumentRemittanceDataTests : ShredderTestBase
    {
        private static DocumentRemittanceData ShredSingleDocumentRemittanceData(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.DocumentRemittanceData);
        }

        [TestMethod]
        public void BatchWithTwoDocumentRemittanceDataElements()
        {
            var batch = CreateBatchElement();

            batch.Add(CreateDocumentRemittanceDataElement());
            batch.Add(CreateDocumentRemittanceDataElement());

            var documentRemittanceData = ShredAll(batch).DocumentRemittanceData;
            Assert.AreEqual(2, documentRemittanceData.Count, "Document remittance data count");
        }
        [TestMethod]
        public void GeneratedBatchId()
        {
            var documentRemittanceData = ShredSingleDocumentRemittanceData(CreateDocumentRemittanceDataElement());
            Assert.AreEqual(DataImportQueueId, documentRemittanceData.GeneratedBatchId);
        }
        [TestMethod]
        public void GeneratedTransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transaction_id: "9876543210");
            transaction.Add(CreateDocumentRemittanceDataElement());
            var documentRemittanceData = ShredSingleDocumentRemittanceData(transaction);
            Assert.AreEqual(9876543210, documentRemittanceData.GeneratedTransactionId);
        }
        [TestMethod]
        public void TransactionId_ComesFromParentTransaction()
        {
            var transaction = CreateTransactionElement(transactionId: "123");
            transaction.Add(CreateDocumentRemittanceDataElement());
            var documentRemittanceData = ShredSingleDocumentRemittanceData(transaction);
            Assert.AreEqual(123, documentRemittanceData.TransactionId);
        }
        [TestMethod]
        public void GeneratedDocumentId_ComesFromʽDocument_IdʼXmlAttribute()
        {
            var documentRemittanceData = ShredSingleDocumentRemittanceData(CreateDocumentRemittanceDataElement(
                document_id: "9876543210"));
            Assert.AreEqual(9876543210, documentRemittanceData.GeneratedDocumentId);
        }
        [TestMethod]
        public void GeneratedRemittanceDataRecordId_ComesFromʽRemittanceDataRecord_IdʼXmlAttribute()
        {
            var documentRemittanceData = ShredSingleDocumentRemittanceData(CreateDocumentRemittanceDataElement(
                remittanceDataRecord_id: "9876543210"));
            Assert.AreEqual(9876543210, documentRemittanceData.GeneratedRemittanceDataRecordId);
        }
        [TestMethod]
        public void BatchSequence()
        {
            var documentRemittanceData = ShredSingleDocumentRemittanceData(CreateDocumentRemittanceDataElement(
                batchSequence: "123"));
            Assert.AreEqual(123, documentRemittanceData.BatchSequence);
        }
        [TestMethod]
        public void FieldName()
        {
            var documentRemittanceData = ShredSingleDocumentRemittanceData(CreateDocumentRemittanceDataElement(
                fieldName: "abc"));
            Assert.AreEqual("abc", documentRemittanceData.FieldName);
        }
        [TestMethod]
        public void FieldName_MaximumLengthIs32()
        {
            var documentRemittanceData = ShredSingleDocumentRemittanceData(CreateDocumentRemittanceDataElement(
                fieldName: "123456789012345678901234567890123"));
            Assert.AreEqual("12345678901234567890123456789012", documentRemittanceData.FieldName);
        }
        [TestMethod]
        public void FieldValue()
        {
            var documentRemittanceData = ShredSingleDocumentRemittanceData(CreateDocumentRemittanceDataElement(
                fieldValue: "abc"));
            Assert.AreEqual("abc", documentRemittanceData.FieldValue);
        }
        [TestMethod]
        public void FieldValue_MaximumLengthIs256()
        {
            var documentRemittanceData = ShredSingleDocumentRemittanceData(CreateDocumentRemittanceDataElement(
                fieldValue: new string('*', 257)));
            Assert.AreEqual(new string('*', 256), documentRemittanceData.FieldValue);
        }
    }
}