﻿using System;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class BatchTests : ShredderTestBase
    {
        private static Batch ShredSingleBatch(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.Batches);
        }

        [TestMethod]
        public void GeneratedBatchId()
        {
            var batch = ShredSingleBatch(CreateBatchElement());
            Assert.AreEqual(DataImportQueueId, batch.GeneratedBatchId);
        }
        [TestMethod]
        public void DepositDate()
        {
            var batch = ShredSingleBatch(CreateBatchElement(depositDate: "2016-01-15"));
            Assert.AreEqual(new DateTime(2016, 1, 15), batch.DepositDate);
        }
        [TestMethod]
        public void BatchDate()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchDate: "2016-02-10"));
            Assert.AreEqual(new DateTime(2016, 2, 10), batch.BatchDate);
        }
        [TestMethod]
        public void ImmutableDate_ComesFromProcessingDateXmlAttribute()
        {
            var batch = ShredSingleBatch(CreateBatchElement(processingDate: "2016-03-01"));
            Assert.AreEqual(new DateTime(2016, 3, 1), batch.ImmutableDate);
        }
        [TestMethod]
        public void SiteBankId_ComesFromBankIdXmlAttribute()
        {
            var batch = ShredSingleBatch(CreateBatchElement(bankId: "123"));
            Assert.AreEqual(123, batch.SiteBankId);
        }
        [TestMethod]
        public void SiteClientAccountId_ComesFromClientIdXmlAttribute()
        {
            var batch = ShredSingleBatch(CreateBatchElement(clientId: "456"));
            Assert.AreEqual(456, batch.SiteClientAccountId);
        }
        [TestMethod]
        public void SourceBatchId_ComesFromBatchIdXmlAttribute_IfNonZero()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchId: "1234567890123"));
            Assert.AreEqual(1234567890123, batch.SourceBatchId);
        }
        [TestMethod]
        public void SourceBatchId_ComesFromBatchIdXmlAttribute_IfZero_WritesNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchId: "0"));
            Assert.AreEqual(null, batch.SourceBatchId);
        }
        [TestMethod]
        public void BatchNumber_IfNonZero()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchNumber: "123"));
            Assert.AreEqual(123, batch.BatchNumber);
        }
        [TestMethod]
        public void BatchNumber_IfZero_WritesNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchNumber: "0"));
            Assert.AreEqual(null, batch.BatchNumber);
        }
        [TestMethod]
        public void BatchSiteCode()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchSiteCode: "123"));
            Assert.AreEqual(123, batch.BatchSiteCode);
        }
        [TestMethod]
        public void BatchSource()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchSource: "abc"));
            Assert.AreEqual("abc", batch.BatchSource);
        }
        [TestMethod]
        public void BatchSource_MaximumLengthIs30()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchSource: "1234567890123456789012345678901"));
            Assert.AreEqual("123456789012345678901234567890", batch.BatchSource);
        }
        [TestMethod]
        public void BatchPaymentType_ComesFromPaymentTypeXmlAttribute()
        {
            var batch = ShredSingleBatch(CreateBatchElement(paymentType: "abc"));
            Assert.AreEqual("abc", batch.BatchPaymentType);
        }
        [TestMethod]
        public void BatchPaymentType_MaximumLengthIs30()
        {
            var batch = ShredSingleBatch(CreateBatchElement(paymentType: "1234567890123456789012345678901"));
            Assert.AreEqual("123456789012345678901234567890", batch.BatchPaymentType);
        }
        [TestMethod]
        public void BatchCueId()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchCueId: "123"));
            Assert.AreEqual(123, batch.BatchCueId);
        }
        [TestMethod]
        public void BatchTrackingId()
        {
            var batch = ShredSingleBatch(CreateBatchElement(batchTrackingId: "d2d18cbb-08ae-4931-9fb9-afc303ae1563"));
            Assert.AreEqual(new Guid("d2d18cbb-08ae-4931-9fb9-afc303ae1563"), batch.BatchTrackingId);
        }
        [TestMethod]
        public void Aba()
        {
            var batch = ShredSingleBatch(CreateBatchElement(aba: "123"));
            Assert.AreEqual("123", batch.Aba);
        }
        [TestMethod]
        public void Aba_MaximumLengthIs10()
        {
            var batch = ShredSingleBatch(CreateBatchElement(aba: "12345678901"));
            Assert.AreEqual("1234567890", batch.Aba);
        }
        [TestMethod]
        public void Aba_EmptyString_SetsPropertyToNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement(aba: ""));
            Assert.AreEqual(null, batch.Aba);
        }
        [TestMethod]
        public void Aba_AttributeMissing_SetsPropertyToNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement());
            Assert.AreEqual(null, batch.Aba);
        }
        [TestMethod]
        public void Dda()
        {
            var batch = ShredSingleBatch(CreateBatchElement(dda: "123"));
            Assert.AreEqual("123", batch.Dda);
        }
        [TestMethod]
        public void Dda_MaximumLengthIs35()
        {
            var batch = ShredSingleBatch(CreateBatchElement(dda: "123456789012345678901234567890123456"));
            Assert.AreEqual("12345678901234567890123456789012345", batch.Dda);
        }
        [TestMethod]
        public void Dda_EmptyString_SetsPropertyToNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement(dda: ""));
            Assert.AreEqual(null, batch.Dda);
        }
        [TestMethod]
        public void Dda_AttributeMissing_SetsPropertyToNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement());
            Assert.AreEqual(null, batch.Dda);
        }
        [TestMethod]
        public void FileHash()
        {
            var batch = ShredSingleBatch(CreateBatchElement(fileHash: "123"));
            Assert.AreEqual("123", batch.FileHash);
        }
        [TestMethod]
        public void FileHash_MaximumLengthIs40()
        {
            var batch = ShredSingleBatch(CreateBatchElement(fileHash: "12345678901234567890123456789012345678901"));
            Assert.AreEqual("1234567890123456789012345678901234567890", batch.FileHash);
        }
        [TestMethod]
        public void FileHash_EmptyString_SetsPropertyToNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement(fileHash: ""));
            Assert.AreEqual(null, batch.FileHash);
        }
        [TestMethod]
        public void FileHash_AttributeMissing_SetsPropertyToNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement());
            Assert.AreEqual(null, batch.FileHash);
        }
        [TestMethod]
        public void FileSignature()
        {
            var batch = ShredSingleBatch(CreateBatchElement(fileSignature: "123"));
            Assert.AreEqual("123", batch.FileSignature);
        }
        [TestMethod]
        public void FileSignature_MaximumLengthIs55()
        {
            var batch = ShredSingleBatch(
                CreateBatchElement(fileSignature: "12345678901234567890123456789012345678901234567890123456"));
            Assert.AreEqual("1234567890123456789012345678901234567890123456789012345", batch.FileSignature);
        }
        [TestMethod]
        public void FileSignature_EmptyString_SetsPropertyToNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement(fileSignature: ""));
            Assert.AreEqual(null, batch.FileSignature);
        }
        [TestMethod]
        public void FileSignature_AttributeMissing_SetsPropertyToNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement());
            Assert.AreEqual(null, batch.FileSignature);
        }
        [TestMethod]
        public void FileHashChecker_FileHashIsShorterThan10Characters_IsFileHashParsedAsInt64()
        {
            var batch = ShredSingleBatch(CreateBatchElement(fileHash: "123"));
            Assert.AreEqual(123, batch.FileHashChecker);
        }
        [TestMethod]
        public void FileHashChecker_FileHashIsExactly10Characters_IsFileHashParsedAsInt64()
        {
            var batch = ShredSingleBatch(CreateBatchElement(fileHash: "9876543210"));
            Assert.AreEqual(9876543210, batch.FileHashChecker);
        }
        [TestMethod]
        public void FileHashChecker_FileHashIsLongerThan10Characters_IsFirst10CharactersParsedAsHexadecimal()
        {
            var batch = ShredSingleBatch(CreateBatchElement(fileHash: "12345678901"));
            Assert.AreEqual(0x1234567890, batch.FileHashChecker);
        }
        [TestMethod]
        public void FileHashChecker_FileHashIsEmptyString_IsNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement(fileHash: ""));
            Assert.AreEqual(null, batch.FileHashChecker);
        }
        [TestMethod]
        public void FileHashChecker_FileHashIsMissingAttribute_IsNull()
        {
            var batch = ShredSingleBatch(CreateBatchElement());
            Assert.AreEqual(null, batch.FileHashChecker);
        }
    }
}
