﻿using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date:
*
* Purpose:
*
* Modification History
* CR 33230 WJS 2/10/2012
*           - Initial Revision
* CR 52633 WJS 5/22/2012
 *          - Change Namespace
******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public class GhostDocument
    {
        public XmlElement BuildXMLNode(XmlDocument doc)
        {
            return doc.CreateElement("GhostDocument");
        }
        public XmlElement BuildXMLNode(bool isCorrespondence, XmlDocument doc)
        {
            XmlElement el = doc.CreateElement("GhostDocument");
            el.SetAttribute("IsCorrespondence", isCorrespondence ? "1" : "0");
            return el;
        }
    }
}
