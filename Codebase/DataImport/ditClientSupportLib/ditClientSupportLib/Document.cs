﻿using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date:
*
* Purpose:
*
* Modification History
* CR 33230 WJS 2/10/2012
*           - Initial Revision
* CR 52633 WJS 5/22/2012
 *          - Change Namespace
******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public  class Document
    {
        public int BatchSequence { get; set; }
        public string DocumentDescriptor { get; set; }
        public string SequenceWithinTransaction { get; set; }
        public string DocumentSequence { get; set; }
        public bool IsCorrespondence { get; set; }

        public XmlElement BuildXMLNode(XmlDocument doc)
        {
            XmlElement el = (XmlElement)doc.CreateElement("Document");
            el.SetAttribute("BatchSequence", BatchSequence.ToString());
            el.SetAttribute("DocumentDescriptor", DocumentDescriptor);
            el.SetAttribute("SequenceWithinTransaction", SequenceWithinTransaction);
            el.SetAttribute("DocumentSequence", DocumentSequence);
            el.SetAttribute("IsCorrespondence", IsCorrespondence ? "1" : "0");

            return el;
        }
    }
}
