﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.IO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR 33230 WJS 2/10/2012
*           - Initial Revision -
* CR 52633 WJS 5/22/2012
 *          - Change Namespace
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientAPI
{
    // call Validate to validate an xml file/stream against an xsd file/stream
    // returns true if valid, false otherwise. GetError will return
    // any error messages taht were produced in the last validation
    public static class XML_XSD_Validator
    {
        static int numErrors = 0;
        static string msgError = "";

       
       
        public  static bool Validate(string xml, string xsd)
        {
            bool bValid = false;
            ClearErrorMessage();
            try
            {
                using (XmlTextReader tr = new XmlTextReader(new StringReader(xsd)))
                {
                    tr.Read();
                    XmlSchemaSet schema = new XmlSchemaSet();
                    schema.Add(null, tr);

                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.DtdProcessing = DtdProcessing.Prohibit;
                    settings.XmlResolver = null;
                    settings.ValidationType = ValidationType.Schema;
                    settings.Schemas.Add(schema);
                    settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                    settings.ValidationEventHandler += new ValidationEventHandler(ErrorHandler);
                    using (XmlReader reader = XmlReader.Create(new StringReader(xml), settings))
                    {

                        // Validate XML data
                        while (reader.Read())
                            ;
                        reader.Close();
                    }

                    // exception if validation failed
                    if (numErrors > 0)
                        throw new Exception(msgError);
                }

                bValid = true;
              
            }
            catch
            {
                msgError = "Validation failed\r\n" + msgError;
                bValid = false;
            }
            return bValid;
        }

        private static void ErrorHandler(object sender, ValidationEventArgs args)
        {
            msgError = msgError + "\r\n" + args.Message;
            numErrors++;
        }

        // if a validation error occurred, this will return the message
        public static string GetError()
        {
            return msgError;
        }

        private static void ClearErrorMessage()
        {
            msgError = "";
            numErrors = 0;
        }
       

    }
}
