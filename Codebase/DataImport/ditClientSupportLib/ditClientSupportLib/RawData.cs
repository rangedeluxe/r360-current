﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Chris Colombo
* Date:
*
* Purpose:
*
* Modification History
* WI 143075 CMC 05/22/2014
*           - Initial Revision
* WI 148599 CMC 06/18/2014
*           - Changes to ClientSupportLib for RawDataRecord and RawData classes
******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public class RawData
    {
        public const int ChunkSize = 128;

        private readonly string _unparsedData;
        private readonly int _sequence;

        public RawData(string unparsedData, int sequence)
        {
            _unparsedData = unparsedData;
            _sequence = sequence;
        }

        public IEnumerable<XElement> ToRawDataElements()
        {
            return Chunker.ChunkData(_unparsedData, ChunkSize)
                .Select((chunk, index) =>
                {
                    var element = new XElement("RawData");
                    element.SetAttributeValue("RawSequence", _sequence.ToString(CultureInfo.CurrentCulture));
                    element.SetAttributeValue("RawDataPart", (index + 1).ToString(CultureInfo.CurrentCulture));
                    element.SetValue(chunk ?? string.Empty);
                    return element;
                });
        }
    }
}
