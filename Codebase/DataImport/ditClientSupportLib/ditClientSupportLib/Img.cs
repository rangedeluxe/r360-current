﻿using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date:
*
* Purpose:
*
* Modification History
* CR 52633 WJS 6/28/2012
*           - Initial Revision
******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public class Img
    {
        public string ClientFilePath { get; set; } = "";
        public string Page { get; set; }
        public string ColorMode { get; set; }
        public bool AllowMultiPageTiff { get; set; }

        public XmlElement BuildXMLNode(XmlDocument doc)
        {
            XmlElement el = doc.CreateElement("img");
            el.SetAttribute("ClientFilePath", ClientFilePath.Trim());
            el.SetAttribute("Page", Page?.Trim() ?? "0");
            el.SetAttribute("ColorMode", ColorMode?.Trim() ?? "");
            el.SetAttribute("AllowMultiPageTiff", AllowMultiPageTiff ? "true" : "false");
            return el;
        }
    }
}
