﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR 33230 WJS 2/10/2012
*           - Initial Revision
* CR 52633 WJS 5/22/2012
 *          - Change Namespace
* WI 128741 CMC 02/11/2014
*           - Adding DataRetentionDays and ImageRetentionDays 
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public  class Client
    {

        public  Int32 SiteCode
        {
            get;
            set;
        }
        public  Int32 ClientID
        {
            get;
            set;
        }
        public  Int16 OnlineColorMode
        {
            get;
            set;
        }
        public  string ShortName
        {
            get;
            set;
        }

        public  string LongName
        {
            get;
            set;
        }
        public  string DDA
        {
            get;
            set;
        }

        public  string POBox
        {
            get;
            set;
        }

        public Int32 DataRetentionDays
        {
            get;
            set;
        }

        public Int32 ImageRetentionDays
        {
            get;
            set;
        }

        public  XmlElement BuildXMLNode(XmlDocument doc)
        {
            XmlElement el = doc.CreateElement("Client");
            el.SetAttribute("ClientID", ClientID.ToString());
            if (!String.IsNullOrEmpty(POBox))
            {
                el.SetAttribute("POBOX", POBox);
            }
            el.SetAttribute("SiteCode", SiteCode.ToString());
            if (OnlineColorMode != 0)
            {
                el.SetAttribute("OnlineColorMode", OnlineColorMode.ToString());
            }
            el.SetAttribute("ShortName", ShortName);
            el.SetAttribute("LongName", LongName);
            if (!String.IsNullOrEmpty(DDA))
            {
                el.SetAttribute("DDA", DDA);
            }

            el.SetAttribute("DataRetentionDays", DataRetentionDays.ToString(CultureInfo.CurrentCulture));
            el.SetAttribute("ImageRetentionDays", ImageRetentionDays.ToString(CultureInfo.CurrentCulture));

            return el;
        }

    }
}
