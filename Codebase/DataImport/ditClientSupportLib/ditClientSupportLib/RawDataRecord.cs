﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Chris Colombo
* Date: 
*
* Purpose: 
*
* Modification History
* WI 143074 CMC 05/22/2014
*           - Initial Revision
* WI 148233 CMC 06/17/2014
*           - Add Property For RawDataSequence And Increment RawDataPart
* WI 148599 CMC 06/18/2014
*           - Changes to ClientSupportLib for RawDataRecord and RawData classes
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public class RawDataRecord
    {
        public Int32 BatchSequence
        {
            get;
            set;
        }

        public XElement ToRawDataRecordElement()
        {
            XElement element = new XElement("RawDataRecord");
            element.SetAttributeValue("BatchSequence", BatchSequence.ToString());
            
            return element;
        }
    }
}
