﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR 33230 WJS 2/10/2012
*           - Initial Revision
* CR 52633 WJS 5/22/2012
 *          - Change Namespace
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public  class DataEntryColumn
    {

        public DataEntryColumn()
        {
            MarkSense = string.Empty;
        }
        public  Int32 ScreenOrder
        {
            get;
            set;
        }
        public  Int32 DataEntryColumnID
        {
            get;
            set;
        }
        public  Int32 DataType
        {
            get;
            set;
        }
        public  Int32 FieldLength
        {
            get;
            set;
        }

        public  string FieldName
        {
            get;
            set;
        }
        public  string DisplayName
        {
            get;
            set;
        }

        public  string DisplayGroup
        {
            get;
            set;
        }
        public string MarkSense
        {
            get;
            set;
        }
        public  XmlElement BuildXMLNode(XmlDocument doc)
        {
            XmlElement el = doc.CreateElement("DataEntryColumn");
            el.SetAttribute("DataEntryColumnID", DataEntryColumnID.ToString());
            el.SetAttribute("FieldLength", FieldLength.ToString());
            el.SetAttribute("DataType", DataType.ToString());
            el.SetAttribute("ScreenOrder", ScreenOrder.ToString());
            el.SetAttribute("FieldName", FieldName);
            el.SetAttribute("DisplayName", DisplayName);
            el.SetAttribute("DisplayGroup", DisplayGroup);
            if (!String.IsNullOrEmpty(MarkSense))
            {
                el.SetAttribute("MarkSense", MarkSense);
            }
            return el;
        }

    }
}
