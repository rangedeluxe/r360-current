﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR 52633 WJS 5/22/2012
*           - Initial Revision
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public class ImageRPSAliasMappings
    {
        public string ExtractType
        {
            get;
            set;
        }
        public string DocType
        {
            get;
            set;
        }
        public string AliasName
        {
            get;
            set;
        }
        public string FieldType
        {
            get;
            set;
        }
        public string DataType
        {
            get;
            set;
        }

        public string FieldLength
        {
            get;
            set;
        }

        public string BatchSource { get; set; }

        public XmlElement BuildXMLNode(XmlDocument doc)
        {
            XmlElement el = (XmlElement)doc.CreateElement("ImageRPSAliasMappings");
            el.SetAttribute("ExtractType", ExtractType.Trim());
            el.SetAttribute("DocType", DocType.Trim());
            el.SetAttribute("AliasName", AliasName.Trim());
            el.SetAttribute("FieldType", FieldType.Trim());
            el.SetAttribute("DataType", DataType.Trim());
            el.SetAttribute("FieldLength", FieldLength.Trim());
            el.SetAttribute("BatchSource", BatchSource.Trim());
            return el;
        }
    }
}
