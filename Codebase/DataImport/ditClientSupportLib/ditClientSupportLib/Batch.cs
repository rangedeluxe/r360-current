﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR 33230 WJS 2/10/2012
*           - Initial Revision
* CR 52633 WJS 5/22/2012
 *          - Change Namespace
* WI 144070, 144071 CMC 5/27/2014
*          - Handle payment source and payment type short name
* WI 152403 CMC 07/08/2014
*          - Change BatchID to Int64 for batch collision support
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public  class Batch
    {

        public  DateTime ProcessingDate
        {
            get;
            set;
        }
        public  DateTime DepositDate
        {
            get;
            set;
        }
        public  DateTime BatchDate
        {
            get;
            set;
        }
        public  Int32 BankID
        {
            get;
            set;
        }
        public  Int32 ClientID
        {
            get;
            set;
        }

        public long BatchID
        {
            get;
            set;
        }
        public  Int32 BatchSiteCode
        {
            get;
            set;
        }

        public string BatchSource
        {
            get;
            set;
        }
        public string PaymentType
        {
            get;
            set;
        }

        public  Guid BatchTrackID
        {
            get;
            set;
        }
        public int BatchCueID
        {
            get;
            set;
        }
        public int BatchNumber
        {
            get;
            set;
        }

        public  XmlElement BuildXMLNode(XmlDocument doc)
        {
            XmlElement el = doc.CreateElement("Batch");
            el.SetAttribute("ProcessingDate", ProcessingDate.ToString("yyyy-MM-dd"));
            el.SetAttribute("DepositDate", DepositDate.ToString("yyyy-MM-dd"));
            el.SetAttribute("BatchDate", BatchDate.ToString("yyyy-MM-dd"));
            el.SetAttribute("BankID", BankID.ToString());
            el.SetAttribute("ClientID", ClientID.ToString());
            el.SetAttribute("BatchID", BatchID.ToString());
            el.SetAttribute("BatchCueID", BatchCueID.ToString());
            el.SetAttribute("BatchNumber", BatchNumber.ToString());
            el.SetAttribute("BatchSiteCode", BatchSiteCode.ToString());
            el.SetAttribute("BatchSource", string.IsNullOrEmpty(BatchSource) ? string.Empty : BatchSource.ToString());
            el.SetAttribute("PaymentType", string.IsNullOrEmpty(PaymentType) ? string.Empty : PaymentType.ToString());
            el.SetAttribute("BatchTrackingID", BatchTrackID.ToString());
           
            return el;
        }

    }
}
