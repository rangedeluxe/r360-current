﻿using System;
using System.Collections.Generic;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Chris Colombo
* Date:
*
* Purpose:
*
* Modification History
* WI 143076 CMC 05/22/2014
*           - Initial Revision
******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientAPI
{
    /// <summary>
    /// Used to "chunk" or "buffer data
    /// </summary>
    public static class Chunker
    {
        /// <summary>
        /// Chunks a string of data into a list of string elements based on the chunk size.
        /// </summary>
        /// <param name="unchunkedData">An unchunked piece of data</param>
        /// <param name="chunkSize">Represents the chunk or buffer size to break the data up into</param>
        /// <returns>A list of string elements broken into the specified chunk size</returns>
        public static IEnumerable<string> ChunkData(string unchunkedData, int chunkSize)
        {
            if (unchunkedData == null)
                throw new ArgumentNullException(nameof(unchunkedData));

            if (chunkSize == 0)
                chunkSize = unchunkedData.Length;

            var chunkedData = new List<string>();
            int position = 0;

            do
            {
                chunkedData.Add(new string(unchunkedData.Skip(position).Take(chunkSize).ToArray()));
                position += chunkSize;
            } while (position < unchunkedData.Length);

            return chunkedData;
        }
    }
}
