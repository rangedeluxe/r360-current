﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Chris Colombo
* Date: 
*
* Purpose: 
*
* Modification History
* WI 148599 CMC 06/18/2014
*           - Initial Revision
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientAPI
{
    internal class RawDataPart
    {

        public Int32 DataSequence
        {
            get;
            set;
        }

        public Int32 DataPart
        {
            get;
            set;
        }

        public string DataValue
        {
            get;
            set;
        }

    }
}
