﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR 33230 WJS 2/10/2012
*           - Initial Revision
* CR 52633 WJS 5/22/2012
 *          - Change Namespace   -
* WI 128741 CMC 02/11/2014
*           - Requiring BankName all the time
*           - Empty string check           
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientAPI
{
    public  class Bank
    {

        public  Int32 BankID
        {
            get;
            set;
        }

        public  string ABA
        {
            get;
            set;
        }
        public  string BankName
        {
            get;
            set;
        }

        public  XmlElement BuildXMLNode(XmlDocument doc)
        {
            XmlElement el = doc.CreateElement("Bank");
            el.SetAttribute("BankID", BankID.ToString());
            if (!String.IsNullOrEmpty(ABA))
            {
                el.SetAttribute("ABA", ABA.Trim());
            }
           
            el.SetAttribute("BankName", !string.IsNullOrEmpty(BankName) ? BankName.Trim() : string.Empty);

            return el;
        }

    }
}
