﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     01/12/2012
*
* Purpose:  
*
* Modification History
* CR 33230 JMC 01/12/2012
*    -New File
* CR 49967 WJS 2/3/2012
 *   - Add DataImportInsertClientSetup, DataImportInsertBatch, DataImportResponseBatch,DataImportResponseClient
 *   - Add xsdversion support
 *   - Add DataImportRequestClientSetup 
 *   - Add DataImportDeleteBatch
* CR 49967 WJS 4/3/2012
 *   - Change names of usp_DataImportQueue to usp_DataImportIntegrationServices
* CR 52261 WJS 5/11/2012
 *   - Add functions DataImportGetItemDataSetupFields,DataImportGetBatchDataSetupFields, DataImportGetDocumentTypes, DataImportGetImageRPSAliasMapping
* CR 52501 WJS 5/22/2012
 *    - Add methods GetBatchDataXSL and GetClientSetupXSL and DataImportGetImageRPSAliasMapping
 * CR 50212 WJS 6/13/2012
 *     - Added details to DataImportImageTransferComplete
* CR 52261 WJS 6/22/2012
*     - Change to allowed DataImportGetImageRPSAliasMapping to take sitebankID/siteLockboxID
* CR 50212 WJS 7/20/2012
 *    - Update DataImageTransferComplete
 *    - Update try/catch logic
 * CR 50212 WJS 8/15/2012
 *    - Change DataImageTransferComplete to pass processingDate and not sourceProcessingDate
* CR 50212 WJS 8/28/2012
 *     - Pass in client code to DataImportResponseBatch and DataImportResponseClient
* CR 55488 WJS 10/18/2012 
 *     - Add call to usp_DataImportIntegrationServices_GetImsInterfaceQueueResponse and usp_UpdateImsInterfaceQueue_ImageImport
 *WI 70412 WJS 12/12/2012
 *      - Add call to usp_DataImportIntegrationServices_UpdateIMSInterfaceQueue
* WI 87184 CRG 02/05/2013 
*     - Remove all SQL string files from the ipoDAL
* WI 87323 CRG 02/05/2013 
*     - Remove SQLDataImport.cs
* WI 90244 CRG 03/19/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
* WI 126987 CMC 01/20/2014
*  - Updated to use 2.00 libraries.
* WI 128739 CMC 02/07/2014
*  - Update data access class to call new stored procedure RecHubSystem.usp_IMSInterfaceQueue_Ins.
* WI 128739 CMC 02/10/2014
*  - Update data access class to call new stored procedure RecHubSystem.usp_dimDocumentTypes_Get_ByCreationDate.
* WI 128739 CMC 02/17/2014
*  - Update data access class to call new stored procedure RecHubData.usp_dimImageRPSAliasMappings_GetRecords.
* WI 152405 CMC 07/21/2014
*   Support for Int64 BatchID  
******************************************************************************/
namespace WFS.RecHub.DAL
{

    /// <summary>
    /// Used to access Data Import records from the database
    /// </summary>
    public class cDataImportDAL : _DALBase, IcDataImportDAL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="cDataImportDAL" /> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        public cDataImportDAL(string vSiteKey)
            : base(vSiteKey)
        {
        }


        /// <summary>
        /// Data import request document types.
        /// </summary>
        /// <param name="loadDate">The load date.</param>
        /// <param name="dt">The data table</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool DataImportRequestDocumentTypes(DateTime? loadDate, out DataTable dt)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms = new List<SqlParameter>();
            dt = null;
            const string PROCNAME = "RecHubSystem.usp_dimDocumentTypes_Get_ByCreationDate";
            try
            {
                arParms.Add(BuildParameter("@parmCreationDate", SqlDbType.DateTime, loadDate, ParameterDirection.Input));

                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bRtnval = false;
            }
            return bRtnval;
        }

        /// <summary>
        /// Data import get image RPS alias mappings.
        /// </summary>
        /// <param name="siteBankID">The site bank ID.</param>
        /// <param name="siteLockboxId">The site lockbox id.</param>
        /// <param name="modficiationDate">The modification date.</param>
        /// <param name="dt">The data table</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool DataImportGetImageRPSAliasMappings(Int32 siteBankID, Int32 siteLockboxId, DateTime? modficiationDate, out DataTable dt)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms = new List<SqlParameter>();
            const string PROCNAME = "RecHubSystem.usp_dimImageRPSAliasMappings_GetRecords";
            dt = null;
            try
            {
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountID", SqlDbType.Int, siteLockboxId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmModificationDate", SqlDbType.DateTime, modficiationDate, ParameterDirection.Input));

                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bRtnval = false;
            }
            return bRtnval;
        }
    }
}
