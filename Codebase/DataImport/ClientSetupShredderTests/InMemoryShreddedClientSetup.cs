﻿using System.Collections.Generic;

namespace WFS.LTA.DataImport
{
    public class InMemoryShreddedClientSetup : IShreddedClientSetupWriter
    {
        public IList<Bank> Banks { get; } = new List<Bank>();
        public IList<DataImportWorkClientSetupResponse> DataImportWorkClientSetupResponses { get; } =
            new List<DataImportWorkClientSetupResponse>();

        public void WriteBank(Bank bank)
        {
            Banks.Add(bank);
        }
        public void WriteDataImportWorkClientSetupResponse(DataImportWorkClientSetupResponse response)
        {
            DataImportWorkClientSetupResponses.Add(response);
        }
    }
}