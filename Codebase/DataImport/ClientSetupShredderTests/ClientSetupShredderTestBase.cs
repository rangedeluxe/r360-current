﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WFS.LTA.DataImport
{
    public class ClientSetupShredderTestBase
    {
        protected const long DataImportQueueId = 0x123456789ABC;

        protected static XElement CreateBankElement(string bankId = "1", string bankName = "Bank", string aba = null)
        {
            var bank = new XElement("Bank",
                new XAttribute("BankID", bankId));

            if (bankName != null)
                bank.Add(new XAttribute("BankName", bankName));
            if (aba != null)
                bank.Add(new XAttribute("ABA", aba));

            return bank;
        }
        protected static InMemoryShreddedClientSetup ShredAll(XElement xml)
        {
            var shreddedClientSetup = new InMemoryShreddedClientSetup();
            var shredder = new ClientSetupShredder(xml.CreateReader(), DataImportQueueId, shreddedClientSetup);
            shredder.ProcessAll();
            return shreddedClientSetup;
        }
        protected static T ShredSingle<T>(XElement xml, Func<InMemoryShreddedClientSetup, IList<T>> getCollection)
        {
            var all = getCollection(ShredAll(xml));
            if (all.Count != 1)
                Assert.Fail($"Expected exactly 1 {typeof(T).Name} but found {all.Count}");
            return all[0];
        }
    }
}