﻿using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
// ReSharper disable RedundantArgumentDefaultValue

namespace WFS.LTA.DataImport
{
    [TestClass]
    public class BankTests : ClientSetupShredderTestBase
    {
        private static Bank ShredSingleBank(XElement xml)
        {
            return ShredSingle(xml, shreddedBatch => shreddedBatch.Banks);
        }

        [TestMethod]
        public void GeneratedClientGroupId()
        {
            var bank = ShredSingleBank(CreateBankElement());
            Assert.AreEqual(DataImportQueueId, bank.GeneratedClientGroupId);
        }
        [TestMethod]
        public void BankId()
        {
            var bank = ShredSingleBank(CreateBankElement(bankId: "123"));
            Assert.AreEqual(123, bank.BankId);
        }
        [TestMethod]
        public void BankName()
        {
            var bank = ShredSingleBank(CreateBankElement(bankName: "Bank1"));
            Assert.AreEqual("Bank1", bank.BankName);
        }
        [TestMethod]
        public void BankName_IsOptional()
        {
            var bank = ShredSingleBank(CreateBankElement(bankName: null));
            Assert.IsNull(bank.BankName);
        }
        [TestMethod]
        public void BankName_AllowsSpecialCharacters()
        {
            var bank = ShredSingleBank(CreateBankElement(bankName: "<&>'\""));
            Assert.AreEqual("<&>'\"", bank.BankName);
        }
        [TestMethod]
        public void BankName_MaximumLengthIs25()
        {
            var bank = ShredSingleBank(CreateBankElement(bankName: new string('x', 26)));
            Assert.AreEqual(new string('x', 25), bank.BankName);
        }
        [TestMethod]
        public void Aba()
        {
            var bank = ShredSingleBank(CreateBankElement(aba: "12345"));
            Assert.AreEqual("12345", bank.Aba);
        }
        [TestMethod]
        public void Aba_IsOptional()
        {
            var bank = ShredSingleBank(CreateBankElement(aba: null));
            Assert.IsNull(bank.Aba);
        }
        [TestMethod]
        public void Aba_AllowsSpecialCharacters()
        {
            var bank = ShredSingleBank(CreateBankElement(aba: "<&>'\""));
            Assert.AreEqual("<&>'\"", bank.Aba);
        }
        [TestMethod]
        public void Aba_MaximumLengthIs10()
        {
            var bank = ShredSingleBank(CreateBankElement(aba: new string('x', 11)));
            Assert.AreEqual(new string('x', 10), bank.Aba);
        }
    }
}