﻿using System;
using System.Collections;
using System.Xml;
using WFS.LTA.DataImport.DataImportClientAPI;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
*
* Purpose: This class will hold the Continuation data for a Account.
*
* Modification History
* WI 136775 SAS 04/18/2014
* Changes done to create canonical XML.
*
* WI 136895 SAS 05/07/2014
* Changes done to parse addenda data.
*
* WI 139926 SAS 05/07/2014
* Changes done to remove ItemDataRecords element and add GhostDocument Elements
*
* WI 139929 SAS 05/07/2014
* Changes done for duplicate file and payment detection.
*
* WI 158097 SAS 08/12/2014
* Changes done to add Remitter Name property to DITBAI2Continuations collection class.
*
* WI 158856 SAS 08/15/2014
* Changes done to exclude BPRMonetaryAmount attribute if the amount is not valid
*
* WI 169845 SAS 10/16/2014
* Changes done to replace GhostDocument with Document Element
*
* WI 173023 SAS 10/21/2014
* Changes done to set BatchSequence number
*
* WI 178469 SAS 11/18/2014
* Changes done to link RMR, BPR and TRN records
******************************************************************************/

namespace DMP.BAI2.Components
{
    /// <summary>
    /// Wire Transfer Continuation Item class
    /// </summary>
    public sealed class DITBAI2Continuation
    {
        /// <summary>
        /// The input line, exactly as read from the import file
        /// </summary>
        public string RawContinuationData { get; set; } = string.Empty;

        /// <summary>
        /// The addenda raw data, as parsed from <see cref="RawContinuationData"/>.
        /// This is exracted from the input line, but not yet split into logical parsed fields.
        /// </summary>
        public string AddendaRawData
            => DITBAI2Common.GetParsedData(RawContinuationData, oneBasedIndex: 2, includeRestOfLine: true);
    }

    /// <summary>
    /// Continuation Collection class
    /// </summary>
    public sealed class DITBAI2Continuations : CollectionBase
    {
        #region Class Fields
        /// <summary>
        /// Added tags
        /// </summary>
        private ArrayList m_AddedRemittanceTags;
        /// <summary>
        /// m_RemittanceBatchSequenceNo
        /// </summary>
        private int m_RemittanceBatchSequenceNo = 0;
        /// <summary>
        /// Added tags
        /// </summary>
        private ArrayList m_RemittanceTags;
        /// <summary>
        /// Remittance Type
        /// </summary>
        private DITBAI2Common.RemittanceType m_RemittanceType;
        /// <summary>
        /// Checks if data is available 
        /// </summary>
        private bool m_DataExists;
        /// <summary>
        /// Remitter Name 
        /// </summary>
        private string m_RemitterName = string.Empty;

        #endregion Class Fields

        #region Class Properties
        /// <summary>
        /// Remittance BatchSequenceNo
        /// </summary>
        public int RemittanceBatchSequenceNo
        {
            get { return m_RemittanceBatchSequenceNo; }
            set { m_RemittanceBatchSequenceNo = value; }
        }
        #endregion Class Properties
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITBAI2Continuations this[int index]
        {
            get
            {
                return ((DITBAI2Continuations)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }
        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITBAI2Continuation value)
        {
            return (List.Add(value));
        }
        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITBAI2Continuation value)
        {
            return (List.IndexOf(value));
        }
        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITBAI2Continuation value)
        {
            List.Insert(index, value);
        }
        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITBAI2Continuation value)
        {
            List.Remove(value);
        }
        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITBAI2Continuation value)
        {
            // If value is not of type ACHBatch, this will return false.
            return (List.Contains(value));
        }
        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.BAI2.Components.DITBAI2Continuation"))
                throw new ArgumentException("value must be of type DITBAI2Continuation.", "value");
        }

        /// <summary>
        /// Remitter Name
        /// </summary>
        public string RemitterName
        {
            get { return m_RemitterName; }
        }

        /// <summary>
        /// Build Remittance Node
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="xDocumentElement"></param>
        public void BuildRemittanceNode(XmlDocument xDoc, ref XmlElement xDocumentElement)
        {
            StringBuilder sbAddendaData = new StringBuilder();
            m_AddedRemittanceTags = new ArrayList();
            //Concatenate all the continuation records within the transaction.
            foreach (DITBAI2Continuation objContinuation in this)
            {
                sbAddendaData.Append(objContinuation.AddendaRawData);
            }

            if (!string.IsNullOrWhiteSpace(sbAddendaData.ToString()))
            {
                switch (CheckRemittanceMethod(sbAddendaData.ToString()))
                {
                    case DITBAI2Common.RemittanceMethod.FEDWIRE_TAGS:
                        m_RemittanceType = this.SetRemittanceType(sbAddendaData.ToString(), DITBAI2Common.RemittanceMethod.FEDWIRE_TAGS);
                        ParseFedWireOrChipsAddendaData(sbAddendaData.ToString(), DITBAI2Common.RemittanceMethod.FEDWIRE_TAGS, xDoc, ref xDocumentElement);
                        break;
                    case DITBAI2Common.RemittanceMethod.CHIPS_TAGS:
                        m_RemittanceType = this.SetRemittanceType(sbAddendaData.ToString(), DITBAI2Common.RemittanceMethod.CHIPS_TAGS);
                        ParseFedWireOrChipsAddendaData(sbAddendaData.ToString(), DITBAI2Common.RemittanceMethod.CHIPS_TAGS, xDoc, ref xDocumentElement);
                        break;
                    case DITBAI2Common.RemittanceMethod.ISO_XML_TAGS:
                        break; //TO DO:
                }
            }
        }

        /// <summary>
        /// Check Remittance Method
        /// </summary>
        /// <param name="sAddendaData"></param>
        /// <returns>Returns the Remittance method</returns>
        private DITBAI2Common.RemittanceMethod CheckRemittanceMethod(string sAddendaData)
        {
            if (sAddendaData.Contains("{") && sAddendaData.Contains("}"))
                return DITBAI2Common.RemittanceMethod.FEDWIRE_TAGS;
            if (sAddendaData.Contains("[") && sAddendaData.Contains("]"))
                return DITBAI2Common.RemittanceMethod.CHIPS_TAGS;
            if (sAddendaData.Contains("<") && sAddendaData.Contains(">"))
                return DITBAI2Common.RemittanceMethod.ISO_XML_TAGS;
            else
                return DITBAI2Common.RemittanceMethod.INVALID_TAG;
        }

        /// <summary>
        /// Set Remittance Type
        /// </summary>
        /// <param name="RemittanceMethod"></param>
        private DITBAI2Common.RemittanceType SetRemittanceType(string sAddendaData, DITBAI2Common.RemittanceMethod RemittanceMethod)
        {

            m_RemittanceTags = new ArrayList();
            switch (RemittanceMethod)
            {
                case DITBAI2Common.RemittanceMethod.FEDWIRE_TAGS:
                    //Structured Fed Wire Tags
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_REMM_ORIG_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_REMM_BENE_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_PRI_REMM_DOC_INFO_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_ACT_AMT_PAID_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_GROSS_AMT_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_DISC_AMT_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_ADJ_INFO_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_REMM_DATE_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_SEC_REMM_DOC_INFO_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.FEDW_REMM_FREE_TXT_TAG);
                    foreach (string sRemmitanceTags in m_RemittanceTags)
                    {
                        if (sAddendaData.Contains(sRemmitanceTags))
                            return DITBAI2Common.RemittanceType.STRUCTURED;
                    }

                    //No Structured tags found hence clear the array list
                    m_RemittanceTags.Clear();

                    //Unstructured Fed Wire tags
                    if (sAddendaData.Contains(DITBAI2Common.FEDW_UNSTRUCTURED_TAG))
                        return DITBAI2Common.RemittanceType.UNSTRUCTURED;

                    //Related Fed Wire tags
                    if (sAddendaData.Contains(DITBAI2Common.FEDW_RELATED_TAG))
                        return DITBAI2Common.RemittanceType.RELATED;
                    break;

                case DITBAI2Common.RemittanceMethod.CHIPS_TAGS:
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_REMM_ORIG_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_REMM_BENE_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_PRI_REMM_DOC_INFO_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_ACT_AMT_PAID_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_GROSS_AMT_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_DISC_AMT_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_ADJ_INFO_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_REMM_DATE_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_SEC_REMM_DOC_INFO_TAG);
                    m_RemittanceTags.Add(DITBAI2Common.CHIPS_REMM_FREE_TXT_TAG);
                    foreach (string sRemmitanceTags in m_RemittanceTags)
                    {
                        if (sAddendaData.Contains(sRemmitanceTags))
                            return DITBAI2Common.RemittanceType.STRUCTURED;
                    }
                    //No Structured tags found hence clear the array list
                    m_RemittanceTags.Clear();

                    //Unstructured Fed Wire tags
                    if (sAddendaData.Contains(DITBAI2Common.CHIPS_UNSTRUCTURED_TAG))
                        return DITBAI2Common.RemittanceType.UNSTRUCTURED;

                    //Related Fed Wire tags
                    if (sAddendaData.Contains(DITBAI2Common.CHIPS_RELATED_TAG))
                        return DITBAI2Common.RemittanceType.RELATED;
                    break;
                case DITBAI2Common.RemittanceMethod.ISO_XML_TAGS:
                    break;

            }
            return DITBAI2Common.RemittanceType.UNKNOWN;
        }
        /// <summary>
        /// Parse Fed Wire or chip tag Addenda Data
        /// </summary>
        /// <param name="sAddendaData"></param>
        /// <param name="RemittanceMethod"></param>
        /// <param name="xDoc"></param>
        /// <param name="xDocumentElement"></param>
        private void ParseFedWireOrChipsAddendaData(string sAddendaData, DITBAI2Common.RemittanceMethod RemittanceMethod, XmlDocument xDoc, ref XmlElement xDocumentElement)
        {
            switch (m_RemittanceType)
            {
                case DITBAI2Common.RemittanceType.STRUCTURED:
                    ParseStructuredAddendaData(sAddendaData, RemittanceMethod, xDoc, ref xDocumentElement);
                    break;
                case DITBAI2Common.RemittanceType.UNSTRUCTURED:
                    ParseUnStructuredAddendaData(sAddendaData, RemittanceMethod, xDoc, ref xDocumentElement);
                    break;
                case DITBAI2Common.RemittanceType.RELATED:
                    ParseRelatedAddendaData(sAddendaData, RemittanceMethod, xDoc, ref xDocumentElement);
                    break;
            }
        }

        #region PARSE STRUCTURED FED WIRE/CHIP TAG METHODS

        /// <summary>
        /// Parse Structured addenda data for Fed wire or Chip tags
        /// </summary>
        /// <param name="sAddendaData"></param>
        /// <param name="RemittanceMethod"></param>
        /// <param name="xDoc"></param>
        /// <param name="xDocumentElement"></param>
        private void ParseStructuredAddendaData(string sAddendaData, DITBAI2Common.RemittanceMethod RemittanceMethod, XmlDocument xDoc, ref XmlElement xDocumentElement)
        {
            XmlElement xRemittanceDataRecordElement;
            xRemittanceDataRecordElement = BuildRemittanceDataRecordNode(xDoc);
            string sTag = string.Empty;
            m_DataExists = false;
            while (!string.IsNullOrEmpty(sTag = GetRemittanceTag(sAddendaData, RemittanceMethod)))
            {

                //Check if tag was already added
                if (m_AddedRemittanceTags.Contains(sTag))
                {
                    xDocumentElement.AppendChild(xRemittanceDataRecordElement);
                    //If the tag is repeated again then the new RemittanceDataRecord  element needs to be created.                     
                    //So add the nodes to xRemittanceDataRecordElement and create a new xRemittanceDataRecordElement node
                    xRemittanceDataRecordElement = BuildRemittanceDataRecordNode(xDoc);
                    //clear the tag array 
                    m_AddedRemittanceTags.Clear();
                    m_DataExists = false;
                }
                if (RemittanceMethod == DITBAI2Common.RemittanceMethod.FEDWIRE_TAGS)
                {
                    switch (sTag)
                    {
                        case DITBAI2Common.FEDW_REMM_ORIG_TAG:
                            //Set Remittance Data Record
                            //Remittance Originator
                            ParseStructuredData(DITBAI2Common.FEDW_REMM_ORIG_TAG, 6, DITBAI2Common.REM_ORIGINATOR, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.FEDW_REMM_BENE_TAG:
                            //RemittanceBeneficiary
                            ParseStructuredData(DITBAI2Common.FEDW_REMM_BENE_TAG, 0, DITBAI2Common.REM_BENEFICIARY, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);

                            break;
                        case DITBAI2Common.FEDW_PRI_REMM_DOC_INFO_TAG:
                            //PrimaryRemittanceDocInfo
                            ParseStructuredData(DITBAI2Common.FEDW_PRI_REMM_DOC_INFO_TAG, 4, DITBAI2Common.PRI_REM_DOC_INFO, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);

                            break;
                        case DITBAI2Common.FEDW_ACT_AMT_PAID_TAG:
                            //ActualAmountPaid
                            ParseStructuredData(DITBAI2Common.FEDW_ACT_AMT_PAID_TAG, 3, DITBAI2Common.ACTUAL_AMT_PAID, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);

                            break;
                        case DITBAI2Common.FEDW_GROSS_AMT_TAG:
                            //GrossAmount
                            ParseStructuredData(DITBAI2Common.FEDW_GROSS_AMT_TAG, 3, DITBAI2Common.GROSS_AMT, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);

                            break;
                        case DITBAI2Common.FEDW_DISC_AMT_TAG:
                            //DiscountAmount
                            ParseStructuredData(DITBAI2Common.FEDW_DISC_AMT_TAG, 3, DITBAI2Common.DISCOUNT_AMT, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);

                            break;
                        case DITBAI2Common.FEDW_ADJ_INFO_TAG:
                            //AdjustmentInformation
                            ParseStructuredData(DITBAI2Common.FEDW_ADJ_INFO_TAG, 0, DITBAI2Common.ADJ_INFO, DITBAI2Common.FW_DATA_DELIMITER_BRACE, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.FEDW_REMM_DATE_TAG:
                            //RemittanceDate
                            ParseStructuredData(DITBAI2Common.FEDW_REMM_DATE_TAG, 0, DITBAI2Common.REM_DATE, DITBAI2Common.FW_DATA_DELIMITER_BRACE, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.FEDW_SEC_REMM_DOC_INFO_TAG:
                            //SecondaryRemittanceDocInfo
                            ParseStructuredData(DITBAI2Common.FEDW_SEC_REMM_DOC_INFO_TAG, 5, DITBAI2Common.SEC_REM_DOC_INFO, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.FEDW_REMM_FREE_TXT_TAG:
                            //RemittanceFreeText
                            ParseStructuredData(DITBAI2Common.FEDW_REMM_FREE_TXT_TAG, 0, DITBAI2Common.REM_FREE_TXT, string.Empty, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        default:
                            sAddendaData = sAddendaData.Remove(sAddendaData.IndexOf(sTag), (sTag.Length));
                            break;
                    }

                }
                else if (RemittanceMethod == DITBAI2Common.RemittanceMethod.CHIPS_TAGS)
                {
                    switch (sTag)
                    {
                        case DITBAI2Common.CHIPS_REMM_ORIG_TAG:
                            //Set Remittance Data Record
                            //Remittance Originator
                            ParseStructuredData(DITBAI2Common.CHIPS_REMM_ORIG_TAG, 6, DITBAI2Common.REM_ORIGINATOR, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.CHIPS_REMM_BENE_TAG:
                            //RemittanceBeneficiary
                            ParseStructuredData(DITBAI2Common.CHIPS_REMM_BENE_TAG, 0, DITBAI2Common.REM_BENEFICIARY, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.CHIPS_PRI_REMM_DOC_INFO_TAG:
                            //PrimaryRemittanceDocInfo
                            ParseStructuredData(DITBAI2Common.CHIPS_PRI_REMM_DOC_INFO_TAG, 4, DITBAI2Common.PRI_REM_DOC_INFO, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.CHIPS_ACT_AMT_PAID_TAG:
                            //ActualAmountPaid
                            ParseStructuredData(DITBAI2Common.CHIPS_ACT_AMT_PAID_TAG, 3, DITBAI2Common.ACTUAL_AMT_PAID, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.CHIPS_GROSS_AMT_TAG:
                            //GrossAmount
                            ParseStructuredData(DITBAI2Common.CHIPS_GROSS_AMT_TAG, 3, DITBAI2Common.GROSS_AMT, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.CHIPS_DISC_AMT_TAG:
                            //DiscountAmount
                            ParseStructuredData(DITBAI2Common.CHIPS_DISC_AMT_TAG, 3, DITBAI2Common.DISCOUNT_AMT, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.CHIPS_ADJ_INFO_TAG:
                            //AdjustmentInformation
                            ParseStructuredData(DITBAI2Common.CHIPS_ADJ_INFO_TAG, 0, DITBAI2Common.ADJ_INFO, DITBAI2Common.CHIP_DATA_DELIMITER_BRACKET, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.CHIPS_REMM_DATE_TAG:
                            //RemittanceDate
                            ParseStructuredData(DITBAI2Common.CHIPS_REMM_DATE_TAG, 0, DITBAI2Common.REM_DATE, DITBAI2Common.CHIP_DATA_DELIMITER_BRACKET, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.CHIPS_SEC_REMM_DOC_INFO_TAG:
                            //SecondaryRemittanceDocInfo
                            ParseStructuredData(DITBAI2Common.CHIPS_SEC_REMM_DOC_INFO_TAG, 5, DITBAI2Common.SEC_REM_DOC_INFO, DITBAI2Common.FW_DATA_DELIMITER_ASTREK, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        case DITBAI2Common.CHIPS_REMM_FREE_TXT_TAG:
                            //RemittanceFreeText
                            ParseStructuredData(DITBAI2Common.CHIPS_REMM_FREE_TXT_TAG, 5, DITBAI2Common.REM_FREE_TXT, string.Empty, xDoc, ref sAddendaData, ref  xRemittanceDataRecordElement);
                            break;
                        default:
                            sAddendaData = sAddendaData.Remove(sAddendaData.IndexOf(sTag), (sTag.Length));
                            break;
                    }
                }

            }

            //Add the last element
            if (m_DataExists)
                xDocumentElement.AppendChild(xRemittanceDataRecordElement);
        }

        /// <summary>
        /// Checks if the addenda string still has a valid pending FedWire/Chip tags for parsing and returns the corresponding Remittance tags.
        /// </summary>
        /// <param name="svalue"></param>
        /// <returns></returns>
        private string GetRemittanceTag(string svalue, DITBAI2Common.RemittanceMethod RemittanceMethod)
        {
            int iIndex;
            foreach (string sTag in m_RemittanceTags)
            {
                if (svalue.Contains(sTag))
                {
                    if (RemittanceMethod == DITBAI2Common.RemittanceMethod.FEDWIRE_TAGS)
                    {
                        iIndex = svalue.IndexOf(DITBAI2Common.FW_DATA_DELIMITER_BRACE);
                        return svalue.Substring(iIndex, 6);
                    }
                    else
                    {
                        iIndex = svalue.IndexOf(DITBAI2Common.CHIP_DATA_DELIMITER_BRACKET);
                        return svalue.Substring(iIndex, 5);
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// ParseData for Fed wire tags
        /// </summary>
        /// <param name="sTagName"></param>
        /// <param name="iLeadingPos"></param>
        /// <param name="sNodeName"></param>
        /// <param name="sDataDilimiter"></param>
        /// <param name="xDoc"></param>
        /// <param name="sAddendaData"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        private void ParseStructuredData(string sTagName, int iLeadingPos, string sNodeName, string sDataDilimiter, XmlDocument xDoc, ref string sAddendaData, ref XmlElement xRemittanceDataRecordElement)
        {
            int iIndex = 0;
            string sNodeValue = string.Empty;
            if ((iIndex = sAddendaData.IndexOf(sTagName)) > -1)
            {   //Add tag in the list to make sure same tags are not added in the xRemittanceDataRecordElement node.
                m_AddedRemittanceTags.Add(sTagName);
                sNodeValue = DITBAI2Common.GetParsedAddendaData(ref sAddendaData, iIndex, sTagName, sDataDilimiter, iLeadingPos);
                //Validate data
                switch (sNodeName)
                {
                    case DITBAI2Common.ACTUAL_AMT_PAID:
                    case DITBAI2Common.GROSS_AMT:
                    case DITBAI2Common.DISCOUNT_AMT:
                        if (!DITBAI2Common.ValidateMoney(sNodeValue))
                            sNodeValue = string.Empty;
                        break;
                    case DITBAI2Common.REM_DATE:
                        sNodeValue = DITBAI2Common.ValidateDate(sNodeValue);
                        break;
                    case DITBAI2Common.REM_ORIGINATOR:
                        m_RemitterName = sNodeValue;
                        break;

                }
                this.AddChildNodes(sNodeName, sNodeValue, xDoc, ref xRemittanceDataRecordElement);
            }
        }
        #endregion PARSE STRUCTURED FED WIRE/CHIP TAG METHODS

        #region PARSE UNSTRUCTURED FED WIRE/CHIP TAG METHODS

        /// <summary>
        /// Parse UnStructured addenda data for Fed wire or Chip tags
        /// </summary>
        /// <param name="sAddendaData"></param>
        /// <param name="RemittanceMethod"></param>
        /// <param name="xDoc"></param>
        /// <param name="xDocumentElement"></param>
        private void ParseUnStructuredAddendaData(string sAddendaData, DITBAI2Common.RemittanceMethod RemittanceMethod, XmlDocument xDoc, ref XmlElement xDocumentElement)
        {
            bool bRMRAdded = false;
            bool bBPRStillExists = false;
            bool bTRNStillExists = false;

            int iIndex;
            string[] sParsedAddenda = sAddendaData.Split(DITBAI2Common.UNSTRUC_ADDENDA_SEG_DELIMITER.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string sBPRAmount = string.Empty;
            string sBPRAccount = string.Empty;
            string sTraceNo = string.Empty;
            XmlElement xRemittanceDataRecordElement;
            
            foreach (string sLine in sParsedAddenda)
            {
                string sSegment = sLine.Trim();            
                if (sSegment.Length > 0)
                {
                    //Set Remittance Data Record
                    RemittanceDataRecord objRemittanceDataRecord = new RemittanceDataRecord();
                    objRemittanceDataRecord.BatchSequence = ++RemittanceBatchSequenceNo;
                    xRemittanceDataRecordElement = objRemittanceDataRecord.BuildXMLNode(xDoc);

                    // Determine if the Addenda Record contains BPR                    
                    if ((iIndex = sSegment.ToUpper().IndexOf(DITBAI2Common.UNSTRUC_BPR)) > -1)
                    {
                        // Remove any extra data before the BPR string. This is not needed
                        sSegment = sSegment.Remove(0, iIndex);
                        if (GetBPRRemittanceData(sSegment, out sBPRAmount, out sBPRAccount))
                            bBPRStillExists = true;  //Indicator to detected the BPR record has not yet added.                        
                    }

                    // Determine if the Addenda Record contains RMR
                    else if ((iIndex = sSegment.ToUpper().IndexOf(DITBAI2Common.UNSTRUC_RMR)) > -1)
                    {
                        // Remove any extra data before the RMR string. This is not needed
                        sSegment = sSegment.Remove(0, iIndex);
                        if (CreateRMRRemittanceData(sSegment, sBPRAmount, sBPRAccount, xDoc, ref xRemittanceDataRecordElement))
                        {
                            bRMRAdded = true;  //Indicator states the RMR Records has been added.
                            bBPRStillExists = false; //Indicator to detected the BPR record has been added.
                        }

                    }

                    // Determine if the Addenda Record contains TRN
                    else if ((iIndex = sSegment.ToUpper().IndexOf(DITBAI2Common.UNSTRUC_TRN)) > -1)
                    {
                        // Remove any extra data before the TRN string. This is not needed
                        sSegment = sSegment.Remove(0, iIndex);
                        sTraceNo = GetTRNRemittanceData(sSegment);
                        bTRNStillExists = true;
                    }

                    //Check if the RMR record are added then add a TRN records and add the entire data in Document Element
                    if (bRMRAdded)
                    {
                        if (!string.IsNullOrEmpty(sTraceNo))
                        {
                            CreateTransactionRemittanceData(sTraceNo, xDoc, ref xRemittanceDataRecordElement);
                            bTRNStillExists = false;
                        }
                        bRMRAdded = false;
                        xDocumentElement.AppendChild(xRemittanceDataRecordElement);
                    }
                    else
                        this.RemittanceBatchSequenceNo -= 1;
                }
            }

            //If no BPR or TRN records were added then add them here, since no corresponding RMR records exists in Addenda
            if ((bBPRStillExists) || (bTRNStillExists))
            {
                RemittanceDataRecord objRemittanceDataRecord = new RemittanceDataRecord();
                objRemittanceDataRecord.BatchSequence = ++this.RemittanceBatchSequenceNo;
                xRemittanceDataRecordElement = objRemittanceDataRecord.BuildXMLNode(xDoc);

                if (CreateBPRRemittanceData(string.Empty, sBPRAmount, sBPRAccount, xDoc, ref xRemittanceDataRecordElement) ||
                   (CreateTransactionRemittanceData(sTraceNo, xDoc, ref xRemittanceDataRecordElement)))
                    xDocumentElement.AppendChild(xRemittanceDataRecordElement);
                else
                    this.RemittanceBatchSequenceNo -= 1;
            }
        }

        /// <summary>
        /// Get BPR RemittanceData record
        /// </summary>
        /// <param name="sSegment"></param>
        /// <param name="sBPRAmount"></param>
        /// <param name="sBPRAccountNo"></param>
        /// <returns></returns>
        private bool GetBPRRemittanceData(string sSegment, out string sBPRAmount, out string sBPRAccountNo)
        {
            bool bRetVal = false;
            // Remove any extra data before the BPR string. This is not needed
            string[] sParsedSegment = sSegment.Split(DITBAI2Common.UNSTRUC_ADDENDA_DATA_DELIMITER.ToCharArray(), StringSplitOptions.None);
            sBPRAmount = string.Empty;
            sBPRAccountNo = string.Empty;
            if (sParsedSegment.Length > 0)
                ParseBPR(sParsedSegment, out sBPRAmount, out sBPRAccountNo);
            if (!(string.IsNullOrEmpty(sBPRAmount)) || !(string.IsNullOrEmpty(sBPRAccountNo)))
                bRetVal = true;
            return bRetVal;
        }

        /// <summary>
        /// Create CreateBPRRemittanceData
        /// </summary>
        /// <param name="sRMRAmt"></param>
        /// <param name="sBPRAmount"></param>
        /// <param name="sBPRAccountNo"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        /// <returns></returns>
        private bool CreateBPRRemittanceData(string sRMRAmt, string sBPRAmount, string sBPRAccountNo, XmlDocument xDoc, ref XmlElement xRemittanceDataRecordElement)
        {
            bool bRetVal = false;
            //Remittance Data for BPR
            if (AddChildNodes(DITBAI2Common.AMOUNT, sRMRAmt, xDoc, ref xRemittanceDataRecordElement))
                bRetVal = true;

            if (AddChildNodes(DITBAI2Common.ACCT_NO, sBPRAccountNo, xDoc, ref xRemittanceDataRecordElement))
                bRetVal = true;

            if (AddChildNodes(DITBAI2Common.BPR_MONETARY_AMT, sBPRAmount, xDoc, ref xRemittanceDataRecordElement))
                bRetVal = true;

            if (AddChildNodes(DITBAI2Common.BPR_ACT_NO, sBPRAccountNo, xDoc, ref xRemittanceDataRecordElement))
                bRetVal = true;

            return bRetVal;
        }


        /// <summary>
        /// Create RMR RemittanceData record
        /// </summary>
        /// <param name="sSegment"></param>
        /// <param name="sBPRAmount"></param>
        /// <param name="sBPRAccountNo"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        /// <returns></returns>
        private bool CreateRMRRemittanceData(string sSegment, string sBPRAmount, string sBPRAccountNo, XmlDocument xDoc, ref XmlElement xRemittanceDataRecordElement)
        {
            bool bRetVal = false;
            string[] sParsedSegment = sSegment.Split(DITBAI2Common.UNSTRUC_ADDENDA_DATA_DELIMITER.ToCharArray(), StringSplitOptions.None);
            if (sParsedSegment.Length > 0)
            {
                string sRMRRefNo;
                string sRMRAmt;
                string sRMRTotInvoivceAmt;
                string sRMRDiscountAmt;
                ParseRMR(sParsedSegment, out sRMRRefNo, out sRMRAmt, out sRMRTotInvoivceAmt, out sRMRDiscountAmt);

                CreateBPRRemittanceData(sRMRAmt, sBPRAmount, sBPRAccountNo, xDoc, ref xRemittanceDataRecordElement);
                //Remittance Data for RMR
                 if (AddChildNodes(DITBAI2Common.INVOICE_NO, sRMRRefNo, xDoc, ref xRemittanceDataRecordElement))
                     bRetVal = true;

                 if (AddChildNodes(DITBAI2Common.RMR_REF_NO, sRMRRefNo, xDoc, ref xRemittanceDataRecordElement))
                     bRetVal = true;

                 if (AddChildNodes(DITBAI2Common.RMR_MONETARY_AMT, sRMRAmt, xDoc, ref xRemittanceDataRecordElement))
                     bRetVal = true;

                 if (AddChildNodes(DITBAI2Common.RMR_TOT_INVOICE_AMT, sRMRTotInvoivceAmt, xDoc, ref xRemittanceDataRecordElement))
                     bRetVal = true;

                 if (AddChildNodes(DITBAI2Common.RMR_DISC_AMT, sRMRDiscountAmt, xDoc, ref xRemittanceDataRecordElement))
                     bRetVal = true;
            }
            return bRetVal;
        }

        /// <summary>
        /// Create Transaction Remittance Data
        /// </summary>
        /// <param name="sTraceNo"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        /// <returns></returns>
        private bool CreateTransactionRemittanceData(string sTraceNo, XmlDocument xDoc, ref XmlElement xRemittanceDataRecordElement)
        {   
            bool bRetVal=false;
            //Remittance Data for BPR
            if (AddChildNodes("ReassociationTraceNumber", sTraceNo, xDoc, ref xRemittanceDataRecordElement))
                bRetVal = true;
            return bRetVal;
        }

        /// <summary>
        /// Get TRN RemittanceData record
        /// </summary>
        /// <param name="sSegment"></param>
        /// <returns></returns>
        private string GetTRNRemittanceData(string sSegment)
        {
            string sTraceNo = string.Empty;
            string[] sParsedSegment = sSegment.Split(DITBAI2Common.UNSTRUC_ADDENDA_DATA_DELIMITER.ToCharArray(), StringSplitOptions.None);
            if (sParsedSegment.Length > 0)
                ParseTRN(sParsedSegment, out sTraceNo);
            return sTraceNo;
        }


        /// <summary>
        ///  Parse the BPR Segment of the Addenda Record
        /// </summary>
        /// <param name="strParsedSegment"></param>
        /// <param name="dBPRAmount"></param>
        /// <param name="sBPRAccountNo"></param>
        private void ParseBPR(string[] sParsedSegment, out string sBPRAmt, out string sBPRAccountNo)
        {
            bool bIsDebit = false;
            decimal dAmount = 0.00m;
            sBPRAmt = string.Empty;
            sBPRAccountNo = string.Empty;

            // The BPR Segment will have the format            
            // BPR*C*666.01*C*FWT*CTX*01*067012099*DA*0000000000*11111
            // 11111**01*072000096*DA*100021111111111*20140324\
            //
            // Need to parse out the following fields:
            //    Field 03 = BPR Monetary Amount =666.01
            //    Field 04 = Credit or Debit indicator=C
            //    Field 10 = BPR Account Number=0000000000

            //BPR Monetary Amount 
            if (sParsedSegment.Length > 2)
                sBPRAmt = sParsedSegment[2];

            //Credit or Debit indicator
            if (sParsedSegment.Length > 3)
                if (string.Compare(sParsedSegment[3].ToUpper(), "D", true) == 0)
                    bIsDebit = true;

            //BPR Account Number
            if (sParsedSegment.Length > 9)
                sBPRAccountNo = sParsedSegment[9];

            // Add the cents if it is missing
            if (sBPRAmt.Length > 0 && sBPRAmt.IndexOf('.') < 0)
                sBPRAmt += ".00";

            if ((sBPRAmt.Length > 0) && (DITBAI2Common.ValidateMoney(sBPRAmt)))
            {
                dAmount = decimal.Parse(sBPRAmt, System.Globalization.NumberStyles.Currency);
                if (bIsDebit)
                    dAmount = dAmount * -1;
                sBPRAmt = dAmount.ToString();
            }
            else
            {
                sBPRAmt = string.Empty;
            }
        }

        /// <summary>
        /// Parse the RMR Segment of the Addenda Record
        /// </summary>
        /// <param name="sParsedSegment"></param>
        /// <param name="sRMRRefNo"></param>
        /// <param name="sRMRAmt"></param>
        /// <param name="sRMRTotInvoivceAmt"></param>
        /// <param name="sRMRDiscountAmt"></param>
        private static void ParseRMR(string[] sParsedSegment, out string sRMRRefNo, out  string sRMRAmt, out  string sRMRTotInvoivceAmt, out  string sRMRDiscountAmt)
        {
            sRMRRefNo = string.Empty;
            sRMRAmt = string.Empty;
            sRMRTotInvoivceAmt = string.Empty;
            sRMRDiscountAmt = string.Empty;

            // The RMR Segment will have the format            
            //    RMR*IV*I000001*ER*126.76*126.76\
            // Parse out the fields that are available
            //    Field 03 = RMR Reference Number=I000001
            //    Field 05 = RMR Monetary Amount=126.76
            //    Field 06 = RMR Total Invoice Amount=126.76
            //    Field 07 = RMR Discount Amount
            //No data for RMR Discount Amount , hence do not include in the XML

            //RMR Reference Number
            if (sParsedSegment.Length > 2)
                sRMRRefNo = sParsedSegment[2];

            //RMR Monetary Amount
            if (sParsedSegment.Length > 4)
                sRMRAmt = sParsedSegment[4];

            //RMR Total Invoice Amount
            if (sParsedSegment.Length > 5)
                sRMRTotInvoivceAmt = sParsedSegment[5];

            //RMR Discount Amount
            if (sParsedSegment.Length > 6)
                sRMRDiscountAmt = sParsedSegment[6];

            // Add the cents if it is missing
            if (sRMRAmt.Length > 0 && sRMRAmt.IndexOf('.') < 0)
                sRMRAmt += ".00";

            if (sRMRTotInvoivceAmt.Length > 0 && sRMRTotInvoivceAmt.IndexOf('.') < 0)
                sRMRTotInvoivceAmt += ".00";

            if (sRMRDiscountAmt.Length > 0 && sRMRDiscountAmt.IndexOf('.') < 0)
                sRMRDiscountAmt += ".00";

            // Begin Validating the Currency fields
            if (!DITBAI2Common.ValidateMoney(sRMRAmt))
                sRMRAmt = string.Empty;

            if (!DITBAI2Common.ValidateMoney(sRMRTotInvoivceAmt))
                sRMRTotInvoivceAmt = string.Empty;

            if (!DITBAI2Common.ValidateMoney(sRMRDiscountAmt))
                sRMRDiscountAmt = string.Empty;
        }

        /// <summary>
        ///  Parse the BPR Segment of the Addenda Record
        /// </summary>
        /// <param name="strParsedSegment"></param>
        /// <param name="dBPRAmount"></param>
        /// <param name="sBPRAccountNo"></param>
        private void ParseTRN(string[] sParsedSegment, out string sTraceNo)
        {
            sTraceNo = string.Empty;
            // The TRN Segment will have the format            
            //   TRN*1*140324120011\           
            // Need to parse out the following fields:            
            //    Field 03 =ReassociationTraceNumber             
            if (sParsedSegment.Length > 2)
                sTraceNo = sParsedSegment[2];
        }
        #endregion PARSE UNSTRUCTURED FED WIRE/CHIP TAG METHODS

        #region PARSE RELATED FED WIRE/CHIP TAG METHODS
        /// <summary>
        /// Parse Related addenda data for Fed wire or Chip tags
        /// </summary>
        /// <param name="sAddendaData"></param>
        /// <param name="RemittanceMethod"></param>
        /// <param name="xDoc"></param>
        /// <param name="xDocumentElement"></param>
        private void ParseRelatedAddendaData(string sAddendaData, DITBAI2Common.RemittanceMethod RemittanceMethod, XmlDocument xDoc, ref XmlElement xDocumentElement)
        {
            int iIndex;
            string sValue = string.Empty;
            XmlElement xRemittanceDataRecordElement;
            xRemittanceDataRecordElement = BuildRemittanceDataRecordNode(xDoc);
            if ((iIndex = sAddendaData.IndexOf(DITBAI2Common.FEDW_RELATED_TAG)) > -1)
                sValue = DITBAI2Common.GetParsedAddendaData(ref sAddendaData, iIndex, DITBAI2Common.FEDW_RELATED_TAG, string.Empty, 0);
            else if ((iIndex = sAddendaData.IndexOf(DITBAI2Common.CHIPS_RELATED_TAG)) > -1)
                sValue = DITBAI2Common.GetParsedAddendaData(ref sAddendaData, iIndex, DITBAI2Common.CHIPS_RELATED_TAG, string.Empty, 0);

            if (!string.IsNullOrWhiteSpace(sValue))
            {
                this.AddChildNodes(DITBAI2Common.RELATED_REM_INFO, sValue, xDoc, ref xRemittanceDataRecordElement);
                xDocumentElement.AppendChild(xRemittanceDataRecordElement);
            }
            else
                RemittanceBatchSequenceNo -= 1;
        }
        #endregion PARSE RELATED FED WIRE/CHIP TAG METHODS

        #region COMMON METHODS
        /// <summary>
        /// Build Remittance data record
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="iRemittanceSequenceNo"></param>
        /// <returns></returns>
        private XmlElement BuildRemittanceDataRecordNode(XmlDocument xDoc)
        {
            RemittanceDataRecord objRemittanceDataRecord = new RemittanceDataRecord();
            objRemittanceDataRecord = new RemittanceDataRecord();
            objRemittanceDataRecord.BatchSequence = ++RemittanceBatchSequenceNo;
            return objRemittanceDataRecord.BuildXMLNode(xDoc);
        }

        /// <summary>
        /// Adds the child nodes into the XML RemittanceDataRecordElement 
        /// </summary>
        /// <param name="sFieldName"></param>
        /// <param name="sFieldValue"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        /// <returns></returns>
        private bool AddChildNodes(string sFieldName, string sFieldValue, XmlDocument xDoc, ref XmlElement xRemittanceDataRecordElement)
        {
            bool bRetVal = false;
            if (!string.IsNullOrEmpty(sFieldValue))
            {
                RemittanceData objRemittanceData;
                objRemittanceData = new RemittanceData();
                objRemittanceData.FieldName = sFieldName;
                objRemittanceData.FieldValue = sFieldValue.ToString();
                xRemittanceDataRecordElement.AppendChild(objRemittanceData.BuildXMLNode(xDoc));
                m_DataExists = true;
                bRetVal = true;
            }
            return bRetVal;
        }
        #endregion COMMON METHODS
    }
}

