﻿using System;
using System.IO;
using WFS.LTA.DataImport.DataImportXClientBase;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
* Purpose: This is the main class which will generate the canonical XML
*
* Modification History 
* WI 136772 SAS 04/18/2014 
* Changes done to validate wire transfer file.
* 
* WI 136775 SAS 04/18/2014 
* Changes done to create canonical XML.
* 
* WI 157223 SAS 08/8/2014 
* Changes done to remove the time part from BAIFileCreationDate
******************************************************************************/

namespace DMP.BAI2.Components
{
    /// <summary>
    /// FileProcessor exposes core functions of DIT ACH File Import.
    /// </summary>
    public class DITBAI2Import : cDataImportXClientBase, IWireLogger
    {
        #region Class Methods
        /// Main processing routine.
        public override bool StreamToXML(StreamReader stmInputFile, enmFileType fltFileType, cConfigData cfgSettings, out System.Xml.XmlDocument xmdResult)
        {
            bool bResult = true;
            try
            {
                DITBAI2FileObject fileObject;
                fileObject = ValidateFile(stmInputFile);
                bool skipDebit = cfgSettings is cConfigData_DebitSwitch && ((cConfigData_DebitSwitch)cfgSettings).SkipDebit;

                if (fileObject != null)
                    xmdResult = fileObject.GetXML(skipDebit);
                else
                    xmdResult = null;
            }
            catch (Exception ex)
            {
                DoLogEvent("Exception in StreamToXML : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bResult = false;
                xmdResult = null;
            }
            return bResult;
        }

        /// <summary>
        /// Process Import File
        /// </summary>
        /// <param name="fileName">File name for import</param>
        /// <returns></returns>
        private DITBAI2FileObject ValidateFile(StreamReader stmInputFile)
        {            
            DITBAI2FileObject fileObject;
            try
            {
                fileObject = new DITBAI2FileObject(this);
                string sLine;
                DITBAI2Group objGroup = null;
                DITBAI2Account objAccount = null;
                DITBAI2Transaction objTransaction = null;
                DITBAI2Continuation objContinuation = null;                
                DoLogEvent("Reading file...", "ValidateFile()", LogEventType.Information);
                while ((sLine = stmInputFile.ReadLine()) != null)
                {                   
                    if (!string.IsNullOrEmpty(sLine) && DITBAI2Common.IsNumeric(DITBAI2Common.GetSubString(sLine, 0, 2)))
                    {
                        switch (DITBAI2Common.GetSubString(sLine, 0, 2))
                        {
                            case DITBAI2Common.HEADER_RECORD_TYPE_CODE:
                                fileObject.FileHeader = sLine;
                                fileObject.TotalRecords++;
                                break;

                            case DITBAI2Common.GROUP_HEADER_RECORD_TYPE_CODE:
                                objGroup = new DITBAI2Group(this);
                                objGroup.GroupHeader = sLine;
                                objGroup.TotalRecordCount++;
                                fileObject.TotalRecords++;
                                break;

                            case DITBAI2Common.ACCOUNT_IDENTIFER_RECORD_TYPE_CODE:
                                if (objGroup != null)
                                {
                                    objAccount = new DITBAI2Account(this);
                                    objAccount.AccountHeaderData = sLine;
                                    objAccount.TotalRecordCount++;
                                    objGroup.TotalRecordCount++;
                                }
                                fileObject.TotalRecords++;
                                break;

                            case DITBAI2Common.TRANSACTION_DETAIL_RECORD_TYPE_CODE:
                                if (objAccount != null)
                                {
                                    objTransaction = new DITBAI2Transaction(this);
                                    objTransaction.RawTransactionData = sLine;
                                    //Set ABA and DDA Value
                                    objTransaction.ABA = fileObject.SenderIdentification;
                                    objTransaction.DDA = objAccount.CustomerAccountNumber;

                                    if (fileObject.FileCreationDate != null)
                                        objTransaction.FileCreationDate = Convert.ToDateTime(fileObject.FileCreationDate).ToString("MM/dd/yyyy");
                                    else
                                        objTransaction.FileCreationDate = string.Empty;

                                    objTransaction.BAIFileID = fileObject.FileIdentificationNumber;
                                    objAccount.TotalRecordCount++;
                                    objAccount.Transactions.Add(objTransaction);
                                }
                                objGroup.TotalRecordCount++;
                                fileObject.TotalRecords++;
                                break;

                            case DITBAI2Common.CONTINUATION_RECORD_TYPE_CODE:
                                if (objAccount != null)
                                {
                                    objContinuation = new DITBAI2Continuation();
                                    objContinuation.RawContinuationData = sLine;
                                    objTransaction = objAccount.Transactions[objAccount.Transactions.Count - 1];
                                    objTransaction.Continuations.Add(objContinuation);                                    
                                    objAccount.TotalRecordCount++;
                                }
                                objGroup.TotalRecordCount++;
                                fileObject.TotalRecords++;

                                break;

                            case DITBAI2Common.ACCOUNT_FOOTER_RECORD_TYPE_CODE:
                                if (objGroup != null && objAccount != null)
                                {
                                    objAccount.AccountFooterData = sLine;
                                    objAccount.TotalRecordCount++;
                                    objGroup.Accounts.Add(objAccount);
                                }
                                objGroup.TotalRecordCount++;
                                fileObject.TotalRecords++;
                                break;

                            case DITBAI2Common.GROUP_FOOTER_RECORD_TYPE_CODE:
                                if (objGroup != null)
                                {
                                    objGroup.GroupFooter = sLine;
                                    fileObject.Groups.Add(objGroup);
                                }
                                objGroup.TotalRecordCount++;
                                fileObject.TotalRecords++;
                                break;
                            case DITBAI2Common.FILE_FOOTER_RECORD_TYPE_CODE:
                                fileObject.FileFooter = sLine;
                                fileObject.TotalRecords++;
                                break;
                        }                                          
                    }
                }
                if (!fileObject.DoValidate())
                    fileObject = null;
            }
            catch (Exception ex)
            {
                DoLogEvent("Exception in ValidateFile : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                fileObject = null;
            }
            return fileObject;
        }

        #endregion Class Methods
    }
}
