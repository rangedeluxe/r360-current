﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using System.Xml;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
*
* Purpose: This class holds the constants and the common methods used by other classes.
*
* Modification History
* WI 136775 SAS 04/18/2014 
* Changes done to create canonical XML.
* 
* WI 136895 SAS 05/07/2014
* Changes done to parse addenda data.
*
* WI 139926 SAS 05/07/2014
* Changes done to remove ItemDataRecords element and add GhostDocument Elements
*
* WI 139929 SAS 05/07/2014
* Changes done for duplicate file and payment detection.
* 
* WI 169845 SAS 10/16/2014 
* Added a constant for Document Descriptor
******************************************************************************/

namespace DMP.BAI2.Components
{
    public static class DITBAI2Common
    {


        public const string CLIENTPROCESSCODE = "";
        public  const string BATCHXSDVERSION = "2.01.00.00";
        public const string DOCUMENT_DESCRIPTOR = "ELECTRONIC";
        #region RECORD_TYPE_CONSTANTS
        /// <summary>
        /// Record Type Code of the file header 
        /// </summary>
        public const string HEADER_RECORD_TYPE_CODE = "01";
        /// <summary>
        /// Record Type Code of the group header 
        /// </summary>
        public const string GROUP_HEADER_RECORD_TYPE_CODE = "02";
        /// <summary>
        /// Record Type Code of the Account Identifier
        /// </summary>
        public const string ACCOUNT_IDENTIFER_RECORD_TYPE_CODE = "03";
        /// <summary>
        /// Record Type Code of the Transaction Detail
        /// </summary>
        public const string TRANSACTION_DETAIL_RECORD_TYPE_CODE = "16";
        /// <summary>
        /// Record Type Code of the Continuation 
        /// </summary>
        public const string CONTINUATION_RECORD_TYPE_CODE = "88";
        /// <summary>
        /// Record Type Code of the Account Trailer 
        /// </summary>        
        public const string ACCOUNT_FOOTER_RECORD_TYPE_CODE = "49";
        /// <summary>
        /// Record Type Code of the Group Trailer 
        /// </summary>
        public const string GROUP_FOOTER_RECORD_TYPE_CODE = "98";
        /// <summary>
        /// Record Type Code of the File Trailer  Record.
        /// </summary>
        public const string FILE_FOOTER_RECORD_TYPE_CODE = "99";
        #endregion RECORD_TYPE_CONSTANTS

        #region FED WIRE/CHIP TAG CONSTANTS
        /// <summary>
        /// Fed Wire Structured tags
        /// </summary>
        public const string FEDW_REMM_ORIG_TAG = "{8300}";        
        public const string FEDW_REMM_BENE_TAG = "{8350}";        
        public const string FEDW_PRI_REMM_DOC_INFO_TAG = "{8400}";        
        public const string FEDW_ACT_AMT_PAID_TAG = "{8450}";        
        public const string FEDW_GROSS_AMT_TAG = "{8500}";        
        public const string FEDW_DISC_AMT_TAG = "{8550}";        
        public const string FEDW_ADJ_INFO_TAG = "{8600}";
        public const string FEDW_REMM_DATE_TAG = "{8650}";        
        public const string FEDW_SEC_REMM_DOC_INFO_TAG = "{8700}";        
        public const string FEDW_REMM_FREE_TXT_TAG = "{8750}";
        
        /// <summary>
        /// Chips Structured tags
        /// </summary>
        public const string CHIPS_REMM_ORIG_TAG = "[830]";
        public const string CHIPS_REMM_BENE_TAG = "[835]";
        public const string CHIPS_PRI_REMM_DOC_INFO_TAG = "[840]";
        public const string CHIPS_ACT_AMT_PAID_TAG = "[845]";
        public const string CHIPS_GROSS_AMT_TAG = "[850]";
        public const string CHIPS_DISC_AMT_TAG = "[855]";
        public const string CHIPS_ADJ_INFO_TAG = "[860]";
        public const string CHIPS_REMM_DATE_TAG = "[865]";
        public const string CHIPS_SEC_REMM_DOC_INFO_TAG = "[870]";
        public const string CHIPS_REMM_FREE_TXT_TAG = "[875]";
        
        /// <summary>
        /// Fed wire unstructured tag
        /// </summary>
        public const string FEDW_UNSTRUCTURED_TAG = "{8200}";        
        /// <summary>
        /// Chips unstructured tag
        /// </summary>
        public const string CHIPS_UNSTRUCTURED_TAG = "[820]";        
        /// <summary>
        /// Fed wire Related tag
        /// </summary>
        public const string FEDW_RELATED_TAG = "{8250}";        
        /// <summary>
        /// Chips Related tag
        /// </summary>
        public const string CHIPS_RELATED_TAG = "[825]";

        public const string UNSTRUC_BPR = "BPR";
        public const string UNSTRUC_RMR = "RMR";
        public const string UNSTRUC_TRN = "TRN";

        #endregion FED WIRE/CHIP TAG CONSTANTS        

        #region REMITTANCE DATA FIELD NAMES CONSTANTS
        /// <summary>
        /// Remittance addenda field names
        /// </summary>        
        public const string AMOUNT = "Amount";
        public const string ACCT_NO = "AccountNumber";
        public const string INVOICE_NO = "InvoiceNumber";
        public const string BPR_MONETARY_AMT = "BPRMonetaryAmount";
        public const string BPR_ACT_NO = "BPRAccountNumber";
        public const string RMR_REF_NO = "RMRReferenceNumber";
        public const string RMR_MONETARY_AMT = "RMRMonetaryAmount";
        public const string RMR_TOT_INVOICE_AMT = "RMRTotalInvoiceAmount";
        public const string RMR_DISC_AMT = "RMRDiscountAmount";
        public const string REASSOCIATION_TRACE_NO = "ReassociationTraceNumber";
        public const string REM_ORIGINATOR = "RemittanceOriginator";
        public const string REM_BENEFICIARY = "RemittanceBeneficiary";
        public const string PRI_REM_DOC_INFO = "PrimaryRemittanceDocInfo";
        public const string ACTUAL_AMT_PAID = "ActualAmountPaid";
        public const string GROSS_AMT = "GrossAmount";
        public const string DISCOUNT_AMT = "DiscountAmount";
        public const string ADJ_INFO = "AdjustmentInformation";
        public const string REM_DATE = "RemittanceDate";
        public const string SEC_REM_DOC_INFO = "SecondaryRemittanceDocInfo";
        public const string REM_FREE_TXT = "RemittanceFreeText";
        public const string RELATED_REM_INFO = "RelatedRemittanceInfo";

        public const string ORIGINATOR_INFO = "OriginatorInfo";
        public const string ORIGINATOR_NAME = "OriginatorName";
        public const string ORIGINATOR_ACCT = "OriginatorAccount";
        public const string ORIGINATOR_INFO_START = "Originator to Beneficiary Info:";
        public const string ORIGINATOR_NAME_START = "Originator:ACCT-";
        public const string ORIGINATOR_ACCT_START = "Originator:ACCT-";
        #endregion REMITTANCE DATA FIELD NAMES CONSTANTS

        /// <summary>
        /// Transaction Type Codes Constants
        /// </summary>
        public const string INCOMINGMONEYTRANSFER = "195";
        public const string OUTGOINGMONEYTRANSFER = "495";

        #region DATA DELIMITERS
        /// <summary>
        /// BAI2 Data Element Delimiter
        /// </summary>
        public const string BAI2_DATA_DELIMITER = ",";        
        /// <summary>
        /// BAI2 Addenda FedWire Data Element Delimiter
        /// </summary>
        public const string FW_DATA_DELIMITER_ASTREK= "*";
        public const string FW_DATA_DELIMITER_BRACE = "{";
        public const string CHIP_DATA_DELIMITER_BRACKET = "[";
        /// <summary>
        /// Addenda Segment Delimiter
        /// </summary>
        public const string UNSTRUC_ADDENDA_SEG_DELIMITER = "\\";
        /// <summary>
        /// Addenda Data Element Delimiter
        /// </summary>
        public const string UNSTRUC_ADDENDA_DATA_DELIMITER = "*";

        #endregion DATA DELIMITERS

        #region ENUMERATION CONSTANTS
        /// <summary>
        /// Remittance Method
        /// </summary>
        public enum RemittanceMethod
        {
            INVALID_TAG = 0,
            FEDWIRE_TAGS = 1,
            CHIPS_TAGS = 2,
            ISO_XML_TAGS = 3
        }
        /// <summary>
        ///FedWire/CHIPS remittance Type
        /// </summary>
        public enum RemittanceType
        {
            UNKNOWN = 0,
            STRUCTURED = 1,
            UNSTRUCTURED = 2,
            RELATED = 3
        }
        
        #endregion ENUMERATION CONSTANTS

        #region COMMON_FUNCTIONS
        /// <summary>
        /// This function will return a substring
        /// </summary>
        /// <param name="sstring"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetSubString(string sstring, int startIndex, int length)
        {
            return sstring.Substring(startIndex, length);
        }
        /// <summary>
        /// Check if the value is numeric
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNumeric(string value)
        {
            bool  retvalue = true;
            foreach (Char c in value.ToCharArray())
            {
                retvalue = retvalue && Char.IsDigit(c);
            }
            return retvalue;
        }
        /// <summary>
        /// Check if the value is numeric, with an optional leading "+" or "-" sign.
        /// </summary>
        public static bool IsNumericWithOptionalLeadingSign(string value)
        {
            return Regex.IsMatch(value, @"^[-+]?\d*$");
        }

        /// <summary>
        /// Validate Currency
        /// </summary>
        /// <param name="strMoney"></param>
        /// <returns></returns>
        public static bool ValidateMoney(string sMoney)
        {            
            try
            {
                decimal dMoney = 0.00m;
                if (sMoney.Length > 0)
                {
                    dMoney = decimal.Parse(sMoney, System.Globalization.NumberStyles.Currency);
                    return true;
                }
                return false;
            }
            catch 
            {
                return false;
            }
        }

        /// <summary>
        /// Validates and returns date if its valid
        /// </summary>
        /// <param name="sDate"></param>
        /// <returns></returns>
        public static string ValidateDate(string sDate)
        {
            string sResult = string.Empty;
            try
            {
              sResult =(DateTime.ParseExact(sDate, "yyyyMMdd", null).ToString("MM/dd/yyyy"));
            }
            catch 
            {
               sResult=string.Empty;
            }
            return sResult;

        }
        /// <summary>
        /// Gets the nth (comma-separated) value from a line of a BAI2 input file.
        /// </summary>
        /// <param name="input">The input string</param>
        /// <param name="oneBasedIndex">The one-based index of the value to return</param>
        /// <param name="includeRestOfLine">True to treat this as the last value in the line,
        /// and return the rest of the line including delimiters.</param>
        /// <returns></returns>
        public static string GetParsedData(string input, int oneBasedIndex, bool includeRestOfLine = false)
        {
            var index = oneBasedIndex - 1;

            try
            {
                var count = includeRestOfLine ? index + 1 : int.MaxValue;
                var segments = input.Split(BAI2_DATA_DELIMITER.ToCharArray(), count);
                if (index >= segments.Length)
                    return string.Empty;

                var segment = segments[index];
                if (segment.Trim().EndsWith("/"))
                    segment = segment.Replace("/", "");
                return segment;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Parses the data
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="iStartIndex"></param>
        /// <param name="sStripOffTag"></param>
        /// <param name="iStripOffPos"></param>
        /// <returns></returns>
        public static string GetParsedAddendaData(ref string sValue,int iStartIndex, string sStripOffTag, string sDelimiter,int iStripOffPos)
        {
            string sResult = string.Empty;
            sResult = sValue.Substring(iStartIndex + (sStripOffTag.Length + iStripOffPos));
            sValue = sValue.Remove(iStartIndex, (sStripOffTag.Length));
            if (!string.IsNullOrWhiteSpace(sDelimiter))
                sResult = sResult.Substring(0, sResult.IndexOf(sDelimiter));
            return sResult;
        }

        /// <summary>
        /// Get hash
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static string GetHash(string sValue)
        {
            string sResult = string.Empty;
            var data = Encoding.ASCII.GetBytes(sValue);
            var hashData = new SHA1Managed().ComputeHash(data);
            foreach (var b in hashData)
                sResult += b.ToString("X2");
            return sResult;
        }

        #endregion COMMON_FUNCTIONS
    }
}
