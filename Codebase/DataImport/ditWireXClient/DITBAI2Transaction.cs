﻿using System;
using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;



/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2014
*
* Purpose: This class will hold the Transaction data for a Account.
*
* Modification History
* WI 136775 SAS 04/18/2014 
* Changes done to create canonical XML.
* 
* WI 136895 SAS 05/07/2014
* Changes done to parse addenda data.
*
* WI 139926 SAS 05/07/2014
* Changes done to remove ItemDataRecords element and add GhostDocument Elements
*
* WI 139929 SAS 05/07/2014
* Changes done for duplicate file and payment detection.
*
* WI 143081 SAS 05/26/2014 
* Changes done to add RawDataRecord elements
*
* WI 147424 SAS 6/17/2014
* Adding RawDataRecord with proper sequence
* 
* WI 158063 SAS 08/12/2014
* Added value for Serial attribute in Payment Element to CustomerReferenceNumber
* 
* WI 158097 SAS 08/12/2014
* Changes done to add Remitter Name.
* 
* WI 169845 SAS 10/16/2014 
* Changes done to replace GhostDocument with Document Element
* 
* WI 173023 SAS 10/21/2014 
* Changes done to set BatchSequence number
* 
* WI 181857 SAS 12/15/2014 
* Changes done to add criteria to check if addenda records exists 
******************************************************************************/

namespace DMP.BAI2.Components
{
    /// <summary>
    /// Wire Transfer Transaction Item class
    /// </summary>
    public sealed class DITBAI2Transaction
    {
        #region Class Fields
        /// <summary>
        /// Transaction Raw Wire Transfer Data
        /// </summary>
        private string m_RawTransactionData = string.Empty;
        /// used for exception handling
        /// </summary>
        private readonly IWireLogger _logger;
        /// <summary>
        /// Transaction ID
        /// </summary>
        private int m_TransactionID = 0;
        /// <summary>
        /// Transaction Sequence number
        /// </summary>
        private int m_TransactionSeqNo = 0;
        /// <summary>
        /// Payment Batch Sequence
        /// </summary>
        private int m_PaymentBatchSequence = 0;
        /// <summary>
        /// Document Batch Sequence
        /// </summary>
        private int m_DocumentBatchSequence;
        /// <summary>
        /// Document Sequence
        /// </summary>
        private int m_DocumentSequence;
        /// <summary>
        /// Document Sequence within transaction;
        /// </summary>
        private int m_DocSeqWithinTransaction;
        /// <summary>
        /// ABA 
        /// </summary>
        private string m_ABA = string.Empty;
        /// <summary>
        /// DDA
        /// </summary>
        private string m_DDA = string.Empty;
        /// <summary>
        /// File Creation Date
        /// </summary>
        private string m_FileCreationDate = string.Empty;
        /// <summary>
        /// BAI File ID
        /// </summary>
        private string m_BAIFileID = string.Empty;
        /// <summary>
        /// Transaction Signature
        /// </summary>
        private string m_TransactionSignature = string.Empty;
        /// <summary>
        /// Payment Remitter Name 
        /// </summary>
        private string m_RemitterName = string.Empty;
        /// <summary>
        /// Transaction Continuation records
        /// </summary>
        private DITBAI2Continuations m_Continuations = null;
        #endregion Class Fields
        #region Class Properties
        /// <summary>
        /// m_RawACHData public property
        /// </summary>
        public string RawTransactionData
        {
            get { return m_RawTransactionData; }
            set { m_RawTransactionData = value; }
        }

        /// <summary>
        /// Record Code
        /// </summary>
        public string RecordCode
        {
            get { return DITBAI2Common.GetParsedData(m_RawTransactionData, 1); }
        }
        /// <summary>
        /// Transaction Type Code 
        /// </summary>
        public string TransactionTypeCode
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_RawTransactionData, 2);
                if (!string.IsNullOrEmpty(sResult))
                    return sResult;
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Transaction Amount 
        /// </summary>
        public string Amount
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_RawTransactionData, 3);
                if (!string.IsNullOrEmpty(sResult) && DITBAI2Common.IsNumeric(sResult))
                    return sResult;
                else
                    return string.Empty;
            }
        }
        /// <summary>
        /// Fund Types
        /// </summary>
        public string FundTypeCode
        {
            get { return DITBAI2Common.GetParsedData(m_RawTransactionData, 4); }
        }

        /// <summary>
        /// Bank Reference Number 
        /// </summary>
        public string BankReferenceNumber
        {
            get { return DITBAI2Common.GetParsedData(m_RawTransactionData, 5); }
        }

        /// <summary>
        /// Customer Reference Number 
        /// </summary>
        public string CustomerReferenceNumber
        {
            get { return DITBAI2Common.GetParsedData(m_RawTransactionData, 6); }
        }

        /// <summary>
        /// Unparsed Text
        /// </summary>
        public string UnparsedText
        {
            get { return DITBAI2Common.GetParsedData(m_RawTransactionData, 7); }
        }


        /// <summary>
        /// Transaction ID
        /// </summary>
        public int TransactionID
        {
            get { return m_TransactionID; }
            set { m_TransactionID = value; }
        }

        /// <summary>
        /// Transaction Sequence number
        /// </summary>
        public int TransactionSeqNo
        {
            get { return m_TransactionSeqNo; }
            set { m_TransactionSeqNo = value; }
        }

        /// <summary>
        /// Payment Batch Sequence no
        /// </summary>
        public int PaymentBatchSequence
        {
            get { return m_PaymentBatchSequence; }
            set { m_PaymentBatchSequence = value; }
        }


        /// <summary>
        /// Document Batch Sequence no
        /// </summary>
        public int DocumentBatchSequence
        {
            get { return m_DocumentBatchSequence; }
            set { m_DocumentBatchSequence = value; }
        }

        /// <summary>
        /// Document Sequence
        /// </summary>
        public int DocumentSequence
        {
            get { return m_DocumentSequence; }
            set { m_DocumentSequence = value; }
        }

        /// <summary>
        /// Document Sequence
        /// </summary>
        public int DocSeqWithinTransaction
        {
            get { return m_DocSeqWithinTransaction; }
            set { m_DocSeqWithinTransaction = value; }
        }

        /// <summary>
        /// ABA 
        /// </summary>
        public string ABA
        {
            get { return m_ABA; }
            set { m_ABA = value; }
        }

        /// <summary>
        /// DDA
        /// </summary>
        public string DDA
        {
            get { return m_DDA; }
            set { m_DDA = value; }
        }

        /// <summary>
        /// FileCreationDate
        /// </summary>
        public string FileCreationDate
        {
            get { return m_FileCreationDate; }
            set { m_FileCreationDate = value; }
        }

        /// <summary>
        /// BAI File ID
        /// </summary>
        public string BAIFileID
        {
            get { return m_BAIFileID; }
            set { m_BAIFileID = value; }
        }

        /// <summary>
        /// Transaction Signature
        /// </summary>
        public string TransactionSignature
        {
            get
            {
                return m_TransactionSignature;
            }
            set
            {
                m_TransactionSignature = value;
                StringBuilder sbResult = new StringBuilder();
                sbResult.Append(m_TransactionSignature);
                sbResult.Append(this.RecordCode);
                sbResult.Append(this.TransactionTypeCode);
                sbResult.Append(this.Amount);
                sbResult.Append(this.FundTypeCode);
                sbResult.Append(this.BankReferenceNumber);
                sbResult.Append(this.CustomerReferenceNumber);
                m_TransactionSignature = sbResult.ToString();
            }
        }

        /// <summary>
        /// Returns the Transaction hash
        /// </summary>
        public string TransactionHash
        {
            get { return DITBAI2Common.GetHash(TransactionSignature); }
        }

        /// <summary>
        /// Remitter Name
        /// </summary>
        public string RemitterName
        {
            get { return m_RemitterName; }
            set { m_RemitterName = value; }
        }

        /// <summary>
        /// m_Continuations public property
        /// </summary>
        public DITBAI2Continuations Continuations
        {
            get { return m_Continuations; }
        }

        public string OriginatorInfo { get; set; }

        public string OriginatorName
        { get; set; }

        public string OriginatorAccount
        { get; set; }

        public string SendingBank { get; set; }

        #endregion Class Properties

        #region Class Constructor
        /// <summary>
        /// Class constructor
        /// </summary>
        public DITBAI2Transaction(IWireLogger logger)
        {
            _logger = logger;
            this.m_Continuations = new DITBAI2Continuations();
            this.m_TransactionID = 0;
            this.m_TransactionSeqNo = 0;
            this.m_DocumentBatchSequence = 0;
            this.m_DocumentSequence = 0;
            this.m_DocSeqWithinTransaction = 0;
        }

        #endregion Class Constructor

        #region Class Methods

        private void LogException(Exception exception, [CallerMemberName] string caller = null)
        {
            _logger.DoLogEvent($"Exception in {caller} : {exception}", GetType().Name,
                LogEventType.Error, LogEventImportance.Essential);
        }

        /// <summary>
        /// Build nodes for Transaction
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement BuildTransactionNode(XmlDocument xDoc)
        {
            XmlElement xTransactionElement = (XmlElement)xDoc.CreateElement("Transaction"); ;
            try
            {
                xTransactionElement.SetAttribute("TransactionID", this.TransactionID.ToString());
                xTransactionElement.SetAttribute("TransactionSequence", this.TransactionSeqNo.ToString());
                xTransactionElement.SetAttribute("TransactionHash", TransactionHash);
                xTransactionElement.SetAttribute("TransactionSignature", this.TransactionSignature);
            }
            catch (Exception ex)
            {
                LogException(ex);
                xTransactionElement = null;
            }
            return xTransactionElement;
        }

        /// <summary>
        /// Build Payment nodes
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement BuildPaymentNode(XmlDocument xDoc)
        {
            XmlElement xPaymentElement = (XmlElement)xDoc.CreateElement("Payment");
            int iCheckSequence = 1;
            decimal dPaymentAmount = 0;
            try
            {
                xPaymentElement.SetAttribute("BatchSequence", this.PaymentBatchSequence.ToString());
                string sResult = DITBAI2Common.GetParsedData(m_RawTransactionData, 3);
                //Calculate the payment
                if (!string.IsNullOrEmpty(this.Amount))
                {
                    if (TransactionTypeCode == DITBAI2Common.OUTGOINGMONEYTRANSFER)
                        dPaymentAmount = (Convert.ToDecimal(sResult) / 100) * -1;
                    else
                        dPaymentAmount = Convert.ToDecimal(sResult) / 100;
                }
                xPaymentElement.SetAttribute("Amount", dPaymentAmount.ToString());
                xPaymentElement.SetAttribute("CheckSequence", iCheckSequence.ToString());
                xPaymentElement.SetAttribute("Serial", this.CustomerReferenceNumber);
                xPaymentElement.SetAttribute("RemitterName", string.Empty);
                xPaymentElement.SetAttribute("RT", string.Empty);
                xPaymentElement.SetAttribute("Account", string.Empty);
                xPaymentElement.SetAttribute("TransactionCode", string.Empty);
                xPaymentElement.SetAttribute("ABA", this.ABA.ToString());
                xPaymentElement.SetAttribute("DDA", this.DDA.ToString());

                XmlElement xRawDataRecordElement = BuildRawDataRecordNode(xDoc);
                if (xRawDataRecordElement != null)
                    xPaymentElement.AppendChild(xRawDataRecordElement);
                //originator data
                BuildOriginatorNode();
                xPaymentElement.AppendChild(BuildPaymentRemittanceDataRecordNode(xDoc));
            }
            catch (Exception ex)
            {
                LogException(ex);
                xPaymentElement = null;
            }
            return xPaymentElement;
        }

        private void BuildOriginatorNode()
        {
            var originatorData = false;
            var cleanRawData = "";
            if (this.Continuations.Count > 0)
            {
                cleanRawData = this.Continuations.Cast<DITBAI2Continuation>().Aggregate(cleanRawData,
                    (current, ditbai2Continuation) => current + ditbai2Continuation.RawContinuationData.Substring(3));

                Regex rg = new Regex("Originator to Beneficiary Info: ([^-]*)-");
                var res = rg.Match(cleanRawData);
                this.OriginatorInfo = res.Success ? res.Groups[1].Value : "";

                rg = new Regex("Originator: ACCT-[^,]*,([^,]+),");
                res = rg.Match(cleanRawData);
                this.OriginatorName = res.Success ? res.Groups[1].Value : "";

                rg = new Regex("Originator: ACCT-([^,]*),");
                res = rg.Match(cleanRawData);
                this.OriginatorAccount = res.Success ? res.Groups[1].Value : "";

                rg = new Regex("Sending Bank:([^-]*)");
                res = rg.Match(cleanRawData);
                this.SendingBank = res.Success ? res.Groups[1].Value.Trim() : "";
            }
        }


        /// <summary>
        /// Build Raw DataRecord Element
        /// WI 147424 : Adding RawDataRecord sequentially
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement BuildRawDataRecordNode(XmlDocument xDoc)
        {
            bool bDataExists = false;
            int iRawDataSequence = 0;
            XmlElement xRawDataRecordElement;
            try
            {
                if (this.Continuations.Count > 0)
                {
                    RawDataRecord objRawDataRecord = new RawDataRecord();
                    objRawDataRecord.BatchSequence = this.PaymentBatchSequence;
                    xDoc.Load(objRawDataRecord.ToRawDataRecordElement().CreateReader());
                    xRawDataRecordElement = xDoc.DocumentElement;
                    foreach (DITBAI2Continuation objContinuation in this.Continuations)
                    {
                        RawData objRawData = new RawData(objContinuation.AddendaRawData, ++iRawDataSequence);
                        foreach (XElement xRawDataElement in objRawData.ToRawDataElements())
                        {
                            xDoc.Load(xRawDataElement.CreateReader());
                            xRawDataRecordElement.AppendChild(xDoc.DocumentElement);
                        }
                        bDataExists = true;
                    }
                }
                else
                    xRawDataRecordElement = null;

                if (!bDataExists)
                    xRawDataRecordElement = null;

            }
            catch (Exception ex)
            {
                LogException(ex);
                xRawDataRecordElement = null;
            }
            return xRawDataRecordElement;

        }

        /// <summary>
        /// Build Document nodes
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement BuildDocumentNode(XmlDocument xDoc)
        {
            XmlElement xDocumentElement;
            try
            {
                Document objDocument = new Document();
                objDocument.BatchSequence = this.DocumentBatchSequence;
                objDocument.DocumentDescriptor = DITBAI2Common.DOCUMENT_DESCRIPTOR;
                objDocument.DocumentSequence = this.DocumentSequence.ToString();
                objDocument.SequenceWithinTransaction = this.DocSeqWithinTransaction.ToString();
                xDocumentElement = objDocument.BuildXMLNode(xDoc);
                if (this.Continuations.Count > 0)
                {
                    this.Continuations.RemittanceBatchSequenceNo = this.DocumentBatchSequence;
                    this.Continuations.BuildRemittanceNode(xDoc, ref xDocumentElement);
                    this.RemitterName = this.Continuations.RemitterName;
                    //Reset to the latest Batch Sequence , that will be used for the next payment within the same Batch
                    this.DocumentBatchSequence = this.Continuations.RemittanceBatchSequenceNo;
                }

            }
            catch (Exception ex)
            {
                LogException(ex);
                xDocumentElement = null;
            }
            return xDocumentElement;
        }

        /// <summary>
        /// Build Payment Remittance Data Record nodes.
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        private XmlElement BuildPaymentRemittanceDataRecordNode(XmlDocument xDoc)
        {
            XmlElement xPaymentRemittanceDataRecord = null;
            try
            {
                RemittanceDataRecord objRemittanceDataRecord = new RemittanceDataRecord();
                objRemittanceDataRecord.BatchSequence = this.PaymentBatchSequence;
                xPaymentRemittanceDataRecord = objRemittanceDataRecord.BuildXMLNode(xDoc);


                this.BuildPaymentRemittanceDataNode("BAIFileCreationDate", this.FileCreationDate, xDoc, ref xPaymentRemittanceDataRecord);
                this.BuildPaymentRemittanceDataNode("BAIFileID", this.BAIFileID, xDoc, ref xPaymentRemittanceDataRecord);
                this.BuildPaymentRemittanceDataNode("CustomerAccountNumber", this.DDA, xDoc, ref xPaymentRemittanceDataRecord);
                this.BuildPaymentRemittanceDataNode("TypeCode", this.TransactionTypeCode.ToString(), xDoc, ref xPaymentRemittanceDataRecord);
                this.BuildPaymentRemittanceDataNode("BAIBankReference", this.BankReferenceNumber, xDoc, ref xPaymentRemittanceDataRecord);
                this.BuildPaymentRemittanceDataNode("BAICustomerReference", this.CustomerReferenceNumber, xDoc, ref xPaymentRemittanceDataRecord);

                /*Originator Info   Originator Name   OriginatorAccount*/
                if (!string.IsNullOrEmpty(this.OriginatorInfo))
                    this.BuildPaymentRemittanceDataNode("OriginatorInfo", this.OriginatorInfo, xDoc, ref xPaymentRemittanceDataRecord);
                if (!string.IsNullOrEmpty(this.OriginatorName))
                    this.BuildPaymentRemittanceDataNode("OriginatorName", this.OriginatorName, xDoc, ref xPaymentRemittanceDataRecord);
                if (!string.IsNullOrEmpty(this.OriginatorAccount))
                    this.BuildPaymentRemittanceDataNode("OriginatorAccount", this.OriginatorAccount, xDoc, ref xPaymentRemittanceDataRecord);
                if (!string.IsNullOrEmpty(this.SendingBank))
                    this.BuildPaymentRemittanceDataNode("SendingBank", this.SendingBank, xDoc, ref xPaymentRemittanceDataRecord);

            }
            catch (Exception ex)
            {
                LogException(ex);
                xPaymentRemittanceDataRecord = null;
            }
            return xPaymentRemittanceDataRecord;
        }

        /// <summary>
        /// Build payment Remittance data node
        /// </summary>
        /// <param name="sFieldName"></param>
        /// <param name="sFieldValue"></param>
        /// <param name="xDoc"></param>
        /// <param name="xRemittanceDataRecordElement"></param>
        public void BuildPaymentRemittanceDataNode(string sFieldName, string sFieldValue, XmlDocument xDoc, ref XmlElement xRemittanceDataRecordElement)
        {
            try
            {
                if (!string.IsNullOrEmpty(sFieldValue))
                {
                    RemittanceData objRemittanceData;
                    objRemittanceData = new RemittanceData();
                    objRemittanceData.FieldName = sFieldName;
                    objRemittanceData.FieldValue = sFieldValue.ToString();
                    xRemittanceDataRecordElement.AppendChild(objRemittanceData.BuildXMLNode(xDoc));
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                xRemittanceDataRecordElement = null;
            }
        }


        #endregion Class Methods
    }


    /// <summary>
    /// Transaction Collection class
    /// </summary>
    internal sealed class DITBAI2Transactions : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITBAI2Transaction this[int index]
        {
            get
            {
                return ((DITBAI2Transaction)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }
        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITBAI2Transaction value)
        {
            return (List.Add(value));
        }
        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITBAI2Transaction value)
        {
            return (List.IndexOf(value));
        }
        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITBAI2Transaction value)
        {
            List.Insert(index, value);
        }
        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITBAI2Transaction value)
        {
            List.Remove(value);
        }
        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITBAI2Transaction value)
        {
            // If value is not of type ACHBatch, this will return false.
            return (List.Contains(value));
        }
        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.BAI2.Components.DITBAI2Transaction"))
                throw new ArgumentException("value must be of type DITBAI2Transaction.", "value");
        }

    }
}
