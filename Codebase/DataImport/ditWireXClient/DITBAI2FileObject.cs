﻿using System;
using System.Xml;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Text;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/03/2104
*
*
* Purpose: This class acts as a parent class and all the remaining records types 
* will be underneath.
*
* Modification History
* WI 136775 SAS 04/18/2014
* Changes done to create canonical XML.
*
* WI 136895 SAS 05/07/2014
* Changes done to parse addenda data.
*
* WI 139926 SAS 05/07/2014
* Changes done to remove ItemDataRecords element and add GhostDocument Elements
*
* WI 139929 SAS 05/07/2014
* Changes done for duplicate file and payment detection.
* 
* WI 157223 SAS 08/8/2014 
* Changes done to remove the time part from BAIFileCreationDate. Removed property 
* FileCreatedDateTime because the property FileCreationDate will be used to assign to 
* BAIFileCreation Date
*******************************************************************************/

namespace DMP.BAI2.Components
{
    public sealed class DITBAI2FileObject
    {
        #region Class Fields
        /// <summary>
        /// File Header 
        /// </summary>
        private string m_FileHeader = string.Empty;
        /// <summary>
        /// File Trailer
        /// </summary>
        private string m_FileFooter = string.Empty;
        /// <summary>
        /// Total Records
        /// </summary>
        private long m_TotalRecords = 0;
        /// <summary>
        /// Group Collection
        /// </summary>
        private DITBAI2Groups m_Groups;

        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        private readonly IWireLogger _logger;

        #endregion Class Fields

        #region Class Properties

        /// <summary>
        /// m_FileHeader public property
        /// </summary>
        public string FileHeader
        {
            get { return m_FileHeader; }
            set { m_FileHeader = value; }
        }
        /// <summary>
        /// m_FileFooter public property
        /// </summary>
        public string FileFooter
        {
            get { return m_FileFooter; }
            set { m_FileFooter = value; }
        }
        /// <summary>
        /// m_TotalRecords public property
        /// </summary>
        public long TotalRecords
        {
            get { return m_TotalRecords; }
            set { m_TotalRecords = value; }
        }

        /// <summary>
        /// Header Record Code
        /// </summary>
        public string HeaderRecordCode
        {
            get { return DITBAI2Common.GetParsedData(m_FileHeader, 1); }
        }   
     
        /// <summary>
        /// Sender Identification  
        /// </summary>
        public string SenderIdentification
        {
            get{return DITBAI2Common.GetParsedData(m_FileHeader, 2);}
        }

        /// <summary>
        /// Receiver Information 
        /// </summary>
        public string ReceiverInformation
        {
            get{return DITBAI2Common.GetParsedData(m_FileHeader, 3);}
        }

        /// <summary>
        /// FileCreationDate 
        /// </summary>
        public object FileCreationDate
        {
            get
            {
                DateTime dt;
                try
                {
                    dt = DateTime.ParseExact(DITBAI2Common.GetParsedData(m_FileHeader, 4), "yyMMdd", null);
                }
                catch
                {
                    return DBNull.Value;
                }
                return dt;
            }
        }

        /// <summary>
        /// File Creation Time 
        /// </summary>
        public string FileCreationTime
        {
            get{return DITBAI2Common.GetParsedData(m_FileHeader, 5);}
        }

        /// <summary>
        /// File Identification Number 
        /// </summary>
        public string FileIdentificationNumber
        {
            get{ return DITBAI2Common.GetParsedData(m_FileHeader, 6);}
        }

        /// <summary>
        /// Physical Record Length 
        /// </summary>
        public string PhysicalRecordLength
        {
            get{return DITBAI2Common.GetParsedData(m_FileHeader, 7);}
        }

        /// <summary>
        /// Block Size 
        /// </summary>
        public string BlockSize
        {
            get{return DITBAI2Common.GetParsedData(m_FileHeader, 8);}
        }

        /// <summary>
        /// Version Number 
        /// </summary>
        public string VersionNumber
        {
            get
            {
                return DITBAI2Common.GetParsedData(m_FileHeader, 9);
            }
        }

        /// <summary>
        /// Footer Record Code
        /// </summary>
        public string FooterRecordCode
        {
            get { return DITBAI2Common.GetParsedData(m_FileFooter, 1); }
        }

        /// <summary>
        /// File Control Total
        /// </summary>
        public string FileControlTotal
        {
            get
            {
                var sResult = DITBAI2Common.GetParsedData(m_FileFooter, 2);
                return !string.IsNullOrEmpty(sResult) && DITBAI2Common.IsNumericWithOptionalLeadingSign(sResult)
                    ? sResult
                    : "0";
            }
        }

        /// <summary>
        /// Group Count
        /// </summary>
        public string GroupCount
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_FileFooter, 3);
                if ((!string.IsNullOrEmpty(sResult)) && DITBAI2Common.IsNumeric(sResult))
                    return sResult;
                else
                    return "0";
            }
        }

        /// <summary>
        /// Record Count
        /// </summary>
        public string RecordCount
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_FileFooter, 4);
                if ((!string.IsNullOrEmpty(sResult)) && DITBAI2Common.IsNumeric(sResult))
                    return sResult;
                else
                    return "0";
            }
        }

        /// <summary>
        /// Get the file signature
        /// </summary>
        public string FileSignature
        {
            get
            {
                StringBuilder sbFileSignature = new StringBuilder();
                sbFileSignature.Append(HeaderRecordCode);
                if (FileCreationDate != null)
                    sbFileSignature.Append(Convert.ToDateTime(FileCreationDate).ToString("yyMMdd"));   
                sbFileSignature.Append(FileIdentificationNumber);
                sbFileSignature.Append(FooterRecordCode);
                sbFileSignature.Append(FileControlTotal);
                sbFileSignature.Append(GroupCount);
                sbFileSignature.Append(RecordCount);
                return sbFileSignature.ToString(); ;
            }
        }

        /// <summary>
        /// Returns the file hash
        /// </summary>
        public string FileHash
        {
            get { return DITBAI2Common.GetHash(FileSignature); }
        }

        /// <summary>
        /// m_Batches public property
        /// </summary>
        internal DITBAI2Groups Groups
        {
            get { return m_Groups; }
        }


        #endregion Class Properties

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="name">Full File Name</param>
        public DITBAI2FileObject(IWireLogger logger)
        {
            m_Groups = new DITBAI2Groups();
            //m_DataImportXClient = oDataImportXClient;
            _logger = logger;
        }

        /// <summary>
        /// Validate Data
        /// </summary>
        /// <returns></returns>
        public bool DoValidate()
        {
            bool bResult = true;
            try
            {
                //Validate Count of records in a file
                if (Convert.ToInt64(RecordCount) != TotalRecords)
                {
                    _logger.DoLogEvent($"Error: Incorrect Number of Records, File Trailer Count: {Convert.ToInt64(RecordCount)}. Calculated: {TotalRecords}." , GetType().Name, LogEventType.Error,LogEventImportance.Essential);
                    bResult = false;
                }

                //Validate Count of Groups in a file
                if (Convert.ToInt64(GroupCount) != Groups.Count)
                {
                    _logger.DoLogEvent($"Error: Incorrect Number of Groups, File Trailer Group Count: {Convert.ToInt64(GroupCount)}. Calculated: {Groups.Count}.", GetType().Name, LogEventType.Error,LogEventImportance.Essential);
                    bResult = false;
                }

                //Validate Group Control Total
                long iGroupControlTotal = 0;
                iGroupControlTotal = this.Groups.GetGroupControlTotal();
                if (Convert.ToInt64(this.FileControlTotal) != iGroupControlTotal)
                {
                    _logger.DoLogEvent($"Error: Incorrect File Control Total, File Control Total : {Convert.ToInt64(FileControlTotal)}. Group Control Total : {iGroupControlTotal}.", GetType().Name, LogEventType.Error,LogEventImportance.Essential);
                    bResult = false;
                }

                //Validate Group Data
                if (!this.Groups.DoValidate())
                    bResult = false;

            }
            catch (Exception ex)
            {
                bResult = false;
                _logger.DoLogEvent("Exception in DoValidate : " + ex.Message, GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                _logger.DoLogEvent($"  File Trailer Count: {Convert.ToInt64(RecordCount)}. ", GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                _logger.DoLogEvent($"  File Trailer Group Count: {Convert.ToInt64(GroupCount)}.", GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                _logger.DoLogEvent($"  File Control Total : {Convert.ToInt64(FileControlTotal)}.", GetType().Name, LogEventType.Error, LogEventImportance.Essential);
            }
            return bResult;
        }


        /// <summary>
        /// Generate the required Canonical XML
        /// </summary>
        /// <param name="skipDebit"></param>
        /// <returns></returns>
        public XmlDocument GetXML(bool skipDebit)
        {
            XmlDocument xDoc = new XmlDocument();
            XmlElement xRootElement = xDoc.CreateElement("Batches");
            xRootElement.SetAttribute("SourceTrackingID", Guid.NewGuid().ToString());
            xRootElement.SetAttribute("ClientProcessCode", DITBAI2Common.CLIENTPROCESSCODE);
            xRootElement.SetAttribute("XSDVersion", DITBAI2Common.BATCHXSDVERSION);
            xRootElement.SetAttribute("FileHash", this.FileHash);
            xRootElement.SetAttribute("FileSignature", this.FileSignature);
            int transactionInBatchcount = 0;
            try
            {
                foreach (DITBAI2Group objDITBAI2Group in this.Groups)
                {
                    if (objDITBAI2Group.BuildBatchNode(xDoc, ref xRootElement, skipDebit)) transactionInBatchcount++;
                    xDoc.AppendChild(xRootElement);
                }
                if (transactionInBatchcount == 0)
                {
                    _logger.DoLogEvent("No transactions in Batches", GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                    xDoc = null;
                }
            }
            catch (Exception ex)
            {
                _logger.DoLogEvent("Exception in GetXML : " + ex.Message, GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xDoc = null;
            }
            return xDoc;
        }
    }
}
