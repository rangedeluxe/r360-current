﻿using System;
using System.Collections;
using System.Xml;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/18/2104
*
* Purpose: This class will hold the data for a Group header and Group header collection
*
* Modification History
* WI 136775 SAS 04/18/2014 
* Changes done to create canonical XML.
*
* WI 136895 SAS 05/07/2014
* Changes done to parse addenda data.
*
* WI 139926 SAS 05/07/2014
* Changes done to remove ItemDataRecords element and add GhostDocument Elements
*
* WI 139929 SAS 05/07/2014
* Changes done for duplicate file and payment detection.
* 
* WI 180187 SAS 12/16/2014 
* Changes done to discontinue to pass value for BatchID
******************************************************************************/

namespace DMP.BAI2.Components
{
    internal sealed class DITBAI2Group
    {

        #region Class Fields
        /// <summary>
        /// Stores "Group header" value of the file structure
        /// </summary>
        private string m_GroupHeader;
        /// <summary>
        /// Stores "Group trailer" value of the file structure
        /// </summary>
        private string m_GroupFooter;
        /// <summary>
        /// Record Counts
        /// </summary>
        private long m_TotalRecordCount;
        /// <summary>
        ///  Wire Transfer Batch Id
        /// </summary>
        /// Account Collection 
        /// </summary>
        private DITBAI2Accounts m_Accounts;
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        #endregion Class Fields

        /// <summary>
        /// Class constructor 
        /// </summary>
        public DITBAI2Group(cDataImportXClientBase oDataImportXClient)
        {
            m_Accounts = new DITBAI2Accounts();
            m_DataImportXClient = oDataImportXClient;
        }

        #region Class Properties

        /// <summary>
        /// m_GroupHeader public property
        /// </summary>
        public string GroupHeader
        {
            get { return m_GroupHeader; }
            set { m_GroupHeader = value; }
        }

        /// <summary>
        /// m_GroupFooter public property 
        /// </summary>
        public string GroupFooter
        {
            get { return m_GroupFooter; }
            set { m_GroupFooter = value; }
        }

        /// <summary>
        /// m_TotalRecordCount public property 
        /// </summary>
        public long TotalRecordCount
        {
            get { return m_TotalRecordCount; }
            set { m_TotalRecordCount = value; }
        }

        /// <summary>
        /// Ultimate Receiver Identification
        /// </summary>
        public string UltimateReceiverIdentification
        {
            get { return DITBAI2Common.GetParsedData(m_GroupHeader, 2); }
        }

        /// <summary>
        /// Originator Identification
        /// </summary>
        public string OriginatorIdentification
        {
            get { return DITBAI2Common.GetParsedData(m_GroupHeader, 3); }
        }

        /// <summary>
        /// Group Status 
        /// </summary>
        public string GroupStatus
        {
            get { return DITBAI2Common.GetParsedData(m_GroupHeader, 4); }
        }

        /// <summary>
        /// As-of-Date 
        /// </summary>
        public object AsofDate
        {
            get
            {
                DateTime dt;
                try
                {
                    dt = DateTime.ParseExact(DITBAI2Common.GetParsedData(m_GroupHeader, 5), "yyMMdd", null);
                }
                catch
                {
                    return DBNull.Value;
                }
                return dt;
            }
        }

        /// <summary>
        /// As-of-Time
        /// </summary>
        public string AsofTime
        {
            get { return DITBAI2Common.GetParsedData(m_GroupHeader, 6); }
        }

        /// <summary>
        /// Currency Code 
        /// </summary>
        public string CurrencyCode
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_GroupHeader, 7);
                if (string.IsNullOrEmpty(sResult))
                    return "USD";
                else
                    return sResult;
            }
        }

        /// <summary>
        /// As-of-Date Modifier
        /// </summary>
        public string AsofDateModifier
        {
            get { return DITBAI2Common.GetParsedData(m_GroupHeader, 8); }
        }

        /// <summary>
        ///Group Control Total
        /// </summary>
        public long GroupControlTotal
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_GroupFooter, 2);
                if (!string.IsNullOrEmpty(sResult))
                    return Convert.ToInt64(sResult);
                else
                    return 0;
            }
        }

        /// <summary>
        ///Number of Accounts 
        /// </summary>
        public long AccountCount
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_GroupFooter, 3);
                if (!string.IsNullOrEmpty(sResult))
                    return Convert.ToInt64(sResult);
                else
                    return 0;
            }
        }

        /// <summary>
        /// Record Count
        /// </summary>
        public long RecordCount
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_GroupFooter, 4);
                if (!string.IsNullOrEmpty(sResult))
                    return Convert.ToInt64(sResult);
                else
                    return 0;
            }
        }

        /// <summary>
        /// Read only Account Collection public property
        /// </summary>
        public DITBAI2Accounts Accounts
        {
            get { return m_Accounts; }
        }

        #endregion Class Properties

        /// <summary>
        /// Validate Group Data
        /// </summary>
        /// <returns></returns>
        public bool DoValidate()
        {

            bool bResult = true;
            try
            {
                //Validate Number of Records
                if (RecordCount != TotalRecordCount)
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: Incorrect Number of Group Records, Group Trailer Number Of Records: {0}. Calculated: {1}.", this.RecordCount, this.TotalRecordCount), this.GetType().Name, LogEventType.Error);
                    bResult = false;
                }

                //Validate Number of Accounts
                if (AccountCount != Accounts.Count)
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: Incorrect Number of Accounts Records, Group Trailer Number Of Accounts: {0}. Calculated: {1}.", AccountCount, this.Accounts.Count), this.GetType().Name, LogEventType.Error);
                    bResult = false;
                }

                //Validate Group Control Total                 
                long iAccountControlTotal = 0;
                iAccountControlTotal = Accounts.GetAccountControlTotal();
                if (GroupControlTotal != iAccountControlTotal)
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: Incorrect Group Control Total, Group Control Total : {0}. Account Control Total : {1}.", this.GroupControlTotal, iAccountControlTotal), this.GetType().Name, LogEventType.Error);
                    bResult = false;
                }

                if (!Accounts.DoValidate())
                    bResult = false;

            }
            catch (Exception ex)
            {
                bResult = false;
                m_DataImportXClient.DoLogEvent("Exception in DoValidate : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
            }
            return bResult;
        }

        /// <summary>
        /// Build the Batch nodes
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public bool BuildBatchNode(XmlDocument xDoc, ref XmlElement xRootElement, bool skipDebit)
        {
            bool bResult = false;
            int iBatchId = 0;
            try
            {
                foreach (DITBAI2Account objAccount in this.Accounts)
                {
                    objAccount.WireTransferBatchID = iBatchId;
                    var newBatch = objAccount.BuildBatchNode(xDoc, Convert.ToDateTime(this.AsofDate), skipDebit);
                    if (newBatch == null) continue;
                    xRootElement.AppendChild(newBatch);
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                bResult = false;
                m_DataImportXClient.DoLogEvent("Exception in BuildBatchNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);

            }

            return bResult;
        }

        /// <summary>
        /// Build document element
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement BuildDocumentNode(XmlDocument xDoc)
        {
            XmlElement xDocElement;
            try
            {
                Document objDoc = new Document();
                //objDoc.BatchSequence = this.SequenceNo;  //Does not allows null value, hence setting it to some value
                objDoc.SequenceWithinTransaction = null;
                objDoc.DocumentSequence = null;
                objDoc.DocumentDescriptor = null;
                xDocElement = objDoc.BuildXMLNode(xDoc);
                //xDocElement.AppendChild(BuildRemittanceDataRecordNode(xDoc)); To Do Add Remittance data from Transaction Records
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildDocumentNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xDocElement = null;
            }
            return xDocElement;
        }

    }

    /// <summary>
    /// ACH Batch Collection class
    /// </summary>
    internal class DITBAI2Groups : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITBAI2Groups this[int index]
        {
            get
            {
                return ((DITBAI2Groups)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }
        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITBAI2Group value)
        {
            return (List.Add(value));
        }
        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITBAI2Group value)
        {
            return (List.IndexOf(value));
        }
        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITBAI2Group value)
        {
            List.Insert(index, value);
        }
        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITBAI2Group value)
        {
            List.Remove(value);
        }
        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITBAI2Group value)
        {
            // If value is not of type ACHBatchObject, this will return false.
            return (List.Contains(value));
        }
        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.BAI2.Components.DITBAI2Group"))
                throw new ArgumentException("value must be of type DITBAI2Group.", "value");
        }

        /// <summary>
        /// Calculates the Group Control Total
        /// </summary>
        /// <returns></returns>
        public long GetGroupControlTotal()
        {
            long iGroupControlTotalSum = 0;
            foreach (DITBAI2Group oDITBAI2Group in this)
            {
                iGroupControlTotalSum = iGroupControlTotalSum + oDITBAI2Group.GroupControlTotal;
            }
            return iGroupControlTotalSum;
        }

        /// <summary>
        /// Validate Group Data
        /// </summary>
        /// <returns></returns>
        public bool DoValidate()
        {
            bool bResult = true;
            foreach (DITBAI2Group oDITBAI2Group in this)
            {
                if (!oDITBAI2Group.DoValidate())
                {
                    bResult = false;
                }
            }
            return bResult;
        }

    }
}
