﻿using System;
using System.Collections;
using System.Xml;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 03/18/2104
*
*
* Purpose: This class will hold the data for a Account detail and Account collection
*
* Modification History
* WI 136775 SAS 04/18/2014 
* Changes done to create canonical XML.
*
* WI 136895 SAS 05/07/2014
* Changes done to parse addenda data.
*
* WI 139926 SAS 05/07/2014
* Changes done to remove ItemDataRecords element and add GhostDocument Elements
*
* WI 139929 SAS 05/07/2014
* Changes done for duplicate file and payment detection.
*
* WI 144075, 144076 CMC 6/03/2014
* Handle payment source and payment type short name
*
* WI 147424 SAS 6/17/2014
* Adding Ghost Document to the Canonical XML
* 
* WI 158097 SAS 08/12/2014
* Changes done to add Remitter Name value in Payment element.
*
* WI 169845 SAS 10/16/2014 
* Changes done to replace GhostDocument with Document Element
* 
* WI 173023 SAS 10/21/2014 
* Changes done to set BatchSequence number
*******************************************************************************/


namespace DMP.BAI2.Components
{
    /// <summary>
    /// Account Collection Item class
    /// </summary>
    internal sealed class DITBAI2Account
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        /// <summary>
        /// Stores Account header detail 
        /// </summary>
        private string m_AccountHeaderData = string.Empty;
        /// <summary>
        /// Account Footer record
        /// </summary>
        private string m_AccountFooterData = string.Empty;
        /// <summary>
        /// Record Counts
        /// </summary>
        private long m_TotalRecordCount;
        /// <summary>
        ///  Wire Transfer Batch Id
        /// </summary>
        private int m_WireTransferBatchID;
        /// <summary>
        /// Transactions
        /// </summary>
        private DITBAI2Transactions m_Transactions = null;
        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="oDataImportXClient"></param>
        public DITBAI2Account(cDataImportXClientBase oDataImportXClient)
        {

            this.m_DataImportXClient = oDataImportXClient;
            this.m_Transactions = new DITBAI2Transactions();
            this.m_WireTransferBatchID = 0;
        }
        #endregion Class constructors

        #region Class Properties

        /// <summary>
        /// AccountHeaderData public property
        /// </summary>
        public string AccountHeaderData
        {
            get { return m_AccountHeaderData; }
            set { m_AccountHeaderData = value; }
        }

        /// <summary>
        /// AccountFooterData public property
        /// </summary>
        public string AccountFooterData
        {
            get { return m_AccountFooterData; }
            set { m_AccountFooterData = value; }
        }

        /// <summary>
        /// Record Code
        /// </summary>
        public string RecordCode
        {
            get { return DITBAI2Common.GetParsedData(m_AccountHeaderData, 1); }
        }

        /// <summary>
        /// Customer Account Number 
        /// </summary>
        public string CustomerAccountNumber
        {
            get { return DITBAI2Common.GetParsedData(m_AccountHeaderData, 2); }
        }

        /// <summary>
        /// Currency Code
        /// </summary>
        public string CurrencyCode
        {
            get { return DITBAI2Common.GetParsedData(m_AccountHeaderData, 3); }
        }

        /// <summary>
        ///Type Code 
        /// </summary>
        public string TypeCode
        {
            get { return DITBAI2Common.GetParsedData(m_AccountHeaderData, 4); }
        }

        /// <summary>
        ///Amount
        /// </summary>
        public string Amount
        {
            get { return DITBAI2Common.GetParsedData(m_AccountHeaderData, 5); }
        }

        ///<summary>
        ///Item Count 
        /// </summary>
        public string ItemCount
        {
            get { return DITBAI2Common.GetParsedData(m_AccountHeaderData, 6); }
        }

        //<summary>
        ///Item Count 
        /// </summary>
        public string FundsType
        {
            get { return DITBAI2Common.GetParsedData(m_AccountHeaderData, 6); }
        }


        /// <summary>
        /// m_TotalRecordCount public property 
        /// </summary>
        public long TotalRecordCount
        {
            get { return m_TotalRecordCount; }
            set { m_TotalRecordCount = value; }
        }

        /// <summary>
        /// Account Control Total
        /// </summary>
        public long AccountControlTotal
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_AccountFooterData, 2);
                if (!string.IsNullOrEmpty(sResult))
                    return Convert.ToInt64(sResult);
                else
                    return 0;
            }
        }

        /// <summary>
        /// Record Count
        /// </summary>
        public long RecordCount
        {
            get
            {
                string sResult = DITBAI2Common.GetParsedData(m_AccountFooterData, 3);
                if (!string.IsNullOrEmpty(sResult))
                    return Convert.ToInt64(sResult);
                else
                    return 0;
            }
        }

        /// <summary>
        /// m_Transactions public property
        /// </summary>
        public DITBAI2Transactions Transactions
        {
            get { return m_Transactions; }
        }



        /// <summary>
        /// Bank ID
        /// </summary>
        public int BankID
        {
            get { return 0; }
        }

        /// <summary>
        /// WorkGroupID
        /// </summary>
        public int WorkGroupID
        {
            get { return 0; }
        }

        /// <summary>
        /// BatchCueID 
        /// </summary>
        public int BatchCueID
        {
            get { return 0; }
        }

        /// <summary>
        ///m_WireTransferBatchID public property
        ///</summary>
        public int WireTransferBatchID
        {
            get { return m_WireTransferBatchID; }
            set { m_WireTransferBatchID = value; }
        }

        /// <summary>
        /// Batch Site Code
        /// </summary>
        public int BatchSiteCode
        {
            get { return 0; }
        }

        /// <summary>
        /// Batch number
        /// </summary>
        public int BatchNumber
        {
            get { return 0; }
        }

        /// <summary>
        /// Payment Type 
        /// </summary>
        public string PaymentType
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Batch Source 
        /// </summary>
        public string BatchSource
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Batch Track ID
        /// </summary>
        public Guid BatchTrackID
        {
            get { return Guid.NewGuid(); }
        }

        #endregion Class Properties

        #region Class Methods


        /// <summary>
        /// Validate Account Data
        /// </summary>
        /// <returns></returns>
        public bool DoValidate()
        {
            bool bResult = true;
            try
            {
                //Validate Number of Records
                if (RecordCount != TotalRecordCount)
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: Incorrect Number of Account Records, Account Trailer Number Of Records: {0}. Calculated: {1}.", this.RecordCount, this.TotalRecordCount), this.GetType().Name, LogEventType.Error);
                    bResult = false;
                }
            }
            catch (Exception ex)
            {
                bResult = false;
                m_DataImportXClient.DoLogEvent("Exception in DoValidate : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
            }
            return bResult;

        }

        /// <summary>
        ///  Build the Batch nodes
        /// WI 147424 :Adding Ghost Document to the Canonical XML
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="dtAsOfDate"></param>
        /// <param name="skipDebit"></param>
        /// <returns></returns>
        public XmlElement BuildBatchNode(XmlDocument xDoc, DateTime dtAsOfDate, bool skipDebit)
        {
            XmlElement xBatchElement;
            int iTransactionId = 0;
            int iTransactionSeqNo = 0;
            int iDocumentSequence = 0;
            int iDocSeqWithinTransaction = 0;
            int iBatchSequenceNo = 0;
            try
            {
                Batch objBatch = new Batch();
                objBatch.ProcessingDate = dtAsOfDate;
                objBatch.DepositDate = dtAsOfDate;
                objBatch.BankID = this.BankID;
                objBatch.ClientID = this.WorkGroupID;
                objBatch.BatchCueID = this.BatchCueID;
                objBatch.BatchID = this.WireTransferBatchID;
                objBatch.BatchDate = dtAsOfDate;
                objBatch.BatchSiteCode = this.BatchSiteCode;
                objBatch.BatchNumber = this.BatchNumber;
                objBatch.PaymentType = this.PaymentType;
                objBatch.BatchSource = this.BatchSource;
                objBatch.BatchTrackID = this.BatchTrackID;
                xBatchElement = objBatch.BuildXMLNode(xDoc);
                //Build Transaction Node
                foreach (DITBAI2Transaction objTransaction in this.Transactions)
                {
                    //We want to skip the whole transaction node when skipping debits
                    //to avoid lingering Document nodes from the debit transaction
                    if (!skipDebit || objTransaction.TransactionTypeCode != DITBAI2Common.OUTGOINGMONEYTRANSFER)
                    {
                        iDocSeqWithinTransaction = 0;
                        objTransaction.TransactionID = ++iTransactionId;
                        objTransaction.TransactionSeqNo = ++iTransactionSeqNo;
                        objTransaction.TransactionSignature = string.Concat(this.RecordCode, this.CustomerAccountNumber);
                        //Transaction node
                        XmlElement xTransactionElement = objTransaction.BuildTransactionNode(xDoc);

                        //Payment Node
                        objTransaction.PaymentBatchSequence = ++iBatchSequenceNo;
                        XmlElement xPaymentElement = objTransaction.BuildPaymentNode(xDoc);

                        //Document Remittance Data Record Data Record
                        objTransaction.DocSeqWithinTransaction = ++iDocSeqWithinTransaction;
                        objTransaction.DocumentBatchSequence = ++iBatchSequenceNo;
                        objTransaction.DocumentSequence = ++iDocumentSequence;
                        XmlElement xDocument = objTransaction.BuildDocumentNode(xDoc);

                        //Add Payment Node
                        xPaymentElement.SetAttribute("RemitterName", objTransaction.RemitterName);

                        xTransactionElement.AppendChild(xPaymentElement);
                        iBatchSequenceNo = objTransaction.DocumentBatchSequence;
                        //Add document element 
                        xTransactionElement.AppendChild(xDocument);
                        xBatchElement.AppendChild(xTransactionElement);
                    }
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in BuildBatchNode : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xBatchElement = null;
            }
            //this avoids a system error by skipping batches with no transactions
            return iTransactionId > 0 ? xBatchElement : null; 
        }
        #endregion Class Methods
    }

    /// <summary>
    /// ACH Detail Collection class
    /// </summary>
    internal class DITBAI2Accounts : CollectionBase
    {

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITBAI2Accounts this[int index]
        {
            get
            {
                return ((DITBAI2Accounts)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITBAI2Account value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITBAI2Account value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITBAI2Account value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITBAI2Account value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITBAI2Account value)
        {
            // If value is not of type ACHDetailObject, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }

        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }

        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }

        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.BAI2.Components.DITBAI2Account"))
                throw new ArgumentException("value must be of type DITBAI2Account.", "value");
        }

        /// <summary>
        /// Calculates the Account Control Total
        /// </summary>
        /// <returns></returns>
        public long GetAccountControlTotal()
        {
            long iAccountControlTotalSum = 0;
            foreach (DITBAI2Account oDITBAI2Account in this)
            {
                iAccountControlTotalSum = iAccountControlTotalSum + oDITBAI2Account.AccountControlTotal;
            }
            return iAccountControlTotalSum;
        }

        /// <summary>
        /// Validate Account Data
        /// </summary>
        /// <returns></returns>
        public bool DoValidate()
        {
            bool bResult = true;
            foreach (DITBAI2Account oDITBAI2Account in this)
            {
                if (!oDITBAI2Account.DoValidate())
                {
                    bResult = false;
                }
            }
            return bResult;
        }

    }




}