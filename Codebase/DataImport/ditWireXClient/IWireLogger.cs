using WFS.LTA.DataImport.DataImportXClientBase;

namespace DMP.BAI2.Components
{
    public interface IWireLogger
    {
        void DoLogEvent(string sMsg, string sSrc, LogEventType mstType, LogEventImportance msiImportance);
    }
}