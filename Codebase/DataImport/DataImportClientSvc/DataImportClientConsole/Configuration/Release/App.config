﻿<?xml version="1.0"?>
<configuration>

	<configSections>
		<!-- Used with ILMerge -->
		<section name="DataImportSettings" type="WFS.LTA.DataImport.DataImportClientSvcAPI.cDataImportSettingsConfigSection, DataImportClientConsole"/>
		<!-- End Used with ILMerge -->
		<!-- Used for development
		<section name="DataImportSettings" type="WFS.LTA.DataImport.DataImportClientSvcAPI.cDataImportSettingsConfigSection, DataImportClientSvcAPI"/>
		End Used for development -->
	</configSections>

	<DataImportSettings>
		<DataImportConfigSections>
		  <!--Client file drops-->
		  @@DitClientFile@@

		  <!--ImageRps Batch file drops-->
		  @@DitImageRpsBatchFile@@

		  <!--Ach file drops-->
		  @@DitAchFile@@

		  <!--Ach Service file drops-->
		  @@DitAchServiceFile@@

		  <!--Generic file drops-->
		  @@DitGenericFile@@

		  <!--Wire file drops-->
		  @@DitWireFile@@

    </DataImportConfigSections>
	</DataImportSettings>

	<appSettings>
		<add key="SiteKey" value="IPOnline" />
		<add key="ImageStorageModeSetting" value="0"/>
		<add key="ServerLocation" value="https://@@FILE_SERVER@@/DataImportServices/DataImportService.svc"/>
    <add key="AuditingService" value="https://@@FILE_SERVER@@/RecHubAuditingServices/auditingDit.svc"/>
		<add key="LogonName" value="@@CLIENT_LOGON_NAME@@"/>
		<add key="Password" value="@@CLIENT_PASSWORD@@"/>
		<add key="LogFilePath" value="@@LOG_FOLDER@@\{0:yyyyMMdd}_DITClientConsoleLog.txt"/>
		<add key="LogFileMaxSize" value="2048"/>
		<add key="LoggingDepth" value="@@DITCLIENTSVC_LOGGING_DEPTH@@"/>
		<add key="LogMaxArchiveFiles" value="100"/>
		<add key="ICONImportType" value="ImageRPS"/>
    <add key="ImageSplitLocation" value="Client"/>
    <!-- JPEG quality for grayscale and color images. Larger = higher quality, smaller = smaller files. Default = 60. Range is 1 to 100. -->
    <add key="ImageSplitJpegQuality" value="60"/>
    <!-- Maximum number of bytes of images to send to the server at one time. Default = 8388608 (8 MB). Max = 16777216 (16 MB). -->
    <add key="ImageTransferMaxBytes" value="8388608"/>
    <!-- Maximum number of images to send to the server at one time. Default = 100. Max = 100. -->
    <add key="ImageTransferMaxCount" value="100"/>
  </appSettings>

  <system.serviceModel>
    <serviceHostingEnvironment aspNetCompatibilityEnabled="false"/>
    <bindings>
      <ws2007HttpBinding>
        <binding name="DataImportServiceConfig"
                 sendTimeout="00:05:00"
                 maxReceivedMessageSize="67108864"
                 messageEncoding="Mtom">
          <readerQuotas maxDepth="64"
                        maxStringContentLength="2147483647"
                        maxArrayLength="2147483647"
                        maxBytesPerRead="4096"
                        maxNameTableCharCount="16384"/>
          <security mode="Transport">
            <transport clientCredentialType="None"/>
          </security>
        </binding>
        <binding name="DITAuditingServiceConfig" >
          <security mode="Transport">
            <transport clientCredentialType="None"/>
          </security>
        </binding>
      </ws2007HttpBinding>
    </bindings> 
  </system.serviceModel>

  <system.diagnostics>
    <sources>
      <!--  switchValue indicates what level of logging 
        Critical        - 	  Logs the following exceptions: OutOfMemoryException,ThreadAbortException,StackOverflowException,
                              ConfigurationErrorsException, SEHException,Application start errors,Failfast events,System hangs,
                              Poison messages: message traces that cause the application to fail
        Error           -     All exceptions are logged.
        Warning         -     Events logged: The application is receiving more requests than its throttling settings allow, 
                              the receiving queue is near its maximum configured capacity, timeout has exceeded.Credentials are rejected
        Information     -     Messages helpful for monitoring and diagnosing system status, measuring performance or profiling are generated.
        Verbose         -     Positive events are logged. Events that mark successful milestones.Low level events for both user code and servicing are emitted.
        ActivityTracing -     Flow events between processing activities and components.
        All             -     All events are logged.
        Off             -     No logs.
      -->
      <source name="System.ServiceModel" switchValue="Error,ActivityTracing" propagateActivity="true">
        <listeners>
          <add name="CircularTraceListener" />
        </listeners>
      </source>
      <source name="System.ServiceModel.MessageLogging">
        <listeners>
          <add name="messages"
               type="System.Diagnostics.XmlWriterTraceListener"
               initializeData="@@LOG_FOLDER@@\DitImportClientConsole_Messages.svclog" />
        </listeners>
      </source>
    </sources>
    <sharedListeners>
      <add name="CircularTraceListener" type="WFS.System.Tracing.CircularTraceListener, DataImportClientConsole, Version=1.0, Culture=neutral"
           initializeData="@@LOG_FOLDER@@\DitImportClientConsole_Traces.svclog" maxFileSizeKB="5000" />
    </sharedListeners>
    <trace autoflush="true" />
  </system.diagnostics>

  <startup>
		<supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.6"/>
	</startup>
</configuration>
