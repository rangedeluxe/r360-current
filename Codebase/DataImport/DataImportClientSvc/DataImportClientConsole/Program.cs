﻿using System;
using System.Reflection;
using Wfs.Logging;
using Wfs.Logging.NLog;
using WFS.LTA.Common;
using WFS.LTA.DataImport.DataImportClientSvcAPI;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     08/08/2012
*
* Purpose:
*
* Modification History
*
* Created
* CR 52267 JMC 08/08/2012
*   - Initial Version
* WI 126987 CMC 01/20/2014
*  - Updated to use 2.00 libraries.
*******************************************************************************/

namespace DataImportClientConsole
{
    internal class Program
    {
        private WfsLog Log { get; set; }

        private static void Main()
        {
            try
            {
                var prog = new Program();
                prog.Run();
            }
            catch (Exception ex)
            {
                // Run should have caught exceptions; this is just a last-ditch catch-all in case
                // the exception handler throws or something.
                Console.WriteLine("Exception occurred: " + ex.Message);
            }
        }

        private void Run()
        {
            cDITFileProcessor objProcessor;
            string sError;
            string strMsg;

            try
            {
                Log = new WfsLog();
                Log.Trace("DIT Client Console is starting");
	            GblUtil.OutputLog += XProc_LogEvent;
                GblUtil.InitializeClientOptions();
                GblUtil.WriteClientOptionsConfigurationLog();

                objProcessor = new cDITFileProcessor();

                strMsg = "Initializing...";
                GblUtil.OnOutputLogAndTrace(strMsg, Assembly.GetEntryAssembly().GetType().Name,
                    LTAMessageType.Information, LTAMessageImportance.Essential);

                if (objProcessor.Initialize(Log, out sError))
                {
                    strMsg = "Starting File Processing..";
                    GblUtil.OnOutputLogAndTrace(strMsg, Assembly.GetEntryAssembly().GetType().Name,
                        LTAMessageType.Information, LTAMessageImportance.Essential);
                }
                else
                {
                    GblUtil.OnOutputLogAndTrace(string.Format("Initialization Error: {0}", sError), "Initalize",
                        LTAMessageType.Information, LTAMessageImportance.Essential);
                    throw new Exception(sError);
                }

                PressAnyKey();
                Log.Trace("DIT Client Console is beginning graceful shutdown");
            }
            catch (Exception ex)
            {
                if (Log != null)
                    Log.logError(ex, "Exception occurred: " + ex.Message, "DataImportClientConsole");
                else
                    Console.WriteLine($"Exception occurred: {ex}");

                // Flush the console log before showing "Press any key"
                Log?.Trace("DIT Client Console is exiting ungracefully");
                Log?.Flush();

                PressAnyKey();
            }
            finally
            {
                Log?.Trace("DIT Client Console is exiting");
                Log?.Dispose();
            }
        }

        private void XProc_LogEvent(string msg, string src, LTAMessageType messageType,
            LTAMessageImportance messageImportance)
        {
            Log.logEvent(msg, src, messageType, messageImportance);
        }

        private static void PressAnyKey()
        {
            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
