﻿using System;
using WFS.LTA.Common;

/******************************************************************************
** Wausau
** Copyright ? 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013-2014.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Chris Colombo
* Date:     05/07/2014
*
* Purpose:  This class handles front/back parsing of Images.
*
* Modification History
* Created
* WI 140511 CMC 05/07/2014
*   - Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public static class ImageInfo
    {
        public const int DefaultImageSplitJpegQuality = 60;

        public static IImageInfo Create(string imagePathParam, int imageSplitJpegQuality = DefaultImageSplitJpegQuality)
        {
            try
            {
                // Try WPF first, since it's much faster, and since it's relatively rare
                // to have an image it can't load successfully.
                return new WpfImageInfo(imagePathParam, imageSplitJpegQuality);
            }
            catch (Exception ex)
            {
                // Fall back on the old System.Drawing implementation.
                var retryMessage = string.Format(
                    "Image load: Unable to load image '{0}' with WPF: {1}. Retrying with System.Drawing.",
                    imagePathParam, ex.Message.Replace("corrupted", "non-standard"));
                GblUtil.OnOutputLogAndTrace(retryMessage, "ImageInfo.Create",
                    LTAMessageType.Information, LTAMessageImportance.Essential);
                var result = new SystemDrawingImageInfo(imagePathParam);
                var infoMessage = string.Format(
                    "Image load: Image '{0}' was reloaded successfully with System.Drawing",
                    imagePathParam);
                GblUtil.OnOutputLogAndTrace(infoMessage, "ImageInfo.Create",
                    LTAMessageType.Information, LTAMessageImportance.Verbose);
                return result;
            }
        }
    }
}
