﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/13/2012
*
* Purpose:  This class keeps the Batch and Item field lists together with the date they 
*           where last updated.  An instance of this class is maintained for each Batch 
*           Sourse
*
* Modification History
* Created
*  CR 52267 CEJ 13/06/2012
 *   - DIT Client
*
*******************************************************************************/


namespace WFS.LTA.DataImport.DataImportClientSvcAPI
    {
    public class cDimFieldLists
        {
        private DateTime? _dttLastUpdated = null;
        private List<string> _lstBatchFieldList = new List<string>();
        private List<string> _lstItemFieldList = new List<string>();

        public List<string> ItemFieldList
            {
            get
                {
                return _lstItemFieldList;
                }
            }

        public List<string> BatchFieldList
            {
            get 
                { 
                return _lstBatchFieldList; 
                }
            }

        public DateTime? LastUpdated
            {
            get 
                { 
                return _dttLastUpdated; 
                }
            set 
                { 
                _dttLastUpdated = value; 
                }
            }

        }
    }
