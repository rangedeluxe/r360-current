﻿using System;
using System.Collections.Generic;
using System.Configuration;
using WFS.LTA.Common;
using WFS.LTA.Common.FileCollector;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 52267 JMC 08/09/2012 
*   -Initial version.
* WI 126987 CMC 01/20/2014
*  - Updated to use 2.00 libraries.
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientSvcAPI {

    internal class cProcessOptionsList : List<_SourcePathBase> {

        public cProcessOptionsList() {
            GetClientProcessOptions();
        }

        private void GetClientProcessOptions() {

            cInboxPathData objProcessOptions;

            try
            {
                cDataImportSettingsConfigSection arClientProcess = (cDataImportSettingsConfigSection)ConfigurationManager.GetSection("DataImportSettings");

                if (arClientProcess.ProcessItems.Count > 0)
                {
                    foreach (ProcessElement xproc in arClientProcess.ProcessItems)
                    {
                        objProcessOptions = new cInboxPathData(xproc);
                        base.Add(objProcessOptions);
                    }
                }
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace("Unable to get find section  DataImportSettings in ini .", "GetClientProcessOptions",
                                 ex);
            }
        }
    }
}
