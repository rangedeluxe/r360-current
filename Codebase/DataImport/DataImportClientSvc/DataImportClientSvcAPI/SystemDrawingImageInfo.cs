﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using WFS.LTA.Common;

/******************************************************************************
** Wausau
** Copyright ? 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013-2014.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Chris Colombo
* Date:     05/07/2014
*
* Purpose:  This class handles front/back parsing of Images.
*
* Modification History
* Created
* WI 140511 CMC 05/07/2014
*   - Created
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class SystemDrawingImageInfo : IImageInfo
    {
        public SystemDrawingImageInfo(string imagePathParam)
        {
            if (string.IsNullOrEmpty(imagePathParam))
                throw new ArgumentException("Must provide the image path in order to do the parse.");

            ImagePath = imagePathParam;
            var fileSize = new FileInfo(imagePathParam).Length;
            if (fileSize > 0)
            {
                Image = Image.FromFile(imagePathParam);
                NumberOfPages = Image.GetFrameCount(FrameDimension.Page);
            }
        }
        public void Dispose()
        {
            if (Image != null)
            {
                Image.Dispose();
            }
        }

        public int NumberOfPages { get; private set; }
        private Image Image { get; set; }
        private string ImagePath { get; }

        public Stream GetFrontPageStream()
        {
            if (NumberOfPages > 0)
                return GetPageStream(0);

            return new MemoryStream();
        }

        public Stream GetBackPageStream()
        {
            if (NumberOfPages > 1)
            {
                try
                {
                    return GetPageStream(1);
                }
                catch (ExternalException ex)
                {
                    // If GDI+ blows up trying to read the page,
                    // return an empty stream.
                    var message = "Unable to read back page; treating image as single-page. " +
                        "Image filename: '" + ImagePath + "'; error message: " + ex.Message;
                    GblUtil.OnOutputLogAndTrace(message, "GetBackPageStream",
                        LTAMessageType.Warning, LTAMessageImportance.Essential);
                    return new MemoryStream();
                }
            }

            return GetFrontPageStream();
        }

        private ImageCodecInfo GetCodecInfo(ImageFormat format)
        {
            ImageCodecInfo[] codecs;
            ImageCodecInfo codecInfo = null;
            Guid clsid;

            clsid = format.Guid;
            codecs = ImageCodecInfo.GetImageEncoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (clsid.Equals(codec.FormatID))
                {
                    codecInfo = codec;
                    break;
                }
            }
            return codecInfo;
        }

        private Stream GetPageStream(int page)
        {
            Guid objGuid = Image.FrameDimensionsList[0];
            FrameDimension objDimension = new FrameDimension(objGuid);
            Image.SelectActiveFrame(objDimension, page);

            MemoryStream ms = new MemoryStream();
            PixelFormat myformat = Image.PixelFormat;
            Encoder enc = Encoder.Compression;
            EncoderParameters ep = new EncoderParameters(1);
            if (myformat == PixelFormat.Format1bppIndexed)
            {
                ep.Param[0] = new EncoderParameter(enc, (long)EncoderValue.CompressionCCITT4);
                Image.Save(ms, GetCodecInfo(ImageFormat.Tiff), ep);
            }
            else
                Image.Save(ms, ImageFormat.Jpeg);

            ms.Position = 0;
            return ms;
        }
    }
}