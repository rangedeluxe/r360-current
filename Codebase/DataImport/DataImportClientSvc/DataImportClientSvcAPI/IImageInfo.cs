﻿using System;
using System.IO;

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public interface IImageInfo : IDisposable
    {
        int NumberOfPages { get; }

        Stream GetBackPageStream();
        Stream GetFrontPageStream();
    }
}