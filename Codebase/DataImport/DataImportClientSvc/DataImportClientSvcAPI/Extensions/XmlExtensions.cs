﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.Extensions
{
    public static class XmlExtensions
    {
        public static bool IsSameBatch(this XElement batchElement, ResponseItem responseItem)
        {
            if (batchElement == null)
                throw new ArgumentNullException();

            if (responseItem == null)
                throw new ArgumentNullException();

            var batchTrackingIdAttribute = batchElement.Attribute("BatchTrackingID") != null
                ? batchElement.Attribute("BatchTrackingID").Value
                : string.Empty;

            Guid batchTrackingId;
            Guid.TryParse(batchTrackingIdAttribute, out batchTrackingId);

            if (batchTrackingId == responseItem.BatchTrackingId)
                return true;
            else
                return false;
        }
    }
}
