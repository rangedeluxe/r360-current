﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.Extensions
{
    public static class StringExtensions
    {
        public static IEnumerable<ResponseItem> ToResponseItemList(this string xml)
        {
            if (string.IsNullOrEmpty(xml))
                throw new ArgumentNullException();

            var response = new List<ResponseItem>();
            var xElement = XElement.Parse(xml);
            var results = xElement.DescendantsAndSelf("Results");//grab the results
            foreach (var result in results)
            {
                var responseTrackingIdAttribute = result.Parent.Parent.Parent.Attribute("ResponseTrackingID");
                var sourceTrackingIdAttribute = result.Parent.Parent.Attribute("SourceTrackingID");
                var auditDateKeyAttribute = result.Parent.Attribute("AuditDateKey");
                var errorMessageAttribute = result.Parent.Element("ErrorMessage");
                var bankIdAttribute = result.Parent.Attribute("BankID");
                var clientIdAttribute = result.Parent.Attribute("ClientID");
                var batchTrackingIdAttribute = result.Parent.Attribute("BatchTrackingID");

                var responseItem = new ResponseItem
                {
                    ResponseTrackingId = responseTrackingIdAttribute == null ? Guid.Empty : new Guid(responseTrackingIdAttribute.Value),
                    AuditDateKey = int.Parse(auditDateKeyAttribute != null ? auditDateKeyAttribute.Value : "0"),
                    SourceTrackingId = sourceTrackingIdAttribute == null ? Guid.Empty : new Guid(sourceTrackingIdAttribute.Value),
                    Message = errorMessageAttribute == null ? string.Empty : errorMessageAttribute.Value,
                    IsSuccessful = string.Equals(result.Value, "success", StringComparison.InvariantCultureIgnoreCase) ? true : false,
                    bankId = int.Parse(bankIdAttribute != null ? bankIdAttribute.Value : "0"),
                    clientId = int.Parse(clientIdAttribute != null ? clientIdAttribute.Value : "0"),
                    BatchTrackingId = batchTrackingIdAttribute == null ? Guid.Empty : new Guid(batchTrackingIdAttribute.Value)
                };

                response.Add(responseItem);
            }
            return response;
        }
    }
}
