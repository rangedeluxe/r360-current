﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using WFS.LTA.Common;
using WFS.LTA.DataImport.DataImportServiceClient;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public class ImageUploadWorker : IImageConsumer
    {
        public ImageUploadWorker(IBatchImageSender batchImageSender)
        {
            BatchImageSender = batchImageSender;
            ItemsToUpload = new List<UploadRequest>();
        }

        private IBatchImageSender BatchImageSender { get; }
        private int ImageTransferMaxBytes
        {
            get { return GblUtil.ClientOptions.ImageTransferMaxBytes; }
        }
        private int ImageTransferMaxCount
        {
            get { return GblUtil.ClientOptions.ImageTransferMaxCount; }
        }
        private List<UploadRequest> ItemsToUpload { get; set; }

        public void Add(string imagePath, UploadRequest upiImage)
        {
            ItemsToUpload.Add(upiImage);

            var initialBytes = BitConverter.ToString(upiImage.ImageData, 0, Math.Min(4, upiImage.ImageData.Length));
            var message = string.Format(
                "Image queued for upload: path='{0}', {1}, {2} bytes ({3}...), BatchSequence={4}",
                imagePath, upiImage.ImageInfo.IsBack ? "Back" : "Front",
                upiImage.ImageData.Length, initialBytes, upiImage.ImageInfo.BatchSequence);
            GblUtil.OnOutputLogAndTrace(message, "ImageUploadWorker.Add",
                LTAMessageType.Information, LTAMessageImportance.Debug);

            if (ItemsToUpload.Count >= ImageTransferMaxCount ||
                ItemsToUpload.Sum(item => item.ImageData.LongLength) >= ImageTransferMaxBytes)
            {
                Submit();
            }
        }
        public void Flush()
        {
            Submit();
        }
        private void Submit()
        {
            if (!ItemsToUpload.Any())
            {
                return;
            }

            cRetryCounter rtyCounter = GblUtil.GetRetryCounter();
            // Our data is all from a single batch, so it doesn't matter which image we pull
            // batch-related IDs from
            var contract = ItemsToUpload.First().ImageInfo;
            var imageCount = ItemsToUpload.Count;
            var imageSize = ItemsToUpload.Sum(item => item.ImageData.LongLength);
            var blockInfo = string.Format(
                "{0} image{1}, {2} total bytes, BankID={3}, WorkgroupID={4}, SourceBatchID={5}",
                imageCount, imageCount == 1 ? "" : "s", imageSize,
                contract.BankID, contract.LockboxID, contract.SourceBatchID);

            while (rtyCounter.CycleRetry())
            {
                try
                {
                    var stopwatch = Stopwatch.StartNew();

                    BatchImageSender.SendBatchImages(ItemsToUpload);
                    ItemsToUpload.Clear();

                    rtyCounter.HasSucceeded = true;
                    GblUtil.OnOutputLogAndTrace(
                        "Images uploaded: " + blockInfo + ". Upload duration: " + stopwatch.Elapsed,
                        "ImageUploadWorker.Submit",
                        LTAMessageType.Information, LTAMessageImportance.Verbose);
                }
                catch (Exception ex)
                {
                    string sErrorMessage = string.Format(
                        "Failed to upload images: {0}: {1}",
                        blockInfo, ex.Message);
                    if (rtyCounter.ShouldRetry)
                    {
                        GblUtil.OnOutputLogAndTrace(sErrorMessage + ", Retrying",
                                "cDITUploadManager(UploadImage)", LTAMessageType.Error,
                                LTAMessageImportance.Verbose);
                    }
                    else
                    {
                        throw new Exception(sErrorMessage);
                    }
                }
            }
        }
    }
}