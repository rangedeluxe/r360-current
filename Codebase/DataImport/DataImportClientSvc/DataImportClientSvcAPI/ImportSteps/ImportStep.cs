﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public abstract class ImportStep : IImportStep
    {
        protected ImportStep(IPendingService pendingService, ImportStepRequest request)
        {
            PendingService = pendingService;
            ImportStepRequest = request;
        }

        protected IPendingService PendingService { get; private set; }
        protected ImportStepRequest ImportStepRequest { get; private set; }

        public PendingItem GetPendingFile(Guid sourceTrackingId)
        {
            return PendingService
                .GetPending()
                .Where(pendingItem => pendingItem.SourceTrackingId == sourceTrackingId)
                .FirstOrDefault();
        }

        private void Validate(ImportStepRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("Must provide a request object.");

            if (request.ResponseItem == null)
                throw new ArgumentNullException("Must provide a response item object within the request.");

            request.ResponseItem.Context = request.ResponseItem.Context ?? new ResponseContext();
            request.ResponseItem.Context.Messages = request.ResponseItem.Context.Messages ?? new List<string>();
        }

        protected string ToImageNotFoundMessage(ImageInfoContract imageInfo)
        {
            return
                new StringBuilder()
                    .Append(string.Format("Image File {0} did not import from Data Import Toolkit.  ", imageInfo.ImagePath))
                    .Append(string.Format("Date = {0}, ", imageInfo.ProcessingDateKey))
                    .Append(string.Format("Workgroup ID = {0}, ", imageInfo.LockboxID))
                    .Append(string.Format("Bank ID = {0}, ", imageInfo.BankID))
                    .Append(string.Format("Payment Source = {0}, ", imageInfo.BatchSourceShortName))
                    .Append(string.Format("Batch ID = {0}, ", imageInfo.SourceBatchID))
                    .Append(string.Format("Batch Number = {0}.", imageInfo.BatchNumber))
                    .ToString();
        }

        protected abstract ImportStepResponse Execute(ImportStepRequest request);
        public ImportStepResponse Execute()
        {
            Validate(ImportStepRequest);
            return Execute(ImportStepRequest);
        }
        
    }
}
