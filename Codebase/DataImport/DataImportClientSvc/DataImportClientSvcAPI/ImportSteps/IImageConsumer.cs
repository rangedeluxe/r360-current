using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public interface IImageConsumer
    {
        void Add(string imagePath, UploadRequest upiImage);
    }
}