﻿using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public interface IImageLoader
    {
        IImageInfo LoadImageInfo(ImageInfoContract imageInfoContract);
        byte[] LoadImageRawBytes(string path);
    }
}