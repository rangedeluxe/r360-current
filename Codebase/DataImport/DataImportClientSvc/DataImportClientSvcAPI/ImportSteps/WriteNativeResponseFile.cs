﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using WFS.LTA.Common.FileCollector;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public class WriteNativeResponseFile : ImportStep
    {
        public WriteNativeResponseFile(IPendingService pendingService, ImportStepRequest request)
            : base(pendingService, request)
        {
        }

        protected override ImportStepResponse Execute(ImportStepRequest request)
        {
            var file = base.GetPendingFile(request.ResponseItem.SourceTrackingId);
            if (file == null)
                return new ImportStepResponse
                {
                    IsSuccessful = false,
                    Messages = new List<string>
                    {
                        string.Format("Unable to write out native response file. The import step could not find a pending item for source tracking id: {0}", request.ResponseItem.SourceTrackingId)
                    }
                };

            var imageImportSteps = request.ResponseItem.ImportSteps.Where(step => step is IImageImportStep);
            request.ResponseItem.Context.UnArchivedImages = imageImportSteps.FirstOrDefault() == null ? 0 : (imageImportSteps.FirstOrDefault() as IImageImportStep).GetUnsentImages();
            request.ResponseItem.Context.ArchivedImages = imageImportSteps.FirstOrDefault() == null ? 0 : (imageImportSteps.FirstOrDefault() as IImageImportStep).GetSentImages();
            request.ResponseItem.Context.ImageUploadWasSuccessful = imageImportSteps.FirstOrDefault() == null ? true : (imageImportSteps.FirstOrDefault() as IImageImportStep).ImageImportSuccessful;

            request.ResponseItem.Context.InputFile.TrackCompletedBatch(request.ResponseItem.BatchTrackingId);
            WriteResponse(request.ResponseItem.Context);
            return new ImportStepResponse { IsSuccessful = true };

        }

        private bool WriteResponse(ResponseContext responseContext)
        {
            if (responseContext == null)
                throw new ArgumentNullException("ResponseContext must not be null");

            if (responseContext.InputFile == null)
                throw new ArgumentNullException("ResponseContext.InputFile must not be null");

            bool bAns = false;

            responseContext.ImportWasSuccessful = responseContext.DataLoadWasSuccessful && responseContext.ImageUploadWasSuccessful;

            try
            {
                string responsefolder = string.Empty;

                if (responseContext.ImportWasSuccessful)
                {
                    responsefolder = responseContext.InputFile.SourcePathData.ResponseFolder;
                }
                else
                {
                    responsefolder = responseContext.InputFile.SourcePathData.ResponseErrorFolder;
                }

                //only if all batches in the file are completed do we want to
                //create the response file and delete from the PendingResponse folder
                if (responseContext.InputFile.AllBatchesAreCompleted())
                {
                    using (StreamWriter stwResponseStream = new StreamWriter(
                        Path.Combine(responsefolder,
                        GetResponseFileName(responseContext.InputFile.OriginalInputFileName, responseContext.ImportWasSuccessful))))
                    {
                        DeleteFromPendingResponse(responseContext.InputFile);

                        responseContext.InputFile.WriteLineToResult(string.Format("Finished at {0}", DateTime.Now));
                        stwResponseStream.WriteLine(responseContext.InputFile.GetProcessResult());
                        bAns = true;
                    }
                }
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace(string.Format(
                          "Unable to write response file for processing ({0}): {1}", responseContext.InputFile.FileData.Name,
                          ex.Message), "WriteResponse", ex);
                bAns = false;
            }

            return bAns;
        }
        private string GetResponseFileName(string sOriginalFile, bool bSuccessful)
        {
            return string.Format("{0:yyyyMMdd_HHmmss}_{1}_{2}_{3}.Response",
                DateTime.Now, Path.GetFileNameWithoutExtension(sOriginalFile), bSuccessful ? "Success" : "Error",
                Guid.NewGuid().ToString());
        }

        private bool DeleteFromPendingResponse(cInputFile ipfItem)
        {

            bool bolRetVal;
            try
            {
                XAttribute sourceTrackingIdAttribute =
                    XElement
                        .Parse(ipfItem.SubmittedXML)//create the XElement object
                        .Attributes()//gather the attributes
                        .Where(x => string.Equals(x.Name.ToString(), "sourcetrackingid", StringComparison.InvariantCultureIgnoreCase))//find the source tracking id
                        .FirstOrDefault();

                if (sourceTrackingIdAttribute == null)
                    throw new ArgumentNullException("Unable to find the SourceTrackingID attribute in the response.");

                const string InProcessText = "inprocess";
                string[] separators = new string[] { InProcessText };
                var fileNameParts = ipfItem.FileData.Name.Split(separators, StringSplitOptions.None);//separate the file name into parts before and after the 'inprocess' tag
                var fileName = fileNameParts.FirstOrDefault() != null ? string.Format("{0}{1}", fileNameParts.First(), InProcessText) : ipfItem.FileData.Name;
                string pendingResponseFileName =
                    Path.Combine(ipfItem.SourcePathData.PendingResponseFolder, string.Format("{0}.-{1}-.pendingResponse", fileName, sourceTrackingIdAttribute.Value));
                DeleteFile(pendingResponseFileName);
                bolRetVal = true;
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace("An unexpected exception occurred in DeleteFromPendingResponse()", this.GetType().Name, ex);
                bolRetVal = false;
            }
            return (bolRetVal);
        }

        private bool DeleteFile(string fromFilePath)
        {

            bool bolContinue = true;
            int intRetryCount = 0;
            bool bolRetVal = false;

            while (bolContinue)
            {
                try
                {
                    File.Delete(fromFilePath);
                    bolRetVal = true;
                    bolContinue = false;
                }
                catch (Exception ex)
                {

                    if (intRetryCount < 3)
                    {
                        intRetryCount++;
                    }
                    else
                    {
                        bolContinue = false;
                        throw ex;
                    }
                }
            }
            return (bolRetVal);
        }
    }
}
