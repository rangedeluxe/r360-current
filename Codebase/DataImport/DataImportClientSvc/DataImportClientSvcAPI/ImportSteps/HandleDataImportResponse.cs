﻿using System.Collections.Generic;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public class HandleDataImportResponse : ImportStep
    {
        public HandleDataImportResponse(IPendingService pendingService, ImportStepRequest request) :
            base(pendingService, request)
        {
        }

        protected override ImportStepResponse Execute(ImportStepRequest request)
        {
            var file = GetPendingFile(request.ResponseItem.SourceTrackingId);
            if (file == null)
                return new ImportStepResponse
                {
                    IsSuccessful = false,
                    Messages = new List<string>
                    {
                        string.Format("Unable to handle the import response.  The import step could not find a pending item for source tracking id: {0}", request.ResponseItem.SourceTrackingId)
                    }
                };

            file.PendingFile.WriteLineToResult(string.Format("Response Status: {0}", request.ResponseItem.IsSuccessful ? "Success" : "Failed"));
            file.PendingFile.WriteLineToResult(string.Format("Response Tracking ID: {0}", request.ResponseItem.ResponseTrackingId));
            file.PendingFile.WriteLineToResult(string.Format("Response Message: {0}", request.ResponseItem.Message));
            file.PendingFile.WriteLineToResult(string.Format("{0}", request.ResponseItem.IsSuccessful ? "Data Successfully Imported Into R360" : "Data Was Not Imported Into R360"));
            file.PendingFile.WriteLineToResult(string.Format("For more detail, please reference server Source Tracking ID {0}", request.ResponseItem.SourceTrackingId));

            request.ResponseItem.Context.InputFile = file.PendingFile;
            request.ResponseItem.Context.SourceTrackingId = request.ResponseItem.SourceTrackingId;
            request.ResponseItem.Context.ResponseTrackingId = request.ResponseItem.ResponseTrackingId;
            request.ResponseItem.Context.DataLoadWasSuccessful = request.ResponseItem.IsSuccessful ? true : false;

            return new ImportStepResponse
            {
                IsSuccessful = true
            };
        }
    }
}
