﻿using System;
using System.Linq;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;
using WFS.LTA.DataImport.DataImportServiceClient;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public class UpdateImportStatus : ImportStep
    {
        private readonly DataImportConnect serviceProxy;

        public UpdateImportStatus(IPendingService pendingService, DataImportConnect serviceProxy, ImportStepRequest request)
            : base(pendingService, request)
        {
            this.serviceProxy = serviceProxy;
        }

        protected override ImportStepResponse Execute(ImportStepRequest request)
        {
            var pendingFile = GetPendingFile(request.ResponseItem.SourceTrackingId);
            if (pendingFile == null)
            {
                this.serviceProxy.UpdateStatus(new UpdateStatusContext
                {
                    ClientKnowsAboutRecord = false,
                    ResponseTrackingId = request.ResponseItem.ResponseTrackingId,
                    SourceTrackingId = request.ResponseItem.SourceTrackingId,
                    EntityTrackingId = request.ResponseItem.BatchTrackingId == Guid.Empty 
                        ? new Guid?() 
                        : request.ResponseItem.BatchTrackingId
                });
            }

            else
            {
                this.serviceProxy.UpdateStatus(new UpdateStatusContext
                {
                    ClientKnowsAboutRecord = true,
                    ResponseTrackingId = request.ResponseItem.ResponseTrackingId,
                    SourceTrackingId = request.ResponseItem.SourceTrackingId,
                    EntityTrackingId = request.ResponseItem.BatchTrackingId == Guid.Empty 
                        ? new Guid?() 
                        : request.ResponseItem.BatchTrackingId
                });
                if (request.ResponseItem.Context.InputFile.AllBatchesAreCompleted())
                    base.PendingService.SetPending(base.PendingService.GetPending().Where(pendingItem => pendingItem.SourceTrackingId != pendingFile.SourceTrackingId));
            }

            return new ImportStepResponse { IsSuccessful = true };
        }
    }
}
