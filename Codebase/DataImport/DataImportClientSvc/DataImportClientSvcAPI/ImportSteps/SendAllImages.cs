﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WFS.LTA.Common;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.LTA.DataImport.DataImportServiceClient;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public class SendAllImages : ImportStep, IImageImportStep
    {
        private readonly DataImportConnect serviceProxy;
        private int sentImages = 0;
        private int unsentImages = 0;

        public SendAllImages(IPendingService pendingService, DataImportConnect proxy, ImportStepRequest request)
            : base(pendingService, request)
        {
            this.serviceProxy = proxy;
        }
        protected override ImportStepResponse Execute(ImportStepRequest request)
        {
            var file = base.GetPendingFile(request.ResponseItem.SourceTrackingId);
            if (file == null)
                return new ImportStepResponse
                {
                    IsSuccessful = false,
                    Messages = new List<string>
                    {
                        string.Format("Unable to upload images.  The import step could not find pending item for source tracking id: {0}", request.ResponseItem.SourceTrackingId)
                    }
                };

            var completeImages = cCanonXMLTools.CollectImageData(file.PendingFile.SubmittedXML, request.ResponseItem);

            try
            {
                var images = new List<ImageUploadRequest>();
                var size = 0;

                file.PendingFile.WriteLineToResult(string.Format("Preparing to upload {0} images in chunks of {1} bytes", completeImages.Count, GetImageTransferMaxBytes()));
                GblUtil.OnOutputLogAndTrace(string.Format("Starting image upload for source tracking id {0} at {1}", request.ResponseItem.SourceTrackingId, DateTime.Now), "SendAllImages.Execute()", LTAMessageType.Information, LTAMessageImportance.Essential);
                sentImages = 0;
                unsentImages = 0;
                file.PendingFile.WriteLineToResult(string.Format("Starting image upload at {0}", DateTime.Now));

                foreach (var image in completeImages)
                {
                    if (File.Exists(image.ImagePath))
                    {
                        var fileName = new FileInfo(image.ImagePath);

                        using (Stream stream = new FileStream(image.ImagePath, FileMode.Open))
                        {
                            var bytes = stream.ToBytes(); //convert stream to bytes
                            size = size + bytes.Length;
                            if (SendWhatWeHave(size, images.Count))
                            {
                                var response = Submit(new MultiImageRequest { Images = images, BankId = image.BankID, BatchId = image.SourceBatchID }); //send what you have
                                SetCounts(response.ImageUploadResults.ToList());
                                size = 0; //reset
                                images = null;
                                images = new List<ImageUploadRequest>();

                            }
                            images.Add(new ImageUploadRequest { ImageInfo = image, ImageFileName = image.ImagePath, ImageData = bytes });

                        }
                    }
                    else
                    {
                        GblUtil.OnOutputLogAndTrace(string.Format("Can not find image file ({0})", image.ImagePath),
                            "cDITFileProcessor(UploadImages)", LTAMessageType.Error,
                            LTAMessageImportance.Essential);

                        new AlertManager(serviceProxy).LogAlert(image);
                        request.ResponseItem.Context.InputFile.WriteLineToResult(base.ToImageNotFoundMessage(image));
                        request.ResponseItem.Context.Messages.Add(base.ToImageNotFoundMessage(image));
                        unsentImages++;
                    }
                }

                if (images.FirstOrDefault() != null)
                {
                    var response = Submit(new MultiImageRequest 
                    { 
                        Images = images, BankId = images.FirstOrDefault().ImageInfo.BankID, BatchId = images.FirstOrDefault().ImageInfo.SourceBatchID 
                    });
                    SetCounts(response.ImageUploadResults.ToList());
                }
                    

                file.PendingFile.WriteLineToResult(string.Format("Sent images {0}", sentImages));
                file.PendingFile.WriteLineToResult(string.Format("Unsent images {0}", unsentImages));

                file.PendingFile.WriteLineToResult(string.Format("Finished image upload at {0}", DateTime.Now));
                GblUtil.OnOutputLogAndTrace(string.Format("Finished image upload at {0}", DateTime.Now), "SendAllImages.Execute()", LTAMessageType.Information, LTAMessageImportance.Essential);


                request.ResponseItem.Context.ArchivedImages = sentImages;
                request.ResponseItem.Context.UnArchivedImages = unsentImages;
                ImageImportSuccessful = sentImages == completeImages.Count;
                return new ImportStepResponse { IsSuccessful = true };
            }
            catch
            {
                unsentImages = completeImages.Count;
                file.PendingFile.WriteLineToResult("The image upload encountered errors.");

                file.PendingFile.WriteLineToResult(string.Format("Sent images {0}", sentImages));
                file.PendingFile.WriteLineToResult(string.Format("Unsent images {0}", unsentImages));
                request.ResponseItem.Context.Messages.Add("The image upload encountered errors.");
                throw;
            }
        }

        private int GetImageTransferMaxBytes()
        {
            return GblUtil.ClientOptions.ImageTransferMaxBytes;
        }
        private int GetImageTransferMaxCount()
        {
            return GblUtil.ClientOptions.ImageTransferMaxCount;
        }
        private bool SendWhatWeHave(int imageBytesSize, int imageCount)
        {
            if (imageBytesSize >= GetImageTransferMaxBytes())//check to see if the bytes for this image will result in the total exceeding the max threshold
                return true;

            if (imageCount >= GetImageTransferMaxCount())//let's limit the number of images we send at one time to avoid upper limits and timeouts
                return true;

            return false;
        }

        private void SetCounts(IList<ImageUploadResponse> responses)
        {
            if (responses == null)
                throw new ArgumentNullException("Must provide responses");

            sentImages = sentImages + responses.Where(result => result.IsSuccessfullyWritten).Count();
            unsentImages = unsentImages + responses.Where(result => !result.IsSuccessfullyWritten).Count();
        }

        private MultiImageResponse Submit(MultiImageRequest request)
        {
            MultiImageResponse response = null;
            cRetryCounter rtyCounter = GblUtil.GetRetryCounter();

            while (rtyCounter.CycleRetry())
            {
                try
                {
                    response = serviceProxy.MultiImageUpload(request);
                    if (response.ImageUploadResults.Where(image => !image.IsSuccessfullyWritten).FirstOrDefault() == null)//everything was successfully written  
                    {
                        rtyCounter.HasSucceeded = true;
                        GblUtil.OnOutputLogAndTrace(string.Format("{0} Images successfully uploaded to server for bank id {1}, batch id {2}.", request.Images.Count(), request.BankId, request.BatchId), "SendAllImages.Submit", LTAMessageType.Information,
                                LTAMessageImportance.Verbose);
                    }
                    else
                    {
                        GblUtil.OnOutputLogAndTrace
                            (string.Format("Failed to upload batch {0} images:{1}", request.BatchId, rtyCounter.ShouldRetry ? ", Retrying" : ""),
                                "SendAllImages.Submit", LTAMessageType.Error, LTAMessageImportance.Verbose);
                    }
                }
                catch (Exception ex)
                {
                    string sErrorMessage = string.Format("Failed to upload batch image: {0}", ex.Message);
                    if (rtyCounter.ShouldRetry)
                    {
                        GblUtil.OnOutputLogAndTrace(sErrorMessage + ", Retrying",
                                "SendAllImages.Submit", LTAMessageType.Error,
                                LTAMessageImportance.Verbose);
                    }
                    else
                    {
                        throw new Exception(sErrorMessage);
                    }
                }
            }
            return response;
        }

        public int GetSentImages()
        {
            return sentImages;
        }

        public int GetUnsentImages()
        {
            return unsentImages;
        }

        public bool ImageImportSuccessful
        {
            get;
            private set;
        }
    }
}
