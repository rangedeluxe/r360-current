﻿using System;
using System.Collections.Generic;
using System.IO;
using WFS.LTA.Common;
using WFS.LTA.Common.FileCollector;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;
using WFS.LTA.DataImport.DataImportServiceClient;
using WFS.RecHub.Common;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.RecHub.ImportToolkit.Common.Interfaces;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public class UploadImages : ImportStep, IImageImportStep
    {
        private int sentImages = 0;
        private int unsentImages = 0;
        private ResponseContext responseContext;

        public UploadImages(IPendingService pendingService, IBatchImageSender batchImageSender, IAlertable alertable, ImportStepRequest request)
            : base(pendingService, request)
        {
            BatchImageSender = batchImageSender;
            Alertable = alertable;
        }

        private IAlertable Alertable { get; }
        private IBatchImageSender BatchImageSender { get; }

        protected override ImportStepResponse Execute(ImportStepRequest request)
        {
            var file = base.GetPendingFile(request.ResponseItem.SourceTrackingId);
            if (file == null)
            {
                return new ImportStepResponse
                {
                    IsSuccessful = false,
                    Messages = new List<string>
                    {
                        string.Format("Unable to upload images.  The import step could not find pending item for source tracking id: {0}", request.ResponseItem.SourceTrackingId)
                    }
                };
            }

            var completeImages = cCanonXMLTools.CollectImageData(file.PendingFile.SubmittedXML, request.ResponseItem);

            file.PendingFile.WriteLineToResult(string.Format("Preparing to upload {0} images one at a time", completeImages.Count));
            sentImages = 0;
            unsentImages = 0;
            responseContext = request.ResponseItem.Context ?? new ResponseContext();
            responseContext.Messages = responseContext.Messages ?? new List<string>();

            file.PendingFile.WriteLineToResult(string.Format("Starting image upload at {0}", DateTime.Now));

            if (UploadAllImages(completeImages))
            {
                if (GblUtil.ClientOptions.ImageStorageModeSetting == ImageStorageMode.HYLAND)
                {
                    GblUtil.OnOutputLogAndTrace("Signal Hyland Transfer Complete", " SignalHylandBatchComplete",
                                LTAMessageType.Information, LTAMessageImportance.Verbose);
                }

                GblUtil.OnOutputLogAndTrace("Upload Successfully Completed", " Upload Item Success",
                                    LTAMessageType.Information, LTAMessageImportance.Verbose);

            }
            else
            {
                GblUtil.OnOutputLogAndTrace(string.Format("Failed to upload images for Batch "),
                        "cDITFileProcessor(dumUploadPort_ItemStatusUpdatedEvent)",
                        LTAMessageType.Error, LTAMessageImportance.Essential);
            }

            file.PendingFile.WriteLineToResult(string.Format("Finished image upload at {0}", DateTime.Now));
            request.ResponseItem.Context = responseContext ?? new ResponseContext();
            request.ResponseItem.Context.ImageUploadWasSuccessful = unsentImages == 0 ? true : false;
            request.ResponseItem.Context.ArchivedImages = sentImages;
            request.ResponseItem.Context.UnArchivedImages = unsentImages;
            //UpdateArchiveImages(file.PendingFile.SubmittedXML, sentImages, unsentImages);
            ImageImportSuccessful = sentImages == completeImages.Count;
            return new ImportStepResponse { IsSuccessful = true };
        }

        private bool UploadAllImages(IReadOnlyList<ImageInfoContract> images)
        {
            try
            {
                var imageLoader = new ImageLoader();
                var imageUploadWorker = new ImageUploadWorker(BatchImageSender);
                foreach (var image in images)
                {
                    if (File.Exists(image.ImagePath))
                    {
                        UploadImage(image, imageUploadWorker, responseContext.InputFile, imageLoader);
                        sentImages++;
                    }
                    else
                    {
                        GblUtil.OnOutputLogAndTrace(string.Format("Can not find image file ({0})", image.ImagePath),
                                "cDITFileProcessor(UploadAllImages)", LTAMessageType.Error,
                                LTAMessageImportance.Essential);
                        new AlertManager(Alertable).LogAlert(image);
                        responseContext.InputFile.WriteLineToResult(base.ToImageNotFoundMessage(image));
                        responseContext.Messages.Add(base.ToImageNotFoundMessage(image));
                        unsentImages++;
                    }
                }
                imageUploadWorker.Flush();
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace("Error on Upload Images", "UploadAllImages", ex);
                throw;
            }

            return images.Count == sentImages;
        }
        public static void UploadImage(ImageInfoContract imageInfoContract, IImageConsumer imageConsumer,
            IInputFileResultLogger inputFile, IImageLoader imageLoader)
        {
            if (imageInfoContract.AllowMultiPageTiff)
                UploadImageSides(imageInfoContract, imageConsumer, inputFile, imageLoader);
            else
            {
                var bytes = imageLoader.LoadImageRawBytes(imageInfoContract.ImagePath);
                UploadImageBytes(imageInfoContract, bytes, imageConsumer, inputFile);
            }
        }
        private static void UploadImageSides(ImageInfoContract imageInfoContract, IImageConsumer imageConsumer,
            IInputFileResultLogger inputFile, IImageLoader imageLoader)
        {
            // This will bail with an exception if ImagePath doesn't exist or is invalid
            using (var imageInfo = imageLoader.LoadImageInfo(imageInfoContract))
            {
                if (imageInfo.NumberOfPages == 1)
                {
                    UploadImageSide(imageInfo, imageInfoContract, imageInfoContract.IsBack, imageConsumer, inputFile);
                }
                else if (imageInfo.NumberOfPages > 1)
                {
                    UploadImageSide(imageInfo, imageInfoContract, imageInfoContract.IsBack, imageConsumer, inputFile);
                    UploadImageSide(imageInfo, imageInfoContract, true, imageConsumer, inputFile);
                }
            }
        }
        private static void UploadImageSide(IImageInfo imageInfo, ImageInfoContract imageInfoContract, bool uploadBack,
            IImageConsumer imageConsumer, IInputFileResultLogger inputFile)
        {
            using (Stream stream = uploadBack ? imageInfo.GetBackPageStream() : imageInfo.GetFrontPageStream())
            {
                // The same imageInfoContract instance is passed in for both front and back,
                // so clone it before adding it to the list
                var sideImageInfoContract = imageInfoContract.Clone();
                sideImageInfoContract.IsBack = uploadBack;
                var imageBytes = stream.ToBytes();
                UploadImageBytes(sideImageInfoContract, imageBytes, imageConsumer, inputFile);
            }
        }
        private static void UploadImageBytes(ImageInfoContract imageInfoContract, byte[] imageBytes,
            IImageConsumer imageConsumer, IInputFileResultLogger inputFile)
        {
            var uploadItem = new UploadRequest
            {
                ImageInfo = imageInfoContract,
                ImageData = imageBytes
            };
            imageConsumer.Add(imageInfoContract.ImagePath, uploadItem);
            inputFile.WriteLineToResult(
                $"{imageInfoContract.ImagePath} successfully uploaded (is {(imageInfoContract.IsBack ? "back" : "front")} of image)");
        }

        public int GetSentImages()
        {
            return sentImages;
        }

        public int GetUnsentImages()
        {
            return unsentImages;
        }

        public bool ImageImportSuccessful
        {
            get;
            private set;
        }
    }
}
