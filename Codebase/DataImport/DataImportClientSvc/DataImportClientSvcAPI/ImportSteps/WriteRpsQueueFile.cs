﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using WFS.LTA.Common;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public class WriteRpsQueueFile : ImportStep
    {
        private cInboxPathData sourceData;

        public WriteRpsQueueFile(IPendingService pendingService, ImportStepRequest request) :
            base(pendingService, request)
        {
        }

        protected override ImportStepResponse Execute(ImportStepRequest request)
        {
            sourceData = (cInboxPathData)request.ResponseItem.Context.InputFile.SourcePathData;

            if (string.IsNullOrEmpty(sourceData.ImageRPSResponseFolder))
                return new ImportStepResponse
                {
                    IsSuccessful = false,
                    Messages = new List<string>
                    {
                        "Unable to write out RPS queue file because the 'ImageRPSResponseFolder' is not configured."
                    }

                };

            var file = base.GetPendingFile(request.ResponseItem.SourceTrackingId);
            if (file == null)
                return new ImportStepResponse
                {
                    IsSuccessful = false,
                    Messages = new List<string>
                    {
                        string.Format("Unable to write out RPS queue file. The import step could not find a pending item for source tracking id: {0}", request.ResponseItem.SourceTrackingId)
                    }
                };

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.SafeLoadXml(request.ResponseItem.Context.InputFile.SubmittedXML);
            foreach (XmlNode xmnCurBatchNode in xmlDoc.SelectNodes("//Batches/Batch"))
            {
                string batchID = xmnCurBatchNode.Attributes["BatchID"].Value;
                string lockboxID = xmnCurBatchNode.Attributes["ClientID"].Value;
                request.ResponseItem.Context.BatchId = batchID;
                request.ResponseItem.Context.WorkgroupId = lockboxID;
                request.ResponseItem.Context.Messages = request.ResponseItem.Context.Messages ?? new List<string>();
                request.ResponseItem.Context.Messages.Add(request.ResponseItem.Message);

                var imageImportSteps = request.ResponseItem.ImportSteps.Where(step => step is IImageImportStep);
                request.ResponseItem.Context.UnArchivedImages = imageImportSteps.FirstOrDefault() == null ? 0 : (imageImportSteps.FirstOrDefault() as IImageImportStep).GetUnsentImages();
                request.ResponseItem.Context.ArchivedImages = imageImportSteps.FirstOrDefault() == null ? 0 : (imageImportSteps.FirstOrDefault() as IImageImportStep).GetSentImages();
                request.ResponseItem.Context.ImageUploadWasSuccessful = imageImportSteps.FirstOrDefault() == null ? true : (imageImportSteps.FirstOrDefault() as IImageImportStep).ImageImportSuccessful;

                WriteImageRPSResponseFile(request.ResponseItem.Context);
            }
            return new ImportStepResponse { IsSuccessful = true };
        }

        private void WriteImageRPSResponseFile(ResponseContext responseContext)
        {
            if (responseContext == null)
                throw new ArgumentNullException("ResponseContext must not be null");

            string sInProcessSubFolder;
            string strInProcessResponseFilePath;
            string strResponseFilePath;
            StringBuilder stringBuffer;
            StringBuilder filename;

            responseContext.ImportWasSuccessful = responseContext.DataLoadWasSuccessful && responseContext.ImageUploadWasSuccessful;
            
            try
            {
                GblUtil.OnOutputLogAndTrace("Write ImageRPS response file started."
                           , this.GetType().Name, LTAMessageType.Information
                          , LTAMessageImportance.Debug);

                filename = new StringBuilder();
                filename.AppendFormat("{0}_{1}.dat", responseContext.WorkgroupId.PadLeft(4, '0'), responseContext.BatchId.PadLeft(6, '0'));

                sInProcessSubFolder = Path.Combine(sourceData.ImageRPSResponseFolder, "InProcess");

                GblUtil.OnOutputLogAndTrace("Checking for InProcess subfolder in " + sInProcessSubFolder
                           , this.GetType().Name, LTAMessageType.Information
                          , LTAMessageImportance.Debug);

                if (!Directory.Exists(sInProcessSubFolder))
                    Directory.CreateDirectory(sInProcessSubFolder);

                strInProcessResponseFilePath = Path.Combine(sInProcessSubFolder, filename.ToString());

                using (StreamWriter sw = new StreamWriter(strInProcessResponseFilePath))
                {
                    stringBuffer = new StringBuilder().AppendFormat(ToRPSQueueFileContent(responseContext, sourceData.PreviousRPSQueueFileAllowed));
                    sw.WriteLine(stringBuffer);
                }

                strResponseFilePath = Path.Combine(sourceData.ImageRPSResponseFolder, filename.ToString());

                GblUtil.OnOutputLogAndTrace("Deleting ImageRPS response file if it already exists,  filename: " + strResponseFilePath
                            , this.GetType().Name, LTAMessageType.Information
                            , LTAMessageImportance.Debug);

                if (File.Exists(strResponseFilePath))
                    File.Delete(strResponseFilePath);

                File.Move(strInProcessResponseFilePath, strResponseFilePath);

                GblUtil.OnOutputLogAndTrace("Finished writing ImageRPS response file"
                       , this.GetType().Name, LTAMessageType.Information
                       , LTAMessageImportance.Debug);
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace("Exception thrown trying to write ImageRPS response file. Exception: " + ex.Message
                         , this.GetType().Name, LTAMessageType.Error,
                         LTAMessageImportance.Essential);
                throw ex;
            }
        }

        private string ToRPSQueueFileContent(ResponseContext responseContext, bool createPreviousVersionContent)
        {
            if (responseContext == null)
                throw new ArgumentNullException("ResponseContext must not be null");

            var content = new StringBuilder();
            var time = DateTime.Now;
            var currentMidnight = new DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
            var seconds = time.TimeOfDay.TotalSeconds - currentMidnight.TimeOfDay.TotalSeconds;
            var dateString = createPreviousVersionContent ? time.ToString("MM/dd/yyyy") + " " + Convert.ToInt32(seconds) : string.Format("{0} 1", DateTime.Now.ToString("MM/dd/yyyy"));

            content.AppendFormat
                (
                    "\"{0}\" \"{1}\" \"{2}\" \"{3}\" ",
                    responseContext.BatchId,
                    createPreviousVersionContent ? responseContext.BatchId : BuildQueueFileMessage(responseContext.Messages),
                    responseContext.ArchivedImages, responseContext.UnArchivedImages
                );

            if (responseContext.ImportWasSuccessful)
                content.AppendFormat("\"Good\" ");
            else
                content.AppendFormat(createPreviousVersionContent ? "\"Review\" " : "\"Resend\" ");

            content.AppendFormat
                (
                    "\"unattend/{0}\" {1} \"Archived RPS Batch {2} as {3} Batch {4} ",
                    createPreviousVersionContent ? "imsdip" : "r360dip", dateString, responseContext.BatchId, createPreviousVersionContent ? "OLTA" : "R360", responseContext.BatchId);

            if (responseContext.ImportWasSuccessful)
                content.AppendFormat(" Successfully\"");
            else
                content.AppendFormat(" with Errors/Warnings/Invalids\"");

            return content.ToString();
        }

        private string BuildQueueFileMessage(IEnumerable<string> messages)
        {

            if (messages == null)
                return string.Empty;

            var queueFileMessage = new StringBuilder();

            foreach (var message in messages)
            {
                queueFileMessage.Append(message.Replace("\"", "'")); //replace double quotes with single quotes in queue file message
            }

            return queueFileMessage.Length > 210 ? queueFileMessage.ToString().Substring(0, 210) : queueFileMessage.ToString();//queue file message can only be 210 characters long
        }
    }
}
