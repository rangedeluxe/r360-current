﻿using System;
using System.IO;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public class ImageLoader : IImageLoader
    {
        private static string GetFileSizeToDisplay(string path)
        {
            try
            {
                return new FileInfo(path).Length + " bytes";
            }
            catch
            {
                return "(error reading file size)";
            }
        }
        public IImageInfo LoadImageInfo(ImageInfoContract imageInfoContract)
        {
            var imagePath = imageInfoContract.ImagePath;
            try
            {
                return ImageInfo.Create(imagePath, GblUtil.ClientOptions.ImageSplitJpegQuality);
            }
            catch (Exception ex)
            {
                var message = string.Format(
                    "Error loading image: path='{0}', {1}, BankID={2}, WorkgroupID={3}, SourceBatchID={4}, BatchSequence={5}: {6}",
                    imagePath, GetFileSizeToDisplay(imagePath),
                    imageInfoContract.BankID, imageInfoContract.LockboxID,
                    imageInfoContract.SourceBatchID, imageInfoContract.BatchSequence,
                    ex.Message);
                throw new Exception(message, ex);
            }
        }
        public byte[] LoadImageRawBytes(string path)
        {
            return File.ReadAllBytes(path);
        }
    }
}