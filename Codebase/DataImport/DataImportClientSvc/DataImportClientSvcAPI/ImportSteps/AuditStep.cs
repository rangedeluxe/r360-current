﻿using System;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;
using WFS.RecHub.AuditingServicesCommon.Interfaces;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps
{
    public class AuditStep : ImportStep
    {
        private readonly IAuditingDit _auditingServicesClient;

        public AuditStep(IPendingService pendingService, ImportStepRequest request,
            IAuditingDit auditingServicesClient)
            : base(pendingService, request)
        {
            _auditingServicesClient = auditingServicesClient;
        }

        protected override ImportStepResponse Execute(ImportStepRequest request)
        {
            var auditDateKey = request.ResponseItem.AuditDateKey;
            var sourceTrackingId = request.ResponseItem.SourceTrackingId;

            _auditingServicesClient.DitEndProcess(
                auditDateKey, sourceTrackingId, DateTime.Now, request.ResponseItem.IsSuccessful);

            return new ImportStepResponse
            {
                IsSuccessful = true
            };
        }
    }
}