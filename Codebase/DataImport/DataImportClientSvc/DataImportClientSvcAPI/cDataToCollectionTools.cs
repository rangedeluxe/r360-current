﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     08/07/2012
*
* Purpose:  Static class to provide tools for converting datasets to collections.  It also contains other functions
*              for working with collections.          
*
* Modification History
*  Created
*  CR 52267 CEJ 07/08/2012   Data Import Client
*******************************************************************************/


namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    class cDataToCollectionTools
    {
        public static bool DataSetToDictionary(DataSet dsData, string sKeyFieldName, string sValueFieldName, out Dictionary<string, string> dctResult)
        {
            bool bAns = false;

            dctResult = new Dictionary<string, string>();
            if (dsData.Tables.Count > 0)
            {
                if (dsData.Tables[0].Columns.Contains(sKeyFieldName))
                {
                    if (dsData.Tables[0].Columns.Contains(sValueFieldName))
                    {
                        dctResult = dsData.Tables[0].AsEnumerable()
                            .ToDictionary(row => row[sKeyFieldName].ToString(), row => row[sValueFieldName].ToString());
                        bAns = true;
                    }
                    else
                        throw new Exception(string.Format("Fieldname ({0}) not found in the first table of the Dataset", sValueFieldName));
                }
                else
                    throw new Exception(string.Format("Fieldname ({0}) not found in the first table of the Dataset", sKeyFieldName));
            }
            else
                throw new Exception("No tables where found in the Dataset");
            return bAns;
        }

        public static void UpsertHashTable(Hashtable hshBase, object objKey, object objValue)
        {
            UpsertHashTable(hshBase, new DictionaryEntry(objKey, objValue));
        }

        public static void UpsertHashTable(Hashtable hshBase, DictionaryEntry dceUpdate)
        {
            if (hshBase.ContainsKey(dceUpdate.Key))
                hshBase[dceUpdate.Key] = dceUpdate.Value;
            else
                hshBase.Add(dceUpdate.Key, dceUpdate.Value);
        }
    }
}
