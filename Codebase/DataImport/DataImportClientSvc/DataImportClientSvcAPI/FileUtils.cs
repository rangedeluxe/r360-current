﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.LTA.Common.FileCollector;

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    /// <summary>
    /// Entire file was copied from cDITFileProcessor so that others can use these utility functions.
    /// </summary>
    public static class FileUtils
    {

        public static FileInfo Compress(FileInfo fi)
        {
            FileInfo returnFileInfo = null;
            string outPutFileName = string.Empty;
            try
            {
                using (FileStream inFile = fi.OpenRead())
                {
                    if ((File.GetAttributes(fi.FullName)
                                & FileAttributes.Hidden)
                                != FileAttributes.Hidden & fi.Extension != ".gz")
                    {
                        outPutFileName = Path.Combine(fi.FullName + ".gz");
                        using (FileStream outFile =
                                    File.Create(outPutFileName))
                        {
                            using (GZipStream Compress =
                                        new GZipStream(outFile,
                                            CompressionMode.Compress))
                            {
                                inFile.CopyTo(Compress);
                                returnFileInfo = new FileInfo(outPutFileName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnFileInfo;
        }

        public static bool MoveFile(string fromFilePath, string toFilePath)
        {

            bool bolContinue = true;
            int intRetryCount = 0;
            bool bolRetVal = false;

            while (bolContinue)
            {

                try
                {
                    File.Move(fromFilePath, toFilePath);
                    bolRetVal = true;
                    bolContinue = false;
                }
                catch (Exception ex)
                {

                    if (intRetryCount < 3)
                    {
                        intRetryCount++;

                    }
                    else
                    {

                        bolContinue = false;
                        throw ex;
                    }
                }
            }

            return (bolRetVal);
        }

        public static void ArchiveItem(cInputFile inputFile)
        {
            try
            {
                FileInfo rtnArchiveInfo = null;
                if (String.IsNullOrEmpty(inputFile.SourcePathData.ArchiveFolder))
                {
                    File.Delete(inputFile.FileData.FullName);
                }
                else
                {
                    rtnArchiveInfo = Compress(inputFile.FileData);
                    if (rtnArchiveInfo != null)
                    {
                        string newFileName = Path.Combine(inputFile.SourcePathData.ArchiveFolder, rtnArchiveInfo.Name);
                        MoveFile(rtnArchiveInfo.FullName, newFileName); ;
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public static bool MoveFileToPendingResponse(cInputFile inputFile, Guid transmissionID)
        {

            FileInfo pendingFileInfo = inputFile.FileData;
            try
            {
                var pendingResponseFileName = Path.Combine(inputFile.SourcePathData.PendingResponseFolder, pendingFileInfo.Name + ".-" + transmissionID.ToString() + "-.pendingResponse");
                ArchiveItem(inputFile);
                GblUtil.MoveFile(pendingFileInfo.FullName, pendingResponseFileName);
                File.WriteAllText(pendingResponseFileName, inputFile.SubmittedXML);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool MoveFileToError(FileInfo pendingFileInfo, cInputFile inputFile, Guid transmissionID)
        {
            var errorFile = Path.Combine(inputFile.SourcePathData.InputErrorFolder, pendingFileInfo.Name + "." + transmissionID.ToString() + ".error");
            return MoveFile(inputFile.FileData.FullName, errorFile);
        }


    }
}
