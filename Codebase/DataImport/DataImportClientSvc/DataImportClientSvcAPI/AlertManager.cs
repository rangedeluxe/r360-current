﻿using System;
using System.Linq;
using System.Text;
using WFS.LTA.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.RecHub.ImportToolkit.Common.Interfaces;

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{

    public class AlertManager 
    {
        private const string ProcessingExceptionEventName = "ProcessingException";
        private readonly IAlertable alertService;

        public AlertManager(IAlertable service)
        {
            alertService = service;
        }


        public void LogAlert(ImageInfoContract imageInfo)
        {
            if (imageInfo == null)
                throw new ArgumentNullException("Please provide image info");

            LogAlert(ProcessingExceptionEventName, GetAlertMessage(imageInfo));
        }

        public string GetAlertMessage(ImageInfoContract imageInfo)
        {
            return
                new StringBuilder()
                    .Append(string.Format("Image File {0} did not import from Data Import Toolkit.  ", imageInfo.ImagePath))
                    .Append(string.Format("Date = {0}, ", imageInfo.ProcessingDateKey))
                    .Append(string.Format("Workgroup ID = {0}, ", imageInfo.LockboxID))
                    .Append(string.Format("Bank ID = {0}, ", imageInfo.BankID))
                    .Append(string.Format("Payment Source = {0}, ", imageInfo.BatchSourceShortName))
                    .Append(string.Format("Batch ID = {0}, ", imageInfo.SourceBatchID))
                    .Append(string.Format("Batch Number = {0}.", imageInfo.BatchNumber))
                    .ToString();
        }

        public void LogAlert(string eventName, string message)
        {
            var response = alertService.Log(new AlertRequest { EventName = eventName, Message = message });

            if (response.Errors.FirstOrDefault() != null)
            {
                foreach (var error in response.Errors)
                {
                    GblUtil.OnOutputLogAndTrace(error, "LogAlert", LTAMessageType.Information, LTAMessageImportance.Essential);
                }
            }
        }
    }
}
