﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WFS.LTA.DataImport.DataImportServiceClient;
using WFS.LTA.DataImport.DataImportWCFLib;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     08/07/2012
*
* Purpose:  This class allows for the comparison of ImageInfoContract objects.  This helps provide a unique key when the ImageInfoContract is
*               used as the key of a dictionary.
*
* Modification History
*
*  Created
*  CR 52267 CEJ 07/08/2012   Data Import Client
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
public class cImageInforContractEquality:IEqualityComparer<ImageInfoContract>
    {
        public bool Equals(ImageInfoContract x, ImageInfoContract y)
        {
            bool bAns = x.BankID == y.BankID;
            bAns &= x.LockboxID == y.LockboxID;
            bAns &= x.ProcessingDateKey == y.ProcessingDateKey;
            bAns &= x.BatchSequence == y.BatchSequence;
            bAns &= x.FileDescriptor == y.FileDescriptor;
            bAns &= x.ColorMode == y.ColorMode;
            bAns &= x.ImagePath == y.ImagePath;
            bAns &= x.SourceBatchID == y.SourceBatchID;
            return bAns;
        }

        public int GetHashCode(ImageInfoContract obj)
        {
            return string.Join("|",
                    new string[] 
                        {obj.BankID.ToString(), 
                            obj.LockboxID.ToString(), 
                            obj.ProcessingDateKey.ToString(),
                            obj.BatchSequence.ToString(),
                            obj.FileDescriptor,
                            obj.ColorMode.ToString(),
                            obj.ImagePath,
                            obj.SourceBatchID.ToString()
                        }).GetHashCode();
        }
    }
}
