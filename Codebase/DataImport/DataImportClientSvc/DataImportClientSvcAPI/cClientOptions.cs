﻿using System;
using System.Collections.Generic;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Crypto;
using WFS.LTA.Common;
using System.Configuration;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     08/07/2012
*
* Purpose:  
*
* Modification History
*
* Created
*  CR 52267 JMC 08/07/2012
*   -Initial Version
* WI 126987 CMC 01/20/2014
*  - Updated to use 2.00 libraries.
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientSvcAPI {

    public class cClientOptions
    {
        private const string INIKEY_SERVER_LOCATION = "ServerLocation";
        private const string INIKEY_AUDITING_SERVICE = "AuditingService";
        private const string INIKEY_LOGON_NAME = "LogonName";
        private const string INIKEY_PASSWORD = "Password";
        private const string LOG_FILE = "LogFile";
        private const string LOGGING_DEPTH = "LoggingDepth";
        private const string LOG_FILE_MAX_SIZE = "LogFileMaxSize";
        private const string INIKEY_UPLOAD_STATUS_UPDATE_FREQ_MILLISEC = "UploadStatusUpdateFrequencyMillisec";
        private const string INIKEY_UPDATE_DIMFIELDLISTS_FREQ = "UpdateDimFieldListsFrequency";
        private const string INIKEY_RETRY_DELAY = "RetryDelay";
        private const string INIKEY_MAX_RETRIES = "MaxRetries";
        private const string INIKEY_PROGRESSIVE_RETRY_DELAY = "ProgressiveRetryDelay";
        private const string INIKEY_IMAGE_STORAGE_MODE_SETTING = "ImageStorageModeSetting";
        private const string ICON_IMPORT_TYPE = "ICONImportType";
        private const string ImageSplitLocationSettingName = "ImageSplitLocation";
        private const string ImageSplitJpegQualitySettingName = "ImageSplitJpegQuality";
        private const int ImageSplitJpegQualityMinValue = 1;
        private const int ImageSplitJpegQualityMaxValue = 100;
        private const int ImageSplitJpegQualityDefaultValue = ImageInfo.DefaultImageSplitJpegQuality;
        private const string ImageTransferMaxBytesSettingName = "ImageTransferMaxBytes";
        private const int ImageTransferMaxBytesMinValue = 1;
        private const int ImageTransferMaxBytesMaxValue = 16 * 1024 * 1024;
        private const int ImageTransferMaxBytesDefaultValue = 8 * 1024 * 1024;
        private const string ImageTransferMaxCountSettingName = "ImageTransferMaxCount";
        private const int ImageTransferMaxCountMinValue = 1;
        private const int ImageTransferMaxCountMaxValue = 100;
        private const int ImageTransferMaxCountDefaultValue = 100;

        private readonly List<Tuple<string, LTAMessageType, LTAMessageImportance>> _configurationLog =
            new List<Tuple<string, LTAMessageType, LTAMessageImportance>>();

        public cClientOptions()
        {
            UploadStatusUpdateFrequency = 1000;
            UpdateDimFieldListsFrequency = 60;
            RetryDelay = 3.0;
            MaxRetries = 3;
            ProgressiveRetryDelay = true;

            GetSettings();
        }

        private void GetSettings()
        {
            cCrypto3DES cryp3DES = new cCrypto3DES();

            AuditingService = ConfigurationManager.AppSettings.Get(INIKEY_AUDITING_SERVICE);
            ServerLocation = ConfigurationManager.AppSettings.Get(INIKEY_SERVER_LOCATION);

            string logonName;
            cryp3DES.Decrypt(ConfigurationManager.AppSettings.Get(INIKEY_LOGON_NAME), out logonName);
            LogonName = logonName;

            string password;
            cryp3DES.Decrypt(ConfigurationManager.AppSettings.Get(INIKEY_PASSWORD), out password);
            Password = password;

            SetImageModeSetting(ConfigurationManager.AppSettings.Get(INIKEY_IMAGE_STORAGE_MODE_SETTING));
            this.logFilePath = ConfigurationManager.AppSettings.Get(LOG_FILE);
            int depth = 0;
            int fileMaxSize = 0;
            int.TryParse(ConfigurationManager.AppSettings.Get(LOGGING_DEPTH), out depth);
            int.TryParse(ConfigurationManager.AppSettings.Get(LOG_FILE_MAX_SIZE), out fileMaxSize);
            this.loggingDepth = depth;
            this.logFileMaxSize = fileMaxSize;
            IconImportType = ConfigurationManager.AppSettings.Get(ICON_IMPORT_TYPE);

            var imageSplitLocationString = ConfigurationManager.AppSettings.Get(ImageSplitLocationSettingName);
            var isServer = string.Equals(imageSplitLocationString, "Server", StringComparison.InvariantCultureIgnoreCase);
            ImageSplitLocation = isServer
                ? ImageSplitLocation.Server
                : ImageSplitLocation.Client;

            ImageSplitJpegQuality = GetIntSetting(ImageSplitJpegQualitySettingName,
                ImageSplitJpegQualityMinValue, ImageSplitJpegQualityMaxValue, ImageSplitJpegQualityDefaultValue);
            ImageTransferMaxBytes = GetIntSetting(ImageTransferMaxBytesSettingName,
                ImageTransferMaxBytesMinValue, ImageTransferMaxBytesMaxValue, ImageTransferMaxBytesDefaultValue);
            ImageTransferMaxCount = GetIntSetting(ImageTransferMaxCountSettingName,
                ImageTransferMaxCountMinValue, ImageTransferMaxCountMaxValue, ImageTransferMaxCountDefaultValue);
        }

        private int GetIntSetting(string settingName, int minValue, int maxValue, int defaultValue)
        {
            var stringValue = ConfigurationManager.AppSettings.Get(settingName);
            if (string.IsNullOrEmpty(stringValue))
            {
                var defaultValueMessage = string.Format(
                    "Configuration value '{0}' is not specified, using default value {1}",
                    settingName, defaultValue);
                _configurationLog.Add(Tuple.Create(defaultValueMessage,
                    LTAMessageType.Information, LTAMessageImportance.Verbose));
                return defaultValue;
            }

            int intValue;
            var success = int.TryParse(stringValue, out intValue);
            if (success && intValue >= minValue && intValue <= maxValue)
            {
                var valueMessage = string.Format("Configuration value '{0}' = {1}", settingName, intValue);
                _configurationLog.Add(Tuple.Create(valueMessage,
                    LTAMessageType.Information, LTAMessageImportance.Verbose));
                return intValue;
            }
            var warningMessage = string.Format(
                "Configuration value '{0}' has invalid value '{1}'. " +
                "Value should be from {2} to {3}. Using default value {4} instead.",
                settingName, stringValue, minValue, maxValue, defaultValue);
            _configurationLog.Add(Tuple.Create(warningMessage,
                LTAMessageType.Warning, LTAMessageImportance.Essential));
            return defaultValue;
        }

        private void SetImageModeSetting(string value)
        {
            int imageSetting = 0;

            if (int.TryParse(value, out imageSetting))
            {

                try
                {
                    ImageStorageModeSetting = (ImageStorageMode)imageSetting;
                }
                catch (Exception)
                {
                    //Log something???
                    ImageStorageModeSetting = ImageStorageMode.PICS;
                }

            }
            else
            {
                //Log something???
                ImageStorageModeSetting = ImageStorageMode.PICS;
            }
        }

        public IReadOnlyList<Tuple<string, LTAMessageType, LTAMessageImportance>> ConfigurationLog
        {
            get { return _configurationLog; }
        }
        public string AuditingService { get; private set; }
        public string ServerLocation { get; private set; }
        public string LogonName { get; private set; }
        public string Password { get; private set; }
        public string IconImportType { get; private set; }
        public int ImageSplitJpegQuality { get; private set; }
        public ImageSplitLocation ImageSplitLocation { get; private set; }
        public int ImageTransferMaxBytes { get; private set; }
        public int ImageTransferMaxCount { get; private set; }

        public string logFilePath { get; private set; }
        public int logFileMaxSize { get; private set; }
        public int loggingDepth { get; private set; }

        public int UploadStatusUpdateFrequency { get; private set; }
        public int UpdateDimFieldListsFrequency { get; private set; }
        public double RetryDelay { get; private set; }
        public int MaxRetries { get; private set; }
        public bool ProgressiveRetryDelay { get; private set; }
        public ImageStorageMode ImageStorageModeSetting { get; private set; }
    }
}
