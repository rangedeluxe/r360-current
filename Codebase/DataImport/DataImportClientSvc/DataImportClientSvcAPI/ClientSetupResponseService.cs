﻿using System.Collections.Generic;
using System.Linq;
using WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;
using WFS.RecHub.AuditingServicesCommon.Interfaces;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class ClientSetupResponseService : ResponseService
    {
        private readonly IAuditingDit _auditingServicesClient;

        public ClientSetupResponseService(ResponseServiceContext context, IAuditingDit auditingServicesClient)
            : base(context)
        {
            _auditingServicesClient = auditingServicesClient;
        }

        public override IEnumerable<ResponseItem> GetResponses()
        {
            var clientSetupResponses = Context.Proxy.GetClientSetupResponses(Context.ClientProcessCode);
            var responses = GetResponses(clientSetupResponses);
            foreach (var response in responses)
            {
                response.ImportSteps = new List<IImportStep>
                {
                    new HandleDataImportResponse(Context.PendingService, new ImportStepRequest {ResponseItem = response}),
                    new AuditStep(Context.PendingService, new ImportStepRequest {ResponseItem = response}, _auditingServicesClient),
                    new WriteNativeResponseFile(Context.PendingService, new ImportStepRequest {ResponseItem = response}),
                    new UpdateImportStatus(Context.PendingService, Context.Proxy, new ImportStepRequest {ResponseItem = response}),
                };
            }

            return responses;
        }

        public static IReadOnlyList<ResponseItem> GetResponses(ClientSetupResponses clientSetupResponses)
        {
            return clientSetupResponses.Responses
                .Select(response => new ResponseItem
                {
                    ResponseTrackingId = response.ResponseTrackingId,
                    AuditDateKey = response.AuditDateKey,
                    SourceTrackingId = response.SourceTrackingId,
                    Message = response.Message,
                    IsSuccessful = response.IsSuccessful,
                })
                .ToList();
        }
    }
}
