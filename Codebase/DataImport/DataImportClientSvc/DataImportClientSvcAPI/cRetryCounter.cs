﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/13/2012
*
* Purpose:  This class keeps track of the number of times a section of code is processed
*           as well and inserting delays between the reties
* 
*    Usage Structure:
*        cRetryCounter rtyCounter=new cRetryCounter(3, 0.5, true); 
*        while(rtyCounter.CycleRetry())
*            {
*            try
*                {
*                ... //Code to retry
*                rtyCounter.HasSuccessed = [was the code successful?] ;
*                }
*            catch(Exception ex)
*                {
*                if(rtyCounter.ShouldRetry)
*                    ... //Log Exception
*                else
*                    throw ex;
*                }
*            }
*
* Modification History
* Created
*  CR 52267: CEJ 13/06/2012
*   - DIT Client
* WI 34771 WJS 11/26/2012
 *   - Add support for pause client
*******************************************************************************/


namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class cRetryCounter
    {
        private int _iTryCount = 0;
        private int _iMaxRetries = 0;
        private double _dDelayInSec = 0;
        private bool _bProgressiveDelay = false;
        private bool _bHasSucceeded = false;

        ///<param name="iMaxRetries"> number of retries to attempt before giving up, not including the initial try </param>
        ///<param name="dDelayInSec"> number of seconds to wait between retries </param>
        ///<param name="bProgressiveDelay"> if this is true the retry delay doubles for each subsequent retry </param>
        public cRetryCounter(int iMaxRetries, double dDelayInSec = 0.0, bool bProgressiveDelay = false)
        {
            _iTryCount = 0;
            MaxRetries = iMaxRetries;
            DelayInSec = dDelayInSec;
            ProgressiveDelay = bProgressiveDelay;
            HasSucceeded = false;
        }

        public bool HasSucceeded
        {
            get { return _bHasSucceeded; }
            set { _bHasSucceeded = value; }
        }

        ///<summary> If this property is true the retry delay doubles for each progressive retry</summary>
        public bool ProgressiveDelay
        {
            get { return _bProgressiveDelay; }
            set { _bProgressiveDelay = value; }
        }

        public int MaxRetries
        {
            get { return _iMaxRetries; }
            set { _iMaxRetries = value; }
        }

        public double DelayInSec
            {
            get { return _dDelayInSec; }
            set { _dDelayInSec = value; }
            }

        ///<summary>The number of times the process was attempted including the initial execution</summary>
        public int TryCount
        {
            get { return _iTryCount; }
        }

        public void DoDelay()
        {
            double dSecondsDelay;

            if(!_bHasSucceeded)
                if (_iTryCount > 0)
                {
                    dSecondsDelay = _dDelayInSec * Math.Pow(2, _bProgressiveDelay ? _iTryCount - 1 : 0);

                    Thread.Sleep((int)(1000.0 * dSecondsDelay));
                }
        }
        
        ///<summary>Returns true if there are retries available and the process has not yet succeeded, 
                /// also increments retry counter and if bDoDelay is true does the retry delay</summary>
        ///<param name="bDoDelay"> if true does a thread sleep for the appropriate delay</param>
        public bool CycleRetry(bool bDoDelay = true)
        {
            bool bAns = ShouldRetry;

            if(bDoDelay)
                DoDelay();
            if(bAns)
                _iTryCount += 1;
            return bAns;
        }

        public bool ShouldRetry
        {
            get
            {
                return (!HasSucceeded) && (_iTryCount < _iMaxRetries + 1);
            }
        }
    }
}
