﻿namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    class DitTrackingResponse
    {
        public int FileStatus { get; set; }
        public string BatchTrackingIds { get; set; }
    }
}
