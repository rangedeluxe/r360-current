﻿
using System.Collections.Generic;
using System.Data;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Extensions;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public abstract class ResponseService : IResponseService
    {

        public ResponseService(ResponseServiceContext context)
        {
            this.Context = context;
        }

        protected ResponseServiceContext Context { get; private set; }

        protected static IEnumerable<ResponseItem> ToImportResponses(DataSet ds)
        {
            var responses = new List<ResponseItem>();

            if (ds.HasData())
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    var xmlResponse = row["XMLResponseDocument"].ToString();
                    responses.AddRange(xmlResponse.ToResponseItemList());
                }
            }
            return responses;
        }

        public abstract IEnumerable<ResponseItem> GetResponses();
    }
}
