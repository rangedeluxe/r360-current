﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 52267 JMC 08/09/2012 
*   -Initial version.
* WI 129729 CMC 02/14/2014 
*   - Adding additional config attributes 
*   (BankName, DefaultBankID, DefaultLockboxID, DataRetentionDays, ImageRetentionDays, and ImageRPSResponseFolder)
* WI 135681 CMC 04/21/2014
*   - Support for Input Error Folder 
* WI 143271, 143409 CMC 05/29/2014
*   - Support for default payment type and batch source 
* WI 148294 CMC 06/17/2014
*   - Change DefaultLockboxID to DefaultOrganizationID 
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientSvcAPI {

    public class cDataImportSettingsConfigSection : ConfigurationSection {
        /// <summary>
        /// The value of the property here "Process" needs to match that of the config file section
        /// </summary>
        [ConfigurationProperty("DataImportConfigSections")]
        public ProcessCollection ProcessItems {
            get { 
                return ((ProcessCollection)(base["DataImportConfigSections"])); 
            }
        }
    }

    /// <summary>
    /// The collection class that will store the list of each element/item that
    ///        is returned back from the configuration manager.
    /// </summary>
    [ConfigurationCollection( typeof( ProcessElement ) )]
    public class ProcessCollection : ConfigurationElementCollection {

        protected override ConfigurationElement CreateNewElement() {
            return new ProcessElement();
        }
 
        protected override object GetElementKey( ConfigurationElement element ) {
            return ( (ProcessElement)( element ) ).ClientProcessCode;
        }
 
        public ProcessElement this[int idx ] {
            get {
                return (ProcessElement) BaseGet(idx);
            }
        }
    }

    public interface _ProcessElement {
        string ClientProcessCode { get; }

        string ToCanonicalXSLFile { get; }

        string FileType { get; }

        string InputFolder { get; }

        string InProcessFolder { get; }

        string PendingResponseFolder { get; }

        string ResponseFolder { get; }

        string InputErrorFolder { get; }

        string ResponseErrorFolder { get; }

        string ArchiveFolder { get; }

        string FilePattern { get; }

        string XClientDllFilePath { get; }

        string IniSectionKey { get; }

         string BankName { get; }

         string DefaultBankId { get; }

         string DefaultOrganizationId { get; }

         string ImageRetentionDays { get; }

         string DataRetentionDays { get; }

         string ImageRPSResponseFolder { get; }

         string DefaultPaymentType { get; }

         string DefaultBatchSource { get; }

         string DefaultSiteCode { get; }

         string PreviousRpsQueueFileVersionAllowed { get; }
         bool SkipDebit { get; }
        string SendEndPoint { get; }
        string StatusEndPoint { get; }
    }

    /// <summary>
    /// The class that holds onto each element returned by the configuration manager.
    /// </summary>
    public class ProcessElement : ConfigurationElement, _ProcessElement {

        private const string CONFIG_PROCESS_CLIENTPROCESSCODE_KEY = "ClientProcessCode";
        private const string CONFIG_PROCESS_XSL_FILE_PATH_KEY = "ToCanonicalXSLFile";
        private const string CONFIG_PROCESS_FILE_TYPE_KEY = "FileType";
        private const string CONFIG_PROCESS_INPUT_PATH_KEY = "InputFolder";
        private const string CONFIG_PROCESS_INPROCESS_PATH_KEY = "InProcessFolder";
        private const string CONFIG_PROCESS_PENDING_RESPONSE_FOLDER_KEY = "PendingResponseFolder";
        private const string CONFIG_PROCESS_RESPONSE_FOLDER_KEY = "ResponseFolder";
        private const string CONFIG_PROCESS_INPUT_ERROR_FOLDER_KEY = "InputErrorFolder";
        private const string CONFIG_PROCESS_RESPONSE_ERROR_FOLDER_KEY = "ErrorResponseFolder";
        private const string CONFIG_PROCESS_ARCHIVE_FOLDER_KEY = "ArchiveFolder";
        private const string CONFIG_PROCESS_FILE_PATTERN_KEY = "FilePattern";
        private const string CONFIG_PROCESS_XCLIENT_DLL_KEY = "XClientDllFilePath";
        private const string CONFIG_PROCESS_INI_SECTION_KEY = "IniSectionKey";
        private const string CONFIG_PROCESS_BANK_NAME = "BankName";
        private const string CONFIG_PROCESS_DEFAULT_BANK_ID = "DefaultBankID";
        private const string CONFIG_PROCESS_DEFAULT_ORGANIZATION_ID = "DefaultOrganizationID";
        private const string CONFIG_PROCESS_DATA_RETENTION_DAYS = "DataRetentionDays";
        private const string CONFIG_PROCESS_IMAGE_RETENTION_DAYS = "ImageRetentionDays";
        private const string CONFIG_PROCESS_IMAGE_RPS_RESPONSE_FOLDER = "ImageRPSResponseFolder";
        private const string CONFIG_PROCESS_DEFAULT_PAYMENT_TYPE = "DefaultPaymentType";
        private const string CONFIG_PROCESS_DEFAULT_BATCH_SOURCE = "DefaultPaymentSource";
        private const string CONFIG_PROCESS_DEFAULT_SITE_CODE = "DefaultSiteCode";
        private const string CONFIG_PROCESS_PREVIOUS_RPS_QUEUE_FILE_VERSION_ALLOWED = "PreviousRPSQueueFileAllowed";
        private const string CONFIG_PROCESS_SKIP_DEBIT = "SkipDebit";
        private const string CONFIG_PROCESS_SEND_END_POINT = "SendEndPoint";
        private const string CONFIG_PROCESS_STATUS_END_POINT = "StatusEndPoint";

        [ConfigurationProperty(CONFIG_PROCESS_CLIENTPROCESSCODE_KEY, DefaultValue = "Undefined", IsKey = true, IsRequired = true)]
        public string ClientProcessCode {
            get {
                return ((string)(base[CONFIG_PROCESS_CLIENTPROCESSCODE_KEY]));
            }
        }

      
        [ConfigurationProperty(CONFIG_PROCESS_XSL_FILE_PATH_KEY, DefaultValue="", IsKey=true, IsRequired=false)]
        public string ToCanonicalXSLFile {
            get {
                return ((string) (base[CONFIG_PROCESS_XSL_FILE_PATH_KEY]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_FILE_TYPE_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string FileType {
            get {
                return ( (string)( base[ CONFIG_PROCESS_FILE_TYPE_KEY ] ) );
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_INPUT_PATH_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string InputFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_INPUT_PATH_KEY ] ) );
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_INPROCESS_PATH_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string InProcessFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_INPROCESS_PATH_KEY ] ) );
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_PENDING_RESPONSE_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string PendingResponseFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_PENDING_RESPONSE_FOLDER_KEY ] ) );
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_RESPONSE_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ResponseFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_RESPONSE_FOLDER_KEY ] ) );
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_INPUT_ERROR_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string InputErrorFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_INPUT_ERROR_FOLDER_KEY ] ) );
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_RESPONSE_ERROR_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ResponseErrorFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_RESPONSE_ERROR_FOLDER_KEY ] ) );
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_ARCHIVE_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ArchiveFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_ARCHIVE_FOLDER_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_FILE_PATTERN_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string FilePattern {
            get {
                return ( (string)( base[ CONFIG_PROCESS_FILE_PATTERN_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_XCLIENT_DLL_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string XClientDllFilePath {
            get {
                return ( (string)( base[ CONFIG_PROCESS_XCLIENT_DLL_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_INI_SECTION_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string IniSectionKey {
            get {
                return ( (string)( base[ CONFIG_PROCESS_INI_SECTION_KEY ] ) );
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_BANK_NAME, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string BankName
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_BANK_NAME]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_DEFAULT_BANK_ID, DefaultValue = "-1", IsKey = false, IsRequired = false)]
        public string DefaultBankId
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_DEFAULT_BANK_ID]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_DEFAULT_ORGANIZATION_ID, DefaultValue = "-1", IsKey = false, IsRequired = false)]
        public string DefaultOrganizationId
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_DEFAULT_ORGANIZATION_ID]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_IMAGE_RETENTION_DAYS, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ImageRetentionDays
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_IMAGE_RETENTION_DAYS]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_DATA_RETENTION_DAYS, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string DataRetentionDays
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_DATA_RETENTION_DAYS]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_IMAGE_RPS_RESPONSE_FOLDER, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ImageRPSResponseFolder
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_IMAGE_RPS_RESPONSE_FOLDER]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_DEFAULT_PAYMENT_TYPE, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string DefaultPaymentType
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_DEFAULT_PAYMENT_TYPE]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_DEFAULT_BATCH_SOURCE, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string DefaultBatchSource
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_DEFAULT_BATCH_SOURCE]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_DEFAULT_SITE_CODE, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string DefaultSiteCode
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_DEFAULT_SITE_CODE]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_PREVIOUS_RPS_QUEUE_FILE_VERSION_ALLOWED, DefaultValue = "false", IsKey = false, IsRequired = false)]
        public string PreviousRpsQueueFileVersionAllowed
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_PREVIOUS_RPS_QUEUE_FILE_VERSION_ALLOWED]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_SKIP_DEBIT, DefaultValue = "false", IsKey = false, IsRequired = false)]
        public bool SkipDebit
        {
            get
            {
                return ((bool)(base[CONFIG_PROCESS_SKIP_DEBIT]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_SEND_END_POINT, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string SendEndPoint
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_SEND_END_POINT]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_STATUS_END_POINT, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string StatusEndPoint
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_STATUS_END_POINT]));
            }
        }
    }
}
