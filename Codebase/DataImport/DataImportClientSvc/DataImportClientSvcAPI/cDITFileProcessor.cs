using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.Xml.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO.Compression;
using System.Runtime.CompilerServices;
using Wfs.Logging;
using WFS.LTA.Common;
using WFS.LTA.Common.FileCollector;
using WFS.LTA.DataImport.DataImportIconClient;
using WFS.LTA.DataImport.DataImportServiceClient;
using WFS.LTA.DataImport.DataImportXClientBase;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.AuditingServicesClient;
using WFS.RecHub.R360BaseResponse;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

/******************************************************************************
** Wausau
** Copyright ? 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/13/2012
*
* Purpose:  This is the master class of the Data Import Client.  It house the business logic need
*           to take an input file through all of its translations and then place the contents in
*           queue for uploading
*
* Modification History
* Created
* CR 52267 CEJ 07/19/2012
*   -DIT Client
* WI 34771 WJS 11/26/2012
 *   - Add support for pause client
* WI 70280 WJS 12/12/2012
*    - Add support for archive images
* WI 92398 WJS 3/15/2013
*    - Add check for file failing to move to in process folder since it already exists. Allow the service to still start up
 * WI 90202 WJS 03/04/2013
 *    -FP:Add support for archive images
* WI 126987 CMC 01/20/2014
*  - Updated to use 2.00 libraries.
* WI 129729 CMC 02/14/2014 
*   - Adding additional config attributes 
*   (BankName, DefaultBankID, DefaultLockboxID, DataRetentionDays, ImageRetentionDays, and ImageRPSResponseFolder)
* WI 131219 CMC 02/28/2014
*    - Fix WriteResponse check for comparing to ICON enum.
* WI 137153 TWE 04/16/2014
*    FP-move file from inprocess folder
*    FP-remove file from pending response folder
* WI 135681 CMC 04/21/2014
*   - Support for Input Error Folder  
* WI 140511 CMC 05/07/2014
*   - Front/Back image processing
* WI 143271, 143409 CMC 05/29/2014
*   - Support for default payment type and batch source
* WI 143271, 143409 CMC 05/29/2014
*   - Support for default payment type and batch source
* WI 148294 CMC 06/17/2014
*   - Change DefaultLockboxID to DefaultOrganizationID
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class cDITFileProcessor
    {
        private Dictionary<string, cDataImportXClientBase> _dctStreamToXMLDLLs = new Dictionary<string, cDataImportXClientBase>();
        private bool _bHasIconProcessing = false;

	    public delegate bool PauseProcessingDelegate();

	    public PauseProcessingDelegate CheckForPause = null;

		private bool IsPaused()
	    {
		    if (CheckForPause != null)
		    {
			    return CheckForPause();
		    }

		    return false;
	    }

        private cFileCollector _flcFileWatcher;
        private cDITUploadManager _dumUploadPort;
        private DataImportConnect _dicLinkToDITService;
        private AuditingServicesClient _auditingServicesClient;

        private cDimFieldLists _dctDimFieldLists = null;
        private Dictionary<string, string> _dctDocumentType = null;

        private System.Timers.Timer _tmrDimListsUpdate=null;
        private DateTime? _dttImageAliasLastUpdated = null;

        private cProcessOptionsList _ProcessOptionsList = null;
        private cInboxPathData sourceData = null;
        private const string ProcessingExceptionEventName = "ProcessingException";
        private const string AuditApplicationName = "DIT";
        private const string AuditFileSubmissionEventType = "File Submission";
        private const string AuditFileSubmissionSuccessfulEventName = "Moved To Pending Response Folder";
        private const string AuditFileSubmissionFailureEventName = "Moved To Input Error Folder";
	    private const int _sleepInterval = 30 * 1000; // 30,000 miliseconds ==> 30 seconds
        /// <summary>
        /// 
        /// </summary>
        private cProcessOptionsList ProcessOptionsList {
            get {
                try {
                    if (_ProcessOptionsList == null) { 
                        _ProcessOptionsList = new cProcessOptionsList();
                    }
                } catch(Exception ex) {
                    LogException("An unexpected exception occurred in ProcessOptionsList()", ex);
                }

                return _ProcessOptionsList;
            }
        }
        
        //Log: public cDITFileProcessor(ltaLog lgoLogObject)
        public cDITFileProcessor()
        {

        }

        private void Log(string message, string caller, LTAMessageType messageType,
            LTAMessageImportance messageImportance)
        {
            GblUtil.OnOutputLogAndTrace(message, caller, messageType, messageImportance);
        }
        private void LogDebug(string message, [CallerMemberName] string caller = null)
        {
            Log(message, caller, LTAMessageType.Information, LTAMessageImportance.Debug);
        }
        private void LogError(string message, [CallerMemberName] string caller = null)
        {
            Log(message, caller, LTAMessageType.Error, LTAMessageImportance.Essential);
        }
        private void LogException(string message, Exception exception, [CallerMemberName] string caller = null)
        {
            GblUtil.OnOutputLogAndTrace(message, caller, exception);
        }
        private void LogInfo(string message, [CallerMemberName] string caller = null)
        {
            Log(message, caller, LTAMessageType.Information, LTAMessageImportance.Essential);
        }
        private void LogStartupInfo(string message)
        {
            Log(message, "", LTAMessageType.Information, LTAMessageImportance.Essential);
        }
        private void LogVerbose(string message, [CallerMemberName] string caller = null)
        {
            Log(message, caller, LTAMessageType.Information, LTAMessageImportance.Verbose);
        }
        public bool Initialize(IWfsLog logger, out string sErrorMessage)
        {
            bool bAns = false;

            sErrorMessage = string.Empty;
            LogVerbose("Starting");

            try
            {
                string sSettingErrors;
                bool bValidate = ValidateSettings(out sSettingErrors);
                if (!bValidate)
                {
                    LogInfo("Error validating settings. Error are:" + sSettingErrors);
                    return bAns;
                }
                OnOutputClientProcessList();
                LogVerbose("Settings:");
                if (ProcessOptionsList != null)
                {
                    string siteKey = System.Configuration.ConfigurationManager.AppSettings["SiteKey"];
                    foreach (_SourcePathBase xproc in ProcessOptionsList)
                    {
                        LogVerbose("Connection to DIT for server location:" + GblUtil.ClientOptions.ServerLocation + " For Input Folder is " + xproc.InputFolder + " SiteKey: " + siteKey);
                    }

	                var success = false;
	                int retries = 0;
	                do
	                {
		                try
		                {
			                if (IsPaused())
			                {
				                LogVerbose("Service is paused.");
								Thread.Sleep(3000);
				                continue;
			                }

							//Connect to DIT Service
			                retries++;
			                LogVerbose("Attempting to connect to DITService.");
						   _dicLinkToDITService = new DataImportConnect(new ConnectionContext
			                {
				                SiteKey = siteKey,
				                ServerNameLocation = GblUtil.ClientOptions.ServerLocation,
				                Entity = string.Empty,
				                Logger = logger,
				                LogonName = GblUtil.ClientOptions.LogonName,
				                Password = GblUtil.ClientOptions.Password,
			                });
			                _dicLinkToDITService.LogOn();
			                success = true;
		                }
						catch (Exception e)
		                {
							LogException($"Failed to connect to DIT Service, try {retries}.  Retrying in {_sleepInterval/1000} seconds.", e);
			                Thread.Sleep(_sleepInterval);
		                }
	                } while (success == false);

	                //set up the connection to the DIT Auditing Service
                    _auditingServicesClient = new AuditingServicesClient();

                    LogVerbose("Got Session Established");

                    /*If a successful connection has been made to the DIT service, get the upload manager ready,
                        get the file watchers going, and populate dim lists
                    */
                    _dumUploadPort = new cDITUploadManager(_dicLinkToDITService, ProcessOptionsList,
                        GblUtil.ClientOptions.UploadStatusUpdateFrequency, _auditingServicesClient);

                    LogVerbose("Restore In Process Items");
                    //need to update any that got removed before start looking for the
                    CheckForItemsWhichDidNotFinishProcessingLastTimeWasRunning();
                    LogVerbose("Finish Restore In Process Items");

                    //Set up XMLToStream DLLs for each data source and Dim Field Lists for each Batch Source Name
                    foreach (cInboxPathData ipdCurInputPath in ProcessOptionsList)
                    {
                        if (GetXClient(ipdCurInputPath) is IDataImportIconClient)
                        {
                            LogVerbose("Populating dim field list for icon client list " + ipdCurInputPath.ClientProcessCode);

                            _bHasIconProcessing = true;
                            _dctDimFieldLists = new cDimFieldLists();
                        }
                    }

                    //If we have Icon Processing then set up a timer to update the Dim Lists including the document types
                    if (_bHasIconProcessing)
                    {
                        _tmrDimListsUpdate = new System.Timers.Timer(GblUtil.ClientOptions.UpdateDimFieldListsFrequency * 60000);
                        _tmrDimListsUpdate.Elapsed += _tmrDimListsUpdate_Elapsed;
                        LogVerbose("Setup time for dim lists at frequency of: " + GblUtil.ClientOptions.UpdateDimFieldListsFrequency.ToString() + " minutes");

                        _tmrDimListsUpdate.Enabled = true;
                    }

                    //Start up the file watcher
                    _flcFileWatcher = new cFileCollector(ProcessOptionsList.ToArray(), GblUtil.OnOutputLogAndTrace);
                    _flcFileWatcher.SetWorkAvailEvent(_flcFileWatcher_InputFileAvailable);
                    LogVerbose("Setup file watcher");

                    bAns = true;
                }
                else
                {
                    LogError("Unable to get process option list establish. It is null.");
                }
            }
            catch (Exception ex)
            {
                LogException("Failed to establish base objects", ex);
                throw new Exception(string.Format("Failed to establish base objects: {0}", ex.Message), ex);
            }
            return bAns;
        }


        private void OnOutputClientProcessList() {


            try {

                foreach(cInboxPathData xproc in this.ProcessOptionsList) {

                    LogStartupInfo("Client Process code: " + xproc.ClientProcessCode);
                    LogStartupInfo("  Type: " + xproc.FileType);

                    LogStartupInfo("  Input Folder: " + xproc.InputFolder);
                    LogStartupInfo("  InProcess Folder: " + xproc.InProcessFolder);
                    LogStartupInfo("  Pending Response Folder: " + xproc.PendingResponseFolder);
                    LogStartupInfo("  Response Folder: " + xproc.ResponseFolder);
                    LogStartupInfo("  Archive Folder: " + xproc.ArchiveFolder);
                    LogStartupInfo("  Input Error Folder: " + xproc.InputErrorFolder);
                    LogStartupInfo("  Response Error Folder: " + xproc.ResponseErrorFolder);
                    LogStartupInfo("  File Pattern: " + xproc.FilePattern);

                    if( String.IsNullOrEmpty(xproc.SendEndPoint) )
                        LogStartupInfo("  XClientDllFilePath: " + xproc.XClientDllFilePath);
                    else LogStartupInfo($"  Send End Point: {xproc.SendEndPoint}");

                    LogStartupInfo("");
                }

            } catch(Exception ex) {
                LogException("An unexpected exception occurred in OnOutputClientProcessList()", ex);
            }
        }


      

        private void _tmrDimListsUpdate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _tmrDimListsUpdate.Enabled = false;
            UpdateDimFieldLists();
            _tmrDimListsUpdate.Enabled = true;
        }

    

        //Updates the Dim List Batch Fields, Item Fields and Document Types
        private void UpdateDimFieldLists()
        {
            try
            {
                DateTime dttNewUpdateDate = DateTime.Now;

                LogVerbose("Getting Document Types");
                _dumUploadPort.UpdateDocumentTypes(ref _dttImageAliasLastUpdated, ref _dctDocumentType);
                LogVerbose("Doc Type size is: " + _dctDocumentType.Count);
                if (_dctDimFieldLists != null)
                {
                    LogVerbose("Getting Batch Item Field");
                    LogVerbose("Update Batch Field");
                    _dumUploadPort.UpdateBatchFields(_dctDimFieldLists.LastUpdated, _dctDimFieldLists.BatchFieldList);
                    LogVerbose("  list size is " + _dctDimFieldLists.BatchFieldList.Count);
                    LogVerbose("Update Item Field");
                    _dumUploadPort.UpdateItemFields(_dctDimFieldLists.LastUpdated, _dctDimFieldLists.ItemFieldList);
                    LogVerbose("  list size is " + _dctDimFieldLists.ItemFieldList.Count);

                    _dctDimFieldLists.LastUpdated = dttNewUpdateDate;
                }
           }
            catch (Exception ex)
            {
                LogException("Failed to updated UpdateDimFieldLists", ex);
            }
        }

        public bool ValidateSettings(out string sErrorMessages)
        {
            string sTemp = string.Empty;
            List<string> lstErrorMessages = new List<string>();

            sErrorMessages = string.Empty;

            try
            {

                if (String.IsNullOrEmpty(GblUtil.ClientOptions.LogonName))
                    lstErrorMessages.Add("No User Name is given for the Data Import Service");
                if (String.IsNullOrEmpty(GblUtil.ClientOptions.Password))
                    lstErrorMessages.Add("No Password is given for the Data Import Service");

                if (GblUtil.ClientOptions.logFileMaxSize < 1)
                    lstErrorMessages.Add("The log file max size must not be less than one");

                if (!GblUtil.IsValueInRange(GblUtil.ClientOptions.UpdateDimFieldListsFrequency, 0, 86400, out sTemp))
                    lstErrorMessages.Add(sTemp);
                if (!GblUtil.IsValueInRange(GblUtil.ClientOptions.UploadStatusUpdateFrequency, 0, 86400, out sTemp))
                    lstErrorMessages.Add(sTemp);

                if (String.IsNullOrEmpty(GblUtil.ClientOptions.ServerLocation))
                    lstErrorMessages.Add("The Data Import Service URL has not been specified");

                if (GblUtil.ClientOptions.UploadStatusUpdateFrequency < 0)
                    lstErrorMessages.Add("Upload Status Update Frequency must be more than 0");

            }
            catch (Exception ex)
            {
                lstErrorMessages.Add(string.Format("{0}",  ex.Message));
            }
            sErrorMessages = string.Join("\r\n", lstErrorMessages);
            return sErrorMessages == string.Empty;
        }

        //After work has lulled and then input comes in this event wakes up the process
        private void _flcFileWatcher_InputFileAvailable(object objSender, cInputFile inputFile)
        {
            try
            {
                LogInfo("Found a file being processed: " + inputFile.FileData.FullName);

                ProcessFile(inputFile);
            }
            catch (Exception ex)
            {
                LogException("_flcFileWatcher_InputFileAvailable", ex);
            }
        }
        protected bool MoveFile(string fromFilePath, string toFilePath, out FileInfo movedFileInfo)
        {

            bool bolRetVal;
            int intFileIncrementer = 1;
            string strIncrementedToFilePath;
            FileInfo fiMovedFileInfo;

            LogDebug($"MoveFile: fromFilePath: {fromFilePath}, toFilePath: {toFilePath}");

            try
            {
                if (File.Exists(toFilePath))
                {
                    LogDebug($"MoveFile: {toFilePath} existed");
                    while (true)
                    {
                        FileInfo fiToFile = new FileInfo(toFilePath);
                        strIncrementedToFilePath = Path.Combine(fiToFile.DirectoryName, Path.GetFileNameWithoutExtension(toFilePath) + " - Copy (" + intFileIncrementer.ToString() + ")" + Path.GetExtension(toFilePath));
                        if (File.Exists(strIncrementedToFilePath))
                        {
                            intFileIncrementer++;
                        }
                        else
                        {
                            bolRetVal = GblUtil.MoveFile(fromFilePath, strIncrementedToFilePath);
                            fiMovedFileInfo = new FileInfo(strIncrementedToFilePath);
                            break;
                        }
                    }
                }
                else
                {
                    LogDebug($"MoveFile: {toFilePath} did not exist, calling GblUtil.MoveFile");
                    if (GblUtil.MoveFile(fromFilePath, toFilePath))
                    {
                        fiMovedFileInfo = new FileInfo(toFilePath);
                        bolRetVal = true;
                    }
                    else
                    {
                        fiMovedFileInfo = null;
                        bolRetVal = false;
                    }
                }



            }
            catch (Exception ex)
            {

                fiMovedFileInfo = null;
                bolRetVal = false;
                throw ex;
            }

            movedFileInfo = fiMovedFileInfo;

            if (bolRetVal)
                LogDebug($"MoveFile: returning file name: {movedFileInfo.FullName}");
            else
                LogDebug("MoveFile: returning false");

            return (bolRetVal);
        }
        private bool MoveFile(string fromFilePath, string toFilePath)
        {

            bool bolContinue = true;
            int intRetryCount = 0;
            bool bolRetVal = false;

            while (bolContinue)
            {

                try
                {
                    File.Move(fromFilePath, toFilePath);
                    bolRetVal = true;
                    bolContinue = false;
                }
                catch (Exception ex)
                {

                    if (intRetryCount < 3)
                    {
                        intRetryCount++;

                    }
                    else
                    {

                        bolContinue = false;
                        throw ex;
                    }
                }
            }

            return (bolRetVal);
        }

        private bool MoveFileToInProcess(cInputFile inputFile, Guid trackingID, out FileInfo inProcessFile)
        {

            bool bolRetVal;
            Guid gidTemporarySourceTrackingID;
            string strInProcessFileName = string.Empty;
            FileInfo fiInProcessFile;
            FileInfo pendingFileInfo = inputFile.FileData;
            inProcessFile = null;
            LogVerbose("Moving Job File to InProcess folder..." + pendingFileInfo.Name);

            try
            {
                gidTemporarySourceTrackingID = trackingID == Guid.Empty ? Guid.NewGuid() : trackingID;
                strInProcessFileName = Path.Combine(inputFile.SourcePathData.InProcessFolder, pendingFileInfo.Name + "." + gidTemporarySourceTrackingID.ToString() + ".inprocess");
                MoveFile(pendingFileInfo.FullName, strInProcessFileName, out fiInProcessFile);
                inProcessFile = fiInProcessFile;
                bolRetVal = true;
            }
            catch (Exception ex)
            {
                LogException("An unexpected exception occurred in MoveFileToInProcess()", ex);
                fiInProcessFile = null;
                bolRetVal = false;
            }

          

            return (bolRetVal);
        }

        private bool MoveFileToPendingResponse(cInputFile inputFile, Guid transmissionID)
        {

            bool bolRetVal;
            FileInfo pendingFileInfo = inputFile.FileData;
            try
            {
                string pendingResponseFileName = Path.Combine(inputFile.SourcePathData.PendingResponseFolder, pendingFileInfo.Name + ".-" + transmissionID.ToString() + "-.pendingResponse");
                //must archive it first before moving it or the compress will not work
                ArchiveItem(inputFile);
                GblUtil.MoveFile(pendingFileInfo.FullName, pendingResponseFileName);
                File.WriteAllText(pendingResponseFileName, inputFile.SubmittedXML);
                bolRetVal = true;
                LogAuditEvent
                    (new AuditRequest
                        {
                            ApplicationName = AuditApplicationName,
                            EventName = AuditFileSubmissionSuccessfulEventName,
                            EventType = AuditFileSubmissionEventType,
                            Message = string.Format("Submitted file '{0}' and awaiting response.", pendingResponseFileName),
                        }
                    );
            }
            catch (Exception ex)
            {
                LogException("An unexpected exception occurred in MoveFileToPendingResponse()", ex);
                bolRetVal = false;
            }
            return (bolRetVal);
        }
        private FileInfo Compress(FileInfo fi)
        {
            string logMessage;
            FileInfo returnFileInfo = null;
            string outPutFileName = string.Empty;
            try
            {
                // Get the stream of the source file.
                using (FileStream inFile = fi.OpenRead())
                {
                    // Prevent compressing hidden and 
                    // already compressed files.
                    if ((File.GetAttributes(fi.FullName)
                                & FileAttributes.Hidden)
                                != FileAttributes.Hidden & fi.Extension != ".gz")
                    {
                        outPutFileName = Path.Combine(fi.FullName + ".gz");
                        // Create the compressed file.
                        using (FileStream outFile =
                                    File.Create(outPutFileName))
                        {
                            using (GZipStream Compress =
                                        new GZipStream(outFile,
                                            CompressionMode.Compress))
                            {
                                // Copy the source file into 
                                // the compression stream.
                                inFile.CopyTo(Compress);

                                logMessage = String.Format("Compressed {0} from {1} to {2} bytes.",
                                    fi.Name, fi.Length.ToString(), outFile.Length.ToString());
                                LogDebug(logMessage);
                                returnFileInfo = new FileInfo(outPutFileName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogException("Exception in compress", ex);
                throw ex;
            }
            return returnFileInfo;
        }

        private void ArchiveItem(cInputFile inputFile)
        {
            try
            {
                FileInfo rtnArchiveInfo = null;
                if (String.IsNullOrEmpty(inputFile.SourcePathData.ArchiveFolder))
                {
                    LogDebug("Archive Folder is empty. Delete the file from in process");
                    File.Delete(inputFile.FileData.FullName);
                }
                else
                {
                    rtnArchiveInfo = Compress(inputFile.FileData);
                    if (rtnArchiveInfo != null)
                    {
                        string newFileName = Path.Combine(inputFile.SourcePathData.ArchiveFolder, rtnArchiveInfo.Name);
                        MoveFile(rtnArchiveInfo.FullName, newFileName); ;
                        LogDebug("Moved filed to archive file name:" + newFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException("Exception trying to archive", ex);
            }
        }

        private bool ProcessFile(cInputFile inputFile)
        {
            bool bolRetVal = false ;
            TrackingKeys recordTrackingKeys = new TrackingKeys
            {
                AuditDateKey = int.Parse(DateTime.Now.ToString("yyyyMMdd")),
                SourceTrackingId = Guid.NewGuid(),
                StopWatch = Stopwatch.StartNew()
            };

            inputFile.WriteLineToResult(string.Format("Starting at {0}", DateTime.Now));
            inputFile.WriteLineToResult(string.Format("More detail can be found in the log file located at {0}", GblUtil.ClientOptions.logFilePath));

            try
            {
                lock (this)
                {
                    var config = (cInboxPathData)inputFile.SourcePathData;
                    var serverside = !String.IsNullOrEmpty(config.SendEndPoint);
                    FileInfo pendingFileInfo = inputFile.FileData;

                    bolRetVal = serverside
                        ? ProcessServerSide(inputFile, recordTrackingKeys)
                        : ProcessWorkItem(inputFile, recordTrackingKeys);
                }
            }
            catch (Exception ex)
            {
                FileInfo pendingFileInfo = inputFile.FileData;
                LogException("An unexpected exception occurred in ProcessFile()", ex);
                MoveFileToError(inputFile, pendingFileInfo, recordTrackingKeys.SourceTrackingId);
                bolRetVal = false;
            }

            LogInfo(string.Empty);

            return (bolRetVal);
        }

        private void MoveFileToError(cInputFile inputFile, FileInfo pendingFileInfo, Guid sourceTrackingId)
        {
            LogInfo("Error processing. Move to error");
            string errorFile = Path.Combine(inputFile.SourcePathData.InputErrorFolder, pendingFileInfo.Name + "." + sourceTrackingId.ToString() + ".error");
            MoveFile(inputFile.FileData.FullName, errorFile, out pendingFileInfo);
            LogInfo("Moved File to Error:" + pendingFileInfo.FullName);
            var message =
                string.Format("There was an issue processing file {0}.  The file has been moved to {1}, and is named {2}",
                    inputFile.FileData.FullName, inputFile.SourcePathData.InputErrorFolder, errorFile);
            var alertService = new AlertManager(_dicLinkToDITService);
            alertService.LogAlert(ProcessingExceptionEventName, message);
            LogAuditEvent
                (
                    new AuditRequest 
                    {
                        ApplicationName = AuditApplicationName,
                        EventName = AuditFileSubmissionFailureEventName,
                        EventType = AuditFileSubmissionEventType,
                        Message = message 
                    }
                );
        }

        public void LogAuditEvent(AuditRequest requestContext)
        {
            var response = _dicLinkToDITService.LogAuditEvent(requestContext);

            if (response.Errors.FirstOrDefault() != null)
            {
                foreach (var error in response.Errors)
                {
                    LogInfo(error);
                }
            }
        }

        protected virtual XmlDocument BuildCanonicalXml(cInputFile inputFile, cInboxPathData ipdSourceData, StreamReader smrReader, out bool bTranslateXMLDocument)
        {
            XmlDocument xmdCurDocument = null;
            bTranslateXMLDocument = false;
            try
            {
                if (string.IsNullOrEmpty(ipdSourceData.XClientDllFilePath))
                {
                    inputFile.WriteLineToResult("No Translation");

                    xmdCurDocument = new XmlDocument();
                    xmdCurDocument.SafeLoadXml(smrReader);
                    inputFile.WriteLineToResult("XML Successfully Loaded");
                }
                else
                {
                    cDataImportXClientBase xClient = GetXClient(ipdSourceData);

                    if (xClient == null)
                        throw new Exception(string.Format("The given XClientDllFilePath ({0}) was not found for source path {1}",
                                ipdSourceData.XClientDllFilePath,
                                inputFile.SourcePathData.InputFolder));
                    else
                    {
                        xmdCurDocument = BuildCanonicalXmlUsingXClient(inputFile, smrReader, ipdSourceData, xClient);
                        bTranslateXMLDocument = true;
                    }
                }
            }
            catch (Exception ex)
            {
                 //If the file is not to be translated to XML and the contents fails to load as XML
                //  it is a bad file so we just need to log it and move the file to the error folder
                LogException(ex.Message, ex);
                inputFile.WriteLineToResult("XML Failed to Load");
                xmdCurDocument = null;
            }
            return xmdCurDocument;
        }

        protected bool ProcessServerSide(cInputFile inputFile, TrackingKeys recordTrackingKeys)
        {
            var ret = false;

            // first thing is to wait until the file is ready to be processed. 
            // (meaning, it's readable, and isn't locked by windows)
            var retries = 1;
            var locked = false;
            while (!locked && retries < 5)
            {
                retries++;
                locked = GblUtil.IsFileClosed(inputFile.OriginalInputFileName);
                if (!locked)
                {
                    LogDebug("File is currently locked, waiting to see if it opens...");
                    Thread.Sleep(500);
                }
            }

            // File is all set, now we get to push it to the server.
            LogDebug("Sending File to DIT Service for processing.");

            if (!String.IsNullOrEmpty(inputFile.FileData.FullName))
            {
                sourceData = (cInboxPathData)inputFile.SourcePathData;

                HttpResponseMessage response = null;

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));

                    //Send the file to the DIT service
                    using (var content = new MultipartFormDataContent())
                    {
                        var fileContent = new StreamContent(File.OpenRead(inputFile.FileData.FullName));
                        content.Add(new StringContent(sourceData.ClientProcessCode), "clientprocesscode");
                        content.Add(new StringContent(sourceData.DefaultSiteCode.ToString()), "BatchSiteCode");
                        content.Add(new StringContent(sourceData.DefaultPaymentType), "PaymentType");
						content.Add(new StringContent(sourceData.DefaultBatchSource), "BatchSource");

                        content.Add(fileContent, "formfile", recordTrackingKeys.SourceTrackingId.ToString());
                        response = client.PostAsync(sourceData.SendEndPoint, content).Result;
                    }
                }

                if (response.IsSuccessStatusCode)
                {
                    LogDebug($"Successfully sent {inputFile.FileData.FullName} to DIT Service");
                    using (HttpContent content = response.Content)
                    {
                        var result = JObject.Parse(content.ReadAsStringAsync().Result);
                        var id = result
                            ?.Value<JObject>("data")
                            ?.Value<string>("fileId");
                        recordTrackingKeys.SourceTrackingId = Guid.Parse(id);
                    }

                    // Now move to in-processing so that the next process can start checking the status of the item.
                    if (MoveFileToInProcess(inputFile, recordTrackingKeys.SourceTrackingId, out FileInfo inProcessFile))
                    {
                        LogDebug($"Successfully moved {inputFile.FileData.FullName} to In-Processing for Status Checking.");
                        ret = true;
                    }
                    else
                    {
                        LogError($"Failed to move {inputFile.FileData.FullName} to In-Processing for Status Checking.");
                    }

                }
                else
                {
                    //MeasureWork("DITServerSideFile", recordTrackingKeys,
                    //    Path.GetFileName(inputFile.OriginalInputFileName), sourceData.ClientProcessCode, null,
                    //    false);
                    LogError($"Error calling DIT REST Service: {response.ReasonPhrase}");
                    MoveFileToError(inputFile, inputFile.FileData, recordTrackingKeys.SourceTrackingId);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(inputFile.FileData.FullName));
            }

            return ret;
        }

        protected bool ProcessWorkItem(cInputFile inputFile, TrackingKeys recordTrackingKeys)
        {
            bool bAns = false;

            bool bTranslateXMLDocument = false;
            XmlDocument xmdCurDocument = null;
            sourceData = (cInboxPathData)inputFile.SourcePathData;
            FileInfo fiInProcess;

            // Verify that the pending input file exists, and move it to the inprocess folder.
            if (File.Exists(inputFile.OriginalInputFileName) && MoveFileToInProcess(inputFile, recordTrackingKeys.SourceTrackingId, out fiInProcess))
            {
                inputFile.FileData = fiInProcess;

                try
                {
                    if (!String.IsNullOrEmpty(inputFile.FileData.FullName))
                    {
                        using (StreamReader smrReader = new StreamReader(inputFile.FileData.FullName))
                        {
                            xmdCurDocument = BuildCanonicalXml(inputFile, sourceData, smrReader, out bTranslateXMLDocument);
                            //if xmdCurDocument is null then there is probably something wrong with the input file and we will need to 
                            //      skip it
                            if (xmdCurDocument == null)
                            {
                                LogError($"The contents of ({inputFile.FileData.FullName}) are empty or in error");
                            }
                            if (bTranslateXMLDocument)
                            {
                                xmdCurDocument = ApplyDefaults(xmdCurDocument);//apply default configurations

                                //Place document into upload queue if you have translated the XML Document or transformed it

                                if (_dumUploadPort.UploadXML(xmdCurDocument, sourceData.ClientProcessCode, inputFile, recordTrackingKeys))
                                {
                                    MeasureWork("DITProcessFile", recordTrackingKeys, Path.GetFileName(inputFile.OriginalInputFileName), sourceData.ClientProcessCode, xmdCurDocument);
                                    inputFile.WriteLineToResult("XML Started Uploading");
                                    bAns = true;
                                }
                                else
                                {
                                    MeasureWork("DITProcessFile", recordTrackingKeys, Path.GetFileName(inputFile.OriginalInputFileName), sourceData.ClientProcessCode, xmdCurDocument, false);
                                    inputFile.WriteLineToResult("XML Failed to upload");
                                    LogError($"Failed to upload file: {inputFile.FileData.Name}");
                                    //replacing with measures
                                    //SetEndProcessingAuditingRecord(recordTrackingKeys, false);
                                }
                            }
                            else
                            {
                                MeasureWork("DITProcessFile", recordTrackingKeys, Path.GetFileName(inputFile.OriginalInputFileName), sourceData.ClientProcessCode, xmdCurDocument, false);
                                LogError("The XClient failed to translate the input file");
                                //replacing with measures
                                //SetEndProcessingAuditingRecord(recordTrackingKeys, false);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MeasureWork("DITProcessFile", recordTrackingKeys, Path.GetFileName(inputFile.OriginalInputFileName), sourceData.ClientProcessCode, xmdCurDocument, false);
                    LogError("The XClient failed to translate the input file.");
                    //replacing with measures
                    //SetEndProcessingAuditingRecord(recordTrackingKeys, false);
                    throw ex;
                }

                if (bAns && MoveFileToPendingResponse(inputFile, recordTrackingKeys.SourceTrackingId))
                {
                    LogDebug("Successfully processed. Moved to pending Responses.");
                }
                else if (!bAns)
                {
                    LogDebug("Failed to upload or process the XML file.");
                }
                else
                {
                    LogDebug("Successfully processed file, but failed to move the file to Pending Response.");
                    bAns = false;
                }

            }
  
            return bAns;
        }

        protected virtual void MeasureWork(string measurementTypeName, TrackingKeys recordTrackingKeys, string sourceFileName, string clientProcessingCode, XmlDocument xmlDocument, bool isSuccessful = true)
        {
            recordTrackingKeys.StopWatch.Stop();
            var fileImportMeasures = new BatchFileImportMeasures
            {
                MeasurementTypeName = measurementTypeName,
                IsSuccessful = isSuccessful,
                Duration = recordTrackingKeys.StopWatch.ElapsedMilliseconds,
                SourceFileName = sourceFileName,
                ClientProcessingCode = clientProcessingCode,
                SourceTrackingId = recordTrackingKeys.SourceTrackingId
            };
            MeasureWorkGetXMLData(fileImportMeasures,xmlDocument);
            // call service to send the measure
            _dumUploadPort.SendBatchMeasureData(fileImportMeasures);
        }

        private void MeasureWorkGetXMLData(BatchFileImportMeasures fileImportMeasures, XmlDocument xmlDocument)
        {
            var batchNodes = xmlDocument.SelectNodes("Batches/Batch");

            if (batchNodes == null) return;

            foreach (XmlNode batchNode in batchNodes)
            {
                fileImportMeasures.BatchFileImportMeasure.Add(new BatchFileImportMeasure
                {
                    SourceBatchId = cCanonXMLTools.GetLongAttribute(batchNode, "BatchID"),
                    SiteBankId = cCanonXMLTools.GetIntAttribute(batchNode, "BankID"),
                    SiteWorkgroupId = cCanonXMLTools.GetIntAttribute(batchNode, "ClientID"),
                    DepositDateKey =
                        int.Parse(cCanonXMLTools.GetDateTimeAttribute(batchNode, "DepositDate").ToString("yyyyMMdd")),
                    ImmutableDateKey =
                        int.Parse(cCanonXMLTools.GetDateTimeAttribute(batchNode, "BatchDate").ToString("yyyyMMdd")),
                    BatchTrackingId = cCanonXMLTools.GetGuidAttribute(batchNode, "BatchTrackingID"),
                    TransactionCount = cCanonXMLTools.GetNodeCount(batchNode, "Transaction"),
                    PaymentCount = cCanonXMLTools.GetNodeCount(batchNode, "Transaction/Payment"),
                    DocumentCount = cCanonXMLTools.GetNodeCount(batchNode, "Transaction/Document"),
                    StubCount = cCanonXMLTools.GetNodeCount(batchNode, "Transaction/GhostDocument")
                });
            }
        }
        private void SetInitialAuditingRecord(TrackingKeys recordTrackingKeys)
        {
            //create the initial auditing record for this file.
            try
            {
                LogDebug("  Initial Call to DIT Auditing:  Creating initial auditing record ");

                BaseResponse response = _auditingServicesClient.DitStartProcess(recordTrackingKeys.AuditDateKey,recordTrackingKeys.SourceTrackingId, DateTime.Now);
                LogResponse(response, "Processing Initial Call");
            }
            catch (Exception ex)
            {
                LogError(string.Format("DIT Initial AuditingCall Failed: {0}", ex.Message));
            }
        }

        protected virtual void SetEndProcessingAuditingRecord(TrackingKeys recordTrackingKeys, bool isDataSuccessful)
        {
            //create the End Processing auditing record for this file.
            try
            {
                LogDebug("  Processing End Call to DIT Auditing:  Creating End Processing auditing record, isDataSuccessful = " + isDataSuccessful);

                BaseResponse response = _auditingServicesClient.DitEndProcess(recordTrackingKeys.AuditDateKey, recordTrackingKeys.SourceTrackingId, DateTime.Now, isDataSuccessful);
                LogResponse(response,"Processing End Call");
            }
            catch (Exception ex)
            {
                LogError(string.Format("DIT End Processing AuditingCall Failed: {0}", ex.Message));
            }
        }

        private void LogResponse(BaseResponse response, string theMessage)
        {
            if (response.Status == StatusCode.SUCCESS)
            {
                LogDebug(theMessage + " to DIT Auditing:  Success ");
            }
            else
            {
                LogDebug(theMessage + " to DIT Auditing:  Failed ");
            }
        }

        private XmlDocument ApplyDefaults(XmlDocument xml)
        {
            if (xml == null)
                throw new ArgumentNullException("XML Document cannot be null when attempting to set the defaults.");

            if (string.IsNullOrEmpty(xml.InnerXml))
                throw new ArgumentNullException("XML cannot be null or empty when attempting to set the defaults.");
            var defaultContext = new DefaultContext
            {
                BatchDefaults = new List<DefaultItem>()
                {
                    new DefaultItem{Setting = "BatchSource", Value = sourceData.DefaultBatchSource},
                    new DefaultItem{Setting = "PaymentType", Value = sourceData.DefaultPaymentType},
                    new DefaultItem{Setting = "BankID", Value = sourceData.DefaultBankId.ToString(), DefaultIfZero = true},
                    new DefaultItem{Setting = "BatchSiteCode", Value = sourceData.DefaultSiteCode.ToString(), DefaultIfZero = true}

                },
                ClientSetupDefaults = new List<DefaultItem>()
                {
                    new DefaultItem{Setting = "BatchSource", Value = sourceData.DefaultBatchSource},
                }

            };
            var defaulter = new Defaulter(defaultContext);
            var defaulted =  defaulter.ApplyDefaults(XElement.Parse(xml.InnerXml));
            var xmlDocument = new XmlDocument();
            xmlDocument.SafeLoadXml(defaulted.ToString());
            return xmlDocument;
        }

        private XmlDocument BuildCanonicalXmlUsingXClient(cInputFile ipfFileData, StreamReader smrReader,
            cInboxPathData ipdSourceData, cDataImportXClientBase xClient)
        {
            try
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                var iconClient = xClient as IDataImportIconClient;

                return iconClient != null
                    ? BuildCanonicalXmlUsingIconXClient(ipfFileData, smrReader, ipdSourceData, iconClient)
                    : BuildCanonicalXmlUsingStandardXClient(ipfFileData, smrReader, ipdSourceData, xClient);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to translate the XML document: {0}", ex.Message), ex);
            }
        }
        private XmlDocument BuildCanonicalXmlUsingIconXClient(cInputFile ipfFileData, StreamReader smrReader,
            cInboxPathData ipdSourceData, IDataImportIconClient icnIconClient)
        {
            XmlDocument xmdAns;
            LogDebug("Translate XML Document for Client ID:" + ipdSourceData.DefaultOrganizationId + " and BankID: " +
                     ipdSourceData.DefaultBankId);

            //Compile the Configuration Settings data into a cIconConfigData object to use in the call
            cICONConfigData cfgIconConfig = new cICONConfigData(ipdSourceData.DefaultBankId, ipdSourceData.DefaultOrganizationId, ipdSourceData.ImageRPSResponseFolder, ipdSourceData.DefaultBatchSource)
            {
                BankName = ipdSourceData.BankName,
                DataRetentionDays = ipdSourceData.DataRetentionDays,
                ImageRetentionDays = ipdSourceData.ImageRetentionDays
            };

            switch (ipdSourceData.FileType)
            {
                case enmFileType.Batch:
                    xmdAns = BuildCanonicalXmlForIconBatch(ipfFileData, smrReader, ipdSourceData, icnIconClient, cfgIconConfig);
                    break;

                case enmFileType.ClientSetup:
                    xmdAns = BuildCanonicalXmlForIconClientSetup(ipfFileData, smrReader, ipdSourceData, icnIconClient, cfgIconConfig);
                    break;

                default:
                    throw new Exception("The DLL used for this process type should not implement the IDataImportIconClient Interface");
            }
            return xmdAns;
        }
        private XmlDocument BuildCanonicalXmlForIconBatch(cInputFile ipfFileData, StreamReader smrReader,
            cInboxPathData ipdSourceData, IDataImportIconClient icnIconClient, cICONConfigData cfgIconConfig)
        {
            string sErrorMessage;
            XmlDocument xmdAns;
            int[] aiRequiredClientIDs;
            Hashtable hshImageRPSAliasList = new Hashtable();

            if (_dctDimFieldLists.LastUpdated == null)
                UpdateDimFieldLists();

            //First get the client ID (lockbox ID)
            if (!icnIconClient.GetClientIDs(smrReader, out aiRequiredClientIDs, out sErrorMessage))
            {
                throw new Exception(
                    $"Failed to retrieve a list of Client IDs references in this file ({ipfFileData.FileData.Name}): {sErrorMessage}");
            }

            smrReader.BaseStream.Position = 0;
            if (!_dumUploadPort.UpdateImageRPSAliasList(ipdSourceData.DefaultBankId, aiRequiredClientIDs, (DateTime?) null, hshImageRPSAliasList))
            {
                throw new Exception(
                    $"Fail to get the ImageRPSAliasLists for Client ID{(aiRequiredClientIDs.Length == 1 ? "" : "s")} " +
                    $"({string.Join(", ", aiRequiredClientIDs)})");
            }

            //Compile the Dim Table data into a cDimData object to use in the call
            cDimData dmdDimData = GetDimData(_dctDimFieldLists, _dctDocumentType,
                (Hashtable) hshImageRPSAliasList);

            //Attempt to translate to XML
            if (icnIconClient.StreamToBatchXML(smrReader, cfgIconConfig, dmdDimData, ipdSourceData.ClientProcessCode, out xmdAns))
            {
                LogDebug(string.Format("Resulting XML: {0}", xmdAns.OuterXml));
                ipfFileData.WriteLineToResult("Translated Contents to XML");
                ipfFileData.WriteLineToResult(xmdAns.OuterXml);
            }
            else
            {
                ipfFileData.WriteLineToResult("Failed to translate contents to XML");
                LogError(string.Format("Failed to translate file ({0}) to XML with ({1})",
                    ipfFileData.FileData.Name,
                    ipdSourceData.XClientDllFilePath));
                xmdAns = null;
            }
            return xmdAns;
        }
        private XmlDocument BuildCanonicalXmlForIconClientSetup(cInputFile ipfFileData, StreamReader smrReader,
            cInboxPathData ipdSourceData, IDataImportIconClient icnIconClient, cICONConfigData cfgIconConfig)
        {
            cClientInfo cinClientInfo;
            string sErrorMessage;
            XmlDocument xmdAns;
            //First get the client ID (bank ID, lockbox ID)
            if (icnIconClient.GetClientInfo(ipfFileData.FileData.Name, out cinClientInfo, out sErrorMessage))
            {
                smrReader.BaseStream.Position = 0;
                //Compile the Dim Table data into a cDimData object to use in the call
                //reset field lists
                _dctDimFieldLists = new cDimFieldLists();
                cDimData dmdDimData = GetDimData(_dctDimFieldLists, _dctDocumentType,
                    null);
                if (icnIconClient.StreamToClientSetupXML(smrReader, cinClientInfo, dmdDimData, cfgIconConfig, ipdSourceData.ClientProcessCode,
                    out xmdAns))
                {
                    LogDebug(string.Format("Resulting XML: {0}", xmdAns.OuterXml));
                    ipfFileData.WriteLineToResult("Translated Contents to XML");
                    ipfFileData.WriteLineToResult(xmdAns.OuterXml);
                }
                else
                {
                    ipfFileData.WriteLineToResult("Failed to translate contents to XML");
                    LogError(string.Format("Failed to translate file ({0}) to XML with ({1})",
                            ipfFileData.FileData.Name,
                            ipdSourceData.XClientDllFilePath));
                    xmdAns = null;
                }
            }
            else
                throw new Exception(
                    string.Format("Failed to get the Client Information from this filename ({0}): {1}",
                        ipfFileData.FileData.Name, sErrorMessage));
            return xmdAns;
        }
        private XmlDocument BuildCanonicalXmlUsingStandardXClient(cInputFile ipfFileData, StreamReader smrReader,
            cInboxPathData ipdSourceData, cDataImportXClientBase xClient)
        {
            XmlDocument xmdAns;
            if (xClient.StreamToXML(smrReader, ipdSourceData.FileType,
                    new cConfigData_DebitSwitch(ipdSourceData.DefaultBankId, ipdSourceData.DefaultOrganizationId)
                    {
                        BankName = ipdSourceData.BankName,
                        ImageRetentionDays = ipdSourceData.ImageRetentionDays,
                        DataRetentionDays = ipdSourceData.DataRetentionDays,
                        SkipDebit = ipdSourceData.SkipDebit
                    },
                    out xmdAns) && xmdAns != null)
            {
                ipfFileData.WriteLineToResult("Translated Contents to XML");
                ipfFileData.WriteLineToResult(xmdAns.OuterXml);
            }
            else
            {
                ipfFileData.WriteLineToResult("Failed to translate contents to XML");
                LogError(string.Format("Failed to translate file ({0}) to XML with ({1})",
                        ipfFileData.FileData.Name,
                        ipdSourceData.XClientDllFilePath));
                xmdAns = null;
            }
            return xmdAns;
        }

        private cDimData GetDimData(cDimFieldLists dflFieldList, Dictionary<string, string> dctDocTypes, Hashtable hshImageRPSMapping)
        {
            return new cDimData(dflFieldList.BatchFieldList.ToArray(), dflFieldList.ItemFieldList.ToArray(), dctDocTypes.ToArray(),
                hshImageRPSMapping);
        }
        /// <summary>
        /// Returns the XClient for the given process path, loading it if necessary. Returns null if the process path
        /// does not have an XClientDllFilePath configured.
        /// </summary>
        /// <param name="ipdPathData"></param>
        /// <returns>The XClient, or null if none is configured.</returns>
        private cDataImportXClientBase GetXClient(cInboxPathData ipdPathData)
        {
            cDataImportXClientBase dxcAns = null;
            try
            {
                if (ipdPathData.XClientDllFilePath != string.Empty)
                {
                    if (_dctStreamToXMLDLLs == null)
                        throw new Exception("_dctStreamToXMLDLLs is null, it should never be null");
                    if (_dctStreamToXMLDLLs.ContainsKey(ipdPathData.XClientDllFilePath))
                        if (_dctStreamToXMLDLLs[ipdPathData.XClientDllFilePath] == null)
                            _dctStreamToXMLDLLs.Remove(ipdPathData.XClientDllFilePath);
                    if (!_dctStreamToXMLDLLs.ContainsKey(ipdPathData.XClientDllFilePath))
                    {
                        _dctStreamToXMLDLLs.Add(ipdPathData.XClientDllFilePath, LoadXClient(ipdPathData.XClientDllFilePath));
                        _dctStreamToXMLDLLs[ipdPathData.XClientDllFilePath].LogEvent +=  new dlgLogEventHandler(StreamToXMLDLL_LogEvent);
                        if (_dctStreamToXMLDLLs[ipdPathData.XClientDllFilePath] is IDataImportIconClient)
                        {
                            UpdateDimFieldLists();
                        }
                    }
                    dxcAns = _dctStreamToXMLDLLs[ipdPathData.XClientDllFilePath];
                }
            }
            catch (Exception ex)
            {
                LogException("GetXClient error", ex);
                throw;
            }
            return dxcAns;
        }

        //Extracts the class from the given DLL that inherits from IStreamToXMLDLL interface
        private cDataImportXClientBase LoadXClient(string sFilename)
        {
            cDataImportXClientBase stxAns = null;

            try
            {
                Assembly asmDLLsAssembly = Assembly.LoadFrom(sFilename);

                foreach (Type typCurType in asmDLLsAssembly.GetTypes())
                {
                    if (typCurType.IsSubclassOf(typeof(cDataImportXClientBase)))
                    {
                        stxAns = (cDataImportXClientBase)typCurType.GetConstructor(new Type[] { }).Invoke(null);
                    }
                }
                if (stxAns == null)
                {
                    throw new Exception(string.Format(
                            "DLL File ({0}) does not contain a class that properly inherit from cDataImportXClientBase",
                            sFilename));
                }
            }
            catch (Exception ex)
            {
                LogException("LoadXClient failed Trying to load:" + sFilename, ex);
                throw new Exception(string.Format("LoadXClient failed to load ({0}), Error: {1}",
                        sFilename, ex.Message), ex);
            }
            return stxAns;
         }

        //Logs message from the StreamToXMLDLL(DITXClientBase)
        private void StreamToXMLDLL_LogEvent(object sender, string sMsg, string sSrc, LogEventType mstMsgType, LogEventImportance msiImportance)
        {
            Dictionary<LogEventImportance, LTAMessageImportance> dctTransEventImprt = new Dictionary<LogEventImportance, LTAMessageImportance> 
                { { LogEventImportance.Debug, LTAMessageImportance.Debug}, 
                  { LogEventImportance.Essential, LTAMessageImportance.Essential},
                  {LogEventImportance.Verbose, LTAMessageImportance.Verbose}};

            Dictionary<LogEventType, LTAMessageType> dctTransEventType = new Dictionary<LogEventType, LTAMessageType> 
                {{LogEventType.Error, LTAMessageType.Error},
                {LogEventType.Information, LTAMessageType.Information},
                {LogEventType.Warning, LTAMessageType.Warning} };

            GblUtil.OnOutputLogAndTrace(sMsg, sSrc, dctTransEventType[mstMsgType], dctTransEventImprt[msiImportance]);
        }
        private void CheckForItemsWhichDidNotFinishProcessingLastTimeWasRunning()
        {
            //move anything in process back to input file
            //also anything in pending response go back to wait for the stuff

            foreach (_SourcePathBase srcBase in ProcessOptionsList.ToArray())
            {
                if (String.IsNullOrEmpty(srcBase.PendingResponseFolder))
                {
                    LogInfo("Pending Response Folder is Empty. This needs to be filled out.");
                }
                LogVerbose("Reading from pending response folder:" + srcBase.PendingResponseFolder);
                string[] pendingResponses = Directory.GetFiles(srcBase.PendingResponseFolder, "*.pendingresponse");
                foreach (string pendingResponse in pendingResponses)
                {
                    string submittedXMLProcess =string.Empty;
                    const string inProcessString = "inprocess.-";
                    int findStartGuid = pendingResponse.IndexOf(inProcessString);
                    int findEndGuid = pendingResponse.IndexOf("-.pendingResponse");
                    using (StreamReader sr = new StreamReader(pendingResponse))
                    {
                        submittedXMLProcess =  sr.ReadToEnd();
                    }
                    if (findStartGuid > 0 && findEndGuid > 0)
                    {
                        //found a guid now offset it 
                        findStartGuid += inProcessString.Length;
                        string guidString = pendingResponse.Substring(findStartGuid, findEndGuid - findStartGuid);
                        Guid transmissionId = new Guid(guidString);
                        FileInfo fileInfo = new FileInfo(pendingResponse);
                        cInputFile inputFile = new cInputFile(fileInfo, srcBase);
                        inputFile.SubmittedXML = submittedXMLProcess;
                        _dumUploadPort.AddItemToInFlightNodes(transmissionId, inputFile);
                    }
                }
                string[] inProcessItems = Directory.GetFiles(srcBase.InProcessFolder, "*.inprocess");
                foreach (string archiveItem in inProcessItems)
                {
                    //make sure the file pattern is valid and starts with *. It always should
                    if (srcBase.FilePattern.Length > 1 && srcBase.FilePattern[0] == '*')
                    {
                        int positionOfInputMask = archiveItem.LastIndexOf(srcBase.FilePattern.Substring(1, srcBase.FilePattern.Length - 1));
                        if (positionOfInputMask > 0)
                        {
                            LogVerbose("Archive Item Found :" + archiveItem);
                            string destFile = Path.GetFileName(archiveItem.Substring(0, positionOfInputMask + srcBase.FilePattern.Length - 1));
                            LogVerbose("Dest item  :" + destFile);
                            LogVerbose("Input Folder  :" + srcBase.InputFolder);
                            try
                            {
                            GblUtil.MoveFile(archiveItem, Path.Combine(srcBase.InputFolder, destFile));
                        }
                            catch (Exception ex)
                            {
                                LogException("Error trying to move item", ex);
                                LogVerbose("Now try to delete file: " + destFile);
                                try
                                {
                                    File.Delete(archiveItem);
                                }
                                catch (Exception ex1)
                                {
                                    LogException("Failed try to delete file", ex1);
                                }

                            }
                        }
                    }
                }

            }
        }

        public void Dispose()
        {
            if (_dctStreamToXMLDLLs != null)
                _dctStreamToXMLDLLs.Clear();
            if (_dumUploadPort != null)
                _dumUploadPort.Dispose();
            if (_dicLinkToDITService != null)
                _dicLinkToDITService = null;
        }
    }
}
