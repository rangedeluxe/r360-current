﻿using System;
using System.IO;
using System.Threading;
using WFS.LTA.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/13/2012
*
*  Purpose:  This class contains static functions that are used throughout the 
*            application.  This includes functions that return the settings, that 
*            instantiates a retry counter based on configuration settings, that returns 
*            an instance of the logging object, 
*
* Modification History
* CR 52267 CEJ 07/19/2012
*   -DIT Client
* WI 34771 WJS 11/26/2012
 *   - Add support for pause client
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public static class GblUtil
    {
        public static event Action<string, string, LTAMessageType, LTAMessageImportance> OutputLog;

        public static void InitializeClientOptions()
        {
            if (ClientOptions != null)
                throw new InvalidOperationException("InitializeClientOptions may only be called once");

	        try
	        {
		        ClientOptions = new cClientOptions();
	        }
	        catch (ArgumentNullException)
	        {
		        throw new Exception(
			        "Error loading client options, invalid or missing username or password. Data Import Client Service is stopping.");
	        }
	        catch (FormatException)
	        {
		        throw new Exception(
			        "Error loading client options, invalid username or password encrypted length. Data Import Client Service is stopping.");
	        }
	        catch
	        {
		        throw;
	        }
		}
        public static void WriteClientOptionsConfigurationLog()
        {
            foreach (var item in ClientOptions.ConfigurationLog)
            {
                var message = item.Item1;
                var messageType = item.Item2;
                var importance = item.Item3;
                OnOutputLogAndTrace(message, typeof (cClientOptions).Name, messageType, importance);
            }
        }

        private static void OnOutputLog(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance)
        {
            if (OutputLog == null)
            {
                Console.WriteLine(msg);
            }
            else
            {
                OutputLog(msg, src, messageType, messageImportance);
            }

        }

        public static void OnOutputLogAndTrace(string msg, string src, Exception ex)
        {
            OnOutputLog(msg, src, LTAMessageType.Error, LTAMessageImportance.Essential);
            OnOutputLog("Exception: " + ex.Message, src, LTAMessageType.Error, LTAMessageImportance.Essential);
        }

        public static void OnOutputLogAndTrace(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance)
        {
            OnOutputLog(msg, src, messageType, messageImportance);
        }

        public static bool IsValueInRange(int iValue, int iMin, int iMax, out string sMessage)
        {
            bool bAns = iValue > iMin;

            sMessage = string.Empty;
            if (!bAns)
                sMessage = string.Format("Value ({0}) is less than {1} and must be between {1} and {2}", iValue, iMin, iMax);
            if (iValue > iMax)
            {
                sMessage = string.Format("Value ({0}) is more than {2} and must be between {1} and {2}", iValue, iMin, iMax);
                bAns = false;
            }

            return bAns;
        }

        public static cRetryCounter GetRetryCounter()
        {
            return new cRetryCounter(ClientOptions.MaxRetries, ClientOptions.RetryDelay, ClientOptions.ProgressiveRetryDelay);
        }

        public static cClientOptions ClientOptions { get; private set; }

        public static bool MoveFile(string fromFilePath, string toFilePath)
        {

            bool bolContinue = true;
            int intRetryCount = 0;
            bool bolRetVal = false;

            while (bolContinue)
            {

                try
                {
                    if (!IsFileClosed(fromFilePath))
                    {
                        GblUtil.OnOutputLogAndTrace("Waiting for the file to close...", "MoveFile", LTAMessageType.Information, LTAMessageImportance.Essential);
                        Thread.Sleep(1000);
                    }
                    
                    File.Move(fromFilePath, toFilePath);
                    bolRetVal = true;
                    bolContinue = false;
                }
                catch (Exception ex)
                {
                    if (intRetryCount < 3)
                    {
                        intRetryCount++;

                    }
                    else
                    {

                        bolContinue = false;
                        throw ex;
                    }
                }
            }

            return (bolRetVal);
        }

        public static bool IsFileClosed(string filename)
        {
            try
            {
                using (var inputStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    return true;
                }
            }
            catch (IOException)
            {
                return false;
            }
        }

    }
}
