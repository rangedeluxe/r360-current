﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wfs.Logging.Performance;
using WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class ImportManager
    {

        public ImportManager()
        {
        }

        public IEnumerable<ImportStepResponse> ProcessImportSteps(IEnumerable<IImportStep> importSteps,
            IPerformanceLogger performanceLogger)
        {
            if (importSteps == null)
                throw new ArgumentNullException("ProcessImportSteps will not accept a null list");

            var importStepResponses = new List<ImportStepResponse>();

            foreach (var step in importSteps)
            {
                using (performanceLogger.StartPerformanceTimer($"Import step: {step.GetType().Name}"))
                {
                    var response = ToImportStepResponse(step);
                    importStepResponses.Add(response);
                }
            }

            return importStepResponses;
        }

        public ImportStepResponse ToImportStepResponse(IImportStep importStep)
        {
            try
            {
                var response = importStep.Execute();
                if (!response.IsSuccessful)
                    GblUtil.OnOutputLogAndTrace(string.Format
                        ("Import step {0} failed with message: {1}", importStep.GetType().Name, ToMessage(response.Messages)), "ImportManager.ToImportStepResponse", Common.LTAMessageType.Information, Common.LTAMessageImportance.Debug);

                return response;
            }
            catch (Exception ex)
            {
                var message = string.Format("Import Step {0} failed with exception message: {1}", importStep.GetType().Name, ex.Message);
                GblUtil.OnOutputLogAndTrace(message, "ImportManager.ToImportStep()", ex);
                return new ImportStepResponse { IsSuccessful = false, Messages = new List<string> { message } };

            }
        }

        private string ToMessage(IEnumerable<string> messages)
        {
            if (messages == null)
                messages = new List<string>();

            var message = new StringBuilder();
            foreach (var item in messages)
            {
                message.AppendLine(item);
            }

            return message.ToString();

        }

    }
}
