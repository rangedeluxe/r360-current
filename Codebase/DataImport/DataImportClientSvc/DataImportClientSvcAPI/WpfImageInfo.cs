﻿using System;
using System.IO;
using System.Windows.Media.Imaging;

/******************************************************************************
** Wausau
** Copyright ? 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013-2014.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Chris Colombo
* Date:     05/07/2014
*
* Purpose:  This class handles front/back parsing of Images.
*
* Modification History
* Created
* WI 140511 CMC 05/07/2014
*   - Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class WpfImageInfo : IImageInfo
    {
        public WpfImageInfo(string imagePathParam, int imageSplitJpegQuality)
        {
            if (string.IsNullOrEmpty(imagePathParam))
                throw new ArgumentException("Must provide the image path in order to do the parse.");

            ImageSplitJpegQuality = imageSplitJpegQuality;
            using (var stream = new FileStream(imagePathParam, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                Decoder = DecodeStream(stream, imagePathParam);
            }
            NumberOfPages = Decoder.Frames.Count;
        }
        void IDisposable.Dispose()
        {
        }

        private BitmapDecoder Decoder { get; set; }
        public int NumberOfPages { get; private set; }
        private int ImageSplitJpegQuality { get; set; }

        private BitmapEncoder CreateEncoder(BitmapFrame frame)
        {
            if (frame.Format.BitsPerPixel == 1)
                return new TiffBitmapEncoder {Compression = TiffCompressOption.Ccitt4};
            return new JpegBitmapEncoder {QualityLevel = ImageSplitJpegQuality};
        }
        private static BitmapDecoder DecodeStream(Stream stream, string imagePath)
        {
            var header = new byte[4];
            stream.Read(header, 0, header.Length);
            stream.Position = 0;
            // TIFF (little-endian and big-endian)
            if ((header[0] == 0x49 && header[1] == 0x49 && header[2] == 0x2A && header[3] == 0x00) ||
                (header[0] == 0x4D && header[1] == 0x4D && header[2] == 0x00 && header[3] == 0x2A))
            {
                return new TiffBitmapDecoder(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }
            // JPEG
            if (header[0] == 0xFF && header[1] == 0xD8)
            {
                return new JpegBitmapDecoder(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }
            // PNG
            if (header[0] == 0x89 && header[1] == 0x50 && header[2] == 0x4E && header[3] == 0x47)
            {
                return new PngBitmapDecoder(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }
            // GIF
            if (header[0] == 0x47 && header[1] == 0x49 && header[2] == 0x46 && header[3] == 0x38)
            {
                return new GifBitmapDecoder(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }
            // BMP
            if (header[0] == 0x42 && header[1] == 0x4D)
            {
                return new BmpBitmapDecoder(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }
            throw new NotSupportedException("Unrecognized file format: " + imagePath);
        }
        public Stream GetFrontPageStream()
        {
            if (NumberOfPages > 0)
                return GetPageStream(0);

            return new MemoryStream();
        }

        public Stream GetBackPageStream()
        {
            if (NumberOfPages > 1)
                return GetPageStream(1);

            return GetFrontPageStream();
        }

        private Stream GetPageStream(int page)
        {
            var frame = Decoder.Frames[page];
            var encoder = CreateEncoder(frame);
            encoder.Frames.Add(frame);

            var stream = new MemoryStream();
            encoder.Save(stream);
            return stream;
        }
    }
}