﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms.Design;
using System.Drawing.Design;
using System.ComponentModel;

using WFS.LTA.Common.FileCollector;
using WFS.LTA.DataImport.DataImportXClientBase;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/13/2012
*
*  Purpose:  This class describes a processing path including the input directory, the 
*            DLL used to translate the input file to xml, the XSL file used to translate 
*            the XML to the canonical xml schema, the directory in which to stage the file
*            while it is being processed, the type of the file [Client Setup, Batch or Both], 
*            Batch Source Name and the default BankID and LockboxID.
*
* Modification History
* Created
*  CR 52267: CEJ 06/13/2012
*   - DIT Client
* WI 34771 WJS 11/20/2012
 *   - Support for Pending Response
* WI 129729 CMC 02/14/2014 
*   - Adding additional config attributes 
*   (BankName, DefaultBankID, DefaultLockboxID, DataRetentionDays, ImageRetentionDays, and ImageRPSResponseFolder)
* WI 135681 CMC 04/21/2014
*   - Support for Input Error Folder   
* WI 143271, 143409 CMC 05/29/2014
*   - Support for default payment type and batch source 
* WI 148294 CMC 06/17/2014
*   - Change DefaultLockboxID to DefaultOrganizationID
*******************************************************************************/


namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class cInboxPathData : _SourcePathBase
    {
        private enmFileType _FileType;
        private string _XClientDllFilePath;
        private string _ToCanonicalXSLFile;

        public cInboxPathData(_SourcePathBase copyfrom)
        {
            base.ArchiveFolder = copyfrom.ArchiveFolder;
            base.ResponseErrorFolder = copyfrom.ResponseErrorFolder;
            base.FilePattern = copyfrom.FilePattern;
            base.InProcessFolder = copyfrom.InProcessFolder;
            base.InputFolder = copyfrom.InputFolder;
            base.ResponseFolder = copyfrom.ResponseFolder;
            base.PendingResponseFolder = copyfrom.PendingResponseFolder;
            base.InputErrorFolder = copyfrom.InputErrorFolder;
            base.StatusEndPoint = copyfrom.StatusEndPoint;
        }

        public cInboxPathData(_ProcessElement processElement) {

            ClientProcessCode = processElement.ClientProcessCode;
            base.ArchiveFolder = processElement.ArchiveFolder;
            base.ResponseErrorFolder = processElement.ResponseErrorFolder;
            base.FilePattern = processElement.FilePattern;
            base.InProcessFolder = processElement.InProcessFolder;
            base.InputFolder = processElement.InputFolder;
            base.ResponseFolder = processElement.ResponseFolder;
            base.PendingResponseFolder = processElement.PendingResponseFolder;
            base.InputErrorFolder = processElement.InputErrorFolder;
            base.StatusEndPoint = processElement.StatusEndPoint;
           
            if(processElement.FileType.ToLower() == enmFileType.ClientSetup.ToString().ToLower()) {
                _FileType = enmFileType.ClientSetup;
            } else if(processElement.FileType.ToLower() == enmFileType.Batch.ToString().ToLower()) {
                _FileType = enmFileType.Batch;
            } else {
                _FileType = enmFileType.Both;
            }

            _XClientDllFilePath = processElement.XClientDllFilePath;
            _ToCanonicalXSLFile = processElement.ToCanonicalXSLFile;
            
            int bankId;
            int organizationId;
            int dataRetentionDays;
            int imageRetentionDays;
            int siteCode;
            bool previousRPSQueueFileAllowed;

            int.TryParse(processElement.DefaultBankId, out bankId);
            int.TryParse(processElement.DefaultSiteCode, out siteCode);
            int.TryParse(processElement.DefaultOrganizationId, out organizationId);
            int.TryParse(processElement.DataRetentionDays, out dataRetentionDays);
            int.TryParse(processElement.ImageRetentionDays, out imageRetentionDays);
            bool.TryParse(processElement.PreviousRpsQueueFileVersionAllowed, out previousRPSQueueFileAllowed);

            BankName =  processElement.BankName;
            DefaultBankId = bankId;
            DefaultSiteCode = siteCode;
            DefaultOrganizationId = organizationId;
            DataRetentionDays = dataRetentionDays;
            ImageRetentionDays = imageRetentionDays;
            ImageRPSResponseFolder = processElement.ImageRPSResponseFolder;

            DefaultPaymentType = processElement.DefaultPaymentType;
            DefaultBatchSource = processElement.DefaultBatchSource;
            SkipDebit = processElement.SkipDebit;
            PreviousRPSQueueFileAllowed = previousRPSQueueFileAllowed;
            SendEndPoint = processElement.SendEndPoint;
            StatusEndPoint = processElement.StatusEndPoint;
        }



        public string ClientProcessCode
        {
            get;
            set;
        }

        public string XClientDllFilePath
        {
            get
            {
                return _XClientDllFilePath;
            }
            set
            {
                _XClientDllFilePath = value;
            }
        }

        public enmFileType FileType
        {
            get
            {
                return _FileType;
            }
            set
            {
                _FileType = value;
            }
        }

        public string BankName
        {
            get;
            set;
        }

        public int DefaultBankId
        {
            get;
            set;
        }

        public int DefaultSiteCode
        {
            get;
            set;
        }

        public int DefaultOrganizationId
        {
            get;
            set;
        }

        public int DataRetentionDays
        {
            get;
            set;
        }

        public int ImageRetentionDays
        {
            get;
            set;
        }

        public string ImageRPSResponseFolder
        {
            get;
            set;
        }

        public string DefaultPaymentType
        {
            get;
            private set;
        }

        public string DefaultBatchSource
        {
            get;
            private set;
        }

        public bool PreviousRPSQueueFileAllowed
        {
            get;
            private set;
        }

        public bool SkipDebit { get; set; }
        public string SendEndPoint { get; set; }
        public string StatusEndPoint { get; set; }
    }
}
