﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class Defaulter
    {
        private DefaultContext context;

        public Defaulter(DefaultContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            this.context = context;
        }

        public XElement ApplyDefaults(XElement importElement)
        {
            ApplyDefaults(importElement, context.BatchDefaults, "batch");
            ApplyDefaults(importElement, context.ClientSetupDefaults, "imagerpsaliasmappings");
            ApplyDefaults(importElement, context.ClientSetupDefaults, "dataentrycolumn");

            return importElement;
        }

        public void ApplyDefaults(XElement importElement, IList<DefaultItem> defaultItems, string elementToFind)
        {
            if (importElement == null)
                throw new ArgumentNullException(nameof(importElement));

            if (defaultItems == null)
                throw new ArgumentNullException(nameof(defaultItems));

            if (string.IsNullOrEmpty(elementToFind))
                throw new ArgumentNullException(nameof(elementToFind));

            foreach (var element in importElement
                .DescendantsAndSelf()
                .Where(element => string.Equals(element.Name.ToString(), elementToFind, StringComparison.InvariantCultureIgnoreCase)))
            {
                ApplyDefaults(element, defaultItems);
            }
        }

        public void ApplyDefaults(XElement importElement, IList<DefaultItem> defaultItems)
        {
            if (importElement == null)
                throw new ArgumentNullException(nameof(importElement));

            if (defaultItems == null)
                throw new ArgumentNullException(nameof(defaultItems));

            foreach (var item in defaultItems)
            {
                if (ShouldDefault(importElement, item))
                    importElement.SetAttributeValue(item.Setting, item.Value);
            }
        }

        public bool ShouldDefault(XElement importElement, DefaultItem defaultItem)
        {
            if (importElement == null)
                throw new ArgumentNullException(nameof(importElement));

            if (defaultItem == null)
                throw new ArgumentNullException(nameof(defaultItem));

            if (importElement.Attribute(defaultItem.Setting) == null)
                return true;

            if (string.IsNullOrEmpty(importElement.Attribute(defaultItem.Setting).Value))
                return true;

            if (defaultItem.DefaultIfZero && string.Equals(importElement.Attribute(defaultItem.Setting).Value, "0"))
                return true;

            return false;
        }
    }
}
