﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using WFS.LTA.Common;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Extensions;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/13/2012
*
* Purpose:  This class holds tools for reading and altering Canonical Nodes
*
* Modification History
*
* Created
*  CR 52267: CEJ 6/13/2012
*   - DIT Client
*
* WI 143222 CEJ 05/20/2014
*    Remove single batch restriction from DataImportClientSvc
*
* WI 146666: CMC 06/10/2014
*   - Change BatchNodeToImageInfoContract Function To Use BatchDate 
*   
* WI 152407 CMC 07/08/2014
*   Support for Int64 BatchID
* WI 175570 SAS 10/31/2014
*   *   Update ImportTypeShortName field to BatchSourceName   
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class cCanonXMLTools
    {
        private static Dictionary<string, string> _dctCanonNodeChildName = 
                new Dictionary<string, string>()
                    {   {"ClientGroups", "ClientGroup"},
                        {"Batches", "Batch"}};
      

        public static bool ExtractCanonNodeGroup(XmlNode xmnCanonNodeGroup, out XmlNodeList xmnCanonNode)
        {
            bool bAns = false;
            string sVersion;
            xmnCanonNode = null;
            if (xmnCanonNodeGroup != null)
            {
                if (_dctCanonNodeChildName.ContainsKey(xmnCanonNodeGroup.Name))
                    if (xmnCanonNodeGroup.Attributes["XSDVersion"] != null)
                    {
                        sVersion = xmnCanonNodeGroup.Attributes["XSDVersion"].Value;
                        xmnCanonNode = xmnCanonNodeGroup.SelectNodes( _dctCanonNodeChildName[xmnCanonNodeGroup.Name]);
                            if (xmnCanonNode != null)
                            {
                                bAns = true;
                            }
                        else
                            throw new Exception(String.Format(
                                    "This application does not support input files with multiple {0}",
                                    xmnCanonNodeGroup.Name));
                    }
            }
            return bAns;
        }

        public static string GetSchemaVersion(XmlNode xmnCanonNodeGroup)
        {
            XmlAttribute xmaAns=null;

            try
            {
                xmaAns = xmnCanonNodeGroup.Attributes["XSDVersion"];
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to get the schema Version: {0}", ex.Message), ex);
            }

            if (xmaAns == null)
                throw new Exception("XSDVersion attribute was not found in the XML Node");
                
            return xmaAns==null?"":xmaAns.Value;
        }
        public static bool RepackageCanonNodeGroup(CanonXmlInfo canonXmlInfo, out XmlDocument xmdNewCanonNodeGroup)
        {
            if (canonXmlInfo == null)
                throw new ArgumentNullException();

            if (canonXmlInfo.ChildNodes == null)
                throw new ArgumentNullException();

            bool bAns = false;
            Dictionary<string, XElement> ParentNodes = new Dictionary<string, XElement>();
            XDocument xdcNewCanonNodeGroup = null;

            foreach (string CurrentChildNode in canonXmlInfo.ChildNodes)
            {
                XElement xmnChildNode = XElement.Parse(CurrentChildNode);
                if (_dctCanonNodeChildName.ContainsValue(xmnChildNode.Name.ToString()))
                {
                    string sParentName = _dctCanonNodeChildName.First(KeyValPair => KeyValPair.Value == xmnChildNode.Name.ToString()).Key;

                    if (xdcNewCanonNodeGroup == null)
                    {
                        xdcNewCanonNodeGroup = new XDocument();
                    }
                    if (!ParentNodes.ContainsKey(sParentName))
                    {
                        ParentNodes.Add(sParentName, ToXElement(canonXmlInfo, sParentName));
                        xdcNewCanonNodeGroup.Add(ParentNodes[sParentName]);
                    }
                    ParentNodes[sParentName].Add(xmnChildNode);
                    bAns = true;
                }
                else
                {
                    GblUtil.OnOutputLogAndTrace("Node name \"{0}\" is not a recognized Canon Child Node Name",
                         $"cCanonXMLTools({nameof(RepackageCanonNodeGroup)})", LTAMessageType.Error,
                         LTAMessageImportance.Essential);
                }
            }
            if (xdcNewCanonNodeGroup == null)
            {
                xmdNewCanonNodeGroup = null;
            }
            else
            {
                xmdNewCanonNodeGroup = new XmlDocument();
                xmdNewCanonNodeGroup.SafeLoadXml(xdcNewCanonNodeGroup.ToString());
            }
            return bAns;

        }

        public static bool RepackageCanonNodeGroup(string[] sChildCanonNodeGroup,   Guid gudTrackingID, string sVersion, string sClientProcessCode, out XmlDocument xmdNewCanonNodeGroup)
        {
            return RepackageCanonNodeGroup
                (
                    new CanonXmlInfo 
                    {
                        ChildNodes = sChildCanonNodeGroup, 
                        TrackingId = gudTrackingID, 
                        SchemaVersion = sVersion,
                        ClientProcessCode = sClientProcessCode
                    },
                    out xmdNewCanonNodeGroup
                );
        }
       
        public static IReadOnlyList<ImageInfoContract> CollectImageData(string xmlString, ResponseItem responseItem)
        {
            try
            {
                var results = new List<ImageInfoContract>();
                var keysSeen = new HashSet<ImageInfoContract>(new cImageInforContractEquality());

                var batchElements = ToXElementList(XElement.Parse(xmlString), "Batch");
                foreach (var batch in batchElements)
                {
                    if (batch.IsSameBatch(responseItem))
                    {
                        var imageList = 
                            ToImageInfoContractList(batch, responseItem.bankId, responseItem.clientId);
                        foreach (var image in imageList)
                        {
                            if (keysSeen.Contains(image))
                                throw new Exception($"Image has been duplicated for Batch Sequence {image.BatchSequence}");

                            results.Add(image);
                            keysSeen.Add(image);
                        }
                    }
                    
                }
                return results;
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace($"{nameof(CollectImageData)} got exception:" + ex.Message, nameof(CollectImageData), ex);
                throw;
            }
        }
        
        public static IEnumerable<ImageInfoContract> ToImageInfoContractList(XElement batchElement, int bankId, int clientId)
        {
            var imageInfoList = new List<ImageInfoContract>();
            var transactionElements = ToXElementList(batchElement, "Transaction");//grab all the transaction elements
            var paymentElements = transactionElements.Elements().Where(element => string.Equals(element.Name.ToString(), "Payment", StringComparison.InvariantCultureIgnoreCase));
            var documentElements = transactionElements.Elements().Where(element => string.Equals(element.Name.ToString(), "Document", StringComparison.InvariantCultureIgnoreCase));

            imageInfoList.AddRange(ToImageInfoContractList(batchElement, paymentElements, bankId, clientId));
            imageInfoList.AddRange(ToImageInfoContractList(batchElement, documentElements, bankId, clientId));

            return imageInfoList;
        }

        public static IEnumerable<ImageInfoContract> ToImageInfoContractList(XElement batchElement, IEnumerable<XElement> documentElements, 
            int bankId, int clientId)
        {
            var imageInfoList = new List<ImageInfoContract>();

            foreach (var documentElement in documentElements)
            {
                imageInfoList.AddRange(ToImageInfoContractList(batchElement, documentElement, bankId, clientId));
            }

            return imageInfoList;
        }

        public static IEnumerable<ImageInfoContract> ToImageInfoContractList(XElement batchElement, XElement documentElement, 
            int bankId, int clientId)
        {
            var imageInfoList = new List<ImageInfoContract>();
            var images = ToImageElementList(documentElement);
            var isPayment = string.Equals(documentElement.Name.ToString(), "payment", StringComparison.InvariantCultureIgnoreCase) ? true : false;

            foreach (var image in images)
            {
                var imageInfo = ToImageInfoContract(batchElement, isPayment, documentElement, image, bankId, clientId);

                imageInfoList.Add(imageInfo);
            }

            return imageInfoList;
        }
        public static ImageInfoContract ToImageInfoContract(XElement batchElement,
            bool isPayment, XElement documentElement, XElement image, int bankId, int clientId)
        {
            var imageInfo = new ImageInfoContract();
            PopulateImageInfoContractFromBatchElement(imageInfo, batchElement, bankId, clientId);

            imageInfo.FileDescriptor = isPayment ? "C" : documentElement.Attribute("DocumentDescriptor") == null ? string.Empty : documentElement.Attribute("DocumentDescriptor").Value;
            imageInfo.IsCheck = isPayment;
            imageInfo.IsBack = string.IsNullOrEmpty(image.Attribute("Page").Value) ? false : string.Equals(image.Attribute("Page").Value, "1") ? true : false;
            imageInfo.ImagePath = image.Attribute("ClientFilePath") == null ? string.Empty : image.Attribute("ClientFilePath").Value;

            short colorMode;
            short.TryParse(string.IsNullOrEmpty(image.Attribute("ColorMode").Value) ? string.Empty : image.Attribute("ColorMode").Value, out colorMode);
            imageInfo.ColorMode = colorMode;

            int batchSequence;
            int.TryParse(string.IsNullOrEmpty(documentElement.Attribute("BatchSequence").Value) ? string.Empty : documentElement.Attribute("BatchSequence").Value, out batchSequence);
            imageInfo.BatchSequence = batchSequence;

            bool allowMultiPageTiff;
            bool.TryParse(image.Attribute("AllowMultiPageTiff")?.Value, out allowMultiPageTiff);
            imageInfo.AllowMultiPageTiff = allowMultiPageTiff;

            return imageInfo;
        }

        public static IEnumerable<XElement> ToImageElementList(XElement element)
        {
            var images = ToXElementList(element, "Images");
            return ToXElementList(images.FirstOrDefault(), "img");
        }

        public static ImageInfoContract PopulateImageInfoContractFromBatchElement(ImageInfoContract imageInfo, XElement batchElement, int bankId, int clientId)
        {
            var batchId = batchElement.Attribute("BatchID") == null ? string.Empty : batchElement.Attribute("BatchID").Value;
            var batchSiteCode = batchElement.Attribute("BatchSiteCode") == null ? string.Empty : batchElement.Attribute("BatchSiteCode").Value;
            var batchDate = batchElement.Attribute("BatchDate") == null ? string.Empty : batchElement.Attribute("BatchDate").Value;
            var batchSource = batchElement.Attribute("BatchSource") == null ? string.Empty : batchElement.Attribute("BatchSource").Value;
            var batchNumber = batchElement.Attribute("BatchNumber") == null ? string.Empty : batchElement.Attribute("BatchNumber").Value;

            try
            {
                var message = string.Format("BankID: {0}, ClientID: {1}, BatchID: {2}, BatchSiteCode: {3}", bankId, clientId, batchId, batchSiteCode);
                GblUtil.OnOutputLogAndTrace(string.Format("Batch Element conversion for {0} ", message), nameof(PopulateImageInfoContractFromBatchElement), LTAMessageType.Information, LTAMessageImportance.Debug);

                imageInfo.BankID = bankId;
                if (!long.TryParse(batchId, out long longValue))
                    throw new Exception("Batch ID attribute of the batch node is invalid");

                imageInfo.SourceBatchID = longValue;

                imageInfo.LockboxID = clientId;

                DateTime dateValue;
                if (!DateTime.TryParse(batchDate, out dateValue))
                    throw new Exception("Processing Date attribute of the batch node is invalid");
                imageInfo.ProcessingDateKey = int.Parse(dateValue.ToString("yyyyMMdd"));
                imageInfo.BatchSourceShortName = batchSource;
                imageInfo.ImportTypeShortName = string.Empty;
                imageInfo.BatchNumber = batchNumber;
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace("BatchNode conversion failed " + ex.Message, nameof(PopulateImageInfoContractFromBatchElement), LTAMessageType.Error, LTAMessageImportance.Essential);
                throw new Exception($"{nameof(PopulateImageInfoContractFromBatchElement)} encountered invalid data. Error: {ex.Message}", ex);
            }
            return imageInfo;
        }

        public static IEnumerable<XElement> ToXElementList(XElement xElementToSearch, string searchElement)
        {
            if (xElementToSearch == null)
                return new List<XElement>();

            return xElementToSearch.Elements(searchElement);
        }

        public static XElement ToXElement(CanonXmlInfo canonXmlInfo, string elementName)
        {
            if (canonXmlInfo == null)
                throw new ArgumentNullException();

            XElement xElement = 
                new XElement
                (
                    elementName,
                    new XAttribute("SourceTrackingID", canonXmlInfo.TrackingId.ToString()),
                    new XAttribute("XSDVersion", canonXmlInfo.SchemaVersion),
                    new XAttribute("ClientProcessCode", canonXmlInfo.ClientProcessCode)
                );

            if (!string.IsNullOrEmpty(canonXmlInfo.FileHash))
                xElement.Add(new XAttribute("FileHash", string.IsNullOrEmpty(canonXmlInfo.FileHash) ? string.Empty : canonXmlInfo.FileHash));

            if (!string.IsNullOrEmpty(canonXmlInfo.FileSignature))
                xElement.Add(new XAttribute("FileSignature", string.IsNullOrEmpty(canonXmlInfo.FileSignature) ? string.Empty : canonXmlInfo.FileSignature));

            return xElement;
        }

        public static DateTime GetDateTimeAttribute(XmlNode xmlNode, string attributeName)
        {
            DateTime result;
            return DateTime.TryParse(xmlNode?.Attributes?[attributeName]?.InnerText, out result)
                ? result
                : DateTime.MinValue;
        }

        public static Guid GetGuidAttribute(XmlNode xmlNode, string attributeName)
        {
            Guid result;
            return Guid.TryParse(xmlNode?.Attributes?[attributeName]?.Value, out result) ? result : Guid.Empty;
        }

        public static int GetIntAttribute(XmlNode xmlNode, string attributeName)
        {
            int result;
            return int.TryParse(xmlNode?.Attributes?[attributeName]?.Value, out result) ? result : 0;
        }

        public static long GetLongAttribute(XmlNode xmlNode, string attributeName)
        {
            long result;
            return long.TryParse(xmlNode?.Attributes?[attributeName]?.Value, out result) ? result : 0L;
        }

        public static int GetNodeCount(XmlNode xmlNode, string nodeName)
        {
            return xmlNode?.SelectNodes(nodeName)?.Count ?? 0;
        }

    }
}
