﻿using System;
using System.Collections.Generic;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class ResponseItem
    {
        public int AuditDateKey { get; set; }
        public Guid SourceTrackingId { get; set; }
        public Guid ResponseTrackingId { get; set; }
        public bool IsSuccessful { get; set; }
        public ResponseContext Context { get; set; }
        public IEnumerable<IImportStep> ImportSteps { get; set; }
        public string Message { get; set; }
        //bankId and clientId are only valid with success or informational responses
        public int bankId { get; set; }
        public int clientId { get; set; }
        public Guid BatchTrackingId { get; set; }
    }
}
