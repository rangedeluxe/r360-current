﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Timers;
using System.Linq;
using Wfs.Logging.Performance;
using WFS.RecHub.Common;
using WFS.LTA.Common;
using WFS.LTA.DataImport.DataImportServiceClient;
using WFS.LTA.DataImport.DataImportXClientBase;
using WFS.LTA.Common.FileCollector;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;
using WFS.RecHub.AuditingServicesCommon.Interfaces;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/13/2012
*
* Purpose:  This class handles uploading canonical XML to the Data Import Service and then
*            monitors for the response
*
* Modification History
* Created
*  CR 52267: CEJ 06/13/2012
*   - DIT Client
*  WI 70280 WJS 12/12/2012
 *    - Add support for archive images
*  WI 90202 WJS 3/4/2013
 *    -FP:Add support for archive images
* WI 126987 CMC 01/20/2014
*  - Updated to use 2.00 libraries.
* WI 130119 CMC 02/18/2014
*  - Fix cDITUploadManager.UploadImageRPSAliasList to use correct column name.
* WI 139494 CMC 05/01/2014
*  - Remove Client Side Call To Validate Bank And Client.
* WI 143222 CEJ 05/20/2014
 * *    Remove single batch restriction from DataImportClientSvc
* WI 144705
*   - Correct XMLResponseToData function so it doesn't add duplicates  
* WI 142879 CMC 05/16/2014
*   Fix GetBatchInformation method to look for the ClientProcessCode 
*   attribute in the Batches element instead of the Batch element
* WI 151773 CMC 07/02/2014
*   Change Import Type from ICON to DIT
* WI 152407 CMC 07/08/2014
*   Support for Int64 BatchID
* WI 155269 CMC 07/29/2014
*   End Session
* WI 168672 CMC 09/30/2014
*   Stop throwing exception.  This is a forward patch item from 1.05.
*******************************************************************************/


namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    class cDITUploadManager:IDisposable, IPendingService
    {
        private bool _bDisposed = false;
        private Timer _tmrStatusUpdater = new Timer();

        private readonly IAuditingDit _auditingServicesClient;
        private Dictionary<Guid, cInputFile> _dctInFlightInputFiles = new Dictionary<Guid, cInputFile>();
        private DataImportConnect _dicDITLink;
        private cProcessOptionsList _processOptionList;
        private DataImportConnect _dicLinkToDITService;

        public cDITUploadManager(DataImportConnect dicDITLink, cProcessOptionsList processOptionList,
            double dUploadStatusUpdateInterval, IAuditingDit auditingServicesClient)
        {
            _auditingServicesClient = auditingServicesClient;
            _tmrStatusUpdater.Interval = dUploadStatusUpdateInterval;
            _tmrStatusUpdater.Enabled = true;
            _dicDITLink = dicDITLink;
            _processOptionList = processOptionList;
            GblUtil.OnOutputLogAndTrace("Setup time for upload manager lists at frequency of: " + dUploadStatusUpdateInterval + " milliseconds", " Initialize",
                                 LTAMessageType.Information, LTAMessageImportance.Debug);
            _tmrStatusUpdater.Elapsed+=new System.Timers.ElapsedEventHandler(_tmrStatusUpdater_Elapsed);
        }

        public Dictionary<Guid, cInputFile> InFlightNodes
        {
            get
            {
                return _dctInFlightInputFiles;
            }
        }
        private void UpdateStatus()
        {
            try
            {
                bool hadAnyWork;
                var performanceLogger = new PerformanceLogger(new StopwatchHighResolutionTimerProvider());
                using (performanceLogger.StartPerformanceTimer("Complete imports"))
                {
                    var responses = GetResponses().ToList();
                    hadAnyWork = responses.Any();

                    var importManager = new ImportManager();
                    foreach (var response in responses)
                    {
                        importManager.ProcessImportSteps(response.ImportSteps, performanceLogger);
                    }
                }

                if (hadAnyWork)
                {
                    var reportHeader = new[] {"Metrics: Complete Imports"};
                    var reportBodyLines = PerformanceLogReporter.GetReport(performanceLogger.GetMeasuredActions());
                    var report = string.Join(Environment.NewLine, reportHeader.Concat(reportBodyLines));
                    GblUtil.OnOutputLogAndTrace(report, nameof(UpdateStatus),
                        LTAMessageType.Information, LTAMessageImportance.Debug);
                }
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace(ex.Message, "UpdateStatus", ex);
            }
        }
        private void _tmrStatusUpdater_Elapsed(object objSender, ElapsedEventArgs e)
        {
            _tmrStatusUpdater.Enabled = false;
            CheckServerSideStatuses();
            UpdateStatus();
            _tmrStatusUpdater.Enabled = true;
        }

        /// <summary>
        /// parses the ID out of the filename string. nothing fancy.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private string GetInProcessFileId(string filename)
        {
            return Regex
                .Match(filename, "[0-9|a-f]{8}-[0-9|a-f]{4}-[0-9|a-f]{4}-[0-9|a-f]{4}-[0-9|a-f]{12}")
                ?.Value;
        }

        private void CheckServerSideStatuses()
        {
            // grab all ach folders that require server-side parsing.
            var achfolders = _processOptionList
                .Where(option => !string.IsNullOrWhiteSpace(option.StatusEndPoint));

            // grab all of our files that need status updates.
            var sets = achfolders
                .SelectMany(option => new DirectoryInfo(option.InProcessFolder)
                    .GetFiles()
                    .Select(info => new { file = info.FullName, option = option }))
                .Select(set => new { id = GetInProcessFileId(set.file), file = set.file, option = set.option });

            // at this point we have a list of ids that match our filenames.
            // we just need to fire them off to the service and check for status updates.
            Parallel.ForEach(sets, set =>
            {
                var url = $"{set.option.StatusEndPoint}/{set.id}";
                try
                {
                    using (var client = new HttpClient())
                    {
                        // '.Result' is the await call.
                        var result = client
                            .GetStringAsync(url)
                            .Result;

                        var ditTrackingResponse = JsonConvert.DeserializeObject<DitTrackingResponse>(JObject.Parse(result)?.GetValue("data").ToString());
                        var status = ditTrackingResponse?.FileStatus;

                        var inputFile = new cInputFile(new FileInfo(set.file), new cInboxPathData(set.option))
                        {
                            SubmittedXML = $"<Batches SourceTrackingID=\"{set.id}\">{ditTrackingResponse?.BatchTrackingIds}</Batches>"
                        };

                        if (status == 2)
                        {
                            // completed!
                            // only add to the inflight queue once.
                            if (!_dctInFlightInputFiles.ContainsKey(Guid.Parse(set.id)))
                            {
                                AddItemToInFlightNodes(Guid.Parse(set.id), inputFile);
                                FileUtils.MoveFileToPendingResponse(inputFile, Guid.Parse(set.id));
                                GblUtil.OnOutputLogAndTrace($"Status check on file '{set.id}': Completed. ",
                                    "Server-Status-Check", LTAMessageType.Information, LTAMessageImportance.Debug);
                            }
                        }
                        else if (status == 1)
                        {
                            // processing
                            GblUtil.OnOutputLogAndTrace($"Status check on file '{set.id}': Still Processing. ",
                                "Server-Status-Check", LTAMessageType.Information, LTAMessageImportance.Debug);
                        }
                        
                        else if (status == 0)
                        {
                            AddItemToInFlightNodes(Guid.Parse(set.id), inputFile);
                            // not found
                            GblUtil.OnOutputLogAndTrace($"Status check on file '{set.id}': Not Found. ",
                                "Server-Status-Check", LTAMessageType.Warning, LTAMessageImportance.Essential);
                            FileUtils.MoveFileToError(
                                new FileInfo(set.file),
                                inputFile,
                                Guid.Parse(set.id));
                        }
                        else if (status == -1)
                        {
                            AddItemToInFlightNodes(Guid.Parse(set.id), inputFile);
                            // error
                            GblUtil.OnOutputLogAndTrace($"Status check on file '{set.id}': Error. ",
                                "Server-Status-Check", LTAMessageType.Warning, LTAMessageImportance.Essential);
                            FileUtils.MoveFileToError(
                                new FileInfo(set.file),
                                inputFile, 
                                Guid.Parse(set.id));
                        }
                    }
                }
                catch (Exception ex)
                {
                    GblUtil.OnOutputLogAndTrace($"Error communicating with Server at URL: '{url}': {ex.Message}",
                        "Server-Status-Check", LTAMessageType.Error, LTAMessageImportance.Essential);
                }
            });
        }

        private XmlDocument ValidateBatch(XmlDocument xmdUpload, string clientProcessCode, Guid transmissionID)
        {
            string sValidationResponse = string.Empty;
            XmlNodeList xmlNodes = null;
            XmlDocument xmdNewDoc = null;
           
            GblUtil.OnOutputLogAndTrace("Validating Batch Information","Validate Batch",LTAMessageType.Information, LTAMessageImportance.Debug);
            foreach (XmlNode xmnCurBatches in xmdUpload.SelectNodes("/Batches"))
            {
                if (ValidateBatchesNode(xmnCurBatches, out sValidationResponse))
                {
                    if (cCanonXMLTools.ExtractCanonNodeGroup(xmnCurBatches, out xmlNodes))
                    {
                        var canonXmlInfo =
                            new CanonXmlInfo
                                {
                                    ChildNodes = (from XmlNode CurrentNode in xmlNodes select CurrentNode.OuterXml).ToArray(),
                                    TrackingId = transmissionID,
                                    SchemaVersion = cCanonXMLTools.GetSchemaVersion(xmnCurBatches),
                                    ClientProcessCode = clientProcessCode,
                                    FileSignature = xmnCurBatches.Attributes["FileSignature"] == null ? string.Empty : xmnCurBatches.Attributes["FileSignature"].Value,
                                    FileHash = xmnCurBatches.Attributes["FileHash"] == null ? string.Empty : xmnCurBatches.Attributes["FileHash"].Value
 
                                };
                        if (!cCanonXMLTools.RepackageCanonNodeGroup(canonXmlInfo, out xmdNewDoc))
                        {
                            throw new Exception(
                                    string.Format("Failed to repackage Batch ID() into its own document"));
                        }
                    }
                }
                else
                    GblUtil.OnOutputLogAndTrace(string.Format("Batch node is invalid: {0}", sValidationResponse), "cDITUploadManager(UploadXML)",
                            LTAMessageType.Error, LTAMessageImportance.Essential);
            }
            return xmdNewDoc;
        }

        private XmlDocument ValidateClientGroup(XmlDocument xmdUpload, string clientProcessCode, Guid transmissionID)
        {
            string sValidationResponse = string.Empty;
            XmlDocument xmdNewDoc =  null;
            Dictionary<Guid, XmlNode> dctNewItems = new Dictionary<Guid, XmlNode>();
            XmlNodeList xmlNodes = null;

            GblUtil.OnOutputLogAndTrace("Validating Client Group","ValidateClientGroup",LTAMessageType.Information, LTAMessageImportance.Debug);
            
            foreach (XmlNode xmnCurClientGroups in xmdUpload.SelectNodes("/ClientGroups"))
            {
                if (ValidateSchema(cCanonXMLTools.GetSchemaVersion(xmnCurClientGroups), xmnCurClientGroups.OuterXml,
                        enmItemType.ClientSetup, out sValidationResponse))
                {
                    if (cCanonXMLTools.ExtractCanonNodeGroup(xmnCurClientGroups, out xmlNodes))
                    {
                       
                       if (!cCanonXMLTools.RepackageCanonNodeGroup((from XmlNode CurrentNode in xmlNodes select CurrentNode.OuterXml).ToArray(), transmissionID, cCanonXMLTools.GetSchemaVersion(xmnCurClientGroups), clientProcessCode, out xmdNewDoc))
                       {
                            throw new Exception(string.Format("Failed to repackage Client Setup ID(into its own document"));
                       }
                    }
                }
                else
                    GblUtil.OnOutputLogAndTrace(string.Format("Client Setup node has invalid format: {0}", sValidationResponse), "cDITUploadManager(UploadXML)",
                            LTAMessageType.Error, LTAMessageImportance.Essential);
            }
            return xmdNewDoc;
        }

        public bool UploadXML(XmlDocument xmdUpload, string clientProcessCode, cInputFile inputFile, TrackingKeys recordTrackingKeys)
        {
            bool bAns = false;
            XmlNode xmnClientSetups = xmdUpload.SelectSingleNode("/");
            XmlDocument xmdNewDoc = null;
            string sValidationResponse = string.Empty;
            string sUploadResponse = string.Empty;
            enmItemType itemType = enmItemType.Batch;            

            xmdNewDoc = ValidateBatch(xmdUpload, clientProcessCode, recordTrackingKeys.SourceTrackingId);
            if (xmdNewDoc == null)
            {
                itemType = enmItemType.ClientSetup;
                xmdNewDoc = ValidateClientGroup(xmdUpload,clientProcessCode, recordTrackingKeys.SourceTrackingId);
            }

            if (xmdNewDoc != null)
            {
                if (SendXMLDocument(xmdNewDoc.InnerXml, itemType, recordTrackingKeys.AuditDateKey, out sUploadResponse))
                {
                    inputFile.SubmittedXML = xmdNewDoc.InnerXml;
                    AddItemToInFlightNodes(recordTrackingKeys.SourceTrackingId, inputFile);
                    bAns = true;

                }
                else
                {
                    throw new Exception(string.Format("Error in UploadXML: {0}", sUploadResponse));
                }
            }
            else
            {
                GblUtil.OnOutputLogAndTrace("Failed to add source Tracking ID to client batch information. Can not upload document", "cDITUploadManager(UploadXML)",
                       LTAMessageType.Error, LTAMessageImportance.Essential);
            }
            return bAns;
        }



        /// <summary>
        /// call this to add items to in flight node
        /// </summary>
        /// <param name="transmissionID"></param>
        /// <param name="inputFile"></param>
        public void AddItemToInFlightNodes(Guid transmissionID, cInputFile inputFile)
        {
            _dctInFlightInputFiles.Add(transmissionID, inputFile);
        }
        private bool ValidateBatchesNode(XmlNode xmnBatches, out string sBatchValidationResponse)
        {
            bool bAns = false;
            XDocument xmdMessages = null;

            sBatchValidationResponse = string.Empty;
            /* Before we can validate the schema we must ascertain what version of the schema is used, that is what GetSchemaVersion does
                 This means before we validate the xml we must parse it enough to get the schema version
                 The schema version must be found in the "XSDVersion" attribute of the root element otherwise it will not be found
            */
            if (ValidateSchema(cCanonXMLTools.GetSchemaVersion(xmnBatches), xmnBatches.OuterXml,  enmItemType.Batch, 
                    out sBatchValidationResponse))
             {
                bAns = true;
            }
            if (xmdMessages == null)
                sBatchValidationResponse = string.Empty;
            else
                sBatchValidationResponse = string.Join(", ", (from XElement xmeCurMessage in xmdMessages.Element("Root").Element("Messages").Elements()
                                                              select xmeCurMessage.Value).ToArray());
            return bAns;
         }

        private bool ValidateSchema(string sXSDVersion, string sXML, enmItemType itemType, out string sResponseXML)
        {
            bool bValid = false;
            string sXSD = string.Empty;
            
            sResponseXML = string.Empty;
            try
            {
                string type = (itemType== enmItemType.Batch) ? "Batch" : "Client";
                if (_dicDITLink.GetSchemaDefinition(type,sXSDVersion, out sXSD, out sResponseXML))
                 {
                    if (!(bValid = XML_XSD_Validator.Validate(sXML, sXSD)))
                    {
                        GblUtil.OnOutputLogAndTrace("Error validing XML. Error is " + XML_XSD_Validator.GetError(),"ValidateSchema",LTAMessageType.Information, LTAMessageImportance.Essential);
                        sResponseXML = BuildMsgDoc("Error",
                                XML_XSD_Validator.GetError().Replace("\r\n", "\n")
                                .Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries)).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                sResponseXML = BuildMsgDoc("Error", string.Format("Error in ValidateSchema: {0}", ex.Message)).ToString();
            }
            return bValid;
        }

        private bool SendXMLDocument(string sNewItem, enmItemType ttyType, int auditDateKey, out string sUploadResponse)
        {
            cRetryCounter rtyCounter = GblUtil.GetRetryCounter();

            sUploadResponse = "";

            while (rtyCounter.CycleRetry())
            {
                try
                {
                    switch (ttyType)
                    {
                        case enmItemType.Batch:
                            rtyCounter.HasSucceeded = _dicDITLink.SendBatchData(sNewItem, auditDateKey, out sUploadResponse);
                            break;

                        case enmItemType.ClientSetup:
                            rtyCounter.HasSucceeded = _dicDITLink.SendClientSetup(sNewItem, auditDateKey, out sUploadResponse);
                            break;

                    }
                    if (!rtyCounter.HasSucceeded)
                    {
                        GblUtil.OnOutputLogAndTrace("Failed to send XML document" +
                                (string)(rtyCounter.ShouldRetry ? ", Retrying" : ""),
                                "cDITUploadManager(SendXMLDocument)",
                                LTAMessageType.Warning, LTAMessageImportance.Verbose);
                    }
                }
                catch (Exception ex)
                {
                    string sErrorMessage = string.Format("Failed to send XML document: {0}", ex.Message);
                    if(rtyCounter.ShouldRetry)
                        GblUtil.OnOutputLogAndTrace(sErrorMessage + ", Retrying", "cDITUploadManager (SendXMLDocument)",
                                LTAMessageType.Warning, LTAMessageImportance.Verbose);
                    else
                    {
                        //If we are here things are bad and we should take a break until there is another upload attempt
                 
                        throw new Exception(sErrorMessage, ex);
                    }
                }
            }
            return rtyCounter.HasSucceeded;
        }

        public bool SendBatchMeasureData(BatchFileImportMeasures batchFileImportMeasure)
        {
            cRetryCounter rtyCounter = GblUtil.GetRetryCounter();

            while (rtyCounter.CycleRetry())
            {
                try
                {
                    rtyCounter.HasSucceeded = _dicDITLink.SendBatchMeasureData(batchFileImportMeasure);
                    if (!rtyCounter.HasSucceeded)
                    {
                        GblUtil.OnOutputLogAndTrace("Failed to send measure data" +
                                (string)(rtyCounter.ShouldRetry ? ", Retrying" : ""),
                                "cDITUploadManager(SendBatchMeasureData)",
                                LTAMessageType.Warning, LTAMessageImportance.Verbose);
                    }
                }
                catch (Exception ex)
                {
                    string sErrorMessage = $"Failed to send batch import measure data: {ex.Message}";
                    if (rtyCounter.ShouldRetry)
                        GblUtil.OnOutputLogAndTrace(sErrorMessage + ", Retrying", "cDITUploadManager (SendBatchMeasureData)",
                                LTAMessageType.Warning, LTAMessageImportance.Verbose);
                    else
                    {
                        //If we are here things are bad and we should take a break until there is another upload attempt

                        throw new Exception(sErrorMessage, ex);
                    }
                }
            }
            return rtyCounter.HasSucceeded;
        }

        private XDocument BuildMsgDoc(string sMsgType, string sErrorMessage)
        {
            return BuildMsgDoc(null, sMsgType, sErrorMessage);
        }

        private XDocument BuildMsgDoc(XDocument xdcOriginal, string sMsgType, string sErrorMessage)
        {
            return BuildMsgDoc(xdcOriginal, sMsgType, new string[] { sErrorMessage });
        }

        private XDocument BuildMsgDoc(string sMsgType, string[] sErrorMessages)
        {
            return BuildMsgDoc(null, sMsgType, sErrorMessages);
        }
        private XDocument BuildMsgDoc(XDocument xdcOriginal, string sMsgType, string[] sErrorMessages)
        {
            XElement nodeMessages;

            if (xdcOriginal == null)
                xdcOriginal = CreateMsgDoc();
            nodeMessages = GetMessagesNode(xdcOriginal);
            foreach(string sCurErrorMessage in sErrorMessages)
            {
                XElement xmeNewNode;
                nodeMessages.Add(xmeNewNode = new XElement("Message", new XAttribute("Type", sMsgType)));
                xmeNewNode.Value = sCurErrorMessage;
            }
            return (xdcOriginal);
        }

        private static XElement GetMessagesNode(XDocument xdcOriginal)
        {
            XElement nodeMessages;
            nodeMessages = xdcOriginal.Element("Root").Element("Messages");
            if (nodeMessages == null)
                xdcOriginal.Element("Root").Add(nodeMessages = new XElement("Messages"));
            return nodeMessages;
        }

        private XDocument CreateMsgDoc()
        {
            return new XDocument(new XElement("Root", new XElement("Messages")));
        }
        
        public void UpdateItemFields(DateTime? dttUpdateFrom,  List<string> lstFields)
        {
            cRetryCounter rtcRetryControler = GblUtil.GetRetryCounter();
            string strKeyword = string.Empty;

            while (rtcRetryControler.CycleRetry())
            {
                try
                {
                    bool success;

                    using (DataSet dtsData = _dicDITLink.GetItemDataSetupField(GblUtil.ClientOptions.IconImportType, dttUpdateFrom, out success))
                    {
                        
                        if (success && dtsData.Tables.Count > 0)
                        {
                            foreach (DataRow dRow in dtsData.Tables[0].Rows)
                            {
                                strKeyword = dRow["Keyword"].ToString();
                                if (!lstFields.Contains(strKeyword))
                                {
                                    lstFields.Add(strKeyword);
                                }
                                rtcRetryControler.HasSucceeded = true;
                            }
                        }
                        else
                        {
                            string sErrorMessage = "Failed to get item fields dataset from Data Import server-side service";

                            rtcRetryControler.HasSucceeded = false;
                            if (rtcRetryControler.ShouldRetry)
                                GblUtil.OnOutputLogAndTrace(sErrorMessage + ", attempting retry", "UpdateItemFields",
                                        LTAMessageType.Error, LTAMessageImportance.Verbose);
                            else
                                throw new Exception(sErrorMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    rtcRetryControler.HasSucceeded = false;
                    if (rtcRetryControler.ShouldRetry)
                        GblUtil.OnOutputLogAndTrace(
                                string.Format("Error: {0}, attempting retry", ex.Message), "UpdateItemFields",
                                LTAMessageType.Error, LTAMessageImportance.Verbose);
                    else
                        throw new Exception(string.Format("Error in UpdateItemFields: {0}", ex.Message), ex);
                }
            }
        }

        public void UpdateBatchFields(DateTime? dttUpdateFrom,  List<string> lstFields)
        {
            bool success;
            string strKeyword;
            cRetryCounter rtcRetryControler = GblUtil.GetRetryCounter();

            try
            {
                while (rtcRetryControler.CycleRetry())
                {
                    using (DataSet dtsData = _dicDITLink.GetBatchDataSetupField(GblUtil.ClientOptions.IconImportType, dttUpdateFrom, out success))
                    {
                        if (success && dtsData.Tables.Count > 0)
                        {
                            foreach (DataRow dRow in dtsData.Tables[0].Rows)
                            {
                                strKeyword = dRow["Keyword"].ToString();
                                if (!lstFields.Contains(strKeyword))
                                {
                                    lstFields.Add(strKeyword);
                                }
                                rtcRetryControler.HasSucceeded = true;
                            }
                        }
                        else
                        {
                            string sErrorMessage = "Failed to get batch fields dataset from Data Import server-side service";

                            rtcRetryControler.HasSucceeded = false;
                            if (rtcRetryControler.ShouldRetry)
                                GblUtil.OnOutputLogAndTrace(sErrorMessage + ", attempting retry", "UpdateBatchFields",
                                        LTAMessageType.Error, LTAMessageImportance.Verbose);
                            else
                                throw new Exception(sErrorMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
             {
                rtcRetryControler.HasSucceeded = false;
                if (rtcRetryControler.ShouldRetry)
                    GblUtil.OnOutputLogAndTrace(
                            string.Format("Error: {0}, attempting retry", ex.Message), "UpdateBatchFields",
                            LTAMessageType.Error, LTAMessageImportance.Verbose);
                else
                    throw new Exception(string.Format("Error in UpdateBatchFields: {0}", ex.Message), ex);
            }
         }

        public void UpdateDocumentTypes(ref DateTime? dttUpdateFrom, ref Dictionary<string, string> dctDocTypes)
        {
            DateTime dttNewUpdateFrom = DateTime.Now;
            cRetryCounter rtcRetryControler = GblUtil.GetRetryCounter();
            bool success;
            try
            {
                while (rtcRetryControler.CycleRetry())
                {
                    using (DataSet dtsData = _dicDITLink.GetDocumentTypes(dttUpdateFrom, out success))
                    {
                        if (success)
                        {
                            if (cDataToCollectionTools.DataSetToDictionary(dtsData, "FileDescriptor", "IMSDocumentType",
                                    out dctDocTypes))
                            {
                                dttUpdateFrom = dttNewUpdateFrom;
                                rtcRetryControler.HasSucceeded = true;
                            }
                        }
                        else
                        {
                            string sErrorMessage = "Failed to get document types from the Data Import server-side service";

                            rtcRetryControler.HasSucceeded = false;
                            if (rtcRetryControler.ShouldRetry)
                                GblUtil.OnOutputLogAndTrace(sErrorMessage + ", attempting retry", "UpdateDocumentTypes",
                                        LTAMessageType.Error, LTAMessageImportance.Verbose);
                            else
                                throw new Exception(sErrorMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtcRetryControler.HasSucceeded = false;
                if (rtcRetryControler.ShouldRetry)
                    GblUtil.OnOutputLogAndTrace(string.Format("Error: {0}, attempting retry", ex.Message), "UpdateDocumentTypes",
                            LTAMessageType.Error, LTAMessageImportance.Verbose);
                else
                    throw new Exception(string.Format("Error in UpdateDocumentType: {0}", ex.Message), ex);
            }
            
        }

        public bool UpdateImageRPSAliasList(int iBankID, int[] aiClientIDs, DateTime? dttLastUpdateDate, Hashtable dctImageRPSAliasList)
        {
            cRetryCounter rtcRetryController = GblUtil.GetRetryCounter();

            DataSet dtsData = null;
            if (dctImageRPSAliasList == null)
            {
                dctImageRPSAliasList = new Hashtable();
            }

            while(rtcRetryController.CycleRetry())
            {
                try
                {
                    bool success;

                    rtcRetryController.HasSucceeded = true;
                    foreach (int iCurClientID in aiClientIDs)
                    {
                        dtsData = _dicDITLink.GetImageRPSAliasMappings(iBankID, iCurClientID, dttLastUpdateDate, out success);
                        rtcRetryController.HasSucceeded &= success;
                        if (success)
                            {
                                if (dtsData != null && dtsData.Tables.Count > 0)
                                {
                                    foreach (DataRow rowCurRow in dtsData.Tables[0].Rows)
                                    {

                                        int iClientID = -1;
                                        if (Int32.TryParse(rowCurRow["SiteClientAccountID"].ToString(), out iClientID))
                                        {

                                            if (!dctImageRPSAliasList.ContainsKey(iClientID))
                                            {
                                                dctImageRPSAliasList.Add(iClientID, new Hashtable());
                                            }
                                            cDataToCollectionTools.UpsertHashTable((Hashtable)dctImageRPSAliasList[iClientID],
                                                    string.Format("{0}|{1}", rowCurRow["AliasName"].ToString(),
                                                            rowCurRow["DocType"].ToString()),
                                                    new string[] {rowCurRow["ExtractType"].ToString(),
                                                                rowCurRow["FieldType"].ToString()});
                                        }
                                        else
                                        {
                                            GblUtil.OnOutputLogAndTrace("Failed to convert client ID to integer", "UpdateImageRPSAliasList", LTAMessageType.Error, LTAMessageImportance.Essential);
                                        }
                                    }
                                }
                                else
                                {
                                    rtcRetryController.HasSucceeded = false;
                                    throw new Exception(
                                            string.Format("No tables where found in the Dataset{0}",
                                                rtcRetryController.ShouldRetry ? ", attempting retry" : ""));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    rtcRetryController.HasSucceeded = false;
                    string sErrorMessage = string.Format("Error in UpdateImageRPSAliasList: {0}{1}",
                            ex.Message,
                            rtcRetryController.ShouldRetry ? "; Attempting retry" : "");

                    if (rtcRetryController.ShouldRetry)
                    {
                         GblUtil.OnOutputLogAndTrace(sErrorMessage, this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Verbose);
                    }
                    else
                    {
                        throw new Exception(sErrorMessage);
                    }
                }
                finally
                {
                    if (dtsData != null)
                        dtsData.Dispose();
                }
            }

            return rtcRetryController.HasSucceeded; 
        }
       

        #region IDisposable Implementation

        ~cDITUploadManager()
        {
            Dispose(false);
         }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool bDisposing)
        {
            try
            {
                if (!_bDisposed)
                {
                    if (bDisposing)
                    {
                        if (_tmrStatusUpdater != null)
                            _tmrStatusUpdater.Dispose();


                        if (_dctInFlightInputFiles != null)
                            _dctInFlightInputFiles.Clear();


                        _bDisposed = true;

                    }

                    _dicDITLink.EndSession();
                }
            }
            catch
            {
            }
        }
        #endregion

        public void SetPending(IEnumerable<PendingItem> pendingItems)
        {
            this.InFlightNodes.Clear();
            foreach (var pendingItem in pendingItems)
            {
                this.InFlightNodes.Add(pendingItem.SourceTrackingId, pendingItem.PendingFile);
            }
        }

        public IEnumerable<PendingItem> GetPending()
        {
            var pendingItems = new List<PendingItem>();

            foreach (var node in InFlightNodes)
            {
                pendingItems.Add(new PendingItem {SourceTrackingId = node.Key, PendingFile = node.Value});
            }

            return pendingItems;
        }

        private IEnumerable<ResponseItem> GetResponses()
        {
            cRetryCounter rtyCounter = GblUtil.GetRetryCounter();

            var responses = new List<ResponseItem>();

            while (rtyCounter.CycleRetry())
            {
                try
                {
                    foreach (cInboxPathData ipdCurInputPath in _processOptionList)
                    {
                        var serviceContext = new ResponseServiceContext
                        {
                            ClientProcessCode = ipdCurInputPath.ClientProcessCode,
                            Proxy = _dicDITLink,
                            PendingService = this
                        };
                        if (ipdCurInputPath.FileType == enmFileType.ClientSetup)
                        {

                            var responseManager = new ResponseManager(
                                new ClientSetupResponseService(serviceContext, _auditingServicesClient));
                            responses.AddRange(responseManager.GetResponses());
                        }

                        if (ipdCurInputPath.FileType == enmFileType.Batch)
                        {
                            var responseManager = new ResponseManager(
                                new BatchResponseService(serviceContext, _auditingServicesClient));
                            responses.AddRange(responseManager.GetResponses());
                        }

                        if (ipdCurInputPath.FileType == enmFileType.Both)
                        {
                            var clientSetupResponseManager = new ResponseManager(
                                new ClientSetupResponseService(serviceContext, _auditingServicesClient));
                            var batchDataResponseManager = new ResponseManager(
                                new BatchResponseService(serviceContext, _auditingServicesClient));
                            responses.AddRange(clientSetupResponseManager.GetResponses());
                            responses.AddRange(batchDataResponseManager.GetResponses());
                        }
                    }

                    rtyCounter.HasSucceeded = true;
                }
                catch (Exception ex)
                {
                    string sErrorMessage = String.Format("Failed to retrieve response dataset from the Data Import Service, Error: {0}", ex.Message);
                    if (rtyCounter.ShouldRetry)
                        GblUtil.OnOutputLogAndTrace(sErrorMessage + ", Retrying", "cDITUploadManager (GetDITResponses)",
                                LTAMessageType.Warning, LTAMessageImportance.Verbose);
                    else
                        throw new Exception(sErrorMessage, ex);
                }
            }
            return responses;
        }
    }
}
