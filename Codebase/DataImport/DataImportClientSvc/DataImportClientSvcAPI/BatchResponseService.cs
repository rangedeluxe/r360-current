﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WFS.LTA.DataImport.DataImportClientSvcAPI.ImportSteps;
using WFS.LTA.DataImport.DataImportClientSvcAPI.Interfaces;
using WFS.RecHub.AuditingServicesCommon.Interfaces;

/******************************************************************************
** Wausau
** Copyright © 1997-2016 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2016.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:  Wausau
* Date:     01/26/2016
*
* Purpose:  
*
* Modification History
* WI 000000 WFS 01/26/2016   Created
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvcAPI
{
    public class BatchResponseService : ResponseService
    {
        private readonly IAuditingDit _auditingServicesClient;

        public BatchResponseService(ResponseServiceContext context, IAuditingDit auditingServicesClient)
            : base(context)
        {
            _auditingServicesClient = auditingServicesClient;
        }

        public override IEnumerable<ResponseItem> GetResponses()
        {
            var isSuccessful = false;
            using (DataSet ds = base.Context.Proxy.GetBatchDataResponses(base.Context.ClientProcessCode, out isSuccessful))
            {
                var responses = ToImportResponses(ds);
                foreach (var response in responses)
                {
                    var importSteps = new List<IImportStep>();
                    importSteps.Add(new HandleDataImportResponse(base.Context.PendingService, new ImportStepRequest { ResponseItem = response }));
                    importSteps.Add(new AuditStep(Context.PendingService,
                        new ImportStepRequest {ResponseItem = response}, _auditingServicesClient));

                    if (response.IsSuccessful)//only going to add the image upload step if the response was successful
                        importSteps.Add(GetImageImportStep(response));

                    importSteps.Add(new WriteRpsQueueFile(base.Context.PendingService, new ImportStepRequest { ResponseItem = response }));
                    importSteps.Add(new WriteNativeResponseFile(base.Context.PendingService, new ImportStepRequest { ResponseItem = response }));
                    importSteps.Add(new UpdateImportStatus(base.Context.PendingService, base.Context.Proxy, new ImportStepRequest { ResponseItem = response }));
                    response.ImportSteps = new List<IImportStep>(importSteps);
                }

                return responses;

            }
        }

        private IImportStep GetImageImportStep(ResponseItem response)
        {
            if (response == null)
                throw new ArgumentNullException("response");

            if (GblUtil.ClientOptions.ImageSplitLocation == ImageSplitLocation.Server)
            {
                return new SendAllImages(base.Context.PendingService, base.Context.Proxy,
                    new ImportStepRequest {ResponseItem = response});
            }

            return new UploadImages(base.Context.PendingService, Context.Proxy, Context.Proxy,
                new ImportStepRequest {ResponseItem = response});
        }
    }
}
