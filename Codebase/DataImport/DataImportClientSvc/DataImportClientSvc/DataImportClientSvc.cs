﻿using System;
using System.ServiceProcess;
using System.Threading;
using Wfs.Logging;
using Wfs.Logging.NLog;
using WFS.LTA.DataImport.DataImportClientSvcAPI;
using WFS.LTA.Common;
using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     08/03/2012
*
* Purpose:  This class hosts the DataImportClientSvcAPI as
*
* Modification History
* WI 126987 CMC 01/20/2014
*  - Updated to use 2.00 libraries.
*******************************************************************************/

namespace WFS.LTA.DataImport.DataImportClientSvc
{
    public partial class DataImportClientSvc : ServiceBase
    {
        private cDITFileProcessor dfpProcessor;

		private bool _isPaused = false;

        private WfsLog Log { get; set; }

	    public bool IsServicePaused()
	    {
		    return _isPaused;
	    }

        public DataImportClientSvc()
        {
            try
            {
                Log = new WfsLog();
                Log.Trace("DIT Client Service is initializing");
	            GblUtil.OutputLog += XProc_LogEvent;
                GblUtil.InitializeClientOptions();
                GblUtil.WriteClientOptionsConfigurationLog();

                InitializeComponent();
                evlLog.Source = "Data Import Service Client";
                evlLog.WriteEntry("Starting Data Client Service");

                evlLog.WriteEntry("Ini File location " + ipoINILib.IniFileLocation + " Log File is:" +
                                  GblUtil.ClientOptions.logFilePath + " Log Level:" + GblUtil.ClientOptions.loggingDepth);
                GblUtil.OnOutputLogAndTrace("DataImport Client Service is starting", "DataImportClientSvc",
                    LTAMessageType.Information, LTAMessageImportance.Essential);
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace(string.Format("Failed to initialize service: {0}", ex.Message),
                    "DataImportClientSvc", ex);
                Log?.Trace("DIT Client Service is stopping ungracefully");
                this.Stop();
            }
        }

	    private void InitializeProcessor()
	    {
		    string sError = string.Empty;

		    try
		    {
			    Log.Trace("DIT Client Service is starting");
				dfpProcessor = new cDITFileProcessor();
			    dfpProcessor.CheckForPause = new cDITFileProcessor.PauseProcessingDelegate(IsServicePaused);
			    GblUtil.OnOutputLogAndTrace("Service Starting", "OnStart", LTAMessageType.Information,
				    LTAMessageImportance.Essential);

			    if (!dfpProcessor.Initialize(Log, out sError))
			    {
				    GblUtil.OnOutputLogAndTrace($"Initialization Error: {sError}", "OnStart",
					    LTAMessageType.Information, LTAMessageImportance.Essential);
				    this.Stop();
			    }
			    evlLog.WriteEntry("Service Started");
			    GblUtil.OnOutputLogAndTrace("Service Started", "OnStart", LTAMessageType.Information,
				    LTAMessageImportance.Essential);
		    }
		    catch (Exception ex)
		    {
			    GblUtil.OnOutputLogAndTrace($"Failed to start service: {ex.Message}", "OnStart", ex);
			    Log.Trace("DIT Client Service is stopping ungracefully");
			    this.Stop();
		    }
		}

		protected override void OnStart(string[] args)
        {
			// NOTE: Need to move this to anthor thread as we allow continuous retries due to 
			// see: https://stackoverflow.com/questions/31529508/windows-service-execute-logic-after-onstart
			new Thread(() =>
	        {
		        Thread.CurrentThread.IsBackground = true;
		        InitializeProcessor();

	        }).Start();
		}

		protected override void OnPause()
        {
            try
            {
	            _isPaused = true;
                evlLog.WriteEntry("Service Paused");
                GblUtil.OnOutputLogAndTrace("Service Paused", "OnPause", LTAMessageType.Information,
                    LTAMessageImportance.Essential);
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace(string.Format("Failed to pause service: {0}", ex.Message), "OnPause", ex);
                this.Stop();
            }
        }

        protected override void OnContinue()
        {
            try
            {
	            _isPaused = false;
                evlLog.WriteEntry("Service Continued");
                GblUtil.OnOutputLogAndTrace("Service Continued", "OnContinue", LTAMessageType.Information,
                    LTAMessageImportance.Essential);
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace(string.Format("Failed to continue service: {0}", ex.Message), "OnContinue",
                    ex);
                this.Stop();
            }
        }

        protected override void OnStop()
        {
            try
            {
                Log?.Trace("DIT Client Service is stopping");
                evlLog?.WriteEntry("Service Stopped");
                GblUtil.OnOutputLogAndTrace("Service Stopped", "OnStop", LTAMessageType.Information,
                    LTAMessageImportance.Essential);
            }
            catch (Exception ex)
            {
                GblUtil.OnOutputLogAndTrace(string.Format("Failed while shutting down service: {0}", ex.Message),
                    "OnStop", ex);
                this.Stop();
            }
            finally
            {
                dfpProcessor.Dispose();
                Log?.Dispose();
            }
        }
        private void XProc_LogEvent(string msg, string src, LTAMessageType messageType,
            LTAMessageImportance messageImportance)
        {
            Log.logEvent(msg, src, messageType, messageImportance);
        }
    }
}
