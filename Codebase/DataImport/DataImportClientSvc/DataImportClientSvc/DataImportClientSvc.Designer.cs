﻿using System.ComponentModel;
using System.Diagnostics;


namespace WFS.LTA.DataImport.DataImportClientSvc
    {
    partial class DataImportClientSvc
        {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            this.evlLog = new EventLog();
            ((ISupportInitialize)(this.evlLog)).BeginInit();
            // 
            // svcShoehorn
            // 
            this.CanPauseAndContinue = true;
            this.ServiceName = "WFS Data Import Client Service";
            ((ISupportInitialize)(this.evlLog)).EndInit();

            }

        #endregion

        private EventLog evlLog;
        }
    }
