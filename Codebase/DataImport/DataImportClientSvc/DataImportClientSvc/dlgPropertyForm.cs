﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ConfigDialog
    {
    public partial class dlgPropertyForm : Form
        {
        public dlgPropertyForm(object objSelectedObject):this()
            {
            prgDisplay.SelectedObject = objSelectedObject;
            }

        public object SelectedObject
            {
            get
                {
                return prgDisplay.SelectedObject;
                }
            set
                {
                prgDisplay.SelectedObject=value;
                }
            }

        public dlgPropertyForm()
            {
            InitializeComponent();
            }

        private void cmdOK_Click(object sender, EventArgs e)
            {
            this.DialogResult = DialogResult.OK;
            this.Close();
            }
        }
    }
