﻿using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration.Install;

namespace WFS.LTA.DataImport.DataImportClientSvc
    {
    partial class ProjectInstaller
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            this.svcProcessInstaller = new ServiceProcessInstaller();
            this.svcInstaller = new ServiceInstaller();
            // 
            // svcProcessInstaller
            // 
            this.svcProcessInstaller.Account = ServiceAccount.LocalSystem;
            this.svcProcessInstaller.Installers.AddRange(new Installer[] {
            this.svcInstaller});
            this.svcProcessInstaller.Password = null;
            this.svcProcessInstaller.Username = null;
            this.svcProcessInstaller.AfterInstall += new InstallEventHandler(this.svcProcessInstaller_AfterInstall);
            // 
            // svcInstaller
            // 
            this.svcInstaller.DisplayName = "WFS Data Import Client Service";
            this.svcInstaller.ServiceName = "WFS Data Import Client Service";
            this.svcInstaller.AfterInstall += new InstallEventHandler(this.svcInstaller_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new Installer[] {
            this.svcProcessInstaller});

            }

        #endregion

        private ServiceProcessInstaller svcProcessInstaller;
        private ServiceInstaller svcInstaller;
        }
    }