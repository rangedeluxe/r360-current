﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WFS.LTA.DataImport.DataImportClientSvcAPI;


/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/13/2012
*
* Purpose:  This class holds tools for reading and altering Canonical Nodes
*
* Modification History
*
* Created
*  CR 52267: CEJ 13/06/2012
*   - DIT Client
*
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportClientSvc
    {
    class cDataImportClientSvcSettings: cSettings 
        {
        public static cDataImportClientSvcSettings Settings()
            {
            return new cDataImportClientSvcSettings();
            }

        public bool LoadOnStartup
            {
            get
                {
                return base.GetSetting("LoadOnStartup", "False").ToLower() == "true";
                }
            set
                {
                base.SetSetting("LoadOnStartup", value ? "True" : "False");
                }
            }
        }
    }
