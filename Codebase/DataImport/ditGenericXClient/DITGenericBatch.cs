﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the batch data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
* WI 173024 SAS 10/22/2014 
* Changes done to set proper batch sequence numbers
******************************************************************************/

namespace DMP.Generic.Components
{
    public class DITGenericBatch : Batch
    {        
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        /// <summary>
        /// Transaction Collection
        /// </summary>
        private DITGenericTransactionCollection m_TransactionCollection = null;
        private int m_UniqueBatchID;        
        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>        
        /// <param name="oDataImportXClient"></param>
        public DITGenericBatch(cDataImportXClientBase oDataImportXClient)
        {   
            this.m_DataImportXClient = oDataImportXClient;
            m_TransactionCollection = new DITGenericTransactionCollection();
        }        
        #endregion Class constructors

        #region Class Properties
        /// <summary>
        /// Unique Identifier for Batch
        /// </summary>
        public int UniqueBatchID
        {
            get { return m_UniqueBatchID; }
            set { m_UniqueBatchID = value; }
        }
        /// <summary>
        /// m_Transaction collection
        /// </summary>
        public DITGenericTransactionCollection TransactionCollection
        {
            get { return m_TransactionCollection; }
        }      
        #endregion Class Properties
                
        
        #region Class Methods
        /// <summary>
        /// Set Batch Data
        /// </summary>
        /// <param name="drBatchRow"></param>
        public bool SetData(DataRow drBatchRow)
        {
            bool bRetValue = false;
            try
            {
                //Record Identifier Batch_ID
                this.UniqueBatchID = Convert.ToInt32(drBatchRow[DITGenericCommon.UNIQUE_BATCH_ID].ToString());
                //Processing Date
                base.ProcessingDate = Convert.ToDateTime(drBatchRow[DITGenericCommon.BATCH_PROCESS_DATE].ToString());
                //Deposit Date             
                base.DepositDate = Convert.ToDateTime(drBatchRow[DITGenericCommon.BATCH_DEPOSIT_DATE].ToString());
                //Set Default Value for BankID
                base.BankID = 0;
                //WorkGroupID
                int clientId = Convert.ToInt32(drBatchRow[DITGenericCommon.BATCH_WORK_GRP_ID].ToString());
                if (clientId > 0 )
                    base.ClientID = clientId;
                else
                    base.ClientID = 0;
                //Set Default Value for BankCueID
                base.BatchCueID = 0;
                //BatchID
                base.BatchID = Convert.ToInt32(drBatchRow[DITGenericCommon.BATCH_ID].ToString()); ;
                //Batch Date
                base.BatchDate = Convert.ToDateTime(drBatchRow[DITGenericCommon.BATCH_DATE].ToString());
                //Set Default Value for BatchSiteCode
                base.BatchSiteCode = 0;
                //Batch Number
                if ((drBatchRow.Table.Columns.Contains(DITGenericCommon.BATCH_NUMBER)) && (DITGenericCommon.IsAllDigits(drBatchRow[DITGenericCommon.BATCH_NUMBER].ToString())))
                {
                    base.BatchNumber = Convert.ToInt32(drBatchRow[DITGenericCommon.BATCH_NUMBER].ToString());
                }
                //Batch Payment Type
                base.PaymentType = drBatchRow[DITGenericCommon.BATCH_PAYMENT_TYPE].ToString();
                //Batch Source
                base.BatchSource = drBatchRow[DITGenericCommon.BATCH_BATCH_SRC].ToString();
                //BatchTrackID            
                base.BatchTrackID = Guid.NewGuid();
                bRetValue = true;
            }
            catch(Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in SetData : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }


        /// <summary>
        /// Validate Data
        /// </summary>
        /// <param name="drBatchRow"></param>
        public bool DoValidate(DataRow drBatchRow)
        {
            bool bRetValue = true;
            try
            {

                //Processing Date      
                if (!drBatchRow.Table.Columns.Contains(DITGenericCommon.BATCH_PROCESS_DATE))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in batch element.", DITGenericCommon.BATCH_PROCESS_DATE), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }
                else
                {
                    if (string.IsNullOrEmpty(DITGenericCommon.ValidateDate(drBatchRow[DITGenericCommon.BATCH_PROCESS_DATE].ToString())))
                    {
                        m_DataImportXClient.DoLogEvent(string.Format("Error: Processing Date:{0} is invalid in batch element.", drBatchRow[DITGenericCommon.BATCH_PROCESS_DATE]), this.GetType().Name, LogEventType.Error);
                        bRetValue = false;
                    }
                }

                //Deposit Date             
                if (!drBatchRow.Table.Columns.Contains(DITGenericCommon.BATCH_DEPOSIT_DATE))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in batch element.", DITGenericCommon.BATCH_DEPOSIT_DATE), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }
                else
                {
                    if (string.IsNullOrEmpty(DITGenericCommon.ValidateDate(drBatchRow[DITGenericCommon.BATCH_DEPOSIT_DATE].ToString())))
                    {
                        m_DataImportXClient.DoLogEvent(string.Format("Error: DepositDate:{0} is invalid in batch element.", drBatchRow[DITGenericCommon.BATCH_DEPOSIT_DATE]), this.GetType().Name, LogEventType.Error);
                        bRetValue = false;
                    }
                }

                //WorkGroupID             
                if (!drBatchRow.Table.Columns.Contains(DITGenericCommon.BATCH_WORK_GRP_ID))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in batch element.", DITGenericCommon.BATCH_WORK_GRP_ID), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }
                else
                {
                    if (!DITGenericCommon.IsInteger(drBatchRow[DITGenericCommon.BATCH_WORK_GRP_ID].ToString()))
                    {
                        m_DataImportXClient.DoLogEvent(
                            string.Format("Error: WorkGroupID:{0} is not numeric in batch element.",
                                drBatchRow[DITGenericCommon.BATCH_WORK_GRP_ID]), this.GetType().Name,
                            LogEventType.Error);
                        bRetValue = false;
                    }
                }

                //BatchID
                if (!drBatchRow.Table.Columns.Contains(DITGenericCommon.BATCH_ID))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in batch element.", DITGenericCommon.BATCH_ID), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }
                else
                {
                    if (!DITGenericCommon.IsAllDigits(drBatchRow[DITGenericCommon.BATCH_ID].ToString()))
                    {
                        m_DataImportXClient.DoLogEvent(string.Format("Error: BatchID:{0} is not numeric in batch element.", drBatchRow[DITGenericCommon.BATCH_ID]), this.GetType().Name, LogEventType.Error);
                        bRetValue = false;
                    }
                }

                //Batch Date
                if (!drBatchRow.Table.Columns.Contains(DITGenericCommon.BATCH_DATE))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in batch element.", DITGenericCommon.BATCH_DATE), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }
                else
                {
                    if (string.IsNullOrEmpty(DITGenericCommon.ValidateDate(drBatchRow[DITGenericCommon.BATCH_DATE].ToString())))
                    {
                        m_DataImportXClient.DoLogEvent(string.Format("Error: BatchDate:{0} is invalid in batch element.", drBatchRow[DITGenericCommon.BATCH_DATE]), this.GetType().Name, LogEventType.Error);
                        bRetValue = false;
                    }
                }

                //Batch Number
                if ((drBatchRow.Table.Columns.Contains(DITGenericCommon.BATCH_NUMBER)) && (!DITGenericCommon.IsAllDigits(drBatchRow[DITGenericCommon.BATCH_NUMBER].ToString())))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The value for Batch Number:{0} is invalid, it should be numeric.", drBatchRow[DITGenericCommon.BATCH_NUMBER].ToString()), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }

                //PaymentType
                if (!drBatchRow.Table.Columns.Contains(DITGenericCommon.BATCH_PAYMENT_TYPE))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in batch element.", DITGenericCommon.BATCH_PAYMENT_TYPE), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }

                //BatchSource
                if (!drBatchRow.Table.Columns.Contains(DITGenericCommon.BATCH_BATCH_SRC))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in batch element.", DITGenericCommon.BATCH_BATCH_SRC), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }                
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in DoValidate : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }


        /// <summary>
        /// Build Batch Elements
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement GetBatchElement(XmlDocument xDoc)
        {
            int iTransactionSequence = 0;            
            int iBatchSequenceNo = 0;            
            XmlElement xBatchElement;            
            try
            {
                xBatchElement = base.BuildXMLNode(xDoc);
                foreach(DITGenericTransaction objTransaction in this.TransactionCollection)
                {
                    objTransaction.TransactionSequence = ++iTransactionSequence;
                    xBatchElement.AppendChild(objTransaction.GetTransactionElement(xDoc, ref iBatchSequenceNo));
                }
                
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetBatchElement : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xBatchElement = null;                
            }
            return xBatchElement;
        }
        #endregion Class Methods
    }



/// <summary>
/// Batch Collection class
/// </summary>
internal class DITGenericBatchCollection : CollectionBase
{
 
    /// <summary>
    /// Gets or sets the element at the specified index.
    /// </summary>
    /// <param name="index">The zero-based index of the element to be get or set.</param>
    /// <returns>The element at the specified index.</returns>
    public DITGenericBatchCollection this[int index]
    {
        get{return ((DITGenericBatchCollection)List[index]);}
        set{ List[index] = value;}
    }

    /// <summary>
    /// Adds an object to the end of the collection.
    /// </summary>
    /// <param name="value">The object to be added to the end of the collection.</param>
    /// <returns>Collection index at which the value has been added.</returns>
    public int Add(DITGenericBatch value)
    {
        return (List.Add(value));
    }

    /// <summary>
    /// Searches for the specified object and returns zero-based index 
    /// of the first occurrence within the entire collection.
    /// </summary>
    /// <param name="value">The object to locate in the collection. </param>
    /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
    public int IndexOf(DITGenericBatch value)
    {
        return (List.IndexOf(value));
    }

    /// <summary>
    /// Inserts an element into the collection at the specified index.
    /// </summary>
    /// <param name="index">Zero-based index at which value should be inserted.</param>
    /// <param name="value">The object to insert.</param>
    public void Insert(int index, DITGenericBatch value)
    {
        List.Insert(index, value);
    }

    /// <summary>
    /// Removes the first occurrence of a specific object from the collection.
    /// </summary>
    /// <param name="value">The object to remove from the collection.</param>
    public void Remove(DITGenericBatch value)
    {
        List.Remove(value);
    }

    /// <summary>
    /// Determines whether the collection contains a specific element.
    /// </summary>
    /// <param name="value">The object to locate in the collection.</param>
    /// <returns>If the collection contains the specified value.</returns>
    public bool Contains(DITGenericBatch value)
    {   
        return (List.Contains(value));
    }


    /// <summary>
    /// Performs additional custom processes before inserting a new element into the collection instance.
    /// </summary>
    /// <param name="index">Zero-based index at which to insert value.</param>
    /// <param name="value">New value of the element at index.</param>
    protected override void OnInsert(int index, object value)
    {
        base.OnInsert(index, value);
    }
    /// <summary>
    /// Performs additional custom processes when removing an element from the collection instance.
    /// </summary>
    /// <param name="index">Zero-based index at which value can be found.</param>
    /// <param name="value">The value of the element to remove at index.</param>
    protected override void OnRemove(int index, object value)
    {
        base.OnRemove(index, value);
    }
    /// <summary>
    /// Performs additional custom processes before setting a value in the collection instance.
    /// </summary>
    /// <param name="index">Zero-based index at which oldValue can be found.</param>
    /// <param name="oldValue">Value to replace with newValue.</param>
    /// <param name="newValue">New value of the element at index.</param>
    protected override void OnSet(int index, object oldValue, object newValue)
    {
        base.OnSet(index, oldValue, newValue);
    }
    /// <summary>
    /// Performs additional custom processes when validating a value.
    /// </summary>
    /// <param name="value">The object to validate.</param>
    protected override void OnValidate(object value)
    {
        if (value.GetType() != Type.GetType("DMP.Generic.Components.DITGenericBatch"))
            throw new ArgumentException("value must be of type DITGenericBatch.", "value");
    }
}
}