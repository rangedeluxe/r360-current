﻿using System;
using System.Collections;
using System.Text;
using System.Security.Cryptography;
using System.Data;
using System.Globalization;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/10/2014
*
*
* Purpose: This class holds the constants and the common methods used by other classes.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
******************************************************************************/

namespace DMP.Generic.Components
{
    public static class DITGenericCommon
    {

        #region ObjectTypeEnum
        /// <summary>
        /// Object Type enumeration
        /// </summary>        
        public enum ObjectType
        {
            PAYMENT=1,
            DOCUMENT=2,
            GHOSTDOCUMENT=3
        }        
        #endregion ObjectTypeEnum
        
        #region BatchRootField
        /// <summary>
        /// Batch Root Fields
        /// </summary>
        public const string BATCHXSDVERSION = "2.01.00.00";
        public const string CLIENTPROCESSCODE = "";
        #endregion BatchRootField

        #region BatchTableFields
        /// <summary>
        /// Batch Elements required column names
        /// </summary>        
        public const string BATCH_DATE = "batchdate";
        public const string BATCH_ID = "batchid";
        public const string BATCH_PAYMENT_TYPE = "paymenttype";
        public const string BATCH_PAYMENT_SRC = "paymentsource";
        public const string BATCH_BATCH_SRC = "batchsource";        
        public const string BATCH_WORK_GRP_ID = "workgroupid";
        public const string BATCH_DEPOSIT_DATE = "depositdate";
        public const string BATCH_PROCESS_DATE = "processingdate";
        public const string UNIQUE_BATCH_ID = "batch_id";
        /// <summary>
        /// Batch Optional Field
        /// </summary>        
        public const string BATCH_NUMBER = "batchnumber";
        #endregion BatchTableFields        

        #region TransactionTableFields
        /// <summary>
        /// Transaction Elements required column names
        /// </summary>        
        public const string TRANS_ID= "transactionid";
        public const string UNIQUE_TRANS_ID = "transaction_id";
        #endregion TransactionTableFields

        #region PaymentTableFields
        public const string UNIQUE_PAYMENT_ID = "payment_id";        
        /// <summary>
        /// Payment Mandatory Columns
        /// </summary>
        public const string PAYMENT_AMOUNT = "amount";
        public const string PAYMENT_ABA = "aba";
        public const string PAYMENT_DDA = "dda";
        /// <summary>
        /// Payment Optional Field
        /// </summary>        
        public const string PAYMENT_SERIAL_NUMBER = "serial";
        public const string PAYMENT_REMITTER_NAME = "payername";
        public const string PAYMENT_RT = "rt";
        public const string PAYMENT_ACCOUNT = "accountnumber";
        public const string PAYMENT_TRANS_CD = "transactioncode";
        #endregion PaymentTableFields        

        #region DocumentTableFields
        /// <summary>
        /// Document Table fields
        /// </summary>                
        public const string UNIQUE_DOCUMENT_ID = "document_id";
        #endregion DocumentTableFields

        #region GhostDocumentTableFields
        /// <summary>
        /// Document Table fields
        /// </summary>                
        public const string UNIQUE_GHOST_DOCUMENT_ID = "ghostdocument_id";
        #endregion GhostDocumentTableFields

        #region ImagesTableFields
        /// <summary>
        /// Images Table fields
        /// </summary>                
        public const string UNIQUE_IMAGES_ID = "images_id";
        #endregion ImagesTableFields

        #region ImgTableFields
        /// <summary>
        /// Images Table fields
        /// </summary>                        
        public const string IMG_CLIENT_FILE_PATH = "imagefilepath";
        public const string IMG_COLOR_MODE = "colormode";
        public const string IMG_PAGE = "FrontBack";
        #endregion ImgTableFields

        #region RemittanceDataRecordTable
        /// <summary>
        /// Remittance Date Record Table fields
        /// </summary>                
        public const string UNIQUE_REMITTANCE_DATA_REC_ID = "remittancedatarecord_id";
        #endregion RemittanceDataRecordTable

        #region RemittanceDataTable
        /// <summary>
        /// Remittance Data Table fields
        /// </summary>                
        public const string REMITTANCE_DATA_FIELD_NAME = "fieldname";
        public const string REMITTANCE_DATA_FIELD_VALUE = "fieldvalue";
        #endregion RemittanceDataTable

        #region TableNamesInBatches
        /// <summary>
        /// Table names in batch
        /// </summary>
        public const string TBL_BAT = "batch";
        public const string TBL_BAT_TRANS = "transaction";
        public const string TBL_BAT_TRANS_PMT = "payment";
        public const string TBL_BAT_TRANS_DOC = "document";
        public const string TBL_BAT_TRANS_GHOST_DOC = "ghostdocument";
        public const string TBL_IMAGES = "images";
        public const string TBL_IMAGES_IMG = "img";
        public const string TBL_REM_DATA_REC = "remittancedatarecord";        
        public const string TBL_REM_DATA_REC_REM_DATA = "remittancedata";
        #endregion TableNamesInBatches

        #region ElementNames
        /// <summary>
        /// Element Names
        /// </summary>
        public const string BATCH = "Batch";
        #endregion ElementNames

        #region ErrorMessage
        public const string ERR_REMITT_DATA_ELEMENT_MSG = "The input file contains Remittance Data Record Element, but the corresponding Remittance Data element is missing.";
        public const string ERR_IMG_ELEMENT_MSG = "The input file contains Images Element, but the corresponding Img element is missing.";
        public const string ERR_INVALID_FILE_MSG = "The input file is not valid.";
        #endregion ErrorMessage
        public const int DEFAULT_UNIQUE_ID = -99;
        #region COMMON_FUNCTIONS
        /// <summary>
        /// Get hash
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static string GetHash(string sValue)
        {
            string sResult = string.Empty;
            var data = Encoding.ASCII.GetBytes(sValue);
            var hashData = new SHA1Managed().ComputeHash(data);
            foreach (var b in hashData)
                sResult += b.ToString("X2");
            return sResult;
        }

        /// <summary>
        /// Check if the value is numeric
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsAllDigits(string value)
        {
            bool retvalue = true;
            foreach (Char c in value.ToCharArray())
            {
                retvalue = retvalue && Char.IsDigit(c);
            }
            return retvalue;
        }

        public static bool IsInteger(string value)
        {
            int parsedValue;

            return int.TryParse(value, NumberStyles.Integer, CultureInfo.InvariantCulture, out parsedValue)
                ? true
                : false;
        }

        /// <summary>
        /// Validates and returns date if its valid
        /// </summary>
        /// <param name="sDate"></param>
        /// <returns></returns>
        public static string ValidateDate(string sDate)
        {
            string sResult=string.Empty;
            try
            {
                sResult = Convert.ToDateTime(sDate).ToString();
            }
            catch
            {
                sResult = string.Empty;
            }
            return sResult;
        }
        /// <summary>
        /// Validate if its currency
        /// </summary>
        /// <param name="strMoney"></param>
        /// <returns></returns>
        public static bool ValidateMoney(string strMoney)
        {
            try
            {
                decimal dMoney = 0.00m;
                if (strMoney.Length > 0)
                {
                    dMoney = decimal.Parse(strMoney, System.Globalization.NumberStyles.Currency);
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion COMMON_FUNCTIONS
    }
}
