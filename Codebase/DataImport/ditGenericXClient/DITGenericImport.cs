﻿using System;
using System.IO;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Data;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
* Purpose: This is the main class which will generate the canonical XML
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
******************************************************************************/

namespace DMP.Generic.Components
{
    /// <summary>
    /// FileProcessor exposes core functions of DIT ACH File Import.
    /// </summary>    
    public class DITGenericImport : cDataImportXClientBase
    {
        #region Class Methods
        /// Main processing routine.
        public override bool StreamToXML(StreamReader stmInputFile, enmFileType fltFileType, cConfigData cfgSettings, out System.Xml.XmlDocument xmdResult)
        {
            bool bResult = true;
            try
            {
                DITGenericRoot objRoot;
                objRoot = ValidateFile(stmInputFile);
                if (!(objRoot == null))
                    xmdResult = objRoot.GetXML();
                else
                    xmdResult = null;
            }
            catch (Exception ex)
            {
                DoLogEvent("Exception in StreamToXML : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bResult = false;
                xmdResult = null;
            }
            return bResult;
        }

        /// <summary>
        /// Validate input stream
        /// </summary>
        /// <param name="stmInputStream"></param>
        /// <returns></returns>
        private DITGenericRoot ValidateFile(StreamReader stmInputStream)
        {
            DITGenericRoot objRoot = null;

            bool bIsValid = true;
            DITGenericBatchCollection objBatchCollection = new DITGenericBatchCollection();
            DITGenericTransactionCollection objTransactionCollection = new DITGenericTransactionCollection();
            DITGenericPaymentCollection objPaymentCollection = new DITGenericPaymentCollection();
            DITGenericDocumentCollection objDocumentCollection = new DITGenericDocumentCollection();
            DITGenericGhostDocumentCollection objGhostDocumentCollection = new DITGenericGhostDocumentCollection();
            DITGenericImagesCollection objImagesCollection = new DITGenericImagesCollection();
            DITGenericImgCollection objImgCollection = new DITGenericImgCollection();
            DITGenericRemittanceDataRecordCollection objRemittanceDataRecordCollection = new DITGenericRemittanceDataRecordCollection();
            DITGenericRemittanceDataCollection objRemittanceDataCollection = new DITGenericRemittanceDataCollection();
            try
            {
                DoLogEvent("Reading file...", "ValidateFile()", LogEventType.Information);
                DataSet ds = new DataSet();
                ds.ReadXml(stmInputStream);
                if (ds.Tables.Count > 0)
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        switch (dt.TableName.ToLower())
                        {
                            case DITGenericCommon.TBL_BAT:
                                foreach (DataRow drBatch in dt.Rows)
                                {
                                    DITGenericBatch objBatch = new DITGenericBatch(this);
                                    if (objBatch.DoValidate(drBatch) && objBatch.SetData(drBatch))
                                    {
                                        // Check to see if we want to add it or edit it.
                                        var index = objBatchCollection.Cast<DITGenericBatch>()
                                            .ToList()
                                            .FindIndex(x => x.BatchID == objBatch.BatchID);
                                        if (index > -1)
                                            objBatchCollection.RemoveAt(index);
                                        objBatchCollection.Add(objBatch);

                                    }
                                    else
                                        bIsValid = false;
                                }
                                break;
                            case DITGenericCommon.TBL_BAT_TRANS:
                                foreach (DataRow drTrans in dt.Rows)
                                {
                                    DITGenericTransaction objTransaction = new DITGenericTransaction(this);
                                    if (objTransaction.DoValidate(drTrans) && objTransaction.SetData(drTrans))
                                        objTransactionCollection.Add(objTransaction);
                                    else
                                        bIsValid = false;

                                }
                                break;
                            case DITGenericCommon.TBL_BAT_TRANS_PMT:
                                foreach (DataRow drPayment in dt.Rows)
                                {
                                    DITGenericPayment objPayment = new DITGenericPayment(this);
                                    if (objPayment.DoValidate(drPayment) && objPayment.SetData(drPayment))
                                        objPaymentCollection.Add(objPayment);
                                    else
                                        bIsValid = false;
                                }
                                break;
                            case DITGenericCommon.TBL_BAT_TRANS_DOC:

                                foreach (DataRow drDoc in dt.Rows)
                                {
                                    DITGenericDocument objDocument = new DITGenericDocument(this);
                                    if (objDocument.SetData(drDoc))
                                        objDocumentCollection.Add(objDocument);
                                    else
                                        bIsValid = false;
                                }
                                break;
                            case DITGenericCommon.TBL_BAT_TRANS_GHOST_DOC:
                                foreach (DataRow drGhostDoc in dt.Rows)
                                {
                                    DITGenericGhostDocument objGhostDocument = new DITGenericGhostDocument(this);
                                    if (objGhostDocument.SetData(drGhostDoc))
                                        objGhostDocumentCollection.Add(objGhostDocument);
                                    else
                                        bIsValid = false;
                                }
                                break;
                            case DITGenericCommon.TBL_IMAGES:
                                foreach (DataRow drImages in dt.Rows)
                                {
                                    DITGenericImages objImages = new DITGenericImages(this);
                                    if (objImages.SetData(drImages))
                                        objImagesCollection.Add(objImages);
                                    else
                                        bIsValid = false;
                                }
                                break;

                            case DITGenericCommon.TBL_IMAGES_IMG:
                                foreach (DataRow drImg in dt.Rows)
                                {
                                    DITGenericImg objImg = new DITGenericImg(this);
                                    if (objImg.DoValidate(drImg) && objImg.SetData(drImg))
                                        objImgCollection.Add(objImg);
                                    else
                                        bIsValid = false;
                                }
                                break;
                            case DITGenericCommon.TBL_REM_DATA_REC:
                                foreach (DataRow drRemDataRec in dt.Rows)
                                {
                                    DITGenericRemittanceDataRecord objRemittanceDataRecord = new DITGenericRemittanceDataRecord(this);
                                    if (objRemittanceDataRecord.SetData(drRemDataRec))
                                        objRemittanceDataRecordCollection.Add(objRemittanceDataRecord);
                                    else
                                        bIsValid = false;
                                }
                                break;
                            case DITGenericCommon.TBL_REM_DATA_REC_REM_DATA:
                                foreach (DataRow drRemData in dt.Rows)
                                {
                                    DITGenericRemittanceData objRemittanceData = new DITGenericRemittanceData(this);
                                    if (objRemittanceData.DoValidate(drRemData) && objRemittanceData.SetData(drRemData))
                                        objRemittanceDataCollection.Add(objRemittanceData);
                                    else
                                        bIsValid = false;
                                }
                                break;
                        }
                    }
                    if (bIsValid)
                    {
                        objRoot = this.FillObjects(objBatchCollection,
                                                  objTransactionCollection,
                                                  objPaymentCollection,
                                                  objDocumentCollection,
                                                  objGhostDocumentCollection,
                                                  objImagesCollection,
                                                  objImgCollection,
                                                  objRemittanceDataRecordCollection,
                                                  objRemittanceDataCollection);
                    }

                }
                else
                {
                    DoLogEvent(string.Format("Error in ValidateFile : {0}", DITGenericCommon.ERR_INVALID_FILE_MSG), this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                    bIsValid = false;
                }

            }
            catch (Exception ex)
            {
                DoLogEvent("Exception in ValidateFile : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                objRoot = null;
            }

            return objRoot;
        }



        /// <summary>
        /// Fill out all the required collection objects
        /// </summary>
        /// <param name="objBatchCollection"></param>
        /// <param name="objTransactionCollection"></param>
        /// <param name="objPaymentCollection"></param>
        /// <param name="objDocumentCollection"></param>
        /// <param name="objGhostDocumentCollection"></param>
        /// <param name="objImagesCollection"></param>
        /// <param name="objImgCollection"></param>
        /// <param name="objRemittanceDataRecordCollection"></param>
        /// <param name="objRemittanceDataCollection"></param>
        /// <returns></returns>
        private DITGenericRoot FillObjects(DITGenericBatchCollection objBatchCollection,
                                  DITGenericTransactionCollection objTransactionCollection,
                                  DITGenericPaymentCollection objPaymentCollection,
                                  DITGenericDocumentCollection objDocumentCollection,
                                  DITGenericGhostDocumentCollection objGhostDocumentCollection,
                                  DITGenericImagesCollection objImagesCollection,
                                  DITGenericImgCollection objImgCollection,
                                  DITGenericRemittanceDataRecordCollection objRemittanceDataRecordCollection,
                                  DITGenericRemittanceDataCollection objRemittanceDataCollection
                                )
        {

            bool bIsValid = true;
            DITGenericRoot objRoot = new DITGenericRoot(this);
            try
            {
                //Set Objects
                foreach (DITGenericBatch objBatch in objBatchCollection)
                {
                    foreach (DITGenericTransaction objTransaction in objTransactionCollection.Filter(objBatch.UniqueBatchID))
                    {

                        //Payment Object under Transaction 
                        foreach (DITGenericPayment objPayment in objPaymentCollection.Filter(objTransaction.UniqueTransactionID))
                        {
                            //Add Images Element
                            foreach (DITGenericImages objImages in objImagesCollection.FilterImages(objPayment.UniquePaymentID, DITGenericCommon.ObjectType.PAYMENT))
                            {
                                if (SetImagesObject(objImages, objImgCollection))
                                    objPayment.ImagesCollection.Add(objImages);
                                else
                                {
                                    DoLogEvent(string.Format("Error in FillObjects : {0}", DITGenericCommon.ERR_IMG_ELEMENT_MSG), this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                                    bIsValid = false;
                                }
                            }
                            //Add Remittance Element
                            foreach (DITGenericRemittanceDataRecord objRemittanceDataRecord in objRemittanceDataRecordCollection.FilterRemittanceDataRecord(objPayment.UniquePaymentID, DITGenericCommon.ObjectType.PAYMENT))
                            {
                                if (SetRemittanceDataRecordObject(objRemittanceDataRecord, objRemittanceDataCollection))
                                    objPayment.RemittanceDataRecordCollection.Add(objRemittanceDataRecord);
                                else
                                {
                                    DoLogEvent(string.Format("Error in FillObjects : {0}", DITGenericCommon.ERR_REMITT_DATA_ELEMENT_MSG), this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                                    bIsValid = false;
                                }
                            }
                            objTransaction.PaymentCollection.Add(objPayment);
                        }

                        //Document Object under Transaction 
                        foreach (DITGenericDocument objDocument in objDocumentCollection.Filter(objTransaction.UniqueTransactionID))
                        {
                            //Add Images Element
                            foreach (DITGenericImages objImages in objImagesCollection.FilterImages(objDocument.UniqueDocumentID, DITGenericCommon.ObjectType.DOCUMENT))
                            {
                                if (SetImagesObject(objImages, objImgCollection))
                                    objDocument.ImagesCollection.Add(objImages);
                                else
                                {
                                    DoLogEvent(string.Format("Error in FillObjects : {0}", DITGenericCommon.ERR_IMG_ELEMENT_MSG), this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                                    bIsValid = false;
                                }
                            }
                            //Add Remittance Element
                            foreach (DITGenericRemittanceDataRecord objRemittanceDataRecord in objRemittanceDataRecordCollection.FilterRemittanceDataRecord(objDocument.UniqueDocumentID, DITGenericCommon.ObjectType.DOCUMENT))
                            {
                                if (SetRemittanceDataRecordObject(objRemittanceDataRecord, objRemittanceDataCollection))
                                    objDocument.RemittanceDataRecordCollection.Add(objRemittanceDataRecord);
                                else
                                {
                                    DoLogEvent(string.Format("Error in FillObjects : {0}", DITGenericCommon.ERR_REMITT_DATA_ELEMENT_MSG), this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                                    bIsValid = false;
                                }
                            }
                            objTransaction.DocumentCollection.Add(objDocument);
                        }

                        //Ghost Document Object under Transaction 
                        foreach (DITGenericGhostDocument objGhostDocument in objGhostDocumentCollection.Filter(objTransaction.UniqueTransactionID))
                        {
                            //Add Remittance Element
                            foreach (DITGenericRemittanceDataRecord objRemittanceDataRecord in objRemittanceDataRecordCollection.FilterRemittanceDataRecord(objGhostDocument.UniqueGhostDocumentID, DITGenericCommon.ObjectType.GHOSTDOCUMENT))
                            {
                                if (SetRemittanceDataRecordObject(objRemittanceDataRecord, objRemittanceDataCollection))
                                    objGhostDocument.RemittanceDataRecordCollection.Add(objRemittanceDataRecord);
                                else
                                {
                                    DoLogEvent(string.Format("Error in FillObjects : {0}", DITGenericCommon.ERR_REMITT_DATA_ELEMENT_MSG), this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                                    bIsValid = false;
                                }

                            }
                            objTransaction.GhostDocumentCollection.Add(objGhostDocument);
                        }
                        objBatch.TransactionCollection.Add(objTransaction);
                    }
                    objRoot.BatchCollection.Add(objBatch);
                }
                if (!bIsValid)
                    objRoot = null;
            }
            catch (Exception ex)
            {
                DoLogEvent("Exception in FillObjects : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                objRoot = null;
            }
            return objRoot;
        }

        /// <summary>
        /// Set Images object
        /// </summary>
        /// <param name="objImages"></param>
        /// <param name="objImgCollection"></param>
        /// <returns></returns>
        private bool SetImagesObject(DITGenericImages objImages, DITGenericImgCollection objImgCollection)
        {
            bool bImgExists = false;
            foreach (DITGenericImg objImg in objImgCollection.Filter(objImages.UniqueImagesID))
            {
                objImages.ImgCollection.Add(objImg);
                bImgExists = true;
            }
            return bImgExists;
        }

        /// <summary>
        /// Set RemittanceDataRecord object
        /// </summary>
        /// <param name="objRemittanceDataRecord"></param>
        /// <param name="objRemittanceDataCollection"></param>
        /// <returns></returns>
        private bool SetRemittanceDataRecordObject(DITGenericRemittanceDataRecord objRemittanceDataRecord, DITGenericRemittanceDataCollection objRemittanceDataCollection)
        {
            bool bRemittanceDataExists = false;

            foreach (DITGenericRemittanceData objRemittanceData in objRemittanceDataCollection.Filter(objRemittanceDataRecord.UniqueRemittDataRecID))
            {
                objRemittanceDataRecord.RemittanceDataCollection.Add(objRemittanceData);
                bRemittanceDataExists = true;
            }
            return bRemittanceDataExists;
        }
        #endregion Class Methods
    }
}
