﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Xml;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the transaction data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
* WI 173024 SAS 10/22/2014 
* Changes done to set proper batch sequence numbers 
******************************************************************************/
namespace DMP.Generic.Components
{
    public class DITGenericTransaction : Transaction
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        private int m_UniqueTransactionID;
        private int m_UniqueBatchID;
        /// <summary>
        /// Payment Collection
        /// </summary>
        private DITGenericPaymentCollection m_PaymentCollection = null;
        /// <summary>
        /// Document Collection
        /// </summary>
        private DITGenericDocumentCollection m_DocumentCollection = null;
        /// <summary>
        /// Ghost Document Collection
        /// </summary>
        private DITGenericGhostDocumentCollection m_GhostDocumentCollection = null;

        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>        
        /// <param name="oDataImportXClient"></param>
        public DITGenericTransaction(cDataImportXClientBase oDataImportXClient)
        {
            this.m_DataImportXClient = oDataImportXClient;
            m_PaymentCollection = new DITGenericPaymentCollection();
            m_DocumentCollection = new DITGenericDocumentCollection();
            m_GhostDocumentCollection = new DITGenericGhostDocumentCollection();
        }
        #endregion Class constructors

        #region Class Properties
        /// <summary>
        /// Unique Identifier for Transaction
        /// </summary>
        public int UniqueTransactionID
        {
            get { return m_UniqueTransactionID; }
            set { m_UniqueTransactionID = value; }
        }
        /// <summary>
        /// Unique Identifier for Batch
        /// </summary>
        public int UniqueBatchID
        {
            get { return m_UniqueBatchID; }
            set { m_UniqueBatchID = value; }
        }
        /// <summary>
        /// m_PaymentCollection collection
        /// </summary>
        public DITGenericPaymentCollection PaymentCollection
        {
            get { return m_PaymentCollection; }
        }
        /// <summary>
        /// m_DocumentCollection collection
        /// </summary>
        public DITGenericDocumentCollection DocumentCollection
        {
            get { return m_DocumentCollection; }
        }
        /// <summary>
        /// m_GhostDocumentCollection collection
        /// </summary>
        public DITGenericGhostDocumentCollection GhostDocumentCollection
        {
            get { return m_GhostDocumentCollection; }
        }
        #endregion Class Properties


        #region Class Methods
        /// <summary>
        ///    Set Transaction Data
        /// </summary>
        /// <param name="drTransactionRow"></param>
        /// <returns></returns>
        public bool SetData(DataRow drTransactionRow)
        {
            bool bRetValue = false;
            try
            {
                //Record Identifier Transaction_ID
                this.UniqueTransactionID = Convert.ToInt32(drTransactionRow[DITGenericCommon.UNIQUE_TRANS_ID].ToString());
                //Record Identifier Batch_ID
                this.UniqueBatchID = Convert.ToInt32(drTransactionRow[DITGenericCommon.UNIQUE_BATCH_ID].ToString());
                base.TransactionID = Convert.ToInt32(drTransactionRow[DITGenericCommon.TRANS_ID].ToString());
                bRetValue = true;
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in SetData : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        /// Validate Transaction Data
        /// </summary>
        /// <param name="drBatchRow"></param>
        public bool DoValidate(DataRow drTransactionRow)
        {
            bool bRetValue = true;
            try
            {
                //TransactionID      
                if (!drTransactionRow.Table.Columns.Contains(DITGenericCommon.TRANS_ID))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in transaction element.", DITGenericCommon.TRANS_ID), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }
                else
                {
                    if (!DITGenericCommon.IsAllDigits(drTransactionRow[DITGenericCommon.TRANS_ID].ToString()))
                    {
                        m_DataImportXClient.DoLogEvent(string.Format("Error: Transaction ID :{0} is not numeric in transaction element.", drTransactionRow[DITGenericCommon.TRANS_ID]), this.GetType().Name, LogEventType.Error);
                        bRetValue = false;
                    }
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in DoValidate : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        /// Build Transaction Elements
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="iBatchSequenceNo"></param>
        /// <returns></returns>
        public XmlElement GetTransactionElement(XmlDocument xDoc, ref int iBatchSequenceNo)
        {
            XmlElement xTransactionElement;
            int iCheckSequence = 0;
            int iSequenceWithinTransaction = 0;
            int iDocumentSequence = 0;
            try
            {

                xTransactionElement = base.BuildXMLNode(xDoc);
                //Payment element under Transaction
                foreach (DITGenericPayment objPayment in this.PaymentCollection)
                {
                    objPayment.BatchSequence = ++iBatchSequenceNo; //Sequential no of payment within a batch
                    objPayment.CheckSequence = ++iCheckSequence;
                    xTransactionElement.AppendChild(objPayment.GetPaymentElement(xDoc, ref iBatchSequenceNo));
                }
                //Document element under Transaction
                foreach (DITGenericDocument objDocument in this.DocumentCollection)
                {
                    objDocument.BatchSequence = ++iBatchSequenceNo; //Sequential no of document within a batch
                    objDocument.DocumentSequence =  (++iDocumentSequence).ToString(); //Sequential no of document within a batch
                    objDocument.SequenceWithinTransaction = (++iSequenceWithinTransaction).ToString(); //Sequential no of document within the Transaction
                    xTransactionElement.AppendChild(objDocument.GetDocumentElement(xDoc, ref iBatchSequenceNo));
                }
                //Ghost Document element under Transaction
                foreach (DITGenericGhostDocument objGhostDocument in this.GhostDocumentCollection)
                {
                    xTransactionElement.AppendChild(objGhostDocument.GetGhostDocumentElement(xDoc, ref iBatchSequenceNo));
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetTransactionElement : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xTransactionElement = null;

            }
            return xTransactionElement;
        }
        #endregion Class Methods
    }



    /// <summary>
    /// Transaction Collection class
    /// </summary>
    //internal class DITGenericTransactionCollection : CollectionBase
    public class DITGenericTransactionCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITGenericTransactionCollection this[int index]
        {
            get { return ((DITGenericTransactionCollection)List[index]); }
            set { List[index] = value; }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITGenericTransaction value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITGenericTransaction value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITGenericTransaction value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITGenericTransaction value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITGenericTransaction value)
        {
            return (List.Contains(value));
        }


        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.Generic.Components.DITGenericTransaction"))
                throw new ArgumentException("value must be of type DITGenericTransaction.", "value");
        }

        /// <summary>
        /// Filter Transaction based on Batch
        /// </summary>
        /// <param name="iUniqueBatch_ID"></param>
        public DITGenericTransactionCollection Filter(int UniqueBatchID)
        {
            DITGenericTransactionCollection objTransactionCollection = new DITGenericTransactionCollection();
            var FilteredTransaction = from objTransaction in this.OfType<DITGenericTransaction>()
                                      where objTransaction.UniqueBatchID == UniqueBatchID
                                      select objTransaction;
            foreach (DITGenericTransaction objTransaction in FilteredTransaction)
            {
                objTransactionCollection.Add(objTransaction);
            }

            return objTransactionCollection;
        }
    }
}