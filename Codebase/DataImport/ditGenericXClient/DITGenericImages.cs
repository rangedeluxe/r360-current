﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Xml;
using System.Linq;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the Images data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
******************************************************************************/
namespace DMP.Generic.Components
{
    public class DITGenericImages : Images
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        private int m_UniqueImagesID;
        private int m_UniquePaymentID;
        private int m_UniqueDocumentID;
        /// <summary>
        /// Img Collection
        /// </summary>
        private DITGenericImgCollection m_ImgCollection = null;

        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>        
        /// <param name="oDataImportXClient"></param>
        public DITGenericImages(cDataImportXClientBase oDataImportXClient)
        {
            this.m_DataImportXClient = oDataImportXClient;
            m_ImgCollection = new DITGenericImgCollection();
        }
        #endregion Class constructors

        #region Class Properties

        /// <summary>
        /// Unique Identifier for Images
        /// </summary>
        public int UniqueImagesID
        {
            get { return m_UniqueImagesID; }
            set { m_UniqueImagesID = value; }
        }

        /// <summary>
        /// Unique Identifier for PaymentID
        /// </summary>
        public int UniquePaymentID
        {
            get { return m_UniquePaymentID; }
            set { m_UniquePaymentID = value; }
        }
        /// <summary>
        /// Unique Identifier for Document ID 
        /// </summary>
        public int UniqueDocumentID
        {
            get { return m_UniqueDocumentID; }
            set { m_UniqueDocumentID = value; }
        }
        /// <summary>
        /// m_ImgCollection collection
        /// </summary>
        public DITGenericImgCollection ImgCollection
        {
            get { return m_ImgCollection; }
        }

        #endregion Class Properties


        #region Class Methods
        /// <summary>
        /// Set Images Data
        /// </summary>        
        public bool SetData(DataRow drImagesRow)
        {
            bool bRetValue = false;
            try
            {
                //Record Identifier Images ID
                this.UniqueImagesID = Convert.ToInt32(drImagesRow[DITGenericCommon.UNIQUE_IMAGES_ID].ToString());
                //Record Identifier Payment_ID                
                if (drImagesRow.Table.Columns.Contains(DITGenericCommon.UNIQUE_PAYMENT_ID) && (!string.IsNullOrEmpty(drImagesRow[DITGenericCommon.UNIQUE_PAYMENT_ID].ToString())))
                    this.UniquePaymentID = Convert.ToInt32(drImagesRow[DITGenericCommon.UNIQUE_PAYMENT_ID].ToString());
                else
                    this.m_UniquePaymentID = DITGenericCommon.DEFAULT_UNIQUE_ID;                
                //Record Identifier Document_ID
                if (drImagesRow.Table.Columns.Contains(DITGenericCommon.UNIQUE_DOCUMENT_ID) && (!string.IsNullOrEmpty(drImagesRow[DITGenericCommon.UNIQUE_DOCUMENT_ID].ToString())))                
                    this.UniqueDocumentID = Convert.ToInt32(drImagesRow[DITGenericCommon.UNIQUE_DOCUMENT_ID].ToString());
                else
                    this.UniqueDocumentID = DITGenericCommon.DEFAULT_UNIQUE_ID;                
                bRetValue = true;
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in SetData : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        /// Build Images Elements
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement GetImagesElement(XmlDocument xDoc)
        {
            XmlElement xImagesElement;
            try
            {
                xImagesElement = base.BuildXMLNode(xDoc);
                foreach (DITGenericImg objImg in this.ImgCollection)
                {
                    xImagesElement.AppendChild(objImg.GetImgElement(xDoc));
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetImagesElement : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xImagesElement = null;
            }
            return xImagesElement;
        }
        #endregion Class Methods
    }

    /// <summary>
    /// Images Collection class
    /// </summary>
    //internal  class DITGenericImagesCollection : CollectionBase
    public class DITGenericImagesCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITGenericImagesCollection this[int index]
        {
            get { return ((DITGenericImagesCollection)List[index]); }
            set { List[index] = value; }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITGenericImages value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITGenericImages value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITGenericImages value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITGenericImages value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITGenericImages value)
        {
            return (List.Contains(value));
        }


        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.Generic.Components.DITGenericImages"))
                throw new ArgumentException("value must be of type DITGenericImages.", "value");
        }

        /// <summary>
        /// Filter Images 
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <param name="enumObjectType"></param>
        /// <returns></returns>
        public DITGenericImagesCollection FilterImages(int UniqueID, DITGenericCommon.ObjectType enumObjectType)
        {
            DITGenericImagesCollection objImagesCollection = new DITGenericImagesCollection();
            if (enumObjectType== DITGenericCommon.ObjectType.PAYMENT)
            {
                var FilterImages=from objImages in this.OfType<DITGenericImages>()
                                                             where objImages.UniquePaymentID == UniqueID
                                                            select objImages;
                foreach (DITGenericImages objImages in FilterImages)
                {
                    objImagesCollection.Add(objImages);
                }
            }
            else
            {
                var FilterImages = from objImages in this.OfType<DITGenericImages>()
                                                       where objImages.UniqueDocumentID == UniqueID
                                                       select objImages;
                foreach (DITGenericImages objImages in FilterImages)
                {
                    objImagesCollection.Add(objImages);
                }
            }
            return objImagesCollection;
        }
    }
}