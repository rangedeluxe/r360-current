﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Xml;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the Img data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
******************************************************************************/
namespace DMP.Generic.Components
{
    public class DITGenericImg : Img
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;        
        private int m_UniqueImagesID;
        #endregion Class Fields
        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>        
        /// <param name="oDataImportXClient"></param>
        public DITGenericImg(cDataImportXClientBase oDataImportXClient)
        {
            this.m_DataImportXClient = oDataImportXClient;            
        }
        #endregion Class constructors

        #region Class Properties

        /// <summary>
        /// Unique Identifier for Images
        /// </summary>
        public int UniqueImagesID
        {
            get { return m_UniqueImagesID; }
            set { m_UniqueImagesID = value; }
        }

        #endregion Class Properties


        #region Class Methods
        /// <summary>
        /// Set Img Data
        /// </summary>        
        public bool SetData(DataRow drImgRow)
        {
            bool bRetValue = false;
            try
            {                
                //Record Identifier Images ID
                this.UniqueImagesID = Convert.ToInt32(drImgRow[DITGenericCommon.UNIQUE_IMAGES_ID].ToString());
                this.ColorMode = drImgRow[DITGenericCommon.IMG_COLOR_MODE].ToString();
                this.Page = drImgRow[DITGenericCommon.IMG_PAGE].ToString();
                this.ClientFilePath = drImgRow[DITGenericCommon.IMG_CLIENT_FILE_PATH].ToString();                
                bRetValue = true;
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in SetData : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }


        /// <summary>
        /// Validate Img Data
        /// </summary>
        /// <param name="drImgRow"></param>
        /// <returns></returns>
        public bool DoValidate(DataRow drImgRow)
        {
            bool bRetValue = true;
            try
            {
                //Client File Path
                if (!drImgRow.Table.Columns.Contains(DITGenericCommon.IMG_CLIENT_FILE_PATH))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in img element.", DITGenericCommon.IMG_CLIENT_FILE_PATH), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }

                //Color mode
                if (!drImgRow.Table.Columns.Contains(DITGenericCommon.IMG_COLOR_MODE))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in img element.", DITGenericCommon.IMG_COLOR_MODE), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }

                //page
                if (!drImgRow.Table.Columns.Contains(DITGenericCommon.IMG_PAGE))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in img element.", DITGenericCommon.IMG_PAGE), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in DoValidate : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        /// Build Img  Elements
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement GetImgElement(XmlDocument xDoc)
        {
            XmlElement xImgElement;
            try
            {
                xImgElement = base.BuildXMLNode(xDoc);
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetImgElement : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xImgElement = null;
            }
            return xImgElement;
        }

        #endregion Class Methods
    }

    /// <summary>
    /// Img Collection class
    /// </summary>
    public class DITGenericImgCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITGenericImgCollection this[int index]
        {
            get { return ((DITGenericImgCollection)List[index]); }
            set { List[index] = value; }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITGenericImg value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITGenericImg value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITGenericImg value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITGenericImg value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITGenericImg value)
        {
            return (List.Contains(value));
        }


        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.Generic.Components.DITGenericImg"))
                throw new ArgumentException("value must be of type DITGenericImg.", "value");
        }

        /// <summary>
        /// Filter Img based on Images
        /// </summary>
        /// <param name="UniquePaymentID"></param>
        /// <returns></returns>
        public DITGenericImgCollection Filter(int UniqueImagesID)
        {
            DITGenericImgCollection objImgCollection = new DITGenericImgCollection();
            var FilteredImg = from objImg in this.OfType<DITGenericImg>()
                                 where objImg.UniqueImagesID== UniqueImagesID
                                 select objImg;
            foreach(DITGenericImg objGenericImg in FilteredImg)
            {
                objImgCollection.Add(objGenericImg);
            }
            return objImgCollection;
        }
    }
}