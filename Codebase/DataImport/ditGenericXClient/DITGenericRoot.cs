﻿using System;
using System.Xml;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Text;
using System.Linq;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the Batches data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
* WI 177626 SAS 11/13/2014 
* Changes done to calculate proper file hash
******************************************************************************/
namespace DMP.Generic.Components
{
    class DITGenericRoot
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        /// <summary>
        /// Batch Collection
        /// </summary>
        private DITGenericBatchCollection m_BatchCollection = null;       
        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>
        public DITGenericRoot(cDataImportXClientBase oDataImportXClient)
        {
            this.m_DataImportXClient = oDataImportXClient;
            m_BatchCollection = new DITGenericBatchCollection();            
        }
        #endregion Class constructors

        #region Class Properties
        /// <summary>
        /// m_Batch collection
        /// </summary>
        public DITGenericBatchCollection BatchCollection
        {
            get { return m_BatchCollection; }
        }        
        #endregion Class Properties

        /// <summary>
        /// Generate the required Canonical XML
        /// </summary>
        /// <returns></returns>
        public XmlDocument GetXML()
        {
            XmlDocument xDoc = new XmlDocument();            
            try
            {
                StringBuilder sbFileSignature = new StringBuilder();
                XmlElement xRootElement = xDoc.CreateElement("Batches");
                xRootElement.SetAttribute("SourceTrackingID", Guid.NewGuid().ToString());
                xRootElement.SetAttribute("ClientProcessCode", DITGenericCommon.CLIENTPROCESSCODE);
                xRootElement.SetAttribute("XSDVersion", DITGenericCommon.BATCHXSDVERSION);                
                foreach (DITGenericBatch objBatch in this.BatchCollection)
                {                    
                    xRootElement.AppendChild(objBatch.GetBatchElement(xDoc));

                    // If we have multiple batches, the file signature gets too big.
                    // So we'll only use the first batch for this.
                    if (sbFileSignature.Length > 0)
                        continue; 

                    sbFileSignature.Append(objBatch.BatchDate.ToString("yyyy-MM-dd"));
                    sbFileSignature.Append(objBatch.BatchID);
                    sbFileSignature.Append(objBatch.PaymentType);
                    sbFileSignature.Append(objBatch.BatchSource);
                    sbFileSignature.Append(objBatch.ClientID);
                }
                xRootElement.SetAttribute("FileHash", DITGenericCommon.GetHash(sbFileSignature.ToString()));
                xRootElement.SetAttribute("FileSignature", sbFileSignature.ToString()); 
                xDoc.AppendChild(xRootElement);   
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetXML : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xDoc = null;
            }
            return xDoc;
        }    
    }
}
