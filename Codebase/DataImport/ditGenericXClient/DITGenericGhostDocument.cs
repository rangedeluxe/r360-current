﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Xml;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the Ghostdocument data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
* WI 173024 SAS 10/22/2014 
* Changes done to set proper batch sequence numbers 
******************************************************************************/
namespace DMP.Generic.Components
{
    public class DITGenericGhostDocument : GhostDocument
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        private int m_UniqueTransactionID;
        private int m_UniqueGhostDocumentID;
        /// <summary>
        /// Remittance Data Record Collection
        /// </summary>
        private DITGenericRemittanceDataRecordCollection m_RemittanceDataRecordCollection = null;
        #endregion Class Fields
        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>        
        /// <param name="oDataImportXClient"></param>
        public DITGenericGhostDocument(cDataImportXClientBase oDataImportXClient)
        {
            this.m_DataImportXClient = oDataImportXClient;
            m_RemittanceDataRecordCollection = new DITGenericRemittanceDataRecordCollection();
        }
        #endregion Class constructors

        #region Class Properties
        /// <summary>
        /// Unique Identifier for Transaction
        /// </summary>
        public int UniqueTransactionID
        {
            get { return m_UniqueTransactionID; }
            set { m_UniqueTransactionID = value; }
        }
        /// <summary>
        /// Unique Identifier for Ghost Document
        /// </summary>
        public int UniqueGhostDocumentID
        {
            get { return m_UniqueGhostDocumentID; }
            set { m_UniqueGhostDocumentID = value; }
        }
        /// <summary>
        /// m_RemittanceDataRecordCollection collection
        /// </summary>
        public DITGenericRemittanceDataRecordCollection RemittanceDataRecordCollection
        {
            get { return m_RemittanceDataRecordCollection; }
        }
        #endregion Class Properties
        
        #region Class Methods
        /// <summary>
        /// Set GhostDocument Data
        /// </summary>
        /// <param name="drGhostDocumentRow"></param>
        /// <returns></returns>
        public bool SetData(DataRow drGhostDocumentRow)
        {
            bool bRetValue = false;
            try
            {
                //Record Identifier Transaction_ID
                this.UniqueTransactionID = Convert.ToInt32(drGhostDocumentRow[DITGenericCommon.UNIQUE_TRANS_ID].ToString());
                //Record Identifier GhostDocument_ID
                if (drGhostDocumentRow.Table.Columns.Contains(DITGenericCommon.UNIQUE_GHOST_DOCUMENT_ID))
                    this.UniqueGhostDocumentID = Convert.ToInt32(drGhostDocumentRow[DITGenericCommon.UNIQUE_GHOST_DOCUMENT_ID].ToString());
                
                bRetValue = true;
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in SetData : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        /// Build Ghost Document Elements
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="iBatchSequenceNo"></param>
        /// <returns></returns>
        public XmlElement GetGhostDocumentElement(XmlDocument xDoc, ref  int iBatchSequenceNo)
        {
            XmlElement xGhostDocumentElement;
            try
            {
                xGhostDocumentElement = base.BuildXMLNode(xDoc);
                //Remittance Data Record Element Under Document
                foreach (DITGenericRemittanceDataRecord objRemittanceDataRecord in this.RemittanceDataRecordCollection)
                {
                    objRemittanceDataRecord.BatchSequence = ++iBatchSequenceNo;
                    xGhostDocumentElement.AppendChild(objRemittanceDataRecord.GetRemittanceDataRecordElement(xDoc));
                }                
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetGhostDocumentElement : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xGhostDocumentElement = null;

            }
            return xGhostDocumentElement;
        }
        #endregion Class Methods
    }
    
    /// <summary>
    /// GhostDocument Collection class
    /// </summary>
    public class DITGenericGhostDocumentCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITGenericGhostDocumentCollection this[int index]
        {
            get { return ((DITGenericGhostDocumentCollection)List[index]); }
            set { List[index] = value; }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITGenericGhostDocument value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITGenericGhostDocument value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITGenericGhostDocument value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITGenericGhostDocument value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITGenericGhostDocument value)
        {
            return (List.Contains(value));
        }


        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.Generic.Components.DITGenericGhostDocument"))
                throw new ArgumentException("value must be of type DITGenericGhostDocument.", "value");
        }

        /// <summary>
        ///  Filter Ghost Document based on Transaction
        /// </summary>
        /// <param name="UniqueTransactionID"></param>
        /// <returns></returns>
        public DITGenericGhostDocumentCollection Filter(int UniqueTransactionID)
        {
            DITGenericGhostDocumentCollection objGhostDocumentCollection = new DITGenericGhostDocumentCollection();
            var FilteredGhostDocument = from objGhostDocument in this.OfType<DITGenericGhostDocument>()
                                        where objGhostDocument.UniqueTransactionID == UniqueTransactionID
                                        select objGhostDocument;
            foreach(DITGenericGhostDocument objGhostDocument in FilteredGhostDocument)
            {
                objGhostDocumentCollection.Add(objGhostDocument);
            }
            return objGhostDocumentCollection;
        }
    }
}