﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Xml;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the payment data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
* WI 173024 SAS 10/22/2014 
* Changes done to set proper batch sequence numbers 
* WI 177626 SAS 11/13/2014 
* Changes done to remove $ sign from amount
******************************************************************************/
namespace DMP.Generic.Components
{
    public class DITGenericPayment : Payment
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        private int m_UniqueTransactionID;
        private int m_UniquePaymentID;
        private int m_CheckSequence;
        /// <summary>
        /// Remittance Data Record Collection
        /// </summary>
        private DITGenericRemittanceDataRecordCollection m_RemittanceDataRecordCollection = null;
        /// <summary>
        /// Images Collection
        /// </summary>
        private DITGenericImagesCollection m_ImagesCollection = null;

        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>        
        /// <param name="oDataImportXClient"></param>
        public DITGenericPayment(cDataImportXClientBase oDataImportXClient)
        {
            this.m_DataImportXClient = oDataImportXClient;
            m_ImagesCollection = new DITGenericImagesCollection();
            m_RemittanceDataRecordCollection = new DITGenericRemittanceDataRecordCollection();
        }
        #endregion Class constructors

        #region Class Properties

        /// <summary>
        /// Unique Identifier for Payment
        /// </summary>
        public int UniquePaymentID
        {
            get { return m_UniquePaymentID; }
            set { m_UniquePaymentID = value; }
        }

        /// <summary>
        /// Unique Identifier for Transaction
        /// </summary>
        public int UniqueTransactionID
        {
            get { return m_UniqueTransactionID; }
            set { m_UniqueTransactionID = value; }
        }

        /// <summary>
        /// Payment Check Sequence
        /// </summary>
        public int CheckSequence
        {
            get { return m_CheckSequence; }
            set { m_CheckSequence = value; }
        }
        /// <summary>
        /// m_ImagesCollection collection
        /// </summary>
        public DITGenericImagesCollection ImagesCollection
        {
            get { return m_ImagesCollection; }
        }
        /// <summary>
        /// m_RemittanceDataRecordCollection collection
        /// </summary>
        public DITGenericRemittanceDataRecordCollection RemittanceDataRecordCollection
        {
            get { return m_RemittanceDataRecordCollection; }
        }
        #endregion Class Properties


        #region Class Methods
        /// <summary>
        ///  Set Payment Data
        /// </summary>
        /// <param name="drPaymentRow"></param>
        /// <returns></returns>
        public bool SetData(DataRow drPaymentRow)
        {
            bool bRetValue = false;
            try
            {
                //Record Identifier Transaction_ID
                this.UniqueTransactionID = Convert.ToInt32(drPaymentRow[DITGenericCommon.UNIQUE_TRANS_ID].ToString());
                
                //Record Identifier Payment_ID
                if (drPaymentRow.Table.Columns.Contains(DITGenericCommon.UNIQUE_PAYMENT_ID))
                    this.UniquePaymentID = Convert.ToInt32(drPaymentRow[DITGenericCommon.UNIQUE_PAYMENT_ID].ToString());
                
                //Amount
                this.Amount = Convert.ToDecimal(drPaymentRow[DITGenericCommon.PAYMENT_AMOUNT].ToString()).ToString();
                //Serial                
                if (drPaymentRow.Table.Columns.Contains(DITGenericCommon.PAYMENT_SERIAL_NUMBER))
                {
                    this.Serial = drPaymentRow[DITGenericCommon.PAYMENT_SERIAL_NUMBER].ToString();
                }
                //Remitter Name
                if (drPaymentRow.Table.Columns.Contains(DITGenericCommon.PAYMENT_REMITTER_NAME))
                {
                    this.RemitterName = drPaymentRow[DITGenericCommon.PAYMENT_REMITTER_NAME].ToString();
                }
                //RT
                if (drPaymentRow.Table.Columns.Contains(DITGenericCommon.PAYMENT_RT))
                {
                    this.RT = drPaymentRow[DITGenericCommon.PAYMENT_RT].ToString();
                }
                //Account
                if (drPaymentRow.Table.Columns.Contains(DITGenericCommon.PAYMENT_ACCOUNT))
                {
                    this.Account = drPaymentRow[DITGenericCommon.PAYMENT_ACCOUNT].ToString();
                }
                //Transaction Code                
                if (drPaymentRow.Table.Columns.Contains(DITGenericCommon.PAYMENT_TRANS_CD))
                {
                    this.TransactionCode = drPaymentRow[DITGenericCommon.PAYMENT_TRANS_CD].ToString();
                }
                //ABA 
                this.ABA = drPaymentRow[DITGenericCommon.PAYMENT_ABA].ToString();
                //DDA
                this.DDA = drPaymentRow[DITGenericCommon.PAYMENT_DDA].ToString();
                bRetValue = true;
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in SetData : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        /// Validate Payment Data
        /// </summary>
        /// <param name="drPaymentRow"></param>
        /// <returns></returns>
        public bool DoValidate(DataRow drPaymentRow)
        {
            bool bRetValue = true;
            try
            {
                //Amount      
                if (!drPaymentRow.Table.Columns.Contains(DITGenericCommon.PAYMENT_AMOUNT))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in payment element.", DITGenericCommon.PAYMENT_AMOUNT), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }
                else
                {
                    if (!DITGenericCommon.ValidateMoney(drPaymentRow[DITGenericCommon.PAYMENT_AMOUNT].ToString()))
                    {
                        m_DataImportXClient.DoLogEvent(string.Format("Error: Amount :{0} is not valid amount in payment element.", drPaymentRow[DITGenericCommon.PAYMENT_AMOUNT]), this.GetType().Name, LogEventType.Error);
                        bRetValue = false;
                    }
                }

                //ABA
                if (!drPaymentRow.Table.Columns.Contains(DITGenericCommon.PAYMENT_ABA))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in payment element.", DITGenericCommon.PAYMENT_ABA), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }

                //DDA
                if (!drPaymentRow.Table.Columns.Contains(DITGenericCommon.PAYMENT_DDA))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in payment element.", DITGenericCommon.PAYMENT_DDA), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }

            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in DoValidate : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        /// Build Payment Elements
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="iBatchSequenceNo"></param>
        /// <returns></returns>
        public XmlElement GetPaymentElement(XmlDocument xDoc, ref  int iBatchSequenceNo)
        {
            XmlElement xPaymentElement;
            try
            {
                xPaymentElement = base.BuildXMLNode(xDoc);
                xPaymentElement.SetAttribute("CheckSequence", this.CheckSequence.ToString());
                //Images Element Under Payment
                foreach (DITGenericImages objImages in this.ImagesCollection)
                {
                    xPaymentElement.AppendChild(objImages.GetImagesElement(xDoc));
                }
                //Remittance Data Record Element Under Payment
                foreach (DITGenericRemittanceDataRecord objRemittanceDataRecord in this.RemittanceDataRecordCollection)
                {
                    objRemittanceDataRecord.BatchSequence = iBatchSequenceNo;
                    xPaymentElement.AppendChild(objRemittanceDataRecord.GetRemittanceDataRecordElement(xDoc));
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetPaymentElement : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xPaymentElement = null;

            }
            return xPaymentElement;
        }
        #endregion Class Methods
    }

    /// <summary>
    /// Payment Collection class
    /// </summary>
    //internal class DITGenericPaymentCollection : CollectionBase
    public class DITGenericPaymentCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITGenericPaymentCollection this[int index]
        {
            get { return ((DITGenericPaymentCollection)List[index]); }
            set { List[index] = value; }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITGenericPayment value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITGenericPayment value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITGenericPayment value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITGenericPayment value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITGenericPayment value)
        {
            return (List.Contains(value));
        }


        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.Generic.Components.DITGenericPayment"))
                throw new ArgumentException("value must be of type DITGenericPayment.", "value");
        }

        /// <summary>
        /// Filter Payment based on Transaction
        /// </summary>
        /// <param name="UniqueTransactionID"></param>
        /// <returns></returns>
        public DITGenericPaymentCollection Filter(int UniqueTransactionID)
        {
            DITGenericPaymentCollection objPaymentCollection = new DITGenericPaymentCollection();
            var FilteredPayment = from objPayment in this.OfType<DITGenericPayment>()
                                  where objPayment.UniqueTransactionID == UniqueTransactionID
                                  select objPayment;
            foreach (DITGenericPayment objPayment in FilteredPayment)
            {
                objPaymentCollection.Add(objPayment);
            }
            return objPaymentCollection;
        }
    }
}