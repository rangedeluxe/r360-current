﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Xml;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the document data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
*WI 173024 SAS 10/22/2014 
* Changes done to set proper batch sequence numbers 
******************************************************************************/
namespace DMP.Generic.Components
{
    public class DITGenericDocument : Document
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        /// <summary>
        /// Images Collection
        /// </summary>
        private DITGenericImagesCollection m_ImagesCollection = null;
        private int m_UniqueTransactionID;
        private int m_UniqueDocumentID;
        /// <summary>
        /// Remittance Data Record Collection
        /// </summary>
        private DITGenericRemittanceDataRecordCollection m_RemittanceDataRecordCollection = null;
        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>        
        /// <param name="oDataImportXClient"></param>
        public DITGenericDocument(cDataImportXClientBase oDataImportXClient)
        {
            this.m_DataImportXClient = oDataImportXClient;
            m_ImagesCollection = new DITGenericImagesCollection();
            m_RemittanceDataRecordCollection = new DITGenericRemittanceDataRecordCollection();
        }
        #endregion Class constructors

        #region Class Properties
        /// <summary>
        /// Unique Identifier for Transaction
        /// </summary>
        public int UniqueTransactionID
        {
            get { return m_UniqueTransactionID; }
            set { m_UniqueTransactionID = value; }
        }
        /// <summary>
        /// Unique Identifier for Document
        /// </summary>
        public int UniqueDocumentID
        {
            get { return m_UniqueDocumentID; }
            set { m_UniqueDocumentID = value; }
        }
        /// <summary>
        /// m_ImagesCollection collection
        /// </summary>
        public DITGenericImagesCollection ImagesCollection
        {
            get { return m_ImagesCollection; }
        }
        /// <summary>
        /// m_RemittanceDataRecordCollection collection
        /// </summary>
        public DITGenericRemittanceDataRecordCollection RemittanceDataRecordCollection
        {
            get { return m_RemittanceDataRecordCollection; }
        }
        #endregion Class Properties

        #region Class Methods

        /// <summary>
        /// Set Document Data
        /// </summary>
        /// <param name="drDocumentRow"></param>
        /// <returns></returns>
        public bool SetData(DataRow drDocumentRow)
        {
            bool bRetValue = false;
            try
            {
                //Record Identifier Transaction_ID
                this.UniqueTransactionID = Convert.ToInt32(drDocumentRow[DITGenericCommon.UNIQUE_TRANS_ID].ToString());
                //Record Identifier Document_ID
                if (drDocumentRow.Table.Columns.Contains(DITGenericCommon.UNIQUE_DOCUMENT_ID))
                    this.UniqueDocumentID = Convert.ToInt32(drDocumentRow[DITGenericCommon.UNIQUE_DOCUMENT_ID].ToString());
                
                base.DocumentDescriptor = "IN";
                bRetValue = true;
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in SetData : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        ///  Build Document Elements
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="iBatchSequenceNo"></param>
        /// <returns></returns>
        public XmlElement GetDocumentElement(XmlDocument xDoc, ref  int iBatchSequenceNo)
        {
            XmlElement xDocumentElement;
            try
            {
                xDocumentElement = base.BuildXMLNode(xDoc);
                //Images Element Under Document
                foreach (DITGenericImages objImages in this.ImagesCollection)
                {
                    xDocumentElement.AppendChild(objImages.GetImagesElement(xDoc));
                }
                //Remittance Data Record Element Under Document
                foreach (DITGenericRemittanceDataRecord objRemittanceDataRecord in this.RemittanceDataRecordCollection)
                {
                    objRemittanceDataRecord.BatchSequence = ++iBatchSequenceNo;
                    xDocumentElement.AppendChild(objRemittanceDataRecord.GetRemittanceDataRecordElement(xDoc));
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetDocumentElement : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xDocumentElement = null;
            }
            return xDocumentElement;
        }
        #endregion Class Methods
    }



    /// <summary>
    /// Document Collection class
    /// </summary>
    //internal class DITGenericDocumentCollection : CollectionBase
    public class DITGenericDocumentCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITGenericDocumentCollection this[int index]
        {
            get { return ((DITGenericDocumentCollection)List[index]); }
            set { List[index] = value; }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITGenericDocument value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITGenericDocument value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITGenericDocument value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITGenericDocument value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITGenericDocument value)
        {
            return (List.Contains(value));
        }


        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.Generic.Components.DITGenericDocument"))
                throw new ArgumentException("value must be of type DITGenericDocument.", "value");
        }

        /// <summary>
        /// Filter Document based on Transaction
        /// </summary>
        /// <param name="UniqueTransactionID"></param>
        /// <returns></returns>
        public DITGenericDocumentCollection Filter(int UniqueTransactionID)
        {
            DITGenericDocumentCollection objDocumentCollection = new DITGenericDocumentCollection();
            var FilteredDocument = from objDocument in this.OfType<DITGenericDocument>()
                                   where objDocument.UniqueTransactionID == UniqueTransactionID
                                   select objDocument;
            foreach (DITGenericDocument objDocument in FilteredDocument)
            {
                objDocumentCollection.Add(objDocument);
            }

            return objDocumentCollection;
        }
    }
}