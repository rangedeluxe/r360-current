﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Xml;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the RemittanceDataRecord  data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
******************************************************************************/
namespace DMP.Generic.Components
{
    public class DITGenericRemittanceData : RemittanceData
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        private int m_UniqueRemittDataRecID;
        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>        
        /// <param name="oDataImportXClient"></param>
        public DITGenericRemittanceData(cDataImportXClientBase oDataImportXClient)
        {
            this.m_DataImportXClient = oDataImportXClient;
        }
        #endregion Class constructors

        #region Class Properties
        /// <summary>
        /// Unique Identifier for Remittance Data Record
        /// </summary>
        public int UniqueRemittDataRecID
        {
            get { return m_UniqueRemittDataRecID; }
            set { m_UniqueRemittDataRecID = value; }
        }
        #endregion Class Properties


        #region Class Methods

        /// <summary>
        /// Set Document Data
        /// </summary>
        /// <param name="drDocumentRow"></param>
        /// <returns></returns>
        public bool SetData(DataRow drRemittDataRow)
        {
            bool bRetValue = false;
            try
            {
                //Record Identifier Transaction_ID
                this.UniqueRemittDataRecID = Convert.ToInt32(drRemittDataRow[DITGenericCommon.UNIQUE_REMITTANCE_DATA_REC_ID].ToString());
                //Field Name
                this.FieldName = drRemittDataRow[DITGenericCommon.REMITTANCE_DATA_FIELD_NAME].ToString();
                //Field Value
                this.FieldValue= drRemittDataRow[DITGenericCommon.REMITTANCE_DATA_FIELD_VALUE].ToString();
                bRetValue = true;
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in SetData : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        /// Validate Remittance Data
        /// </summary>
        /// <param name="drDocumentRow"></param>
        /// <returns></returns>
        public bool DoValidate(DataRow drRemittDataRow)
        {
            bool bRetValue = true;
            try
            {
                //Record Field Name
                if (!drRemittDataRow.Table.Columns.Contains(DITGenericCommon.REMITTANCE_DATA_FIELD_NAME))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in RemittanceData element.", DITGenericCommon.REMITTANCE_DATA_FIELD_NAME), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }
                //Record Field Value
                if (!drRemittDataRow.Table.Columns.Contains(DITGenericCommon.REMITTANCE_DATA_FIELD_VALUE))
                {
                    m_DataImportXClient.DoLogEvent(string.Format("Error: The generic XML file is missing the attribute {0} in RemittanceData element.", DITGenericCommon.REMITTANCE_DATA_FIELD_VALUE), this.GetType().Name, LogEventType.Error);
                    bRetValue = false;
                }                
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in DoValidate : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }

        /// <summary>
        /// Build Remittance Data Elements
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement GetRemittanceDataElement(XmlDocument xDoc)
        {
            XmlElement xRemittanceDataElement;
            try
            {
                xRemittanceDataElement = base.BuildXMLNode(xDoc);
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetRemittanceDataElement : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xRemittanceDataElement = null;

            }
            return xRemittanceDataElement;
        }

        #endregion Class Methods
    }
    
    /// <summary>
    /// Document Collection class
    /// </summary>
    public class DITGenericRemittanceDataCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITGenericRemittanceDataCollection this[int index]
        {
            get { return ((DITGenericRemittanceDataCollection)List[index]); }
            set { List[index] = value; }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITGenericRemittanceData value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITGenericRemittanceData value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITGenericRemittanceData value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITGenericRemittanceData value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITGenericRemittanceData value)
        {
            return (List.Contains(value));
        }


        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.Generic.Components.DITGenericRemittanceData"))
                throw new ArgumentException("value must be of type DITGenericRemittanceData.", "value");
        }

        /// <summary>
        /// Filter Remittance Data based on Remittance Data Record
        /// </summary>
        /// <param name="UniquePaymentID"></param>
        /// <returns></returns>
        public DITGenericRemittanceDataCollection Filter(int UniqueRemittanceDataRecordID)
        {
            DITGenericRemittanceDataCollection objRemittanceDataCollection = new DITGenericRemittanceDataCollection();
            var FilteredRemittanceData = from objRemittanceData in this.OfType<DITGenericRemittanceData>()
                                         where objRemittanceData.UniqueRemittDataRecID == UniqueRemittanceDataRecordID
                                         select objRemittanceData;
            foreach (DITGenericRemittanceData objRemittanceData in FilteredRemittanceData)
            {
                objRemittanceDataCollection.Add(objRemittanceData);
            }
            return objRemittanceDataCollection;
        }
    }
}