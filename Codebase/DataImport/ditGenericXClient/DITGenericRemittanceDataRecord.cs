﻿using System;
using WFS.LTA.DataImport.DataImportClientAPI;
using WFS.LTA.DataImport.DataImportXClientBase;
using System.Collections;
using System.Data;
using System.Xml;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 07/14/2014
*
*
* Purpose: This class holds the RemittanceDataRecord  data.
*
* Modification History
* WI 143424 SAS 07/14/2014
* Initial Creation
******************************************************************************/
namespace DMP.Generic.Components
{
    public class DITGenericRemittanceDataRecord : RemittanceDataRecord
    {
        #region Class Fields
        /// <summary>
        /// used for exception handling
        /// </summary>
        private cDataImportXClientBase m_DataImportXClient = null;
        private int m_UniqueRemittDataRecID;
        private int m_UniqueDocumentID;
        private int m_UniqueGhostDocumentID;
        private int m_UniquePaymentID;
        /// <summary>
        /// Remittance Data Collection
        /// </summary>
        private DITGenericRemittanceDataCollection m_RemittanceDataCollection = null;

        #endregion Class Fields

        #region Class constructors
        /// <summary>
        /// Class constructor
        /// </summary>        
        /// <param name="oDataImportXClient"></param>
        public DITGenericRemittanceDataRecord(cDataImportXClientBase oDataImportXClient)
        {
            this.m_DataImportXClient = oDataImportXClient;
            m_RemittanceDataCollection = new DITGenericRemittanceDataCollection();
        }
        #endregion Class constructors

        #region Class Properties
        /// <summary>
        /// Unique Identifier for Remittance Data Record
        /// </summary>
        public int UniqueRemittDataRecID
        {
            get { return m_UniqueRemittDataRecID; }
            set { m_UniqueRemittDataRecID = value; }
        }
        /// <summary>
        /// Unique Identifier for Document
        /// </summary>
        public int UniqueDocumentID
        {
            get { return m_UniqueDocumentID; }
            set { m_UniqueDocumentID = value; }
        }
        /// <summary>
        /// Unique Identifier for GhostDocumentID
        /// </summary>
        public int UniqueGhostDocumentID
        {
            get { return m_UniqueGhostDocumentID; }
            set { m_UniqueGhostDocumentID = value; }
        }
        /// <summary>
        /// Unique Identifier for Payment
        /// </summary>
        public int UniquePaymentID
        {
            get { return m_UniquePaymentID; }
            set { m_UniquePaymentID = value; }
        }
        /// <summary>
        /// m_RemittanceDataCollection collection
        /// </summary>
        public DITGenericRemittanceDataCollection RemittanceDataCollection
        {
            get { return m_RemittanceDataCollection; }
        }
        #endregion Class Properties


        #region Class Methods

        /// <summary>
        /// Set Document Data
        /// </summary>
        /// <param name="drDocumentRow"></param>
        /// <returns></returns>
        public bool SetData(DataRow drRemittDataRecRow)
        {
            bool bRetValue = false;
            try
            {
                //Record Identifier Transaction_ID
                this.UniqueRemittDataRecID = Convert.ToInt32(drRemittDataRecRow[DITGenericCommon.UNIQUE_REMITTANCE_DATA_REC_ID].ToString());
                //Record Identifier Payment_ID
                if (drRemittDataRecRow.Table.Columns.Contains(DITGenericCommon.UNIQUE_PAYMENT_ID) && (!string.IsNullOrEmpty(drRemittDataRecRow[DITGenericCommon.UNIQUE_PAYMENT_ID].ToString())))
                    this.UniquePaymentID = Convert.ToInt32(drRemittDataRecRow[DITGenericCommon.UNIQUE_PAYMENT_ID].ToString());
                else
                    this.UniquePaymentID = DITGenericCommon.DEFAULT_UNIQUE_ID;

                //Record Identifier Document_ID
                if (drRemittDataRecRow.Table.Columns.Contains(DITGenericCommon.UNIQUE_DOCUMENT_ID) && (!string.IsNullOrEmpty(drRemittDataRecRow[DITGenericCommon.UNIQUE_DOCUMENT_ID].ToString())))
                    this.UniqueDocumentID = Convert.ToInt32(drRemittDataRecRow[DITGenericCommon.UNIQUE_DOCUMENT_ID].ToString());
                else
                    this.UniqueDocumentID = DITGenericCommon.DEFAULT_UNIQUE_ID;

                //Record Identifier Ghost_Document_ID
                if (drRemittDataRecRow.Table.Columns.Contains(DITGenericCommon.UNIQUE_GHOST_DOCUMENT_ID) && (!string.IsNullOrEmpty(drRemittDataRecRow[DITGenericCommon.UNIQUE_GHOST_DOCUMENT_ID].ToString())))
                    this.UniqueGhostDocumentID = Convert.ToInt32(drRemittDataRecRow[DITGenericCommon.UNIQUE_GHOST_DOCUMENT_ID].ToString());
                else
                    this.UniqueGhostDocumentID = DITGenericCommon.DEFAULT_UNIQUE_ID;
                bRetValue = true;
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in SetData : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                bRetValue = false;
            }
            return bRetValue;
        }     

        /// <summary>
        /// Build Remittance Data Record Elements
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public XmlElement GetRemittanceDataRecordElement(XmlDocument xDoc)
        {
            XmlElement xRemittanceDataRecordElement;
            try
            {
                xRemittanceDataRecordElement = base.BuildXMLNode(xDoc);
                foreach (DITGenericRemittanceData objRemittanceData in this.RemittanceDataCollection)
                {
                    xRemittanceDataRecordElement.AppendChild(objRemittanceData.GetRemittanceDataElement(xDoc));
                }
            }
            catch (Exception ex)
            {
                m_DataImportXClient.DoLogEvent("Exception in GetRemittanceDataRecordElement : " + ex.Message, this.GetType().Name, LogEventType.Error, LogEventImportance.Essential);
                xRemittanceDataRecordElement = null;
            }
            return xRemittanceDataRecordElement;
        }
        #endregion Class Methods
    }

    /// <summary>
    /// Document Collection class
    /// </summary>
    public class DITGenericRemittanceDataRecordCollection : CollectionBase
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public DITGenericRemittanceDataRecordCollection this[int index]
        {
            get { return ((DITGenericRemittanceDataRecordCollection)List[index]); }
            set { List[index] = value; }
        }

        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(DITGenericRemittanceDataRecord value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// Searches for the specified object and returns zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(DITGenericRemittanceDataRecord value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, DITGenericRemittanceDataRecord value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(DITGenericRemittanceDataRecord value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(DITGenericRemittanceDataRecord value)
        {
            return (List.Contains(value));
        }


        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("DMP.Generic.Components.DITGenericRemittanceDataRecord"))
                throw new ArgumentException("value must be of type DITGenericRemittanceDataRecord.", "value");
        }

        /// <summary>
        /// Filter Remittance Data Records based on Payment/Document/Ghost Document
        /// </summary>
        /// <param name="UniquePaymentID"></param>  
        /// <returns></returns>
        public DITGenericRemittanceDataRecordCollection FilterRemittanceDataRecord(int UniqueID, DITGenericCommon.ObjectType enumObjectType)
        {
            DITGenericRemittanceDataRecordCollection objRemittanceDataRecordCollection = new DITGenericRemittanceDataRecordCollection();

            switch (enumObjectType)
            {
                case DITGenericCommon.ObjectType.PAYMENT:
                    var FilterPaymentRemittanceDataRecord = from objRemittanceDataRecord in this.OfType<DITGenericRemittanceDataRecord>()
                                                            where objRemittanceDataRecord.UniquePaymentID == UniqueID 
                                                            select objRemittanceDataRecord;
                    foreach (DITGenericRemittanceDataRecord objRemittanceDataRecord in FilterPaymentRemittanceDataRecord)
                    {
                        objRemittanceDataRecordCollection.Add(objRemittanceDataRecord);
                    }
                    break;
                case DITGenericCommon.ObjectType.DOCUMENT:
                    var FilterDocumentRemittanceDataRecord = from objRemittanceDataRecord in this.OfType<DITGenericRemittanceDataRecord>()
                                                             where objRemittanceDataRecord.UniqueDocumentID == UniqueID 
                                                             select objRemittanceDataRecord;
                    foreach (DITGenericRemittanceDataRecord objRemittanceDataRecord in FilterDocumentRemittanceDataRecord)
                    {
                        objRemittanceDataRecordCollection.Add(objRemittanceDataRecord);
                    }
                    break;
                case DITGenericCommon.ObjectType.GHOSTDOCUMENT:
                    var FilterGhostDocumentRemittanceDataRecord = from objRemittanceDataRecord in this.OfType<DITGenericRemittanceDataRecord>()
                                                                  where objRemittanceDataRecord.UniqueGhostDocumentID == UniqueID 
                                                                  select objRemittanceDataRecord;
                    foreach (DITGenericRemittanceDataRecord objRemittanceDataRecord in FilterGhostDocumentRemittanceDataRecord)
                    {
                        objRemittanceDataRecordCollection.Add(objRemittanceDataRecord);
                    }
                    break;
            }
            return objRemittanceDataRecordCollection;
        }
    }
}