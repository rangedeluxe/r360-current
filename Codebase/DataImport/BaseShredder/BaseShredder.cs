﻿using System;
using System.Globalization;
using System.Xml;

namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Contains helper methods for parsing XML attributes. Edge cases (e.g. how empty strings
    /// are handled) are modeled after the stored-proc-based XML parsing that we're replacing.
    /// </summary>
    public class BaseShredder
    {
        public BaseShredder(XmlReader xml)
        {
            Xml = xml;
        }

        protected XmlReader Xml { get; }

        private string GetAttributeValue(string attributeName)
        {
            var attributeValue = TryGetAttributeValue(attributeName);
            if (attributeValue == null)
            {
                throw new InvalidOperationException(
                    $"Element '{Xml.Name}' was expected to contain attribute '{attributeName}'");
            }
            return attributeValue;
        }
        public bool GetBoolean(string attributeName)
        {
            return GetInt32(attributeName) != 0;
        }
        public DateTime GetDate(string attributeName)
        {
            var attributeValue = GetAttributeValue(attributeName);
            return string.IsNullOrWhiteSpace(attributeValue)
                ? new DateTime(1900, 1, 1)
                : DateTime.ParseExact(attributeValue, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        }
        public decimal GetDecimal(string attributeName)
        {
            var attributeValue = GetAttributeValue(attributeName);
            return !string.IsNullOrWhiteSpace(attributeValue)
                ? decimal.Parse(attributeValue)
                : 0m;
        }
        public Guid GetGuid(string attributeName)
        {
            var attributeValue = GetAttributeValue(attributeName);
            return Guid.Parse(attributeValue);
        }
        public short GetInt16(string attributeName)
        {
            var attributeValue = GetAttributeValue(attributeName);
            return ParseInt16(attributeValue);
        }
        public int GetInt32(string attributeName)
        {
            var attributeValue = GetAttributeValue(attributeName);
            return ParseInt32(attributeValue);
        }
        public long GetInt64(string attributeName)
        {
            var attributeValue = GetAttributeValue(attributeName);
            return ParseInt64(attributeValue);
        }
        /// <summary>
        /// Returns the XML attribute parsed as an <see cref="Int32"/>. If the attribute is
        /// empty ("") or missing, returns null. Throws on invalid input.
        /// </summary>
        public int? GetOptionalInt32(string attributeName)
        {
            var attributeValue = TryGetAttributeValue(attributeName);
            return !string.IsNullOrEmpty(attributeValue)
                ? ParseInt32(attributeValue)
                : (int?) null;
        }
        /// <summary>
        /// Returns the XML attribute as a string, truncated to <see cref="maximumLength"/> if
        /// needed. If the attribute is empty ("") or missing, returns null.
        /// </summary>
        public string GetOptionalString(string attributeName, int maximumLength)
        {
            var attributeValue = TryGetAttributeValue(attributeName);
            return !string.IsNullOrEmpty(attributeValue)
                ? ParseString(maximumLength, attributeValue)
                : null;
        }
        public string GetString(string attributeName, int maximumLength)
        {
            var attributeValue = GetAttributeValue(attributeName);
            return ParseString(maximumLength, attributeValue);
        }
        private static short ParseInt16(string attributeValue)
        {
            return string.IsNullOrWhiteSpace(attributeValue)
                ? (short) 0
                : short.Parse(attributeValue, CultureInfo.InvariantCulture);
        }
        private static int ParseInt32(string attributeValue)
        {
            return string.IsNullOrWhiteSpace(attributeValue)
                ? 0
                : int.Parse(attributeValue, CultureInfo.InvariantCulture);
        }
        private static long ParseInt64(string attributeValue)
        {
            return string.IsNullOrWhiteSpace(attributeValue)
                ? 0
                : long.Parse(attributeValue, CultureInfo.InvariantCulture);
        }
        private static string ParseString(int maximumLength, string attributeValue)
        {
            return attributeValue.Length <= maximumLength
                ? attributeValue
                : attributeValue.Substring(0, maximumLength);
        }
        /// <summary>
        /// Returns the value of the specified XML attribute, or null if the attribute is not present.
        /// </summary>
        private string TryGetAttributeValue(string attributeName)
        {
            return Xml.GetAttribute(attributeName);
        }
    }
}