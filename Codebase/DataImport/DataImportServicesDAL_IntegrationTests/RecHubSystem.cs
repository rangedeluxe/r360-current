﻿using System;
using DatabaseIntegrationTestHelpers;

namespace DataImportServicesDAL_IntegrationTests
{
    public static class RecHubSystem
    {
        private const int DefaultAuditDateKey = 20170301;
        public const string DefaultClientProcessCode = "DefaultClientProcessCode";
        private const string DefaultXsdVersion = "0.01.00.00";

        public static InsertResult InsertDataImportQueue(IntegrationDatabase database, int queueType,
            int auditDateKey = DefaultAuditDateKey, string xsdVersion = DefaultXsdVersion, int queueStatus = 10,
            string clientProcessCode = DefaultClientProcessCode, Guid? sourceTrackingId = null,
            Guid? entityTrackingId = null, string xmlDataDocument = "", string xmlResponseDocument = "")
        {
            return database.Insert("RecHubSystem.DataImportQueue",
                new
                {
                    QueueType = queueType,
                    QueueStatus = queueStatus,
                    AuditDateKey = auditDateKey,
                    XsdVersion = xsdVersion,
                    ClientProcessCode = clientProcessCode,
                    SourceTrackingId = sourceTrackingId ?? Guid.NewGuid(),
                    EntityTrackingId = entityTrackingId ?? Guid.NewGuid(),
                    XmlDataDocument = xmlDataDocument,
                    XmlResponseDocument = xmlResponseDocument,
                });
        }
    }
}