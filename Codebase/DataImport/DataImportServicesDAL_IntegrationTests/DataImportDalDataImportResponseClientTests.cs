﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DatabaseIntegrationTestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.DataImport.DataImportClientSvcAPI;
using WFS.LTA.DataImport.DataImportServicesAPI;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.DAL;

namespace DataImportServicesDAL_IntegrationTests
{
    [TestClass]
    public class DataImportDalDataImportResponseClientTests
    {
        [ClassInitialize]
        public static void ClassSetUp(TestContext context)
        {
            Database = IntegrationDatabase.CreateNew();
            Database.MakeTable("RecHubSystem", "DataImportQueue", IgnorePartitions);
            Database.MakeStoredProcedure("RecHubSystem", "usp_DataImportQueue_Get_ClientSetupResponse");
        }
        [ClassCleanup]
        public static void ClassTearDown()
        {
            Database?.Dispose();
            Database = null;
        }
        [TestInitialize]
        public void SetUp()
        {
            Database.ClearTables();
        }

        private static IntegrationDatabase Database { get; set; }

        private static void CheckResponseItem(ResponseItem item, int expectedAuditDateKey, Guid expectedSourceTrackingId,
            string expectedMessage, bool expectedIsSuccessful)
        {
            Assert.AreNotEqual(Guid.Empty, item.ResponseTrackingId, "ResponseTrackingId");
            Assert.AreEqual(expectedAuditDateKey, item.AuditDateKey, "AuditDateKey");
            Assert.AreEqual(expectedSourceTrackingId, item.SourceTrackingId, "SourceTrackingId");
            Assert.AreEqual(expectedMessage, item.Message, "Message");
            Assert.AreEqual(expectedIsSuccessful, item.IsSuccessful, "IsSuccessful");
        }
        private static string IgnorePartitions(string sql)
        {
            return sql.Replace(" ON DataImport (AuditDateKey)", "");
        }
        
        private static IReadOnlyList<ResponseItem> LoadResponseItems()
        {
            var dal = new DataImportRepository(new R360DBConnectionSettings());
            //var dal = new cDataImportDAL("DummySiteKey");
            //dal.UseDatabase(Database.CDatabase);
            var clientSetupResponses = dal.DataImportResponseClient(RecHubSystem.DefaultClientProcessCode);
            return ClientSetupResponseService.GetResponses(clientSetupResponses);
        }

        [TestMethod]
        [Ignore]
        public void NoRecords()
        {
            var responseItems = LoadResponseItems();
            Assert.AreEqual(0, responseItems.Count, "Count");
        }

        [TestMethod]
        [Ignore]
        public void IgnoresRecordsWithUninterestingQueueStatuses()
        {
            var xmlResponseDocument = new XElement("ClientGroup",
                new XElement("Results",
                    "Success"));
            var queueStatusesToTest = new[] {10, 20, 30, 99, 120, 145, 150};
            foreach (var queueStatus in queueStatusesToTest)
            {
                // Repeat the queueStatus in the sourceTrackingId, so we can tell which records got through
                RecHubSystem.InsertDataImportQueue(Database, queueType: 0, queueStatus: queueStatus,
                    sourceTrackingId: new Guid($"00000000-0000-0000-0000-{queueStatus:D12}"),
                    xmlResponseDocument: xmlResponseDocument.ToString());
            }

            var queueStatusesAccepted = LoadResponseItems()
                .Select(responseItem => responseItem.SourceTrackingId.ToString().TrimStart('0', '-'))
                .OrderBy(int.Parse);

            Assert.AreEqual("30,99,120", string.Join(",", queueStatusesAccepted), "Filtered queue statuses");
        }

        [TestMethod]
        [Ignore]
        public void OneRecord_WithAllAttributes_AndSuccessResult()
        {
            // Let's go ahead and write one test that fills in all the attributes on the
            // <ClientGroup> element, even though it looks like this sproc doesn't use them.
            var xmlResponseDocument = new XElement("ClientGroup",
                new XAttribute("ClientTrackingID", "55519D50-F3EA-4824-BAD7-B79FBAD264FD"),
                new XAttribute("ClientGroupID", "-1"),
                new XElement("Results",
                    "Success"));
            RecHubSystem.InsertDataImportQueue(Database, queueType: 0, queueStatus: 99, auditDateKey: 20170101,
                sourceTrackingId: new Guid("07CBB672-B105-44B4-98CC-94F8A89A8E70"),
                xmlResponseDocument: xmlResponseDocument.ToString());

            var responseItems = LoadResponseItems();

            Assert.AreEqual(1, responseItems.Count, "Count");
            CheckResponseItem(responseItems[0], expectedAuditDateKey: 20170101,
                expectedSourceTrackingId: new Guid("07CBB672-B105-44B4-98CC-94F8A89A8E70"),
                expectedMessage: "", expectedIsSuccessful: true);
        }

        [TestMethod]
        [Ignore]
        public void TwoRecords_WithSuccessResults()
        {
            var xmlResponseDocument = new XElement("ClientGroup",
                new XElement("Results",
                    "Success"));
            RecHubSystem.InsertDataImportQueue(Database, queueType: 0, queueStatus: 99, auditDateKey: 20170101,
                sourceTrackingId: new Guid("07CBB672-B105-44B4-98CC-94F8A89A8E70"),
                xmlResponseDocument: xmlResponseDocument.ToString());
            RecHubSystem.InsertDataImportQueue(Database, queueType: 0, queueStatus: 99, auditDateKey: 20170317,
                sourceTrackingId: new Guid("B8629B1B-F816-4B0F-AB6F-C3DEB003CCC6"),
                xmlResponseDocument: xmlResponseDocument.ToString());

            var responseItems = LoadResponseItems()
                .OrderBy(r => r.SourceTrackingId.ToString())
                .ToList();

            Assert.AreEqual(2, responseItems.Count, "Count");
            CheckResponseItem(responseItems[0], expectedAuditDateKey: 20170101,
                expectedSourceTrackingId: new Guid("07CBB672-B105-44B4-98CC-94F8A89A8E70"),
                expectedMessage: "", expectedIsSuccessful: true);
            CheckResponseItem(responseItems[1], expectedAuditDateKey: 20170317,
                expectedSourceTrackingId: new Guid("B8629B1B-F816-4B0F-AB6F-C3DEB003CCC6"),
                expectedMessage: "", expectedIsSuccessful: true);
        }

        [TestMethod]
        [Ignore]
        public void OneRecord_WithFailureResult()
        {
            var xmlResponseDocument = new XElement("ClientGroup",
                new XAttribute("ClientTrackingID", "1C32EFB0-16D2-44C0-9F3A-A84DD98F88A1"),
                new XAttribute("ClientGroupID", "-1"),
                new XElement("Results",
                    "Fail"),
                new XElement("ErrorMessage",
                    "Something went wrong"),
                new XElement("ErrorMessage",
                    "Something else went wrong"));
            RecHubSystem.InsertDataImportQueue(Database, queueType: 0, queueStatus: 99, auditDateKey: 20170101,
                sourceTrackingId: new Guid("07CBB672-B105-44B4-98CC-94F8A89A8E70"),
                xmlResponseDocument: xmlResponseDocument.ToString());

            var responseItems = LoadResponseItems();

            Assert.AreEqual(1, responseItems.Count, "Count");
            CheckResponseItem(responseItems[0], expectedAuditDateKey: 20170101,
                expectedSourceTrackingId: new Guid("07CBB672-B105-44B4-98CC-94F8A89A8E70"),
                expectedMessage: "Something went wrong", expectedIsSuccessful: false);
        }
    }
}
