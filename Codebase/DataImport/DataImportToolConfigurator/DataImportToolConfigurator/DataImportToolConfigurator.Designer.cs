﻿namespace WFS.LTA.DataImportToolConfigurator
{
    partial class DITConfigurator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Add = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.ConfigControl = new System.Windows.Forms.TabControl();
            this.DatabasePage = new System.Windows.Forms.TabPage();
            this.DBName = new System.Windows.Forms.TextBox();
            this.ServerName = new System.Windows.Forms.TextBox();
            this.DBNameLabel = new System.Windows.Forms.Label();
            this.ServerNameLabel = new System.Windows.Forms.Label();
            this.ConfigPage = new System.Windows.Forms.TabPage();
            this.PackagePathTextBox = new System.Windows.Forms.TextBox();
            this.FilterTextBox = new System.Windows.Forms.TextBox();
            this.FilterLabel = new System.Windows.Forms.Label();
            this.ConfiguredValueTextBox = new System.Windows.Forms.TextBox();
            this.PackageVariableTextBox = new System.Windows.Forms.TextBox();
            this.ConfiguredValueLabel = new System.Windows.Forms.Label();
            this.PackageVariableLabel = new System.Windows.Forms.Label();
            this.SettingsGridView = new System.Windows.Forms.DataGridView();
            this.LoadBtn = new System.Windows.Forms.Button();
            this.ConfigControl.SuspendLayout();
            this.DatabasePage.SuspendLayout();
            this.ConfigPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SettingsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(256, 177);
            this.Add.Margin = new System.Windows.Forms.Padding(4);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(100, 28);
            this.Add.TabIndex = 0;
            this.Add.Text = "Update";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Visible = false;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(527, 741);
            this.Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(100, 28);
            this.Cancel.TabIndex = 1;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // ConfigControl
            // 
            this.ConfigControl.Controls.Add(this.DatabasePage);
            this.ConfigControl.Controls.Add(this.ConfigPage);
            this.ConfigControl.Location = new System.Drawing.Point(16, 5);
            this.ConfigControl.Margin = new System.Windows.Forms.Padding(4);
            this.ConfigControl.Name = "ConfigControl";
            this.ConfigControl.SelectedIndex = 0;
            this.ConfigControl.Size = new System.Drawing.Size(616, 729);
            this.ConfigControl.TabIndex = 3;
            // 
            // DatabasePage
            // 
            this.DatabasePage.Controls.Add(this.DBName);
            this.DatabasePage.Controls.Add(this.ServerName);
            this.DatabasePage.Controls.Add(this.DBNameLabel);
            this.DatabasePage.Controls.Add(this.ServerNameLabel);
            this.DatabasePage.Location = new System.Drawing.Point(4, 25);
            this.DatabasePage.Margin = new System.Windows.Forms.Padding(4);
            this.DatabasePage.Name = "DatabasePage";
            this.DatabasePage.Padding = new System.Windows.Forms.Padding(4);
            this.DatabasePage.Size = new System.Drawing.Size(608, 700);
            this.DatabasePage.TabIndex = 1;
            this.DatabasePage.Text = "Database Connection";
            this.DatabasePage.UseVisualStyleBackColor = true;
            // 
            // DBName
            // 
            this.DBName.Location = new System.Drawing.Point(253, 95);
            this.DBName.Margin = new System.Windows.Forms.Padding(4);
            this.DBName.Name = "DBName";
            this.DBName.Size = new System.Drawing.Size(272, 22);
            this.DBName.TabIndex = 27;
            // 
            // ServerName
            // 
            this.ServerName.Location = new System.Drawing.Point(253, 48);
            this.ServerName.Margin = new System.Windows.Forms.Padding(4);
            this.ServerName.Name = "ServerName";
            this.ServerName.Size = new System.Drawing.Size(272, 22);
            this.ServerName.TabIndex = 26;
            // 
            // DBNameLabel
            // 
            this.DBNameLabel.AutoSize = true;
            this.DBNameLabel.Location = new System.Drawing.Point(28, 95);
            this.DBNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DBNameLabel.Name = "DBNameLabel";
            this.DBNameLabel.Size = new System.Drawing.Size(108, 17);
            this.DBNameLabel.TabIndex = 25;
            this.DBNameLabel.Text = "Config DBName";
            // 
            // ServerNameLabel
            // 
            this.ServerNameLabel.AutoSize = true;
            this.ServerNameLabel.Location = new System.Drawing.Point(28, 48);
            this.ServerNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ServerNameLabel.Name = "ServerNameLabel";
            this.ServerNameLabel.Size = new System.Drawing.Size(87, 17);
            this.ServerNameLabel.TabIndex = 24;
            this.ServerNameLabel.Text = "ServerName";
            // 
            // ConfigPage
            // 
            this.ConfigPage.Controls.Add(this.PackagePathTextBox);
            this.ConfigPage.Controls.Add(this.FilterTextBox);
            this.ConfigPage.Controls.Add(this.FilterLabel);
            this.ConfigPage.Controls.Add(this.Add);
            this.ConfigPage.Controls.Add(this.ConfiguredValueTextBox);
            this.ConfigPage.Controls.Add(this.PackageVariableTextBox);
            this.ConfigPage.Controls.Add(this.ConfiguredValueLabel);
            this.ConfigPage.Controls.Add(this.PackageVariableLabel);
            this.ConfigPage.Controls.Add(this.SettingsGridView);
            this.ConfigPage.Location = new System.Drawing.Point(4, 25);
            this.ConfigPage.Name = "ConfigPage";
            this.ConfigPage.Size = new System.Drawing.Size(608, 700);
            this.ConfigPage.TabIndex = 2;
            this.ConfigPage.Text = "Config Controller";
            this.ConfigPage.UseVisualStyleBackColor = true;
            // 
            // PackagePathTextBox
            // 
            this.PackagePathTextBox.Enabled = false;
            this.PackagePathTextBox.Location = new System.Drawing.Point(256, 132);
            this.PackagePathTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.PackagePathTextBox.Name = "PackagePathTextBox";
            this.PackagePathTextBox.Size = new System.Drawing.Size(272, 22);
            this.PackagePathTextBox.TabIndex = 25;
            this.PackagePathTextBox.Visible = false;
            // 
            // FilterTextBox
            // 
            this.FilterTextBox.Enabled = false;
            this.FilterTextBox.Location = new System.Drawing.Point(256, 26);
            this.FilterTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.FilterTextBox.Name = "FilterTextBox";
            this.FilterTextBox.Size = new System.Drawing.Size(272, 22);
            this.FilterTextBox.TabIndex = 23;
            // 
            // FilterLabel
            // 
            this.FilterLabel.AutoSize = true;
            this.FilterLabel.Location = new System.Drawing.Point(29, 26);
            this.FilterLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FilterLabel.Name = "FilterLabel";
            this.FilterLabel.Size = new System.Drawing.Size(43, 17);
            this.FilterLabel.TabIndex = 24;
            this.FilterLabel.Text = "Filter:";
            // 
            // ConfiguredValueTextBox
            // 
            this.ConfiguredValueTextBox.Location = new System.Drawing.Point(256, 102);
            this.ConfiguredValueTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.ConfiguredValueTextBox.Name = "ConfiguredValueTextBox";
            this.ConfiguredValueTextBox.Size = new System.Drawing.Size(272, 22);
            this.ConfiguredValueTextBox.TabIndex = 19;
            // 
            // PackageVariableTextBox
            // 
            this.PackageVariableTextBox.Enabled = false;
            this.PackageVariableTextBox.Location = new System.Drawing.Point(256, 60);
            this.PackageVariableTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.PackageVariableTextBox.Name = "PackageVariableTextBox";
            this.PackageVariableTextBox.Size = new System.Drawing.Size(272, 22);
            this.PackageVariableTextBox.TabIndex = 18;
            // 
            // ConfiguredValueLabel
            // 
            this.ConfiguredValueLabel.AutoSize = true;
            this.ConfiguredValueLabel.Location = new System.Drawing.Point(27, 107);
            this.ConfiguredValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ConfiguredValueLabel.Name = "ConfiguredValueLabel";
            this.ConfiguredValueLabel.Size = new System.Drawing.Size(121, 17);
            this.ConfiguredValueLabel.TabIndex = 21;
            this.ConfiguredValueLabel.Text = "Configured Value:";
            // 
            // PackageVariableLabel
            // 
            this.PackageVariableLabel.AutoSize = true;
            this.PackageVariableLabel.Location = new System.Drawing.Point(27, 60);
            this.PackageVariableLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PackageVariableLabel.Name = "PackageVariableLabel";
            this.PackageVariableLabel.Size = new System.Drawing.Size(123, 17);
            this.PackageVariableLabel.TabIndex = 20;
            this.PackageVariableLabel.Text = "Package Variable:";
            // 
            // SettingsGridView
            // 
            this.SettingsGridView.AllowUserToAddRows = false;
            this.SettingsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SettingsGridView.Location = new System.Drawing.Point(3, 254);
            this.SettingsGridView.MultiSelect = false;
            this.SettingsGridView.Name = "SettingsGridView";
            this.SettingsGridView.ReadOnly = true;
            this.SettingsGridView.RowTemplate.Height = 24;
            this.SettingsGridView.Size = new System.Drawing.Size(602, 421);
            this.SettingsGridView.TabIndex = 0;
            this.SettingsGridView.SelectionChanged += new System.EventHandler(this.SettingsGridView_SelectionChanged);
            // 
            // LoadBtn
            // 
            this.LoadBtn.Location = new System.Drawing.Point(72, 741);
            this.LoadBtn.Margin = new System.Windows.Forms.Padding(4);
            this.LoadBtn.Name = "LoadBtn";
            this.LoadBtn.Size = new System.Drawing.Size(100, 28);
            this.LoadBtn.TabIndex = 2;
            this.LoadBtn.Text = "Load";
            this.LoadBtn.UseVisualStyleBackColor = true;
            this.LoadBtn.Click += new System.EventHandler(this.Load_Click);
            // 
            // DITConfigurator
            // 
            this.AcceptButton = this.Add;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(734, 784);
            this.Controls.Add(this.LoadBtn);
            this.Controls.Add(this.ConfigControl);
            this.Controls.Add(this.Cancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DITConfigurator";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Data Import Tool SSIS Configurator";
            this.ConfigControl.ResumeLayout(false);
            this.DatabasePage.ResumeLayout(false);
            this.DatabasePage.PerformLayout();
            this.ConfigPage.ResumeLayout(false);
            this.ConfigPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SettingsGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.TabControl ConfigControl;
        private System.Windows.Forms.TabPage DatabasePage;
        private System.Windows.Forms.TextBox DBName;
        private System.Windows.Forms.TextBox ServerName;
        private System.Windows.Forms.Label DBNameLabel;
        private System.Windows.Forms.Label ServerNameLabel;
        private System.Windows.Forms.Button LoadBtn;
        private System.Windows.Forms.TabPage ConfigPage;
        private System.Windows.Forms.DataGridView SettingsGridView;
        private System.Windows.Forms.TextBox ConfiguredValueTextBox;
        private System.Windows.Forms.TextBox PackageVariableTextBox;
        private System.Windows.Forms.Label ConfiguredValueLabel;
        private System.Windows.Forms.Label PackageVariableLabel;
        private System.Windows.Forms.TextBox FilterTextBox;
        private System.Windows.Forms.Label FilterLabel;
        private System.Windows.Forms.TextBox PackagePathTextBox;
    }
}

