﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 4/23/2012
*
* Purpose:Build DIT configurator
*
* Modification History
* CR  51036 WJS 4/23/2012 Created
* CR  52514 WJS 5/3/2102  Added support from SMTPServer, SMTPMailTo and SMTPMailFrom
* WI 103181 CMC 10/21/2013 Refactored to make config controller tab dynamic
*****************************************************************************/

namespace WFS.LTA.DataImportToolConfigurator
{
    public partial class DITConfigurator : Form
    {
        private const string DatabaseOrServerNotFilledMessage = "You must fill in a database name and a server name to continue";

        public DITConfigurator()
        {
            InitializeComponent();
            ConfigControl.TabPages.Remove(ConfigPage);
        }

        # region Private methods


        private void Add_Click(object sender, EventArgs e)
        {
            if ( DatabaseAndServerCheckSuccessful() )
            {
                if ( DataUpdated() )
                {
                    MessageBox.Show("Data Successfully updated");

                    if ( ConfigurationsRefreshed() )
                        MessageBox.Show("Grid successfully refreshed.");

                }
                else
                {
                    MessageBox.Show("Unable to connect to database.");
                }
            }
        }


        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Load_Click(object sender, EventArgs e)
        {
            if ( DatabaseAndServerCheckSuccessful() )
                if ( ConfigurationsRefreshed() )
                {
                    MessageBox.Show("Loaded SSIS Table successfully");

                    if ( ConfigControl.TabPages.Count == 1 )
                    {
                        ConfigControl.TabPages.Add(ConfigPage);
                        Add.Show();
                        ConfigControl.SelectedTab = ConfigPage;
                    }
                }
        }

        private bool DataUpdated()
        {
            using (cDataImportToolConfiguratorDAL dbImportToolDAL = new cDataImportToolConfiguratorDAL(this.DBName.Text, this.ServerName.Text))
            {
                return dbImportToolDAL.UpdateConfiguration(this.FilterTextBox.Text, this.PackagePathTextBox.Text, this.ConfiguredValueTextBox.Text);
            }
        }

        private bool DatabaseAndServerCheckSuccessful()
        {
            if (String.IsNullOrEmpty(this.DBName.Text) || String.IsNullOrEmpty(this.ServerName.Text))
            {
                MessageBox.Show(DatabaseOrServerNotFilledMessage);

                return false;
            }
            else
            {
                return true;
            }
        }

        private bool ConfigurationsRefreshed()
        {
            bool bolRetVal = false;

            using (cDataImportToolConfiguratorDAL dbImportToolDAL = new cDataImportToolConfiguratorDAL(this.DBName.Text, this.ServerName.Text))
            {
                DataTable dt;

                bolRetVal = dbImportToolDAL.LoadSSISConfiguration(out dt);

                if (!bolRetVal)
                {
                    MessageBox.Show("Unable to connect to database.");
                    return false;
                }

                if (dt == null)
                {
                    MessageBox.Show("No rows returned from stored proc.");
                    return false;
                }

                LoadGrid(dt);

                return true;
            }
        }

        private void LoadGrid(DataTable dt)
        {
            //dynamically build the grid view and add four columns
            this.SettingsGridView.Rows.Clear();
            this.SettingsGridView.Columns.Clear();

            this.SettingsGridView.ColumnCount = 4;
            this.SettingsGridView.Columns[0].Name = "Filter ";
            this.SettingsGridView.Columns[1].Name = "Package Variable";
            this.SettingsGridView.Columns[2].Name = "Configured Value";
            this.SettingsGridView.Columns[3].Name = "Package Path";
            this.SettingsGridView.Columns[3].Visible = false;

            //add each data row from the data table to the grid view
            foreach (var dataRow in dt.AsEnumerable())
            {
                this.SettingsGridView.Rows
                    .Add
                    (
                        new object[]
                        {
                            dataRow["ConfigurationFilter"].ToString(),
                            ToDisplayVariable(dataRow["PackagePath"].ToString()),
                            dataRow["ConfiguredValue"].ToString(),
                            dataRow["PackagePath"].ToString()
                        }
                    );
            }
        }

        private string ToDisplayVariable(string variable)
        {
            //Need to parse the string for display.
            //Have to find the "::" string, and the "]"
            string display = variable;
            int startIndexOfVariableIndentifier = display.IndexOf("::");
            int endIndexOfVariableIdentifier = display.IndexOf("]");

            //This split should result in an array of 3 rows.  The first row will contain the text before the "::".
            //The second row will contain the text of the variable we want to display.
            //The third row will contain the text after the "]".
            string[] splits = variable.Split(new string[]{"::", "]"}, StringSplitOptions.RemoveEmptyEntries);

            if (splits.Count() == 3)
                display = splits[1];

            return display;
        }

        private void SettingsGridView_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView dataGridView = sender as DataGridView;
            if (dataGridView != null)
            {
                if (dataGridView.CurrentRow != null)
                {
                    this.FilterTextBox.Text = dataGridView.CurrentRow.Cells[0].Value == null ? string.Empty : dataGridView.CurrentRow.Cells[0].Value.ToString();
                    this.PackageVariableTextBox.Text = dataGridView.CurrentRow.Cells[1].Value == null ? string.Empty : dataGridView.CurrentRow.Cells[1].Value.ToString();
                    this.ConfiguredValueTextBox.Text = dataGridView.CurrentRow.Cells[2].Value == null ? string.Empty : dataGridView.CurrentRow.Cells[2].Value.ToString();
                    this.PackagePathTextBox.Text = dataGridView.CurrentRow.Cells[3].Value == null ? string.Empty : dataGridView.CurrentRow.Cells[3].Value.ToString();
                }
            }
        }

        #endregion
    }
}
