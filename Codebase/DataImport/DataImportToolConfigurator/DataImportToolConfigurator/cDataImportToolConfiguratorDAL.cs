﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 4/23/2012
*
* Purpose:Build DIT configurator
*
* Revisions: 
* CR  51036 WJS 4/23/2012 Created
* CR  52514 WJS 5/3/2102  Added support from SMTPServer, SMTPMailTo and SMTPMailFrom
* WI 103181 CMC 10/21/2013 Added generic UpdateConfiguration method 
*******************************************************************************/
namespace WFS.LTA.DataImportToolConfigurator
{
  
    internal static class SQLPublic
    {
        internal static string SQLEncodeString(string Value)
        {
            return Value.Replace("'", "''");
        }
    }
    internal class cDataImportToolConfiguratorDAL : _DALBase 
    {

        public const string LoggingServerPackagePath   = "\\Package.Variables[User::WFSLoggingServerName].Properties[Value]";
        public const string LoggingInitialCatalog = "\\Package.Variables[User::WFSLoggingInitialCatalog].Properties[Value]";
        public const string ADOTargetInitialCatalog = "\\Package.Variables[User::adoTargetInitialCatalog].Properties[Value]";
        public const string ADOTargetServerName = "\\Package.Variables[User::adoTargetServerName].Properties[Value]";
        public const string OLETargetInitialCatalog = "\\Package.Variables[User::oleTargetInitialCatalog].Properties[Value]";
        public const string OLETargetServerName = "\\Package.Variables[User::oleTargetServerName].Properties[Value]";
        public const string ADOSourceInitialCatalog = "\\Package.Variables[User::adoSourceInitialCatalog].Properties[Value]";
        public const string ADOSourceServerName = "\\Package.Variables[User::adoSourceServerName].Properties[Value]";
        public const string OLESourceInitialCatalog = "\\Package.Variables[User::oleSourceInitialCatalog].Properties[Value]";
        public const string OLESourceServerName = "\\Package.Variables[User::oleSourceServerName].Properties[Value]";
        public const string SetupXSD = "\\Package.Variables[User::XMLSetupFileXSDFileName].Properties[Value]";
        public const string DataFileXSD = "\\Package.Variables[User::XMLDataFileXSDFileName].Properties[Value]";

        public const string SMTPServer = "\\Package.Variables[User::SMTPServer].Properties[Value]";
        public const string SMTPMailTo = "\\Package.Variables[User::SMTPMailTo].Properties[Value]";
        public const string SMTPMailFrom = "\\Package.Variables[User::SMTPMailFrom].Properties[Value]";

        private const string Service_ClientSetup        = "DataImportIntegrationServices_ClientSetup";
        private const string Service_BatchData          = "DataImportIntegrationServices_BatchData";
        private const string ConfigurationFilter        = "CommonConfigurations";

        public cDataImportToolConfiguratorDAL(string dbName, string server)
            : base(dbName, server)
        {
        }

        /// <summary>
        /// Updates the configuration setting value.
        /// </summary>
        /// <param name="filter">Configuration Filter</param>
        /// <param name="packagePath">Package Path</param>
        /// <param name="configuredValue">Configured Value</param>
        /// <returns>True/False whether the update was successful.</returns>
        public bool UpdateConfiguration(string filter, string packagePath, string configuredValue)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, packagePath, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, configuredValue, filter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateConfiguration");
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        public bool UpdateClientInput(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, SetupXSD, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, Service_ClientSetup));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateClientInput");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        public bool UpdateBatchData(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, DataFileXSD, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, Service_BatchData));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateBatchData");
                bolRetVal = false;
            }

            return (bolRetVal);
        }



        public bool UpdateLoggingServerName(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, LoggingServerPackagePath, ParameterDirection.Input));


                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));
                 
            } catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "UpdateLoggingServerName");
                bolRetVal = false;
            }

            return(bolRetVal);
        }


        public bool UpdateLoggingInitialCatalog(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, LoggingInitialCatalog, ParameterDirection.Input));



                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateLoggingInitialCatalog");
                bolRetVal = false;
            }

            return (bolRetVal);
        }

     
        public bool UpdateADOTargetInitialCatalog(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, ADOTargetInitialCatalog, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateADOTargetInitialCatalog");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        public bool UpdateADOTargetServerName(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, ADOTargetServerName, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateADOTargetServerName");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        public bool UpdateOLESourceInitialCatalog(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, OLESourceInitialCatalog, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateOLESourceInitialCatalog");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        public bool UpdateOLESourceServerName(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, OLESourceServerName, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateOLESourceServerName");
                bolRetVal = false;
            }

            return (bolRetVal);
        }


        public bool UpdateADOSourceInitialCatalog(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, ADOSourceInitialCatalog, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateADOSourceInitialCatalog");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        public bool UpdateADOSourceServerName(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, ADOSourceServerName, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateADOTargetServerName");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        public bool UpdateOLETargetInitialCatalog(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, OLETargetInitialCatalog, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateOLETargetInitialCatalog");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        public bool UpdateOLETargetServerName(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, OLETargetServerName, ParameterDirection.Input));
                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateOLETargetServerName");
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        public bool UpdateSMTPServer(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, SMTPServer, ParameterDirection.Input));


                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateSMTPServer");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        public bool UpdateSMTPMailFrom(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, SMTPMailFrom, ParameterDirection.Input));


                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateSMTPMailFrom");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        public bool UpdateSMTPMailTo(string value)
        {
            bool bolRetVal = false;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPackagePath", SqlDbType.VarChar, SMTPMailTo, ParameterDirection.Input));


                return (ExecuteConfigUpdate(arParms, value, ConfigurationFilter));

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateSMTPMailTo");
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        public bool LoadSSISConfiguration(out DataTable dt)
        {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            dt = null;
            try
            {

                arParms = new List<SqlParameter>();
                parms = arParms.ToArray();
                bolRetVal = executeProcedure("dbo.usp_SSISConfigurations_Select", parms, out dt);

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "LoadSSISConfiguration");
                bolRetVal = false;
            }

            return (bolRetVal);
        }
       



        /// <summary>
        /// common routine for all the updates to call
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        private bool ExecuteConfigUpdate(  List<SqlParameter> arParms, string value, string configFilter)
        {
            bool bolRetVal = false;

            SqlParameter[] parms;
            try
            {
                //one parameter is already filled out 
                arParms.Add(BuildParameter("@parmConfiguredValue", SqlDbType.VarChar, value, System.Data.ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmConfigurationFilter", SqlDbType.VarChar, configFilter, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmErrorDescription", SqlDbType.VarChar, 0, ParameterDirection.Output));
                arParms.Add(BuildParameter("@parmSQLErrorNumber", SqlDbType.Int, 0, ParameterDirection.Output));

                parms = arParms.ToArray();
                bolRetVal = executeProcedure("dbo.usp_SSISConfigurations_Update", parms);
                if (bolRetVal)
                {
                    if ((int)parms[4].Value != (int)0)
                    {
                        EventLog.logEvent("An error occurred while executing usp_SSISConfigurations_Update " + parms[3].Value.ToString()
                                        , string.Empty
                                        , MessageType.Error
                                        , MessageImportance.Essential);
                        bolRetVal = false;
                    }
                    else
                    {
                        bolRetVal = true;
                    }
                }
            } catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "UpdateLoggingServerName");
                bolRetVal = false;
            }
            return(bolRetVal); 
        }

     
    }
}
