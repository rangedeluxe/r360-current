﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Collections;
using WFS.RecHub.Common;
using WFS.LTA.Common;
using WFS.RecHub.Common.Log;
//using WFS.integraPAY.Online.Common;


/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author: Wayne Schwarz
* Date: 4/23/2012
*
* Purpose:Build DIT configurator
*
* Modification History
* CR  51036 WJS 4/23/2012 Created
* WI 126987 CMC 01/20/2014
*  - Updated to use 2.00 libraries.
******************************************************************************/
namespace WFS.LTA.DataImportToolConfigurator
{
    /// <summary></summary>
    public abstract class _DALBase : IDisposable {

        private string _DecryptedConnectionString = string.Empty;
        private cError _LastError = null;
        private Exception _LastException = null;
        private string _Server = string.Empty;
        private string _DBname = string.Empty;
        private SqlTransaction _SQLTrans = null;

        /// <summary></summary>
        public _DALBase(string dbName, string server) 
        {
            _Server = server;
            _DBname = dbName;
        }

        private SqlConnection _Connection = null;
        private byte _QueryRetryAttempts = 10;
        private const string APP_NAME = "ipoServices";
        private cSiteOptions _SiteOptions = null;
        private cEventLog _EventLog = null;
        private string logFile = "DataImportToolConfigurator.txt";

        /// <summary>
        /// Event handler to output informational text.
        /// </summary>
        public event outputMessageEventHandler outputMessage;

        /// <summary>
        /// EventHandler to output error information.
        /// </summary>
        public event outputErrorEventHandler outputError;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void onOutputError(System.Exception e) {
            if (outputError != null) {
                outputError(e);
            }
            _LastException = e;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        public void onOutputMessage(string message
                                  , string src
                                  , MessageType messageType
                                  , MessageImportance messageImportance) {
            if (outputMessage != null) {
                outputMessage(message
                            , src
                            , messageType
                            , messageImportance);
            }
        }

        /// <summary>
        /// Implement IDisposable.
        /// Do not make this method virtual.
        /// A derived class should not be able to override this method.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (disposing)
            {
                if (_Connection != null)
                {

                    _Connection.Dispose();
                    _Connection = null;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private SqlConnection connection
        {
            get
            {
                if (_Connection == null)
                {
                    StringBuilder connString = new StringBuilder();
                    connString.AppendFormat("Server={0};Integrated Security=True;Database={1}", _Server, _DBname);
                    _Connection = new SqlConnection(connString.ToString());
                }
                if (_Connection.State != ConnectionState.Open)
                {
                    _Connection.Open();
                }
                return (_Connection);
            }
        }

        /// <summary>
        /// Logging component using settings from the local Options object.
        /// </summary>
        public  cEventLog EventLog {
            get {
                if (_EventLog == null) {
                    _EventLog = new cEventLog(logFile, 
                                              4192, 
                                              MessageImportance.Essential); 
                }

                return _EventLog;
            }
        }

        public byte QueryRetryAttempts
        {
            get { return (_QueryRetryAttempts); }
        }

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        internal cSiteOptions SiteOptions {

            get {

                if(_SiteOptions == null) { 
                    _SiteOptions = new cSiteOptions(APP_NAME);
                }

                return _SiteOptions;
            }
        }

        public SqlParameter BuildParameter(string ParameterName, SqlDbType Type, object Value, ParameterDirection Direction)
        {

            SqlParameter objParameter = new SqlParameter();
            int intSize = 0;
            int i;

            objParameter.ParameterName = ParameterName;
            objParameter.SqlDbType = Type;
            objParameter.Value = Value;
            objParameter.Direction = Direction;

            if (Type == SqlDbType.Xml)
            {
                if (Value == null)
                {
                    objParameter.Size = 4096;
                }
                else
                {
                    i = 10;
                    intSize = 2 ^ i;
                    while (intSize < Value.ToString().Length)
                    {
                        ++i;
                        intSize = 2 ^ i;
                    }
                    objParameter.Size = intSize;
                }
            }

            return (objParameter);
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        protected bool executeProcedure(string procName, SqlParameter[] parms)
        {
            SqlParameter[] objOutParms;
            return (executeProcedure(procName, parms, out objOutParms));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <param name="outParms"></param>
        protected bool executeProcedure(string procName, SqlParameter[] parms, out SqlParameter[] outParms)
        {

            SqlCommand cmd = null;
            SqlParameter[] returnParms;
            int intTries = 0;
            bool bolContinue;
            bool bolRetVal;

            try
            {

                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procName;
                cmd.Connection = connection;



                foreach (SqlParameter parm in parms)
                {
                    cmd.Parameters.Add(parm);
                }

                bolContinue = true;
                bolRetVal = false;

                while (bolContinue)
                {
                    ++intTries;
                    try
                    {
                        cmd.ExecuteNonQuery();
                        bolContinue = false;
                        bolRetVal = true;
                    }
                    catch (Exception ex)
                    {
                        if (intTries > this.QueryRetryAttempts)
                        {
                            EventLog.logEvent("Exception is " + ex.Message + "An Error occurred while executing Procedure: " + procName, this.GetType().ToString(), MessageType.Error, MessageImportance.Essential);
                            bolContinue = false;
                        }
                        else
                        {
                            EventLog.logEvent("Exception is " + ex.Message + "An Error occurred while executing Procedure.  Attempting retry(" + intTries.ToString() + ")...", this.GetType().ToString(), MessageType.Error, MessageImportance.Essential);
                        }
                    }
                }

                returnParms = new SqlParameter[cmd.Parameters.Count];
                cmd.Parameters.CopyTo(returnParms, 0);
            }
            catch (Exception ex)
            {
                EventLog.logEvent(ex.Message, this.GetType().ToString(), MessageType.Error, MessageImportance.Essential);
                returnParms = null;
                bolRetVal = false;
            }
            finally
            {

                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }

            }

            outParms = returnParms;

            return (bolRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <param name="datatable"></param>
        /// <returns></returns>
        public bool executeProcedure(string procName, SqlParameter[] parms, out System.Data.DataTable datatable)
        {
            SqlParameter[] objOutParms;
            return (executeProcedure(procName, parms, out objOutParms, out datatable));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <param name="datatable"></param>
        /// <param name="outParms"></param>
        /// <returns></returns>
        public bool executeProcedure(string procName, SqlParameter[] parms, out SqlParameter[] outParms, out System.Data.DataTable datatable)
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procName;
            cmd.Connection = connection;
            SqlParameter[] returnParms = null;
            int intTries = 0;
            bool bolContinue = true;
            DataSet ds = null;
            SqlDataAdapter da;
            bool bolRetVal;

            _LastException = null;

            if (_SQLTrans != null)
            {
                cmd.Transaction = _SQLTrans;
            }

            foreach (SqlParameter parm in parms)
            {
                cmd.Parameters.Add(parm);
            }

            da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            datatable = null;

            try
            {
                ds = new DataSet();
                bolRetVal = false;

                while (bolContinue)
                {
                    ++intTries;
                    try
                    {
                        da.Fill(ds);
                        bolContinue = false;
                        bolRetVal = true;
                    }
                    catch (Exception ex)
                    {
                        if (intTries > this.QueryRetryAttempts)
                        {
                            onOutputMessage("An Error occurred while running stored procedure: " + procName, ""
                                          , MessageType.Error
                                          , MessageImportance.Essential);
                            onOutputError(ex);
                            bolContinue = false;
                        }
                        else
                        {
                            onOutputMessage("An Error occurred while running stored procedure.  Attempting retry(" + intTries.ToString() + ")...", ""
                                          , MessageType.Warning
                                          , MessageImportance.Essential);
                        }
                    }
                }
                da.Dispose();
                da = null;

                if (ds != null && ds.Tables.Count > 0)
                {
                    datatable = ds.Tables[0];
                }
                else
                {
                    datatable = null;
                }
                returnParms = new SqlParameter[cmd.Parameters.Count];
                cmd.Parameters.CopyTo(returnParms, 0);
            }
            catch (Exception e)
            {
                onOutputError(e);

                if (datatable != null)
                {
                    datatable.Dispose();
                    datatable = null;
                }
                returnParms = null;
                bolRetVal = false;
            }
            finally
            {

                if (da != null)
                {
                    da.Dispose();
                    da = null;
                }

                if (ds != null)
                {
                    ds.Dispose();
                    ds = null;
                }

                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }

                if (_SQLTrans == null && _Connection != null)
                {
                    TerminateConnection();
                }
            }

            outParms = returnParms;

            return (bolRetVal);
        }

        private void TerminateConnection()
        {

            if (_SQLTrans != null)
            {
                try
                {
                    _SQLTrans.Rollback();
                }
                catch
                {
                    // Do Nothing.
                }
                _SQLTrans = null;
            }

            try
            {
                if (_Connection != null && _Connection.State == ConnectionState.Open)
                {
                    _Connection.Close();
                }
            }
            catch (Exception ex)
            {
                outputMessage("An exception occurred closing the database connection: " + ex.Message, this.GetType().Name.ToString(), MessageType.Error, MessageImportance.Debug);
            }

            try
            {
                if (_Connection != null)
                {
                    _Connection.Dispose();
                    _Connection = null;
                }
            }
            catch (Exception ex)
            {
                outputMessage("An exception occurred disposing the database connection: " + ex.Message, this.GetType().Name.ToString(), MessageType.Error, MessageImportance.Debug);
            }
        }
        public int executeNonQuery(SqlCommand cmd)
        {
            int intRetVal = -1;
            int intTries = 0;
            bool bolContinue = true;

            try
            {
                while (bolContinue)
                {
                    ++intTries;
                    try
                    {
                        intRetVal = cmd.ExecuteNonQuery();
                        bolContinue = false;
                    }
                    catch (Exception ex)
                    {
                        if (intTries > this.QueryRetryAttempts)
                        {
                            EventLog.logEvent( "Exception is " + ex.Message + "An Error occurred while executing SQL: " + cmd.CommandText, this.GetType().ToString(), MessageType.Error, MessageImportance.Essential);

                            bolContinue = false;
                        }
                        else
                        {
                            EventLog.logEvent("Exception is " + ex.Message + "An Error occurred while executing SQL.  Attempting retry(" + intTries.ToString() + ")...", this.GetType().ToString(), MessageType.Error, MessageImportance.Essential);
                            System.Threading.Thread.Sleep(500);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return -1;
            }
            return intRetVal;

        }
      
        /// <summary>
        /// Executes a database call that does not return a recordset
        /// (UPDATE, INSERT, DELETE, etc...)
        /// </summary>
        /// <param name="SQL">
        /// SQL Statement to be executed on the database.
        /// </param>
        /// <returns>
        /// Boolean indicating whether the SQL statement returned results 
        /// successfully.
        /// </returns>
        public int executeNonQuery(string SQL)
        {

            int intRetVal = -1;
            SqlCommand cmd = null;

            try
            {
                EventLog.logEvent("Execute Query: " + SQL, this.GetType().ToString(), MessageType.Information, MessageImportance.Debug);


                cmd = new SqlCommand(SQL, connection);
                

                intRetVal = executeNonQuery(cmd);

               

               
            }
            catch (Exception)
            {
                return -1;
            }
            finally
            {

                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }

            }
            return (intRetVal);
        }
        
      

        /// <summary></summary>
        public cError GetLastError {
            get {
                if(_LastError == null) { 
                    _LastError = new cError(0, string.Empty);
                }
                return(_LastError);
            }
        }
    }

    /// <summary></summary>
    public class cError {

        private int _Number = 0;
        private string _Description = string.Empty;

        public cError(int Number, string vDescription) {
            _Number = -1;
            _Description = vDescription;
        }

        /// <summary></summary>
        public int Number {
            get {
                return(_Number);
            }
        }

        /// <summary></summary>
        public string Description {
            get {
                return(_Description);
            }
        }
    }
}
