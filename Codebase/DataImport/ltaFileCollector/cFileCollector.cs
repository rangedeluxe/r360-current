﻿using System;
using System.Collections.Generic;
using System.IO;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/14/2012
*
* // Purpose:  This class supplies and maintains a list of input files waiting to be processed per 
*           source path. This list is sorted by the priority of the file being processed.  This 
*           priority is determined by an ICompare object supplied to this class. This class also 
*           supplies methods to lock the input file by moving it to the staging directory, to move
*           staged files back to the input directory in the case of retry, to move errored files
*           to an error directory, and move successfully processed files to the archive directory
*
* Modification History
* Created
*  CR 52267: CEJ 06/14/2012
*   - DIT Client
* WI 71402 WJS 11/20/2012
 *   - Add Pending Response Folder
*******************************************************************************/

namespace WFS.LTA.Common.FileCollector
{
    public delegate void dlgInputFileAvailable(object sender, cInputFile file);

    public class cFileCollector
    {
        private cMultiPathWatcher _tmrWatchMan = new cMultiPathWatcher();
        private Dictionary<_SourcePathBase, List<string>> _dctEnqueuedFiles = new Dictionary<_SourcePathBase, List<string>>();
        private readonly Action<string, string, LTAMessageType, LTAMessageImportance> _outputLog;

        public void SetWorkAvailEvent(dlgInputFileAvailable eventItem)
        {
            _tmrWatchMan.InputFileAvaiable += eventItem;
        }

        public cFileCollector(_SourcePathBase[] lstSources,
            Action<string, string, LTAMessageType, LTAMessageImportance> outputLog)
        {
            _outputLog = outputLog;

            foreach (_SourcePathBase srcBase in lstSources)
            {

                if (String.IsNullOrEmpty(srcBase.ArchiveFolder))
                {
                    OnOutputLog("Archive Folder is empty will not archive files", "CFileCollector", LTAMessageType.Information, LTAMessageImportance.Debug);

                }
                else
                {
                    IsDirReady(srcBase.ArchiveFolder);
                }
                IsDirReady(srcBase.InProcessFolder);
                IsDirReady(srcBase.InputFolder);
                IsDirReady(srcBase.PendingResponseFolder);
                IsDirReady(srcBase.ResponseErrorFolder);
                IsDirReady(srcBase.ResponseFolder);
                OnOutputLog("Setting up input folder: " + srcBase.InputFolder, "CFileCollector", LTAMessageType.Information, LTAMessageImportance.Debug);
                _tmrWatchMan.AddName(srcBase, srcBase.InputFolder);
                OnOutputLog("Input Folder: " + srcBase.InputFolder + " Pattern is :" +  srcBase.FilePattern, "CFileCollector", LTAMessageType.Information, LTAMessageImportance.Debug);
                _tmrWatchMan.Add(srcBase.InputFolder, srcBase.FilePattern);
            }

            _tmrWatchMan.Enabled = true;
        }

        private bool IsDirReady(string sPath)
        {
            bool bDirExists=false;

            try
            {
                bDirExists = Directory.Exists(sPath);
                if (!bDirExists)
                {
                    Directory.CreateDirectory(sPath);
                    bDirExists = Directory.Exists(sPath);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return bDirExists;
        }

        public void Dispose()
        {
            try
            {
                if (_tmrWatchMan != null)
                    _tmrWatchMan.Dispose();
            }
            catch { }
        }

        private void OnOutputLog(string msg, string src, LTAMessageType messageType,
            LTAMessageImportance messageImportance)
        {
            if (_outputLog == null)
            {
                Console.WriteLine(msg);
            }
            else
            {
                _outputLog(msg, src, messageType, messageImportance);
            }
        }
    }
}
