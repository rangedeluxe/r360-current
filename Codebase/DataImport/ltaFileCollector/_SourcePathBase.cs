﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms.Design;
using System.Drawing.Design;
using System.ComponentModel;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/14/2012
*
* Purpose:  The base class for the classes that house the source data for the directory 
*           being monitored.
*
* Modification History
* Created
*  CR 52267: CEJ 06/14/2012
*   - DIT Client
* WI 70082 WJS 11/20/2012
 *   - Add Pending Response Folder
* WI 136006 CMC 04/21/2014
*   - Add Input Error Folder   
*******************************************************************************/


namespace WFS.LTA.Common.FileCollector {
    
    public abstract class _SourcePathBase {

        protected string _InputFolder;
        protected string _FilePattern;
        protected string _InProcessFolder;
        protected string _ResponseFolder;
        protected string _ResponseErrorFolder;
        protected string _ArchiveFolder;
        protected string _PendingResponseFolder;
        protected string inputErrorFolder;
        protected string _StatusEndPoint;

        public _SourcePathBase() : this(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty) {
        }

        public _SourcePathBase(
            string inputFolder,
            string filePattern,
            string inProcessFolder,
            string responseFolder,
            string pendingResponseFolder,
            string statusEndPoint) {

            InputFolder = inputFolder;
            _FilePattern = filePattern;
            _InProcessFolder = inProcessFolder;
            _ResponseFolder = responseFolder;
            _PendingResponseFolder = pendingResponseFolder;
            _StatusEndPoint = statusEndPoint;
        }

        [Browsable(true)]
        [EditorAttribute(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string StatusEndPoint
        {
            get
            {
                return _StatusEndPoint;
            }
            set
            {
                _StatusEndPoint = value;
            }
        }

        [Browsable(true)]
        [EditorAttribute(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string ArchiveFolder {
            get {
                return _ArchiveFolder;
            }
            set {
                _ArchiveFolder = value;
            }
        }

        [Browsable(true)]
        [EditorAttribute(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string ResponseFolder {
            get {
                return _ResponseFolder;
            }
            set {
                _ResponseFolder = value;
            }
        }

        [Browsable(true)]
        [EditorAttribute(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string InProcessFolder {
            get {
                return _InProcessFolder;
            }
            set {
                _InProcessFolder = value;
            }
        }

        [Browsable(true)]
        [EditorAttribute(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string InputFolder {
            get {
                return _InputFolder; 
            }
            set {
                _InputFolder = value;
            }
        }

        [Browsable(true)]
        [EditorAttribute(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string ResponseErrorFolder {
            get {
                return _ResponseErrorFolder;
            }
            set {
                _ResponseErrorFolder = value;
            }
        }

        [Browsable(true)]
        [EditorAttribute(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string PendingResponseFolder
        {
            get
            {
                return _PendingResponseFolder;
            }
            set
            {
                _PendingResponseFolder = value;
            }
        }

        [Browsable(true)]
        [EditorAttribute(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string InputErrorFolder
        {
            get
            {
                return inputErrorFolder;
            }
            set
            {
                inputErrorFolder = value;
            }
        }

        public string FilePattern {
            get {
                return _FilePattern;
            }
            set {
                _FilePattern = value;
            }
        }

      

        public FileInfo[] ScanPath() {
        
            DirectoryInfo driCurDir = new DirectoryInfo(InputFolder);

            return driCurDir.GetFiles(_FilePattern);
        }

        public string GetStageDirectory(string sFileProcessorID) {
            return System.IO.Path.Combine(_InProcessFolder, sFileProcessorID);
        }
    }
}
