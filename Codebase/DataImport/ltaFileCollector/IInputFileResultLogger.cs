﻿namespace WFS.LTA.Common.FileCollector
{
    public interface IInputFileResultLogger
    {
        void WriteLineToResult(string sMessage);
    }
}