﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/14/2012
*
* Purpose:  Binds the file info with the source info in order to tie the file to the 
*           processing details
*
* Modification History
* Created
*  CR 52267: CEJ 14/06/2012
*   - DIT Client
*
*******************************************************************************/


namespace WFS.LTA.Common.FileCollector
{
    public class cInputFile : IInputFileResultLogger
    {
        private FileInfo _fioFileData;
        private _SourcePathBase _spdSourcePath;
        private StringBuilder _sXMLResult;
        private IList<Guid> _completedBatchTrackingIds;

        public cInputFile():this(null, null)
        {
        }
        
        public cInputFile(FileInfo fioFileData, _SourcePathBase spdPath)
        {
            _fioFileData = fioFileData;
            _spdSourcePath = spdPath;
            _sXMLResult = new StringBuilder();
            _completedBatchTrackingIds = new List<Guid>();
        }

        public string OriginalInputFileName
        {
            get;
            set;
        }


        public string SubmittedXML
        {
            get;
            set;
        }

        public FileInfo FileData
        {
            get
            {
                return _fioFileData;
            }
            set
            {
                _fioFileData = value;
            }
        }

        public string GetProcessResult()
        {
            return _sXMLResult.ToString();
        }

        public void SetProcessResult(string sResult)
        {
            _sXMLResult = new StringBuilder(sResult);
        }

        public void WriteToResult(string sMessage)
        {
            _sXMLResult.Append(sMessage);
        }

        public void WriteLineToResult(string sMessage)
        {
            _sXMLResult.AppendLine(sMessage);
        }

        public _SourcePathBase SourcePathData
         {
            get
            {
                return _spdSourcePath;
            }
            set
            {
                _spdSourcePath = value;
            }
        }

        public void TrackCompletedBatch(Guid batchTrackingId)
        {
            _completedBatchTrackingIds.Add(batchTrackingId);
        }

        public bool AllBatchesAreCompleted()
        {
            if (string.IsNullOrEmpty(SubmittedXML))
                return true;//there wasn't anything submitted in the file, return true

            var submittedBatches = XElement.Parse(SubmittedXML);//parse the submitted xml

            var batchElements = submittedBatches.Elements("Batch");//get list of 'Batch' elements
            if (batchElements.FirstOrDefault() == null)//if there are no batch elements, return true
                return true;

            var batchesNotCompleted = batchElements //query batch elements that were submitted in the file
                .Where(batchElement => _completedBatchTrackingIds
                .All(completedId => //get all of the batches that are not completed
                {
                    Guid batchTrackingId;
                    Guid.TryParse(batchElement.Attribute("BatchTrackingID").Value, out batchTrackingId);
                    return completedId != batchTrackingId;
                }));
                
            //if the list of batches not completed is null, we've accounted for all submitted batches 
            return batchesNotCompleted.FirstOrDefault() == null;
        }
    }
 }
