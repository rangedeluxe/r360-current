﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     06/14/2012
*
* Purpose:  This is an extension of the file watcher functionality to include multiple directories 
*
* Modification History
* Created
*  CR 52267: CEJ 06/14/2012
*   - DIT Client
*  WI 70082 WJS  11/27/2012
 *   - Add pending response
*******************************************************************************/


namespace WFS.LTA.Common.FileCollector
{
    public delegate void dlgChangedEvent(FileSystemEventArgs e);

    public class cMultiPathWatcher : IDisposable
    {
        private List<FileSystemWatcher> _FileWatchers = new List<FileSystemWatcher>();

        private Hashtable _sourcePathBaseHashTable = new Hashtable();
        public event dlgInputFileAvailable InputFileAvaiable;
        private System.Timers.Timer _FileWatcherBackupTimer;
        private bool _initialTimerFired = false;
        private const int TIMER_INTERVAL = 2000 * 60; // 2 minutes

        public cMultiPathWatcher()
        {
            _initialTimerFired = false;
            _FileWatcherBackupTimer = new System.Timers.Timer();
            _FileWatcherBackupTimer.Interval = 10000; //start at 10  seconds
            _FileWatcherBackupTimer.Elapsed += new System.Timers.ElapsedEventHandler(_FileWatcherBackupTimer_Elapsed);
            _FileWatcherBackupTimer.Enabled = true;

        }

        private FileSystemWatcher StringToFileWatcher(string sPath, string sFilter)
        {
            FileSystemWatcher wtcAns = new FileSystemWatcher(sPath, sFilter);

            wtcAns.EnableRaisingEvents = Enabled;
            wtcAns.Created += mlstWatchers_Created;
            wtcAns.Changed += mlstWatchers_Changed;
            wtcAns.Renamed += mlstWatchers_Renamed;

            return wtcAns;
        }
        private void _FileWatcherBackupTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {

            try {

                _FileWatcherBackupTimer.Enabled = false;
                if (!_initialTimerFired)
                {
                    //change to 2 minutes after inital run
                    _initialTimerFired = true;
                    _FileWatcherBackupTimer.Interval = TIMER_INTERVAL;
                }
                if (_FileWatchers != null)
                {
                    for (int i = 0; i < _FileWatchers.Count; ++i)
                    {
                        FileSystemWatcher watch = _FileWatchers[i];
                        ProcessFile(watch);
                    }
                }
                
            } catch(Exception ex) {
                throw ex;
            } finally {
                _FileWatcherBackupTimer.Enabled = true;
            }
        }

        public bool Enabled
        {
            get
            {
                bool bAns = false;
                if (_FileWatchers.Count > 0)
                    bAns = _FileWatchers[0].EnableRaisingEvents;
                return bAns;
            }
            set
            {
                foreach (FileSystemWatcher wtcCurWatcher in _FileWatchers)
                    wtcCurWatcher.EnableRaisingEvents = value;
            }
        }
        protected void mlstWatchers_Created(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("FileSystemWatcher_Created");
            ProcessFileDrop(sender);
        }
        protected void mlstWatchers_Changed(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("FileSystemWatcher_Changed");
            ProcessFileDrop(sender);
        }
        protected void mlstWatchers_Renamed(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("FileSystemWatcher_Renamed");
            ProcessFileDrop(sender);
        }

        private void ProcessFile(FileSystemWatcher watch)
        {
            int intFilesProcessed;
            try
            {
                 ProcessPendingFiles(watch.Path, watch.Filter, out intFilesProcessed);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void ProcessFileDrop(object sender)
        {
            try
            {
                //turn off timer while processing files
                _FileWatcherBackupTimer.Enabled = false;
                ((FileSystemWatcher)sender).EnableRaisingEvents = false;
                FileSystemWatcher watch = (FileSystemWatcher)sender;
                ProcessFile(watch);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ((FileSystemWatcher)sender).EnableRaisingEvents = true;
                _FileWatcherBackupTimer.Enabled = true;
            }
        }
        private bool ProcessPendingFiles(string inputFolder, string pattern, out int FilesProcessed)
        {

            DirectoryInfo directoryInfo;
            List<FileInfo> arFiles;
            int intFilesProcessed;
            bool bolRetVal, bolContinue;

            intFilesProcessed = 0;

            try
            {
                _SourcePathBase srcPathBase = null;
                if (_sourcePathBaseHashTable.Contains(inputFolder))
                {
                    srcPathBase = (_SourcePathBase)_sourcePathBaseHashTable[inputFolder];
                }
                bolContinue = true;
                while (bolContinue)
                {

                    directoryInfo = new DirectoryInfo(inputFolder);
                    arFiles = directoryInfo.GetFiles(pattern).OrderBy(t => t.LastWriteTime).ToList();

                    if (arFiles.Count > 0)
                    {
                        foreach (FileInfo fileinfo in arFiles)
                        {
                            cInputFile inpCurFile = new cInputFile(fileinfo, srcPathBase);
                            Console.WriteLine("Processing File:" + fileinfo.FullName);
                            inpCurFile.OriginalInputFileName = fileinfo.FullName;
                            InputFileAvaiable(this, inpCurFile);
                            intFilesProcessed++;
                        }
                    }
                    else
                    {
                        bolContinue = false;
                    }
                }

                bolRetVal = true;

            }
            catch (Exception ex)
            {
                bolRetVal = false;
                throw ex;
            }

            FilesProcessed = intFilesProcessed;
            return (bolRetVal);
        }
        public int IndexOf(string item)
        {
            int iCurIndex = 0;

            foreach (FileSystemWatcher wtcCurWatcher in _FileWatchers)
            {
                if (wtcCurWatcher.Path.ToLower() == item.ToLower())
                    break;
                iCurIndex++;
            }
            return iCurIndex == _FileWatchers.Count ? -1 : iCurIndex;
        }

        public void Insert(int index, string path, string filter)
        {
            if (index <= _FileWatchers.Count && index > -1)
                _FileWatchers.Insert(index, StringToFileWatcher(path, filter));
            else
                throw new Exception("Insert Failed Invalid Index");
        }

        public void RemoveAt(int index)
        {
            if (index < _FileWatchers.Count && index > -1)
                _FileWatchers.RemoveAt(index);
            else
                throw new Exception("Remove Failed Invalid Index");
        }

        public string this[int index]
         {
            get
            {
                string sAns = string.Empty;
                if (index < _FileWatchers.Count && index > -1)
                    sAns = _FileWatchers[index].Path;
                else
                    throw new Exception("Invalid Index referenced");
                return sAns;
            }
         
        }



        public void AddName(_SourcePathBase srcBase, string fileDirectory)
        {
            _sourcePathBaseHashTable.Add(fileDirectory, srcBase);
        }

        public void Add(string path, string filter)
        {
            FileSystemWatcher fswNewWatcher = StringToFileWatcher(path, filter);

            if (!_FileWatchers.Contains(fswNewWatcher))
                _FileWatchers.Add(StringToFileWatcher(path, filter));
        }

        public void Clear()
        {
            foreach (FileSystemWatcher wtcCurWatcher in new List<FileSystemWatcher>(_FileWatchers))
            {
                wtcCurWatcher.Dispose();
                _FileWatchers.Remove(wtcCurWatcher);
            }
        }



        #region IDisposable Implementation
        private bool _bDisposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool bDisposing)
        {
            if (!_bDisposed)
            {
                if (bDisposing)
                {
                    if (_FileWatchers != null)
                    {
                        Clear();
                    }
                    _FileWatcherBackupTimer.Dispose();
                }
                _bDisposed = true;
            }
        }

        ~cMultiPathWatcher()
        {
            Dispose(false);
        }
        #endregion
    }
}
