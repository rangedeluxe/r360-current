﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents ghost document item data that has been parsed out of the XML data document.
    /// </summary>
    public class GhostDocumentItemData
    {
        public GhostDocumentItemData(long generatedBatchId, long generatedTransactionId, int transactionId,
            long generatedGhostDocumentId, int batchSequence, string fieldName, string fieldValue)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            TransactionId = transactionId;
            GeneratedGhostDocumentId = generatedGhostDocumentId;
            BatchSequence = batchSequence;
            FieldName = fieldName;
            FieldValue = fieldValue;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public int TransactionId { get; }
        public long GeneratedGhostDocumentId { get; }
        public int BatchSequence { get; }
        public string FieldName { get; }
        public string FieldValue { get; }
    }
}