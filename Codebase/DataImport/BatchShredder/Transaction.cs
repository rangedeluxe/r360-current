﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents a transaction that has been parsed out of the XML data document.
    /// </summary>
    public class Transaction
    {
        public Transaction(long generatedBatchId, long generatedTransactionId, int transactionId,
            int transactionSequence, string transactionHash, string transactionSignature, long? transactionHashChecker)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            TransactionId = transactionId;
            TransactionSequence = transactionSequence;
            TransactionHash = transactionHash;
            TransactionSignature = transactionSignature;
            TransactionHashChecker = transactionHashChecker;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public int TransactionId { get; }
        public int TransactionSequence { get; }
        public string TransactionHash { get; }
        public string TransactionSignature { get; }
        public long? TransactionHashChecker { get; }
    }
}