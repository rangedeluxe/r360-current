﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents a chunk of payment raw data that has been parsed out of the XML data document.
    /// </summary>
    public class PaymentRawData
    {
        public PaymentRawData(long generatedBatchId, long generatedTransactionId, int transactionId,
            long generatedPaymentId, int batchSequence, short rawDataPart, int rawDataSequence, string rawData)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            TransactionId = transactionId;
            GeneratedPaymentId = generatedPaymentId;
            BatchSequence = batchSequence;
            RawDataPart = rawDataPart;
            RawDataSequence = rawDataSequence;
            RawData = rawData;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public int TransactionId { get; }
        public long GeneratedPaymentId { get; }
        public int BatchSequence { get; }
        public short RawDataPart { get; }
        public int RawDataSequence { get; }
        public string RawData { get; }
    }
}