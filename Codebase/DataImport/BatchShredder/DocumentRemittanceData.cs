﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents document remittance data that has been parsed out of the XML data document.
    /// </summary>
    public class DocumentRemittanceData
    {
        public DocumentRemittanceData(long generatedBatchId, long generatedTransactionId, int transactionId,
            long generatedDocumentId, long generatedRemittanceDataRecordId, int batchSequence,
            string fieldName, string fieldValue)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            TransactionId = transactionId;
            GeneratedDocumentId = generatedDocumentId;
            GeneratedRemittanceDataRecordId = generatedRemittanceDataRecordId;
            BatchSequence = batchSequence;
            FieldName = fieldName;
            FieldValue = fieldValue;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public int TransactionId { get; }
        public long GeneratedDocumentId { get; }
        public long GeneratedRemittanceDataRecordId { get; }
        public int BatchSequence { get; }
        public string FieldName { get; }
        public string FieldValue { get; }
    }
}