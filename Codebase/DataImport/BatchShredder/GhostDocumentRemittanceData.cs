﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents ghost document remittance data that has been parsed out of the XML data document.
    /// </summary>
    public class GhostDocumentRemittanceData
    {
        public GhostDocumentRemittanceData(long generatedBatchId, long generatedTransactionId, int transactionId,
            long generatedGhostDocumentId, long generatedRemittanceDataRecordId, int batchSequence,
            string fieldName, string fieldValue)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            TransactionId = transactionId;
            GeneratedGhostDocumentId = generatedGhostDocumentId;
            GeneratedRemittanceDataRecordId = generatedRemittanceDataRecordId;
            BatchSequence = batchSequence;
            FieldName = fieldName;
            FieldValue = fieldValue;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public int TransactionId { get; }
        public long GeneratedGhostDocumentId { get; }
        public long GeneratedRemittanceDataRecordId { get; }
        public int BatchSequence { get; }
        public string FieldName { get; }
        public string FieldValue { get; }
    }
}