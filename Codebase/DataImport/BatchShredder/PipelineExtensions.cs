﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.DataImport
{
    public static class PipelineExtensions
    {
        public static string ToPipelineHash(this string hash)
        {
            return hash.ToPipelineString(40);
        }

        public static long? ToPipelineHashChecker(this string hash, bool isTransactionHash = false)
        {
            // Yes, we parse longer strings as hex and shorter strings as decimal. No, it doesn't make sense,
            // but it's how we calculated the hashes that are already in the database, so we're stuck with it.
            var hashChecker = hash.ToPipelineHash();

            if (string.IsNullOrEmpty(hashChecker))
                return null;

            if (hashChecker.Length > 10)
                return long.Parse(hashChecker.Substring(0, 10), NumberStyles.HexNumber);

            
            if (isTransactionHash)
                return long.Parse(hashChecker, NumberStyles.HexNumber); //The TransacionHash is parsed as hex, regardless of the length.
            else
                return long.Parse(hashChecker);
        }

        public static string ToPipelineBatchSource(this string batchSource)
        {
            return batchSource.ToPipelineString(30);
        }

        public static string ToPipelineFileSignature(this string fileSignature)
        {
            return fileSignature.ToPipelineString(55);
        }

        public static string ToPipelineTransactionSignature(this string transactionSignature)
        {
            return transactionSignature.ToPipelineString(65);
        }

        public static string ToPipelineABA(this string aba)
        {
            return aba.ToPipelineString(10);
        }

        public static string ToPipelineDDA(this string dda)
        {
            return dda.ToPipelineString(35);
        }

        public static string ToPipelineRemittanceFieldName(this string remittanceFieldName)
        {
            return remittanceFieldName.ToPipelineString(32);
        }

        public static string ToPipelineRemittanceFieldValue(this string remittanceFieldValue)
        {
            return remittanceFieldValue.ToPipelineString(256);
        }

        public static string ToPipelineTransactionCode(this string transactionCode)
        {
            return transactionCode.ToPipelineString(30);
        }

        public static string ToPipelineRemitterName(this string remitterName)
        {
            return remitterName.ToPipelineString(60);
        }

        public static string ToPipelineSerial(this string serial)
        {
            return serial.ToPipelineString(30);
        }

        public static string ToPipelineAccount(this string account)
        {
            return account.ToPipelineString(30);
        }

        public static string ToPipelineString(this string attribute, int maxCharacters)
        {
            if (string.IsNullOrEmpty(attribute))
                return string.Empty;

            return attribute.Length > maxCharacters ? attribute.Substring(0, maxCharacters) : attribute;
        }

        public static string ToPipelineRT(this string rt)
        {
            return rt.ToPipelineString(30);
        }

        public static string ToPipelineDocumentDescriptor(this string documentDescriptor)
        {
            return documentDescriptor.ToPipelineString(30);
        }

        public static string ToPipelinePaymentType(this string paymentType)
        {
            return paymentType.ToPipelineString(30);
        }

        public static long? ToPipelineSourceBatchId(this int batchId)
        {
            if (batchId == 0)
                return null;

            return batchId;
        }
        public static int? ToPipelineBatchNumber(this int batchNumber)
        {
            if (batchNumber == 0)
                return null;

            return batchNumber;
        }
    }
}
