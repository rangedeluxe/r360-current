﻿using System;

namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents an import error in the batch import.
    /// </summary>
    public class DataImportWorkBatchResponse
    {
        public DataImportWorkBatchResponse(long generatedBatchId, short responseStatus, string resultsMessage,
            Guid batchTrackingId)
        {
            GeneratedBatchId = generatedBatchId;
            ResponseStatus = responseStatus;
            ResultsMessage = resultsMessage;
            BatchTrackingId = batchTrackingId;
        }

        public long GeneratedBatchId { get; }
        public short ResponseStatus { get; }
        public string ResultsMessage { get; }
        public Guid BatchTrackingId { get; }
    }
}