﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.DataImport.JsonShredder
{
    public class BatchDataRecord
    {
        public IList<Field> fields { get; set; }
    }
}
