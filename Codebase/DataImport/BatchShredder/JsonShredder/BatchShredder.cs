﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.DataImport.JsonShredder
{
    public class BatchShredder
    {
        private readonly IShreddedBatchWriter _writer;
        private readonly long _dataImportQueueId;
        private readonly string _jsonData;

        private int _currentBatchSequence = 0;
        private Guid _currentBatchTrackingId = default(Guid);
        private int _currentTransactionId = 0;
        private int _currentDocumentId = 0;
        private int _currentPaymentId = 0;
        private int _currentRawDataSequence = 0;
        private int _currentDataRecordId = 0;
        private int _currentDocumentSequence = 0;
        private int _currentSequenceWithinTransaction = 0;

        public BatchShredder(string jsonData, long dataImportQueueId, IShreddedBatchWriter writer)
        {
            _dataImportQueueId = dataImportQueueId;
            _writer = writer;
            _jsonData = jsonData;
        }

        public void ProcessAll()
        {
            try
            {
                var batch = JsonConvert.DeserializeObject<CommonBatch>(_jsonData);
                
                ShredBatch(batch);
            }
            catch (Exception ex)
            {
                // If we don't have a batch tracking ID, just die a horrible death
                // and let SSIS mark all the batches as failed.
                if (_currentBatchTrackingId == default(Guid))
                    throw;

                _writer.WriteDataImportWorkBatchResponse(new DataImportWorkBatchResponse(
                    generatedBatchId: _dataImportQueueId,
                    responseStatus: 1,
                    resultsMessage: ex.Message,
                    batchTrackingId: _currentBatchTrackingId));
            }
            
        }

        private void ShredBatch(CommonBatch batch)
        {
            //write batch info to pipeline
            var pipelineData = new DataImport.Batch
            (
                generatedBatchId: _dataImportQueueId,
                depositDate: batch.DepositDate,
                batchDate: batch.BatchDate,
                immutableDate: batch.ProcessingDate,
                siteBankId: batch.BankId,
                siteClientAccountId: batch.ClientId,
                sourceBatchId: batch.BatchId.ToPipelineSourceBatchId(),
                batchNumber: batch.BatchNumber.ToPipelineBatchNumber(),
                batchSiteCode: batch.BatchSiteCode,
                batchSource: batch.BatchSource.ToPipelineBatchSource(),
                batchPaymentType: batch.PaymentType,
                batchCueId: batch.BatchCueID,
                batchTrackingId: batch.BatchTrackingID,
                aba: batch.ABA.ToPipelineABA(),
                dda: batch.DDA.ToPipelineDDA(),
                fileHash: batch.FileHash.ToPipelineHash(),
                fileSignature: batch.FileSignature.ToPipelineFileSignature(),
                fileHashChecker: batch.FileHash.ToPipelineHashChecker()
            );

            _writer.WriteBatch(pipelineData);
            _currentBatchTrackingId = pipelineData.BatchTrackingId;

            _currentTransactionId = 0;
            _currentDocumentId = 0;
            batch.Transactions?.ToList().ForEach(transaction =>
            {
                ++_currentTransactionId;
                ShredTransaction(transaction);
            });

            batch.BatchRecords?.ToList().ForEach(batchDataRecord =>
            {
                ShredBatchData(batchDataRecord);
            });
        }

        private void ShredBatchData(BatchDataRecord batchData)
        {
            batchData.fields?.ToList().ForEach(field =>
            {
                var pipeline = new DataImport.BatchData
                (
                    generatedBatchId: _dataImportQueueId,
                    fieldName: field.FieldName.ToPipelineRemittanceFieldName(),
                    fieldValue: field.FieldValue.ToPipelineRemittanceFieldValue()
                );

                _writer.WriteBatchData(pipeline);
            });
        }

        private void ShredTransaction(Transaction transaction)
        {
            _currentSequenceWithinTransaction = 0;//reset for each transaction

            //write pipeline data here
            var pipelineData = new DataImport.Transaction
            (
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: _currentTransactionId,
                transactionId: _currentTransactionId,  
                transactionSequence: _currentTransactionId, 
                transactionHash: transaction.TransactionHash.ToPipelineHash(), 
                transactionSignature: transaction.TransactionSignature.ToPipelineTransactionSignature(),
                transactionHashChecker: transaction.TransactionHash.ToPipelineHashChecker()
            );

            _writer.WriteTransaction(pipelineData);

            _currentPaymentId = 0;

            transaction.Payments?.ToList().ForEach(payment =>
            {
                ++_currentPaymentId;
                ShredPayment(payment);
            });

            _currentDocumentId = 0;
            transaction.Documents?.ToList().ForEach(document =>
            {
                ++_currentDocumentId;
                ShredDocument(document);
            });

            _currentDocumentId = 0;
            transaction.GhostDocuments?.ToList().ForEach(document =>
            {
                ++_currentDocumentId;
                ShredDocument(document);
            });
        }
        

        private void ShredPayment(Payment payment)
        {
            ++_currentBatchSequence;

            //write pipeline data here
            var pipelineData = new DataImport.Payment
            (
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: _currentTransactionId,
                generatedPaymentId: _currentPaymentId,
                transactionId: _currentTransactionId,
                transactionSequence: _currentTransactionId,
                batchSequence: _currentBatchSequence,
                amount: payment.Amount,
                rt: payment.RT.ToPipelineRT(),
                account: payment.Account.ToPipelineAccount(),
                serial: payment.Serial.ToPipelineSerial(),
                transactionCode: payment.TransactionCode.ToPipelineTransactionCode(),
                remitterName: payment.RemitterName.ToPipelineRemitterName(),
                aba: payment.ABA.ToPipelineABA(),
                dda: payment.DDA.ToPipelineDDA(),
                checkSequence: null
            );

            _writer.WritePayment(pipelineData);

            _currentRawDataSequence = 0;
            payment.RawData?.ToList().ForEach(item =>
            {
                ++_currentRawDataSequence;
                ShredRawData(item);
            });

            payment.RemittanceData?.ToList().ForEach(item =>
            {
                ShredPaymentRemittanceData(item);
            });
        }
        
        private void ShredDocument(GhostDocument document)
        {
            ++_currentBatchSequence;

            var pipelineData = new DataImport.GhostDocument
            (
               generatedBatchId: _dataImportQueueId,
               generatedTransactionId: _currentTransactionId,
               transactionId: _currentTransactionId,
               transactionSequence: _currentTransactionId,
               generatedGhostDocumentId: _currentDocumentId,
               batchSequence: _currentBatchSequence,
               isCorrespondence: document.IsCorrespondance
            );

            _writer.WriteGhostDocument(pipelineData);

            _currentDataRecordId = 0;
            document.GhostFields?.ToList().ForEach(field =>
            {
                ++_currentDataRecordId;
                ShredGhostRemittanceData(field);   
            });
        }


        private void ShredGhostRemittanceData(GhostField field)
        {
            ++_currentBatchSequence; 

            var pipelineData = new DataImport.GhostDocumentRemittanceData
            (
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: _currentTransactionId,
                transactionId: _currentTransactionId,
                generatedGhostDocumentId: _currentDocumentId,
                generatedRemittanceDataRecordId: _currentDataRecordId,
                batchSequence: _currentBatchSequence,
                fieldName: field.FieldName.ToPipelineRemittanceFieldName(),
                fieldValue: field.FieldValue.ToPipelineRemittanceFieldValue()
            );

            _writer.WriteGhostDocumentRemittanceData(pipelineData);
        }

        private void ShredDocument(Document document)
        {
            ++_currentBatchSequence;
            ++_currentDocumentSequence;
            ++_currentSequenceWithinTransaction;

            var documentPipeline = new DataImport.Document
            (
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: _currentTransactionId,
                transactionId: _currentTransactionId,
                transactionSequence: _currentTransactionId,
                generatedDocumentId: _currentDocumentId,
                batchSequence: _currentBatchSequence,
                documentSequence: _currentDocumentSequence,
                sequenceWithinTransaction: _currentSequenceWithinTransaction,
                documentDescriptor: document.DocumentDescriptor.ToPipelineDocumentDescriptor(),
                isCorrespondence: document.IsCorrespondence
            );
            
            _writer.WriteDocument(documentPipeline);

            document.DocumentRemittanceData?.ToList().ForEach(item =>
            {
                ShredRemittanceData(item);
            });
        }

        private void ShredPaymentRemittanceData(IList<Field> remittanceData)
        {
            remittanceData?.ToList().ForEach(item =>
            {
                ShredPaymentRemittanceData(item);
            });
        }

        private void ShredPaymentRemittanceData(Field field)
        {
            var pipeline = new DataImport.PaymentRemittanceData
            (
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: _currentTransactionId,
                transactionId: _currentTransactionId,
                generatedPaymentId: _currentPaymentId,
                batchSequence: _currentBatchSequence,
                fieldName: field.FieldName.ToPipelineRemittanceFieldName(),
                fieldValue: field.FieldValue.ToPipelineRemittanceFieldValue()
            );

            _writer.WritePaymentRemittanceData(pipeline);
        }

        private void ShredRemittanceData(IList<Field> remittanceData)
        {
            ++_currentBatchSequence;

            remittanceData?.ToList().ForEach(item =>
            {
                ShredRemittanceData(item);
            });
        }

        private void ShredRemittanceData(Field documentField)
        {
            
            var pipeline = new DataImport.DocumentRemittanceData
            (
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: _currentTransactionId,
                transactionId: _currentTransactionId,
                generatedDocumentId: _currentDocumentId,
                generatedRemittanceDataRecordId: _currentDataRecordId,
                batchSequence: _currentBatchSequence,
                fieldName: documentField.FieldName.ToPipelineRemittanceFieldName(),
                fieldValue: documentField.FieldValue.ToPipelineRemittanceFieldValue()
            );

            _writer.WriteDocumentRemittanceData(pipeline);
        }

        private void ShredRawData(RawData rawData)
        {
            if (rawData.Data.Length <= 128)
            {
                _writer.WritePaymentRawData(new PaymentRawData(
                    generatedBatchId: _dataImportQueueId,
                    generatedTransactionId: _currentTransactionId,
                    transactionId: _currentTransactionId,
                    generatedPaymentId: _currentPaymentId,
                    batchSequence: _currentBatchSequence,
                    rawDataPart: 0, // (short)rawData.Part,
                    rawDataSequence: _currentRawDataSequence,
                    rawData: rawData.Data));
            }
            else
            {
                var resultsMessage =
                    $"Raw data sequence {rawData.Data} is greater than 128 characters. " +
                    $"(Actual length is {rawData.Data.Length}.) Correct and resubmit.";

                _writer.WriteDataImportWorkBatchResponse(new DataImportWorkBatchResponse(
                    generatedBatchId: _dataImportQueueId,
                    responseStatus: 1,
                    resultsMessage: resultsMessage,
                    batchTrackingId: _currentBatchTrackingId));
            }
        }
    }
}
