﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.DataImport.JsonShredder
{
    public class Document
    {
        public int DocumentSequence { get; set; }
        public int SequenceWithinTransaction { get; set; }
        public string DocumentDescriptor { get; set; }
        public bool IsCorrespondence { get; set; }
        public IList<IList<Field>> DocumentRemittanceData { get; set; }
    }
}
