﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.DataImport.JsonShredder
{
    public class GhostDocument
    {
        public bool IsCorrespondance { get; set; }
        public IList<GhostField> GhostFields { get; set; }
    }
}
