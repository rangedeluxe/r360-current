﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.DataImport.JsonShredder
{
    public class Transaction
    {
        public IList<Payment> Payments { get; set; } = new List<Payment>();
        public IList<GhostDocument> GhostDocuments { get; set; }
        public IList<Document> Documents { get; set; }
        public string TransactionHash { get; set; }
        public string TransactionSignature { get; set; }

    }
}
