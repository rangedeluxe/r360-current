﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents batch data that has been parsed out of the XML data document.
    /// </summary>
    public class BatchData
    {
        public BatchData(long generatedBatchId, string fieldName, string fieldValue)
        {
            GeneratedBatchId = generatedBatchId;
            FieldName = fieldName;
            FieldValue = fieldValue;
        }

        public long GeneratedBatchId { get; }
        public string FieldName { get; }
        public string FieldValue { get; }
    }
}