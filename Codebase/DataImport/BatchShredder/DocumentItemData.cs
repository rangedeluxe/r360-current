﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents document item data that has been parsed out of the XML data document.
    /// </summary>
    public class DocumentItemData
    {
        public DocumentItemData(long generatedBatchId, long generatedTransactionId, int transactionId, int batchSequence,
            long generatedDocumentId, string fieldName, string fieldValue)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            TransactionId = transactionId;
            BatchSequence = batchSequence;
            GeneratedDocumentId = generatedDocumentId;
            FieldName = fieldName;
            FieldValue = fieldValue;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public int TransactionId { get; }
        public int BatchSequence { get; }
        public long GeneratedDocumentId { get; }
        public string FieldName { get; }
        public string FieldValue { get; }
    }
}