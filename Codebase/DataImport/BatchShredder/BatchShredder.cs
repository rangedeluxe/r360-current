﻿using System;
using System.Globalization;
using System.Xml;

namespace WFS.LTA.DataImport
{
    public class BatchShredder : BaseShredder
    {
        private readonly long _dataImportQueueId;
        private readonly IShreddedBatchWriter _writer;

        public BatchShredder(XmlReader xml, long dataImportQueueId, IShreddedBatchWriter writer)
            : base(xml)
        {
            _dataImportQueueId = dataImportQueueId;
            _writer = writer;
        }

        private int LastBatchSequence { get; set; }
        private Guid LastBatchTrackingId { get; set; }
        private long LastGeneratedTransactionId { get; set; }
        private int LastTransactionId { get; set; }
        private int LastTransactionSequence { get; set; }

        public void ProcessAll()
        {
            try
            {
                while (Xml.Read())
                    ProcessElement();
            }
            catch (Exception ex)
            {
                // If we don't have a batch tracking ID, just die a horrible death
                // and let SSIS mark all the batches as failed.
                if (LastBatchTrackingId == default(Guid))
                    throw;

                _writer.WriteDataImportWorkBatchResponse(new DataImportWorkBatchResponse(
                    generatedBatchId: _dataImportQueueId,
                    responseStatus: 1,
                    resultsMessage: ex.Message,
                    batchTrackingId: LastBatchTrackingId));
            }
        }
        private void ProcessElement()
        {
            if (!Xml.IsStartElement())
                return;

            switch (Xml.Name)
            {
                case "Batch":
                    ProcessBatch();
                    break;
                case "BatchData":
                    ProcessBatchData();
                    break;
                case "Document":
                    ProcessDocument();
                    break;
                case "DocumentItemData":
                    ProcessDocumentItemData();
                    break;
                case "DocumentRemittanceData":
                    ProcessDocumentRemittanceData();
                    break;
                case "GhostDocument":
                    ProcessGhostDocument();
                    break;
                case "GhostDocumentItemData":
                    ProcessGhostDocumentItemData();
                    break;
                case "GhostDocumentRemittanceData":
                    ProcessGhostDocumentRemittanceData();
                    break;
                case "Payment":
                    ProcessPayment();
                    break;
                case "PaymentItemData":
                    ProcessPaymentItemData();
                    break;
                case "PaymentRawData":
                    ProcessPaymentRawData();
                    break;
                case "PaymentRemittanceData":
                    ProcessPaymentRemittanceData();
                    break;
                case "Transaction":
                    ProcessTransaction();
                    break;
            }
        }
        private void ProcessBatch()
        {
            var batchIdInput = GetInt64("BatchID");
            var sourceBatchIdOutput = batchIdInput != 0
                ? batchIdInput
                : (long?) null;

            var batchNumberInput = GetInt32("BatchNumber");
            var batchNumberOutput = batchNumberInput != 0
                ? batchNumberInput
                : (int?) null;

            var batchTrackingId = GetGuid("BatchTrackingID");
            var fileHash = GetOptionalString("FileHash", 40);

            long? fileHashChecker = fileHash.ToPipelineHashChecker();

            _writer.WriteBatch(new Batch(
                generatedBatchId: _dataImportQueueId,
                depositDate: GetDate("DepositDate"),
                batchDate: GetDate("BatchDate"),
                immutableDate: GetDate("ProcessingDate"),
                siteBankId: GetInt32("BankID"),
                siteClientAccountId: GetInt32("ClientID"),
                sourceBatchId: sourceBatchIdOutput,
                batchNumber: batchNumberOutput,
                batchSiteCode: GetInt32("BatchSiteCode"),
                batchSource: GetString("BatchSource", 30),
                batchPaymentType: GetString("PaymentType", 30),
                batchCueId: GetInt32("BatchCueID"),
                batchTrackingId: batchTrackingId,
                aba: GetOptionalString("ABA", 10),
                dda: GetOptionalString("DDA", 35),
                fileHash: fileHash,
                fileSignature: GetOptionalString("FileSignature", 55),
                fileHashChecker: fileHashChecker));

            LastBatchTrackingId = batchTrackingId;
        }
        private void ProcessBatchData()
        {
            _writer.WriteBatchData(new BatchData(
                generatedBatchId: _dataImportQueueId,
                fieldName: GetString("FieldName", 32),
                fieldValue: GetString("FieldValue", 256)));
        }
        private void ProcessDocument()
        {
            var batchSequence = GetInt32("BatchSequence");

            _writer.WriteDocument(new Document(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: GetInt64("Transaction_Id"),
                transactionId: LastTransactionId,
                transactionSequence: LastTransactionSequence,
                generatedDocumentId: GetInt64("Document_Id"),
                batchSequence: batchSequence,
                documentSequence: GetInt32("DocumentSequence"),
                sequenceWithinTransaction: GetInt32("SequenceWithinTransaction"),
                documentDescriptor: GetString("DocumentDescriptor", 30),
                isCorrespondence: GetBoolean("IsCorrespondence")));

            LastBatchSequence = batchSequence;
        }
        private void ProcessDocumentItemData()
        {
            _writer.WriteDocumentItemData(new DocumentItemData(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: LastGeneratedTransactionId,
                transactionId: LastTransactionId,
                batchSequence: LastBatchSequence,
                generatedDocumentId: GetInt64("Document_Id"),
                fieldName: GetString("FieldName", 32),
                fieldValue: GetString("FieldValue", 256)));
        }
        private void ProcessDocumentRemittanceData()
        {
            _writer.WriteDocumentRemittanceData(new DocumentRemittanceData(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: LastGeneratedTransactionId,
                transactionId: LastTransactionId,
                generatedDocumentId: GetInt64("Document_Id"),
                generatedRemittanceDataRecordId: GetInt64("RemittanceDataRecord_Id"),
                batchSequence: GetInt32("BatchSequence"),
                fieldName: GetString("FieldName", 32),
                fieldValue: GetString("FieldValue", 256)));
        }
        private void ProcessGhostDocument()
        {
            _writer.WriteGhostDocument(new GhostDocument(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: GetInt64("Transaction_Id"),
                transactionId: LastTransactionId,
                transactionSequence: LastTransactionSequence,
                generatedGhostDocumentId: GetInt64("GhostDocument_Id"),
                batchSequence: GetInt32("BatchSequence"),
                isCorrespondence: GetBoolean("IsCorrespondence")));
        }
        private void ProcessGhostDocumentItemData()
        {
            _writer.WriteGhostDocumentItemData(new GhostDocumentItemData(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: LastGeneratedTransactionId,
                transactionId: LastTransactionId,
                generatedGhostDocumentId: GetInt64("GhostDocument_Id"),
                batchSequence: GetInt32("BatchSequence"),
                fieldName: GetString("FieldName", 32),
                fieldValue: GetString("FieldValue", 256)));
        }
        private void ProcessGhostDocumentRemittanceData()
        {
            _writer.WriteGhostDocumentRemittanceData(new GhostDocumentRemittanceData(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: LastGeneratedTransactionId,
                transactionId: LastTransactionId,
                generatedGhostDocumentId: GetInt64("GhostDocument_Id"),
                generatedRemittanceDataRecordId: GetInt64("RemittanceDataRecord_Id"),
                batchSequence: GetInt32("BatchSequence"),
                fieldName: GetString("FieldName", 32),
                fieldValue: GetString("FieldValue", 256)));
        }
        private void ProcessPayment()
        {
            var batchSequence = GetInt32("BatchSequence");

            _writer.WritePayment(new Payment(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: GetInt64("Transaction_Id"),
                generatedPaymentId: GetInt64("Payment_Id"),
                transactionId: LastTransactionId,
                transactionSequence: LastTransactionSequence,
                batchSequence: batchSequence,
                amount: GetDecimal("Amount"),
                rt: GetString("RT", 30),
                account: GetString("Account", 30),
                serial: GetString("Serial", 30),
                transactionCode: GetString("TransactionCode", 30),
                remitterName: GetString("RemitterName", 60),
                aba: GetString("ABA", 10),
                dda: GetString("DDA", 35),
                checkSequence: GetOptionalInt32("CheckSequence")));

            LastBatchSequence = batchSequence;
        }
        private void ProcessPaymentItemData()
        {
            _writer.WritePaymentItemData(new PaymentItemData(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: LastGeneratedTransactionId,
                transactionId: LastTransactionId,
                generatedPaymentId: GetInt64("Payment_Id"),
                batchSequence: LastBatchSequence,
                fieldName: GetString("FieldName", 32),
                fieldValue: GetString("FieldValue", 256)));
        }
        private void ProcessPaymentRawData()
        {
            var generatedPaymentId = GetInt64("Payment_Id");
            var rawDataPart = GetInt16("RawDataPart");
            var rawDataSequence = GetInt32("RawSequence");

            var innerText = "";
            if (!Xml.IsEmptyElement)
            {
                Xml.Read();
                innerText = Xml.Value;
            }

            if (innerText.Length <= 128)
            {
                _writer.WritePaymentRawData(new PaymentRawData(
                    generatedBatchId: _dataImportQueueId,
                    generatedTransactionId: LastGeneratedTransactionId,
                    transactionId: LastTransactionId,
                    generatedPaymentId: generatedPaymentId,
                    batchSequence: LastBatchSequence,
                    rawDataPart: rawDataPart,
                    rawDataSequence: rawDataSequence,
                    rawData: innerText));
            }
            else
            {
                var resultsMessage =
                    $"Raw data sequence {rawDataSequence} is greater than 128 characters. " +
                    $"(Actual length is {innerText.Length}.) Correct and resubmit.";

                _writer.WriteDataImportWorkBatchResponse(new DataImportWorkBatchResponse(
                    generatedBatchId: _dataImportQueueId,
                    responseStatus: 1,
                    resultsMessage: resultsMessage,
                    batchTrackingId: LastBatchTrackingId));
            }
        }
        private void ProcessPaymentRemittanceData()
        {
            _writer.WritePaymentRemittanceData(new PaymentRemittanceData(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: LastGeneratedTransactionId,
                transactionId: LastTransactionId,
                generatedPaymentId: GetInt64("Payment_Id"),
                batchSequence: LastBatchSequence,
                fieldName: GetString("FieldName", 32),
                fieldValue: GetString("FieldValue", 256)));
        }
        private void ProcessTransaction()
        {
            var generatedTransactionId = GetInt64("Transaction_Id");
            var transactionId = GetInt32("TransactionID");
            var transactionSequence = GetInt32("TransactionSequence");
            var transactionHash = GetOptionalString("TransactionHash", 40);

            long? transactionHashChecker = transactionHash.ToPipelineHashChecker(true);

            _writer.WriteTransaction(new Transaction(
                generatedBatchId: _dataImportQueueId,
                generatedTransactionId: generatedTransactionId,
                transactionId: transactionId,
                transactionSequence: transactionSequence,
                transactionHash: transactionHash,
                transactionSignature: GetOptionalString("TransactionSignature", 65),
                transactionHashChecker: transactionHashChecker));

            LastGeneratedTransactionId = generatedTransactionId;
            LastTransactionId = transactionId;
            LastTransactionSequence = transactionSequence;
        }
    }
}