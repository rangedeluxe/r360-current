﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents payment item data that has been parsed out of the XML data document.
    /// </summary>
    public class PaymentItemData
    {
        public PaymentItemData(long generatedBatchId, long generatedTransactionId, int transactionId,
            long generatedPaymentId, int batchSequence, string fieldName, string fieldValue)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            TransactionId = transactionId;
            GeneratedPaymentId = generatedPaymentId;
            BatchSequence = batchSequence;
            FieldName = fieldName;
            FieldValue = fieldValue;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public int TransactionId { get; }
        public long GeneratedPaymentId { get; }
        public int BatchSequence { get; }
        public string FieldName { get; }
        public string FieldValue { get; }
    }
}