﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents a payment that has been parsed out of the XML data document.
    /// </summary>
    public class Payment
    {
        public Payment(long generatedBatchId, long generatedTransactionId, long generatedPaymentId, int transactionId,
            int transactionSequence, int batchSequence, decimal amount, string rt, string account, string serial,
            string transactionCode, string remitterName, string aba, string dda, int? checkSequence)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            GeneratedPaymentId = generatedPaymentId;
            TransactionId = transactionId;
            TransactionSequence = transactionSequence;
            BatchSequence = batchSequence;
            Amount = amount;
            Rt = rt;
            Account = account;
            Serial = serial;
            TransactionCode = transactionCode;
            RemitterName = remitterName;
            Aba = aba;
            Dda = dda;
            CheckSequence = checkSequence;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public long GeneratedPaymentId { get; }
        public int TransactionId { get; }
        public int TransactionSequence { get; }
        public int BatchSequence { get; }
        public decimal Amount { get; }
        public string Rt { get; }
        public string Account { get; }
        public string Serial { get; }
        public string TransactionCode { get; }
        public string RemitterName { get; }
        public string Aba { get; }
        public string Dda { get; }
        public int? CheckSequence { get; }
    }
}