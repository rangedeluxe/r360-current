﻿namespace WFS.LTA.DataImport
{
    public interface IShreddedBatchWriter
    {
        void WriteBatch(Batch batch);
        void WriteBatchData(BatchData batchData);
        void WriteDataImportWorkBatchResponse(DataImportWorkBatchResponse response);
        void WriteDocument(Document document);
        void WriteDocumentItemData(DocumentItemData documentItemData);
        void WriteDocumentRemittanceData(DocumentRemittanceData documentRemittanceData);
        void WriteGhostDocument(GhostDocument ghostDocument);
        void WriteGhostDocumentItemData(GhostDocumentItemData ghostDocumentItemData);
        void WriteGhostDocumentRemittanceData(GhostDocumentRemittanceData ghostDocumentRemittanceData);
        void WritePayment(Payment payment);
        void WritePaymentItemData(PaymentItemData paymentItemData);
        void WritePaymentRawData(PaymentRawData paymentRawData);
        void WritePaymentRemittanceData(PaymentRemittanceData paymentRemittanceData);
        void WriteTransaction(Transaction transaction);
    }
}