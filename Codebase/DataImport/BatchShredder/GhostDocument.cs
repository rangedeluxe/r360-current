﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents a ghost document that has been parsed out of the XML data document.
    /// </summary>
    public class GhostDocument
    {
        public GhostDocument(long generatedBatchId, long generatedTransactionId, int transactionId,
            int transactionSequence, long generatedGhostDocumentId, int batchSequence, bool isCorrespondence)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            TransactionId = transactionId;
            TransactionSequence = transactionSequence;
            GeneratedGhostDocumentId = generatedGhostDocumentId;
            BatchSequence = batchSequence;
            IsCorrespondence = isCorrespondence;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public int TransactionId { get; }
        public int TransactionSequence { get; }
        public long GeneratedGhostDocumentId { get; }
        public int BatchSequence { get; }
        public bool IsCorrespondence { get; }
    }
}