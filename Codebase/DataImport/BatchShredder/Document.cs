﻿namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents a document that has been parsed out of the XML data document.
    /// </summary>
    public class Document
    {
        public Document(long generatedBatchId, long generatedTransactionId, int transactionId, int transactionSequence,
            long generatedDocumentId, int batchSequence, int documentSequence, int sequenceWithinTransaction,
            string documentDescriptor, bool isCorrespondence)
        {
            GeneratedBatchId = generatedBatchId;
            GeneratedTransactionId = generatedTransactionId;
            TransactionId = transactionId;
            TransactionSequence = transactionSequence;
            GeneratedDocumentId = generatedDocumentId;
            BatchSequence = batchSequence;
            DocumentSequence = documentSequence;
            SequenceWithinTransaction = sequenceWithinTransaction;
            DocumentDescriptor = documentDescriptor;
            IsCorrespondence = isCorrespondence;
        }

        public long GeneratedBatchId { get; }
        public long GeneratedTransactionId { get; }
        public int TransactionId { get; }
        public int TransactionSequence { get; }
        public long GeneratedDocumentId { get; }
        public int BatchSequence { get; }
        public int DocumentSequence { get; }
        public int SequenceWithinTransaction { get; }
        public string DocumentDescriptor { get; }
        public bool IsCorrespondence { get; }
    }
}