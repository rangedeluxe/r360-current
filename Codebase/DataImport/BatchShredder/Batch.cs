﻿using System;

namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents a batch that has been parsed out of the XML data document. Includes
    /// generated fields like <see cref="FileHashChecker"/>.
    /// </summary>
    public class Batch
    {
        public Batch(long generatedBatchId, DateTime depositDate, DateTime batchDate, DateTime immutableDate,
            int siteBankId, int siteClientAccountId, long? sourceBatchId, int? batchNumber,
            int batchSiteCode, string batchSource, string batchPaymentType, int batchCueId, Guid batchTrackingId,
            string aba, string dda, string fileHash, string fileSignature, long? fileHashChecker)
        {
            GeneratedBatchId = generatedBatchId;
            DepositDate = depositDate;
            BatchDate = batchDate;
            ImmutableDate = immutableDate;
            SiteBankId = siteBankId;
            SiteClientAccountId = siteClientAccountId;
            SourceBatchId = sourceBatchId;
            BatchNumber = batchNumber;
            BatchSiteCode = batchSiteCode;
            BatchSource = batchSource;
            BatchPaymentType = batchPaymentType;
            BatchCueId = batchCueId;
            BatchTrackingId = batchTrackingId;
            Aba = aba;
            Dda = dda;
            FileHash = fileHash;
            FileSignature = fileSignature;
            FileHashChecker = fileHashChecker;
        }

        public long GeneratedBatchId { get; }
        public DateTime DepositDate { get; }
        public DateTime BatchDate { get; }
        public DateTime ImmutableDate { get; }
        public int SiteBankId { get; }
        public int SiteClientAccountId { get; }
        public long? SourceBatchId { get; }
        public int? BatchNumber { get; }
        public int BatchSiteCode { get; }
        public string BatchSource { get; }
        public string BatchPaymentType { get; }
        public int BatchCueId { get; }
        public Guid BatchTrackingId { get; }
        public string Aba { get; }
        public string Dda { get; }
        public string FileHash { get; }
        public string FileSignature { get; }
        public long? FileHashChecker { get; }
    }
}