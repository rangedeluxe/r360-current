﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WFS.LTA.DataImport.DataImportServiceClient
{
    public class SessionManager
    {
        private readonly Action<Guid> _endSession;
        private readonly Func<Guid> _getSessionId;
        private readonly Func<DateTime> _getTime;

        private DateTime _expiredSessionEndTime;
        private List<Guid> _expiredSessions = new List<Guid>();
        private readonly object _lock = new object();
        private Guid _sessionId;
        private DateTime _sessionIdTimeIssued;

        public SessionManager(Func<DateTime> getTime, Func<Guid> getSessionId, Action<Guid> endSession)
        {
            _getTime = getTime;
            _getSessionId = getSessionId;
            _endSession = endSession;
        }

        private void AddSessionToExpiredList(DateTime now)
        {
            if (_sessionId != Guid.Empty)
            {
                _expiredSessions.Add(_sessionId);
                _expiredSessionEndTime = now + TimeSpan.FromMinutes(10);
            }
        }
        public void Clear()
        {
            var now = _getTime();
            lock (_lock)
            {
                AddSessionToExpiredList(now);
                _sessionId = Guid.Empty;
                _sessionIdTimeIssued = DateTime.MinValue;
            }
        }
        public void Close()
        {
            var sessionIds = new List<Guid>();
            lock (_lock)
            {
                if (_sessionId != Guid.Empty)
                    sessionIds.Add(_sessionId);
                _sessionId = Guid.Empty;
                _sessionIdTimeIssued = DateTime.MinValue;

                sessionIds.AddRange(_expiredSessions);
                _expiredSessions.Clear();
            }

            foreach (var sessionId in sessionIds)
            {
                _endSession(sessionId);
            }
        }
        private void EndOldSessions(DateTime now)
        {
            IList<Guid> sessionsToEnd = null;
            lock (_lock)
            {
                if (_expiredSessions.Any() && now >= _expiredSessionEndTime)
                {
                    sessionsToEnd = _expiredSessions;
                    _expiredSessions = new List<Guid>();
                }
            }
            if (sessionsToEnd != null)
            {
                foreach (var sessionId in sessionsToEnd)
                {
                    _endSession(sessionId);
                }
            }
        }
        public Guid GetSessionId()
        {
            var now = _getTime();
            EndOldSessions(now);

            lock (_lock)
            {
                if (_sessionIdTimeIssued.Date == now.Date)
                    return _sessionId;
            }

            var newSessionId = _getSessionId();

            lock (_lock)
            {
                AddSessionToExpiredList(now);
                _sessionId = newSessionId;
                _sessionIdTimeIssued = now;
            }
            return newSessionId;
        }
    }
}