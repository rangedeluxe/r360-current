﻿using Wfs.Logging;

namespace WFS.LTA.DataImport.DataImportServiceClient
{
    public class ConnectionContext
    {
        public string SiteKey { get; set; }
        public string Entity { get; set; }
        public string ServerNameLocation { get; set; }
        public IWfsLog Logger { get; set; }
        public string LogonName { get; set; }
        public string Password { get; set; }
    }
}
