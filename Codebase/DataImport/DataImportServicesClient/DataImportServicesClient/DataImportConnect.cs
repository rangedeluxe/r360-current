﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Data;
using System.Linq;
using WFS.LTA.Common;
using WFS.LTA.DataImport.DataImportWCFLib;
using System.Runtime.CompilerServices;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.RecHub.ImportToolkit.Common.Interfaces;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date:
*
* Purpose:
*
* Modification History
* CR 33230 WJS 2/10/2012
*           - Initial Revision -
* CR 52633 WJS 5/17/2012
 *          - Add SetSession and EndSession
* CR 52633 WJS 5/18/2012
 *          - Add GetSession with no out Guid parameter
* CR 53547 WJS 8/2/2012
 *          - Add nice message on over max message size
* CR 56501 WJS 10/18/12
 *          - Add Method called GetHylandResponses
* WI 155269 CMC 07/29/2014
 *          - Handle single session
******************************************************************************/

namespace WFS.LTA.DataImport.DataImportServiceClient
{
    public class DataImportConnect : _ServicesClientBase, IAlertable, IBatchImageSender
    {
        private readonly WSHttpBinding _Binding = new WS2007HttpBinding("DataImportServiceConfig");
        private readonly IDataImportService _DataImportService = null;
        private const long MAXBUFFERSIZE = 32000000;

        public DataImportConnect(ConnectionContext connectionContext)
        {
            if (connectionContext == null)
                throw new ArgumentNullException("connectionContext");

            base.SiteKey = connectionContext.SiteKey;
            base.EventLog = connectionContext.Logger;
            Entity = connectionContext.Entity;
            LogonName = connectionContext.LogonName;
            Password = connectionContext.Password;

            var endpointAddress = new EndpointAddress(connectionContext.ServerNameLocation);
            _DataImportService = new ChannelFactory<IDataImportService>(_Binding, endpointAddress).CreateChannel();
            SessionManager = new SessionManager(
                () => DateTime.Now,
                () => NewSessionId(),
                guid => EndSession(guid));
        }

        private string Entity { get; set; }
        private string LogonName { get; set; }
        private string Password { get; set; }
        private SessionManager SessionManager { get; set; }

        public string Ping()
        {

            try
            {
                return _DataImportService.Ping();
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "Ping");
                throw;
            }
        }

        public bool SendClientSetup( string docXml, int auditDateKey, out string respXml)
        {

            bool bolRetVal;
            var xmlResponse = string.Empty;

            bolRetVal = Execute<bool>(operation => { return operation.SendClientSetup(docXml, auditDateKey, out xmlResponse); });
            respXml = xmlResponse;

            return bolRetVal;
        }

        public bool SendBatchData(string batchDataXml, int auditDateKey, out string respXml)
        {

            bool bolRetVal;
            var xmlResponse = string.Empty;

            bolRetVal = Execute<bool>(operation => 
            {
                return operation.SendBatchData(batchDataXml,auditDateKey, out xmlResponse);
            });
            respXml = xmlResponse;

            return bolRetVal;
        }

        public bool SendBatchMeasureData(BatchFileImportMeasures batchFileImportMeasures)
        {
            return Execute<bool>(operation => { return operation.SendBatchMeasureData(batchFileImportMeasures); }); 
        }

        public bool GetSchemaDefinition(string type, string XSDVersion,  out string dataXSD, out string respXml) {

            bool bolRetVal = false;
            var xmlResponse = string.Empty;
            var xsd = string.Empty;

            bolRetVal = Execute<bool>(operation => { return operation.GetSchemaDefinition(type, XSDVersion, out xsd, out xmlResponse); });
            respXml = xmlResponse;
            dataXSD = xsd;

            return bolRetVal;
        }

        public void SendBatchImages(IList<UploadRequest> items)
        {
            Execute<object>(dataImportService =>
            {
                var totalBytes = items.Sum(item => item.ImageData.Length);
                if (totalBytes > MAXBUFFERSIZE)
                {
                    throw new InvalidOperationException("The max size of an image you can send is 32MB");
                }

                dataImportService.SendBatchImages(items);
                return null;
            });
        }
        
        private Guid NewSessionId()
        {
            var sessionId = Guid.Empty;
            var success = Execute(
                operation => operation.GetSession(LogonName, Password, out sessionId),
                sessionId: Guid.Empty);
            if (!success)
                throw new Exception("Unable to establish a session with the Data Import Service");
            return sessionId;
        }
        public void LogOn()
        {
            SessionManager.GetSessionId();
        }
        public void EndSession()
        {
            SessionManager.Close();
        }
        private void EndSession(Guid guid)
        {
            Execute<bool>(operation => operation.EndSession(), sessionId: guid);
        }

        public ClientSetupResponses GetClientSetupResponses(string clientProcessCode)
        {
            return Execute(operation =>
                operation.GetClientSetupResponses(clientProcessCode));
        }

        public DataSet GetBatchDataResponses(string clientProcessCode, out bool success)
        {

            DataSet dsBatchSetup = null;
            success = false;
            var isSuccessful = false;

            dsBatchSetup = Execute<DataSet>(operation => { return operation.GetBatchDataResponses(clientProcessCode, out isSuccessful); });
            success = isSuccessful;
            return dsBatchSetup;
        }

        public DataSet GetDocumentTypes(DateTime? loadDate, out bool success)
        {

            DataSet dsDocumentTypes = null;
            var isSuccessful = false;

            dsDocumentTypes = Execute<DataSet>(operation => { return operation.GetDocumentTypes(loadDate, out isSuccessful); });
            success = isSuccessful;
            return dsDocumentTypes;
        }

        public DataSet GetImageRPSAliasMappings(Int32 siteBankID, Int32 siteLockboxId, DateTime? modificationDate, out bool success)
        {

            DataSet dsImageRPSAliasMapping = null;
            success = false;
            var isSuccessful = false;

            dsImageRPSAliasMapping = Execute<DataSet>(operation => { return operation.GetImageRPSAliasMappings(siteBankID, siteLockboxId, modificationDate, out isSuccessful); });
            success = isSuccessful;
            return dsImageRPSAliasMapping;
        }

        public DataSet GetBatchDataSetupField(string batchSourceName, DateTime? modificationDate, out bool success)
        {

            DataSet dsBatchDataSetup = null;
            success = false;
            var isSuccessful = false;

            dsBatchDataSetup = Execute<DataSet>(operation => { return operation.GetBatchDataSetupField(batchSourceName, modificationDate, out isSuccessful); });
            success = isSuccessful;

            return dsBatchDataSetup;
        }

        public DataSet GetItemDataSetupField(string batchSourceName, DateTime? modificationDate, out bool success)
        {

            DataSet dsItemDataSetup = null;


            success = false;
            var isSuccessful = false;

            dsItemDataSetup = Execute<DataSet>(operation => { return operation.GetItemDataSetupField(batchSourceName, modificationDate, out isSuccessful); });
            success = isSuccessful;

            return dsItemDataSetup;
        }

        public AlertResponse Log(AlertRequest requestContext)
        {
            return Execute<AlertResponse>(operation => { return operation.Log(requestContext); });
        }

        public AuditResponse LogAuditEvent(AuditRequest requestContext)
        {
            return Execute<AuditResponse>(operation => { return operation.LogAuditEvent(requestContext); });
        }

        public WorkgroupResponse GetWorkgroupInfo(WorkgroupRequest requestContext)
        {
            return Execute<WorkgroupResponse>(operation => { return operation.GetWorkgroupInfo(requestContext); });
        }

        public bool UpdateStatus(UpdateStatusContext context)
        {

            return Execute<bool>(operation => { return operation.UpdateStatus(context); });
        }

        public MultiImageResponse MultiImageUpload(MultiImageRequest request)
        {
            return Execute<MultiImageResponse>(opertion => { return opertion.MultiImageUpload(request); });
        }

        private T Execute<T>(Func<IDataImportService, T> operation, [CallerMemberName] string procName = null,
            Guid? sessionId = null) where T : new()
        {
            try
            {
                return new OperationContextScope((IContextChannel)_DataImportService).Use(service =>
                    {
                        // The DataImportServiceContext is set and will exist for the duration of the operationscope
                        DataImportServiceContext.Current = new DataImportServiceContext(
                            this.SiteKey, (sessionId ?? SessionManager.GetSessionId()).ToString(), Entity);
                        return operation(_DataImportService);
                    });
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, this.GetType().Name, procName);
                if (string.Equals(ex.Message, "session has expired.", StringComparison.InvariantCultureIgnoreCase))
                {
                    SessionManager.Clear();
                    EventLog.logEvent("Going to reestablish a session for the client", procName, LTAMessageType.Information, LTAMessageImportance.Essential);
                }

                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, procName);
                throw (new Exception(ex.Message, ex));
            }

        }
    }
}
