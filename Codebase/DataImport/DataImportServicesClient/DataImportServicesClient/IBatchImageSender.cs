﻿using System.Collections.Generic;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

namespace WFS.LTA.DataImport.DataImportServiceClient
{
    public interface IBatchImageSender
    {
        void SendBatchImages(IList<UploadRequest> items);
    }
}