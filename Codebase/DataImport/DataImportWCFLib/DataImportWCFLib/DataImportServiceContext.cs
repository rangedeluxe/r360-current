using System.Runtime.Serialization;
using System.ServiceModel;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     07/25/2012
*
* Purpose:  
*
* Modification History
* CR 54513 JMC 07/25/2012
*   -New File used for Data Import Toolkit
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportWCFLib {

  
    [DataContract(Namespace="urn:spike.DataImportServices:v1", Name="DataImportServiceContext")]
    public class DataImportServiceContext {

        /// <summary>
        /// Unique name of object stored in headers
        /// </summary>
        internal const string HeaderServiceContext = "DataImportServiceContext";

        /// <summary>
        /// Unique name of object stored in headers
        /// </summary>
        internal const string HeaderSiteKey = "SiteKey";
        internal const string HeaderSessionID = "SessionID";
        internal const string HeaderEntity = "Entity";

        /// <summary>
        /// namespace of object stored in headers
        /// </summary>
        internal const string HeaderNamespace = "urn:spike.DataImportServices:v1";

        private string _SiteKey = string.Empty;
        private string _SessionID = string.Empty;

        public DataImportServiceContext() { 
        }

        public DataImportServiceContext(string siteKey, string sessionId, string entity)
            : this(siteKey, sessionId)
        {
            Entity = entity;
        }

        public DataImportServiceContext(string vSiteKey, string vSessionID) {
            _SiteKey = vSiteKey;
            _SessionID = vSessionID;
        }

        public DataImportServiceContext(string vSiteKey) {
            _SiteKey = vSiteKey;
            _SessionID = string.Empty;
        }


        [DataMember(IsRequired = true, Name = "SiteKey")]
        public string SiteKey {   
            get { return _SiteKey; }
            set { _SiteKey = value; }
        }

        [DataMember(IsRequired = true, Name = "SessionID")]
        public string SessionID {   
            get { return _SessionID; }
            set { _SessionID = value; }
        }

        [DataMember(IsRequired = true, Name = "Entity")] 
        public string Entity
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the current context a service call is running on
        /// </summary>
        /// <remarks>This is set on the client and retrieved on the host</remarks>
        /// <value>The current.</value>
        public static DataImportServiceContext Current {

            set {
                MessageHeader<DataImportServiceContext> header = new MessageHeader<DataImportServiceContext>(value);
                OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader(DataImportServiceContext.HeaderServiceContext, DataImportServiceContext.HeaderNamespace));
            } 
            
            get {

                string strSiteKey;
                string strSessionID;

                int intIndexOfHeader;

                // determine if servicecontext was sent in headers
                intIndexOfHeader = OperationContext.Current.IncomingMessageHeaders.FindHeader(HeaderServiceContext, HeaderNamespace);

                if (intIndexOfHeader == -1) {

                    // determine if the name of the source application was sent in the headers
                    intIndexOfHeader = OperationContext.Current.IncomingMessageHeaders.FindHeader(HeaderSiteKey, HeaderNamespace);
                    
                    if(intIndexOfHeader==-1) {
                        ////throw new ApplicationException("Site Key not found!"); 
                        strSiteKey = string.Empty;
                    } else {
                        strSiteKey = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(HeaderSiteKey, HeaderNamespace);
                    }

                    // determine if the name of the source application was sent in the headers
                    intIndexOfHeader = OperationContext.Current.IncomingMessageHeaders.FindHeader(HeaderSessionID, HeaderNamespace);
                    
                    if(intIndexOfHeader==-1) {
                        ////throw new ApplicationException("SessionID not found!"); 
                        strSessionID = string.Empty;
                    } else {
                        strSessionID = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(HeaderSessionID, HeaderNamespace);
                    }

                    return new DataImportServiceContext(strSiteKey, strSessionID);
                }

                // return the sent servicecontext
                return OperationContext.Current.IncomingMessageHeaders.GetHeader<DataImportServiceContext>(HeaderServiceContext, HeaderNamespace);                
            }
        }
    }
}
