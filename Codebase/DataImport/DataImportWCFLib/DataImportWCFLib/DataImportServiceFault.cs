﻿using System;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Runtime.Serialization;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     07/25/2012
*
* Purpose:  
*
* Modification History
* CR 54513 JMC 07/25/2012
*   -New File used for Data Import Toolkit
* CR 54513 WJS 7/25/2012
 *  - Change to make it DataImportServiceFault
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportWCFLib {

    /// <summary>
    /// Common information for faults from WFS application WCF services.
    /// </summary>
    /// <remarks>
    /// This class defines a custom SOAP fault base type to be used by WFS services. 
    /// It is used in services in conjunction with the <see cref="FaultException{TDetail}"/> 
    /// class to pass safe information about exceptions to a client. Derive from this base 
    /// class to create specific fault messages.
    /// </remarks>
    /// <example>
    /// The following code shows how to declare a method on a service that 
    /// uses this class.
    /// 
    /// <code>
    /// [ServiceContract]
    /// public interface IExampleService
    /// {
    ///     [OperationContract]
    ///     [FaultContract(typeof(ServiceFault))]
    ///     void TestMethod();
    /// }
    /// </code>
    /// </example>
    /// <seealso cref="Wfs.ServiceModel.Service.ExceptionShield"/>
    [DataContract(Namespace="http://Wfs.ServiceModel", Name="DataImportServiceFault")]
    public class DataImportServiceFault {

        private int _Category = 0;
        private int _ErrorCode = 0;
        private string _Source = null;
        private string _Message = null;
        private ExceptionDetail _Detail = null;

        /// <summary>
        /// Default constructor initializes a new instance with no data set
        /// </summary>
        public DataImportServiceFault() {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceFault"/> class 
        /// without error code and source information.
        /// </summary>
        /// <param name="message">
        /// The human readable description of the exception.  Ensure that 
        /// this does not include any information that might be useful to an 
        /// attacker.
        /// </param>
        /// <remarks>
        /// Use this constructor for errors that do not have a specific error code 
        /// and source associated with them (typically unknown exceptions).
        /// </remarks>
        public DataImportServiceFault(string message) : this(0, 0, null, message) {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceFault"/> class 
        /// without error code information.
        /// </summary>
        /// <param name="source">Source application or component</param>
        /// <param name="message">
        /// The human readable description of the exception.  Ensure that 
        /// this does not include any information that might be useful to an 
        /// attacker.
        /// </param>
        /// <remarks>
        /// Use this constructor for errors that do not have a specific error code 
        /// associated with them (typically unknown exceptions).
        /// </remarks>
        public DataImportServiceFault(string source, string message) : this(0, 0, source, message) {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceFault"/> class 
        /// </summary>
        /// <param name="category">Error or event application defined category</param>
        /// <param name="errorCode">Application error code</param>
        /// <param name="source">Source application or component</param>
        /// <param name="message">
        /// The human readable description of the error.  Ensure that 
        /// this does not include any information that might be useful to an 
        /// attacker.
        /// </param>
        public DataImportServiceFault(int category, int errorCode, string source, string message) {
            Category = category;
            ErrorCode = errorCode;
            Source = source;
            Message = message;
        }

        /// <summary>
        /// Gets or sets the error category of the exception.
        /// </summary>
        /// <value>The category of the exception.</value>
        [DataMember]
        public int Category {

            get { return _Category; }
            set  {
                if (value < 0) {
                    throw new ArgumentOutOfRangeException("Category");
                }
                _Category = value;
            }
        }

        /// <summary>
        /// Gets or sets the error code of the exception.
        /// </summary>
        /// <value>The error code of the exception.</value>
        [DataMember]
        public int ErrorCode {

            get { 
                return _ErrorCode;
            }
            set {
                if (value < 0) {
                    throw new ArgumentOutOfRangeException("ErrorCode");
                }
                _ErrorCode = value; 
            }
        }

        /// <summary>
        /// Gets or sets the source application or component property
        /// </summary>
        /// <value>source string</value>
        [DataMember]
        public string Source {
            get { 
                return _Source; 
            }
            set {
                _Source = value;
            }
        }

        /// <summary>
        /// Gets or sets the human readable description of the exception.
        /// </summary>
        /// <value>A message string.</value>
        /// <remarks>
        /// This value can also be used as the fault reason for this fault.
        /// It is the caller's responsibility to ensure that the value of this
        /// property does not contain any confidential or security related 
        /// information.
        /// </remarks>
        [DataMember]
        public string Message {
            get { 
                return _Message;
            }
            set {
                if (String.IsNullOrEmpty(value)) {
                    throw new ArgumentNullException("Message");
                }
                _Message = value;
            }
        }

        /// <summary>
        /// Get or sets the detail exception data associated with this fault
        /// </summary>
        /// <remarks>
        /// This is used when show exception details in faults is enabled to support both
        /// getting exception detail information and keeping the typed faults exception intact
        /// at the same time.
        /// </remarks>
        [DataMember]
        public ExceptionDetail ExceptionDetail {
            get { return _Detail; }
            set { _Detail = value; }
        }

        /// <summary>
        /// Returns a string that represents this <see cref="ServiceFault"/>.
        /// When the exception detail property is set the formatted string 
        /// will include the detail information.
        /// </summary>
        /// <returns>
        /// A formatted string representing the fault.
        /// </returns>
        public override string ToString() {

            StringBuilder sb = new StringBuilder();

            sb.Append("Data Import Service Fault: ");

            if (_Category != 0 || _ErrorCode != 0) {
                sb.AppendFormat("Error={0}-{1} ", _Category, _ErrorCode);
            }

            if (!String.IsNullOrEmpty(_Source)) {
                sb.AppendFormat("Source={0} ", _Source);
            }

            sb.Append(_Message);

            if (_Detail != null) {
                sb.AppendLine();
                sb.Append(_Detail.ToString());
            }

            return sb.ToString();
        }
    }
}
