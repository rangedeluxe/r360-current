﻿using System;

namespace WFS.LTA.DataImport
{
    /// <summary>
    /// Represents an import error in the client-setup import.
    /// </summary>
    public class DataImportWorkClientSetupResponse
    {
        public DataImportWorkClientSetupResponse(Guid clientTrackingId, sbyte responseStatus, string resultsMessage)
        {
            ClientTrackingId = clientTrackingId;
            ResponseStatus = responseStatus;
            ResultsMessage = resultsMessage;
        }

        public Guid ClientTrackingId { get; }
        public sbyte ResponseStatus { get; }
        public string ResultsMessage { get; }
    }
}