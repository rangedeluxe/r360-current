﻿namespace WFS.LTA.DataImport
{
    public class Bank
    {
        public Bank(long generatedClientGroupId, int bankId, string bankName, string aba)
        {
            GeneratedClientGroupId = generatedClientGroupId;
            BankId = bankId;
            BankName = bankName;
            Aba = aba;
        }

        public long GeneratedClientGroupId { get; }
        public int BankId { get; }
        public string BankName { get; }
        public string Aba { get; }
    }
}