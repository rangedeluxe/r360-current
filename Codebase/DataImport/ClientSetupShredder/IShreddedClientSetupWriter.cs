﻿namespace WFS.LTA.DataImport
{
    public interface IShreddedClientSetupWriter
    {
        void WriteBank(Bank bank);
        void WriteDataImportWorkClientSetupResponse(DataImportWorkClientSetupResponse response);
    }
}