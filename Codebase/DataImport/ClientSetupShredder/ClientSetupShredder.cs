﻿using System;
using System.Xml;

namespace WFS.LTA.DataImport
{
    public class ClientSetupShredder : BaseShredder
    {
        private readonly long _dataImportQueueId;
        private readonly IShreddedClientSetupWriter _writer;

        public ClientSetupShredder(XmlReader xml, long dataImportQueueId, IShreddedClientSetupWriter writer)
            : base(xml)
        {
            _dataImportQueueId = dataImportQueueId;
            _writer = writer;
        }

        private Guid LastClientTrackingId { get; set; }

        public void ProcessAll()
        {
            try
            {
                while (Xml.Read())
                    ProcessElement();
            }
            catch (Exception ex)
            {
                // If we don't have a client tracking ID, just die a horrible death
                // and let SSIS mark all the client setups as failed.
                if (LastClientTrackingId == default(Guid))
                    throw;

                _writer.WriteDataImportWorkClientSetupResponse(new DataImportWorkClientSetupResponse(
                    responseStatus: 1,
                    resultsMessage: ex.Message,
                    clientTrackingId: LastClientTrackingId));
            }
        }
        private void ProcessElement()
        {
            if (!Xml.IsStartElement())
                return;

            switch (Xml.Name)
            {
                case "ClientGroup":
                    ProcessClientGroup();
                    break;
                case "Bank":
                    ProcessBank();
                    break;
            }
        }
        private void ProcessClientGroup()
        {
            LastClientTrackingId = GetGuid("ClientTrackingID");
        }
        private void ProcessBank()
        {
            var bankId = GetInt32("BankID");
            var bankName = GetOptionalString("BankName", 25);
            var aba = GetOptionalString("ABA", 10);
            _writer.WriteBank(new Bank(_dataImportQueueId, bankId, bankName, aba));
        }
    }
}