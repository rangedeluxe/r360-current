﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     08/02/2012
*
* Purpose:  
*
* Modification History
* Created
* CR 55550 CEJ 08/02/2012
*   -Initial Version
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportIconClient
{
    public class cDimData
    {
        public cDimData(string[] asBatchFields, string[] asItemFields, KeyValuePair<string, string>[] akvpDocTypes, Hashtable hshImageRPSAliasMapping)
        {
            BatchFieldList = asBatchFields.ToList();
            ItemFieldList = asItemFields.ToList();
            DocumentTypes = akvpDocTypes.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            ImageRPSAliasList = hshImageRPSAliasMapping;
        }

        public Dictionary<string, string> DocumentTypes
            { get; set; }

        public Hashtable ImageRPSAliasList
            { get; set; }

        public List<string> BatchFieldList
            { get; set; }

        public List<string> ItemFieldList
            { get; set; }
    }
}
