﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections;
using WFS.LTA.DataImport.DataImportWCFLib;
using WFS.LTA.DataImport.DataImportXClientBase;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     08/02/2012
*
* Purpose:  
*
* Modification History
* Created
* CR 55550 CEJ 08/02/2012
*   -Initial Version
*******************************************************************************/
namespace WFS.LTA.DataImport.DataImportIconClient
{
    public interface IDataImportIconClient 
    {
        event dlgLogEventHandler LogEvent;
        bool GetClientInfo(string sFilename, out cClientInfo cinClientInfo, out string sErrorMessage);

        bool GetClientIDs(StreamReader stmInputFile, out int[] aiClientIDs, out string sErrorMessage);

        bool StreamToBatchXML(StreamReader stmInputFile, cICONConfigData cfgSettings, cDimData ddaDimData, string strClientProcessCode, out XmlDocument xmdResult);

        bool StreamToClientSetupXML(StreamReader stmInputFile, cClientInfo cinClientInfo, cDimData ddaDimData, cICONConfigData cfgSettings,
                 string strClientProcessCode, out XmlDocument xmdResult);

        bool WriteBatchResponse(string sBankID, string sLockboxID, string sBatchID, DateTime datProcessDate,
                bool bSuccessfullyUploaded, cICONConfigData cfgSettings, out string sErrorMessage);
    }
 }
