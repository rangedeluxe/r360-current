﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Xml;
using System.Data;
using WFS.RecHub.Common;
using WFS.LTA.DataImport.DataImportServicesAPI;
using System.Configuration;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ExternalLogon.Common;
using Toolkit = WFS.RecHub.ImportToolkit.Common.Interfaces;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     01/12/2012
*
* Purpose:
*
* Modification History
* CR 33230 JMC 01/12/2012
*    -New File
* CR 52652 WJS 5/14/2012
*   - Add new methods GetDocumentTypes, GetImageRPSAliasMappings,
*		GetBatchDataSetupField,ImageTransferComplete,  and GetItemDataSetupField
* CR 55486 WJS 10/18/2012
*   - Add new method called GetHylandResponses
* WI 70445 WJS 12/12/2012
 *  - Add new method called HylandImageTransferUpdateArchiveImageCount
* WI 95771 WJS 4/11/2013
 *  - Allow DIT to be hosted service
* WI 126987 CMC 01/20/2014
*  - Updated to use 2.00 libraries.
* WI 143238 CEJ 05/20/2014
 * *    Remove multiple batch restriction on DataImportServices
* WI 152405 CMC 07/21/2014
*   Support for Int64 BatchID
* WI 155192 CMC 07/28/2014
*  - Refactoring to remove logon dependencies.
 * ******************************************************************************/
namespace WFS.LTA.DataImport.DataImportServices {

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class DataImportService : _LTAService, Toolkit.IDataImportService, Toolkit.IAlertable, Toolkit.IAuditable
    {
        private const string BATCH_SCHEMATYPE = "BATCH";
        private const string CLIENT_SCHEMATYPE = "CLIENT";

        //Constructor
        public DataImportService()
        {
        }

        public bool GetSchemaDefinition(string type, string XSDVersion, out string setupDataXsd,
                                         out string respXml) {
            bool bolRetVal;
            XmlDocument docClientSetupXsd = null;
            XmlDocument docrespXml;

            string msg = string.Empty;
            msg = String.Format("Type is {0} XSD Version {1}", type, XSDVersion);
            EventLog.logEvent(msg, MessageImportance.Debug);


            if (string.Equals(type, CLIENT_SCHEMATYPE, StringComparison.InvariantCultureIgnoreCase))
            {
                if (Execute<bool>(service => { return service.GetClientSchemaDefinition(XSDVersion, out docClientSetupXsd); }))
                {
                    docrespXml = BuildOperationSuccessDoc();
                    bolRetVal = true;
                }
                else
                {
                    docClientSetupXsd = null;
                    docrespXml = BuildOperationFailedDoc();
                    bolRetVal = false;
                }
            }
            else if (string.Equals(type, BATCH_SCHEMATYPE, StringComparison.InvariantCultureIgnoreCase))
            {
                if (Execute<bool>(service => { return service.GetBatchSchemaDefinitionFromDatabase(XSDVersion, out docClientSetupXsd); }))
                {
                    docrespXml = BuildOperationSuccessDoc();
                    bolRetVal = true;
                }
                else
                {
                    docClientSetupXsd = null;
                    docrespXml = BuildOperationFailedDoc();
                    bolRetVal = false;
                }
            }
            else
            {
                throw new OnlineFormattedException("Invalid type of schema definition.");
            }

            setupDataXsd = (docClientSetupXsd == null ? string.Empty : docClientSetupXsd.OuterXml);
            respXml = docrespXml.OuterXml;

            return (bolRetVal);
        }


        public bool SendClientSetup( string clientSetupXml, int auditDateKey, out string respXml) {

            bool bolRetVal;
            string clientSetupXSDStr;
            XmlDocument docclientSetupXml;
            XmlDocument docrespXml;
            XmlDocument clientSetupXSD = null;
            XmlNode clientGroup = null;
            string clientXSDVersion = string.Empty;

            docclientSetupXml = new XmlDocument();
            docclientSetupXml.XmlResolver = null;
            docclientSetupXml.LoadXml(clientSetupXml);

            clientGroup = docclientSetupXml.SelectSingleNode("ClientGroups");

            if (clientGroup != null)
            {
                clientXSDVersion = clientGroup.Attributes["XSDVersion"].Value;
                bolRetVal = Execute<bool>(service => { return service.GetClientSchemaDefinition(clientXSDVersion, out clientSetupXSD); });
                clientSetupXSDStr = (clientSetupXSD == null ? string.Empty : clientSetupXSD.OuterXml);
                if (bolRetVal)
                {
                    bolRetVal = XML_XSD_Validator.Validate(clientSetupXml, clientSetupXSDStr);
                    if (bolRetVal)
                    {
                        bolRetVal = Execute<bool>(service => { return service.SendClientSetup(docclientSetupXml, auditDateKey); });
                    }
                    else
                    {
                        throw new OnlineFormattedException("Client Setup valid validation of XML.");
                    }
                }
                else
                {
                    throw new OnlineFormattedException("Unable to get schema definitions");
                }
            }
            else
            {
                throw new OnlineFormattedException("No client group node.");
            }

            docrespXml = bolRetVal ? BuildOperationSuccessDoc() : BuildOperationFailedDoc();

            respXml = docrespXml.OuterXml;

            return(bolRetVal);
        }

        public bool SendBatchData(string batchDataXml, int auditDateKey, out string respXml) {

            bool bolRetVal = false;
            string batchDataXSDStr;
            XmlDocument docbatchDataXml;
            XmlDocument docrespXml;
            XmlDocument batchDataXSD = null;
            int rtnNumberOfBatchNodes = 0;
            string batchXSDVersion = string.Empty;
            XmlNode batchNode = null;

            docbatchDataXml = new XmlDocument();
            docbatchDataXml.XmlResolver = null;
            docbatchDataXml.LoadXml(batchDataXml);

            batchNode = docbatchDataXml.SelectSingleNode("Batches");
            if (batchNode != null)
            {
                batchXSDVersion = batchNode.Attributes["XSDVersion"].Value;
                bolRetVal = Execute<bool>(service => { return service.GetBatchSchemaDefinitionFromDatabase(batchXSDVersion, out  batchDataXSD); });
                batchDataXSDStr = (batchDataXSD == null ? string.Empty : batchDataXSD.OuterXml);
                if (bolRetVal)
                {
                    bolRetVal = XML_XSD_Validator.Validate(batchDataXml, batchDataXSDStr);
                    if (bolRetVal)
                    {
                        rtnNumberOfBatchNodes = GetNumberOfBatchNodes(docbatchDataXml);
                        bolRetVal = Execute<bool>(service => { return service.SendBatchData(docbatchDataXml, auditDateKey); });
                    }
                    else
                    {
                        throw new OnlineFormattedException("Batch Data valid validation of XML.");
                    }
                }
                else
                {
                    throw new OnlineFormattedException("Unable to get schema definitions");
                }
            }
            else
            {
                throw new OnlineFormattedException("No Batches node");
            }

            docrespXml = bolRetVal ? BuildOperationSuccessDoc() : BuildOperationFailedDoc();

            respXml = docrespXml.OuterXml;

            return(bolRetVal);
        }

        private int GetNumberOfBatchNodes(XmlDocument docbatchDataXml){
            int rtnNumberOfBatchNodes = 0;
            XmlNodeList batchNodeList = docbatchDataXml.SelectNodes("Batches/Batch");
            if (batchNodeList != null)
            {
                rtnNumberOfBatchNodes = batchNodeList.Count;
            }

            return rtnNumberOfBatchNodes;
        }

        public bool SendBatchMeasureData(BatchFileImportMeasures batchFileImportMeasures)
        {
            return Execute<bool>(service => { return service.SendBatchMeasureData(batchFileImportMeasures); });
        }

        public void SendBatchImages(IList<UploadRequest> items)
        {
            Execute<object>(service =>
            {
                service.SendBatchImages(items);
                return null;
            });
        }


        private XmlDocument BuildOperationSuccessDoc() {
            return(BuildMsgDoc("Information", new string[]{"Operation Successful"}));
        }

        private XmlDocument BuildOperationFailedDoc() {
            return(BuildMsgDoc("Error", new string[]{"Operation Failed"}));
        }

        private XmlDocument BuildErrorDoc(string[] ErrorMessages) {
            return(BuildMsgDoc("Error", ErrorMessages));
        }

        private XmlDocument BuildMsgDoc(string MsgType, string[] ErrorMessages) {

            XmlDocument docXml;
            XmlNode nodeRoot;
            XmlNode nodeMessages;
            XmlAttribute attType;
            XmlNode nodeMsg;


            docXml = new XmlDocument();
            nodeRoot = docXml.CreateElement("Root", docXml.NamespaceURI);
            nodeMessages = docXml.CreateElement("Messages", docXml.NamespaceURI);
            foreach(string msg in ErrorMessages) {
                nodeMsg = docXml.CreateElement("Message");
                attType = docXml.CreateAttribute("Type", docXml.NamespaceURI);
                attType.Value = MsgType;
                nodeMsg.Attributes.Append(attType);
                nodeMsg.InnerText = msg;
                nodeMessages.AppendChild(nodeMsg);
            }
            nodeRoot.AppendChild(nodeMessages);
            docXml.AppendChild(nodeRoot);

            return(docXml);
        }


        /// <summary>
        ///
        /// </summary>
        /// <param name="respXml"></param>
        /// <returns></returns>
        public DataSet GetBatchDataResponses(string clientProcessCode, out bool success)
        {
            XmlDocument docrespXml;
            DataSet dsBatchData = null;
            DataTable dtBatchDataResponses = null;
            success = false;

            //todo: remove out parm??
            success = Execute<bool>(service => { return service.GetBatchResponse(clientProcessCode, out dtBatchDataResponses); });
            if (success && dtBatchDataResponses != null)
            {
                dsBatchData = dtBatchDataResponses.DataSet;
            }

            docrespXml = success ? BuildOperationSuccessDoc() : BuildOperationFailedDoc();

            return (dsBatchData);
        }

        /// <summary>
        /// Returns a list of client-setup files whose processing is complete on the
        /// server, and that are ready for the client to complete processing.
        /// </summary>
        /// <param name="clientProcessCode">The client process code. May be null or empty.</param>
        public ClientSetupResponses GetClientSetupResponses(string clientProcessCode)
        {
            return Execute(service => service.GetClientSetupResponses(clientProcessCode));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="responseIdentifier"></param>
        /// <returns></returns>
        public bool SendResponseCompleteClientUnknown(Guid responseIdentifier)
        {

            bool bolRetVal = false;
            XmlDocument docrespXml;

            bolRetVal = Execute<bool>(service => { return service.DataImportSetResponseComplete(responseIdentifier, false); });
            docrespXml = bolRetVal ? BuildOperationSuccessDoc() : BuildOperationFailedDoc();
            return bolRetVal;
        }

        protected override void SetUp()
        {
            DataImportRepository = new DataImportRepository(new R360DBConnectionSettings());
            SiteOptions = new cSiteOptions(SiteKey);

            //grab the assembly and the service name (the object name to instantiate) from the config file
            var assembly = ConfigurationManager.AppSettings.Get("LogonModuleAssembly");
            var moduleName = ConfigurationManager.AppSettings.Get("LogonModuleName");

            try
            {
                LogonService = Activator.CreateInstance(assembly, moduleName).Unwrap() as IExternalLogonService;
            }
            catch { }//if we can't dynamically load the login client, it will be defaulted

        }

        protected override void SetUp(Guid sessionId)
        {
            if (DataImportService == null)
            {
                SetUp();
                DataImportService = new DataImportServicesAPI.DataImportServices
                    (new ServiceSetupContext
                        {
                            DataImportRepository = DataImportRepository,
                            SiteOptions = SiteOptions,
                            LogonService = LogonService,
                            Alert = Alert,
                            Audit = Audit,
                            WorkgroupRetrieval = WorkgroupRetrieval,
                            ClientSetupSchemaVersion = ConfigurationManager.AppSettings["ClientSetupSchemaVersion"],
                            BatchSchemaVersion = ConfigurationManager.AppSettings["BatchSchemaVersion"],
                        });
            }
            if (!DataImportService.IsSessionValid(new SessionRequest { Session = sessionId }))
                throw new InvalidSessionException();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="responseIdentifier"></param>
        /// <returns></returns>
        public bool SendResponseComplete(Guid responseIdentifier)
        {
            bool bolRetVal = false;
            return bolRetVal = Execute<bool>(service => { return service.DataImportSetResponseComplete(responseIdentifier, true); });
        }

        public DataSet GetDocumentTypes(DateTime? loadDate, out bool success)
        {

            DataSet dsDocumentTypes = null;
            DataTable dtDocumentTypesTable = null;
            success = false;

            success = Execute<bool>(service => { return service.GetDocumentTypes(loadDate, out dtDocumentTypesTable); });
            if (success && dtDocumentTypesTable != null)
            {
                dsDocumentTypes = dtDocumentTypesTable.DataSet;
            }

            return dsDocumentTypes;
        }

        public DataSet GetBatchDataSetupField(string batchSourceName, DateTime? modificationDate, out bool success)
        {
            DataSet dsBatchSetup = null;
            DataTable dtBatchSetupTable = null;
            success = false;

            success = Execute<bool>(service => { return service.GetBatchDataSetupField(batchSourceName, modificationDate, out dtBatchSetupTable); });

            if (success && dtBatchSetupTable != null)
            {
                dsBatchSetup = dtBatchSetupTable.DataSet;
            }

            return dsBatchSetup;
        }

        public DataSet GetItemDataSetupField(string batchSourceName, DateTime? modificationDate, out bool success) {

            DataSet dsItemSetup = null;
            DataTable dtItemSetupTable = null;
            success = false;

            success = Execute<bool>(service => { return service.GetItemDataSetupField(batchSourceName, modificationDate, out dtItemSetupTable); });

            if (success && dtItemSetupTable != null)
            {
                dsItemSetup = dtItemSetupTable.DataSet;
            }

            return dsItemSetup;
        }

        public DataSet GetImageRPSAliasMappings(Int32 siteBankID, Int32 siteLockboxId, DateTime? modificationDate, out bool success)
        {
            DataSet dsImageRPSAliasMappings = null;
            DataTable dtImageRPSAliasMappingTable = null;
            success = false;

            success = Execute<bool>(service => { return service.GetImageRPSAliasMappings(siteBankID, siteLockboxId, modificationDate, out dtImageRPSAliasMappingTable); });

            if (success && dtImageRPSAliasMappingTable != null)
            {
                dsImageRPSAliasMappings = dtImageRPSAliasMappingTable.DataSet;
            }

            return dsImageRPSAliasMappings;
        }

        public AlertResponse Log(AlertRequest requestContext)
        {
            return Execute<AlertResponse>(service => { return service.LogAlert(requestContext); });
        }

        public AuditResponse LogAuditEvent(AuditRequest requestContext)
        {
            return Execute<AuditResponse>(service => { return service.LogAuditEvent(requestContext); });
        }

        public WorkgroupResponse GetWorkgroupInfo(WorkgroupRequest requestContext)
        {
            return Execute<WorkgroupResponse>(service => { return service.GetWorkgroupInfo(requestContext); });
        }

        public bool UpdateStatus(UpdateStatusContext context)
        {

            bool bolRetVal;
            XmlDocument docrespXml;

            if (context == null)
                throw new ArgumentNullException("Must provide a status context.");

            bolRetVal = Execute<bool>(service => { return service.DataImportSetResponseComplete(context); });

            docrespXml = bolRetVal ? BuildOperationSuccessDoc() : BuildOperationFailedDoc();

            return bolRetVal;
        }


        public MultiImageResponse MultiImageUpload(MultiImageRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("Must provide a request context.");

            var response = Execute<MultiImageResponse>(service => { return service.MultiImageUpload(request); });
            return response;
        }
    }

}