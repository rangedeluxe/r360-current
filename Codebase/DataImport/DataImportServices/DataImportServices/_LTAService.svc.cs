﻿using System;
using System.ServiceModel;
using WFS.LTA.DataImport.DataImportServicesAPI;
using WFS.RecHub.Common.Log;
using WFS.RecHub.Common;
using WFS.LTA.DataImport.DataImportWCFLib;
using System.Runtime.CompilerServices;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ExternalLogonService;
using WFS.RecHub.ImportToolkit.Common.Interfaces;
using WFS.RecHub.ImportToolkit.API;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     01/12/2012
*
* Purpose:  
*
* Modification History
* CR 33230 JMC 01/12/2012
*    -New file
* WI 95771 WJS 4/11/2013
 *  - Allow DIT to be hosted service. Do not pass host context to session table
* WI 126987 CMC 01/20/2013
*  - Updated to use 2.00 libraries.
* WI 155192 CMC 07/28/2014
*  - Refactoring to remove logon dependencies.
******************************************************************************/
namespace WFS.LTA.DataImport.DataImportServices {

    public abstract class _LTAService : IDisposable
    {
        // Track whether Dispose has been called.
        private bool disposed = false;

        private cSiteOptions _SiteOptions = null;
        private cEventLog _EventLog = null;
        private string _SiteKey = null;
        private Guid sessionId;
        private IExternalLogonService logonService;
        private IDataImportRepository dataImportRepository;
        private IAlertable alert;
        private IAuditable audit;
        private IWorkgroupRetrievable workgroupRetrieval;
        private const string INISECTION_DataImportSERVICES = "DataImportServices";
        private const string INIKEY_INTERMEDIATESERVER = "IntermediateServer";
        private const string INIKEY_SERVERLOCATION = "ServerLocation";
        protected const string HTTPHEADER_SITEKEY = "SiteKey";
        protected const string HTTPHEADER_SESSIONID = "SessionID";
        protected const string HTTPHEADER_ENTITY = "Entity";
        protected DataImportServicesAPI.DataImportServices DataImportService;

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        protected cSiteOptions SiteOptions {
            get {
                if (_SiteOptions == null){
                    _SiteOptions = new cSiteOptions(_SiteKey);
                }
                return _SiteOptions;
            }
            set
            {
                _SiteOptions = value;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog EventLog {
            get {
                if (_EventLog == null){
                    _EventLog = new cEventLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (MessageImportance)SiteOptions.loggingDepth);
                }

                return _EventLog;
            }
        }

        protected string SiteKey {
            get {
                return(_SiteKey);
            }
            set {
                _SiteKey = value;
            }
        }

        protected string Entity
        {
            get;
            set;
        }

        protected IDataImportRepository DataImportRepository
        {
            get
            {
                return (dataImportRepository);
            }
            set
            {
                dataImportRepository = value;
            }
        }

        protected IAlertable Alert
        {
            get
            {
                if (alert == null)
                    return new ImportToolkitRepository(SiteKey);
                
                return alert;
            }
            set
            {
                alert = value;
            }
        }

        protected IAuditable Audit
        {
            get
            {
                if (audit == null)
                    return new ImportToolkitRepository(SiteKey);

                return audit;
            }
            set
            {
                audit = value;
            }
        }

        protected IWorkgroupRetrievable WorkgroupRetrieval
        {
            get
            {
                if (workgroupRetrieval == null)
                    return new ImportToolkitRepository(SiteKey);

                return workgroupRetrieval;
            }
            set
            {
                workgroupRetrieval = value;
            }
        }

        private ExternalLogonClient _externalLogon = null;
        protected IExternalLogonService LogonService
        {
            get
            {
                if (_externalLogon == null)
                    _externalLogon = new ExternalLogonClient();
                return _externalLogon;
                //return logonService;
            }
            set
            {
                logonService = value;
            }
        }

        protected abstract void SetUp(Guid sessionId);
        protected abstract void SetUp();


        // Ping
        public string Ping() {

            FaultException error;

            try {

               return "Pong";

            } catch(Exception ex) {
                error = BuildGeneralException(ex, "Ping", "A general exception occurred while executing Ping() method");
                throw error; 
            }
        }

        public bool GetSession(string LogonName,
                               string Password,
                               out Guid SessionID) {

            bool bolRetVal;
            Guid gidSessionID;
            var session = ManageLogon<ExternalLogonResponse>(operation =>
                { return operation.ExternalLogon(new ExternalLogonRequest { Username = LogonName, Password = Password, Entity = string.Empty, IPAddress = "1.1.1.1" }); });

                gidSessionID = session.Session;
                bolRetVal = session.Status == StatusCode.SUCCESS ? true : false;
            if (!bolRetVal)
            {
                foreach (var message in session.Errors)
                {
                    EventLog.logEvent(message, "GetSession", MessageType.Error, MessageImportance.Essential);
                }
            }

            SessionID = gidSessionID;

            return(bolRetVal);
        }

        public bool EndSession() 
        {

            bool bolRetVal;

            GetHeaderItems();

            if (sessionId == null)
                throw new InvalidSessionIDException();

            var response = ManageLogon<SessionResponse>(operation => { return operation.EndSession(new SessionRequest { Session = sessionId }); });                 
            return bolRetVal = response.Status == StatusCode.SUCCESS ? true : false;
               
        }

        protected T Execute<T>(Func<DataImportServicesAPI.DataImportServices, T> operation, [CallerMemberName] string procName = null)
        {
            try
            {
                GetHeaderItems();

                if (sessionId == null)
                    throw new InvalidSessionIDException();

                SetUp(sessionId);
                return operation(DataImportService);
            }
            catch (InvalidSiteKeyException ex)
            {
                var fault = new DataImportServiceFault(0, (int)DataImportServiceErrorCodes.InvalidSiteKey, this.GetType().Name, ex.Message);
                throw new FaultException<DataImportServiceFault>(fault, fault.Message);
            }
            catch (InvalidSessionIDException ex)
            {
                var fault = new DataImportServiceFault(0, (int)DataImportServiceErrorCodes.InvalidSessionID, this.GetType().Name, ex.Message);
                throw new FaultException<DataImportServiceFault>(fault, fault.Message);
            }
            catch (SessionExpiredException ex)
            {
                DataImportServiceFault fault = new DataImportServiceFault(0, (int)DataImportServiceErrorCodes.SessionExpired, this.GetType().Name, ex.Message);
                throw new FaultException<DataImportServiceFault>(fault, fault.Message);
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                var fault = new DataImportServiceFault(0, (int)DataImportServiceErrorCodes.UndefinedError, this.GetType().Name, ex.Message);
                throw new FaultException<DataImportServiceFault>(fault, fault.Message);
            }
        }

        protected T ManageLogon<T>(Func<IExternalLogonService, T> operation, [CallerMemberName] string procName = null) where T : new()
        {

            try
            {
                GetHeaderItems();

                SetUp();

                return operation(LogonService);

            }
            catch (InvalidSiteKeyException ex)
            {
                var fault = new DataImportServiceFault(0, (int)DataImportServiceErrorCodes.InvalidSiteKey, this.GetType().Name, ex.Message);
                throw new FaultException<DataImportServiceFault>(fault, fault.Message);
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw BuildGeneralException(ex, this.GetType().Name, ex.Message);
            }
        }

 

//==================================================================================================

        private void GetHeaderItems()
        {
            _SiteKey = Utility.GetPropertyValue<DataImportServiceContext, string>(DataImportServiceContext.Current, HTTPHEADER_SITEKEY) ?? string.Empty;
            Entity = Utility.GetPropertyValue<DataImportServiceContext, string>(DataImportServiceContext.Current, HTTPHEADER_ENTITY) ?? string.Empty;
            sessionId = Utility.GetPropertyValue<DataImportServiceContext, Guid>(DataImportServiceContext.Current, HTTPHEADER_SESSIONID);
           
            if (string.IsNullOrEmpty(_SiteKey))
                throw new InvalidSiteKeyException();
        }

        private FaultException BuildGeneralException(Exception ex, string procName, string Msg) {
            EventLog.logEvent("A general exception occurred", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return(BuildServerFaultException(DataImportServiceErrorCodes.UndefinedError, Msg, Msg));
        }

        private FaultException BuildIniReadException(Exception ex, string procName, string section, string key) {
            EventLog.logEvent("Unable to read ini setting: [" + section + "]." + key, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return(BuildServerFaultException(DataImportServiceErrorCodes.ConfigurationError, 
                                             "A configuration error occurred at the server",
                                             "A configuration error occurred at the server"));
        }

        private FaultException BuildCommunicationsChannelException(Exception ex, string procName, string Msg) {
            EventLog.logEvent("Unable to establish communications channel - " + Msg, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return(BuildServerFaultException(DataImportServiceErrorCodes.CommunicationError, 
                                             "A communications channel occurred at the server",
                                             "A communications channel occurred at the server"));
        }

        private static FaultException BuildServerFaultException(DataImportServiceErrorCodes ErrorCode, string Msg, string Details) {

            FaultException objError;

            var errorCode = ((int)ErrorCode).ToString();
            var message = string.Format("{0} - {1}", errorCode, Msg);
            objError = new FaultException(string.Format("Error Code {0} Encountered.  Message: {1} Details: {2}", errorCode, message, Details));

            return(objError);
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }


   }
}