﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Xml;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.LTA.DataImport.DataImportServicesAPI.DTO;
using WFS.RecHub.ApplicationBlocks.DataAccess;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using System.Data.SqlClient;

namespace WFS.LTA.DataImport.DataImportServicesAPI
{
    public class DataImportRepository : IDataImportRepository
    {
        /// <summary>
        /// Data Import Queue Status enumeration 
        /// </summary>
        private enum DataImportQueueStatus
        {
            ReadyToProcess = 10,
            FailedProcessing = 15,
            InProcess = 20,
            Failed = 30,
            CompletedResponseReady = 99,
            ResponseSendWaitingForReceipt = 120,
            ClientUnknownErrorDeadRecord = 145,
            ResponseComplete = 150
        }

        private const string XSD_BATCH = "Data Import Integration Services for Batch Data";
        private const string XSD_CLIENT_SETUP = "Data Import Integration Services for Client Setup";
        private const string XSL_BATCH = "Data Import Integration Services for Batch Data XSL";
        private const string XSL_CLIENT_SETUP = "Data Import Integration Services for Client Setup XSL";

        private ConnectionStringSettings _connectionStringSettings { get; set; }

        public DataImportRepository(R360DBConnectionSettings connectionStringSettings)
        {
            if (connectionStringSettings == null)//defend here, don't create the object
                throw new ArgumentNullException("Must provide R360DBConnectionStringSettings");
            _connectionStringSettings = connectionStringSettings.Value;
        }

        public PossibleResult<IEnumerable<ImportSchemasDto>> GetBatchDataXsd(string xsdVersion)
        {
            if (xsdVersion == null)
                return Result.None<IEnumerable<ImportSchemasDto>>();

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmSchemaType", dbProviderFactory, DbType.String, ParameterDirection.Input, XSD_BATCH),
                Database.CreateParm("@parmXSDVersion", dbProviderFactory, DbType.String, ParameterDirection.Input, xsdVersion)
            };

            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure, "RecHubSystem.usp_ImportSchemas_Get_BySchemaType", parms);

            if (!dataSetResult.HasData())
                return Result.None<IEnumerable<ImportSchemasDto>>();

            return Result.Real(dataSetResult.Tables[0].ToObjectList<ImportSchemasDto>() as IEnumerable<ImportSchemasDto>);
        }

        public PossibleResult<IEnumerable<ImportSchemasDto>> GetClientSetupXsd(string xsdVersion)
        {
            if (xsdVersion == null)
                return Result.None<IEnumerable<ImportSchemasDto>>();

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmSchemaType", dbProviderFactory, DbType.String, ParameterDirection.Input, XSD_CLIENT_SETUP),
                Database.CreateParm("@parmXSDVersion", dbProviderFactory, DbType.String, ParameterDirection.Input, xsdVersion)
            };

            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure, "RecHubSystem.usp_ImportSchemas_Get_BySchemaType", parms);

            if (!dataSetResult.HasData())
                return Result.None<IEnumerable<ImportSchemasDto>>();

            return Result.Real(dataSetResult.Tables[0].ToObjectList<ImportSchemasDto>() as IEnumerable<ImportSchemasDto>);
        }

        public PossibleResult<IEnumerable<ImportSchemasDto>> GetBatchDataXSL(string xslVersion)
        {
            if (xslVersion == null)
                return Result.None<IEnumerable<ImportSchemasDto>>();

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmSchemaType", dbProviderFactory, DbType.String, ParameterDirection.Input, XSL_BATCH),
                Database.CreateParm("@parmXSDVersion", dbProviderFactory, DbType.String, ParameterDirection.Input, xslVersion)
            };

            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure, "RecHubSystem.usp_ImportSchemas_Get_BySchemaType", parms);

            if (!dataSetResult.HasData())
                return Result.None<IEnumerable<ImportSchemasDto>>();

            return Result.Real(dataSetResult.Tables[0].ToObjectList<ImportSchemasDto>() as IEnumerable<ImportSchemasDto>);
        }

        public PossibleResult<IEnumerable<ImportSchemasDto>> GetClientSetupXSL(string xslVersion)
        {
            if (xslVersion == null)
                return Result.None<IEnumerable<ImportSchemasDto>>();

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmSchemaType", dbProviderFactory, DbType.String, ParameterDirection.Input, XSL_CLIENT_SETUP),
                Database.CreateParm("@parmXSDVersion", dbProviderFactory, DbType.String, ParameterDirection.Input, xslVersion)
            };

            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure, "RecHubSystem.usp_ImportSchemas_Get_BySchemaType", parms);

            if (!dataSetResult.HasData())
                return Result.None<IEnumerable<ImportSchemasDto>>();

            return Result.Real(dataSetResult.Tables[0].ToObjectList<ImportSchemasDto>() as IEnumerable<ImportSchemasDto>);
        }

        public bool DataImportInsertClientSetup(string xml, int auditDateKey)
        {
            bool bRtnval = true;

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmClientSetup", dbProviderFactory, DbType.Xml, ParameterDirection.Input, xml),
                Database.CreateParm("@parmAuditDateKey", dbProviderFactory, DbType.Int32, ParameterDirection.Input, auditDateKey)
            };

            //var dataSetResult =
                Database.ExecuteNonQuery(_connectionStringSettings, CommandType.StoredProcedure, "RecHubSystem.usp_DataImportQueue_Ins_ClientSetup", parms);

            return bRtnval;
        }

        public bool DataImportInsertBatch(XmlDocument doc, int auditDateKey)
        {
            bool bRtnval = true;

            var columns = new string[]
            {
                "XSDVersion",
                "ClientProcessCode",
                "SourceTrackingID",
                "BatchTrackingID",
                "XMLDataDocument"
            };
            var currenttable = new DataTable();
            currenttable.Columns.AddRange(columns.Select(c => new DataColumn(c)).ToArray());
            // Grab some common data from the 'batches' row.
            var batches = doc.SelectSingleNode("/Batches");
            var sourcetrackingid = Guid.Parse(batches.Attributes["SourceTrackingID"].Value);
            var xsdversion = batches.Attributes["XSDVersion"].Value;
            var clientcode = batches.Attributes["ClientProcessCode"].Value;
            foreach (XmlNode batch in batches.SelectNodes("Batch"))
            {
                // Set data for our new batch.
                var batchxml = batch.OuterXml;
                var row = currenttable.NewRow();
                row["XSDVersion"] = xsdversion;
                row["ClientProcessCode"] = clientcode;
                row["SourceTrackingID"] = sourcetrackingid;
                row["BatchTrackingID"] = Guid.Parse(batch.Attributes["BatchTrackingID"].Value);
                row["XMLDataDocument"] = batchxml;
                currenttable.Rows.Add(row);
            }

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                new SqlParameter{
                    ParameterName = "@parmDataImportQueueInsertTable",
                    SqlDbType = SqlDbType.Structured,
                    Value = currenttable,
                    Direction = ParameterDirection.Input

                }, 
                Database.CreateParm("@parmAuditDateKey", dbProviderFactory, DbType.Int32, ParameterDirection.Input, auditDateKey)
            };
                //        arParms.Add(BuildParameter("@parmDataImportQueueInsertTable", SqlDbType.Structured, currenttable, ParameterDirection.Input));
                //        arParms.Add(BuildParameter("@parmAuditDateKey", SqlDbType.Int, auditDateKey, ParameterDirection.Input));

            //var dataSetResult =
            Database.ExecuteNonQuery(_connectionStringSettings,  CommandType.StoredProcedure, "RecHubSystem.usp_DataImportQueue_Ins_Batch", parms);
            return bRtnval;
        }

        public PossibleResult<DataTable> DataImportResponseBatch(string clientProcessCode)
        {
            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            List<DbParameter> parms = null;

            if (!String.IsNullOrEmpty(clientProcessCode))
            {
                parms = new List<DbParameter>
                {
                    Database.CreateParm("@parmClientProcessCode", dbProviderFactory, DbType.String,
                        ParameterDirection.Input, clientProcessCode)
                };
            }

            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure, "RecHubSystem.usp_DataImportQueue_Get_BatchResponse", parms);

            if (!dataSetResult.HasData())
                return Result.None<DataTable>();

            //var abc = Result.Real(dataSetResult.Tables[0]);
            //DataTable lll = abc.GetResult(new DataTable());

            return Result.Real(dataSetResult.Tables[0]);
        }

        public ClientSetupResponses DataImportResponseClient(string clientProcessCode)
        {
            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            List<DbParameter> parms = null;

            if (!String.IsNullOrEmpty(clientProcessCode))
            {
                parms = new List<DbParameter>
                {
                    Database.CreateParm("@parmClientProcessCode", dbProviderFactory, DbType.String,
                        ParameterDirection.Input, clientProcessCode)
                };
            }

            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure, "RecHubSystem.usp_DataImportQueue_Get_ClientSetupResponse", parms);

            DataTable dt = dataSetResult.Tables[0];
            
            if (!dataSetResult.HasData())
            {
                return new ClientSetupResponses {Responses = new ClientSetupResponse[] { }};
            }

            //var theResponse = ClientSetupResponses.CreateFromDataTable(dt, true);
            return ClientSetupResponses.CreateFromDataTable(dt, true);
        }

        public bool DataImportSetResponseComplete(Guid parmResponseTrackingID, bool clientKnowsAboutRecord)
        {
            return DataImportSetResponseComplete(parmResponseTrackingID, null, null, clientKnowsAboutRecord);
        }

        public bool DataImportSetResponseComplete(Guid responseTrackingId, Guid? entityTrackingId, Guid? sourceTrackingId, bool clientKnowsAboutRecord)
        {
            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);
           
            DataImportQueueStatus queueStatus = DataImportQueueStatus.ResponseComplete;
            if (clientKnowsAboutRecord)
            {
                queueStatus = DataImportQueueStatus.ResponseComplete;
            }
            else
            {
                queueStatus = DataImportQueueStatus.ClientUnknownErrorDeadRecord;
            }

            List<DbParameter> parms = new List<DbParameter>
            {
                Database.CreateParm("@parmResponseTrackingID", dbProviderFactory, DbType.Guid, ParameterDirection.Input, responseTrackingId),
                Database.CreateParm("@parmEntityTrackingID", dbProviderFactory, DbType.Guid, ParameterDirection.Input, entityTrackingId),
                Database.CreateParm("@parmSourceTrackingID", dbProviderFactory, DbType.Guid, ParameterDirection.Input, sourceTrackingId),
                Database.CreateParm("@parmQueueStatus", dbProviderFactory, DbType.Int32, ParameterDirection.Input, queueStatus)
            };

            var dataSetResult =
                Database.ExecuteNonQuery(_connectionStringSettings, CommandType.StoredProcedure, "RecHubSystem.usp_DataImportQueue_Upd_QueueStatus", parms);

            return true;
        }


        public PossibleResult<DataTable> DataImportRequestDocumentTypes(DateTime? loadDate)
        {
            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            List<DbParameter> parms = new List<DbParameter>
            {
                Database.CreateParm("@parmCreationDate", dbProviderFactory, DbType.DateTime, ParameterDirection.Input,
                    loadDate)
            };


            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure,
                    "RecHubSystem.usp_dimDocumentTypes_Get_ByCreationDate", parms);

            if (!dataSetResult.HasData())
                return Result.None<DataTable>();

            return Result.Real(dataSetResult.Tables[0]);
        }
        
        public PossibleResult<DataTable> DataImportGetImageRPSAliasMappings(int siteBankID, int siteLockboxId, DateTime? modificationDate)
        {
            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            List<DbParameter> parms = new List<DbParameter>
            {
                Database.CreateParm("@parmBankID", dbProviderFactory, DbType.Int32, ParameterDirection.Input, siteBankID),
                Database.CreateParm("@parmClientAccountID", dbProviderFactory, DbType.Int32, ParameterDirection.Input, siteLockboxId),
                Database.CreateParm("@parmModificationDate", dbProviderFactory, DbType.DateTime, ParameterDirection.Input, modificationDate)
            };


            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure,
                    "RecHubSystem.usp_dimImageRPSAliasMappings_GetRecords", parms);

            if (!dataSetResult.HasData())
                return Result.None<DataTable>();

            return Result.Real(dataSetResult.Tables[0]);
        }

        public PossibleResult<DataTable> GetItemDataSetupField(string sBatchSourceName, DateTime? modificationDate)
        {
            if (sBatchSourceName == null)
                return Result.None<DataTable>();

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmImportShortName", dbProviderFactory, DbType.String, ParameterDirection.Input, sBatchSourceName),
                Database.CreateParm("@parmModificationDate", dbProviderFactory, DbType.DateTime, ParameterDirection.Input, modificationDate)
            };

            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure, "RecHubData.usp_dimItemDataSetupFields_Get_ByShortName", parms);

            if (!dataSetResult.HasData())
                return Result.None<DataTable>();

            return Result.Real(dataSetResult.Tables[0]);
        }

        public PossibleResult<DataTable> GetBatchDataSetupField(string sBatchSourceName, DateTime? modificationDate)
        {
            if (sBatchSourceName == null)
                return Result.None<DataTable>();

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            List<DbParameter> parms = new List<DbParameter>
            {
                Database.CreateParm("@parmImportShortName", dbProviderFactory, DbType.String, ParameterDirection.Input, sBatchSourceName),
                Database.CreateParm("@parmModificationDate", dbProviderFactory, DbType.DateTime, ParameterDirection.Input, modificationDate)
            };

            var dataSetResult =
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure,
                    "RecHubData.usp_dimBatchDataSetupFields_Get_ByShortName", parms);

            if (!dataSetResult.HasData())
                return Result.None<DataTable>();

            return Result.Real(dataSetResult.Tables[0]);
        }

        public bool InsertBatchMeasurement(BatchFileImportMeasures batchFileImportMeasures)
        {
            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            foreach (var batchFileImportMeasure in batchFileImportMeasures.BatchFileImportMeasure)
            {
                List<DbParameter> parms = new List<DbParameter>
                {
                    Database.CreateParm("@parmMeasureTypeName", dbProviderFactory,      DbType.String,ParameterDirection.Input, batchFileImportMeasures.MeasurementTypeName),
                    Database.CreateParm("@parmDurationMilliseconds", dbProviderFactory, DbType.Int64,ParameterDirection.Input, batchFileImportMeasures.Duration),
                    Database.CreateParm("@parmBatchCount", dbProviderFactory,           DbType.Int32,ParameterDirection.Input, batchFileImportMeasures.BatchFileImportMeasure.Count),
                    Database.CreateParm("@parmSourceFileName", dbProviderFactory,       DbType.String,ParameterDirection.Input, batchFileImportMeasures.SourceFileName),
                    Database.CreateParm("@parmSourceTrackingID", dbProviderFactory,     DbType.Guid,ParameterDirection.Input, batchFileImportMeasures.SourceTrackingId),
                    Database.CreateParm("@parmClientProcessCode", dbProviderFactory,    DbType.String,ParameterDirection.Input, batchFileImportMeasures.ClientProcessingCode),
                    Database.CreateParm("@parmIsSuccessful", dbProviderFactory,         DbType.Boolean,ParameterDirection.Input, batchFileImportMeasures.IsSuccessful),
                    Database.CreateParm("@parmSiteBankID", dbProviderFactory,           DbType.Int32,ParameterDirection.Input, batchFileImportMeasure.SiteBankId),
                    Database.CreateParm("@parmSiteWorkgroupID", dbProviderFactory,      DbType.Int32,ParameterDirection.Input, batchFileImportMeasure.SiteWorkgroupId),
                    Database.CreateParm("@parmDepositDateKey", dbProviderFactory,       DbType.Int32,ParameterDirection.Input, batchFileImportMeasure.DepositDateKey),
                    Database.CreateParm("@parmImmutableDateKey", dbProviderFactory,     DbType.Int32,ParameterDirection.Input, batchFileImportMeasure.ImmutableDateKey),
                    Database.CreateParm("@parmSourceBatchID", dbProviderFactory,        DbType.Int64,ParameterDirection.Input, batchFileImportMeasure.SourceBatchId),
                    Database.CreateParm("@parmTransactionCount", dbProviderFactory,     DbType.Int32,ParameterDirection.Input, batchFileImportMeasure.TransactionCount),
                    Database.CreateParm("@parmPaymentCount", dbProviderFactory,         DbType.Int32,ParameterDirection.Input, batchFileImportMeasure.PaymentCount),
                    Database.CreateParm("@parmStubCount", dbProviderFactory,            DbType.Int32,ParameterDirection.Input, batchFileImportMeasure.StubCount),
                    Database.CreateParm("@parmDocumentCount", dbProviderFactory,        DbType.Int32,ParameterDirection.Input, batchFileImportMeasure.DocumentCount),
                    Database.CreateParm("@parmBatchTrackingID", dbProviderFactory,      DbType.Guid,ParameterDirection.Input, batchFileImportMeasure.BatchTrackingId)
                };

                var dataSetResult =
                    Database.ExecuteNonQuery(_connectionStringSettings, CommandType.StoredProcedure, "RecHubMeasure.usp_factBatchImport_Ins", parms);
            }

            return true;
        }
    }
}
