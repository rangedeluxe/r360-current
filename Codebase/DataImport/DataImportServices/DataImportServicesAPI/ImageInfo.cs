﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.DataImport.DataImportServicesAPI
{
    public class ImageInfo
    {
        public ImageInfo(byte[] image)
        {
            Image = image;
            ValidateImage();
        }


        public int NumberOfPages
        {
            get;
            private set;
        }

        public byte[] Image
        {
            get;
            private set;
        }

        public Stream GetFrontPageStream()
        {
            if (NumberOfPages > 0)
                return GetPageStream(0);

            return new MemoryStream();//return empty memory stream

        }

        public byte[] GetBackPageBytes()
        {
            return GetPageBytes(false);
        }

        public byte[] GetFrontPageBytes()
        {
            return GetPageBytes(true);
        }

        public byte[] GetPageBytes(bool frontPage)
        {
            using (Stream stream = frontPage ? GetFrontPageStream() : GetBackPageStream())
            {
                byte[] bytes = new byte[stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                return bytes;
            }
        }

        public Stream GetBackPageStream()
        {
            if (NumberOfPages > 1)
                return GetPageStream(1);

            return new MemoryStream();
        }

        private ImageCodecInfo GetCodecInfo(ImageFormat format)
        {
            ImageCodecInfo[] codecs;
            ImageCodecInfo codecInfo = null;
            Guid clsid;

            clsid = format.Guid;
            codecs = ImageCodecInfo.GetImageEncoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (clsid.Equals(codec.FormatID))
                {
                    codecInfo = codec;
                    break;
                }
            }
            return codecInfo;
        }

        private void ValidateImage()
        {
            if (Image == null)
                throw new ArgumentNullException("Must provide an image in order to do the parse.");

            if (Image.Length == 0)
                throw new ArgumentOutOfRangeException("Must provide a valid image in order to do the parse.");

            using (Image img = Bitmap.FromStream(new MemoryStream(Image)))
            {
                NumberOfPages = img.GetFrameCount(FrameDimension.Page);//determine how many pages

            }
        }

        private Stream GetPageStream(int page)
        {
            using (Image image = Bitmap.FromStream(new MemoryStream(Image)))
            {
                Guid objGuid = image.FrameDimensionsList[0];
                FrameDimension objDimension = new FrameDimension(objGuid);
                image.SelectActiveFrame(objDimension, page);

                MemoryStream ms = new MemoryStream();
                PixelFormat myformat = image.PixelFormat;
                System.Drawing.Imaging.Encoder enc = System.Drawing.Imaging.Encoder.Compression;
                EncoderParameters ep = new EncoderParameters(1);
                if (myformat == PixelFormat.Format1bppIndexed)
                {
                    ep.Param[0] = new EncoderParameter(enc, (long)EncoderValue.CompressionCCITT4);
                    image.Save(ms, GetCodecInfo(ImageFormat.Tiff), ep);
                }
                else
                    image.Save(ms, ImageFormat.Jpeg);

                ms.Position = 0;
                return ms;
            }
        }
    }
}
