﻿using WFS.RecHub.Common;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.ImportToolkit.Common.Interfaces;

namespace WFS.LTA.DataImport.DataImportServicesAPI
{
    public class ServiceSetupContext
    {
        public IExternalLogonService LogonService { get; set; }
        public IDataImportRepository DataImportRepository { get; set; }
        public cSiteOptions SiteOptions { get; set; }
        public IAlertable Alert { get; set; }
        public IAuditable Audit { get; set; }
        public IWorkgroupRetrievable WorkgroupRetrieval { get; set; }
        public string ClientSetupSchemaVersion { get; set; }
        public string BatchSchemaVersion { get; set; }
    }
}
