﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using WFS.LTA.DataImport.DataImportServicesAPI.DTO;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

namespace WFS.LTA.DataImport.DataImportServicesAPI
{
    public interface IDataImportRepository
    {
        /// <summary>
        /// Gets the batch data XSD.
        /// </summary>
        /// <param name="xsdVersion">The XSD version.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// results of the store procedure call
        /// </returns>
        PossibleResult<IEnumerable<ImportSchemasDto>> GetBatchDataXsd(string xsdVersion);

        /// <summary>
        /// Gets the client setup XSD.
        /// </summary>
        /// <param name="xsdVersion">The XSD version.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// results of the store procedure call
        /// </returns>
        PossibleResult<IEnumerable<ImportSchemasDto>> GetClientSetupXsd(string xsdVersion);

        /// <summary>
        /// Gets the batch data XSL.
        /// </summary>
        /// <param name="xslVersion">The XSL version.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// results of the store procedure call
        /// </returns>
        PossibleResult<IEnumerable<ImportSchemasDto>> GetBatchDataXSL(string xslVersion);

        /// <summary>
        /// Gets the client setup XSL.
        /// </summary>
        /// <param name="xslVersion">The XSL version.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// results of the store procedure call
        /// </returns>
        PossibleResult<IEnumerable<ImportSchemasDto>> GetClientSetupXSL(string xslVersion);

        /// <summary>
        /// Data import insert client setup.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <param name="auditDateKey">The audit date key.</param>
        /// <returns>
        /// results of the store procedure call
        /// </returns>
        bool DataImportInsertClientSetup(string xml, int auditDateKey);

        /// <summary>
        /// Data import insert batch.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <param name="auditDateKey">The audit date key.</param>
        /// <returns>
        /// results of the store procedure call
        /// </returns>
        bool DataImportInsertBatch(XmlDocument doc, int auditDateKey);

        /// <summary>
        /// Data import response batch.
        /// </summary>
        /// <param name="clientProcessCode">The client process code.</param>
        /// <param name="dt">The data table</param>
        /// <returns>
        /// results of the store procedure call
        /// </returns>
        PossibleResult<DataTable> DataImportResponseBatch(string clientProcessCode);

        /// <summary>
        /// Returns a list of client-setup files whose processing is complete on the
        /// server, and that are ready for the client to complete processing.
        /// </summary>
        /// <param name="clientProcessCode">The client process code. May be null or empty.</param>
        ClientSetupResponses DataImportResponseClient(string clientProcessCode);

        /// <summary>
        /// Data import set response complete.
        /// </summary>
        /// <param name="parmResponseTrackingID">The parameter response tracking ID.</param>
        /// <param name="clientKnowsAboutRecord">if set to <c>true</c> [client knows about record].</param>
        /// <returns>
        /// results of the store procedure call
        /// </returns>
        bool DataImportSetResponseComplete(Guid parmResponseTrackingID, bool clientKnowsAboutRecord);

        bool DataImportSetResponseComplete(Guid responseTrackingId, Nullable<Guid> entityTrackingId, Nullable<Guid> sourceTrackingId, bool clientKnowsAboutRecord);
        
        /// <summary>
        /// Data import request document types.
        /// </summary>
        /// <param name="loadDate">The load date.</param>
        /// <param name="dt">The data table</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        PossibleResult<DataTable> DataImportRequestDocumentTypes(DateTime? loadDate);

        /// <summary>
        /// Data import get image RPS alias mappings.
        /// </summary>
        /// <param name="siteBankID">The site bank ID.</param>
        /// <param name="siteLockboxId">The site lockbox id.</param>
        /// <param name="modficiationDate">The modification date.</param>
        /// <param name="dt">The data table</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        PossibleResult<DataTable> DataImportGetImageRPSAliasMappings(Int32 siteBankID, Int32 siteLockboxId, DateTime? modificationDate);

        PossibleResult<DataTable> GetItemDataSetupField(string sBatchSourceName, DateTime? modificationDate);
        PossibleResult<DataTable> GetBatchDataSetupField(string sBatchSourceName, DateTime? modificationDate);

        bool InsertBatchMeasurement(BatchFileImportMeasures batchFileImportMeasures);
    }
}
