﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using WFS.LTA.DataImport.DataImportServicesAPI.DTO;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.RecHub.ImportToolkit.Common.Interfaces;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Chris Colombo
* Date:     02/03/2014
*
* Purpose:
*
* Modification History
* WI 128535 CMC 02/03/2014
*    - New class to replace obsolete cDataImportServices class.
*    - Key refactors include:
*       1) abstracted data access layer (IDataImportRepository)
*       2) abstracted security context (ISecurityValidation)
*       3) abstracted site options
*
* WI 130084 CMC 02/18/2014
*  - Change xsl version to 2.01.00.00.
* WI 155192 CMC 07/28/2014
*  - Refactoring to remove logon dependencies.
******************************************************************************/

namespace WFS.LTA.DataImport.DataImportServicesAPI
{
    public class DataImportServices : IDisposable
    {
        private cSiteOptions siteOptions = null;
        private cEventLog eventLog = null;

        private string siteKey = string.Empty;
        private cSession appSession = null;
        private Guid sessionID = Guid.Empty;
        private readonly IDataImportRepository repository;
        private readonly IExternalLogonService logonService;
        private readonly IAlertable alert;
        private readonly IAuditable audit;
        private readonly IWorkgroupRetrievable workgroupRetrieval;

        // Track whether Dispose has been called.
        private bool disposed = false;

        private const string ServiceKey = "DataImportServices";


        private string _SiteKey = string.Empty;

        private string clientSchemaVersion;
        private string batchSchemaVersion;

        public DataImportServices(ServiceSetupContext paramServiceSetupContext)
        {
            if (paramServiceSetupContext == null)
                throw new ArgumentNullException("paramServiceSetupContext", "Service setup context must be injected.");

            if (paramServiceSetupContext.LogonService == null)
                throw new ArgumentNullException("paramServiceSetupContext.LogonService", "Logon service must be injected.");

            if (paramServiceSetupContext.SiteOptions == null)
                throw new ArgumentNullException("paramServiceSetupContext.SiteOptions", "Site options must be injected.");

            if (paramServiceSetupContext.DataImportRepository == null)
                throw new ArgumentNullException("paramServiceSetupContext.SiteOptions", "Data Import repository must be injected.");

            if (paramServiceSetupContext.Alert == null)
                throw new ArgumentNullException("paramServiceSetupContext.Alert", "Alert module must be injected.");

            if (paramServiceSetupContext.Audit == null)
                throw new ArgumentNullException("paramServiceSetupContext.Audit", "Audit module must be injected.");

            if (paramServiceSetupContext.WorkgroupRetrieval == null)
                throw new ArgumentNullException("paramServiceSetupContext.WorkgroupRetrieval", "WorkgroupRetrieval module must be injected.");

            if (string.IsNullOrEmpty(paramServiceSetupContext.ClientSetupSchemaVersion))
                throw new ArgumentNullException("paramServiceSetupContext.ClientSetupSchemaVersion", "ClientSetupSchemaVersion must be configured.");

            if (string.IsNullOrEmpty(paramServiceSetupContext.BatchSchemaVersion))
                throw new ArgumentNullException("paramServiceSetupContext.BatchSchemaVersion", "BatchSchemaVersion must be configured.");

            logonService = paramServiceSetupContext.LogonService;
            siteOptions = paramServiceSetupContext.SiteOptions;
            repository = paramServiceSetupContext.DataImportRepository;
            alert = paramServiceSetupContext.Alert;
            audit = paramServiceSetupContext.Audit;
            workgroupRetrieval = paramServiceSetupContext.WorkgroupRetrieval;
            batchSchemaVersion = paramServiceSetupContext.BatchSchemaVersion;
            clientSchemaVersion = paramServiceSetupContext.ClientSetupSchemaVersion;
        }

        public bool IsSessionValid(SessionRequest session)
        {
            var response = logonService.ValidateSession(session);
            if (response.SessionStatus == SessionStatus.PREVIOUSLY_ENDED)
                throw new SessionExpiredException();

            return response.Status == StatusCode.SUCCESS ? true : false;
        }

        /// <summary>
        /// Determines the section of the local .ini file to be used for local
        /// options.
        /// </summary>
        public string SiteKey
        {
            set { SiteKey = value; }
            get { return _SiteKey; }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog EventLog {
            get {
                if (eventLog == null) {
                    eventLog = new cEventLog(siteOptions.logFilePath,
                                              siteOptions.logFileMaxSize,
                                              (MessageImportance)siteOptions.loggingDepth);
                }

                return eventLog;
            }
        }


        public bool GetBatchSchemaDefinitionFromDatabase(string batchXSDVersion, out XmlDocument BatchDataXsd)
        {
            bool bolRetVal;
            XmlDocument docBatchDataXsd;

            try
            {
                var xsd = repository.GetBatchDataXsd(batchXSDVersion).GetResult(new List<ImportSchemasDto>());

                if (xsd != null )
                {
                    if (xsd.Any())
                    {
                        docBatchDataXsd = new XmlDocument();
                        docBatchDataXsd.SafeLoadXml(xsd.ToList()[0].ImportSchema);
                        bolRetVal = true;
                    }
                    else
                    {
                        EventLog.logEvent("Batch Data Xsd was not found in the database.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        docBatchDataXsd = null;
                        bolRetVal = false;
                    }
                }
                else
                {
                    EventLog.logEvent("Unable to retreive Batch Data Xsd from the database.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    docBatchDataXsd = null;
                    bolRetVal = false;
                }

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchSchemaDefinition");
                throw (new SystemErrorException());
            }

            BatchDataXsd = docBatchDataXsd;
            return (bolRetVal);
        }

        public bool GetClientSchemaDefinition(string clientXSDVersion,
                                         out XmlDocument ClientSetupXsd)
        {
            bool bolRetVal;
            XmlDocument docClientSetupXsd;

            try
            {
                var xsd = repository.GetClientSetupXsd(clientXSDVersion).GetResult(new List<ImportSchemasDto>());

                if (xsd != null)
                {
                    if (xsd.Any())
                    {
                        docClientSetupXsd = new XmlDocument();
                        docClientSetupXsd.SafeLoadXml(xsd.ToList()[0].ImportSchema);
                        bolRetVal = true;
                    }
                    else
                    {
                        EventLog.logEvent("Client Setup Xsd was not found in the database.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        docClientSetupXsd = null;
                        bolRetVal = false;
                    }
                }
                else
                {
                    EventLog.logEvent("Unable to retreive Client Setup Xsd from the database.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    docClientSetupXsd = null;
                    bolRetVal = false;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetSchemaDefinitions");
                throw (new SystemErrorException());
            }

            ClientSetupXsd = docClientSetupXsd;
            return (bolRetVal);
        }

        public bool SendClientSetup(XmlDocument docXml, int auditDateKey)
        {

            bool bolRetVal = false;
            XmlNode onlineColorModeNode = null;
            XmlNodeList dataTypeNodeList = null;
            int tempInt = 0;
            bool bValidWorkgroupColorMode = true;
            bool bValidDataEntryDataType = false;

            try
            {

                onlineColorModeNode = docXml.SelectSingleNode("ClientGroups/ClientGroup/Client/@OnlineColorMode");

                if (onlineColorModeNode != null)
                {
                    if (Int32.TryParse(onlineColorModeNode.Value, out tempInt))
                    {
                        if (Enum.IsDefined(typeof(WorkgroupColorMode), tempInt))
                        {
                            bValidWorkgroupColorMode = true;
                        }
                        else
                        {
                            bValidWorkgroupColorMode = false;
                        }
                    }

                }
                if (!bValidWorkgroupColorMode)
                {
                    throw new OnlineFormattedException("Lockbox Color mode is not valid");
                }

                dataTypeNodeList = docXml.SelectNodes("ClientGroups/ClientGroup/Client/DataEntryColumns/DataEntryColumn/@DataType");
                if (dataTypeNodeList != null && dataTypeNodeList.Count > 0)
                {
                    foreach (XmlNode dataTypeNode in dataTypeNodeList)
                    {
                        if (Int32.TryParse(dataTypeNode.Value, out tempInt))
                        {
                            if (Enum.IsDefined(typeof(WFSDataTypes), tempInt))
                            {
                                bValidDataEntryDataType = true;
                            }
                            else
                            {
                                bValidDataEntryDataType = false;
                                break;
                            }
                        }
                        else
                        {
                            bValidDataEntryDataType = false;
                            break;
                        }

                    }
                }
                else
                {
                    bValidDataEntryDataType = true;
                }

                if (!bValidDataEntryDataType)
                {
                    throw new OnlineFormattedException("Data Type DataEntryColumn is not valid. Data Type Node Value is  " + tempInt.ToString());
                }
                

                var xsd = repository.GetClientSetupXSL(clientSchemaVersion).GetResult(new List<ImportSchemasDto>());
                if (xsd == null || !xsd.Any())
                {
                    throw new OnlineFormattedException("Failed to get client setup XSL transform");
                }

                if (xsd.Any() && xsd.ToList()[0].ImportSchema != null)
                {
                    string clientXSL = xsd.ToList()[0].ImportSchema;
                    docXml.InnerXml = TransformXML(clientXSL, docXml.InnerXml);
                    bolRetVal = true;
                }
                else
                    throw new OnlineFormattedException(string.Format("Failed to get client setup XSL version {0}", clientSchemaVersion));

                if (bolRetVal && repository.DataImportInsertClientSetup(docXml.InnerXml, auditDateKey))
                {
                    bolRetVal = true;
                }
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "SendClientSetup");
                throw (new SystemErrorException());
            }

            return (bolRetVal);
        }

        private string TransformXML(string xslTransformString, string inputXML)
        {

            string returnString = string.Empty;
            try
            {
                XmlDocument docXml = new XmlDocument();
                XmlDocument docXsl = new XmlDocument();
                XPathNavigator navXsl = docXsl.CreateNavigator();
                docXsl.SafeLoadXml(xslTransformString);
                docXml.SafeLoadXml(inputXML);
                XslCompiledTransform transXsl = new XslCompiledTransform();
                transXsl.Load(navXsl, null, null);
                XPathNavigator navXml = docXml.CreateNavigator();

                //CR 51697 Created PathDocument from Document for speedier transformations.
                XPathDocument pathDocXml = new XPathDocument(new XmlNodeReader(docXml));
                navXml = pathDocXml.CreateNavigator();
                using (StringWriter sw = new StringWriter())
                {
                    transXsl.Transform(pathDocXml, null, sw);
                    returnString = sw.ToString();
                }

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "TransformXML");
                throw (new SystemErrorException());
            }


            return returnString;
        }

        /// <summary>
        /// </summary>
        /// <param name="batchDataXml"></param>
        /// <returns></returns>
        public bool SendBatchData(XmlDocument batchDataXml, int auditDateKey)
        {

            bool bolRetVal = true;

            try
            {
                var xsd = repository.GetBatchDataXSL(batchSchemaVersion).GetResult(new List<ImportSchemasDto>());

                if (xsd == null || !xsd.Any())
                {
                    throw new OnlineFormattedException("Failed to get batch data XSL transform");
                }
                
                if (xsd.Any() && xsd.ToList()[0].ImportSchema != null )
                {
                    string batchXSL = xsd.ToList()[0].ImportSchema;
                    // Ok so we have 3 issues here.
                    //   - TransformXML() cannot handle large files at all. (Out of memory error)
                    //   - TransformXML() cannot be executed for each batch, is it just takes forever.
                    //   - DataImportInsertBatch() cannot be executed for each batch, as that also takes forever.
                    // So, we're going to break up the XML document into a list of documents based on a byte counter
                    // and transform and send those files as chunks.
                    // Setting the bytecounter to 1MB.
                    var max_bytes = 1048576;
                    var currentbytes = 0;
                    var documents = new List<XmlDocument>();
                    var currentdoc = new XmlDocument();
                    var batcheselement = currentdoc.ImportNode(batchDataXml.SelectSingleNode("/Batches"), false);
                    currentdoc.AppendChild(batcheselement);
                    documents.Add(currentdoc);
                    
                    var batches = batchDataXml.SelectNodes("/Batches/Batch");
                    foreach (XmlNode batch in batches)
                    {
                        var node = currentdoc.ImportNode(batch, true);
                        batcheselement.AppendChild(node);

                        // Check the bytecounts and totals to see if we need to create a new document.
                        currentbytes += node.InnerXml.Length;
                        if (currentbytes > max_bytes)
                        {
                            currentdoc = new XmlDocument();
                            batcheselement = currentdoc.ImportNode(batchDataXml.SelectSingleNode("/Batches"), false);
                            currentdoc.AppendChild(batcheselement);
                            documents.Add(currentdoc);
                            currentbytes = 0;
                        }
                    }

                    // Now we have a nice list of documents, just transform them all and pass them through.
                    documents = documents
                        .Where(d => d.SelectNodes("/Batches/Batch").Count > 0)
                        .ToList();
                    foreach (var doc in documents)
                        doc.InnerXml = TransformXML(batchXSL, doc.InnerXml);
                    foreach (var doc in documents)
                        bolRetVal = bolRetVal && repository.DataImportInsertBatch(doc, auditDateKey);
                }
                else
                {
                    bolRetVal = false;
                }

            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "SendBatchData");
                throw (new SystemErrorException());
            }

            return bolRetVal;
        }

        /// <summary>
        /// Returns a list of client-setup files whose processing is complete on the
        /// server, and that are ready for the client to complete processing.
        /// </summary>
        /// <param name="clientProcessCode">The client process code. May be null or empty.</param>
        public ClientSetupResponses GetClientSetupResponses(string clientProcessCode)
        {
            try
            {
                return repository.DataImportResponseClient(clientProcessCode);
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetClientSetupResponses");
                throw new SystemErrorException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="batchResponseXML"></param>
        /// <returns></returns>
        public bool GetBatchResponse(string clientProcessCode, out DataTable dt)
        {
            try
            {
                dt = repository.DataImportResponseBatch(clientProcessCode).GetResult(HandleNoResult);
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchResponse");
                throw (new SystemErrorException());
            }

            return true;
        }


        /// <summary>
        ///
        /// </summary>
        /// <param name="uniqueIDResponseComplete"></param>
        /// <returns></returns>
        public bool DataImportSetResponseComplete(Guid uniqueIDResponseComplete, bool clientKnowsAboutRecord)
        {

            return DataImportSetResponseComplete(new UpdateStatusContext
                {ResponseTrackingId = uniqueIDResponseComplete, ClientKnowsAboutRecord = clientKnowsAboutRecord});
        }

        public bool DataImportSetResponseComplete(UpdateStatusContext context)
        {
            bool bolRetVal = false;
            try
            {

                if (context == null)
                    throw new ArgumentNullException("Must provide a context in order to set the status complete.");

                if (repository.DataImportSetResponseComplete(context.ResponseTrackingId, context.EntityTrackingId, context.SourceTrackingId, context.ClientKnowsAboutRecord))
                {
                    bolRetVal = true;
                }
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DataImportSetResponseComplete");
                throw (new SystemErrorException());
            }

            return (bolRetVal);
        }

        public bool SendBatchMeasureData(BatchFileImportMeasures batchFileImportMeasures)
        {
            bool bolRetVal = false;

            try
            {

                bolRetVal = repository.InsertBatchMeasurement(batchFileImportMeasures);

            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "SendBatchMeasureData");
                throw (new SystemErrorException());
            }
            return (bolRetVal);
        }

        public void SendBatchImages(IEnumerable<UploadRequest> items)
        {
            try
            {
                var imageDirectorySource = CreateImageDirectorySource();
                // The client has already split multi-page TIFFs for us
                foreach (var item in items)
                {
                    var result = SaveImage(item.ImageInfo, item.ImageData, imageDirectorySource);
                    if (!result)
                    {
                        throw new InvalidOperationException("Error uploading image: " + item.ImageInfo);
                    }
                }
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "PutBatchImages");
                throw new SystemErrorException();
            }
        }

        public bool GetDocumentTypes(DateTime? loadDate, out DataTable dt)
        {
            try
            {
                dt = repository.DataImportRequestDocumentTypes(loadDate).GetResult(HandleNoResult);
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetDocumentTypes");
                throw (new SystemErrorException());
            }

            return true;
        }

        public bool GetImageRPSAliasMappings(Int32 siteBankID, Int32 siteLockboxId, DateTime? modificationDate, out DataTable dt)
        {
            try
            {
                dt = repository.DataImportGetImageRPSAliasMappings(siteBankID, siteLockboxId, modificationDate).GetResult(HandleNoResult);
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetImageRPSAliasMappings");
                throw (new SystemErrorException());
            }

            return true;
        }

        private DataTable HandleNoResult()
        {
            var ds = new DataSet();
            ds.Tables.Add(new DataTable());
            return ds.Tables[0];
        }

        public bool GetBatchDataSetupField(string batchSourceName, DateTime? modificationDate, out DataTable dt)
        {
            try
            {
                dt = repository.GetBatchDataSetupField(batchSourceName, modificationDate).GetResult(HandleNoResult);
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchDataSetupField");
                throw (new SystemErrorException());
            }

            return true;
        }

        public bool GetItemDataSetupField(string batchSourceName, DateTime? modificationDate, out DataTable dt)
        {
            try
            {
                dt = repository.GetItemDataSetupField(batchSourceName, modificationDate).GetResult(HandleNoResult);
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetItemDataSetupField");
                throw (new SystemErrorException());
            }

            return true;
        }

        public XmlElement BuildXMLNode(XmlDocument doc, DateTime processingDate, DateTime depositDate,
                    DateTime batchDate, Int32 siteBankID, Int32 siteLockboxId, Int32 batchID)
        {
            const int ACTION_CODE_DELETE = 4;
            const int NOTIFY_IMS = 0;
            const int KEEP_STATS = 0;
            XmlElement el = doc.CreateElement("Batch");
            el.SetAttribute("ProcessingDate", processingDate.ToString("yyyy-MM-dd"));
            el.SetAttribute("DepositDate", depositDate.ToString("yyyy-MM-dd"));
            el.SetAttribute("SourceProcesingDate", batchDate.ToString("yyyy-MM-dd"));
            el.SetAttribute("BankID", siteBankID.ToString());
            el.SetAttribute("LockboxID", siteLockboxId.ToString());
            el.SetAttribute("BatchID", batchID.ToString());
            el.SetAttribute("ActionCode", ACTION_CODE_DELETE.ToString());
            el.SetAttribute("NotifyIMS", KEEP_STATS.ToString());
            el.SetAttribute("KeepStats", NOTIFY_IMS.ToString());
            el.SetAttribute("QueuedTime", DateTime.Now.ToString());
            return el;
        }

        public MultiImageResponse MultiImageUpload(MultiImageRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("Must provide a valid image request.");

            if (request.Images == null)
                throw new ArgumentNullException("Must provide a valid image request list.");

            var imageDirectorySource = CreateImageDirectorySource();

            var imageResults = new List<ImageUploadResponse>();

            foreach (var image in request.Images)
            {
                imageResults.Add(SplitAndSaveImage(image, imageDirectorySource));
            }

            return new MultiImageResponse { ImageUploadResults = imageResults };

        }
        private ImageUploadResponse SplitAndSaveImage(ImageUploadRequest request, ImageDirectorySource imageDirectorySource)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            bool isSuccessfullyWritten = false;
            var imageInfo = new ImageInfo(request.ImageData);
            if (imageInfo.NumberOfPages == 1)
            {
                isSuccessfullyWritten = SaveImage(request.ImageInfo, request.ImageData, imageDirectorySource);
            }
            else if (imageInfo.NumberOfPages > 1)
            {
                var imageFront = request.ImageInfo.Clone();
                var imageBack = request.ImageInfo.Clone();
                imageFront.IsBack = false;
                imageBack.IsBack = true;

                isSuccessfullyWritten =
                    SaveImage(imageFront, imageInfo.GetFrontPageBytes(), imageDirectorySource) &&
                    SaveImage(imageBack, imageInfo.GetBackPageBytes(), imageDirectorySource);
            }

            return new ImageUploadResponse
            {
                ImageInfo = request.ImageInfo,
                ImageFileName = request.ImageFileName,
                IsSuccessfullyWritten = isSuccessfullyWritten,
            };
        }
        private ImageDirectorySource CreateImageDirectorySource()
        {
            var filegroupDirectoryDatabaseReader = new FilegroupDirectoryDatabaseReader(SiteKey, EventLog);
            var source = new ImageDirectorySource(siteOptions.imageStorageMode, siteOptions.imagePath,
                filegroupDirectoryDatabaseReader, EventLog);
            return source;
        }
        private bool SaveImage(ImageInfoContract imageInfoContract, byte[] imageData,
            ImageDirectorySource imageDirectorySource)
        {
            var imageDirectory = imageDirectorySource.GetImageDirectory(
                imageInfoContract.BankID, imageInfoContract.LockboxID);

            using (cipoImages objImages = new cipoImages(this.SiteKey, siteOptions.imageStorageMode, imageDirectory))
            {
                return objImages.PutBatchImages(imageInfoContract, imageData);
            }
        }

        private byte[] ReadToEnd(System.IO.Stream stream)
        {

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "ReadToEnd");
                throw (new SystemErrorException());
            }
        }

        public AlertResponse LogAlert(AlertRequest requestContext)
        {
            return alert.Log(requestContext);
        }

        public AuditResponse LogAuditEvent(AuditRequest requestContext)
        {
            return audit.LogAuditEvent(requestContext);
        }

        public WorkgroupResponse GetWorkgroupInfo(WorkgroupRequest requestContext)
        {
            return workgroupRetrieval.GetWorkgroupInfo(requestContext);
        }

        private cSession AppSession {
            get {
                if(appSession == null) {
                    appSession = new cSession();
                }

                return(appSession);
            }
        }


        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose() {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing) {
            // Check to see if Dispose has already been called.
            if(!this.disposed) {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if(disposing) {
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }

    }
}
