﻿using System;

namespace WFS.LTA.DataImport.DataImportServicesAPI.DTO
{
    public class DataImportQueueDto
    {
        public Guid ResponseTrackingID { get; set; }
        public Guid SourceTrackingID { get; set; }
        public int AuditDateKey { get; set; }
        public string XMLResponseDocument { get; set; }
    }
}
