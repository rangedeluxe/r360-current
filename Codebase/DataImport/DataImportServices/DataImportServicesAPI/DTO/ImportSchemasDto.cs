﻿

namespace WFS.LTA.DataImport.DataImportServicesAPI.DTO
{
    public class ImportSchemasDto
    {
        public string ImportSchema { get; set; }
    }
}
