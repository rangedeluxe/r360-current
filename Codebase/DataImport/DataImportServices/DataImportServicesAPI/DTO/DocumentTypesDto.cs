﻿
namespace WFS.LTA.DataImport.DataImportServicesAPI.DTO
{
    public class DocumentTypesDto
    {
        public string FileDescriptor { get; set; }
        public string IMSDocumentType { get; set; }
    }
}
