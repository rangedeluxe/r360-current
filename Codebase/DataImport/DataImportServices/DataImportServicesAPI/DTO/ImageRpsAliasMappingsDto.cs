﻿

namespace WFS.LTA.DataImport.DataImportServicesAPI.DTO
{
    public class ImageRpsAliasMappingsDto
    {
        public string ImportSchema { get; set; }
        public int SiteClientAccountID { get; set; }
        public int ExtractType { get; set; }
        public int DocType { get; set; }
        public string FieldType { get; set; }
        public string AliasName { get; set; }
    }
}
