using System;
using System.IdentityModel.Services;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using R360UI.Utils;
using Wfs.Logging;
using Wfs.Logging.NLog;
using Wfs.Raam.Core;
using Wfs.Raam.Core.WCF;
using Wfs.Raam.Service.IdentityServer.Contracts.ServiceContracts;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using WFS.RecHub.SessionMaintenance.Common;
using WFS.RecHub.SessionMaintenance.ServiceClient;

namespace Issuer.Controllers
{
    /// <summary>
    /// This controller only has a purpose for handling RAAM authentication.
    /// Index:
    ///   - Sends the user to the issuer (raam) to login if we're not authenticated.
    ///   - Accepts the post-back from the issuer (raam) and creates the auth cookie.
    ///   - Redirects the user back to index (itself) after the cookie is created.
    ///   - Calls session maintenance to produce our client account entitlements.
    /// </summary>
    public class MainController : Controller
    {
        private readonly ISessionMaintenance sessionMaintenance;
        private readonly IR360Services services;
        private readonly IWfsLog log;
        public MainController()
        {
            sessionMaintenance = new SessionMaintenanceManager();
            services = new R360ServiceManager();
            log = new WfsLog();
        }

        [Authorize]
        public ActionResult Index()
        {
            try
            {
                if (SessionEstablished())
                    return File("index.html", "text/html");

                Logout(string.Empty);
                return new HttpUnauthorizedResult();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, "R360UI-EstablishSession");
                log.Error(ex.StackTrace);
                throw ex;
            }
        }

        [Authorize]
        public void Logout(string returnUrl)
        {
           var ssoApi = new SsoApiClient(log);
           var principal = ClaimsPrincipal.Current;
           var sid = principal.Claims.FirstOrDefault(x => x.Type.Equals(WfsClaimTypes.Sid));
           if (sid != null) ssoApi.DeleteSession(new Guid(sid.Value));

           // TODO - this was copied from Raam.Core's Signout method. Because Raam.Core
            //  doesnt support returnUrls. We need to update Raam.Core soon to allow them.
            FederatedAuthentication.SessionAuthenticationModule.DeleteSessionTokenCookie();
            FederatedAuthentication.SessionAuthenticationModule.CookieHandler.Delete();
            var authModule = FederatedAuthentication.WSFederationAuthenticationModule;

            // This is for security - the returnUrl needs to be relative.
            returnUrl = returnUrl
                ?.Replace(@"\", string.Empty)
                ?.Replace("/", string.Empty);

            if (authModule != null)
            {
                var signoutReply =
                    FederatedAuthentication.SessionAuthenticationModule.FederationConfiguration
                        .WsFederationConfiguration.SignOutReply;

                

                if (string.IsNullOrWhiteSpace(signoutReply))
                    WSFederationAuthenticationModule.FederatedSignOut(
                        new Uri(authModule.Issuer),
                        new Uri(new Uri(authModule.Realm), returnUrl)
                    );
                else
                {
                    var fedContract = ServiceFactory.CreateUniversal<IFederationContract>(ServiceFactory.DefaultConfigLocation);

                    fedContract.FederatedSignOut(signoutReply);

                    HttpContext.Response.Redirect(signoutReply);
                }
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult HasPermission(string permission, string type)
        {
            var hasperm = R360Permissions.Current.Allowed(permission, (R360Permissions.ActionType)Enum.Parse(typeof(R360Permissions.ActionType), type));
            return Json(hasperm);
        }

        [Authorize]
        [HttpPost]
        public JsonResult PageView(string page)
        {
            var response = services.WriteAuditEvent("Viewed Page", "Page View", "R360", page);
            return Json(new
            {
                success = response.Status == StatusCode.SUCCESS
            });
        }

        private bool SessionEstablished()
        {
            var response = sessionMaintenance.EstablishSession();
            if (response == null) return false;
            if (response.Status == StatusCode.FAIL) return false;

            return true;
        }
    }
}