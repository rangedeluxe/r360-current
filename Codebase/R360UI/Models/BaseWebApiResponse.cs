using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R360UI.Models
{
   public enum WebApiStatusCode
   {
      SUCCESS,
      FAIL
   }
   public class BaseWebApiResponse
   {
      public BaseWebApiResponse()
      {
         Status = WebApiStatusCode.FAIL;
         ErrorMessages = new List<string>();
         Response = String.Empty;
      }

      public WebApiStatusCode Status { get; set; }
      public List<string> ErrorMessages { get; set; }

      public string Response { get; set; }
        
   }
}