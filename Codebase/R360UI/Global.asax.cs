using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Wfs.Raam.Core;
using Wfs.Raam.TokenCache;

namespace Issuer
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            RouteTable.Routes.RouteExistingFiles = true;
            RouteTable.Routes.MapMvcAttributeRoutes();
            RouteTable.Routes.MapRoute(
                  name: "Index html",
                  url: "index.html",
                  defaults: new { controller = "Main", action = "Index" }
              );
            RouteTable.Routes.MapRoute(
                  name: "Default",
                  url: "{action}",
                  defaults: new { controller = "Main", action = "Index" }
              );
            WSPassiveSessionConfiguration.Start();
        }

        public override void Init()
        {
            WSPassiveSessionConfiguration.Init();
            base.Init();
        }

        protected void Application_PreSendRequestHeaders()
        {
           var allowFrom = string.Empty;
           var principal = Thread.CurrentPrincipal as ClaimsPrincipal;
           if (principal != null)
           {
              var identity = principal.Identity as ClaimsIdentity;
              if (identity != null)
              {
                 var allowFromClaim = identity.Claims.FirstOrDefault(c => c.Type == WfsClaimTypes.AllowFromUrlInFrame);
                 allowFrom = allowFromClaim == null ? "" : allowFromClaim.Value;
              }
           }

           if (!string.IsNullOrEmpty(allowFrom))
           {
              Response.Headers["X-Frame-Options"] = "ALLOW-FROM " + allowFrom;
              Response.Headers["Content-Security-Policy"] = "frame-ancestors " + allowFrom;
           }
           else
           {
              Response.Headers["X-Frame-Options"] = "SAMEORIGIN";
              Response.Headers["Content-Security-Policy"] = "frame-ancestors 'self'";
           }
        }
   }
}