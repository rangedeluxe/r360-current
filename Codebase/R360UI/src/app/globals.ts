
export class Globals {
   // RAAM
   static RAAM_LOGIN_URL:string = './';

	// Framework
	static TABS_URL:string = '/Framework/Framework/GetTabs';
	static USER_URL:string = '/Framework/Framework/GetUser';
	static PING_URL:string = '/Framework/Framework/Ping';
	static BRANDING_URL:string = '/Framework/Framework/GetBranding';
	static SESSIONINFO_URL:string = '/Framework/Framework/GetSessionInfo';
	static LOGOUT_URL:string = './Logout';
	static JWTSSO:string = '/Framework/Framework/GenerateJwtSsoToken';

	// R360
	static CHECK_PERMISSION_URL = './HasPermission';
	static AUDIT_PAGE_URL = './PageView';

	static USERPREFERENCES_GETPREFERENCES_URL:string = '/RecHubConfigViews/UserPreferences/GetPreferences';
	static USERPREFERENCES_RESTOREDEFAULTS_URL:string = '/RecHubConfigViews/UserPreferences/RestoreDefaults';
	static USERPREFERENCES_SAVEPREFERENCES_URL:string = '/RecHubConfigViews/UserPreferences/SavePreferences';

	static ALERTS_GETEVENTRULES_URL:string = '/RecHubConfigViews/Alerts/GetEventRules';
	static ALERTS_ADDALERT_URL: string = '/RecHubConfigViews/Alerts/AddAlert';
	static ALERTS_EDITEVENTRULE_URL : string = '/RecHubConfigViews/Alerts/EditAlert';
	static ALERTS_INSERTEVENTRULE_URL: string = '/RecHubConfigViews/Alerts/InsertEventRule';
	static ALERTS_UPDATEEVENTRULE_URL : string  = '/RecHubConfigViews/Alerts/UpdateEventRule';

   static DASHBOARD_SUMMARY_URL : string = '/RecHubViews/Dashboard/GetSummaryData';
   static RECEIVABLES_SUMMARY_URL : string = '/RecHubViews/ReceivablesSummary/GetSummaryData';

	// Batch Summary
	static PAYMENTSOURCES_URL : string = '/RecHubViews/BatchSummary/GetPaymentSources';
	static PAYMENTTYPES_URL : string = '/RecHubViews/BatchSummary/GetPaymentTypes';

	// Batch Detail
	static BATCHDETAILS_URL : string = '/RecHubViews/BatchDetail/GetBatchDetails';

	// Raam proxy
	static ENTITY_USER_URL : string = '/RecHubRaamProxy/api/EntityUser/'

  // Transaction Detail
   static TRANSACTIONDETAILS_URL : string = '/RecHubViews/TransactionDetail/GetTransactionDetail';

   // Breadcrumbs
   static LOCAL_STORAGE_BREADCRUMB_KEY: string = 'breadcrumb_local_storage';
   static BREADCRUMBS_RECHUBVIEWS_IDENTIFIER: string = 'RecHubViews';
   static BREADCRUMBS_SEARCHRESULTS_IDENTIFIER: string = 'Search Result';
   static LOCAL_STORAGE_ADVANCEDSEARCH_RESULTS: string = 'redirectorasesults';

   //Notifications
   static NOTIFICATION_URL: string = '/RecHubViews/Notifications/GetNotifications';
   static NOTIFICATIONFILETYPES_URL: string = '/RechubViews/Notifications/GetNotificationsFileTypes';   
   static NOTIFICATIONDETAIL_URL: string = '/RechubViews/Notifications/GetNotificationDetail';
}