// We add our 3rd party dependencies here. They will all be packed up into a JS file
// when webpack reads this file.
import '../node_modules/core-js/client/shim.min.js';
import 'zone.js/dist/zone';
import 'reflect-metadata';
import '../node_modules/systemjs/dist/system.js';