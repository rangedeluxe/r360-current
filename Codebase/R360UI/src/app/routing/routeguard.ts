
import { throwError as observableThrowError, forkJoin as observableForkJoin, Observable, observable } from 'rxjs';

import { catchError, map } from 'rxjs/operators';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { FrameworkService } from '../services/framework.service/framework.service';
import { TrimPipe } from '../pipes/trim.pipe';
import { PermissionService } from '../services/permission.service/permission.service';

import { Globals } from '../globals';

import { AuditService } from "../services/audit.service/audit.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { TokenService } from '../services/token.service/token.service';

declare const window;

@Injectable()
export class RouteGuard implements CanActivate {
  private router: Router;
  private trimpipe: TrimPipe;
  private canaccess: string;
  private routeMappings: any = [
    { route: 'Dashboard', permission: 'Dashboard' },
    { route: 'UserPreferences', permission: 'UserPreferences' },
    { route: 'AlertManager', permission: 'Alerts' },
    { route: 'AccessDenied', permission: 'AccessDenied' },
    { route: 'BatchSummary', permission: 'BatchSummary' },
    { route: 'BatchDetail', permission: 'BatchSummary' },
    { route: 'TransactionDetail', permission: 'BatchSummary' },
    { route: 'PayerMaintenance', permission: 'BatchSummary' },
    { route: 'Notifications', permission: 'Notifications' },
    { route: 'WorkgroupMaintenance', permission: 'WorkgroupMaintenance'},
    { route: 'NotificationDetail', permission: 'Notifications' }
  ];


  private permissionService: PermissionService;
  private frameworkService: FrameworkService;
  private auditService: AuditService;
  private http: HttpClient;
  private tokenService: TokenService;

  constructor(
    router: Router,
    trimpipe: TrimPipe,
    permissionService: PermissionService,
    frameworkService: FrameworkService,
    auditService: AuditService,
    http: HttpClient,
    tokenService: TokenService
  ) {
    this.router = router;
    this.permissionService = permissionService;
    this.frameworkService = frameworkService;
    this.trimpipe = trimpipe;
    this.auditService = auditService;
    this.http = http;
    this.tokenService = tokenService;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.canaccess && route.url.toString() == this.canaccess) {
      console.log(`Router: Allowing access to '${route.url}'.`);
      this.canaccess = null;
      return true;
    }

    console.log(`Router: Checking URL: '${route.url}'.`);

    // First send the user to the loading without pushing a state into history
    this.router.navigateByUrl('Loading', { skipLocationChange: true });

    // Now we need to send a request to the server to request access to this route.
    let mapping = this.routeMappings.find((e) => { return e.route == route.url.toString() });
    if (!mapping) {
      console.log(`Router: ERROR: Attempted to access '${route.url}', but could not find the route.`);
      return false;
    }

    // Fire off all requests, allow them to finish with ForkJoin.
    observableForkJoin(
      this.permissionService.hasPermission(mapping.permission, 'View'),
      this.frameworkService.getTabs(),
      this.frameworkService.getSessionInfo(),
      this.tokenService.refreshToken())
      .subscribe(
        result => {
          if (result === undefined) {
            // no server response, relog in.
            console.log(`Router: Error communicating with Framework to gather Tabs, Session, and Permissions.`);
            window.location.href = './';
            return false;
          }
          let hasperm = result[0];
          let tabs: Array<any> = result[1].Data;
          let sessionalive: boolean = result[2].IsAlive;
          let sessionkeepaliveurl: string = result[2].KeepAliveURL;
          let sso: boolean = result[2].IsSso;
          let keepalivenot200: boolean = result[2].KeepAliveNot200;
          
          console.log('Router: sso = ' + sso);
          console.log('Router: Keep alive not 200 = ' + keepalivenot200 );

          // If we have permission to the page, and we ARE using keepalive, then we have to make some checks at that URL.
          if (hasperm === true) {
            // Session is not alive on the R360 end, so we'll just deny access and refresh the page. Should go back to the login page.
            if (!sessionalive) {
              console.log(`Router: Keep alive is enabled, but the R360 Session has already ended. Logging out...`);
              window.location.href = Globals.LOGOUT_URL;
              return false;
            }
            // If logic: Allow through if we have permission, we're not using keepalive, and we're not SSO.
            // But also - we need to be SSO and the Keep Alive URL has to be set.
            if (((!sessionkeepaliveurl || sessionkeepaliveurl.length === 0) || sso === false)) {
              this.openRoute(route);
            }
            else {

              // Session is alive in R360, so we need to check the KeepAliveURL as well.
              console.log(`Router: Keep alive URL '${sessionkeepaliveurl}' is enabled, attempting communication...`);
              //Making this an http head request because we don't care about the 
              //body of the request anyway in the case of the keep alive url.
              this.http.head(sessionkeepaliveurl, { withCredentials: true, observe: 'response' }).pipe(
                map((res) => {
                  // Check the status, make sure the user can come in.
                  console.log('Router: Keep alive URL response: Success ');
                  if (keepalivenot200) {
                    if (res.status === 200) {
                      this.openRoute(route);
                    }
                    else {
                      // Session is dead on their end, so we need to logout.
                      console.log(`Router: Keep alive URL '${sessionkeepaliveurl}' returned status ${res.status}. Logging out...`);
                      window.location.href = Globals.LOGOUT_URL;
                    }
                  }
                  else {
                    console.log('Route:  Keep alive communiction: Success (noFail)');
                    this.openRoute(route);
                  }

                }), catchError((err) => {
                  // Server error communicating with KeepAlive endpoint.
                  console.log(`Router: Server Error while communicating to the KeepAlive Endpoint:
                  '${sessionkeepaliveurl}'. Status: ${err.status}.`);
                  if (keepalivenot200) {
                    window.location.href = Globals.LOGOUT_URL;
                    return observableThrowError(err);
                  }
                  else {
                    console.log('Route:  Error found but (noFail)')
                    this.openRoute(route);
                    return observableThrowError(err);
                  }
                })).subscribe();

            }
          }
          // We can't access the page, so lets try to find a page we can access.
          else {
            this.openDefaultRoute(route, tabs);
          }

        },
        error => {
          // log back in.
          console.log(`Router: Error communicating with Framework to gather Tabs, Session, and Permissions.`);
          console.dir(error);
          window.location.href = './';
        });

    // While we're checking if we have access to the route, return false so that user can't get there.
    return false;
  }

  // Function if we don't have access to the requested route, but we'll try to find one we do have access too.
  private openDefaultRoute(route: ActivatedRouteSnapshot, tabs: Array<any>) {
    console.log(`Router: ERROR: Access denied on route '${route.url}'.`);
    let redir = this.findFirstRoute(tabs, route.url.toString());
    if (redir) {
      console.log(`Router: Attempting to redirect to route '${redir.Name}' instead.`);
      if (redir.IsLink)
        this.router.navigateByUrl(redir);
      else
        window.location.href = `/Framework/?tab=${redir.Id}`;
    }
  }

  // Function to open the door to the requested page.
  private openRoute(route: ActivatedRouteSnapshot) {
    this.auditService.auditPageView(route.url.toString()).subscribe((res) => {
      console.log(`Router: Audited page view to '${route.url.toString()}'. Result: ${res.success}`);
    });
    this.canaccess = route.url.toString();
    this.router.navigateByUrl(route.url.toString());
  }

  // A little recursive function to find the first route a user has access to.
  private findFirstRoute(tabs: any, failedroute: string): any {
    let routes = tabs.map(d => {
      return d.Name ? d
        : d.Children ? this.findFirstRoute(d.Children, failedroute)
          : d.length && d.length > 0 ? this.findFirstRoute(d, failedroute)
            : null;
    }).filter(d => { return d != null && d.Name != failedroute; });
    return routes.length > 0 ? routes[0] : null;
  }

}
