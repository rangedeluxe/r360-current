import { Injectable } from '@angular/core';
import { LocalStorageService } from '../localstorage.service/localstorage.service';
import { BreadcrumbService } from '../breadcrumb.service/breadcrumb.service';
import { Pages } from '../../DTOs/Pages';



@Injectable()
export class SharedStorageService {
   private regPages: Array<any> = [
      { key: Pages.Dashboard, query: 'dashboard-saved-query', breadcrumb: 'Dashboard' },
      { key: Pages.RecSummary, query: '', breadcrumb: '' },
      { key: Pages.BatchSummary, query: 'batchsummarydata', breadcrumb: 'Batch Summary' },
      { key: Pages.BatchDetail, query: 'batchdetaildata', breadcrumb: 'Batch Detail' },
      { key: Pages.TransactionDetail, query: 'transactiondetaildata', breadcrumb: 'Transaction Detail' },
      { key: Pages.Notifications, query: 'notificationsdata', breadcrumb: 'Notifications' },
      { key: Pages.NotificationDetail, query: 'notificationsdetaildata', breadcrumb: 'Transaction Detail' },
   ];


   constructor(private localStorageService: LocalStorageService,
      private breadcrumbService: BreadcrumbService) {
   }

   public GetData(page: Pages) {
      const storedvalues = this.regPages.find((element) => element.key === page);
      const bc = this.breadcrumbService.pop(storedvalues.breadcrumb);
      return bc
         ? bc.params
         : this.localStorageService.get(storedvalues.query);
   }
   public DeleteData(page: Pages) {
      const storedvalues = this.regPages.find((element) => element.key === page);
      this.localStorageService.delete(storedvalues.query);
   }
   public WriteData(key: string, data: any){
      data = typeof (data) == 'object' ? JSON.stringify(data) : data.toString();
      window.localStorage.setItem(key, data);
   }
}