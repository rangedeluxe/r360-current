import { Injectable } from '@angular/core';
import { HttpService } from '../http.service/http.service';
import { Globals } from '../../globals';

import { DataService } from '../http.data.service/data.service';

@Injectable()
export class FrameworkService {
    private http : DataService;
    constructor(http: DataService) {
        this.http = http;
    }

    getBranding() {
        return this.http.post<any>(Globals.BRANDING_URL, {});
    }

    getSessionInfo() {
        return this.http.post<any>(Globals.SESSIONINFO_URL, {});
    }

    getTabs() {
        return this.http.post<any>(Globals.TABS_URL, {});
    }

    getUser() {
        return this.http.post<any>(Globals.USER_URL, {});
    }

    ping() {
        return this.http.post<any>(Globals.PING_URL, { });
    }

    sendToSso(Id) {
        return this.http.post<any>(Globals.JWTSSO, {menuItemId: Id})
    }
}