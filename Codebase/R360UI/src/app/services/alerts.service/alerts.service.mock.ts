
import {of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Globals } from '../../globals';

import { HttpService } from '../http.service/http.service';
import { Alert } from "../../DTOs/Alert";
import { AlertsService } from "./alerts.service";
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class MockAlertsService extends AlertsService {
    constructor(http: DataService) {
        super(http);
    }

    getAlerts(showInactive:boolean) {
        return observableOf({
            Data: [
                {"ID":0,"EventRuleID":1,"Description":"operaaator","EventName":"PROCESSINGEXCEPTION","EventLongName":"Fatal System Error","SiteCodeID":-1,"SiteBankID":-1,"SiteClientAccountID":-1,"IsActive":true,"Table":null,"Column":null,"EventID":2,"Operator":null,"EventValue":null,"AssignedSIDs":null,"EntityID":null,"ParentHierarchy":"","WorkgroupName":null,"AssociatedUsers":null}
            ]
        });
    }

    addAlert() {
        return observableOf({
            Data: { 
                Events: [ { ID: 1, Name: "EXTRACTCOMPLETE", EventLongName: "Extract Complete", Type: 1, Level: 3, IsActive: true, Column: "", Message: "NONE", Operators: [], Schema: "", Table: "" } ]
            }
        });
    }

    editAlert(alertID :number) {
        return observableOf({
            Data: { 
                Events: [ { ID: 1, Name: "EXTRACTCOMPLETE", EventLongName: "Extract Complete", Type: 1, Level: 3, IsActive: true, Column: "", Message: "NONE", Operators: [], Schema: "", Table: "" } ],
                EventRule: { AssignedSIDS: [], AssociatedUsers: null, Column: "", Description: "blah", EntityID: null, EventID: 1, EventLongName: "Extract Complete", EventName: "EXTRACTCOMPLETE", EventRuleID: 73, EventValue: "", ID: 0, IsActive: true, Operator: "", ParentHierarchy: "", SiteBankID: 2, SiteClientAccountID: 666, SiteCodeID: -1, Table: "", WorkgroupName: null },
                AssignedUsers: []
            }
        });
    }

    insertAlert(alert:Alert) {
        return observableOf({ "Errors":null, "HasErrors":false, "Data":null });
    }

    updateAlert(alert:Alert) {
        return observableOf({ "Errors":null, "HasErrors":false, "Data":null });
    }
}