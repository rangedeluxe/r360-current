import { Injectable } from '@angular/core';
import { Globals } from '../../globals';

import { HttpService } from '../http.service/http.service';
import { Alert } from "../../DTOs/Alert";
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class AlertsService {
    private http : DataService;
    constructor(http: DataService) {
        this.http = http;
    }

    getAlerts(showInactive:boolean) {
        return this.http.post<any>(Globals.ALERTS_GETEVENTRULES_URL, { inactive: showInactive });
    }

    addAlert(){
        return this.http.post<any>(Globals.ALERTS_ADDALERT_URL,{});
    }

    editAlert(alertID :number){
        return this.http.post<any>(Globals.ALERTS_EDITEVENTRULE_URL, {eventRuleID: alertID });
    }
    insertAlert(alert:Alert){
        return this.http.post<any>(Globals.ALERTS_INSERTEVENTRULE_URL, {
            eventID: alert.EventID,
            description: alert.Description,
            isActive : alert.IsActive,
            eventOperator : alert.Operator.text,
            eventValue: alert.EventValue,
            siteBankID: alert.Workgroup.BankId,
            siteClientAcctID: alert.Workgroup.WorkgroupId,
            users: alert.UserIds
        });
    }
    updateAlert(alert:Alert){
        return this.http.post<any>(Globals.ALERTS_UPDATEEVENTRULE_URL,{
            eventRuleID : alert.EventRuleID,
            eventID : alert.EventID,
            description : alert.Description,
            isActive : alert.IsActive,
            eventOperator : alert.Operator.text,
            eventValue : alert.EventValue,
            siteBankID : alert.Workgroup.BankId,
            siteClientAcctID : alert.Workgroup.WorkgroupId,
            users: alert.UserIds
        });
    }
}