import { Injectable } from '@angular/core';
import { HttpService } from '../http.service/http.service';
import { DataService } from '../http.data.service/data.service';

declare var window;

@Injectable()
export class ImageService {
    private baseUrl:string = '/RecHubViews/';
    private singleImagePath:string = 'Image?';
    private multipleImagePath:string = 'Image/AllImages?';
    private reportPath:string = 'Image/Report?';
    private pdfPath:string = "Image/AdvancedSearchPDF?";
    private localStorageImageRequest:string = "imagerequestobject";
    private imagesExistPath:string = "Image/ImagesExist";

    private httpService: DataService;
    constructor(httpService: DataService) {
        this.httpService = httpService;
    }

    displaySinglePayment(bankid, workgroupid, batchid, depositdate, transactionid, batchsequence) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate,
            TxnSequence: transactionid, BatchSequence: batchsequence, ImportTypeKey: 3
        });
        window.open(this.baseUrl + this.singleImagePath + querystring, '_blank');
    };

    displaySingleDocument(bankid, workgroupid, batchid, depositdate, transactionid, batchsequence, importTypeShortName) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate,
            TxnSequence: transactionid, BatchSequence: batchsequence, ImportTypeKey: 1,
            ImportTypeShortName: importTypeShortName
        });
        window.open(this.baseUrl + this.singleImagePath + querystring, '_blank');
    };

    displayTransactionDocuments(bankid, workgroupid, batchid, depositdate, transactionid) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate,
            TransactionId: transactionid, ImportTypeKey: 2, Page: -1, ImageFilterOption: 'DocumentsOnly'
        });
        window.open(this.baseUrl + this.multipleImagePath + querystring, '_blank');
    };

    displayTransactionImages(bankid, workgroupid, batchid, depositdate, transactionid) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate,
            TransactionId: transactionid, ImportTypeKey: 1, Page: -1
        });
        window.open(this.baseUrl + this.multipleImagePath + querystring, '_blank');
    };

    displayTransactionReport(bankid, workgroupid, batchid, depositdate, transactionsequence, transactionid, batchsequence, batchnumber, processingdate) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate, TxnSequence: transactionsequence,
            TransactionId: transactionid, BatchNumber: batchnumber, ReportName: 'TransactionReport', ProcessingDateString: processingdate
        });
        window.open(this.baseUrl + this.reportPath + querystring, '_blank');
    };

    displayItemReport(bankid, workgroupid, batchid, depositdate, transactionsequence, transactionid, batchsequence, batchnumber, processingdate) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate, TxnSequence: transactionsequence, TransactionId: transactionid,
            BatchSequence: batchsequence, BatchNumber: batchnumber, ProcessingDateString: processingdate, ReportName: 'ItemReport'
        });
        window.open(this.baseUrl + this.reportPath + querystring, '_blank');
    };

    displayBatchImages(bankid, workgroupid, batchid, batchnumber, depositdate) {
        var querystring = $.param({ BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate, BatchNumber: batchnumber });
        window.open(this.baseUrl + this.multipleImagePath + querystring, '_blank');
    };

    displayASPdf(model) {
        window.localStorage.setItem(this.localStorageImageRequest, JSON.stringify(
            { Data: model, URL: '/RecHubViews/Image/AdvancedSearchPDFRequest' }
        ));
        window.open('/RecHubViews/Image/LoadFromLocal', '_blank');
    };

    displayASSelected(model) {
        window.localStorage.setItem(this.localStorageImageRequest, JSON.stringify(
            { Data: model, URL: '/RecHubViews/Image/AdvancedSearchPDFSelectedRequest' }
        ));
        window.open('/RecHubViews/Image/LoadFromLocal', '_blank');
    };
    
    displayDownloadAS(model) {
        window.localStorage.setItem(this.localStorageImageRequest, JSON.stringify(
            { Data: model, URL: '/RecHubViews/Image/AdvancedSearchDownloadRequest' }
        ));
        window.open('/RecHubViews/Image/LoadFromLocal', '_blank');
    };

    displayInProcessExceptionImages(model) {
        window.localStorage.setItem(this.localStorageImageRequest, JSON.stringify(
            { Data: model, URL: this.baseUrl + 'Image/InProcessExceptionImages' }
        ));
        window.open('/RecHubViews/Image/LoadFromLocal', '_blank');
    };

    downloadCENDSFile(path) {
        window.location = '/RecHubViews/Image/DownloadCENDSFile?path=' + path;
    };

    imagesExist(query): Promise<boolean> {
        return new Promise<boolean>((res, rej) => {
            const url = `${this.baseUrl}${this.imagesExistPath}`;
            this.httpService.post<any>(url, query)
                .subscribe((result) => {
                    const hasimages = result && result.Data && result.Data.IsSuccessful && result.Data.ImagesAvailable;
                    console.log(`ImageService: Images exist: ${hasimages}`);
                    res(hasimages);
                }, (err) => {
                    console.log(`ImageService: Error requesting image exists.`);
                    res(false);
                });
        });
    }

}