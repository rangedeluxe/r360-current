import { Injectable } from '@angular/core';
import { DataService } from '../http.data.service/data.service';
import { Globals } from '../../globals';
import { HttpService } from '../http.service/http.service';
import { Observable } from 'rxjs';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { NotificationsQuery } from '../../DTOs/NotificationsQuery';

@Injectable()
export class NotificationsService {
    private http: DataService;
    constructor(http: DataService) {
        this.http = http;
    }

    getNotifications(query: NotificationsQuery): Observable<ServiceResponse>{
        return this.http.post<any>(Globals.NOTIFICATION_URL, query);
    }

    getNotificationDetail(): Observable<ServiceResponse>{
        return this.http.post<any>(Globals.NOTIFICATIONDETAIL_URL, null);
    }

    getNotificationsFileTypes(): Observable<ServiceResponse> {
        return this.http.post<any>(Globals.NOTIFICATIONFILETYPES_URL, null);
    }
}
