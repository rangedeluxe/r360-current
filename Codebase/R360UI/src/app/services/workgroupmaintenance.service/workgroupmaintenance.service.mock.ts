import { Injectable } from '@angular/core';
import { of as observableOf, Observable, of } from 'rxjs';
//import { DataService } from '../http.data.service/data.service';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
//import { WorkgroupMaintenanceService } from './workgroupmaintenance.service';
//import { Workgroup } from '../../DTOs/Workgroup';
import { IWorkgroupMaintenanceService } from './iworkgroupmaintenance-service';
import { WorkgroupSetting } from '../../DTOs/WorkgroupSetting';

@Injectable({
  providedIn: 'root'
})
export class WorkgroupMaintenanceMockService implements IWorkgroupMaintenanceService {


  getEntityDefaults(entityId: number): Observable<any> {
    let response: any = {
      "paymentImageDisplayMode": 0,
      "documentImageDisplayMode": 1,
      "displayBatchID": false,
      "viewingDays": "300",
      "maximumSearchDays": "96"
    }
    return observableOf(response);
  }

  getWorkgroupList(isPageView: boolean): Observable<any> {
    return observableOf(null);
  }

  getWorkgroupSettings(entityId: number): Observable<WorkgroupSetting[]> {
    let settings: WorkgroupSetting[] = [];

    console.log("entity id:" + entityId)
    let s1 = new WorkgroupSetting();
    s1.bankName = "Bank1";
    s1.isActive = true;
    s1.longName = "Long Name1";
    s1.workgroupId = 1;
    let count: number = 0;

    if (entityId % 2 === 0)
      count = 200;
    else
      count = 50

    for (var i = 0; i < count; i++) {
      let s1 = new WorkgroupSetting();
      s1.bankName = "Bank" + i;
      s1.isActive = (entityId % i === 0) ? true : false;
      s1.longName = "Long Name" + i;
      s1.workgroupId = i;
      settings.push(s1);
    }

    const options = { edit: false }
    settings = settings.reduce((acc, val) => acc.concat(val), [])
    settings = settings.map(p => Object.assign(p, options));

    return of(settings);
  }
}
