import { Injectable } from '@angular/core';
import { DataService } from '../http.data.service/data.service';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { Observable, of, forkJoin } from 'rxjs';
//import { Workgroup } from '../../DTOs/Workgroup';
import { IWorkgroupMaintenanceService } from './iworkgroupmaintenance-service';
import { HttpService } from '../http.service/http.service';
import { map } from 'rxjs-compat/operator/map';
import { catchError, switchMap } from 'rxjs/operators';
import { TokenService } from '../token.service/token.service';
import { HttpHeaders, HttpRequest, HttpClient } from '@angular/common/http';
import { AppConfigService } from '../appconfig.service/appconfig.service';
import { WorkgroupSetting } from '../../DTOs/WorkgroupSetting';

@Injectable({
  providedIn: 'root'
})
export class WorkgroupMaintenanceService implements IWorkgroupMaintenanceService {

  constructor(
    private http: DataService,
    private httpService: HttpService,
    private tokenService: TokenService,
    private httpApi: HttpClient,
    private appConfigService: AppConfigService
  ) { }

  getEntityDefaults(entityId: number): Observable<any> {
    var url = `${this.appConfigService.apiConfig.adminApiBaseUrl}/entities/${entityId}/defaultsettings`;
    return this.http.get<any>(url, { observe: 'response' });
  }

  getWorkgroupList(isPageView: boolean): Observable<any> {
    var url = `${this.appConfigService.apiConfig.adminApiBaseUrl}/workgroup?isPageView=${isPageView}`;
    return this.http.get<any>(url, { observe: 'response' });
  }

  getWorkgroupSettings(entityId: number): Observable<WorkgroupSetting[]> {
    var url = `${this.appConfigService.apiConfig.adminApiBaseUrl}/entities/${entityId}/optionalsettings`;
    return this.http.get<any>(url, { observe: 'response' });
  }
}