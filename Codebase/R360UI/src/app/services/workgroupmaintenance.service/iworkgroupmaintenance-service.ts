import { Observable } from "rxjs";
import { ServiceResponse } from "../../DTOs/ServiceResponse";
import { Workgroup } from "../../DTOs/Workgroup";
import { WorkgroupSetting } from "../../DTOs/WorkgroupSetting";

export interface IWorkgroupMaintenanceService {
    getEntityDefaults(entityId: number): Observable<any>;
    getWorkgroupList(isPageView: boolean): Observable<any>;
    getWorkgroupSettings(entityId: number): Observable<WorkgroupSetting[]>;
}