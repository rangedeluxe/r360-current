import { Injectable, APP_INITIALIZER } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  private appConfig: any;

  constructor(
    private httpClient: HttpClient
  ) { }

  loadAppConfig() {
    return this.httpClient.get('./assets/config.json')
      .toPromise()
      .then(data => {
        this.appConfig = data;
      });
  }

  get apiConfig() {
    return this.appConfig;
  }
}

export const AppConfigServiceProvider = {
  provide: APP_INITIALIZER,
  deps: [AppConfigService],
  useFactory: (appConfigService: AppConfigService) => {
    return () => {
      //Make sure to return a promise!
      return appConfigService.loadAppConfig();
    };
  },
  multi: true,
};
