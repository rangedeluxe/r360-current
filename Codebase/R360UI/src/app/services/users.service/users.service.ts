import { Injectable } from '@angular/core';
import { Globals } from '../../globals';

import { HttpService } from '../http.service/http.service';
@Injectable()
export class UsersService{
    private http : HttpService;
    constructor(http: HttpService) {
        this.http = http;
    }

    getUsersByEntity (entityId:number){
        return this.http.get(Globals.ENTITY_USER_URL + entityId);
    }

}