import { Injectable } from '@angular/core';
import { DataService } from '../http.data.service/data.service';
import { HttpService } from '../http.service/http.service';
import { Globals } from '../../globals';
import { DateService } from '../../services/date.service/date.service';


@Injectable()


export class NotificationDetailService {
    private http: DataService;
    private dateService: DateService;

    constructor(http: DataService, dateService: DateService) {
        this.http = http;
        this.dateService = dateService;
    }

    getNotificationDetail(messageGroup: number) {
        return this.http.post<any>(Globals.NOTIFICATIONDETAIL_URL, 
        { 
            messageGroup: messageGroup,
            utcOffset: this.dateService.getCurrentUtcOffset()
        });
    }
}
