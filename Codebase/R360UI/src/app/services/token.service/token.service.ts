import { Injectable } from '@angular/core';
import { Globals } from '../../globals';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AppConfigService } from '../appconfig.service/appconfig.service';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private TOKEN_STORAGE_ID = 'token';

  constructor(
    private http: HttpClient,
    private appConfigService: AppConfigService
  ) { }
  

  refreshToken() : Observable<any> {

    return this.http.get(this.appConfigService.apiConfig.jwtTokenUrl, {observe: 'response'})
    .pipe
    (
      map(response => {
        this.setToken(response);
        return response;
      } )
    );
      
  }

  getToken(): string {
    return localStorage.getItem(this.TOKEN_STORAGE_ID);
  }

  private setToken(tokenResult: HttpResponse<any>) {
    localStorage.setItem(this.TOKEN_STORAGE_ID, tokenResult.body.token);
  }
}
