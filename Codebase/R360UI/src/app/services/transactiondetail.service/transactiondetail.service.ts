import { Injectable } from '@angular/core';
import { Globals } from '../../globals';

import { HttpService } from '../http.service/http.service';
import { Observable } from 'rxjs';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { TransactionDetailDTO } from '../../DTOs/TransactionDetailDTO';
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class TransactionDetailService {
    private http: DataService;
    constructor(http: DataService) {
        this.http = http;
    }

    getTransactionDetails(transaction: TransactionDetailDTO): Observable<any> {
        return this.http.post<any>(Globals.TRANSACTIONDETAILS_URL,
            {
                BankId: transaction.bankId,
                WorkgroupId: transaction.workgroupId,
                BatchId: transaction.batchId,
                DepositDateString: transaction.depositDate,
                TransactionId: transaction.transactionId,
                TransactionSequence: transaction.transactionSequence
            });
    }
}