import { Injectable, Inject } from '@angular/core';
import { Globals } from '../../globals';

import { HttpService } from '../http.service/http.service';
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class UserPreferencesService {
    private http: DataService;

    constructor(http: DataService) {
        this.http = http;
    }

    restoreDefaults() {
        return this.http.post<any>(Globals.USERPREFERENCES_RESTOREDEFAULTS_URL, {});
    }

    savePreferences(data) {
        return this.http.post<any>(Globals.USERPREFERENCES_SAVEPREFERENCES_URL, data);
    }

    getPreferences() {
        return this.http.post<any>(Globals.USERPREFERENCES_GETPREFERENCES_URL, {});
    }

    getAuthorizedPreferences() {
        return this.http.postWithAuthCheck<any>(Globals.USERPREFERENCES_GETPREFERENCES_URL, {});
    }
}