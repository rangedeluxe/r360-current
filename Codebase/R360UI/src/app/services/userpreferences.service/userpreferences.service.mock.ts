import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient, HttpHandler } from '@angular/common/http'
import { HttpService} from '../http.service/http.service';
import { UserPreferencesService } from './userpreferences.service';
import { MockLocalStorageService } from '../localstorage.service/localstorage.service.mock';

//
// This is just a mocked up version of the userpref service. Stores everything locally.
//
@Injectable()
export class UserPreferencesMockService {

    private showPayer:boolean = false;
    constructor() {
    }

    restoreDefaults() {
        this.showPayer = false;
        return this.getPreferences();
    }

    savePreferences(data) {
        this.showPayer = data.displayRemitterNameInPDF;
        return Observable.of({ 'HasErrors': false });
    }

    getPreferences() {
        return Observable.of({ 'displayRemitterNameInPDF': this.showPayer, 'displayRemitterAccesible': true });
    }
}

