/// Don't use this class for new services. 
// Please use the data.service for http requests and create a DTO model for whatever the server returns
import {throwError as observableThrowError, of as observableOf,  Observable } from 'rxjs';

import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


declare var window;

@Injectable()
export class HttpService{
    private http: HttpClient;
    constructor(http : HttpClient ) {
        this.http = http;
    }

    public get(url) {
        return this.http.get(url,  
            {
                observe: 'response'
            }).pipe(
            catchError(error => this.handleError(error)));
    }

    public getAuthorizedResults(url) {
        return this.http.get<any>(url, {observe: 'response'}).pipe(
            map(result => {
                if (result.status !== 200 && //success
                    result.status !== 204) { //no content
                    throw new Error('Http: Unknown status received from server.');
                }
                if ((<any>result)._body == ''){
                    return {};
                }
                return result;
            }),
            catchError( error => {
                if (error.status == 401 || //unauthorized or forbidden
                    error.status == 403)  {
                    return observableOf({});//return an empty json array
                }
                return observableThrowError(error);//any other error, throw the observable
            }));
    }

    public postAuthorizedResults(url, data) {
        return this.http.post<any>(url,  data, {observe: 'response'}).pipe(
            map(result => {
                if (result.status !== 200 && //success
                    result.status !== 204) { //no content
                    throw new Error('Http: Unknown status received from server.');
                }
                if ((<any>result)._body == ''){
                    return {};
                }
                return result;
            }),
            catchError( error => {
                if (error.status == 401 || //unauthorized or forbidden
                    error.status == 403)  {
                    return observableOf({});//return an empty json array
                }
                return observableThrowError(error);//any other error, throw the observable
            }));
    }

    // Purpose - Return an http promise, but also handle 401 responses.
    public post(url, data) {
        // we have to add another header to prevent RAAM from returning a redirect.
        return this.http.post<any>(url, data, {
            observe: 'response',
            headers: new HttpHeaders({ 'X-Requested-With': 'XMLHttpRequest'})}).pipe(
            map(result => {
                if (result.status < 200 || result.status > 300) {
                    throw new Error('Http: Unknown status received from server.');
                }
                if ((<any>result)._body == '')
                    return {};
                return result;
            }),
            catchError( error => this.handleError(error)),);
    }

    private handleError(error: any) {
        let errMsg = error.message || 'Server error';
        if(error.status == 401){
            console.log('Error 401 returned from server, redirecting to login page...');
            let current = encodeURIComponent(window.location.href);
            window.location.reload(true);
        }
        else {
            console.log("Http: Error.");
            console.dir(error);
        }
        return observableThrowError(errMsg);
    }
}
