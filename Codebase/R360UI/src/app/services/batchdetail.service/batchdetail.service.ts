import { Injectable } from '@angular/core';
import { Globals } from '../../globals';

import { HttpService } from '../http.service/http.service';
import { Workgroup } from "../../DTOs/Workgroup";
import { Observable } from 'rxjs';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class BatchDetailService {
    private http : DataService;
    constructor(http: DataService) {
        this.http = http;
    }

    getPreferences() : Observable<ServiceResponse>
    {
        return this.http.post<any>(Globals.USERPREFERENCES_GETPREFERENCES_URL,{});
    }

    getBatchDetail(bankId:number, workgroupId:number, depositDate: string, batchId:number, starting: number, pages: number): Observable<any>{                    
        let parms = {BankId : bankId, WorkgroupId : workgroupId, BatchId : batchId, DepositDateString : depositDate, start : starting, length : pages};
        return this.http.post<any>(Globals.BATCHDETAILS_URL, parms
        );
        
    }
}
