
import {of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpService } from '../http.service/http.service';

import { BatchDetailService } from "../../services/batchdetail.service/batchdetail.service";
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class MockBatchDetailService extends BatchDetailService {
    constructor(http: DataService) {
        super(http);
    }

    getBatchDetail(bankId: number, workgroupId: number, depositDate: string, batchId: number): Observable<any> {
        let response: any = {
            success: true,
            error: false,
            bank: '12345',
            depositData: '12/1/2000',
            batchId: 19876,
            data: [['12345',
                '664321'
            ],
            ['54321',
                '0987676']
            ]

        };
        return observableOf(response);
    }
}