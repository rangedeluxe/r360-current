import { Injectable } from '@angular/core';
import { HttpService } from '../http.service/http.service';
import { Globals } from '../../globals';
import {IPermission } from './ipermission.service';


import { DataService } from '../http.data.service/data.service';


@Injectable()
export class PermissionService implements IPermission {
    private http : DataService;
    constructor(http: DataService) {
        this.http = http;
    }

    hasPermission(permission:string, type:string) {        
        return this.http.post<any>(Globals.CHECK_PERMISSION_URL, { permission: permission, type: type });
    }
}