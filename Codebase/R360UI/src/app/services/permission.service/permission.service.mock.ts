
import { of as observableOf, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpService } from '../http.service/http.service';

import { PermissionService } from "./permission.service";
import { IPermission } from './ipermission.service';
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class MockPermissionService extends PermissionService {
    constructor(http: DataService) {
        super(http);

    }
    hasPermission(permission: string, type: string) {
        return observableOf(true);
    }
}