
export interface IPermission {
    hasPermission(permission:string, type:string);
}