import { Injectable } from "@angular/core";
import { BatchSummaryQuery } from "../../DTOs/BatchSummaryQuery";
import { LocalStorageService } from "../localstorage.service/localstorage.service";
declare var window;

@Injectable()
export class NavigationService {

    // globals
    private BATCH_SUMMARY_URL : string = "/Framework/?tab=2";
    private BREADCRUMB_STORAGE : string = "breadcrumb_local_storage";
    private BATCH_SUMMARY_STORAGE: string = "batch_summary_query";

    private localStorageService: LocalStorageService;

    constructor(localStorageService: LocalStorageService) {
        this.localStorageService = localStorageService;
    }

    public registerBreadcrumb(name: string, url: string, data: any) : void {
        // in the future this will need to be updated to work like breadcrumbs.js, but for now we only need to push
        // the breadcrumb for the framework pages.
        let bread = [
            { Name: name, URL: url, Data: data, Reload: true }
        ];
        this.localStorageService.write(this.BREADCRUMB_STORAGE, bread);
    }

    navigateToBatchSummary(name: string, query: BatchSummaryQuery): void {
        this.localStorageService.write(this.BATCH_SUMMARY_STORAGE, query);
        this.registerBreadcrumb(name, window.location.pathname, null);
        // we should be able to rely on these tabs maintaining the same ID.
        window.location.href = "/Framework/?tab=2";
    }

}