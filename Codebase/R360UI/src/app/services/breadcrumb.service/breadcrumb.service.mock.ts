
import {of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpService } from '../http.service/http.service';

import { BreadcrumbService } from "../../services/Breadcrumb.service/breadcrumb.service";
import { ServiceResponse } from '../../DTOs/ServiceResponse';

@Injectable()
export class MockBreadcrumbService extends BreadcrumbService {

  
    getBatchDetail(bankId: number, workgroupId: number, depositDate: string, batchId: number): Observable<any> {
        let response: any = {
            success: true,
         };
        return observableOf(response);
    }
}