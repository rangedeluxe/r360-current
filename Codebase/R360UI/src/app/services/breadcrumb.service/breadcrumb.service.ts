import { Injectable } from '@angular/core';
import { Globals } from '../../globals';

import { Workgroup } from '../../DTOs/Workgroup';
import { Observable } from 'rxjs';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { Router } from '@angular/router';
import { Breadcrumb } from '../../DTOs/Breadcrumb';

declare var window;

@Injectable()
export class BreadcrumbService {
   constructor(router: Router) {
      this.router = router;
   }
   private router: Router;

   public getBreadcrumbs() {
      let bcs = JSON.parse(window.localStorage.getItem(Globals.LOCAL_STORAGE_BREADCRUMB_KEY));
      if (bcs === null) {
         bcs = [];
      }
      return bcs;
   }

   public setBreadcrumbs(data) {
      window.localStorage.setItem(Globals.LOCAL_STORAGE_BREADCRUMB_KEY, JSON.stringify(data));
   }

   public push(text: string, url: string, data: any) {
      // Logic: push the current breadcrumb. However, if the breadcrumb already exists
      //        in the stack, then we need to pop that breadcrumb.
      //        For example, RecSummary->BatchSummary->BatchDetail, then we click
      //        on RecSummary.

      this.pop(text);
      const breaddata = this.getBreadcrumbs();
      breaddata.unshift({ label: text, url: url, params: data });

      this.setBreadcrumbs(breaddata);
   }

   public pop(text: string) {
      // Logic: pop a breadcrumb, regardless if it's the first in the queue or not.
      //        We will continue popping until we have popped the breadcrumb we pass.
      const data = this.getBreadcrumbs();

      // First, we find if the text exists.  We do nothing if not.
      let index = -1;
      $.each(data, function (i, e) {
         if (e.label === text) {
            index = Number(i);
         }
      });

      if (index < 0) {
         return null;
      }
      // Shift until the breadcrumb is found.
      let breadcrumb;
      for (let shifts = 0; shifts <= index; shifts++) {
         breadcrumb = data.shift();
      }
      this.setBreadcrumbs(data);
      return breadcrumb;
   }

   public loadBreadcrumb(name: string) {
      const bcs = this.getBreadcrumbs();
      let bc = null;
      $.each(bcs, function (i, e) {
         if (e.label === name) {
            bc = e;
         }
      });

      if (bc === null) {
         return;
      }
      // TODO: This needs to be removed when we convert AS and PaymentSearch results to angular
      if (bc.url.includes(Globals.BREADCRUMBS_RECHUBVIEWS_IDENTIFIER)) {
         window.location.href = bc.externalUrl;
         if (bc.Name.includes(Globals.BREADCRUMBS_SEARCHRESULTS_IDENTIFIER)) {
            localStorage.setItem(Globals.LOCAL_STORAGE_ADVANCEDSEARCH_RESULTS, JSON.stringify(bc.params));
         }
      }
      else {
         this.router.navigateByUrl(bc.url, bc.params);
      }
   }

   public clear() {
      this.setBreadcrumbs([]);
   }
}
