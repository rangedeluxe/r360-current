import { Injectable } from '@angular/core';
import { DataService } from '../http.data.service/data.service';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { Observable, of, forkJoin } from 'rxjs';
import { Payer } from '../../DTOs/Payer';
import { IPayerService } from './ipayer-service';
import { HttpService } from '../http.service/http.service';
import { map } from 'rxjs-compat/operator/map';
import { catchError, switchMap } from 'rxjs/operators';
import { TokenService } from '../token.service/token.service';
import { HttpHeaders, HttpRequest, HttpClient } from '@angular/common/http';
import { AppConfigService } from '../appconfig.service/appconfig.service';

@Injectable({
  providedIn: 'root'
})
export class PayerService implements IPayerService {

  constructor(
    private http: DataService,
    private httpService: HttpService,
    private tokenService: TokenService,
    private httpApi: HttpClient,
    private appConfigService: AppConfigService
  ) { }

  getPayers(bankId: number, workgroupId: number, isPageView: boolean): Observable<ServiceResponse> {
    var url = `${this.appConfigService.apiConfig.adminApiBaseUrl}/banks/${bankId}/workgroups/${workgroupId}/payers?isPageView=${isPageView}`;
    return this.http.get<any>(url, { observe: 'response' });
  }

  updatePayer(payer: Payer): Observable<ServiceResponse> {
    return this.http.post<ServiceResponse>(`${this.appConfigService.apiConfig.adminApiBaseUrl}/payers`, payer);
  }

  deletePayer(bankId: number, workgroupId: number, rn: string, acct: string): Observable<ServiceResponse> {

    return this.http.delete<ServiceResponse>(`${this.appConfigService.apiConfig.adminApiBaseUrl}/banks/${bankId}/workgroups/${workgroupId}/routing/${rn}/account/${acct}`, {});
  }
}