import { Injectable } from '@angular/core';
import { of as observableOf, Observable, of } from 'rxjs';
import { DataService } from '../http.data.service/data.service';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { PayerService } from './payer.service';
import { Payer } from '../../DTOs/Payer';
import { IPayerService } from './ipayer-service';

@Injectable({
  providedIn: 'root'
})
export class MockPayerService implements IPayerService {
  public respValues: any = { "siteBankId": 0, "siteBankName": "Sample Bank", "siteClientAccountId": 0, "siteClientAccountName": "Sample Workgroup", "payers": [{ "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "456004087", "account": "87432345", "payerName": "Anna Mull", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.417", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.417" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "104000111", "account": "738972347", "payerName": "Barb Dwyer", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.43", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.43" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "104000016", "account": "547898", "payerName": "Brock Lee", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.43", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.43" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "733363878", "account": "73800-28374", "payerName": "Cliff Hanger", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.43", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.43" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "210007178", "account": "632432-01", "payerName": "Gail Forcewind", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.417", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.417" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "854432293", "account": "24843172", "payerName": "Gene Jacket", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.417", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.417" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "541119890", "account": "61110622", "payerName": "Mario Speedwagon", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.417", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.417" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "111990111", "account": "54893711", "payerName": "Paige Turner", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.43", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.43" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "611130301", "account": "19034783", "payerName": "Pat Agonia", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.43", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.43" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "733311178", "account": "50493630", "payerName": "Paul Molive", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.417", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.417" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "634405137", "account": "24843172", "payerName": "Petey Cruiser", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.417", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.417" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "913011526", "account": "328734783243", "payerName": "Phil Harmonic", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.43", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.43" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "104000032", "account": "654321", "payerName": "Sal Vidge", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.417", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.417" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "401500002", "account": "32817583927", "payerName": "Walter Melon", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.43", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.43" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "104000016", "account": "123456", "payerName": "Will Power", "isDefault": true, "createdBy": "M. Van Kekerix", "creationDate": "2019-05-22T10:50:54.417", "modifiedBy": "M. Van Kekerix", "modificationDate": "2019-05-22T10:50:54.417" }, { "siteBankId": 0, "siteClientAccountId": 0, "routingNumber": "104000016", "account": "2378347823", "payerName": "Test Name", "isDefault": false, "createdBy": "", "creationDate": "2019-05-24T13:14:56.033", "modifiedBy": "", "modificationDate": "2019-05-24T13:17:56.63" }] }

  getPayers(siteBankId: number, workgroupId: number, isPageView: boolean): Observable<ServiceResponse> {
    let response: ServiceResponse = {
      success: true,
      error: false,
      data: this.respValues
    };
    return observableOf(response);
  }
  updatePayer(payer: Payer): Observable<ServiceResponse> {
    let idx = this.respValues.payers.findIndex(obj => obj.account === payer.account);
    this.respValues.payers[idx] = payer;
    let resp = new ServiceResponse();
    resp.success = true;
    return of(resp);
  }

  deletePayer(bankId: number, workgroupId: number, rn: string, acct: string): Observable<ServiceResponse> {
    this.respValues.payers = this.respValues.payers.filter(obj => obj.account !== acct);
    let resp = new ServiceResponse();
    resp.success = true;
    return of(resp);
  }
}
