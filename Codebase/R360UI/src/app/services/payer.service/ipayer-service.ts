import { Observable } from "rxjs";
import { ServiceResponse } from "../../DTOs/ServiceResponse";
import { Payer } from "../../DTOs/Payer";

export interface IPayerService {
    getPayers(bankId: number, workgroupId: number, isPageView: boolean): Observable<ServiceResponse>;
    updatePayer(payer: Payer): Observable<ServiceResponse>;
    deletePayer(bankId: number, workgroupId: number, rn: string, acct: string): Observable<ServiceResponse>;
}
