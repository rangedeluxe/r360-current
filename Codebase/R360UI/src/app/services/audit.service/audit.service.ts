import { Injectable } from '@angular/core';
import { HttpService } from '../http.service/http.service';
import { Globals } from '../../globals';


import { DataService } from '../http.data.service/data.service';


@Injectable()
export class AuditService {
    private http : DataService;
    constructor(http: DataService) {
        this.http = http;
    }

    auditPageView(page:string) {
        return this.http.post<any>(Globals.AUDIT_PAGE_URL, { page: page });
    }
}