import { Injectable, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()

export class dlxSanitizerService {

    constructor(private sanitizer: DomSanitizer) { }

    sanitizeHTML(html: string): string {
        return this.sanitizer.sanitize(SecurityContext.HTML,html)
    }
}