import { Injectable } from "@angular/core";

declare var window;

@Injectable()
export class LocalStorageService {

   write(key: string, val: any): void {
      val = typeof (val) == 'object' ? JSON.stringify(val) : val.toString();
      window.localStorage.setItem(key, val);
   }

   get(key: string): any {
      return JSON.parse(window.localStorage.getItem(key));
   }

   delete(key: string): void {
      window.localStorage.removeItem(key);
   }

   clear(): void {
      window.localStorage.clear();
   }
}