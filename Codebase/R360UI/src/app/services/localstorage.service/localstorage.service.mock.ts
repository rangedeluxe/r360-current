import { Injectable } from "@angular/core";
import { LocalStorageService } from "./localstorage.service";

@Injectable()
export class MockLocalStorageService extends LocalStorageService{

    write(key:string, val:any):void{
        
    }

    get(key:string):any{
        return null;
    }

    delete(key:string):void{
        
    }

    clear():void{
        
    }
}