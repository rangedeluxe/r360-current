
import {of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpService } from '../http.service/http.service';

import { BatchSummaryService } from "../../services/batchsummary.service/batchsummary.service";
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class MockBatchSummaryService extends BatchSummaryService {
    constructor(http: DataService) {
        super(http);
    }


    getPaymentSources() : Observable<ServiceResponse>
    {
        let response : ServiceResponse = {
            success: true,
            error : false,
            data : [{
                '1' : 'TestSource1',
                '2' :'TestSource2',
                '3' : 'TestSource3'
                }
            ]
        };
        return observableOf(response);
    }

    getPaymentTypes() : Observable<ServiceResponse>
    {
        let response : ServiceResponse = {
            success: true,
            error : false,
            data : [{
                '1' : 'TestPayment1',
                '2' : 'TestPayment2',
                '3' : 'TestPayment3'
                }
            ]
        };
        return observableOf(response);
    }
    
}