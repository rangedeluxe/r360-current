import { Injectable } from '@angular/core';
import { Globals } from '../../globals';

import { HttpService } from '../http.service/http.service';
import { Workgroup } from "../../DTOs/Workgroup";
import { Observable } from 'rxjs';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class BatchSummaryService {
    private http : DataService;
    constructor(http: DataService) {
        this.http = http;
    }

    getPaymentSources() : Observable<ServiceResponse>
    {
        return this.http.post<any>(Globals.PAYMENTSOURCES_URL,{});
    }

    getPaymentTypes() : Observable<ServiceResponse>
    {
        return this.http.post<any>(Globals.PAYMENTTYPES_URL,{});
    }
}
