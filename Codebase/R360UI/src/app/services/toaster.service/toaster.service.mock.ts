import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { Toast, ToasterService } from "angular2-toaster";


@Injectable()
export class MockToasterService extends ToasterService {
    constructor() {
        super();
    }
    public pop(type: string | Toast, title?: string, body?: string) : Toast {
        // do nothing!
        return null;
    }
}