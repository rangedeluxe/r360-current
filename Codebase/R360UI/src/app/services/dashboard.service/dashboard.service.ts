import { Injectable } from '@angular/core';
import { Globals } from '../../globals';

import { HttpService } from '../http.service/http.service';
import { Workgroup } from "../../DTOs/Workgroup";
import { Observable } from 'rxjs';
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class DashboardService {
    private http : DataService;
    constructor(http: DataService) {
        this.http = http;
    }

    getSummaryData(workgroup:any, depositDate:string) {
        workgroup.items = [];
        return this.http.post(Globals.DASHBOARD_SUMMARY_URL, {
            "DepositDate": depositDate,
            "Entity": workgroup
        });
    }

    getRecivablesSummary(depositDate:string, bankId:number, workgroupId?:number, entityId?:number): Observable<ServiceResponse>{                    
        return this.http.post(Globals.RECEIVABLES_SUMMARY_URL, {
            DepositDateString : depositDate,
            BankId : bankId,
            WorkgroupId : workgroupId,
            EntityId : entityId
        });
        
    }
}