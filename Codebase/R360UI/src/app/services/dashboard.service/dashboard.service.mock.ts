
import {of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Globals } from '../../globals';

import { HttpService } from '../http.service/http.service';
import { Workgroup } from "../../DTOs/Workgroup";
import { DashboardService } from "./dashboard.service";
import { ServiceResponse } from '../../DTOs/ServiceResponse';
import 'rxjs/Rx';
import { DataService } from '../http.data.service/data.service';

@Injectable()
export class MockDashboardService extends DashboardService {
    constructor(http: DataService) {
        super(http);
    }

    getSummaryData(workgroup:any, depositDate:string) {
        return observableOf({
            "Errors":null,
            "HasErrors":false,
            "Data":[
                {"PaymentType":"Check","PaymentTypeKey":0,"ItemCount":100,"ItemTotal":100},
                {"PaymentType":"Wire","PaymentTypeKey":1,"ItemCount":100,"ItemTotal":100},
                {"PaymentType":"ACH","PaymentTypeKey":2,"ItemCount":100,"ItemTotal":100},
            ]
        });
    }
    getRecivablesSummary(depositDate:string, bankId:number, workgroupId?:number, entityId?:number):Observable<ServiceResponse>{
        let response : ServiceResponse = {
            success: true,
            error : false,
            data : [{
                'Account' : '1123211 - 1123211',
                'BatchCount':'1',
                'ClientSummaryURL' : '',
                'EntityID' : '7013',
                'Organization' : 'WFS\AutomationTest\AutomationImports',
                'PaymentID' : '72',
                'PaymentSource' : 'AUTOMATIONIMAGERPS',
                'PaymentSourceId': '127',
                'PaymentType' : 'Check',
                'PaymentTypeId' : '0',
                'RecHubBankId':'1159511',
                'RecHubWorkgroupId': '1123211',
                'Total': '110',
                'TransactionCount' : '10',
                'PaymentCount': 10
                }
            ]
        };
        return observableOf(response);
    }
}