import { Injectable } from '@angular/core';
import * as moment from 'moment';
import 'moment-timezone';


@Injectable()
export class DateService {
    constructor() { }

    getDate(): Date {
        return new Date();
    }

    getCurrentUtcOffset(): number {
        return moment().utcOffset();
    }

    getCurrentTimeZone(): string {
        return moment.tz.guess()
    }

    getCurrentTimeZoneAbbreviation(): string {
        var zoneAbbr = moment.tz(this.getCurrentTimeZone()).zoneAbbr();
        //some zone abbreviations will return the number of hours from UTC,
        //we'll just return an empty string in those situations.
        if (isNaN(parseInt(zoneAbbr))) return zoneAbbr;
        return '';
    }
}