import { Injectable } from '@angular/core';


@Injectable()
export class MockDateService {
    constructor() { }

    public date: Date = new Date();

    getDate(): Date {
        // Default to January 1st.
        return this.date;
    }

    getCurrentUtcOffset(): number {
        return 10;
    }

    getCurrentTimeZone(): string {
        return "Eastern";
    }

    getCurrentTimeZoneAbbreviation(): string {
        return "EDT";
    }
}