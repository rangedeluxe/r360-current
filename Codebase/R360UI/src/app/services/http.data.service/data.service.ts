
import { empty as observableEmpty, Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';


@Injectable()
export class DataService {
  constructor(private httpClient: HttpClient) { }

  // notice the <T>, making the method generic
  get<T>(url, params): Observable<T> {
    return this.httpClient
      .get<T>(url, { params }).pipe(
        catchError((err: HttpErrorResponse) => {
          
          if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', err.error.message);
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
          }

          // ...optionally return a default fallback value so app can continue (pick one)
          // which could be a default value
          // return Observable.of<any>({my: "default value..."});
          // or simply an empty observable
          return observableEmpty();
        }));
  }
  
  public postWithAuthCheck<T>(url, data, headers?: HttpHeaders): Observable<T> {
    return this.httpClient.post<any>(url, data, { headers: { 'X-Requested-With': 'XMLHttpRequest'}})
    .pipe(
      catchError( (error: any)  => {
        return of({});//return an empty json array
      }));
  }
  
  public post<T>(url, data, headers?: HttpHeaders): Observable<T> {
    return this.httpClient.post<T>(url, data, {
      headers: { 'X-Requested-With': 'XMLHttpRequest' }
    });
  }

  public delete<T>(url, httpParams): Observable<T> {
    return this.httpClient.delete<T>(url, {
      headers: { 'X-Requested-With': 'XMLHttpRequest' },
      params: httpParams
    });
  }
}