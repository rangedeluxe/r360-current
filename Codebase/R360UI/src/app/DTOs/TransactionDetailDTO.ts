export class TransactionDetailDTO {
    constructor(public bankId:number, public workgroupId:number, public batchId:number, public depositDate:string, public transactionId:number, public transactionSequence:number) {}
}