import { Workgroup } from './Workgroup';

export class BatchSummaryQuery {
    dateFrom: string;
    dateTo: string;
    paymentSourceId: number;
    paymentTypeId: number;
    workgroup: Workgroup;
    searchValue: string;
}