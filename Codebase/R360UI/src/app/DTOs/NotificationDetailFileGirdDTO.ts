export class NotificationDetailFileGridDTO {
    constructor(
        public hasDownloadPermission: boolean,
        public files: Array<any>) {}    
}
