export class DocumentGridDTO {
    constructor(
        public bankId:number, 
        public workgroupId:number, 
        public batchId:number, 
        public depositDateString:string, 
        public transactionId:number, 
        public documentData:Array<any>,
        public importTypeShortName:string) {}
}