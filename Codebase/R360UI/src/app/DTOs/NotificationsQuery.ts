import { Workgroup } from './Workgroup';

export class NotificationsQuery {
    public StartDate: string;
    public EndDate: string;
    public FileName: string;
    public FileTypeKey: number;
    public UtcOffset: number;
    public Workgroup: Workgroup;
}