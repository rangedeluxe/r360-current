export class DataTableGroup {
    name: string;
    dataSource: string;
    columnIndex: number;
}