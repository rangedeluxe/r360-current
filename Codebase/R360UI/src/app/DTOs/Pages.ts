export enum Pages{
   Dashboard,
   RecSummary,
   BatchSummary,
   BatchDetail,
   TransactionDetail,
   Notifications,
   NotificationDetail
}