export class WorkgroupSetting {
    bankName: string = ""; //From BankObject
    longName: string = "";
    workgroupId: number = 0; //SiteClientAccountID
    isActive: boolean = false;

}