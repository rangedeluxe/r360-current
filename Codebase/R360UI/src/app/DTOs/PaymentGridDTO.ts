export class PaymentGridDTO {
    constructor(
        public bankId:number, 
        public workgroupId:number, 
        public batchId:number, 
        public depositDateString:string, 
        public batchNumber: number,
        public transactionId:number, 
        public transactionSequence: number,
        public paymentData:Array<any>) {}
}