export class Breadcrumb {
   url: string;
   params: string;
   label: string;
   externalUrl: string;
   constructor(url: string, params: string, label: string, reload: boolean, externalUrl: string) {
      this.url = url;
      this.params = params;
      this.label = label;
      this.externalUrl = externalUrl;
   }
}