export class Workgroup {
    BankId: number = 0;
    WorkgroupId: number = 0;
    EntityId: number = 0;
    IsNode: boolean = false;
    Label: string = "";
    EntityTypeCode: string = "";

    public constructor(copy?:Workgroup) {
        this.BankId = copy ? copy.BankId : 0;
        this.WorkgroupId = copy ? copy.WorkgroupId : 0;
        this.EntityId = copy ? copy.EntityId : 0;
        this.IsNode = copy ? copy.IsNode : false;
        this.Label = copy ? copy.Label : "";
        this.EntityTypeCode = copy ? copy.EntityTypeCode : "";        
    }

    public getFullWorkgroupId(): string {
        return this.WorkgroupId && !isNaN(this.WorkgroupId)
            ? `${this.BankId}|${this.WorkgroupId}`
            : `${this.EntityId}`;
    }
}