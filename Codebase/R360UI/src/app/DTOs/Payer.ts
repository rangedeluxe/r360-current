export class Payer {
    siteBankId: number;
    siteClientAccountId: number;
    routingNumber: string;
    account: string;
    payerName: string;
    isDefault: boolean;
    createdBy: string;
    creationDate: Date;
    modifiedBy: string;
    modificationDate: Date;

    constructor() {
        this.payerName = '';
        this.account = '';
        this.createdBy = '';
        this.modifiedBy = '';
        this.routingNumber = '';
    }
}