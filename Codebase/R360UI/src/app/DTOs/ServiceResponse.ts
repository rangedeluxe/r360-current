export class ServiceResponse {
    success: boolean;
    error: any;
    data: any;
}