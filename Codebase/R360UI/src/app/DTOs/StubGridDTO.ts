export class StubGridDTO {
    constructor(
        public bankId:number, 
        public workgroupId:number, 
        public batchId:number, 
        public depositDateString:string, 
        public batchNumber: number,
        public transactionId:number, 
        public transactionSequence: number,
        public stubData:Array<any>,
        public columns:Array<any>,
        public sort: boolean,
        public order: Array<any>,
        public importTypeShortName: string
    ) {}
}