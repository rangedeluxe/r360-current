import { Workgroup } from "./Workgroup";

export class Alert {
    private pristine : boolean = true;
    get Pristine():boolean {
        return this.pristine;
    }
    set Pristine(value:boolean) {
        this.pristine = value;
    }

    private eventRuleId: number;
    get EventRuleID():number {
        return this.eventRuleId;
    }
    set EventRuleID(value:number) {
        this.eventRuleId = value;
    }

    private eventId: number;
    get EventID():number {
        return this.eventId;
    }
    set EventID(value:number) {
        this.eventId = value;
        this.IsValid = this.isEventValid(this.eventId.toString());
    }

    private eventName: string;
    get EventName():string {
        return this.eventName;
    }
    set EventName(value:string) {
        this.eventName = value;
        this.IsValid = this.isEventValid(this.eventId.toString());
    }

    private eventValue: string;
    get EventValue():string {
        return this.eventValue;
    }
    set EventValue(value:string) {
        this.eventValue = value;
        this.IsEventValueValid = this.isEventValueValid();
        this.IsEventValueNumeric = this.isEventValueNumeric();
        this.IsValid = this.isEventValid(this.eventId.toString());
    }

    private isActive: boolean = true;
    get IsActive():boolean {
        return this.isActive;
    }
    set IsActive(value:boolean) {
        this.isActive = value;
    }

    private description: string;
    get Description():string {
        return this.description;
    }
    set Description(value:string) {
        this.description = value;
        this.IsDescriptionValid = this.isDescriptionValid();
        this.IsValid = this.isEventValid(this.eventId.toString());
    }

    private workgroup: Workgroup;
    get Workgroup():Workgroup {
        return this.workgroup;
    }
    set Workgroup(value:Workgroup) {
        this.workgroup = value;
        this.IsWorkgroupValid = this.isWorkgroupValid();
        this.IsValid = this.isEventValid(this.eventId.toString());
    }

    IsValid: boolean = true;
    IsDescriptionValid: boolean = true;
    IsEventValueValid: boolean = true;
    IsEventValueNumeric: boolean = true;
    IsOperatorValid: boolean = true;
    IsWorkgroupValid: boolean = true;

    Operator : any = {id:"0", text: ""};
    WorkgroupName: string = '';    
    AlertUsers: any [] = new Array<any>();
    get UserIds(): string[] {
        return this.AlertUsers 
            ? this.AlertUsers.map( user => user.Id )
            : [];
    }

    isDescriptionValid(): boolean {
        return this.Description != null && this.Description.trim().length > 0;
    }

    isEventValueValid(): boolean {
        return this.EventValue != null && this.EventValue.toString().trim().length > 0;
    }
    
    isEventValueNumeric():boolean {
        return this.EventValue && /^[0-9.]+$/.test(this.EventValue);
    }

    isOperatorValid(): boolean {
        return this.Operator && this.Operator.id != 0;
    }

    isWorkgroupValid(): boolean {
        return this.Workgroup != null && this.Workgroup.BankId > 0 && this.Workgroup.WorkgroupId > 0;
    }

    isEventValid(eventId: string): boolean {
        if (eventId == "1")
            return this.isDescriptionValid() && this.isWorkgroupValid();
        else if (eventId == "2")
            return this.isDescriptionValid();
        else if (eventId == "3")
            return this.isDescriptionValid() && this.isWorkgroupValid() &&
                this.isOperatorValid() && this.isEventValueValid() &&
                this.isEventValueNumeric();
        else if(eventId =="4")
            return this.isDescriptionValid() && this.isWorkgroupValid() &&
                this.isOperatorValid() && this.isEventValueValid();
    }
}