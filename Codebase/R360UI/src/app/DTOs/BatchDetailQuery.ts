import { DataTableGroup } from "./DataTableGroup";
import { PaymentSources } from './PaymentSources';
import { PaymentTypes } from './PaymentTypes';
import { Workgroup } from "./Workgroup";

export class BatchDetailQuery {
    bankId: number;
    depositDate: string;
    batchId: number;
    workGroup: number;
    batchNumber: number;
}