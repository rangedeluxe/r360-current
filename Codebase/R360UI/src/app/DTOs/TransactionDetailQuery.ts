export class TransactionDetailQuery {
    bankId:number;
    workgroupId: number;
    batchId: number;
    transactionId: number;
    transactionSequence: number;
    depositDate: string;
}