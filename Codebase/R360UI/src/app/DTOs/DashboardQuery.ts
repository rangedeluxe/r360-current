import { DataTableGroup } from "./DataTableGroup";
import { Workgroup } from "./Workgroup";

export class DashboardQuery {
    grouping: DataTableGroup;
    workgroup: Workgroup;
    depositDate: string;
}