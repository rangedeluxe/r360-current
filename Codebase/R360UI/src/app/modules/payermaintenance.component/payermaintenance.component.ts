
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Workgroup } from '../../DTOs/Workgroup';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { DatatableComponent } from '../../controls/datatable.component/datatable.component';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { LocalStorageService } from '../../services/localstorage.service/localstorage.service';
import { PayerService } from '../../services/payer.service/payer.service';
import { PayerModal } from '../../controls/payermodal.component/payermodal.component';
import { Operation } from '../../DTOs/Operation';
import { Payer } from '../../DTOs/Payer';
import { ConfirmDialogComponent } from '../../controls/confirmdialog.component/confirmdialog.component';
import { PermissionService } from '../../services/permission.service/permission.service';


@Component({
  moduleId: module.id.toString(),
  selector: 'r360-payermaintenance',
  templateUrl: 'payermaintenance.component.html',
  styleUrls: ['payermaintenance.component.css'],
  providers: [LocalStorageService],
})

export class PayerMaintenanceComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: false }) dataTable: DatatableComponent;
  @ViewChild('payerModal', { static: false }) payerModal: PayerModal;
  @ViewChild('confirmDialog', { static: false }) confirmDialog: ConfirmDialogComponent;

  payerData: Array<any> = [];
  selectedPayer: Payer = null;
  loading: boolean = true;
  searchValue: string = '';
  emptyTableMessage: string = 'No data available in table'
  hasSharedStorageValue: Boolean = false;
  lengthOptions: Array<number> = [10, 25, 50, 100, 1000];
  selectedWorkgroup: Workgroup;
  bankDetail: string = '';
  confirmText: string = 'Are you sure you want to delete the Payer?';
  confirmTitleText: string = 'Delete Payer';
  workGroupDetail: string = '';
  isPageView: boolean = true;
  isValidWorkgroup: boolean = false;
  hasManagePermission: boolean = false;

  columns: Array<any>;
  permissionsLoading: boolean = true;

  constructor(
    private payerService: PayerService,
    private toasterService: ToasterService,
    private permissionService: PermissionService
  ) {

    this.permissionService.hasPermission('PayerMaintenance', 'Manage').subscribe(
      result => {
        this.hasManagePermission = result;
        this.permissionsLoading = false;
        this.setGridDisplayByPermission();
      });
  }

  ngOnInit() {
    let self = this;
    $(window).on('popstate', function () {
      self.payerModal.close();
    });
  }

  setGridDisplayByPermission() {
    this.columns = [
      { data: 'siteBankId', visible: false, name: 'SiteBankId', title: 'Site BankId', class: 'alignRight' },
      { data: 'edit', visible: this.hasManagePermission, name: "edit", title: '', sortable: false },
      { data: 'delete', visible: this.hasManagePermission, name: "delete", title: '', sortable: false },
      { data: 'routingNumber', title: 'R/T', class: 'alignRight' },
      { data: 'account', title: 'Account Number', class: 'alignRight' },
      { data: 'payerName', title: 'Payer' },
      { data: 'modificationDate', name: "modDate", title: 'Modified' },
      { data: 'modifiedBy', title: 'Modified By' }
    ];
  }
  getPayers(siteBankId: number, workgroupId: number) {

    if ((siteBankId === 0) && (workgroupId === 0)) {
      this.loading = false;
      this.isValidWorkgroup = false;
      this.emptyTableMessage = 'No data available in table(Please select a valid workgroup)';
      this.payerData = [];
    }
    else {
      this.isValidWorkgroup = true;
      this.emptyTableMessage = 'No data available in table';
      this.loading = true;
      this.payerService.getPayers(siteBankId, workgroupId, this.isPageView).subscribe(
        records => {
          this.loading = false;
          if (!records.success) {
            this.displayNotification('error', 'Error!', 'Error retrieving payers!')
          } else {
            let data = records.data.payers;
            const options = { edit: "", delete: "" }
            data = data.reduce((acc, val) => acc.concat(val), [])
            this.payerData = data.map(p => Object.assign(p, options));
            this.bankDetail = `${records.data.siteBankId} - ${records.data.siteBankName}`;
            this.workGroupDetail = `${records.data.siteClientAccountId} - ${records.data.siteClientAccountName}`;
          }
        },
        error => {
          this.loading = false;
          this.displayNotification('error', 'Error!', 'Error retrieving payers!')
        });
    }
  }

  displayNotification(type, title, body) {
    this.loading = false;
    this.toasterService.pop(type, title, body);
  }

  addPayer() {
    this.payerModal.currentOperation = Operation.Add;
    this.payerModal.payer = new Payer();
    this.payerModal.payer.siteBankId = this.selectedWorkgroup.BankId;
    this.payerModal.payer.siteClientAccountId = this.selectedWorkgroup.WorkgroupId;
    this.payerModal.open();
  }

  payerAdded(success: boolean) {
    if (success) {
      this.getPayers(this.selectedWorkgroup.BankId, this.selectedWorkgroup.WorkgroupId);
      this.displayNotification("success", "Success!", "Payer successfully added!")
    } else {
      this.displayNotification("error", "Error", "An error occurred while adding payer!")
    }
  }

  editPayer(payerJson) {
    this.payerModal.currentOperation = Operation.Update;
    this.payerModal.payer = Object.assign(new Payer(), payerJson);
    this.payerModal.open();
  }

  payerUpdated(success) {
    if (success) {
      this.displayNotification("success", "Success!", "Payer Updated!");
      this.getPayers(this.selectedWorkgroup.BankId, this.selectedWorkgroup.WorkgroupId)
    } else {
      this.displayNotification("error", "Error", "An error occurred while updating payer!")
    }
  }

  deletePayer(payerJson) {

    this.selectedPayer = Object.assign(new Payer(), payerJson);
    this.confirmDialog.confirmText = "Delete"
    this.confirmDialog.body = this.getDeleteModalHtml(this.selectedPayer);
    this.confirmDialog.open();
  }

  getDeleteModalHtml(payer: Payer) {
    let html: string;
    return html = `<label>Are you sure you want to delete the payer?</label><div class="form">
    <div class="form-group">
    <div class="row">
    <div class="col-sm-3">
     <label>Payer:</label>
    </div>
    <div class="col-sm-9">
     ${payer.payerName}
    </div>
    </div>
    </div>
    <div class="form-group">
    <div class="row">
    <div class="col-sm-3">
    <label>Account Number:</label>
    </div>
    <div class="col-sm-9">
    ${payer.account}
    </div>
    </div>
    </div>
    <div class="form-group">
    <div class="row">
    <div class="col-sm-3">
    <label>R / T:</label> 
    </div>
    <div class="col-sm-9">
     ${payer.routingNumber}
    </div>
    </div>
    </div>
    <div class="form-group">
    <div class="row">
    <div class="col-sm-3">
    <label>Bank:</label>
    </div>
    <div class="col-sm-9">
    ${this.bankDetail}
    </div>
    </div>
    </div>
    <div class="form-group">
    <div class="row">
    <div class="col-sm-3">
    <label>Workgroup:</label>
    </div>
    <div class="col-sm-9">
    ${this.workGroupDetail}
    </div>
    </div>
    </div>
    </div>`;
  }

  confirmDelete(payer) {
    this.loading = true;
    this.payerService.deletePayer(this.selectedWorkgroup.BankId, this.selectedWorkgroup.WorkgroupId, this.selectedPayer.routingNumber, this.selectedPayer.account).subscribe(
      result => {
        this.loading = false;
        if (result.success) {
          this.getPayers(this.selectedWorkgroup.BankId, this.selectedWorkgroup.WorkgroupId);
          this.displayNotification("success", "Success!", "Payer successfully deleted!")
        } else {
          this.displayNotification("error", "Error", "An error occurred while deleting payer!")
        }
      });
  }


  createdRow(data: DataTableRow) {
    if (this.loading) {
      return;
    }

    let api = data.table;
    let colindex = api.column('edit:name')[0][0];
    let td = api.cells(data.element, colindex).nodes()[0];
    $(td).html('<i class="fa fa-edit"  style="font-size:14px;cursor:pointer"></i>');

    colindex = api.column('delete:name')[0][0];
    td = api.cells(data.element, colindex).nodes()[0];
    $(td).html('<i class="fa fa-trash"  style="font-size:14px;cursor:pointer"></i>');
  }

  clickedRow(row: any) {
  }

  clickedColumn(event: any) {
    if (event.column.childNodes.length > 0) {
      if (event.column.childNodes[0].className == "fa fa-edit") {
        this.editPayer(event.data);
      }
      if (event.column.childNodes[0].className == "fa fa-trash") {
        this.deletePayer(event.data);
      }
    }
  }

  tableInitiated(event) {
    //update icons column width
    //
    var theaders = $('#payermaintenance-grid th');
    theaders[0].style.width = '20px';
    theaders[1].style.width = '20px';
  }

  workgroupChange() {
    if (!this.selectedWorkgroup)
      return;

    this.getPayers(this.selectedWorkgroup.BankId, this.selectedWorkgroup.WorkgroupId);
    this.isPageView = false;
    $('#payermaintenance-wgs').css('max-width', '280px');
  }

  selectedEntityChange() {
  }

  refresh() {
    if (this.permissionsLoading) {
      console.log('Payer Maintenance: Supressing refresh, we do not have permissions loaded.');
      return;
    }
    if (!this.selectedWorkgroup) {
      console.log('Payer Maintenance: Supressing refresh, we do not have an entity or workgroup selected.');
      return;
    }

    this.getPayers(this.selectedWorkgroup.BankId, this.selectedWorkgroup.WorkgroupId);
  }
}