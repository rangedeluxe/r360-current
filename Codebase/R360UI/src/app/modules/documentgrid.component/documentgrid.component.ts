import { Component, Input, OnInit } from '@angular/core';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { ImageService } from '../../services/image.service/image.service';
import { DocumentGridDTO } from '../../DTOs/DocumentGridDTO';

let self;

@Component({
    moduleId: module.id.toString(),
    selector: 'documentgrid',
    templateUrl: 'documentgrid.component.html',
    styleUrls: ['documentgrid.component.css'],
    providers: [ImageService]
})

export class DocumentGridComponent implements OnInit {
    
    @Input() documentDataDTO: DocumentGridDTO;
    @Input() loading: boolean;

    public lengthOptions: Array<number> = [10, 25, 50, 100, 1000];

    public columns: Array<any> = [
        { data: 'DocumentSequence', name: 'Image', orderable: false },
        { data: 'DocumentSequence', title: 'Document Sequence' },
        { data: 'SequenceWithinTransaction', title: 'Sequence Within Transaction'},
        { data: 'Description', title: 'Description' }
    ];
    public columnOrder: Array<any> = [
        [1,'asc']
    ];


    constructor ( 
        private imageService: ImageService,
    ) {
        self = this;
    }

    public createdRow(row: DataTableRow) {
        if (this.loading)
            return;

        const imgindex = row.table.column('Image:name')[0][0];
        const imgtd = row.table.cells(row.index, imgindex).nodes()[0];
        $(imgtd).empty();

        if (row.data.DocumentIconVisible === true) {
            $(imgtd).append($('<a class="hideOnPrint" />')
                .attr('title', 'View Transaction Image(s)')
                .append(
                    $('<i class="fa fa-file-o fa-lg" />')
                )
                .click((evt) => {
                    this.imageService.displaySingleDocument(this.documentDataDTO.bankId, this.documentDataDTO.workgroupId, this.documentDataDTO.batchId,
                        this.documentDataDTO.depositDateString, this.documentDataDTO.transactionId, row.data.BatchSequence, this.documentDataDTO.importTypeShortName);
                    return false;
                }));
        }
    }

    ngOnInit(): void {
    }
}
