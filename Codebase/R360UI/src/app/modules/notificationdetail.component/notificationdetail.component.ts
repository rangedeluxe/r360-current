import { Component, OnInit } from '@angular/core';

import { BreadcrumbService } from '../../services/breadcrumb.service/breadcrumb.service';
import { Pages } from '../../DTOs/Pages';
import { SharedStorageService } from '../../services/sharedstorage.service/sharedstorage.service';
import { ToasterService } from 'angular2-toaster';

import { NotificationsData} from '../../DTOs/NotificationsData';
import { NotificationDetailService} from '../../services/notificationdetail.service/notificationdetail.service';
import { NotificationDetailFileGridDTO } from '../../DTOs/NotificationDetailFileGirdDTO';
import { DateService } from '../../services/date.service/date.service';

@Component({
   moduleId: module.id.toString(),
   selector: 'notificationdetail',
   templateUrl: 'notificationdetail.component.html',
   styleUrls: ['notificationdetail.component.css'],
   providers: [
      SharedStorageService,
      NotificationDetailService,
      DateService
   ]
})

export class NotificationDetailComponent implements OnInit {

   public loading: boolean = true;

   public hasSharedStorageValue: Boolean = false;
   public notificationDetail: any;
   public notificationDetailFileGridDTO: NotificationDetailFileGridDTO;
   public selectedMessageGroup: any;

   constructor(        
      private breadcrumbService: BreadcrumbService,
      private sharedStorageService: SharedStorageService,
      private toasterService: ToasterService,
      private notificationDetailService: NotificationDetailService,
      private dateService: DateService
   ) { }

   public loadData(query: NotificationsData) {
      if (query == null) {
          return;
      }
      this.selectedMessageGroup = query.MessageGroup;
      this.hasSharedStorageValue = true;
      this.sharedStorageService.DeleteData(Pages.NotificationDetail);
  }

   public reload() {
      this.loading = true;
      this.notificationDetailService.getNotificationDetail(this.selectedMessageGroup)
      .subscribe(
         results => {
            if (results && results.success) {
               this.notificationDetail = results.data;
               this.notificationDetail.DateString = this.notificationDetail.DateString + 
               ' ' + this.dateService.getCurrentTimeZoneAbbreviation();
               this.notificationDetailFileGridDTO = new NotificationDetailFileGridDTO (
                  <boolean>results.metadata.HasDownloadPermission,
                  this.notificationDetail.Files
               );
            }
            else {
               console.log(results.error);
               this.toasterService.pop('error', 'Error', 'An error occurred while loading notification details data.');
            }
            this.loading = false;
         },
         errors => {
            this.toasterService.pop('error', 'Error', 'An error occurred while loading notification details data.');
            this.loading = false;
         }
      );
   }

   ngOnInit() {
      this.loadData(this.sharedStorageService.GetData(Pages.NotificationDetail));
      this.reload();
   }
}
