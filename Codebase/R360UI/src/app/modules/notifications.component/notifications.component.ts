import { Component, ViewChild, Input, OnInit, ViewChildren, QueryList } from '@angular/core';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { DatatableComponent } from '../../controls/datatable.component/datatable.component';
import { Pages } from '../../DTOs/Pages';
import { SharedStorageService } from '../../services/sharedstorage.service/sharedstorage.service';
import { ToasterService } from 'angular2-toaster';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsQuery } from '../../DTOs/NotificationsQuery';
import { NotificationFileTypes } from '../../DTOs/NotificationFileTypes';
import { NotificationsService } from '../../services/notifications.service/notifications.service';
import { Workgroup } from '../../DTOs/Workgroup';
import { forkJoin } from 'rxjs';
import { Globals } from '../../globals';
import { Router } from '@angular/router';
import { BreadcrumbService } from '../../services/breadcrumb.service/breadcrumb.service';
import { DatePipe } from '@angular/common';
import { DatePickerComponent } from '../../controls/datepicker.component/datepicker.component';
import { WorkgroupSelectorComponent } from '../../controls/workgroupselector.component/workgroupselector.component';
import { DateService } from '../../services/date.service/date.service';


let self;

@Component({
   moduleId: module.id.toString(),
   selector: 'notifications',
   templateUrl: 'notifications.component.html',
   styleUrls: ['notifications.component.css'],
   providers: [
      SharedStorageService,
      NotificationsService,
      DatePipe,
      DateService
   ]
})

export class NotificationsComponent implements OnInit {

   @ViewChild(DatatableComponent, { static: false }) dataTable: DatatableComponent;
   @ViewChildren(DatePickerComponent) datePickers: QueryList<any>;
   @ViewChild(WorkgroupSelectorComponent, { static: false }) workgroupSelector: WorkgroupSelectorComponent;

   public loading: boolean = true;
   public query: NotificationsQuery = new NotificationsQuery;
   public notificationFileTypes: NotificationFileTypes[];
   public selectedNotificationFileType: NotificationFileTypes;
   public selectedWorkgroup: Workgroup;
   public selectedStartDate: string;
   public selectedEndDate: string;
   public enteredFileName: string;
   public selectedFileTypeKey: number;
  
   private executeSearch: boolean = false;
   private BC_NAME = 'Notifications';

   // datatable settings
   public lengthOptions: Array<number> = [10, 25, 50, 100, 1000];
   public columns: Array<any> = [
      { data: 'DateString', title: 'Date', name: 'Date', OrderByName: 'DATE', width: '15' },
      { data: 'Id', title: 'ID', name: 'ID', OrderByName: 'NOTIFICATIONSOURCE', width: '10%' },
      { data: 'Message', title: 'Message', OrderByName: 'MESSAGE', width: '65%' },
      { data: 'AttachmentCount', title: 'Attachments', name: 'Attachments', OrderByName: 'FILE', width: '10%' }
   ];
   public notificationsdata: Array<any>;
   public columnOrder: Array<any> = [
      [1, 'asc']
   ];

   constructor(
      private sharedStorageService: SharedStorageService,
      private toasterService: ToasterService,
      private notificationsService: NotificationsService,
      private router: Router,
      private breadCrumbService: BreadcrumbService,
      private datePipe: DatePipe,
      private dateService: DateService) {
      self = this;
   }

   ngOnInit(): void {
      this.loadData(this.sharedStorageService.GetData(Pages.Notifications));
      this.reload();
   }

   workgroupSelectorLoaded() {
      if (this.executeSearch) {
         this.dataTable.refresh();
      }
   }

   private loadData(query: NotificationsQuery) {
      if (!query) {
         this.executeSearch = false;
         return;
      }
      
      this.executeSearch = true;
      this.selectedStartDate = query.StartDate;
      this.selectedEndDate = query.EndDate;
      this.selectedWorkgroup = new Workgroup(query.Workgroup);
      this.selectedFileTypeKey = query.FileTypeKey;
      this.enteredFileName = query.FileName;
      this.sharedStorageService.DeleteData(Pages.Notifications);
   }

   private getToday(): string {
      return self.datePipe.transform(new Date(), 'MM/dd/yyyy');
   }

   private validateDateRange() {
      

      if (!this.selectedStartDate) {return;} 
      if (!this.selectedEndDate) {return;}

      let startDate = new Date(this.selectedStartDate);
      let endDate = new Date(this.selectedEndDate);
      let startDatePicker = this.datePickers.find(datePicker => datePicker.htmlId == "startDate");
      let endDatePicker = this.datePickers.find(datePicker => datePicker.htmlId == "endDate");

      if (startDate > endDate) {
         var newEndDate = this.selectedStartDate;
         this.selectedStartDate = this.selectedEndDate
         this.selectedEndDate = newEndDate;
         startDatePicker.setDateValue(this.selectedStartDate);
         endDatePicker.setDateValue(this.selectedEndDate);
      }
   }

   public startDateChanged(date: string) {
      if (date) {
         this.selectedStartDate = date;
      }
   }

   public endDateChanged(date: string) {
      if (date) {
         this.selectedEndDate = date;
      }
   }


   public workgroupChanged() {
      if (!this.selectedWorkgroup) {
         return;
      }
   }

   public fileTypeChanged(fileTypeKey: number) {
      this.selectedFileTypeKey = fileTypeKey;
   }

   public setSearchValues(d) {

      if (self.selectedWorkgroup && self.selectedWorkgroup.BankId !== 0 &&
         self.selectedWorkgroup.WorkgroupId !== 0) {
         d.Workgroup = self.selectedWorkgroup.BankId + '|' + self.selectedWorkgroup.WorkgroupId;
      }
      else if (self.selectedWorkgroup) {
         d.Workgroup = self.selectedWorkgroup.EntityId;
      }

      d.StartDate = self.selectedStartDate;
      d.EndDate = self.selectedEndDate;
      d.FileName = self.enteredFileName;
      d.FileTypeKey = self.selectedFileTypeKey;
      d.SortBy = self.columns[d.order[0]['column']];
      d.SortByDir = d.order[0]['dir'];
      d.UtcOffset = self.dateService.getCurrentUtcOffset();
      console.log("Using utc offset of " + d.UtcOffset);
      console.log("Recognized time zone as " + self.dateService.getCurrentTimeZone());
   }
   
   public clickedRow(row: DataTableRow) {
      const breadCrumbData = {
         Workgroup: this.selectedWorkgroup,
         StartDate: this.selectedStartDate,
         EndDate: this.selectedEndDate,
         FileName: this.enteredFileName,
         FileTypeKey: this.selectedFileTypeKey
      };

      this.breadCrumbService.push(this.BC_NAME, '/Notifications', breadCrumbData);
      const notificationDetailData = { MessageGroup: row.data.MessageGroup };
      this.sharedStorageService.WriteData('notificationsdetaildata', notificationDetailData);
      this.router.navigateByUrl('/NotificationDetail');
   }

   public search() {
      this.validateDateRange();
      this.dataTable.refresh();
   }

   public reset() {
      this.selectedStartDate = this.getToday();
      this.selectedEndDate = this.getToday();
      this.datePickers.forEach(datePicker => datePicker.setDateValue(this.getToday()));
      this.selectedWorkgroup = new Workgroup();
      this.workgroupSelector.selectedWorkgroup = this.selectedWorkgroup;
      this.enteredFileName = '';

      if (this.notificationFileTypes) {
         this.selectedNotificationFileType = this.notificationFileTypes[0];
      }

      this.selectedFileTypeKey = this.selectedNotificationFileType.NotificationFileTypeKey;

      this.dataTable.refresh();
   }

   public setOrder(e, settings, data) {
      var column = this.columns[data.order[0].column];
      data.SortBy = column.OrderByName;
      data.SortByDir = data.order[0].dir;
   }

   public reload() {
      this.loading = true;
      this.notificationsService.getNotificationsFileTypes()
         .subscribe(
            results => {
               if (results) {

                  this.notificationFileTypes = results.data;
                  if (this.selectedFileTypeKey == null) {
                     this.selectedNotificationFileType = this.notificationFileTypes[0];
                  }
                  else {
                     this.selectedNotificationFileType =
                        this.notificationFileTypes.find(nft => nft.NotificationFileTypeKey === this.selectedFileTypeKey);
                  }
               }
               else {
                  console.log(results.error);
                  this.toasterService.pop('error', 'Error', 'An error occurred while loading notification data.');
               }
               this.loading = false;
            },
            errors => {
               this.toasterService.pop('error', 'Error', 'An error occurred while loading notification data.');
               this.loading = false;
            }
         );
   }

}
