
import {forkJoin as observableForkJoin,  Observable } from 'rxjs';

import { Component, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { Workgroup } from '../../DTOs/Workgroup';
import { BatchDetailQuery } from '../../DTOs/BatchDetailQuery';
import { RowGroup } from '../../DTOs/RowGroup';
import { DataTableGroup } from '../../DTOs/DataTableGroup';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { DatatableComponent } from '../../controls/datatable.component/datatable.component';
import { BatchDetailService } from '../../services/batchdetail.service/batchdetail.service';
import { ToCurrencyPipe } from '../../pipes/tocurrency.pipe';
import { ToasterService } from 'angular2-toaster';
import { CurrencyPipe } from '@angular/common';
import { Router } from '@angular/router';
import { ImageService } from '../../services/image.service/image.service';
import { UserPreferencesService } from '../../services/userpreferences.service/userpreferences.service';
import { LocalStorageService } from '../../services/localstorage.service/localstorage.service';
import { BreadcrumbService } from '../../services/breadcrumb.service/breadcrumb.service';
import { TransactionDetailQuery } from '../../DTOs/TransactionDetailQuery';
import { SharedStorageService } from '../../services/sharedstorage.service/sharedstorage.service';
import { Pages } from '../../DTOs/Pages';


let self;

@Component({
   moduleId: module.id.toString(),
   selector: 'batchdetail',
   templateUrl: 'batchdetail.component.html',
   styleUrls: ['batchdetail.component.css'],
   providers: [ToCurrencyPipe, BatchDetailService, ImageService,
      UserPreferencesService, LocalStorageService, BreadcrumbService],
   outputs: ['onNavigateAwayClick']
})

export class BatchDetailComponent implements OnInit {

   public onNavigateAwayClick: EventEmitter<void> = new EventEmitter<void>();
   private router: Router;

   public loading: boolean = true;
   private batchDetailService: BatchDetailService;
   private toasterService: ToasterService;
   private localStorage: LocalStorageService;
   private currencyPipe: ToCurrencyPipe;
   private imageService: ImageService;
   private userPrefService: UserPreferencesService;
   private breadCrumbService: BreadcrumbService;
   private sharedStorageService: SharedStorageService;

   private readonly STORED_QUERY: string = 'batchdetaildata';
   private readonly BC_NAME: string = 'Batch Detail';
   public hasSharedStorageValue: Boolean = false;
   public searchValue: string;
   public selectedDepositDate: string;
   public selectedBankId: number;
   public selectedBatchId: number;
   public selectedWorkgroup: number;
   public depositDate: string;
   public batchId: number;
   public sourceBatchId: number;
   public accountSiteCode: number;
   public batchCue: number;
   public showAccountSiteCode: boolean;
   public showBank: boolean;
   public showBatchCue: boolean;
   public showBatchSiteCode: boolean;
   public showImageRpsAudit: boolean;
   public showPayerName: boolean = false;
   public showImageAllIcon: boolean = false;
   public showBatchId: boolean = false;
   public workGroup: string;
   public bank: number;
   public batchSiteCode: number;
   public lengthOptions: Array<number> = [10, 25, 50, 100, 1000];
   public totals: number;
   public transactionID: number;
   public workgroupId: number;
   public recordsTotal: number;
   public recordsFiltered: number;
   public BatchNumber : string;

   @ViewChild(DatatableComponent, {static: false}) dataTable: DatatableComponent;
   @Input() batchDetailData: Array<any>;

   public columns: Array<any> = [
      { data: 'TxnSequence', visible: true, name: 'Transaction', title: 'Transaction', class: 'alignRight' },
      { data: 'Amount', title: 'Payment Amount', name: 'Amount', class: 'alignRight' },
      { data: 'TransactionID', name: 'Images', orderable: false },
      { data: 'RT', title: 'R/T', class: 'alignRight' },
      { data: 'Account', title: 'Account Number', class: 'alignRight' },
      { data: 'Serial', title: 'Check/Trace/Ref Number', class: 'alignRight' },
      { data: 'RemitterName', name: 'Payer', title: 'Payer', class: 'alignRight', visible: this.showPayerName },
      { data: 'DDA', title: 'DDA', class: 'alignRight' },
      { data: 'TransactionID', name: 'RPS', orderable: false }
   ];

   constructor(
      batchDetailService: BatchDetailService,
      toasterService: ToasterService,
      localStorageService: LocalStorageService,
      currencyPipe: ToCurrencyPipe,
      router: Router,
      imageService: ImageService,
      userPrefService: UserPreferencesService,
      breadcrumbService: BreadcrumbService,
      sharedStorageService: SharedStorageService

   ) {
      this.batchDetailService = batchDetailService;
      this.toasterService = toasterService;
      this.localStorage = localStorageService;
      this.currencyPipe = currencyPipe;
      this.router = router;
      this.imageService = imageService;
      this.userPrefService = userPrefService;
      this.breadCrumbService = breadcrumbService;
      this.sharedStorageService = sharedStorageService;
      self = this;
   }

   public errorHandler(e, settings, techNote, message) {
      console.log(`BatchDetail: Error from datatable: ${message}`);
   }

   public clickedRow(row: DataTableRow) {
      // bread the data required by batch detail.
      const breaddata = {
         depositDate: this.selectedDepositDate,
         workGroup: this.selectedWorkgroup,
         bankId: this.selectedBankId,
         batchId: this.selectedBatchId
      } as BatchDetailQuery;
      this.breadCrumbService.push(this.BC_NAME, '/BatchDetail', breaddata);

      // push data for transaction detail (this really should be router data)
      const data = row.data;
      const transactiondetaildata = {
         bankId: data.BankId,
         workgroupId: data.WorkgroupId,
         batchId: data.BatchId,
         depositDate: data.DepositDateString,
         transactionId: data.TransactionID,
         transactionSequence: data.TxnSequence
      } as TransactionDetailQuery;
      this.onNavigateAwayClick.emit();
      this.localStorage.write('transactiondetaildata', transactiondetaildata);
      this.router.navigateByUrl('/TransactionDetail');
   }

   public createdRow(row: DataTableRow) {
      if (this.loading) {
         return;
      }

      // format currency for amount
      const colIndex = row.table.column('Amount:name')[0][0];
      const td = row.table.cells(row.index, colIndex).nodes()[0];
      const amt = this.currencyPipe.transform(row.data.Amount.toFixed(2));
      $(td).html(amt);


      // handle icons for images
      const imgindex = row.table.column('Images:name')[0][0];
      const imgtd = row.table.cells(row.index, imgindex).nodes()[0];
      $(imgtd).empty();
      if (row.data.ShowPaymentIcon === true) {
         $(imgtd).append($('<a class="hideOnPrint" />')
            .attr('title', 'View Payment Image(s)')
            .append(
               $('<i class="fa fa-money fa-lg" />')
            )
            .click((evt) => {
               this.imageService.displaySinglePayment(row.data.BankId, row.data.WorkgroupId, row.data.BatchId,
                  row.data.DepositDateString, row.data.TransactionID, row.data.BatchSequence);
               return false;
            }));
      }
      if (row.data.ShowDocumentIcon === true) {
         $(imgtd).append($('<a class="hideOnPrint" />')
            .attr('title', 'View Transaction Image(s)')
            .append(
               $('<i class="fa fa-file-o fa-lg" />')
            )
            .click((evt) => {
               this.imageService.displayTransactionDocuments(row.data.BankId, row.data.WorkgroupId, row.data.BatchId,
                  row.data.DepositDateString, row.data.TransactionID);
               return false;
            }));
      }
      if (row.data.ShowAllIcon === true) {
         $(imgtd).append($('<a class="hideOnPrint" />')
            .attr('title', 'View All Images for Transaction')
            .append(
               $('<i class="fa fa-picture-o fa-lg" />')
            )
            .click((evt) => {
               this.imageService.displayTransactionImages(row.data.BankId, row.data.WorkgroupId, row.data.BatchId,
                  row.data.DepositDateString, row.data.TransactionID);
               return false;
            }));
      }

      const rpsindex = row.table.column('RPS:name')[0][0];
      const rpstd = row.table.cells(row.index, rpsindex).nodes()[0];
      $(rpstd).empty();
      if (this.showImageRpsAudit === true) {
         $(rpstd).append($('<a class="hideOnPrint" />')
            .attr('title', 'View ImageRPS Audit')
            .append(
               $('<i class="fa fa-print fa-lg" />')
            )
            .click((evt) => {
               this.imageService.displayTransactionReport(row.data.BankId, row.data.WorkgroupId, row.data.BatchId,
                  row.data.DepositDateString, row.data.TxnSequence, row.data.TransactionID,
                  row.data.BatchSequence, row.data.BatchNumber, row.data.ProcessingDateString);
               return false;
            }));
      }
   }

   public loadQuery() {
      // check for query from batch summary AND from breadcrumbs.
      const query = this.sharedStorageService.GetData(Pages.BatchDetail) as BatchDetailQuery;
      if (query) {
         this.hasSharedStorageValue = true;
         this.selectedDepositDate = query.depositDate;
         this.selectedWorkgroup = query.workGroup;
         this.selectedBankId = query.bankId;
         this.selectedBatchId = query.batchId;
         this.batchId = query.batchId;
         // Get the first record back for display
         observableForkJoin(
            
            this.batchDetailService.getBatchDetail(this.selectedBankId, this.selectedWorkgroup,
                  this.selectedDepositDate, this.batchId, 0, 1),
            this.userPrefService.getAuthorizedPreferences()
                  
         ).subscribe(
            async results => {
               if (!results || !results[0] || !results[1] || !results[0].success) {
                  this.toasterService.pop('error', 'Error', 'An error occurred while loading batch detail data.');
                  return;
               }
               // Now we load up the image data.
               const batchdata = results[0];
               const userprefdata = results[1];
               if (batchdata.data[0].PaymentType.toUpperCase() === 'ACH'
                  || batchdata.data[0].PaymentType.toUpperCase() === 'WIRE') {
                  this.showImageAllIcon = true;
               }
               else {
                  this.showImageAllIcon = await this.imageService.imagesExist({
                     BankId: batchdata.bank,
                     WorkgroupId: batchdata.workgroupId,
                     BatchId: batchdata.batchId,
                     DepositDateString: batchdata.depositDate,
                     SourceBatchId: batchdata.sourceBatchId,
                     PICSDate: batchdata.data[0].PICSDate,
                     BatchPaymentSource: batchdata.data[0].BatchSourceShortName,
                     ImportTypeShortName: batchdata.data[0].ImportTypeShortName
                  });
               }

               this.depositDate = batchdata.depositDate;
               this.batchId = batchdata.batchId;
               this.sourceBatchId = batchdata.sourceBatchId;
               this.accountSiteCode = batchdata.accountSiteCode;
               this.batchCue = batchdata.batchCue;
               this.batchSiteCode = batchdata.batchSiteCode;
               this.showAccountSiteCode = batchdata.showAccountSiteCode;
               this.showBank = batchdata.showBank;
               this.showBatchCue = batchdata.showBatchCue;
               this.showBatchId = batchdata.showBatchId;
               this.showBatchSiteCode = batchdata.showBatchSiteCode;               
               this.showImageRpsAudit = batchdata.showImageRpsAudit;
               this.workGroup = batchdata.workgroup;
               this.bank = batchdata.bank;
               this.totals = batchdata.totalAmount;
               this.workgroupId = batchdata.workgroupId;
               this.recordsTotal = batchdata.recordsTotal;
               this.recordsFiltered = batchdata.recordsFiltered;
               
               this.showPayerName = (!userprefdata.displayRemitterNameInPDF) 
                  ? false 
                  : userprefdata.displayRemitterNameInPDF;

               this.BatchNumber = batchdata.batch;
               this.loading = false;
            },
            error => {
                  
               this.toasterService.pop('error', 'Error', 'An error occurred while loading batch detail data.');
               this.loading = false;
            }
         );
      }
      else{
         this.loading = false;
      }
   }

   public buildFooter(tabledata: any) {
      const pipe = new ToCurrencyPipe();
      const cssclass = self.showImageAllIcon ? '' : 'hide';
      return [
         { columnLength: 2, value: '' },
         {
            columnLength: 1,
            value: $(`<td class="viewall hideOnPrint ${cssclass}" />`)
               .append(($(`<a />`))
                  .attr('title', 'View All Images for Batch')
                  .append($('<i class="fa fa-picture-o fa-lg" />'))
                  .append($('<span> View All</span>'))
               )
               .click((evt) => {
                  const rowdata = tabledata[0];
                  self.imageService.displayBatchImages(rowdata.BankId, rowdata.WorkgroupId,
                     rowdata.BatchId, rowdata.BatchNumber, rowdata.DepositDateString);
                  return false;
               })
         },
         { columnLength: 3, value: '' },
         { columnLength: 4, value: 'Total Amount: ' + self.currencyPipe.transform(self.totals), class: 'alignRight' }
      ] as Array<RowGroup>;
   }

   public search(searchValue: string) {
      self.searchValue = searchValue;
      self.buildData();
   }

   public setColumnVisibility() {
      this.dataTable.setColumnVisibility('Payer', this.showPayerName);
   }

   public buildData() {
      this.dataTable.refresh();
   }

   public setSearchValues(d) {
      d.DepositDateString = self.depositDate;
      d.BankId = self.bankId;
      d.BatchId = self.batchId;
      d.WorkgroupId = self.workgroupId;
      d.search = self.searchValue;
   }

   public buildGroup(tabledata: any, group: DataTableGroup, groupvalue: any): Array<RowGroup> {
      // Reminder - we're not in the context of this component, as this is called from the child datatable.
      let pipe = new ToCurrencyPipe();
      return [
         { columnLength: 6, value: '' },
         { columnLength: 1, value: '', class: 'alignRight' }
      ] as Array<RowGroup>;
   }

   ngOnInit() {
      this.loading = true;
      this.loadQuery();
   }
}