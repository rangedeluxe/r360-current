import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id.toString(),
    selector: 'r360-invoicesearch',
    templateUrl: './invoicesearch.component.html',
    styleUrls: ['./invoicesearch.component.css']
})
export class InvoiceSearchComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}