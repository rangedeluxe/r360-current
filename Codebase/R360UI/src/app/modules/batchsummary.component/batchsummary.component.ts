
import { forkJoin as observableForkJoin, Observable } from 'rxjs';

import { Component, OnInit, ViewChild, Input, EventEmitter, AfterViewInit } from '@angular/core';
import { BatchSummaryService } from '../../services/batchsummary.service/batchsummary.service';
import { ToCurrencyPipe } from '../../pipes/tocurrency.pipe';
import { Workgroup } from '../../DTOs/Workgroup';
import { BatchSummaryQuery } from '../../DTOs/BatchSummaryQuery';
import { BatchDetailQuery } from '../../DTOs/BatchDetailQuery';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { DatatableComponent } from '../../controls/datatable.component/datatable.component';
import { PaymentSources } from '../../DTOs/PaymentSources';
import { PaymentTypes } from '../../DTOs/PaymentTypes';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { RowGroup } from '../../DTOs/RowGroup';

import { BreadcrumbService } from '../../services/breadcrumb.service/breadcrumb.service';
import { LocalStorageService } from '../../services/localstorage.service/localstorage.service';
import { Breadcrumb } from '../../DTOs/Breadcrumb';
import { SharedStorageService } from '../../services/sharedstorage.service/sharedstorage.service';
import { Pages } from '../../DTOs/Pages';

let self;

@Component({
    moduleId: module.id.toString(),
    selector: 'batchsummary',
    templateUrl: 'batchsummary.component.html',
    styleUrls: ['batchsummary.component.css'],
    providers: [BatchSummaryService, BreadcrumbService, LocalStorageService, ToCurrencyPipe],
    outputs: ['onNavigateAwayClick']
})

/*
service URL:
/RecHubViews/BatchSummary/GetBatches
*/
export class BatchSummaryComponent implements OnInit {

    @Input() batchSummaryData: Array<any>;
    @ViewChild(DatatableComponent, { static: false }) dataTable: DatatableComponent;

    private localStorageService: LocalStorageService;
    private currencyPipe: ToCurrencyPipe;
    private breadcrumbService: BreadcrumbService;
    private router: Router;
    private readonly BC_NAME: string = 'Batch Summary';
    private readonly STORED_QUERY = 'batchsummarydata';
    private batchSummaryService: BatchSummaryService;
    private toasterService: ToasterService;
    private selectedPaymentSourceId: number;
    private selectedPaymentTypeId: number;
    private sharedStorageService: SharedStorageService;

    public onNavigateAwayClick: EventEmitter<void> = new EventEmitter<void>();
    public loading: boolean = true;
    public paymentSources: PaymentSources[];
    public selectedPaymentSource: PaymentSources;
    public paymentTypes: PaymentTypes[];
    public selectedPaymentType: PaymentTypes;
    public depositDateFrom: string;
    public depositDateTo: string;
    public searchValue: string = '';
    public hasSharedStorageValue: Boolean = false;
    public lengthOptions: Array<number> = [10, 25, 50, 100, 1000];
    public selectedWorkgroup: Workgroup;
    public visibleColumns: number;

    public columns: Array<any> = [
        { data: 'SourceBatchID', visible: false, name: 'BatchID', title: 'Batch ID', class: 'alignRight' },
        { data: 'BatchNumber', title: 'Batch', class: 'alignRight' },
        { data: 'DepositDateString', title: 'Deposit Date' },
        { data: 'PaymentSource', title: 'Payment Source' },
        { data: 'PaymentType', title: 'Payment Type' },
        { data: 'DDA', title: 'DDA' },
        { data: 'BatchSiteCode', name: 'BatchSiteCode', title: 'Batch Site Code', visible: false, class: 'alignRight' },
        { data: 'TransactionCount', title: 'Transaction Count', class: 'alignRight' },
        { data: 'PaymentCount', title: 'Payment Count', class: 'alignRight' },
        { data: 'DocumentCount', title: 'Document Count', class: 'alignRight' },
        { data: 'TotalAmount', title: 'Batch Total', name: 'Total', class: 'alignRight' }
    ];

    constructor(
        batchSummaryService: BatchSummaryService,
        toasterService: ToasterService,
        currencyPipe: ToCurrencyPipe,
        router: Router,
        breadcrumbService: BreadcrumbService,
        sharedService: SharedStorageService
    ) {
        this.batchSummaryService = batchSummaryService;
        this.toasterService = toasterService;
        this.currencyPipe = currencyPipe;
        this.router = router;
        this.breadcrumbService = breadcrumbService;
        this.sharedStorageService = sharedService;
        self = this;
    }

    public clickedRow(row: any) {
        const breaddata = {
            dateFrom: this.depositDateFrom,
            dateTo: this.depositDateTo,
            workgroup: this.selectedWorkgroup,
            paymentTypeId: this.selectedPaymentTypeId,
            paymentSourceId: this.selectedPaymentSourceId,
            searchValue: this.searchValue
        };
        this.breadcrumbService.push(this.BC_NAME, '/BatchSummary', breaddata);
        const data = row.data;
        const batchdetaildata = {
            bankId: data.BankID,
            workGroup: data.WorkgroupID,
            depositDate: data.DepositDateString,
            batchId: data.BatchID,
            batchNumber: data.BatchNumber
        } as BatchDetailQuery;
        this.onNavigateAwayClick.emit();
        this.sharedStorageService.WriteData('batchdetaildata', batchdetaildata);
        this.router.navigateByUrl('/BatchDetail');
    }

    public dateFromChange(date: string) {
        if (date) {
            this.depositDateFrom = date;
        }
    }

    public dateToChange(date: string) {
        if (date) {
            this.depositDateTo = date;
        }
    }

    public workgroupChange() {
        if (!this.selectedWorkgroup) {
            return;
        }
    }

    public loadData(query: BatchSummaryQuery) {
        if (query == null) {
            return;
        }
        this.depositDateFrom = query.dateFrom;
        this.depositDateTo = query.dateTo;
        this.selectedWorkgroup = new Workgroup(query.workgroup);
        this.selectedPaymentSourceId = query.paymentSourceId;
        this.selectedPaymentTypeId = query.paymentTypeId;
        this.searchValue = query.searchValue;
        this.hasSharedStorageValue = true;
        this.sharedStorageService.DeleteData(Pages.BatchSummary);
    }

    public validate(): boolean {
        if (!this.selectedWorkgroup || this.selectedWorkgroup.BankId === 0 || this.selectedWorkgroup.WorkgroupId == 0) {
            this.toasterService.pop('error', 'Error', 'Please select a workgroup first.');
            return false;
        }
        return true;
    }

    public buildData() {
        if (this.validate()) {
            this.dataTable.refresh();
        }
    }

    // Called by the datatable when enter is pressed.
    public search(searchValue: string) {
        self.buildData();
    }

    public createdRow(row: DataTableRow) {
        if (this.loading) {
            return;
        }
        let colIndex = row.table.column('Total:name')[0][0];
        let td = row.table.cells(row.index, colIndex).nodes()[0];
        let amt = this.currencyPipe.transform(row.data.TotalAmount.toFixed(2).toString());
        $(td).html(amt);
    }

    public buildHeader(api: any, data: any) {
        if (data.length > 0) {
            this.visibleColumns = this.columns.length;
            api.column('BatchID:name').visible(data[0].ShowBatchId);
            api.column('BatchSiteCode:name').visible(data[0].ShowBathSiteCode);
            if (!data[0].ShowBatchId) {
                this.visibleColumns--;
            }
            if (!data[0].ShowBathSiteCode) {
                this.visibleColumns--;
            }
        }
    }

    public buildFooter(data: any) {
        if (self.dataTable === undefined || self.dataTable.table.data().count() === 0) {
            return [] as Array<RowGroup>;
        }

        const pipe = new ToCurrencyPipe();
        let responseData = self.dataTable.table.ajax.json();
        return [
            { columnLength: this.visibleColumns - 4, value: 'Grand Total for All Entries:', class: 'alignRight' },
            { columnLength: 1, value: responseData && responseData.transactionCount || 0, class: 'alignRight' },
            { columnLength: 1, value: responseData && responseData.checkCount || 0, class: 'alignRight' },
            { columnLength: 1, value: responseData && responseData.documentCount || 0, class: 'alignRight' },
            { columnLength: 1, value: self.currencyPipe.transform(responseData && responseData.checkTotal || 0.00), class: 'alignRight' }
        ] as Array<RowGroup>;
    }

    public errorHandler(e, settings, techNote, message) {
        self.toasterService.pop('error', 'Error', settings.jqXHR.responseJSON.error);
    }

    public setSearchValues(d) {
        if (self.selectedWorkgroup.BankId != 0 && self.selectedWorkgroup.WorkgroupId != 0) {
            d.Workgroup = self.selectedWorkgroup.BankId + '|' + self.selectedWorkgroup.WorkgroupId;
        }
        d.DateFrom = self.depositDateFrom;
        d.DateTo = self.depositDateTo;
        d.PaymentType = self.selectedPaymentType.PaymentTypeKey;
        d.PaymentSource = self.selectedPaymentSource.PaymentSourceKey;
        d.search = self.searchValue;
    }

    ngOnInit() {
        this.loadData(this.sharedStorageService.GetData(Pages.BatchSummary));
        observableForkJoin(
            this.batchSummaryService.getPaymentSources(),
            this.batchSummaryService.getPaymentTypes()
        ).subscribe(
            results => {
                if (results) {
                    this.paymentSources = results[0].data;
                    this.paymentTypes = results[1].data;
                    if (this.selectedPaymentSourceId == null) {
                        this.selectedPaymentSource = this.paymentSources[0];
                    }
                    else {
                        this.selectedPaymentSource = this.paymentSources.find(ps => ps.PaymentSourceKey == this.selectedPaymentSourceId);
                    }
                    if (this.selectedPaymentTypeId == null) {
                        this.selectedPaymentType = this.paymentTypes[0];
                    }
                    else {
                        this.selectedPaymentType = this.paymentTypes.find(ps => ps.PaymentTypeKey == this.selectedPaymentTypeId);
                    }
                    this.loading = false;
                }
            },
            error => {
                this.toasterService.pop('error', 'Error', 'An error occurred while loading payment source and/or payment type data.');
                this.loading = false;
            });
    }
}