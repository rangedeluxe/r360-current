import { Component, Input, OnInit } from '@angular/core';
import { RowGroup } from '../../DTOs/RowGroup';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { ImageService } from '../../services/image.service/image.service';
import { StubGridDTO } from '../../DTOs/StubGridDTO';
import { ToCurrencyPipe } from '../../pipes/tocurrency.pipe';

let self;

@Component({
    moduleId: module.id.toString(),
    selector: 'relatedgrid',
    templateUrl: 'relatedgrid.component.html',
    styleUrls: ['relatedgrid.component.css'],
    providers: [ImageService, ToCurrencyPipe]
})

export class RelatedGridComponent implements OnInit {

    @Input() stubDataDTO: StubGridDTO;
    @Input() loading: boolean;

    public lengthOptions: Array<number> = [10, 25, 50, 100, 1000];

    constructor(
        private currencyPipe: ToCurrencyPipe,
        private imageService: ImageService,
    ) {
        self = this;
    }

    public createdRow(row: DataTableRow) {
        const imgindex = row.table.column('image:name')[0][0];
        const imgtd = row.table.cells(row.index, imgindex).nodes()[0];
        $(imgtd).empty();

        $(imgtd).append($('<span class="iconbox" style="white-space: nowrap" >'));
        if (row.data.StubIconVisible === true) {
            $(imgtd).append($('<a class="hideOnPrint" />')
                .attr('title', 'View Image(s)')
                .append(
                    $('<i class="fa fa-lg fa-file-o" />')
                )
                .click((evt) => {
                    this.imageService.displaySingleDocument(this.stubDataDTO.bankId, this.stubDataDTO.workgroupId, this.stubDataDTO.batchId,
                        this.stubDataDTO.depositDateString, this.stubDataDTO.transactionId, row.data.DocumentBatchSequence, this.stubDataDTO.importTypeShortName);
                    return false;
                }));
        }
        if (row.data.ItemReportIconVisible === true) {
            $(imgtd).append($('<a class="hideOnPrint" />')
                .attr('title', 'View Item Report')
                .append(
                    $('<i class="fa fa-lg fa-print" />')
                )
                .click((evt) => {
                    this.imageService.displayItemReport(this.stubDataDTO.bankId, this.stubDataDTO.workgroupId, this.stubDataDTO.batchId,
                        this.stubDataDTO.depositDateString, this.stubDataDTO.transactionSequence, this.stubDataDTO.transactionId, row.data.BatchSequence, 
                        this.stubDataDTO.batchNumber, row.data.SourceProcessingDate);
                    return false;
                }));
        }
        $(imgtd).append($("</span>"));
    }

    public buildFooter(tabledata: any) {
        let pipe = new ToCurrencyPipe();
        return [
            { columnLength: 35, value: `Total: ${pipe.transform(tabledata.reduce((t, c) => t + c.PaymentAmount, 0))}`, class: 'alignRight' }
        ] as Array<RowGroup>;
    }

    ngOnInit(): void {

    }
}
