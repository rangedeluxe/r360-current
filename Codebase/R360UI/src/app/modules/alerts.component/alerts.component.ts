
import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { Globals } from '../../globals';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { AlertsService } from '../../services/alerts.service/alerts.service';
import { AlertsModal } from '../../controls/alertsmodal.component/alertsmodal.component';
import { Operation } from '../../DTOs/Operation'
import { DataTableRow } from '../../DTOs/DataTableRow';
import { PermissionService } from "../../services/permission.service/permission.service";



import * as $ from 'jquery';

@Component({
    moduleId: module.id.toString(),
    selector: 'alerts',
    templateUrl: 'alerts.component.html',
    styleUrls: ['alerts.component.css'],
    providers: [AlertsService]
})
export class AlertsComponent implements OnInit {
    @ViewChild('alertModal', {static: false}) childModal: AlertsModal;

    public columns: Array<any> = [];
    public data: Array<any> = [];
    public loading: boolean = false;
    public hideInactiveAlerts: boolean = true;
    public hasAddPermission: boolean = false;

    private alertsService: AlertsService;
    private toasterService: ToasterService;
    private permissionService: PermissionService;

    constructor(service: AlertsService, toast: ToasterService, permissionService: PermissionService) {
        this.alertsService = service;
        this.toasterService = toast;
        this.permissionService = permissionService;
    }

    ngOnInit() {
        console.log(`Alerts: ngOnInit.`);
        this.columns = [
            { data: "EventLongName", title: "Event Name" },
            { data: "Description", title: "Alert Description" },
            { data: "ParentHierarchy", title: "Entity" },
            { data: "WorkgroupName", title: "Workgroup", name: "WorkgroupName" },
            { data: "IsActive", title: "Active", name: "Active", class: "alignRight" }
        ];

        // Fire off both requests, allow them both to finish with ForkJoin.
        observableForkJoin(
            this.permissionService.hasPermission('Alerts', 'Manage'),
            this.permissionService.hasPermission('AlertFatal', 'View'),
            this.permissionService.hasPermission('AlertHighDollar', 'View'),
            this.permissionService.hasPermission('AlertPaymentFrom', 'View'),
            this.permissionService.hasPermission('AlertExtractComplete', 'View'))
            .subscribe(
                result => {
                    if (result === undefined) {
                        this.hasAddPermission = false;
                    } else {

                        let hasAlertManage = result[0];
                        if (hasAlertManage && (result[1] || result[2] || result[3] || result[4])) {
                            this.hasAddPermission = true;
                        }
                        else {
                            this.hasAddPermission = false;
                        }
                    }
                    this.refreshPage();
                },
                error => {
                    this.hasAddPermission = false;
                    this.toasterService.pop('error', 'Error', 'There was an error requesting the alerts.');
                });
    }

    refreshPage() {
        console.log("Alerts: Refreshing...");
        this.loading = true;
        this.alertsService.getAlerts(this.hideInactiveAlerts).subscribe(
            result => {
                this.data = result.Data;
                this.loading = false;
            },
            error => {
                this.toasterService.pop('error', 'Error', 'An error occurred while loading the alerts.');
                this.loading = false;
            }
        );
    }

    createdRow(data: DataTableRow) {
        let api = data.table;
        let colindex = api.column('Active:name')[0][0];
        let td = api.cells(data.element, colindex).nodes()[0];
        let row = data.element;
        $(row).prop('title', "Click here to edit this alert");
        $(td).empty();
        if (data.data.IsActive === true) {
            $(td).html('<i class="fa fa-check" style="font-size: 18px;"></i>');
        }

        colindex = api.column('WorkgroupName:name')[0][0];
        td = api.cells(data.element, colindex).nodes()[0];
        if (!data.data.WorkgroupName || data.data.WorkgroupName == "") {
            $(td).html('All');
        }

    }

    clickedRow(row: any) {
        this.childModal.currentOperation = Operation.Update;
        this.childModal.editId = row.data.EventRuleID;
        this.childModal.open();
    }

    addAlert() {
        this.childModal.currentOperation = Operation.Add;
        this.childModal.open();
    }
}