import { Component, Input, OnInit } from '@angular/core';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { ImageService } from '../../services/image.service/image.service';

import { NotificationDetailFileGridDTO } from '../../DTOs/NotificationDetailFileGirdDTO'

let self;

@Component({
    moduleId: module.id.toString(),
    selector: 'notificationdetailfilegrid',
    templateUrl: 'notificationdetailfilegrid.component.html',
    styleUrls: ['notificationdetailfilegrid.component.css'],
    providers: [ImageService]
})

export class NotificationDetailFileGridComponent implements OnInit {
    
    @Input() notificationDetailFileGridDTO: NotificationDetailFileGridDTO; 
    @Input() loading: boolean;

    public lengthOptions: Array<number> = [10, 25, 50, 100, 1000];
    
    public columns: Array<any> = [
        { data: 'UserFileName', title: "File" },
        { data: 'FileTypeDescription', title: 'File Type' },
        { data: 'Id', name: 'download', title:'Download', class: 'alignRight'}
    ];
 
    public columnOrder: Array<any> = [
        [1,'asc']
    ];

    constructor (
        private imageService: ImageService
     ) {
        self = this;
    }

    public createdRow(row: DataTableRow) {
        if (this.loading)
            return;
        let colIndex = row.table.column('download:name')[0][0];
        let downloadImage = row.table.cells(row.index, colIndex).nodes()[0];
        let showDownloadLink = row.data.FileSize > 0 && this.notificationDetailFileGridDTO.hasDownloadPermission === true;
        if( showDownloadLink) {
            $(downloadImage)
            .html("")
            .append($('<a class="hideOnPrint" />')
            .attr('title', 'View Image(s)')
            .append($('<i class="fa fa-cloud-download fa-lg download-icon"></i>'))
            .click((evt) => {
                this.imageService.downloadCENDSFile(row.data.FilePath)
            }));
        }
        else {
            $(downloadImage).html('File not available');
        }
    }

    ngOnInit(): void {
    }
}

