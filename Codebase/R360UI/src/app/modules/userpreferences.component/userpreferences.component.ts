import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Globals } from '../../globals';
import { UserPreferencesService } from '../../services/userpreferences.service/userpreferences.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { CheckboxComponent } from '../../controls/checkbox.component/checkbox.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'r360-userpreferences',
    templateUrl: 'userpreferences.component.html',
    styleUrls: ['userpreferences.component.css'],
    providers: [ UserPreferencesService ]
})
export class UserPreferencesComponent implements OnInit {
    public displayRemitterAccessible: boolean;
    public displayPayerName: boolean;
    public loading: boolean;
    
    private userPreferencesService : UserPreferencesService;
    private toasterService: ToasterService;

    constructor(
        userPreferencesService: UserPreferencesService,
        toasterService: ToasterService
        ) {
        this.userPreferencesService = userPreferencesService;
        this.toasterService = toasterService;
    }

    ngOnInit() { 
        // Called when the routes activates this page.
        this.displayRemitterAccessible = true;
        this.displayPayerName = false;
        this.loading = false;
        this.refresh();
    }

    public refresh() {
        this.loading = true;
        this.userPreferencesService.getPreferences()
            .subscribe(
                result => {
                    this.displayRemitterAccessible = result.displayRemitterAccesible;
                    this.displayPayerName = result.displayRemitterNameInPDF;
                    this.loading = false;
                },
                error => {
                    this.loading = false;
                    this.toasterService.pop('error', 'Error', 'An error occurred while loading the user preferences.');
                }
            );
    }

    public restoreDefaults() {
        this.loading = true;
        this.userPreferencesService.restoreDefaults()
            .subscribe(
                result => {
                    this.displayPayerName = result.displayRemitterNameInPDF;
                    this.loading = false;
                    this.toasterService.pop('success', 'Success!', 'User preferences have been restored to default settings.');
                },
                error => {
                    this.loading = false;
                    this.toasterService.pop('error', 'Error', 'An error occurred while restoring to default settings.');
                }
            );
    }

    public saveChanges() {
        this.loading = true;
        this.userPreferencesService.savePreferences({ displayRemitterNameInPDF: this.displayPayerName })
            .subscribe(
                result => {
                    this.loading = false;
                    this.toasterService.pop('success', 'Success!', 'User preferences have been updated.');
                },
                error => {
                    this.loading = false;
                    this.toasterService.pop('error', 'Error', 'An error occurred while saving the user preferences.');
                }
            );
    }

    public submitChangesClick() {
        this.saveChanges();
    }

    public restoreDefaultsClick() {
        this.restoreDefaults();
    }
}