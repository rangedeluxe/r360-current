import { Component, OnInit, ViewChild, Optional } from '@angular/core';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { Workgroup } from '../../DTOs/Workgroup';
import { WorkgroupMaintenanceService } from '../../services/workgroupmaintenance.service/workgroupmaintenance.service';
import { WorkgroupSetting } from '../../DTOs/WorkgroupSetting';
import { PermissionService } from '../../services/permission.service/permission.service';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { UnifyDatatableComponent } from '../../controls/unifydatatable.component/unifydatatable.component';

declare var DataTable: any;

let self;

@Component({
    moduleId: module.id.toString(),
    selector: 'workgroupmaintenance',
    templateUrl: './workgroupmaintenance.component.html',
    styleUrls: ['./workgroupmaintenance.component.css'],
    providers: []
})


export class WorkgroupMaintenanceComponent implements OnInit {

    @ViewChild(UnifyDatatableComponent, { static: false }) dataTable: UnifyDatatableComponent;
    selectedWorkgroup: Workgroup;
    loading: boolean = true;

    workgorupMaintenanceTitle: string = "Workgroup Maintenance";
    paymentImageDisplayMode: string = "display mode";
    documentImageDisplayMode: string = "display mode 2";
    workgroupSettingsData: WorkgroupSetting[] = null;
    displayBatchId: string = "Yes";
    viewingDays: string = "365";
    maxSearchDays: string = "20";
    showWorkgroup: boolean = false;
    showEntity: boolean = false;
    showBilling: boolean = false;
    isValidWorkgroup: boolean = false;
    hasManagePermission: boolean = false;
    permissionsLoading: boolean = true;

    lengthOptions: Array<number> = [10, 25, 50, 100, 1000];
    columns: Array<any>;


    constructor(
        private workgroupMaintenanceService: WorkgroupMaintenanceService,
        private toasterService: ToasterService,
        @Optional() private permissionService: PermissionService) {

        if (permissionService != null) {
            this.permissionService.hasPermission('PayerMaintenance', 'Manage').subscribe(
                result => {
                    this.hasManagePermission = result;
                    this.permissionsLoading = false;
                    this.setGridDisplayByPermission();
                });
        }
        self = this;
    }

    ngOnInit(): void {
    }

    setGridDisplayByPermission() {
        this.columns = [
            { data: 'workgroupId', visible: false, name: "workgroupId", title: '', sortable: false },
            { data: 'longName', visible: true, name: "longName", title: 'Long Name', sortable: true },
            { data: 'bankName', visible: true, name: 'bankName', title: 'Bank' },
            { data: 'workgroupId', visible: true, name: "workgroupId", title: 'Workgroup ID', sortable: true },
            { data: 'isActive', name: "isActive", title: 'Active', sortable: false },
            { data: 'edit', visible: this.hasManagePermission, name: "edit", title: '', sortable: false }
        ];
    }

    createdRow(data: DataTableRow) {
        if (this.loading) {
            return;
        }

        let api = data.table;
        let colindex = api.column('edit:name')[0][0];
        let td = api.cells(data.element, colindex).nodes()[0];
        $(td).html('<i class="fa fa-pencil"  style="font-size:14px;cursor:pointer;color:#3174d8"></i>');

        colindex = api.column('isActive:name')[0][0];
        td = api.cells(data.element, colindex).nodes()[0];
        if ($(td).text() === "true")
            $(td).html('<i class="fas fa-check-circle"  style="font-size:14px;color:green"></i>');
        else
            $(td).html('<i class="fas fa-check-circle"  style="font-size:14px;color:gray"></i>');

    }

    public selectedEntityChange() {
        this.loading = false;
        if (!this.selectedWorkgroup)
            return;
        this.workgorupMaintenanceTitle = "Workgroup Maintenance - " + this.selectedWorkgroup.Label;
        if (this.selectedWorkgroup.EntityTypeCode !== "Corp" &&
            this.selectedWorkgroup.EntityTypeCode !== "HC" &&
            this.selectedWorkgroup.EntityTypeCode !== "LOB") {
            this.isValidWorkgroup = false;
        }
        else {
            this.isValidWorkgroup = true;
            this.loading = true;
            this.reload();
        }
    }

    private getImageDisplayMode(mode) {
        switch (mode) {
            case 0: return "Use System Setting";
            case 1: return "Front and Back";
            case 2: return "Front only";
            default: return "";
        }
    }

    getDefaultSettings() {

        console.log("Get default settings for entity id = " + this.selectedWorkgroup.EntityId + "  " + this.selectedWorkgroup.Label)
        this.workgroupMaintenanceService.getEntityDefaults(this.selectedWorkgroup.EntityId)
            .subscribe(
                results => {
                    if (results) {
                        this.paymentImageDisplayMode = this.getImageDisplayMode(results.paymentImageDisplayMode);
                        this.documentImageDisplayMode = this.getImageDisplayMode(results.documentImageDisplayMode);
                        this.displayBatchId = results.displayBatchID ? "Yes" : "No";
                        this.viewingDays = results.viewingDays;
                        this.maxSearchDays = results.maximumSearchDays;
                    }
                    else {
                        this.toasterService.pop('error', 'Error', 'An error occurred while loading default settings.');
                    }
                    this.loading = false;
                },
                errors => {
                    this.toasterService.pop('error', 'Error', 'An error occurred while loading default settings.');
                    this.loading = false;
                }
            );
    }


    getWorkgroupSettings() {
        this.workgroupMaintenanceService.getWorkgroupSettings(this.selectedWorkgroup.EntityId)
            .subscribe(
                results => {
                    if (results) {
                        const options = { edit: "" }
                        results = results.reduce((acc, val) => acc.concat(val), [])
                        results = results.map(p => Object.assign(p, options));
                        this.workgroupSettingsData = results;
                    }
                    else {
                        this.toasterService.pop('error', 'Error', 'An error occurred while loading workgroup settings.');
                    }
                    this.loading = false;
                },
                errors => {
                    this.toasterService.pop('error', 'Error', 'An error occurred while loading workgroup settings.');
                    this.loading = false;
                }

            );

    }

    private reload() {
        this.workgroupSettingsData = null;
        this.getDefaultSettings();
        this.getWorkgroupSettings();
    }

    public workgroupList(showWorkgroup) {
        if (!showWorkgroup) {
            this.showEntity = false;
            this.showBilling = false;
            this.showWorkgroup = true;
        }
        else {
            this.showWorkgroup = false;
            return;
        }
    }

    public entityList(showEntity) {
        if (!showEntity) {
            this.showEntity = true;
            this.showBilling = false;
            this.showWorkgroup = false;
        }
        else {
            this.showEntity = false;
            return;
        }
    }

    public billinginfo(showbILLING) {
        if (!showbILLING) {
            this.showEntity = false;
            this.showBilling = true;
            this.showWorkgroup = false;
        } else {
            this.showBilling = false;
            return;
        }
    }

}
