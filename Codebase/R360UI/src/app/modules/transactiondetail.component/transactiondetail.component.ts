
import { forkJoin as observableForkJoin, Observable } from 'rxjs';

import { Component, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { BreadcrumbService } from '../../services/breadcrumb.service/breadcrumb.service';
import { DocumentGridDTO } from '../../DTOs/DocumentGridDTO';
import { LocalStorageService } from '../../services/localstorage.service/localstorage.service';
import { ImageService } from '../../services/image.service/image.service';
import { PaymentGridDTO } from '../../DTOs/PaymentGridDTO';
import { StubGridDTO } from '../../DTOs/StubGridDTO';
import { SharedStorageService } from '../../services/sharedstorage.service/sharedstorage.service';
import { ToasterService } from 'angular2-toaster';
import { TransactionDetailDTO } from '../../DTOs/TransactionDetailDTO';
import { TransactionDetailQuery } from '../../DTOs/TransactionDetailQuery';
import { TransactionDetailService } from '../../services/TransactionDetail.service/TransactionDetail.service';
import { ToCurrencyPipe } from '../../pipes/tocurrency.pipe';
import { Pages } from '../../DTOs/Pages';
let self;

@Component({
    moduleId: module.id.toString(),
    selector: 'transactiondetail',
    templateUrl: 'transactiondetail.component.html',
    styleUrls: ['transactiondetail.component.css'],
    providers: [
        BreadcrumbService,
        ImageService,
        SharedStorageService,
        TransactionDetailService,
        ToCurrencyPipe]
})

export class TransactionDetailComponent implements OnInit {



    public documentDataDTO: DocumentGridDTO;
    public paymentDataDTO: PaymentGridDTO;
    public stubDataDTO: StubGridDTO;
    public transactionData: any;

    public loading: boolean = true;

    public hasSharedStorageValue: Boolean = false;
    public selectedBankId: number = 9999;
    public selectedBatchId: number = 28391;
    public selectedDepositDate: string = "11/30/2017";
    public selectedTransactionId: number = 1;
    public selectedTransactionSequence: number = 1;
    public selectedWorkgroupId: number = 303;

    // common preferences used in the ui
    public showBankIdOnline: boolean;
    public displayBatchId: boolean;
    public showLockboxSiteCodeOnline: boolean;
    public showBatchSiteCodeOnline: boolean;
    public displayBatchCueIdOnline: boolean;
    public viewAllImagesIconVisible: boolean;
    public imageRPSAuditReportVisible: boolean;
    private importTypeShortName: string;
    public ICONSIZE: number = 15;

    constructor(
        private breadcrumbService: BreadcrumbService,
        private imageService: ImageService,
        private localStorageService: LocalStorageService,
        private sharedStorageService: SharedStorageService,
        private toasterService: ToasterService,
        private transactionDetailService: TransactionDetailService,
        private currencyPipe: ToCurrencyPipe
    ) {
        self = this;
    }

    public loadQuery() {
        const query = this.sharedStorageService.GetData(Pages.TransactionDetail) as TransactionDetailQuery;
        if (query) {
            this.selectedBankId = query.bankId;
            this.selectedBatchId = query.batchId;
            this.selectedDepositDate = query.depositDate;
            this.selectedTransactionId = query.transactionId;
            this.selectedTransactionSequence = query.transactionSequence;
            this.selectedWorkgroupId = query.workgroupId;
            this.hasSharedStorageValue = true;

        }
    }

    public goToTransaction() {
        let first = this.transactionData.BatchTransactions[0].Item2;
        let last = this.transactionData.BatchTransactions[this.transactionData.BatchTransactions.length - 1].Item2;
        let selected = this.selectedTransactionSequence;
        //we set transactionid = -1 to indicate we want to navigate by transaction sequence instead of id
        this.selectedTransactionId = -1;
        // a little validation first.        
        if (selected < first || selected > last) {
            let message = first == last
                ? 'There is only one transaction in this batch.'
                : `Please select an transaction between ${first} and ${last}.`;
            this.toasterService.pop('warning', 'Error', message);
            return;
        }
        const foundtran = this.transactionData.BatchTransactions.find(tran => {
            return tran.Item2 === selected;
        });
        if (!foundtran) {
            this.toasterService.pop('warning', 'Error', `There is no transaction ${selected}.`);
            return;
        }
        // reload the new transaction.
        this.reload();
    }

    public reload() {
        this.loading = true;
        this.transactionDetailService.getTransactionDetails(new TransactionDetailDTO(
            this.selectedBankId,
            this.selectedWorkgroupId,
            this.selectedBatchId,
            this.selectedDepositDate,
            this.selectedTransactionId,
            this.selectedTransactionSequence))
            .subscribe(
                results => {
                    if (results && results.success) {
                        this.documentDataDTO = new DocumentGridDTO(
                            this.selectedBankId,
                            this.selectedWorkgroupId,
                            this.selectedBatchId,
                            this.selectedDepositDate,
                            results.data.TransactionId,
                            results.data.Documents,
                            results.data.ImportTypeShortName);
                        this.paymentDataDTO = new PaymentGridDTO(
                            this.selectedBankId,
                            this.selectedWorkgroupId,
                            this.selectedBatchId,
                            this.selectedDepositDate,
                            results.data.BatchNumber,
                            results.data.TransactionId,
                            this.selectedTransactionSequence,
                            results.data.Payments);
                        this.transactionData = results.data;
                        this.showBankIdOnline = this.transactionData.Preferences!.find((p) => p.Name == 'ShowBankIDOnline')!.Value == 'Y';
                        this.displayBatchId = this.transactionData.DisplayBatchID;
                        this.showLockboxSiteCodeOnline = this.transactionData.Preferences.find
                            ((p) => p.Name == 'ShowLockboxSiteCodeOnline').Value == 'Y';
                        this.showBatchSiteCodeOnline = this.transactionData.Preferences.find(
                            (p) => p.Name == 'ShowBatchSiteCodeOnline').Value == 'Y';
                        this.displayBatchCueIdOnline = this.transactionData.Preferences.find(
                            (p) => p.Name == 'DisplayBatchCueIDOnline').Value == 'Y';
                        this.viewAllImagesIconVisible = this.transactionData.ViewAllImagesIconVisible;
                        this.imageRPSAuditReportVisible = this.transactionData.HasImageRPSReportPermissions;

                        // todo - currently the visibility flag is coming from this property, but it probably
                        // should be coming from each item (like the payment grid is reading). This needs
                        // to be updated on the app tier.
                        for (let p of this.transactionData.Payments) {
                            p.ItemReportIconVisible = this.imageRPSAuditReportVisible;
                        }

                        let displayseq = this.transactionData.Preferences
                            .find((p) => p.Name == 'ShowTransactionStubSeqOnline')
                            .Value == 'Y';

                        // Related items have dynamic columns. We need to build them here.
                        let cols: Array<any> = displayseq
                            ? [{ data: 'StubSequence', width: 35 }]
                            : [];
                        let data: Array<any> = [];

                        // Would make a lot of sense to pull the columns from the workgroup columns in the DB,
                        // but they are a part of the stub result set for now.
                        for (let stub of this.transactionData.Stubs) {
                            let deObj = {
                                StubSequence: stub.StubSequence,
                                StubIconVisible: stub.StubIconVisible,
                                ItemReportIconVisible: stub.ItemReportIconVisible,
                                BatchSequence: stub.BatchSequence,
                                DocumentBatchSequence: stub.DocumentBatchSequence,
                                SourceProcessingDate: stub.SourceProcessingDate
                            };

                            if (stub.DataEntryFields) {
                                for (let de of stub.DataEntryFields) {
                                    let prevcol = cols.find((value) => value.data === de.Title);
                                    if (!prevcol) {
                                        const width = this.getTextWidth(de.Title, '12pt Verdana', this.ICONSIZE);
                                        let col: any = { data: de.Title, title: de.Title.trim(), width: width };
                                        // If type is either number or currency, right-justify.
                                        if (de.Type === '7' || de.Type === '6') {
                                            col.class = 'alignRight';
                                        }
                                        cols.push(col);
                                    }
                                    deObj[de.Title] = (de.Type === '7' && de.Value && de.Value.length > 0)
                                        ? this.currencyPipe.transform(de.Value)
                                        : de.Value;
                                }
                            }
                            data.push(deObj);
                        }

                        // First column is a placeholder for icons.
                        cols.unshift({ name: 'image', data: 'StubSequence', sortable: false, width: 35 });

                        // Sort and order disabled if we only have 1 column (icons).
                        let sort = cols.length > 1;
                        let order = sort ? [[1, 'asc']] : [];
                        this.stubDataDTO = new StubGridDTO(this.selectedBankId,
                            this.selectedWorkgroupId,
                            this.selectedBatchId,
                            this.selectedDepositDate,
                            this.transactionData.BatchNumber,
                            this.transactionData.TransactionId,
                            this.selectedTransactionSequence,
                            data, cols, sort, order, results.data.ImportTypeShortName);
                    }
                    else {
                        console.log(results[0].error);
                        this.toasterService.pop('error', 'Error', 'An error occurred while loading transaction details data.');
                    }
                    this.loading = false;
                },
                errors => {
                    this.toasterService.pop('error', 'Error', 'An error occurred while loading transaction details data.');
                    this.loading = false;
                }
            );
    }

    public viewAllImages() {
        this.imageService.displayTransactionImages(
            this.transactionData.BankID,
            this.transactionData.WorkgroupID,
            this.transactionData.BatchID,
            this.transactionData.DepositDateString,
            this.transactionData.TransactionId);
    }

    ngOnInit(): void {
        this.loadQuery();
        this.reload();
    }


    /**
     * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
     * 
     * @param {String} text The text to be rendered.
     * @param {String} font The css font descriptor that text is to be rendered with (e.g. "bold 14px verdana").
     * 
     * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
     */
    private getTextWidth(text, font, offset) {
        // re-use canvas object for better performance
        var canvas = document.createElement("canvas");
        var context = canvas.getContext("2d");
        context.font = font;
        var metrics = context.measureText(text);
        return metrics.width + offset;
    }
}