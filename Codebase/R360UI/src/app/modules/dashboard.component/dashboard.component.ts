import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Workgroup } from "../../DTOs/Workgroup";
import { DashboardService } from "../../services/dashboard.service/dashboard.service";
import { ToasterService } from "angular2-toaster";
import { DateService } from "../../services/date.service/date.service";
import { ToCurrencyPipe } from "../../pipes/tocurrency.pipe";
import { PermissionService } from "../../services/permission.service/permission.service";
import { DatePickerComponent } from '../../controls/datepicker.component/datepicker.component';
import { Observable, Subscription } from 'rxjs';
import { LocalStorageService } from '../../services/localstorage.service/localstorage.service';
import { DataTableGroup } from '../../DTOs/DataTableGroup';

import '../../polyfills/groupBy';
import { DashboardQuery } from '../../DTOs/DashboardQuery';
import { BreadcrumbService } from '../../services/breadcrumb.service/breadcrumb.service';
import { ToNumberPipe } from '../../pipes/tonumber.pipe';


declare var Chart;
@Component({
   moduleId: module.id.toString(),
   selector: 'r360-dashboard',
   templateUrl: './dashboard.component.html',
   styleUrls: ['./dashboard.component.css'],
   providers: [DashboardService, DateService, BreadcrumbService, 
      ToCurrencyPipe, ToNumberPipe]
})
export class DashboardComponent implements OnInit, AfterViewInit {


   // To convert long names into shorter ones for the tooltips.
   private toolTipMappings: any = [
      { PaymentType: "Correspondence Only", Name: "CORR" }
   ];
   private readonly EXPANDED_STYLE: string = 'expand';
   private readonly COLLAPSED_STYLE: string = 'collapse';
   private readonly DASHCHARTCOLLAPSED: string = "DASHBOARDCHARTCOLLAPSED";
   private readonly STORED_QUERY: string = 'dashboard-saved-query';
   public readonly MAX_CHART_GROUPS: number = 15;

   private chartMinimizer: string = this.EXPANDED_STYLE;
   private expanderLabel: string = '-';
   private hasChartPermission: boolean = false;
   private totalAmount: string = '$0.00';
   private totalCount: string = '0';
   private amountChartLegend: string;
   private chartAmountLabels: Array<string> = new Array<string>();

   private chartAmountData: Array<number> = new Array<number>();
   private legendData: Array<any> = new Array<any>();
   public chartData: Array<any> = new Array<any>();
   private serverData: Array<any> = new Array<any>();

   private chartTransactionLabels: Array<string> = new Array<string>();
   private chartTransactionData: Array<number> = new Array<number>();
   private chartOptionsCurrency: any;
   private chartOptionsTotals: any;

   private chartColors: Array<any> = [{
      backgroundColor: [
         '#373D42', '#F47920', '#4B7229', '#005860', '#a54e4e',
         '#001228', '#FFDC5D', '#C6D486', '#A7B6BF', '#7BC0D2',
         '#227BA6', '#ff9b53', '#ffb37b', '#63ab24', '#8bd04f'
      ]
   }];

   private dashboardservice: DashboardService;
   private toasterService: ToasterService;
   private dateService: DateService;
   private currencyPipe: ToCurrencyPipe;
   private numberPipe: ToNumberPipe;
   private permissionService: PermissionService;
   private localStorageService: LocalStorageService;
   private receivablesData: Array<any>;
   private breadcrumbService: BreadcrumbService;
   private renderbreadCrumbs: Boolean;
   public selectedWorkgroup: Workgroup;
   public groupingOptions: Array<DataTableGroup> = [
      { name: 'DDA', columnIndex: 3, dataSource: 'DDA' },
      { name: 'Entity', columnIndex: 6, dataSource: 'Organization' },
      { name: 'Payment Source', columnIndex: 1, dataSource: 'PaymentSource' },
      { name: 'Payment Type', columnIndex: 2, dataSource: 'PaymentType' },
      { name: 'Workgroup', columnIndex: 0, dataSource: 'Account' },
   ];
   public selectedGrouping: DataTableGroup = this.groupingOptions[3];
   public depositDate: string;
   public loading: boolean = true;
   public permissionsLoading: boolean = true;
   


   constructor(
      dashboardservice: DashboardService,
      toasterService: ToasterService,
      dateService: DateService,
      currencyPipe: ToCurrencyPipe,
      permissionService: PermissionService,
      localStorageService: LocalStorageService,
      breadcrumbService: BreadcrumbService,
      numberPipe: ToNumberPipe
   ) {
      this.dashboardservice = dashboardservice;
      this.toasterService = toasterService;
      this.dateService = dateService;
      this.currencyPipe = currencyPipe;
      this.permissionService = permissionService;
      this.localStorageService = localStorageService;
      this.breadcrumbService = breadcrumbService;
      this.numberPipe = numberPipe;
      this.chartOptionsCurrency = {
         tooltips: {
            callbacks: {
               label: (item, data) => {
                  let map = this.toolTipMappings.find(e => e.PaymentType === this.chartAmountLabels[item.index]);
                  let label = map ? map.Name : this.chartAmountLabels[item.index];
                  let slice = this.chartData[item.index];
                  label = this.trimChartLabel(label);
                  
                  return this.buildChartLabel(label, currencyPipe.transform(slice.ItemTotal), slice.ItemTotalPercent);
               }
            }
         }
      };
      this.chartOptionsTotals = {
         tooltips: {
            callbacks: {
               label: (item, data) => {
                  let map = this.toolTipMappings.find(e => e.PaymentType === this.chartTransactionLabels[item.index]);
                  let label = map ? map.Name : this.chartTransactionLabels[item.index];
                  let slice = this.chartData[item.index];
                  label = this.trimChartLabel(label);
                  return this.buildChartLabel(label, numberPipe.transform(slice.ItemCount), slice.ItemCountPercent);
               }
            }
         }
      };
      this.loadQuery();
   }

   ngAfterViewInit() {
      this.loadDashboardState();
      const breaddata = {
         groupBy: this.selectedGrouping,
         DateTo: this.depositDate,
         Workgroup: this.selectedWorkgroup,
      };
      this.breadcrumbService.push('Dashboard', '/UI/Dashboard', breaddata);
   }

   public saveQuery() {
      let query = {
         depositDate: this.depositDate,
         grouping: this.selectedGrouping,
         workgroup: this.selectedWorkgroup
      } as DashboardQuery;

      this.localStorageService.write(this.STORED_QUERY, query);
   }

   public loadQuery() {
      let query = this.localStorageService.get(this.STORED_QUERY) as DashboardQuery;
      if (!query) {
         return;
      }

      this.localStorageService.delete(this.STORED_QUERY);
      this.selectedGrouping = this.groupingOptions.find((e) => e.name === query.grouping.name);
      this.depositDate = query.depositDate;
      this.selectedWorkgroup = new Workgroup(query.workgroup);
   }

   public buildChartLabel(label: string, total: string, percent: string): string {
      return ` ${label != '' ? label : 'None'}: ${total} (${percent}%)`;
   }

   // utility function for trimming long labels on the charts.
   public trimChartLabel(label: string): string {
      return label && label.length > 11
         ? `${label.substr(0, 4)}...${label.substr(label.length - 4)}`
         : label;
   }

   public rebuildData() {
      this.setGroupData();
      this.setLegendData();
      this.setChartData();
   }

   public setGroupData() {
      // Assumes 'selectedGrouping' is set, and we modify our data based on that.
      let groupings = this.serverData.groupBy(this.selectedGrouping.dataSource);
      // data is in format { 'ACH': [], 'CHECK': [] };
      let groupeddata = [];
      for (const label in groupings) {
         groupeddata.push({
            PaymentType: label,
            ItemCount: groupings[label].reduce((t, c) => t + c.PaymentCount, 0),
            ItemTotal: groupings[label].reduce((t, c) => t + c.Total, 0),
            Enabled: true
         });
      }
      this.legendData = groupeddata;
   }

   public setChartData() {
      // set the chart data based on filters.
      let amountdata = this.legendData.map(d => d.Enabled ? d.ItemTotal : 0);
      let countdata = this.legendData.map(d => d.Enabled ? d.ItemCount : 0);

      // calculate percentages
      let totalAmount = amountdata.reduce((t, c) => t + c, 0);
      let totalCount = countdata.reduce((t, c) => t + c, 0);
      this.chartData = this.legendData.map(d => {
         return {
            ItemTotalPercent: totalAmount == 0
               ? (0).toFixed(2)
               : ((d.Enabled ? d.ItemTotal : 0) / totalAmount * 100).toFixed(2),
            ItemCountPercent: totalCount == 0
               ? (0).toFixed(2)
               : ((d.Enabled ? d.ItemCount : 0) / totalCount * 100).toFixed(2),
            PaymentType: d.PaymentType,
            ItemCount: (d.Enabled ? d.ItemCount : 0),
            ItemTotal: (d.Enabled ? d.ItemTotal : 0),
            Enabled: d.Enabled
         };
      });

      // Check for percent rounding issues.
      let totalAmountPercent = this.chartData.reduce((t, c) => t + parseFloat(c.ItemTotalPercent), 0);
      let totalCountPercent = this.chartData.reduce((t, c) => t + parseFloat(c.ItemCountPercent), 0);

      // Special note - 'sort' modifies the array in-place and returns it. So that's why we're using concat, to make a new array.
      if (totalAmountPercent !== 100 && this.chartData.length > 0) {
         let item = this.chartData.concat().sort((a, b) => parseFloat(b.ItemTotalPercent) - parseFloat(a.ItemTotalPercent))[0];
         item.ItemTotalPercent = (parseFloat(item.ItemTotalPercent) + 100 - totalAmountPercent).toFixed(2);
      }
      if (totalCountPercent !== 100 && this.chartData.length > 0) {
         let item = this.chartData.concat().sort((a, b) => parseFloat(b.ItemCountPercent) - parseFloat(a.ItemCountPercent))[0];
         item.ItemCountPercent = (parseFloat(item.ItemCountPercent) + 100 - totalCountPercent).toFixed(2);
      }

      // set our binding data to redraw the charts.
      this.totalAmount = this.currencyPipe.transform(amountdata.reduce((total, num) => total + num, 0).toString());
      this.totalCount = countdata.reduce((total, num) => total + num, 0).toString();
      this.chartAmountLabels = this.chartData.map(d => d.PaymentType);
      this.chartAmountData = this.chartData.map(d => d.ItemTotal);
      this.chartTransactionLabels = this.chartData.map(d => d.PaymentType);
      this.chartTransactionData = this.chartData.map(d => d.ItemCount);
   }

   public setLegendData() {
      // Set the legend data without worring about filters.
      let totalLegendAmount = this.legendData.map(d => d.ItemTotal).reduce((t, c) => t + c, 0);
      let totalLegendCount = this.legendData.map(d => d.ItemCount).reduce((t, c) => t + c, 0);
      this.legendData = this.legendData.map(d => {
         return {
            ItemTotalPercent: totalLegendAmount == 0
               ? (0).toFixed(2)
               : (d.ItemTotal / totalLegendAmount * 100).toFixed(2),
            ItemCountPercent: totalLegendCount == 0
               ? (0).toFixed(2)
               : (d.ItemCount / totalLegendCount * 100).toFixed(2),
            PaymentType: d.PaymentType,
            ItemCount: d.ItemCount,
            ItemTotal: d.ItemTotal,
            Enabled: true
         };
      });
      let totalAmountPercent = this.legendData.reduce((t, c) => t + parseFloat(c.ItemTotalPercent), 0);
      let totalCountPercent = this.legendData.reduce((t, c) => t + parseFloat(c.ItemCountPercent), 0);
      if (totalAmountPercent !== 100 && this.legendData.length > 0) {
         let item = this.legendData.concat().sort((a, b) => parseFloat(b.ItemTotalPercent) - parseFloat(a.ItemTotalPercent))[0];
         item.ItemTotalPercent = (parseFloat(item.ItemTotalPercent) + 100 - totalAmountPercent).toFixed(2);
      }
      if (totalCountPercent !== 100 && this.legendData.length > 0) {
         let item = this.legendData.concat().sort((a, b) => parseFloat(b.ItemCountPercent) - parseFloat(a.ItemCountPercent))[0];
         item.ItemCountPercent = (parseFloat(item.ItemCountPercent) + 100 - totalCountPercent).toFixed(2);
      }
   }

   public toggleItem(item: any) {
      console.log(`Dashboard: Toggling '${item.PaymentType}'.`);
      item.Enabled = !item.Enabled;
      this.setChartData();
   }

   ngOnInit() {
      this.permissionsLoading = true;
      this.permissionService.hasPermission('PaymentTypePieChart', 'View')
         .subscribe(
            result => {
               this.hasChartPermission = result;
               this.permissionsLoading = false;
               this.refresh();
            },
            error => {
               this.permissionsLoading = false;
               this.hasChartPermission = false;
               this.toasterService.pop('error', 'Error', 'There was an error requesting the dashboard data.');
            });
      this.breadcrumbService.pop('Dashboard');
   }

   public workgroupChange() {
      if (!this.selectedWorkgroup)
         return;
      this.refresh();
   }

   public dateChange(date: string) {
      if (date) {
         this.depositDate = date;
      }
      this.refresh();
   }

   public refresh() {
      if (this.permissionsLoading) {
         console.log('Dashboard: Supressing refresh, we do not have permissions loaded.');
         return;
      }
      if (!this.selectedWorkgroup) {
         console.log('Dashboard: Supressing refresh, we do not have an entity or workgroup selected.');
         return;
      }
      this.loading = true;
      this.dashboardservice.getRecivablesSummary(this.depositDate,
         this.selectedWorkgroup.BankId, this.selectedWorkgroup.WorkgroupId,
         this.selectedWorkgroup.IsNode ? null : this.selectedWorkgroup.EntityId)
         .subscribe(
            result => {
               if (result.data) {
                  this.receivablesData = result.data;
                  this.serverData = result.data;
                  this.rebuildData();
               }
               this.loading = false;
            },
            error => {
               this.toasterService.pop('error', 'Error', 'An error occurred while loading dashboard summary data.');
               this.loading = false;
            });
   }

   public toggleChartClass(): void {
      if (this.chartMinimizer == this.EXPANDED_STYLE) {
         this.chartMinimizer = this.COLLAPSED_STYLE;
         this.expanderLabel = '+';
         this.localStorageService.write(this.DASHCHARTCOLLAPSED, true)
      }
      else {
         this.chartMinimizer = this.EXPANDED_STYLE;
         this.expanderLabel = '-';
         this.localStorageService.write(this.DASHCHARTCOLLAPSED, false);
      }

   }

   private loadDashboardState() {
      let state = this.localStorageService.get(this.DASHCHARTCOLLAPSED);
      if (state) {
         this.chartMinimizer = this.COLLAPSED_STYLE;
         this.expanderLabel = '+';
      }
      else {
         this.chartMinimizer = this.EXPANDED_STYLE;
         this.expanderLabel = '-';
      }
   }
}