import { Component, Input, OnInit, SecurityContext } from '@angular/core';
import { RowGroup } from '../../DTOs/RowGroup';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { ImageService } from '../../services/image.service/image.service';
import { PaymentGridDTO } from '../../DTOs/PaymentGridDTO';
import { ToCurrencyPipe } from '../../pipes/tocurrency.pipe';
import { dlxSanitizerService } from '../../services/dlxSanitizer.service/dlxSanitizer.service';

let self;

@Component({
    moduleId: module.id.toString(),
    selector: 'paymentgrid',
    templateUrl: 'paymentgrid.component.html',
    styleUrls: ['paymentgrid.component.css'],
    providers: [ImageService, ToCurrencyPipe, dlxSanitizerService]
})

export class PaymentGridComponent implements OnInit {
    
    @Input() paymentDataDTO: PaymentGridDTO;
    @Input() loading: boolean;

    public lengthOptions: Array<number> = [10, 25, 50, 100, 1000];
    
    public columns: Array<any> = [
        { data: 'PaymentSequence', name: 'Image', orderable: false, width: "6%" },
        { data: 'PaymentSequence', name: 'DataEntry', title: 'Payment Sequence' },
        { data: 'RT', title: 'R/T' },
        { data: 'AccountNumber', title: 'Account Number' },
        { data: 'CheckTraceRefNumber', title: 'Check/Trace/Ref Number'},
        { data: 'Payer', title: 'Payer'},
        { data: 'DDA', title: 'DDA'},
        { data: 'PaymentAmount', name: 'Total', title: 'Payment Amount', class: 'alignRight'}
    ];

    public columnOrder: Array<any> = [
        [1,'asc']
    ];

    constructor ( 
        private currencyPipe: ToCurrencyPipe,
        private imageService: ImageService,
        private sanitizerService: dlxSanitizerService
    ) {
        self = this;
    }

    private addDataEntry(row: DataTableRow){
        if (!row.data.DataEntryFields)
            return;

        let colIndex = row.table.column('DataEntry:name')[0][0];
        let td = row.table.cells(row.index, colIndex).nodes()[0];
        var html = "</br><br><span class ='defields'>Data Entry Fields</span><ul style='min-width:200px;'>";
        $.each(row.data.DataEntryFields, function (i, e) {
            var deVal = e.Type === "7" ? self.currencyPipe.transform(e.Value) : e.Value;
            html += "<li><small>" + self.sanitizerService.sanitizeHTML(e.Title) + ": " + self.sanitizerService.sanitizeHTML(deVal) + "</small></li>";
        });
        html += "</ul>";
        $(td).append(html);
    }

    public createdRow(row: DataTableRow) {
        if (this.loading)
            return;
        let colIndex = row.table.column('Total:name')[0][0];
        let td = row.table.cells(row.index, colIndex).nodes()[0];
        let amt = this.currencyPipe.transform(row.data.PaymentAmount.toFixed(2).toString());
        $(td).html(this.sanitizerService.sanitizeHTML(amt));

        const imgindex = row.table.column('Image:name')[0][0];
        const imgtd = row.table.cells(row.index, imgindex).nodes()[0];
        $(imgtd).empty();

        $(imgtd).append($('<span class="iconbox" style="white-space: nowrap" >'));
        if (row.data.CheckIconVisible === true) {
            $(imgtd).append($('<a class="hideOnPrint" />')
                .attr('title', 'View Image(s)')
                .append(
                    $('<i class="fa fa-money fa-lg" />')
                )
                .click((evt) => {
                    this.imageService.displaySinglePayment(this.paymentDataDTO.bankId, this.paymentDataDTO.workgroupId, this.paymentDataDTO.batchId,
                        this.paymentDataDTO.depositDateString, this.paymentDataDTO.transactionId, row.data.BatchSequence);
                    return false;
                }));
        }
        if (row.data.ItemReportIconVisible === true) {
            $(imgtd).append($('<a class="hideOnPrint" />')
                .attr('title', 'View Item Report')
                .append(
                    $('<i class="fa fa-lg fa-print" />')
                )
                .click((evt) => {
                    this.imageService.displayItemReport(this.paymentDataDTO.bankId, this.paymentDataDTO.workgroupId, this.paymentDataDTO.batchId,
                        this.paymentDataDTO.depositDateString, this.paymentDataDTO.transactionSequence, this.paymentDataDTO.transactionId, row.data.BatchSequence, this.paymentDataDTO.batchNumber, row.data.SourceProcessingDate);
                    return false;
                }));
        }
        $(imgtd).append($("</span>"));

        this.addDataEntry(row);
    }

    public buildFooter(tabledata: any) {
        let pipe = new ToCurrencyPipe();
        return [
            { columnLength: 35, value: `Total: ${pipe.transform(tabledata.reduce((t, c) => t + c.PaymentAmount, 0))}`, class: 'alignRight' }
        ] as Array<RowGroup>;
    }

    ngOnInit(): void {
    }
}
