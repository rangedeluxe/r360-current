// Angular directives
import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Third Party directives
import { ToasterModule } from 'angular2-toaster';
import { NgIdleModule } from '@ng-idle/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { ChartsModule } from 'ng2-charts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

// App Pages
import { AppComponent } from './app.component';
import { DashboardComponent } from './modules/dashboard.component/dashboard.component';
import { BatchSummaryComponent } from './modules/batchsummary.component/batchsummary.component';
import { BatchDetailComponent } from './modules/batchdetail.component/batchdetail.component';
import { TransactionDetailComponent } from './modules/transactiondetail.component/transactiondetail.component';
import { UserPreferencesComponent } from './modules/userpreferences.component/userpreferences.component';
import { InvoiceSearchComponent } from './modules/invoicesearch.component/invoicesearch.component';
import { AlertsComponent } from './modules/alerts.component/alerts.component';
import { LoadingPageComponent } from './modules/loadingpage.component/loadingpage.component';
import { NotificationsComponent } from './modules/notifications.component/notifications.component';
import { NotificationDetailComponent } from './modules/notificationdetail.component/notificationdetail.component';
import { WorkgroupMaintenanceComponent } from './modules/workgroupmaintenance.component/workgroupmaintenance.component';

// App controls
import { HeaderComponent } from './controls/header.component/header.component';
import { NavComponent } from './controls/nav.component/nav.component';
import { ErrorComponent } from './controls/error.component/error.component';
import { LoadingComponent } from './controls/loading.component/loading.component';
import { CheckboxComponent } from './controls/checkbox.component/checkbox.component';
import { WorkgroupSelectorComponent } from './controls/workgroupselector.component/workgroupselector.component';
import { DatatableComponent } from './controls/datatable.component/datatable.component';
import { UnifyDatatableComponent } from './controls/unifydatatable.component/unifydatatable.component';
import { AlertsModal } from './controls/alertsmodal.component/alertsmodal.component';
import { AlertExtractComplete } from './controls/alertextractcomplete.component/alertextractcomplete.component';
import { AlertFatalSystemError } from './controls/alertfatalsystemerror.component/alertfatalsystemerror.component';
import { AlertHighDollar } from './controls/alerthighdollar.component/alerthighdollar.component';
import { AlertPayer } from './controls/alertpayer.component/alertpayer.component';
import { EntityUserSelector } from './controls/entityuserselector.component/entityuserselector.component';
import { DatePickerComponent } from './controls/datepicker.component/datepicker.component';
import { ReceivablesSummary } from './controls/receivablessummary.component/receivablessummary.component';
import { RefreshComponent } from './controls/refresh.component/refresh.component';
import { EllipsisComponent } from './controls/ellipsis.component/ellipsis.component';
import { BreadcrumbComponent } from './controls/breadcrumb.component/breadcrumb.component';
import { BooleanWidgetComponent } from './controls/booleanwidget.component/booleanwidget.component';
import { NumericWidgetComponent } from './controls/numericwidget.component/numericwidget.component';
import { DocumentGridComponent } from './modules/documentgrid.component/documentgrid.component';
import { PaymentGridComponent } from './modules/paymentgrid.component/paymentgrid.component';
import { RelatedGridComponent } from './modules/relatedgrid.component/relatedgrid.component';
import { PayerMaintenanceComponent } from './modules/payermaintenance.component/payermaintenance.component';
import { ConfirmDialogComponent } from './controls/confirmdialog.component/confirmdialog.component';
import { PayerModal } from './controls/payermodal.component/payermodal.component';
import { NotificationDetailFileGridComponent } from './modules/notificationdetailfilegrid.component/notificationdetailfilegrid.component';

// Routing controls
import { RouteGuard } from './routing/routeguard';

// Global services
import { LocalStorageService } from './services/localstorage.service/localstorage.service';
import { HttpService } from './services/http.service/http.service';
import { DataService } from './services/http.data.service/data.service';
import { PermissionService } from './services/permission.service/permission.service';
import { FrameworkService } from './services/framework.service/framework.service';
import { DateService } from './services/date.service/date.service';
import { SharedStorageService } from './services/sharedstorage.service/sharedstorage.service';
import { BreadcrumbService } from './services/breadcrumb.service/breadcrumb.service';
import { AuditService } from './services/audit.service/audit.service';
import { dlxSanitizerService } from './services/dlxSanitizer.service/dlxSanitizer.service'

// Custom Pipes
import { TrimPipe } from './pipes/trim.pipe';
import { ToCurrencyPipe } from './pipes/tocurrency.pipe';
import { ToNumberPipe } from './pipes/tonumber.pipe';
import { HeaderimageComponent } from './controls/headerimage/headerimage.component';

// Interceptors
import { ErrorInterceptorProvider } from './interceptors/http.error.interceptor';
import { AuthenticationInterceptorProvider } from './interceptors/authentication.interceptor';

//Directives
import { OnlyNumbersDirective } from './directives/only-numbers.directive';
import { AppConfigServiceProvider } from './services/appconfig.service/appconfig.service';

const appRoutes: Routes = [
  { path: '', redirectTo: 'Dashboard', pathMatch: 'full' },
  { path: 'Dashboard', component: DashboardComponent, canActivate: [RouteGuard] },
  { path: 'BatchSummary', component: BatchSummaryComponent, canActivate: [RouteGuard] },
  { path: 'BatchDetail', component: BatchDetailComponent, canActivate: [RouteGuard] },
  { path: 'TransactionDetail', component: TransactionDetailComponent, canActivate: [RouteGuard] },
  { path: 'UserPreferences', component: UserPreferencesComponent, canActivate: [RouteGuard] },
  { path: 'InvoiceSearch', component: InvoiceSearchComponent, canActivate: [RouteGuard] },
  { path: 'AlertManager', component: AlertsComponent, canActivate: [RouteGuard] },
  { path: 'AccessDenied', component: DashboardComponent, canActivate: [RouteGuard] },
  { path: 'Loading', component: LoadingPageComponent },
  { path: 'PayerMaintenance', component: PayerMaintenanceComponent, canActivate: [RouteGuard] },
  { path: 'Notifications', component: NotificationsComponent, canActivate: [RouteGuard] },
  { path: 'NotificationDetail', component: NotificationDetailComponent, canActivate: [RouteGuard] },
  { path: 'WorkgroupMaintenance', component: WorkgroupMaintenanceComponent, canActivate: [RouteGuard] },
  { path: '**', redirectTo: 'Dashboard' }
];

@NgModule({
  providers: [
    RouteGuard,
    LocalStorageService,
    HttpService,
    DataService,
    PermissionService,
    TrimPipe,
    FrameworkService,
    DateService,
    BreadcrumbService,
    AuditService,
    SharedStorageService,
    Title,
    AuthenticationInterceptorProvider,
    ErrorInterceptorProvider,
    AppConfigServiceProvider
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    ToasterModule.forRoot(),
    NgIdleModule.forRoot(),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    ChartsModule,
    NgxDatatableModule
  ],
  declarations: [
    AppComponent,
    NavComponent,
    LoadingComponent,
    HeaderComponent,
    DashboardComponent,
    BatchSummaryComponent,
    BatchDetailComponent,
    TransactionDetailComponent,
    UserPreferencesComponent,
    ErrorComponent,
    CheckboxComponent,
    WorkgroupSelectorComponent,
    DatatableComponent,
    UnifyDatatableComponent,
    InvoiceSearchComponent,
    TrimPipe,
    ToCurrencyPipe,
    ToNumberPipe,
    AlertsComponent,
    AlertsModal,
    AlertExtractComplete,
    AlertFatalSystemError,
    AlertHighDollar,
    AlertPayer,
    EntityUserSelector,
    LoadingPageComponent,
    DatePickerComponent,
    ReceivablesSummary,
    RefreshComponent,
    EllipsisComponent,
    HeaderimageComponent,
    BreadcrumbComponent,
    BooleanWidgetComponent,
    NumericWidgetComponent,
    DocumentGridComponent,
    PaymentGridComponent,
    RelatedGridComponent,
    PayerMaintenanceComponent,
    PayerModal,
    ConfirmDialogComponent,
    NotificationsComponent,
    NotificationDetailComponent,
    NotificationDetailFileGridComponent,
    WorkgroupMaintenanceComponent,
    OnlyNumbersDirective
  ],
  bootstrap: [AppComponent],

})
export class AppModule {
}