
import {filter} from 'rxjs/operators';
import { Component, ViewEncapsulation, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertsService } from '../../services/alerts.service/alerts.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { Operation } from '../../DTOs/Operation';
import { Alert } from '../../DTOs/Alert';
import { Workgroup } from "../../DTOs/Workgroup";
import { FormsModule, NgForm } from '@angular/forms';
import { User } from "../../DTOs/User";
import { Router, NavigationStart } from '@angular/router';



@Component({
  moduleId: module.id.toString(),
  selector: 'alerts-modal',
  templateUrl: 'alertsmodal.component.html',
  styleUrls: ['alertsmodal.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [AlertsService]
})

export class AlertsModal {

  closeResult: string;
  private alertsService: AlertsService;
  private toastService: ToasterService;
  private modal: NgbModalRef;
  public message: string = "";
  public operation: string = "";
  public alertsData: any;
  public alertTypes: any[];
  public alertOperators: any[];
  public selectedEventName: string = "";
  public selectedObj: any = { id: "0", text: "" };
  public currentOperation: Operation;
  public currentAlert: Alert = new Alert();
  public editId: number;
  public loading: boolean = false;
  @Output() onClose: EventEmitter<any> = new EventEmitter();
  @ViewChild('content', {static: false}) childHtml: any;

  constructor(private modalService: NgbModal, service: AlertsService, toast: ToasterService, private router: Router) {
    this.alertsService = service;
    this.toastService = toast;
    router.events.pipe(
      filter(event => event instanceof NavigationStart))
      .subscribe((event: NavigationStart) => {
        this.close();
      });
  }


  open() {
    this.loading = true;
    this.operation = this.currentOperation == Operation.Add
      ? "Add"
      : "Edit";
    this.currentOperation == Operation.Add
      ? this.addAlert()
      : this.editAlert();

    this.modal = this.modalService.open(this.childHtml, { windowClass: 'modal-frame' });
  }

  private loadEvents(Events: any[]) {
    this.alertTypes = Events
      .filter(x => x.EventLongName != 'Decision Pending' && x.EventLongName != 'File Available')
      .map(
      function (item) {
        return { id: item.ID, text: item.EventLongName }
      });
  }

  private addAlert() {
    this.currentAlert = new Alert();
    this.alertsService.addAlert()
      .subscribe(
      result => {
        this.loading = false;
        if (result.Data) {
          this.alertsData = result.Data;
          this.loadEvents(result.Data.Events);
          this.selectedObj = this.alertTypes[0];
          this.loadOperators();
          this.currentAlert.EventID = this.alertTypes[0].id;
          this.selectedEventName = this.alertsData.Events[0].Name;
        }
      },
      error => {
        console.log('Error getting data to add alert');
        this.toastService.pop('error', 'Error', 'An error occurred while adding an alert.');
      });
  }

  private editAlert() {
    this.alertsService.editAlert(this.editId)
      .subscribe(
      result => {
        this.loading = false;
        if (!result.HasErrors) {
          let alert = new Alert();
          let currentAlert = result.Data;
          this.loadEvents(currentAlert.Events);
          this.alertsData = currentAlert;
          alert.EventRuleID = currentAlert.EventRule.EventRuleID;
          alert.EventID = currentAlert.EventRule.EventID;
          alert.EventName = currentAlert.EventRule.EventLongName;
          alert.IsActive = currentAlert.EventRule.IsActive;
          alert.EventValue = currentAlert.EventRule.EventValue;
          alert.Workgroup = new Workgroup();
          alert.Workgroup.BankId = currentAlert.EventRule.SiteBankID;
          alert.Workgroup.WorkgroupId = currentAlert.EventRule.SiteClientAccountID;
          alert.Description = currentAlert.EventRule.Description;
          alert.Operator = currentAlert.EventRule.Operator ?
            { id: currentAlert.EventRule.Operator, text: currentAlert.EventRule.Operator } : { id: "0", text: "" };
          alert.EventValue = currentAlert.EventRule.EventValue;
          alert.AlertUsers = currentAlert.EventRule.AssociatedUsers as User[];
          this.currentAlert = alert;
          this.selectedEventName = (this.alertsData.Events.filter(x => x.ID === alert.EventID))[0].Name;
          this.selectedObj = this.alertTypes.filter(x => x.id === alert.EventID && x.text === alert.EventName)[0];
          this.loadOperators();
        }

      },
      error => {
        console.log('Error getting data to edit alert');
        this.toastService.pop('error', 'Error', 'An error occurred while editing an alert.');
      });
  }

  eventTypeChanged() {
    console.log('changed.');
    let selectedEvent = this.alertsData.Events
      .filter(x => x.ID == this.selectedObj.id)[0];
    this.currentAlert.EventID = selectedEvent.ID;
    this.selectedEventName = selectedEvent.Name;
    this.currentAlert.Operator.id = 0;
    this.loadOperators();
  }

  private loadOperators() {
    this.alertOperators = [];
    if (this.alertsData.Events) {
      this.alertOperators = this.alertsData.Events
        .filter(x => x.ID === this.selectedObj.id)[0]
        .Operators.map((item, index) => ({ id: item.Name, text: item.Name }));
      if (this.alertOperators.length > 0 &&
        this.currentAlert.Operator.id == 0) this.currentAlert.Operator = this.alertOperators[0];
    }
  }

  close(): void {
    this.selectedEventName = null;
    this.currentAlert = new Alert();
    this.alertOperators = [];
    this.selectedObj = this.alertTypes ? this.alertTypes[0] : null;
    if (this.modal)
      this.modal.close();
    this.onClose.emit();
  }

  save(): void {
    this.loading = true;
    console.log("AlertsModal: Saving.");
    if (this.currentOperation == Operation.Add) {
      this.alertsService.insertAlert(this.currentAlert).subscribe(
        result => {
          if (result && !result.HasErrors) {
            this.loading = false;
            console.log("AlertsModal: Alert added complete");
            this.close();
            return;
          }
          this.toastService.pop('error', 'Error', 'An error occurred while adding an alert.');

        },
        error => {
          this.loading = false;
          this.toastService.pop('error', 'Error', 'An error occurred while adding an alert.');
        }
      )
    }
    else if (this.currentOperation == Operation.Update) {
      this.alertsService.updateAlert(this.currentAlert).subscribe(
        result => {
          if (result && !result.HasErrors) {
            this.loading = false;
            console.log("AlertsModal: Alert Update complete");
            this.close();
            return;
          }
          this.toastService.pop('error', 'Error', 'An error occurred while updating an alert.');

        },
        error => {
          this.loading = false;
          this.toastService.pop('error', 'Error', 'An error occurred while updating an alert.');
        }

      );
    }
  }

}