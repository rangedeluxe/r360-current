import { Component, Input, OnInit, EventEmitter, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Payer } from '../../DTOs/Payer';

@Component({
  moduleId: module.id.toString(),
  selector: 'confirm-dialog',
  templateUrl: 'confirmdialog.component.html',
  styleUrls: ['confirmdialog.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class ConfirmDialogComponent implements OnInit {

  private modal: NgbModalRef;
  @Input() header: string = '';
  @Input() body: string = '';
  @Input() confirmText: string = '';
  @Input() cancelText: string = '';
  @Output() onConfirm: EventEmitter<any> = new EventEmitter();
  @Output() onCancel: EventEmitter<any> = new EventEmitter();
  @ViewChild('confirmContent', {static: false}) childHtml: any;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    // Default message.
    this.header = this.header || "Confirm";
    this.body = this.body || "";
    this.confirmText = this.confirmText || "Ok";
    this.cancelText = this.cancelText || "Cancel";
  }

  open() {
    this.modal = this.modalService.open(this.childHtml, { windowClass: 'modal-frame' });
  }
  confirm() {
    this.onConfirm.emit(true);
    if (this.modal)
      this.modal.close();
  }

  cancel() {
    this.onCancel.emit();
    if (this.modal)
      this.modal.close();
  }

}




