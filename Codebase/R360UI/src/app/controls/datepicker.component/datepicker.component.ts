import { Component, OnInit, OnChanges } from '@angular/core';
import { Input, Output, EventEmitter, ViewChild, ElementRef, SimpleChange } from '@angular/core';
import { ToasterModule, ToasterService } from 'angular2-toaster';

import { DateService } from "../../services/date.service/date.service";
import { FormattedDate } from '../../DTOs/FormattedDate';

declare let document:any;

@Component({
    moduleId: module.id.toString(),
    selector: 'datepicker',
    templateUrl: 'datepicker.component.html',
    styleUrls: ['datepicker.component.css']
})
export class DatePickerComponent implements OnInit, OnChanges {
    @ViewChild('dateInput', {static: false}) dateInput: ElementRef;

    @Input() public text: string = '';
    @Input() public date;
    @Input() public loading: boolean;
    @Output() public dateChange = new EventEmitter(true);
    @Input() public width: number = 290;
    @Input() public htmlId;
    
    public dateValue: Date;
    public dateInputValue: string;

    private initialFire: boolean = true;
    private isBlurring: boolean = false;

    private dateService: DateService;
    private toasterService: ToasterService;
    constructor(dateService: DateService,
        toasterService: ToasterService
    ) {
        this.dateService = dateService;
        this.toasterService = toasterService;
    }

    ngOnInit () {
        console.log("DATE: " + this.date);
        this.setDateValue(this.date);
        this.fireDateChanged(false);
    }

    ngOnChanges(changes: any) {
        let load = (changes.loading) as SimpleChange;

        // If we changed from enabled to disabled and the datepicker still has focus.
        if (load
            && load.currentValue === true
            && load.firstChange === false
            && load.previousValue === false
            && document.activeElement == this.dateInput.nativeElement) {
            // Force a blur as firefox does not auto-blur when an element becomes disabled.
            console.log('DatePicker: Changes detected, losing focus of element.');
            this.isBlurring = true;
            this.dateInput.nativeElement.blur();
        }
    }

    private isNumber(num: string): boolean {
        return /^\d*$/.test(num);
    }

    private getToday(): string {
        return this.dateService.getDate()
            .toLocaleDateString()
            .replace(/[^ -~]/g,'');
    }

    // takes input from text and tries to make a date out of it.
    // returns today's date if something wasn't parseable.
    public formatDate(dateInputValue: string): FormattedDate {
        let datenum = NaN;
        let date = null;

        let dateregex = /^(\d{1,2})\/(\d{1,2})\/(\d{2})$/;

        // check for format of mmddyyyy
        if (isNaN(datenum) && this.isNumber(dateInputValue) && dateInputValue.length == 8) {
            let month = dateInputValue.slice(0, 2);
            let day = dateInputValue.slice(2, 4);
            let year = dateInputValue.slice(4, 8);
            let str = `${month}/${day}/${year}`;
            datenum = Date.parse(str);
            date = new Date(datenum);
        }
        // check for format of mmddyy
        else if (isNaN(datenum) && this.isNumber(dateInputValue) && dateInputValue.length == 6) {
            let month = dateInputValue.slice(0, 2);
            let day = dateInputValue.slice(2, 4);
            let year = dateInputValue.slice(4, 6);
            datenum = Date.parse(`${month}/${day}/20${year}`);
            date = new Date(datenum);
        }
        // check for mm/dd/yy or m/d/yy (etc) - this is a case that IE does a terrible job of parsing.
        else if (dateregex.test(dateInputValue)) {
            let matches = dateregex.exec(dateInputValue);
            let month = matches[1];
            let day = matches[2];
            let year = matches[3];
            datenum = Date.parse(`${month}/${day}/20${year}`);
            date = new Date(datenum);
        }
        // check for format of other numbers, but they shouldn't make sense if they're not of length 8 or 6.
        else if (this.isNumber(dateInputValue)) {
            datenum = NaN;
        }
        // try to parse a normal date
        else if (isNaN(datenum)) {
            datenum = Date.parse(dateInputValue);
            date = new Date(datenum);
        }

        if (isNaN(datenum) || isNaN(date.getTime())) {
            this.toasterService.pop('warning', 'Warning',
                `An invalid date was entered. The date was defaulted to ${this.getToday()}.`);
        }

        // The replaces are for IE11.
        let d = !isNaN(datenum) && !isNaN(date.getTime())
            ? date.toLocaleDateString().replace(/[^ -~]/g,'')
            : this.getToday();
        return {
             DateValue:  new Date(d),
             DateString: d
         };
    }

    public setDateValue(datevalue?: string): void{
        datevalue = datevalue || this.getToday();
        let fValue =  this.formatDate(datevalue);
        this.dateValue = fValue.DateValue;
        this.dateInputValue = fValue.DateString;
        this.date = fValue.DateString;
    }

    public updateDateValue(event: any) {
        this.dateInputValue = this.dateInput.nativeElement.value;
    }

    public fireDateChanged(update: boolean, selectedDate?: string) {
        if (this.isBlurring) {
            console.log("DatePicker: Suppressing fired event, the control is blurring.");
            this.isBlurring = false;
            return;
        }
        else if (this.loading && !this.initialFire) {
            console.log("DatePicker: Suppressing fired event, the control is loading.");
            return;
        }
        else if (this.initialFire) {
            this.initialFire = false;
        }

        if (update && selectedDate) {
            let format = this.formatDate(selectedDate);
            this.dateInputValue = format.DateString;
            this.date = format.DateString;
            this.dateValue = format.DateValue;
        }
        else if (update) {
            let format = this.formatDate(this.dateInputValue);
            this.dateValue = format.DateValue;
            this.dateInputValue = format.DateString;
            this.date = format.DateString;
        }
        console.log('Datepicker: Emitting date:' + this.date);
        this.dateChange.emit(this.date);
    }

}