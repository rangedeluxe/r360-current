import { Component, Input, EventEmitter } from '@angular/core';
import { ToCurrencyPipe } from '../../pipes/tocurrency.pipe';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { LocalStorageService } from '../../services/localstorage.service/localstorage.service';
import { RowGroup } from '../../DTOs/RowGroup';
import { DataTableGroup } from '../../DTOs/DataTableGroup';
import { NavigationService } from '../../services/navigation.service/navigation.service';
import { BatchSummaryQuery } from '../../DTOs/BatchSummaryQuery';
import { PaymentSources } from '../../DTOs/PaymentSources';
import { PaymentTypes } from '../../DTOs/PaymentTypes';
import { Workgroup } from '../../DTOs/Workgroup';
import { Router } from '@angular/router';
import { ToNumberPipe } from '../../pipes/tonumber.pipe';

@Component({
    moduleId: module.id.toString(),
    selector: 'receivablesSummary',
    templateUrl: 'receivablessummary.component.html',
    styleUrls: ['receivablessummary.component.css'],
    providers: [ToCurrencyPipe, NavigationService, ToNumberPipe],
    outputs: ["onNavigateAwayClick"]
})

export class ReceivablesSummary {
    @Input() receivablesData: Array<any>;
    @Input() selectedGrouping: DataTableGroup;
    @Input() depositDate: string;

    public onNavigateAwayClick: EventEmitter<void> = new EventEmitter<void>();

    private readonly EXPANDED_STYLE: string = 'expand';
    private readonly COLLAPSED_STYLE: string = 'collapse';
    private readonly RECSUMCOLLAPSED : string = "DASHBOARDRECSUMCOLLAPSED";
    private router: Router;
    public recSummaryMinimizer: string = this.EXPANDED_STYLE;
    public minimizerLabel: string = '-';
    private currencyPipe: ToCurrencyPipe;
    private localStorageService:  LocalStorageService;
    private navigationService: NavigationService;

    public columns: Array<any> = [{ data: "Account", title: "Workgroup" },
                                   { data: "PaymentSource", title: "Payment Source" },
                                   { data: "PaymentType", title: "Payment Type" },
                                   { data: "DDA", title: "DDA", name: "DDA", class: "ellipsis", width: '150px' },
                                   { data: "PaymentCount", title: "Payment Count", class: "alignRight" },
                                   { data: "Total", title: "Total", name:"Total", class: "alignRight" },
                                   { data: "Organization", visible: false }
                                ];
    constructor(
        currencyPipe: ToCurrencyPipe,
        localStorageService: LocalStorageService,
        navigationService: NavigationService,
        router: Router
    ) {
        this.currencyPipe = currencyPipe;
        this.localStorageService = localStorageService;
        this.navigationService = navigationService;
        this.router = router;
    }

    ngAfterViewInit(){
        this.loadRecSummaryState();
    }

    createdRow(row: DataTableRow) {
        let colindex = row.table.column('Total:name')[0][0];
        let amt = this.currencyPipe.transform(row.data.Total.toFixed(2).toString());
        row.element.querySelectorAll('td')[colindex].innerText = amt;

        // Just adding a title to the dda column for the ellipsis.
        colindex = row.table.column('DDA:name')[0][0];
        row.element.querySelectorAll('td')[colindex].setAttribute('title', row.data.DDA);
    }

    clickedRow(row: any) {
        let data = row.data;
        let batchsummarydata = {
            dateFrom: this.depositDate,
            dateTo: this.depositDate,
            paymentTypeId: data.PaymentTypeId,
            workgroup: {BankId: data.RecHubBankId, WorkgroupId: data.RecHubWorkgroupId, EntityId: data.EntityID} as Workgroup,
            paymentSourceId: data.PaymentSourceId,
            searchValue: (data.DDA && data.DDA.length > 0) ? `DDA:${data.DDA}` : 'DDA:None'
        } as BatchSummaryQuery;
        this.onNavigateAwayClick.emit();
        this.localStorageService.write('batchsummarydata', batchsummarydata);
        this.router.navigateByUrl('/BatchSummary');
    }

    buildFooter(tabledata: any) {
        let pipe = new ToCurrencyPipe();
        return [
            { columnLength: 5, value: '' },
            { columnLength: 1, value: `Total: ${pipe.transform(tabledata.reduce((t, c) => t + c.Total, 0))}`, class: 'alignRight' }
        ] as Array<RowGroup>;
    }

    buildGroup(tabledata: any, group: DataTableGroup, groupvalue: any) : Array<RowGroup> {
        // Reminder - we're not in the context of this component, as this is called from the child datatable.
        let pipe = new ToCurrencyPipe();
        let numberPipe = new ToNumberPipe();
        return [
            { columnLength: 4, value: `${group.name}: ${(groupvalue != '' ? groupvalue : 'None' )}` },
            { columnLength: 1, value: numberPipe.transform(tabledata.reduce((t, c) => c[group.dataSource] === groupvalue ? t + c.PaymentCount : t, 0)), class: 'alignRight' },
            { columnLength: 1, value: pipe.transform(tabledata.reduce((t, c) => c[group.dataSource] === groupvalue ? t + c.Total : t, 0)), class: 'alignRight' }
        ] as Array<RowGroup>;
    }

    public toggleRecSummaryClass(): void {
        if (this.recSummaryMinimizer == this.EXPANDED_STYLE) {
            this.recSummaryMinimizer =  this.COLLAPSED_STYLE;
            this.minimizerLabel = "+";
            this.localStorageService.write(this.RECSUMCOLLAPSED, true);
        }
        else{
            this.recSummaryMinimizer = this.EXPANDED_STYLE;
            this.minimizerLabel = "-";
            this.localStorageService.write(this.RECSUMCOLLAPSED, false);
        }
    }

    private loadRecSummaryState(){
        let state = this.localStorageService.get(this.RECSUMCOLLAPSED);
        if(state){
            this.recSummaryMinimizer = this.COLLAPSED_STYLE;
            this.minimizerLabel = "+" ;
        }
        else{
            this.recSummaryMinimizer = this.EXPANDED_STYLE;
            this.minimizerLabel = "-";
        }
    }
}