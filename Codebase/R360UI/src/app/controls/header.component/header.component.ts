import { Component, OnInit } from '@angular/core';
import { LoadingComponent } from '../loading.component/loading.component';
import { FrameworkService } from '../../services/framework.service/framework.service';
import { Title } from '@angular/platform-browser';

@Component({
    moduleId: module.id.toString(),
    selector: 'framework-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
    providers: [ FrameworkService, Title ]
})
export class HeaderComponent implements OnInit {
    public error: boolean = false;
    public branding: any;

    private frameworkService: FrameworkService;

    constructor(frameworkService: FrameworkService, private titleService: Title ) {
        this.frameworkService = frameworkService;
    }

    ngOnInit() { 
        this.frameworkService.getBranding()
            .subscribe(
                result => {
                    this.branding = result;
                    if (this.branding.ShowTitle)
                        this.titleService.setTitle(this.branding.Title);
                    else
                        this.titleService.setTitle("R360")
                },
                error => {
                    this.error = true;
                }
            );
    }
}