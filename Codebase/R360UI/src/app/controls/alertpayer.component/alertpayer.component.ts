import { Component, Input, OnInit } from '@angular/core';
import { Alert } from "../../DTOs/Alert";

@Component({
  moduleId: module.id.toString(),
  selector: 'alertpayer',
  templateUrl: 'alertpayer.component.html',
  styleUrls: ['alertpayer.component.css']
})

export class AlertPayer {
  @Input() currentAlert: Alert;
  @Input() public alertOperators: any[];

  operatorChanged(selVal: any) {    
    this.onBlur();
  }

  onBlur() {
    this.currentAlert.Pristine = false;
  }

  eventKeyPress(event: any){
    var key = event.keyCode || event.charCode;
    if(this.currentAlert != null && this.currentAlert.EventValue != null 
      && this.currentAlert.EventValue.length >= 80 && 
      (key != 8 &&  key != 46 && key < 37 && key > 40 )) //del, backspace and arrows are allowed
      {
        event.preventDefault();
      }
  }

  compareOperators(val1, val2):boolean{
    return val1 != null && val2 != null && val1.id == val2.id;
  }
}

