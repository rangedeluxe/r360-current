import { Component } from '@angular/core';
import { Input } from '@angular/core';

@Component({
    moduleId: module.id.toString(),
    selector: 'booleanwidget',
    templateUrl: './booleanwidget.component.html',
    styleUrls: ['./booleanwidget.component.css']
})
export class BooleanWidgetComponent {

    @Input() public title:string = "";
    @Input() public threshold:number = 0;
    @Input() public positive:string = "more";

    // data that would come from a REST call.
    @Input() public data:number = 0;

    constructor() {
        
    }

}