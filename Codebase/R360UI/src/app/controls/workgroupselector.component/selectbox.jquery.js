﻿$.widget("wfs.wfsSelectbox", {
    // Default options.
    options: {
        'displayField': 'Name',// can be set to an array of strings
        'idField': 'Id',// can be set to an array of strings
        'displayFieldSeperator': ' - ', // used when displayField is an array
        'items': null,
        'multiple': false,
        'maximumSelectionSize': 0,
        'minimumInputLength': 0,
        'width': '200px',
        'placeHolder': 'Select one...',
        'callback': function () { },
        'query': undefined,
        'minimumResultsForSearch': 0,
        'allowClear': true,
        'formatInputTooShort': function () { }
    },
    _create: function () {
        var widget = this;
        var opts = widget.options;

        // select2 must have 'id' and 'text' fields... add them if the data list doesn't using defulats or provided fields
        if (opts && opts.items) {
            widget._cleanItems(opts.items);
        }
        // init the dropdown
        var selector = this.element;

        selector.select2({
            placeholder: opts.placeHolder,
            allowClear: opts.allowClear,
            query: opts.query,
            formatResult: opts.formatResult,
            formatSelection: opts.formatSelection,
            maximumSelectionSize: opts.maximumSelectionSize,
            multiple: opts.multiple,
            minimumInputLength: opts.minimumInputLength,
            minimumResultsForSearch: opts.minimumResultsForSearch,
            data: function () { return { results: opts.items }; },
            width: opts.width,
            formatInputTooShort: opts.formatInputTooShort
        });

        if (!opts.placeHolder && opts.items && opts.items.length > 0) {
            selector.select2("val", opts.items[0].Id);
            selector.change();
        }

        // set the callback to the change event if there is one
        if (typeof opts['callback'] == 'function') {
            selector.on("change", function (e) {
                var data = $(this).select2("data");
                opts['callback'](data);
            });
        }
    },
    setItems: function (items) {
        var widget = this;
        var selector = widget.element;
        var opts = widget.options;

        if (items) {
            widget._cleanItems(items);
            opts.items = items;
        }
        // if no placeholder, set to first item if there is one
        if (!opts.placeHolder && opts.items && opts.items.length > 0) {
            selector.select2("val", opts.items[0].Id);
            selector.change();
        } else {
            selector.select2("val", null);
        }
    },
    setValue: function (itemId, triggerChange) {
        // triggerChange defaults to true, unless you explicitly pass false

        var selector = this.element;

        selector.select2("val", itemId);
        // treat undefined as true
        if (triggerChange !== false) {
            selector.change();
        }
    },
    setValueByData: function (itemId, triggerChange) {
        // triggerChange defaults to true, unless you explicitly pass false

        var selector = this.element;

        selector.select2("data", itemId);
        // treat undefined as true
        if (triggerChange !== false) {
            selector.change();
        }
    },
    getValue: function () {
        var selector = this.element;
        return selector.select2("val");
    },
    getData: function () {
        var selector = this.element;
        return selector.select2("data");
    },
    setEnabled: function (enabled) {
        var selector = this.element;
        selector.select2("enable", enabled);
    },
    setReadOnly: function (readonly) {
        var selector = this.element;
        selector.select2("readonly", readonly);
    },
    _cleanItems: function (items) {
        var widget = this;
        var opts = widget.options;

        for (var i = 0; i < items.length; i++) {
            var itemIn = items[i];
            if (opts != null) {
                itemIn['id'] = itemIn[opts.idField];
                // the displayfield could be an array which means concat the array fields specified
                itemIn['text'] = widget._getDisplayField(itemIn, opts.displayField, opts.displayFieldSeperator);
            }
        }
    },
    _getDisplayField: function (item, display, seperator) {
        var widget = this;
        var opts = widget.options;

        var rvalue = "";
        if (typeof display === 'string') {
            rvalue = item[display];
        }
        else if ($.isArray(display)) {
            for (var i = 0; i < display.length; i++) {
                var field = display[i].toString();
                if (!rvalue)
                    rvalue = item[field].toString();
                else
                    rvalue = rvalue + seperator + item[field].toString();
            }
        }
        return rvalue;
    }
});
