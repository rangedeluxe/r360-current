import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { Globals } from '../../globals';
import { ToasterModule, ToasterService } from 'angular2-toaster';
declare let $: any;
import './workgroupselector.jquery';
import './selectbox.jquery.js';
import './select2.min.js';
import { Workgroup } from "../../DTOs/Workgroup";
@Component({
    moduleId: module.id.toString(),
    selector: 'workgroupselector',
    templateUrl: 'workgroupselector.component.html',
    styleUrls: ['workgroupselector.component.css']
})
export class WorkgroupSelectorComponent implements OnInit, AfterViewInit {
    @Input() public htmlid: string = "";
    @Input() public width: number = 320;
    @Input() public height: string = "375px";
    @Input() public margin: string = "0";
    @Input() public display: string;
    @Input() public label: string;
    @Input() public userSelector: boolean;
    @Input() public allowEntitySelection: boolean = false;
    @Input() public entitiesOnly: boolean = false;
    @Input() public selectedWorkgroup: Workgroup = new Workgroup();
    @Input() public selectedUsers: Array<any>;
    @Input() public returnFullSelection: boolean = false;
    @Input() public useExpander: boolean = true;
    @Output() selectedWorkgroupChange: EventEmitter<any> = new EventEmitter<any>();
    @Output() selectedEntityChange: EventEmitter<number> = new EventEmitter<number>();
    @Output() selectedUsersChange: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

    @Output() workgroupFinishedLoading: EventEmitter<any> = new EventEmitter<any>();

    public selectedObject: any;

    private workgroupSelector: any;
    private idhash: string;
    private initialized: boolean = false;
    private initSelection: boolean = false;
    private isSelecting: boolean = false;

    private toasterService: ToasterService;

    constructor(toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        console.log(`WGS: ngOnInit. ID: ${this.idhash}`);
        this.idhash = `#${this.htmlid}`;
        this.display = this.display ? this.display
            : this.label ? "inline-block"
                : "block";
        if (this.entitiesOnly)
            this.allowEntitySelection = true;
    }

    ngOnChanges(changes: any) {
        if (this.isSelecting) {
            this.isSelecting = false;
            return;
        }
        if (!changes.selectedWorkgroup)
            return;
        //reset was hit
        if (changes.selectedWorkgroup && changes.selectedWorkgroup.currentValue == null && changes.selectedWorkgroup.previousValue != null) {
            this.selectedWorkgroupChange.emit(null);
            return;
        }

        if (!this.selectedWorkgroup)
            return;

        let id = this.selectedWorkgroup.IsNode
            ? this.selectedWorkgroup.getFullWorkgroupId()
            : this.selectedWorkgroup.EntityId;
        console.log(`WGS: changed detected. Selecting id ${id}`);
        if (this.initialized) {
            this.workgroupSelector.wfsTreeSelector('setSelection', id);
        }
        else {
            console.log('WGS: Waiting until initialized to select workgroup.');
            this.initSelection = true;
        }
    }

    ngAfterViewInit() {
        // init the workgroup selector.
        $(document).ready(() => {
            let self = this;
            if (this.userSelector) {
                this.workgroupSelector = $(this.idhash).wfsTreeSelector({
                    useExpander: this.useExpander,
                    entityURL: '/RecHubRaamProxy/api/entityUser/',
                    checkedStatusChanged: (selected, initial) => this.selectedUser(selected, initial),
                    dataCallback: (data) => this.userDataCallback(data),
                    expanderTitle: 'Select User',
                    height: this.height,
                    entitiesOnly: false,
                    icon: 'fa fa-user',
                    multiSelect: true,
                    checkedItems: this.selectedUsers
                });
            }
            else {
                this.workgroupSelector = $(this.idhash).wfsTreeSelector({
                    useExpander: this.useExpander,
                    entityURL: '/RecHubRaamProxy/api/entity',
                    callback: (selected, initial) => this.selectedTreeItemCallback(selected, initial),
                    dataCallback: (response, widget) => this.treeDataCallback(response, widget),
                    expanderTitle: "Select Workgroup",
                    height: this.height,
                    entitiesOnly: this.entitiesOnly,
                    closeOnSelectNonNode: this.allowEntitySelection
                });
            }
        });
    }

    private treeDataCallback(resp, widget) {
        console.log("WGS: Tree Data Callback.");
        let entities = this.workgroupSelector.wfsTreeSelector('getEntities');
        let workgroups = this.workgroupSelector.wfsTreeSelector('getWorkgroups');
        let ecount = entities.length;
        let wcount = workgroups.length;

        if (ecount === 1 && wcount === 1) {
            let element = $('<div class="oneworkgroup-mode"><input value="'
                + workgroups[0].label
                + `" class="form-control input-md" type="text" readonly style="width: ${this.width}px;display:inline;"></div>`);
            $(this.idhash).html(element);
            console.log("WGS: Only have 1 workgroup.");
            let sel = this.getWorkgroupSelection(workgroups[0]);
            this.selectedObject = workgroups[0];
            this.isSelecting = true;
            this.selectedWorkgroupChange.emit(sel);
        }
        else {
            this.workgroupSelector.wfsTreeSelector('initialize');
        }
        this.workgroupFinishedLoading.emit(null);
    }

    private selectedTreeItemCallback(selected, initial) {
        this.initialized = true;

        if (this.initSelection && this.selectedWorkgroup && this.selectedWorkgroup.BankId > 0 && this.selectedWorkgroup.WorkgroupId > 0) {
            this.initSelection = false;
            this.workgroupSelector.wfsTreeSelector('setSelection', this.selectedWorkgroup.BankId + '|' + this.selectedWorkgroup.WorkgroupId);
        }
        else if (this.initSelection && this.selectedWorkgroup && this.selectedWorkgroup.EntityId > 0 && this.allowEntitySelection) {
            this.initSelection = false;
            this.workgroupSelector.wfsTreeSelector('setSelection', this.selectedWorkgroup.EntityId);
        }
        else if (selected && initial && !selected.isNode && !this.allowEntitySelection) {
            return;
        }
        else if (selected && !selected.isNode && !this.allowEntitySelection) {
            this.toasterService.pop('error', 'Error', 'Please select a single workgroup.');
        }
        else if (selected) {
            let title = `Selected: ${selected.label}`;
            this.workgroupSelector.wfsTreeSelector('setTitle', title);
            this.isSelecting = true;
            if (this.allowEntitySelection) {
                this.selectedEntityChange.emit(selected.id);
            }
            let sel = this.getWorkgroupSelection(selected);
            console.log(`WGS: Selected ${isNaN(this.selectedWorkgroup.WorkgroupId) ? 'Entity' : 'Workgroup'}: ${selected.label}`);
            this.selectedWorkgroupChange.emit(sel);
            this.selectedObject = selected;
        }
        else {
            let title = initial;
            this.workgroupSelector.wfsTreeSelector('setTitle', title);
            this.selectedWorkgroupChange.emit(null);
        }
    }

    private getWorkgroupSelection(selected: any) {
        let split = selected.id.split('|');
        this.selectedWorkgroup = new Workgroup();
        this.selectedWorkgroup.BankId = selected.isNode ? parseInt(split[0]) : 0;
        this.selectedWorkgroup.WorkgroupId = selected.isNode ? parseInt(split[1]) : 0;
        this.selectedWorkgroup.EntityId = selected.isNode ? 0 : selected.id;
        this.selectedWorkgroup.IsNode = selected.isNode;
        this.selectedWorkgroup.Label = selected.label;
        this.selectedWorkgroup.EntityTypeCode = selected.entityTypeCode;
        return this.returnFullSelection ? selected : this.selectedWorkgroup;
    }

    private selectedUser(selected, initial) {
        console.log(`UserSelector: Selected count: ${selected.length}.`);
        let title = "";
        if (selected.length > 1)
            title = 'Selected: Multiple Users';
        else if (selected.length == 1)
            title = 'Selected: ' + selected[0].label;
        this.workgroupSelector.wfsTreeSelector('setTitle', title);
        this.selectedUsers = [];
        for (let user of selected) {
            this.selectedUsers.push(user.id);
        }
        this.selectedUsersChange.emit(this.selectedUsers);
    }

    private userDataCallback(data) {
        this.workgroupSelector.wfsTreeSelector('initialize');
        if (this.selectedUsers != null && this.selectedUsers.length > 0) {
            //this is only to prevent a wgs issue that occurs
            //when the selected users are returned with an htmlID
            //appended to them. We need to fix the issue in the wgs
            if (this.selectedUsers[0].startsWith(this.htmlid)) {
                this.selectedUsers.forEach((value, index, ary) =>
                    ary[index] = value.replace(this.htmlid + '-', '')
                )
            }
        }
        let title = "";
        if (this.selectedUsers.length > 1) {
            title = 'Selected: Multiple Users';
        }
        else if (this.selectedUsers.length == 1) {
            let id = `${this.htmlid}-${this.selectedUsers[0]}`;
            let user = this.workgroupSelector.wfsTreeSelector('getItemById', id);
            if (user != null) {
                title = `Selected : ${user.label}`
            }
        }
        this.workgroupSelector.wfsTreeSelector('setTitle', title);
    }
}