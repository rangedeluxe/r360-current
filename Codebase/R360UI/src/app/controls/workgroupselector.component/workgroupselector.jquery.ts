export {}
declare let $:any;
$(function() {
    $.widget("wfs.wfsTreeSelector", {
        // Default options.
        options: {
            lazyLoad: true,
            callback: function () { },
            dataCallback: null,
            entitiesOnly: false,
            entityURL: '/FrameworkViews/api/entity',
            entityChildrenUrl: '',
            expanderTitle: 'Please Select',
            getEntities: function () {
                return JSON.parse(JSON.stringify(this.originalEntities));
            },
            height: '375px',
            initialSelect: true,
            inSearchContext: false,
            icon: 'fa fa-list-alt',
            loadingMessage: 'Loading...',
            maxItemsPer: 100,
            minimumInputLength: 3,
            multiSelect: false,
            originalEntities: [],
            placeHolderMessage: 'Search',
            selectFilteredTo: 'Filtered to: ',
            selectNoItems: 'No items to search.',
            selectSearchMessage: 'Click here to search in: ',
            tooManyToastMessage: "There are too many elements to display.  Please use the filter.",
            useExpander: false,
            useFilterMessage: 'Use search filter.',
            closeOnSelectNode: true,
            closeOnSelectNonNode: true,
            // Millis to wait after the last keystroke to search.
            quietMillis: 1000,
            formatInputTooShort: function (input, min) { return "Enter a minimum of 3 characters"; },
            checkedItems: [],
            initialExpansion: true,
            displayMessageOnSelectNonNode: false,
            messageOnselectNonNode: "Please select a user or group.",
            checkedStatusChanged: function () { },
            debouncetimeout: null
        },
        _create: function () {
            console.log("WGS-JQ: Create.");
            var widget = this;
            var opts = widget.options;
    
            opts.nuts = $('<div">' + opts['loadingMessage'] + '</div>');
    
            widget.element.append(opts.nuts);
            // call for the data
            $.get(opts['entityURL'], { entitiesOnly: opts['entitiesOnly'] }, $.proxy(widget._orgGetCallback, widget), 'json');
        },
        initialize: function () {
            console.log("WGS-JQ: Initialize.");
            var widget = this;
            var opts = widget.options;
    
            // clear loading
            opts.nuts.html('');
    
            // create dom elements that support the widget
            var container = $('<div style="margin-bottom: 5px;"></div>');
            widget.element.append(container);
    
            if (opts['useExpander']) {
                var expander = $('<div id="filterExpander" style="display: none;"></div>');
                var eHeader = $('<div id="filterTitle">' + opts['expanderTitle'] + '</div>');
                var eBody = $('<div></div>');
                expander.append(eHeader).append(eBody);
    
                container.append(expander);
                opts.expander = expander;
                // do this to add the selector and tree to this element
                container = eBody;
            }
    
            if (opts['lazyLoad']) {
                opts.entitySelect = $('<input type="hidden" class="entitySelect" />');
                container.append(opts.entitySelect);
    
                opts.filteredContainer = $('<div id="filteredContainer" style="display: none;border: 1px solid #c7c7c7;border-bottom: none;padding: 5px 0px 5px 15px" ></div>');
                opts.filteredText = $('<span id="filterText"></span>');
                opts.filteredClear = $('<a id="filterClear" style="float: right;margin-right: 5px;cursor: pointer;" class="btn btn-primary btn-xxs">Reset</a>');
                opts.filteredContainer.append(opts.filteredText);
                opts.filteredContainer.append(opts.filteredClear);
                container.append(opts.filteredContainer);
    
                //event for clear
                opts.filteredClear.click(function () {
                    opts['inSearchContext'] = true;
                    opts.entitySelect.select2("val", "").trigger("change");
                    opts['callback'](null, opts['initialSelect']);			
                    widget.element.find('#filterTitle').html("");
                });
            }
            var domtreeid = widget.element[0].id + "-" + "treeSelect";
            opts.treeSelect = $('<div id="' + domtreeid + '"></div>');
            container.append(opts.treeSelect);
    
            if (opts['useExpander']) {
                // these lines are needed because the filter expander messes up the tree render
                opts.expander.jqxExpander({ expanded: false });
                opts.expander.on('expanded', function () {
                    opts.treeSelect.jqxTree('refresh');
                });
                // little styling
                opts.expander.on('expanding', function () {
                    if (opts['initialExpansion']) {
                        // Tree-Expand our first element, we're no longer doing this on page load.
                        opts.treeSelect.jqxTree('expandItem', opts.treeSelect.find('li:first')[0]);
                        opts['initialExpansion'] = false;
                    }
                    opts.expander.find('.jqx-expander-header').css("border-radius", "4px 4px 0px 0px");
                    widget._setTreeHeight();
                });
                opts.expander.on('collapsing', function () {
                    // if using expander... make call back when collapsed
                    var selected = opts.treeSelect.jqxTree('getSelectedItem');
                    var selecteditem = widget._findById(opts.getEntities(), selected.id, []);
                    selecteditem.id = selecteditem.originalId;
                    opts['callback'](selecteditem, opts['initialSelect']);
                    opts['initialSelect'] = false;
                    opts.expander.find('.jqx-expander-header').css("border-radius", "4px");
                });
                // show expander
                opts.expander.show();
                opts.expander.jqxExpander({ expanded: false });
                opts.expander.find('.jqx-expander-header').css("border-radius", "4px")
                    .css("background", "none repeat scroll 0 0 #fff")
                    .css("border-color", "#bebebe")
                    .css("padding", "5px 5px 4px 4px");
    
                // Calculates width with other features as well, like padding/margins.
                var w = opts.expander.find('.jqx-expander-header').outerWidth();
                opts.expander.find('.jqx-expander-content').css("position", "absolute").css("z-index", "200").css("width", w).css("border", "none");
            }
    
            var formatResult = function (entity) {
                // We're actually in the context of the select2, not the treeselector.
                var widget = this;
    
                // formats the entity filter dropdown li's
                var markup = "<table><tr><td style='font-size: 11px;'>";
                if (entity) {
                    markup += entity.path.map(function (elem) { return elem.label; }).join(' / ');
                    markup += "</td></tr><tr><td ><b>";
                    if (entity.isNode) {
                        markup += '<i class="' + opts['icon'] + '" style="margin-right: 3px;"></i>';
                    }
                    markup += entity.label + "</b>";
                }
                markup += "</td></tr></table>";
                return markup;
            };
    
            // init the entity filter dropdown
            if (opts['lazyLoad']) {
    
                opts.entitySelect.wfsSelectbox({
                    minimumInputLength: opts['minimumInputLength'],
                    width: '100%',
                    placeHolder: opts['placeHolderMessage'],
                    allowClear: false,
                    formatResult: formatResult,
                    callback: $.proxy(widget._entitySelection, widget),
                    formatInputTooShort: opts['formatInputTooShort'],
                    query: function (query) {
                        widget._debounceFn(opts['quietMillis'], function () {
                            var data = { results: [] };
                            var selected = widget._findById(opts.getEntities(), opts.treeSelect.jqxTree('getSelectedItem').id, []);
    
                            var searchFrom = opts.getEntities();
                            if (selected) {
                                selected.path.push(selected);
                                searchFrom = widget._setParentPath(selected);
                            }
    
                            // Here we really only want to search by string.contains.
                            // _find uses a Regex, so we're going to slash our input here on all regex special chars.
                            var querydata = query.term.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
                            widget._find(searchFrom, querydata, 'label', data.results, []);
    
                            // Now we need to filter out the parent data.
                            var itemstofilter = selected.path;
                            itemstofilter.push(selected);
    
                            $.each(itemstofilter, function (ind, ele) {
                                data.results = data.results.filter(function (el) {
                                    return el.originalId !== ele.originalId;
                                });
                            });
    
                            query.callback(data);
                        });
                    }
                });
            }
            //custom select style for this widget
            if (opts['useExpander'])
                widget.element.find('.select2-container .select2-choice').css("border-radius", "0px").css("border-bottom", "none");
            else
                widget.element.find('.select2-container .select2-choice').css("border-radius", "4px 4px 0px 0px").css("border-bottom", "none");
    
            // alter the IDs of the sent checkedItems.
            $(opts["checkedItems"]).each(function (i, e) {
                opts["checkedItems"][i] = widget.element[0].id + '-' + e;
            });
    
            // build the tree
            var trimmedItems = widget._pareDepth(0, 1, opts.getEntities());
            widget._buildTree(trimmedItems);
    
            // Moved from the Tree 'on expand' method.
            if (opts['multiSelect']) {
                var selectorid = widget.element[0].id;
                if (opts['checkedItems'].length > 0) {
                    for (var i = 0; i < opts['checkedItems'].length; ++i) {
                        var item = opts['checkedItems'][i];
                        var itemId = "#" + item;
                        opts.treeSelect.jqxTree('checkItem', $(itemId)[0], true);
                    }
                }
            }
        },
        reset: function(){
            var widget = this;
            var opts = widget.options;			
            widget.element.find('#filterTitle').html(opts['expanderTitle']);			
            opts.entitySelect.select2("val", "").trigger("change");			
        },
        setItems: function (value, refresh) {
    
            var widget = this;
            var opts = widget.options;
    
            opts.originalEntities = value;
    
            // Check to see if we want to refresh the treeview, yes by default.
            if (refresh === undefined)
                refresh = true;
    
            // Alter the ids so we can use multiple trees in one page.
            var treeid = widget.element[0].id;
            $(opts.originalEntities).each(function (index, ele) {
                widget._alterIds(widget, treeid, ele);
            });
    
            // Only refresh the TreeView if we need to.
            // If we don't want to, we'll be calling setFocus right after.
            if (refresh === true) {
                opts.treeSelect.jqxTree('clear');
                opts.treeSelect.jqxTree('addTo', widget._pareDepth(0, 1, opts.getEntities()));
                opts.treeSelect.jqxTree('expandItem', opts.treeSelect.find('li:first')[0]);
                opts.treeSelect.jqxTree('selectItem', opts.treeSelect.find('li:first')[0]);
            }
    
        },
        setFocus: function (id) {
            // Unlike setSelection, this method focuses the treeview and ensures expansion.
            // This method Assumes the parents of element of 'id' are visible, which they will be for RAAM.
            var widget = this;
            var opts = widget.options;
            var path = [];
            var entity = null;
    
            // Kick out early if we don't have a valid id.
            if (id === undefined || id === null)
                return;
    
            id = widget.element[0].id + '-' + id;
            entity = widget._findById(opts.getEntities(), id.toString(), path);
    
            // Kick out early if we could not find the entity in the list.
            if (entity === undefined || entity === null)
                return;
    
            var domid = '#' + entity.id;
            var domelement = $(domid);
    
            if ($(widget.element[0]).find("#filterClear:visible").length > 0
                && domelement && domelement.length > 0) {
                // If we're already in the context of the filter, we'll reset the filter to
                // the new selection.
                widget._entitySelection(entity);
            }
            else if (!domelement.length || domelement.length === 0) {
                // If we couldn't find the element, then either the parent was not yet expanded,
                // or the item was not yet added to the list.  We need to ensure both.
    
                // Magic number of '1' for the most immediate parent.
                var parent = widget._findById(opts.getEntities(), entity.path[entity.path.length - 1].id.toString(), path);
                var parentid = '#' + parent.id;
                var parentdom = $(parentid);
                var parenttreeitem = opts.treeSelect.jqxTree('getItem', parentdom[0]);
    
                if (parenttreeitem.isExpanded && !widget._isOverMaxItems(parent.items)) {
                    // When the parent has already been expanded.
                    //  and not over the limit
                    widget._insertIntoTree(entity, parent);
                }
                else if (widget._isOverMaxItems(parent.items)) {
                    // When the parent isn't expanded AND it has too many children to be expanded.
                    // So, we filter the tree (via search) to select it.
                    widget._entitySelection(entity);
                }
                else {					
                    opts.treeSelect.jqxTree('expandItem', parentdom[0]);   
                    // When the parent was expanded & collapsed.
                    widget._waitUntilTreeItemVisible(parentid, function(){widget._insertIntoTree(entity, parent);});
                }
    
                // Finally, select the added child node.
                widget._waitUntilTreeItemVisible(domid, function () {
                    domelement = $(domid);
                    opts.treeSelect.jqxTree('ensureVisible', domelement[0]);
                    opts.treeSelect.jqxTree('selectItem', domelement[0]);
                });
            }
            else {
                // Item already exist, so we should update it, and force a reselection event.
                // Incase the Name changed, we need to move it to the new position.
                // Possible to change the name of the root entity here.  Need to check for that.
    
                var parent = entity.path.length > 0
                    ? widget._findById(opts.getEntities(), entity.path[entity.path.length - 1].id.toString(), path)
                    : null;
    
                // if the parent is null, we probably got here by deleting an entity
                // who is a child of the root. The parent is null because 'entity' is
                // the root.
                if (parent === null)
                {
                    opts.treeSelect.jqxTree('refresh');
                }
                else
                {
                    opts.treeSelect.jqxTree('removeItem', domelement[0]);
                    widget._insertIntoTree(entity, parent);
                    domelement = $(domid);
                }
    
                // Update the children.
                var childrendom = domelement.children('ul').children('li');
                $(childrendom).each(function (ind, ele) {
                    opts.treeSelect.jqxTree('removeItem', ele);
                });
                $(entity.items).each(function (ind, ele) {
                    opts.treeSelect.jqxTree('addTo', [ele], domelement[0]);
                });
    
                // Finally selecting the item.
                opts.treeSelect.jqxTree('updateItem', domelement[0], entity);
                opts.treeSelect.jqxTree('selectItem', null);
                opts.treeSelect.jqxTree('selectItem', domelement[0]);
            }
        },
        setSelection: function (id) {
            // Not called by anything inside this control - for external use.
    
            var widget = this;
            var opts = widget.options;
            var path = [];
            var entity = null;
    
            if (id !== undefined && id !== null) {
                id = widget.element[0].id + '-' + id;
                entity = widget._findById(opts.getEntities(), id.toString(), path);
            }
    
            widget._entitySelection(entity, true);
    
            if (opts["useExpander"] && !opts.expander.jqxExpander("expanded")) {
                //must call get selection to determine what was selected as setting selection to nothing
                // default selection to the root
                var selected = opts.treeSelect.jqxTree('getSelectedItem');
                var selecteditem = widget._findById(opts.getEntities(), selected.id, []);
                selecteditem.id = selecteditem.originalId;
                opts['callback'](selecteditem, opts['initialSelect']);
                opts['initialSelect'] = false;
            }
    
        },
        getSelection: function () {
            var widget = this;
            var opts = widget.options;
    
            var selected = opts.treeSelect.jqxTree('getSelectedItem');
            var entity = widget._findById(opts.getEntities(), selected.id, []);
            entity.id = entity.originalId;
            return entity;
        },
        getItemById: function (id) {
            var widget = this;
            var opts = widget.options;
            return widget._findById(opts.getEntities(), id, []);
        },
        getCheckedItems: function () {
            var widget = this;
            var opts = widget.options;
            var rvalue = [];
    
            if (opts['multiSelect']) {
                var treeid = widget.element[0].id;
                var selected = opts['checkedItems'];
                for (var i = 0; i < selected.length; i++) {
                    var id = selected[i];
                    var checkedItem = widget._findById(opts.getEntities(), id, []);
                    if (checkedItem) {
                        checkedItem.id = checkedItem.originalId;
                        rvalue.push(checkedItem);
                    }
                }
            }
            return rvalue;
        },
        checkItem: function (id, checked) {
            // Not called by anything inside this control - for external use.
    
            var widget = this;
            var opts = widget.options;
            if (id !== undefined && id !== null) {
                opts.treeSelect.jqxTree('checkItem', $(id)[0], checked);
            }
        },
        setTitle: function (title) {
            var widget = this;
            var opts = widget.options;
    
            if (title)
                widget.element.find('#filterTitle').html(title);
            else
                widget.element.find('#filterTitle').html(opts['expanderTitle']);
        },
        getEntities: function () {
            var widget = this;
            var opts = widget.options;
    
            var results = [];
            var ents = widget._find(opts.originalEntities, false, 'isNode', results, []);
            $(ents).each(function (i, e) {
                // We're de-referencing the object here (cloning) to prevent editing of the items in the original list.
                // It seems _find returns a reference, but _findById returns a non-reference.
                e = $.extend({}, e);
                e.id = e.originalId;
                ents[i] = e;
            });
            return ents;
        },
        getWorkgroups: function () {
            var widget = this;
            var opts = widget.options;
    
            var results = [];
            var workgroups = widget._find(opts.originalEntities, true, 'isNode', results, []);
            $(workgroups).each(function (i, e) {
                // We're de-referencing the object here (cloning) to prevent editing of the items in the original list.
                // It seems _find returns a reference, but _findById returns a non-reference.
                e = $.extend({}, e);
                e.id = e.originalId;
                workgroups[i] = e;
            });
            return workgroups;
        },
        getWorkgroupsForEntity: function (entity) {
            var widget = this;
            var opts = widget.options;
    
            var results = [];
            var workgroups = widget._find([entity], true, 'isNode', results, []);
            $(workgroups).each(function (i, e) {
                e = $.extend({}, e);
                e.id = e.originalId;
                workgroups[i] = e;
            });
            return workgroups;
        },
        _insertIntoTree: function (entity, parent)
        {
            var widget = this;
            var opts = widget.options;
    
            // We need to find out the correct (sorted) position where the new child is needs to go.
            var parentid = '#' + parent.id;
            var parentdom = $(parentid);
            var parenttreeitem = opts.treeSelect.jqxTree('getItem', parentdom[0]);
            var setafter = null;
            var setbefore = null;
            var siblingdom = null;
            var siblings = parent.items;
            siblings.sort(function (a, b) { return a.label < b.label ? -1 : a.label > b.label ? 1 : 0 });
            var index = siblings.map(function (e) { return e.label; }).indexOf(entity.label);
    
            if (index === 0 && siblings.length === 1) {
                // means we're the only child.
                // we'll leave both before/after set to null.
            }
            else if (index === 0) {
                // means we need to go to the first position.
                setbefore = siblings[1];
                siblingdom = $('#' + siblings[1].id);
            }
            else if (siblings.length > 0) {
                // means we need to add after a particular entity.
                setafter = siblings[index - 1];
                siblingdom = $('#' + siblings[index - 1].id);
            }
    
            if (setbefore == null && setafter == null)
                opts.treeSelect.jqxTree('addTo', [entity], parentdom[0]);
            else if (setbefore != null)
                opts.treeSelect.jqxTree('addBefore', [entity], siblingdom[0]);
            else
                opts.treeSelect.jqxTree('addAfter', [entity], siblingdom[0]);
        },
        _alterIds: function (widget, parentid, current) {
            if (!current || current == null)
                return;
            current.originalId = current.id;
            current.id = parentid + '-' + current.id;
            $(current.items).each(function (index, ele) {
                widget._alterIds(widget, parentid, ele);
            });
        },
        _waitUntilTreeItemVisible: function (treeid, callback) {
            // Function to actually ensure visibility of a treeitem.
            // The Tree.EnsureVisibility does not work on Lazy-Loaded trees.
            var widget = this;
            var opts = widget.options;
            var timeout;
            timeout = window.setInterval(function () {
                var dom = $(treeid);
                if (dom.length && dom.length > 0) {
                    window.clearInterval(timeout);
                    callback();
                }
    
            }, 300);
        },
        _orgGetCallback: function (resp, callbackArguments) {
            var widget = this;
            var opts = widget.options;
            opts.originalEntities = resp;
    
            // Alter the IDs based on the ID of the parent widget.
            // Need to do this if we want to have more than 1 tree selector on the page.
            var treeid = widget.element[0].id;
            $(opts.originalEntities).each(function (index, ele) {
                widget._alterIds(widget, treeid, ele);
            });
    
            $(opts.originalEntities).each(function (index, ele) {
                widget._addLoading(widget, ele);
            });
    
            if (opts['dataCallback']) {
                opts['dataCallback'](resp, widget);
            }
            else {
                widget.initialize();
            }
        },
        _addLoading: function (widget, ele) {
            if(ele.loadDynamically) {
                ele.items =[{"value": "", "label": widget.options['loadingMessage']}];
            } else {
                if (ele.items) {
                    $(ele.items).each(function (index, ele) {
                        widget._addLoading(widget, ele);
                    });
                }
            }
        },
        _setTreeHeight: function () {
            // This little guy sets the height of the tree window.
            // For example, if the user's browser window is small, we don't want the tree
            // to be so vertical as to go beyond the window frame.
    
            var widget = this;
            var opts = widget.options;
    
            // If we're not using the expander (raam), then just kick out. Nothing to do.
            if (opts.useExpander !== true)
                return;
    
            // The tree appears where the expander does.
    
            var tree = opts.treeSelect;
            var expander = opts.expander;
            var windowheight = window.innerHeight;
            var treeheight = $(tree).jqxTree('height');
            var scrolltop = $(window).scrollTop();
            var optsheight = parseInt(opts.height.match(/(\d*\.?\d*)(.*)/)[1]);
            treeheight = parseInt(treeheight.match(/(\d*\.?\d*)(.*)/)[1]);
            var treeposition = $(expander).offset().top + expander.height() - scrolltop;
    
            var newheight = windowheight - treeposition - expander.height();
            if (newheight > optsheight)
                newheight = optsheight;
    
            // Finally set the tree.
            let h = newheight + "px";
            tree.jqxTree({ height: h});
            tree.jqxTree('refresh');
        },
        _buildTree: function (items) {
            var widget = this;
            var opts = widget.options;
    
            var treeSettings = {
                source: items,
                allowDrag: false,
                height: opts['height'],
                width: '100%',
                toggleMode: 'none',
                checkboxes: false,
                hasThreeStates: false
            };
    
            if (opts['multiSelect']) {
                treeSettings.checkboxes = true;
                treeSettings.hasThreeStates = true;
            }
    
            var tree = opts.treeSelect.jqxTree(treeSettings);
            var treeid = widget.element[0].id;
            tree.on('checkChange', function (event) {
                var args = event.args;
                var element = args.element;
                var checked = args.checked;
                var id = treeid + '-' + element.id;
                var item = widget._findById(opts.getEntities(), element.id, []);
                var checkedItems = opts['checkedItems'];
                if (item.isNode) {
                    if (checked) {
                        var index = checkedItems.indexOf(element.id);
                        if (index == -1) {
                            checkedItems.push(element.id);
                        }
                    } else {
                        var index = checkedItems.indexOf(element.id);
                        if (index > -1) {
                            checkedItems.splice(index, 1);
                        }
                    }
                }
    
                // Build the return callback object.
                //  - Which is an array of all selected items.
                var retary = [];
                $(checkedItems).each(function (i, e) {
                    // e is a string.
                    var itemid = e;
                    var item = widget._findById(opts.getEntities(), itemid, []);
                    if (item) {
                        item.id = item.originalId;
                        retary.push(item);
                    }
                });
    
                opts["checkedStatusChanged"](retary);
    
            });
    
            tree.on('expand', function (event) {
    
                var eItem = tree.jqxTree('getItem', event.args.element);
                var origItem = widget._findById(opts.getEntities(), eItem.id, []);
                if (origItem.loadDynamically) {                    
                    var item = widget._findById(opts.originalEntities, eItem.id, []);
                    var loadChildrenEvent = event;
                    $.get(opts['entityChildrenUrl'], { entityId: origItem.realId }, 
                        function (resp, result) {
                            item.loadDynamically = false;
                            item.items = resp.items;
                            var treeid = widget.element[0].id
                            $(item.items).each(function (index, ele) {
                                widget._alterIds(widget, treeid, ele);
                                widget._addLoading(widget, ele);
                            });
                            tree.trigger(loadChildrenEvent);
                        }, 'json');
                    return;
                }
                var $element = $(event.args.element);
                var loader = false;
                var loaderItem = null;
                var children = $element.find('ul:first').children();
                $.each(children, function () {
                    var item = tree.jqxTree('getItem', this);
                    if (item && (item.label === opts['loadingMessage'] || item.label === opts['useFilterMessage'])) {
                        loaderItem = item;
                        loader = true;
                        return false
                    };
                });
                if (loader) {
                    if (widget._isOverMaxItems(origItem.items)) {
                        opts.entitySelect.select2("open");
                        opts['inSearchContext'] = true;
                        //framework.noticeToast(widget.element, opts['tooManyToastMessage'], { sticky: true });
                        tree.jqxTree('addTo', [{ "value": "", "label": opts['useFilterMessage'] }], $element[0]);
                        tree.jqxTree('selectItem', event.args.element);
                    }
                    else {
                        var itemstoadd = widget._pareDepth(0, 0, origItem.items);
                        var checked = widget.getCheckedItems();
                        $.each(itemstoadd, function (ind, ele) {
                            var preexisting = tree.jqxTree('getItem', ele);
                            if (!preexisting || preexisting === undefined)
                            {
                                tree.jqxTree('addTo', [ele], $element[0]);
                                // If filtered on the server add loader element
                                // We need to select the item (check it) if it was previously checked.
                                var res = $.grep(checked, function (checkeditem) { return checkeditem.originalId == ele.originalId; });
                                if (res && res.length > 0)
                                    opts.treeSelect.jqxTree('checkItem', $('#' + ele.id)[0], true);
                            }
                        });
    
                        // Now (after we've expanded the entity) we want to scroll down until the entity is on top.
                        var scrollingpanel = $('#' + eItem.id).parents('.jqx-widget-content')[0];
                        var scrollto = $('#' + eItem.id).position().top;
                        var scrollbar = $(scrollingpanel)
                            .parent()
                            .find('.jqx-scrollbar');
                        if (scrollbar && scrollbar.length > 0)
                            $(scrollbar[0]).jqxScrollBar('setPosition', scrollto);
                    }
    
                    // Now just remove the "loading..." element.
                    tree.jqxTree('removeItem', loaderItem.element);
                }
                $('.itemicon').replaceWith('<i class="' + opts['icon'] + '" style="margin-right: 3px;"></i>');
    
            });
            $(tree).on('dblclick', '.jqx-tree-item', function (event) {
                // Called on Every double-click.
    
                var id = $(this).parent().attr('id');
                var item = widget._findById(opts.getEntities(), id, []);
    
                widget._fireClickEvent(item);
            });
    
            tree.on('select', function (event) {
                // Called on Every single-click & every selection from the Select2 dropdown.
    
                var item = tree.jqxTree('getItem', event.args.element);
                // check for sub items... no sub items should not allow a search
                if (item['hasItems']) {
                    widget.element.find('.select2-chosen').html(opts['selectSearchMessage'] + item.label);
                    opts.entitySelect.select2("readonly", false);
                }
                else {
                    widget.element.find('.select2-chosen').html(opts['selectNoItems']);
                    opts.entitySelect.select2("readonly", true);
                }
    
                var value = widget._findById(opts.getEntities(), item.id, []);
                if (!opts['useExpander'] || opts['initialSelect']) {
                    // callback on select if not in expander or when data is first loaded and default selection made
                    value.id = value.originalId;
                    opts['callback'](value, opts['initialSelect']);
                    opts['initialSelect'] = false;
                }
    
            });
    
            if (opts['lazyLoad']) {
                opts.treeSelect.jqxTree('selectItem', opts.treeSelect.find('li:first')[0]);
                if (!opts['useExpander']) {
                    // If we're using the expander, we're going to be doing this in our 'Expanding' callback instead.
                    opts.treeSelect.jqxTree('expandItem', opts.treeSelect.find('li:first')[0]);
                }
            }
            else {
                opts.treeSelect.jqxTree('selectItem', opts.treeSelect.find('li:first')[0]);
                opts.treeSelect.jqxTree('expandAll');
            }
        },
        _formatSelection: function (entity) {
            // formats the text in the entity filter dropdown once selected
            var widget = this;
            var opts = widget.options;
            return opts['selectFilteredTo'] + entity.label;
        },
        _fireClickEvent: function (value) {
            // Just consolidating some on-click copied code.
            var widget = this;
            var opts = widget.options;
    
            if (opts['useExpander'] && opts['closeOnSelectNode']
                && value.isNode) {
                if (opts.expander.find(".jqx-expander-content:visible")[0])
                    opts.expander.find('.jqx-expander-header').click();
            }
            else if (opts.useExpander && !opts.closeOnSelectNode && value.isNode) {
                // means we selected a workgroup, but DONT want to auto-close the expander.
                value.id = value.originalId;
                opts['callback'](value, opts['initialSelect']);
                opts['initialSelect'] = false;
            }
    
            //entity has been selected
            if (opts['useExpander'] && !value.isNode) {
                if (opts.closeOnSelectNonNode) {
                    if (opts.expander.find(".jqx-expander-content:visible")[0])
                        opts.expander.find('.jqx-expander-header').click();
                }
                else {
                    // means we selected an entity, but DONT want to auto-close the expander.
                    value.id = value.originalId;
                    opts['callback'](value, opts['initialSelect']);
                    opts['initialSelect'] = false;
                }
                if (opts.displayMessageOnSelectNonNode) {
                    //framework.errorToast(widget.element, opts['messageOnselectNonNode'], { sticky: true });
                }
            }
    
        },
        _entitySelection: function (value) {
    
            var widget = this;
            var opts = widget.options;
            var items = [];
    
            var tooMany = false;
            if (value) {
                // set the text to communicate the data is filtered
                opts.filteredContainer.css("display", "block");
                opts.filteredText.html(opts['selectFilteredTo'] + value['label']);
    
                if (widget._isOverMaxItems(value.items)) {
                    // the searched item has too many items to display
                    tooMany = true;
                    value.items = [{ "value": "", "label": opts['useFilterMessage'] }];
                }
                // add this obj to its path to build the full path for the tree grid
                value.path.push(value);
                items = widget._setParentPath(value);
    
            }
            else {
                // no filter clear and hide the filter
                opts.filteredContainer.css("display", "none");
                opts.filteredText.html("");
    
                items = widget._pareDepth(0, 1, opts.getEntities());
            }
    
            opts.treeSelect.jqxTree('clear');
            opts.treeSelect.jqxTree('addTo', items);
    
            // defualt expansion/selection
            var toExpand = opts.treeSelect.find('li:first')[0];
            var toSelect = opts.treeSelect.find('li:first')[0];
            if (value) {
                var selectId = value.id;
                if (typeof selectId === "string") {
                    selectId = selectId.replace("|", "\\|");
                }
                // if there is a value... override the values above with this element
                var element = $("#" + selectId)[0];
                // just expand the parent if the selected item has too many items)
                toExpand = tooMany ? opts.treeSelect.jqxTree('getItem', element).parentElement : element;
                toSelect = element;
            }
    
            opts.treeSelect.jqxTree('expandItem', toExpand);
            opts.treeSelect.jqxTree('selectItem', toSelect);
    
            if (value) {
                widget._fireClickEvent(value);
            }
    
        },
        _pareDepth: function (inProcDepth, depth, items) {
            var widget = this;
            var opts = widget.options;
            // allows you to remove sub items so that the tree grid doesn't 
            // have to apply thousands of items that are not in view.
            // if lazy load is not on then no pairing since we need all items avail...
            if (opts['lazyLoad']) {
                var widget = this;
                var opts = widget.options;
                if (widget._isOverMaxItems(items)) {
                    items = [{ "value": "", "label": opts['useFilterMessage'] }];
                }
                else {
                    for (var i = 0; i < items.length; i++) {
                        if (depth > inProcDepth && items[i].items) {
                            // leave alone, need this level's items
                            items[i].items = widget._pareDepth(inProcDepth + 1, depth, items[i].items);
                        }
                        else {
                            // this item level needs its items removed
                            if (items[i].items && items[i].items.length > 0) {
                                items[i].items = [{ "value": "", "label": opts['loadingMessage'] }];
                            }
                        }
                    }
                }
            }
            return items;
        },
        _find: function (arr, term, prop, rvalue, path) {
            var widget = this;
            var opts = widget.options;
            // method to find items by label text... regex contains.  
            // also, creates a path property on searched items from the root.  path does not include the matched object
            for (var i = 0; i < arr.length; i++) {
                var obj = arr[i];
    
                var result = null;
                if (typeof term === "boolean") {
                    result = (obj[prop] === term) ? 1 : -1;
                }
                else {
                    result = obj[prop].search(new RegExp(term, "i"));
                }
    
                if (result > -1) {
                    obj.path = path.slice(0);
                    rvalue.push(obj);
                }
                if (obj['items']) {
                    var clean = $.extend({}, obj);
                    clean.items = [];
                    path.push(clean);
                    widget._find(obj['items'], term, prop, rvalue, path);
                }
            }
            path.pop();
            return rvalue;
        },
        _findById: function (arr, id, path) {
            var widget = this;
            var opts = widget.options;
            // method to find first item with matching id.  
            // also, creates a path property on searched items from the root.  path does not include the matched object
            var rvalue = null;
            for (var i = 0; i < arr.length; i++) {
                var obj = arr[i];
                if (obj.id === id) {
                    obj.path = path.slice(0);
                    rvalue = obj;
                }
                else if (!rvalue && obj['items']) {
                    var clean = $.extend({}, obj);
                    clean.items = [];
                    path.push(clean);
                    rvalue = widget._findById(obj['items'], id, path);
                    path.pop();
                }
                if (rvalue) break;
            }
    
            return rvalue;
        },
        _setParentPath: function (node) {
            // builds the heirarchy "parent-> node -> node...) based on given node's path.  
            // the node will have its path set by _find or _findById
            if (node) {
                for (var i = node.path.length - 1; i > 0; i--) {
                    if (node.path[i]) {
                        node.path[i - 1].items = [node.path[i]]
                    }
                }
            }
            return (node && node.path) ? [node.path[0]] : null;
        },
        _isOverMaxItems: function (items) {
            // conveinence method for checking item array length compared to max items allowed
            return (items && items.length > this.options['maxItemsPer']);
        },
        _debounceFn: function(quietMillis, fn, ctx) {
            var widget = this;
            var opts = widget.options;
            ctx = ctx || undefined;
    
            var args = arguments;
            window.clearTimeout(opts.debouncetimeout);
            opts.debouncetimeout = window.setTimeout(function () {
                fn(ctx, args);
            }, quietMillis);
        }
    
    });
});
