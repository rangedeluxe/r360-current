import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
declare let window;
@Component({
    moduleId: module.id.toString(),
    selector: 'refresh',
    templateUrl: './refresh.component.html',
    styleUrls: ['./refresh.component.css']
})
export class RefreshComponent implements OnInit {
    @Input() hoverMessage: string = "Click to refresh.";
    @Input() printHoverMessage: string = "Click to print.";
    @Input() showRefresh: boolean = true;
    @Input() showPrint: boolean = true;
    @Output() onRefresh: EventEmitter<any> = new EventEmitter();
    constructor() {
        
    }

    ngOnInit() {
        
    }

    print() {
        window.print();
    }
}