import { Component, Input, ViewChild } from '@angular/core';
import { Alert } from '../../DTOs/Alert';

@Component({
  moduleId: module.id.toString(),
  selector: 'extractcomplete',
  templateUrl: 'alertextractcomplete.component.html',
  styleUrls: ['alertextractcomplete.component.css']
})
export class AlertExtractComplete {
  @Input() currentAlert: Alert;
  private loading: boolean = false;
  constructor() {  }

  onBlur() {
    this.currentAlert.Pristine = false;
  }
  
}
