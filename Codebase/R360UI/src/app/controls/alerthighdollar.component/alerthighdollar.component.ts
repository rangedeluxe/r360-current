import { Component, Input } from '@angular/core';
import { Alert } from "../../DTOs/Alert";

@Component({
  moduleId: module.id.toString(),
  selector: 'highdollar',
  templateUrl: 'alerthighdollar.component.html',
  styleUrls: ['alerthighdollar.component.css']
})

export class AlertHighDollar {
  @Input() currentAlert: Alert;
  @Input() public operators: any[];

  operatorChanged(selVal: any) {    
    this.onBlur();
  }

  onBlur() {
    this.currentAlert.Pristine = false;
  }

  eventKeyPress(event: any) {
    //numbers over a certain magnitud get turned into an exponential form
    //and their length is not the length of the string. This calculates it properly.
    //Comparison is >= to account for the new character that hasn't been added yet
    let numberOfDigits = Math.floor(Math.log(+this.currentAlert.EventValue) / Math.LN10 + 1)
    if (numberOfDigits >= 80)
      event.preventDefault();
  }

  compareOperators(val1, val2):boolean{
    return val1 != null && val2 != null && val1.id == val2.id;
  }
}
