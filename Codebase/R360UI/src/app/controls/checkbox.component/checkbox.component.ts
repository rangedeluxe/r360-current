import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

@Component({
    moduleId: module.id.toString(),
    selector: 'checkbox',
    templateUrl: 'checkbox.component.html',
    styleUrls: ['checkbox.component.css']
})
export class CheckboxComponent implements OnInit {
    @Input() public checked: boolean = false;
    @Input() public text: string = "";
    @Input() public htmlid: string = "";
    @Input() public subtext: string = "";

    @Output() checkedChange = new EventEmitter();

    constructor() {

    }

    public toggleChecked() {
        console.log(`Checkbox: Toggle value for ${this.text}.`);
        this.checked = !this.checked;
        this.checkedChange.emit(this.checked);
    }

    ngOnInit() { 
        // A little validation to ease future headaches.
        if (!this.htmlid || this.htmlid.length == 0) {
            console.error("Checkbox: HtmlID is required in HTML binding.");
        }
    }
}