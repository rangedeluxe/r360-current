import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Alert } from "../../DTOs/Alert";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Workgroup } from "../../DTOs/Workgroup";


@Component({
  moduleId: module.id.toString(),
  selector: 'fatalsystemerror',
  templateUrl: 'alertfatalsystemerror.component.html',
  styleUrls: ['alertfatalsystemerror.component.css']
})

export class AlertFatalSystemError  {


  @Input() public alertMessage: string = "";
  @Input() currentAlert: Alert;
  @Output() isFormValid = new EventEmitter();


  ngOnInit() {
    this.currentAlert.Workgroup = new Workgroup();
    this.currentAlert.Workgroup.BankId = -1;
    this.currentAlert.Workgroup.WorkgroupId = -1;
  }
  
  onBlur(){
    this.currentAlert.Pristine = false;
  }

}
