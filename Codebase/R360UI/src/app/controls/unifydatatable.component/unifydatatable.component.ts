import { Component, OnInit, AfterViewInit, SecurityContext } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { DataTableRow } from '../../DTOs/DataTableRow';
import { RowGroup } from '../../DTOs/RowGroup';
import { DataTableGroup } from '../../DTOs/DataTableGroup';
import { dlxSanitizerService } from '../../services/dlxSanitizer.service/dlxSanitizer.service';

declare let $: any;

@Component({
    moduleId: module.id.toString(),
    selector: 'unifydatatable',
    templateUrl: 'unifydatatable.component.html',
    styleUrls: ['unifydatatable.component.css'],
    providers: [dlxSanitizerService]
})
export class UnifyDatatableComponent implements OnInit, AfterViewInit {
    @Input() public htmlid: string = "";
    @Input() public class: string = "table dataTable table-striped table-hover stretch";
    @Input() public columns: Array<any> = [];
    @Input() public dataUrl: string;
    @Input() public data: Array<any> = [];
    @Input() public lengthOptions = [10, 25, 50, 100];
    @Input() public loading: boolean = false;
    @Input() public searching: boolean = true;
    @Input() public lengthChange: boolean = true;
    @Input() public info: boolean = true;
    @Input() public emptyTable: string = "No data available in table"
    @Input() public addNewRowText: string = "Add New Row"
    @Input() public grouping: DataTableGroup;
    @Input() public groupingFunction: Function;
    @Input() public headerFunction: Function;
    @Input() public footerFunction: Function;
    @Input() public preDataFunction: Function;
    @Input() public pageLength: number = 10;
    @Input() public dataRowHoverMessage: string;
    @Input() public groupRowHoverMessage: string;
    @Input() public isServerSide: boolean = false;
    @Input() public isDeferLoading: boolean = false;
    @Input() public errorFunction: Function;
    @Input() public isSuppressDefaultErrors: boolean = false;
    @Input() public searchFunction: Function;
    @Input() public preLoadFunction: Function;
    @Input() public postLoadFunction: Function;
    @Input() public search: string = '';
    @Input() public sortEnabled: boolean = true;
    @Input() public order: Array<any>;
    @Input() public scrollX: boolean = false;
    @Input() public autoWidth: boolean = true;
    @Output() createdRow = new EventEmitter<DataTableRow>();
    @Output() clickedRow = new EventEmitter<any>();
    @Output() clickedColumn = new EventEmitter<any>();
    @Output() createdFooter = new EventEmitter<any>();
    @Output() searchChange = new EventEmitter<any>();
    @Output() initComplete = new EventEmitter<any>();

    private table: any;
    private currentSortColumn: Array<any> = [0, 'asc'];


    constructor(private sanitizerService: dlxSanitizerService) {
    }

    ngOnChanges(changes: any) {
        if (this.grouping && this.groupingFunction && this.table)
            this.sortGrouping();
        if (changes && changes.search && this.table) {
            this.table.search(changes.search.currentValue);
        }
        this.redraw(changes);
    }

    ngOnInit() {
        console.log(`DataTable: ngOnInit. ID: ${this.htmlid}`);
    }

    ngAfterViewInit() {
        // we keep the spinner on the table if it is server side
        // since we have to wait until the actual first call happens
        //this.loading = this.isServerSide ? true : false;
        // A quick check to make sure we don't pass some common incorrect values.
        if (typeof this.pageLength === 'string') {
            console.error("DataTable: Error: passed a string to 'pagelength' value.");
            return;
        }

        let self = this;
        if (this.grouping && this.groupingFunction) {
            // Override the default sorting behavior.
            for (let c of this.columns)
                c.sortable = false;
        }



        this.table = $(`#${this.htmlid}`).
            on('error.dt', (e, settings, techNote, message) => {
                if (self.errorFunction)
                    self.errorFunction(e, settings, techNote, message);
            }).
            on('preInit.dt', (e, settings) => {
                this.loading = true;
            }).
            DataTable({
                scrollX: this.scrollX == null ? false : this.scrollX,
                autoWidth: this.autoWidth == null ? true : this.autoWidth,

                lengthMenu: this.lengthOptions,
                columns: this.columns,
                data: this.data,
                pageLength: this.pageLength,
                serverSide: this.isServerSide,
                deferLoading: this.isDeferLoading ? 0 : null,
                order: this.order ? this.order : [0, 'asc'],
                search: {
                    search: this.search
                },
                sort: this.sortEnabled,
                initComplete: function (settings, json) {
                    self.initComplete.emit(this);
                    self.loading = false;
                    //self.loading = self.isServerSide ? true : false;
                    //this hides the extra row created by datatables when scrollX is enabled...
                    if (self.scrollX) {
                        $('.dataTables_scrollBody thead tr').css({ visibility: 'collapse' });
                    }

                    // Add 2 buttons here for the expand all / collapse all groups.
                    if (!self.grouping || !self.groupingFunction)
                        return;

                    $(`#${self.htmlid}_wrapper .dataTables_filter > label`)
                        .after($('<button class="btn btn-inverse expand-button" title="Expand all groups"><i class="fa fa-expand"></i></button>')
                            .click(function () {
                                $(`#${self.htmlid} tr.group`)
                                    .find('.fa')
                                    .removeClass('fa-plus')
                                    .addClass('fa-minus');
                                $(`#${self.htmlid} tr.datarow`)
                                    .show();
                            }))
                        .after($('<button class="btn btn-inverse collapse-button" title="Collapse all groups"><i class="fa fa-compress"></i></button>')
                            .click(function () {
                                $(`#${self.htmlid} tr.group`)
                                    .find('.fa')
                                    .removeClass('fa-minus')
                                    .addClass('fa-plus');
                                $(`#${self.htmlid} tr.datarow`)
                                    .hide();
                            }));
                },
                createdRow: function (row, data, index) {
                    $(row).addClass('datarow');
                    for (let cell of row.cells)
                        cell.innerText = self.sanitizerService.sanitizeHTML(cell.innerText);
                    self.createdRow.emit({ element: row, data: data, index: index, table: this.api() });
                    if (self.dataRowHoverMessage)
                        $(row).attr('title', self.dataRowHoverMessage);
                },
                headerCallback: function (row, data, start, end, display) {
                    if (!self.headerFunction)
                        return;
                    self.headerFunction(this.api(), data);
                },
                footerCallback: function (row, data, start, end, display) {
                    if (!self.footerFunction)
                        return;

                    let values = self.footerFunction(data) as Array<RowGroup>;
                    let tds = values.map((v, i) => {
                        let td = `<td colspan="${v.columnLength}" class="${v.class}">`;
                        td += `<strong>${v.value}</strong></td>`;
                        return v.value instanceof $ ? v.value : td;
                    });
                    $(row).empty();
                    for (let td of tds)
                        $(row).append(td);


                },
                drawCallback: function (settings) {


                    //update the pager and default label text
                    $('.previous').find('a').html('‹');
                    $('.next').find('a').html('›');
                    var flabel = $('.dataTables_info');
                    flabel.html($('.dataTables_info').text().replace("to", "-").replace("entries", "").replace("Showing", ""));

                    //update pagination to bootstrap 4
                    $('.pagination').find('li').each(function () {
                        $(this).addClass('page-item');
                        $(this).find('a').each(function () {
                            $(this).addClass('page-link');
                        })
                    });

                    //this hides the extra row created by datatabkes when scrollX is enabled...
                    //it readds it every time the draw event happens....lame
                    if (self.scrollX) {
                        $('.dataTables_scrollBody thead tr').css({ visibility: 'collapse' });
                    }


                    let api = this.api();
                    let rows = api.rows({ page: 'current' }).nodes();
                    let currentrows = api.rows({ page: 'current' }).data();
                    let last = null;
                    let alldata = api.rows().data();

                    //update header row visibility based on data
                    if (alldata.length == 0) { $(`#${self.htmlid} thead`).hide(); }
                    else { $(`#${self.htmlid} thead`).show(); }

                    // this is to handle grouping and nothing more.
                    if (!self.grouping || !self.groupingFunction)
                        return;

                    // Show all rows, we could have hid them due to grouping.
                    $(`#${self.htmlid} .datarow`).show();

                    $.each(currentrows, function (ind, data) {
                        let value = data[self.grouping.dataSource];
                        let groupingname = self.grouping;
                        if (value !== last) {
                            let values = self.groupingFunction(alldata, self.grouping, value) as Array<RowGroup>;
                            let tds = values.map((v, i) => {
                                let td = `<td colspan="${v.columnLength}" class="${v.class}">`;
                                if (i == 0)
                                    td += '<span class="fa fa-2 fa-minus" /> ';
                                td += `<strong>${v.value}</strong></td>`;
                                return td;
                            });
                            let $tr = $('<tr class="group"></tr>');
                            if (self.groupRowHoverMessage)
                                $tr.attr('title', self.groupRowHoverMessage);
                            for (let td of tds)
                                $tr.append(td);
                            $(rows).eq(ind)
                                .before($tr);
                        }
                        last = value;
                    });
                },
                ajax: !self.isServerSide ? null : {
                    url: self.dataUrl,
                    type: 'POST',
                    data: (d) => {
                        if (self.preDataFunction)
                            self.preDataFunction(d);
                    }
                },
                searching: this.searching,
                lengthChange: this.lengthChange,
                info: this.info,
                language: { "emptyTable": this.emptyTable }

            });
        if (this.isServerSide) {
            $(`#${this.htmlid}`).on('preXhr.dt', (e, settings, data) => {
                this.loading = true;
                if (this.preLoadFunction)
                    this.preLoadFunction(e, settings, data);
            });

            $(`#${this.htmlid}`).on('xhr.dt', (e, settings, xhr) => {
                this.loading = false;
                if (this.postLoadFunction)
                    this.postLoadFunction(e, settings, xhr);
            });
        }

        if (this.isSuppressDefaultErrors)
            $.fn.dataTable.ext.errMode = 'none';

        if (self.isServerSide) {
            $(`#${self.htmlid}_filter input`).unbind();
            $(`#${self.htmlid}_filter input`).keyup((e) => {
                self.search = $(`#${self.htmlid}_filter input`).val();
                self.searchChange.emit(self.search);
                if (e.which === 13) {
                    // 13 is the 'enter' key.
                    if (self.searchFunction)
                        self.searchFunction($(`#${self.htmlid}_filter input`).val());
                }
            });
        }

        if (this.grouping && this.groupingFunction) {

            // If we have grouping, we need to hide/show the groups on clicks.
            let self = this;
            $(`#${this.htmlid} tbody`).on('click', 'tr.group', function () {
                self.toggleRowGroup(this);
            });

            // Also we want to handle custom sorting.
            $(`#${this.htmlid} th`).click(function () {
                let index = $(this).index();

                let order = self.currentSortColumn[1] == 'desc'
                    ? 'asc'
                    : 'desc';
                self.currentSortColumn = [index, order];
                self.sortGrouping();

                $(this).removeClass('sorting');
            });

            this.sortGrouping();
        }

        $(`#${this.htmlid} tbody`).on('click', 'tr.datarow', function () {
            self.clickedRow.emit({ data: self.table.row(this).data(), table: self.table });
        });
        $(`#${this.htmlid} tbody`).on('click', 'tr.datarow td', function () {
            self.clickedColumn.emit({ data: self.table.row($(this).parent()).data(), table: self.table, column: this });
        });


        $(`#${self.htmlid}_wrapper`).removeClass('form-inline');//causes issues in bs4
        $('.dataTables_paginate').addClass('pull-right');

        $('.pagination').find('li').each(function () {
            $(this).addClass('page-item');
            $(this).find('a').each(function () {
                $(this).addClass('page-link');
            })
        })

        this.unifytable();

    }

    unifytable() {
        var flabel = $('.dataTables_info');
        flabel.html($('.dataTables_info').text().replace("to", "-").replace("entries", "").replace("Showing", ""));

        flabel.remove();
        flabel.addClass('labelInfo');
        $('.dataTables_length').parent().prepend('<div class="dataTables_spacer">&nbsp;</div>');
        $('.dataTables_length').parent().prepend(flabel);
        $('.dataTables_length').parent().removeClass('col-sm-6');
        $('.dataTables_length').parent().addClass('col-sm-12 black');

        var llabel = $('.dataTables_length').find('label');
        llabel.contents()[0].nodeValue = 'View: ';
        $(llabel.contents()[1]).addClass('custom-select custom-select-sm form-control form-control-sm');
        llabel.contents()[2].nodeValue = '';
        var dtFilter = $('.dataTables_filter');
        var lblFilter = dtFilter.find('label');
        lblFilter.contents()[0].nodeValue = ' ';
        $(lblFilter.contents()[1]).attr('placeholder', 'Search...');
        $(lblFilter.contents()[1]).addClass('form-control form-control-sm');

        dtFilter.addClass("pull-left");
        dtFilter.parent().removeClass('col-sm-6');
        //dtFilter.remove();

        $('.dataTables_length').addClass('pull-left');
        $('.dataTables_length').parent().append(dtFilter);

        $('.dataTables_paginate').parent().parent().find('.col-sm-5').remove();
        $('.dataTables_paginate').parent().removeClass('col-sm-7');
        $('.dataTables_paginate').parent().addClass('col-sm-12');
        $('.dataTables_paginate').removeClass('pull-right')
        $('.dataTables_paginate').addClass('d-flex');
        $('.dataTables_paginate').addClass('justify-content-center');
        $('.previous').find('a').html('‹');
        $('.next').find('a').html('›');

        $('.dataTables_length').parent().append('<div class="dt-buttons d-flex justify-content-end"><button id=' + `btn_${this.htmlid}` + ' class="dt-button btn btn-primary btn-sm" type="button"><span><i class="fas fa-plus-circle"></i> ' + `${this.addNewRowText}` + '</span></button> </div>');

    }
    sortGrouping() {
        // Grab the grouping from the dropdown.
        let groupsorting = [this.grouping.columnIndex, 'asc'];

        // Grab the current sorting on the grid. This will be in format [0, "asc"];
        let gridsort = this.currentSortColumn;

        // Do the sort!
        this.table.order(groupsorting, gridsort);
        this.table.draw();

        // Adds the dimmed sort icon to the headers unless the column is the currently the one sorted as
        $(`#${this.htmlid} > thead > tr > th`)
            .not('.sorting_asc')
            .not('.sorting_desc')
            .addClass('sorting');

        $(`#${this.htmlid} > thead > tr > th`).each(function () {
            if ($(this).hasClass("sorting_asc") && $(this).hasClass("sorting") ||
                $(this).hasClass("sorting_desc") && $(this).hasClass("sorting")) {
                $(this).removeClass("sorting");
            }
        })

    }

    toggleRowGroup(tr: any) {
        let head = $(tr);
        let icon = $(head).find('.fa');
        let newicon = icon.hasClass('fa-plus')
            ? 'fa-minus'
            : 'fa-plus';
        icon.removeClass('fa-plus')
            .removeClass('fa-minus')
            .addClass(newicon);

        let rows = $(head).nextUntil('.group', 'tr[role="row"]');
        if (newicon === 'fa-plus')
            rows.hide();
        else
            rows.show();
    }

    setColumnVisibility(name: string, isvisible: boolean) {
        if (!this.table) {
            console.log(`DataTable: cannot set visibility before table has been initialized.`);
            return;
        }
        console.log(`DataTable: setting column '${name}' to visiblity '${isvisible}'.`);
        const index = this.table.column(`${name}:name`);
        this.table.column(index).visible(isvisible);
    }

    redraw(changes: any = null) {

        if (this.table && this.data) {
            console.log("DataTable: changed detected. Redrawing...");
            this.table.clear();
            this.table.rows.add(this.data);
            this.table.draw();
        }
        else {
            console.log("DataTable: changed detected, but no data set. Ignoring...");
        }
    }

    refresh() {
        this.table.ajax.reload();
    }

    destroy(destroyDom: boolean) {
        this.table.destroy(false);
    }
}