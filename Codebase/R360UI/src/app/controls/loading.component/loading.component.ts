import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import stickybits from 'stickybits';
@Component({
    moduleId: module.id.toString(),
    selector: 'loading',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {
    @Input() size: number = 100;
    @Input() static: boolean = false;
    constructor() {
    }

    ngOnInit() {
    }
}