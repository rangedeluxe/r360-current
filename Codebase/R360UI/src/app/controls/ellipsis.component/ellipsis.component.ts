import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Input, ViewChild, ElementRef, SimpleChange } from '@angular/core';

/*
    Why is this written as a jquery plugin?
     - Dev was completed by some internet-goer on short notice.
     - JQuery actually does a good job calculating widths.
     - We should convert it at some point.
*/
declare let $: any;
declare let window: any;

@Component({
    moduleId: module.id.toString(),
    selector: 'ellipsis',
    templateUrl: './ellipsis.component.html',
    styleUrls: ['./ellipsis.component.css']
})
export class EllipsisComponent implements OnInit, AfterViewInit {
    @ViewChild('ellipsis', { static: false }) element: ElementRef;
    @Input() text: string;
    @Input() type: string = "left";
    @Input() class: string = '';
    public innerClass: string = "left-ellipsis";
    public spanClass: string = '';

    constructor() {

    }

    ngOnInit() {
    }

    ngOnChanges(changes: any) {
        let change = changes.class as SimpleChange;
        if (this.element !== undefined) {
            try {
                let $element = $(this.element.nativeElement);
                let $span = $element.find('span');
                if (change.currentValue)
                    $span.addClass(change.currentValue);
                else
                    $span.removeClass(change.previousValue);
            } catch (e) { }
        }
    }

    ngOnDestroy() {
        // Note - this removes size watching for all ellipsis, not just 'this' one.
        console.log("Ellipsis: Closing event handlers.")
        $(window).off("resize");
    }

    ngAfterViewInit() {
        this.calculateEllipsis();
        $(window).resize(() => this.calculateEllipsis());
    }

    calculateEllipsis() {
        let $element = $(this.element.nativeElement);
        let $span = $element.find('span.ellipsistext');
        $element.find('.fulltext').html(this.text);
        $span.text(this.text);
        $span.removeClass('ellipsis');

        if ($span.width() > $element.width()) {
            $span.addClass('text-right-pad');
            /*while ($span.width() > $element.width()) {
                $span.text($span.text().substr(1));
            }*/
        }

    }

}