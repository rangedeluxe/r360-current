import { Component, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { UsersService } from "../../services/users.service/users.service";
import { AlertsService } from "../../services/alerts.service/alerts.service";
import { User } from "../../DTOs/User";
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { DataTablePagerComponent } from '@swimlane/ngx-datatable';

@Component({
    moduleId: module.id.toString(),
    selector: 'entityuserselector',
    templateUrl: 'entityuserselector.component.html',
    styleUrls: ['entityuserselector.component.css'],
    providers: [UsersService, AlertsService]
})

export class EntityUserSelector {
    private userSerivce: UsersService;
    public entityUsers: User[] = new Array<User>();
    @Input() public selectedUsers: User[] = new Array<User>();
    @Input() public htmlid: string;
    @ViewChild("availUsersNgxPaginator", {static: false}) availPaginator: DataTablePagerComponent;
    @Output() public selectedUsersChange = new EventEmitter<Array<User>>();

    private userColumns: any[];
    private entityUsercolumns: any[];
    private toastService: ToasterService;
    public loading: boolean = false;
    public selectedEntity: any = { Id: 0, Name: "" };
    entityListUsercolumns = [{ name: "Name", prop: "Name", width: "285" }];
    selectedListUserColumns = [{ name: "Name", prop: "Name" }, { name: "Entity", prop: "Entity" }];

    currentlySelected = [];
    availableSelected = [];
    constructor(service: UsersService, toast: ToasterService) {
        this.userSerivce = service;
        this.toastService = toast;
    }

    clickedEntity(entityId: number) {
        console.log("Display users for entityId:" + entityId);
        this.loading = true;
        this.availPaginator.selectPage(1);
        this.userSerivce.getUsersByEntity(entityId).
            subscribe(
                result => {
                    //[{id:'guid', label:"SomeEntity", items:[{id:'guid', label:"Billy West"}]}]
                    this.loading = false;
                    this.selectedEntity = { Id: result.body[0].id, Name: result.body[0].label };
                    let self = this;
                    //maps to a user object and filters out already selected and nodes that aren't users
                    if (result && result.body[0]) {
                        this.entityUsers = result.body[0].items.filter(user => {
                            let index = self.indexOfUser(self.selectedUsers, { Id: user.id })
                            return index == -1 && user.isNode
                        })
                        .map((user) => {
                            return { Id: user.id, Name: user.label, Entity: result.body[0].label, EntityId: result.body[0].id }
                        });
                        return;
                    }
                    this.toastService.pop('error', 'Error', 'An error occurred while retrieving users.');
                },
                error => {
                    this.loading = false;
                    this.toastService.pop('error', 'Error', 'An error occurred while retrieving users.');
                }
            );
    }

    addSelectedUsers() {
        if (this.availableSelected && this.availableSelected.length > 0) {
            let entityAry = new Array<User>();
            let selectedAry = new Array<User>();
            entityAry = this.entityUsers.filter((user) => {
                return this.indexOfUser(this.availableSelected, user) == -1;
            });
            this.selectedUsers = this.selectedUsers != null ? this.selectedUsers : new Array<User>();
            selectedAry = this.availableSelected.concat(this.selectedUsers);
            this.entityUsers = entityAry;
            this.selectedUsers = selectedAry;
            this.selectedUsersChange.emit(this.selectedUsers);
            this.availableSelected = [];
        }
    }

    removeSelectedUsers() {

        if (this.currentlySelected && this.currentlySelected.length > 0) {
            let selectedAry = this.selectedUsers.
                filter(user => { return this.indexOfUser(this.currentlySelected, user) == -1 });
            let entityAry = this.currentlySelected.filter((user) => { return user.EntityId == this.selectedEntity.Id });
            entityAry = entityAry.concat(this.entityUsers);
            this.entityUsers = entityAry;
            this.selectedUsers = selectedAry;
            this.selectedUsersChange.emit(this.selectedUsers);
            this.currentlySelected = [];
        }
    }
    getSelectedUsers() {
        return this.selectedUsers.map(user => { return user.Id });
    }
    private indexOfUser(arr, user) {
        if (arr == null || arr.length <= 0) return -1;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].Id == user.Id) return i;
        }
        return -1;
    }


}