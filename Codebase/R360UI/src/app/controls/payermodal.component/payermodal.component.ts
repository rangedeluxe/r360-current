import { Component, Input, ViewChild, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Operation } from '../../DTOs/Operation';
import { Payer } from '../../DTOs/Payer';
import { PayerService } from '../../services/payer.service/payer.service';

@Component({
  moduleId: module.id.toString(),
  selector: 'payer-modal',
  templateUrl: 'payermodal.component.html',
  styleUrls: ['payermodal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PayerModal {

  closeResult: string;
  private modal: NgbModalRef;
  public currentOperation: Operation;
  public payer: Payer;
  public editMode: boolean = true;
  public modeTitle: string = '';
  public loading: boolean;
  isValidPayerName: boolean = true;
  isValidAcct: boolean = true;
  isValidRN: boolean = true;

  @Input() bankDetail: string = '';
  @Input() workGroupDetail: string = '';
  @Output() onClose: EventEmitter<any> = new EventEmitter();
  @Output() onAdd: EventEmitter<any> = new EventEmitter();
  @Output() onUpdate: EventEmitter<any> = new EventEmitter();
  @ViewChild('content', { static: false }) childHtml: any;

  constructor(private modalService: NgbModal,
    private payerService: PayerService) {
  }


  ngOnInit() { }

  open() {
    this.loading = false;
    this.isValidRN = true;
    this.isValidAcct = true;
    this.isValidPayerName = true;

    this.editMode = (this.currentOperation == Operation.Update);
    if (this.editMode)
      this.modeTitle = 'Edit Payer'
    else
      this.modeTitle = 'Add Payer';
    this.modal = this.modalService.open(this.childHtml, { windowClass: 'modal-frame' });
  }

  private addPayer() {

    this.isValidAcct = true;
    this.isValidPayerName = true;
    this.isValidRN = true;

    if (this.payer.account.trim() === "")
      this.isValidAcct = false;
    if (this.payer.payerName.trim() === "")
      this.isValidPayerName = false;
    if (this.payer.routingNumber.trim() === "")
      this.isValidRN = false;

    if (this.isValidAcct === false || this.isValidPayerName === false || this.isValidRN === false)
      return;

    this.payerService.updatePayer(this.payer).subscribe(
      response => {
        this.loading = false;
        this.onAdd.emit(response.success);
        if (this.modal)
          this.modal.close();
      });
  }

  private editPayer() {
    this.isValidPayerName = true;
    if (this.payer.payerName.trim() === "") {
      this.isValidPayerName = false;
      return;
    }
    this.payerService.updatePayer(this.payer).subscribe(
      response => {
        this.loading = false;
        this.onUpdate.emit(response.success);
        if (this.modal)
          this.modal.close();
      });
  }

  close(): void {
    if (this.modal)
      this.modal.close();
    this.onClose.emit();
  }

  save(): void {
    this.loading = true;
    if (this.editMode == true) {
      this.editPayer();
    }
    else {
      this.addPayer();
    }
  }
}