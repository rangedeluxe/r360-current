import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
    moduleId: module.id.toString(),
    selector: 'error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
    @Input() message: string;

    constructor() {
        
    }

    ngOnInit() {
       // Default message.
       this.message = this.message || "An unexpected error occurred.";
    }
}