import { Component } from '@angular/core';
import { Input } from '@angular/core';

@Component({
    moduleId: module.id.toString(),
    selector: 'numericwidget',
    templateUrl: './numericwidget.component.html',
    styleUrls: ['./numericwidget.component.css']
})
export class NumericWidgetComponent {

    @Input() public title:string = "";

    // data that would come from a REST call.
    @Input() public data:number = 0;

    constructor() {
        
    }

}