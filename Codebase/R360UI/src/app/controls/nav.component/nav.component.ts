
import {timer as observableTimer, forkJoin as observableForkJoin,  Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { FrameworkService } from '../../services/framework.service/framework.service';
import { LoadingComponent } from '../loading.component/loading.component';
import { Globals } from '../../globals';


import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '../../services/localstorage.service/localstorage.service';
import { ToasterService } from 'angular2-toaster';
import { BreadcrumbService } from '../../services/breadcrumb.service/breadcrumb.service';

declare var window;

@Component({
   moduleId: module.id.toString(),
   selector: 'framework-nav',
   templateUrl: './nav.component.html',
   providers: [FrameworkService, LocalStorageService],
   styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
   private PING_INTERVAL: number = 60000;
   private IDLE_TIMEOUT: number = 60;
   public tabs: any;
   public user: any;
   public branding: any;

   public error: boolean;
   public finishedLoading: boolean;

   private frameworkService: FrameworkService;
   private timer;
   private idle: Idle;
   private router: Router;
   private toastservice: ToasterService;
   private breadcrumbService: BreadcrumbService;
   constructor(
      frameworkService: FrameworkService,
      idle: Idle,
      router: Router,
      toast: ToasterService,
      breadcrumb: BreadcrumbService) {
      this.idle = idle;
      this.frameworkService = frameworkService;
      this.error = false;
      this.finishedLoading = false;
      this.router = router;
      this.toastservice = toast;
      this.breadcrumbService = breadcrumb;
   }

   ngOnInit() {
      this.initPing();
      // Get the tabs and the user information.
      observableForkJoin([
         this.frameworkService.getTabs(),
         this.frameworkService.getUser(),
         this.frameworkService.getSessionInfo(),
         this.frameworkService.getBranding()
      ]
      ).subscribe(
         result => {
            this.tabs = result[0];
            this.user = result[1];
            this.finishedLoading = true;
            this.initTimeout((<any>result[2]).IdleTimeout * 60);
            this.branding = result[3];
         },
         error => {
            console.log('Nav: Error getting framework & session information.');
            this.error = true;
            this.finishedLoading = true;
         }
      );
   }

   private initPing() {
      console.log(`Session: Initializing ping timer to ${this.PING_INTERVAL / 1000} seconds.`);
      // Starts up the ping timer.
      this.timer = observableTimer(0, this.PING_INTERVAL);
      this.timer.subscribe(t => {
         this.ping();
      });
   }

   private ping() {
      this.frameworkService.ping()
         .subscribe(result => {
            console.log('Session: Ping.');
         }, error => {
            console.log('Session: Ping failed.');
         });
   }

   public logOut() {
      // Send the user to the logout page with the current URL so we can log in back to it.
      // window.location.href = `${Globals.LOGOUT_URL}?returnUrl=${encodeURIComponent(this.router.url)}`;
      window.location.href = `${Globals.LOGOUT_URL}`;
   }

   private initTimeout(timeout: number) {
      console.log(`Session: Initializing session timeout to ${timeout} seconds.`);
      // We become idle when the Raam's setting expires.
      this.idle.setIdle(timeout);
      // We timeout when we're idle after timeout seconds.
      this.idle.setTimeout(this.IDLE_TIMEOUT);
      // Sets the default interrupts, in this case, things like clicks, scrolls, touches to the document.
      this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

      this.idle.onIdleEnd.subscribe(() => {
         console.log('Session: No longer idle.');
      });
      this.idle.onTimeout.subscribe(() => {
         console.log('Session: Timed out.');
         this.logOut();
      });
      this.idle.onIdleStart.subscribe(() => {
         console.log('Session: Idle status started.');
      });
      this.idle.onTimeoutWarning.subscribe((countdown) => {
         console.log(`Session: Timeout in ${countdown} seconds.`);
      });
      this.idle.watch();
   }

   public customMenuClick() {
      if (!this.branding.CustomMenuUrl.toLowerCase().startsWith('http') && !this.branding.CustomMenuUrl.startsWith('//')) {
         this.branding.CustomMenuUrl = '//' + this.branding.CustomMenuUrl;
      }
      console.log(`Nav: Opened window for url ${this.branding.CustomMenuUrl} seconds.`);
      window.open(this.branding.CustomMenuUrl, '_blank');
   }

   public sendToSso(tabId: number) {
      this.frameworkService.sendToSso(tabId)
         .subscribe((result: any) => {
            const form = document.createElement('form');
            form.action = result.Data.PostToUrl;
            form.method = 'POST';
            //form.target = result.Data.IsRedirect === true ? '' : '_blank';
            form.target = '_blank';

            const input = document.createElement('input');
            input.name = result.Data.PostFieldName;
            input.value = result.Data.JwtToken;
            form.appendChild(input);
            form.style.display = 'none';
            document.body.appendChild(form);
            form.submit();
         }, error => {
            this.toastservice.pop('error', 'error', 'Error connecting.  Please contact your administrator.');
            console.error(error);
            console.log('Error connecting.  Please contact your administrator.');
         }
         );
   }

   /**
    * @param url URL the link is attempting to go to
    * @param external Whether the url is external to the angular app
    * @param clearBreadcrumbs Whether to clear the current accumulated breadcrumbs
    * @param param any extra params for the url
    * @param resourceName an application wide resource name for the url being attempted to navigate to
    */
   public navigateTo(url: string, external: boolean, clearBreadcrumbs: boolean, param = '',
                     resourceName: string = '', tab: any) {
      if (clearBreadcrumbs) {
         this.clearBreadcrumbs();
      }
      // First, special cases:
      // we open a new tab for RPSwebexceptions.
      // and for links that are not displayed inline
      const r360external = tab && tab.IsLink && !tab.DisplayInline;
      if (resourceName === 'RPSWebExceptions' || r360external){
         window.open(url);
         return;
      }
      if (external){
         const fullurl = url + param;
         window.location.href = fullurl;
      }
      else{
         this.router.navigateByUrl(url);
      }
   }

   public clearBreadcrumbs() {
      this.breadcrumbService.clear();
   }
}