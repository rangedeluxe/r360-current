import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'headerimage',
  templateUrl: './headerimage.component.html',
  styleUrls: ['./headerimage.component.css']
})
export class HeaderimageComponent implements OnInit {
  private _branding: any;
  @Input() 
  set branding(value:any){
    this._branding = value;
    this.backgroundimage = this._branding.ShowBackgroundImage 
      ? 'url(/Framework/Framework/HeaderBackgroundImage)' 
      : '';
  }
  get branding(){
    return this._branding;
  }
  public backgroundimage: string = '';
  constructor() { }

  ngOnInit() {
  }

}
