import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../services/breadcrumb.service/breadcrumb.service';
import { Breadcrumb } from '../../DTOs/Breadcrumb';

let self;

@Component({
   moduleId: module.id.toString(),
   selector: 'breadcrumb',
   templateUrl: 'breadcrumb.component.html',
   styleUrls: ['breadcrumb.component.css'],
   providers: [BreadcrumbService],
})


export class BreadcrumbComponent implements OnInit{

   private breadcrumbService: BreadcrumbService;
   public crumbs: Array<Breadcrumb>;

   constructor(breadcrumbService: BreadcrumbService) {
      self = this;
      this.breadcrumbService = breadcrumbService;
   }

   ngOnInit(){
      this.crumbs = this.breadcrumbService.getBreadcrumbs().reverse();
   }

   public load(name: string){
      this.breadcrumbService.loadBreadcrumb(name);
  }

  private clear(){
     this.breadcrumbService.clear();
  }
}
