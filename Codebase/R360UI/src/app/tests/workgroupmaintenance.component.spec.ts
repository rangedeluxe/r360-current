import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';



import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { WorkgroupMaintenanceMockService } from '../services/workgroupmaintenance.service/workgroupmaintenance.service.mock';
import { WorkgroupMaintenanceComponent } from '../modules/workgroupmaintenance.component/workgroupmaintenance.component';
import { IWorkgroupMaintenanceService } from '../services/workgroupmaintenance.service/iworkgroupmaintenance-service';
import { WorkgroupMaintenanceService } from '../services/workgroupmaintenance.service/workgroupmaintenance.service';
import { Workgroup } from '../DTOs/Workgroup';
import { UnifyDatatableComponent } from '../controls/unifydatatable.component/unifydatatable.component';
import { LoadingComponent } from '../controls/loading.component/loading.component';
import { WorkgroupSelectorComponent } from '../controls/workgroupselector.component/workgroupselector.component';
import { ToasterService } from 'angular2-toaster';
import { PermissionService } from '../services/permission.service/permission.service';
import { MockToasterService } from '../services/toaster.service/toaster.service.mock';
import { MockPermissionService } from '../services/permission.service/permission.service.mock';

describe('WorkgroupMaintenance Component', () => {
  let mockService: WorkgroupMaintenanceMockService = new WorkgroupMaintenanceMockService();
  let toasterService: MockToasterService = new MockToasterService();
  let fixture: ComponentFixture<WorkgroupMaintenanceComponent>;
  let component: WorkgroupMaintenanceComponent;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WorkgroupMaintenanceComponent, UnifyDatatableComponent, LoadingComponent, WorkgroupSelectorComponent],
      providers: [
        { provide: WorkgroupMaintenanceService, useClass: WorkgroupMaintenanceMockService },
        { provide: ToasterService, useClass: MockToasterService }
      ],
      imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(WorkgroupMaintenanceComponent);
    component = fixture.componentInstance;
    mockService = TestBed.get(WorkgroupMaintenanceService);
    toasterService = TestBed.get(ToasterService);

    fixture.detectChanges();
    fixture.debugElement.nativeElement.style.visibility = "hidden"; //hides component from karma results
  });

  it('should verify that page is created', () => {
    expect(component).toBeTruthy();
  });

  it('should validate default setttings api, check that data exists', () => {
    let w1: Workgroup = new Workgroup();
    w1.EntityId = 1;
    component.selectedWorkgroup = w1;
    component.getDefaultSettings();
    expect(component.paymentImageDisplayMode).toBe("Use System Setting");
    expect(component.documentImageDisplayMode).toBe("Front and Back");
    expect(component.displayBatchId).toBe("No");
    expect(component.viewingDays).toBe("300");
    expect(component.maxSearchDays).toBe("96");
  });

  it('should validate workgroup setttings api, check that data exists', () => {
    let w1: Workgroup = new Workgroup();
    let w2: Workgroup = new Workgroup();
    w1.EntityId = 1;
    w2.EntityId = 2;
    component.selectedWorkgroup = w1;
    component.getWorkgroupSettings();
    expect(component.workgroupSettingsData.length).toBe(50);

    component.selectedWorkgroup = w2;
    component.getWorkgroupSettings();
    expect(component.workgroupSettingsData.length).toBe(200);

  });

});
