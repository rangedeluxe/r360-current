// Globals - needed in every spec.
import 'zone.js';
import 'reflect-metadata';

// Imports for this test spec.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DatePickerComponent } from '../controls/datepicker.component/datepicker.component';
import { MockDateService } from "../services/date.service/date.service.mock";
import { MockToasterService } from "../services/toaster.service/toaster.service.mock";

// Unit test suite for the dashboard component.
describe('date picker tests', () => {
    let component: DatePickerComponent;
    let fixture: ComponentFixture<DatePickerComponent>;
    let dateservice: MockDateService;
    let toaster: MockToasterService;

    // Suite setup code
    beforeEach(() => {
        dateservice = new MockDateService();
        toaster = new MockToasterService();
        component = new DatePickerComponent(dateservice, toaster);
        dateservice.date = new Date('10/10/2000');
    });

    // Now the tests themselves.
    it('datepicker_shouldallow_format_mm/dd/yyyy', () => {
        let format = component.formatDate('10/10/2017');
        expect(format.DateString).toEqual('10/10/2017');
    });

    it('datepicker_shouldallow_format_mm/dd/yy', () => {
        let format = component.formatDate('10/10/17');
        expect(format.DateString).toEqual('10/10/2017');
    });

    it('datepicker_shouldallow_format_m/dd/yy', () => {
        let format = component.formatDate('1/10/17');
        expect(format.DateString).toEqual('1/10/2017');
    });

    it('datepicker_shouldallow_format_mm/d/yy', () => {
        let format = component.formatDate('10/1/17');
        expect(format.DateString).toEqual('10/1/2017');
    });

    it('datepicker_shouldallow_format_m/d/yy', () => {
        let format = component.formatDate('1/1/17');
        expect(format.DateString).toEqual('1/1/2017');
    });

    it('datepicker_shouldallow_format_mmddyyyy', () => {
        let format = component.formatDate('10102017');
        expect(format.DateString).toEqual('10/10/2017');
    });

    it('datepicker_shouldallow_format_mmddyy', () => {
        let format = component.formatDate('101017');
        expect(format.DateString).toEqual('10/10/2017');
    });

    it('datepicker_shouldnotallow_format_mddyy', () => {
        let format = component.formatDate('11017');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_asdf', () => {
        let format = component.formatDate('asdf');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_$%^&', () => {
        let format = component.formatDate('$%^&');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_Aa***', () => {
        let format = component.formatDate('Aa***');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_999999', () => {
        let format = component.formatDate('999999');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_9999999', () => {
        let format = component.formatDate('9999999');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_99999999', () => {
        let format = component.formatDate('99999999');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_000000', () => {
        let format = component.formatDate('000000');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_0000000', () => {
        let format = component.formatDate('0000000');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_00000000', () => {
        let format = component.formatDate('00000000');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_222222', () => {
        let format = component.formatDate('222222');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_2222222', () => {
        let format = component.formatDate('2222222');
        expect(format.DateString).toEqual('10/10/2000');
    });

    it('datepicker_shouldnotallow_22222222', () => {
        let format = component.formatDate('22222222');
        expect(format.DateString).toEqual('10/10/2000');
    });

});