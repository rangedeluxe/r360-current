// Globals - needed in every spec.
import 'zone.js';
import 'reflect-metadata';

// Imports for this test spec.
import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing/'
import { By } from '@angular/platform-browser';
import { UserPreferencesComponent } from '../modules/userpreferences.component/userpreferences.component';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { UserPreferencesMockService } from '../services/userpreferences.service/userpreferences.service.mock';
import { UserPreferencesService } from '../services/userpreferences.service/userpreferences.service';
import { post } from 'selenium-webdriver/http';
import { of } from 'rxjs';

describe('user preferences tests', () => {
    let component: UserPreferencesComponent;
    let fixture: ComponentFixture<UserPreferencesComponent>;
    let toastservice: ToasterService = new ToasterService();
    let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy };
    let userpreferences : UserPreferencesService;
    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        userpreferences = new UserPreferencesService(<any>httpClientSpy);
        component = new UserPreferencesComponent(userpreferences, toastservice);
        component.ngOnInit();
    });

    it('userpreferences loads payername after restore', () => {
        const expectedDefaults = { 'displayRemitterNameInPDF': this.showPayer, 'displayRemitterAccesible': true };

        httpClientSpy.post.and.returnValue(of(expectedDefaults));
        userpreferences.restoreDefaults();
        component.refresh();
        expect(component.displayPayerName).toEqual(false);
    });

    it('userpreferences loads payername after submit', () => {
        const expectedDefaults = { 'displayRemitterNameInPDF': this.showPayer, 'displayRemitterAccesible': true };
        const retSave = { 'HasErrors': false };
        httpClientSpy.post.and.returnValues(of(expectedDefaults), of(retSave));
        userpreferences.restoreDefaults();
        userpreferences.savePreferences({
            displayRemitterNameInPDF: true
        });
        component.refresh();
        expect(component.displayPayerName).toEqual(true);
    });

});