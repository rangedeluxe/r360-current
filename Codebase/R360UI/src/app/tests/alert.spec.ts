// Globals - needed in every spec.
import 'zone.js';
import 'reflect-metadata';

import { Alert } from "../DTOs/Alert";
import { Workgroup } from "../DTOs/Workgroup";

describe('alert tests', () => {
    let alert: Alert;

    beforeEach(() => {
        alert = new Alert();
    });

    it('a fresh alert should be pristine', () => {
        expect(alert.Pristine).toBeTruthy();
    });

    it('null and empty description is not valid', () => {
        alert.Description = '';
        expect(alert.isDescriptionValid()).toBeFalsy();
        alert.Description = null;
        expect(alert.isDescriptionValid()).toBeFalsy();
        alert.Description = undefined;
        expect(alert.isDescriptionValid()).toBeFalsy();
    });

    it('non-empty description is valid', () => {
        alert.Description = '1';
        expect(alert.isDescriptionValid()).toBeTruthy();
    });

    it('null and empty event value is not valid', () => {
        alert.EventValue = '';
        expect(alert.isEventValueValid()).toBeFalsy();
        alert.EventValue = null;
        expect(alert.isEventValueValid()).toBeFalsy();
        alert.EventValue = undefined;
        expect(alert.isEventValueValid()).toBeFalsy();
    });

    it('non-empty event value is valid', () => {
        alert.EventValue = '1';
        expect(alert.isEventValueValid()).toBeTruthy();
    });

    it('non-numeric event values are not valid', () => {
        alert.EventValue = 'a';
        expect(alert.isEventValueNumeric()).toBeFalsy();
        alert.EventValue = '10a';
        expect(alert.isEventValueNumeric()).toBeFalsy();
        alert.EventValue = '10.0a';
        expect(alert.isEventValueNumeric()).toBeFalsy();
        alert.EventValue = '.99a';
        expect(alert.isEventValueNumeric()).toBeFalsy();
        alert.EventValue = 'a99';
        expect(alert.isEventValueNumeric()).toBeFalsy();
        alert.EventValue = 'a99.99';
        expect(alert.isEventValueNumeric()).toBeFalsy();
        alert.EventValue = '99a99';
        expect(alert.isEventValueNumeric()).toBeFalsy();
    });

    it('numeric event values are valid', () => {
        alert.EventValue = '1';
        expect(alert.isEventValueNumeric()).toBeTruthy();
        alert.EventValue = '12';
        expect(alert.isEventValueNumeric()).toBeTruthy();
        alert.EventValue = '.12';
        expect(alert.isEventValueNumeric()).toBeTruthy();
        alert.EventValue = '12.01';
        expect(alert.isEventValueNumeric()).toBeTruthy();
        alert.EventValue = '9999.9999';
        expect(alert.isEventValueNumeric()).toBeTruthy();
    });

    it('invalid operator is not valid', () => {
        alert.Operator = null;
        expect(alert.isOperatorValid()).toBeFalsy();
        alert.Operator = { id:0 };
        expect(alert.isOperatorValid()).toBeFalsy();
    });

    it('valid operator is valid', () => {
        alert.Operator = { id: 1 };
        expect(alert.isOperatorValid()).toBeTruthy();
    });

    it('invalid workgroup is not valid', () => {
        alert.Workgroup = null;
        expect(alert.isWorkgroupValid()).toBeFalsy();
        alert.Workgroup = new Workgroup();
        expect(alert.isWorkgroupValid()).toBeFalsy();
    });

    it('valid workgroup is valid', () => {
        alert.Workgroup = new Workgroup();
        alert.Workgroup.BankId = 1;
        alert.Workgroup.WorkgroupId = 1;
        expect(alert.isWorkgroupValid()).toBeTruthy();
    });

    it('event is valid for id 1', () => {
        alert.Description = '1';
        alert.Workgroup = new Workgroup();
        alert.Workgroup.BankId = 1;
        alert.Workgroup.WorkgroupId = 1;
        expect(alert.isEventValid("1")).toBeTruthy();
    });

    it('event is valid for id 2', () => {
        alert.Description = '1';
        expect(alert.isEventValid("2")).toBeTruthy();
    });

    it('event is valid for id 3', () => {
        alert.Description = '1';
        alert.Workgroup = new Workgroup();
        alert.Workgroup.BankId = 1;
        alert.Workgroup.WorkgroupId = 1;
        alert.Operator = { id:1 };
        alert.EventValue = "1";
        expect(alert.isEventValid("3")).toBeTruthy();
    });

    it('event is valid for id 4', () => {
        alert.Description = '1';
        alert.Workgroup = new Workgroup();
        alert.Workgroup.BankId = 1;
        alert.Workgroup.WorkgroupId = 1;
        alert.Operator = { id:1 };
        alert.EventValue = "a";
        expect(alert.isEventValid("4")).toBeTruthy();
    });
});