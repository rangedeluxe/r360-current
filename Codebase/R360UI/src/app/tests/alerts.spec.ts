// Globals - needed in every spec.
import 'zone.js';
import 'reflect-metadata';

// Imports for this test spec.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MockToasterService } from "../services/toaster.service/toaster.service.mock";
import { AlertsComponent } from "../modules/alerts.component/alerts.component";
import { MockAlertsService } from "../services/alerts.service/alerts.service.mock";
import { MockPermissionService } from '../services/permission.service/permission.service.mock';

// Unit test suite for the dashboard component.
describe('alert tests', () => {
    let component: AlertsComponent;
    let fixture: ComponentFixture<AlertsComponent>;
    let toaster: MockToasterService;
    let alertservice: MockAlertsService;
    let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy };
    // Suite setup code
    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        toaster = new MockToasterService();
        alertservice = new MockAlertsService(null);
        component = new AlertsComponent(alertservice, toaster, new MockPermissionService(<any>httpClientSpy));
        component.ngOnInit();
    });

    // Now the tests themselves.
    it('should display data', () => {
        component.refreshPage();
        expect(component.data.length).toBeGreaterThan(0);
    });
});