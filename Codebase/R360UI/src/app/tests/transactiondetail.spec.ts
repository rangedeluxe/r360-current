// Globals - needed in every spec.
import 'zone.js';
import 'reflect-metadata';
import { element, by } from 'protractor';

// Imports for this test spec.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MockLocalStorageService } from '../services/localstorage.service/localstorage.service.mock';
import { MockToasterService } from '../services/toaster.service/toaster.service.mock';
import { TransactionDetailComponent } from '../modules/transactiondetail.component/transactiondetail.component';

// Unit test suite for the batchsummary component.
describe('transaction detail tests', () => {
    let batchSummaryComponent: TransactionDetailComponent;

    // Suite setup code
    beforeEach(() => {
        // todo.
    });

});