// Globals - needed in every spec.
import 'zone.js';
import 'reflect-metadata';
import { element, by } from 'protractor';

// Imports for this test spec.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MockLocalStorageService } from '../services/localstorage.service/localstorage.service.mock';
import { MockToasterService } from '../services/toaster.service/toaster.service.mock';
import { BatchSummaryComponent } from '../modules/batchsummary.component/batchsummary.component';
import { MockBatchSummaryService } from '../services/batchsummary.service/batchsummary.service.mock';
import { elementAt } from 'rxjs/operator/elementAt';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { BreadcrumbService } from '../services/breadcrumb.service/breadcrumb.service';
import { ToCurrencyPipe } from '../pipes/tocurrency.pipe';
import { SharedStorageService } from '../services/sharedstorage.service/sharedstorage.service';

// Unit test suite for the batchsummary component.
describe('batch summary tests', () => {
    let batchSummaryComponent: BatchSummaryComponent;
    let batchSummaryService: MockBatchSummaryService;
    let toaster: MockToasterService;
    let localStorageService: MockLocalStorageService;
    let fixture: ComponentFixture<BatchSummaryComponent>;
    let breadrumbService: BreadcrumbService;
    let router: Router;
    let currencyPipe: ToCurrencyPipe;
    let sharedServ: SharedStorageService;
    // Suite setup code
    beforeEach(() => {
        currencyPipe = new ToCurrencyPipe();
        var store = {};
        spyOn(localStorage, 'getItem').and.callFake(function (key) {
          return store[key];
        });
        spyOn(localStorage, 'setItem').and.callFake(function (key, value) {
          return store[key] = value + '';
        });
        spyOn(localStorage, 'clear').and.callFake(function () {
            store = {};
        });

        TestBed.configureTestingModule({
            imports: [
                BatchSummaryComponent,
                RouterTestingModule.withRoutes([]),
            ],
        }).compileComponents();
        router = TestBed.get(Router);
        breadrumbService = new BreadcrumbService(router);

        localStorageService = new MockLocalStorageService();
        batchSummaryService = new MockBatchSummaryService(null);
        toaster = new MockToasterService();
        batchSummaryComponent = new BatchSummaryComponent(batchSummaryService,toaster, currencyPipe, router, breadrumbService, sharedServ);
        batchSummaryComponent.ngOnInit();
    });

    // Now the tests themselves.
    it('batchSummary paymentSources count should be 3', () => {
        // arrange
        let count = batchSummaryComponent.paymentSources.length;
        console.log(batchSummaryComponent.paymentSources);
        // assert
        expect(count).toEqual(3);
    });

    it('batchSummary paymentTypes count should be 3', () => {
        // arrange
        let count = batchSummaryComponent.paymentTypes.length;
        // assert
        expect(count).toEqual(3);
    });
    
    it('batchSummary PaymentTypes 2nd element should be TestPayment2', () => {
        // arrange
        let paymentType = batchSummaryComponent.selectedPaymentType;
        // assert
        expect(paymentType.LongName).toEqual(batchSummaryComponent.paymentTypes[1].LongName);
        expect(paymentType.PaymentTypeKey).toEqual(batchSummaryComponent.paymentTypes[1].PaymentTypeKey);
    });
});