// Globals - needed in every spec.
import 'zone.js';
import 'reflect-metadata';

// Imports for this test spec.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MockDateService } from "../services/date.service/date.service.mock";
import { MockToasterService } from "../services/toaster.service/toaster.service.mock";
import { DashboardComponent } from "../modules/dashboard.component/dashboard.component";
import { MockDashboardService } from '../services/dashboard.service/dashboard.service.mock';
import { MockPermissionService } from '../services/permission.service/permission.service.mock';
import { ToCurrencyPipe } from '../pipes/tocurrency.pipe';
import { MockLocalStorageService } from '../services/localstorage.service/localstorage.service.mock';
import { Workgroup } from '../DTOs/Workgroup';
import { BreadcrumbService } from '../services/breadcrumb.service/breadcrumb.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { ToNumberPipe } from '../pipes/tonumber.pipe';

// Unit test suite for the dashboard component.
describe('dashboard tests', () => {
    let component: DashboardComponent;
    let fixture: ComponentFixture<DashboardComponent>;
    let dateservice: MockDateService;
    let toaster: MockToasterService;
    let currency: ToCurrencyPipe;
    let numberPipe: ToNumberPipe;
    let dashboardservice: MockDashboardService;
    let permissionservice: MockPermissionService;
    let localstorageservice: MockLocalStorageService;
    let breadcrumbService: BreadcrumbService;
    let router: Router;
    // Suite setup code
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                DashboardComponent,
                RouterTestingModule.withRoutes([]),
            ],
        }).compileComponents();
        router = TestBed.get(Router);
        dateservice = new MockDateService();
        toaster = new MockToasterService();
        currency = new ToCurrencyPipe();
        numberPipe = new ToNumberPipe();
        permissionservice = new MockPermissionService(null);
        dashboardservice = new MockDashboardService(null);
        localstorageservice = new MockLocalStorageService();
        breadcrumbService = new BreadcrumbService(router);
        component = new DashboardComponent(
            dashboardservice,
            toaster,
            dateservice,
            currency,
            permissionservice,
            localstorageservice,
            breadcrumbService,
            numberPipe);
        dateservice.date = new Date('10/10/2000');
        component.ngOnInit();
        component.selectedWorkgroup = new Workgroup();
    });

    // Now the tests themselves.

    it('should display data', () => {
        component.refresh();
        expect(component.chartData.length).toBeGreaterThan(0);
    });

    it('count data should add up to 100%', () => {
        component.refresh();
        let total = component.chartData
            .map(e => { return { percent: e.ItemCountPercent }; })
            .reduce((t, p) => (parseFloat(p.percent) + t), 0);
        expect(total).toBe(100);
    });

    it('amount data should add up to 100%', () => {
        component.refresh();
        let total = component.chartData
            .map(e => { return { percent: e.ItemTotalPercent }; })
            .reduce((t, p) => (parseFloat(p.percent) + t), 0);
        expect(total).toBe(100);
    });

});