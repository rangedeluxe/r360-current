import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayerModal } from '../controls/payermodal.component/payermodal.component';

describe('PayeraddmodalComponent', () => {
  let component: PayerModal;
  let fixture: ComponentFixture<PayerModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PayerModal]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayerModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
