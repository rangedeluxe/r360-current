interface Array<T> {
    groupBy(prop: string):Array<any>;
}

Array.prototype.groupBy = function(prop) {
    return this.reduce(function (rv, x) {
        (rv[x[prop]] = rv[x[prop]] || []).push(x);
        return rv;
    }, {});
}