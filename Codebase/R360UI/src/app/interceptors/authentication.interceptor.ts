import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { TokenService } from '../services/token.service/token.service'
import { Observable } from 'rxjs/Observable';
@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  constructor
  (
    private tokenService: TokenService
  ) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    const token = this.tokenService.getToken(); 
    if (token) {
      let authenticatedRequest = request.clone({
        headers: request.headers.set("Authorization", "Bearer " + token)
      });

      return next.handle(authenticatedRequest);
    }

    return next.handle(request);
  }
}

/**
 * Provider POJO for the interceptor
 */
export const AuthenticationInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthenticationInterceptor,
  multi: true,
};