import { Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

@Component({
  moduleId: module.id.toString(),
  selector: 'dashboard-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent 
{
  
}