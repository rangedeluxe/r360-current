import { Pipe, PipeTransform } from '@angular/core';

/*
  This is a pipe to remove all spaces from a string.
*/
@Pipe({name: 'trim'})
export class TrimPipe implements PipeTransform {
  transform(value: string): string {
    return value.replace(/ /, "");
  }
}