import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'tocurrency'})
export class ToCurrencyPipe implements PipeTransform {
  transform(value: string): string {
    
    if (value === null || value === undefined)
        return '';

    if (typeof value === 'number' && isNaN(parseFloat(value)))
        return '';

    if(isNaN(parseFloat(value)))
        return '';

    var val = typeof value === 'string'
        ? parseFloat(value).toFixed(2)
        : (<number>(value)).toFixed(2);

    val = '$' + val.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,");

    // If we have an "infinity" result, clear it.
    if (/[Ii]nfinity/.test(val))
        return '';

    return val;

  }
}