# R360UI
A "3.0" version of R360's web views written in Angular. (version 4).

## Installing & Running
Execute the following:
```
npm install
npm run watch
```

Go to the solution within your r360-Current > Codebase > ApplicationBlocks
Compile the ApplicationBlocks.sln file to generate the ApplicationBlocks assemblies
Mine was located at
C:\dev\r360-current\Codebase\ApplicationBlocks\ApplicationBlocks.sln



The watch script will start up an angular-cli process that watches your development files and will auto-transpile any source that changes.

**Note**: This does not serve up the angular project, it just listens for changes and transpiles the typescript.  After configuring IISExpress (shown below) you should be able to get access R360 as you normally would.

Installing into IIS Express:
```
<!-- Manually added for Angular2 Support -->
<application path="/UI" applicationPool="Clr2IntegratedAppPool">
	<virtualDirectory path="/" physicalPath="C:\workspaces\git-r360-current\codebase\R360UI\dist" />
</application>
```

Open up the /R360UI.sln file and build it once in VS2017. After you build it once (and pull down the nuget packages), copy these files from the root directory into dist.
 - bin
 - Global.asax
 - web.config
 - webMachineKey.config

## Unit tests
Run the command:
```
npm run test
```
This will open a new chrome window to display the results of the test and also will launch the typescript-watcher process. The tests will auto-execute after a file is saved.