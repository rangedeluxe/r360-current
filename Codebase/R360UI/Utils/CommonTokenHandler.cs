using System;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using R360UI.Models;
using Microsoft.IdentityModel.Tokens;

namespace R360UI.Utils
{
   public class CommonTokenHandler
   {
      private string GetFormmatedBaseUrl()
      {
         return ConfigurationManager.AppSettings.Get("webapiurl").EndsWith("/")
            ? ConfigurationManager.AppSettings.Get("webapiurl")
            : ConfigurationManager.AppSettings.Get("webapiurl") + "/";
      }
      private string RequestToken()
      {
         var signedAndEncodedToken = string.Empty;
         try
         {
            var certificate = CertificateHelper.FindByThumbprint(ConfigurationManager.AppSettings.Get("Thumbprint"),
               StoreName.My, StoreLocation.LocalMachine);

            var x509SigningKey = new X509SecurityKey(certificate);
            var signingCredentials = new SigningCredentials(x509SigningKey, SecurityAlgorithms.RsaSha256);

            int lifetime = int.TryParse(ConfigurationManager.AppSettings.Get("Lifetime"), out lifetime)
               ? lifetime
               : 0;

            var token = new JwtSecurityToken(
               ConfigurationManager.AppSettings.Get("Issuer").ToLower(),
               ConfigurationManager.AppSettings.Get("Audience").ToLower(),
               null,
               //ClaimsPrincipal.Current.Claims,
               expires: DateTime.Now.AddMinutes(lifetime),
               signingCredentials: signingCredentials
            );
            signedAndEncodedToken = new JwtSecurityTokenHandler().WriteToken(token);
         }
         catch (Exception e)
         {
            throw new ArgumentException(nameof(RequestToken) + "Error generating token: " + e);
         }

         return signedAndEncodedToken;
      }

      public BaseWebApiResponse Delete(string controller, Guid sid)
      {
         var url = GetFormmatedBaseUrl() + $"{controller}/{sid}";
         var token = RequestToken();
         var baseWebApiResponse = new BaseWebApiResponse();
         using (var client = new HttpClient())
         {
            //setup client
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var response = client.DeleteAsync(url).Result;
            if (!response.IsSuccessStatusCode) return baseWebApiResponse;
            baseWebApiResponse.Status = WebApiStatusCode.SUCCESS;
            baseWebApiResponse.Response = response.Content.ReadAsStringAsync().Result;
         }
         return baseWebApiResponse;
      }
   }
}