using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace R360UI.Utils
{
   public static class CertificateHelper
   {
      public static X509Certificate2 FindByThumbprint(
         string thumbprint,
         StoreName storeName,
         StoreLocation storeLocation)
      {
         var certificateStore = new X509Store(storeName, storeLocation);
         certificateStore.Open(OpenFlags.ReadOnly);

         foreach (var certificate in certificateStore.Certificates)
         {
            if (string.Equals(certificate?.Thumbprint, thumbprint, StringComparison.CurrentCultureIgnoreCase))
            {
               certificateStore.Close();
               return certificate;
            }
         }
         throw new ArgumentException($"Cannot find certificate with thumbprint {thumbprint} in certificate store");
      }
   }
}