using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using R360UI.Models;
using System.Configuration;
using Wfs.Logging;

namespace R360UI.Utils
{
   public class SsoApiClient
   {
      private IWfsLog _log;
      public SsoApiClient(IWfsLog log)
      {
         _log = log;
      }

      public bool DeleteSession(Guid sid)
      {
         if (!IsCallApiEnabled())
         {
            return true;
         }
         var th = new CommonTokenHandler();
         var response = th.Delete("Session", sid);
         return response.Status == WebApiStatusCode.SUCCESS;
      }

      private static bool IsCallApiEnabled()
      {
         return ConfigurationManager.AppSettings.Get("CallWebAPI") != null &&
                ConfigurationManager.AppSettings.Get("CallWebAPI").ToLower() == "true";
      }
   }
}