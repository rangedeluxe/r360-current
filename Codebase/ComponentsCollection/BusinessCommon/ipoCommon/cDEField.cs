
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*
*    Module: cDEField
*  Filename: cDEField.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
******************************************************************************/

namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cDEField {

        private string _TableName;
        private string _FldName;
        private string _FldValue;
        private FieldActions _FldAction;

        /// <summary></summary>
        public cDEField(string vTableName, string vFldName, string vFldValue, DataEntryDataType dataType, FieldActions vAction) {
            _TableName = vTableName;
            _FldName = vFldName;
            _FldValue = vFldValue;
            FldDataTypeEnum = dataType;
            _FldAction = vAction;
        }

        /// <summary></summary>
        public string TableName {
            get { return(_TableName); }
        }

        /// <summary></summary>
        public string FldName {
            get { return(_FldName); }
        }

        /// <summary></summary>
        public string FldValue {
            get { return(_FldValue); }
        }

        /// <summary></summary>
        public DataEntryDataType FldDataTypeEnum { get; }

        /// <summary></summary>
        public FieldActions FldAction{
            get { return(_FldAction); }
        }       
    }
}
