﻿/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    3/2/2011
*
* Purpose:  cICONBll.cs.
*
* Modification History
* 03/02/2011 CR 31536 WJS
*     - Initial release.
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
******************************************************************************/
namespace WFS.RecHub.Common
{
    /// <summary>
    /// Summary description for cItemData.
    /// </summary>
    public class cItemData :  cData
    {
        /// <summary>
        /// Constructor for cItemData object
        /// </summary>
        /// <param name="iDataSetupFieldKey"></param>
        /// <param name="sKeyword"></param>
        /// <param name="iDataType"></param>
        /// <param name="sDataValue"></param>
        public cItemData(int iDataSetupFieldKey, string sKeyword, int iDataType, string sDataValue)
            : base(iDataSetupFieldKey, sKeyword, iDataType, sDataValue)
        {
		}
    }
}
