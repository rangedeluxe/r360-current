using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 7728 JMC 06/01/2004
*    -Created Class
* CR 12501 JMC 05/09/2005
*    -Added DepositDate object member.
* 05/20/2009 CR 23584 MEH
*   -integraPAY Online Interface Into Hyland IMS Archive
* CR 32562 JMC 03/02/2011 
*   -Removed PICSDate, DepositDate, ProcessingDate, FileDescriptor, TxnSequence, SiteCode, 
*    and LockboxColorMode properties
*   -Added ProcessingDateKey property.
* CR 50564 JMC 02/24/2012
*   -Added SiteCode property.
* CR 50983 WJS 03/14/2012
*  -Added BatchSiteCode property, remove siteCode property
* CR 54169 JNE 08/08/2012
*  -Added BatchNumber property 
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
* WI 143419 DJW 6/2/2013
*   BatchID to Long, and BatchId to SourceBatchID conversion
* WI 166398 SAS 9/17/2014
*    Added ImportTypeShortName field 
* WI 166924 SAS 9/19/2014
*   Updated the datatype for Source BatchID       
* WI 175186 SAS 10/30/2014
*    Update ImportTypeShortName field to BatchSourceName
* WI 176351 SAS 11/06/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// Image object, used as a base class for Checks and Documents.  Object
    /// represents an image stored in a file system using attributes of this
    /// class.
    /// </summary>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.dmpinc.com/ipoServices")]
	public class cImage {

        private int _BatchSiteCode = -1;
        private int _BankID = -1;
        private int _LockboxID = -1;
        private int _ProcessingDateKey = -1;
        private long _BatchID = -1;
        private long _SourceBatchID = -1;
        private int _BatchNumber = -1;
        private int _BatchSequence = -1;
        private int _PaymentType = -1;
        private string _BatchSourceShortName = string.Empty;
        private string _ImportTypeShortName = string.Empty;

        /// <summary>
        /// Default Constructor; required for serialization.
        /// </summary>
		public cImage() {
        }

        /// <summary>
        /// Overloaded constructor accepts any descendent of cImage to populate 
        /// this new object.  This constructor is necessary when a function expects
        /// a serialized cImage object.  Casting a descendent object as a cImage is
        /// not sufficient in this case, only a base cImage class will do.
        /// </summary>
		public cImage(cImage baseImage) {
            this.BatchSiteCode = baseImage.BatchSiteCode;
            this.BankID = baseImage.BankID;
            this.LockboxID = baseImage.LockboxID;
            this.ProcessingDateKey = baseImage.ProcessingDateKey;
            this.BatchID = baseImage.BatchID;
            this.BatchNumber = baseImage.BatchNumber;
            this.BatchSequence = baseImage.BatchSequence;         
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BatchSiteCode"></param>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="ProcessingDateKey"></param>
        /// <param name="BatchID"></param>
        /// <param name="BatchNumber"></param>
        /// <param name="BatchSequence"></param>
        public cImage(
            int BatchSiteCode,
            int BankID,
            int LockboxID,
            int ProcessingDateKey,
            int BatchID,
            int BatchNumber,
            int BatchSequence
            ) {

            this.BankID = BankID;
            this.BatchID = BatchID;
            this.BatchNumber = BatchNumber;
            this.BatchSequence = BatchSequence;
            this.ProcessingDateKey = ProcessingDateKey;
            this.LockboxID = LockboxID;            
        }

        /// <summary>
        /// Batch.ProcessingDate
        /// </summary>
        public string PICSDate {
            get { return(_ProcessingDateKey.ToString()); }
        }

        /// <summary>
        /// Batch.ProcessingDate
        /// </summary>
        public int ProcessingDateKey {
            get { return(_ProcessingDateKey); }
            set { _ProcessingDateKey = value; }
        }

        /// <summary>
        /// Batch.BatchSiteCode
        /// </summary>
        public int BatchSiteCode {
            get { return (_BatchSiteCode); }
            set { _BatchSiteCode = value; }
        }

        /// <summary>
        /// Batch.BankID
        /// </summary>
        public int BankID {
            get { return(_BankID); }
            set { _BankID = value; }
        }

        /// <summary>
        /// Batch.LockboxID
        /// </summary>
        public int LockboxID {
            get { return(_LockboxID); }
            set { _LockboxID = value; }
        }

        /// <summary>
        /// Batch.BatchID
        /// </summary>
        public long BatchID {
            get { return(_BatchID); }
            set { _BatchID = value; }
        }
        /// <summary>
        /// Batch.SourceBatchID
        /// </summary>
        public long SourceBatchID
        {
            //added due to WI143419
            get { return (_SourceBatchID); }
            set { _SourceBatchID = value; }
        }
        /// <summary>
        /// Batch.BatchNumber
        /// </summary>
        public int BatchNumber {
            get { return(_BatchNumber); }
            set { _BatchNumber = value; }
        }

        /// <summary>
        /// Batch.BatchSequence
        /// </summary>
        public int BatchSequence {
            get { return(_BatchSequence); }
            set { _BatchSequence = value; }
        }

        /// <summary>
        /// BatchSourceShortName
        /// </summary>
        public string BatchSourceShortName
        {
            get { return (_BatchSourceShortName); }
            set { _BatchSourceShortName = value; }
        }

        /// <summary>
        /// ImportTypeShortName
        /// </summary>
        public string ImportTypeShortName
        {
            get { return (_ImportTypeShortName); }
            set { _ImportTypeShortName = value; }
        }


        /// <summary>
        /// PaymentType Key
        /// </summary>
        public int PaymentType
        {
            get { return (_PaymentType); }
            set { _PaymentType = value; }
        }
            
        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public virtual bool LoadDataRow(ref DataRow dr) {

            int intTemp;

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
                        case ("batchsitecode"):
                        case ("sitecodeid"):
                            this.BatchSiteCode = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("bankid"):
                            this.BankID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("lockboxid"):
                            this.LockboxID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("sitebankid"):
                            this.BankID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("sitelockboxid"):
                            this.LockboxID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("processingdatekey"):
                            this.ProcessingDateKey=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("picsdate"):
                            if(!dr.IsNull(dc)) {
                                if(int.TryParse(dr[dc].ToString(), out intTemp)) {
                                    this.ProcessingDateKey=intTemp;
                                }
                            }
                            break;
                        case("batchid"):
                            this.BatchID=ipoLib.NVL(ref dr, dc.ColumnName, (long)-1);
                            break;
                        //added due to WI143419
                        case ("sourcebatchid"):
                            this.SourceBatchID = ipoLib.NVL(ref dr, dc.ColumnName, (long)-1);
                            break;
                        case ("batchsequence"):
                            this.BatchSequence=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("batchnumber"):
                            this.BatchNumber=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("batchsourceshortname"):
                            this.BatchSourceShortName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty).TrimEnd();
                            break;
                        case ("importtypeshortname"):
                            this.ImportTypeShortName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty).TrimEnd();
                            break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }
    }
}
