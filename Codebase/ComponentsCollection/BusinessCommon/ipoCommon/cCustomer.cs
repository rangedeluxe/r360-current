using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Common  object
*  Filename: cCustomer.cs
*    Author: Joel Caples
*
* Revisions:
* ----------------------
* CR  JMC 
*    -Created Class
* CR 22115 JMC 01/26/2010
*    - Added new property: Description in support of Research
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
******************************************************************************/
namespace WFS.RecHub.Common {

	/// <summary>
	/// Summary description for cCustomer.
	/// </summary>
	public class cCustomer: IComparable {

        private int _BankID = -1;
        private int _CustomerID = -1;
        private string _CustomerName = string.Empty;
    	private string _Description;

        /// <summary>
        /// Default Constructor; required for serialization.
        /// </summary>
		public cCustomer() {
		}

        /// <summary>
        /// Bank.BankID
        /// </summary>
        public int BankID {
            get { return _BankID; }
            set { _BankID=value; }
        }

        /// <summary>
        /// Documents.GlobalDocumentID
        /// </summary>
        public int CustomerID {
            get { return _CustomerID; }
            set { _CustomerID=value; }
        }
    
        /// <summary>
        /// Documents.FileDescriptor
        /// </summary>
        public string CustomerName {
            get { return _CustomerName; }
            set { _CustomerName=value; }
        }

        /// <summary></summary>
        public string Description {
            get {
                return(_Description);
            }
            set {
                _Description = value.TrimEnd();
            }
        }

        /// <summary>
        /// Orders class in a collection by CustomerID.
        /// </summary>
        /// <param name="x">cCustomer object to compare this object to.</param>
        /// <returns>
        /// A signed number indicating the relative values of this instance and value.    
        /// Return Value Description:
        ///     Less than zero: This instance is less than value.     
        ///     Zero: This instance is equal to value.     
        ///     Greater than zero:  This instance is greater than value.  -or-  value is null.     
        /// </returns>
        public int CompareTo(object x) {
            if (x is cCustomer) {
                cCustomer objCustomerX = (cCustomer)x;
 
                return(this.CustomerID.CompareTo(objCustomerX.CustomerID));
           }
            else {
                throw(new Exception("Invalid document object"));
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    if(dr[dc] != null) {
                        switch(dc.ColumnName.ToLower()) {
                            case "bankid":
                                _BankID = (int)dr[dc];
                                break;
                            case "customerid":
                                _CustomerID = (int)dr[dc];
                                break;
                            case "name":
                                _CustomerName = dr[dc].ToString();
                                break;
                            case "description":
                                _Description = dr[dc].ToString();
                                break;
                        }
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// Create an XML node of the current EventRule with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public XmlNode AsXMLNode(string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;

            try {

                // Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                // Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);

                // set each property as an attribute to the node

                ipoXmlLib.addAttribute(node, "BankID", this.BankID.ToString());
                ipoXmlLib.addAttribute(node, "CustomerID", this.CustomerID.ToString());
                ipoXmlLib.addAttribute(node, "CustomerName", this.CustomerName);
                ipoXmlLib.addAttribute(node, "Description", this.Description);
                ipoXmlLib.addAttribute(node, "DisplayName", this.DisplayName);

                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
             return(this.CustomerID.ToString() + " - " + this.CustomerName.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayName {
            get {
                return(this.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CustomerList"></param>
        /// <param name="BankID"></param>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        public static bool CustomerExists(List<cCustomer> CustomerList, 
                                          int BankID, 
                                          int CustomerID) {
            cCustomer objCustomer;
            return(FindCustomer(CustomerList, BankID, CustomerID, out objCustomer));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CustomerList"></param>
        /// <param name="BankID"></param>
        /// <param name="CustomerID"></param>
        /// <param name="Customer"></param>
        /// <returns></returns>
        public static bool FindCustomer(List<cCustomer> CustomerList, 
                                        int BankID, 
                                        int CustomerID, 
                                        out cCustomer Customer) {
            
            bool bolRetVal = false;
            Customer = null;
            
            if(CustomerList != null) {
                foreach(cCustomer customer in CustomerList) {
                    if(customer.BankID == BankID &&
                       customer.CustomerID == CustomerID) {
                        Customer = customer;
                        bolRetVal = true;
                        break;
                    }
                }
            } else {
                Customer = null;
                bolRetVal = false;
            }
            
            return(bolRetVal);
        }
    }
}
