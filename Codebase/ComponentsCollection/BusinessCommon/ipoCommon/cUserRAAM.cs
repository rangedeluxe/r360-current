﻿using System;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 11/06/2006
*
* Purpose:	User Interface for RAAM user (very little data)
*
* Modification History
* WI 432xxx TWE 06/20/2014
*    -Created Class.
* WI 149481 TWE 06/23/2014
*    Remove OLOrganizationID from user object
******************************************************************************/

namespace WFS.RecHub.Common
{
    /// <summary>
    /// Summary description for cUserRAAM
    /// </summary>
    public class cUserRAAM
    {
        private int _UserID = -1;
        private Guid _RaamSid = Guid.Empty;
        private Guid _SessionID = Guid.Empty;

        /// <summary>
        /// Constructor
        /// </summary>
        public cUserRAAM()
        {
		}

        /// <summary>
        /// User ID for R360
        /// </summary>
        public int UserID {
            get {
                return(_UserID);
            }
            set {
                _UserID = value;
            }
        }

        /// <summary>
        /// The SID assigned by RAAM to the user
        /// </summary>
        public Guid RaamSid
        {
            get
            {
                return (_RaamSid);
            }
            set
            {
                _RaamSid = value;
            }
        }

        /// <summary>
        /// The SID assigned by RAAM to the user
        /// </summary>
        public Guid SessionID
        {
            get
            {
                return (_SessionID);
            }
            set
            {
                _SessionID = value;
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(ref DataRow dr)
        {

            if (dr.Table.Columns.Count > 0)
            {
                foreach (DataColumn dc in dr.Table.Columns)
                {
                    switch (dc.ColumnName.ToLower())
                    {
                        case ("userid"):
                            _UserID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("ra3msid"):
                            _RaamSid = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                     }
                }
                return (true);
            }
            else
            {
                return (false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <returns></returns>
        public XmlNode ToXml(XmlNode nodeParent)
        {
            return (this.ToXml(nodeParent, "User"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public XmlNode ToXml(XmlNode nodeParent, string nodeName)
        {
            XmlNode nodeUser;
            XmlAttribute att;

            nodeUser = nodeParent.OwnerDocument.CreateNode(XmlNodeType.Element, nodeName, nodeParent.NamespaceURI);

            att = nodeParent.OwnerDocument.CreateAttribute("UserID", nodeParent.NamespaceURI);
            att.Value = this.UserID.ToString();
            nodeUser.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("RaamSid", nodeParent.NamespaceURI);
            att.Value = this.RaamSid.ToString();
            nodeUser.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("SessionID", nodeParent.NamespaceURI);
            att.Value = this.SessionID.ToString();
            nodeUser.Attributes.Append(att);

            nodeParent.AppendChild(nodeUser);

            return (nodeUser);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeUser"></param>
        /// <returns></returns>
        public bool FromXml(XmlNode nodeUser)
        {

            try
            {
                foreach (XmlAttribute att in nodeUser.Attributes)
                {
                    switch (att.Name.ToLower())
                    {
                        case ("userid"):
                            _UserID = int.Parse(att.Value);
                            break;
                        case ("raamsid"):
                            _RaamSid = new Guid(att.Value);
                            break;
                        case ("sessionid"):
                            _SessionID = new Guid(att.Value);
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                throw (e);
            }

            return (true);
        }
    }
}
