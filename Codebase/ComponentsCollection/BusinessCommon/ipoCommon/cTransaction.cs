﻿using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:	Joel Caples
* Date:		08/08/2012
*
* Purpose:	Structure containing instructions when processing a Batch or Transactional level job.
*
* Modification History
* CR 54169 JNE 08/08/12
*   - Added Flowerbox and BatchNumber 
* WI 89972 CRG 03/01/2013
*	Modify ipoCommon for .NET 4.5 and NameSpace
* WI 143274 SAS 05/21/2014
*   Changes done to change the datatype of BatchID from int to long
******************************************************************************/
namespace WFS.RecHub.Common
{

#if(!USE_ITEMPROC)


    /// <summary>
    /// Structure containing instructions when processing a Batch or 
    /// Transactional level job.
    /// </summary>
    public class cTransaction : IComparable {


        private int _BankID = -1;
        private int _LockboxID = -1;
        private DateTime _DepositDate = DateTime.MinValue;
        private long _BatchID = -1;
        private int _TransactionID = -1;
        private int _TxnSequence = -1;
        private int _BatchNumber = -1;

        /// <summary></summary>
        public int BankID {
            get {
                return(_BankID);
            }
        }

        /// <summary></summary>
        public int LockboxID {
            get {
                return(_LockboxID);
            }
        }

        /// <summary></summary>
        public DateTime DepositDate {
            get {
                return(_DepositDate);
            }
        }

        /// <summary></summary>
        public long BatchID
        {
            get {
                return(_BatchID);
            }
        }

        /// <summary></summary>
        public int BatchNumber {
            get {
                return(_BatchNumber);
            }
        }

        /// <summary></summary>
        public int TransactionID {
            get {
                return(_TransactionID);
            }
        }

        /// <summary></summary>
        public int TxnSequence {
            get {
                return(_TxnSequence);
            }
        }

        /// <summary></summary>
        public cTransaction(int vBankID, 
                            int vLockboxID, 
                            DateTime vDepositDate, 
                            long vBatchID,
                            int vBatchNumber, 
                            int vTransactionID, 
                            int vTxnSequence) {

            _BankID = vBankID;
            _LockboxID = vLockboxID;
            _DepositDate = vDepositDate;
            _BatchID = vBatchID;
            _BatchNumber = vBatchNumber;
            _TransactionID = vTransactionID;
            _TxnSequence = vTxnSequence;
        }

        /////// <summary></summary>
        ////public override string ToString()
        ////{
        ////    return(this.GlobalBatchID.ToString()
        ////         + "~" + this.TransactionID.ToString());
        ////}
        #region IComparable Members

        /// <summary></summary>
        public int CompareTo(object obj)
        {
            if(obj is cTransaction) {
                cTransaction testObj = (cTransaction)obj;

                if(this.BatchID==testObj.BatchID) {
                    return(this.TxnSequence.CompareTo(testObj.TxnSequence));
                } else {
                    //return((int)(this.globalBatchID - testObj.globalBatchID));
                    return(this.BatchID.CompareTo(testObj.BatchID));
                }
            }

            throw new ArgumentException("object is not a cTransaction");
        }

        #endregion
    }

#endif
}


