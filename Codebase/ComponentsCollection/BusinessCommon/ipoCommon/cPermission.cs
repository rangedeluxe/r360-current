using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*  Module:           CheckBOX Online Common Permission Class Library
* 
*  Filename:         cPermission.cls
* 
*  Author:           Jon Sucha
* 
*  Description:
* 
*    This class library provides ...
* 
*  Revisions:
* CR 12627 EJG 11/29/2005 - Added AsXMLNode() and FromXMLNode() to convert the object to an
*                           IXMLDomNode and to set the properties from an IXMLDomNode.
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
**********************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cPermission {

        //-------------------------------------------------------------------------------
        // Private member variables declaration
        //-------------------------------------------------------------------------------
        private int _PermissionId;
        private string _PermissionCategory;
        private string _PermissionName;
        private string _ScriptFile;
        private bool _PermissionDefault;
        private string _PermissionMode;

        /// <summary>
        /// 
        /// </summary>
        public int PermissionID {
	        get { return(_PermissionId); }
	        set { _PermissionId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string PermissionCategory {
	        get { return(_PermissionCategory); }
	        set { _PermissionCategory = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string PermissionName {
	        get { return(_PermissionName); }
	        set { _PermissionName = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ScriptFile {
	        get { return(_ScriptFile); }
	        set { _ScriptFile = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool PermissionDefault {
	        get { return(_PermissionDefault); }
	        set { _PermissionDefault = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string PermissionMode {
	        get { return(_PermissionMode); }
	        set { _PermissionMode = value; }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
			            case "permissionid":
				            _PermissionId = ipoLib.NVL(ref dr, dc.ColumnName, -1);
				            break;
			            case "permissioncategory":
				            _PermissionCategory = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty).TrimEnd();
				            break;
			            case "permissionname":
				            _PermissionName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty).TrimEnd();
				            break;
			            case "scriptfile":
				            _ScriptFile = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty).TrimEnd();
				            break;
			            case "permissiondefault":
				            _PermissionDefault = ((int)ipoLib.NVL(ref dr, dc.ColumnName, 0) > 0);
				            break;
			            case "permissionmode":
				            _PermissionMode = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty).TrimEnd();
				            break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// Create an XML node of the current Permission with properties as attributes
        /// </summary>
        /// <returns>
        /// IXMLDomNode reference
        /// Format: <NodeName Property="Value"/>
        ///</returns>
        public XmlNode AsXMLNode() {

            return(AsXMLNode("Record"));
        }

        /// <summary>
        /// Create an XML node of the current Permission with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        /// Format: <NodeName Property="Value"/>
        ///</returns>
        public XmlNode AsXMLNode(string NodeName) {

	        XmlDocument xmlDoc;
	        XmlNode node;
	        XmlNode nodeRetVal = null;

	         try {

	            //Get XML DOM Document
	            xmlDoc = ipoXmlLib.GetXMLDoc();

	            //Add the node to the XML Document
	            node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);

	            //set each property as an attribute to the node
	            ipoXmlLib.addAttribute(node, "PermissionID", this.PermissionID.ToString());
	            ipoXmlLib.addAttribute(node, "PermissionCategory", this.PermissionCategory);
	            ipoXmlLib.addAttribute(node, "PermissionName", this.PermissionName);
	            ipoXmlLib.addAttribute(node, "ScriptFile", this.ScriptFile);
	            ipoXmlLib.addAttribute(node, "PermissionDefault", PermissionDefault.ToString());
	            ipoXmlLib.addAttribute(node, "PermissionMode", this.PermissionMode);

	            //return the node reference
	            nodeRetVal = node;

            } catch(Exception) {
	            nodeRetVal = null;
            }

            return nodeRetVal;
        }

        /// <summary>
        /// Populate properties of a cLockbox object from an xml node generated
        /// by AsXMLNode() above.
        /// </summary>
        /// <param name="node">Format: <NodeName Property="Value"/></param>
        /// <returns></returns>
        public bool FromXMLNode(XmlNode node) {

            try {
                //loop through the attributes in the node reference
                foreach(XmlAttribute att in node.Attributes) {
                    //using the attribute name, assign the value to the corresponding cLockbox property
                    switch(att.Name.ToLower()) {
                        case "permissionid":
                            _PermissionId = int.Parse(att.Value);
                            break;
                        case "permissioncategory":
                            _PermissionCategory = att.Value;
                            break;
                        case "permissionname":
                            _PermissionName = att.Value;
                            break;
                        case "scriptfile":
                            _ScriptFile = att.Value;
                            break;
                        case "permissiondefault":
                            _PermissionDefault = bool.Parse(att.Value);
                            break;
                        case "permissionmode":
                            _PermissionMode = att.Value;
                            break;
                    }
                }
                
                return(true);
            } catch(Exception) {
                return(false);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class cPermissions : Dictionary<int, cPermission> {

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <returns></returns>
        //public cPermission this[ int key ]  {
        //    get  {
        //        return( (cPermission) Dictionary[key] );
        //    }
        //    set  {
        //        Dictionary[key] = value;
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //public ICollection Keys  {
        //    get  {
        //        return( Dictionary.Keys );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //public ICollection Values  {
        //    get  {
        //        return( Dictionary.Values );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="value"></param>
        //public cPermission Add( int key, cPermission value )  {
        //    Dictionary.Add( key, value );
        //    return(value);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PermissionID"></param>
        /// <param name="PermissionCategory"></param>
        /// <param name="PermissionName"></param>
        /// <param name="ScriptFile"></param>
        /// <param name="PermissionDefault"></param>
        /// <param name="PermissionMode"></param>
        /// <returns></returns>
        public cPermission Add(int PermissionID, 
                               string PermissionCategory, 
                               string PermissionName, 
                               string ScriptFile, 
                               bool PermissionDefault, 
                               string PermissionMode) {

            cPermission objPermission = new cPermission();

            objPermission.PermissionID = PermissionID;
            objPermission.PermissionCategory = PermissionCategory;
            objPermission.PermissionName = PermissionName;
            objPermission.ScriptFile = ScriptFile;
            objPermission.PermissionDefault = PermissionDefault;
            objPermission.PermissionMode = PermissionMode;
            
            this.Add(PermissionID, objPermission);
            
            return(objPermission);
        }


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <returns></returns>
        //public bool Contains( int key )  {
        //    return( Dictionary.Contains( key ) );
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        //public void Remove( int key )  {
        //    Dictionary.Remove( key );
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="value"></param>
        //protected override void OnInsert(Object key, Object value)  {
        //    if (key.GetType() != typeof(System.Int32)) {
        //        throw new ArgumentException( "key must be of type Int32.", "key" );
        //    }

        //    if ( value.GetType() != typeof(cPermission) ) {
        //        throw new ArgumentException( "value must be of type cPermission.", "value" );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="value"></param>
        //protected override void OnRemove( Object key, Object value )  {
        //    if ( key.GetType() != typeof(System.Int32) ) {
        //        throw new ArgumentException( "key must be of type Int32.", "key" );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="oldValue"></param>
        ///// <param name="newValue"></param>
        //protected override void OnSet( Object key, Object oldValue, Object newValue )  {
        //    if (key.GetType() != typeof(System.Int32)) {
        //        throw new ArgumentException( "key must be of type Int32.", "key" );
        //    }

        //    if (newValue.GetType() != typeof(cPermission)) {
        //        throw new ArgumentException( "newValue must be of type cPermission.", "newValue" );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="value"></param>
        //protected override void OnValidate( Object key, Object value )  {
        //    if ( key.GetType() != typeof(System.Int32) ) {
        //        throw new ArgumentException( "key must be of type Int32.", "key" );
        //    }

        //    if (value.GetType() != typeof(cPermission) ) {
        //        throw new ArgumentException( "value must be of type cPermission.", "value" );
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool LoadDataTable(DataTable dt) {

            cPermission objPermission;

            foreach(DataRow dr in dt.Rows) {
                objPermission = new cPermission();
                objPermission.LoadDataRow(dr);
                this.Add(objPermission.PermissionID, objPermission);
            }

            return(true);
        }

        /// <summary>
        /// Create an XML node of the current cPermissions collection
        /// </summary>
        /// <returns>
        /// Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///         </NodeName></returns>
        public XmlNode AsXMLNode() {
            return(AsXMLNode("Permissions", "Recordset"));
        }

        /// <summary>
        /// Create an XML node of the current cPermissions collection
        /// </summary>
        /// <param name="AttributeName"></param>
        /// <returns>
        /// Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///         </NodeName></returns>
        public XmlNode AsXMLNode(string AttributeName) {
            return(AsXMLNode(AttributeName, "Recordset"));
        }

        /// <summary>
        /// Create an XML node of the current cPermissions collection
        /// </summary>
        /// <param name="AttributeName"></param>
        /// <param name="NodeName"></param>
        /// <returns>
        /// Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///         </NodeName></returns>
        public XmlNode AsXMLNode(string AttributeName, string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;
            
            try {
                //Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                //Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);
                    
                //Add the Name attribute to the node
                ipoXmlLib.addAttribute(node, "Name", AttributeName);
                
                //Add each permission as a child node
                foreach(cPermission permission in this.Values) {
                    node.AppendChild(xmlDoc.ImportNode(permission.AsXMLNode(), true));
                }
                
                //return reference to node
                return(node);
            
            } catch(Exception) {
                return(null);
            }
        }

        /// <summary>
        /// Populate a cPermissions collection w/ cPermission objects from an xml node
        /// built using the AsXMLNode function above.
        /// </summary>
        /// <param name="node">
        /// IXMLDomNode
        ///           Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///                   </NodeName>
        /// </param>
        /// <returns>True/False</returns>
        public bool FromXMLNode(XmlNode node) {

            cPermission objPermission;
            
            try {
            
                // loop through each child node in the node reference
                foreach(XmlNode n in node.ChildNodes) {
                    // create a new cPermission object reference
                    objPermission = new cPermission();
                    
                    // assign the cPermission properties from the child node reference
                    if(objPermission.FromXMLNode(n)) {
                        // assign the cPermission to the collection
                        this.Add(objPermission.PermissionID, objPermission);
                    }
                }
                
                return(true);

            } catch(Exception) {
                return(false);
            }
        }
    }

}
