using System;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*  Module:           CheckBOX Online Common EventRule Class Library
* 
*  Filename:         cEventRule.cls
* 
*  Author:           Eric Goetzinger
* 
*  Description:
* 
*  Revisions:
*    CR 10043 EJG 10/05/2004 - Added CustomerName and LockboxName properties to class for
*                              online user changes.
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
**********************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cEventRule {

	    //-------------------------------------------------------------------------------
	    // Private member variables declaration
	    //-------------------------------------------------------------------------------
	    private Guid _EventRuleID;
	    private int _EventID;
	    private int _BankID;
	    private int _CustomerID;
	    private int _LockboxID;
	    private bool _RuleIsActive;
	    private string _EventOperator;
	    private string _EventArgumentCode;
	    private string _EventRuleDescription;
	    private string _EventRuleValue;
	    private string _EventAction;
	    private string _InternetDeliverableID;
	    private bool _OnlineViewable;
	    private bool _OnlineModifiable;
	    private DateTime _CreationDate;
	    private DateTime _ModificationDate;
	    private string _CreatedBy;
	    private string _ModifiedBy;
	    private string _CustomerName;
	    private string _LockboxName;

        /// <summary>
        /// 
        /// </summary>
        public cEventRule() {
            Initialize();
        }

        private void Initialize() {
	        _EventRuleID = Guid.Empty;
	        _EventID = -1;
	        _BankID = -1;
	        _CustomerID = -1;
	        _LockboxID = -1;
	        _RuleIsActive = false;
	        _EventOperator = string.Empty;
	        _EventArgumentCode = string.Empty;
	        _EventRuleDescription = string.Empty;
	        _EventRuleValue = string.Empty;
	        _EventAction = string.Empty;
	        _InternetDeliverableID = string.Empty;
	        _OnlineViewable = false;
	        _OnlineModifiable = false;
	        _CreationDate = DateTime.MinValue;
	        _ModificationDate = DateTime.MinValue;
	        _CreatedBy = string.Empty;
	        _ModifiedBy = string.Empty;
	        _CustomerName = string.Empty;
	        _LockboxName = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
	    public Guid EventRuleID {
		    get { return(_EventRuleID); }
		    set { _EventRuleID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public int EventID {
		    get { return(_EventID); }
		    set { _EventID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public int BankID {
		    get { return(_BankID); }
		    set { _BankID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public int CustomerID {
		    get { return(_CustomerID); }
		    set { _CustomerID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public int LockboxID {
		    get { return(_LockboxID); }
		    set { _LockboxID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public bool RuleIsActive {
		    get { return(_RuleIsActive); }
		    set { _RuleIsActive = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string EventOperator {
		    get { return(_EventOperator); }
		    set { _EventOperator = value.TrimEnd(); }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string EventArgumentCode {
		    get { return(_EventArgumentCode); }
		    set { _EventArgumentCode = value.TrimEnd(); }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string EventRuleDescription {
		    get { return(_EventRuleDescription); }
		    set { _EventRuleDescription = value.TrimEnd(); }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string EventRuleValue {
		    get { return(_EventRuleValue); }
		    set { _EventRuleValue = value.TrimEnd(); }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string EventAction {
		    get { return(_EventAction); }
		    set { _EventAction = value.TrimEnd(); }
	    }

        /// <summary>
        /// 
        /// </summary>
        public string EventActionType {

            get {
                int intPos1;
                int intPos2;
                string strRetVal;

                intPos1 = _EventAction.IndexOf('<');
                if(intPos1 > -1) {
                    intPos2 = _EventAction.IndexOf('>', intPos1);
                    
                    if(intPos2 > intPos1 + 1) {
                        strRetVal = _EventAction.Substring(intPos1 + 1, intPos2 - intPos1 - 1);
                    } else {
                        strRetVal = _EventAction;
                    }

                } else {
                    strRetVal = _EventAction;
                }

                if(strRetVal.ToLower().Trim() == "message") {
                    strRetVal = "Message";
                } else if(strRetVal.ToLower().Trim() == "report") {
                    strRetVal = "Report";
                } else if(strRetVal.ToLower().Trim() == "report group") {
                    strRetVal = "Report Group";
                }

                return(strRetVal);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string EventActionValue {

            get {
                int intPos1;
                int intPos2;
                string strRetVal;

                intPos1 = _EventAction.IndexOf('<');
                if(intPos1 > -1) {
                    intPos2 = _EventAction.IndexOf('>', intPos1);
                    
                    if(intPos2 > intPos1 + 1) {
                        strRetVal = _EventAction.Substring(intPos2 + 1);
                    } else {
                        strRetVal = _EventAction;
                    }

                } else {
                    strRetVal = _EventAction;
                }

                return(strRetVal);
            }
        }

        /// <summary>
        /// This function will strip out portions of an Event Rule value that should
        /// not be displayed to the User.
        /// * Reports are saved to the EventRules.EventAction field using the format:
        ///     &lt;Report&gt;[Group Name],[Report Name].  We only want to display the 
        ///   Report Name.
        /// * Extracts are saved using their file extension which should be stripped.
        /// </summary>
        public string EventActionSimpleValue {
            
            get {
            
                int intPos;
                string strEventActionValue = this.EventActionValue;
                string strRetVal;
                
                if(strEventActionValue == null) {
                    strRetVal = string.Empty;
                } else {
                    switch(this.EventActionType.Trim().ToLower()) {
                        case "report":
                            intPos = strEventActionValue.IndexOf(',');
                            if(intPos > -1 && intPos < strEventActionValue.Length - 1) {
                                strRetVal = strEventActionValue.Substring(intPos + 1);
                            } else {
                                strRetVal = strEventActionValue;
                            }
                            break;
                        case "extract":
                            // Remove file extension from file name (usually .cxs)
                            intPos = strEventActionValue.LastIndexOf('.');
                            if(intPos > 1) {
                                strRetVal = strEventActionValue.Substring(0, intPos -1);
                            } else {
                                strRetVal = strEventActionValue;
                            }
                            break;
                        default:
                            strRetVal = strEventActionValue;
                            break;
                    }
                }
                            
                return(strRetVal.Trim());
            }
        }

        /// <summary>
        /// 
        /// </summary>
	    public string InternetDeliverableID {
		    get { return(_InternetDeliverableID); }
		    set { _InternetDeliverableID = value.TrimEnd(); }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public bool OnlineViewable {
		    get { return(_OnlineViewable); }
		    set { _OnlineViewable = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public bool OnlineModifiable {
		    get { return(_OnlineModifiable); }
		    set { _OnlineModifiable = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public System.DateTime CreationDate {
		    get { return(_CreationDate); }
		    set { _CreationDate = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public System.DateTime ModificationDate {
		    get { return(_ModificationDate); }
		    set { _ModificationDate = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string CreatedBy {
		    get { return(_CreatedBy); }
		    set { _CreatedBy = value.TrimEnd(); }
	    }


        /// <summary>
        /// 
        /// </summary>
	    public string ModifiedBy {
		    get { return(_ModifiedBy); }
		    set { _ModifiedBy = value.TrimEnd(); }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string CustomerName {
		    get { return(_CustomerName); }
		    set { _CustomerName = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string LockboxName {
		    get { return(_LockboxName); }
		    set { _LockboxName = value; }
	    }


        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(ref DataRow dr) {

            Initialize();

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
						    case "eventruleid":
							    _EventRuleID=ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
							    break;
						    case "eventid":
							    _EventID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
							    break;
						    case("bankid"):
                                _BankID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                                break;
                            case("customerid"):
                                _CustomerID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                                break;
                            case("lockboxid"):
                                _LockboxID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                                break;
						    case "ruleisactive":
							    _RuleIsActive=ipoLib.NVL(ref dr, dc.ColumnName, false);
							    break;
						    case "eventoperator":
							    _EventOperator=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;
						    case "eventargumentcode":
							    _EventArgumentCode=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;
						    case "eventruledescription":
							    _EventRuleDescription=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;
						    case "eventrulevalue":
							    _EventRuleValue=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;
						    case "eventaction":
							    _EventAction=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;
						    case "internetdeliverableid":
							    _InternetDeliverableID=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;
						    case "onlineviewable":
							    _OnlineViewable=ipoLib.NVL(ref dr, dc.ColumnName, false);
							    break;
						    case "onlinemodifiable":
							    _OnlineModifiable=ipoLib.NVL(ref dr, dc.ColumnName, false);
							    break;
						    case "creationdate":
							    _CreationDate=ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
							    break;
						    case "modificationdate":
							    _ModificationDate=ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
							    break;
						    case "createdby":
							    _CreatedBy=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;
						    case "modifiedby":
							    _ModifiedBy=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;
						    case "customername":
							    _CustomerName=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;
						    case "lockboxname":
							    _LockboxName=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							    break;

                    }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// Create an XML node of the current EventRule with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public XmlNode AsXMLNode(string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;

            try {

                // Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                // Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);

                // set each property as an attribute to the node
                ipoXmlLib.addAttribute(node, "BankID", this.BankID.ToString());
                ipoXmlLib.addAttribute(node, "CreatedBy", this.CreatedBy);
                ipoXmlLib.addAttribute(node, "CreationDate", this.CreationDate.ToString());
                ipoXmlLib.addAttribute(node, "CustomerID", this.CustomerID.ToString());
                ipoXmlLib.addAttribute(node, "CustomerName", this.CustomerName);
                ipoXmlLib.addAttribute(node, "EventAction", this.EventAction);
                ipoXmlLib.addAttribute(node, "EventActionType", this.EventActionType);
                ipoXmlLib.addAttribute(node, "EventActionValue", this.EventActionValue);
                ipoXmlLib.addAttribute(node, "EventActionSimpleValue", this.EventActionSimpleValue);
                ipoXmlLib.addAttribute(node, "EventArgumentCode", this.EventArgumentCode);
                ipoXmlLib.addAttribute(node, "EventID", this.EventID.ToString());
                ipoXmlLib.addAttribute(node, "EventOperator", this.EventOperator);
                ipoXmlLib.addAttribute(node, "EventRuleDescription", this.EventRuleDescription);
                ipoXmlLib.addAttribute(node, "EventRuleID", this.EventRuleID.ToString());
                ipoXmlLib.addAttribute(node, "EventRuleValue", this.EventRuleValue);
                ipoXmlLib.addAttribute(node, "InternetDeliverableID", this.InternetDeliverableID.ToString());
                ipoXmlLib.addAttribute(node, "LockboxID", this.LockboxID.ToString());
                ipoXmlLib.addAttribute(node, "LockboxName", this.LockboxName);
                ipoXmlLib.addAttribute(node, "ModificationDate", this.ModificationDate.ToString());
                ipoXmlLib.addAttribute(node, "ModifiedBy", this.ModifiedBy);
                ipoXmlLib.addAttribute(node, "OnlineModifiable", (this.OnlineModifiable ? "1" : "0"));
                ipoXmlLib.addAttribute(node, "OnlineViewable", (this.OnlineViewable ? "1" : "0"));
                ipoXmlLib.addAttribute(node, "RuleIsActive", (this.RuleIsActive ? "1" : "0"));
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class cAction {

        private string _Action;
        private string _DisplayName;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vAction"></param>
        /// <param name="vDisplayName"></param>
        public cAction(string vAction, string vDisplayName) {
            _Action = vAction;
            _DisplayName = vDisplayName;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string Action {
            get {
                return(_Action);
            }
            set {
                _Action = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayName {
            get {
                return(_DisplayName);
            }
            set {
                _DisplayName = value;
            }
        }
    }
}
