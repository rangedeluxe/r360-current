using System;
using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 7728 JMC 06/01/2004
*    -Created Class
* CR 32562 JMC 03/02/2011 
*    -Modified class to implement the IItem interface
*    -To adhere to the IItem interface, added DepositDate, TransactionID, TxnSequence, and 
*     DefaultColorMode properties
*    -FileDescriptor is no longer an override property
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
* WI 148949 DJW 6/19/2013
*    Added SessionID to methods for sprocs that needed it (related to RAAM)
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// Document abstraction object.  inherits from Image object.
    /// </summary>
	public class cDocument : cImage, IItem, IComparable
	{
        private DateTime _DepositDate = DateTime.MinValue;
        private int _TransactionID = -1;
        private int _TxnSequence = -1;
        private string _FileDescriptor = string.Empty;
        private int _DocumentSequence = -1;
        // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
        // the IsColor flag is no longer used
        //private bool _IsColor = false;
        private WorkgroupColorMode _DefaultColorMode = WorkgroupColorMode.COLOR_MODE_BITONAL;

        /// <summary>
        /// Default Constructor; required for serialization.
        /// </summary>
		public cDocument() {
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DepositDate {
            get { return _DepositDate; }
            set { _DepositDate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TransactionID {
            get { return _TransactionID; }
            set { _TransactionID=value; }
        }

        /// <summary>
        /// Transactions.TxnSequence
        /// </summary>
        public int TxnSequence{
            get { return(_TxnSequence); }
            set { _TxnSequence=value; }
        }

        /// <summary>
        /// Documents.DocumentSequence
        /// </summary>
        public int DocumentSequence {
            get { return _DocumentSequence; }
            set { _DocumentSequence=value; }
        }
    
        /// <summary>
        /// Documents.FileDescriptor
        /// </summary>
        public string FileDescriptor {
            get { return _FileDescriptor; }
            set { _FileDescriptor=value; }
        }

        // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
        /*
        /// <summary>
        /// Documents.IsColor
        /// </summary>
        public bool IsColor {
            get { return _IsColor; }
            set { _IsColor=value; }
        }
        */
        // END MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 

        public Guid SessionID
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// 
        /// </summary>
        public WorkgroupColorMode DefaultColorMode
        {
            get { return (_DefaultColorMode); }
            set { _DefaultColorMode = value; }
        }         

        /// <summary>
        /// Orders class in a collection by BatchSequence, and then by DocumentSequence.
        /// </summary>
        /// <param name="x">cDocument object to compare this object to.</param>
        /// <returns>
        /// A signed number indicating the relative values of this instance and value.    
        /// Return Value Description:
        ///     Less than zero: This instance is less than value.     
        ///     Zero: This instance is equal to value.     
        ///     Greater than zero:  This instance is greater than value.  -or-  value is null.     
        /// </returns>
        public int CompareTo(object x) {

            if (x is cDocument) {

                cDocument objDocX = (cDocument)x;
 
                if(this.BankID == objDocX.BankID) {

                    if(this.LockboxID == objDocX.LockboxID) {

                        if(this.BatchID == objDocX.BatchID) {

                            if(this.TransactionID == objDocX.TransactionID) {

                                if(this.BatchSequence.ToString()==objDocX.BatchSequence.ToString()) {
                                    return(this.DocumentSequence.CompareTo(objDocX.DocumentSequence));
                                } else {
                                    return(this.BatchSequence.CompareTo(objDocX.BatchSequence));
                                }

                            } else {
                                return(this.TransactionID.CompareTo(objDocX.TransactionID));
                            }

                        } else {
                            return(this.BatchID.CompareTo(objDocX.BatchID));
                        }

                    } else {
                        return(this.LockboxID.CompareTo(objDocX.LockboxID));
                    }

                } else {
                    return(this.BankID.CompareTo(objDocX.BankID));
                }

            } else {
                throw(new Exception("Invalid document object"));
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public override bool LoadDataRow(ref DataRow dr) {

            base.LoadDataRow(ref dr);

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
                        // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                        //case("iscolor"):
                        //    this.IsColor=ipoLib.NVL(ref dr, dc.ColumnName, false);
                        //    break;
                        // END MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                        case("depositdate"):
                            this.DepositDate=ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("transactionid"):
                            this.TransactionID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("txnsequence"):
                            this.TxnSequence=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("filedescriptor"):
                            this.FileDescriptor=ipoLib.NVL(ref dr, dc.ColumnName, "");
                            break;
                        case("documentsequence"):
                            this.DocumentSequence=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("defaultcolormode"):
                        case ("clientaccountcolormode"):
                        case ("onlinecolormode"):
                            switch((byte)ipoLib.NVL(ref dr, dc.ColumnName, (byte)WorkgroupColorMode.COLOR_MODE_BITONAL)) {
                                case (byte)WorkgroupColorMode.COLOR_MODE_GRAYSCALE:
                                    _DefaultColorMode = WorkgroupColorMode.COLOR_MODE_GRAYSCALE;
                                    break;
                                case (byte)WorkgroupColorMode.COLOR_MODE_COLOR:
                                    _DefaultColorMode = WorkgroupColorMode.COLOR_MODE_COLOR;
                                    break;
                                default:
                                    _DefaultColorMode = WorkgroupColorMode.COLOR_MODE_BITONAL;
                                    break;
                            }
                            break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }
	}
}
