﻿using System;
using System.Text.RegularExpressions;
using JetBrains.Annotations;

namespace WFS.RecHub.Common
{
    public static class DataEntryDataTypeValidation
    {
        private const decimal MaxCurrencyValue = 900000000000000;

        public static DataEntryDataTypeValidationError? ValidateInputValue(DataEntryDataType dataType,
            [NotNull] string value)
        {
            //Case - if the value must be numeric
            if (dataType == DataEntryDataType.Numeric)
            {
                double numericValue;
                if (!double.TryParse(value, out numericValue))
                {
                    return DataEntryDataTypeValidationError.InvalidNumericValue;
                }
            }
            //Case - value is currency
            else if (dataType == DataEntryDataType.Currency)
            {
                var regex = new Regex(@"^\s*-?(\d+(?:\.\d{1,2})?|(\.\d{1,2}))\s*$");
                if (!regex.IsMatch(value))
                {
                    return DataEntryDataTypeValidationError.InvalidCurrencyValue;
                }
                // Case - the value is currency and the max value is exceeded.
                else if (decimal.Parse(value) > MaxCurrencyValue)
                {
                    return DataEntryDataTypeValidationError.MaxCurrencyValueExceeded;
                }
                // Case - the value is currency and the min value is exceeded.
                else if (decimal.Parse(value) < -MaxCurrencyValue)
                {
                    return DataEntryDataTypeValidationError.MinCurrencyValueExceeded;
                }
            }
            //Case - value is date
            else if (dataType == DataEntryDataType.Date)
            {
                DateTime tDate;
                if (!DateTime.TryParse(value, out tDate))
                {
                    return DataEntryDataTypeValidationError.InvalidDateTimeValue;
                }
            }

            return null;
        }
    }
}