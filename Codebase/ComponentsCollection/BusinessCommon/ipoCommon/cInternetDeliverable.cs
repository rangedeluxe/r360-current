﻿using System;
using System.Data;
using System.Xml;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:    
*
* Purpose:  cInternetDeliverable.cs.
*
* Modification History
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cInternetDeliverable {

        private Guid _InternetDeliverableID;
        private string _Type;
        private string _DeliverableName;
        private string _GroupName;
        private string _Description;

        /// <summary>
        /// 
        /// </summary>
        public cInternetDeliverable() {
            Initialize();
        }

        /// <summary>
        /// 
        /// </summary>
        public cInternetDeliverable(DataRow dr) {
            Initialize();
            LoadDataRow(dr);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vInternetDeliverableID"></param>
        /// <param name="vDeliverableName"></param>
        /// <param name="vType"></param>
        /// <param name="vGroupName"></param>
        /// <param name="vDescription"></param>
        public cInternetDeliverable(Guid vInternetDeliverableID, 
                                    string vDeliverableName,
                                    string vType,
                                    string vGroupName,
                                    string vDescription) {

            _InternetDeliverableID = vInternetDeliverableID;
            _DeliverableName = vDeliverableName;
            _Type = vType;
            _GroupName = vGroupName;
            _Description = vDescription;
        }

        private void Initialize() {

            _InternetDeliverableID = Guid.Empty;
            _DeliverableName = string.Empty;
            _Type = string.Empty;
            _GroupName = string.Empty;
            _Description = string.Empty;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Guid InternetDeliverableID {
            get {
                return(_InternetDeliverableID);
            }
            set {
                _InternetDeliverableID = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Type {
            get {
                return(_Type);
            }
            set {
                _Type = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DeliverableName {
            get {
                return(_DeliverableName);
            }
            set {
                _DeliverableName = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string GroupName {
            get {
                return(_GroupName);
            }
            set {
                _GroupName = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Description {
            get {
                return(_Description);
            }
            set {
                _Description = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public bool LoadDataRow(DataRow dr) {

            Initialize();

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    if(!dr.IsNull(dc)) {
                        switch(dc.ColumnName.ToLower()) {
						    case "internetdeliverableid":
							    _InternetDeliverableID = (Guid)dr[dc];
							    break;
						    case "type":
						        switch(dr[dc].ToString().ToLower().Trim()) {
                                    case "report":
                                        _Type = "Report";
                                        break;
                                    case "extract":
                                        _Type = "Extract";
                                        break;
                                    default:
							            _Type = dr[dc].ToString();
                                        break;
						        }
							    break;
						    case("deliverablename"):
							    _DeliverableName = dr[dc].ToString();
                                break;
                            case("groupname"):
							    _GroupName = dr[dc].ToString();
                                break;
                            case("description"):
							    _Description = dr[dc].ToString();
                                break;
                        }
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// Create an XML node of the current InternetDeliverable with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public XmlNode AsXMLNode(string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;

            try {

                // Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                // Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);

                // set each property as an attribute to the node
                ipoXmlLib.addAttribute(node, "InternetDeliverableID", this.InternetDeliverableID.ToString());
                ipoXmlLib.addAttribute(node, "Type", this.Type);
                ipoXmlLib.addAttribute(node, "DeliverableName", this.DeliverableName);
                ipoXmlLib.addAttribute(node, "GroupName", this.GroupName);
                ipoXmlLib.addAttribute(node, "Description", this.Description);
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }
    }

    ///// <summary>
    ///// 
    ///// </summary>
    //public class cDeliveryMethods : Dictionary<Guid, cDeliveryMethod> {

    //    ///// <summary>
    //    ///// 
    //    ///// </summary>
    //    //public cDeliveryMethods() {
    //    //}
    //}
}
