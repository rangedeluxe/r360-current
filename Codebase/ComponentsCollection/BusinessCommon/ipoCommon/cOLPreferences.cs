
/*********************************************************************
** Wausau
** Copyright � 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:     8/10/2011
*
* Purpose:  Description of functionality.
*
* Modification History
* 08/10/2011 CR 32263  WJS
*   -Initial release.
* CR 28865 JNE 08/26/2011
*   -Added MaximumQueriesSaved property.
* CR 43238 WJS 08/30/2011
*   -Added support for password notification from email address
* CR 46868 WJS 09/19/2011
*   -Added support for password notifciation subject.
* CR 47037 WJS 10/06/2011
*   -Added support for failed user challenge questions
* CR 33230 JMC 01/12/2012
*   -Added new ILogonPreferences interface.  cOLPreferences now uses this
*    interface.
* CR 54173 JNE 08/08/2012
*   -Added DisplayBatchNumberAndBatchIDResearch property
* WI 71427 CEJ 12/04/2012
*  - Add MaxPrintableRows property to OLPrefences
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
*********************************************************************************/
namespace WFS.RecHub.Common
{
    /// <summary>
    ///
    /// </summary>
    public interface ILogonPreferences {

        /// <summary>
        ///
        /// </summary>
        int SessionExpiration {
            get;
        }

        /// <summary>
        ///
        /// </summary>
        int MaxLogonAttempts {
            get;
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class cOLPreferences : ILogonPreferences
    {
        private int _MaxPrintableRows;
        private bool _DisplayScannedCheckOnline;
        private bool _DisplayScannedCheckResearch;
        private int _PasswordExpiration;
        private int _MaxLogonAttempts;
        private int _MaxSecurityUserQuestionAttempts;
        private bool _UseCutoff;
        private int _RecordsPerPage = 20;
        private int _LockboxSummaryRecordsPerPage = 20;
        private string _SiteKey = "";
        private bool _DisplayRemitterNameInPDF;
        private int _LockoutInactiveUserAccount = FFIECPasswordConstraints.LockoutInactiveUserAccountsMaxSize;
        private int _PasswordMinLength = FFIECPasswordConstraints.PasswordMinLength;
        private int _PasswordHistorySize = FFIECPasswordConstraints.NumberOfDaysPasswordHistory;
        private bool _UseCutoffResearch;            //  Display batch data after lockbox cutoff in CSR Research
        private bool _ViewProcessAheadResearch;     //  Allow viewing of items processed ahead of current processing date in CSR Research
        private bool _DisplayBatchNumberAndBatchIDResearch;

        private string _PasswordRequiredFormat =string.Empty;
        private OLFImageDisplayMode _CheckImageDisplayMode = OLFImageDisplayMode.BothSides;
        private OLFImageDisplayMode _DocumentImageDisplayMode = OLFImageDisplayMode.BothSides;
        private int _MaximumQueriesSaved;
        private int _ChangePasswordMinutesExpired;

        private string _PasswordNotificationFromAddress = string.Empty;
        private string _ChangePasswordNotificationSubject = string.Empty;
        private string _ForgotPasswordNotificationSubject = string.Empty;

        /// <summary>
        ///
        /// </summary>
        /// <param name="siteKeyStr"></param>
        public cOLPreferences(string siteKeyStr)
        {
            _SiteKey = siteKeyStr;
        }

        /// <summary>
        ///
        /// </summary>
        public int maxPrintableRows {
            get {
                return _MaxPrintableRows;
            }
            set {
                _MaxPrintableRows = value;
            }
        }

        /// <summary></summary>
        public string siteKey
        {
            get
            {
                return (_SiteKey);
            }
        }
        /// <summary>
        /// Session expiration in minutes.
        /// </summary>
        public int SessionExpiration { get; set; }
        /// <summary>
        ///
        /// </summary>
        public int PasswordExpiration
        {
            set
            {
                _PasswordExpiration = value;
            }
            get
            {
                return (_PasswordExpiration);
            }
        }
        /// <summary>
        ///
        /// </summary>
        public int MaxLogonAttempts
        {
            set
            {
                _MaxLogonAttempts = value;
            }
            get
            {
                return (_MaxLogonAttempts);
            }
        }
         /// <summary>
        ///
        /// </summary>
        public int MaxSecurityUserQuestionAttempts
        {
            set
            {
                _MaxSecurityUserQuestionAttempts = value;
            }
            get
            {
                return (_MaxSecurityUserQuestionAttempts);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public bool UseCutoff
        {
            set
            {
                _UseCutoff = value;
            }
            get
            {
                return (_UseCutoff);
            }
        }



        /// <summary>
        ///
        /// </summary>
        public int RecordsPerPage
        {
            set
            {
                _RecordsPerPage = value;
            }
            get
            {
                if (_RecordsPerPage > 0 && _RecordsPerPage <= 1000)
                {
                    return (_RecordsPerPage);
                }
                else
                {
                    return (20);
                }
            }
        }
        /// <summary>
        ///
        /// </summary>
        public int LockboxSummaryRecordsPerPage
        {
            set
            {
                _LockboxSummaryRecordsPerPage = value;
            }
            get
            {
                if (_LockboxSummaryRecordsPerPage > 0 && _LockboxSummaryRecordsPerPage <= 1000)
                {
                    return (_LockboxSummaryRecordsPerPage);
                }
                else
                {
                    return (20);
                }
            }
        }
        /// <summary>
        ///
        /// </summary>
        public int PasswordMinLength
        {
            set
            {
                _PasswordMinLength = value;
            }
            get
            {
                if (_PasswordMinLength > FFIECPasswordConstraints.PasswordMinLength)
                {
                    return (_PasswordMinLength);
                }
                else
                {
                    return (FFIECPasswordConstraints.PasswordMinLength);
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int PasswordHistorySize
        {
            set
            {
                _PasswordHistorySize = value;
            }
            get
            {
                if (_PasswordHistorySize < FFIECPasswordConstraints.NumberOfDaysPasswordHistory)
                {
                    return (_PasswordHistorySize);
                }
                else
                {
                    return (FFIECPasswordConstraints.NumberOfDaysPasswordHistory);
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int LockoutInactiveUserAccount
        {
            set
            {
                _LockoutInactiveUserAccount = value;
            }
            get
            {
                if (_LockoutInactiveUserAccount > FFIECPasswordConstraints.LockoutInactiveUserAccountsMaxSize)
                {
                    return (FFIECPasswordConstraints.LockoutInactiveUserAccountsMaxSize);
                }
                else
                {
                    return (_LockoutInactiveUserAccount);
                }
            }
        }




        /// <summary>
        ///
        /// </summary>
        public string PasswordRequiredFormat {
            set {
                _PasswordRequiredFormat = value;
            }
            get {
                return(_PasswordRequiredFormat);
            }
        }


        /// <summary>
        ///
        /// </summary>
        public string PasswordNotificationFromAddress {
            set {
                _PasswordNotificationFromAddress = value;
            }
            get {
                return (_PasswordNotificationFromAddress);
            }
        }

           /// <summary>
        ///
        /// </summary>
        public string ChangePasswordNotificationSubject {
            set {
                _ChangePasswordNotificationSubject = value;
            }
            get {
                return (_ChangePasswordNotificationSubject);
            }
        }
        /// <summary>
        ///
        /// </summary>
        public string ForgotPasswordNotificationSubject
        {
            set
            {
                _ForgotPasswordNotificationSubject = value;
            }
            get
            {
                return (_ForgotPasswordNotificationSubject);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int ChangePasswordMinutesExpired
        {
            set
            {
                _ChangePasswordMinutesExpired = value;
            }
            get
            {
                return (_ChangePasswordMinutesExpired);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public bool DisplayRemitterNameInPDF
        {
            set
            {
                _DisplayRemitterNameInPDF = value;
            }
            get
            {
                return (_DisplayRemitterNameInPDF);
            }
        }
        /// <summary>
        ///
        /// </summary>
        public OLFImageDisplayMode CheckImageDisplayMode
        {
            set
            {
                _CheckImageDisplayMode = value;
            }
            get
            {
                return (_CheckImageDisplayMode);
            }
        }
        /// <summary>
        ///
        /// </summary>
        public OLFImageDisplayMode DocumentImageDisplayMode
        {
            set
            {
                _DocumentImageDisplayMode = value;
            }
            get
            {
                return (_DocumentImageDisplayMode);
            }
        }
        /// <summary>
        ///
        /// </summary>
        public bool DisplayScannedCheckOnline
        {
            set
            {
                _DisplayScannedCheckOnline = value;
            }
            get
            {
                return (_DisplayScannedCheckOnline);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public bool DisplayScannedCheckResearch
        {
            set
            {
                _DisplayScannedCheckResearch = value;
            }
            get
            {
                return (_DisplayScannedCheckResearch);
            }
        }

        /// <summary>
        /// Use cutoff research
        /// </summary>
        public bool UseCutoffResearch
        {
            set
            {
                _UseCutoffResearch = value;
            }
            get
            {
                return (_UseCutoffResearch);
            }
        }

        /// <summary>
        /// View process ahead research
        /// </summary>
        public bool ViewProcessAheadResearch
        {
            set
            {
                _ViewProcessAheadResearch = value;
            }
            get
            {
                return (_ViewProcessAheadResearch);
            }
        }

        /// <summary>
        /// Maximum PreDef queries allowed per User.
        /// </summary>
        public int MaximumQueriesSaved
        {
            set
            {
                _MaximumQueriesSaved = value;
            }
            get
            {
                return(_MaximumQueriesSaved);
            }
        }

        /// <summary>
        /// Display BatchNumber and BatchID for Research
        /// </summary>
        public bool DisplayBatchNumberAndBatchIDResearch
        {
            set
            {
                _DisplayBatchNumberAndBatchIDResearch = value;
            }
            get
            {
                return(_DisplayBatchNumberAndBatchIDResearch);
            }
         }
    }
}
