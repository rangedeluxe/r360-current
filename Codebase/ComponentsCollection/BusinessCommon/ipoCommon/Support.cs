﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WFS.integraPAY.Online.Common {

    /// <summary>
    /// 
    /// </summary>
    public enum ActivityCodes {
        /// <summary></summary>
        acUnknown = 0,
        /// <summary></summary>
        acPageAccess = 1,
        /// <summary></summary>
        acViewImageFront = 11,
        /// <summary></summary>
        acViewImageBack = 12,
        /// <summary></summary>
        acViewAll = 13,
        /// <summary></summary>
        acViewSingle = 14,
        /// <summary></summary>
        acSearchResultsPrintView = 21,
        /// <summary></summary>
        acSearchResultsPdfView = 22,
        /// <summary></summary>
        acSearchResultsCsv = 23,
        /// <summary></summary>
        acSearchResultsZipDownload = 24,
        /// <summary></summary>
        acSearchResultsDownloadCsv = 25,
        /// <summary></summary>
        acSearchResultsDownloadXml = 26,
        /// <summary></summary>
        acSearchResultsDownloadHtml = 27,
        /// <summary></summary>
        acDownloadImageFront = 41,
        /// <summary></summary>
        acDownloadImageBack = 42
    }
}
