using System;
using System.Data;
using System.Xml;

/*******************************************************************************
*    Module: Common Session object
*  Filename: cSession.cs
*    Author: Joel Caples
*
* Revisions:
* ----------------------
* CR 18946 JMC 11/06/2006
*    -Created Class.
* CR 22115 JMC 01/26/2010
*    - Made UserID property writeable in support of Research
* WI 89972 CRG 03/01/2013
*	Modify ipoCommon for .NET 4.5 and NameSpace
******************************************************************************/
namespace WFS.RecHub.Common {

	/// <summary>
	/// Summary description for cSession.
	/// </summary>
	public class cSession
	{
        private Guid _SessionID = Guid.Empty;
        //private string _SiteKey = string.Empty;
        private Guid _EmulationID = Guid.Empty;
        private int _UserID = -1;
        private string _IPAddress = string.Empty;
        private string _BrowserType = string.Empty;
        private DateTime _LogonDateTime = DateTime.MinValue;
        private DateTime _LastPageServed = DateTime.MinValue;
        private int _PageCounter = 0;
        private bool _IsSuccess = false;
        private bool _IsSessionEnded = false;
        private string _LogonName = string.Empty;
        private Guid _OLCustomerID = Guid.Empty;
        private string _CustomerCode = string.Empty;
        private bool _IsRegistered = false;

        /// <summary></summary>
		public cSession() {
		}

        /// <summary></summary>
        public Guid SessionID {
            get {
                return(_SessionID);
            }
            set {
                _SessionID = value;
            }
        }

        ///// <summary></summary>
        //public string SiteKey {
        //    get {
        //        return(_SiteKey);
        //    }
        //    //set {
        //    //    _SiteKey = value;
        //    //}
        //}

        /// <summary></summary>
        public Guid EmulationID {
            get {
                return(_EmulationID);
            }
            set {
                _EmulationID = value;
            }
        }
        
        /// <summary></summary>
        public int UserID {
            get {
                return(_UserID);
            }
            set {
                _UserID = value;
            }
        }
        
        /// <summary></summary>
        public string IPAddress {
            get {
                return(_IPAddress);
            }
        }

        /// <summary></summary>
        public string BrowserType {
            get {
                return(_BrowserType);
            }
        }

        /// <summary></summary>
        public DateTime LogonDateTime {
            get {
                return(_LogonDateTime);
            }
        }

        /// <summary></summary>
        public DateTime LastPageServed {
            get {
                return(_LastPageServed);
            }
        }

        /// <summary></summary>
        public int PageCounter {
            get {
                return(_PageCounter);
            }
        }

        /// <summary></summary>
        public bool IsSuccess {
            get {
                return(_IsSuccess);
            }
        }

        /// <summary></summary>
        public bool IsSessionEnded {
            get {
                return(_IsSessionEnded);
            }
        }

        /// <summary></summary>
        public string LogonName {
            get {
                return(_LogonName);
            }
        }

        /// <summary></summary>
        public Guid OLCustomerID {
            get {
                return(_OLCustomerID);
            }
        }

        /// <summary></summary>
        public string CustomerCode {
            get {
                return(_CustomerCode);
            }
        }

        /// <summary></summary>
        public bool IsRegistered {
            get {
                return(_IsRegistered);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEmulationSession {
            get {
                return(_EmulationID != Guid.Empty);
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
                        case("sessionid"):
                            _SessionID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("userid"):
                            _UserID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("ipaddress"):
                            _IPAddress = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("browsertype"):
                            _BrowserType = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("logondatetime"):
                            _LogonDateTime = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("lastpageserved"):
                            _LastPageServed = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("pagecounter"):
                            _PageCounter = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case("issuccess"):
                            _IsSuccess = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case("issessionended"):
                            _IsSessionEnded = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case("logonname"):
                            _LogonName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("olcustomerid"):
                            _OLCustomerID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("customercode"):
                            _CustomerCode = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("isregistered"):
                            _IsRegistered = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <returns></returns>
        public XmlNode ToXml(XmlNode nodeParent) {

            XmlNode nodeSession;
            XmlAttribute att;

            nodeSession = nodeParent.OwnerDocument.CreateNode(XmlNodeType.Element, "Session", nodeParent.NamespaceURI);

            att = nodeParent.OwnerDocument.CreateAttribute("SessionID", nodeParent.NamespaceURI);
            att.Value = this.SessionID.ToString();
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("UserID", nodeParent.NamespaceURI);
            att.Value = this.UserID.ToString();
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("IPAddress", nodeParent.NamespaceURI);
            att.Value = this.IPAddress;
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("BrowserType", nodeParent.NamespaceURI);
            att.Value = this.BrowserType;
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("LogonDateTime", nodeParent.NamespaceURI);
            att.Value = this.LogonDateTime.ToString();
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("LastPageServed", nodeParent.NamespaceURI);
            att.Value = this.LastPageServed.ToString();
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("PageCounter", nodeParent.NamespaceURI);
            att.Value = this.PageCounter.ToString();
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("IsSuccess", nodeParent.NamespaceURI);
            att.Value = this.IsSuccess.ToString();
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("IsSessionEnded", nodeParent.NamespaceURI);
            att.Value = this.IsSessionEnded.ToString();
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("LogonName", nodeParent.NamespaceURI);
            att.Value = this.LogonName;
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("OLCustomerID", nodeParent.NamespaceURI);
            att.Value = this.OLCustomerID.ToString();
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("CustomerCode", nodeParent.NamespaceURI);
            att.Value = this.CustomerCode;
            nodeSession.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("IsRegistered", nodeParent.NamespaceURI);
            att.Value = this.IsRegistered.ToString();
            nodeSession.Attributes.Append(att);
            
            nodeParent.AppendChild(nodeSession);

            return(nodeSession);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeSession"></param>
        /// <returns></returns>
        public bool FromXml(XmlNode nodeSession) {

            try {
                foreach(XmlAttribute att in nodeSession.Attributes) {
                    switch(att.Name.ToLower()) {
                        case("sessionid"):
                            _SessionID = new Guid(att.Value);
                            break;
                        case("userid"):
                            _UserID = int.Parse(att.Value);
                            break;
                        case("ipaddress"):
                            _IPAddress = att.Value;
                            break;
                        case("browsertype"):
                            _BrowserType = att.Value;
                            break;
                        case("logondatetime"):
                            _LogonDateTime = DateTime.Parse(att.Value);
                            break;
                        case("lastpageserved"):
                            _LastPageServed = DateTime.Parse(att.Value);
                            break;
                        case("pagecounter"):
                            _PageCounter = int.Parse(att.Value);
                            break;
                        case("issuccess"):
                            _IsSuccess = bool.Parse(att.Value);
                            break;
                        case("issessionended"):
                            _IsSessionEnded = bool.Parse(att.Value);
                            break;
                        case("logonname"):
                            _LogonName = att.Value;
                            break;
                        case("olcustomerid"):
                            _OLCustomerID = new Guid(att.Value);
                            break;
                        case("customercode"):
                            _CustomerCode = att.Value;
                            break;
                        case("isregistered"):
                            _IsRegistered = bool.Parse(att.Value);
                            break;
                    }
                }
            } catch(Exception e) {
                throw(e);
            }

            return(true);
        }
    }
}
