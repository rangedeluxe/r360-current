using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*  Module:           CheckBOX Online Common Event Class Library
* 
*  Filename:         cEvent.cls
* 
*  Author:           Eric Goetzinger
* 
*  Description:
* 
*  Revisions:
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
**********************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cEvent {

	    //-------------------------------------------------------------------------------
	    // Private member variables declaration
	    //-------------------------------------------------------------------------------
	    private int _EventID;
	    private string _EventName;
	    private bool _EventIsActive;
	    private string _MessageDefault;
	    private bool _OnlineViewable;
	    private bool _OnlineModifiable;
        private List<cEventArgument> _EventArguments = null;

        /// <summary>
        /// 
        /// </summary>
	    public int EventID {
		    get { return(_EventID); }
		    set { _EventID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string EventName {
		    get { return(_EventName); }
		    set { _EventName = value.TrimEnd(); }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public bool EventIsActive {
		    get { return(_EventIsActive); }
		    set { _EventIsActive = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string MessageDefault {
		    get { return(_MessageDefault); }
		    set { _MessageDefault = value.TrimEnd(); }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public bool OnlineViewable {
		    get { return(_OnlineViewable); }
		    set { _OnlineViewable = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public bool OnlineModifiable {
		    get { return(_OnlineModifiable); }
		    set { _OnlineModifiable = value; }
	    }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
					    case "eventid":
						    _EventID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
						    break;
					    case "eventname":
						    _EventName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
						    break;
					    case "eventisactive":
						    _EventIsActive = ipoLib.NVL(ref dr, dc.ColumnName, false);
						    break;
					    case "messagedefault":
						    _MessageDefault = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
						    break;
					    case "onlineviewable":
						    _OnlineViewable = ipoLib.NVL(ref dr, dc.ColumnName, false);
						    break;
					    case "onlinemodifiable":
						    _OnlineModifiable = ipoLib.NVL(ref dr, dc.ColumnName, false);
						    break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
	    public List<cEventArgument> EventArguments {
		    get {
			    if (_EventArguments == null) {
				    _EventArguments = new List<cEventArgument>();
			    }

			    return(_EventArguments);
		    }
		    set { 
		        _EventArguments = value;
		    }
	    }


	    /*//-------------------------------------------------------------------------------
	    // Purpose:  Populate class object from recordset
	    // Inputs:
	    // Returns:
	    //-------------------------------------------------------------------------------
	    public cEventArgument GetEventArgument(short EventArgumentID)
	    {
		    cEventArgument functionReturnValue = null;
		    if (EventArguments.Item((string)EventArgumentID) == null)
		    {
			    //UPGRADE_NOTE: Object GetEventArgument may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			    functionReturnValue = null;
		    }
		    else
		    {
			    functionReturnValue = EventArguments.Item((string)EventArgumentID);
		    }
		    return functionReturnValue;
	    }
        */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EventArgumentID"></param>
        /// <param name="EventID"></param>
        /// <param name="EventArgumentDescription"></param>
        /// <param name="EventArgumentCode"></param>
        /// <returns></returns>
	    public bool SetEventArgument(int EventArgumentID, int EventID, string EventArgumentDescription, string EventArgumentCode) {

		    cEventArgument objEventArgument;
		    bool bolRetVal = false;
		    bool bolArgExists;

            try {

                if(_EventArguments == null) {
                    _EventArguments = new List<cEventArgument>();
                }

                bolArgExists = false;
                for(int i=0;i<_EventArguments.Count;++i) {
                    if(_EventArguments[i].EventArgumentID == EventArgumentID) {

			            EventArguments[i].EventID = EventID;
			            EventArguments[i].EventArgumentDescription = EventArgumentDescription;
			            EventArguments[i].EventArgumentCode = EventArgumentCode;

                        bolArgExists = true;
			            bolRetVal = true;

                        break;
                    }
                }

		        if(!bolArgExists) {

                    objEventArgument = new cEventArgument();

                    objEventArgument.EventArgumentID = EventArgumentID;
                    objEventArgument.EventID = EventID;
                    objEventArgument.EventArgumentDescription = EventArgumentDescription;
                    objEventArgument.EventArgumentCode = EventArgumentCode;

			        EventArguments.Add(objEventArgument);

                    bolRetVal = true;
		        }

		    } catch(Exception) {
		        bolRetVal = false;
            }

            return(bolRetVal);
	    }

        /// <summary>
        /// Create an XML node of the current Event with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public XmlNode AsXMLNode(string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;
            XmlNode nodeEventArgs;

            try {

                // Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                // Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName, string.Empty);

                // set each property as an attribute to the node
                ipoXmlLib.addAttribute(node, "EventID", this.EventID.ToString());
                ipoXmlLib.addAttribute(node, "EventIsActive", (this.EventIsActive ? "1" : "0"));
                ipoXmlLib.addAttribute(node, "EventName", this.EventName);
                ipoXmlLib.addAttribute(node, "MessageDefault", this.MessageDefault);
                ipoXmlLib.addAttribute(node, "OnlineModifiable", (this.OnlineModifiable ? "1" : "0"));
                ipoXmlLib.addAttribute(node, "OnlineViewable", (this.OnlineViewable ? "1" : "0"));

                foreach(cEventArgument evtarg in this.EventArguments) {
                    nodeEventArgs = ipoXmlLib.addElement(node, "EventArgument");
                    ipoXmlLib.addAttribute(nodeEventArgs, "EventArgumentCode", evtarg.EventArgumentCode);
                    ipoXmlLib.addAttribute(nodeEventArgs, "EventArgumentDescription", evtarg.EventArgumentDescription);
                    ipoXmlLib.addAttribute(nodeEventArgs, "EventArgumentID", evtarg.EventArgumentID.ToString());
                }
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public class cEvents : Dictionary<int, cEvent> {

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <returns></returns>
        //public cEvent this[ int key ]  {
        //    get  {
        //        return( (cEvent) Dictionary[key] );
        //    }
        //    set  {
        //        Dictionary[key] = value;
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //public ICollection Keys  {
        //    get  {
        //        return( Dictionary.Keys );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //public ICollection Values  {
        //    get  {
        //        return( Dictionary.Values );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="value"></param>
        //public void Add( int key, cEvent value )  {
        //    Dictionary.Add( key, value );
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <returns></returns>
        //public bool Contains( int key )  {
        //    return( Dictionary.Contains( key ) );
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        //public void Remove( int key )  {
        //    Dictionary.Remove( key );
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="value"></param>
        //protected override void OnInsert(Object key, Object value)  {
        //    if (key.GetType() != typeof(System.Int32)) {
        //        throw new ArgumentException( "key must be of type Int32.", "key" );
        //    }

        //    if ( value.GetType() != typeof(cEvent) ) {
        //        throw new ArgumentException( "value must be of type cEvent.", "value" );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="value"></param>
        //protected override void OnRemove( Object key, Object value )  {
        //    if ( key.GetType() != typeof(System.Int32) ) {
        //        throw new ArgumentException( "key must be of type Int32.", "key" );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="oldValue"></param>
        ///// <param name="newValue"></param>
        //protected override void OnSet( Object key, Object oldValue, Object newValue )  {
        //    if (key.GetType() != typeof(System.Int32)) {
        //        throw new ArgumentException( "key must be of type Int32.", "key" );
        //    }

        //    if (newValue.GetType() != typeof(cEvent)) {
        //        throw new ArgumentException( "newValue must be of type cEvent.", "newValue" );
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="key"></param>
        ///// <param name="value"></param>
        //protected override void OnValidate( Object key, Object value )  {
        //    if ( key.GetType() != typeof(System.Int32) ) {
        //        throw new ArgumentException( "key must be of type Int32.", "key" );
        //    }

        //    if (value.GetType() != typeof(cEvent) ) {
        //        throw new ArgumentException( "value must be of type cEvent.", "value" );
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool LoadDataTable(DataTable dt) {

            cEvent objEvent;

            foreach(DataRow dr in dt.Rows) {
                objEvent = new cEvent();
                objEvent.LoadDataRow(dr);
                this.Add(objEvent.EventID, objEvent);
            }

            return(true);
        }
    }
}
