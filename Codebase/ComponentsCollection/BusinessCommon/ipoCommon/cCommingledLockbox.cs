using System;
using System.Data;
using System.Xml;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     03/23/2010
*
* Purpose:  
*
* Modification History
* CR 28738 JMC 03/23/2010
*    - Created File.
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cCommingledLockbox : cLockbox {

        private string _DestLockboxIdentifierLabel = string.Empty;
        private string _DestLockboxIdentifier = string.Empty;


        /// <summary>
        /// 
        /// </summary>
        public cCommingledLockbox() : base() {

        }

        /// <summary>
        /// 
        /// </summary>
        public string DestLockboxIdentifierLabel {
            get{return(_DestLockboxIdentifierLabel);}
            set{_DestLockboxIdentifierLabel=value;}
        }

        /// <summary>
        /// 
        /// </summary>
        public string DestLockboxIdentifier {
            get{return(_DestLockboxIdentifier);}
            set{_DestLockboxIdentifier=value;}
        }

        
        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public override bool LoadDataRow(DataRow dr) {

            base.LoadDataRow(dr);

            foreach(DataColumn dc in dr.Table.Columns) {
                if(!dr.IsNull(dc)) {
                    switch(dc.ColumnName.ToLower()) {
                        case("destlockboxidentifierlabel"):
                            _DestLockboxIdentifierLabel = dr[dc].ToString();
                            break;
                        case("destlockboxidentifier"):
                            _DestLockboxIdentifier = dr[dc].ToString();
                            break;
                    }
                }
            }
            return(true);
        }

        /// <summary>
        /// Create an XML node of the current Lockbox with properties as attributes
        /// </summary>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public override XmlNode AsXMLNode() {
            return(AsXMLNode("Record"));
        }

        /// <summary>
        /// Create an XML node of the current Lockbox with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public override XmlNode AsXMLNode(string NodeName) {
            
            XmlNode node;

            try {

                node = base.AsXMLNode(NodeName);
                if(node != null) {

                    // set each property as an attribute to the node
                    ipoXmlLib.addAttribute(node, "DestLockboxIdentifierLabel", this.DestLockboxIdentifierLabel);
                    ipoXmlLib.addAttribute(node, "DestLockboxIdentifier", this.DestLockboxIdentifier);
                }
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }

        /// <summary>
        /// Populate properties of a cLockbox object from an xml node generated
        /// by AsXMLNode() above.
        /// </summary>
        /// <param name="node"></param>
        /// <returns>
        /// IXMLDomNode
        ///           Format: <NodeName Property="Value"/>
        /// Returns:  True/False 
        ///</returns>
        public override bool FromXMLNode(XmlNode node) {
            
            base.FromXMLNode(node);
            
            try {
                //loop through the attributes in the node reference
                foreach(XmlAttribute att in node.Attributes) {
                    //using the attribute name, assign the value to the corresponding cLockbox property
                    switch(att.Name.ToLower()) {
                        case "DestLockboxIdentifierLabel":
                            _DestLockboxIdentifierLabel = att.Value;
                            break;
                        case "DestLockboxIdentifier":
                            _DestLockboxIdentifier = att.Value;
                            break;
                    }
                }
                
                return(true);
            } catch(Exception) {
                return(false);
            }
        }
    }
}
