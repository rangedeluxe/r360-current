using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

/******************************************************************************
** Wausau
** Copyright � 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2010.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     
*
* Purpose:  
*
* Modification History
* CR 46139 JCS 09/08/2011
*    - Added InvoiceBalancingOption property
* CR 50236 JNE 02/08/2012
*    - Added HOA
* CR 54173 JNE 08/08/2012
*    - Added DisplayBatchID 
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
* WI 149727 TWE 06/23/2014
*    change key for clockbox object to olworkgroupsid
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cOLLockbox : cLockbox {

        private Guid _OLCustomerID = Guid.Empty;
        private Guid _OLLockboxID = Guid.Empty;
        private int _OLWorkGroupsID = -1;
        private string _DisplayName = "";
        private int _MaximumSearchDays = -1; //CR 29099 3/25/2010 JNE
        private OLFImageDisplayMode _DocumentImageDisplayMode = OLFImageDisplayMode.BothSides;
        private OLFImageDisplayMode _CheckImageDisplayMode = OLFImageDisplayMode.BothSides;
        private InvoiceBalancingOptions _InvoiceBalancingOption = InvoiceBalancingOptions.BalancingNotRequired;
        private bool _HOA = false; //CR 50236 2/8/2012 JNE
        private bool _DisplayBatchID = false; // CR 54173 07/24/2012 JNE


        /// <summary>
        /// 
        /// </summary>
        public cOLLockbox() : base() {

        }

        /// <summary>
        /// 
        /// </summary>
        public Guid OLCustomerID {
            get {
                return(_OLCustomerID);
            }
            set {
                _OLCustomerID = value;
            }
        }

        /// <summary>
        /// Lockbox.OLWorkGroupsID
        /// </summary>
        public int OLWorkGroupsID
        {
            get { return _OLWorkGroupsID; }
            set { _OLWorkGroupsID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid OLLockboxID {
            get {
                return(_OLLockboxID);
            }
            set {
                _OLLockboxID = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName{
            get{return(_DisplayName);}
            set{_DisplayName=value;}
        }

        /// <summary>
        /// 
        /// </summary>
        public int MaximumSearchDays{
            get{return(_MaximumSearchDays);}
            set{_MaximumSearchDays=value;}
        }

        /// <summary>
        /// 
        /// </summary>
        public OLFImageDisplayMode DocumentImageDisplayMode{
            get{return(_DocumentImageDisplayMode);}
            set{_DocumentImageDisplayMode=value;}
        }

        /// <summary>
        /// 
        /// </summary>
        public OLFImageDisplayMode CheckImageDisplayMode{
            get{return(_CheckImageDisplayMode);}
            set{_CheckImageDisplayMode=value;}
        }

        /// <summary>
        /// 
        /// </summary>
        public InvoiceBalancingOptions InvoiceBalancingOption {
            get {return(_InvoiceBalancingOption);}
            set {_InvoiceBalancingOption=value;}
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HOA {
            get {return(_HOA);}
            set {_HOA=value;}
        }

        /// <summary>
        /// 
        /// </summary>
        public bool DisplayBatchID {
            get {return(_DisplayBatchID);}
            set {_DisplayBatchID=value;}
        }
        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public override bool LoadDataRow(DataRow dr) {

            base.LoadDataRow(dr);

            foreach(DataColumn dc in dr.Table.Columns) {
                if(!dr.IsNull(dc)) {
                    switch(dc.ColumnName.ToLower()) {
                        case("olcustomerid"):
                            _OLCustomerID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("ollockboxid"):
                            _OLLockboxID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case ("olworkgroupid"):
                            this.OLWorkGroupsID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("displayname"):
                            _DisplayName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("maximumsearchdays"):
                            _MaximumSearchDays = ipoLib.NVL(ref dr, dc.ColumnName, -1); //CR 29099 3/25/10 JNE
                            break;
                        case("documentimagedisplaymode"):
                            try {
                                _DocumentImageDisplayMode = (OLFImageDisplayMode)ipoLib.NVL(ref dr,dc.ColumnName,(byte)OLFImageDisplayMode.Undefined);
                            } catch(Exception) {
                                //TODO: Throw Message to log: "Invalid Document Image Display Mode: " + 
                                //                            dr[dc].ToString() + ".  Defaulting to 'Undefined'."
                                _DocumentImageDisplayMode = OLFImageDisplayMode.Undefined;
                            }
                            break;
                        case("checkimagedisplaymode"):
                            try {
                                _CheckImageDisplayMode = (OLFImageDisplayMode)ipoLib.NVL(ref dr,dc.ColumnName,(byte)OLFImageDisplayMode.Undefined);
                            } catch(Exception) {
                                //TODO: Throw Message to log: "Invalid Checks Image Display Mode: " + 
                                //                            dr[dc].ToString() + ".  Defaulting to 'Undefined'."
                                _CheckImageDisplayMode = OLFImageDisplayMode.Undefined;
                            }
                            break;
                        case ("invoicebalancingoption"):
                            try {
                                _InvoiceBalancingOption = (InvoiceBalancingOptions) ipoLib.NVL(ref dr, dc.ColumnName, (byte) InvoiceBalancingOptions.BalancingNotRequired);
                            } catch (Exception) {
                                _InvoiceBalancingOption = InvoiceBalancingOptions.BalancingNotRequired;
                            }
                            break;
                        case ("hoa"):
                            _HOA = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case ("displaybatchid"):
                            _DisplayBatchID = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                    }
                }
            }
            return(true);
        }

        /// <summary>
        /// Create an XML node of the current Lockbox with properties as attributes
        /// </summary>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public override XmlNode AsXMLNode() {
            return(AsXMLNode("Record"));
        }

        /// <summary>
        /// Create an XML node of the current Lockbox with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public override XmlNode AsXMLNode(string NodeName) {
            
            XmlNode node;

            try {

                node = base.AsXMLNode(NodeName);
                if(node != null) {

                    // set each property as an attribute to the node
                    ipoXmlLib.addAttribute(node, "OLLockboxID", this.OLLockboxID.ToString());
                    ipoXmlLib.addAttribute(node, "OLCustomerID", this.OLCustomerID.ToString());
                    ipoXmlLib.addAttribute(node, "OLWorkGroupsID", this.OLWorkGroupsID.ToString());
                    ipoXmlLib.addAttribute(node, "InvoiceBalancingOption", ((int) this._InvoiceBalancingOption).ToString());
                }
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }


        /// <summary>
        /// Populate properties of a cLockbox object from an xml node generated
        /// by AsXMLNode() above.
        /// </summary>
        /// <param name="node"></param>
        /// <returns>
        /// IXMLDomNode
        ///           Format: <NodeName Property="Value"/>
        /// Returns:  True/False 
        ///</returns>
        public override bool FromXMLNode(XmlNode node) {
            
            base.FromXMLNode(node);
            
            try {
                //loop through the attributes in the node reference
                foreach(XmlAttribute att in node.Attributes) {
                    //using the attribute name, assign the value to the corresponding cLockbox property
                    switch(att.Name.ToLower()) {
                        case "ollockboxid":
                            _OLLockboxID = new Guid(att.Value);
                            break;
                        case "olcustomerid":
                            _OLCustomerID = new Guid(att.Value);
                            break;
                        case "olworkgroupsid":
                            this.OLWorkGroupsID = int.Parse(att.Value);
                            break;
                        // load InvoiceBalancingOption from Xml
                        //case "InvoiceBalancingOption":
                        //    _InvoiceBalancingOption = (InvoiceBalancingOptions)att.Value;
                    }
                }
                
                return(true);
            } catch(Exception) {
                return(false);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class cOLLockboxes : Dictionary<int, cOLLockbox> {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="OLCustomerID"></param>
        /// <param name="OLLockboxID"></param>
        /// <param name="ShortName"></param>
        /// <param name="LongName"></param>
        /// <returns></returns>
        public cOLLockbox Add(Guid OLCustomerID, int OLWorkgroupsID, string ShortName, string LongName) {

            cOLLockbox objLockbox = new cOLLockbox();

            objLockbox.OLCustomerID = OLCustomerID;
            //objLockbox.OLLockboxID = OLLockboxID;
            objLockbox.OLWorkGroupsID = OLWorkgroupsID;
            objLockbox.ShortName = ShortName;
            objLockbox.LongName = LongName;

            this.Add(objLockbox.OLWorkGroupsID, objLockbox);

            return(objLockbox);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool LoadDataTable(DataTable dt) {

            cOLLockbox objOLLockbox;

            foreach(DataRow dr in dt.Rows) {
                objOLLockbox = new cOLLockbox();
                objOLLockbox.LoadDataRow(dr);
                this.Add(objOLLockbox.OLWorkGroupsID, objOLLockbox);
            }

            return(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>
        /// Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///         </NodeName></returns>
        public XmlNode AsXMLNode() {
            return(AsXMLNode("Lockboxes", "Recordset"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="AttributeName"></param>
        /// <returns>
        /// Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///         </NodeName></returns>
        public XmlNode AsXMLNode(string AttributeName) {
            return(AsXMLNode(AttributeName, "Recordset"));
        }

        /// <summary>
        /// Create an XML node of the current Lockboxes collection
        /// </summary>
        /// <param name="AttributeName"></param>
        /// <param name="NodeName"></param>
        /// <returns>
        /// Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///         </NodeName></returns>
        public XmlNode AsXMLNode(string AttributeName, string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;
            
            try {
                //Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                //Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);
                    
                //Add the Name attribute to the node
                ipoXmlLib.addAttribute(node, "Name", AttributeName);
                
                //Add each permission as a child node
                foreach(cOLLockbox ollockbox in this.Values) {
                    node.AppendChild(xmlDoc.ImportNode(ollockbox.AsXMLNode(), true));
                }
                
                //return reference to node
                return(node);
            
            } catch(Exception ex) {
                throw(ex);
            }
        }

        /// <summary>
        /// Populate a cLockboxes collection w/ cLockbox objects from an xml node
        /// built using the AsXMLNode function above.
        /// </summary>
        /// <param name="node">
        /// IXMLDomNode
        ///           Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///                   </NodeName>
        /// </param>
        /// <returns>
        /// True/False 
        ///</returns>
        public bool FromXMLNode(XmlNode node) {

            cOLLockbox objOLLockbox;
            
            try {
            
                //loop through each child node in the node reference
                foreach(XmlNode n in node.ChildNodes) {
                    //create a new cLockbox object reference
                    objOLLockbox = new cOLLockbox();
                    
                    //assign the cLockbox properties from the child node reference
                    if(objOLLockbox.FromXMLNode(n)) {
                        //assign the cLockbox to the collection
                        this.Add(objOLLockbox.OLWorkGroupsID, objOLLockbox);
                    }
                }
                
                return(true);
            } catch(Exception) {
                return(false);
            }
        }

    }

}
