using System;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 11/06/2006
*
* Purpose:	Interface for the Notification File
*
* Modification History
* CR 18946 JMC 11/06/2006
*    -Created Class.
* CR 47037 WJS 10/6/2011
*    -Update to include FailedSecurityQuestionAttempts
* CR 50158 JMC 03/15/2012
*    -Add dynamic branding to the OLTA Logon page.
* CR 54442 JCS 07/24/2012
*    -Added ExternalIDHash as new class member
* WI 89972 CRG 03/01/2013
*	Modify ipoCommon for .NET 4.5 and NameSpace
******************************************************************************/
namespace WFS.RecHub.Common {

	/// <summary>
	/// Summary description for cUser.
	/// </summary>
	public class cUser
	{
        private int _UserID = -1;
        private int _BankID = -1;
        private int _CustomerID = -1;
        private string _LogonName = string.Empty;
        private string _FirstName = string.Empty;
        private string _MiddleInitial = string.Empty;
        private string _LastName = string.Empty;
        private bool _SuperUser = false;
        private DateTime _PasswordSet = DateTime.MinValue;
        private bool _IsFirstTime = false;
        private DateTime _CreationDate = DateTime.MinValue;
        private string _CreatedBy = string.Empty;
        private DateTime _DateLastUpdated = DateTime.MinValue;
        private string _LastUpdatedBy = string.Empty;
        private byte _FailedLogonAttempts = 0;
        private int _FailedSecurityQuestionAttempts = 0;
        private string _EmailAddress = string.Empty;
        private bool _IsActive = false;
        private Guid _ContactID = Guid.Empty;
        private Guid _OLCustomerID = Guid.Empty;
        private string _CustomerCode = string.Empty;
        private byte _UserType = 99;
        private string _ExternalID1 = string.Empty;
        private string _ExternalID2 = string.Empty;
        private int _SecretQuestionCount = 0;
        private string _ExternalIDHash = string.Empty;
        
        private bool _HasBeenAuthenticated = false;

        /// <summary>
        /// 
        /// </summary>
		public cUser() {
		}

        /// <summary></summary>
        public int UserID {
            get {
                return(_UserID);
            }
            set {
                _UserID = value;
            }
        }

        /// <summary></summary>
        public int BankID {
            get {
                return(_BankID);
            }
            set {
                _BankID = value;
            }
        }

        /// <summary></summary>
        public int CustomerID {
            get {
                return(_CustomerID);
            } 
            set {
                _CustomerID = value;
            }
        }

        /// <summary></summary>
        public string LogonName {
            get {
                return(_LogonName);
            }
            set {
                _LogonName = value;
            }
        }

        /// <summary></summary>
        public string FirstName {
            get {
                return(_FirstName);
            }
            set {
                _FirstName = value;
            }
        }

        /// <summary></summary>
        public string MiddleInitial {
            get {
                return(_MiddleInitial);
            }
            set {
                _MiddleInitial = value;
            }
        }

        /// <summary></summary>
        public string LastName {
            get {
                return(_LastName);
            }
            set {
                _LastName = value;
            }
        }

        /// <summary></summary>
        public bool SuperUser {
            get {
                return(_SuperUser);
            }
            set {
                _SuperUser = value;
            }
        }

        /// <summary></summary>
        public DateTime PasswordSet {
            get {
                return(_PasswordSet);
            }
            set {
                _PasswordSet = value;
            }
        }

        /// <summary></summary>
        public bool IsFirstTime {
            get {
                return(_IsFirstTime);
            }
            set {
                _IsFirstTime = value;
            }
        }

        /// <summary></summary>
        public DateTime CreationDate {
            get {
                return(_CreationDate);
            }
            set {
                _CreationDate = value;
            }
        }

        /// <summary></summary>
        public string CreatedBy {
            get {
                return(_CreatedBy);
            }
            set {
                _CreatedBy = value;
            }
        }

        /// <summary></summary>
        public DateTime DateLastUpdated {
            get {
                return(_DateLastUpdated);
            }
            set {
                _DateLastUpdated = value;
            }
        }

        /// <summary></summary>
        public string LastUpdatedBy {
            get {
                return(_LastUpdatedBy);
            }
            set {
                _LastUpdatedBy = value;
            }
        }

        /// <summary></summary>
        public byte FailedLogonAttempts {
            get {
                return(_FailedLogonAttempts);
            }
            set {
                _FailedLogonAttempts = value;
            }
        }

        /// <summary></summary>
        public int FailedSecurityQuestionAttempts {
            get {
                return (_FailedSecurityQuestionAttempts);
            }
            set {
                _FailedSecurityQuestionAttempts = value;
            }
        }

        /// <summary></summary>
        public string EmailAddress {
            get {
                return(_EmailAddress);
            }
            set {
                _EmailAddress = value;
            }
        }

        /// <summary></summary>
        public bool IsActive {
            get {
                return(_IsActive);
            }
            set {
                _IsActive = value;
            }
        }

        /// <summary></summary>
        public Guid ContactID {
            get {
                return(_ContactID);
            }
            set {
                _ContactID = value;
            }
        }

        /// <summary></summary>
        public Guid OLCustomerID {
            get {
                return(_OLCustomerID);
            }
            set {
                _OLCustomerID = value;
            }
        }

        /// <summary></summary>
        public string CustomerCode {
            get {
                return(_CustomerCode);
            }
            set {
                _CustomerCode = value;
            }
        }

//TODO: Change DataType to UDT.
        /// <summary></summary>
        public byte UserType {
            get {
                return(_UserType);
            }
            set {
                _UserType = value;
            }
        }

        /// <summary></summary>
        public string ExternalID1 {
            get {
                return(_ExternalID1);
            }
            set {
                _ExternalID1 = value;
            }
        }

        /// <summary></summary>
        public string ExternalID2 {
            get {
                return(_ExternalID2);
            }
            set {
                _ExternalID2 = value;
            }
        }
        
        /// <summary></summary>
        public bool HasBeenAuthenticated {
            get {
                return(_HasBeenAuthenticated);
            }
            set {
                _HasBeenAuthenticated = value;
            }
        }
        
        /// <summary></summary>
        public int SecretQuestionCount {
            get {
                return(_SecretQuestionCount);
            }
            set {
                _SecretQuestionCount = value;
            }
        }

        /// <summary></summary>
        public string ExternalIDHash {
            get {
                return (_ExternalIDHash);
            }
            set {
                _ExternalIDHash = value;
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(ref DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
                        case("userid"):
                            _UserID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("bankid"):
                            _BankID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("customerid"):
                            _CustomerID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("logonname"):
                            _LogonName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("firstname"):
                            _FirstName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("middleinitial"):
                            _MiddleInitial = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("lastname"):
                            _LastName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("superuser"):
                            _SuperUser = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case("passwordset"):
                            _PasswordSet = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("isfirsttime"):
                            _IsFirstTime = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case("creationdate"):
                            _CreationDate = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("createdby"):
                            _CreatedBy = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("datelastupdated"):
                            _DateLastUpdated = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("lastupdatedby"):
                            _LastUpdatedBy = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("failedlogonattempts"):
                            _FailedLogonAttempts = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
                            break;
                        case("emailaddress"):
                            _EmailAddress = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("isactive"):
                            _IsActive = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case("contactid"):
                            _ContactID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("olcustomerid"):
                            _OLCustomerID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("customercode"):
                            _CustomerCode = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("usertype"):
                            _UserType = ipoLib.NVL(ref dr, dc.ColumnName, (byte)99);
                            break;
                        case("externalid1"):
                            _ExternalID1 = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty); 
                            break;
                        case("externalid2"):
                            _ExternalID2 = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("hasbeenauthenticated"):
                            _HasBeenAuthenticated = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case("secretquestioncount"):
                            _SecretQuestionCount = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case ("failedsecurityquestionattempts"):
                            _FailedSecurityQuestionAttempts = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
                            break;
                        case ("externalidhash"):
                            _ExternalIDHash = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <returns></returns>
        public XmlNode ToXml(XmlNode nodeParent) {
            return(this.ToXml(nodeParent, "User"));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public XmlNode ToXml(XmlNode nodeParent, string nodeName) {

            XmlNode nodeUser;
            XmlAttribute att;

            nodeUser = nodeParent.OwnerDocument.CreateNode(XmlNodeType.Element, nodeName, nodeParent.NamespaceURI);

            att = nodeParent.OwnerDocument.CreateAttribute("UserID", nodeParent.NamespaceURI);
            att.Value = this.UserID.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("BankID", nodeParent.NamespaceURI);
            att.Value = this.BankID.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("CustomerID", nodeParent.NamespaceURI);
            att.Value = this.CustomerID.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("LogonName", nodeParent.NamespaceURI);
            att.Value = this.LogonName;
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("FirstName", nodeParent.NamespaceURI);
            att.Value = this.FirstName;
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("MiddleInitial", nodeParent.NamespaceURI);
            att.Value = this.MiddleInitial;
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("LastName", nodeParent.NamespaceURI);
            att.Value = this.LastName;
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("IsSuperUser", nodeParent.NamespaceURI);
            att.Value = this.SuperUser.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("PasswordSet", nodeParent.NamespaceURI);
            att.Value = this.PasswordSet.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("IsFirstTime", nodeParent.NamespaceURI);
            att.Value = this.IsFirstTime.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("CreationDate", nodeParent.NamespaceURI);
            att.Value = this.CreationDate.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("CreatedBy", nodeParent.NamespaceURI);
            att.Value = this.CreatedBy;
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("DateLastUpdated", nodeParent.NamespaceURI);
            att.Value = this.DateLastUpdated.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("LastUpdatedBy", nodeParent.NamespaceURI);
            att.Value = this.LastUpdatedBy;
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("FailedLogonAttempts", nodeParent.NamespaceURI);
            att.Value = this.FailedLogonAttempts.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("EmailAddress", nodeParent.NamespaceURI);
            att.Value = this.EmailAddress;
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("IsActive", nodeParent.NamespaceURI);
            att.Value = this.IsActive.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("ContactID", nodeParent.NamespaceURI);
            att.Value = this.ContactID.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("OLCustomerID", nodeParent.NamespaceURI);
            att.Value = this.OLCustomerID.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("CustomerCode", nodeParent.NamespaceURI);
            att.Value = this.CustomerCode.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("UserType", nodeParent.NamespaceURI);
            att.Value = this.UserType.ToString();
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("ExternalID1", nodeParent.NamespaceURI);
            att.Value = this.ExternalID1;
            nodeUser.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("ExternalID2", nodeParent.NamespaceURI);
            att.Value = this.ExternalID2;
            nodeUser.Attributes.Append(att);
                       
            att = nodeParent.OwnerDocument.CreateAttribute("HasBeenAuthenticated", nodeParent.NamespaceURI);
            att.Value = this.HasBeenAuthenticated.ToString();
            nodeUser.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("SecretQuestionCount", nodeParent.NamespaceURI);
            att.Value = this.SecretQuestionCount.ToString();
            nodeUser.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("FailedSecurityQuestionsAttempts", nodeParent.NamespaceURI);
            att.Value = this.FailedSecurityQuestionAttempts.ToString();
            nodeUser.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("ExternalIDHash", nodeParent.NamespaceURI);
            att.Value = this.ExternalIDHash;
            nodeUser.Attributes.Append(att);

            nodeParent.AppendChild(nodeUser);

            return(nodeUser);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeUser"></param>
        /// <returns></returns>
        public bool FromXml(XmlNode nodeUser) {

            try {
                foreach(XmlAttribute att in nodeUser.Attributes) {
                    switch(att.Name.ToLower()) {
                        case("userid"):
                            _UserID = int.Parse(att.Value);
                            break;
                        case("bankid"):
                            _BankID = int.Parse(att.Value);
                            break;
                        case("customerid"):
                            _CustomerID = int.Parse(att.Value);
                            break;
                        case("logonname"):
                            _LogonName = att.Value;
                            break;
                        case("firstname"):
                            _FirstName = att.Value;
                            break;
                        case("middleinitial"):
                            _MiddleInitial = att.Value;
                            break;
                        case("lastname"):
                            _LastName = att.Value;
                            break;
                        case("issuperuser"):
                            _SuperUser = bool.Parse(att.Value);
                            break;
                        case("passwordset"):
                            _PasswordSet = DateTime.Parse(att.Value);
                            break;
                        case("isfirsttime"):
                            _IsFirstTime = bool.Parse(att.Value);
                            break;
                        case("creationdate"):
                            _CreationDate = DateTime.Parse(att.Value);
                            break;
                        case("createdby"):
                            _CreatedBy = att.Value;
                            break;
                        case("datelastupdated"):
                            _DateLastUpdated = DateTime.Parse(att.Value);
                            break;
                        case("lastupdatedby"):
                            _LastUpdatedBy = att.Value;
                            break;
                        case("failedlogonattempts"):
                            _FailedLogonAttempts = byte.Parse(att.Value);
                            break;
                        case("emailaddress"):
                            _EmailAddress = att.Value;
                            break;
                        case("isactive"):
                            _IsActive = bool.Parse(att.Value);
                            break;
                        case("contactid"):
                            _ContactID = new Guid(att.Value);
                            break;
                        case("olcustomerid"):
                            _OLCustomerID = new Guid(att.Value);
                            break;
                        case("customercode"):
                            _CustomerCode = att.Value;
                            break;
                        case("usertype"):
                            _UserType = byte.Parse(att.Value);
                            break;
                        case("externalid1"):
                            _ExternalID1 = att.Value; 
                            break;
                        case("externalid2"):
                            _ExternalID2 = att.Value;
                            break;
                        case("hasbeenauthenticated"):
                            _HasBeenAuthenticated = bool.Parse(att.Value);
                            break;
                        case("secretquestioncount"):
                            _SecretQuestionCount = int.Parse(att.Value);
                            break;
                        case("failedsecurityquestionsattempts"):
                            _FailedSecurityQuestionAttempts = int.Parse(att.Value);
                            break;
                        case ("externalidhash"):
                            _ExternalIDHash = att.Value;
                            break;
                    }
                }
            } catch(Exception e) {
                throw(e);
            }

            return(true);
        }
	}
}
