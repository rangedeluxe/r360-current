using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: Common Lockbox object
*
* Modification History
* CR  JMC 
*    -Created Class
* CR 22115 JMC 01/26/2010
*    - Added new property: 'LockboxKey' in support of Research
* CR 28738 JMC 05/03/2010
*    - Added SiteLockboxKey and IsCommingled properties in support of
*      Lockbox Commingling.
*    - Reworked cLockboxes class.
* CR 29829 JNE 6/25/2010
*    - When creating XML, check OnlineViewingDays for -1, which translates to NULL 
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
* WI 96823 CRG 03/05/2013
*    Purge Development - Admin Web Service layer
* WI 149727 TWE 06/23/2014
*    change key for clockbox object to olworkgroupsid
* WI 151989 TWE 07/07/2014
*    Fix IPOnline to work with new Batch ID and RAAM
******************************************************************************/
namespace WFS.RecHub.Common {

	/// <summary>
	/// Summary description for cLockbox.
	/// </summary>
	public class cLockbox: IComparable {
        private int _BankID = -1;
        private int _CustomerID = -1;
        private int _LockboxID = -1;
        private int _EntityID = -1;
        private int _OLWorkgroupID = -1;

        private string _LongName = "";
        private string _ShortName = "";
        private int _SiteCode = -1;
        private bool _IsCutoff = false;
        private bool _IsActive = false;
		private int _DataRetentionDays = -1;
		private int _ImageRetentionDays = -1;
		private int _OnlineViewingDays = -1; //CR 29099 3/25/2010 JNE
		private DateTime _CurrentProcessingDate = DateTime.MinValue;
        private int _LockboxKey = -1;
        private Guid _SiteLockboxKey = Guid.Empty;
        private bool _IsCommingled = false;

        /// <summary>
        /// Default Constructor; required for serialization.
        /// </summary>
		public cLockbox() {
		}

		/// <summary>
		/// Lockbox.BankID
		/// </summary>
		public int BankID {
            get { return _BankID; }
            set { _BankID=value; }
        }

        /// <summary>
        /// Lockbox.EntityID
        /// </summary>
        public int EntityID
        {
            get { return _EntityID; }
            set { _EntityID = value; }
        }

        /// <summary>
        /// Lockbox.OLWorkgroupID
        /// </summary>
        public int OLWorkgroupID
        {
            get { return _OLWorkgroupID; }
            set { _OLWorkgroupID = value; }
        }
       

		/// <summary>
		/// Lockbox.CustomerID
		/// </summary>
		public int CustomerID {
            get { return _CustomerID; }
            set { _CustomerID=value; }
        }

		/// <summary>
		/// Lockbox.LockboxID
		/// </summary>
		public int LockboxID {
            get { return _LockboxID; }
            set { _LockboxID=value; }
        }

		/// <summary>
		/// Lockbox.LongName
		/// </summary>
		public string LongName {
            get { return _LongName; }
            set { _LongName=value; }
        }

		/// <summary>
		/// 
		/// </summary>
		public string ShortName {
            get { return _ShortName; }
            set { _ShortName=value; }
        }

		/// <summary>
		/// 
		/// </summary>
		public int SiteCode {
            get { return _SiteCode; }
            set { _SiteCode=value; }
        }

		/// <summary>
		/// 
		/// </summary>
		public bool IsCutoff {
            get { return _IsCutoff; }
            set { _IsCutoff=value; }
        }

		/// <summary>
		/// 
		/// </summary>
		public bool IsActive {
            get { return _IsActive; }
            set { _IsActive=value; }
        }

		/// <summary>
		/// 
		/// </summary>
		public int OnlineViewingDays {
			get { return _OnlineViewingDays; }
			set { _OnlineViewingDays=value; }
		}

		/// <summary>
		/// Gets or sets the data retention days.
		/// </summary>
		/// <value>
		/// The data retention days.
		/// </value>
		public int DataRetentionDays {
			get { return _DataRetentionDays; }
			set { _DataRetentionDays = value; }
		}

		/// <summary>
		/// Gets or sets the image retention days.
		/// </summary>
		/// <value>
		/// The image retention days.
		/// </value>
		public int ImageRetentionDays {
			get { return _ImageRetentionDays; }
			set { _ImageRetentionDays = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime CurrentProcessingDate {
            get { return _CurrentProcessingDate;}
            set { _CurrentProcessingDate = value;}
        }

		/// <summary></summary>
		public int LockboxKey {
            get { return _LockboxKey; }
            set { _LockboxKey = value; }
        }

		/// <summary></summary>
		public Guid SiteLockboxKey {
            get { return _SiteLockboxKey; }
            set { _SiteLockboxKey = value; }
        }

		/// <summary></summary>
		public bool IsCommingled {
            get { return _IsCommingled; }
            set { _IsCommingled = value; }
        }
        
        /// <summary>
        /// Orders class in a collection by BankID, CustomerID, LockboxID.
        /// </summary>
        /// <param name="x">cLockbox object to compare this object to.</param>
        /// <returns>
        /// A signed number indicating the relative values of this instance and value.    
        /// Return Value Description:
        ///     Less than zero: This instance is less than value.     
        ///     Zero: This instance is equal to value.     
        ///     Greater than zero:  This instance is greater than value.  -or-  value is null.     
        /// </returns>
        public int CompareTo(object x) {
            if (x is cLockbox) {
                cLockbox objLockboxX = (cLockbox)x;

                if(this.BankID == objLockboxX.BankID) { 
                    if(this.CustomerID == objLockboxX.CustomerID) {
                        return(this.LockboxID.CompareTo(objLockboxX.LockboxID));
                    } else {
                        return(this.CustomerID.CompareTo(objLockboxX.CustomerID));
                    }
                } else {
                    return(this.BankID.CompareTo(objLockboxX.BankID));
                }
           }
            else {
                throw(new Exception("Invalid Lockbox object"));
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public virtual bool LoadDataRow(DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
                        case("bankid"):
                            this.BankID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("entitiyid"):
                            this.EntityID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break; 
                        case("customerid"):
                            this.CustomerID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("lockboxid"):
                            this.LockboxID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("olworkgroupid"):
                            this.OLWorkgroupID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("longname"):
                            this.LongName=ipoLib.NVL(ref dr, dc.ColumnName, "");
                            break;
                        case("shortname"):
                            this.ShortName=ipoLib.NVL(ref dr, dc.ColumnName, "");
                            break;
                         case("sitecode"):
                            this.SiteCode=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("cutoff"):
                            this.IsCutoff=(ipoLib.NVL(ref dr, dc.ColumnName, (byte)0) > 0);
                            break;
                        case("siteisactive"):
                            this.IsActive=ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case("isactive"):
                            this.IsActive=ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
						case ("dataretentiondays"):
							this.DataRetentionDays = ipoLib.NVL(ref dr, dc.ColumnName, -1);
							break;
						case ("imageretentiondays"):
							this.ImageRetentionDays = ipoLib.NVL(ref dr, dc.ColumnName, -1);
							break;
						case("onlineviewingdays"):
							this.OnlineViewingDays=ipoLib.NVL(ref dr, dc.ColumnName, -1); //CR 29099 3/25/10 JNE
							break;
						case("currentprocessingdate"):
                            _CurrentProcessingDate=ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("lockboxkey"):
                            _LockboxKey = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("sitelockboxkey"):
                            _SiteLockboxKey = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("iscommingled"):
                            _IsCommingled = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                   }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
             return(this.LockboxID.ToString() + " - " + this.LongName.ToString());
        }

        /// <summary>
        /// Create an XML node of the current Lockbox with properties as attributes
        /// </summary>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public virtual XmlNode AsXMLNode() {
            return(AsXMLNode("Record"));
        }

        /// <summary>
        /// Create an XML node of the current Lockbox with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public virtual XmlNode AsXMLNode(string NodeName) {
            XmlDocument xmlDoc;
            XmlNode node;
            try {
            
                // Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                // Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);

                // set each property as an attribute to the node
                ipoXmlLib.addAttribute(node, "BankID", this.BankID.ToString());
                ipoXmlLib.addAttribute(node, "EntityID", this.EntityID.ToString());
               
                ipoXmlLib.addAttribute(node, "CustomerID", this.CustomerID.ToString());
                ipoXmlLib.addAttribute(node, "LockboxID", this.LockboxID.ToString());
                ipoXmlLib.addAttribute(node, "ShortName", this.ShortName.Trim());
                ipoXmlLib.addAttribute(node, "LongName", this.LongName.Trim());
                ipoXmlLib.addAttribute(node, "SiteCode", this.SiteCode.ToString());
                ipoXmlLib.addAttribute(node, "IsActive", this.IsActive.ToString());
                ipoXmlLib.addAttribute(node, "IsCutoff", this.IsCutoff.ToString());
                ipoXmlLib.addAttribute(node, "IsCommingled", (IsCommingled ? "1" : "0"));
				if(this.OnlineViewingDays.Equals(-1)) { //CR 29829 6/25/2010 JNE
					ipoXmlLib.addAttribute(node, "OnlineViewingDays", null);
				} else {
					ipoXmlLib.addAttribute(node, "OnlineViewingDays", this.OnlineViewingDays.ToString());
				}

				if(this.DataRetentionDays.Equals(-1)) { 
					ipoXmlLib.addAttribute(node, "DataRetentionDays", null);
				} else {
					ipoXmlLib.addAttribute(node, "DataRetentionDays", this.DataRetentionDays.ToString());
				}

				if(this.ImageRetentionDays.Equals(-1)) { 
					ipoXmlLib.addAttribute(node, "ImageRetentionDays", null);
				} else {
					ipoXmlLib.addAttribute(node, "ImageRetentionDays", this.ImageRetentionDays.ToString());
				}
				
				ipoXmlLib.addAttribute(node, "CurrentProcessingDate", this.CurrentProcessingDate.ToShortDateString());
                ipoXmlLib.addAttribute(node, "LockboxKey", this.LockboxKey.ToString());
                ipoXmlLib.addAttribute(node, "SiteLockboxKey", this.SiteLockboxKey.ToString());
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }

        /// <summary>
        /// Populate properties of a cLockbox object from an xml node generated
        /// by AsXMLNode() above.
        /// </summary>
        /// <param name="node"></param>
        /// <returns>
        /// IXMLDomNode
        ///           Format: <NodeName Property="Value"/>
        /// Returns:  True/False 
        ///</returns>
        public virtual bool FromXMLNode(XmlNode node) {
            Guid gidTemp;
            int intTemp;
            bool bolTemp;
            
            try {

				//loop through the attributes in the node reference
                foreach(XmlAttribute att in node.Attributes) {
                
					//using the attribute name, assign the value to the corresponding cLockbox property
                    switch(att.Name.ToLower()) {
                        case "bankid":
                            this.BankID = int.Parse(att.Value);
                            break;
                        case "entityid":
                            this.EntityID = int.Parse(att.Value);
                            break;
                        
                        case "customerid":
                            this.CustomerID = int.Parse(att.Value);
                            break;
                        case "lockboxid":
                            this.LockboxID = int.Parse(att.Value);
                            break;
                        case "shortname":
                            this.ShortName = att.Value;
                            break;
                        case "longname":
                            this.LongName = att.Value;
                            break;
                        case "sitecode":
                            this.SiteCode = int.Parse(att.Value);
                            break;
                        case "isactive":
                            if(int.TryParse(att.Value, out intTemp)) {
                                this.IsActive = (intTemp > 0);
                            } else if(bool.TryParse(att.Value, out bolTemp)) {
                                this.IsActive = bolTemp;
                            } else {
                                this.IsActive = false;
                            }
                            break;
                        case "iscutoff":
                            if(int.TryParse(att.Value, out intTemp)) {
                                this.IsCutoff = (intTemp > 0);
                            } else if(bool.TryParse(att.Value, out bolTemp)) {
                                this.IsCutoff = bolTemp;
                            } else {
                                this.IsCutoff = false;
                            }
                            break;
                        case "iscommingled":
                            if(int.TryParse(att.Value, out intTemp)) {
                                this.IsCommingled = (intTemp > 0);
                            } else if(bool.TryParse(att.Value, out bolTemp)) {
                                this.IsCommingled = bolTemp;
                            } else {
                                this.IsCommingled = false;
                            }
                            break;
						case "onlineviewingdays":
							this.OnlineViewingDays = int.Parse(att.Value);
							break;
						case "dataretentiondays":
							this.DataRetentionDays = int.Parse(att.Value);
							break;
						case "imageretentiondays":
							this.ImageRetentionDays = int.Parse(att.Value);
							break;
						case "currentprocessingdate":
                            this.CurrentProcessingDate = DateTime.Parse(att.Value);
                            break;
                        case "lockboxkey":
                            this.LockboxKey = int.Parse(att.Value);
                            break;
                        case "sitelockboxkey":
                            if(ipoLib.TryParse(att.Value, out gidTemp)) {
                                this.SiteLockboxKey = gidTemp;
                            } else {
                                this.SiteLockboxKey = Guid.Empty;
                            }
                            break;
                    }
                }
                
                return(true);
            } catch(Exception) {
                return(false);
            }
        }
    }

	/// <summary>
	/// 
	/// </summary>
    public class cLockboxes : List<cLockbox> {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool LoadDataTable(DataTable dt) {

            cLockbox objLockbox;

            foreach(DataRow dr in dt.Rows) {
                objLockbox = new cLockbox();
                objLockbox.LoadDataRow(dr);
                this.Add(objLockbox);
            }

            return(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>
        /// Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///         </NodeName></returns>
        public XmlNode AsXMLNode() {
            return(AsXMLNode("Lockboxes", "Recordset"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="AttributeName"></param>
        /// <returns>
        /// Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///         </NodeName></returns>
        public XmlNode AsXMLNode(string AttributeName) {
            return(AsXMLNode(AttributeName, "Recordset"));
        }

        /// <summary>
        /// Create an XML node of the current Lockboxes collection
        /// </summary>
        /// <param name="AttributeName"></param>
        /// <param name="NodeName"></param>
        /// <returns>
        /// Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///         </NodeName></returns>
        public XmlNode AsXMLNode(string AttributeName, string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;
            
            try {
                //Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                //Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);
                    
                //Add the Name attribute to the node
                ipoXmlLib.addAttribute(node, "Name", AttributeName);
                
                //Add each permission as a child node
                foreach(cLockbox lockbox in this) {
                    node.AppendChild(xmlDoc.ImportNode(lockbox.AsXMLNode(), true));
                }
                
                //return reference to node
                return(node);
            
            } catch(Exception ex) {
                throw(ex);
            }
        }

        /// <summary>
        /// Populate a cLockboxes collection w/ cLockbox objects from an xml node
        /// built using the AsXMLNode function above.
        /// </summary>
        /// <param name="node">
        /// IXMLDomNode
        ///           Format: <NodeName Name="AttributeName">
        ///                       <Record Property="Value"/>
        ///                   </NodeName>
        /// </param>
        /// <returns>
        /// True/False 
        ///</returns>
        public bool FromXMLNode(XmlNode node) {

            cLockbox objLockbox;
            
            try {
            
                //loop through each child node in the node reference
                foreach(XmlNode n in node.ChildNodes) {
                    //create a new cLockbox object reference
                    objLockbox = new cOLLockbox();
                    
                    //assign the cLockbox properties from the child node reference
                    if(objLockbox.FromXMLNode(n)) {
                        //assign the cLockbox to the collection
                        this.Add(objLockbox);
                    }
                }
                
                return(true);
            } catch(Exception) {
                return(false);
            }
        }
    }
}
