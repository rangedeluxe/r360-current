using System;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:	JMC
* Date:		11/06/2006
*
* Purpose:	Common SessionInfo object
*
* Modification History
* CR 18946 JMC 11/06/2006
*    -Created Class.
* WI 89972 CRG 03/01/2013
*	Modify ipoCommon for .NET 4.5 and NameSpace
******************************************************************************/
namespace WFS.RecHub.Common {

	/// <summary>
	/// Summary description for cSessionInfo.
	/// </summary>
	public class cSessionInfo
	{
        
        /// <summary></summary>
		public cSessionInfo() {
		}

        /// <summary></summary>
        public Guid SessionID = Guid.Empty;

        /// <summary></summary>
        public string SiteKey = string.Empty;

        /// <summary></summary>
        public Guid EmulationID = Guid.Empty;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <returns></returns>
        public XmlNode ToXml(XmlNode nodeParent) {

            XmlNode nodeSessionInfo;
            XmlAttribute att;

            nodeSessionInfo = nodeParent.OwnerDocument.CreateNode(XmlNodeType.Element, "SessionInfo", nodeParent.NamespaceURI);

            att = nodeParent.OwnerDocument.CreateAttribute("SessionID", nodeParent.NamespaceURI);
            att.Value = this.SessionID.ToString();
            nodeSessionInfo.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("SiteKey", nodeParent.NamespaceURI);
            att.Value = this.SiteKey.ToString();
            nodeSessionInfo.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("EmulationID", nodeParent.NamespaceURI);
            att.Value = this.EmulationID.ToString();
            nodeSessionInfo.Attributes.Append(att);

            nodeParent.AppendChild(nodeSessionInfo);

            return(nodeSessionInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeSessionInfo"></param>
        /// <returns></returns>
        public bool FromXml(XmlNode nodeSessionInfo) {

            try {
                foreach(XmlAttribute att in nodeSessionInfo.Attributes) {
                    switch(att.Name.ToLower()) {
                        case("sessionid"):
                            if(ipoLib.IsGuid(att.Value)) {
                                SessionID = new Guid(att.Value);
                            }
                            break;
                        case("sitekey"):
                            SiteKey = att.Value;
                            break;
                        case("emulationid"):
                            if(ipoLib.IsGuid(att.Value)) {
                                EmulationID = new Guid(att.Value);
                            }
                            break;
                    }
                }
            } catch(Exception e) {
                throw(e);
            }
            
            return(true);
        }
	}
}
