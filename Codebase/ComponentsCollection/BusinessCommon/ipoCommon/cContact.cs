using System;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*  Module:           CheckBOX Online Common Contact Class Library
* 
*  Filename:         cContact.cls
* 
*  Author:           Jon Sucha
* 
*  Description:
* 
*    This class library provides ...
* 
*  Revisions:
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
**********************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cContact {

	    //-------------------------------------------------------------------------------
	    // Private member variables declaration
	    //-------------------------------------------------------------------------------
	    private Guid _ContactID;
	    private int _BankID;
	    private int _CustomerID;
	    private int _LockboxID;
	    private string _ContactName;
	    private bool _IsActive;
	    private string _Address;
	    private string _City;
	    private string _State;
	    private string _Zip;
	    private string _Voice;
	    private string _Fax;
	    private string _Email;
	    private string _TextPager;
	    private DateTime _CreationDate;
	    private string _CreatedBy;
	    private DateTime _ModificationDate;
	    private string _ModifiedBy;

        /// <summary>
        /// 
        /// </summary>
	    public Guid ContactID {
		    get { return(_ContactID); }
		    set { _ContactID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public int BankID {
		    get { return(_BankID); }
		    set { _BankID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public int CustomerID {
		    get { return(_CustomerID); }
		    set { _CustomerID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public int LockboxID {
		    get { return(_LockboxID); }
		    set { _LockboxID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string ContactName {
		    get { return(_ContactName); }
		    set { _ContactName = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public bool IsActive {
		    get { return(_IsActive); }
		    set { _IsActive = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string Address {
		    get { return(_Address); }
		    set { _Address = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string City {
		    get { return(_City); }
		    set { _City = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string State {
		    get { return(_State); }
		    set { _State = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string Zip {
		    get { return(_Zip); }
		    set { _Zip = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string Voice {
		    get { return(_Voice); }
		    set { _Voice = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string Fax {
		    get { return(_Fax); }
		    set { _Fax = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string Email {
		    get { return(_Email); }
		    set { _Email = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string TextPager {
		    get { return(_TextPager); }
		    set { _TextPager = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public DateTime ModificationDate {
		    get { return(_ModificationDate); }
		    set { _ModificationDate = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public DateTime CreationDate {
		    get { return(_CreationDate); }
		    set { _CreationDate = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string CreatedBy {
		    get { return(_CreatedBy); }
		    set { _CreatedBy = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string ModifiedBy {
		    get { return(_ModifiedBy); }
		    set { _ModifiedBy = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public bool LoadDataRow(DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    if(!dr.IsNull(dc)) {
                        switch(dc.ColumnName.ToLower()) {
					        case "contactid":
						        _ContactID = (Guid)dr[dc];
						        break;
					        case "bankid":
						        _BankID = (int)dr[dc];
						        break;
					        case "customerid":
						        _CustomerID = (int)dr[dc];
						        break;
					        case "lockboxid":
						        _LockboxID = (int)dr[dc];
						        break;
					        case "contactname":
						        _ContactName = dr[dc].ToString().TrimEnd();
						        break;
					        case "address":
						        _Address = dr[dc].ToString().TrimEnd();
						        break;
					        case "city":
						        _City = dr[dc].ToString().TrimEnd();
						        break;
					        case "state":
						        _State = dr[dc].ToString().TrimEnd();
						        break;
					        case "zip":
						        _Zip = dr[dc].ToString().TrimEnd();
						        break;
					        case "voice":
						        _Voice = dr[dc].ToString().TrimEnd();
						        break;
					        case "fax":
						        _Fax = dr[dc].ToString().TrimEnd();
						        break;
					        case "creationdate":
						        _CreationDate = (DateTime)dr[dc];
						        break;
					        case "CreatedBy":
						        _CreatedBy = dr[dc].ToString().TrimEnd();
						        break;
					        case "modificationdate":
						        _ModificationDate = (DateTime)dr[dc];
						        break;
					        case "modifiedby":
						        _ModifiedBy = dr[dc].ToString().TrimEnd();
						        break;
					        case "email":
						        _Email = dr[dc].ToString().TrimEnd();
						        break;
					        case "isactive":
						        _IsActive = ((byte)dr[dc] > 0);
						        break;
					        case "textpager":
						        _TextPager = dr[dc].ToString().TrimEnd();
						        break;
                        }
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// Create an XML node of the current Contact with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public XmlNode AsXMLNode(string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;

            try {

                // Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                // Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);

                // set each property as an attribute to the node
                ipoXmlLib.addAttribute(node, "Address", this.Address);
                ipoXmlLib.addAttribute(node, "BankID", this.BankID.ToString());
                ipoXmlLib.addAttribute(node, "City", this.City);
                ipoXmlLib.addAttribute(node, "ContactID", this.ContactID.ToString());
                ipoXmlLib.addAttribute(node, "ContactName", this.ContactName);
                ipoXmlLib.addAttribute(node, "CreatedBy", this.CreatedBy);
                ipoXmlLib.addAttribute(node, "CustomerID", this.CustomerID.ToString());
                ipoXmlLib.addAttribute(node, "CreationDate", this.CreationDate.ToString());
                ipoXmlLib.addAttribute(node, "Email", this.Email);
                ipoXmlLib.addAttribute(node, "Fax", this.Fax);
                ipoXmlLib.addAttribute(node, "IsActive", (this.IsActive ? "1" : "0"));
                ipoXmlLib.addAttribute(node, "LockboxID", this.LockboxID.ToString());
                ipoXmlLib.addAttribute(node, "ModificationDate", this.ModificationDate.ToString());
                ipoXmlLib.addAttribute(node, "ModifiedBy", this.ModifiedBy);
                ipoXmlLib.addAttribute(node, "State", this.State);
                ipoXmlLib.addAttribute(node, "TextPager", this.TextPager);
                ipoXmlLib.addAttribute(node, "Voice", this.Voice);
                ipoXmlLib.addAttribute(node, "Zip", this.Zip);
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }
    }
}
