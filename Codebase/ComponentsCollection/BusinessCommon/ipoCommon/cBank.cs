using System;
using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Common  object
*  Filename: .cs
*    Author: Joel Caples
*
* Revisions:
* ----------------------
* CR  JMC 
*    -Created Class
* CR 22115 JMC 01/26/2010
*    - Added new properties: Address, City, State, Zip in support of Research
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
******************************************************************************/
namespace WFS.RecHub.Common {

	/// <summary>
	/// Summary description for cBank.
	/// </summary>
	public class cBank: IComparable
	{
        private long _BankID = -1;
        private string _BankName = string.Empty;
    	private string _Address;
    	private string _City;
    	private string _State;
    	private string _Zip;	

        /// <summary>
        /// Default Constructor; required for serialization.
        /// </summary>
		public cBank() {
		}

        /// <summary>
        /// Bank.BankID
        /// </summary>
        public long BankID {
            get { return _BankID; }
            set { _BankID=value; }
        }
    
        /// <summary>
        /// Bank.BankName
        /// </summary>
        public string BankName {
            get { return _BankName; }
            set { _BankName=value; }
        }

        /// <summary></summary>
        public string Address {
            get {
                return(_Address);
            }
            set {
                _Address = value.TrimEnd();
            }
        }

        /// <summary></summary>
        public string City {
            get {
                return(_City);
            }
            set {
                _City = value.TrimEnd();
            }
        }

        /// <summary></summary>
        public string State {
            get {
                return(_State);
            }
            set {
                _State = value.TrimEnd();
            }
        }

        /// <summary></summary>
        public string Zip {
            get {
                return(_Zip);
            }
            set {
                _Zip = value.TrimEnd();
            }
        }

        /// <summary>
        /// Orders class in a collection by BankID.
        /// </summary>
        /// <param name="x">cBank object to compare this object to.</param>
        /// <returns>
        /// A signed number indicating the relative values of this instance and value.    
        /// Return Value Description:
        ///     Less than zero: This instance is less than value.     
        ///     Zero: This instance is equal to value.     
        ///     Greater than zero:  This instance is greater than value.  -or-  value is null.     
        /// </returns>
        public int CompareTo(object x) {
            if (x is cBank) {
                cBank objBankX = (cBank)x;
 
                return(this.BankID.CompareTo(objBankX.BankID));
           }
            else {
                throw(new Exception("Invalid document object"));
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(ref DataRow dr) {

            if(dr.Table.Columns.Count>0) {
	            foreach(DataColumn dc in dr.Table.Columns) {
	                if(!dr.IsNull(dc)) {
	                    switch(dc.ColumnName.ToLower()) {
	                        case "bankid":
	                            _BankID = (int)dr[dc];
	                            break;
	                        case "bankname":
	                            _BankName = dr[dc].ToString();
	                            break;
	                        case "address":
	                            _Address = dr[dc].ToString();
	                            break;
	                        case "city":
	                            _City = dr[dc].ToString();
	                            break;
	                        case "state":
	                            _State = dr[dc].ToString();
	                            break;
	                        case "zip":
	                            _Zip = dr[dc].ToString();
	                            break;
                        }
	                }
	            }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
             return(this.BankID.ToString() + " - " + this.BankName.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayName {
            get {
                return(this.ToString());
            }
        }
    }
}
