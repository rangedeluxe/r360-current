namespace WFS.RecHub.Common
{
    public enum DataEntryDataTypeValidationError
    {
        InvalidNumericValue,
        InvalidCurrencyValue,
        MaxCurrencyValueExceeded,
        MinCurrencyValueExceeded,
        InvalidDateTimeValue,
    }
}