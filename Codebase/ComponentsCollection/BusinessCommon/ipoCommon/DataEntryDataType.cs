﻿namespace WFS.RecHub.Common
{
    public enum DataEntryDataType
    {
        /// <summary>
        /// AKA string.
        /// </summary>
        Alphanumeric = 1,
        /// <summary>
        /// AKA float / double.
        /// </summary>
        Numeric = 6,
        Currency = 7,
        Date = 11,
    }
}