
using System;

namespace WFS.RecHub.Common
{

    public class cDESetupField
    {
        public Guid DESetupFieldID { get; set; }
        public string TableName { get; set; }
        public string FldName { get; set; }
        public DataEntryDataType FldDataTypeEnum { get; set; }

        public cDESetupField(string vTableName, string vFldName, DataEntryDataType dataType)
        {
            TableName = vTableName;
            FldName = vFldName;
            FldDataTypeEnum = dataType;
        }

        public cDESetupField()
        {

        }
    }
}
