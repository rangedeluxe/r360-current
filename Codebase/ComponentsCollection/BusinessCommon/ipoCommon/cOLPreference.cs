﻿using System;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Common OLPerference object
*  Filename: cOLPreference.cs
*    Author: Joel Caples
*
* Revisions:
* ----------------------
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
******************************************************************************/
namespace WFS.RecHub.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class cOLPreference
    {
        /// <summary>
        /// 
        /// </summary>
        public cOLPreference(){}
        /// <summary>
        /// 
        /// </summary>
	    public string PreferenceName{ get; set;}
        /// <summary>
        /// 
        /// </summary>
        public string Description{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public bool IsOnline{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public bool IsSystem{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public string DefaultSetting{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public int AppType{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public string PreferenceGroup{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public int fldDataTypeEnum{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public int fldSize{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public int fldMaxLength{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public string fldExp{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public bool EncodeOnSerialization{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public int fldMinValue{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public int fldIntMaxValue{ get ; set ;}
        /// <summary>
        /// 
        /// </summary>
        public string UserSetting{ get ; set ;}

	    /// <summary>
        /// 
        /// </summary>
        public XmlNode AsXMLNode(){
            XmlDocument xmlDoc;
            XmlNode node;
            XmlNode nodeRetVal = null;
            
            try {
                xmlDoc = ipoXmlLib.GetXMLDoc();
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, "Record");
                ipoXmlLib.addAttribute(node,"Name", this.PreferenceName);
                ipoXmlLib.addAttribute(node,"Value",this.UserSetting.Length>0?this.UserSetting.ToString():this.DefaultSetting.ToString());
                nodeRetVal = node;
                return(nodeRetVal);
            } catch(Exception) {
                return(nodeRetVal);
            }
        }
    }
}
