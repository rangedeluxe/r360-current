﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*  Module:           Delivery Method Class Library
*  Filename:         cDeliveryMethod.cs
*  Author:           Jon Sucha
*  Description:
* 
*    This class library provides ...
* 
*  Revisions:
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
**********************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cDeliveryMethod {

        private Guid _DeliveryMethodID;
        private string _DeliveryMethodName;

        /// <summary>
        /// 
        /// </summary>
        public cDeliveryMethod() {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vDeliveryMethodID"></param>
        /// <param name="vDeliveryMethodName"></param>
        public cDeliveryMethod(Guid vDeliveryMethodID, string vDeliveryMethodName) {
            _DeliveryMethodID = vDeliveryMethodID;
            _DeliveryMethodName = vDeliveryMethodName;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Guid DeliveryMethodID {
            get {
                return(_DeliveryMethodID);
            }
            set {
                _DeliveryMethodID = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DeliveryMethodName {
            get {
                return(_DeliveryMethodName);
            }
            set {
                _DeliveryMethodName = value;
            }
        }

        /// <summary>
        /// Create an XML node of the current DeliveryMethod with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public XmlNode AsXMLNode(string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;

            try {

                // Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                // Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);

                // set each property as an attribute to the node
                ipoXmlLib.addAttribute(node, "DeliveryMethodID", this.DeliveryMethodID.ToString());
                ipoXmlLib.addAttribute(node, "DeliveryMethodName", this.DeliveryMethodName);
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }
    }

    ///// <summary>
    ///// 
    ///// </summary>
    //public class cDeliveryMethods : Dictionary<Guid, cDeliveryMethod> {

    //    ///// <summary>
    //    ///// 
    //    ///// </summary>
    //    //public cDeliveryMethods() {
    //    //}
    //}
}
