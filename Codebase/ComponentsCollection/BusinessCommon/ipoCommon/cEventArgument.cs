using System.Collections.Generic;
using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*  Module:           
* 
*  Filename:         cEventArgument.cs
* 
*  Author:           Joel Caples
* 
*  Description:
* 
*    
* 
*  Revisions:
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
**********************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cEventArgument {

        private int _EventArgumentID = -1;
        private int _EventID = -1;
        private string _EventArgumentDescription = string.Empty;
        private string _EventArgumentCode = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public int EventArgumentID {
            get {
                return(_EventArgumentID);
            }
            set {
                _EventArgumentID = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public int EventID {
            get {
                return(_EventID);
            }
            set {
                _EventID = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string EventArgumentDescription {
            get {
                return(_EventArgumentDescription);
            }
            set {
                _EventArgumentDescription = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string EventArgumentCode {
            get {
                return(_EventArgumentCode);
            }
            set {
                _EventArgumentCode = value;
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
					    case "eventargumentid":
						    _EventArgumentID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
						    break;
					    case "eventid":
						    _EventID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
						    break;
					    case "eventargumentdescription":
						    _EventArgumentDescription = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
						    break;
					    case "eventargumentcode":
						    _EventArgumentCode = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
						    break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class cEventArguments : Dictionary<int, cEventArgument> {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EventArgumentID"></param>
        /// <param name="EventID"></param>
        /// <param name="EventArgumentDescription"></param>
        /// <param name="EventArgumentCode"></param>
        /// <returns></returns>
        public cEventArgument Add(int EventArgumentID, int EventID, string EventArgumentDescription, string EventArgumentCode) {
            
            cEventArgument objEventArgument = new cEventArgument();
            
            objEventArgument.EventArgumentID = EventArgumentID;
            objEventArgument.EventID = EventID;
            objEventArgument.EventArgumentDescription = EventArgumentDescription;
            objEventArgument.EventArgumentCode = EventArgumentCode;
            
            this.Add(EventArgumentID, objEventArgument);
            
            return(objEventArgument);
        }
    }

}
