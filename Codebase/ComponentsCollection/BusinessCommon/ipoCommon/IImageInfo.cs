﻿
/******************************************************************************
** Wausau
** Copyright © 1997-2011 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     01/12/2012
*
* Purpose:  Interface for LTA Image Info
*
* Modification History
* CR 33230 JMC 01/12/2012
*    -New File
* WI 89972 CRG 03/01/2013
*	Modify ipoCommon for .NET 4.5 and NameSpace
*WI 166398 SAS 9/17/2014
*   Added ImportTypeShortName and BatchSourceID field    
*WI 166924 SAS 9/19/2014
*   Updated the datatype for Source BatchID  
*WI 175186 SAS 10/30/2014
*    Update ImportTypeShortName field to BatchSourceName 
*WI 176351 SAS 11/06/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName    
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary></summary>
    public interface ILTAImageInfo {

        /// <summary></summary>
        int ProcessingDateKey {
            get;
            set;
        }

        /// <summary></summary>
        int BankID {
            get;
            set;
        }

        /// <summary></summary>
        int LockboxID {
            get;
            set;
        }

        /// <summary></summary>
        int BatchID {
            get;
            set;
        }

        /// <summary></summary>
        int BatchSequence {
            get;
            set;
        }

        /// <summary></summary>
        string FileDescriptor {
            get;
            set;
        }

        /// <summary></summary>
        bool IsCheck {
            get;
            set;
        }

        /// <summary></summary>
        bool IsBack {
            get;
            set;
        }

        /// <summary></summary>
        short ColorMode {
            get;
            set;
        }

        /// <summary></summary>
        long SourceBatchID
        {
            get;
            set;
        }

        /// <summary></summary>
        string BatchSourceShortName
        {
            get;
            set;
        }

        /// <summary></summary>
        string ImportTypeShortName
        {
            get;
            set;
        }
    }
}
