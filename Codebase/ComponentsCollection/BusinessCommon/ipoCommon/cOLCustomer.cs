using System;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Common OLCustomer object
*  Filename: cOLCustomer.cs
*    Author: Joel Caples
*
* Revisions:
* ----------------------
* CR 18946 JMC 11/06/2006
*    -Created Class.
* CR 22116 JNE 02/11/2009
*    -Added Maximum Search Days field and allowed certain fields to be set.
* CR 49406 JCS 12/12/2011 
*    -Added FromXml() method to load an OLCustomer from Xml data.
* CR 45984 JCS 01/19/2011
*    -Added BrandingSchemeID property as the assigned Branding Scheme.
* CR 54173 JNE 08/08/2012
*    -Added DisplayBatchID property 
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
* WI 105837 EAS 06/17/2013
*    Add Parent level properties 
* WI 109012 EAS 07/15/2013
*    Add Parent level items to parsing.  Change Parent Code to Guid. 
******************************************************************************/
namespace WFS.RecHub.Common {
	/// <summary>
	/// Summary description for cOLCustomer.
	/// </summary>
	public class cOLCustomer {

        private Guid _OLCustomerID = Guid.Empty;
        private string _CustomerCode = string.Empty;
        private string _Description = string.Empty;
        private bool _IsActive = false;
        private bool _IsParent = false;
        private Guid _ParentCode = Guid.Empty;
        private int _ViewingDays = -1; // CR 29099 3/25/2010 JNE
        private string _ExternalID1 = string.Empty;
        private string _ExternalID2 = string.Empty;
        private DateTime _CreationDate = DateTime.MinValue;
        private string _CreatedBy = string.Empty;
        private DateTime _ModificationDate = DateTime.MinValue;
        private string _ModifiedBy = string.Empty;
        private int _MaximumSearchDays = -1; // CR 29099 3/25/2010 JNE
        private OLFImageDisplayMode _DocumentImageDisplayMode = OLFImageDisplayMode.BothSides;
        private OLFImageDisplayMode _CheckImageDisplayMode = OLFImageDisplayMode.BothSides;
        private int _BrandingSchemeID = -1;
        private bool _DisplayBatchID = false;

        /// <summary>
        /// 
        /// </summary>
		public cOLCustomer() {
		}

        /// <summary></summary>
        public Guid OLCustomerID {
            get {return(_OLCustomerID);}
            set {_OLCustomerID = value;}
        }
        
        /// <summary></summary>
        public string CustomerCode {
            get {return(_CustomerCode);}
            set {_CustomerCode = value;}
        }

        /// <summary></summary>
        public string Description {
            get {return(_Description);}
            set {_Description = value;}
        }

        /// <summary></summary>
        public bool IsActive {
            get {return(_IsActive);}
            set {_IsActive = value;}
        }

        /// <summary></summary>
        public bool IsParent
        {
            get { return (_IsParent);  }
            set { _IsParent = value; }
        }

        /// <summary></summary>
        public Guid ParentCode
        {
            get { return (_ParentCode); }
            set { _ParentCode = value; }
        }

        /// <summary></summary>
        public int ViewingDays {
            get {return(_ViewingDays);}
            set {_ViewingDays = value;}
        }

        /// <summary></summary>
        public string ExternalID1 {
            get {return(_ExternalID1);}
            set {_ExternalID1 = value;}
        }

        /// <summary></summary>
        public string ExternalID2 {
            get {return(_ExternalID2);}
            set {_ExternalID2 = value;}
        }

        /// <summary></summary>
        public DateTime CreationDate {
            get {return(_CreationDate);}
        }

        /// <summary></summary>
        public string CreatedBy {
            get {return(_CreatedBy);}
        }

        /// <summary></summary>
        public DateTime ModificationDate {
            get {return(_ModificationDate);}
        }

        /// <summary></summary>
        public string ModifiedBy {
            get { return(_ModifiedBy);}
        }
        
        
        /// <summary></summary>
        public int MaximumSearchDays {
            get {return(_MaximumSearchDays);}
            set {_MaximumSearchDays = value;}
        }

         /// <summary></summary>
        public OLFImageDisplayMode CheckImageDisplayMode {
            get {return(_CheckImageDisplayMode);}
            set {_CheckImageDisplayMode = value;}
        }
        
         /// <summary></summary>
        public OLFImageDisplayMode DocumentImageDisplayMode {
            get {return(_DocumentImageDisplayMode);}
            set {_DocumentImageDisplayMode = value;}
        }

        /// <summary></summary>
        public int BrandingSchemeID {
            get { return (_BrandingSchemeID); }
            set { _BrandingSchemeID = value; }
        }

        /// <summary></summary>
        public bool DisplayBatchID {
            get {return(_DisplayBatchID);}
            set {_DisplayBatchID = value;}
        }
        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(ref DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
                        case("olcustomerid"):
                            _OLCustomerID=ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("customercode"):
                            _CustomerCode=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("description"):
                            _Description=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("isactive"):
                            _IsActive=ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case("isparent"):
                            _IsParent = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "olorganizationparentid":
                            _ParentCode = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("viewingdays"):
                            _ViewingDays=ipoLib.NVL(ref dr, dc.ColumnName, -1); //CR 29099 3/25/10 JNE
                            break;
                        case("externalid1"):
                            _ExternalID1=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("externalid2"):
                            _ExternalID2=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("creationdate"):
                            _CreationDate=ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("createdby"):
                            _CreatedBy=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("modificationdate"):
                            _ModificationDate=ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("modifiedby"):
                            _ModifiedBy=ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("maximumsearchdays"):
                            _MaximumSearchDays=ipoLib.NVL(ref dr, dc.ColumnName, -1); //CR 29099 3/25/10 JNE
                            break;
                        case("checkimagedisplaymode"):
                            //_CheckImageDisplayMode=ipoLib.NVL(ref dr, dc.ColumnName,(byte)0);
                            try {
                                _CheckImageDisplayMode = (OLFImageDisplayMode)ipoLib.NVL(ref dr,dc.ColumnName,(byte)OLFImageDisplayMode.BothSides);
                            } catch(Exception) {
                                //TODO: Throw Message to log: "Invalid Checks Image Display Mode: " + 
                                //                            dr[dc].ToString() + ".  Defaulting to display both sides."
                                _CheckImageDisplayMode = OLFImageDisplayMode.BothSides;
                            }
                            break;
                        case("documentimagedisplaymode"):
                            //_DocumentImageDisplayMode=ipoLib.NVL(ref dr, dc.ColumnName,(byte)0);
                            try {
                                _DocumentImageDisplayMode = (OLFImageDisplayMode)ipoLib.NVL(ref dr,dc.ColumnName,(byte)OLFImageDisplayMode.BothSides);
                            } catch(Exception) {
                                //TODO: Throw Message to log: "Invalid Document Image Display Mode: " + 
                                //                            dr[dc].ToString() + ".  Defaulting to display both sides."
                                _DocumentImageDisplayMode = OLFImageDisplayMode.BothSides;
                            }
                            break;
                        case ("brandingschemeid"):
                            _BrandingSchemeID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("displaybatchid"):
                            _DisplayBatchID = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <returns></returns>
        public XmlNode ToXml(XmlNode nodeParent) {
            return(this.ToXml(nodeParent, "OLCustomer"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public XmlNode ToXml(XmlNode nodeParent, string nodeName) {

            XmlNode nodeOLCustomer;
            XmlAttribute att;

            nodeOLCustomer = nodeParent.OwnerDocument.CreateNode(XmlNodeType.Element, nodeName, nodeParent.NamespaceURI);

            att = nodeParent.OwnerDocument.CreateAttribute("OLCustomerID", nodeParent.NamespaceURI);
            att.Value = this.OLCustomerID.ToString();
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("CustomerCode", nodeParent.NamespaceURI);
            att.Value = this.CustomerCode;
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("Description", nodeParent.NamespaceURI);
            att.Value = this.Description;
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("IsActive", nodeParent.NamespaceURI);
            att.Value = this.IsActive.ToString();
            nodeOLCustomer.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("IsParent", nodeParent.NamespaceURI);
            att.Value = this.IsParent.ToString();
            nodeOLCustomer.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("OLOrganizationParentID", nodeParent.NamespaceURI);
            att.Value = this.ParentCode.ToString();
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("ViewingDays", nodeParent.NamespaceURI);
            att.Value = this.ViewingDays.ToString();
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("ExternalID1", nodeParent.NamespaceURI);
            att.Value = this.ExternalID1;
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("ExternalID2", nodeParent.NamespaceURI);
            att.Value = this.ExternalID2;
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("CreationDate", nodeParent.NamespaceURI);
            att.Value = this.CreationDate.ToString();
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("CreatedBy", nodeParent.NamespaceURI);
            att.Value = this.CreatedBy;
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("ModificationDate", nodeParent.NamespaceURI);
            att.Value = this.ModificationDate.ToString();
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("ModifiedBy", nodeParent.NamespaceURI);
            att.Value = this.ModifiedBy;
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("MaximumSearchDays", nodeParent.NamespaceURI);
            att.Value = this.MaximumSearchDays.ToString();
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("CheckImageDisplayMode", nodeParent.NamespaceURI);
            att.Value = this.CheckImageDisplayMode.ToString();
            nodeOLCustomer.Attributes.Append(att);
            
            att = nodeParent.OwnerDocument.CreateAttribute("DocumentImageDisplayMode", nodeParent.NamespaceURI);
            att.Value = this.DocumentImageDisplayMode.ToString();
            nodeOLCustomer.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("BrandingSchemeID", nodeParent.NamespaceURI);
            att.Value = this.BrandingSchemeID.ToString();
            nodeOLCustomer.Attributes.Append(att);

            att = nodeParent.OwnerDocument.CreateAttribute("DisplayBatchID", nodeParent.NamespaceURI);
            att.Value = this.DisplayBatchID.ToString();
            nodeOLCustomer.Attributes.Append(att);
                       
            nodeParent.AppendChild(nodeOLCustomer);

            return(nodeOLCustomer);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeCustomer"></param>
        /// <returns></returns>
        public bool FromXml(XmlNode nodeCustomer) {
            int intTemp;
            bool bolTemp;
            Guid gidTemp;

            try {
                foreach (XmlAttribute att in nodeCustomer.Attributes) {
                    switch (att.Name.ToLower()) {
                        case ("olcustomerid"):
                            if (ipoLib.TryParse(att.Value, out gidTemp)) {
                                _OLCustomerID = gidTemp;
                            } else {
                                _OLCustomerID = Guid.Empty;
                            }
                            break;
                        case ("customercode"):
                            _CustomerCode = att.Value;
                            break;
                        case ("description"):
                            _Description = att.Value;
                            break;
                        case ("isactive"):
                            if(int.TryParse(att.Value, out intTemp)) {
                                _IsActive = (intTemp > 0);
                            } else if(bool.TryParse(att.Value, out bolTemp)) {
                                _IsActive = bolTemp;
                            } else {
                                _IsActive = false;
                            }
                            break;
                        case ("isparent"):
                            if (int.TryParse(att.Value, out intTemp))
                            {
                                _IsParent = (intTemp > 0);
                            }
                            else if (bool.TryParse(att.Value, out bolTemp))
                            {
                                _IsParent = bolTemp;
                            }
                            else
                            {
                                _IsParent = false;
                            }
                            break;
                        case ("olorganizationparentid"):
                            if (ipoLib.TryParse(att.Value, out gidTemp))
                            {
                                _ParentCode = gidTemp;
                            }
                            else
                            {
                                _ParentCode = Guid.Empty;
                            }
                            break;
                        case ("viewingdays"):
                            if (int.TryParse(att.Value, out intTemp)) {
                                _ViewingDays = intTemp;
                            }
                            break;
                        case ("externalid1"):
                            _ExternalID1 = att.Value;
                            break;
                        case ("externalid2"):
                            _ExternalID2 = att.Value;
                            break;
                        case ("creationdate"):
                            _CreationDate = DateTime.Parse(att.Value);
                            break;
                        case ("createdby"):
                            _CreatedBy = att.Value;
                            break;
                        case ("modificationdate"):
                            _ModificationDate = DateTime.Parse(att.Value);
                            break;
                        case ("modifiedby"):
                            _ModifiedBy = att.Value;
                            break;
                        case ("maximumsearchdays"):
                            if (int.TryParse(att.Value, out intTemp)) {
                                _MaximumSearchDays = intTemp;
                            }
                            break;
                        case ("checkimagedisplaymode"):
                            if (int.TryParse(att.Value, out intTemp)) {
                                _CheckImageDisplayMode = (OLFImageDisplayMode) intTemp;
                            } else {
                                _CheckImageDisplayMode = OLFImageDisplayMode.BothSides;
                            }
                            break;
                        case ("documentimagedisplaymode"):
                            if (int.TryParse(att.Value, out intTemp)) {
                                _DocumentImageDisplayMode = (OLFImageDisplayMode)intTemp;
                            } else {
                                _DocumentImageDisplayMode = OLFImageDisplayMode.BothSides;
                            }
                            break;
                        case ("brandingschemeid"):
                            if (int.TryParse(att.Value, out intTemp)) {
                                _BrandingSchemeID = intTemp;
                            }
                            break;
                        case ("displaybatchid"):
                           if(int.TryParse(att.Value, out intTemp)) {
                                _DisplayBatchID = (intTemp > 0);
                            } else if(bool.TryParse(att.Value, out bolTemp)) {
                                _DisplayBatchID = bolTemp;
                            } else {
                                _DisplayBatchID = false;
                            }
                            break;
                    }
                }
            } catch (Exception e) {
                throw (e);
            }

            return (true);
        }

	}
}
