using System;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*  Module:           
* 
*  Filename:         cRemitter.cs
* 
*  Author:           Joel Caples
* 
*  Description:
* 
*    
* 
*  Revisions:
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
**********************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cRemitter {

        private Guid _LockboxRemitterID = Guid.Empty;
        private Guid _GlobalRemitterID = Guid.Empty;
        private int _BankID = -1;
        private int _CustomerID = -1;
        private int _LockboxID = -1;
        private string _RoutingNumber = string.Empty;
        private string _Account = string.Empty;
        private string _GlobalRemitterName = string.Empty;
        private string _LockboxRemitterName = string.Empty;
        private int _TimesRemitterUsed = 0;
        private DateTime _CreationDate = DateTime.MinValue;
        private string _CreatedBy = string.Empty;
        private DateTime _ModificationDate = DateTime.MinValue;
        private string _ModifiedBy = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public Guid LockboxRemitterID {
            get {
                return(_LockboxRemitterID);
            }
            set {
                _LockboxRemitterID = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Guid GlobalRemitterID {
            get {
                return(_GlobalRemitterID);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public int BankID {
            get {
                return(_BankID);
            }
            set {
                _BankID = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public int CustomerID {
            get {
                return(_CustomerID);
            }
            set {
                _CustomerID = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int LockboxID {
            get {
                return(_LockboxID);
            }
            set {
                _LockboxID = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string RoutingNumber {
            get {
                return(_RoutingNumber.Trim());
            }
            set {
                _RoutingNumber = value.Trim();
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string Account {
            get {
                return(_Account);
            }
            set {
                _Account = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string GlobalRemitterName {
            get {
                return(_GlobalRemitterName);
            }
            set {
                _GlobalRemitterName = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string LockboxRemitterName {
            get {
                return(_LockboxRemitterName);
            }
            set {
                _LockboxRemitterName = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public int TimesRemitterUsed {
            get {
                return(_TimesRemitterUsed);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreationDate {
            get {
                return(_CreationDate);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string CreatedBy {
            get {
                return(_CreatedBy);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime ModificationDate {
            get {
                return(_ModificationDate);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string ModifiedBy {
            get {
                return(_ModifiedBy);
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(ref DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
                        case("lockboxremitterid"):
                            _LockboxRemitterID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("globalremitterid"):
                            _GlobalRemitterID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case("bankid"):
                            _BankID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("customerid"):
                            _CustomerID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("lockboxid"):
                            _LockboxID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("routingnumber"):
                            _RoutingNumber = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("account"):
                            _Account = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("globalremittername"):
                            _GlobalRemitterName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("lockboxremittername"):
                            _LockboxRemitterName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("timesremitterused"):
                            _TimesRemitterUsed = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case("creationdate"):
                            _CreationDate = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("createdby"):
                            _CreatedBy = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case("modificationdate"):
                            _ModificationDate = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("modifiedby"):
                            _ModifiedBy = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }


        /// <summary>
        /// Create an XML node of the current Contact with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public XmlNode AsXMLNode(string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;

            try {

                // Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                // Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);

                // set each property as an attribute to the node
                ipoXmlLib.addAttribute(node, "Account", this.Account.Trim());
                ipoXmlLib.addAttribute(node, "BankID", this.BankID.ToString());
                ipoXmlLib.addAttribute(node, "CreatedBy", this.CreatedBy.Trim());
                ipoXmlLib.addAttribute(node, "CreationDate", this.CreationDate.ToString());
                ipoXmlLib.addAttribute(node, "CustomerID", this.CustomerID.ToString());
                ipoXmlLib.addAttribute(node, "GlobalRemitterID", this.GlobalRemitterID.ToString());
                ipoXmlLib.addAttribute(node, "GlobalRemitterName", this.GlobalRemitterName.Trim());
                ipoXmlLib.addAttribute(node, "LockboxID", this.LockboxID.ToString());
                ipoXmlLib.addAttribute(node, "LockboxRemitterID", this.LockboxRemitterID.ToString());
                ipoXmlLib.addAttribute(node, "LockboxRemitterName", this.LockboxRemitterName.Trim());
                ipoXmlLib.addAttribute(node, "ModificationDate", this.ModificationDate.ToString());
                ipoXmlLib.addAttribute(node, "ModifiedBy", this.ModifiedBy.Trim());
                ipoXmlLib.addAttribute(node, "RoutingNumber", this.RoutingNumber.Trim());
                ipoXmlLib.addAttribute(node, "TimesRemitterUsed", this.TimesRemitterUsed.ToString());
                
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }
    }
}
