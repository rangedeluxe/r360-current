﻿using System;
using System.Collections.Generic;
using System.Text;


/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:    3/2/2011
*
* Purpose:  cICONBll.cs.
*
* Modification History
* 03/02/2011 CR 31536 WJS
*     - Initial release.
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
******************************************************************************/
namespace WFS.RecHub.Common
{
    /// <summary>
    /// Summary description for cData.
    /// </summary>
    public class cData
    {
        private int     _DataSetupFieldKey = 0;
        private string  _Keyword = string.Empty;
        private int     _DataType = 0;
        private string  _DataValue = string.Empty;

        /// <summary>
        /// Constructor for cData objects
        /// </summary>
        /// <param name="iDataSetupFieldKey"></param>
        /// <param name="sKeyword"></param>
        /// <param name="iDataType"></param>
        /// <param name="sDataValue"></param>
        public cData(int iDataSetupFieldKey, string sKeyword, int iDataType, string sDataValue)
        {
            _DataSetupFieldKey = iDataSetupFieldKey;
            _Keyword = sKeyword;
            _DataType = iDataType;
            _DataValue = sDataValue;
        }

        /// <summary>
        /// Data value of the data item you want to insert
        /// </summary>
        public string DataValue
        {
            get { return (_DataValue); }
            set { _DataValue = value; }
        }

        /// <summary>
        /// the field key of the item you want to put in
        /// </summary>
        public int DataSetupFieldKey
        {
            get { return (_DataSetupFieldKey); }
            set { _DataSetupFieldKey = value; }
        }

        /// <summary>
        /// Data value of the data item you want to insert
        /// </summary>
        public string Keyword
        {
            get { return (_Keyword); }
            set { _Keyword = value; }
        }

        /// <summary>
        /// the data type you want to insert
        /// </summary>
        public int DataType
        {
            get { return (_DataType); }
            set { _DataType = value; }
        }
    }
}
