﻿using System;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Common PreDefSearch object
*  Filename: cPreDefSearch.cs
*    Author: Jason Efken
*
* Revisions:
* ----------------------
* CR 31753 JNE 05/17/2011
*    -Created Class.
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
******************************************************************************/
namespace WFS.RecHub.Common {

/// <summary>
/// Summary description for cPreDefSearch.
/// </summary>
public class cPreDefSearch
	{
        private Guid _PreDefSearchID;
        private string _Name;
        private string _Description;
        private string _SearchParmsXML;
 
        /// <summary>
	    /// 
	    /// </summary>
        public cPreDefSearch(){
        }
	    /// <summary>
	    /// 
	    /// </summary>
        public bool LoadXML(XmlDocument objXML){
            Guid tmpGuid = Guid.Empty;
            bool bolRetVal = false;
            try{
                if ((objXML.DocumentElement.SelectSingleNode("PreDefSearchID") != null) && (objXML.DocumentElement.SelectSingleNode("PreDefSearchID").InnerText.ToString().Equals(LTAConstants.PREDEFSEARCH_ADHOC))){
                    PreDefSearchID = Guid.Empty;
                }else{
                    if ((objXML.DocumentElement.SelectSingleNode("PreDefSearchID") != null) && (ipoLib.TryParse(objXML.DocumentElement.SelectSingleNode("PreDefSearchID").InnerText, out tmpGuid))){
                        PreDefSearchID = tmpGuid;
                    }else{
                        PreDefSearchID = Guid.Empty;
                    }
                }
                if (objXML.DocumentElement.SelectSingleNode("SavedQueryName") != null){
                    Name = objXML.DocumentElement.SelectSingleNode("SavedQueryName").InnerText.ToString().Trim();
                }
                if (objXML.DocumentElement.SelectSingleNode("SavedQueryDescription") != null){
                    Description = objXML.DocumentElement.SelectSingleNode("SavedQueryDescription").InnerText.ToString().Trim();
                }
                SearchParmsXML = objXML.DocumentElement.OuterXml.ToString();
                bolRetVal = true;
            }catch(Exception ex){
                throw(ex);    
            }
            return bolRetVal;
        }
	    /// <summary>
	    /// 
	    /// </summary>
        public Guid PreDefSearchID{
            get {return(_PreDefSearchID);}
            set {_PreDefSearchID = value;}
        }
	    /// <summary>
	    /// 
	    /// </summary>
        public string Name{
            get {return(_Name);}
            set {_Name = value;}
        } 
	    /// <summary>
	    /// 
	    /// </summary>
        public string Description{
            get {return(_Description);}
            set {_Description = value;}
        }
	    /// <summary>
	    /// 
	    /// </summary>
        public string SearchParmsXML{
            get {return(_SearchParmsXML);}
            set {_SearchParmsXML = value;}
        }
    }
}