using System;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*  Module:           CheckBOX Online Common Alert Class Library
* 
*  Filename:         cAlert.cls
* 
*  Author:           Jon Sucha
* 
*  Description:
* 
*    This class library provides ...
* 
*  Revisions:
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
**********************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cAlert {

	    //-------------------------------------------------------------------------------
	    // Private member variables declaration
	    //-------------------------------------------------------------------------------
	    private Guid _AlertID;
	    private Guid _ContactID;
	    private Guid _DeliveryMethodID;
	    private Guid _EventRuleID;
	    private DateTime _CreationDate;
	    private string _CreatedBy;
	    private string _ModifiedBy;
	    private DateTime _ModificationDate;

        /// <summary>
        /// 
        /// </summary>
	    public Guid AlertID {
		    get { return(_AlertID); }
		    set { _AlertID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public Guid ContactID {
		    get { return(_ContactID); }
		    set { _ContactID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public Guid DeliveryMethodID {
		    get { return(_DeliveryMethodID); }
		    set { _DeliveryMethodID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public Guid EventRuleID {
		    get { return(_EventRuleID); }
		    set { _EventRuleID = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public DateTime CreationDate {
		    get { return(_CreationDate); }
		    set { _CreationDate = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string CreatedBy {
		    get { return(_CreatedBy); }
		    set { _CreatedBy = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public DateTime ModificationDate {
		    get { return(_ModificationDate); }
		    set { _ModificationDate = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
	    public string ModifiedBy {
		    get { return(_ModifiedBy); }
		    set { _ModifiedBy = value; }
	    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public bool LoadDataRow(DataRow dr) {

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
					    case "contactid":
						    _ContactID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
						    break;
					    case "alertid":
						    _AlertID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
						    break;
					    case "deliverymethodid":
						    _DeliveryMethodID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
						    break;
					    case "eventruleid":
						    _EventRuleID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
						    break;
					    case "creationdate":
						    _CreationDate = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
						    break;
					    case "createdby":
						    _CreatedBy = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty).TrimEnd();
						    break;
					    case "modificationdate":
						    _ModificationDate = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
						    break;
					    case "modifiedby":
						    _ModifiedBy = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty).TrimEnd();
						    break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// Create an XML node of the current Alert with properties as attributes
        /// </summary>
        /// <param name="NodeName"></param>
        /// <returns>
        /// IXMLDomNode reference
        ///           Format: <NodeName Property="Value"/>
        /// </returns>
        public XmlNode AsXMLNode(string NodeName) {
            
            XmlDocument xmlDoc;
            XmlNode node;

            try {

                // Get XML DOM Document
                xmlDoc = ipoXmlLib.GetXMLDoc();
                
                // Add the node to the XML Document
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, NodeName);

                // set each property as an attribute to the node
                ipoXmlLib.addAttribute(node, "AlertID", this.AlertID.ToString());
                ipoXmlLib.addAttribute(node, "ContactID", this.ContactID.ToString());
                ipoXmlLib.addAttribute(node, "CreatedBy", this.CreatedBy);
                ipoXmlLib.addAttribute(node, "CreationDate", this.CreationDate.ToString());
                ipoXmlLib.addAttribute(node, "DeliveryMethodID", this.DeliveryMethodID.ToString());
                ipoXmlLib.addAttribute(node, "EventRuleID", this.EventRuleID.ToString());
                ipoXmlLib.addAttribute(node, "ModificationDate", this.ModificationDate.ToString());
                ipoXmlLib.addAttribute(node, "ModifiedBy", this.ModifiedBy);
                
                // return the node reference
                return(node);
            } catch(Exception) {
                return(null);
            }
        }
    }
}
