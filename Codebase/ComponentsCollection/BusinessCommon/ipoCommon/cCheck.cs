using System;
using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 7728 JMC 06/01/2004
*    -Created cCheck Class
* CR 23661 JCS 0818/2008
*    -Added RemitterName to class
* CR 32562 JMC 03/02/2011 
*    -Modified class to implement the IItem interface in order to
*    -To adhere to the IItem interface, added DepositDate, TransactionID, TxnSequence, and 
*     DefaultColorMode properties
*    -FileDescriptor is no longer an override property
* CR 49316 WJS  02/08/2012 
*    Add sourceProcessingDateKey
*    Add back processingDateKey
* WI 90242 CRG 03/05/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoCommon
* WI 148949 DJW 6/19/2013
*   Added SessionID to methods for sprocs that needed it (related to RAAM)
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// Check abstraction object.  inherits from Image object.
    /// </summary>
	public class cCheck : cImage, IItem, IComparable {

        private DateTime _DepositDate = DateTime.MinValue;
        private int _TransactionID = -1;
        private int _TxnSequence = -1;
        private int _CheckSequence=-1;
        private string _CheckRT = string.Empty;
        private string _CheckAccount = string.Empty;
        private string _CheckSerial = string.Empty;
        private decimal _CheckAmount;
        private string _RemitterName = string.Empty;
        private WorkgroupColorMode _DefaultColorMode = WorkgroupColorMode.COLOR_MODE_BITONAL;

        /// <summary>
        /// Default Constructor; required for serialization.
        /// </summary>
		public cCheck() {
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DepositDate {
            get { return _DepositDate; }
            set { _DepositDate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TransactionID {
            get { return _TransactionID; }
            set { _TransactionID=value; }
        }

        /// <summary>
        /// Transactions.TxnSequence
        /// </summary>
        public int TxnSequence{
            get { return(_TxnSequence); }
            set { _TxnSequence=value; }
        }

        /// <summary>
        /// Checks.CheckSequence
        /// </summary>
        public int CheckSequence {
            get { return _CheckSequence; }
            set { _CheckSequence=value; }
        }

        /// <summary>
        /// Checks.RT
        /// </summary>
        public string RT {
            get { return _CheckRT; }
            set { _CheckRT=value; }
        }

        /// <summary>
        /// Checks.Account
        /// </summary>
        public string Account {
            get { return _CheckAccount; }
            set { _CheckAccount=value; }
        }

        /// <summary>
        /// Checks.Serial
        /// </summary>
        public string Serial {
            get { return _CheckSerial; }
            set { _CheckSerial=value; }
        }

        /// <summary>
        /// Checks.Amount
        /// </summary>
        public decimal Amount {
            get { return _CheckAmount; }
            set { _CheckAmount=value; }
        }

        /// <summary>
        /// Checks.RemitterName
        /// </summary>
        public string RemitterName {
            get { return _RemitterName; }
            set { _RemitterName = value; }
        }
        
        /// <summary>
        /// Used to override the base cImage objects property.
        /// Hardcoded to 'C', which indicates that the image is a check.
        /// </summary>
        public string FileDescriptor {
            get { return "C"; }
        }

        /// <summary>
        /// 
        /// 
        /// </summary>
        public WorkgroupColorMode DefaultColorMode
        {
            get { return (_DefaultColorMode); }
            set { _DefaultColorMode = value; }
        }        

        public Guid SessionID
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public int SourceProcessingDateKey
        {
            get;
            set;
        }


        /// <summary>
        /// Orders class in a collection by BatchSequence, and then by CheckSequence.
        /// </summary>
        /// <param name="x">cCheck object to compare this object to.</param>
        /// <returns>
        /// A signed number indicating the relative values of this instance and value.    
        /// Return Value Description:
        ///     Less than zero: This instance is less than value.     
        ///     Zero: This instance is equal to value.     
        ///     Greater than zero:  This instance is greater than value.  -or-  value is null.     
        /// </returns>
        public int CompareTo(object x) {

            if (x is cCheck) {

                cCheck objCheckX = (cCheck)x;

                if(this.BankID == objCheckX.BankID) {

                    if(this.LockboxID == objCheckX.LockboxID) {

                        if(this.BatchID == objCheckX.BatchID) {

                            if(this.TransactionID == objCheckX.TransactionID) {

                                if(this.BatchSequence == objCheckX.BatchSequence) {
                                    return(this.CheckSequence.CompareTo(objCheckX.CheckSequence));
                                } else {
                                    return(this.BatchSequence.CompareTo(objCheckX.BatchSequence));
                                }

                            } else {
                                return(this.TransactionID.CompareTo(objCheckX.TransactionID));
                            }

                        } else {
                            return(this.BatchID.CompareTo(objCheckX.BatchID));
                        }

                    } else {
                        return(this.LockboxID.CompareTo(objCheckX.LockboxID));
                    }

                } else {
                    return(this.BankID.CompareTo(objCheckX.BankID));
                }
           }
            else {
                throw(new Exception("Invalid check object"));
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public override bool LoadDataRow(ref DataRow dr) {

            base.LoadDataRow(ref dr);

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
                        case("depositdate"):
                            this.DepositDate=ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case("transactionid"):
                            this.TransactionID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("txnsequence"):
                            this.TxnSequence=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("rt"):
                        case ("routingnumber"):
                            this.RT=ipoLib.NVL(ref dr, dc.ColumnName, "");
                            break;
                        case("account"):
                            this.Account=ipoLib.NVL(ref dr, dc.ColumnName, "");
                            break;
                        case("serial"):
                            this.Serial=ipoLib.NVL(ref dr, dc.ColumnName, "");
                            break;
                        case("amount"):
                            this.Amount=ipoLib.NVL(ref dr, dc.ColumnName, (Decimal)(1.0));
                            break;
                        case("checksequence"):
                            this.CheckSequence=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("remittername"):
                            this.RemitterName = ipoLib.NVL(ref dr, dc.ColumnName, "");
                            break;
                        case ("onlinecolormode"):
                        case ("clientaccountcolormode"):
                        case ("defaultcolormode"):
                            switch ((byte)ipoLib.NVL(ref dr, dc.ColumnName, (byte)WorkgroupColorMode.COLOR_MODE_BITONAL))
                            {
                                case (byte)WorkgroupColorMode.COLOR_MODE_GRAYSCALE:
                                    _DefaultColorMode = WorkgroupColorMode.COLOR_MODE_GRAYSCALE;
                                    break;
                                case (byte)WorkgroupColorMode.COLOR_MODE_COLOR:
                                    _DefaultColorMode = WorkgroupColorMode.COLOR_MODE_COLOR;
                                    break;
                                default:
                                    _DefaultColorMode = WorkgroupColorMode.COLOR_MODE_BITONAL;
                                    break;
                            }
                            break;
                        case ("processingdatekey"):
                            this.ProcessingDateKey = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("sourceprocessingdatekey"):
                            this.SourceProcessingDateKey = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }
	}
}
