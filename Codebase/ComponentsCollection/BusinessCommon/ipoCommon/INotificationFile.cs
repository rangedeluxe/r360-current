﻿using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 07/017/2012
*
* Purpose:	Interface for the Notification File
*
* Modification History
* CR 54208 JMC 07/17/2012
*   -Initial Version used for File Import toolkit.
* WI 89972 CRG 03/01/2013
*	Modify ipoCommon for .NET 4.5 and NameSpace
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary></summary>
    public interface INotificationFile {

        /// <summary></summary>
        int NotificationDateKey {
            get;
            set;
        }

        /// <summary></summary>
        int BankID {
            get;
            set;
        }

        /// <summary></summary>
        int CustomerID {
            get;
            set;
        }

        /// <summary></summary>
        int LockboxID {
            get;
            set;
        }

        /// <summary></summary>
        Guid FileIdentifier {
            get;
            set;
        }

        /// <summary></summary>
        string FileType {
            get;
            set;
        }

        /// <summary></summary>
        string UserFileName {
            get;
            set;
        }

        /// <summary></summary>
        string FileExtension {
            get;
            set;
        }
    }
}
