using System;
using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:	Wayne Schwarz
* Date:		04/01/2011
*
* Purpose:	Stub abstraction object.
*
* Modification History
*  4/1/2011 CR 31536 WJS Initial Creation
*  02/08/2012 CR 49316 WJS Add sourceProcessingDateKey
*  02/29/2012 CR 49316 WJS Add back ProcessingDateKey
* WI 89972 CRG 03/01/2013
*	Modify ipoCommon for .NET 4.5 and NameSpace
* WI 148949 DJW 6/19/2013
*    Added SessionID to methods for sprocs that needed it (related to RAAM)
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// Stub abstraction object.  
    /// </summary>
    public class cStub : cImage, IItem, IComparable
    {
        /// <summary>
        /// Default Constructor; required for serialization.
        /// </summary>
		public cStub() {
        }

        public WorkgroupColorMode DefaultColorMode { get; set; } = WorkgroupColorMode.COLOR_MODE_BITONAL;
        public DateTime DepositDate { get; set; } = DateTime.MinValue;

        /// <summary>
        /// Documents.FileDescriptor
        /// </summary>
        public string FileDescriptor { get; set; } = "";

        /// <summary>
        /// True if the stub has a corresponding document; false if not.
        /// </summary>
        public bool HasDocument { get; set; }

        public int TransactionID { get; set; }

        /// <summary>
        /// Transactions.TxnSequence
        /// </summary>
        public int TxnSequence { get; set; }

        /// <summary>
        /// Stubs.StubSequence
        /// </summary>
        public int StubSequence { get; set; }

        /// <summary>
        /// Stubs.DocumentBatchSequence
        /// </summary>
        public int DocumentBatchSequence { get; set; }

        /// <summary>
        /// Stubs.AccountNumber
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Stubs.Amount
        /// </summary>
        public decimal Amount { get; set; }

        public int SourceProcessingDateKey { get; set; }
        public Guid SessionID { get; set; }

        /// <summary>
        /// Orders class in a collection by BatchSequence, and then by StubSequence.
        /// </summary>
        /// <param name="x">cStub object to compare this object to.</param>
        /// <returns>
        /// A signed number indicating the relative values of this instance and value.    
        /// Return Value Description:
        ///     Less than zero: This instance is less than value.     
        ///     Zero: This instance is equal to value.     
        ///     Greater than zero:  This instance is greater than value.  -or-  value is null.     
        /// </returns>
        public int CompareTo(object x) {

            if (x is cStub) {

                cStub objStubX = (cStub)x;

                if(this.BankID == objStubX.BankID) {

                    if(this.LockboxID == objStubX.LockboxID) {

                        if(this.BatchID == objStubX.BatchID) {

                            if(this.TransactionID == objStubX.TransactionID) {

                                if(this.BatchSequence == objStubX.BatchSequence) {
                                    return (this.StubSequence.CompareTo(objStubX.StubSequence));
                                } else {
                                    return(this.BatchSequence.CompareTo(objStubX.BatchSequence));
                                }

                            } else {
                                return(this.TransactionID.CompareTo(objStubX.TransactionID));
                            }

                        } else {
                            return(this.BatchID.CompareTo(objStubX.BatchID));
                        }

                    } else {
                        return(this.LockboxID.CompareTo(objStubX.LockboxID));
                    }

                } else {
                    return(this.BankID.CompareTo(objStubX.BankID));
                }
           }
            else {
                throw(new Exception("Invalid stub object"));
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public override bool LoadDataRow(ref DataRow dr)
        {

            base.LoadDataRow(ref dr);

            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn dc in dr.Table.Columns) {
                    switch(dc.ColumnName.ToLower()) {
                        case("transactionid"):
                            this.TransactionID=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("txnsequence"):
                            this.TxnSequence=ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case("accountnumber"):
                            this.AccountNumber=ipoLib.NVL(ref dr, dc.ColumnName, "");
                            break;
                        case("amount"):
                            this.Amount=ipoLib.NVL(ref dr, dc.ColumnName, (Decimal)(1.0));
                            break;
                        case("documentbatchsequence"):
                            this.DocumentBatchSequence = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("stubsequence"):
                            this.StubSequence = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case "hasdocument":
                            HasDocument = ipoLib.NVL(ref dr, dc.ColumnName, 0) != 0;
                            break;
                        case ("filedescriptor"):
                            this.FileDescriptor = ipoLib.NVL(ref dr, dc.ColumnName, "");
                            break;
                        case ("defaultcolormode"):
                        case ("clientaccountcolormode"):
                        case ("onlinecolormode"):
                            switch ((byte)ipoLib.NVL(ref dr, dc.ColumnName, (byte)WorkgroupColorMode.COLOR_MODE_BITONAL))
                            {
                                case (byte)WorkgroupColorMode.COLOR_MODE_GRAYSCALE:
                                    DefaultColorMode = WorkgroupColorMode.COLOR_MODE_GRAYSCALE;
                                    break;
                                case (byte)WorkgroupColorMode.COLOR_MODE_COLOR:
                                    DefaultColorMode = WorkgroupColorMode.COLOR_MODE_COLOR;
                                    break;
                                default:
                                    DefaultColorMode = WorkgroupColorMode.COLOR_MODE_BITONAL;
                                    break;
                            }
                            break;
                        case("depositdate"):
                            this.DepositDate=ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case ("processingdatekey"):
                            this.ProcessingDateKey = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case ("sourceprocessingdatekey"):
                            this.SourceProcessingDateKey = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            
                            break;
                        
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }
	}
}
