﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;

/*********************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:     8/10/2011
*
* Purpose:  Description of functionality.
*
* Modification History
* 08/10/2011 CR 32263  WJS 
*     - Initial release.
* CR 28865 JNE 8/26/2011
*    -Added MaximumQueriesSaved preference
* CR 45637 JNE 8/31/2011
*   - Made MaximumQueriesSaved text to lower case  
* CR 46238 WJS  8/30/2011
*   - Added support for password notification from email address
* CR 46314 WJS 09/26/2011
*    -Add new preferences for forgot password
* CR 46315,46316,46317,46318 JNE 12/20/2012
*    -Modified to use IList<cOLPreference> instead of hard coding preference names
* CR 54173 JNE 08/08/2012
*    -Removed Obsolete tag. 
* WI 71427 CEJ 12/04/2012 
*  - Add MaxPrintableRows property to OLPrefences
* WI 90253 CRG 03/18/2013 
*  Change the NameSpace, Framework, Using's and Flower Boxes for ltaBusinessCommonLib
* WI 103280 WJS 5/16/2013
 * Change get Prefs to method to allow ilmerge issues to resolve.
* WI 111080 EAS 08/05/2013
*  Add Connection type.  Allow for differenct connection scenarios.
*********************************************************************************/
namespace WFS.RecHub.BusinessCommon
{
    public class OLPreferences
    {
        private WFS.RecHub.DAL._DMPObjectRoot.ConnectionType _connection;
        private cOLPreferences _preferences = null;
        private IList<cOLPreference> _ILPreferences= null;

        public OLPreferences(string vSiteKey)
        {
            _preferences = new cOLPreferences(vSiteKey);
        }

        public OLPreferences(string vSiteKey, WFS.RecHub.DAL._DMPObjectRoot.ConnectionType enmConnection) : this(vSiteKey)
        {
            _connection = enmConnection;
        }

        public bool LoadOLPreferences(int userID)
        {
            DataTable dtPreferences = null;
            bool bolRetVal = false;

            try
            {
                using (cPreferencesDAL prefDAL = GetPreferencesDAL())
                {
                    if (prefDAL.GetOLPreferences(userID, out dtPreferences))
                    {
                        ProcessPreferences(dtPreferences);
                    }
                    if (prefDAL.GetOLPreferencesWithDescriptions(userID, out dtPreferences))
                    {
                       ProcessPreferencesToList(dtPreferences);
                    }
                }
                bolRetVal = true;
            }
            catch (Exception)
            {
                bolRetVal = false;
            }
            finally
            {
                if (dtPreferences != null)
                {
                    dtPreferences.Dispose();
                    dtPreferences = null;
                }
            }
            return (bolRetVal);
        }
        /// <summary>
        /// Get OL Preferences
        /// </summary>
        /// <returns></returns>
        public bool LoadOLPreferences()
        {
            DataTable dtPreferences = null;
            bool bolRetVal = false;

            try
            {
                using (cPreferencesDAL prefDAL = GetPreferencesDAL())
                {
                    if (prefDAL.GetOLPreferences(out dtPreferences))
                    {
                        ProcessPreferences(dtPreferences);
                    }
                    if (prefDAL.GetOLPreferencesWithDescriptions(out dtPreferences))
                    {
                       ProcessPreferencesToList(dtPreferences);
                    }
                }
                bolRetVal = true;

            }
            catch (Exception)
            {
                bolRetVal = false;
            }
            finally
            {
                if (dtPreferences != null)
                {
                    dtPreferences.Dispose();
                    dtPreferences = null;
                }
            }
            return (bolRetVal);
        }

        public cOLPreferences GetPrefs()
        {
            return (_preferences);
        }

        public IList<cOLPreference> GetPreferences
        {
            get{ return (_ILPreferences); }
        }

        public  XmlNode AsXMLNode(string sAttributeName, string sNodeName, string sModule) {
            XmlDocument xmlDoc;
            XmlNode node;
            XmlNode nodeRetVal = null;
            OLPreferenceAppType AppType;
            
            try {
                // Match sModule to OLPreferenceAppType.
                switch(sModule) {
                    case "Online": AppType = OLPreferenceAppType.Online; break;
                    case "CSRResearch": AppType = OLPreferenceAppType.CSRResearch; break;
                    case "Admin": AppType = OLPreferenceAppType.Admin; break;
                    case "System": AppType = OLPreferenceAppType.System; break;
                default: AppType = OLPreferenceAppType.System; break;
                }

                xmlDoc = ipoXmlLib.GetXMLDoc();
                node = ipoXmlLib.addElement(xmlDoc.DocumentElement, sNodeName);
                ipoXmlLib.addAttribute(node, "Name", sAttributeName);

                //Loop through preferences only appending Module specific and Display Options
                foreach(cOLPreference preference in this._ILPreferences){
                    if (preference.AppType == (int)AppType && preference.PreferenceGroup == "Display Options"){
                        node.AppendChild(xmlDoc.ImportNode(preference.AsXMLNode(), true));
                    }
                }
                nodeRetVal = node;              
                //return reference to node
                return(nodeRetVal);
            
            } catch(Exception) {
                return(nodeRetVal);
            }
        }     
            
        private void ProcessPreferencesToList(DataTable dtPreferences)
        {
			//Map class names to column names in datatable.
			// mapping.. (field name, class name)
            Dictionary<string,string> mappings = new Dictionary<string,string>();
            mappings.Add("PreferenceName","PreferenceName");
            mappings.Add("DefaultSetting","DefaultSetting");
            mappings.Add("UserSettings","UserSettings");
            mappings.Add("PreferenceGroup","PreferenceGroup");
            mappings.Add("AppType","AppType");
            _ILPreferences = dtPreferences.ToList<cOLPreference>(mappings);
        }
        /// <summary>
        /// Determine the effective OLPreference to be used, User or Default
        /// </summary>
        /// <param name="DefaultSetting"></param>
        /// <param name="UserSetting"></param>
        /// <param name="IsOnline"></param>
        /// <returns></returns>
        private string DetermineOLPreferenceSetting(DataRow dr)
        {
            string strEffectiveSetting = string.Empty;
            bool isOnline =   (bool)dr["IsOnline"];
            if (dr.Table.Columns.Contains("UserSetting") && isOnline && dr["UserSetting"].ToString().Length > 0)
            {
                strEffectiveSetting = dr["UserSetting"].ToString();
            }
            else
            {
                strEffectiveSetting = dr["DefaultSetting"].ToString();
            }

            return (strEffectiveSetting);
        }

        private void ProcessPreferences(DataTable dtPreferences)
        {
            int intTemp;
            foreach (DataRow dr in dtPreferences.Rows)
            {
                if (!dr.IsNull("PreferenceName"))
                {
                    switch (dr["PreferenceName"].ToString().ToLower())
                    {
                        case "maxprintablerows":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp)) {
                                _preferences.maxPrintableRows = intTemp;
                            }
                            break;

                        case "maxlogonattempts":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                _preferences.MaxLogonAttempts = intTemp;
                            }
                            break;
                        case "sessionexpiration":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                _preferences.SessionExpiration = intTemp;
                            }
                            break;
                        case "passwordexpiration":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                _preferences.PasswordExpiration = intTemp;
                            }
                            break;
                        case "usecutoff":
                            _preferences.UseCutoff = DetermineOLPreferenceSetting(dr).CompareTo("Y") == 0;
                            break;
                        case "displayscannedcheckonline":
                            _preferences.DisplayScannedCheckOnline = DetermineOLPreferenceSetting(dr).CompareTo("Y") == 0;
                            break;
                        case "displayscannedcheckresearch":
                            _preferences.DisplayScannedCheckResearch = DetermineOLPreferenceSetting(dr).CompareTo("Y") == 0;
                            break;
                        case "recordsperpage":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                if (intTemp > 0 && intTemp <= 1000)
                                {
                                    _preferences.RecordsPerPage = intTemp;
                                }
                                else
                                {
                                    _preferences.RecordsPerPage = 20;
                                }
                            }
                            else
                            {
                                _preferences.RecordsPerPage = 20;
                            }
                            break;
                        case "lockboxsummaryrecordsperpage":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                if (intTemp > 0 && intTemp <= 1000)
                                {
                                    _preferences.LockboxSummaryRecordsPerPage = intTemp;
                                }
                                else
                                {
                                    _preferences.LockboxSummaryRecordsPerPage = 20;
                                }
                            }
                            else
                            {
                                _preferences.LockboxSummaryRecordsPerPage = 20;
                            }
                            break;
                        case "displayremitternameinpdf":
                            _preferences.DisplayRemitterNameInPDF = DetermineOLPreferenceSetting(dr).CompareTo("Y") == 0;
                            break;
                        case "checkimagedisplaymode":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                _preferences.CheckImageDisplayMode = (OLFImageDisplayMode)intTemp;
                            }

                            break;
                        case "documentimagedisplaymode":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                _preferences.DocumentImageDisplayMode = (OLFImageDisplayMode)intTemp;
                            }
                            break;
                        case "usecutoffresearch":
                            _preferences.UseCutoffResearch = DetermineOLPreferenceSetting(dr).CompareTo("Y") == 0;;
                            break;
                        case "viewprocessaheadresearch":
                            _preferences.ViewProcessAheadResearch = DetermineOLPreferenceSetting(dr).CompareTo("Y") == 0;
                            break;
                        //WJS CR 32263 8/3/2011 
                        case "minpasswordlength":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                if (intTemp > FFIECPasswordConstraints.PasswordMinLength)
                                {
                                    _preferences.PasswordMinLength = intTemp;
                                }
                                else
                                {
                                    _preferences.PasswordMinLength = FFIECPasswordConstraints.PasswordMinLength;
                                }
                            }
                            else
                            {
                                _preferences.PasswordMinLength = FFIECPasswordConstraints.PasswordMinLength;
                            }
                            break;
                        case "lockoutinactiveuseraccounts":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                if (intTemp > FFIECPasswordConstraints.LockoutInactiveUserAccountsMaxSize)
                                {
                                    _preferences.LockoutInactiveUserAccount = FFIECPasswordConstraints.LockoutInactiveUserAccountsMaxSize;
                                }
                                else
                                {
                                    _preferences.LockoutInactiveUserAccount = intTemp;
                                }
                            }
                            else
                            {
                                _preferences.LockoutInactiveUserAccount = FFIECPasswordConstraints.LockoutInactiveUserAccountsMaxSize;
                            }
                            break;
                        case "passwordhistoryretention":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                _preferences.PasswordHistorySize = intTemp;

                            }
                            break;
                        case "passwordrequiredformat":
                            _preferences.PasswordRequiredFormat = DetermineOLPreferenceSetting(dr);
                            break;
                        case "maximumqueriessaved":
                            if(int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                _preferences.MaximumQueriesSaved = intTemp;
                            } else 
                            {
                                _preferences.MaximumQueriesSaved = 20;
                            }
                            break;
                         case "passwordnotificationfromaddress":
                            _preferences.PasswordNotificationFromAddress = DetermineOLPreferenceSetting(dr);
                            break;
                         case "forgotpasswordnotificationsubject":
                            _preferences.ForgotPasswordNotificationSubject = DetermineOLPreferenceSetting(dr);
                            break;
                         case "changepasswordnotificationsubject":
                            _preferences.ChangePasswordNotificationSubject = DetermineOLPreferenceSetting(dr);
                            break;
                         case "changepasswordminutesexpired":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                _preferences.ChangePasswordMinutesExpired = intTemp;
                            }
                            break;
                         case "maxsecurityquestionattempts":
                            if (int.TryParse(DetermineOLPreferenceSetting(dr), out intTemp))
                            {
                                _preferences.MaxSecurityUserQuestionAttempts = intTemp;
                            }
                        
                            break;
                         case "displaybatchnumberandbatchidresearch":
                            _preferences.DisplayBatchNumberAndBatchIDResearch = DetermineOLPreferenceSetting(dr).CompareTo("Y") == 0;
                            break;
                     }
                }
            }
        }

        private cPreferencesDAL GetPreferencesDAL()
        {
            if (_connection == _DMPObjectRoot.ConnectionType.RecHubData)
                return new cPreferencesDAL(_preferences.siteKey);
            else
                return new cPreferencesDAL(_preferences.siteKey, _connection);
        }
    }
}
