﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomImageAPI.Models
{
    public class ImageDetails
    {
        public ImageDetails(string imageFullPath)
        {
            if (string.IsNullOrEmpty(imageFullPath)) { imageFullPath = string.Empty; }

            RelativePath = imageFullPath;
            Extension = Path.GetExtension(RelativePath);
        }

        public string RelativePath { get; private set; }
        public string Extension { get; private set; }
    }
}
