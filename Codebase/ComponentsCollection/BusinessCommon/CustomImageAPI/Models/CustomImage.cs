﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomImageAPI.Models
{
    public class CustomImage
    {
        public byte[] Data { get; set; }
        public ImageDetails Details {get; set;}
    }
}
