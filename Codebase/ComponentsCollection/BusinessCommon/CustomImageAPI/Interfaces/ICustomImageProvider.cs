﻿using CustomImageAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.Common;

namespace CustomImageAPI.Interfaces
{
    public interface ICustomImageProvider
    {
        PossibleResult<CustomImage> GetImage(IItem requestContext);
        PossibleResult<CustomImage> GetImage(IItem requestContext, int page = 0);
    }
}
