﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

namespace ipoPicsTests
{
    [TestClass]
    public class ImageDirectorySourceTests
    {
        private class FakeFilegroupDirectorySource : IFilegroupDirectorySource
        {
            private readonly Func<int, int, string> _getFilegroupImageDirectory;

            public FakeFilegroupDirectorySource(Func<int, int, string> getFilegroupImageDirectory)
            {
                _getFilegroupImageDirectory = getFilegroupImageDirectory;
            }

            public string GetFilegroupDirectory(int bankId, int workgroupId)
            {
                if (_getFilegroupImageDirectory != null)
                    return _getFilegroupImageDirectory(bankId, workgroupId);

                throw new InvalidOperationException(
                    "Did not expect getFilegroupImageDirectory to be called in this test");
            }
        }

        [TestInitialize]
        public void SetUp()
        {
            cEventLog.UseFakeInMemEventLog = true;
            EventLog = new cEventLog(null, 0, MessageImportance.Essential);
        }

        private cEventLog EventLog { get; set; }

        private ImageDirectorySource CreateImageDirectorySource(ImageStorageMode imageStorageMode,
            string picsImageDirectory, Func<int, int, string> getFilegroupImageDirectory = null)
        {
            var filegroupDirectorySource = new FakeFilegroupDirectorySource(getFilegroupImageDirectory);
            return new ImageDirectorySource(imageStorageMode, picsImageDirectory, filegroupDirectorySource, EventLog);
        }

        [TestMethod]
        public void Pics_ReturnsPicsImageDirectory()
        {
            var source = CreateImageDirectorySource(ImageStorageMode.PICS, @"C:\Pics");
            var dir = source.GetImageDirectory(12, 23);
            Assert.AreEqual(@"C:\Pics", dir);
        }
        [TestMethod]
        public void Hyland_ReturnsPicsDirectory()
        {
            var source = CreateImageDirectorySource(ImageStorageMode.HYLAND, @"C:\Pics");
            var dir = source.GetImageDirectory(12, 23);
            Assert.AreEqual(@"C:\Pics", dir);
        }
        [TestMethod]
        public void Custom_ReturnsPicsDirectory()
        {
            var source = CreateImageDirectorySource(ImageStorageMode.CUSTOM, @"C:\Pics");
            var dir = source.GetImageDirectory(12, 23);
            Assert.AreEqual(@"C:\Pics", dir);
        }
        [TestMethod]
        public void Filegroup_IfWorkgroupHasNullFilegroupDirectory_ReturnsPicsImageDirectory()
        {
            var source = CreateImageDirectorySource(ImageStorageMode.FILEGROUP, @"C:\Pics",
                (bankId, workgroupId) => null);
            var dir = source.GetImageDirectory(12, 23);
            Assert.AreEqual(@"C:\Pics", dir);
        }
        [TestMethod]
        public void Filegroup_IfWorkgroupHasEmptyFilegroupDirectory_ReturnsPicsImageDirectory()
        {
            var source = CreateImageDirectorySource(ImageStorageMode.FILEGROUP, @"C:\Pics",
                (bankId, workgroupId) => "");
            var dir = source.GetImageDirectory(12, 23);
            Assert.AreEqual(@"C:\Pics", dir);
        }
        [TestMethod]
        public void Filegroup_IfWorkgroupHasFilegroupDirectory_ReturnsFilegroupDirectory()
        {
            var source = CreateImageDirectorySource(ImageStorageMode.FILEGROUP, @"C:\Pics",
                (bankId, workgroupId) => @"C:\Filegroup");
            var dir = source.GetImageDirectory(12, 23);
            Assert.AreEqual(@"C:\Filegroup", dir);
        }
        [TestMethod]
        public void Filegroup_CachesFilegroupDirectories()
        {
            var idsRequestedFromDatabase = new List<string>();
            var source = CreateImageDirectorySource(ImageStorageMode.FILEGROUP, @"C:\Pics",
                (bankId, workgroupId) =>
                {
                    idsRequestedFromDatabase.Add(bankId + "," + workgroupId);
                    return @"C:\Filegroup";
                });

            source.GetImageDirectory(12, 23);
            source.GetImageDirectory(12, 24);
            source.GetImageDirectory(11, 23);
            source.GetImageDirectory(12, 23);

            Assert.AreEqual(@"12,23; 12,24; 11,23", string.Join("; ", idsRequestedFromDatabase),
                "Expected GetFilegroupDirectory to be cached and not called again for the same key pair");
        }
        [TestMethod]
        public void Filegroup_CachesFilegroupDirectories_EvenIfWorkgroupHasNoFilegroupDirectory()
        {
            var idsRequestedFromDatabase = new List<string>();
            var source = CreateImageDirectorySource(ImageStorageMode.FILEGROUP, @"C:\Pics",
                (bankId, workgroupId) =>
                {
                    idsRequestedFromDatabase.Add(bankId + "," + workgroupId);
                    return null;
                });

            source.GetImageDirectory(12, 23);
            source.GetImageDirectory(12, 24);
            source.GetImageDirectory(11, 23);
            source.GetImageDirectory(12, 23);

            Assert.AreEqual(@"12,23; 12,24; 11,23", string.Join("; ", idsRequestedFromDatabase),
                "Expected GetFilegroupDirectory to be cached and not called again for the same key pair");
        }
    }
}
