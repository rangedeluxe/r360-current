﻿using System.Data;
using WFS.RecHub.Common.Log;
using WFS.RecHub.DAL.OLFServicesDAL;

namespace WFS.RecHub.Common
{
    /// <summary>
    /// Low-level class that knows how to talk to the database to get a filegroup directory.
    /// </summary>
    public class FilegroupDirectoryDatabaseReader : IFilegroupDirectorySource
    {
        public FilegroupDirectoryDatabaseReader(string siteKey, cEventLog eventLog)
        {
            SiteKey = siteKey;
            EventLog = eventLog;
        }

        private cEventLog EventLog { get; set; }
        private string SiteKey { get; set; }

        public string GetFilegroupDirectory(int bankId, int workgroupId)
        {
            using (var dal = new cOLFServicesDAL(SiteKey))
            {
                DataTable dt;
                if (!dal.GetLockbox(bankId, workgroupId, out dt) || dt.Rows == null || dt.Rows.Count == 0)
                {
                    EventLog.logEvent(
                        string.Format("Error retrieving filegroup. BankID: {0}, WorkgroupID: {1}", bankId, workgroupId),
                        "FILEGROUP", MessageImportance.Essential);
                }
                else if (dt.Rows[0]["filegroup"] != null && dt.Rows[0]["filegroup"] != System.DBNull.Value)
                    return (string)dt.Rows[0]["filegroup"];
            }
            return null;
        }
    }
}