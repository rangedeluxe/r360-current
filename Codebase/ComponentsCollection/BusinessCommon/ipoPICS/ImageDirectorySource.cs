﻿using WFS.RecHub.Common.Log;

namespace WFS.RecHub.Common
{
    public class ImageDirectorySource
    {
        public ImageDirectorySource(ImageStorageMode imageStorageMode, string picsImagePath,
            IFilegroupDirectorySource filegroupDirectorySource, cEventLog eventLog)
        {
            ImageStorageMode = imageStorageMode;
            PicsImagePath = picsImagePath;
            FilegroupDirectorySource = new FilegroupDirectoryMemoizer(filegroupDirectorySource);
            EventLog = eventLog;
        }

        private cEventLog EventLog { get; set; }
        private IFilegroupDirectorySource FilegroupDirectorySource { get; set; }
        private ImageStorageMode ImageStorageMode { get; set; }
        private string PicsImagePath { get; set; }

        public string GetImageDirectory(int bankId, int workgroupId)
        {
            if (ImageStorageMode == ImageStorageMode.FILEGROUP)
            {
                var filegroupDirectory = FilegroupDirectorySource.GetFilegroupDirectory(bankId, workgroupId);
                if (!string.IsNullOrEmpty(filegroupDirectory))
                {
                    EventLog.logEvent(string.Format("Filegroup is '{0}'.", filegroupDirectory),
                        "FILEGROUP", MessageImportance.Essential);
                    return filegroupDirectory;
                }
            }

            return PicsImagePath;
        }
    }
}