using System;
using System.IO;

/*******************************************************************************
*    Module: InegraPAY Online Services PICS object.
*    Filename: cPICS.cs
*    Author: Joel Caples
*    Purpose: Defines images paths, Checks relative path, and puts batch images.
*
*
* Revisions:
* ----------------------
* CR 6661 JMC 01/20/2004
*   -Created class
* CR 6661 JMC 03/03/2004
*   -Changed Datatype of OnlineColorMode variables to use a byte datatype.
* CR 7074 JMC 03/22/2004
*   -Made Paging constants publicly available.
* CR 7728 JMC 06/01/2004
*   -Added "RemoteImageRef" structure to contain image data.
*   -Added 2 versions of new "getRelativeImagePath" function to return an image's 
*    path realative from the image store root directory.
*   -Added "getImagePath" function to replace "getFileName" function.  Did this
*    for consistancy with new "getRelativeImagePath".
*   -Added additional comments to function headers.
* CR 10045 JMC 10/05/2004
*   -Modified getRelativeImagePath function to return an image looking first
*    for the proper OnlineColorMode, and then searching for other types if the
*    first attempt could not be found.  
*   -Images are located in the following order:
*      Desired Color:            Color, Grayscale, BW 
*      Desired Grayscale:        Grayscale, Color, BW 
*      Desired BW:               BW, Grayscale, Color 
*  CR 32576 WJS 2/11/2011
*   -Added new function called GetFilesForBatch. This function will return a hashtable
*    of batches for a particular directory. It will stored the batchsequence as the key and the filenames
*    as the value. Each filename associated will be seperated by semicolons
* CR 32562 JMC 03/07/2011 
*   -Modified getImagePath() method to accept IItem interface instead of cImage object.
*   -Modified getRelativeImagePath() method to accept IItem interface instead of cImage object.
* CR 32188 JMC 03/30/2010
*   -Modified getImagePath() to accept OnlineColorMode parameter to eliminate
*    the call to the database.
* CR 33230 JMC 01/12/2012
*	-Added PutBatchImages() function.
* CR 50214 WJS 2/13/2012
*   - Added  root path to PutAllImages()
*   -Added more errorChecking to PutAllImages()
*   -Change putbatchImage to a single image
* CR 52959 JNE 09/28/2012
*   -Moved PICS.cs to _PICS.cs base class, broke out the path specific code to the child classes. 
* WI 90251 CRG 03/18/2013 
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoPICS
* WI 151405 BLR 07/03/2014
*   Modified the relative pathing to include the BatchSource ShortName. 
* WI 152230 BLR 07/07/2014
*   Filtered the payment short name appending.
** WI 156527 SAS 07/31/2014
*   Added a filedescriptor while fetching Batch Source Short Name.
* WI 166717 SAS 09/19/2014
*   Change done for reteriving image path. 
* * WI 167457 SAS 09/23/2014
*   Change done for retrieving image relative path.  
* WI 175197 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName   
* WI 176353 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName  
* WI 187546 TWE 02/05/2015
*    Add BatchDelete call to OLFservices
*******************************************************************************/
namespace WFS.RecHub.Common
{

    /// <summary>
    /// ipo PICS Object.
    /// </summary>
    public class cPICS : _PICS
    {

        /// <summary>
        /// Public Constructor.  Sets the SiteKey for the class.
        /// </summary>
        public cPICS(string siteKey)
            : base(siteKey)
        {
            this.siteKey = siteKey;
        }

        /// <summary>
        /// Determines the location and filename of the requested image, and 
        /// returns the path to the image only if the image actually exists.
        /// </summary>
        /// <param name="image">Image object to be located.</param>
        /// <param name="page">Page (0=Front, 1=Back)</param>
        /// <param name="imageRootPath">
        /// Image Root directory.  This is required to ensure
        /// that the image exists before returning a value.
        /// </param>
        /// <returns>Path of image relative to the Image root path</returns>
        public override string getRelativeImagePath(byte OnlineColorMode
                                         , IItem image
                                         , int page
                                         , string imageRootPath)
        {

            return (getRelativeImagePath(OnlineColorMode,
                                        image.PICSDate,
                                        image.BankID,
                                        image.LockboxID,
                                        image.BatchID,
                                        image.BatchSequence,
                                        image.FileDescriptor,
                                        page,
                                        imageRootPath,
                                        image.SourceBatchID,
                                        image.BatchSourceShortName,
                                        image.ImportTypeShortName));
        }

        /// <summary>
        /// WI 156527 SAS 07/31/2014- Added a file descriptor while fetching Batch Source Short Name.
        /// </summary>
        /// <param name="OnlineColorMode"></param>
        /// <param name="PICSDate"></param>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="BatchID"></param>
        /// <param name="BatchSequence"></param>
        /// <param name="FileDescriptor"></param>
        /// <param name="page"></param>
        /// <param name="imageRootPath"></param>
        /// <param name="sourceBatchID"></param>
        /// <param name="BatchSourceShortName"></param>
        /// <param name="ImportTypeShortName"></param>
        /// <returns></returns>
        public override string getRelativeImagePath(byte OnlineColorMode,
                                           string PICSDate,
                                           int BankID,
                                           int LockboxID,
                                           long BatchID,
                                           int BatchSequence,
                                           string FileDescriptor,
                                           int page,
                                           string imageRootPath,
                                           long sourceBatchID,
                                           string BatchSourceShortName,
                                           string ImportTypeShortName)
        {

            string strImagePath = string.Empty;
            string strRetVal = string.Empty;

            try
            {
                // Construct path for image
                if (pathNeedsPaymentSource(ImportTypeShortName))
                    strImagePath = Path.Combine(strImagePath, BatchSourceShortName);
                strImagePath = Path.Combine(strImagePath, PICSDate);
                strImagePath = Path.Combine(strImagePath, BankID.ToString());
                strImagePath = Path.Combine(strImagePath, LockboxID.ToString());
                strImagePath = Path.Combine(strImagePath, sourceBatchID + "_"
                                                                + BatchSequence.ToString() + "_"
                                                                + FileDescriptor + "_");

                eventLog.logEvent("GetRelativeImagePath: " + strImagePath, "CPICS", MessageImportance.Debug);
                strRetVal = SetImageTypeInPath(OnlineColorMode, page, imageRootPath, strImagePath);
                eventLog.logEvent("GetRelativeImagePath (After SetImageTypeInPath): " + strImagePath, "CPICS", MessageImportance.Debug);
            }
            catch (Exception e)
            {

                strRetVal = "";
                eventLog.logError(e);
            }
            return (strRetVal);
        }

        public override bool PutBatchImage(string imageRootPath, ILTAImageInfo ImageInfo, byte[] BatchImages)
        {

            bool bolRetVal;
            string strFilePath;

            try
            {

                if (ImageInfo == null)
                {
                    throw new OnlineFormattedException("Image Info cannot be null.");
                }

                if (BatchImages == null)
                {
                    throw new OnlineFormattedException("Batch Images cannot be null.");
                }


                if (String.IsNullOrEmpty(imageRootPath))
                {
                    throw new OnlineFormattedException("Image root path cannot be null.");
                }



                if (BatchImages.Length > 0)
                {



                    if (String.IsNullOrEmpty(ImageInfo.ProcessingDateKey.ToString()))
                    {
                        throw new OnlineFormattedException("Processing Date  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(ImageInfo.BankID.ToString()))
                    {
                        throw new OnlineFormattedException("BankID  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(ImageInfo.LockboxID.ToString()))
                    {
                        throw new OnlineFormattedException("LockboxID  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(ImageInfo.BatchID.ToString()))
                    {
                        throw new OnlineFormattedException("BatchID  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(ImageInfo.BatchSequence.ToString()))
                    {
                        throw new OnlineFormattedException("BatchSequence  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(ImageInfo.FileDescriptor) || String.IsNullOrEmpty(ImageInfo.FileDescriptor.ToString()))
                    {
                        throw new OnlineFormattedException("FileDescriptor  cannot be null.");
                    }

                    eventLog.logEvent("Image Info: " + imageRootPath + " ProcDate:" + ImageInfo.ProcessingDateKey.ToString(),
                                MessageImportance.Debug);
                    eventLog.logEvent("BankID: " + ImageInfo.BankID.ToString() + " LockboxID:" + ImageInfo.LockboxID.ToString(),
                                MessageImportance.Debug);
                    eventLog.logEvent("BatchID: " + ImageInfo.BatchID.ToString() + " BatchSequence:" + ImageInfo.BatchSequence.ToString(),
                                MessageImportance.Debug);
                    eventLog.logEvent("FileDescriptor: " + ImageInfo.FileDescriptor, MessageImportance.Debug);


                    strFilePath = string.Empty;
                    strFilePath = Path.Combine(strFilePath + imageRootPath);

                    // Construct path for image
                    if (pathNeedsPaymentSource(ImageInfo.ImportTypeShortName))
                        strFilePath = Path.Combine(strFilePath, ImageInfo.BatchSourceShortName);

                    strFilePath = Path.Combine(strFilePath, ImageInfo.ProcessingDateKey.ToString());
                    strFilePath = Path.Combine(strFilePath, ImageInfo.BankID.ToString());
                    strFilePath = Path.Combine(strFilePath, ImageInfo.LockboxID.ToString());
                    strFilePath = Path.Combine(strFilePath, ImageInfo.SourceBatchID.ToString() + "_"
                                                                    + ImageInfo.BatchSequence.ToString() + "_"
                                                                    + ImageInfo.FileDescriptor.Trim().ToUpper() + "_"
                                                                    + (ImageInfo.IsBack ? "B" : "F"));

                    switch (ImageInfo.ColorMode)
                    {
                        case (short)WorkgroupColorMode.COLOR_MODE_COLOR:
                            strFilePath += "C";
                            break;
                        case (short)WorkgroupColorMode.COLOR_MODE_GRAYSCALE:
                            strFilePath += "G";
                            break;
                        default:
                            break;
                    }
                    strFilePath += ".TIF";

                    eventLog.logEvent("Got full PICS path:" + strFilePath, MessageImportance.Debug);
                    Directory.CreateDirectory(Path.GetDirectoryName(strFilePath));
                    File.WriteAllBytes(strFilePath, BatchImages);
                }

                bolRetVal = true;
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ////EventLog.logError(ex, this.GetType().Name, "SendBatchImages");
                eventLog.logError(ex, this.GetType().Name, "PutBatchImages");
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        /// <summary>
        /// Construct the path to the images for a batch
        /// </summary>
        /// <param name="ImportTypeShortName"></param>
        /// <param name="BatchSourceShortName"></param>
        /// <param name="strImageRootPath"></param>
        /// <param name="strProcessingDate"></param>
        /// <param name="iBankID"></param>
        /// <param name="iLockboxID"></param>
        /// <returns></returns>
        public string GetImagePath(string importTypeShortName,
                                   string batchPaymentSourceName, 
                                   string strImageRootPath, 
                                   string strProcessingDate, 
                                   int iBankID, 
                                   int iLockboxID)
        {
            string strImagePath = strImageRootPath;
            // Modify the root path, if needed
            if (pathNeedsPaymentSource(importTypeShortName))
                strImagePath = Path.Combine(strImagePath, batchPaymentSourceName);
            return (CombineImagePath(strImagePath, strProcessingDate, iBankID, iLockboxID));
        }

        protected override string CombineImagePath(string strImageRootPath, string strProcessingDate, int iBankID, int iLockboxID)
        {
            string strImagePath = string.Empty;
            strImagePath = Path.Combine(strImageRootPath, strProcessingDate);
            strImagePath = Path.Combine(strImagePath, iBankID.ToString());
            strImagePath = Path.Combine(strImagePath, iLockboxID.ToString());
            return strImagePath;
        }
    }
}
