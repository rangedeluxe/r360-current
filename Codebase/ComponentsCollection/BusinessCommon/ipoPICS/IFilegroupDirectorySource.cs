﻿namespace WFS.RecHub.Common
{
    public interface IFilegroupDirectorySource
    {
        string GetFilegroupDirectory(int bankId, int workgroupId);
    }
}