using System;
using WFS.RecHub.DAL;
using WFS.RecHub.DAL.OLFServicesDAL;
using WFS.RecHub.Common.Log;

/******************************************************************************
** Wausau
** Copyright � 1997-2008 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     01/20/2004
*
* Purpose:  InegraPAY Online Services PICS object root class..
*
* Modification History
* 01/20/2004 JMC
*     - Created class
* 06/01/2004 CR 7728 JMC
*     - Removed unused "serverKey" property.
*     - Added additional comments to function headers.
*     - Changed the modified for the siteOptions property from internal to protected.
*     - Changed the modified for the database property from internal to protected.
* 11/02/2005 CR 14209 JMC
*     - Added code to assign SetDeadlockPriority property to database object
*       after creation.
* 11/04/2005 CR 14211 JMC
*     - Added code to assign QueryRetryAttempts property to database object
*       after creation.
* WI 90251 CRG 03/18/2013 
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoPICS
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// ipo Image Service Object Root class.
    /// </summary>
	public abstract class _DMPObjectRoot : IDisposable
    {
        // Track whether Dispose has been called.
        private bool disposed = false;

        private cSiteOptions _SiteOptions = null;
        protected cOLFServicesDAL _OLFServiceDAL = null;
        private cEventLog _EventLog =null;
        private string _SiteKey;

        /// <summary>
        /// Determines the section of the local .ini file to be used for local 
        /// options.
        /// </summary>
        public string siteKey
        {
            set { _SiteKey = value; }
            get { return _SiteKey; }
        }


        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        protected cSiteOptions siteOptions
        {
            get
            {
                if (_SiteOptions == null)
                    { _SiteOptions = new cSiteOptions(this.siteKey); }

                return _SiteOptions;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog eventLog
        {
            get
            {
                if (_EventLog == null) { 
                    _EventLog = new cEventLog(siteOptions.logFilePath
                                                , siteOptions.logFileMaxSize
                                                , (MessageImportance)siteOptions.loggingDepth); 
                }

                return _EventLog;
            }
        }

        protected cOLFServicesDAL OLFServiceDAL {
            get {
                if(_OLFServiceDAL == null) {
                    _OLFServiceDAL = new cOLFServicesDAL(this.siteKey);
                }
                return(_OLFServiceDAL);
            }
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output messages.
        /// </summary>
        /// <param name="message">Test message to be logged.</param>
        /// <param name="src">Source of the event.</param>
        /// <param name="messageType">MessageType(Information, Warning, Error)</param>
        /// <param name="messageImportance">MessageImportance(Essential, Verbose, Debug)
        /// </param>
        protected void object_outputMessage(string message
                                          , string src
                                          , MessageType messageType
                                          , MessageImportance messageImportance)
        {
            eventLog.logEvent(message
                            , src
                            , messageType
                            , messageImportance);
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output errors.
        /// </summary>
        /// <param name="e">Exception to be logged.</param>
        protected void object_outputError(Exception e)
        {
            eventLog.logError(e);
        }

        #region Dispose

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_OLFServiceDAL != null)
                    {
                        _OLFServiceDAL.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;
            }
        }
        #endregion
    }
}
