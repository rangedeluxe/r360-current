using System;
using System.Collections;
using System.Data;
using System.IO;
using WFS.RecHub.DAL.OLFServicesDAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Jason Efken  
* Date: 09/28/2012
*
* Purpose: _PICS base class.  Defines imagetype,colors, etc...
*
* Modification History
* CR 52959
* Moved PICS.cs to _PICS.cs base class
* WI 90251 CRG 03/18/2013 
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoPICS
* WI 151405 BLR 07/02/2014
*   Added a new stored procedure to gather Batch Source Short Name.
* WI 152230 BLR 07/07/2014
*   Added a function to filer out payment short names. 
* WI 156527 SAS 07/31/2014
*   Added a filedescriptor while fetching Batch Source Short Name.
* * WI 167457 SAS 09/23/2014
*   Change done for retrieving image relative path.  
* WI 175197 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName
* WI 176353 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName  
******************************************************************************/

namespace WFS.RecHub.Common {

    /// <summary>
    /// ipo _PICS Object.
    /// </summary>
	public abstract class _PICS : _DMPObjectRoot {

        protected const string PAYMENT_SOURCE_INTEGRA_PAY = "integraPAY";

        protected const byte COLOR_MODE_BLACK_AND_WHITE = 1;
        protected const byte COLOR_MODE_COLOR = 2;
        protected const byte COLOR_MODE_GRAYSCALE = 4;

        protected System.Collections.SortedList _WorkgroupColorModes = null;
            
        /// <summary></summary>
        public const string IMG_ABRV_BLACK_AND_WHITE_FRONT = "F";

        /// <summary></summary>
        public const string IMG_ABRV_BLACK_AND_WHITE_BACK = "B";

        /// <summary></summary>
        public const string IMG_ABRV_COLOR_FRONT = "FC";

        /// <summary></summary>
        public const string IMG_ABRV_COLOR_BACK = "BC";

        /// <summary></summary>
        public const string IMG_ABRV_GRAYSCALE_FRONT = "FG";

        /// <summary></summary>
        public const string IMG_ABRV_GRAYSCALE_BACK = "BG";

        /// <summary>
        /// Constant indicating the image is a picture of the item's front.
        /// </summary>
        public const int PAGE_FRONT=0;

        /// <summary>
        /// Constant indicating the image is a picture of the item's back.
        /// </summary>
        public const int PAGE_BACK=1;

        /// <summary>
        /// File Descriptor for all checks.
        /// </summary>
        public const string FILE_DESCRIPTOR_CHECK = "C";

        /// <summary>
        /// File Descriptor for documents that are Scanned Checks.
        /// </summary>
        public const string FILE_DESCRIPTOR_SCANNED_CHECK = "SC";

        /// <summary>
        /// File Descriptor for documents that are type Unknown.
        /// </summary>
        public const string FILE_DESCRIPTOR_UNKNOWN = "UN";

        /// <summary>
        /// 
        /// </summary>
        public struct RemoteImageRef {
            /// <summary>
            /// 
            /// </summary>
            public long Size;

            /// <summary>
            /// 
            /// </summary>
            public string FilePath;
        }

        /// <summary>
        /// Public Constructor.  Sets the SiteKey for the class.
        /// </summary>
        public _PICS(string siteKey) {
            this.siteKey=siteKey;
        }

        // Abstract methods
        public abstract bool PutBatchImage(string imageRootPath, ILTAImageInfo imageInfo, byte[] batchImages);
        public abstract string getRelativeImagePath(byte onlineColorMode, IItem image, int page, string imageRootPath);
        protected abstract string CombineImagePath(string imageRootPath, string processingDate, int bankID, int lockboxID); 
        public abstract string getRelativeImagePath(byte onlineColorMode,
                                           string picsDate,
                                           int bankID,
                                           int lockboxID,
                                           long batchID,
                                           int batchSequence,
                                           string fileDescriptor,
                                           int page,
                                           string imageRootPath,
                                           long sourceBatchID,
                                           string BatchSourceShortName,
                                           string ImportTypeShortName);
        
        ///// <summary>
        ///// WI 151519 BLR 07/02/2014 - Added a new stored procedure to gather Batch Source Short Name.
        ///// WI 156527 SAS 07/31/2014-Added a file descriptor while fetching Batch Source Short Name.
        ///// </summary>
        ///// <param name="BatchID"></param>
        ///// <param name="BatchSequence"></param>
        ///// <param name="batchSourceShortName"></param>
        ///// <param name="sourceBatchID"></param>
        ///// <returns></returns>
        //protected bool getBatchSourceInformation(long BatchID, int BatchSequence,string FileDescriptor, 
        //    out string batchSourceShortName, out long sourceBatchID)
        //{
        //    bool output = false;
        //    DataTable batchtable;

        //    output = ItemProcDAL.GetCheckImagePathInfo(BatchID, BatchSequence,FileDescriptor.ToUpper(), out batchtable);
        //    if (output && batchtable.Rows.Count > 0)
        //    {
        //        var dr = batchtable.Rows[0];
        //        try
        //        {
        //            batchSourceShortName = ipoLib.NVL(ref dr, "ShortName", string.Empty);
        //            sourceBatchID = ipoLib.NVL(ref dr, "SourceBatchID", default(long));
        //        }
        //        catch (InvalidCastException)
        //        {
        //            batchSourceShortName = string.Empty;
        //            sourceBatchID = default(int);
        //        }

        //        batchtable.Dispose();
        //    }
        //    else
        //    {
        //        if (batchtable != null)
        //            batchtable.Dispose();
        //        throw new Exception(string.Format("Error in getBatchSourceInformation, could not call the DAL for Input Batch:{0}, BatchSequence:{1}",
        //            BatchID, BatchSequence));
        //    }

        //    return output;
        //}

        /// <summary>
        /// WI 152230 : Used to determine if we need to write out the payment
        /// source short name to the relative path.
        /// </summary>
        /// <param name="shortname"></param>
        /// <returns></returns>
        protected bool pathNeedsPaymentSource(string shortname)
        {
            return !shortname.Equals(PAYMENT_SOURCE_INTEGRA_PAY);
        }

        /// <summary>
        /// Returns the fully qualified path of the image, including 
        /// the root drive information.
        /// </summary>
        /// <param name="image">Image object to be located.</param>
        /// <param name="page">Page (0=Front, 1=Back)</param>
        /// <param name="imageRootPath">Image Root directory.</param>
        /// <returns></returns>
        public string getImagePath(byte OnlineColorMode, IItem image, int page, string imageRootPath) {

            return(Path.Combine(imageRootPath, getRelativeImagePath(OnlineColorMode, image, page, imageRootPath)));

        } 

        /// <summary>
        /// Determines the page part of an image's expected name.
        /// </summary>
        /// <param name="colorMode">B+W, Greyscale, or Color.</param>
        /// <param name="page">0=Front, 1=Back</param>
        /// <returns>The page part of an image's expected name.</returns>
        protected string getImageType(int colorMode, int page) {

            string strRetVal="";
               
            switch(colorMode) {
                case COLOR_MODE_BLACK_AND_WHITE:
                    switch (page) {
                        case 0:
                            strRetVal = IMG_ABRV_BLACK_AND_WHITE_FRONT;
                            break;
                        case 1:
                            strRetVal = IMG_ABRV_BLACK_AND_WHITE_BACK;
                            break;
                    }
                    break;
                case COLOR_MODE_COLOR:
                    switch (page) {
                        case 0:
                            strRetVal = IMG_ABRV_COLOR_FRONT;
                            break;
                        case 1:
                            strRetVal = IMG_ABRV_COLOR_BACK;
                            break;
                    }
                    break;
                case COLOR_MODE_GRAYSCALE:
                    switch (page) {
                        case 0:
                            strRetVal = IMG_ABRV_GRAYSCALE_FRONT;
                            break;
                        case 1:
                            strRetVal = IMG_ABRV_GRAYSCALE_BACK;
                            break;
                    }
                    break;
            }
            
            return(strRetVal);

        }

		/// <summary>
		/// Retrieves the default color mode (B+W, Greyscale, or Color) for the
		/// Lockbox from the database.
		/// </summary>
		/// <param name="bankID">BankID is part of the key on the Lockbox table.</param>
		/// <param name="lockboxID">LockboxID to be selected.</param>
		/// <returns>
		/// Default color mode (B+W, Greyscale, or Color) for the Lockbox.
		/// </returns>
		/// <exception cref="System.Exception">Specified Lockbox does not exist; Lockbox :  
		///                                     + lockboxID.ToString()</exception>
        protected byte getOnlineColorMode(int bankID, int lockboxID) {

            DataTable dt;
            byte bytRetVal=0;
            bool bolRetVal;
            string strCollectionKey=bankID.ToString() + "~" + lockboxID.ToString();

            if(_WorkgroupColorModes == null) {
                _WorkgroupColorModes = new SortedList();
            }

            if(_WorkgroupColorModes.ContainsKey(strCollectionKey)) {
                bytRetVal=(byte)_WorkgroupColorModes[strCollectionKey];
            } else {


                bolRetVal = OLFServiceDAL.GetLockboxOnlineColorMode(bankID, lockboxID, out dt);

                if (bolRetVal && dt.Rows.Count > 0) {
                    System.Data.DataRow dr = dt.Rows[0];

                    try {
                        bytRetVal=ipoLib.NVL(ref dr, "OnlineColorMode", COLOR_MODE_BLACK_AND_WHITE);
                    } catch(InvalidCastException) {
                        bytRetVal=(Byte)ipoLib.NVL(ref dr, "OnlineColorMode", (int)COLOR_MODE_BLACK_AND_WHITE);
                    }

                    dt.Dispose();

                    _WorkgroupColorModes.Add(strCollectionKey, bytRetVal);
                }
                else {
                    dt.Dispose();
                    bytRetVal=COLOR_MODE_BLACK_AND_WHITE;
                    throw(new Exception("Specified Lockbox does not exist; Lockbox : " 
                                    + lockboxID.ToString())); 
                }
            }

            return(bytRetVal);
        }

        public string SetImageTypeInPath(byte onlineColorMode, int page, string imageRootPath,string imagePath) {

            string strRetVal = string.Empty;
            string strImageType = string.Empty;

            switch(onlineColorMode) {
                case COLOR_MODE_BLACK_AND_WHITE:
                    strImageType = getImageType(COLOR_MODE_BLACK_AND_WHITE, page);
                    var fullpath = Path.Combine(imageRootPath, imagePath + strImageType + ".tif");
                    eventLog.logEvent("SetImageTypeInPath: Checking Path: " + fullpath, "CPICS", MessageImportance.Debug);
                    if (File.Exists(fullpath)) 
                    {
                        strRetVal = imagePath + strImageType + ".tif";
                    } 
                    else 
                    {
                        strImageType = getImageType(COLOR_MODE_GRAYSCALE, page);
                        if (File.Exists(Path.Combine(imageRootPath, imagePath + strImageType + ".tif"))) 
                        {
                            strRetVal = imagePath + strImageType + ".tif";
                        } 
                        else 
                        {
                            strImageType = getImageType(COLOR_MODE_COLOR, page);
                            if (File.Exists(Path.Combine(imageRootPath, imagePath + strImageType + ".tif"))) 
                            {
                                strRetVal = imagePath + strImageType + ".tif";
                            } 
                            else 
                            {
                                strRetVal = "";
                            }
                        }
                    }
                    break;
                case COLOR_MODE_COLOR:
                    strImageType = getImageType(COLOR_MODE_COLOR, page);
                    if (File.Exists(Path.Combine(imageRootPath, imagePath + strImageType + ".tif"))) {
                        strRetVal = imagePath + strImageType + ".tif";
                    } else {
                        strImageType = getImageType(COLOR_MODE_GRAYSCALE, page);
                        if (File.Exists(Path.Combine(imageRootPath, imagePath + strImageType + ".tif"))) {
                            strRetVal = imagePath + strImageType + ".tif";
                        } else {
                            strImageType = getImageType(COLOR_MODE_BLACK_AND_WHITE, page);
                            if (File.Exists(Path.Combine(imageRootPath, imagePath + strImageType + ".tif"))) {
                                strRetVal = imagePath + strImageType + ".tif";
                            } else {
                                strRetVal = "";
                            }
                        }
                    }
                    break;
                case COLOR_MODE_GRAYSCALE:
                    strImageType = getImageType(COLOR_MODE_GRAYSCALE, page);
                    if (File.Exists(Path.Combine(imageRootPath, imagePath + strImageType + ".tif"))) {
                        strRetVal = imagePath + strImageType + ".tif";
                    } else {
                        strImageType = getImageType(COLOR_MODE_COLOR, page);
                        if (File.Exists(Path.Combine(imageRootPath, imagePath + strImageType + ".tif"))) {
                            strRetVal = imagePath + strImageType + ".tif";
                        } else {
                            strImageType = getImageType(COLOR_MODE_BLACK_AND_WHITE, page);
                            if (File.Exists(Path.Combine(imageRootPath, imagePath + strImageType + ".tif"))) {
                                strRetVal = imagePath + strImageType + ".tif";
                            } else {
                                strRetVal = "";
                            }
                        }
                    }
                    break;
                default:
                    strRetVal="";
                    break;
            }
            return strRetVal;
        }

        public Hashtable GetFilesForBatch(string imageRootPath, string processingDate, int bankId, int lockboxId, int batchId){

            Hashtable picsFileTable = new Hashtable();
            try
            {
                string strImagePath = string.Empty;
                strImagePath = CombineImagePath(imageRootPath, processingDate, bankId, lockboxId);
                string searchPath;
                searchPath = batchId.ToString() + "_*.tif";
                string[] fileList = Directory.GetFiles(strImagePath, searchPath);

                for (int j = 0; j < fileList.Length; j++)
                {
                    //get the batch sequence
                    string fullPath = fileList[j];
                    string fileName = Path.GetFileName(fullPath);
                    string[] fileNameParts = fileName.Split('_');
                    if (fileNameParts.Length > 1)
                    {
                        string lookupStr = string.Empty;
                        bool bIsCheck = false;
                        int batchSequence;
                        Int32.TryParse(fileNameParts[1], out batchSequence);
                        bIsCheck = fileNameParts[2] == "C" ? true : false;
                        lookupStr = batchSequence.ToString() + bIsCheck.ToString();
                        //insert in hashTable or add it too hashTable
                        if (picsFileTable.ContainsKey(lookupStr))
                        {
                            string currentValue = picsFileTable[lookupStr].ToString();
                            currentValue = currentValue + ";" + fullPath;
                            picsFileTable[lookupStr] = currentValue;
                        }
                        else
                        {
                            picsFileTable.Add(lookupStr, fullPath);
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                eventLog.logError(ex);
            }
            return picsFileTable;
        }

    }
}