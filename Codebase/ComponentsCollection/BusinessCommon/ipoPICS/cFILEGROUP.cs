using System;
using System.IO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Jason Efken  
* Date: 09/28/2012
*
* Purpose: FILEGROUP specific module based upon _PICS base class.  Defines images paths,
*          Checks relative path, and puts batch images. 
*
* Modification History
* WI 90251 CRG 03/18/2013 
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoPICS
* WI 151405 BLR 07/03/2014
*   Modified the relative pathing to include the BatchSource ShortName. 
* WI 152230 BLR 07/07/2014
*   Filtered the payment short name appending.
* WI 156527 SAS 07/31/2014
*   Added a filedescriptor while fetching Batch Source Short Name.
* WI 166717 SAS 09/19/2014
*   Change done for retrieving image path.
* WI 167457 SAS 09/23/2014
*   Change done for retrieving image relative path.
* WI 175197 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName   
* WI 176353 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName     
******************************************************************************/

namespace WFS.RecHub.Common
{

    /// <summary>
    /// ipo FILEGROUP Object.
    /// </summary>
    public class cFILEGROUP : _PICS
    {

        /// <summary>
        /// Public Constructor.  Sets the SiteKey for the class.
        /// </summary>
        public cFILEGROUP(string siteKey)
            : base(siteKey)
        {
            this.siteKey = siteKey;
        }

        /// <summary>
        /// Determines the location and filename of the requested image, and 
        /// returns the path to the image only if the image actually exists.
        /// </summary>
        /// <param name="image">Image object to be located.</param>
        /// <param name="page">Page (0=Front, 1=Back)</param>
        /// <param name="imageRootPath">
        /// Image Root directory.  This is required to ensure
        /// that the image exists before returning a value.
        /// </param>
        /// <returns>Path of image relative to the Image root path</returns>
        public override string getRelativeImagePath(byte OnlineColorMode
                                         , IItem image
                                         , int page
                                         , string imageRootPath)
        {

            return (getRelativeImagePath(OnlineColorMode,
                                        image.PICSDate,
                                        image.BankID,
                                        image.LockboxID,
                                        image.BatchID,
                                        image.BatchSequence,
                                        image.FileDescriptor,
                                        page,
                                        imageRootPath,
                                        image.SourceBatchID,
                                        image.BatchSourceShortName,
                                        image.ImportTypeShortName));
        }

        /// <summary>
        /// * WI 156527 SAS 07/31/2014 Added a file descriptor while fetching Batch Source Short Name.
        /// </summary>
        /// <param name="onlineColorMode"></param>
        /// <param name="picsDate"></param>
        /// <param name="bankID"></param>
        /// <param name="lockboxID"></param>
        /// <param name="batchID"></param>
        /// <param name="batchSequence"></param>
        /// <param name="fileDescriptor"></param>
        /// <param name="page"></param>
        /// <param name="imageRootPath"></param>
        /// <param name="sourceBatchID"></param>
        /// <param name="BatchSourceShortName"></param>
        /// <param name="ImportTypeShortName"></param>
        /// <returns></returns>
        public override string getRelativeImagePath(byte onlineColorMode,
                                           string picsDate,
                                           int bankID,
                                           int lockboxID,
                                           long batchID,
                                           int batchSequence,
                                           string fileDescriptor,
                                           int page,
                                           string imageRootPath,
                                           long sourceBatchID,
                                           string BatchSourceShortName,
                                           string ImportTypeShortName)
        {

            string strImagePath = string.Empty;
            string strRetVal = string.Empty;
            try
            {
                // Construct path for image
                if (pathNeedsPaymentSource(ImportTypeShortName))
                    strImagePath = Path.Combine(strImagePath, BatchSourceShortName);

                strImagePath = Path.Combine(strImagePath, bankID.ToString());
                strImagePath = Path.Combine(strImagePath, lockboxID.ToString());
                strImagePath = Path.Combine(strImagePath, picsDate);
                strImagePath = Path.Combine(strImagePath, sourceBatchID.ToString() + "_"
                                                                + batchSequence.ToString() + "_"
                                                                + fileDescriptor + "_");

                strRetVal = SetImageTypeInPath(onlineColorMode, page, imageRootPath, strImagePath);

            }
            catch (Exception e)
            {

                strRetVal = "";
                eventLog.logError(e);
            }
            return (strRetVal);
        }

        public override bool PutBatchImage(string imageRootPath, ILTAImageInfo imageInfo, byte[] batchImages)
        {

            bool bolRetVal;
            string strFilePath = string.Empty;

            try
            {
                if (imageInfo == null)
                {
                    throw new OnlineFormattedException("Image Info cannot be null.");
                }
                if (batchImages == null)
                {
                    throw new OnlineFormattedException("Batch Images  cannot be null.");
                }
                if (String.IsNullOrEmpty(imageRootPath))
                {
                    throw new OnlineFormattedException("Image root path cannot be null.");
                }
                if (batchImages.Length > 0)
                {
                    if (String.IsNullOrEmpty(imageInfo.ProcessingDateKey.ToString()))
                    {
                        throw new OnlineFormattedException("Processing Date  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(imageInfo.BankID.ToString()))
                    {
                        throw new OnlineFormattedException("BankID  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(imageInfo.LockboxID.ToString()))
                    {
                        throw new OnlineFormattedException("LockboxID  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(imageInfo.BatchID.ToString()))
                    {
                        throw new OnlineFormattedException("BatchID  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(imageInfo.BatchSequence.ToString()))
                    {
                        throw new OnlineFormattedException("BatchSequence  cannot be null.");
                    }
                    if (String.IsNullOrEmpty(imageInfo.FileDescriptor) || String.IsNullOrEmpty(imageInfo.FileDescriptor.ToString()))
                    {
                        throw new OnlineFormattedException("FileDescriptor  cannot be null.");
                    }

                    eventLog.logEvent("Image Info: " + imageRootPath + " ProcDate:" + imageInfo.ProcessingDateKey.ToString(),
                                MessageImportance.Debug);
                    eventLog.logEvent("BankID: " + imageInfo.BankID.ToString() + " LockboxID:" + imageInfo.LockboxID.ToString(),
                                MessageImportance.Debug);
                    eventLog.logEvent("BatchID: " + imageInfo.BatchID.ToString() + " BatchSequence:" + imageInfo.BatchSequence.ToString(),
                                MessageImportance.Debug);
                    eventLog.logEvent("FileDescriptor: " + imageInfo.FileDescriptor, MessageImportance.Debug);

                    strFilePath = string.Empty;
                    strFilePath = Path.Combine(strFilePath + imageRootPath);

                    // Construct path for image
                    if (pathNeedsPaymentSource(imageInfo.ImportTypeShortName))
                        strFilePath = Path.Combine(strFilePath, imageInfo.BatchSourceShortName);

                    strFilePath = Path.Combine(strFilePath, imageInfo.BankID.ToString());
                    strFilePath = Path.Combine(strFilePath, imageInfo.LockboxID.ToString());
                    strFilePath = Path.Combine(strFilePath, imageInfo.ProcessingDateKey.ToString());
                    strFilePath = Path.Combine(strFilePath, imageInfo.SourceBatchID.ToString() + "_"
                                                                    + imageInfo.BatchSequence.ToString() + "_"
                                                                    + imageInfo.FileDescriptor.Trim().ToUpper() + "_"
                                                                    + (imageInfo.IsBack ? "B" : "F"));
                    switch (imageInfo.ColorMode)
                    {
                        case (short)WorkgroupColorMode.COLOR_MODE_COLOR:
                            strFilePath += "C";
                            break;
                        case (short)WorkgroupColorMode.COLOR_MODE_GRAYSCALE:
                            strFilePath += "G";
                            break;
                        default:
                            break;
                    }
                    strFilePath += ".TIF";

                    eventLog.logEvent("Got full FILEGROUP path:" + strFilePath, MessageImportance.Debug);
                    Directory.CreateDirectory(Path.GetDirectoryName(strFilePath));
                    File.WriteAllBytes(strFilePath, batchImages);
                }

                bolRetVal = true;
            }
            catch (OnlineFormattedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ////EventLog.logError(ex, this.GetType().Name, "SendBatchImages");
                eventLog.logError(ex, this.GetType().Name, "PutBatchImages");
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        public string GetImagePath(string importTypeShortName,
                                   string batchPaymentSourceName,
                                   string strImageRootPath,
                                   string strProcessingDate,
                                   int iBankID,
                                   int iLockboxID)
        {
            string strImagePath = strImageRootPath;
            // Modify the root path, if needed
            if (pathNeedsPaymentSource(importTypeShortName))
                strImagePath = Path.Combine(strImagePath, batchPaymentSourceName);
            return (CombineImagePath(strImagePath, strProcessingDate, iBankID, iLockboxID));
        }

        protected override string CombineImagePath(string imageRootPath, string processingDate, int bankID, int lockboxID)
        {
            string strImagePath = string.Empty;

            strImagePath = Path.Combine(imageRootPath, bankID.ToString());
            strImagePath = Path.Combine(strImagePath, lockboxID.ToString());
            strImagePath = Path.Combine(strImagePath, processingDate);
            return strImagePath;
        }
    }
}
