﻿using System;
using System.Collections.Generic;

namespace WFS.RecHub.Common
{
    public class FilegroupDirectoryMemoizer : IFilegroupDirectorySource
    {
        private readonly Dictionary<Tuple<int, int>, string> _dictionary = new Dictionary<Tuple<int, int>, string>();
        private readonly IFilegroupDirectorySource _inner;

        public FilegroupDirectoryMemoizer(IFilegroupDirectorySource inner)
        {
            _inner = inner;
        }

        public string GetFilegroupDirectory(int bankId, int workgroupId)
        {
            var key = Tuple.Create(bankId, workgroupId);
            if (_dictionary.ContainsKey(key))
                return _dictionary[key];

            var result = _inner.GetFilegroupDirectory(bankId, workgroupId);
            _dictionary[key] = result;
            return result;
        }
    }
}