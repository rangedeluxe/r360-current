﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common;

namespace WFS.RecHub.Common
{
    public class LegalAmount
    {
        private const string InvalidLegalAmountMessage = "Invalid Legal Amount";
        private decimal _originalAmount;
        private decimal _amount;

        public string Text { get; private set; }

        public LegalAmount(decimal amount)
        {
            _originalAmount = amount;
            _amount = Math.Abs(amount);//going to parse on the absolute amount
            Text = TryParse().GetResult(HandleInvalidLegalAmount);
        }

        private Dictionary<string, string> OnesPlaceValues
        {
            get
            {
                return new Dictionary<string, string>
                {
                    { "0", "Zero" },
                    { "1", "One" },
                    { "2", "Two" },
                    { "3", "Three" },
                    { "4", "Four" },
                    { "5", "Five" },
                    { "6", "Six" },
                    { "7", "Seven" },
                    { "8", "Eight" },
                    { "9", "Nine"}
                };
            }
        }

        private Dictionary<string, string> TensPlaceValues
        {
            get
            {
                return new Dictionary<string, string>
                {
                    { "10", "Ten" },
                    { "11", "Eleven" },
                    { "12", "Twelve" },
                    { "13", "Thirteen" },
                    { "14", "Fourteen" },
                    { "15", "Fifteen" },
                    { "16", "Sixteen" },
                    { "17", "Seventeen" },
                    { "18", "Eighteen" },
                    { "19", "Nineteen"},
                    { "2", "Twenty" },
                    { "3", "Thirty" },
                    { "4", "Forty" },
                    { "5", "Fifty" },
                    { "6", "Sixty" },
                    { "7", "Seventy" },
                    { "8", "Eighty" },
                    { "9", "Ninety" }
                };
            }
        }

        private PossibleResult<string> TryParse()
        {
            var decimalAmountAsString = _amount.ToString("N");
            var dollarsAndCents = decimalAmountAsString.Split('.');
            var dollars = dollarsAndCents[0];
            var cents = dollarsAndCents[1];
            var thousandPlaces = dollars.Split(',');

            if (thousandPlaces.Length > 4) //will support billions, not trillians (999,999,999,999.99)
                return TryParse(Result.None<string>(), cents);

            if (thousandPlaces.Length == 1)//process hundreds amount (a 'hundreds amount' will be 1 thousands place like 999)
                return TryParse(TryParseHundredsAmount(thousandPlaces[0]), cents);

            if (thousandPlaces.Length == 2)//process thousands amount (a 'thousands amount' will be 2 thousands places like 999,999)
                return TryParse(TryParseThousandsAmount(thousandPlaces), cents);

            if (thousandPlaces.Length == 3)//process millions (a 'millions amount' will be 3 thousands places like 999,999,999)
                return TryParse(TryParseMillionsAmount(thousandPlaces), cents);

            //process billions (a 'billions amount' will be 4 thousand places like 999,999,999,999)
            return TryParse(TryParseBillionsAmount(thousandPlaces), cents);
        }

        private PossibleResult<string> TryParse(PossibleResult<string> possibleParsedText, string cents)
        {
            var result = possibleParsedText.GetResult(HandleInvalidLegalAmount);

            if (string.Equals(result, InvalidLegalAmountMessage, StringComparison.InvariantCultureIgnoreCase))
                return Result.Real(result);

            result = result.TrimEnd();
            result = result.Length == 0 ? "Zero" : result;

            return Result.Real($"{HandleNegativeNumber()}{result} & {cents}/100");
        }
        
        private string HandleNegativeNumber()
        {
            return _originalAmount < 0 ? "-" : string.Empty;
        }

        private string HandleInvalidLegalAmount()
        {
            return InvalidLegalAmountMessage;
        }

        private PossibleResult<string> TryParseSingleDigitAmount(string amountText)
        {
            var amount = int.Parse(amountText);
            amountText = amount.ToString();

            if (amountText.Length != 1) { return Result.None<string>(); }

            var result = OnesPlaceValues.Where(x => x.Key == amountText).FirstOrDefault();

            if (result.Value == null) { return Result.None<string>(); }

            return Result.Real<string>(result.Value);
        }

        private PossibleResult<string> TryParseTensAmount(string amountText)
        {
            var amount = int.Parse(amountText);
            if (amount == 0) { return Result.Real(string.Empty); }

            amountText = amount.ToString();

            if (amountText.Length > 2) { return Result.None<string>(); }

            if (amountText.Length == 1) { return TryParseSingleDigitAmount(amountText); }

            if (amountText[0] == '1') { return TryFindTensPlaceValue(amountText); }

            var tensPlaceResult = TryFindTensPlaceValue(amountText[0].ToString()).GetResult("??");
            var onesPlaceResult = TryParseSingleDigitAmount(amountText[1].ToString()).GetResult("?");

            if (string.Equals(onesPlaceResult, "zero", StringComparison.InvariantCultureIgnoreCase)) { return Result.Real(tensPlaceResult); }

            return Result.Real($"{tensPlaceResult}-{onesPlaceResult}");
        }

        private PossibleResult<string> TryFindTensPlaceValue(string amount)
        {
            var result = TensPlaceValues.Where(x => x.Key == amount.ToString()).FirstOrDefault();
            if (result.Value == null) { return Result.None<string>(); }

            return Result.Real(result.Value);
        }

        private PossibleResult<string> TryParseHundredsAmount(string amountText)
        {
            var amount = int.Parse(amountText);
            if (amount == 0) { return Result.Real(string.Empty); }

            amountText = amount.ToString();

            if (amountText.Length == 1) { return TryParseSingleDigitAmount(amountText); }
            if (amountText.Length == 2) { return TryParseTensAmount(amountText); }

            var hundredsPlace = TryParseSingleDigitAmount(amountText[0].ToString()).GetResult(string.Empty);
            var tensPlace = TryParseTensAmount(amountText.Substring(1, 2).ToString()).GetResult(string.Empty);

            return Result.Real($"{hundredsPlace} Hundred {tensPlace}".TrimEnd());
        }

        private PossibleResult<string> TryParseThousandsAmount(string[] thousandPlaces)
        {
            var thousandsPlace = thousandPlaces[0];
            var hundredsPlace = TryParseHundredsAmount(thousandPlaces[1]).GetResult(string.Empty);

            var amount = int.Parse(thousandsPlace);
            if (amount == 0) { return Result.Real(hundredsPlace); }

            thousandsPlace = amount.ToString();

            var thousandsResult = TryParseThreeDigitPlaceValue(thousandsPlace);

            return Result.Real($"{thousandsResult} Thousand {hundredsPlace}".TrimEnd());
        }

        private PossibleResult<string> TryParseMillionsAmount(string[] thousandPlaces)
        {
            var millionsPlace = thousandPlaces[0];
            var thousandsPlace = TryParseThousandsAmount(new string[] { thousandPlaces[1], thousandPlaces[2] }).GetResult(string.Empty);

            var amount = int.Parse(millionsPlace);
            if (amount == 0) { return Result.Real(thousandsPlace); }

            millionsPlace = amount.ToString();

            var millionsResult = TryParseThreeDigitPlaceValue(millionsPlace);

            return Result.Real($"{millionsResult} Million {thousandsPlace}".TrimEnd());
        }

        private PossibleResult<string> TryParseBillionsAmount(string[] thousandPlaces)
        {
            var billionsPlace = thousandPlaces[0];
            var millionsPlace = TryParseMillionsAmount(new string[] { thousandPlaces[1], thousandPlaces[2], thousandPlaces[3] }).GetResult(string.Empty);

            var amount = int.Parse(billionsPlace);
            if (amount == 0) { return Result.Real(millionsPlace); }

            billionsPlace = amount.ToString();
            var billionsResult = TryParseThreeDigitPlaceValue(billionsPlace);

            return Result.Real($"{billionsResult} Billion {millionsPlace}".TrimEnd());
        }

        private string TryParseThreeDigitPlaceValue(string thousandPlace)
        {
            if (thousandPlace.Length == 1) { return TryParseSingleDigitAmount(thousandPlace).GetResult(string.Empty); }
            if (thousandPlace.Length == 2) { return TryParseTensAmount(thousandPlace).GetResult(string.Empty); }

            return TryParseHundredsAmount(thousandPlace).GetResult(string.Empty);
        }
    }
}
