using System;
using System.Globalization;
using System.Reflection;
using WFS.RecHub.Common.Crypto;
using System.Data;
using System.Configuration;
using CustomImageAPI.Interfaces;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using System.Linq;
using CustomImageAPI.Models;
using WFS.RecHub.ApplicationBlocks.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose: 
*
* Modification History
* CR 32562 JMC 03/02/2011 
*   -Modified GetImage() method to accept IItem interface instead of cImage object.
*   -Modified GetRelativeImagePath() method to accept IItem interface instead of cImage object.
* CR 45627 JMC 07/12/2011
*   -Added IDisposable interface
* CR 23161 JCS 06/01/2011
*   -Updated to use new 3DES encryption class.
* CR 28347 JNE 9/30/2010
*   -set return of GetImage to false by default.   
* CR 45615 JMC 07/12/2011
*   -Added IDisposable interface
* CR 48016 WJS 11/8/2011
*	-Add function to GetAllImages
* CR 47771 WJS 11/14/2011
*   -Add support for custom image dll
* CR 45986 WJS 11/23/2001
*	-Add ability to call GetImage for custom image dll
* CR 48475 WJS 11/24/2011
*	-Add function GetImageStorageMode and SetupOutputImagePath
* CR 33230 JMC 01/12/2012
*	-Added PutBatchImages() function.
* CR 50214 WJS 2/13/2012
*   -Added support for PutBatchImages for PICS
*   -refererence to DmpObjectRoot2 instead of DmpObjectRoot
*   -Add throw onlineFormatException to PutAllImages
*    Change putbatchImage to a single image
* CR 50571   WJS 2/17/2012
*   -Corrected params for getRelativeImagePath
* CR 50564 JMC 02/23/2012
*   -Removed SetSiteID property.
*   -Added SiteCode to calls to custom image Dll.
*   -Now passing in ProcessingDate instead of Now() to custom Dll.
* CR 50983 WJS 03/14/2012
*   -Change calls to custom image DLL from SiteCode to BatchSiteCode
* CR 50212 WJS 5/8/2012
*   - Add support for hyland
* CR 54318 WJS 7/18/2012
*  -Pass transactionID into relativeImagePath
* CR 52959 JNE 09/28/2012
*  - Add FileGroup images 
* WI 90246 CRG 03/18/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoImages
* WI 117833 JMC 10/18/2013
*   -Modified ipoImages and OLFServicesAPI to accept empty File Descriptors.    
* WI 146900 SAS 07/01/2014
*   -Modified ipoImages for Generating Images on a fly for Wire Payment Type
******************************************************************************/
namespace WFS.RecHub.Common
{
    public class cipoImages : _DMPObjectRoot2, IDisposable
    {
        /// <summary>
        /// Constant indicating the image is a picture of the item's front.
        /// </summary>
        public const int PAGE_FRONT = 0;

        /// <summary>
        /// Constant indicating the image is a picture of the item's back.
        /// </summary>
        public const int PAGE_BACK = 1;

        private ImageStorageMode m_ismImageStorageMode = ImageStorageMode.PICS;
        private ipoHylandImages m_objHyland = null;
        private ipoPICSImages m_objPICS = null;
        private ipoFILEGROUPImages m_objFILEGROUP = null;
        private readonly ICustomImageProvider _customImageProvider;


        //custom image properties
        private Object      m_objCustomImage    = null;
        private Type        m_customImageType   = null;

        public cipoImages(string siteKey, ImageStorageMode imageStorageMode, string imageRootPath)
        {
            this.siteKey = siteKey;

            m_ismImageStorageMode = imageStorageMode;

            cCrypto3DES cryp3DES = new cCrypto3DES();

            string sUserName;
            string sPassword;

            if (m_ismImageStorageMode == ImageStorageMode.HYLAND)
            {
                string sDatasource = ipoINILib.IniReadValue("Hyland", "Datasource");
                cryp3DES.Decrypt(ipoINILib.IniReadValue("Hyland", "Username"), out sUserName);
                cryp3DES.Decrypt(ipoINILib.IniReadValue("Hyland", "Password"), out sPassword);
                string sLicenseType = ipoINILib.IniReadValue("Hyland", "LicenseType");
                string sWebServiceURL = ipoINILib.IniReadValue("Hyland", "WebServiceURL");
                m_objHyland = new ipoHylandImages(this.siteKey, sWebServiceURL, sDatasource, sLicenseType, sUserName, sPassword);
            }
            else if (m_ismImageStorageMode == ImageStorageMode.PICS)
            {
                // add "\\" at the end of the PICS path
                if (imageRootPath.EndsWith("\\") == false)
                    imageRootPath = imageRootPath + "\\";
                m_objPICS = new ipoPICSImages(imageRootPath);
            }
            else if (m_ismImageStorageMode == ImageStorageMode.FILEGROUP)
            {
                if (imageRootPath.EndsWith("\\") == false)
                {
                    imageRootPath = imageRootPath + "\\";
                }
                m_objFILEGROUP = new ipoFILEGROUPImages(imageRootPath);
            }
            if (m_ismImageStorageMode == ImageStorageMode.CUSTOM)
            {
                var customImageProviderSetting = ConfigurationManager.AppSettings.Get("customimageprovider");
                var assembly = customImageProviderSetting.ToAssembly();
                var type = assembly.GetTypes().Where(x => x.GetInterface("ICustomImageProvider") != null).FirstOrDefault();
                if (type == null) throw new ConfigurationErrorsException("Unable to find a custom image provider.");
                eventLog.logEvent($"Identified a custom image provider ({type.Name}) from assembly '{assembly.FullName}'.", MessageImportance.Essential);
                _customImageProvider = type.ToInstance<ICustomImageProvider>();
                eventLog.logEvent($"Successfully created an instance of custom image provider ({type.FullName}).", MessageImportance.Essential);
            }
        }
        
        [Obsolete]
   		private void LoadCustomDll(string fileName, string imageRootPath) {

            try
            {
                Assembly a = Assembly.LoadFrom(fileName);
                Type[] myTypes = a.GetTypes();
                if (myTypes.Length > 0)
                {
                    m_customImageType = myTypes[0];
                    m_objCustomImage = Activator.CreateInstance(m_customImageType, imageRootPath);
                }

            }
            catch (Exception e)
            {
                throw;
            }
        }
		
        //Get all image in one request for hyland
        public bool GetAllImages(IItem irRequest)
        {
            bool bRetVal = false;
            if(m_ismImageStorageMode == ImageStorageMode.HYLAND) {
                bRetVal = m_objHyland.GetAllHylandDocumentsForABatch(irRequest);
            }
            return bRetVal;
        }

        // common logic for both PICS and Hyland
        public ImageResponse GetImage(IItem irRequest, int page) {

            string strFileExtension;
            string strFileDescriptor;
            
            return GetImage(irRequest, page, out strFileDescriptor, out strFileExtension);
        }

        public PossibleResult<CustomImage> TryGetCustomImage(IItem requestContext, int page = 0)
        {
            return GetCustomImage(requestContext, page);
        }

        // common logic for both PICS and Hyland
        public ImageResponse GetImage(IItem irRequest, int ipage, out string fileDescriptor, out string fileExtension)
        {
            byte[] imageBytes;
            CultureInfo enUS = new CultureInfo("en-US"); 
            bool bolRetVal = false; //CR 28347 9/30/2010 JNE
            
            if (m_ismImageStorageMode == ImageStorageMode.HYLAND)
            {
                bolRetVal = m_objHyland.GetImage(irRequest, ipage, out imageBytes);
                fileDescriptor = irRequest.FileDescriptor;
                fileExtension = "tif";
            }
            else if (m_ismImageStorageMode == ImageStorageMode.PICS)
            {
                bolRetVal = m_objPICS.GetImage(irRequest, ipage, out imageBytes, out fileDescriptor, out fileExtension);
            }
            else if (m_ismImageStorageMode == ImageStorageMode.FILEGROUP)
            {
                bolRetVal = m_objFILEGROUP.GetImage(irRequest, ipage, out imageBytes, out fileDescriptor, out fileExtension);
            }
            else
            {
                fileDescriptor = string.Empty;
                bolRetVal = true;
                var customImage = GetCustomImage(irRequest, ipage).GetResult(() =>
                {
                    bolRetVal = false;
                    return new CustomImage();
                });
                
                fileExtension = customImage.Details?.Extension ?? string.Empty;
                imageBytes = customImage.Data;
            }

            if (!bolRetVal)
            {
                if (ipage == 0) {
                    imageBytes = null;
                    //If no image data was pulled use ACH or Wire Transfer
                    return GetImageForACHOrWirePayment(irRequest);
                }
            }
            
            return new ImageResponse { IsSuccessful = bolRetVal, ImageBytes = imageBytes };
        }


        /// <summary>
        /// Generate Check Image for ACH or Wire Transfer
        /// </summary>
        /// <param name="irRequest"></param>
        /// <param name="imageBytes"></param>
        /// <returns></returns>
        public ImageResponse GetImageForACHOrWirePayment(IItem irRequest)
        {
            byte[] imageBytes = null;
            var response = new ImageResponse();

            BatchPaymentType enPaymentTypeKey;
            
            var iPaymentTypeKey = irRequest.PaymentType;
            if (iPaymentTypeKey == -1)
            {
                iPaymentTypeKey = GetPaymentTypeKey(irRequest);
            }
            if ((BatchPaymentType)iPaymentTypeKey == BatchPaymentType.ACH || (BatchPaymentType)iPaymentTypeKey == BatchPaymentType.WIRE)
            {
                enPaymentTypeKey = (BatchPaymentType)iPaymentTypeKey;
                imageBytes = null;                    
                ipoEmbeddedImage m_objEmbeddedImage = new ipoEmbeddedImage(this.siteKey);
                switch (ipoImageCommon.GetObjectType(irRequest))
                {
                    case ipoImageCommon.ObjectType.CHECK:
                        response.IsSuccessful = m_objEmbeddedImage.GetCheckImage(irRequest, enPaymentTypeKey, out imageBytes);
                        response.ImageBytes = imageBytes;
                        break;
                    case ipoImageCommon.ObjectType.DOCUMENT:
                        response = m_objEmbeddedImage.GetDocumentImage(irRequest, enPaymentTypeKey);
                        break;
                }
            }
            
            return response;
        }

        /// <summary>
        ///  Get the Payment Type Key
        /// </summary>
        /// <param name="irRequest"></param>
        /// <returns></returns>
        public Int16 GetPaymentTypeKey(IItem irRequest)
        {
            Int16 iBatchPaymentKey = -1;
            try
            {

                DataTable dt = new DataTable();
                if (ItemProcDAL.GetPaymentTypeKey(irRequest.BankID,
                                                   irRequest.LockboxID,
                                                   irRequest.DepositDate,
                                                   irRequest.BatchID,
                                                   out dt) && dt.Rows.Count > 0)                
                    iBatchPaymentKey = Convert.ToInt16(dt.Rows[0]["BatchPaymentTypeKey"]);
            }
            catch (Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, "GetPaymentType");
            }
            return iBatchPaymentKey;
        }

        public ImageStorageMode GetImageStorageMode()
        {
            return m_ismImageStorageMode;
        }

        public string GetRelativeImagePath(IItem irRequest, int ipage)
        {
            CultureInfo enUS = new CultureInfo("en-US"); 
            
            if (m_ismImageStorageMode == ImageStorageMode.HYLAND){
                return m_objHyland.GetRelativeImagePath(irRequest, ipage);
            }else if (m_ismImageStorageMode == ImageStorageMode.PICS){ 
                return m_objPICS.getRelativeImagePath(irRequest, ipage);
            }else if (m_ismImageStorageMode == ImageStorageMode.FILEGROUP){
                return m_objFILEGROUP.getRelativeImagePath(irRequest, ipage);
            }else {
                //eventLog.logEvent("Attempting to retrieve relative image path from custom image provider", MessageImportance.Essential);
                //var customImage = GetCustomImage(irRequest, ipage).GetResult(() =>
                //{
                //    eventLog.logEvent("Unable to successfully retrieve relative image path from custom image provider", MessageImportance.Essential);
                //    return new CustomImage();
                //});
                //return customImage.Details?.RelativePath ?? string.Empty;
                eventLog.logEvent("Not going to retrieve custom image path for custom image storage mode", MessageImportance.Essential);
                return string.Empty;
            }
        }

        public void SetOutputImagePath(string outputImagepath)
        {
            if (m_ismImageStorageMode == ImageStorageMode.HYLAND)
                m_objHyland.SetOutputImagePath(outputImagepath);
        }


        public bool PutBatchImages(ILTAImageInfo imageInfo, byte[] batchImage) {

            CultureInfo enUS = new CultureInfo("en-US"); 
            bool bolRetVal = false;

            try {
                switch(m_ismImageStorageMode) {
                    case ImageStorageMode.HYLAND:
                        //save the images locally they will be transfer up to the netwok via DIT.
                        bolRetVal = m_objPICS.PutBatchImage(siteKey, imageInfo, batchImage);
                        break;
                    case ImageStorageMode.PICS:
                        bolRetVal = m_objPICS.PutBatchImage(siteKey, imageInfo, batchImage);
                        break;
                    case ImageStorageMode.FILEGROUP:
                        bolRetVal = m_objFILEGROUP.PutBatchImage(siteKey, imageInfo, batchImage);
                        break;

                    default:
                        eventLog.logEvent("Custom Image Mode is active.  No images imported in this mode.", this.GetType().Name, MessageImportance.Essential);
                        bolRetVal = true;
                        break;
                }

            } catch(OnlineFormattedException) {
                throw;
            } catch(Exception ex) {
                eventLog.logError(ex, this.GetType().Name, "PutBatchImages");
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        private PossibleResult<CustomImage> GetCustomImage(IItem request, int page = 0)
        {
            eventLog.logEvent("About to call custom image provider.", MessageImportance.Essential);

            var customImage = _customImageProvider.GetImage(request, page).GetResult(() =>
            {
                eventLog.logEvent("Unable to retrieve image from custom image provider.", MessageImportance.Essential);
                return new CustomImage();
            });

            if (customImage.Details == null) return Result.None<CustomImage>();
            return Result.Real(customImage);
        }

        [Obsolete]
        private object CallCustomImageDLL(string methodString, object[] parameters)
        {
            object  result= null;
            try
            {
                MethodInfo method = m_customImageType.GetMethod(methodString);
                result = method.Invoke(m_objCustomImage, parameters);
            } 
            catch (Exception ex)
            {
                eventLog.logError(ex);
                throw;
            }
            return result;
        }
                


        // Pointer to an external unmanaged resource.
        private IntPtr handle;

        ////// Other managed resource this class uses.
        ////private Component component = new Component();

        // Track whether Dispose has been called.
        private bool disposed = false;

        // The class constructor.
        public cipoImages(IntPtr handle)
        {
            this.handle = handle;
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if(!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if(disposing)
                {
                    if (m_objHyland != null) {
                        m_objHyland.Dispose();
                    } 

                    if (m_objPICS != null) {
                        m_objPICS.Dispose();
                    }

                    //Dispose Method
                    if (m_objCustomImage != null) {
                         CallCustomImageDLL("Dispose", null);
                         
                    }

                    if (m_objFILEGROUP != null) {
                        m_objFILEGROUP.Dispose();
                    }

                    ////// Dispose managed resources.
                    ////component.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                CloseHandle(handle);
                handle = IntPtr.Zero;

                // Note disposing has been done.
                disposed = true;

            }
        }

        // Use interop to call the method necessary
        // to clean up the unmanaged resource.
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~cipoImages()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
    } 
}
