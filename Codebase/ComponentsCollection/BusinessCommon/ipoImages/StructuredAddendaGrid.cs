﻿using System;
using System.Collections;
using System.Drawing;
using WFS.RecHub.Common.Log;

namespace WFS.RecHub.Common
{
    public class StructuredAddendaGrid : IDisposable
    {
        private readonly SolidBrush _brushBlack;
        private readonly cEventLog _eventLog;
        private readonly Font _fntEntryData;
        private readonly Font _fntEntryHeader;
        private readonly Graphics _graphics;
        private readonly int _iYAxis;
        private readonly cParsedAddendaCollection _parsedAddendaCollection;
        private readonly Pen _penBlack;
        private readonly StringFormat _stringFormat;

        private StructuredAddendaGrid(cImageData objImageData, int iYAxis, Graphics graphics, cEventLog eventLog)
        {
            _iYAxis = iYAxis;
            _graphics = graphics;
            _eventLog = eventLog;
            _parsedAddendaCollection = objImageData.ParsedAddendas;

            _fntEntryHeader = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Bold);
            _fntEntryData = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Regular);
            _brushBlack = new SolidBrush(Color.Black);
            _penBlack = new Pen(Color.Black, 3);
            _stringFormat = new StringFormat {Alignment = StringAlignment.Center};
        }
        public void Dispose()
        {
            _stringFormat.Dispose();
            _brushBlack.Dispose();
            _fntEntryData.Dispose();
            _fntEntryHeader.Dispose();
        }

        public static void Draw(cImageData objImageData, int iYAxis, Graphics grfx, cEventLog eventLog)
        {
            using (var instance = new StructuredAddendaGrid(objImageData, iYAxis, grfx, eventLog))
            {
                instance.DrawAll();
            }
        }
        private void DrawAll()
        {
            int x = 25;
            int y = _iYAxis;
            int iWidth = 3550;
            int iHdrFntHeight = 63;
            int iDataFntHeight = Convert.ToInt32(_graphics.MeasureString("Ag", _fntEntryData).Height) + 1;
            y += (2 * iHdrFntHeight);
            try
            {
                _graphics.DrawRectangle(_penBlack, x, y, iWidth - 50, iHdrFntHeight * 4);
                int iXMargin = 25;
                int iColWidth = (iWidth - (2 * iXMargin)) / 10;
                for (int i = 1; i < 10; i++)
                {
                    _graphics.DrawLine(_penBlack, iXMargin + (i * iColWidth), y, iXMargin + (i * iColWidth), y + (4 * iHdrFntHeight));
                }
                //RemittanceOriginator
                _graphics.DrawString(ipoImageCommon.REMITTANCE, _fntEntryHeader, _brushBlack, new PointF(iXMargin + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.REMITTANCE, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.ORIGINATOR, _fntEntryHeader, _brushBlack, new PointF(iXMargin + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.ORIGINATOR, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));

                //RemittanceBeneficiary
                _graphics.DrawString(ipoImageCommon.REMITTANCE, _fntEntryHeader, _brushBlack, new PointF(iXMargin + iColWidth + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.REMITTANCE, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.BENEFICIARY, _fntEntryHeader, _brushBlack, new PointF(iXMargin + iColWidth + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.BENEFICIARY, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));

                //PrimaryRemittanceDocInfo
                _graphics.DrawString(ipoImageCommon.PRIMARY, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (2 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.PRIMARY, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.REMITTANCE, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (2 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.REMITTANCE, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));
                _graphics.DrawString(ipoImageCommon.DOC_INFO, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (2 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.DOC_INFO, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight * 2));

                //ActualAmountPaid
                _graphics.DrawString(ipoImageCommon.ACTUAL, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (3 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.ACTUAL, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.AMT_PAID, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (3 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.AMT_PAID, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));

                //Gross Amount
                _graphics.DrawString(ipoImageCommon.GROSS, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (4 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.GROSS, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.AMT, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (4 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.AMT, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));

                //Discount Amount
                _graphics.DrawString(ipoImageCommon.DISCOUNT, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (5 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.DISCOUNT, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.AMT, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (5 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.AMT, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));

                //Adjustment Information
                _graphics.DrawString(ipoImageCommon.ADJUSTMENT, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (6 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.ADJUSTMENT, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.INFORMATION, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (6 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.INFORMATION, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));

                //RemittanceDate
                _graphics.DrawString(ipoImageCommon.REMITTANCE, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (7 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.REMITTANCE, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.DATE, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (7 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.DATE, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));

                //SecondaryRemittanceDocInfo
                _graphics.DrawString(ipoImageCommon.SECONDARY, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (8 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.SECONDARY, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.REMITTANCE, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (8 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.REMITTANCE, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));
                _graphics.DrawString(ipoImageCommon.DOC_INFO, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (8 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.DOC_INFO, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight * 2));

                //RemittanceFreeText
                _graphics.DrawString(ipoImageCommon.REMITTANCE, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (9 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.REMITTANCE, _fntEntryHeader).Width)) / 2), y));
                _graphics.DrawString(ipoImageCommon.FREE_TEXT, _fntEntryHeader, _brushBlack, new PointF(iXMargin + (9 * iColWidth) + ((iColWidth - Convert.ToInt32(_graphics.MeasureString(ipoImageCommon.FREE_TEXT, _fntEntryHeader).Width)) / 2), y + iHdrFntHeight));

                y += (4 * iHdrFntHeight);
                x = 25;
                //Put Data in the grid

                ArrayList arlBatchSequence = _parsedAddendaCollection.GetDistinctBatchSequence();

                foreach (int iBatchSequence in arlBatchSequence)
                {
                    //Fill out data
                    var gridRowHeight = iDataFntHeight;
                    foreach (cParsedAddenda objParsedAddenda in _parsedAddendaCollection)
                    {
                        if (objParsedAddenda.BatchSequence == iBatchSequence)
                        {
                            var cellTextHeight = 0;
                            switch (objParsedAddenda.FieldName.ToLower())
                            {
                                //Remittance Originator
                                case ipoImageCommon.REMITTANCE_ORIGINATOR:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin, iColWidth);
                                    break;
                                case ipoImageCommon.REMITTANCE_BENEFICIARY:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin + iColWidth, iColWidth);
                                    break;
                                case ipoImageCommon.REMITTANCE_PRIM_DOC_INFO:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin + (2 * iColWidth), iColWidth);
                                    break;
                                case ipoImageCommon.REMITTANCE_ACT_AMT_PAID:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin + (3 * iColWidth), iColWidth);
                                    break;
                                case ipoImageCommon.REMITTANCE_GROSS_AMT:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin + (4 * iColWidth), iColWidth);
                                    break;
                                case ipoImageCommon.REMITTANCE_DISC_AMT:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin + (5 * iColWidth), iColWidth);
                                    break;
                                case ipoImageCommon.REMITTANCE_ADJ_INFO:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin + (6 * iColWidth), iColWidth);
                                    break;
                                case ipoImageCommon.REMITTANCE_DATE:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin + (7 * iColWidth), iColWidth);
                                    break;
                                case ipoImageCommon.REMITTANCE_SEC_DOC_INFO:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin + (8 * iColWidth), iColWidth);
                                    break;
                                case ipoImageCommon.REMITTANCE_FREE_TEXT:
                                    cellTextHeight = PutText(objParsedAddenda.FieldValue, y, iXMargin + (9 * iColWidth), iColWidth);
                                    break;
                            }

                            if (cellTextHeight > gridRowHeight)
                            {
                                gridRowHeight = cellTextHeight;
                            }
                        }
                    }

                    //Draw cell borders for this row
                    _graphics.DrawRectangle(_penBlack, x, y, iWidth - 50, gridRowHeight);

                    for (int i = 1; i < 10; i++)
                    {
                        _graphics.DrawLine(_penBlack, iXMargin + (i * iColWidth), y, iXMargin + (i * iColWidth), y + gridRowHeight);
                    }

                    y += gridRowHeight;
                }
            }
            catch (Exception ex)
            {
                _eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Put data into the grid for parsed addenda
        /// </summary>
        /// <returns>Actual height of the cell's contents, in pixels.</returns>
        private int PutText(string strValue, float yAxis, int iXMargin, int iColWidth)
        {
            try
            {
                var size = _graphics.MeasureString(strValue, _fntEntryData, iColWidth, _stringFormat);
                var rect = new RectangleF(iXMargin, yAxis, iColWidth, size.Height);
                _graphics.DrawString(strValue, _fntEntryData, _brushBlack, rect, _stringFormat);
                return (int) Math.Ceiling(size.Height);
            }
            catch (Exception ex)
            {
                _eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
        }
    }
}