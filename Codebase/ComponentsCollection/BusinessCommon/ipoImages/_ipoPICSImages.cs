using System;
using System.IO;
using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Jason Efken
* Date: 9/28/2012
*
* Purpose: Base Class used by ipoPICSImages & ipoFILEGROUPImages.
*          Has basic retrieval and image type functions 
*
* Modification History
* WI 90246 CRG 03/18/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoImages
* WI 117833 JMC 10/18/2013
*   -Modified ipoImages and OLFServicesAPI to accept empty File Descriptors.    
* WI 156594 SAS 07/31/2014 
*   Added a stored procedure to gather Batch Source Short Name.        
*   Used to determine if we need to write out the payment source short name to the relative path.
* WI 166623  SAS 09/17/2014 
*   -Changes done to retrieve Image Path
* WI  173874  SAS 10/23/2014 
*   - Changes done to ignore error if path is not found for ACH/Wire
* WI 175199 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName   
* WI 176352 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName     
******************************************************************************/
namespace WFS.RecHub.Common
{
    public abstract class _ipoPICSImages : _DMPObjectRoot
    {
        protected string m_sImageRootPath = string.Empty;
        public const string IMG_ABRV_BLACK_AND_WHITE_FRONT = "F";
        public const string IMG_ABRV_BLACK_AND_WHITE_BACK = "B";
        public const string IMG_ABRV_COLOR_FRONT = "FC";
        public const string IMG_ABRV_COLOR_BACK = "BC";
        public const string IMG_ABRV_GRAYSCALE_FRONT = "FG";
        public const string IMG_ABRV_GRAYSCALE_BACK = "BG";
        public const string IMG_ABRV_IE_FRONT = "F_IE";
        public const string IMG_ABRV_IE_BACK = "B_IE";

        /// <summary>
        /// File Descriptor for all checks.
        /// </summary>
        public const string FILE_DESCRIPTOR_CHECK = "C";

        /// <summary>
        /// File Descriptor for documents that are Scanned Checks.
        /// </summary>
        public const string FILE_DESCRIPTOR_SCANNED_CHECK = "SC";

        /// <summary>
        /// File Descriptor for documents that are type Unknown.
        /// </summary>
        public const string FILE_DESCRIPTOR_UNKNOWN = "UN";

        /// <summary>
        /// Payment source
        /// </summary>
        protected const string PAYMENT_SOURCE_INTEGRA_PAY = "integraPAY";

        public _ipoPICSImages(string sImageRootPath)
        {
            m_sImageRootPath = sImageRootPath;
        }

        //public void Dispose()
        //{
        //}
        
        //Abstract classes
        public abstract string getRelativeImagePath(string picsDate, int bankID, int lockboxID, long batchID, int batchSequence, string fileDescriptor, WorkgroupColorMode workgroupColorMode, int page, long sourceBatchID, string batchSourceShortName, string ImportSourceShortName); 
        public abstract bool PutBatchImage(string siteKey, ILTAImageInfo imageInfo, byte[] batchImage);
        //

        /// <summary>
        /// Returns the fully qualified path of the image, including 
        /// the root drive information.
        /// </summary>
        /// <param name="image">Image object to be located.</param>
        /// <param name="page">Page (0=Front, 1=Back)</param>
        /// <param name="imageRootPath">Image Root directory.</param>
        /// <returns></returns>
        public string getImagePath(IItem imageRequest, int iPage){

            string strRelativeImagePath;
            
            strRelativeImagePath = getRelativeImagePath(imageRequest, iPage);
            
            if(strRelativeImagePath != null && strRelativeImagePath.Length > 0) {
                return(Path.Combine(m_sImageRootPath, getRelativeImagePath(imageRequest, iPage)));
            } else {
                return(string.Empty);
            }       
        }

        /// <summary>
        /// Determines the location and filename of the requested image, and 
        /// returns the path to the image only if the image actually exists.
        /// </summary>
        /// <param name="image">Image object to be located.</param>
        /// <param name="page">Page (0=Front, 1=Back)</param>
        /// <param name="imageRootPath">
        /// Image Root directory.  This is required to ensure
        /// that the image exists before returning a value.
        /// </param>
        /// <returns>Path of image relative to the Image root path</returns>
        public string getRelativeImagePath(IItem imageRequest, int iPage)
        {
            return (getRelativeImagePath(imageRequest.PICSDate,
                                         imageRequest.BankID,
                                         imageRequest.LockboxID,
                                         imageRequest.BatchID,
                                         imageRequest.BatchSequence,
                                         imageRequest.FileDescriptor,
                                         imageRequest.DefaultColorMode,                                         
                                         iPage,
                                         imageRequest.SourceBatchID,
                                         imageRequest.BatchSourceShortName,
                                         imageRequest.ImportTypeShortName));
        }

        /// <summary>
        /// Determines the page part of an image's expected name.
        /// </summary>
        /// <param name="colorMode">Bitonal, Grayscale, or Color.</param>
        /// <param name="page">0=Front, 1=Back</param>
        /// <returns>The page part of an image's expected name.</returns>
        protected string getImageType(WorkgroupColorMode colorMode, int page)
        {
            /*
            PICS_FRONT					= (0), 
		    PICS_BACK					= (1),
		    PICS_FRONT_COLOR			= (2),
		    PICS_BACK_COLOR				= (3),
		    PICS_FRONT_GRAY				= (4),
		    PICS_BACK_GRAY				= (5),
		    PICS_IN_ORIG				= (6),
		    PICS_IN_REC					= (7),
		    PICS_FRONT_ORIG				= (8), 
		    PICS_FRONT_REC				= (9),
		    PICS_BACK_ORIG				= (10),
		    PICS_BACK_REC				= (11),
		    PICS_FRONT_COLOR_ORIG		= (12),
		    PICS_FRONT_COLOR_REC		= (13),
		    PICS_FRONT_GRAY_ORIG		= (14),
		    PICS_FRONT_GRAY_REC			= (15),
		    PICS_BACK_COLOR_ORIG		= (16),
		    PICS_BACK_COLOR_REC			= (17),
		    PICS_BACK_GRAY_ORIG			= (18),
		    PICS_BACK_GRAY_REC			= (19),
		    PICS_FRONT_IE				= (20),
		    PICS_BACK_IE				= (21)
            */

            string strRetVal = "";

            switch (colorMode)
            {
                case WorkgroupColorMode.COLOR_MODE_BITONAL:
                    switch (page)
                    {
                        case 0:
                            strRetVal = IMG_ABRV_BLACK_AND_WHITE_FRONT;
                            break;
                        case 1:
                            strRetVal = IMG_ABRV_BLACK_AND_WHITE_BACK;
                            break;
                    }
                    break;
                case WorkgroupColorMode.COLOR_MODE_COLOR:
                    switch (page)
                    {
                        case 0:
                            strRetVal = IMG_ABRV_COLOR_FRONT;
                            break;
                        case 1:
                            strRetVal = IMG_ABRV_COLOR_BACK;
                            break;
                    }
                    break;
                case WorkgroupColorMode.COLOR_MODE_GRAYSCALE:
                    switch (page)
                    {
                        case 0:
                            strRetVal = IMG_ABRV_GRAYSCALE_FRONT;
                            break;
                        case 1:
                            strRetVal = IMG_ABRV_GRAYSCALE_BACK;
                            break;
                    }
                    break;
            }

            return (strRetVal);
        }

        public bool GetImage(IItem irRequest, int iPage, out byte[] imageBytes, out string fileDescriptor, out string fileExtension)
        {
            string sFileName;
            byte[] byteData;
            bool bRet;

            try
            {
                sFileName = getImagePath(irRequest, iPage);

                if(sFileName != null && sFileName.Length > 0 && File.Exists(sFileName)) {
                    byteData = null;
                    byteData = ReadFromFile(sFileName);
                    imageBytes = byteData;
                    fileExtension = Path.GetExtension(sFileName);

                    if(string.IsNullOrEmpty(irRequest.FileDescriptor)) {
                        if(!string.IsNullOrEmpty(sFileName)) {
                            string[] arFileParts = sFileName.Split('_');
                            if(arFileParts.Length == 4) {
                                fileDescriptor = arFileParts[2];
                            } else {
                                fileDescriptor = string.Empty;
                            }
                        } else {
                            fileDescriptor = string.Empty;
                        }
                    } else {
                        fileDescriptor = irRequest.FileDescriptor;
                    }

                    bRet = true;
                } else {
                    imageBytes = null;
                    fileExtension = string.Empty;
                    fileDescriptor = string.Empty;
                    bRet = false;
                }
            }
            catch (System.Exception ex)
            {
                imageBytes = null;
                fileExtension = string.Empty;
                fileDescriptor = string.Empty;
                eventLog.logError(ex);
                bRet = false;
            }

            return bRet;
        }

        protected byte[] ReadFromFile(string FilePath)
        {
            byte[] pageData = null;

            try
            {
                using (FileStream fStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader binReader = new BinaryReader(fStream))
                    {
                        pageData = binReader.ReadBytes((int)fStream.Length);
                        binReader.Close();
                    }
                    fStream.Close();
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logError(ex);
            }

            return pageData;
        }

        /// <summary>
        /// WI 156594 : Used to determine if we need to write out the payment
        /// source short name to the relative path.
        /// </summary>
        /// <param name="shortname"></param>
        /// <returns></returns>
        protected bool pathNeedsPaymentSource(string shortname)
        {
            return !shortname.Equals(PAYMENT_SOURCE_INTEGRA_PAY);
        }

        /// <summary>
        /// Is ACH or Wire Payment
        /// </summary>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="PicsDate"></param>
        /// <param name="BatchID"></param>
        /// <returns></returns>
        public bool ISACHOrWIRE(int BankID,
                                 int LockboxID,
                                 string PicsDate,
                                 long BatchID)
        {
            bool bRetVal = false;
            DateTime dtDepositDate = DateTime.ParseExact(PicsDate, "yyyyMMdd", null);
            Int16 iBatchPaymentKey = -1;
            try
            {
                DataTable dt = new DataTable();
                if (OLFServiceDAL.GetPaymentTypeKey(BankID,
                                                  LockboxID,
                                                  dtDepositDate,
                                                  BatchID,
                                                  out dt) && dt.Rows.Count > 0)
                    iBatchPaymentKey = Convert.ToInt16(dt.Rows[0]["BatchPaymentTypeKey"]);
                if ((BatchPaymentType)iBatchPaymentKey == BatchPaymentType.ACH || (BatchPaymentType)iBatchPaymentKey == BatchPaymentType.WIRE)
                    bRetVal = true;
            }
            catch (Exception ex)
            {
                bRetVal = false;
                eventLog.logError(ex, this.GetType().Name, "ISACHOrWIRE");
            }
            return bRetVal;
        }
    }
}