﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using WFS.integraPAY.Online.Common;

namespace WFS.integraPAY.Online.TestipoImages
{
    public partial class Form1 : Form
    {
        public const int PAGE_FRONT = 0;
        public const int PAGE_BACK = 1;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            pictureBox1.Invalidate();

            cipoImages ipoImages = new cipoImages("IPOnline", ImageStorageMode.HYLAND, "\\\\omaipoimgvm\\Img");

            byte[] objBytes;

            bool bRet = false;

            if(cbIsCheck.Checked)
            {
                cCheck check = new cCheck();
                check.BankID = Convert.ToInt32(tbBank.Text);
                check.LockboxID = Convert.ToInt32(tbLockbox.Text);
                check.BatchID = Convert.ToInt32(tbBatch.Text);
                check.BatchSequence = Convert.ToInt32(tbBatchSeq.Text);
                check.ProcessingDate = dateTimePicker1.Value;

                bRet = ipoImages.GetImage((cImage)check, cbFront.Checked ? PAGE_FRONT : PAGE_BACK, out objBytes);
            }
            else
            {
                cDocument document = new cDocument();
                document.BankID = Convert.ToInt32(tbBank.Text);
                document.LockboxID = Convert.ToInt32(tbLockbox.Text);
                document.BatchID = Convert.ToInt32(tbBatch.Text);
                document.BatchSequence = Convert.ToInt32(tbBatchSeq.Text);
                document.ProcessingDate = dateTimePicker1.Value;

                bRet = ipoImages.GetImage((cImage)document, cbFront.Checked ? PAGE_FRONT : PAGE_BACK, out objBytes);
            }

            if(bRet)
            {
                MemoryStream stmBLOBData = new MemoryStream(objBytes);
                pictureBox1.Image = Image.FromStream(stmBLOBData);
            }
            else
            {
                MessageBox.Show("Error", "Could not get image!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
