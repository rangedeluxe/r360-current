﻿namespace WFS.integraPAY.Online.TestipoImages
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbBatchSeq = new System.Windows.Forms.TextBox();
            this.tbBatch = new System.Windows.Forms.TextBox();
            this.tbLockbox = new System.Windows.Forms.TextBox();
            this.tbBank = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cbIsCheck = new System.Windows.Forms.CheckBox();
            this.cbFront = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Processing Date:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(167, 175);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Go!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(12, 204);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(440, 185);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(101, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Bank:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Lockbox:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(98, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Batch:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Batch Sequence:";
            // 
            // tbBatchSeq
            // 
            this.tbBatchSeq.Location = new System.Drawing.Point(142, 121);
            this.tbBatchSeq.Name = "tbBatchSeq";
            this.tbBatchSeq.Size = new System.Drawing.Size(100, 20);
            this.tbBatchSeq.TabIndex = 4;
            // 
            // tbBatch
            // 
            this.tbBatch.Location = new System.Drawing.Point(142, 93);
            this.tbBatch.Name = "tbBatch";
            this.tbBatch.Size = new System.Drawing.Size(100, 20);
            this.tbBatch.TabIndex = 3;
            // 
            // tbLockbox
            // 
            this.tbLockbox.Location = new System.Drawing.Point(142, 65);
            this.tbLockbox.Name = "tbLockbox";
            this.tbLockbox.Size = new System.Drawing.Size(100, 20);
            this.tbLockbox.TabIndex = 2;
            // 
            // tbBank
            // 
            this.tbBank.Location = new System.Drawing.Point(142, 37);
            this.tbBank.Name = "tbBank";
            this.tbBank.Size = new System.Drawing.Size(100, 20);
            this.tbBank.TabIndex = 1;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(142, 9);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // cbIsCheck
            // 
            this.cbIsCheck.AutoSize = true;
            this.cbIsCheck.Location = new System.Drawing.Point(248, 151);
            this.cbIsCheck.Name = "cbIsCheck";
            this.cbIsCheck.Size = new System.Drawing.Size(74, 17);
            this.cbIsCheck.TabIndex = 6;
            this.cbIsCheck.Text = "Is Check?";
            this.cbIsCheck.UseVisualStyleBackColor = true;
            // 
            // cbFront
            // 
            this.cbFront.AutoSize = true;
            this.cbFront.Location = new System.Drawing.Point(142, 151);
            this.cbFront.Name = "cbFront";
            this.cbFront.Size = new System.Drawing.Size(56, 17);
            this.cbFront.TabIndex = 10;
            this.cbFront.Text = "Front?";
            this.cbFront.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 401);
            this.Controls.Add(this.cbFront);
            this.Controls.Add(this.cbIsCheck);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.tbBank);
            this.Controls.Add(this.tbLockbox);
            this.Controls.Add(this.tbBatch);
            this.Controls.Add(this.tbBatchSeq);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Test ipoImages";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbBatchSeq;
        private System.Windows.Forms.TextBox tbBatch;
        private System.Windows.Forms.TextBox tbLockbox;
        private System.Windows.Forms.TextBox tbBank;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckBox cbIsCheck;
        private System.Windows.Forms.CheckBox cbFront;
    }
}

