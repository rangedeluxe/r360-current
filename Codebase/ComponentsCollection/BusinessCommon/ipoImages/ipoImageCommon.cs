﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using WFS.RecHub.ApplicationBlocks.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 06/19/2014
*
* Purpose: Contains constant data for IPOimages
*
* Modification History
* WI 146899 SAS 07/01/2014
*   -Modified ipoImages for Generating Images on a fly for ACH Payment Type
* WI 146900 SAS 07/01/2014
*   -Modified ipoImages for Generating Images on a fly for Wire Payment Type
* WI 161054 SAS 08/25/2014
*   -Modified ipoImages for pulling data to display on check. 
* WI 164621 SAS 09/09/2014
*   -Modified ipoImages for adding DDAKey. 
* WI  174734 SAS 11/03/2014 
*   -Modified ipoImages to replace DDAKey with DDA. 
*  -Added additional data for CheckDataEntry
* WI  175901 SAS 11/04/2014 
* -Modified ipoImages to add all the columns for parsed data. 
* WI  179141 SAS 11/21/2014 
* -Modified ipoImages to added RelatedRemittanceData constant
******************************************************************************/
namespace WFS.RecHub.Common
{
    public static class ipoImageCommon
    {
        /// <summary>
        /// Enumerations for PaymentTypesKey
        /// Added by : SAS
        /// </summary>
        public enum PaymentTypeKey
        {
            INVALID = -1,
            CHECK = 0,
            ACH = 1,
            WIRE = 2,
            CREDIT_CARD = 3
        }


        /// <summary>
        /// Enumerations for Table        
        /// </summary>
        public enum TableName
        {
            CHECK = 0,
            STUBS = 2,
        }

        /// <summary>
        /// Detect if the object is of Check or Document
        /// </summary>
        public enum ObjectType
        {
            INVALID = 0,
            CHECK = 1,
            DOCUMENT = 2
        }

        /// <summary>
        /// Remittance Type
        /// </summary>
        public enum RemittanceType
        {

            UNKNOWN=-1,
            STRUCTURED = 1,
            UNSTRUCTURED = 2,
            RELATED=3
        }


        /// <summary>
        /// Data Entry Table names 
        /// </summary>
        public const string CHECKS_TABLE = "checks";
        public const string STUBS_TABLE = "stubs";
        /// <summary>
        /// Checks Data Entry columns
        /// </summary>


        public const string CHECKS_DATA_ENTRY_RECEIVING_COMPANY = "receivingcompany";
        public const string CHECKS_DATA_ENTRY_RECEIVING_INDIVIDUAL = "receivingindividual";
        public const string CHECKS_DATA_ENTRY_ACH_BATCH_NUMBER = "achbatchnumber";
        public const string CHECKS_DATA_ENTRY_COMPANY_ID = "companyid";
        public const string CHECKS_DATA_ENTRY_COMPANY_NAME = "companyname";
        public const string CHECKS_DATA_ENTRY_STANDARD_ENTRY_CLASS = "standardentryclass";
        public const string CHECKS_DATA_ENTRY_ENTRY_DESCRIPTION = "entrydescription";
        public const string CHECKS_DATA_ENTRY_COMPANY_DATA = "companydata";
        public const string CHECKS_DATA_ENTRY_DESCRIPTIVE_DATE = "descriptivedate";
        public const string CHECKS_DATA_ENTRY_EFFECTIVE_DATE = "effectivedate";
        public const string CHECKS_DATA_ENTRY_SETTLEMENT_DATE = "settlementdate";
        public const string CHECKS_DATA_ENTRY_ELECTRONIC_TRANS_CD = "electronictransactioncode";
        public const string CHECKS_DATA_ENTRY_ACCOUNT_NUMBER = "accountnumber";
        public const string CHECKS_DATA_ENTRY_INDIVIDUAL_ID = "individualid";
        public const string CHECKS_DATA_ENTRY_TRACE_NUMBER = "tracenumber";
        public const string CHECKS_DATA_ENTRY_BAI_FILE_CREATION_DATE = "baifilecreationdate";
        public const string CHECKS_DATA_ENTRY_BAI_FILE_ID = "baifileid";
        public const string CHECKS_DATA_ENTRY_CUSTOMER_ACCOUNT_NUMBER = "customeraccountnumber";
        public const string CHECKS_DATA_ENTRY_TYPE_CODE = "typecode";
        public const string CHECKS_DATA_ENTRY_BAI_BANK_REFERENCE = "baibankreference";
        public const string CHECKS_DATA_ENTRY_BAI_CUSTOMER_REFERENCE = "baicustomerreference";
        public const string CHECKS_DATA_ENTRY_BAI_TEXT = "baitext";
        public const string CHECKS_DATA_ENTRY_REASSOCIATION_TRACE_NUMBER = "reassociationtracenumber";
        public const string CHECKS_DATA_ENTRY_BPR_MONETARY_AMOUNT = "bprmonetaryamount";
        public const string CHECKS_DATA_ENTRY_BPR_ACCOUNT_NUMBER = "bpraccountnumber";

        /// <summary>
        /// Checks table Columns
        /// </summary>
        public const string CHECKS_AMOUNT = "amount";
        public const string CHECKS_REMITTER = "remittername";
        public const string CHECKS_SOURCE_PROCESSING_DATE_KEY = "sourceprocessingdatekey";
        public const string WORK_GROUP_LNG_NAME = "workgrouplongname";
        public const string CHECKS_DDA = "dda";
        

        /// <summary>
        /// Stubs Data Entry columns 
        /// </summary>
        public const string STUBS_DATA_ENTRY_REMIT_BENEF = "remittancebeneficiary";

        /// <summary>
        /// Stubs table Columns
        /// </summary>
        public const string STUBS_INVOICE = "invoice";

        /// <summary>
        /// ACH Document Image Constants
        /// </summary>
        public const string ACH_PAYMENT_HEADING = "ACH Payment";
        public const string ACH_BATCH_INFORMATION = "ACH Batch Information";
        public const string ACH_BATCH_NUMBER = "ACH Batch Number:";
        public const string ACH_COMPANY_ID = "Company ID:";
        public const string ACH_COMPANY_NAME = "Company Name:";
        public const string ACH_ENTRY_CLASS = "Entry Class:";
        public const string ACH_ENTRY_DESCRIPTION = "Entry Description:";
        public const string ACH_COMPANY_DATA = "Company Data:";
        public const string ACH_DESCRIPTIVE_DATE = "Descriptive Date:";
        public const string ACH_EFFECTIVE_DATE = "Effective Date:";
        public const string ACH_SETTLEMENT_DATE = "Settlement Date:";
        public const string ACH_ENTRY_DETAIL = "ACH Entry Detail";
        public const string ACH_TRANSACTION_CODE = "Transaction Code:";
        public const string ACH_ACCOUNT_NUMBER = "Account Number:";
        public const string ACH_AMOUNT = "Amount:";
        public const string ACH_INDIVIDUAL_ID = "Individual ID:";
        public const string ACH_RECEIVING_COMPANY = "Receiving Company:";
        public const string ACH_TRACE_NUMBER = "Trace Number:";
        public const string ACH_RAW_ADDENDA = "ACH Raw Addenda";
        public const string ACH_PARSED_ADDENDA = "ACH Parsed Addenda";


        /// <summary>
        /// Wire Transfer Document Image Constants
        /// </summary>
        public const string WIRE_PAYMENT_HEADING = "Wire Transfer Payment (BAI2)";
        public const string WIRE_FILE_CREATION = "File Creation:";
        public const string WIRE_FILE_ID = "File ID:";
        public const string WIRE_CUSTOMERT_ACCOUNT = "Customer Account:";
        public const string WIRE_PAYMENT_TYPE = "Payment Type:";
        public const string WIRE_AMOUNT = "Amount:";
        public const string WIRE_BANK_REFERENCE_NO = "Bank Reference Number:";
        public const string WIRE_CUSTOMER_REFERENCE_NO = "Customer Reference Number:";
        public const string WIRE_UNPARSED_TEXT = "Unparsed Text:";
        public const string WIRE_PARSED_TEXT = "Parsed Text:";

        /// <summary>
        /// Font Constant
        /// </summary>
        public const string FONT_NAME_E13B = "MICRE13B Match Tryout";
        public const string FONT_NAME_ARIAL = "Arial";
        public const string FONT_NAME_COURIER_NAME = "Courier New";


        /// <summary>
        /// ACH Raw Addenda Data Example
        /// </summary>
        public const string ACH_RAW_DATA_1 = "00000000011111111112222222222333333333344444444445555555555666666666677777777778";
        public const string ACH_RAW_DATA_2 = "12345678901234567890123456789012345678901234567890123456789012345678901234567890";


        /// <summary>
        /// ACH Parsed Addenda Image Column Names
        /// </summary>
        public const string ADDENDA_BPR = "BPR";
        public const string ADDENDA_MONE = "Monetary";
        public const string ADDENDA_AMT = "Amount";
        public const string ADDENDA_NUM = "Number";
        public const string ADDENDA_RMR = "RMR";
        public const string ADDENDA_REF = "Reference";
        public const string ADDENDA_RMR_TOT = "RMR Total";
        public const string ADDENDA_DISC = "Discount";
        public const string ADDENDA_ACCOUNT = "Account";
        public const string ADDENDA_INVOICE = "Invoice";
        public const string REASSOCIATION = "Reassociation";
        public const string TRACE = "Trace";        
        


        /// <summary>
        /// ACH Parsed Addenda Display Name in Stubs and StubsDataEntry
        /// </summary>
        public const string DATA_ENTRY_COL_BPR_MONE_AMT = "bpr monetary amount";
        public const string DATA_ENTRY_COL_BPR_ACT_NUM = "bpr account number";
        public const string DATA_ENTRY_COL_RMR_REF_NUM = "rmr reference number";
        public const string DATA_ENTRY_COL_RMR_MONE_AMT = "rmr monetary amount";
        public const string DATA_ENTRY_COL_RMR_TOT_INV_AMT = "rmr total invoice amount";
        public const string DATA_ENTRY_COL_RMR_DISC_AMT = "rmr discount amount";
        public const string DATA_ENTRY_COL_AMT = "amount";
        public const string DATA_ENTRY_COL_ACCOUNT_NUM = "accountnumber";



        /// <summary>
        /// Payment Type Constants
        /// </summary>
        public const string INCOMING_MONEY_TRANSFER = "- Incoming Money Transfer";
        public const string OUTGOING_MONEY_TRANSFER = "- Outgoing Money Transfer";
        public const string UNKNOWN_MONEY_TRANSFER = "- Unknown";

        public const string INCOMING_MONEY_TRANSFER_TYPE_CODE = "195";
        public const string OUTGOING_MONEY_TRANSFER_TYPE_CODE = "495";


        /// <summary>
        /// Wire Transfer Stubs and StubEntry Fields for Parsed Structured Remittance Data
        /// </summary>        
        public const string REMITTANCE_ORIGINATOR = "remittanceoriginator";
        public const string REMITTANCE_BENEFICIARY = "remittancebeneficiary";
        public const string REMITTANCE_PRIM_DOC_INFO = "primaryremittancedocinfo";
        public const string REMITTANCE_ACT_AMT_PAID = "actualamountpaid";
        public const string REMITTANCE_GROSS_AMT = "grossamount";
        public const string REMITTANCE_DISC_AMT = "discountamount";
        public const string REMITTANCE_ADJ_INFO = "adjustmentinformation";
        public const string REMITTANCE_DATE = "remittancedate";
        public const string REMITTANCE_SEC_DOC_INFO = "secondaryremittancedocinfo";
        public const string REMITTANCE_FREE_TEXT = "remittancefreetext";

        /// <summary>
        /// Wire Transfer Stubs and StubEntry Fields for Parsed unStructured Remittance Data
        /// </summary>
        public const string BPR_MONETARY_AMT = "bprmonetaryamount";
        public const string BPR_ACT_NO = "bpraccountnumber";
        public const string RMR_REF_NO = "rmrreferencenumber";
        public const string RMR_MONETARY_AMT = "rmrmonetaryamount";
        public const string RMR_TOT_INV_AMT = "rmrtotalinvoiceamount";
        public const string RMR_DISC_AMT = "rmrdiscountamount";
        public const string ACCOUNT_NUMBER = "accountnumber";
        public const string ACH_GRID_AMOUNT = "amount";
        public const string ACH_INVOICE_NUMBER= "invoicenumber";
        public const string REASSOCIATION_TRACE_NUMBER = "reassociationtracenumber";



        /// <summary>
        /// Structured Addenda Column name constants
        /// </summary>
        public const string REMITTANCE= "Remittance";
        public const string ORIGINATOR = "Originator";
        public const string BENEFICIARY = "Beneficiary";
        public const string PRIMARY = "Primary";
        public const string DOC_INFO = "Doc Info";
        public const string ACTUAL = "Actual";
        public const string AMT_PAID = "Amount Paid";
        public const string GROSS   = "Gross";
        public const string AMT= "Amount";
        public const string DISCOUNT = "Discount";
        public const string ADJUSTMENT = "Adjustment";
        public const string INFORMATION = "Information";
        public const string DATE = "Date";
        public const string SECONDARY = "Secondary";
        public const string FREE_TEXT = "Free Text";

        /// <summary>
        /// Related addenda Column name constants
        /// </summary>
        public const string RELATED_REMITTANCE_INFO_COL_NM = "Related Remittance Info";        
        /// <summary>
        /// Wire Transfer Stubs and StubEntry Fields for Parsed unStructured Remittance Data
        /// </summary>
        public const string RELATED_REMITTANCE_INFO = "relatedremittanceinfo";

        /// <summary>
        /// Get Parsed Structured remittance Data field names
        /// </summary>
        /// <param name="eTableName"></param>
        /// <param name="alTableFields"></param>
        public static ArrayList GetRemittanceFields(ipoImageCommon.RemittanceType enRemittanceType)
        {
            ArrayList arRemFieldList = new ArrayList();
            switch (enRemittanceType)
            {
                case RemittanceType.STRUCTURED:
                    arRemFieldList.Add(REMITTANCE_ORIGINATOR);
                    arRemFieldList.Add(REMITTANCE_BENEFICIARY);
                    arRemFieldList.Add(REMITTANCE_PRIM_DOC_INFO);
                    arRemFieldList.Add(REMITTANCE_ACT_AMT_PAID);
                    arRemFieldList.Add(REMITTANCE_GROSS_AMT);
                    arRemFieldList.Add(REMITTANCE_DISC_AMT);
                    arRemFieldList.Add(REMITTANCE_ADJ_INFO);
                    arRemFieldList.Add(REMITTANCE_DATE);
                    arRemFieldList.Add(REMITTANCE_SEC_DOC_INFO);
                    arRemFieldList.Add(REMITTANCE_FREE_TEXT);
                    break;
                case RemittanceType.UNSTRUCTURED:
                    arRemFieldList.Add(BPR_MONETARY_AMT);
                    arRemFieldList.Add(BPR_ACT_NO);
                    arRemFieldList.Add(RMR_REF_NO);
                    arRemFieldList.Add(RMR_MONETARY_AMT);
                    arRemFieldList.Add(RMR_TOT_INV_AMT);
                    arRemFieldList.Add(RMR_DISC_AMT);                   
                    break;
                case RemittanceType.RELATED:
                    arRemFieldList.Add(RELATED_REMITTANCE_INFO);                   
                    break;
            }
            return arRemFieldList;
        }


        /// <summary>
        /// Get the field names
        /// </summary>
        /// <param name="eTableName"></param>
        /// <param name="alTableFields"></param>
        public static ArrayList GetFields(TableName eTableName)
        {
            ArrayList arFieldList = new ArrayList();

            switch (eTableName)
            {
                case TableName.CHECK:
                    arFieldList.Add(ipoImageCommon.CHECKS_AMOUNT);
                    arFieldList.Add(ipoImageCommon.CHECKS_REMITTER);
                    arFieldList.Add(ipoImageCommon.CHECKS_SOURCE_PROCESSING_DATE_KEY);
                    arFieldList.Add(ipoImageCommon.CHECKS_DDA);
                    arFieldList.Add(CHECKS_DATA_ENTRY_COMPANY_NAME);
                    arFieldList.Add(CHECKS_DATA_ENTRY_EFFECTIVE_DATE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_RECEIVING_COMPANY);
                    arFieldList.Add(CHECKS_DATA_ENTRY_RECEIVING_INDIVIDUAL);
                    arFieldList.Add(CHECKS_DATA_ENTRY_ACH_BATCH_NUMBER);
                    arFieldList.Add(CHECKS_DATA_ENTRY_COMPANY_ID);
                    arFieldList.Add(CHECKS_DATA_ENTRY_ENTRY_DESCRIPTION);
                    arFieldList.Add(CHECKS_DATA_ENTRY_COMPANY_DATA);
                    arFieldList.Add(CHECKS_DATA_ENTRY_DESCRIPTIVE_DATE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_SETTLEMENT_DATE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_ACCOUNT_NUMBER);
                    arFieldList.Add(CHECKS_DATA_ENTRY_INDIVIDUAL_ID);
                    arFieldList.Add(CHECKS_DATA_ENTRY_TRACE_NUMBER);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_FILE_CREATION_DATE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_FILE_ID);
                    arFieldList.Add(CHECKS_DATA_ENTRY_CUSTOMER_ACCOUNT_NUMBER);
                    arFieldList.Add(CHECKS_DATA_ENTRY_TYPE_CODE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_BANK_REFERENCE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_CUSTOMER_REFERENCE);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BAI_TEXT);
                    arFieldList.Add(CHECKS_DATA_ENTRY_STANDARD_ENTRY_CLASS);
                    arFieldList.Add(CHECKS_DATA_ENTRY_ELECTRONIC_TRANS_CD);
                    arFieldList.Add(CHECKS_DATA_ENTRY_REASSOCIATION_TRACE_NUMBER);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BPR_MONETARY_AMOUNT);
                    arFieldList.Add(CHECKS_DATA_ENTRY_BPR_ACCOUNT_NUMBER);
                    break;
                case TableName.STUBS:
                    arFieldList.Add(ipoImageCommon.STUBS_INVOICE);
                    arFieldList.Add(ipoImageCommon.STUBS_DATA_ENTRY_REMIT_BENEF);
                    break;
            }

            return arFieldList;
        }

        /// <summary>
        /// Get the object type
        /// </summary>
        /// <param name="irRequest"></param>
        /// <returns></returns>
        public static ObjectType GetObjectType(IItem irRequest)
        {
            string strObjectType = string.Empty;
            strObjectType = irRequest.GetType().ToString();
            switch (strObjectType)
            {
                case ("WFS.RecHub.Common.cCheck"):
                    return ObjectType.CHECK;
                case ("WFS.RecHub.Common.cDocument"):
                    return ObjectType.DOCUMENT;
                default:
                    return ObjectType.INVALID;
            }
        }

       
        [Obsolete("This function is being replaced with the extension method 'TryParseToLegalAmount'")]
        /// <summary>
        /// Convert Decimal Money into Text
        /// </summary>
        /// <param name="decAmount"></param>
        /// <returns></returns>
        public static string DecimalToText(decimal decAmount)
        {
            string strAmount = string.Empty;
            char[] amount = decAmount.ToString("F").ToCharArray();
            int places = amount.Length - 3;

            if (amount[0] == '0')
                places = places - 1;

            string strTemp10s;
            string strTemp1000s;
            string strTempMillion;

            switch (places)
            {
                case 0:
                case 1: //dollar
                    strAmount = GetDollars(amount[0]) + GetChange(amount);
                    break;
                case 2: //tens of dollars
                    strTemp10s = (amount[0]).ToString() + (amount[1]).ToString();
                    strAmount = GetTensOfDollars(strTemp10s) +
                        GetChange(amount);
                    break;
                case 3://hundreds of dollars
                    strTemp10s = (amount[1]).ToString() + (amount[2]).ToString();
                    strAmount = GetHundredsOfDollars(amount[0]) +
                        GetTensOfDollars(strTemp10s) +
                        GetChange(amount);
                    break;
                case 4: //thousands of dollars
                    strTemp10s = (amount[2]).ToString() + (amount[3]).ToString();
                    strAmount = GetThousandsOfDollars(amount[0]) +
                        GetHundredsOfDollars(amount[1]) +
                        GetTensOfDollars(strTemp10s) +
                        GetChange(amount);
                    break;
                case 5: //tens of thousands of dollars
                    strTemp1000s = (amount[0]).ToString() + (amount[1]).ToString();
                    strTemp10s = (amount[3]).ToString() + (amount[4]).ToString();
                    strAmount = GetTenThousandsOfDollars(strTemp1000s) +
                        GetHundredsOfDollars(amount[2]) +
                        GetTensOfDollars(strTemp10s) +
                        GetChange(amount);
                    break;
                case 6: //hundreds of thousands of dollars
                    strTemp1000s = (amount[1]).ToString() + (amount[2]).ToString();
                    strTemp10s = (amount[4]).ToString() + (amount[5]).ToString();
                    strAmount = GetHundredThousandsOfDollars(amount[0], amount[1] == '0' && amount[2] == '0') +
                        GetTenThousandsOfDollars(strTemp1000s) +
                        GetHundredsOfDollars(amount[3]) +
                        GetTensOfDollars(strTemp10s) +
                        GetChange(amount);
                    break;
                case 7: //one million dollars
                    strTempMillion = (amount[0]).ToString();
                    strTemp1000s = (amount[2]).ToString() + (amount[3]).ToString();
                    strTemp10s = (amount[5]).ToString() + (amount[6]).ToString();
                    strAmount = GetMillionOfDollars(strTempMillion) +
                        GetHundredThousandsOfDollars(amount[1], amount[2] == '0' && amount[3] == '0') +
                        GetTenThousandsOfDollars(strTemp1000s) +
                        GetHundredsOfDollars(amount[4]) +
                        GetTensOfDollars(strTemp10s) +
                        GetChange(amount);
                    break;
                case 8: //tens of million dollars
                    strTempMillion = (amount[0]).ToString() + (amount[1]).ToString();
                    strTemp1000s = (amount[3]).ToString() + (amount[4]).ToString();
                    strTemp10s = (amount[6]).ToString() + (amount[7]).ToString();
                    strAmount = GetTenMillionOfDollars(strTempMillion) +
                        GetHundredThousandsOfDollars(amount[2], amount[3] == '0' && amount[4] == '0') +
                        GetTenThousandsOfDollars(strTemp1000s) +
                        GetHundredsOfDollars(amount[5]) +
                        GetTensOfDollars(strTemp10s) +
                        GetChange(amount);
                    break;
                case 9:
                    strAmount = "Passed maximum amount";
                    break;
                default:
                    strAmount = "Unknown number submitted";
                    break;
            }

            return strAmount;
        }

        private static string GetChange(char[] amount)
        {
            int index = amount.Length - 1;
            return string.Format("& {0}{1}/100", amount[index - 1], amount[index]);
        }

        private static string GetDollars(char cDollar)
        {
            string strDollars = string.Empty;

            switch (cDollar)
            {
                case '0':
                    strDollars = "Zero ";
                    //strDollars = " ";
                    break;
                case '1':
                    strDollars = "One ";
                    break;
                case '2':
                    strDollars = "Two ";
                    break;
                case '3':
                    strDollars = "Three ";
                    break;
                case '4':
                    strDollars = "Four ";
                    break;
                case '5':
                    strDollars = "Five ";
                    break;
                case '6':
                    strDollars = "Six ";
                    break;
                case '7':
                    strDollars = "Seven ";
                    break;
                case '8':
                    strDollars = "Eight ";
                    break;
                case '9':
                    strDollars = "Nine ";
                    break;
            }

            return strDollars;
        }


        private static string GetTensOfDollars(string cDollar)
        {
            string strDollars = string.Empty;

            switch (cDollar)
            {
                case "00":
                    strDollars = " ";
                    break;
                case "01":
                    strDollars = "One ";
                    break;
                case "02":
                    strDollars = "Two ";
                    break;
                case "03":
                    strDollars = "Three ";
                    break;
                case "04":
                    strDollars = "Four ";
                    break;
                case "05":
                    strDollars = "Five ";
                    break;
                case "06":
                    strDollars = "Six ";
                    break;
                case "07":
                    strDollars = "Seven ";
                    break;
                case "08":
                    strDollars = "Eight ";
                    break;
                case "09":
                    strDollars = "Nine ";
                    break;
                case "10":
                    strDollars = "Ten ";
                    break;
                case "11":
                    strDollars = "Eleven ";
                    break;
                case "12":
                    strDollars = "Twelve ";
                    break;
                case "13":
                    strDollars = "Thirteen ";
                    break;
                case "14":
                    strDollars = "Fourteen ";
                    break;
                case "15":
                    strDollars = "Fifteen ";
                    break;
                case "16":
                    strDollars = "Sixteen ";
                    break;
                case "17":
                    strDollars = "Seventeen ";
                    break;
                case "18":
                    strDollars = "Eighteen ";
                    break;
                case "19":
                    strDollars = "Nineteen ";
                    break;
                case "20":
                    strDollars = "Twenty ";
                    break;
                case "21":
                    strDollars = "Twenty-One ";
                    break;
                case "22":
                    strDollars = "Twenty-Two ";
                    break;
                case "23":
                    strDollars = "Twenty-Three ";
                    break;
                case "24":
                    strDollars = "Twenty-Four ";
                    break;
                case "25":
                    strDollars = "Twenty-Five ";
                    break;
                case "26":
                    strDollars = "Twenty-Six ";
                    break;
                case "27":
                    strDollars = "Twenty-Seven ";
                    break;
                case "28":
                    strDollars = "Twenty-Eight ";
                    break;
                case "29":
                    strDollars = "Twenty-Nine ";
                    break;
                case "30":
                    strDollars = "Thirty ";
                    break;
                case "31":
                    strDollars = "Thirty-One ";
                    break;
                case "32":
                    strDollars = "Thirty-Two ";
                    break;
                case "33":
                    strDollars = "Thirty-Three ";
                    break;
                case "34":
                    strDollars = "Thirty-Four ";
                    break;
                case "35":
                    strDollars = "Thirty-Five ";
                    break;
                case "36":
                    strDollars = "Thirty-Six ";
                    break;
                case "37":
                    strDollars = "Thirty-Seven ";
                    break;
                case "38":
                    strDollars = "Thirty-Eight ";
                    break;
                case "39":
                    strDollars = "Thirty-Nine ";
                    break;
                case "40":
                    strDollars = "Fourty ";
                    break;
                case "41":
                    strDollars = "Fourty-One ";
                    break;
                case "42":
                    strDollars = "Fourty-Two ";
                    break;
                case "43":
                    strDollars = "Fourty-Three ";
                    break;
                case "44":
                    strDollars = "Fourty-Four ";
                    break;
                case "45":
                    strDollars = "Fourty-Five ";
                    break;
                case "46":
                    strDollars = "Fourty-Six ";
                    break;
                case "47":
                    strDollars = "Fourty-Seven ";
                    break;
                case "48":
                    strDollars = "Fourty-Eight ";
                    break;
                case "49":
                    strDollars = "Fourty-Nine ";
                    break;
                case "50":
                    strDollars = "Fifty ";
                    break;
                case "51":
                    strDollars = "Fifty-One ";
                    break;
                case "52":
                    strDollars = "Fifty-Two ";
                    break;
                case "53":
                    strDollars = "Fifty-Three ";
                    break;
                case "54":
                    strDollars = "Fifty-Four ";
                    break;
                case "55":
                    strDollars = "Fifty-Five ";
                    break;
                case "56":
                    strDollars = "Fifty-Six ";
                    break;
                case "57":
                    strDollars = "Fifty-Seven ";
                    break;
                case "58":
                    strDollars = "Fifty-Eight ";
                    break;
                case "59":
                    strDollars = "Fifty-Nine ";
                    break;
                case "60":
                    strDollars = "Sixty ";
                    break;
                case "61":
                    strDollars = "Sixty-One ";
                    break;
                case "62":
                    strDollars = "Sixty-Two ";
                    break;
                case "63":
                    strDollars = "Sixty-Three ";
                    break;
                case "64":
                    strDollars = "Sixty-Four ";
                    break;
                case "65":
                    strDollars = "Sixty-Five ";
                    break;
                case "66":
                    strDollars = "Sixty-Six ";
                    break;
                case "67":
                    strDollars = "Sixty-Seven ";
                    break;
                case "68":
                    strDollars = "Sixty-Eight ";
                    break;
                case "69":
                    strDollars = "Sixty-Nine ";
                    break;
                case "70":
                    strDollars = "Seventy ";
                    break;
                case "71":
                    strDollars = "Seventy-One ";
                    break;
                case "72":
                    strDollars = "Seventy-Two ";
                    break;
                case "73":
                    strDollars = "Seventy-Three ";
                    break;
                case "74":
                    strDollars = "Seventy-Four ";
                    break;
                case "75":
                    strDollars = "Seventy-Five ";
                    break;
                case "76":
                    strDollars = "Seventy-Six ";
                    break;
                case "77":
                    strDollars = "Seventy-Seven ";
                    break;
                case "78":
                    strDollars = "Seventy-Eight ";
                    break;
                case "79":
                    strDollars = "Seventy-Nine ";
                    break;
                case "80":
                    strDollars = "Eighty ";
                    break;
                case "81":
                    strDollars = "Eighty-One ";
                    break;
                case "82":
                    strDollars = "Eighty-Two ";
                    break;
                case "83":
                    strDollars = "Eighty-Three ";
                    break;
                case "84":
                    strDollars = "Eighty-Four ";
                    break;
                case "85":
                    strDollars = "Eighty-Five ";
                    break;
                case "86":
                    strDollars = "Eighty-Six ";
                    break;
                case "87":
                    strDollars = "Eighty-Seven ";
                    break;
                case "88":
                    strDollars = "Eighty-Eight ";
                    break;
                case "89":
                    strDollars = "Eighty-Nine ";
                    break;
                case "90":
                    strDollars = "Ninety ";
                    break;
                case "91":
                    strDollars = "Ninety-One ";
                    break;
                case "92":
                    strDollars = "Ninety-Two ";
                    break;
                case "93":
                    strDollars = "Ninety-Three ";
                    break;
                case "94":
                    strDollars = "Ninety-Four ";
                    break;
                case "95":
                    strDollars = "Ninety-Five ";
                    break;
                case "96":
                    strDollars = "Ninety-Six ";
                    break;
                case "97":
                    strDollars = "Ninety-Seven ";
                    break;
                case "98":
                    strDollars = "Ninety-Eight ";
                    break;
                case "99":
                    strDollars = "Ninety-Nine ";
                    break;
            }

            return strDollars;
        }

        private static string GetHundredsOfDollars(char cDollar)
        {
            string strDollars = string.Empty;

            switch (cDollar)
            {
                case '0':
                    strDollars = " ";
                    break;
                case '1':
                    strDollars = "One Hundred ";
                    break;
                case '2':
                    strDollars = "Two Hundred ";
                    break;
                case '3':
                    strDollars = "Three Hundred ";
                    break;
                case '4':
                    strDollars = "Four Hundred ";
                    break;
                case '5':
                    strDollars = "Five Hundred ";
                    break;
                case '6':
                    strDollars = "Six Hundred ";
                    break;
                case '7':
                    strDollars = "Seven Hundred ";
                    break;
                case '8':
                    strDollars = "Eight Hundred ";
                    break;
                case '9':
                    strDollars = "Nine Hundred ";
                    break;
            }

            return strDollars;
        }

        private static string GetThousandsOfDollars(char cDollar)
        {
            string strDollars = string.Empty;

            switch (cDollar)
            {
                case '0':
                    strDollars = " ";
                    break;
                case '1':
                    strDollars = "One Thousand ";
                    break;
                case '2':
                    strDollars = "Two Thousand ";
                    break;
                case '3':
                    strDollars = "Three Thousand ";
                    break;
                case '4':
                    strDollars = "Four Thousand ";
                    break;
                case '5':
                    strDollars = "Five Thousand ";
                    break;
                case '6':
                    strDollars = "Six Thousand ";
                    break;
                case '7':
                    strDollars = "Seven Thousand ";
                    break;
                case '8':
                    strDollars = "Eight Thousand ";
                    break;
                case '9':
                    strDollars = "Nine Thousand ";
                    break;
            }

            return strDollars;
        }

        private static string GetTenThousandsOfDollars(string cDollar)
        {
            string strDollars = string.Empty;

            switch (cDollar)
            {
                case "00":
                    strDollars = " ";
                    break;
                case "01":
                    strDollars = "One Thousand ";
                    break;
                case "02":
                    strDollars = "Two Thousand ";
                    break;
                case "03":
                    strDollars = "Three Thousand ";
                    break;
                case "04":
                    strDollars = "Four Thousand ";
                    break;
                case "05":
                    strDollars = "Five Thousand ";
                    break;
                case "06":
                    strDollars = "Six Thousand ";
                    break;
                case "07":
                    strDollars = "Seven Thousand ";
                    break;
                case "08":
                    strDollars = "Eight Thousand ";
                    break;
                case "09":
                    strDollars = "Nine Thousand ";
                    break;
                case "10":
                    strDollars = "Ten Thousand ";
                    break;
                case "11":
                    strDollars = "Eleven Thousand ";
                    break;
                case "12":
                    strDollars = "Twelve Thousand ";
                    break;
                case "13":
                    strDollars = "Thirteen Thousand ";
                    break;
                case "14":
                    strDollars = "Fourteen Thousand ";
                    break;
                case "15":
                    strDollars = "Fifteen Thousand ";
                    break;
                case "16":
                    strDollars = "Sixteen Thousand ";
                    break;
                case "17":
                    strDollars = "Seventeen Thousand ";
                    break;
                case "18":
                    strDollars = "Eighteen Thousand ";
                    break;
                case "19":
                    strDollars = "Nineteen Thousand ";
                    break;
                case "20":
                    strDollars = "Twenty Thousand ";
                    break;
                case "21":
                    strDollars = "Twenty-One Thousand ";
                    break;
                case "22":
                    strDollars = "Twenty-Two Thousand ";
                    break;
                case "23":
                    strDollars = "Twenty-Three Thousand ";
                    break;
                case "24":
                    strDollars = "Twenty-Four Thousand ";
                    break;
                case "25":
                    strDollars = "Twenty-Five Thousand ";
                    break;
                case "26":
                    strDollars = "Twenty-Six Thousand ";
                    break;
                case "27":
                    strDollars = "Twenty-Seven Thousand ";
                    break;
                case "28":
                    strDollars = "Twenty-Eight Thousand ";
                    break;
                case "29":
                    strDollars = "Twenty-Nine Thousand ";
                    break;
                case "30":
                    strDollars = "Thirty Thousand ";
                    break;
                case "31":
                    strDollars = "Thirty-One Thousand ";
                    break;
                case "32":
                    strDollars = "Thirty-Two Thousand ";
                    break;
                case "33":
                    strDollars = "Thirty-Three Thousand ";
                    break;
                case "34":
                    strDollars = "Thirty-Four Thousand ";
                    break;
                case "35":
                    strDollars = "Thirty-Five Thousand ";
                    break;
                case "36":
                    strDollars = "Thirty-Six Thousand ";
                    break;
                case "37":
                    strDollars = "Thirty-Seven Thousand ";
                    break;
                case "38":
                    strDollars = "Thirty-Eight Thousand ";
                    break;
                case "39":
                    strDollars = "Thirty-Nine Thousand ";
                    break;
                case "40":
                    strDollars = "Fourty Thousand ";
                    break;
                case "41":
                    strDollars = "Fourty-One Thousand ";
                    break;
                case "42":
                    strDollars = "Fourty-Two Thousand ";
                    break;
                case "43":
                    strDollars = "Fourty-Three Thousand ";
                    break;
                case "44":
                    strDollars = "Fourty-Four Thousand ";
                    break;
                case "45":
                    strDollars = "Fourty-Five Thousand ";
                    break;
                case "46":
                    strDollars = "Fourty-Six Thousand ";
                    break;
                case "47":
                    strDollars = "Fourty-Seven Thousand ";
                    break;
                case "48":
                    strDollars = "Fourty-Eight Thousand ";
                    break;
                case "49":
                    strDollars = "Fourty-Nine Thousand ";
                    break;
                case "50":
                    strDollars = "Fifty Thousand ";
                    break;
                case "51":
                    strDollars = "Fifty-One Thousand ";
                    break;
                case "52":
                    strDollars = "Fifty-Two Thousand ";
                    break;
                case "53":
                    strDollars = "Fifty-Three Thousand ";
                    break;
                case "54":
                    strDollars = "Fifty-Four Thousand ";
                    break;
                case "55":
                    strDollars = "Fifty-Five Thousand ";
                    break;
                case "56":
                    strDollars = "Fifty-Six Thousand ";
                    break;
                case "57":
                    strDollars = "Fifty-Seven Thousand ";
                    break;
                case "58":
                    strDollars = "Fifty-Eight Thousand ";
                    break;
                case "59":
                    strDollars = "Fifty-Nine Thousand ";
                    break;
                case "60":
                    strDollars = "Sixty Thousand ";
                    break;
                case "61":
                    strDollars = "Sixty-One Thousand ";
                    break;
                case "62":
                    strDollars = "Sixty-Two Thousand ";
                    break;
                case "63":
                    strDollars = "Sixty-Three Thousand ";
                    break;
                case "64":
                    strDollars = "Sixty-Four Thousand ";
                    break;
                case "65":
                    strDollars = "Sixty-Five Thousand ";
                    break;
                case "66":
                    strDollars = "Sixty-Six Thousand ";
                    break;
                case "67":
                    strDollars = "Sixty-Seven Thousand ";
                    break;
                case "68":
                    strDollars = "Sixty-Eight Thousand ";
                    break;
                case "69":
                    strDollars = "Sixty-Nine Thousand ";
                    break;
                case "70":
                    strDollars = "Seventy Thousand ";
                    break;
                case "71":
                    strDollars = "Seventy-One Thousand ";
                    break;
                case "72":
                    strDollars = "Seventy-Two Thousand ";
                    break;
                case "73":
                    strDollars = "Seventy-Three Thousand ";
                    break;
                case "74":
                    strDollars = "Seventy-Four Thousand ";
                    break;
                case "75":
                    strDollars = "Seventy-Five Thousand ";
                    break;
                case "76":
                    strDollars = "Seventy-Six Thousand ";
                    break;
                case "77":
                    strDollars = "Seventy-Seven Thousand ";
                    break;
                case "78":
                    strDollars = "Seventy-Eight Thousand ";
                    break;
                case "79":
                    strDollars = "Seventy-Nine Thousand ";
                    break;
                case "80":
                    strDollars = "Eighty Thousand ";
                    break;
                case "81":
                    strDollars = "Eighty-One Thousand ";
                    break;
                case "82":
                    strDollars = "Eighty-Two Thousand ";
                    break;
                case "83":
                    strDollars = "Eighty-Three Thousand ";
                    break;
                case "84":
                    strDollars = "Eighty-Four Thousand ";
                    break;
                case "85":
                    strDollars = "Eighty-Five Thousand ";
                    break;
                case "86":
                    strDollars = "Eighty-Six Thousand ";
                    break;
                case "87":
                    strDollars = "Eighty-Seven Thousand ";
                    break;
                case "88":
                    strDollars = "Eighty-Eight Thousand ";
                    break;
                case "89":
                    strDollars = "Eighty-Nine Thousand ";
                    break;
                case "90":
                    strDollars = "Ninety Thousand ";
                    break;
                case "91":
                    strDollars = "Ninety-One Thousand ";
                    break;
                case "92":
                    strDollars = "Ninety-Two Thousand ";
                    break;
                case "93":
                    strDollars = "Ninety-Three Thousand ";
                    break;
                case "94":
                    strDollars = "Ninety-Four Thousand ";
                    break;
                case "95":
                    strDollars = "Ninety-Five Thousand ";
                    break;
                case "96":
                    strDollars = "Ninety-Six Thousand ";
                    break;
                case "97":
                    strDollars = "Ninety-Seven Thousand ";
                    break;
                case "98":
                    strDollars = "Ninety-Eight Thousand ";
                    break;
                case "99":
                    strDollars = "Ninety-Nine Thousand ";
                    break;
            }

            return strDollars;
        }

        private static string GetHundredThousandsOfDollars(char cDollar, bool evenMoney)
        {
            string strDollars = string.Empty;

            switch (cDollar)
            {
                case '0':
                    strDollars = " ";
                    break;
                case '1':
                    if (evenMoney)
                        strDollars = "One Hundred Thousand ";
                    else
                        strDollars = "One Hundred ";
                    break;
                case '2':
                    if (evenMoney)
                        strDollars = strDollars = "Two Hundred Thousand ";
                    else
                        strDollars = "Two Hundred ";
                    break;
                case '3':
                    if (evenMoney)
                        strDollars = "Three Hundred Thousand ";
                    else
                        strDollars = "Three Hundred ";
                    break;
                case '4':
                    if (evenMoney)
                        strDollars = "Four Hundred Thousand ";
                    else
                        strDollars = "Four Hundred ";
                    break;
                case '5':
                    if (evenMoney)
                        strDollars = "Five Hundred Thousand ";
                    else
                        strDollars = "Five Hundred ";
                    break;
                case '6':
                    if (evenMoney)
                        strDollars = "Six Hundred Thousand ";
                    else
                        strDollars = "Six Hundred ";
                    break;
                case '7':
                    if (evenMoney)
                        strDollars = "Seven Hundred Thousand ";
                    else
                        strDollars = "Seven Hundred ";
                    break;
                case '8':
                    if (evenMoney)
                        strDollars = "Eight Hundred Thousand ";
                    else
                        strDollars = "Eight Hundred ";
                    break;
                case '9':
                    if (evenMoney)
                        strDollars = "Nine Hundred Thousand ";
                    else
                        strDollars = "Nine Hundred ";
                    break;
            }

            return strDollars;
        }

        private static string GetMillionOfDollars(string cDollar)
        {
            string strDollars = string.Empty;

            switch (cDollar)
            {
                case "0":
                    strDollars = " ";
                    break;
                case "1":
                    strDollars = "One Million ";
                    break;
                case "2":
                    strDollars = "Two Million ";
                    break;
                case "3":
                    strDollars = "Three Million ";
                    break;
                case "4":
                    strDollars = "Four Million ";
                    break;
                case "5":
                    strDollars = "Five Million ";
                    break;
                case "6":
                    strDollars = "Six Million ";
                    break;
                case "7":
                    strDollars = "Seven Million ";
                    break;
                case "8":
                    strDollars = "Eight Million ";
                    break;
                case "9":
                    strDollars = "Nine Million ";
                    break;
            }

            return strDollars;
        }

        private static string GetTenMillionOfDollars(string cDollar)
        {
            string strDollars = string.Empty;

            switch (cDollar)
            {
                case "00":
                    strDollars = " ";
                    break;
                case "01":
                    strDollars = "One Million ";
                    break;
                case "02":
                    strDollars = "Two Million ";
                    break;
                case "03":
                    strDollars = "Three Million ";
                    break;
                case "04":
                    strDollars = "Four Million ";
                    break;
                case "05":
                    strDollars = "Five Million ";
                    break;
                case "06":
                    strDollars = "Six Million ";
                    break;
                case "07":
                    strDollars = "Seven Million ";
                    break;
                case "08":
                    strDollars = "Eight Million ";
                    break;
                case "09":
                    strDollars = "Nine Million ";
                    break;
                case "10":
                    strDollars = "Ten Million ";
                    break;
                case "11":
                    strDollars = "Eleven Million ";
                    break;
                case "12":
                    strDollars = "Twelve Million ";
                    break;
                case "13":
                    strDollars = "Thirteen Million ";
                    break;
                case "14":
                    strDollars = "Fourteen Million ";
                    break;
                case "15":
                    strDollars = "Fifteen Million ";
                    break;
                case "16":
                    strDollars = "Sixteen Million ";
                    break;
                case "17":
                    strDollars = "Seventeen Million ";
                    break;
                case "18":
                    strDollars = "Eighteen Million ";
                    break;
                case "19":
                    strDollars = "Nineteen Million ";
                    break;
                case "20":
                    strDollars = "Twenty Million ";
                    break;
                case "21":
                    strDollars = "Twenty-One Million ";
                    break;
                case "22":
                    strDollars = "Twenty-Two Million ";
                    break;
                case "23":
                    strDollars = "Twenty-Three Million ";
                    break;
                case "24":
                    strDollars = "Twenty-Four Million ";
                    break;
                case "25":
                    strDollars = "Twenty-Five Million ";
                    break;
                case "26":
                    strDollars = "Twenty-Six Million ";
                    break;
                case "27":
                    strDollars = "Twenty-Seven Million ";
                    break;
                case "28":
                    strDollars = "Twenty-Eight Million ";
                    break;
                case "29":
                    strDollars = "Twenty-Nine Million ";
                    break;
                case "30":
                    strDollars = "Thirty Million ";
                    break;
                case "31":
                    strDollars = "Thirty-One Million ";
                    break;
                case "32":
                    strDollars = "Thirty-Two Million ";
                    break;
                case "33":
                    strDollars = "Thirty-Three Million ";
                    break;
                case "34":
                    strDollars = "Thirty-Four Million ";
                    break;
                case "35":
                    strDollars = "Thirty-Five Million ";
                    break;
                case "36":
                    strDollars = "Thirty-Six Million ";
                    break;
                case "37":
                    strDollars = "Thirty-Seven Million ";
                    break;
                case "38":
                    strDollars = "Thirty-Eight Million ";
                    break;
                case "39":
                    strDollars = "Thirty-Nine Million ";
                    break;
                case "40":
                    strDollars = "Fourty Million ";
                    break;
                case "41":
                    strDollars = "Fourty-One Million ";
                    break;
                case "42":
                    strDollars = "Fourty-Two Million ";
                    break;
                case "43":
                    strDollars = "Fourty-Three Million ";
                    break;
                case "44":
                    strDollars = "Fourty-Four Million ";
                    break;
                case "45":
                    strDollars = "Fourty-Five Million ";
                    break;
                case "46":
                    strDollars = "Fourty-Six Million ";
                    break;
                case "47":
                    strDollars = "Fourty-Seven Million ";
                    break;
                case "48":
                    strDollars = "Fourty-Eight Million ";
                    break;
                case "49":
                    strDollars = "Fourty-Nine Million ";
                    break;
                case "50":
                    strDollars = "Fifty Million ";
                    break;
                case "51":
                    strDollars = "Fifty-One Million ";
                    break;
                case "52":
                    strDollars = "Fifty-Two Million ";
                    break;
                case "53":
                    strDollars = "Fifty-Three Million ";
                    break;
                case "54":
                    strDollars = "Fifty-Four Million ";
                    break;
                case "55":
                    strDollars = "Fifty-Five Million ";
                    break;
                case "56":
                    strDollars = "Fifty-Six Million ";
                    break;
                case "57":
                    strDollars = "Fifty-Seven Million ";
                    break;
                case "58":
                    strDollars = "Fifty-Eight Million ";
                    break;
                case "59":
                    strDollars = "Fifty-Nine Million ";
                    break;
                case "60":
                    strDollars = "Sixty Million ";
                    break;
                case "61":
                    strDollars = "Sixty-One Million ";
                    break;
                case "62":
                    strDollars = "Sixty-Two Million ";
                    break;
                case "63":
                    strDollars = "Sixty-Three Million ";
                    break;
                case "64":
                    strDollars = "Sixty-Four Million ";
                    break;
                case "65":
                    strDollars = "Sixty-Five Million ";
                    break;
                case "66":
                    strDollars = "Sixty-Six Million ";
                    break;
                case "67":
                    strDollars = "Sixty-Seven Million ";
                    break;
                case "68":
                    strDollars = "Sixty-Eight Million ";
                    break;
                case "69":
                    strDollars = "Sixty-Nine Million ";
                    break;
                case "70":
                    strDollars = "Seventy Million ";
                    break;
                case "71":
                    strDollars = "Seventy-One Million ";
                    break;
                case "72":
                    strDollars = "Seventy-Two Million ";
                    break;
                case "73":
                    strDollars = "Seventy-Three Million ";
                    break;
                case "74":
                    strDollars = "Seventy-Four Million ";
                    break;
                case "75":
                    strDollars = "Seventy-Five Million ";
                    break;
                case "76":
                    strDollars = "Seventy-Six Million ";
                    break;
                case "77":
                    strDollars = "Seventy-Seven Million ";
                    break;
                case "78":
                    strDollars = "Seventy-Eight Million ";
                    break;
                case "79":
                    strDollars = "Seventy-Nine Million ";
                    break;
                case "80":
                    strDollars = "Eighty Million ";
                    break;
                case "81":
                    strDollars = "Eighty-One Million ";
                    break;
                case "82":
                    strDollars = "Eighty-Two Million ";
                    break;
                case "83":
                    strDollars = "Eighty-Three Million ";
                    break;
                case "84":
                    strDollars = "Eighty-Four Million ";
                    break;
                case "85":
                    strDollars = "Eighty-Five Million ";
                    break;
                case "86":
                    strDollars = "Eighty-Six Million ";
                    break;
                case "87":
                    strDollars = "Eighty-Seven Million ";
                    break;
                case "88":
                    strDollars = "Eighty-Eight Million ";
                    break;
                case "89":
                    strDollars = "Eighty-Nine Million ";
                    break;
                case "90":
                    strDollars = "Ninety Million ";
                    break;
                case "91":
                    strDollars = "Ninety-One Million ";
                    break;
                case "92":
                    strDollars = "Ninety-Two Million ";
                    break;
                case "93":
                    strDollars = "Ninety-Three Million ";
                    break;
                case "94":
                    strDollars = "Ninety-Four Million ";
                    break;
                case "95":
                    strDollars = "Ninety-Five Million ";
                    break;
                case "96":
                    strDollars = "Ninety-Six Million ";
                    break;
                case "97":
                    strDollars = "Ninety-Seven Million ";
                    break;
                case "98":
                    strDollars = "Ninety-Eight Million ";
                    break;
                case "99":
                    strDollars = "Ninety-Nine Million ";
                    break;
            }

            return strDollars;
        }
    }
}
