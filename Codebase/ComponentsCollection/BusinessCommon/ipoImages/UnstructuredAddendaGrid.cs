﻿using System;
using System.Collections;
using System.Drawing;
using WFS.RecHub.Common.Log;

namespace WFS.RecHub.Common
{
    public class UnstructuredAddendaGrid : IDisposable
    {
        private const int XMargin = 25;

        private readonly SolidBrush _brushBlack;
        private readonly cEventLog _eventLog;
        private readonly Font _fntEntryData;
        private readonly Font _fntEntryHeader;
        private readonly Graphics _graphics;
        private readonly int _iColWidth;
        private readonly int _iHdrFntHeight;
        private readonly int _iWidth;
        private readonly int _iYAxis;
        private readonly cParsedAddendaCollection _parsedAddendaCollection;
        private readonly Pen _penBlack;

        private UnstructuredAddendaGrid(cImageData objImageData, int iYAxis, int iWidth,
            Graphics graphics, cEventLog eventLog)
        {
            _iYAxis = iYAxis;
            _iWidth = iWidth;
            _graphics = graphics;
            _eventLog = eventLog;
            _parsedAddendaCollection = objImageData.ParsedAddendas;

            _iColWidth = (_iWidth - (2 * XMargin)) / 10;

            _fntEntryHeader = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Bold);
            _iHdrFntHeight = Convert.ToInt32(graphics.MeasureString("Ag", _fntEntryHeader).Height) + 1;

            _fntEntryData = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Regular);
            _brushBlack = new SolidBrush(Color.Black);
            _penBlack = new Pen(Color.Black, 3);
        }
        public void Dispose()
        {
            _penBlack.Dispose();
            _brushBlack.Dispose();
            _fntEntryData.Dispose();
            _fntEntryHeader.Dispose();
        }

        public static void Draw(cImageData objImageData, int iYAxis, int iWidth, Graphics graphics, cEventLog eventLog)
        {
            using (var instance = new UnstructuredAddendaGrid(objImageData, iYAxis, iWidth, graphics, eventLog))
            {
                instance.DrawAll();
            }
        }
        private void DrawAll()
        {
            int y = _iYAxis;
            int iDataFntHeight = Convert.ToInt32(_graphics.MeasureString("Ag", _fntEntryData).Height) + 1;
            y += (2 * _iHdrFntHeight);
            try
            {
                _graphics.DrawRectangle(_penBlack, XMargin, y, _iWidth - 50, _iHdrFntHeight * 3);
                for (int i = 1; i < 10; i++)
                {
                    _graphics.DrawLine(_penBlack, XMargin + (i * _iColWidth), y, XMargin + (i * _iColWidth), y + (3 * _iHdrFntHeight));
                }

                //Put Grid Columns
                //BPR_MONETARY
                DrawHeaderLines(y, 0,
                    ipoImageCommon.ADDENDA_BPR, ipoImageCommon.ADDENDA_MONE, ipoImageCommon.ADDENDA_AMT);

                //BPR_Account Number
                DrawHeaderLines(y, 1,
                    ipoImageCommon.ADDENDA_BPR, ipoImageCommon.ADDENDA_ACCOUNT, ipoImageCommon.ADDENDA_NUM);

                //RMR Reference Number
                DrawHeaderLines(y, 2,
                    ipoImageCommon.ADDENDA_RMR, ipoImageCommon.ADDENDA_REF, ipoImageCommon.ADDENDA_NUM);

                //RMR Monetary Amount
                DrawHeaderLines(y, 3,
                    ipoImageCommon.ADDENDA_RMR, ipoImageCommon.ADDENDA_MONE, ipoImageCommon.ADDENDA_AMT);

                //RMR Total Invoice Amount
                DrawHeaderLines(y, 4,
                    ipoImageCommon.ADDENDA_RMR_TOT, ipoImageCommon.ADDENDA_INVOICE, ipoImageCommon.ADDENDA_AMT);

                //RMR Discount Amount
                DrawHeaderLines(y, 5,
                    ipoImageCommon.ADDENDA_RMR, ipoImageCommon.ADDENDA_DISC, ipoImageCommon.ADDENDA_AMT);

                //Amount
                DrawHeaderLines(y, 6, ipoImageCommon.ADDENDA_AMT);

                //Account number
                DrawHeaderLines(y, 7, ipoImageCommon.ADDENDA_ACCOUNT, ipoImageCommon.ADDENDA_NUM);

                //Invoice number
                DrawHeaderLines(y, 8, ipoImageCommon.ADDENDA_INVOICE, ipoImageCommon.ADDENDA_NUM);

                //Trace number
                DrawHeaderLines(y, 9, ipoImageCommon.REASSOCIATION, ipoImageCommon.TRACE, ipoImageCommon.ADDENDA_NUM);

                y += (3 * _iHdrFntHeight);

                ArrayList arlBatchSequence = _parsedAddendaCollection.GetDistinctBatchSequence();
                arlBatchSequence.Sort();
                foreach (int iBatchSequence in arlBatchSequence)
                {
                    //Put data in grid
                    _graphics.DrawRectangle(_penBlack, 25, y, _iWidth - 50, _iHdrFntHeight * 2);
                    for (int i = 1; i < 10; i++)
                        _graphics.DrawLine(_penBlack, XMargin + (i * _iColWidth), y, XMargin + (i * _iColWidth), y + (2 * _iHdrFntHeight));

                    string sText = string.Empty;
                    foreach (cParsedAddenda objParsedAddenda in _parsedAddendaCollection)
                    {
                        if (objParsedAddenda.BatchSequence == iBatchSequence)
                        {
                            switch (objParsedAddenda.FieldName.ToLower())
                            {
                                //BPR Monetary Amount Value
                                case ipoImageCommon.BPR_MONETARY_AMT:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                    {
                                        sText = Convert.ToDecimal(objParsedAddenda.FieldValue).ToString("C");
                                        _graphics.DrawString(sText, _fntEntryData, _brushBlack, new PointF(XMargin + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(sText, _fntEntryData).Width)) / 2), y));
                                    }
                                    break;

                                //BPR Account Number Value
                                case ipoImageCommon.BPR_ACT_NO:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                        _graphics.DrawString(objParsedAddenda.FieldValue, _fntEntryData, _brushBlack, new PointF(XMargin + _iColWidth + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(objParsedAddenda.FieldValue, _fntEntryData).Width)) / 2), y));
                                    break;

                                //RMR Reference Number Value
                                case ipoImageCommon.RMR_REF_NO:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                        _graphics.DrawString(objParsedAddenda.FieldValue, _fntEntryData, _brushBlack, new PointF(XMargin + (2 * _iColWidth) + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(objParsedAddenda.FieldValue, _fntEntryData).Width)) / 2), y));
                                    break;

                                //RMR Monetary Amount Value
                                case ipoImageCommon.RMR_MONETARY_AMT:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                    {
                                        sText = Convert.ToDecimal(objParsedAddenda.FieldValue).ToString("C");
                                        _graphics.DrawString(sText, _fntEntryData, _brushBlack, new PointF(XMargin + (3 * _iColWidth) + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(sText, _fntEntryData).Width)) / 2), y));
                                    }
                                    break;

                                //RMR Total Invoice Amount Value
                                case ipoImageCommon.RMR_TOT_INV_AMT:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                    {
                                        sText = Convert.ToDecimal(objParsedAddenda.FieldValue).ToString("C");
                                        _graphics.DrawString(sText, _fntEntryData, _brushBlack, new PointF(XMargin + (4 * _iColWidth) + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(sText, _fntEntryData).Width)) / 2), y));
                                    }
                                    break;

                                //RMR Discount Amount
                                case ipoImageCommon.RMR_DISC_AMT:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                    {
                                        sText = Convert.ToDecimal(objParsedAddenda.FieldValue).ToString("C");
                                        _graphics.DrawString(sText, _fntEntryData, _brushBlack, new PointF(XMargin + (5 * _iColWidth) + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(sText, _fntEntryData).Width)) / 2), y));
                                    }
                                    break;

                                //Amount
                                case ipoImageCommon.ACH_GRID_AMOUNT:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                    {
                                        sText = Convert.ToDecimal(objParsedAddenda.FieldValue).ToString("C");
                                        _graphics.DrawString(sText, _fntEntryData, _brushBlack, new PointF(XMargin + (6 * _iColWidth) + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(sText, _fntEntryData).Width)) / 2), y));
                                    }
                                    break;

                                //Account number
                                case ipoImageCommon.ACCOUNT_NUMBER:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                        _graphics.DrawString(objParsedAddenda.FieldValue, _fntEntryData, _brushBlack, new PointF(XMargin + (7 * _iColWidth) + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(objParsedAddenda.FieldValue, _fntEntryData).Width)) / 2), y));
                                    break;

                                //Invoice Number
                                case ipoImageCommon.ACH_INVOICE_NUMBER:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                        _graphics.DrawString(objParsedAddenda.FieldValue, _fntEntryData, _brushBlack, new PointF(XMargin + (8 * _iColWidth) + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(objParsedAddenda.FieldValue, _fntEntryData).Width)) / 2), y));
                                    break;

                                //Reassociation Trace Number
                                case ipoImageCommon.REASSOCIATION_TRACE_NUMBER:
                                    if (!String.IsNullOrWhiteSpace(objParsedAddenda.FieldValue))
                                        _graphics.DrawString(objParsedAddenda.FieldValue, _fntEntryData, _brushBlack, new PointF(XMargin + (9 * _iColWidth) + ((_iColWidth - Convert.ToInt32(_graphics.MeasureString(objParsedAddenda.FieldValue, _fntEntryData).Width)) / 2), y));

                                    break;
                            }
                        }
                    }
                    y += iDataFntHeight * 2;
                }
            }
            catch (Exception ex)
            {
                _eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
        }
        private void DrawHeaderLines(int startY, int columnIndex, params string[] lines)
        {
            var currentY = startY;
            foreach (var line in lines)
            {
                DrawHeaderLine(currentY, columnIndex, line);
                currentY += _iHdrFntHeight;
            }
        }
        private void DrawHeaderLine(int y, int columnIndex, string text)
        {
            var textWidth = Convert.ToInt32(_graphics.MeasureString(text, _fntEntryHeader).Width);
            var point = new PointF(XMargin + (_iColWidth * columnIndex) + ((_iColWidth - textWidth) / 2), y);
            _graphics.DrawString(text, _fntEntryHeader, _brushBlack, point);
        }
    }
}