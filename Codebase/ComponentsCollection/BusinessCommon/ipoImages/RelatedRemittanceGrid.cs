﻿using System;
using System.Collections;
using System.Drawing;
using WFS.RecHub.Common.Log;
using System.Linq;

namespace WFS.RecHub.Common
{
    public class RelatedRemittanceGrid
    {
        public static void Draw(cImageData objImageData, int iYAxis, Graphics grfx, cEventLog eventLog)
        {
            int x = 25;
            int y = iYAxis;
            int iHdrFntHeight = 63;

            using (Font fntEntryHeader = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Bold))
            using (Font fntEntryData = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Regular))
            using (SolidBrush brushBlack = new SolidBrush(Color.Black))
            using (Pen penBlack = new Pen(Color.Black, 3))
            {
                int iDataFntHeight = Convert.ToInt32(grfx.MeasureString("Ag", fntEntryData).Height) + 1;
                y += (2 * iHdrFntHeight);
                try
                {
                    int iXMargin = 25;

                    ArrayList arlBatchSequence = objImageData.ParsedAddendas.GetDistinctBatchSequence();
                    var addendas = objImageData.ParsedAddendas.Cast<cParsedAddenda>();

                    foreach (int iBatchSequence in arlBatchSequence)
                    {
                        var addenda = addendas.FirstOrDefault(ad => ad.BatchSequence == iBatchSequence);
                        if (addenda == null)
                            continue;

                        if ((addenda.FieldName.ToLower() == ipoImageCommon.RELATED_REMITTANCE_INFO) &&
                            (!String.IsNullOrWhiteSpace(addenda.FieldValue)))
                            grfx.DrawString(addenda.FieldValue, fntEntryData, brushBlack, new PointF(iXMargin, y));
                        y += iDataFntHeight * 2;
                    }
                }
                catch (Exception ex)
                {
                    eventLog.logError(ex);
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}