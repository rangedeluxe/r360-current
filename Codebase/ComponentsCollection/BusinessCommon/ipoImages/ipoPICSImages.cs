﻿using System;
using System.IO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose: ipoPICImages contains PICs specific getrelativeimages and putbatchimage.
*
* Modification History
* CR 32562 JMC 03/02/2011 
*    -Modified getImagePath() method to accept IItem interface instead of cImage object.
*    -Modified getRelativeImagePath() method to accept IItem interface instead of cImage object.
*    -Modified GetImage() method to accept IItem interface instead of cImage object.
* CR 50214 WJS 2/13/2012
*   - Added support for PutBatchImages
*   -Change putbatchImage to a single image
* CR 52959 JNE 10/02/2012
*   -Moved common code to base class _ipoPICSImages
* WI 90246 CRG 03/18/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoImages
* WI 117833 JMC 10/18/2013
*   -Modified ipoImages and OLFServicesAPI to accept empty File Descriptors.
* WI 123215 MLH 11/21/2013
*   - Modified getRelativeImagePath to cut off the "root" path, it had been returning full path    
* WI 156594 SAS 07/31/2014 
*   - Added Batch Source Short Name to the image relative path.        
* WI  166623  SAS 09/17/2014 
*   - Changes done to retrieve Image Path
* WI  173874  SAS 10/23/2014 
*   - Changes done to ignore error if path is not found for ACH/Wire
* WI 175199 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName   
* WI 176352 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName        
******************************************************************************/
namespace WFS.RecHub.Common
{
    public class ipoPICSImages : _ipoPICSImages
    {

        public ipoPICSImages(string sImageRootPath): base(sImageRootPath)
        {
            m_sImageRootPath = sImageRootPath;
        }

        /// <summary>
        /// WI 156594 SAS 07/31/2014  Changes done to add source name in relative path
        /// </summary>
        /// <param name="picsDate"></param>
        /// <param name="bankID"></param>
        /// <param name="lockboxID"></param>
        /// <param name="batchID"></param>
        /// <param name="batchSequence"></param>
        /// <param name="fileDescriptor"></param>
        /// <param name="workgroupColorMode"></param>
        /// <param name="page"></param>
        /// <param name="sourceBatchID"></param>
        /// <param name="batchSourceShortName"></param>
        /// <param name="importTypeShortName"></param>
        /// <returns></returns>
        public override string getRelativeImagePath(string picsDate,
                                           int bankID,
                                           int lockboxID,
                                           long batchID,
                                           int batchSequence,
                                           string fileDescriptor,
                                           WorkgroupColorMode workgroupColorMode,
                                           int page,
                                           long sourceBatchID,
                                           string batchSourceShortName,
                                           string importTypeShortName)
        {

            string strImageFolderPath=string.Empty;
            string strImageType = string.Empty;
            string strRetVal = string.Empty;
            string searchPattern = string.Empty;
            string[] arFiles;            
            try
            {
                eventLog.logEvent("getRelativeImagePath:  m_sImageRootPath=" + m_sImageRootPath, MessageImportance.Debug);

                strImageFolderPath = Path.Combine(strImageFolderPath, m_sImageRootPath);

                // Construct path for image
                if (pathNeedsPaymentSource(importTypeShortName))
                {
                    if (!String.IsNullOrEmpty(batchSourceShortName) && batchSourceShortName.ToString().ToLower() != "null")
                    {
                        strImageFolderPath = Path.Combine(strImageFolderPath, batchSourceShortName);
                    }
                }


                // Construct full path for image, then tack on a search pattern for Directory.GetFiles
                // Return the first existing file, minus the imageRootPath
                strImageFolderPath = Path.Combine(strImageFolderPath,picsDate);
                strImageFolderPath = Path.Combine(strImageFolderPath, bankID.ToString());
                strImageFolderPath = Path.Combine(strImageFolderPath, lockboxID.ToString());
                searchPattern = sourceBatchID.ToString() + "_"
                    + batchSequence.ToString() + "_"
                    + (string.IsNullOrEmpty(fileDescriptor) ? "*" : fileDescriptor) + "_";

                //Check if directory does not exist and it is an ACH/Wire Transfer then no need to check for image. since it needs to be generated on a fly
                if (!Directory.Exists(strImageFolderPath) && ISACHOrWIRE(bankID, lockboxID, picsDate, batchID))
                {
                    strRetVal = string.Empty;
                }
                else
                {
                    // Check for file existence using the Image "Root" path + relative path                
                    switch (workgroupColorMode)
                    {

                        case WorkgroupColorMode.COLOR_MODE_BITONAL:
                            strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_BITONAL, page);
                            arFiles = Directory.GetFiles(strImageFolderPath, searchPattern + strImageType + ".tif");
                            if (arFiles.Length > 0)
                            {
                                strRetVal = arFiles[0];
                            }
                            else
                            {
                                strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_GRAYSCALE, page);
                                arFiles = Directory.GetFiles(strImageFolderPath, searchPattern + strImageType + ".tif");
                                if (arFiles.Length > 0)
                                {
                                    strRetVal = arFiles[0];
                                }
                                else
                                {
                                    strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_COLOR, page);
                                    arFiles = Directory.GetFiles(strImageFolderPath, searchPattern + strImageType + ".tif");
                                    if (arFiles.Length > 0)
                                    {
                                        strRetVal = arFiles[0];
                                    }
                                    else
                                    {
                                        strRetVal = "";
                                    }
                                }
                            }
                            break;
                        case WorkgroupColorMode.COLOR_MODE_COLOR:
                            strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_COLOR, page);
                            arFiles = Directory.GetFiles(strImageFolderPath, searchPattern + strImageType + ".tif");
                            if (arFiles.Length > 0)
                            {
                                strRetVal = arFiles[0];
                            }
                            else
                            {
                                strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_GRAYSCALE, page);
                                arFiles = Directory.GetFiles(strImageFolderPath, searchPattern + strImageType + ".tif");
                                if (arFiles.Length > 0)
                                {
                                    strRetVal = arFiles[0];
                                }
                                else
                                {
                                    strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_BITONAL, page);
                                    arFiles = Directory.GetFiles(strImageFolderPath, searchPattern + strImageType + ".tif");
                                    if (arFiles.Length > 0)
                                    {
                                        strRetVal = arFiles[0];
                                    }
                                    else
                                    {
                                        strRetVal = "";
                                    }
                                }
                            }
                            break;
                        case WorkgroupColorMode.COLOR_MODE_GRAYSCALE:
                            strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_GRAYSCALE, page);
                            arFiles = Directory.GetFiles(strImageFolderPath, searchPattern + strImageType + ".tif");
                            if (arFiles.Length > 0)
                            {
                                strRetVal = arFiles[0];
                            }
                            else
                            {
                                strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_COLOR, page);
                                arFiles = Directory.GetFiles(strImageFolderPath, searchPattern + strImageType + ".tif");
                                if (arFiles.Length > 0)
                                {
                                    strRetVal = arFiles[0];
                                }
                                else
                                {
                                    strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_BITONAL, page);
                                    arFiles = Directory.GetFiles(strImageFolderPath, searchPattern + strImageType + ".tif");
                                    if (arFiles.Length > 0)
                                    {
                                        strRetVal = arFiles[0];
                                    }
                                    else
                                    {
                                        strRetVal = "";
                                    }
                                }
                            }
                            break;
                        default:
                            strRetVal = "";
                            break;
                    }

                }

                // Cut off the Root path to return relative, 
                // Format: <yyyymmdd>\<bankID>\<lockboxID>\<fileName>.tif, where fileName = <BatchID>_<sequence>_<fileDescriptor>_<imageType>.tif
                if (strRetVal.Length > 0 && strRetVal.Length > m_sImageRootPath.Length)
                    strRetVal = strRetVal.Substring(m_sImageRootPath.Length);
            }
         
            catch (Exception e)
            {
                strRetVal = "";
                eventLog.logError(e);
            }
            return (strRetVal);
        }

        public override bool PutBatchImage(string siteKey, ILTAImageInfo ImageInfo, byte[] BatchImage){

            bool bRetVal = false;
            cPICS pics = new cPICS(siteKey);
            bRetVal = pics.PutBatchImage(m_sImageRootPath, ImageInfo, BatchImage);
            return bRetVal;
        }
    }
}
