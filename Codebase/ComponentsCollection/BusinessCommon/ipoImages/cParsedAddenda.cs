﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 06/19/2014
*
* Purpose: cImageData contains information to hold the image data for ACH and Wire Transfer 
*
* Modification History
******************************************************************************/
namespace WFS.RecHub.Common
{
    public sealed class cParsedAddenda
    {
        /// <summary>
        /// Field Display Name
        /// </summary>
        string m_DisplayName;
        /// <summary>
        /// Field Name
        /// </summary>
        string m_FieldName;
        /// <summary>
        /// Field Value
        /// </summary>
        string m_FieldValue;
        /// <summary>
        /// Batch Sequence
        /// </summary>
        int m_BatchSequence;
        /// UI Label
        /// </summary>
        string m_UILabel;
        /// Data Type
        /// </summary>
        int m_DataType;

        /// <summary>
        /// Constructor 
        /// </summary>
        public cParsedAddenda()
        {
            m_DisplayName = string.Empty;
            m_FieldValue = string.Empty;
            m_FieldName = string.Empty;
            m_UILabel = string.Empty;
        }

        /// <summary>
        /// Display Name 
        /// </summary>
        public string DisplayName
        {
            get {  return m_DisplayName;}
            set { m_DisplayName = value; }
        }

        /// <summary>
        /// Field Name 
        /// </summary>
        public string FieldName
        {
            get { return m_FieldName; }
            set { m_FieldName = value; }
        }

        /// <summary>
        /// Field Value
        /// </summary>
        public string FieldValue
        {
            get { return m_FieldValue; }
            set { m_FieldValue = value; }
        }
        /// <summary>
        /// Batch Sequence
        /// </summary>
        public int BatchSequence
        {
            get { return m_BatchSequence; }
            set { m_BatchSequence = value; }
        }

        /// <summary>
        /// UI Label
        /// </summary>
        public string UILabel
        {
            get { return m_UILabel; }
            set { m_UILabel = value; }
        }

        /// <summary>
        /// Data Type
        /// </summary>
        public int DataType
        {
            get { return m_DataType; }
            set { m_DataType = value; }
        }
    }

    /// <summary>
    /// Parsed Addenda Collection class
	/// </summary>
	public sealed class cParsedAddendaCollection : CollectionBase, IEnumerable<cParsedAddenda>
    {
        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to be get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public cParsedAddenda this[int index]
        {
            get
            {
                return ((cParsedAddenda)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }
        /// <summary>
        /// Adds an object to the end of the collection.
        /// </summary>
        /// <param name="value">The object to be added to the end of the collection.</param>
        /// <returns>Collection index at which the value has been added.</returns>
        public int Add(cParsedAddenda value)
        {
            return (List.Add(value));
        }
        /// <summary>
        /// Searches for the specified object and cParsedAddenda zero-based index 
        /// of the first occurrence within the entire collection.
        /// </summary>
        /// <param name="value">The object to locate in the collection. </param>
        /// <returns>Zero-based index of the first occurrence of value within the entire collection, if found; otherwise, -1.</returns>
        public int IndexOf(cParsedAddenda value)
        {
            return (List.IndexOf(value));
        }
        /// <summary>
        /// Inserts an element into the collection at the specified index.
        /// </summary>
        /// <param name="index">Zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert.</param>
        public void Insert(int index, cParsedAddenda value)
        {
            List.Insert(index, value);
        }
        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="value">The object to remove from the collection.</param>
        public void Remove(cParsedAddenda value)
        {
            List.Remove(value);
        }
        /// <summary>
        /// Determines whether the collection contains a specific element.
        /// </summary>
        /// <param name="value">The object to locate in the collection.</param>
        /// <returns>If the collection contains the specified value.</returns>
        public bool Contains(cParsedAddenda value)
        {
            // If value is not of type cParsedAddenda, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// Get Distinct Batch Sequence number
        /// </summary>
        /// <returns></returns>
        public ArrayList GetDistinctBatchSequence()
        {
            ArrayList arlBatchSequenceNo = new ArrayList();
            foreach(cParsedAddenda objParsedAddenda in this)
            {
                if (!arlBatchSequenceNo.Contains(objParsedAddenda.BatchSequence))
                    arlBatchSequenceNo.Add(objParsedAddenda.BatchSequence);
            }
            return arlBatchSequenceNo;
        }
        /// <summary>
        /// Get Distinct Field Display Names
        /// </summary>
        /// <returns></returns>
        public ArrayList GetDistinctDisplayNames()
        {
            ArrayList arlFieldNames = new ArrayList();
            foreach (cParsedAddenda objParsedAddenda in this)
            {
                if (!arlFieldNames.Contains(objParsedAddenda.DisplayName))
                    arlFieldNames.Add(objParsedAddenda.DisplayName);
            }
            return arlFieldNames;
        }

        /// <summary>
        /// Get Distinct Field Display Names
        /// </summary>
        /// <returns></returns>
        public IEnumerable<cParsedAddenda> GetDistinctParsedAddendas(int batchSequence)
        {
            var distinctParsedAddendas = this.Distinct(new ParsedAddendaComparer()); ;
            return distinctParsedAddendas.Where(a => a.BatchSequence == batchSequence);
        }

        /// <summary>
        /// Get Remittance Type
        /// </summary>
        /// <returns></returns>
        public ipoImageCommon.RemittanceType GetRemittanceType()
        {
            ArrayList arlStructuredFieldsList;
            ArrayList arlUnStructuredFieldsList;
            ArrayList arlRelatedFieldsList;
            arlStructuredFieldsList = ipoImageCommon.GetRemittanceFields(ipoImageCommon.RemittanceType.STRUCTURED);
            arlUnStructuredFieldsList = ipoImageCommon.GetRemittanceFields(ipoImageCommon.RemittanceType.UNSTRUCTURED);
            arlRelatedFieldsList = ipoImageCommon.GetRemittanceFields(ipoImageCommon.RemittanceType.RELATED);
            
            foreach(cParsedAddenda objPArsedAddenda in this)
            {
                if (arlStructuredFieldsList.Contains(objPArsedAddenda.FieldName.ToLower()))
                    return ipoImageCommon.RemittanceType.STRUCTURED;
                if (arlUnStructuredFieldsList.Contains(objPArsedAddenda.FieldName.ToLower()))
                    return ipoImageCommon.RemittanceType.UNSTRUCTURED;
                if (arlRelatedFieldsList.Contains(objPArsedAddenda.FieldName.ToLower()))
                    return ipoImageCommon.RemittanceType.RELATED;

            }
            return ipoImageCommon.RemittanceType.UNKNOWN;
        }

        /// <summary>
        /// Performs additional custom processes before inserting a new element into the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which to insert value.</param>
        /// <param name="value">New value of the element at index.</param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }
        /// <summary>
        /// Performs additional custom processes when removing an element from the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which value can be found.</param>
        /// <param name="value">The value of the element to remove at index.</param>
        protected override void OnRemove(int index, object value)
        {
            base.OnRemove(index, value);
        }
        /// <summary>
        /// Performs additional custom processes before setting a value in the collection instance.
        /// </summary>
        /// <param name="index">Zero-based index at which oldValue can be found.</param>
        /// <param name="oldValue">Value to replace with newValue.</param>
        /// <param name="newValue">New value of the element at index.</param>
        protected override void OnSet(int index, object oldValue, object newValue)
        {
            base.OnSet(index, oldValue, newValue);
        }
        /// <summary>
        /// Performs additional custom processes when validating a value.
        /// </summary>
        /// <param name="value">The object to validate.</param>
        protected override void OnValidate(object value)
        {
            if (value.GetType() != Type.GetType("WFS.RecHub.Common.cParsedAddenda"))
                throw new ArgumentException("value must be of type cParsedAddenda.", "value");
        }

        public new IEnumerator<cParsedAddenda> GetEnumerator()
        {
            foreach(cParsedAddenda parsedAddenda in this.List)
            {
                yield return parsedAddenda;
            }
        }
    }

}
