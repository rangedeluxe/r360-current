﻿using System;
using System.IO;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Collections;
using WFS.RecHub.Common.Log;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 06/19/2014
*
* Purpose: ipoEmbeddedImage contains information of the image that needs to be displayed for Wire and ACH
*
* Modification History
* WI 146899 SAS 07/01/2014
*   -Modified ipoImages for Generating Images on a fly for ACH Payment Type
* WI 146900 SAS 07/01/2014
*   -Modified ipoImages for Generating Images on a fly for Wire Payment Type
* WI 161054 SAS 08/25/2014
*   -Modified ipoImages for pulling data to display on check.
* WI 162794 SAS 09/03/2014
*   -Modified ipoImages for changing watermark text.
* WI 164621 SAS 09/09/2014
*   -Modified ipoImages for adding DDAKey for Customer Account no and
*  -Updated code for unparsed data to display is one line which has been chunk
*  -into two rows.
* WI  174734 SAS 11/03/2014
*   - Changes done to add the missing fields
* WI  175901 SAS 11/04/2014
* -Modified ipoImages to add all the columns for parsed data.
* WI  177973 SAS 11/14/2014
* -Modified ipoImages to avoid duplication of columns
* WI  179141 SAS 11/21/2014
* -Modified ipoImages to added RelatedRemittanceData
* WI  179505 SAS 11/24/2014
* -Modified ipoImages to added image extensions for surrogate images
******************************************************************************/
namespace WFS.RecHub.Common
{
    public class ipoEmbeddedImage : _DMPObjectRoot2
    {
        public ipoEmbeddedImage(string siteKey)
        {
            this.siteKey = siteKey;
        }

        /// <summary>
        /// Get the Check images data in Bytes for ACH and Wire Transfer
        /// </summary>
        /// <param name="imageBytes"></param>
        /// <returns></returns>
        public bool GetCheckImage(IItem irRequest, BatchPaymentType enPaymentTypeKey, out byte[] imageBytes)
        {
            bool bRetVal;
            imageBytes = null;
            try
            {
                cImageData objImageData = new cImageData(this.siteKey);
                Dictionary<string, string> dicFields;
                int iWidth = 0;
                if (objImageData.LoadDataEntryFields(irRequest))
                {
                    dicFields = objImageData.Fields;
                    Assembly assemblyImage = Assembly.GetExecutingAssembly();
                    Stream sImageStream = assemblyImage.GetManifestResourceStream("WFS.RecHub.Common.Resources.Check.tif");

                    using (Bitmap bmp = new Bitmap(sImageStream))
                    using (Graphics grfx = Graphics.FromImage(bmp))
                    using (SolidBrush brushBlack = new SolidBrush(Color.Black))
                    using (SolidBrush brushGray = new SolidBrush(Color.LightGray))
                    using (Font fntBasic = new Font(ipoImageCommon.FONT_NAME_ARIAL, 10, FontStyle.Bold))
                    {
                        iWidth = bmp.Width;

                        // draw some text on it
                        int x = 25;
                        int y = 25;
                        string sText;
                        // Put on the watermark
                        if (enPaymentTypeKey == BatchPaymentType.ACH)
                        {
                            using (Image rotatedText = RotateText("ACH", grfx, brushGray, (float)20.0))
                            {
                                grfx.DrawImage(rotatedText, new Point(200, 50));
                            }
                        }
                        else
                        {
                            using (Image rotatedText = RotateText("WIRE", grfx, brushGray, (float)20.0))
                            {
                                grfx.DrawImage(rotatedText, new Point(170, 50));
                            }
                        }

                        //Company Name
                        if (objImageData.CheckFields.TryGetValue(ipoImageCommon.CHECKS_REMITTER, out sText))
                            grfx.DrawString(sText, fntBasic, brushBlack, new RectangleF(x, y + 10, iWidth - x, y));

                        y += 65;
                        x += 450;
                        //Effective Date
                        if (objImageData.CheckFields.TryGetValue(ipoImageCommon.CHECKS_SOURCE_PROCESSING_DATE_KEY, out sText))
                        {
                            DateTime temp = DateTime.ParseExact(sText, "yyyyMMdd", null);
                            sText = temp.ToString("MMMM") + " " + temp.ToString("dd") + "," + temp.Year.ToString();
                            grfx.DrawString(sText, fntBasic, brushBlack, new RectangleF(x, y, iWidth - x, y));
                        }

                        y += 35;
                        x = 90;

                        //Pay Order of
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_RECEIVING_COMPANY, out sText))
                            grfx.DrawString(sText, fntBasic, brushBlack, new RectangleF(x, y, iWidth - x, y));
                        else if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_RECEIVING_INDIVIDUAL, out sText))
                            grfx.DrawString(sText, fntBasic, brushBlack, new RectangleF(x, y, iWidth - x, y));
                        else if (objImageData.CheckFields.TryGetValue(ipoImageCommon.WORK_GROUP_LNG_NAME, out sText))
                            grfx.DrawString(sText, fntBasic, brushBlack, new RectangleF(x, y, iWidth - x, y));

                        //Amount
                        x += 490;
                        if (objImageData.CheckFields.TryGetValue(ipoImageCommon.CHECKS_AMOUNT, out sText))
                        {
                            decimal decCheckAmount = Convert.ToDecimal(sText);
                            sText = Convert.ToDecimal(sText).ToString("N2");
                            grfx.DrawString(sText, fntBasic, brushBlack, new RectangleF(x, y, iWidth - x, y));
                            y += 30;
                            x = 30;
                            sText = decCheckAmount.ToLegalAmount().Text;
                            grfx.DrawString(sText, fntBasic, brushBlack, sText.ToLegalAmountRectangle(x, y, iWidth));
                        }

                        //Invoice
                        y += 85;
                        x = 50;
                        if (dicFields.TryGetValue(ipoImageCommon.STUBS_INVOICE, out sText))
                            grfx.DrawString(sText, fntBasic, brushBlack, new RectangleF(x, y, iWidth - x, y));
                        grfx.Dispose();
                        ImageConverter converter = new ImageConverter();
                        imageBytes = (byte[])converter.ConvertTo(bmp, typeof(byte[]));
                    }
                }
                bRetVal = true;
            }
            catch (System.Exception ex)
            {
                eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
            return bRetVal;
        }

        private Image RotateText(string Text, Graphics Graphic, SolidBrush TextBrush, float Angle)
        {
            using (Font fntWater = new Font(ipoImageCommon.FONT_NAME_ARIAL, 85, FontStyle.Regular))
            {
                //Measure size of the text
                SizeF TextSize = Graphic.MeasureString(Text, fntWater);
                SizeF NewSize = GetRotatedTextImageSize(TextSize, Angle);

                //Instantiate a new image for rotated text
                Bitmap RotatedText = new Bitmap((int)(Math.Round(NewSize.Width)), (int)(Math.Round(NewSize.Height)));

                //Get graphic object of new instantiated image for painting
                Graphics TextGraphic = Graphics.FromImage(RotatedText);
                TextGraphic.InterpolationMode = InterpolationMode.High;

                //Calculate coordination of center of the image
                float OX = (float)NewSize.Width / 2f;
                float OY = (float)NewSize.Height / 2f;

                //Apply transformations (translation, rotation, reverse translation)
                TextGraphic.TranslateTransform(OX, OY);
                TextGraphic.RotateTransform(Angle);
                TextGraphic.TranslateTransform(-OX, -OY);

                //Calculate the location of drawing text
                float X = (RotatedText.Width - TextSize.Width) / 2f;
                float Y = (RotatedText.Height - TextSize.Height) / 2f;

                //Draw the string
                TextGraphic.DrawString(Text, fntWater, TextBrush, X, Y);

                //Return the image of rotated text
                return RotatedText;
            }
        }

        private static SizeF GetRotatedTextImageSize(SizeF fontSize, float angle)
        {
            double theta = angle * Math.PI / 180.0;

            while (theta < 0.0)
                theta += 2 * Math.PI;

            double adjacentTop, oppositeTop;
            double adjacentBottom, oppositeBottom;

            if ((theta >= 0.0 && theta < Math.PI / 2.0) || (theta >= Math.PI && theta < (Math.PI + (Math.PI / 2.0))))
            {
                adjacentTop = Math.Abs(Math.Cos(theta)) * fontSize.Width;
                oppositeTop = Math.Abs(Math.Sin(theta)) * fontSize.Width;
                adjacentBottom = Math.Abs(Math.Cos(theta)) * fontSize.Height;
                oppositeBottom = Math.Abs(Math.Sin(theta)) * fontSize.Height;
            }
            else
            {
                adjacentTop = Math.Abs(Math.Sin(theta)) * fontSize.Height;
                oppositeTop = Math.Abs(Math.Cos(theta)) * fontSize.Height;
                adjacentBottom = Math.Abs(Math.Sin(theta)) * fontSize.Width;
                oppositeBottom = Math.Abs(Math.Cos(theta)) * fontSize.Width;
            }

            int nWidth = (int)Math.Ceiling(adjacentTop + oppositeBottom);
            int nHeight = (int)Math.Ceiling(adjacentBottom + oppositeTop);

            return new SizeF(nWidth, nHeight);
        }

        /// <summary>
        /// Get the Document images data in Bytes for ACH and Wire Transfer
        /// </summary>
        /// <param name="imageBytes"></param>
        /// <returns></returns>
        public ImageResponse GetDocumentImage(IItem irRequest, BatchPaymentType enPaymentTypeKey)
        {
            bool bRetVal = false;
            byte[] imageBytes = null;
            cImageData objImageData;
            var response = new ImageResponse();

            try
            {
                objImageData = new cImageData(this.siteKey);
                if (objImageData.LoadDataEntryFields(irRequest))
                {
                    switch (enPaymentTypeKey)
                    {
                        case BatchPaymentType.ACH:
                            //GetDocumentImageForACH(irRequest, objImageData, out imageBytes);
                            //response.ImageBytes = imageBytes;
                            
                            response.ImageBytes = objImageData.ToACHPaymentPdfBytes(irRequest);
                            response.FileType = "pdf";
                            break;
                        case BatchPaymentType.WIRE:
                            GetDocumentImageForWire(irRequest, objImageData, out imageBytes);
                            response.ImageBytes = imageBytes;
                            response.FileType = "tif";
                            break;
                    }
                }
                response.IsSuccessful = true;
                bRetVal = true;
            }
            catch (System.Exception ex)
            {
                eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
            return response;
        }

        private bool GetDocumentImageForACH(IItem irRequest, cImageData objImageData, out byte[] imageBytes)
        {
            bool bRetVal = false;
            string sText;
            imageBytes = null;
            Dictionary<string, string> dicFields;
            ArrayList alRawPaymentData;
            try
            {
                int iWidth = 4000;
                int iHeight = 3300;
                //Raw Payment Data
                alRawPaymentData = objImageData.LoadRawPaymentData(irRequest, BatchPaymentType.ACH);
                dicFields = objImageData.Fields;

                using (Bitmap bmp = new Bitmap(iWidth, iHeight, PixelFormat.Format32bppArgb))
                {
                    bmp.SetResolution(300, 300);
                    Graphics grfx = Graphics.FromImage(bmp);

                    using (Pen penBlack = new Pen(Color.Black, 3))
                    using (SolidBrush brushWhite = new SolidBrush(Color.White))
                    using (SolidBrush brushBlack = new SolidBrush(Color.Black))
                    using (Font fntEntryHeader = new Font(ipoImageCommon.FONT_NAME_ARIAL, 10, FontStyle.Regular))
                    using (Font fntEntryHeaderBold = new Font(ipoImageCommon.FONT_NAME_ARIAL, 14, FontStyle.Bold))
                    using (Font fntEntryHeaderUnder = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Underline))
                    using (Font fntEntryData = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Regular))
                    using (Font fntEntryRaw = new Font(ipoImageCommon.FONT_NAME_COURIER_NAME, 10, FontStyle.Regular))
                    {
                        int iHdrFntHeight = Convert.ToInt32(grfx.MeasureString("Ag", fntEntryHeader).Height) + 1;
                        int iHdrFntWidth = Convert.ToInt32(grfx.MeasureString("W", fntEntryHeader).Width) + 1;
                        int iDataFntHeight = Convert.ToInt32(grfx.MeasureString("Ag", fntEntryData).Height) + 1;
                        int iDataFntWidth = Convert.ToInt32(grfx.MeasureString("W", fntEntryData).Width) + 1;
                        int iRawFntHeight = Convert.ToInt32(grfx.MeasureString("Ag", fntEntryRaw).Height) + 1;
                        int iRawFntWidth = Convert.ToInt32(grfx.MeasureString("W", fntEntryRaw).Width) + 1;

                        // fill the background with white
                        grfx.FillRectangle(brushWhite, 0, 0, iWidth, iHeight);

                        // draw some text on it
                        int x = 25;
                        int y = 150;

                        grfx.DrawString(ipoImageCommon.ACH_PAYMENT_HEADING, fntEntryHeaderBold, brushBlack, new PointF(x, y));
                        y += (2 * iHdrFntHeight);
                        int iSaveY = y;
                        grfx.DrawString(ipoImageCommon.ACH_BATCH_INFORMATION, fntEntryHeaderUnder, brushBlack, new PointF(x, y));
                        y += (2 * iHdrFntHeight);
                        x += Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_BATCH_INFORMATION, fntEntryHeaderUnder).Width * 0.9);

                        //ACH Batch Number
                        grfx.DrawString(ipoImageCommon.ACH_BATCH_NUMBER, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_BATCH_NUMBER, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_ACH_BATCH_NUMBER, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //CompanyID
                        grfx.DrawString(ipoImageCommon.ACH_COMPANY_ID, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_COMPANY_ID, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_COMPANY_ID, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //CompanyName
                        grfx.DrawString(ipoImageCommon.ACH_COMPANY_NAME, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_COMPANY_NAME, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_COMPANY_NAME, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //Entry Class
                        grfx.DrawString(ipoImageCommon.ACH_ENTRY_CLASS, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_ENTRY_CLASS, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_STANDARD_ENTRY_CLASS, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }

                        y += iHdrFntHeight;

                        //Entry Description
                        grfx.DrawString(ipoImageCommon.ACH_ENTRY_DESCRIPTION, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_ENTRY_DESCRIPTION, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_ENTRY_DESCRIPTION, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //Company Data
                        grfx.DrawString(ipoImageCommon.ACH_COMPANY_DATA, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_COMPANY_DATA, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_COMPANY_DATA, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //Descriptive Date
                        grfx.DrawString(ipoImageCommon.ACH_DESCRIPTIVE_DATE, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_DESCRIPTIVE_DATE, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_DESCRIPTIVE_DATE, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //Effective Date
                        grfx.DrawString(ipoImageCommon.ACH_EFFECTIVE_DATE, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_EFFECTIVE_DATE, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_EFFECTIVE_DATE, out sText))
                        {
                            grfx.DrawString(Convert.ToDateTime(sText).ToString("MM/dd/yyyy"), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //Settlement Date
                        grfx.DrawString(ipoImageCommon.ACH_SETTLEMENT_DATE, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_SETTLEMENT_DATE, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_SETTLEMENT_DATE, out sText))
                        {
                            grfx.DrawString(Convert.ToDateTime(sText).ToString("MM/dd/yyyy"), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        x = 1200;
                        y = iSaveY;
                        grfx.DrawString(ipoImageCommon.ACH_ENTRY_DETAIL, fntEntryHeaderUnder, brushBlack, new PointF(x, y));
                        y += (2 * iHdrFntHeight);
                        x += Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_ENTRY_DETAIL, fntEntryHeaderUnder).Width * 0.9);

                        //Transaction Code
                        grfx.DrawString(ipoImageCommon.ACH_TRANSACTION_CODE, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_TRANSACTION_CODE, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_ELECTRONIC_TRANS_CD, out sText))
                        {
                            grfx.DrawString(sText.ToString(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //Account Number
                        grfx.DrawString(ipoImageCommon.ACH_ACCOUNT_NUMBER, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_ACCOUNT_NUMBER, fntEntryHeader).Width), y));
                        if (objImageData.CheckFields.TryGetValue(ipoImageCommon.CHECKS_DDA, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //Amount
                        grfx.DrawString(ipoImageCommon.ACH_AMOUNT, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_AMOUNT, fntEntryHeader).Width), y));
                        if (objImageData.CheckFields.TryGetValue(ipoImageCommon.CHECKS_AMOUNT, out sText))
                        {
                            grfx.DrawString(Convert.ToDecimal(sText).ToString("C"), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //IndividualID
                        grfx.DrawString(ipoImageCommon.ACH_INDIVIDUAL_ID, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_INDIVIDUAL_ID, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_INDIVIDUAL_ID, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //Receiving Company
                        grfx.DrawString(ipoImageCommon.ACH_RECEIVING_COMPANY, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_RECEIVING_COMPANY, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_RECEIVING_COMPANY, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += iHdrFntHeight;

                        //Trace Number
                        grfx.DrawString(ipoImageCommon.ACH_TRACE_NUMBER, fntEntryHeader, brushBlack, new PointF(x - Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_TRACE_NUMBER, fntEntryHeader).Width), y));
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_TRACE_NUMBER, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryHeader, brushBlack, new PointF(x + iHdrFntWidth, y));
                        }
                        y += (5 * iHdrFntHeight);

                        //Raw Addenda Data
                        x = 25;
                        grfx.DrawString(ipoImageCommon.ACH_RAW_ADDENDA, fntEntryHeaderUnder, brushBlack, new PointF(x, y));
                        y += (2 * iHdrFntHeight);
                        grfx.DrawRectangle(penBlack, x, y, Convert.ToInt32(grfx.MeasureString(ipoImageCommon.ACH_RAW_DATA_1, fntEntryRaw).Width), (2 * iRawFntHeight));
                        grfx.DrawString(ipoImageCommon.ACH_RAW_DATA_1, fntEntryRaw, brushBlack, new PointF(x, y));
                        y += iRawFntHeight;
                        grfx.DrawString(ipoImageCommon.ACH_RAW_DATA_2, fntEntryRaw, brushBlack, new PointF(x, y));
                        y += (2 * iRawFntHeight);

                        //Raw Payment Data
                        foreach (string strRayPayment in alRawPaymentData)
                        {
                            grfx.DrawString(strRayPayment.Trim(), fntEntryRaw, brushBlack, new PointF(x, y));
                            y += iRawFntHeight;
                        }
                        y += (2 * iHdrFntHeight);
                        //ACH Parsed Addenda
                        if (objImageData.ParsedAddendas.Count > 0)
                        {
                            x = 25;
                            grfx.DrawString(ipoImageCommon.ACH_PARSED_ADDENDA, fntEntryHeaderUnder, brushBlack, new PointF(x, y));
                            y += (2 * iHdrFntHeight);
                            UnstructuredAddendaGrid.Draw(objImageData, y, iWidth, grfx, eventLog);
                        }
                        grfx.Dispose();
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bmp.Save(ms, ImageFormat.Tiff);
                            imageBytes = ms.ToArray();
                        }
                    }
                }
                bRetVal = true;
            }
            catch (Exception ex)
            {
                eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
            return bRetVal;
        }

        private bool GetDocumentImageForWire(IItem irRequest, cImageData objImageData, out byte[] imageBytes)
        {
            bool bRetVal = false;
            string sText;
            imageBytes = null;
            Dictionary<string, string> dicFields;
            ArrayList alRawPaymentData;
            try
            {
                int iWidth = 3550;
                int iHeight = 3300;

                //Raw Payment Data
                alRawPaymentData = objImageData.LoadRawPaymentData(irRequest, BatchPaymentType.WIRE);
                dicFields = objImageData.Fields;

                using (Bitmap bmp = new Bitmap(iWidth, iHeight, PixelFormat.Format32bppArgb))
                {
                    bmp.SetResolution(300, 300);
                    Graphics grfx = Graphics.FromImage(bmp);

                    using (SolidBrush brushWhite = new SolidBrush(Color.White))
                    using (SolidBrush brushBlack = new SolidBrush(Color.Black))
                    using (Font fntEntryHeader = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Bold))
                    using (Font fntEntryHeaderBold = new Font(ipoImageCommon.FONT_NAME_ARIAL, 14, FontStyle.Bold))
                    using (Font fntEntryHeaderUnder = new Font(ipoImageCommon.FONT_NAME_ARIAL, 14, FontStyle.Underline))
                    using (Font fntEntryData = new Font(ipoImageCommon.FONT_NAME_ARIAL, 12, FontStyle.Regular))
                    using (Font fntEntryRaw = new Font(ipoImageCommon.FONT_NAME_COURIER_NAME, 12, FontStyle.Regular))
                    {
                        int iHdrFntHeight = Convert.ToInt32(grfx.MeasureString("Ag", fntEntryHeader).Height) + 1;
                        int iHdrFntWidth = Convert.ToInt32(grfx.MeasureString("W", fntEntryHeader).Width) + 1;
                        int iDataFntHeight = Convert.ToInt32(grfx.MeasureString("Ag", fntEntryData).Height) + 1;
                        int iDataFntWidth = Convert.ToInt32(grfx.MeasureString("W", fntEntryData).Width) + 1;
                        int iRawFntHeight = Convert.ToInt32(grfx.MeasureString("Ag", fntEntryRaw).Height) + 1;
                        int iRawFntWidth = Convert.ToInt32(grfx.MeasureString("W", fntEntryRaw).Width) + 1;

                        // fill the background with white
                        grfx.FillRectangle(brushWhite, 0, 0, iWidth, iHeight);

                        // draw some text on it
                        int x = 25;
                        int y = 150;

                        grfx.DrawString(ipoImageCommon.WIRE_PAYMENT_HEADING, fntEntryHeaderBold, brushBlack, new PointF(x, y));
                        y += (2 * iHdrFntHeight);
                        int iSaveY = y;
                        y += iHdrFntHeight;
                        //File Creation
                        grfx.DrawString(ipoImageCommon.WIRE_FILE_CREATION, fntEntryHeader, brushBlack, new PointF(x, y));
                        x += Convert.ToInt32(grfx.MeasureString(ipoImageCommon.WIRE_FILE_CREATION, fntEntryHeader).Width);
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_BAI_FILE_CREATION_DATE, out sText))
                        {
                            DateTime temp;
                            if (DateTime.TryParse(sText, out temp))
                            {
                                sText = temp.ToString("MM/dd/yyyy");
                                grfx.DrawString(sText.Trim(), fntEntryData, brushBlack, new PointF(x, y));
                                x += Convert.ToInt32(grfx.MeasureString(sText, fntEntryData).Width);
                            }
                        }
                        //File ID
                        grfx.DrawString(ipoImageCommon.WIRE_FILE_ID, fntEntryHeader, brushBlack, new PointF(x, y));
                        x += Convert.ToInt32(grfx.MeasureString(ipoImageCommon.WIRE_FILE_ID, fntEntryHeader).Width);
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_BAI_FILE_ID, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryData, brushBlack, new PointF(x, y));
                            x += Convert.ToInt32(grfx.MeasureString(sText, fntEntryData).Width);
                        }

                        //Customer Account
                        grfx.DrawString(ipoImageCommon.WIRE_CUSTOMERT_ACCOUNT, fntEntryHeader, brushBlack, new PointF(x, y));
                        x += Convert.ToInt32(grfx.MeasureString(ipoImageCommon.WIRE_CUSTOMERT_ACCOUNT, fntEntryHeader).Width);
                        if (objImageData.CheckFields.TryGetValue(ipoImageCommon.CHECKS_DDA, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryData, brushBlack, new PointF(x, y));
                        }
                        y += iHdrFntHeight + 20;

                        //Payment Type
                        x = 25;
                        grfx.DrawString(ipoImageCommon.WIRE_PAYMENT_TYPE, fntEntryHeader, brushBlack, new PointF(x, y));
                        x += Convert.ToInt32(grfx.MeasureString(ipoImageCommon.WIRE_PAYMENT_TYPE, fntEntryHeader).Width);
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_TYPE_CODE, out sText))
                        {
                            switch (sText)
                            {
                                case ipoImageCommon.INCOMING_MONEY_TRANSFER_TYPE_CODE:
                                    sText = sText + ipoImageCommon.INCOMING_MONEY_TRANSFER;
                                    break;
                                case ipoImageCommon.OUTGOING_MONEY_TRANSFER_TYPE_CODE:
                                    sText = sText + ipoImageCommon.OUTGOING_MONEY_TRANSFER;
                                    break;
                                default:
                                    sText = sText + ipoImageCommon.UNKNOWN_MONEY_TRANSFER;
                                    break;
                            }
                            grfx.DrawString(sText.Trim(), fntEntryData, brushBlack, new PointF(x, y));
                            x += Convert.ToInt32(grfx.MeasureString(sText, fntEntryData).Width);
                        }

                        //Amount
                        grfx.DrawString(ipoImageCommon.WIRE_AMOUNT, fntEntryHeader, brushBlack, new PointF(x, y));
                        x += Convert.ToInt32(grfx.MeasureString(ipoImageCommon.WIRE_AMOUNT, fntEntryHeader).Width);
                        if (objImageData.CheckFields.TryGetValue(ipoImageCommon.CHECKS_AMOUNT, out sText))
                        {
                            grfx.DrawString(Convert.ToDecimal(sText).ToString("C"), fntEntryData, brushBlack, new PointF(x + iHdrFntWidth, y));
                            x += Convert.ToInt32(grfx.MeasureString(sText, fntEntryData).Width);
                        }
                        y += iHdrFntHeight + 20;
                        x = 25;
                        //Bank Reference Number
                        grfx.DrawString(ipoImageCommon.WIRE_BANK_REFERENCE_NO, fntEntryHeader, brushBlack, new PointF(x, y));
                        x += Convert.ToInt32(grfx.MeasureString(ipoImageCommon.WIRE_BANK_REFERENCE_NO, fntEntryHeader).Width);
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_BAI_BANK_REFERENCE, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryData, brushBlack, new PointF(x, y));
                            x += Convert.ToInt32(grfx.MeasureString(sText, fntEntryData).Width);
                        }

                        //Customer Reference Number
                        grfx.DrawString(ipoImageCommon.WIRE_CUSTOMER_REFERENCE_NO, fntEntryHeader, brushBlack, new PointF(x, y));
                        x += Convert.ToInt32(grfx.MeasureString(ipoImageCommon.WIRE_CUSTOMER_REFERENCE_NO, fntEntryHeader).Width);
                        if (dicFields.TryGetValue(ipoImageCommon.CHECKS_DATA_ENTRY_BAI_CUSTOMER_REFERENCE, out sText))
                        {
                            grfx.DrawString(sText.Trim(), fntEntryData, brushBlack, new PointF(x, y));
                            x += Convert.ToInt32(grfx.MeasureString(sText, fntEntryData).Width);
                        }

                        y += iHdrFntHeight * 2;
                        //Raw Addenda Data
                        x = 25;
                        grfx.DrawString(ipoImageCommon.WIRE_UNPARSED_TEXT, fntEntryHeaderUnder, brushBlack, new PointF(x, y));
                        y += (2 * iHdrFntHeight);

                        //Raw Payment Data
                        foreach (string strRayPayment in alRawPaymentData)
                        {
                            grfx.DrawString(strRayPayment.Trim(), fntEntryRaw, brushBlack, new PointF(x, y));
                            y += iRawFntHeight;
                        }
                        y += (2 * iHdrFntHeight);

                        //Wire Transfer Parsed Addenda
                        if (objImageData.ParsedAddendas.Count > 0)
                        {
                            grfx.DrawString(ipoImageCommon.WIRE_PARSED_TEXT, fntEntryHeaderUnder, brushBlack, new PointF(x, y));
                            ipoImageCommon.RemittanceType enRemittanceType = objImageData.ParsedAddendas.GetRemittanceType();
                            switch (enRemittanceType)
                            {
                                case ipoImageCommon.RemittanceType.STRUCTURED:
                                    StructuredAddendaGrid.Draw(objImageData, y, grfx, eventLog);
                                    break;
                                case ipoImageCommon.RemittanceType.UNSTRUCTURED:
                                    UnstructuredAddendaGrid.Draw(objImageData, y, iWidth, grfx, eventLog);
                                    break;
                                case ipoImageCommon.RemittanceType.RELATED:
                                    RelatedRemittanceGrid.Draw(objImageData, y, grfx, eventLog);
                                    break;
                            }
                        }

                        grfx.Dispose();

                        using (MemoryStream ms = new MemoryStream())
                        {
                            bmp.Save(ms, ImageFormat.Tiff);
                            imageBytes = ms.ToArray();
                        }
                    }
                }

                bRetVal = true;
            }
            catch (Exception ex)
            {
                eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
            return bRetVal;
        }
    }
}
