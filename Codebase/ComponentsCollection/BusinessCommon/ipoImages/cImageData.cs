﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 06/19/2014
*
* Purpose: cImageData contains information to hold the image data for ACH and Wire Transfer 
*
* Modification History
* WI 146899 SAS 07/01/2014
*   -Modified ipoImages for Generating Images on a fly for ACH Payment Type
* WI 146900 SAS 07/01/2014
*   -Modified ipoImages for Generating Images on a fly for Wire Payment Type
*   
* WI 161054 SAS 08/25/2014
*   -Modified ipoImages for pulling data to display on check.
* WI 162633 BLR 09/03/2014
*   - Updated the GetDataEntryDetails Stored Proc.
* WI 164621 SAS 09/09/2014
*   -Modified ipoImages for adding DDAKey for Customer Account no and 
*  -Updated code for unparsed data to display is one line which has been chunk 
*  -into two rows. 
* WI 169360 SAS 10/02/2014
*   -Modified ipoImages for Displaying ACH and Wire Transfer Image
*   
* WI 169847 SAS 10/07/2014
*   -Modified calling functions to get Check and invoice data.
* WI  173874  SAS 10/23/2014 
*   - Changes done to retrieve  ACH/Wire check image
* WI  174734 SAS 11/03/2014 
*   - Changes done to the signature for retrieving  ACH/Wire check image
* WI  177973 SAS 11/14/2014 
* -Modified ipoImages to avoid duplication of columns
******************************************************************************/

namespace WFS.RecHub.Common
{
    public class cImageData : _DMPObjectRoot2
       {
        private Dictionary<string, string> m_Fields;
        private Dictionary<string, string> m_CheckFields;
        private cParsedAddendaCollection m_ParsedAddendas;
        private bool m_IsCheck;

      

        /// <summary>
        /// Get the Check Field and field value
        /// </summary>
        public Dictionary<string, string> CheckFields
        {
            get { return m_CheckFields; }
        }

        /// <summary>
        /// Get the Field and field value
        /// </summary>
        public Dictionary<string, string> Fields
        {
            get { return m_Fields; }
        }


        /// <summary>
        /// Get the Parsed addenda Collection
        /// </summary>
        public cParsedAddendaCollection ParsedAddendas
        {
            get { return m_ParsedAddendas; }
        }

        /// <summary>
        /// check the object type
        /// </summary>
        public bool IsCheck
        {
            get { return m_IsCheck; }
            set { m_IsCheck= value; }

        }


        public cImageData(string SiteKey) {
            this.siteKey = SiteKey;
            m_Fields = new Dictionary<string, string>();
            m_CheckFields = new Dictionary<string, string>();
            m_ParsedAddendas = new cParsedAddendaCollection();
        }

        /// <summary>
        /// Load all the data Entry details
        /// </summary>
        /// <param name="irRequest"></param>
        /// <returns></returns>
        public bool LoadDataEntryFields(IItem irRequest)
        {
            bool bolRetVal = false;                        
            try
            {
                switch (ipoImageCommon.GetObjectType(irRequest))
                {
                    case ipoImageCommon.ObjectType.CHECK:
                        IsCheck = true;
                        break;
                    case ipoImageCommon.ObjectType.DOCUMENT:
                        IsCheck = false;
                        break;
                }
                bolRetVal =LoadCheckDataEntry(irRequest);
                    
            }
            catch(Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, " LoadDataEntryFields(IItem irRequest)");
                bolRetVal = false;                
            }
            return bolRetVal;
        }

        /// <summary>
        /// Loads the Check Data Entry Fields 
        /// </summary>
        /// <param name="irRequest"></param>
        /// <returns></returns>
        private bool LoadCheckDataEntry(IItem irRequest)
        {
            bool bolRetVal = false;            
            DataTable dtDEFields = new DataTable();
            try
            {

                if (ItemProcDAL.GetDataEntryDetails(irRequest.BatchID,
                                                    irRequest.DepositDate,
                                                    irRequest.BatchSequence,                                                       
                                                    IsCheck,
                                                    out dtDEFields) && dtDEFields.Rows.Count > 0)
                {                   
                    //Populate Check Data Entry Fields 
                    PopulateCheckDataEntry(dtDEFields);
                    //Populate Stub Fields 
                    PopulateStubs(dtDEFields);
                    //Populate Stubs Data Entry Fields 
                    PopulateStubsDataEntry(dtDEFields);                                        
                }
                //Populate Check Fields 
                PopulateChecks(irRequest);
                bolRetVal = true;
            }
            catch (Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, "LoadCheckDataEntry(IItem irRequest)");
                bolRetVal = false;
            }
            return bolRetVal;
        }


        /// <summary>
        /// Loads Raw Payment Data
        /// </summary>
        /// <param name="irRequest"></param>
        /// <param name="enPaymentTypeKey"></param>
        /// <returns></returns>
        public ArrayList LoadRawPaymentData(IItem irRequest, BatchPaymentType enPaymentTypeKey)
        {
            
            DataTable dtRawPaymentData = new DataTable();
            ArrayList alRawPaymentData=new ArrayList();
            string strRawData = string.Empty;
            try
            {

                if (ItemProcDAL.GetRawPaymentData(irRequest.BatchID,
                                                  irRequest.DepositDate,
                                                  irRequest.BatchSequence,
                                                  IsCheck,
                                                  out dtRawPaymentData) && dtRawPaymentData.Rows.Count > 0)
                {
                    
                    if(enPaymentTypeKey==BatchPaymentType.WIRE)
                    {
                        dtRawPaymentData.Columns.Add("DataAdded");
                        foreach (DataRow drRawPaymentData in dtRawPaymentData.Rows)
                        {
                            strRawData = string.Empty;
                            foreach (DataRow drFilterRawPaymentData in dtRawPaymentData.Select("RawDataSequence='" + drRawPaymentData["RawDataSequence"].ToString() + "' and DataAdded is null"))
                            {
                                strRawData = strRawData + drFilterRawPaymentData["RawData"].ToString();
                                drFilterRawPaymentData["DataAdded"] = 'Y';
                            }
                            if (!string.IsNullOrWhiteSpace(strRawData))
                                alRawPaymentData.Add(strRawData.Trim());
                        }
                        
                    }
                    else 
                    {
                        foreach (DataRow drRawPaymentData in dtRawPaymentData.Rows)
                            alRawPaymentData.Add(drRawPaymentData["RawData"].ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, "LoadRawPaymentData(IItem irRequest)");                
            }
            return alRawPaymentData;
        } 

       /// <summary>
        ///  Populate Check Fields
       /// </summary>
       /// <param name="irRequest"></param>
        private void PopulateChecks(IItem irRequest)
        {            
            DataTable dtCheckDetail = new DataTable();
            try
            {                               
                if (ItemProcDAL.GetCheckDetails(irRequest.BatchID,
                                                irRequest.DepositDate,
                                                irRequest.BatchSequence, 
                                                IsCheck,
                                                out dtCheckDetail) && dtCheckDetail.Rows.Count > 0)
                {
                    foreach (DataRow drCheckDetail in dtCheckDetail.Rows)
                    {
                        m_CheckFields.Add(ipoImageCommon.CHECKS_REMITTER, drCheckDetail[ipoImageCommon.CHECKS_REMITTER].ToString());
                        m_CheckFields.Add(ipoImageCommon.CHECKS_AMOUNT, drCheckDetail[ipoImageCommon.CHECKS_AMOUNT].ToString());
                        m_CheckFields.Add(ipoImageCommon.CHECKS_SOURCE_PROCESSING_DATE_KEY, drCheckDetail[ipoImageCommon.CHECKS_SOURCE_PROCESSING_DATE_KEY].ToString());
                        m_CheckFields.Add(ipoImageCommon.WORK_GROUP_LNG_NAME, drCheckDetail[ipoImageCommon.WORK_GROUP_LNG_NAME].ToString());
                        m_CheckFields.Add(ipoImageCommon.CHECKS_DDA, drCheckDetail[ipoImageCommon.CHECKS_DDA].ToString());
                    }
                }
                else
                    eventLog.logWarning("No information for check found while calling PopulateChecks()", MessageImportance.Verbose);                
            }
            catch (Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, "PopulateChecks(IItem irRequest)");
            }
        }
      
        
        /// <summary>
        /// Populate Check Data Entry Fields
        /// </summary>
        /// <param name="dtEntryDetail"></param>
        private void PopulateCheckDataEntry(DataTable dtEntryDetail)
        {            
            ArrayList arFieldList;
            try
            {
                //CheckDataEntry Columns                     
                arFieldList = ipoImageCommon.GetFields(ipoImageCommon.TableName.CHECK);
                var result = from DataEntryRow in dtEntryDetail.AsEnumerable()
                             where DataEntryRow.Field<string>("TableName").ToLower() == ipoImageCommon.CHECKS_TABLE &&
                                   arFieldList.Contains(DataEntryRow.Field<string>("FldName").ToLower())
                             select DataEntryRow;
                foreach (DataRow Fields in result)
                {
                    if(!m_Fields.ContainsKey(Fields["FldName"].ToString().ToLower()))
                        m_Fields.Add(Fields["FldName"].ToString().ToLower(), Fields["DataEntryValue"].ToString());
                }
            }
            catch (Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, "PopulateCheckDataEntry(DataTable dtEntryDetail)");
            }         
        }
             
        /// <summary>
        /// Populate All the Stub Fields
        /// </summary>
        /// <param name="dtEntryDetail"></param>
        private void PopulateStubs(DataTable dtEntryDetail)
        {
            try
            {
            
                var result = from DataEntryRow in dtEntryDetail.AsEnumerable()
                         where DataEntryRow.Field<string>("TableName").ToLower() == ipoImageCommon.STUBS_TABLE                             
                         select DataEntryRow;
                foreach (DataRow Fields in result)
                {
                    if (!m_Fields.ContainsKey(Fields["FldName"].ToString().ToLower()))
                        m_Fields.Add(Fields["FldName"].ToString().ToLower(), Fields["DataEntryValue"].ToString());
                    
                    cParsedAddenda objParsedAddenda = new cParsedAddenda();
                    objParsedAddenda.DisplayName=Fields["DisplayName"].ToString().ToLower();
                    objParsedAddenda.FieldName = Fields["FldName"].ToString().ToLower();
                    objParsedAddenda.FieldValue = Fields["DataEntryValue"].ToString();
                    objParsedAddenda.BatchSequence = Convert.ToInt32(Fields["BatchSequence"].ToString());
                    objParsedAddenda.UILabel = Fields["UILabel"].ToString();
                    objParsedAddenda.DataType = Convert.ToInt32(Fields["DataType"].ToString());

                    this.ParsedAddendas.Add(objParsedAddenda);                    
                }
            }
            catch (Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, "PopulateStubs(DataTable dtEntryDetail)");
            }
        }

        /// <summary>
        /// Populate All Stubs Data Entry Fields
        /// </summary>
        /// <param name="dtEntryDetail"></param>
        private void PopulateStubsDataEntry(DataTable dtEntryDetail)
        {
            
            try
            {
                
                var result = from DataEntryRow in dtEntryDetail.AsEnumerable()
                             where DataEntryRow.Field<string>("TableName").ToLower() == ipoImageCommon.STUBS_TABLE             
                             select DataEntryRow;
                foreach (DataRow Fields in result)
                {
                    if (!m_Fields.ContainsKey(Fields["FldName"].ToString().ToLower()))
                        m_Fields.Add(Fields["FldName"].ToString().ToLower(), Fields["DataEntryValue"].ToString());

                    cParsedAddenda objParsedAddenda = new cParsedAddenda();
                    objParsedAddenda.DisplayName = Fields["DisplayName"].ToString().ToLower();
                    objParsedAddenda.FieldValue = Fields["DataEntryValue"].ToString();
                    objParsedAddenda.FieldName = Fields["FldName"].ToString().ToLower();
                    objParsedAddenda.BatchSequence = Convert.ToInt32(Fields["BatchSequence"].ToString());
                    objParsedAddenda.UILabel = Fields["UILabel"].ToString();
                    objParsedAddenda.DataType = Convert.ToInt32(Fields["DataType"].ToString());
                    this.ParsedAddendas.Add(objParsedAddenda);    
                }
            }
            catch (Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, "PopulateStubsDataEntry(DataTable dtEntryDetail)");
            }
        }
        
    }
}
