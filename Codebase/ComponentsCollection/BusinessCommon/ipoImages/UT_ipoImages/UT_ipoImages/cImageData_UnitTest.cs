﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using WFS.RecHub.Common;

namespace ipoImages_UnitTest {
    [TestClass]
    public class cImageData_UnitTest {
        [TestMethod]
        public void UT_LoadDataEntryFields_ChecksDE_And_StubsDE_should_load() {
            UT_OLFServicesDAL testDAL = new UT_OLFServicesDAL() {
                    GetDataEntryDetails_XMLData = Properties.Resources.GetDataEntryDetails_ChecksAndStubs 
            };
            cCheck request = new cCheck() {
                Account = "45121838",
                Amount = 0,
                BankID = 9999,
                BatchID = 2,
                BatchSequence=114,
                BatchSiteCode=1,
                BatchSourceShortName = "DevImageRPS",
                CheckSequence = 57,
                DefaultColorMode= WorkgroupColorMode.COLOR_MODE_BITONAL,
                DepositDate = DateTime.Parse("4/28/2015"),
                RemitterName="",
                RT="009674563",
                Serial="",
                SessionID = Guid.Empty,
                SourceProcessingDateKey=0,
                TransactionID = 57,
                TxnSequence = 57,
            };
            cImageData imageData = new cImageData("IPOnline");

            imageData.CreateOLFServicesDAL = sitekey=>testDAL;
            imageData.LoadDataEntryFields(request);
            Assert.IsTrue(imageData.ParsedAddendas.Count == 4 && imageData.Fields.ContainsKey("effectivedate"));
        }
    }
}
