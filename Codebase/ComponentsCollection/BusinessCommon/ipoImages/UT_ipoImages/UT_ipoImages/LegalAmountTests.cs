﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;

namespace ipoImages_UnitTest
{
    [TestClass]
    public class LegalAmountTests
    {
        private const string InvalidLegalAmountMessage = "Invalid Legal Amount";

        [TestMethod]
        public void Can_ParseOnesPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "1.23";
            var expected = "One & 23/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseTensPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "23.45";
            var expected = "Twenty-Three & 45/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseHundredsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "456.78";
            var expected = "Four Hundred Fifty-Six & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseThousandsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "7,890.12";
            var expected = "Seven Thousand Eight Hundred Ninety & 12/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseTenThousandsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "12,345.67";
            var expected = "Twelve Thousand Three Hundred Forty-Five & 67/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseHundredThousandsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "890,123.45";
            var expected = "Eight Hundred Ninety Thousand One Hundred Twenty-Three & 45/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseMillionsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "6,789,012.34";
            var expected = "Six Million Seven Hundred Eighty-Nine Thousand Twelve & 34/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseTenMillionsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "56,000,001.02";
            var expected = "Fifty-Six Million One & 02/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseHundredMillionsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "999,000,000.00";
            var expected = "Nine Hundred Ninety-Nine Million & 00/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseBillionsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "1,000,000,000.00";
            var expected = "One Billion & 00/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseMaxWireDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "999,999,999,999.99";
            var expected = "Nine Hundred Ninety-Nine Billion Nine Hundred Ninety-Nine Million Nine Hundred Ninety-Nine Thousand Nine Hundred Ninety-Nine & 99/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseMaxACHDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "99,999,999.99";
            var expected = "Ninety-Nine Million Nine Hundred Ninety-Nine Thousand Nine Hundred Ninety-Nine & 99/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseLessThanDollar_ToLegalAmount()
        {
            //arrange
            var textValue = ".01";
            var expected = "Zero & 01/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseZeroAmount_ToLegalAmount()
        {
            //arrange
            var textValue = ".00";
            var expected = "Zero & 00/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_HandleInvalidAmount()
        {
            //arrange
            var textValue = "999,999,999,999,999.99";
            var expected = InvalidLegalAmountMessage;

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_HandleNegativeAmount()
        {
            //arrange
            var textValue = "-999,999,999,999.99";
            var expected = "-Nine Hundred Ninety-Nine Billion Nine Hundred Ninety-Nine Million Nine Hundred Ninety-Nine Thousand Nine Hundred Ninety-Nine & 99/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_HandleWholeNumber()
        {
            //arrange
            var textValue = "999,999,999,999";
            var expected = "Nine Hundred Ninety-Nine Billion Nine Hundred Ninety-Nine Million Nine Hundred Ninety-Nine Thousand Nine Hundred Ninety-Nine & 00/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_HandleNegativeWholeNumber()
        {
            //arrange
            var textValue = "-999,999,999,999";
            var expected = "-Nine Hundred Ninety-Nine Billion Nine Hundred Ninety-Nine Million Nine Hundred Ninety-Nine Thousand Nine Hundred Ninety-Nine & 00/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Will_RoundUpAmount_WhenMoreThanTwoDecimalPlaces()
        {
            //arrange
            var textValue = "-999,999,999,999.995";
            var expected = InvalidLegalAmountMessage;

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseZeroTensAmountDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "400.78";
            var expected = "Four Hundred & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseTenAmountDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "10";
            var expected = "Ten & 00/100";

            //assert
            AssertLegalAmount(textValue, expected);
        }

        [TestMethod]
        public void Can_ParseZeroHundredsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "4,000.78";
            var expected = "Four Thousand & 78/100";

            var textValue2 = "4,001.78";
            var expected2 = "Four Thousand One & 78/100";

            var textValue3 = "4,011.78";
            var expected3 = "Four Thousand Eleven & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
            AssertLegalAmount(textValue2, expected2);
            AssertLegalAmount(textValue3, expected3);
        }

        [TestMethod]
        public void Can_ParseZeroThousandsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "40,000.78";
            var expected = "Forty Thousand & 78/100";

            var textValue2 = "40,001.78";
            var expected2 = "Forty Thousand One & 78/100";

            var textValue3 = "40,011.78";
            var expected3 = "Forty Thousand Eleven & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
            AssertLegalAmount(textValue2, expected2);
            AssertLegalAmount(textValue3, expected3);
        }

        [TestMethod]
        public void Can_ParseZeroTenThousandsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "200,000.78";
            var expected = "Two Hundred Thousand & 78/100";

            var textValue2 = "201,000.78";
            var expected2 = "Two Hundred One Thousand & 78/100";

            var textValue3 = "201,001.78";
            var expected3 = "Two Hundred One Thousand One & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
            AssertLegalAmount(textValue2, expected2);
            AssertLegalAmount(textValue3, expected3);
        }

        [TestMethod]
        public void Can_ParseZeroHundredThousandsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "3,000,000.78";
            var expected = "Three Million & 78/100";

            var textValue2 = "3,001,000.78";
            var expected2 = "Three Million One Thousand & 78/100";

            var textValue3 = "3,014,000.78";
            var expected3 = "Three Million Fourteen Thousand & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
            AssertLegalAmount(textValue2, expected2);
            AssertLegalAmount(textValue3, expected3);
        }

        [TestMethod]
        public void Can_ParseZeroMillionsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "10,000,000.78";
            var expected = "Ten Million & 78/100";

            var textValue2 = "20,001,000.78";
            var expected2 = "Twenty Million One Thousand & 78/100";

            var textValue3 = "20,015,001.78";
            var expected3 = "Twenty Million Fifteen Thousand One & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
            AssertLegalAmount(textValue2, expected2);
            AssertLegalAmount(textValue3, expected3);
        }

        [TestMethod]
        public void Can_ParseZeroTenMillionsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "200,000,000.78";
            var expected = "Two Hundred Million & 78/100";

            var textValue2 = "201,001,000.78";
            var expected2 = "Two Hundred One Million One Thousand & 78/100";

            var textValue3 = "202,015,002.78";
            var expected3 = "Two Hundred Two Million Fifteen Thousand Two & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
            AssertLegalAmount(textValue2, expected2);
            AssertLegalAmount(textValue3, expected3);
        }

        [TestMethod]
        public void Can_ParseZeroHundredMillionsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "6,000,000,000.78";
            var expected = "Six Billion & 78/100";

            var textValue2 = "7,001,001,000.78";
            var expected2 = "Seven Billion One Million One Thousand & 78/100";

            var textValue3 = "8,019,401,302.78";
            var expected3 = "Eight Billion Nineteen Million Four Hundred One Thousand Three Hundred Two & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
            AssertLegalAmount(textValue2, expected2);
            AssertLegalAmount(textValue3, expected3);
        }

        [TestMethod]
        public void Can_ParseZeroBillionsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "50,000,000,000.78";
            var expected = "Fifty Billion & 78/100";

            var textValue2 = "70,001,001,000.78";
            var expected2 = "Seventy Billion One Million One Thousand & 78/100";

            var textValue3 = "80,019,401,302.78";
            var expected3 = "Eighty Billion Nineteen Million Four Hundred One Thousand Three Hundred Two & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
            AssertLegalAmount(textValue2, expected2);
            AssertLegalAmount(textValue3, expected3);
        }

        [TestMethod]
        public void Can_ParseZeroTenBillionsPlaceDecimal_ToLegalAmount()
        {
            //arrange
            var textValue = "500,000,000,000.78";
            var expected = "Five Hundred Billion & 78/100";

            var textValue2 = "701,001,001,000.78";
            var expected2 = "Seven Hundred One Billion One Million One Thousand & 78/100";

            var textValue3 = "809,019,401,302.78";
            var expected3 = "Eight Hundred Nine Billion Nineteen Million Four Hundred One Thousand Three Hundred Two & 78/100";

            //assert
            AssertLegalAmount(textValue, expected);
            AssertLegalAmount(textValue2, expected2);
            AssertLegalAmount(textValue3, expected3);
        }

        [TestMethod]
        public void Can_CreateRectangle_ForLegalAmountLessThan81Characters()
        {
            //arrange
            var upperLeftXCoordinate = 30;
            var upperLeftYCoordinate = 155;
            var imageWidth = 722;
            var expectedXCoordinate = upperLeftXCoordinate;
            var expectedYCoordinate = upperLeftYCoordinate;
            var expectedRectangleWidth = imageWidth - upperLeftXCoordinate;
            var expectedHeight = upperLeftYCoordinate;
            var legalAmount = "Nine Hundred Billion Six Hundred Nine Million Nine Hundred Sixty-Six & 00/100";

            //act
            var actual = legalAmount.ToLegalAmountRectangle(upperLeftXCoordinate, upperLeftYCoordinate, imageWidth);

            //assert
            Assert.AreEqual(expectedXCoordinate, actual.X);
            Assert.AreEqual(expectedYCoordinate, actual.Y);
            Assert.AreEqual(expectedHeight, actual.Height);
            Assert.AreEqual(expectedRectangleWidth, actual.Width);
        }

        [TestMethod]
        public void Can_CreateRectangle_ForNullLegalAmount()
        {
            //arrange
            var upperLeftXCoordinate = 30;
            var upperLeftYCoordinate = 155;
            var imageWidth = 722;
            var expectedXCoordinate = upperLeftXCoordinate;
            var expectedYCoordinate = upperLeftYCoordinate;
            var expectedRectangleWidth = imageWidth - upperLeftXCoordinate;
            var expectedHeight = upperLeftYCoordinate;
            string legalAmount = null;

            //act
            var actual = legalAmount.ToLegalAmountRectangle(upperLeftXCoordinate, upperLeftYCoordinate, imageWidth);

            //assert
            Assert.AreEqual(expectedXCoordinate, actual.X);
            Assert.AreEqual(expectedYCoordinate, actual.Y);
            Assert.AreEqual(expectedHeight, actual.Height);
            Assert.AreEqual(expectedRectangleWidth, actual.Width);
        }

        [TestMethod]
        public void Can_CreateRectangle_ForLegalAmountMoreThan80Characters()
        {
            //arrange
            var upperLeftXCoordinate = 30;
            var upperLeftYCoordinate = 155;
            var imageWidth = 722;
            var expectedXCoordinate = upperLeftXCoordinate - 5;
            var expectedYCoordinate = upperLeftYCoordinate - 10;
            var expectedRectangleWidth = (imageWidth - upperLeftXCoordinate) - 145;
            var expectedHeight = upperLeftYCoordinate;
            var legalAmount = "Nine Hundred One Billion Six Hundred Nine Million Nine Hundred Sixty-Six & 00/100";

            //act
            var actual = legalAmount.ToLegalAmountRectangle(upperLeftXCoordinate, upperLeftYCoordinate, imageWidth);

            //assert
            Assert.AreEqual(expectedXCoordinate, actual.X);
            Assert.AreEqual(expectedYCoordinate, actual.Y);
            Assert.AreEqual(expectedHeight, actual.Height);
            Assert.AreEqual(expectedRectangleWidth, actual.Width);

        }

        private void AssertLegalAmount(string textAmount, string expected)
        {
            //arrange
            var decimalValue = Convert.ToDecimal(textAmount);

            //act
            var actual = decimalValue.ToLegalAmount().Text;

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
