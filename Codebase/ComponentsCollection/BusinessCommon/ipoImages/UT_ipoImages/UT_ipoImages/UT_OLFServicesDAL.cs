﻿using System;
using System.Data;
using System.IO;
using System.Text;
using WFS.RecHub.Common;
using WFS.RecHub.DAL.OLFServicesDAL;

namespace ipoImages_UnitTest {
    class UT_OLFServicesDAL:IOLFServicesDAL {
        #region IOLFServicesDAL Members

        public UT_OLFServicesDAL() {
            GetDataEntryDetails_XMLData = "";
        }

        public string GetDataEntryDetails_XMLData {
            get;
            set;
        }

        public bool GetCheck(Guid SessionID, int BankID, int CleintAccountID, long BatchID, DateTime DepositDate, int TransactionID, int BatchSequence, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetCheck(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetCheckDetails(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetCheckImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetCheckImagePathInfo(long BatchID, int BatchSequence, string FileDescriptor, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetDataEntryDetails(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out System.Data.DataTable dt) {
            bool result = false;

            if (GetDataEntryDetails_XMLData == "") {
                result = false;
                dt = null;
            }
            else {
                MemoryStream xmlStream = new MemoryStream(Encoding.ASCII.GetBytes(GetDataEntryDetails_XMLData));
                DataSet dsData=new DataSet();
                
                result = true;
                dsData.ReadXml(xmlStream);
                dt = dsData.Tables[0];
            }
            return result;
        }

        public bool GetDocument(Guid SessionID, int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, int TransactionID, int BatchSequence, bool DisplayScannedCheck, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetDocument(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetDocumentImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetImageInfo(Guid SessionID, int BankID, int ClientAccountID, int ImmutableDateKey, long BatchID, int BatchSequence, int DepositDateKey, WFS.RecHub.Common.TableTypes TableType, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetLockbox(int BankID, int ClientAccountID, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetLockboxOnlineColorMode(int BankID, int ClientAccountID, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetPaymentTypeKey(int BankID, int ClientAccountID, DateTime DepositDate, long BatchID, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetRawPaymentData(long BatchID, DateTime DepositDate, int BatchSequence, bool isCheck, out System.Data.DataTable dt) {
            throw new NotImplementedException();
        }

        public ImageDisplayModes GetImageDisplayModes(int siteBankId, int siteWorkgroupId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDisposable Members

        public void Dispose() {
        }

        #endregion
    }
}
