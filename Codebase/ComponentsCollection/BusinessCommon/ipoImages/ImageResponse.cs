﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.Common
{
    public class ImageResponse
    {
        public bool IsSuccessful { get; set; }
        public string FileType { get; set; }
        public byte[] ImageBytes { get; set; }
    }
}
