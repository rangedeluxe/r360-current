﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.Common
{
    public class ParsedAddendaComparer : IEqualityComparer<cParsedAddenda>
    {
        public bool Equals(cParsedAddenda x, cParsedAddenda y)
        {
            return x.BatchSequence == y.BatchSequence && 
                x.DisplayName == y.DisplayName;
        }

        public int GetHashCode(cParsedAddenda obj)
        {
            return obj.DisplayName.GetHashCode() ^ obj.BatchSequence.GetHashCode();
        }
    }
}
