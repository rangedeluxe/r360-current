﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WFS.RecHub.Common
{
    public static class PdfUtility
    {
        const string DefaultFontName = "Calibri (Body)";
        const string NormalStyle = "Normal";
        const string ACHHeading1Style = "ACHHeading1";
        const string ACHHeading2Style = "ACHHeading2";
        const string RawDataStyle = "RawData";
        const string ParsedAddendaStyle = "ParsedAddenda";
        const string LabelStyle = "Label";
        const string ValueStyle = "Value";

        public static Document GetDocumentTemplate()
        {
            //set up my document
            var document = new Document();
            document = document.ValidateDocument();

            document.DefaultPageSetup.TopMargin = Unit.FromInch(.25);
            document.DefaultPageSetup.BottomMargin = Unit.FromInch(.25);
            document.DefaultPageSetup.LeftMargin = Unit.FromInch(.50);
            document.DefaultPageSetup.RightMargin = Unit.FromInch(.50);
            document.DefaultPageSetup.PageHeight = Unit.FromInch(11);
            document.DefaultPageSetup.PageWidth = Unit.FromInch(8.5);
            document.DefaultPageSetup.Orientation = Orientation.Portrait;

            document.DefineStyles();//define the styles for the document

            return document;
        }

        public static byte[] ToACHPaymentPdfBytes(this cImageData imageData, IItem request)
        {
            if (imageData == null)
                throw new ArgumentNullException("The 'imageData' cannot be null");

            if (request == null)
                throw new ArgumentNullException("The 'request' cannot be null");

            //Raw Payment Data
            var rawPaymentData = imageData.LoadRawPaymentData(request, BatchPaymentType.ACH);
            
            //set up my document
            var document = GetDocumentTemplate();
            
            document.LastSection.AddParagraph(ipoImageCommon.ACH_PAYMENT_HEADING, ACHHeading1Style);
            document.AddACHDataEntryInformation(imageData);//add data entry information
            document.AddACHRawAddenda(rawPaymentData);//add the raw addenda information
            document.AddACHParsedAddenda(imageData, imageData.ParsedAddendas);//add parsed addenda information

            var pdfRenderer = new PdfDocumentRenderer();
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();

            // Save the pdf document to a memory stream...
            var memoryStream = new MemoryStream();
            pdfRenderer.PdfDocument.Save(memoryStream, false);

            return memoryStream.ToArray();//return the stream as a byte array
        }

        public static string SavePdf(this byte[] pdfBytes, string outputFile)
        {
            using (var stream = new MemoryStream(pdfBytes))
            {
                using (var pdfDocument = PdfReader.Open(stream))
                {
                    if (!outputFile.EndsWith(".pdf", StringComparison.CurrentCultureIgnoreCase))
                        outputFile += ".pdf";

                    pdfDocument.Save(outputFile);
                }
            }

            return outputFile;
        }

        public static Document ValidateDocument(this Document document)
        {
            if (document == null)
                document = GetDocumentTemplate();

            if (document.LastSection == null)
                document.AddSection();//pdf document needs at least one section

            return document;
        }

        public static void DefineStyles(this Document document)
        {
            document = document.ValidateDocument();

            // Get the predefined style Normal.
            var style = document.Styles[NormalStyle];
            // Because all styles are derived from Normal, the next line changes the
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Calibri (Body)";
            style.Font.Size = Unit.FromPoint(9);

            //create an ACHHeading1 style
            style = document.Styles.AddStyle(ACHHeading1Style, NormalStyle);
            style.ParagraphFormat.SpaceBefore = Unit.FromPoint(8);
            style.ParagraphFormat.SpaceAfter = Unit.FromPoint(8);
            style.ParagraphFormat.Font.Bold = true;
            style.ParagraphFormat.Font.Size = Unit.FromPoint(13);

            //create an ACHHeading2 style
            style = document.Styles.AddStyle(ACHHeading2Style, NormalStyle);
            style.ParagraphFormat.SpaceBefore = Unit.FromPoint(8);
            style.ParagraphFormat.SpaceAfter = Unit.FromPoint(8);
            style.ParagraphFormat.Font.Underline = Underline.Single;
            style.ParagraphFormat.Font.Bold = true;
            style.ParagraphFormat.Font.Size = Unit.FromPoint(11);

            //create a RawData style
            style = document.Styles.AddStyle(RawDataStyle, NormalStyle);
            style.Font.Name = "Courier New";
            style.Font.Size = Unit.FromPoint(9);

            //create a Label style
            style = document.Styles.AddStyle(LabelStyle, NormalStyle);
            style.ParagraphFormat.Alignment = ParagraphAlignment.Right;
            style.Font.Bold = true;
            style.Font.Size = Unit.FromPoint(9);
        }
        
        public static void AddACHRawAddenda(this Document document, ArrayList rawPaymentData)
        {
            document = document.ValidateDocument();

            if (rawPaymentData == null)
                throw new ArgumentNullException("The 'rawPaymentData' cannot be null");

            document.LastSection.AddParagraph(ipoImageCommon.ACH_RAW_ADDENDA, ACHHeading2Style);
            
            //Raw Payment Data
            foreach (string rawPayment in rawPaymentData)
            {
                document.AddParagraphWithNoBreakSpaces(rawPayment.Trim(), RawDataStyle);
            }
        }
        
        public static void AddPaymentInformation(this Document document, cImageData imageData)
        {
            document = document.ValidateDocument();

            var table = document.AddACHTable();//create an ACH table

            var row = table.AddRow();//create header row
            row.Cells[0].AddCellText("Payment Information", ACHHeading2Style);
            row.Cells[0].Format.LeftIndent = Unit.FromInch(.25);

            table.AddRow();//create empty row between heading and data

            row = table.AddRow();//create BPR Monetary Amount row
            row.Cells[0].AddCellText("BPR Monetary Amount:", LabelStyle);//BPR Monetary Amount
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_BPR_MONETARY_AMOUNT).ToCurrency());
            row.Cells[2].AddCellText("Reassociation Trace Number:", LabelStyle);//Reassociation Trace Number
            row.Cells[3].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_REASSOCIATION_TRACE_NUMBER));

            row = table.AddRow();//create BPR Account Number row
            row.Cells[0].AddCellText("BPR Account Number:", LabelStyle);//BPR Account Number
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_BPR_ACCOUNT_NUMBER));
        }

        public static void AddACHParsedAddenda(this Document document, cImageData imageData, cParsedAddendaCollection parsedAddendas)
        {
            document = document.ValidateDocument();

            if (imageData == null)
                throw new ArgumentNullException("The 'imageData' cannot be null");

            if (parsedAddendas == null)
                throw new ArgumentNullException("The 'parsedAddendas' cannot be null");

            document.LastSection.AddParagraph(ipoImageCommon.ACH_PARSED_ADDENDA, ACHHeading2Style);

            document.AddPaymentInformation(imageData);

            var table = document.AddACHTable();//create an ACH table

            var row = table.AddRow();//create header row
            row.Cells[0].AddCellText("Remittance Detail", ACHHeading2Style);
            row.Cells[0].Format.LeftIndent = Unit.FromInch(.25);
            
            ArrayList batchSequenceList = parsedAddendas.GetDistinctBatchSequence();
            batchSequenceList.Sort();

            foreach (int batchSequence in batchSequenceList)
            {
                table.AddACHParsedAddenda(parsedAddendas, batchSequence);
            }
        }

        public static void AddACHParsedAddenda(this Row row, cParsedAddenda parsedAddenda, bool putInSecondColumn = false)
        {
            if (row == null)
                throw new ArgumentNullException("The 'row' cannot be null");

            if (parsedAddenda == null)
                throw new ArgumentNullException("The 'parsedAddenda' cannot be null");

            int labelColumn = 0;
            int valueColumn = 1;

            if (putInSecondColumn)
            {
                labelColumn = 2;
                valueColumn = 3;
            }
                
            row.Cells[labelColumn].AddCellText($"{parsedAddenda.UILabel}:", LabelStyle);
            row.Cells[valueColumn].AddCellText(parsedAddenda.Format().FieldValue);
            row.VerticalAlignment = VerticalAlignment.Top;
        }

        public static Table AddACHParsedAddenda(this Table table, cParsedAddendaCollection parsedAddendas, int batchSequence)
        {
            if (parsedAddendas == null)
                throw new ArgumentNullException("The 'parsedAddendas' cannot be null");

            if (table == null)
                table = new Table();

            if (table.Columns.Count == 0)
                table.AddACHColumns();

            var rows = new List<Row>();
            
            var distinctParsedAddendas = parsedAddendas.GetDistinctParsedAddendas(batchSequence) 
                .OrderBy(addenda => addenda.UILabel);//order by ui label

            var addendaCount = distinctParsedAddendas.Count();//determine how many there are
            int remainder = 0;
            var numberToTake = Math.DivRem(addendaCount, 2, out remainder);

            if (remainder > 0)
                numberToTake++;
            
            var column1AddendaItems = distinctParsedAddendas.Take(numberToTake);//column 1 gets the first half
            var column2AddendaItems = distinctParsedAddendas.Skip(numberToTake);//column 2 gets the second half

            Row row;

            column1AddendaItems.ToList().ForEach(item =>//process all column 1 addenda items
            {
                row = table.AddRow();//add a new row
                row.AddACHParsedAddenda(item);//add the parsed addenda data to the row
                rows.Add(row);//add the row to the collection of rows to be used below
            });

            var rowNumber = 0;//reset row number so we put column 2's addenda items in the right rows
            column2AddendaItems.ToList().ForEach(item =>//process all column 2 addenda items
            {
                rows[rowNumber].AddACHParsedAddenda(item, true);//add the parsed addenda data to the row
                rowNumber++;
            });

            row = table.AddRow();
            row.Cells[0].AddCellText(batchSequence.ToString());
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            rows.Add(row);

            rows.First().KeepWith = rows.Count - 1;//this will keep the rows together if a page break is encountered
            rows.Last().Borders.Bottom.Visible = true;

            return table;
        }

        public static void AddACHDataEntryInformation(this Document document, cImageData imageData)
        {
            if (imageData == null)
                throw new ArgumentNullException("The 'imageData' cannot be null");

            document = document.ValidateDocument();

            var table = document.AddACHTable();//create an ACH table

            var row = table.AddRow();//create header row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_BATCH_INFORMATION, ACHHeading2Style);
            row.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[2].AddCellText(ipoImageCommon.ACH_ENTRY_DETAIL, ACHHeading2Style);
            row.Cells[2].Format.Alignment = ParagraphAlignment.Right;

            table.AddRow();//create empty row between heading and data

            row = table.AddRow();//create batch number and transaction code row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_BATCH_NUMBER, LabelStyle);//Batch Number
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_ACH_BATCH_NUMBER));
            row.Cells[2].AddCellText(ipoImageCommon.ACH_TRANSACTION_CODE, LabelStyle);//Transaction Code
            row.Cells[3].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_ELECTRONIC_TRANS_CD));

            row = table.AddRow();//create company id and account number row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_COMPANY_ID, LabelStyle);//Company ID
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_COMPANY_ID));
            row.Cells[2].AddCellText(ipoImageCommon.ACH_ACCOUNT_NUMBER, LabelStyle);//Account Number
            row.Cells[3].AddCellText(imageData.CheckFields.GetFieldValue(ipoImageCommon.CHECKS_DDA));

            row = table.AddRow();//create company name and amount row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_COMPANY_NAME, LabelStyle);//Company Name
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_COMPANY_NAME));
            row.Cells[2].AddCellText(ipoImageCommon.ACH_AMOUNT, LabelStyle);//Amount
            row.Cells[3].AddCellText(imageData.CheckFields.GetFieldValue(ipoImageCommon.CHECKS_AMOUNT).ToCurrency());

            row = table.AddRow();//create entry class and individual id row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_ENTRY_CLASS, LabelStyle);//Entry Class
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_STANDARD_ENTRY_CLASS));
            row.Cells[2].AddCellText(ipoImageCommon.ACH_INDIVIDUAL_ID, LabelStyle);//Individual ID
            row.Cells[3].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_INDIVIDUAL_ID));

            row = table.AddRow();//create entry description and receiving company row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_ENTRY_DESCRIPTION, LabelStyle);//Entry Description
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_ENTRY_DESCRIPTION));
            row.Cells[2].AddCellText(ipoImageCommon.ACH_RECEIVING_COMPANY, LabelStyle);//Receiving Company
            row.Cells[3].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_RECEIVING_COMPANY));

            row = table.AddRow();//create company data and trace number row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_COMPANY_DATA, LabelStyle);//Company Data
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_COMPANY_DATA));
            row.Cells[2].AddCellText(ipoImageCommon.ACH_TRACE_NUMBER, LabelStyle);//Trace Number
            row.Cells[3].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_TRACE_NUMBER));

            row = table.AddRow();//create descriptive date row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_DESCRIPTIVE_DATE, LabelStyle);//Descriptive Date
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_DESCRIPTIVE_DATE));

            row = table.AddRow();//create effective date row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_EFFECTIVE_DATE, LabelStyle);//Effective Date
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_EFFECTIVE_DATE));

            row = table.AddRow();//create settlement date row
            row.Cells[0].AddCellText(ipoImageCommon.ACH_SETTLEMENT_DATE, LabelStyle);//Settlement Date
            row.Cells[1].AddCellText(imageData.Fields.GetFieldValue(ipoImageCommon.CHECKS_DATA_ENTRY_SETTLEMENT_DATE));
        }

        public static void AddParagraphWithNoBreakSpaces(this Document document, string text, string style)
        {
            document = document.ValidateDocument();

            //Need to replace the space characters with no-break spaces, because MigraDoc
            //treats multiple spaces as one
            //(http://www.pdfsharp.net/wiki/MigraDoc_Spaces.ashx)
            text = text.Replace(' ', Chars.NonBreakableSpace);

            if (!string.IsNullOrEmpty(style))
                document.LastSection.AddParagraph(text, style);
            else
                document.LastSection.AddParagraph(text);
        }

        public static Table AddACHTable(this Document document)
        {
            document = document.ValidateDocument();

            if (document.LastSection == null)
                document.AddSection();

            var table = document.LastSection.AddTable();//create a table
            table.AddACHColumns();

            return table;
        }

        public static void AddACHColumns(this Table table)
        {
            if (table == null)
                table = new Table();

            //define the columns
            var column = table.AddColumn(Unit.FromInch(1.875));
            column.Format.Alignment = ParagraphAlignment.Right;

            column = table.AddColumn(Unit.FromInch(1.875));
            column.Format.Alignment = ParagraphAlignment.Left;

            column = table.AddColumn(Unit.FromInch(1.875));
            column.Format.Alignment = ParagraphAlignment.Right;

            column = table.AddColumn(Unit.FromInch(1.875));
            column.Format.Alignment = ParagraphAlignment.Left;
        }

        public static void AddCellText(this Cell cell, string value)
        {
            cell.AddCellText(value, string.Empty);
        }

        public static SizeF SizeOfText(this Style style, string value, UnitType unitType)
        {
            //if the style isn't found, we won't be able to measure, so return empty
            if (style == null)
                return SizeF.Empty;

            //if the style doesn't have a font size, we won't be able to measure, so return empty
            if (style.Font.Size.IsEmpty)
                return SizeF.Empty;

            TextMeasurement measurement = new TextMeasurement(style.Font);
            return measurement.MeasureString(value, unitType);//measure the string
        }

        public static Style AddStyle(this Cell cell, string style)
        {
            if (cell == null)
                throw new ArgumentNullException("The 'cell' cannot be null");

            var cellStyle = cell.Document.GetStyle(style);
            cell.Style = cellStyle.Name;

            return cellStyle;
        }

        public static Style GetStyle(this Document document, string style)
        {
            document = document.ValidateDocument();
            //if no style is supplied, then default to normal style for the document
            if (string.IsNullOrEmpty(style))
                return document.Styles.Normal;

            var indexOfStyle = document.Styles.GetIndex(style);
            if (indexOfStyle == -1)
                return document.Styles.Normal;

            return document.Styles[indexOfStyle] as Style;
        }

        public static Cell AddCellText(this Cell cell, string value, string style, int breakLongTextAfterThisManyCharacters = 20)
        {
            var styleToApply = cell.AddStyle(style);
            cell.AddParagraph(cell.FormatText(value, styleToApply));
            return cell;
        }

        public static string FormatText(this Cell cell, string value, Style style, int breakLongTextAfterThisManyCharacters = 20)
        {
            //if the cell or column are null, we'll be unable to measure it against the text, 
            //so just return the value
            if (cell == null)
                return value;

            if (cell.Table == null)
                return value;

            
            var sizeOfTextInPoints = style.SizeOfText(value, UnitType.Point);

            //if we couldn't figure out the size, then we can't measure against the column,
            //so just return the value
            if (sizeOfTextInPoints.IsEmpty)
                return value;

            //MigraDoc automatically breaks on spaces, hyphens, and soft-hyphens so...
            if (sizeOfTextInPoints.Width > cell.Column.Width.Point)//see if text is bigger than the column
            {
                return value.FormatText(breakLongTextAfterThisManyCharacters);
            }

            return value;
        }

        public static string FormatText(this string value, int breakLongTextAfterThisManyCharacters = 20)
        {
            var breakCharacter = Chars.LF;//making the break character a line feed
            StringBuilder valueWithBreaks = new StringBuilder();
            for (int i = 0; i < value.Length; i += breakLongTextAfterThisManyCharacters)
            {
                var length = Math.Min(value.Length - i, breakLongTextAfterThisManyCharacters);
                var text = value.Substring(i, length);

                valueWithBreaks.Append
                (
                    Regex.Replace//user regular expression to apply the break character every x number of characters
                    (
                        text, $".{{{breakLongTextAfterThisManyCharacters}}}", $"$0{breakCharacter}")
                    );
            }

            return valueWithBreaks.ToString();
        }

        public static string ToCurrency(this string value)
        {
            decimal valueAsDecimal;

            if (!decimal.TryParse(value, out valueAsDecimal))
                return value;

            return valueAsDecimal.ToString("C");
        }

        public static string GetFieldValue(this IDictionary<string, string> dictionary, string field)
        {
            if (dictionary == null)
                throw new ArgumentNullException("The 'dictionary' cannot be null");

            string value;
            if (dictionary.TryGetValue(field, out value))
                return value;

            return string.Empty;
        }

        public static cParsedAddenda Format(this cParsedAddenda parsedAddenda)
        {
            if (parsedAddenda == null)
                throw new ArgumentNullException("The 'parsedAddenda' cannot be null");

            if (parsedAddenda.DataType == 7)
                parsedAddenda.FieldValue = parsedAddenda.FieldValue.ToCurrency();

            return parsedAddenda;
        }

    }
}
