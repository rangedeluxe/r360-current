﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common;

namespace WFS.RecHub.Common
{
    public static class Extensions
    {
        public static LegalAmount ToLegalAmount(this decimal decimalAmount)
        {
            return new LegalAmount(decimalAmount);
        }

        public static RectangleF ToLegalAmountRectangle(this string legalAmountText, int upperLeftXCoordinate, 
            int upperLeftYCoordinate, int imageWidth)
        {
            if (string.IsNullOrEmpty(legalAmountText)) { legalAmountText = string.Empty; }

            var height = upperLeftYCoordinate;
            var rectangleWidth = imageWidth - upperLeftXCoordinate;

            if (legalAmountText.Length > 80)
            {
                upperLeftXCoordinate = upperLeftXCoordinate - 5; //move left
                upperLeftYCoordinate = upperLeftYCoordinate - 10;//move up so we can fit a wrapped text line
                rectangleWidth = rectangleWidth - 145;//shrink the rectangle more
            }

            return new RectangleF(upperLeftXCoordinate, upperLeftYCoordinate, rectangleWidth, height);
        }
    }
}
