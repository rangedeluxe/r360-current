using System;
using System.IO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Jason Efken
* Date: 09/28/2012
*
* Purpose: ipoFILEGROUPImages contains FILEGROUPs specific getrelativeimages and putbatchimage.
*
* Modification History
* WI 90246 CRG 03/18/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoImages
* WI 156594 SAS 07/31/2014 
*   - Added Batch Source Short Name to the image relative path.        
* WI  166623  SAS 09/17/2014 
*   - Changes done to retrieve Image Path
* WI  173874  SAS 10/23/2014 
*   - Changes done for retrieving images
* WI 175199 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName 
* WI 176352 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName     
******************************************************************************/
namespace WFS.RecHub.Common
{
    public class ipoFILEGROUPImages : _ipoPICSImages
    {

        public ipoFILEGROUPImages(string sImageRootPath): base(sImageRootPath)
        {
            m_sImageRootPath = sImageRootPath;
        }

        /// <summary>
        /// WI 156594 SAS 07/31/2014  Changes done to add source name in relative path
        /// </summary>
        /// <param name="picsDate"></param>
        /// <param name="bankID"></param>
        /// <param name="lockboxID"></param>
        /// <param name="batchID"></param>
        /// <param name="batchSequence"></param>
        /// <param name="fileDescriptor"></param>
        /// <param name="workgroupColorMode"></param>
        /// <param name="page"></param>
        /// <param name="sourceBatchID"></param>
        /// <param name="batchSourceShortName"></param>
        /// <param name="importTypeShortName"></param>
        /// <returns></returns>
        public override string getRelativeImagePath(string picsDate,
                                           int bankID,
                                           int lockboxID,
                                           long batchID,
                                           int batchSequence,
                                           string fileDescriptor,
                                           WorkgroupColorMode workgroupColorMode,
                                           int page,
                                           long sourceBatchID,
                                           string batchSourceShortName,
                                           string importTypeShortName)
        {

            string strImagePath=string.Empty;
            string strImageType;
            string strRetVal = "";            
            try
            {  

                // Construct path for image
                if (pathNeedsPaymentSource(importTypeShortName))
                    strImagePath = Path.Combine(strImagePath, batchSourceShortName);
                                
                // Construct path for image                
                strImagePath = Path.Combine(strImagePath, bankID.ToString());
                strImagePath = Path.Combine(strImagePath, lockboxID.ToString());
                strImagePath = Path.Combine(strImagePath, picsDate );
                strImagePath = Path.Combine(strImagePath, sourceBatchID.ToString() + "_"
                                                                + batchSequence.ToString() + "_"
                                                                + fileDescriptor + "_");
                
              
                    switch (workgroupColorMode)
                    {
                        case WorkgroupColorMode.COLOR_MODE_BITONAL:
                            strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_BITONAL, page);
                            if (File.Exists(Path.Combine(m_sImageRootPath, strImagePath + strImageType + ".tif")))
                            {
                                strRetVal = strImagePath + strImageType + ".tif";
                            }
                            else
                            {
                                strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_GRAYSCALE, page);
                                if (File.Exists(Path.Combine(m_sImageRootPath, strImagePath + strImageType + ".tif")))
                                {
                                    strRetVal = strImagePath + strImageType + ".tif";
                                }
                                else
                                {
                                    strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_COLOR, page);
                                    if (File.Exists(Path.Combine(m_sImageRootPath, strImagePath + strImageType + ".tif")))
                                    {
                                        strRetVal = strImagePath + strImageType + ".tif";
                                    }
                                    else
                                    {
                                        strRetVal = "";
                                    }
                                }
                            }
                            break;
                        case WorkgroupColorMode.COLOR_MODE_COLOR:
                            strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_COLOR, page);
                            if (File.Exists(Path.Combine(m_sImageRootPath, strImagePath + strImageType + ".tif")))
                            {
                                strRetVal = strImagePath + strImageType + ".tif";
                            }
                            else
                            {
                                strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_GRAYSCALE, page);
                                if (File.Exists(Path.Combine(m_sImageRootPath, strImagePath + strImageType + ".tif")))
                                {
                                    strRetVal = strImagePath + strImageType + ".tif";
                                }
                                else
                                {
                                    strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_BITONAL, page);
                                    if (File.Exists(Path.Combine(m_sImageRootPath, strImagePath + strImageType + ".tif")))
                                    {
                                        strRetVal = strImagePath + strImageType + ".tif";
                                    }
                                    else
                                    {
                                        strRetVal = "";
                                    }
                                }
                            }
                            break;
                        case WorkgroupColorMode.COLOR_MODE_GRAYSCALE:
                            strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_GRAYSCALE, page);
                            if (File.Exists(Path.Combine(m_sImageRootPath, strImagePath + strImageType + ".tif")))
                            {
                                strRetVal = strImagePath + strImageType + ".tif";
                            }
                            else
                            {
                                strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_COLOR, page);
                                if (File.Exists(Path.Combine(m_sImageRootPath, strImagePath + strImageType + ".tif")))
                                {
                                    strRetVal = strImagePath + strImageType + ".tif";
                                }
                                else
                                {
                                    strImageType = getImageType(WorkgroupColorMode.COLOR_MODE_BITONAL, page);
                                    if (File.Exists(Path.Combine(m_sImageRootPath, strImagePath + strImageType + ".tif")))
                                    {
                                        strRetVal = strImagePath + strImageType + ".tif";
                                    }
                                    else
                                    {
                                        strRetVal = "";
                                    }
                                }
                            }
                            break;
                        default:
                            strRetVal = "";
                            break;
                    }
                }
            catch (Exception e)
            {
                strRetVal = "";
                eventLog.logError(e);
            }
            return (strRetVal);
        }

        public override bool PutBatchImage(string siteKey, ILTAImageInfo imageInfo, byte[] batchImage){

            bool bRetVal = false;
            cFILEGROUP filegroup = new cFILEGROUP(siteKey);
            bRetVal = filegroup.PutBatchImage(m_sImageRootPath, imageInfo, batchImage);
            return bRetVal;
        }


        public string GetImagePath(string importTypeShortName,
                                   string batchPaymentSourceName,
                                   string strImageRootPath,
                                   string strProcessingDate,
                                   int iBankID,
                                   int iLockboxID)
        {
            string strImagePath = strImageRootPath;
            // Modify the root path, if needed
            if (pathNeedsPaymentSource(importTypeShortName))
                strImagePath = Path.Combine(strImagePath, batchPaymentSourceName);
            return (CombineImagePath(strImagePath, strProcessingDate, iBankID, iLockboxID));
        }

        protected string CombineImagePath(string strImageRootPath, string strProcessingDate, int iBankID, int iLockboxID)
        {
            string strImagePath = string.Empty;

            strImagePath = Path.Combine(strImageRootPath, iBankID.ToString());
            strImagePath = Path.Combine(strImagePath, iLockboxID.ToString());
            strImagePath = Path.Combine(strImagePath, strProcessingDate);
            return strImagePath;
        }
    }
}
