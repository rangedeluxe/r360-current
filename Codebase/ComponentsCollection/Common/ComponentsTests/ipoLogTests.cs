﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common.Log;
using WFS.RecHub.Common.Log.Fakes;
using System.IO;
using System.Text;
using WFS.RecHub.Common;
using Microsoft.QualityTools.Testing.Fakes;

namespace ComponentsTests
{
	[TestClass]
	public class ipoLogTests
	{
		[TestMethod]
		public void cEventLog_LogNewExceptionSourceProcWithoutTargetSite_MessageLogged()
		{
			// Arrange - test data
			string exMessage = "Test Exception";
			Exception ex = new Exception(exMessage);
			string exSource = this.GetType().Name;
			string exProc = "TestMethod";

			// Arrage - get access to a log
			cEventLog logger = new cEventLog(string.Empty, 0, MessageImportance.Essential);

			// Arrange - Faked log implementation
			StringBuilder sbLog = new StringBuilder();
			StringWriter swLog = new StringWriter(sbLog);
			using (ShimsContext.Create())
			{
				ShimcEventLogImpl.AllInstances.getLogFileTextWriterOut = (cEventLogImpl log, out TextWriter tw) => tw = swLog;

				// Act
				logger.logError(ex, exSource, exProc);
			}

			// Assert
			string messages = sbLog.ToString();
			Assert.IsTrue(messages.Contains(exMessage));
			Assert.IsTrue(messages.Contains(exSource));
			Assert.IsTrue(messages.Contains(exProc));
		}

		[TestMethod]
		public void cEventLog_LogNewExceptionSourceWithoutTargetSite_MessageLogged()
		{
			// Arrange - test data
			string exMessage = "Test Exception";
			Exception ex = new Exception(exMessage);
			string exSource = this.GetType().Name;

			// Arrage - get access to a log
			cEventLog logger = new cEventLog(string.Empty, 0, MessageImportance.Essential);

			// Arrange - Faked log implementation
			StringBuilder sbLog = new StringBuilder();
			StringWriter swLog = new StringWriter(sbLog);
			using (ShimsContext.Create())
			{
				ShimcEventLogImpl.AllInstances.getLogFileTextWriterOut = (cEventLogImpl log, out TextWriter tw) => tw = swLog;

				// Act
				logger.logError(ex, this);
			}

			// Assert
			string messages = sbLog.ToString();
			Assert.IsTrue(messages.Contains(exMessage));
			Assert.IsTrue(messages.Contains(exSource));
		}
	}
}
