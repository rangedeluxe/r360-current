﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;

namespace ComponentsTests
{
    [TestClass]
    public class ImageDisplayModesTests
    {
        [TestMethod]
        public void GetImageDisplayMode()
        {
            var obj = new ImageDisplayModes(OLFImageDisplayMode.BothSides, OLFImageDisplayMode.FrontOnly);

            Assert.AreEqual(OLFImageDisplayMode.BothSides, obj.GetImageDisplayMode(ImageItemType.Check), "Check");
            Assert.AreEqual(OLFImageDisplayMode.FrontOnly, obj.GetImageDisplayMode(ImageItemType.Document), "Document");
        }
    }
}