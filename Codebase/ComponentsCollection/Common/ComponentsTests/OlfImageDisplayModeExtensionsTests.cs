﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;

namespace ComponentsTests
{
    [TestClass]
    public class OlfImageDisplayModeExtensionsTests
    {
        [TestMethod]
        public void GetPageNumbers_FrontOnly()
        {
            var result = OLFImageDisplayMode.FrontOnly.GetPageNumbers();
            Assert.AreEqual("0", string.Join(",", result));
        }
        [TestMethod]
        public void GetPageNumbers_BothSides()
        {
            var result = OLFImageDisplayMode.BothSides.GetPageNumbers();
            Assert.AreEqual("0,1", string.Join(",", result));
        }
    }
}