﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Tom Emery
* Date:     11/30/2012
*
* Purpose:  HUB 360 AES Encryption
*
* Modification History
*-----------------------------------------------------------------------------
* WI 69906 TWE 11/30/2012
*   -Initial release
* WI 72226 TWE 12/07/2012
*   -FP this to 1.05
* WI 89971 CRG 03/05/2013
*	-Modify ipoCrypto for .NET 4.5 and NameSpace
* WI 103996 JMC 05/31/2013
*	-Updated namespace to WFS.RecHub.Common.Crypto
*	-Modified replaced ipoLib Message delegates with internal versions.
******************************************************************************/

namespace WFS.RecHub.Common.Crypto {

    public class cCryptoAES {

        private Exception _LastException = null;

        public cCryptoAES()
        {
        }

        /// <summary>
        /// Generate a random key
        /// </summary>
        /// <returns>Return the key generated</returns>
        public byte[] GenerateKey(int keySize)
        {
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.KeySize = keySize;
            aes.GenerateKey();
            return aes.Key;
        }

        /// <summary>
        /// Generate a random IV
        /// </summary>
        /// <returns>Return the IV generated</returns>
        public byte[] GenerateIV()
        {
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            //aes.KeySize = keySize;
            aes.GenerateIV();
            return aes.IV;
        }


        /// <summary>
        /// Encrypt a string using key and IV supplied
        /// </summary>
        /// <param name="data">The data to encrypt</param>
        /// <param name="key">The key to use</param>
        /// <param name="IV">The IV to use</param>
        /// <param name="encdata">The encrypted data</param>
        /// <returns></returns>
        public bool Encrypt(string data, byte[] key, byte[] IV, out byte[] encdata)
        {

            try
            {
                byte[] arData;

                //Array.Resize(ref Passphrase,192 / 8);

                MemoryStream oMem = new MemoryStream();

                CryptoStream oCrStream = new CryptoStream(oMem,
                    new AesCryptoServiceProvider().CreateEncryptor(key, IV),
                    CryptoStreamMode.Write);

                arData = ASCIIEncoding.ASCII.GetBytes(data);

                oCrStream.Write(arData, 0, arData.Length);
                oCrStream.FlushFinalBlock();

                encdata = oMem.ToArray();

                oMem.Close();
                oCrStream.Close();

                return (true);
            }
            catch (Exception ex)
            {
                onOutputError(ex);
                encdata = new byte[0];
                return (false);
            }

        }


        /// <summary>
        /// Decrypt a string using key and IV supplied
        /// </summary>
        /// <param name="encData">The data to decrypt</param>
        /// <param name="key"></param>
        /// <param name="IV"></param>
        /// <param name="data">The output decrypted data</param>
        /// <returns></returns>
        public bool Decrypt(byte[] encData, byte[] key, byte[] IV, out string data)
        {

            try
            {

                //Array.Resize(ref Passphrase,192 / 8);

                MemoryStream oMem = new MemoryStream(encData);

                CryptoStream oCrStream = new CryptoStream(oMem,
                    new AesCryptoServiceProvider().CreateDecryptor(key, IV),
                    CryptoStreamMode.Read);

                byte[] arDecrypted = new byte[encData.Length];
                oCrStream.Read(arDecrypted, 0, arDecrypted.Length);

                data = new ASCIIEncoding().GetString(arDecrypted).Trim('\0');

                oMem.Close();
                oCrStream.Close();

                return (true);

            }
            catch (Exception ex)
            {
                onOutputError(ex);
                data = "";
                return (false);
            }
        }


        public event OutputCryptoMessageEventHandler outputMessage;

        public event OutputCryptoErrorEventHandler outputError;

        public Exception LastException
        {
            get { return (_LastException); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        public void onOutputError(System.Exception ex)
        {
            if (outputError != null)
            {
                outputError(ex);
            }
            _LastException = ex;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        public void onOutputMessage(string message
                                  , string src
                                  , CryptoMessageType messageType
                                  , CryptoMessageImportance messageImportance)
        {
            if (outputMessage != null)
            {
                outputMessage(message
                            , src
                            , messageType
                            , messageImportance);
            }
        }
    }
}
