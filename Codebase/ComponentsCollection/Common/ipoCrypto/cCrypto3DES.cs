using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joy Sucha
* Date:     03/02/2010
*
* Purpose:  integraPAY Online TripleDES Encryption
*
* Modification History
* CR 28899 JCS 03/02/2010
*   -Initial release.
* CR 23161 JCS 05/17/2011
*   -Added default passphrase and initialization vector (IV)
*   -Added event handlers to return error and message information.
* WI 89971 CRG 03/05/2013
*	-Modify ipoCrypto for .NET 4.5 and NameSpace
* WI 103996 JMC 05/31/2013
*	-Updated namespace to WFS.RecHub.Common.Crypto
* WI 103996 JMC 05/31/2013
*	-Updated namespace to WFS.RecHub.Common.Crypto
*	-Modified replaced ipoLib Message delegates with internal versions.
******************************************************************************/
namespace WFS.RecHub.Common.Crypto {

    public class cCrypto3DES {

        /// Use the TripleDES symmetric encryption algorithm to encrypt data. Without an IV, the same
        /// input block of data will always encrypt to the same output block of encrypted data. Using
        /// an IV guarantees that the output of two identical data blocks are different.

        private const string PASSPHRASE = "VmlLaFkyODc0a09kSXA2ODU5";  // "ViKhY2874kOdIp6859";
        private const string IV = "9HsnyskhQkE=";

        private Exception _LastException = null;

        
        public cCrypto3DES() {
        }


        /// <summary>
        /// Generates a random Triple DES encryption key
        /// </summary>
        /// <returns></returns>
        public byte[] GenerateKey() {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.GenerateKey();

            return (des.Key);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public byte[] GenerateIV () {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider ();
            des.GenerateIV ();

            return (des.IV);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="Passphrase"></param>
        /// <param name="IV"></param>
        /// <param name="encdata"></param>
        /// <returns></returns>
        public bool Encrypt(string data, byte[] Passphrase, byte[] IV, out byte[] encdata) {

            try {
                byte[] arData;

                Array.Resize (ref Passphrase, 192 / 8);

                MemoryStream oMem = new MemoryStream ();
                CryptoStream oCrStream = new CryptoStream (oMem,
                    new TripleDESCryptoServiceProvider ().CreateEncryptor (Passphrase, IV),
                    CryptoStreamMode.Write);

                arData = ASCIIEncoding.ASCII.GetBytes(data);

                oCrStream.Write (arData, 0, arData.Length);
                oCrStream.FlushFinalBlock ();

                encdata = oMem.ToArray();

                oMem.Close ();
                oCrStream.Close ();

                return (true);
            } catch (Exception ex) {
                onOutputError(ex);
                encdata = new byte[0];
                return (false);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="encdata"></param>
        /// <returns></returns>
        public bool Encrypt(string data, out byte[] encdata)
        {
            return EncryptWithDPAPI(data, out encdata);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="encdata"></param>
        /// <returns></returns>
        public bool Encrypt(string data, out string encdata) {

            byte[] arCipherText;
            bool blnSuccess;            

            blnSuccess = Encrypt(data, out arCipherText);

            encdata = Convert.ToBase64String(arCipherText);           

            return (blnSuccess);
        }

        private static readonly byte[] _dpapiEntropy = Convert.FromBase64String("+m5dGpOoAgfSo8P59rPLNSO3sMGK7wR7la/Qj0nKY3SYSJZJ8dpm6pRQupjbCujq5yg=");

        internal static bool EncryptWithDPAPI(string value, out byte[] encBytes)
        {
            bool blnSuccess;
            try
            {
                encBytes = ProtectedData.Protect(Encoding.Unicode.GetBytes(value), _dpapiEntropy, DataProtectionScope.LocalMachine);
                blnSuccess = true;
            }
            catch
            {
                encBytes = null;
                blnSuccess = false;
            }
            return blnSuccess;
        }

        internal static bool DecryptWithDPAPI(byte[] encData, out byte[] value)
        {
            bool blnSuccess;
            try
            {
                value = ProtectedData.Unprotect(encData, _dpapiEntropy, DataProtectionScope.LocalMachine);
                blnSuccess = true;
            }
            catch
            {
                value = null;
                blnSuccess = false;
            }
            return blnSuccess;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="encdata"></param>
        /// <param name="Passphrase"></param>
        /// <param name="IV"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Decrypt (byte[] encData, byte[] Passphrase, byte[] IV, out string data) {

            try {

                Array.Resize (ref Passphrase, 192 / 8);

                MemoryStream oMem = new MemoryStream (encData);
                
                CryptoStream oCrStream = new CryptoStream (oMem,
                    new TripleDESCryptoServiceProvider ().CreateDecryptor (Passphrase, IV), 
                    CryptoStreamMode.Read);

                byte[] arDecrypted = new byte[encData.Length];
                oCrStream.Read (arDecrypted, 0, arDecrypted.Length);

                data = new ASCIIEncoding().GetString(arDecrypted).Trim('\0');

                oMem.Close ();
                oCrStream.Close ();

                return (true);

            } catch (Exception ex) {
                onOutputError(ex);
                data = "";
                return (false);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="encdata"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Decrypt(byte[] encData, out string data)
        {
            byte[] decData;
            DecryptWithDPAPI(encData, out decData);
            data = new ASCIIEncoding().GetString(decData).Replace("\0", "");
            return true;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="encdata"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Decrypt(string encData, out string data) {

            return (Decrypt(Convert.FromBase64String(encData), out data));

        }

        public event OutputCryptoMessageEventHandler outputMessage;

        public event OutputCryptoErrorEventHandler outputError;

        public Exception LastException {
            get { return (_LastException); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        public void onOutputError(System.Exception ex) {
            if (outputError != null) {
                outputError(ex);
            }
            _LastException = ex;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        public void onOutputMessage(string message
                                  , string src
                                  , CryptoMessageType messageType
                                  , CryptoMessageImportance messageImportance) {
            if (outputMessage != null) {
                outputMessage(message
                            , src
                            , messageType
                            , messageImportance);
            }
        }

    }

}
