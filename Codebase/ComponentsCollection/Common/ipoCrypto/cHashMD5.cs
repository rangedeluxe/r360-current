﻿using System;
using System.Security.Cryptography;
using System.Text;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joy Sucha
* Date:   05/03/2011
*
* Purpose: RecHub MD5 Hash Class Library
*
* Modification History
* CR 23161 JCS 05/03/2011
*   -Initial release.
* CR 54427 JCS 07/24/2012
*   -Added GetHashHex() to return MD5 hash as hex string.
* WI 89971 CRG 03/01/2013
*   -Modify ipoCrypto for .NET 4.5
* WI 103996 JMC 05/31/2013
*	-Updated namespace to WFS.RecHub.Common.Crypto
*	-Modified replaced ipoLib Message delegates with internal versions.
******************************************************************************/
namespace WFS.RecHub.Common.Crypto {

    public class cHashMD5 {

        private Exception _LastException = null;

        public cHashMD5() {
        
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Data"></param>
        /// <param name="EncodingType"></param>
        /// <returns></returns>
        public byte[] GetHashCharArray(string Data, Encoding EncodingType) {
            _LastException = null;

            try {
                MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

                //onOutputMessage("Executing GetHashCharArray().  Encoding is " + EncodingType.WebName.ToString() + "."
                //                , this.GetType().ToString()
                //                , MessageType.Information
                //                , MessageImportance.Debug);

                return (md5Hasher.ComputeHash(EncodingType.GetBytes(Data)));
            }

            catch (Exception ex) {
                onOutputError(ex);
                return (new byte[0]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        public byte[] GetHashCharArray(string Data) {
            try {
                return (GetHashCharArray(Data, Encoding.UTF8));
            }

            catch {
                return (new byte[0]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Data"></param>
        /// <param name="EncodingType"></param>
        /// <returns></returns>
        public string GetHash(string Data, Encoding EncodingType) {
            _LastException = null;

            try {
                byte[] data = GetHashCharArray(Data, EncodingType);
                return (EncodingType.GetString(data));
            }

            catch (Exception ex) {
                onOutputError(ex);
                return (String.Empty);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        public string GetHash(string Data) {
            try {
                return (GetHash(Data, Encoding.UTF8));
            }

            catch {
                return (String.Empty);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        public string GetHashHex(string Data) {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = GetHashCharArray(Data);

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++) {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TextData"></param>
        /// <param name="EncodingType"></param>
        /// <returns></returns>
        public string GetHashBase64(string TextData, Encoding EncodingType) {
            _LastException = null;

            try {
                byte[] data = GetHashCharArray(TextData, EncodingType);
                return (Convert.ToBase64String(data));
            }

            catch (Exception ex) {
                onOutputError(ex);
                return (String.Empty);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TextData"></param>
        /// <returns></returns>
        public string GetHashBase64(string TextData) {

            try {
                return (GetHashBase64(TextData, Encoding.UTF8));
            }

            catch {
                return (String.Empty);
            }
        }

        public Exception LastException {
            get { return (_LastException); }
        }

        public event OutputCryptoMessageEventHandler outputMessage;

        public event OutputCryptoErrorEventHandler outputError;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        public void onOutputError(System.Exception ex) {
            if (outputError != null) {
                outputError(ex);
            }
            _LastException = ex;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        public void onOutputMessage(string message
                                  , string src
                                  , CryptoMessageType messageType
                                  , CryptoMessageImportance messageImportance) {
            if (outputMessage != null) {
                outputMessage(message
                            , src
                            , messageType
                            , messageImportance);
            }
        }

    }

}
