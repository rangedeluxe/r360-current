using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Text;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     04/04/2005
*
* Purpose:  IntegraPAY Online Crypto Component.
*
* Modification History
*   04/04/2005 CR 8353 JMC
*       - Added decryption method when accessing local connectionstring property.
*   04/12/2005 CR 8353 JMC
*       - Modified last call to CryptAcquireContext to create the container as a
*         machine key.
*   04/19/2005 CR 12277 JMC
*       - Modified buffer length to handle strings between 8 and 16 characters.
******************************************************************************/
namespace WFS.integraPAY.Online.Common {

    /// <author>Joel Caples</author>
    /// <summary>
    /// InegraPAY Online encryption class
    /// </summary>
    public class cCrypto {

        private const string PASSWORD = "ViKhY2874kOdIp6859";
        private const string CONTAINER_NAME = "DMPCRYPTKEYCONTAINER";
        private string _LastError="";
        private const uint KEYBLOCKSIZE = 8;

        // API functions
        private class WinApi {

            #region Crypto API imports
		
            private const uint ALG_CLASS_HASH = (4 << 13);
            private const uint ALG_TYPE_ANY = (0);
            private const uint ALG_CLASS_DATA_ENCRYPT = (3 << 13);
            private const uint ALG_TYPE_STREAM = (4 << 9);
            private const uint ALG_TYPE_BLOCK = (3 << 9);

//            private const uint ALG_SID_DES = 1;
//            private const uint ALG_SID_RC4 = 1;
//            private const uint ALG_SID_RC2 = 2;
            private const uint ALG_SID_MD5 = 3;
            private const uint ALG_SID_3DES = 3;

            public const string MS_DEF_PROV = "Microsoft Enhanced Cryptographic Provider v1.0";
			
            public const uint PROV_RSA_FULL = 1;
//            public const uint CRYPT_VERIFYCONTEXT = 0xf0000000;
//            public const uint CRYPT_NEWKEYSET = 0x00000008;
            public const uint CRYPT_VERIFYCONTEXT = 0;
            public const uint CRYPT_NEWKEYSET = 8;
            public const uint CRYPT_MACHINE_KEYSET = 32;

            public const uint CRYPT_EXPORTABLE = 0x00000001;

            public static readonly uint CALG_MD5 = (ALG_CLASS_HASH | ALG_TYPE_ANY | ALG_SID_MD5);
//            public static readonly uint CALG_DES = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | ALG_SID_DES);
//            public static readonly uint CALG_RC2 = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | ALG_SID_RC2);
//            public static readonly uint CALG_RC4 = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_STREAM | ALG_SID_RC4);
            public static readonly uint CALG_3DES= (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | ALG_SID_3DES);

 
			const string CryptDll = "advapi32.dll";
			const string KernelDll = "kernel32.dll";
 
			
            [DllImport(CryptDll)] 
            public static extern bool CryptAcquireContext(
                ref IntPtr phProv, string pszContainer, string pszProvider,
                uint dwProvType, uint dwFlags);

            [DllImport(CryptDll)] 
            public static extern bool CryptReleaseContext( 
                IntPtr hProv, uint dwFlags);

            [DllImport(CryptDll)] 
            public static extern bool CryptDeriveKey(
                IntPtr hProv, uint Algid, IntPtr hBaseData, 
                uint dwFlags, ref IntPtr phKey);
				
            [DllImport(CryptDll)] 
            public static extern bool CryptCreateHash(
                IntPtr hProv, uint Algid, IntPtr hKey, 
                uint dwFlags, ref IntPtr phHash);

            [DllImport(CryptDll)] 
            public static extern bool CryptHashData(
                IntPtr hHash, byte[] pbData, 
                uint dwDataLen, uint dwFlags);
				
            [DllImport(CryptDll)] 
            public static extern bool CryptEncrypt(
                IntPtr hKey, IntPtr hHash, uint Final, uint dwFlags, 
                byte[] pbData, ref uint pdwDataLen, uint dwBufLen);

            [DllImport(CryptDll)] 
            public static extern bool CryptDecrypt(
                IntPtr hKey, IntPtr hHash, uint Final, uint dwFlags, 
                byte[] pbData, ref uint pdwDataLen);

            [DllImport(CryptDll)] 
            public static extern bool CryptDestroyHash(IntPtr hHash);

            [DllImport(CryptDll)] 
            public static extern bool CryptDestroyKey(IntPtr hKey);

            #endregion

            #region Error reporting imports
	
            public const uint FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;

            [DllImport(KernelDll)]
            public static extern uint GetLastError();

            [DllImport(KernelDll)]
            public static extern uint FormatMessage(
                uint dwFlags, string lpSource, uint dwMessageId,
                uint dwLanguageId, StringBuilder lpBuffer, uint nSize,
                string [] Arguments);

            #endregion				
        }
	
        // all static methods
        public cCrypto() {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public byte[] Encrypt(string data) {
            return(Encrypt(Encoding.ASCII.GetBytes(data)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public byte[] Decrypt(string data) {
            return(Decrypt(Encoding.ASCII.GetBytes(data)));
        }

        /// <summary>
        /// Encrypt data. Use passphrase to generate the encryption key. 
        /// Returns a byte array that contains the encrypted data.
        /// </summary>
        public byte[] Encrypt(byte[] data) {

            // holds encrypted data
            byte[] buffer = null;		

            // crypto handles
            IntPtr hProv = IntPtr.Zero;
            IntPtr hKey = IntPtr.Zero;

            try
            {
                // get crypto provider, specify the provider (3rd argument)
                // instead of using default to ensure the same provider is 
                // used on client and server
                if (!WinApi.CryptAcquireContext(ref hProv, CONTAINER_NAME, WinApi.MS_DEF_PROV
                                              , WinApi.PROV_RSA_FULL, WinApi.CRYPT_VERIFYCONTEXT)) {
                    if (!WinApi.CryptAcquireContext(ref hProv, CONTAINER_NAME, WinApi.MS_DEF_PROV 
                                                  , WinApi.PROV_RSA_FULL, WinApi.CRYPT_MACHINE_KEYSET)) {
                        if (!WinApi.CryptAcquireContext(ref hProv, CONTAINER_NAME, WinApi.MS_DEF_PROV 
                                                      , WinApi.PROV_RSA_FULL, WinApi.CRYPT_NEWKEYSET|WinApi.CRYPT_MACHINE_KEYSET)) {
                            Failed("CryptAcquireContext");
				        }
                    }
                }

                // generate encryption key from passphrase
                hKey = GetCryptoKey(hProv, PASSWORD);

                // determine how large of a buffer is required
                // to hold the encrypted data
                uint dataLength = (uint)data.Length;
                uint bufLength = (dataLength - (dataLength % KEYBLOCKSIZE)) + KEYBLOCKSIZE;

                // allocate and fill buffer with encrypted data
                buffer = new byte[bufLength];
                Buffer.BlockCopy(data, 0, buffer, 0, data.Length);
				
                if (!WinApi.CryptEncrypt(hKey, IntPtr.Zero, 1, 
                    0, buffer, ref dataLength, bufLength))
                    Failed("CryptEncrypt");

                string strEncoded = Convert.ToBase64String(buffer, 0, (int)dataLength);
			
                return ASCIIEncoding.ASCII.GetBytes(strEncoded);
            }
			
            finally
            {
                // release crypto handles
                if (hKey != IntPtr.Zero)
                    WinApi.CryptDestroyKey(hKey);

                if (hProv != IntPtr.Zero)
                    WinApi.CryptReleaseContext(hProv, 0);
            }
        }


        /// <summary>
        /// Decrypt data. Use passphrase to generate the encryption key. 
        /// Returns a byte array that contains the decrypted data.
        /// </summary>
        public byte[] Decrypt(byte[] data)
        {
            byte[] bytData = data.Clone() as byte[];
            string strData = ASCIIEncoding.ASCII.GetString(bytData);
            
            // make a copy of the encrypted data
            byte[] dataCopy = Convert.FromBase64String(strData);
			
            // holds the decrypted data
            byte[] buffer = null;
            byte[] bytRetVal = null;
			
            // crypto handles
            IntPtr hProv = IntPtr.Zero;
            IntPtr hKey = IntPtr.Zero;
			
            try
            {
                // get crypto provider, specify the provider (3rd argument)
                // instead of using default to ensure the same provider is 
                // used on client and server
                if (!WinApi.CryptAcquireContext(ref hProv, CONTAINER_NAME, WinApi.MS_DEF_PROV
                                              , WinApi.PROV_RSA_FULL, WinApi.CRYPT_VERIFYCONTEXT)) {
                    if (!WinApi.CryptAcquireContext(ref hProv, CONTAINER_NAME, WinApi.MS_DEF_PROV 
                                                  , WinApi.PROV_RSA_FULL, WinApi.CRYPT_MACHINE_KEYSET)) {
                        if (!WinApi.CryptAcquireContext(ref hProv, CONTAINER_NAME, WinApi.MS_DEF_PROV 
                                                      , WinApi.PROV_RSA_FULL, WinApi.CRYPT_NEWKEYSET)) {
                            Failed("CryptAcquireContext");
				        }
                    }
                }
			
                // generate encryption key from the passphrase
                hKey = GetCryptoKey(hProv, PASSWORD);

                // determine how large of a buffer is required
                // to hold the encrypted data
                uint dataLength = (uint)dataCopy.Length;
                uint bufLength = (uint)(dataLength - (dataLength % KEYBLOCKSIZE));
				
                // allocate and fill buffer with encrypted data
                buffer = new byte[bufLength];
                Buffer.BlockCopy(dataCopy, 0, buffer, 0, dataCopy.Length);

                if (!WinApi.CryptDecrypt(hKey, IntPtr.Zero, 1, 
                    0, buffer, ref bufLength))
                    Failed("CryptDecrypt");
				
                // copy to a buffer that is returned to the caller
                // the decrypted data size might be less then
                // the encrypted size
                bytRetVal = new byte[bufLength];
                Buffer.BlockCopy(buffer, 0, bytRetVal, 0, (int)bufLength);
            }
			
            finally
            {
                // release crypto handles
                if (hKey != IntPtr.Zero)
                    WinApi.CryptDestroyKey(hKey);

                if (hProv != IntPtr.Zero)
                    WinApi.CryptReleaseContext(hProv, 0);
            }
			
            return bytRetVal;
        }
		

        /// <summary>
        /// Create a crypto key form a passphrase. This key is 
        /// used to encrypt and decrypt data.
        /// </summary>
        private IntPtr GetCryptoKey(IntPtr hProv, string passphrase)
        {
            // crypto handles
            IntPtr hHash = IntPtr.Zero;
            IntPtr hKey = IntPtr.Zero;
			
            try
            {
                // create 128 bit hash object
                if (!WinApi.CryptCreateHash(hProv, WinApi.CALG_MD5, IntPtr.Zero, 0, ref hHash))
                    Failed("CryptCreateHash");
				
                // add passphrase to hash
                byte[] keyData = ASCIIEncoding.ASCII.GetBytes(passphrase);
                if (!WinApi.CryptHashData(hHash, keyData, (uint)keyData.Length+1, 0))
                    Failed("CryptHashData");
					
                // create 40 bit crypto key from passphrase hash
                if (!WinApi.CryptDeriveKey(hProv, WinApi.CALG_3DES, 
                    hHash, WinApi.CRYPT_EXPORTABLE, ref hKey))
                    Failed("CryptDeriveKey");
            }
			
            finally
            {
                // release hash object
                if (hHash != IntPtr.Zero)
                    WinApi.CryptDestroyHash(hHash);
            }
			
            return hKey;
        }

        /// <summary>
        /// 
        /// </summary>
        public string LastError {
            get {
                return(_LastError);
            }
        }

        /// <summary>
        /// Throws SystemException with GetLastError information.
        /// </summary>
        private void Failed(string command)
        {
            uint lastError = WinApi.GetLastError();
            StringBuilder sb = new StringBuilder(500);

            try
            {
                // get message for last error
                WinApi.FormatMessage(WinApi.FORMAT_MESSAGE_FROM_SYSTEM, 
                    null, lastError, 0, sb, 500, null);
            }
            catch
            {
                // error calling FormatMessage
                sb.Append("N/A.");
            }
					
            throw new SystemException(
                _LastError=string.Format("{0} failed.\r\nLast error - 0x{1:x}.\r\nError message - {2}",
                command, lastError, sb.ToString()));
        }
    }
}

