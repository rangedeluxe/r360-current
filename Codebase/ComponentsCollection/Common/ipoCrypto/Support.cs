﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date:   05/31/2013
*
* Purpose: RecHub SHA Hash Class Library
*
* Modification History
* WI 103996 JMC 05/31/2013
*	-Added file to project.
******************************************************************************/
namespace WFS.RecHub.Common.Crypto {

    /// <summary>
    /// 
    /// </summary>
    public delegate void OutputCryptoMessageEventHandler(
        string message,
        string src,
        CryptoMessageType messageType,
        CryptoMessageImportance messageImportance);

    /// <summary>
    /// 
    /// </summary>
    public delegate void OutputCryptoErrorEventHandler(System.Exception e);

    /// <summary>
    /// MessageType enumeration
    /// </summary>
    public enum CryptoMessageType {
        /// <summary></summary>
        Error = 0,
        /// <summary></summary>
        Information = 1,
        /// <summary></summary>
        Warning = 2
    }

    /// <summary>
    /// MessageImportance enumeration
    /// </summary>
    public enum CryptoMessageImportance {
        /// <summary></summary>
        Essential = 0,
        /// <summary></summary>
        Verbose = 1,
        /// <summary></summary>
        Debug = 2
    }
}
