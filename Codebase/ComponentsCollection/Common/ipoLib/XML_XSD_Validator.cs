﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: XML and XSD Vaildator
*
* Modification History
* CR 54212 JMC 07/17/2012
*   -Ported in to common library project from DIT.
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
******************************************************************************/
namespace WFS.RecHub.Common {
    
    /// <summary>
    /// call Validate to validate an xml file/stream against an xsd file/stream
    /// returns true if valid, false otherwise. GetError will return
    /// any error messages taht were produced in the last validation
    /// </summary>
    public static class XML_XSD_Validator
    {
        static int numErrors = 0;
        static string msgError = "";

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="xsd"></param>
        /// <returns></returns>
        public  static bool Validate(string xml, string xsd)
        {
            bool bValid = false;
            ClearErrorMessage();
            try
            {
                using (XmlTextReader tr = new XmlTextReader(new StringReader(xsd)))
                {
                    tr.Read();
                    XmlSchemaSet schema = new XmlSchemaSet();
                    schema.Add(null, tr);

                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.ValidationType = ValidationType.Schema;
                    settings.Schemas.Add(schema);
                    settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                    settings.ValidationEventHandler += new ValidationEventHandler(ErrorHandler);
                    using (XmlReader reader = XmlReader.Create(new StringReader(xml), settings))
                    {

                        // Validate XML data
                        while (reader.Read())
                            ;
                        reader.Close();
                    }

                    // exception if validation failed
                    if (numErrors > 0)
                        throw new Exception(msgError);
                }

                bValid = true;
              
            }
            catch
            {
                msgError = "Validation failed\r\n" + msgError;
                bValid = false;
            }
            return bValid;
        }

        private static void ErrorHandler(object sender, ValidationEventArgs args)
        {
            msgError = msgError + "\r\n" + args.Message;
            numErrors++;
        }

        /// <summary>
        /// if a validation error occurred, this will return the message
        /// </summary>
        /// <returns></returns>
        public static string GetError()
        {
            return msgError;
        }

        private static void ClearErrorMessage()
        {
            msgError = "";
            numErrors = 0;
        }
       

    }
}
