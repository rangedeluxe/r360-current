using System;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     05/14/2004
*
* Purpose:  ipo Services Options base class.
*
* Modification History
* CR ????? JMC 05/14/2004
*   -Created file.
* CR 7728  JMC 06/01/2004 
*   -Added protected parameterless constructor used when an inheriting class is
*    loading values based on a SiteCode rather than a SiteKey.  The inheriting
*    class needs to retrieve the SiteKey from the ini Sites section in order
*    to determine the SiteKey and load the appropriate values from the .ini file.
*   -Flagged loadSiteOptions method as protected rather than private so that it
*    can be called by inheriting classes.
* CR 14209 JMC 11/02/2005 
*   -Added SetDeadlockPriority property to class.
* CR 14211 JMC 11/04/2005 
*   -Added QueryRetryAttempts property to class.
* CR 21708 GRG 10/31/2007
*   -Changed logFilePath to get the log file path if not specified.
*   -Added checkAccess to check permissions to write logs.
*   -Changed _LoggingDepth to 0, Essential by default.
* CR 28522 JMC 01/06/2010
*   -Reworked the class to maintain all default site settings.
* CR 51046 WJS 3/19/2012
*   -Change Default of Query Retry Attempts if not defined
* CR 54847 JMC 08/08/2012
*   -Now decoding file paths in CheckAccess().  This was needed because logfile
*    paths when stored in the logfile can contain encode regular expression
*    date values.  The file path needs to be normalized before access can be
*    verified.
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
* WI 94256 TWE 03/27/2013
*    Create new connection string just for IntegraPay
* WI 103941 WJS 5/31/2013
 *   Create new connection string for CommonException
* WI 107883 EAS 07/03/2013
*	-Added Admin and Extract database connection string/types.
* WI 136855 RDS 04/15/2014
*	-Added Billing database connection string/types
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// Base options class for use by any ipo application.
    /// </summary>
    public class cIPOServicesOptions {

        private const int DEFAULT_QUERY_RETRY_ATTEMPTS = 3;

        /// <summary></summary>
        protected string _LogFilePath = string.Empty;
        /// <summary></summary>
        protected int _LogFileMaxSize = -1;  // 10 megs.
        /// <summary></summary>
        protected string _ConnectionString = string.Empty;
        /// <summary></summary>
        protected string _ConnectionStringIP = string.Empty;
        /// <summary> </summary>
        protected string _ConnectionStringCommonException = string.Empty;
        /// <summary> </summary>
        protected string _ConnectionStringExtracts = string.Empty;       
		/// <summary> </summary>
		protected string _ConnectionStringBilling = string.Empty;
		/// <summary></summary>
        protected int _LoggingDepth = 0; // Essential by default
        /// <summary></summary>
        protected bool _SetDeadlockPriority = false;

        /// <summary>
        /// the local variable is a short initialized as a -1.  This is the
        /// sentinal value that indicates that the value for this property
        /// has not been initialized.  The public property exposes a byte.
        /// </summary>
        protected short _QueryRetryAttempts = -1;

        /// <summary></summary>
        protected bool _LogFilePathIsDefined = false;
        /// <summary></summary>
        protected bool _LogFileMaxSizeIsDefined = false;
        /// <summary></summary>
        protected bool _ConnectionStringIsDefined = false;
        /// <summary></summary>
        protected bool _ConnectionStringIPIsDefined = false;
        /// <summary></summary>
        protected bool _ConnectionStringCommonExceptionIsDefined = false;
        /// <summary> </summary>
        protected bool _ConnectionStringExtractsIsDefined = false;
		/// <summary> </summary>
		protected bool _ConnectionStringBillingIsDefined = false;
		/// <summary></summary>
        protected bool _IntegraPayIsEnabled = false;
        /// <summary></summary>
        protected bool _LoggingDepthIsDefined = false;
        /// <summary></summary>
        protected bool _SetDeadlockPriorityIsDefined = false;
        /// <summary></summary>
        protected bool _QueryRetryAttemptsIsDefined = false;

        /// <summary>
        /// Parameterless constructor required for overloading.
        /// </summary>
        /// <param name="Section"></param>
        public cIPOServicesOptions(string Section) {
            ////const string INISECTION_IPO_SERVICES = "ipoServices";
            loadSiteOptions(Section);
        }

        /// <summary>
        /// Loads values from the ini file for the given section.
        /// </summary>
        /// <param name="Section"></param>
        protected virtual void loadSiteOptions(string Section) {

            string strKey;
            string strValue;

            const string INIKEY_LOG_FILE = "LogFile";
            const string INIKEY_LOG_FILE_MAX_SIZE = "LogFileMaxSize";
            const string INIKEY_LOGGING_DEPTH = "loggingDepth";
            const string INIKEY_DBCONNECTION2 = "DBConnection2";
            const string INIKEY_DBCONNECTIONIP = "DBConnectionIP";
            const string INIKEY_DBCONNECTIONCOMMONEXCEPTION = "DBConnectionCommonException";
            const string INIKEY_DBCONNECTIONEXTRACTS = "DBConnectionExtracts";
			const string INIKEY_DBCONNECTIONBILLING = "DBConnectionBilling";
            const string INIKEY_INTEGRAPAYENABLED = "IntegraPayEnabled";
            const string INIKEY_SET_DEADLOCK_PRIORITY = "SetDeadlockPriority";
            const string INIKEY_QUERY_RETRY_ATTEMPTS = "QueryRetryAttempts";


            StringCollection colSiteOptions;
            System.Collections.IEnumerator myEnumerator;

            int intTemp;
            Int16 int16Temp;

            //Load section options.
            colSiteOptions = ipoINILib.GetINISection(Section);
            myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {

                strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                if(strKey.ToLower() == INIKEY_LOG_FILE.ToLower()) {
                    _LogFilePath = strValue;
                    _LogFilePathIsDefined = (strValue.Trim().Length > 0);
                }
                else if(strKey.ToLower() == INIKEY_LOG_FILE_MAX_SIZE.ToLower()) {
                    if(int.TryParse(strValue, out intTemp)) {
                        _LogFileMaxSize = intTemp;
                        _LogFileMaxSizeIsDefined = true;
                    } else {
                        _LogFileMaxSize = -1;
                    }
                }
                else if(strKey.ToLower() == INIKEY_LOGGING_DEPTH.ToLower()) {
                    if(int.TryParse(strValue, out intTemp)) {
                        _LoggingDepth = intTemp;
                        _LoggingDepthIsDefined = true;
                    } else {
                        _LoggingDepth = 0;
                    }
                }
                else if(strKey.ToLower() == INIKEY_DBCONNECTION2.ToLower()) {
                    _ConnectionString = strValue;
                    _ConnectionStringIsDefined = (strValue.Trim().Length > 0);
                }
                else if (strKey.ToLower() == INIKEY_INTEGRAPAYENABLED.ToLower())
                {
                    if (strValue.ToLower() == "true")
                    {
                        _IntegraPayIsEnabled = true;
                    }
                    else
                    {
                        _IntegraPayIsEnabled = false;
                    }
                }
                else if (strKey.ToLower() == INIKEY_DBCONNECTIONIP.ToLower())
                {
                    _ConnectionStringIP = strValue;
                    _ConnectionStringIPIsDefined = (strValue.Trim().Length > 0);
                }
                else if (strKey.ToLower() == INIKEY_DBCONNECTIONCOMMONEXCEPTION.ToLower())
                {
                    _ConnectionStringCommonException = strValue;
                    _ConnectionStringCommonExceptionIsDefined = (strValue.Trim().Length > 0);
                }
                else if (strKey.ToLower() == INIKEY_DBCONNECTIONEXTRACTS.ToLower())
                {
                    _ConnectionStringExtracts = strValue;
                    _ConnectionStringExtractsIsDefined = (strValue.Trim().Length > 0);
                }
				else if (strKey.ToLower() == INIKEY_DBCONNECTIONBILLING.ToLower())
				{
					_ConnectionStringBilling = strValue;
					_ConnectionStringBillingIsDefined = (strValue.Trim().Length > 0);
				}
				else if (strKey.ToLower() == INIKEY_SET_DEADLOCK_PRIORITY.ToLower())
				{
                    if(Int16.TryParse(strValue, out int16Temp)) {
                        _SetDeadlockPriority = (int16Temp > 0);
                        _SetDeadlockPriorityIsDefined = true;
                    } else {
                        _SetDeadlockPriority = false;
                    }
                }
                else if(strKey.ToLower() == INIKEY_QUERY_RETRY_ATTEMPTS.ToLower()) {
                    if(Int16.TryParse(strValue, out int16Temp)) {
                        _QueryRetryAttempts = int16Temp;
                        _QueryRetryAttemptsIsDefined = true;
                    } else {
                        _QueryRetryAttempts = -1;
                    }
                }
            }
        }

        /// <summary>
        /// Returns full path to the logfile that the eventLog class should 
        /// write to based on the local INI file.
        /// If logfile is not specified in INI, returns an appropriate file path.
        /// </summary>
        public virtual string logFilePath {
            get {
                bool bolIsPath = true;
                string strTemp = "";
                try {
                    if (_LogFilePath.Trim() == "")
                        bolIsPath = false;

                    if (bolIsPath) {
                        if (CheckAccess(_LogFilePath))
                            return _LogFilePath;

                        int len = _LogFilePath.LastIndexOf("\\");
                        if (len <= 0)
                            bolIsPath = false;
                        else {
                            strTemp = _LogFilePath.Substring(0, len + 1);
                            bolIsPath = System.IO.Directory.Exists(strTemp);
                            if (bolIsPath)
                                strTemp = System.IO.Path.Combine(strTemp, "ipo_log.txt");
                                
                            bolIsPath = CheckAccess(strTemp);
                        }
                    }

                    if (!bolIsPath) {
                        strTemp = AppDomain.CurrentDomain.BaseDirectory;
                        System.IO.DirectoryInfo df = new System.IO.DirectoryInfo(strTemp);

                        if (df.Parent.Parent.Exists) {

                            //check to see if data folder exists and try to create new log file if not already exists.
                            strTemp = System.IO.Path.Combine(df.Parent.Parent.FullName, "data");

                            bolIsPath = System.IO.Directory.Exists(strTemp);
                            if (bolIsPath) {
                                strTemp = System.IO.Path.Combine(strTemp, "ipo_log.txt");
                                bolIsPath = CheckAccess(strTemp);
                            }

                            //check to see if bin folder exists and try to create new log file if not already exists.
                            if (!bolIsPath) {
                                strTemp = System.IO.Path.Combine(df.Parent.Parent.FullName, "bin");
                                bolIsPath = System.IO.Directory.Exists(strTemp);
                                if (bolIsPath) {
                                    strTemp = System.IO.Path.Combine(strTemp, "ipo_log.txt");
                                    bolIsPath = CheckAccess(strTemp);
                                }
                            }

                            //check to see if IPOnline folder exists and try to create new log file if not already exists.
                            if (!bolIsPath) {
                                strTemp = System.IO.Path.Combine(df.Parent.Parent.FullName, "ipo_log.txt");
                                bolIsPath = CheckAccess(strTemp);
                            }

                            //check to see if wwwroot folder exists and try to create new log file if not already exists.
                            if (!bolIsPath) {
                                strTemp = System.IO.Path.Combine(df.Parent.FullName, "ipo_log.txt");
                                bolIsPath = CheckAccess(strTemp);
                            }

                        }
                    }

                    if (bolIsPath)
                        _LogFilePath = strTemp;
                }
                catch { }

                return _LogFilePath;
            }
        }

        /// <summary>
        /// Checks for the file access permisions to write log file.
        /// </summary>
        private bool CheckAccess(string filePath) {

            string strDecodedFilePath;
            System.IO.FileInfo fi;
            string strDirPath;
            bool bolHasAccess = false;
            DirectorySecurity sec;
            AuthorizationRuleCollection dacls;
            SecurityIdentifier sid;

            try {

                if(DecodeLogFilePath(filePath, out strDecodedFilePath)) {

                    fi = new System.IO.FileInfo(strDecodedFilePath);
                    strDirPath = fi.Directory.ToString();

                    sec = System.IO.Directory.GetAccessControl(strDirPath, System.Security.AccessControl.AccessControlSections.Access);
                    dacls = sec.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier));

                    foreach (System.Security.AccessControl.FileSystemAccessRule dacl in dacls) {

                        sid = (System.Security.Principal.SecurityIdentifier)dacl.IdentityReference;
                        
                        if (((dacl.FileSystemRights & System.Security.AccessControl.FileSystemRights.CreateFiles) == System.Security.AccessControl.FileSystemRights.CreateFiles) ||
                           ((dacl.FileSystemRights & System.Security.AccessControl.FileSystemRights.Write) == System.Security.AccessControl.FileSystemRights.Write)) {

                            if ((sid.IsAccountSid() && System.Security.Principal.WindowsIdentity.GetCurrent().User == sid) ||
                                (!sid.IsAccountSid() && System.Security.Principal.WindowsIdentity.GetCurrent().Groups.Contains(sid))) {

                                //If this is a deny right then the user has no access
                                if (dacl.AccessControlType == System.Security.AccessControl.AccessControlType.Deny) {
                                    bolHasAccess = false;
                                } else {
                                    //Allowed, for now    
                                    bolHasAccess = true;
                                }
                            }
                        }
                    }

                } else {
                    bolHasAccess = false;
                }
            }
            catch(Exception ex) {
                Console.WriteLine("Exception Occurred in " + this.GetType().Name + ": " + ex.Message);
                Console.WriteLine("Could not verify access to file: " + filePath);
                bolHasAccess = false;
            }

            return bolHasAccess;
        }

        private static bool DecodeLogFilePath(string encodedLogFilePath, out string decodedLogFilePath) {

            Regex objRegEx;
            bool bolUseRollingLogFile;
            string strDecodedLogFilePath;
            bool bolRetVal;

            const string LOGFILE_TIMESTAMP_TOKEN = @"\{0:[yMHhdms].*\}";

            try {

                // Check if logfile path contains composite date/time formatting for rolling log file.
                objRegEx = new Regex (LOGFILE_TIMESTAMP_TOKEN, RegexOptions.IgnoreCase);
                bolUseRollingLogFile = objRegEx.IsMatch(encodedLogFilePath);

                if (bolUseRollingLogFile) {
                    strDecodedLogFilePath = string.Format(encodedLogFilePath, DateTime.Now);
                } else {
                    strDecodedLogFilePath = encodedLogFilePath;
                }

                bolRetVal = true;

            } catch(Exception ex) {
                Console.WriteLine("Exception Occurred in " + Assembly.GetEntryAssembly().GetType().Name + ": " + ex.Message);
                Console.WriteLine("Could not decode LogFilePath: " + encodedLogFilePath);
                strDecodedLogFilePath = string.Empty;
                bolRetVal = false;
            }

            decodedLogFilePath = strDecodedLogFilePath;
            return(bolRetVal);
        }

        /// <summary>
        /// Returns the maximum size (in megaBytes) that the eventLog's logfile 
        /// can rech before it should be truncated.  Value is from the local INI 
        /// file.
        /// </summary>
        public virtual int logFileMaxSize {
            get {
                return _LogFileMaxSize;
            }
        }


        /// <summary>
        /// Determines what level of logging is to actually be recorded.
        /// </summary>
        public virtual int loggingDepth {
            get {
                return(_LoggingDepth);
            }
        }

        /// <summary>
        /// Returns a value for the database connection string based on
        /// the local INI file.
        /// </summary>
        public virtual string connectionString {
            get {
                return _ConnectionString;
            }
        }

        /// <summary>
        /// Connection string for Integra Pay database
        /// Returns a value for the database connection string based on
        /// the local INI file.
        /// </summary>
        public virtual string connectionStringIP
        {
            get
            {
                return _ConnectionStringIP;
            }
        }

        /// <summary>
        /// Connection string for Common Exception database
        /// Returns a value for the database connection string based on
        /// the local INI file.
        /// </summary>
        public virtual string connectionStringCommonException
        {
            get
            {
                return _ConnectionStringCommonException;
            }
        }

        /// <summary>
        /// Connection string for Extracts database
        /// Returns a value for the database connection string based on
        /// the local INI file.
        /// </summary>
        public virtual string connectionStringExtracts
        {
            get
            {
                return _ConnectionStringExtracts;
            }
        }
              

		/// <summary>
		/// Connection string for Billing database
		/// Returns a value for the database connection string based on
		/// the local INI file.
		/// </summary>
		public virtual string connectionStringBilling
		{
			get
			{
				return _ConnectionStringBilling;
			}
		}

        /// <summary>
        /// Disable or enable the integraPay database
        /// </summary>
        public virtual bool integraPayIsEnabled
        {
            get
            {
                return _IntegraPayIsEnabled;
            }
        }

        /// <summary></summary>
        public virtual bool SetDeadlockPriority {
            get {
                return(_SetDeadlockPriority);
            }
        }

        /// <summary>
        /// The QueryRetryAttempts property exposes a Byte, not a short which is the internal
        /// variable type.  This value indicates the number of times the database component 
        /// should RE-try to execute any SQL.  The database component will initially try once
        /// and retry the number of times indicated by the QueryRetryAttempts property.
        /// </summary>
        public virtual byte QueryRetryAttempts {
            get { 
                if((_QueryRetryAttempts >= System.Byte.MinValue) && (_QueryRetryAttempts <= System.Byte.MaxValue)) {
                    return(Convert.ToByte(_QueryRetryAttempts)); 
                } else {
                    return (DEFAULT_QUERY_RETRY_ATTEMPTS);
                }
            }
        }

        /// <summary></summary>
        protected bool LogFilePathIsDefined {
            get {
                return(_LogFilePathIsDefined);
            }
        }

        /// <summary></summary>
        protected bool LogFileMaxSizeIsDefined {
            get {
                return(_LogFileMaxSizeIsDefined);
            }
        }

        /// <summary></summary>
        protected bool ConnectionStringIsDefined {
            get {
                return(_ConnectionStringIsDefined);
            }
        }

        /// <summary>
        /// This is the connection string used for 
        /// Integra Pay
        /// </summary>
        protected bool ConnectionStringIPIsDefined
        {
            get
            {
                return (_ConnectionStringIPIsDefined);
            }
        }


        /// <summary>
        /// This is the connection string used for 
        /// CommonException
        /// </summary>
        protected bool ConnectionStringCommonExceptionIsDefined
        {
            get
            {
                return (_ConnectionStringCommonExceptionIsDefined);
            }
        }

        /// <summary>
        /// This is the connection string used for 
        /// Extracts
        /// </summary>
        protected bool ConnectionStringExtractsIsDefined
        {
            get
            {
                return (_ConnectionStringExtractsIsDefined);
            }
        }

		/// <summary>
		/// This is the connection string used for 
		/// Billing Extracts
		/// </summary>
		protected bool ConnectionStringBillingIsDefined
		{
			get
			{
				return (_ConnectionStringBillingIsDefined);
			}
		}

        /// <summary></summary>
        protected bool LoggingDepthIsDefined {
            get {
                return(_LoggingDepthIsDefined);
            }
        }

        /// <summary></summary>
        protected bool SetDeadlockPriorityIsDefined {
            get {
                return(_SetDeadlockPriorityIsDefined);
            }
        }

        /// <summary></summary>
        protected bool QueryRetryAttemptsIsDefined {
            get {
                return(_QueryRetryAttemptsIsDefined);
            }
        }

    }
}
