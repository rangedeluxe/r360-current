﻿namespace WFS.RecHub.Common
{
    /// <summary>
    /// Represents the item types that can have images.
    /// </summary>
    /// <remarks>
    /// Checks and documents can have images; stubs don't.
    /// </remarks>
    public enum ImageItemType
    {
        Check,
        Document
    }
}