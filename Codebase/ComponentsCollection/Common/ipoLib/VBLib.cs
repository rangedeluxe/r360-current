﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   
* Date:     08/05/2008
*
* Purpose:  IPOnline common VB library class.
*
* Modification History
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public static class VBLib {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static int Len(string Value) {
            if(Value == null) {
                return(0);
            } else {
                return(Value.Length);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="StartPos"></param>
        /// <returns></returns>
        public static string Mid(string Value, int StartPos) {
            if(Value == null) {
                return(string.Empty);
            } else {
                return(Value.Substring(StartPos));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="StartPos"></param>
        /// <param name="Len"></param>
        /// <returns></returns>
        public static string Mid(string Value, int StartPos, int Len) {
            if(Value == null) {
                return(string.Empty);
            } else {
                return(Value.Substring(StartPos, Len));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="StartPos"></param>
        /// <param name="Value1"></param>
        /// <param name="Value2"></param>
        /// <returns></returns>
        public static int InStr(int StartPos, string Value1, string Value2) {
            if(Value1 == null || Value2 == null) {
                return(-1);
            } else {
                return(Value1.IndexOf(Value2, StartPos));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value1"></param>
        /// <param name="Value2"></param>
        /// <returns></returns>
        public static int InStr(string Value1, string Value2) {
            if(Value1 == null || Value2 == null) {
                return(-1);
            } else {
                return(Value1.IndexOf(Value2));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Len"></param>
        /// <returns></returns>
        public static string Left(string Value, int Len) {
            if(Value == null) {
                return(string.Empty);
            } else {
                return(Value.Substring(0, Len));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static string LCase(string Value) {
            if(Value == null) {
                return(string.Empty);
            } else {
                return(Value.ToLower());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static string UCase(string Value) {
            if(Value == null) {
                return(string.Empty);
            } else {
                return(Value.ToUpper());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static string Trim(string Value) {
            if(Value == null) {
                return(string.Empty);
            } else {
                return(Value.Trim());
            }
        }

        /// <summary>
        /// CR 12576 EJG 01/09/2006 - Created function to determine if value is a valid currency value.
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
	    public static bool IsValidCurrency(string Value) {
		    System.Text.RegularExpressions.Regex objExp = new System.Text.RegularExpressions.Regex(@"^\$*\s?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		    return(objExp.IsMatch(Value));
	    }
    }
}
