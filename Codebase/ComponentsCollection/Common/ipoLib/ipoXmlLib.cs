using System;
using System.Data;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     11/20/2003
*
* Purpose:  IntegraPAY Online Xml Library.
*
* Modification History
* 11/20/2003 JMC
*     - Created class
* 07/28/2005 CR 12257 JMC
*     - Added addAttribute(and ipoXmlLib.addElement methods.
* 05/20/2010 JMC CR 29405
*     - Added AddRecordsetToDoc3 to prevent pagination.
* CR 29951 JMC 07/12/2010
*     - Modified AddDateAsNode to include a 'NumericDate' attribute in 
*       YYYYMMDD format.
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
* WI 108114 TWE 07/08/2013
*    Change primary key of ActivityLog from Guid to Long (bigint)
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public enum xmlMessageType {
        /// <summary>
        /// 
        /// </summary>
        xmlMsgError = 0,
        /// <summary>
        /// 
        /// </summary>
        xmlMsgInformation = 1,
        /// <summary>
        /// 
        /// </summary>
        xmlMsgWarning = 2
    }

    /// <author>Joel Caples</author>
    /// <summary>
    /// ipo Xml Library
    /// </summary>
    public class ipoXmlLib {

        /// <summary>
        /// Adds custom attribute to an Xml node.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="node"></param>
        /// <param name="prefix"></param>
        /// <param name="localName"></param>
        /// <param name="namespaceURI"></param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public static XmlAttribute createAttribute(ref XmlDocument doc, 
                                                   ref XmlNode node, 
                                                   string prefix, 
                                                   string localName, 
                                                   string namespaceURI, 
                                                   string attributeValue) {

            XmlAttribute attribute = doc.CreateAttribute(prefix, localName, namespaceURI);
            attribute.Value = attributeValue;
            node.Attributes.Append(attribute);

            return attribute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attributeName"></param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public static XmlNode addAttribute(XmlNode node, 
                                           string attributeName, 
                                           string attributeValue) {

            XmlNode nodeTemp;

            nodeTemp = node.OwnerDocument.CreateNode(XmlNodeType.Attribute, 
                                                     attributeName, 
                                                     node.NamespaceURI);

            nodeTemp.Value=attributeValue;
            node.Attributes.Append((XmlAttribute)nodeTemp);

            return(nodeTemp);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="nodeName"></param>
        /// <param name="nodeValue"></param>
        /// <returns></returns>
        public static XmlElement addElement(XmlNode parentNode, 
                                            string nodeName, 
                                            string nodeValue) {

            XmlElement objTemp;

            if(nodeName == null) {
                nodeName = string.Empty;
            }

            if(nodeValue == null) {
                nodeValue = string.Empty;
            }

            if(parentNode is XmlDocument) {
                objTemp = (XmlElement)((XmlDocument)parentNode).CreateNode(XmlNodeType.Element, 
                                                                           nodeName, 
                                                                           parentNode.NamespaceURI);
            } else {
                objTemp = (XmlElement)parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, 
                                                                          nodeName, 
                                                                          parentNode.NamespaceURI);
            }

            objTemp.InnerText = nodeValue;
            parentNode.AppendChild(objTemp);

            return(objTemp);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public static XmlElement addElement(XmlNode parentNode, 
                                            string nodeName) {

            XmlElement objTemp;

            if(nodeName == null) {
                nodeName = string.Empty;
            }

            if(parentNode is XmlDocument) {
                objTemp = (XmlElement)((XmlDocument)parentNode).CreateNode(XmlNodeType.Element, 
                                                                           nodeName, 
                                                                           parentNode.NamespaceURI);
            } else {
                objTemp = (XmlElement)parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, 
                                                                          nodeName, 
                                                                          parentNode.NamespaceURI);
            }

            objTemp.IsEmpty = true;
            parentNode.AppendChild(objTemp);

            return(objTemp);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="FormName"></param>
        /// <param name="FieldName"></param>
        /// <param name="FieldValue"></param>
        /// <returns></returns>
        public static XmlNode AddFormFieldAsNode(XmlNode node, 
                                                 string FormName, 
                                                 string FieldName, 
                                                 string FieldValue) {

            return AddFormFieldAsNode(node, FormName, FieldName, FieldValue, string.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="FormName"></param>
        /// <param name="FieldName"></param>
        /// <param name="FieldValue"></param>
        /// <param name="FieldType"></param>
        /// <returns></returns>
        public static XmlNode AddFormFieldAsNode(XmlNode node, 
                                                 string FormName, 
                                                 string FieldName, 
                                                 string FieldValue, 
                                                 string FieldType) {

            XmlNode nodeForm;
            XmlNode nodeField;
            
            //retrieve ref to form node (create if(necessary)
            nodeForm = node.SelectSingleNode("Form[@Name='" + FormName + "']");
            if(nodeForm == null) {
                nodeForm = addElement(node, "Form");
                addAttribute(nodeForm, "Name", FormName);
            }
            
            //add child to form node
            nodeField = addElement(nodeForm, "Field");
            addAttribute(nodeField, "Name", FieldName);
            addAttribute(nodeField, "Value", FieldValue);
            addAttribute(nodeField, "Type", FieldType);
            
            return(nodeField);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="ErrorType"></param>
        /// <param name="ErrorText"></param>
        /// <returns></returns>
        public static XmlNode AddMessageToDoc(XmlDocument xmlDoc, 
                                              xmlMessageType ErrorType, 
                                              string ErrorText) {

            return(AddMessageToDoc(xmlDoc, ErrorType, ErrorText, ""));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="ErrorType"></param>
        /// <param name="ErrorText"></param>
        /// <param name="MessageEnum"></param>
        /// <returns></returns>
        public static XmlNode AddMessageToDoc(XmlDocument xmlDoc, 
                                              xmlMessageType ErrorType, 
                                              string ErrorText, 
                                              string MessageEnum) {

            XmlNode nodeMessages;
            XmlNode nodeMessage;
            
            nodeMessages = xmlDoc.DocumentElement.SelectSingleNode("Messages");
            
            if(nodeMessages == null) {
                nodeMessages = addElement(xmlDoc.DocumentElement, "Messages");
            }
            
            nodeMessage = addElement(nodeMessages, "Message");
            switch(ErrorType) {
                case xmlMessageType.xmlMsgError:
                    addAttribute(nodeMessage, "Type", "Error");
                    break;
                case xmlMessageType.xmlMsgInformation:
                    addAttribute(nodeMessage, "Type", "Information");
                    break;
                case xmlMessageType.xmlMsgWarning:
                    addAttribute(nodeMessage, "Type", "Warning");
                    break;
                default:
                    addAttribute(nodeMessage, "Type", "Message");
                    break;
            }
            addAttribute(nodeMessage, "Text", ErrorText.Trim());
            
            if(MessageEnum.Length > 0) {
                addAttribute(nodeMessage, "MsgEnum", MessageEnum);
            }
            
            return(nodeMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeRS"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <returns></returns>
        public static XmlNode AddPageInfo(XmlNode nodeRS, 
                                          int StartRecord, 
                                          int MaxRecords) {

            return(AddPageInfo(nodeRS, 
                               StartRecord, 
                               MaxRecords, 
                               "Recordset", 
                               "Record", 
                               "TotalRecords", 
                               "EndRecord", 
                               "PageInfo"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeRS"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <returns></returns>
        public static XmlNode AddPageInfo(XmlNode nodeRS, 
                                          int StartRecord, 
                                          int MaxRecords, 
                                          string RecordsetLabel, 
                                          string RecordLabel) {

            return(AddPageInfo(nodeRS, 
                               StartRecord, 
                               MaxRecords, 
                               RecordsetLabel, 
                               RecordLabel, 
                               "TotalRecords", 
                               "EndRecord", 
                               "PageInfo"));
        }

        /// <summary>
        /// Calculate pageing information and add the PageInfo node the first
        /// child node to the main Recordnode in the documnet element.
        /// </summary>
        /// <param name="nodeRS">Reference to the main Recordnode</param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords">How many records per page are displayed</param>
        /// <param name="RecordsetLabel">Optional label if(not default for each Recordnode</param>
        /// <param name="RecordLabel">Optional label if(not default for each Record node</param>
        /// <param name="TotalRecordsLabel"></param>
        /// <param name="EndRecordLabel"></param>
        /// <param name="PageInfoLabel"></param>
        /// <returns>Reference to the PageInfo node</returns>
        public static XmlNode AddPageInfo(XmlNode nodeRS, 
                                          int StartRecord, 
                                          int MaxRecords, 
                                          string RecordsetLabel, 
                                          string RecordLabel, 
                                          string TotalRecordsLabel, 
                                          string EndRecordLabel, 
                                          string PageInfoLabel) {
                                    
            XmlNode nodePageInfo;
            XmlNodeList nodeList;
            int lngRecordCount = 0;
            int lngEndRecord = 0;
                
            if(nodeRS == null) { return null; }
            
            if(StartRecord <= 0) { StartRecord = 1; }

            if(RecordsetLabel == null || RecordsetLabel == string.Empty) {
                RecordsetLabel = "Recordset";
            }

            if(RecordLabel == null || RecordLabel == string.Empty) {
                RecordLabel = "Record";
            }

            if(TotalRecordsLabel == null || TotalRecordsLabel == string.Empty) {
                TotalRecordsLabel = "TotalRecords";
            }

            if(EndRecordLabel == null || EndRecordLabel == string.Empty) {
                EndRecordLabel = "EndRecord";
            }

            if(PageInfoLabel == null || PageInfoLabel == string.Empty) {
                PageInfoLabel = "PageInfo";
            }
            
            //Determine record count and end record
            nodeList = nodeRS.SelectNodes("./" + RecordLabel);
            
            for(int i=0;i<nodeList.Count;++i) {

                foreach(XmlNode node in nodeList[i].ChildNodes) {

                    if(node.Name.Trim().ToLower() == RecordsetLabel.Trim().ToLower()) {
                        //Get the TotalRecords
                        if(node.SelectSingleNode("@" + TotalRecordsLabel) != null) {
                            lngRecordCount += int.Parse(node.SelectSingleNode("@" + TotalRecordsLabel).Value);
                        }

                        //Get the EndRecord
                        if(node.SelectSingleNode("@" + EndRecordLabel) != null) {
                            lngEndRecord += int.Parse(node.SelectSingleNode("@" + EndRecordLabel).Value);
                        }
                    
                    }
                }
            }
                
            //when spanning pages, need to add the StartRecord - 1 to the EndRecord
            if(StartRecord > lngEndRecord) {
                lngEndRecord = StartRecord + lngEndRecord - 1;
            }
                
            //Add the PageInfo node to the node reference the first node in the nodeRS
            nodePageInfo = nodeRS.OwnerDocument.CreateNode(XmlNodeType.Element, PageInfoLabel, nodeRS.NamespaceURI);
                
            if(nodePageInfo != null) {
                addAttribute(nodePageInfo, "StartRecord", StartRecord.ToString());
                addAttribute(nodePageInfo, "EndRecord", lngEndRecord.ToString());
                addAttribute(nodePageInfo, "TotalRecords", lngRecordCount.ToString());
                addAttribute(nodePageInfo, "DisplayRecords", MaxRecords.ToString());
                addAttribute(nodePageInfo, "NextStartRecord", Pagination.GetNextStartRecord(StartRecord, MaxRecords, lngRecordCount).ToString());
                addAttribute(nodePageInfo, "PreviousStartRecord", Pagination.GetPreviousStartRecord(StartRecord, MaxRecords).ToString());
                addAttribute(nodePageInfo, "LastStartRecord", Pagination.GetLastStartRecord(MaxRecords, lngRecordCount).ToString());
                    
                nodeRS.InsertBefore(nodePageInfo, nodeRS.FirstChild);
            }
            
                
            return(nodePageInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="dr"></param>
        /// <param name="UseIntForBool"></param>
        /// <returns></returns>
        public static XmlNode AddRecordAsNode(XmlNode node, 
                                              DataRow dr,
                                              bool UseIntForBool) {

            return(AddRecordAsNode(node, dr, "Record", UseIntForBool));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="dr"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="UseIntForBool"></param>
        /// <returns></returns>
        public static XmlNode AddRecordAsNode(XmlNode node, DataRow dr, string RecordLabel, bool UseIntForBool) {

            XmlNode nodeRecord;
            
            nodeRecord = addElement(node, RecordLabel);
            foreach(DataColumn dc in dr.Table.Columns) {

                if(!dr.IsNull(dc)) {
                    // TODO: RTrim character fields?
                    switch(dc.DataType.Name.ToString()) {
                        case "DateTime":
                            if(((DateTime)dr[dc]).TimeOfDay > new TimeSpan(0, 0, 0)) {
                                addAttribute(nodeRecord, dc.ColumnName, dr[dc].ToString());
                            } else {
                                addAttribute(nodeRecord, dc.ColumnName, ((DateTime)dr[dc]).ToShortDateString());
                            }
                            break;
                        case "Boolean":
                            if(UseIntForBool) {
                                addAttribute(nodeRecord, dc.ColumnName, ((bool)dr[dc] ? "1" : "0"));
                                break;
                            } else {
                                addAttribute(nodeRecord, dc.ColumnName, dr[dc].ToString().Trim());
                                break;
                            }
                        default:
                            //addElement nodeRecord, rs.fields(i).Name, nodeRecord.NamespaceURI, rs.fields(i).Value
                            addAttribute(nodeRecord, dc.ColumnName, dr[dc].ToString());
                            break;
                    }
                } else {
                    // TODO: Do not add binary data?
                    //addElement nodeRecord, rs.fields(i).Name, nodeRecord.NamespaceURI, ""
                    addAttribute(nodeRecord, dc.ColumnName, string.Empty);
                }
            }
            
            return(nodeRecord);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc(XmlDocument xmlDoc, 
                                                DataTable dt, 
                                                string RSName) {

            return(AddRecordsetToDoc(xmlDoc, dt, RSName, "Recordset", "Record", 0, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="StartRecord"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc(XmlDocument xmlDoc, 
                                                DataTable dt, 
                                                string RSName, 
                                                string RecordsetLabel, 
                                                string RecordLabel, 
                                                int StartRecord) {

            return(AddRecordsetToDoc(xmlDoc, dt, RSName, RecordsetLabel, RecordLabel, StartRecord, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc(XmlDocument xmlDoc, 
                                                DataTable dt, 
                                                string RSName, 
                                                string RecordsetLabel, 
                                                string RecordLabel, 
                                                int StartRecord, 
                                                int MaxRecords) {
            return(AddRecordsetToDoc(xmlDoc, 
                                     dt, 
                                     RSName, 
                                     RecordsetLabel, 
                                     RecordLabel, 
                                     StartRecord, 
                                     MaxRecords,
                                     false));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <param name="UseIntForBool"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc(XmlDocument xmlDoc, 
                                                DataTable dt, 
                                                string RSName, 
                                                string RecordsetLabel, 
                                                string RecordLabel, 
                                                int StartRecord, 
                                                int MaxRecords,
                                                bool UseIntForBool) {

            XmlNode nodeRS;
            XmlNode nodeRecord;
            int lngEndRecord;
            int lngTotalRecords;
            
            if(dt == null || xmlDoc == null) { return null; }
            
            if(StartRecord <= 0) { StartRecord = 1; }
            if(StartRecord + MaxRecords > dt.Rows.Count) {
                lngEndRecord = dt.Rows.Count;
            } else {
                lngEndRecord = StartRecord + MaxRecords;
            }
            lngTotalRecords = dt.Rows.Count;

            if(RecordsetLabel == null || RecordsetLabel == string.Empty) {
                RecordsetLabel = "Recordset";
            }

            if(RecordLabel == null || RecordLabel == string.Empty) {
                RecordLabel = "Record";
            }
            
            nodeRS = addElement(xmlDoc.DocumentElement, RecordsetLabel);
            
            if(MaxRecords > 0) {
                Pagination.PaginateRS(dt, StartRecord, MaxRecords);
            }
            
            addAttribute(nodeRS, "Name", RSName);
            addAttribute(nodeRS, "StartRecord", StartRecord.ToString());
            addAttribute(nodeRS, "EndRecord", ((StartRecord + dt.Rows.Count) - 1).ToString());
            addAttribute(nodeRS, "TotalRecords", lngTotalRecords.ToString());
            addAttribute(nodeRS, "DisplayRecords", MaxRecords.ToString());
            
            
            foreach(DataRow dr in dt.Rows) {
                nodeRecord = addElement(nodeRS, RecordLabel);
                foreach(DataColumn dc in dr.Table.Columns) {
                    if(!dr.IsNull(dc)) {
                        // TODO: RTrim character fields?
                        switch(dc.DataType.Name.ToString()) {
                            case "DateTime":
                                if(((DateTime)dr[dc]).TimeOfDay > new TimeSpan(0, 0, 0)) {
                                    addAttribute(nodeRecord, dc.ColumnName, dr[dc].ToString());
                                } else {
                                    addAttribute(nodeRecord, dc.ColumnName, ((DateTime)dr[dc]).ToShortDateString());
                                }
                                break;
                            case "Boolean":
                                if(UseIntForBool) {
                                    addAttribute(nodeRecord, dc.ColumnName, ((bool)dr[dc] ? "1" : "0"));
                                    break;
                                } else {
                                    addAttribute(nodeRecord, dc.ColumnName, dr[dc].ToString().Trim());
                                    break;
                                }
                            default:
                                //addElement nodeRecord, rs.fields(i).Name, nodeRecord.NamespaceURI, rs.fields(i).Value
                                addAttribute(nodeRecord, dc.ColumnName, dr[dc].ToString());
                                break;
                        }
                    } else {
                        // TODO: Do not add binary data?
                        //addElement nodeRecord, rs.fields(i).Name, nodeRecord.NamespaceURI, ""
                        addAttribute(nodeRecord, dc.ColumnName, "");
                    }
                }
            }
            
            return(nodeRS);
        }         

        //#####################################################################################
        //#####################################################################################
        //#####################################################################################

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc2(XmlDocument xmlDoc, 
                                                 DataTable dt, 
                                                 string RSName) {

            return(AddRecordsetToDoc2(xmlDoc, dt, RSName, false));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="UseIntForBool"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc2(XmlDocument xmlDoc, 
                                                 DataTable dt, 
                                                 string RSName,
                                                 bool UseIntForBool) {
            return(AddRecordsetToDoc2(xmlDoc, dt, RSName, "Recordset", "Record", "Fld", 0, 0, UseIntForBool));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="FieldLabel"></param>
        /// <param name="StartRecord"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc2(XmlDocument xmlDoc, 
                                                 DataTable dt, 
                                                 string RSName, 
                                                 string RecordsetLabel, 
                                                 string RecordLabel, 
                                                 string FieldLabel, 
                                                 int StartRecord) {

            return(AddRecordsetToDoc2(xmlDoc, dt, RSName, RecordsetLabel, RecordLabel, FieldLabel, StartRecord, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="FieldLabel"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc2(XmlDocument xmlDoc, 
                                                 DataTable dt, 
                                                 string RSName, 
                                                 string RecordsetLabel, 
                                                 string RecordLabel, 
                                                 string FieldLabel, 
                                                 int StartRecord, 
                                                 int MaxRecords) {

            return(AddRecordsetToDoc2(xmlDoc, 
                                      dt, 
                                      RSName, 
                                      RecordsetLabel, 
                                      RecordLabel, 
                                      FieldLabel, 
                                      StartRecord, 
                                      MaxRecords,
                                      false));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="FieldLabel"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <param name="UseIntForBool"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc2(XmlDocument xmlDoc, 
                                                 DataTable dt, 
                                                 string RSName, 
                                                 string RecordsetLabel, 
                                                 string RecordLabel, 
                                                 string FieldLabel, 
                                                 int StartRecord, 
                                                 int MaxRecords,
                                                 bool UseIntForBool) {
            return(AddRecordsetToDoc2(xmlDoc,
                                      dt,
                                      RSName,
                                      RecordsetLabel,
                                      RecordLabel,
                                      FieldLabel,
                                      StartRecord,
                                      MaxRecords,
                                      dt.Rows.Count,
                                      UseIntForBool));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="FieldLabel"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <param name="TotalRecords">Actual RecordCount before pagination</param>
        /// <param name="UseIntForBool"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc2(XmlDocument xmlDoc, 
                                                 DataTable dt, 
                                                 string RSName, 
                                                 string RecordsetLabel, 
                                                 string RecordLabel, 
                                                 string FieldLabel, 
                                                 int StartRecord, 
                                                 int MaxRecords,
                                                 int TotalRecords,
                                                 bool UseIntForBool) {

            XmlNode nodeRS = null;
            XmlNode nodeRecord;
            XmlNode nodeFld = null;
            
            if(dt == null || xmlDoc == null) { 
                return null; 
            }
            
            if(StartRecord <= 0) { 
                StartRecord = 1; 
            }

            if(RSName == null || RSName == string.Empty) {
                RSName = "RS";
            }

            if(RecordsetLabel == null || RecordsetLabel == string.Empty) {
                RecordsetLabel = "Recordset";
            }

            if(RecordLabel == null || RecordLabel == string.Empty) {
                RecordLabel = "Record";
            }

            if(FieldLabel == null || FieldLabel == string.Empty) {
                FieldLabel = "Fld";
            }
            
            nodeRS = addElement(xmlDoc.DocumentElement, RecordsetLabel);
            
            if(MaxRecords > 0) {
                Pagination.PaginateRS(dt, StartRecord, MaxRecords);
            }
            
            //Add recordcounts
            addAttribute(nodeRS, "Name", RSName);
            addAttribute(nodeRS, "StartRecord", ipoLib.IIf(TotalRecords == 0, "0", StartRecord.ToString()));
            addAttribute(nodeRS, "EndRecord", (StartRecord + dt.Rows.Count - 1).ToString());
            addAttribute(nodeRS, "TotalRecords", TotalRecords.ToString());
            addAttribute(nodeRS, "DisplayRecords", MaxRecords.ToString());
                
            foreach(DataRow dr in dt.Rows) {
                nodeRecord = addElement(nodeRS, RecordLabel);
                foreach(DataColumn dc in dr.Table.Columns) {
                    if(!dr.IsNull(dc)) {
                        // TODO: RTrim character fields?
                        switch(dc.DataType.Name.ToString()) {
                            case "DateTime":
                                if(((DateTime)dr[dc]).TimeOfDay > new TimeSpan(0, 0, 0)) {
                                    nodeFld = addElement(nodeRecord, FieldLabel, dr[dc].ToString());
                                } else {
                                    nodeFld = addElement(nodeRecord, FieldLabel, ((DateTime)dr[dc]).ToShortDateString());
                                }
                                break;
                            case "Boolean":
                                if(UseIntForBool) {
                                    nodeFld = addElement(nodeRecord, FieldLabel, ((bool)dr[dc] ? "1" : "0"));
                                    break;
                                } else {
                                    nodeFld = addElement(nodeRecord, FieldLabel, dr[dc].ToString().Trim());
                                    break;
                                }
                            default:
                                nodeFld = addElement(nodeRecord, FieldLabel, dr[dc].ToString().Trim());
                                break;
                        }
                    } else {
                        // TODO: Do not add binary data?
                        nodeFld = addElement(nodeRecord, FieldLabel);
                    }
                    
                    addAttribute(nodeFld, "ID", dc.ColumnName);
                }
            }
            
            return(nodeRS);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="FieldLabel"></param>
        /// <param name="StartRecord"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc3(XmlDocument xmlDoc, 
                                                 DataTable dt, 
                                                 string RSName, 
                                                 string RecordsetLabel, 
                                                 string RecordLabel, 
                                                 string FieldLabel, 
                                                 int StartRecord) {

            return(AddRecordsetToDoc3(xmlDoc, dt, RSName, RecordsetLabel, RecordLabel, FieldLabel, StartRecord, 0, dt.Rows.Count, false));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="FieldLabel"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <param name="TotalRecords">Actual RecordCount before pagination</param>
        /// <param name="UseIntForBool"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToDoc3(XmlDocument xmlDoc, 
                                                 DataTable dt, 
                                                 string RSName, 
                                                 string RecordsetLabel, 
                                                 string RecordLabel, 
                                                 string FieldLabel, 
                                                 int StartRecord, 
                                                 int MaxRecords,
                                                 int TotalRecords,
                                                 bool UseIntForBool) {

            XmlNode nodeRS = null;
            XmlNode nodeRecord;
            XmlNode nodeFld = null;
            
            if(dt == null || xmlDoc == null) { 
                return null; 
            }
            
            if(StartRecord <= 0) { 
                StartRecord = 1; 
            }

            if(RSName == null || RSName == string.Empty) {
                RSName = "RS";
            }

            if(RecordsetLabel == null || RecordsetLabel == string.Empty) {
                RecordsetLabel = "Recordset";
            }

            if(RecordLabel == null || RecordLabel == string.Empty) {
                RecordLabel = "Record";
            }

            if(FieldLabel == null || FieldLabel == string.Empty) {
                FieldLabel = "Fld";
            }
            
            nodeRS = addElement(xmlDoc.DocumentElement, RecordsetLabel);
            
            //Add recordcounts
            addAttribute(nodeRS, "Name", RSName);
            addAttribute(nodeRS, "StartRecord", ipoLib.IIf(TotalRecords == 0, "0", StartRecord.ToString()));
            addAttribute(nodeRS, "EndRecord", (StartRecord + dt.Rows.Count - 1).ToString());
            addAttribute(nodeRS, "TotalRecords", TotalRecords.ToString());
            addAttribute(nodeRS, "DisplayRecords", MaxRecords.ToString());
                
            foreach(DataRow dr in dt.Rows) {
                nodeRecord = addElement(nodeRS, RecordLabel);
                foreach(DataColumn dc in dr.Table.Columns) {
                    if(!dr.IsNull(dc)) {
                        // TODO: RTrim character fields?
                        switch(dc.DataType.Name.ToString()) {
                            case "DateTime":
                                if(((DateTime)dr[dc]).TimeOfDay > new TimeSpan(0, 0, 0)) {
                                    nodeFld = addElement(nodeRecord, FieldLabel, dr[dc].ToString());
                                } else {
                                    nodeFld = addElement(nodeRecord, FieldLabel, ((DateTime)dr[dc]).ToShortDateString());
                                }
                                break;
                            case "Boolean":
                                if(UseIntForBool) {
                                    nodeFld = addElement(nodeRecord, FieldLabel, ((bool)dr[dc] ? "1" : "0"));
                                    break;
                                } else {
                                    nodeFld = addElement(nodeRecord, FieldLabel, dr[dc].ToString().Trim());
                                    break;
                                }
                            default:
                                nodeFld = addElement(nodeRecord, FieldLabel, dr[dc].ToString().Trim());
                                break;
                        }
                    } else {
                        // TODO: Do not add binary data?
                        nodeFld = addElement(nodeRecord, FieldLabel);
                    }
                    
                    addAttribute(nodeFld, "ID", dc.ColumnName);
                }
            }
            
            return(nodeRS);
        }

        //#####################################################################################
        //#####################################################################################
        //#####################################################################################



        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <param name="Columns"></param>
        /// <param name="ColumnsNodeName"></param>
        /// <param name="ColumnNodeName"></param>
        /// <returns></returns>
        public static XmlNode AddColumnsToNode(XmlNode nodeParent, 
                                               DataColumnCollection Columns, 
                                               string ColumnsNodeName, 
                                               string ColumnNodeName) {
            
            XmlNode nodeColumns;
            XmlNode nodeColumn;
            
            if(ColumnsNodeName == null || ColumnsNodeName == string.Empty) {
                ColumnsNodeName = "Columns";
            }

            if(ColumnNodeName == null || ColumnNodeName == string.Empty) {
                ColumnNodeName = "Column";
            }

            nodeColumns = addElement(nodeParent, ColumnsNodeName);
                
            foreach(DataColumn column in Columns) {
                nodeColumn = addElement(nodeColumns, ColumnNodeName);
                addAttribute(nodeColumn, "ColumnName", column.ColumnName);
                addAttribute(nodeColumn, "DataType", column.DataType.Name);
            }
            
            return(nodeColumns);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <param name="UseIntForBool"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToNode(XmlNode node, 
                                                 DataTable dt, 
                                                 string RSName, 
                                                 string RecordsetLabel, 
                                                 string RecordLabel, 
                                                 int StartRecord, 
                                                 int MaxRecords,
                                                 bool UseIntForBool) {

            XmlNode nodeRS = null;
            XmlNode nodeRecord;
            int lngEndRecord;
            int lngTotalRecords;
            
            if(dt == null || node == null) { return null; }
            
            if(StartRecord <= 0) { StartRecord = 1; }
            if(StartRecord + MaxRecords > dt.Rows.Count) {
                lngEndRecord = dt.Rows.Count;
            } else {
                lngEndRecord = StartRecord + MaxRecords;
            }
            lngTotalRecords = dt.Rows.Count;

            if(RecordsetLabel == null||RecordsetLabel == string.Empty) {
                RecordsetLabel = "Recordset";
            }

            if(RecordLabel == null||RecordLabel == string.Empty) {
                RecordLabel = "Record";
            }
            
            nodeRS = addElement(node, RecordsetLabel);
            
            if(MaxRecords > 0) {
                Pagination.PaginateRS(dt, StartRecord, MaxRecords);
            }
            
            addAttribute(nodeRS, "Name", RSName);
            addAttribute(nodeRS, "StartRecord", StartRecord.ToString());
            addAttribute(nodeRS, "EndRecord", (StartRecord + dt.Rows.Count - 1).ToString());
            addAttribute(nodeRS, "TotalRecords", lngTotalRecords.ToString());
            addAttribute(nodeRS, "DisplayRecords", MaxRecords.ToString());
            
            
            foreach(DataRow dr in dt.Rows) {
                nodeRecord = addElement(nodeRS, RecordLabel);
                foreach(DataColumn dc in dr.Table.Columns) {
                    if(!dr.IsNull(dc)) {
                        // TODO: RTrim character fields?
                    switch(dc.DataType.Name.ToString()) {
                            case "DateTime":
                                if(((DateTime)dr[dc]).TimeOfDay > new TimeSpan(0, 0, 0)) {
                                    addAttribute(nodeRecord, dc.ColumnName, dr[dc].ToString());
                                } else {
                                    addAttribute(nodeRecord, dc.ColumnName, ((DateTime)dr[dc]).ToShortDateString());
                                }
                                break;
                            case "Boolean":
                                if(UseIntForBool) {
                                    addAttribute(nodeRecord, dc.ColumnName, ((bool)dr[dc] ? "1" : "0"));
                                    break;
                                } else {
                                    addAttribute(nodeRecord, dc.ColumnName, dr[dc].ToString().Trim());
                                    break;
                                }
                            default:
                                //addElement nodeRecord, rs.fields(i).Name, nodeRecord.NamespaceURI, rs.fields(i).Value
                                addAttribute(nodeRecord, dc.ColumnName, dr[dc].ToString().Trim());
                                break;
                        }
                    } else {
                        // TODO: Do not add binary data?
                        //addElement nodeRecord, rs.fields(i).Name, nodeRecord.NamespaceURI, ""
                        addAttribute(nodeRecord, dc.ColumnName, "");
                    }
                }
            }
            
            return(nodeRS);
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToNode2(XmlNode node, 
                                                  DataTable dt, 
                                                  string RSName) {

            return(AddRecordsetToNode2(node, dt, RSName, "Recordset", "Record", "Fld", 0, 0, false));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="dt"></param>
        /// <param name="RSName"></param>
        /// <param name="RecordsetLabel"></param>
        /// <param name="RecordLabel"></param>
        /// <param name="FieldLabel"></param>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <param name="UseIntForBool"></param>
        /// <returns></returns>
        public static XmlNode AddRecordsetToNode2(XmlNode node, 
                                                  DataTable dt, 
                                                  string RSName, 
                                                  string RecordsetLabel, 
                                                  string RecordLabel, 
                                                  string FieldLabel, 
                                                  int StartRecord, 
                                                  int MaxRecords,
                                                  bool UseIntForBool) {

            XmlNode nodeRS = null;
            XmlNode nodeRecord;
            XmlNode nodeFld;
            int lngTotalRecords;
            
            if(dt == null || node == null) { return null; }

            if(RecordsetLabel == null || RecordsetLabel.Length == 0) {
                RecordsetLabel = "Recordset";
            }
            
            if(RecordLabel == null || RecordLabel.Trim().Length == 0) {
                RecordLabel = "Record";
            }

            if(FieldLabel == null || FieldLabel.Trim().Length == 0) {
                FieldLabel = "Fld";
            }
            
            if(StartRecord <= 0) { StartRecord = 1; }
            
            //Actual RecordCount before pagination
            lngTotalRecords = dt.Rows.Count;
            
            nodeRS = addElement(node, RecordsetLabel);
            
            if(MaxRecords > 0) {
                Pagination.PaginateRS(dt, StartRecord, MaxRecords);
            }
            
            addAttribute(nodeRS, "Name", RSName);
            addAttribute(nodeRS, "StartRecord", ipoLib.IIf(lngTotalRecords == 0, "0", StartRecord.ToString()));
            addAttribute(nodeRS, "EndRecord", (StartRecord + dt.Rows.Count - 1).ToString());
            addAttribute(nodeRS, "TotalRecords", lngTotalRecords.ToString());
            addAttribute(nodeRS, "DisplayRecords", MaxRecords.ToString());
            
            foreach(DataRow dr in dt.Rows) {
                nodeRecord = addElement(nodeRS, RecordLabel);
                foreach(DataColumn dc in dr.Table.Columns) {
                    if(!dr.IsNull(dc)) {
                        switch(dc.DataType.Name.ToString()) {
                            case "DateTime":
                                if(((DateTime)dr[dc]).TimeOfDay > new TimeSpan(0, 0, 0)) {
                                    nodeFld = addElement(nodeRecord, FieldLabel, dr[dc].ToString().TrimEnd());
                                } else {
                                    nodeFld = addElement(nodeRecord, FieldLabel, ((DateTime)dr[dc]).ToShortDateString());
                                }
                                break;
                            case "Boolean":
                                if(UseIntForBool) {
                                    nodeFld = addElement(nodeRecord, FieldLabel, ((bool)dr[dc] ? "1" : "0"));
                                    break;
                                } else {
                                    nodeFld = addElement(nodeRecord, FieldLabel, dr[dc].ToString().Trim());
                                    break;
                                }
                            default:
                                //addElement nodeRecord, rs.fields(i).Name, nodeRecord.NamespaceURI, rs.fields(i).Value
                                nodeFld = addElement(nodeRecord, FieldLabel, dr[dc].ToString());
                                break;
                        }
                    } else {
                        // TODO: Do not add binary data?
                        nodeFld = addElement(nodeRecord, FieldLabel);
                    }
                    
                    addAttribute(nodeFld, "ID", dc.ColumnName);
                }
            }
            
            return(nodeRS);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static XmlDocument GetXMLBase() {
            
            XmlDocument objXML;
            XmlProcessingInstruction  pi;
            XmlNode root;
            
            objXML = new XmlDocument();
            
            pi = objXML.CreateProcessingInstruction("xml", "version=\"1.0\"");
            objXML.InsertBefore(pi, objXML.ChildNodes[0]);
            
            root = objXML.CreateNode(XmlNodeType.Element, "Root", "");
            objXML.AppendChild(root);
            
            return(objXML);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static XmlDocument GetXMLDoc() {
            return(GetXMLDoc("Root"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RootName"></param>
        /// <returns></returns>
        public static XmlDocument GetXMLDoc(string RootName) {

            XmlDocument objXML;
            XmlProcessingInstruction  pi;
            XmlNode root;
            
            objXML = new XmlDocument();
            
            pi = objXML.CreateProcessingInstruction("xml", "version=\"1.0\"");
            objXML.InsertBefore(pi, objXML.ChildNodes[0]);
            
            root = objXML.CreateNode(XmlNodeType.Element, RootName, "");
            objXML.AppendChild(root);
            
            return(objXML);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <param name="InputDate"></param>
        /// <param name="NodeName"></param>
        public static void AddDateAsNode(XmlNode nodeParent, 
                                         DateTime InputDate, 
                                         string NodeName) {

            XmlElement nodeDate;
            
            nodeDate = addElement(nodeParent, NodeName);
            addAttribute(nodeDate, "Date", InputDate.ToShortDateString());
            addAttribute(nodeDate, "Time", InputDate.ToShortTimeString());
            addAttribute(nodeDate, "Month", InputDate.Month.ToString());
            addAttribute(nodeDate, "Day", InputDate.Day.ToString());
            addAttribute(nodeDate, "Year", InputDate.Year.ToString());
            addAttribute(nodeDate, "DayOfWeek", InputDate.DayOfWeek.ToString());
            addAttribute(nodeDate, "Hour", InputDate.Hour.ToString());
            addAttribute(nodeDate, "NumericDate", InputDate.ToString("yyyyMMdd"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public static long GetActivityIDFromXml(XmlDocument xmlDoc) {

            long lTemp;
            long activityID = 0;
            XmlNode nodeTemp;
            
            nodeTemp = xmlDoc.DocumentElement.SelectSingleNode("Page");
            if(nodeTemp != null) {
                if(nodeTemp.Attributes.GetNamedItem("ActivityLogID") != null && 
                    long.TryParse(nodeTemp.Attributes.GetNamedItem("ActivityLogID").InnerText, out lTemp)) {
                        activityID = lTemp;
                }
            }

            return (activityID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="FieldName"></param>
        /// <param name="FieldValue"></param>
        public static void AddQueryStringValueAsNode(XmlNode node, 
                                                     string FieldName, 
                                                     string FieldValue) {

            XmlNode nde;
            
            // retrieve ref to form node (create if necessary)
            nde = node.SelectSingleNode("QueryString");
            if(nde == null) {
                nde = ipoXmlLib.addElement(node, "QueryString");
            }
            
            //add child to form node
            ipoXmlLib.addElement(nde, FieldName, FieldValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeRecordset"></param>
        /// <param name="childNodeName"></param>
        /// <param name="StartRecord"></param>
        /// <param name="RecordsPerPage"></param>
        /// <returns></returns>
        public static XmlNode PaginateXmlNode(XmlNode nodeRecordset,
                                              string childNodeName,
                                              int StartRecord,
                                              int RecordsPerPage) {

            return(PaginateXmlNode(nodeRecordset,
                                   childNodeName,
                                   StartRecord,
                                   RecordsPerPage,
                                   string.Empty,
                                   string.Empty));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeRecordset"></param>
        /// <param name="childNodeName"></param>
        /// <param name="StartRecord"></param>
        /// <param name="RecordsPerPage"></param>
        /// <param name="TotalRecordsLabel"></param>
        /// <param name="EndRecordLabel"></param>
        /// <returns></returns>
        public static XmlNode PaginateXmlNode(XmlNode nodeRecordset,
                                              string childNodeName,
                                              int StartRecord,
                                              int RecordsPerPage,
                                              string TotalRecordsLabel,
                                              string EndRecordLabel) {

            XmlNodeList nlNodes;

            if(TotalRecordsLabel == null || TotalRecordsLabel.Length == 0) {
                TotalRecordsLabel = "TotalRecords";
            }

            if(EndRecordLabel == null || EndRecordLabel.Length == 0) {
                EndRecordLabel = "EndRecord";
            }
            
            nlNodes = nodeRecordset.SelectNodes(childNodeName);
            
            if(nlNodes == null) {
                ipoXmlLib.addAttribute(nodeRecordset, TotalRecordsLabel, "0");
                ipoXmlLib.addAttribute(nodeRecordset, EndRecordLabel, "0");
                return(nodeRecordset);
            }
            
            if(nlNodes.Count <= RecordsPerPage) {
                ipoXmlLib.addAttribute(nodeRecordset, TotalRecordsLabel, nlNodes.Count.ToString());
                ipoXmlLib.addAttribute(nodeRecordset, EndRecordLabel, nlNodes.Count.ToString());
                return(nodeRecordset);
            }

            if(nlNodes.Count == 0) {
                ipoXmlLib.addAttribute(nodeRecordset, TotalRecordsLabel, "0");
                ipoXmlLib.addAttribute(nodeRecordset, EndRecordLabel, "0");
                return(nodeRecordset);
            }

            if(StartRecord > nlNodes.Count) {
                StartRecord = 1;
            }
            
            if(StartRecord + RecordsPerPage < nlNodes.Count) {
                ipoXmlLib.addAttribute(nodeRecordset, EndRecordLabel, (StartRecord + RecordsPerPage).ToString());
            } else {
                ipoXmlLib.addAttribute(nodeRecordset, EndRecordLabel, nlNodes.Count.ToString());
            }
            ipoXmlLib.addAttribute(nodeRecordset, TotalRecordsLabel, nlNodes.Count.ToString());
            
            for(int i=0;i<nlNodes.Count;++i) {
                if(i + 1 < StartRecord || i + 1 >= RecordsPerPage + StartRecord) {
                    nlNodes[i].ParentNode.RemoveChild(nlNodes[i]);
                }
            }
           
            return(nodeRecordset);
        }
    }
}
