using System;
using System.Runtime.InteropServices;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     02/07/2007
*
* Purpose:  ipo DateTime common library.
*
* Modification History
*   02/07/2007 CR 17138 JMC
*       - Created class
*   10/28/2008 CR 25946 JMC
*       - Added DateDiff method.  Made SystemTime and TimeZoneInformation 
*         structures internal to the class.
*   WI 90277 WJS 02/07/2013 
*           FP: Add TimeZoneBias for notification display
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
* WI 93682 CEJ 03/29/2013 
*  - BNYM Security Audit:  6.6.2 Online Admin: Sensitive Data Cached in the Browser
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary></summary>
    public enum DateInterval {
        /// <summary></summary>
        Second, 
        /// <summary></summary>
        Minute, 
        /// <summary></summary>
        Hour, 
        /// <summary></summary>
        Day, 
        /// <summary></summary>
        Week, 
        ////Month, 
        ////Quarter, 
        ////Year
    }

    /// <summary>
    /// 
    /// </summary>
    public class ipoDateTimeLib {

        [StructLayoutAttribute(LayoutKind.Sequential)]
        private struct SystemTime {

            public short year;
            public short month;
            public short dayOfWeek;
            public short day;
            public short hour;
            public short minute;
            public short second;
            public short milliseconds;
        }

        /// <summary>
        /// 
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private struct TimeZoneInformation {

            /// <summary></summary>
            public int bias;

            /// <summary></summary>
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string standardName;

            SystemTime standardDate;

            /// <summary></summary>
            public int standardBias;

            /// <summary></summary>
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string daylightName;

            SystemTime daylightDate;

            /// <summary></summary>
            public int daylightBias;
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern int GetTimeZoneInformation(out TimeZoneInformation lpTimeZoneInformation);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static int GetTimeZoneBias() {

            const int TIME_ZONE_ID_UNKNOWN = 0;
            const int TIME_ZONE_ID_STANDARD = 1;
            const int TIME_ZONE_ID_DAYLIGHT = 2;

            // create struct instance
            TimeZoneInformation tziInfo;

            int intRetVal;

            // retrieve timezone info
            int intCurrentTimeZone = GetTimeZoneInformation(out tziInfo);

            switch(intCurrentTimeZone) {
                case TIME_ZONE_ID_UNKNOWN:
                    intRetVal = tziInfo.bias;
                    break;
                case TIME_ZONE_ID_STANDARD:
                    intRetVal = tziInfo.bias + tziInfo.standardBias;
                    break;
                case TIME_ZONE_ID_DAYLIGHT:
                    intRetVal = tziInfo.bias + tziInfo.daylightBias;
                    break;
                default:
                    intRetVal = tziInfo.bias;
                    break;
            }

            return intRetVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Interval"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public static long DateDiff(DateInterval Interval, 
                                    DateTime StartDate, 
                                    DateTime EndDate) {

            long lngDateDiffValue = 0;
            System.TimeSpan objTS = new System.TimeSpan(EndDate.Ticks - StartDate.Ticks);

            switch(Interval) {
                case DateInterval.Day:
                    lngDateDiffValue = (long)objTS.Days;
                    break;
                case DateInterval.Hour:
                    lngDateDiffValue = (long)objTS.TotalHours;
                    break;
                case DateInterval.Minute:
                    lngDateDiffValue = (long)objTS.TotalMinutes;
                    break;
                ////case DateInterval.Month:
                ////    lngDateDiffValue = (long)(objTS.Days / 30);
                ////    break;
                ////case DateInterval.Quarter:
                ////    lngDateDiffValue = (long)((objTS.Days / 30) / 3);
                ////    break;
                case DateInterval.Second:
                    lngDateDiffValue = (long)objTS.TotalSeconds;
                    break;
                case DateInterval.Week:
                    lngDateDiffValue = (long)(objTS.Days / 7);
                    break;
                ////case DateInterval.Year:
                ////    lngDateDiffValue = (long)(objTS.Days / 365);
                ////    break;
                default:
                    throw(new Exception("Invalid Interval specified: " + Interval.ToString()));
            }

            return(lngDateDiffValue);
        } //end of DateDiff

        /// <summary>
        /// Return flag to indicate daylight savings time
        /// </summary>
        /// <returns>
        /// true - In daylight savings time
        /// false - not in daylight savings time</returns>
        public static bool GetDaylightSavingsFlag() {
            const int TIME_ZONE_ID_DAYLIGHT = 2;

            // create struct instance
            TimeZoneInformation tziInfo;

            // retrieve timezone info
            int intCurrentTimeZone = GetTimeZoneInformation(out tziInfo);

            if (intCurrentTimeZone == TIME_ZONE_ID_DAYLIGHT) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
