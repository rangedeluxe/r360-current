﻿using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 32562 JMC 03/22/2011 
*   -Consolidated system exception definitions here.
* CR 50458 WJS 02/21/2012
*   -Added invalid ImageContract Type
* CR 54211 JMC 07/17/2012
*   -Added invalid InvalidNotificationFileContract exception
* WI 143419 DJW 6/3/2014 
*    Changed BatchID to SourceBatchID
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary></summary>
    public class ipoException : System.Exception {

        /// <summary></summary>
        public ipoException(string message) {
            Source = message;
        }

        /// <summary></summary>
        public override string Message { 
            get { 
                return Source; 
            }
        }

        /// <summary></summary>
        public string ipoMessage { 
            get { 
                return "Unable to complete request at this time. If the problem persists contact your system administrator.";
            }
        }

        /// <summary></summary>
        public override string Source {
            get {
                return base.Source;
            }
            set {
                base.Source = value;
            }
        }
    }

    //**********************************************
    // Invalid Header Exceptions
    //**********************************************
    /// <summary></summary>
    public class InvalidSiteKeyException : Exception {

        /// <summary></summary>
        public override string Message {
            get {
                return "Invalid Site Key";
            }
        }
    }

    /// <summary></summary>
    public class InvalidSessionIDException : Exception {

        /// <summary></summary>
        public override string Message {
            get {
                return "Invalid Session ID";
            }
        }
    }

      /// <summary></summary>
    public class InvalidImageContract : Exception {

        /// <summary></summary>
        public override string Message {
            get {
                return "Invalid Image Contract";
            }
        }
    }

      /// <summary></summary>
    public class InvalidNotificationFileContract : Exception {

        /// <summary></summary>
        public override string Message {
            get {
                return "Invalid Notification File Contract";
            }
        }
    }
    //**********************************************


    //**********************************************
    // Logon Exceptions
    //**********************************************
    /// <summary></summary>
    public class AccountDisabledException : Exception {

        /// <summary></summary>
        public override string Message {
            get {
                return "The requested User account has been disabled.";
            }
        }
    }

    /// <summary></summary>
    public class MaxLogonAttemptsExceededException : Exception {

        /// <summary></summary>
        public override string Message {
            get {
                return "The requested User account has been disabled.";
            }
        }
    }

    /// <summary></summary>
    public class InvalidCredentialsException : Exception {

        /// <summary></summary>
        public override string Message {
            get {
                return("Logon credentials supplied were not valid.");
            }
        }
    }
    //**********************************************



    //**********************************************
    // Session Exceptions
    //**********************************************
    /// <summary></summary>
    public class SessionExpiredException : Exception {

        /// <summary></summary>
        public override string Message {
            get {
                return "Session has expired.";
            }
        }
    }

    /// <summary></summary>
    public class InvalidSessionException : Exception {

        private string _Message = "Session is not valid.";

        /// <summary></summary>
        public InvalidSessionException(string message) : base(message) {
            _Message = message;
        }

        /// <summary></summary>
        public InvalidSessionException() {
        }

        /// <summary></summary>
        public override string Message {
            get {
                return(_Message);
            }
        }
    }
    //**********************************************



    //**********************************************
    // Image Services
    //**********************************************
    /// <summary></summary>
    public class ImageNotFoundInDbException : Exception {

        private string _FileInfo;

        /// <summary></summary>
        public ImageNotFoundInDbException(int BankID, int LockboxID, int ProcessingDateKey, long BatchID, int BatchSequence)
        {
            _FileInfo = "BankID=" + BankID.ToString() + ", " + 
                        "LockboxId=" + LockboxID.ToString() + ", " + 
                        "ProcessingDateKey=" + ProcessingDateKey.ToString() + ", " +
                        "BatchID=" + BatchID.ToString() + ", " +
                        "BatchSequence=" + BatchSequence.ToString();
        }

        /// <summary></summary>
        public override string Message {
            get {
                return("Requested image was not found in database: " + _FileInfo);
            }
        }
    }

    /// <summary></summary>
    public class ImageNotFoundOnDiskException : Exception {

        private string _FileInfo;

        /// <summary></summary>
        public ImageNotFoundOnDiskException(int BankID, int LockboxID, int ProcessingDateKey, long BatchID, int BatchSequence) {
            _FileInfo = "BankID=" + BankID.ToString() + ", " + 
                        "LockboxId=" + LockboxID.ToString() + ", " + 
                        "ProcessingDateKey=" + ProcessingDateKey.ToString() + ", " +
                        "BatchID=" + BatchID.ToString() + ", " +
                        "BatchSequence=" + BatchSequence.ToString();
        }

        /// <summary></summary>
        public override string Message {
            get {
                return("Requested image was not found on disk: " + _FileInfo);
            }
        }
    }
    //**********************************************
    // Image Services
    //**********************************************
    /// <summary></summary>
    public class ImageNotFoundException : Exception
    {

        private string _FileInfo;

        /// <summary></summary>
        public ImageNotFoundException(int BankID, int LockboxID, int ProcessingDateKey, int BatchID, int BatchSequence)
        {
            _FileInfo = BankID.ToString() + " " +
                        LockboxID.ToString() + " " +
                        ProcessingDateKey.ToString() + " " +
                        BatchID.ToString() + " " +
                        BatchSequence.ToString();
        }

        /// <summary></summary>
        public override string Message
        {
            get
            {
                return ("Requested image was not found.");
            }
        }
    }
    //**********************************************
}
