using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     02/24/2009
*
* Purpose:  IPOnline common enumeration class.
*
* Modification History
* JMC 02/24/2009
*   -Initial release.
* CR 28522 JMC 01/06/2010
*   -Added LogonMethodType enum
* CR 32562 JMC 03/02/2011 
*   -Added Partition Manager and OLFServices types to UserTypes enum.
* CR 31536 JMC 04/02/2011
*   -Added WFSDataTypes enum.  Future apps should convert from DMPFieldTypes.
* CR 32538 JMC 03/24/2011 
*   -Added BatchSourceKeys enum.
* CR 32188 JMC 03/30/2010
*   -Added LTAConstants static class.
* CR 33542 JCS 04/05/2011
*   -Added SSOIDMappingTypes enum
* CR 32263 WJS 8/2/2011
*   -FFIEC Reqs for password
* CR 28865 JNE 8/26/2011
*   -Added PreDef Search Constant.
* CR 46328 WJS 09/02/2011
*	-Add hard code email text
* CR 46139 JCS 09/08/2011
*   -Added InvoiceBalancingOptions enum
*   -Added InvoiceBalancingActions enum
* CR 46314 WJS 9/26/2011
*	-Add max minutes for temp password expired
* CR 46996 JMC 11/02/2011
*   -Aded OLPreferencesDataTypes enumeration
* CR 47771 WJS 11/21/2011
*	-Added a custom type for customDLL
* CR 33230 JMC 01/13/2012
*	-Added OnlineFormattedException class
* CR 36315, CR 36316, CR 36317, CR 36318 JNE 01/19/2012
*   -Added OLPreferenceAppType
* CR 33230 JMC 01/24/2012
*   -Added OLFServices User to the UserTypes enumeration.
* CR 51600 WJS 03/28/2012
*   -Added BatchPayments enumeration for use for Data Import Tool Kit
* CR 53488 WJS 6/21/2012
*   -Enforce captial letter to be in password
* CR 54427 JCS 07/24/2012
*   -Added SSOIDMappingTypes enum CustomerExternalID1UserExternalIDHash
* CR 52959 JNE 08/17/2012
*    -Added FILEGROUP to enum ImageStorageMode
* WI 91994 TWE 03/14/2013
*   -Add new SSO Mapping types (SSOIDMappingTypes) enum UniqueUserExternalID1Only
*   -Added FILEGROUP to enum ImageStorageMode
* WI 89968 CRG 03/01/2013
*   -Modify ipoLIB for .NET 4.5
* WI 94263 TWE 04/24/2013
*   -Restrict access to Integrapay areas:  Events; Reports; Extracts;
* WI 94264 JMC 06/23/2013
*   -Added ExtractWizard and ExtractServices to UserTypes enum.
* WI 117562 CEJ 10/17/2013
*   Add new activity codes for the R360 web services calls
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary></summary>
    public static class LTAConstants {
        /// <summary></summary>
        public const byte COLOR_MODE_BLACK_AND_WHITE = 1;
        /// <summary></summary>
        public const byte COLOR_MODE_COLOR = 2;
        /// <summary></summary>
        public const byte COLOR_MODE_GRAYSCALE = 4;
        /// <summary></summary>
        public const string PREDEFSEARCH_ADHOC = "Ad Hoc Query";
    }

    /// <summary>
    /// 
    /// </summary>
    public enum OLFImageFilterOption {
        /// <summary></summary>
        All = 0, 
        /// <summary></summary>
        ChecksOnly = 1, 
        /// <summary></summary>
        DocsOnly = 2
    }


    /// <summary>
    /// Enumeration of possible job status states.
    /// </summary>
    public enum OLFJobStatus {
        /// <summary></summary>
        Pending=1, 
        /// <summary></summary>
        Processing=2, 
        /// <summary></summary>
        Completed=3, 
        /// <summary></summary>
        Error=4
    }

    /// <summary>
    /// Enumeration of possible states for display sides of image
    /// </summary>
    public enum OLFImageDisplayMode {
        /// <summary></summary>
        Undefined = 0,
        /// <summary></summary>
        BothSides = 1, 
        /// <summary></summary>
        FrontOnly = 2
    }
    /// <summary>
    /// Enumeration of possible OLF Delivery methods
    /// </summary>
    public enum OLFDeliveryMethod{
        /// <summary></summary>
        DetermineBySize,
        /// <summary></summary>
        Download,
        /// <summary></summary>
        Stream
    } 
    // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
    /// <summary>
    /// 
    /// </summary>
    public enum ImageStorageMode
    {
        /// <summary></summary>
        PICS = 0,
        /// <summary></summary>
        HYLAND = 1,
        /// <summary>
        /// Custom image option
        /// </summary>
        CUSTOM = 2,
        /// <summary></summary>
        FILEGROUP = 3
    }    

    /// <summary>
    /// 
    /// </summary>
    public enum WorkgroupColorMode
    {
        /// <summary></summary>
        COLOR_MODE_BITONAL = 1,
        /// <summary></summary>
        COLOR_MODE_COLOR = 2,
        /// <summary></summary>
        COLOR_MODE_GRAYSCALE = 4
    }
    // END MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 

    /// <summary>
    /// 
    /// </summary>
    public enum ActivityCodes {
        /// <summary></summary>
        acUnknown = 0,
        /// <summary></summary>
        acPageAccess = 1,
        /// <summary></summary>
        acViewImageFront = 11,
        /// <summary></summary>
        acViewImageBack = 12,
        /// <summary></summary>
        acViewAll = 13,
        /// <summary></summary>
        acViewSingle = 14,
        /// <summary></summary>
        acSearchResultsPrintView = 21,
        /// <summary></summary>
        acSearchResultsPdfView = 22,
        /// <summary></summary>
        acSearchResultsCsv = 23,
        /// <summary></summary>
        acSearchResultsZipDownload = 24,
        /// <summary></summary>
        acSearchResultsDownloadCsv = 25,
        /// <summary></summary>
        acSearchResultsDownloadXml = 26,
        /// <summary></summary>
        acSearchResultsDownloadHtml = 27,
        /// <summary></summary>
        acDownloadImageFront = 41,
        /// <summary></summary>
        acDownloadImageBack = 42,
        /// <summary></summary>
        acR360WebServiceCall = 43,
        /// <summary></summary>
        acCommonExceptionsWebServiceCall = 44,
        /// <summary></summary>
        acHubReportWebServiceCall = 45
    }

    /// <summary></summary>
    public enum UserTypes {
        /// <summary></summary>
        Online=0,
        /// <summary></summary>
        Research=1,
        /// <summary></summary>
        Admin=2,
        /// <summary></summary>
        PartitionManager=3,
        /// <summary></summary>
        OLFServices=4,
        /// <summary></summary>
        DataImportServices=5,
        /// <summary></summary>
        ExtractWizard = 6,
        /// <summary></summary>
        ExtractServices = 7
    }

    /// <summary></summary>
    public enum DepositStatusValues {
        /// <summary></summary>
        DepositPendingDecision = 240,
        /// <summary></summary>
        DepositInDecision = 241,
        /// <summary></summary>
        DepositPendingSplit = 245,
        /// <summary></summary>
        PendingCAR = 250,
        /// <summary></summary>
        PendingKFI = 350,
        /// <summary></summary>
        PendingDecisionSplit = 427,
        /// <summary></summary>
        PendingEncode = 550,
        /// <summary></summary>
        PendingPrint = 750,
        /// <summary></summary>
        DepositComplete = 850
    }

    /// <summary></summary>
    public enum FieldActions {
        /// <summary></summary>
        NoAction,
        /// <summary></summary>
        Update,
        /// <summary></summary>
        Delete
    }

    /// <summary></summary>
    public enum FieldDecisionStatusTypes {
        /// <summary></summary>
        Undefined = -1,
        /// <summary></summary>
        NoDecisioningRequired = 0,
        /// <summary></summary>
        PendingDecision = 1,
        /// <summary></summary>
        Accept = 2,
        /// <summary></summary>
        Reject = 3
    }

    /// <summary></summary>
    public enum DecisionStatusTypes {
        /// <summary></summary>
        NoDecisioningRequired = 0,
        /// <summary></summary>
        DecisionRequired = 1,
        /// <summary></summary>
        OverrideDecisionAllowed = 2,
        /// <summary></summary>
        DecisionComplete = 3
    }

    /// <summary></summary>
    public enum DecisionSrcStatusTypes {
        /// <summary></summary>
        Undefined = -1,
        /// <summary></summary>
        NoDecisionRequired = 0,
        /// <summary></summary>
        PendingDecisionBatchLogon = 1,
        /// <summary></summary>
        PendingDecisionBatchLogonTDE = 2,
        /// <summary></summary>
        Accept = 3,
        /// <summary></summary>
        Reject = 4
    }

    /// <summary>
    /// 
    /// </summary>
    public enum DocumentTypes {
        /// <summary></summary>
        Check = 0,
        /// <summary></summary>
        Stub = 1
    }

    /// <summary>
    /// 
    /// </summary>
    public enum DataEntryStatusValues {
        /// <summary></summary>
        NotSet = -1,
        /// <summary></summary>
        NoDataEntry = 0,
        /// <summary></summary>
        PendingDataEntry = 1,
        /// <summary></summary>
        InDataEntry = 2,
        /// <summary></summary>
        IncompleteDataEntry = 3,
        /// <summary></summary>
        PendingVerify = 4,
        /// <summary></summary>
        InVerify = 5,
        /// <summary></summary>
        IncompleteVerify = 6,
        /// <summary></summary>
        Split = 7,
        /// <summary></summary>
        DoneDataEntry = 8,
        /// <summary></summary>
        Extracted = 9,
        /// <summary></summary>
        PendingRegistration = 20,
        /// <summary></summary>
        InRegistration = 25,
        /// <summary></summary>
        PendingProfiling = 30,
        /// <summary></summary>
        InProfiling = 35,
        /// <summary></summary>
        PendingRecognition = 40,
        /// <summary></summary>
        InRecognition = 45
    }

    /// <summary>
    /// 
    /// </summary>
    public enum AuditFileEventTypes {
        /// <summary></summary>
        BatchBeginWork,
        /// <summary></summary>
        BatchSaved,
        /// <summary></summary>
        BatchCompleted,
        /// <summary></summary>
        BatchReset,
        /// <summary></summary>
        TransactionRejected,
        /// <summary></summary>
        StubDeleted
    }

    /// <summary>
    /// 
    /// </summary>
    public enum DMPFieldTypes {
        /// <summary></summary>
        UNKNOWN = -1,
        /// <summary></summary>
        STRING = 1,
        /// <summary></summary>
        FLOAT = 6,
        /// <summary></summary>
        CURRENCY = 7,
        /// <summary></summary>
        DATE = 11
    }

    /// <summary>
    /// 
    /// </summary>
    public enum WFSDataTypes {
        /// <summary></summary>
        UNKNOWN = -1,
        /// <summary></summary>
        STRING = 1,
        /// <summary></summary>
        INT = 3,
        /// <summary></summary>
        BIT = 4,
        /// <summary></summary>
        FLOAT = 6,
        /// <summary></summary>
        CURRENCY = 7,
        /// <summary></summary>
        DATE = 11
    }

    /// <summary>
    /// 
    /// </summary>
    public enum OLPreferencesDataTypes {
        /// <summary></summary>
        STRING = 1,
        /// <summary></summary>
        BIT = 4,
        /// <summary></summary>
        INT16 = 5,               // 16 bit signed number
        /// <summary></summary>
        INT32 = 6,               // 32 bit signed number
        /// <summary></summary>
        ImageDisplayMode = 101,
        /// <summary></summary>
        EMailString = 102,
        /// <summary></summary>
        RegEx = 103
    }

    /// <summary>CBXOnline Logon Authentication Methods</summary>
    public enum AuthenticationMethods {
        /// <summary></summary>
        cboDefaultMethod = 0,
        /// <summary></summary>
        cboCorbaSSO = 1,
        /// <summary></summary>
        cbowebACCESS = 3
    }

    /// <summary></summary>
    public enum TableTypes {
        /// <summary></summary>
        Checks=0,
        /// <summary></summary>
        Stubs=2,
        /// <summary></summary>
        Documents=4
    }

    /// <summary>
    /// 
    /// </summary>
    public enum EventType {
        /// <summary></summary>
        Error = 0,
        /// <summary></summary>
        Information = 1,
        /// <summary></summary>
        Warning = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum LogonMethodType {
        /// <summary>CBXOnline authentication (default, if key is missing) </summary>
        Standard = 0,
        /// <summary>CORBA authentication</summary>
        Corba = 1,
        /// <summary>ExternalID1 Logon</summary>
        CustomerExtID1Logon = 2,
        /// <summary>Fundtech webACCESS authentication</summary>
        WebAccess=3,
        /// <summary>Multi-Factor Authentication</summary>
        MultiFactor=4,
        /// <summary>Single Sign-On</summary>
        SingleSignOn = 5
    }


    /// <summary>
    /// 
    /// </summary>
    public delegate void outputMessageEventHandler(string message
                                                 , string src
                                                 , MessageType messageType
                                                 , MessageImportance messageImportance);

    /// <summary>
    /// 
    /// </summary>
    public delegate void outputErrorEventHandler(System.Exception e);

    /// <summary>
    /// MessageType enumeration
    /// </summary>
    public enum MessageType {
        /// <summary></summary>
        Error = 0,
        /// <summary></summary>
        Information = 1,
        /// <summary></summary>
        Warning = 2
    }

    /// <summary>
    /// MessageImportance enumeration
    /// </summary>
    public enum MessageImportance {
        /// <summary></summary>
        Essential = 0,
        /// <summary></summary>
        Verbose = 1,
        /// <summary></summary>
        Debug = 2
    }

    /// <summary></summary>
    public enum OLFServiceErrorCodes {
        /// <summary></summary>
        Success = 200,
        /// <summary></summary>
        UndefinedError = 700,
        /// <summary></summary>
        InvalidSiteKey = 701,
        /// <summary></summary>
        InvalidSessionID = 702,
        /// <summary></summary>
        SessionExpired = 703,
        /// <summary></summary>
        InvalidSession = 704,
        /// <summary></summary>
        ImageNotFoundInDb = 705,
        /// <summary></summary>
        ImageNotFoundOnDisk = 706,
        /// <summary></summary>
        CommunicationError = 731,
        /// <summary></summary>
        ConfigurationError = 732
    }
    /// <summary>
    /// BatchSource enumeration
    /// </summary>
    public enum BatchSourceKeys 
    {
        /// <summary></summary>
        Unknown = 0,
        /// <summary></summary>
        integraPAY = 1,
        /// <summary></summary>
        WebDDL = 2,
        /// <summary></summary>
        ImageRPS = 3,
        /// <summary></summary>
        ICON = 101
    }

    /// <summary></summary>
    public enum SSOIDMappingTypes {
        /// <summary> </summary>
        NoMappingRequired = 0,
        /// <summary> </summary>
        CustomerExternalID1Only = 1,
        /// <summary> </summary>
        UserExternalID1Only = 2,
        /// <summary> </summary>
        CustomerExternalID1UserExternalID1 = 3,
        /// <summary> </summary>
        CustomerExternalID1UserExternalIDHash = 4,
        /// <summary> </summary>
        UniqueUserExternalID1Only = 5
    }
    /// <summary>
    /// 
    /// </summary>
    public static class FFIECPasswordConstraints
    {
        /// <summary>
        /// Password min length for FFIEC 
        /// </summary>
        public const int PasswordMinLength = 8;


        //This is minimum password required for FFIEC 
        // One captial letter
        // One smaller letter
        // one number
        // one special character

        //if you want to do it all at once (i broke into two parts
        // ^(?!.*(.)\1{2})(((?=.*?\p{L})(?=.*?\d))|((?=.*?\p{L})(?=.*?[!@#$%^&*()+=._-]))|((?=.*?[!@#$%^&*()+=._-])(?=.*?\d)))([-!@#$%^&*()+=.\w]){8,}$

        /// <summary>
        /// Password required format
        /// </summary>
        /// This takes care one captial letter, one small letter, one number and one special character
        public const string PasswordRequiredFormat = "([a-z]{1,}).*([0-9]{1,})|[0-9]{1,}.*[A-Z]{1,}";

        /// <summary>
        /// Password special character
        /// </summary>
        /// one special character of the following type
        public const string PasswordSearchForSpecialCharcters = "[A-Z]{1,}.*[~,|,?,=,@,\\,^,_,$,#,!,*,/]{1,}";

        /// <summary>
        /// Passwword repeaating characters
        /// </summary>
        public const string PasswordRepeatingCharacters="^(?!.*(.)\\1{2})";
        /// <summary>
        /// lockout inactive timer
        /// </summary>
        public const int LockoutInactiveUserAccountsMaxSize = 60;

        /// <summary>
        /// number of days password history
        /// </summary>
        public const int NumberOfDaysPasswordHistory = 180;

        /// <summary>
        /// 4 hours (or 240 minutes)
        /// </summary>
        public const int ChangePasswordMinutesExpiredMaxSize = 240;
    }
 
    /// <summary>
    /// 
    /// </summary>
    public static class EmailStrings
    {
        /// <summary>
        /// Email subject hard coded
        /// </summary>
        public const string EMAIL_SUBJECT = "Your Online Account Information";
        /// <summary>
        /// Email top body
        /// </summary>
        public const string EMAIL_BODY1 = "Dear ";
        /// <summary>
        /// Email mid body
        /// </summary>
        public const string EMAIL_BODY2 = "Thanks for visiting our web site.  This e-mail confirms that your temporary password is the following: ";
     
    }
    /// <summary>
    /// Online Decisioning Invoice Balancing Options
    /// </summary>
    public enum InvoiceBalancingOptions {
        /// <summary></summary>
        BalancingNotRequired = 0,
        /// <summary></summary>
        BalancingDesired = 1,
        /// <summary></summary>
        BalancingRequired = 2
    }

    /// <summary>
    /// Online Decisioning Invoice Balancing Actions
    /// </summary>
    public enum InvoiceBalancingActions {
        /// <summary></summary>
        Undefined = 0,
        /// <summary></summary>
        SaveUnbalanced = 1,
        /// <summary></summary>
        ForceBalance = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public class OnlineFormattedException : Exception {
        /// <summary></summary>
        public OnlineFormattedException(string message) : base(message) {
        }
    }

    /// <summary>
    /// OlPreference App Type
    /// </summary>

    public enum OLPreferenceAppType {
        /// <summary></summary>
        Online = 0,
        /// <summary></summary>
        CSRResearch = 1,
        /// <summary></summary>
        Admin = 2,
        /// <summary></summary>
        System = 99
    }

    /// <summary>
    /// Batch Payment Types
    /// </summary>
    public enum BatchPaymentType
    {
        /// <summary> </summary>
        LOCKBOX = 0,
        /// <summary> </summary>
        ACH = 1,
        /// <summary> </summary>
        WIRE = 2,
        /// <summary> </summary>
        CREDIT_CARD =3
    }

    /// <summary>
    /// Integrapay Options Enabled
    /// </summary>
    public enum enmIPCheckEnabled
    {
        /// <summary> IntegraPay Events </summary>
        Events,
        /// <summary> IntegraPay Reports</summary>
        Reports,
        /// <summary> IntegraPay Extracts</summary>
        Extracts,
        /// <summary> IntegraPay Decisioning</summary>
        Decisioning,
        /// <summary> IntegraPay Remitters</summary>
        Remitters
    }
}
