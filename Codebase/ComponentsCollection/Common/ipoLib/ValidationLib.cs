﻿using System;
using System.Text.RegularExpressions;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Author
* Date:     08/05/2008
*
* Purpose:  IPOnline common validation library class.
*
* Modification History
*   08/05/2008 JMC
*       - Initial version.
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public static class ValidationLib {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RoutingNumber"></param>
        /// <param name="FailureReason"></param>
        /// <returns></returns>
        public static bool ValidateRT(string RoutingNumber, out string FailureReason) {

            Regex objRegEx;
            bool bolRetVal;
            string strFailureReason;

		    objRegEx = new Regex(@"^[0-9]{1,9}$", RegexOptions.IgnoreCase);
		
		    // Validate Routing Number
		    if(objRegEx.IsMatch(RoutingNumber) && RoutingNumber.Length == 9) {
			    if(ValidationLib.ValidateRTCheckDigit(RoutingNumber)) {
		            bolRetVal = true;
			        strFailureReason = string.Empty;
                } else {
		            bolRetVal = false;
			        strFailureReason = "Routing Number check digit is invalid.";
			    }
		    } else if(RoutingNumber.Length <= 30 &&  RoutingNumber.Length > 0) {
			    objRegEx = new Regex(@"^[0-9]{1,30}$", RegexOptions.IgnoreCase);
			    if(objRegEx.IsMatch(RoutingNumber)) {
		            bolRetVal = true;
			        strFailureReason = string.Empty;
                } else {
				    objRegEx = new Regex(@"^[0-9]{1,}-[0-9]{1,}$", RegexOptions.IgnoreCase);
				    if(objRegEx.IsMatch(RoutingNumber)) {
		                bolRetVal = true;
			            strFailureReason = string.Empty;
                    } else {
		                bolRetVal = false;
			            strFailureReason = "Routing Number contains invalid character(s).";
				    }
			    }
		    } else if(RoutingNumber.Length > 30) {
		        bolRetVal = false;
			    strFailureReason = "Routing Number is greater than the maximum characters allowed.";
		    } else {
		        bolRetVal = false;
			    strFailureReason = "Routing Number is a required field.";
		    }

            FailureReason = strFailureReason;
            
            return(bolRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Account"></param>
        /// <param name="FailureReason"></param>
        /// <returns></returns>
        public static bool ValidateAccount(string Account, out string FailureReason) {

            Regex objRegEx;
            bool bolRetVal;
            string strFailureReason;

		    // Validate Account Number
		    if(Account.Length == 0) {
		        bolRetVal = false;
			    strFailureReason = "Account Number is a required field.";
		    } else if(Account.Length > 30) {
		        bolRetVal = false;
			    strFailureReason = "Account Number is greater than the maximum characters allowed.";
		    } else {
			    // Account Numbers may contain numbers 0-9, a hyphen, and/or a space
			    // NOTE: Account Numbers MUST begin and end with a number
			    //       Hyphens and/or spaces must be preceeded and proceeded by a number e.g.(1-1)
			    //       n amount of hyphens and/or spaces are allowed e.g.(1-11 101-1)				
			    objRegEx = new Regex(@"^\d{1,}((-|\s)\d{1,}){0,}\d{0,}$", RegexOptions.IgnoreCase);
				
			    if(objRegEx.IsMatch(Account)) {
		            bolRetVal = true;
			        strFailureReason = string.Empty;
                } else {
		            bolRetVal = false;
			        strFailureReason = "Account Number contains invalid character(s).";
			    }
		    }

            FailureReason = strFailureReason;
            
            return(bolRetVal);
        }

        /// <summary>
        /// Validate routine to verify if(the routing number meets the Fed Check 
        /// Digit Routine requirements.
        /// </summary>
        /// <param name="vData">vData - String value of the Routing Number to be validated</param>
        /// <returns>True if successful
        ///			 False if not successful
        ///</returns>
        private static bool ValidateRTCheckDigit(string vData) {

            // Check RT checksum.
            bool bReturn = true;
            const int RT_LENGTH = 9;
            int iDash = -1;
            const string VALID_CHARS = "0123456789-";
            const string WEIGHT = "37137137";
            int sum = 0;
            int nCD;
            char sChar;

            if(vData == null || vData.Length != RT_LENGTH) {    // Check length
                bReturn = false;
            } else {
                for(int i=0;i<vData.Length;++i) {
                    if(VALID_CHARS.IndexOf(vData[i]) < 0) {
                        bReturn = false;
                        break;
                    }
                }

                iDash = vData.IndexOf('-');             // Check for proper dash and return 'true' if OK.
                if(bReturn  && iDash > -1) {            // Has a dash, make sure in right spot                  
                    if((iDash == 4) || (iDash == 5)) {  // Don't worry about any other errors since they should have been taken care of                                                                              
                        return(bReturn);                // Dash in 4 = American w/no check digit, Dash in 5 = Canadian
                    } else {
                        bReturn = false;                // There was an error!
                    }
                }

                if(bReturn) {
                    for(int i=0;i<8;++i) {
                        sChar = vData[i];
                        if(sChar < '0' && sChar > '9') {
                            bReturn = false;
                            break;
                        }

                        sum += (sChar - '0') * int.Parse(WEIGHT[i].ToString());
                        Console.WriteLine("char: " + sChar.ToString().PadLeft(5) + "     sum: " + sum.ToString().PadLeft(5));
                    }
                }

                if(bReturn == true) {
                    nCD = 10 - (sum % 10);

                    if(nCD == 10) {             // This shouldn't happen (mod should be 0)
                        nCD = 0;
                    }

                    if(vData[RT_LENGTH - 1] != nCD + '0') {
                        bReturn = false;
                    }
                }
            }

            return(bReturn);
        }
    }
}
