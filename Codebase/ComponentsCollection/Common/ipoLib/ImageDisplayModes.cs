﻿namespace WFS.RecHub.Common
{
    public class ImageDisplayModes
    {
        public ImageDisplayModes()
        {
        }
        public ImageDisplayModes(OLFImageDisplayMode checkImageDisplayMode, OLFImageDisplayMode documentImageDisplayMode)
        {
            CheckImageDisplayMode = checkImageDisplayMode;
            DocumentImageDisplayMode = documentImageDisplayMode;
        }

        public OLFImageDisplayMode CheckImageDisplayMode { get; }
        public OLFImageDisplayMode DocumentImageDisplayMode { get; }

        public OLFImageDisplayMode GetImageDisplayMode(ImageItemType itemType)
        {
            return itemType == ImageItemType.Check ? CheckImageDisplayMode : DocumentImageDisplayMode;
        }
    }
}