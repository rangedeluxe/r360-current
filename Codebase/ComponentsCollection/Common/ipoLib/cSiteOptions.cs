using System.Collections;
using System.Collections.Specialized;
using System.IO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     05/15/2003
*
* Purpose:  ipo Site Options class.
*
* Modification History
*   05/15/2003 JMC 
*       - Created class
*   11/20/2003 JMC 
*       - First major enhancement : 
*           1.)Added Strong-Naming to the assembly.
*           2.)Changed the namespace of the class to narrow the scope.
*           3.)Modified Comments to utilize C# Xml format.
*           4.)Added ipoTemplatesDir, ipoResourceDir, and loggingDepth properties.
*   06/01/2004 CR 7728 JMC 
*       - Added ImageSource Enumeration.
*       - Added ImageSource, ImageChunkSize, and IRISUrl properties to INI reader class.
*       - Added constructor used to pass in SiteCode which is translated to SiteKey
*         which is used to load values.
*   11/07/2006 CR 18946 JMC
*       - Removed ImageSource enum and ImageSource , IRISUrl , and imageChunkSize
*         properties.
*       - Added LogonMethodType enum.
*       - Added ValidatePassword property.
*       - Added LogonAPIXmlSavePath property.
*       - Added LogonMethod property.
*       - Added SessionExpiration property.
*       - Changed naming convention to no longer use hungarian notation
*         for internal property values.
*       - Removed commented code.
*   10/05/07 CR 22667 GRG
*       - Added ExclusionCharacters
*   10/22/2007 CR 21709 GRG
*       - Changed LogonMethodType enum. Changed MultiFactor value form 1 to 4.
*       - Changed method loadSiteOptions to be consistent with LogonMethodType enum.
*   09/15/2008 CR 25197 JMC
*       - Added SingleSignOn item to LogonMethodType enumeration.
*   09/24/2008CR 25639 JMC
*       - Removed 'ValidatePassword' option
*   09/25/2008 CR 25540 JMC
*       - Added Logon method types that weren't previously available.
*   10/28/2008 CR 25946 JMC
*       - Added TokenExpiration property in support of SessionToken functionality in 
*         ipoLogonAPI.SessionAPI
*   05/20/2009 CR 23584 MEH
*       - integraPAY Online Interface Into Hyland IMS Archive 
* CR 28522 JMC 01/06/2010
*     - Reworked the class to use cIPOServicesOptions to maintain all default 
*       Settings.
* CR 45994 JNE 9/15/2011
*      - Added CheckImageExists to ini, allows this check to be avoided. 
* CR 46314 WJS 9/19/2011
*      - Add support for templates directory for forgot password email
* CR 47771 WJS 10/14/2011
*	   - Add support for custom dll for image
* CR 48201 JCS 11/10/2011
*      - Added Decisioning API log path key DecisioningAPIXmlSavePath to INI.
* CR 45986 WJS 11/23/2011
*	   - Added custom image DLL support
* CR 52959 JNE 09/27/2012
*	- Added File Group support.
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
* WI 85982 CEJ 03/21/2013 
*  - BNYM Security Audit:  6.6.2 Online Admin: Sensitive Data Cached in the Browser
* WI 94256 TWE 03/27/2013
*    Create new connection string just for IntegraPay
* WI 103941 WJS 5/31/2013
 *   Create new connection string for CommonException
* WI 107883 EAS 07/03/2013
*	-Added Admin and Extract database connection string/types.
* WI 136855 RDS 04/15/2014
*	-Added Billing database connection string/types
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// ipo Server Options object class.
    /// </summary>
    public class cSiteOptions : cIPOServicesOptions {

        private string _ImagePath = "";
        private bool _CheckImageExists = true; // CR 45994 JNE 9/15/2011
        private string _OnlineImagePath = "";
        private string _CENDSPath = "";
        private string _SiteKey="";
        private int _TokenExpiration = 60;
        private bool _PreventWebCache = false;

        private string _LogonAPIXmlSavePath = "";
        private string _OnlineAPIXmlSavePath = string.Empty;
        private string _ResearchAPIXmlSavePath = string.Empty;
        private string _DecisioningAPIXmlSavePath = string.Empty;
        private LogonMethodType _LogonMethod = LogonMethodType.Standard;
        // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
        private ImageStorageMode _ImageStorageMode = ImageStorageMode.PICS;

        private int _CSRDepositStatus = 850;

        private string _ExclusionCharacters = "";

        private string _TemplateDirectory = string.Empty;

        private string _CustomImageDLLName = string.Empty;
        /// <summary>
        /// Returns SiteKey that has been loaded.
        /// </summary>

        private cIPOServicesOptions _DefaultServicesOptions = null;

        /// <summary></summary>
        public string siteKey {
            get {
                return(_SiteKey);
            }
        }

        /// <summary>
        /// Public constructor.  Requires siteKey to determine which ini Section 
        /// to read from.
        /// </summary>
        /// <param name="siteKey"></param>
        public cSiteOptions(string siteKey) : base(siteKey) {
            _SiteKey=siteKey;
            loadSiteOptions(this.siteKey);
        }

        private cIPOServicesOptions DefaultServicesOptions {
            get {
                const string INISECTION_IPO_SERVICES = "ipoServices";
                if(_DefaultServicesOptions == null) {
                    _DefaultServicesOptions = new cIPOServicesOptions(INISECTION_IPO_SERVICES);
                }
                return(_DefaultServicesOptions);
            }
        }

        /// <summary>
        /// Loads site options from the local .ini file using the given siteKey.
        /// </summary>
        /// <param name="siteKey"></param>
        protected override void loadSiteOptions(string siteKey) {

            StringCollection colSiteOptions = ipoINILib.GetINISection(siteKey);
            string strKey;
            string strValue;
            
            int intTemp;
 
            const string INIKEY_IMAGE_PATH = "ImagePath";
            const string INIKEY_CHECK_IMAGE_EXIST = "CheckImageExists"; // CR 45994 JNE 9/15/2011
            const string INIKEY_ONLINE_IMAGE_PATH = "OnlineImagePath";
            const string INIKEY_ONLINE_CENDS_PATH = "CENDSPath";
            // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
            const string INIKEY_IMAGE_STORAGE_TYPE = "ImageStorageType";
            const string INIKEY_PREVENT_WEB_CACHE = "PreventWebCache";

            const string INIKEY_CUSTOM_IMAGE_DLL = "CustomImageDLLPath";
            const string LOGON_XML_SAVE_PATH = "LogonAPIXmlSavePath";
            const string ONLINE_XML_SAVE_PATH = "OnlineAPIXmlSavePath";
            const string RESEARCH_XML_SAVE_PATH = "ResearchAPIXmlSavePath";
            const string DECISIONING_XML_SAVE_PATH = "DecisioningAPIXmlSavePath";
            const string LOGON_METHOD = "LogonMethod";
            const string EXCLUSION_CHARACTERS = "ExclusionCharacters";
            const string TOKEN_EXPIRATION = "TokenExpiration";
            const string CSR_DEPOSIT_STATUS = "CSRDepositStatus";
            const string TEMPLATES_DIRECTORY = "ipoTemplatesDir";
            

            base.loadSiteOptions(siteKey);

            System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {

                strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                if(strKey.ToLower() == INIKEY_IMAGE_PATH.ToLower()) {
                    _ImagePath = ipoLib.cleanPath(strValue);
                } else if (strKey.ToLower() == INIKEY_ONLINE_IMAGE_PATH.ToLower()) {
                    _OnlineImagePath = ipoLib.cleanPath(strValue);
                } else if (strKey.ToLower() ==  INIKEY_ONLINE_CENDS_PATH.ToLower()) {
                    _CENDSPath = ipoLib.cleanPath(strValue);
                } else if(strKey.ToLower() == LOGON_XML_SAVE_PATH.ToLower()) {
                    if(Directory.Exists(strValue)) {
                        _LogonAPIXmlSavePath = strValue;
                    } else {
                        try {
                            Directory.CreateDirectory(strValue);
                            _LogonAPIXmlSavePath = strValue;
                        } catch {
                            _LogonAPIXmlSavePath = string.Empty;
                        }
                    }
                } else if(strKey.ToLower() == ONLINE_XML_SAVE_PATH.ToLower()) {
                    if(Directory.Exists(strValue)) {
                        _OnlineAPIXmlSavePath = strValue;
                    } else {
                        try {
                            Directory.CreateDirectory(strValue);
                            _OnlineAPIXmlSavePath = strValue;
                        } catch{
                            _OnlineAPIXmlSavePath = string.Empty;
                        }
                    }
                } else if(strKey.ToLower() == RESEARCH_XML_SAVE_PATH.ToLower()) {
                    if(Directory.Exists(strValue)) {
                        _ResearchAPIXmlSavePath = strValue;
                    } else {
                        try {
                            Directory.CreateDirectory(strValue);
                            _ResearchAPIXmlSavePath = strValue;
                        } catch{
                            _ResearchAPIXmlSavePath = string.Empty;
                        }
                    }
                } else if (strKey.ToLower() == DECISIONING_XML_SAVE_PATH.ToLower()) {
                    if(Directory.Exists(strValue)) {
                        _DecisioningAPIXmlSavePath = strValue;
                    } else {
                        try {
                            Directory.CreateDirectory(strValue);
                            _DecisioningAPIXmlSavePath = strValue;
                        } catch{
                            _DecisioningAPIXmlSavePath = string.Empty;
                        }
                    }
                } else if(strKey.ToLower() == LOGON_METHOD.ToLower()) {
                    switch(strValue) {
                        case "1":
                            _LogonMethod = LogonMethodType.Corba;
                            break;
                        case "2":
                            _LogonMethod = LogonMethodType.CustomerExtID1Logon;
                            break;
                        case "3":
                            _LogonMethod = LogonMethodType.WebAccess;
                            break;
                        case "4":
                            _LogonMethod = LogonMethodType.MultiFactor;
                            break;
                        case "5":
                            _LogonMethod = LogonMethodType.SingleSignOn;
                            break;
                        default:
                            _LogonMethod = LogonMethodType.Standard;
                            break;
                    }
                } else if(strKey.ToLower() == EXCLUSION_CHARACTERS.ToLower()) {
                    _ExclusionCharacters = strValue;
                } else if(strKey.ToLower() == TOKEN_EXPIRATION.ToLower()) {
                    if(int.TryParse(strValue, out intTemp)) {
                        _TokenExpiration = intTemp;
                    }
                }
                // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                else if (strKey.ToLower() == INIKEY_IMAGE_STORAGE_TYPE.ToLower())
                {
                    switch(strValue) {
                        case "0":
                            _ImageStorageMode = ImageStorageMode.PICS;
                            break;
                        case "1":
                            _ImageStorageMode = ImageStorageMode.HYLAND;
                            break;
                        case "2":
                            _ImageStorageMode = ImageStorageMode.CUSTOM;
                            break;
                        case "3":
                            _ImageStorageMode = ImageStorageMode.FILEGROUP;
                            break;
                        default:
                            _ImageStorageMode = ImageStorageMode.PICS;
                            break;
                    }                    
                }
                // END MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                else if(strKey.ToLower() == CSR_DEPOSIT_STATUS.ToLower()) {
                    if(int.TryParse(strValue, out intTemp)) {
                        _CSRDepositStatus = intTemp;
                    }
                }
                //CR 45994 JNE 9/15/2011
                else if(strKey.ToLower() == INIKEY_CHECK_IMAGE_EXIST.ToLower()){
                    if (strValue.Trim().Equals("1") ){
                        _CheckImageExists = true;
                    }else{
                        _CheckImageExists = false;
                    }
                }
                else if(strKey.ToLower() == TEMPLATES_DIRECTORY.ToLower()) {
                    _TemplateDirectory = strValue;
                } 
                else if (strKey.ToLower() == INIKEY_CUSTOM_IMAGE_DLL.ToLower()) {
                    _CustomImageDLLName = strValue;
               }
                else if (strKey.ToLower() == INIKEY_PREVENT_WEB_CACHE.ToLower()) {
                    _PreventWebCache = strValue == "1";
                }
            }            
        }

        /// <summary></summary>
        public override string logFilePath {
            get {
                if(LogFilePathIsDefined) {
                    return(base.logFilePath);
                } else {
                    return(DefaultServicesOptions.logFilePath);
                }
            }
        }

        /// <summary></summary>
        public override int logFileMaxSize {
            get {
                if(LogFileMaxSizeIsDefined) {
                    return(base.logFileMaxSize);
                } else {
                    return(DefaultServicesOptions.logFileMaxSize);
                }
            }
        }

        /// <summary></summary>
        public override string connectionString {
            get {
                if(ConnectionStringIsDefined) {
                    return(base.connectionString);
                } else {
                    return(DefaultServicesOptions.connectionString);
                }
            }
        }

        /// <summary></summary>
        public override string connectionStringIP
        {
            get
            {
                if (ConnectionStringIPIsDefined)
                {
                    return (base.connectionStringIP);
                }
                else
                {
                    return (DefaultServicesOptions.connectionStringIP);
                }
            }
        }

        /// <summary></summary>
        public override string connectionStringExtracts
        {
            get
            {
                if (ConnectionStringExtractsIsDefined)
                {
                    return (base.connectionStringExtracts);
                }
                else
                {
                    return (DefaultServicesOptions.connectionStringExtracts);
                }
            }
        }

        /// <summary></summary>
        public override string connectionStringCommonException
        {
            get
            {
                if (ConnectionStringCommonExceptionIsDefined)
                {
                    return (base.connectionStringCommonException);
                }
                else
                {
                    return (DefaultServicesOptions.connectionStringCommonException);
                }
            }
        }

		/// <summary></summary>
		public override string connectionStringBilling
		{
			get
			{
				if (ConnectionStringBillingIsDefined)
				{
					return (base.connectionStringBilling);
				}
				else
				{
					return (DefaultServicesOptions.connectionStringBilling);
				}
			}
		}

        /// <summary>
        /// Disable or enable the integraPay database
        /// </summary>
        public override bool integraPayIsEnabled
        {
            get
            {
                return base._IntegraPayIsEnabled;
            }
        }

        /// <summary></summary>
        public override int loggingDepth {
            get {
                if(LoggingDepthIsDefined) {
                    return(base.loggingDepth);
                } else {
                    return(DefaultServicesOptions.loggingDepth);
                }
            }
        }

        /// <summary></summary>
        public override bool SetDeadlockPriority {
            get {
                if(SetDeadlockPriorityIsDefined) {
                    return(base.SetDeadlockPriority);
                } else {
                    return(DefaultServicesOptions.SetDeadlockPriority);
                }
            }
        }

        /// <summary></summary>
        public override byte QueryRetryAttempts {
            get {
                if(QueryRetryAttemptsIsDefined) {
                    return(base.QueryRetryAttempts);
                } else {
                    return(DefaultServicesOptions.QueryRetryAttempts);
                }
            }
        }

        // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
        /// <summary>
        /// Returns how images are stored based on the local INI 
        /// file.
        /// </summary>
        public ImageStorageMode imageStorageMode
        {
            get
            {
                return (_ImageStorageMode);
            }
        }
        // END MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 

        /// <summary>
        /// Returns path where CheckBOX images are stored based on the local INI 
        /// file.
        /// </summary>
        public string imagePath {
            get {
                return (_ImagePath);
            }
        }            

        /// <summary>
        /// Returns path Location to write IPO Image Service requests
        /// file.
        /// </summary>
        public string OnlineImagePath {
            get {
                return (_OnlineImagePath);
            }
        }

        /// <summary>
        /// Returns path Location of CENDS notification files.
        /// </summary>
        public string CENDSPath {
            get {
                return (_CENDSPath);
            }
        }    

        /// <summary></summary>
        public string LogonAPIXmlSavePath {
            get {
                return(_LogonAPIXmlSavePath);
            }
        }

        /// <summary></summary>
        public string OnlineAPIXmlSavePath {
            get {
                return(_OnlineAPIXmlSavePath);
            }
        }

        /// <summary></summary>
        public string ResearchAPIXmlSavePath {
            get {
                return(_ResearchAPIXmlSavePath);
            }
        }

        /// <summary></summary>
        public string DecisioiningAPIXmlSavePath {
            get {
                return (_DecisioningAPIXmlSavePath);
            }
        }

        /// <summary></summary>
        public LogonMethodType LogonMethod {
            get {
                return(_LogonMethod);
            }
        }

        /// <summary></summary>
        public string ExclusionCharacters {
            get {
                return (_ExclusionCharacters);
            }
        }

        /// <summary></summary>
        public int TokenExpiration {
            get {
                return(_TokenExpiration);
            }
        }

        /// <summary></summary>
        public int CSRDepositStatus {
            get {
                return(_CSRDepositStatus);
            }
        }

        /// <summary></summary>
        public bool CheckImageExists {
            get {
                return(_CheckImageExists);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string  TemplateDirectory{
            get {
                return (_TemplateDirectory);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CustomImageDLLPath{
            get{
                return (_CustomImageDLLName);
            }
        }

        /// <summary>
        /// If this value is true '1' meta tags are added to all the pages 
        ///    in order to instruct the browser not to cache the pages
        /// </summary>
        public bool PreventWebCache {
            get { 
                return _PreventWebCache;
            }
        }
    }
}
