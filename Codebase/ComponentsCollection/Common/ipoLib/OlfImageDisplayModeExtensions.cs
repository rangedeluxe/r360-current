﻿using System.Collections.Generic;

namespace WFS.RecHub.Common
{
    public static class OlfImageDisplayModeExtensions
    {
        public static IEnumerable<int> GetPageNumbers(this OLFImageDisplayMode displayMode)
        {
            return displayMode == OLFImageDisplayMode.BothSides ? new[] {0, 1} : new[] {0};
        }
    }
}