using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System.Text;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     05/15/2003
*
* Purpose:  IntegraPAY Online ini Library.
*
* Modification History
* JMC 05/15/2003
*   - Created class
* JMC 11/20/2003
*   -First major enhancement : 
*    1.)Added Strong-Naming to the assembly.
*    2.)Changed the namespace of the class to narrow the scope.
*    3.)Modified Comments to utilize C# Xml format.
* CR  7728 JMC 06/01/2004 
*   -Modified local ini path to ensure that the ini to be read exists in the local
*    executing folder.  If the .ini does not exist, it will attempt to read an .ini
*    specified in the local appSettings variable stored in the app.config or
*    web.config configuration files.
* CR 54211 JMC 08/15/2012
*   -Changed precidence of localIni vs. the default IPOnline.ini file.  Now the class.
*    will look to see if localIni is defined, and use that before defaulting to IPOnline.ini
* CR 54211 WJS 8/15/2012
*   - Change path to IniFileLocation as public to expose it
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// ipo ini library.
    /// </summary>
    public class ipoINILib {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section
                                                           , string key
                                                           , string val
                                                           , string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="def"></param>
        /// <param name="retVal"></param>
        /// <param name="size"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section
                                                        , string key
                                                        , string def
                                                        , StringBuilder retVal
                                                        , int size
                                                        , string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lpAppName"></param>
        /// <param name="lpReturnedString"></param>
        /// <param name="nSize"></param>
        /// <param name="lpFileName"></param>
        /// <returns></returns>
        [ DllImport("kernel32")]
        protected internal static extern int GetPrivateProfileSection(string lpAppName
                                                                    , byte[] lpReturnedString
                                                                    , int nSize
                                                                    , string lpFileName);

 
        /// <summary>
        /// Returns the path of the INI file that the executing assembly will use.
        /// </summary>
        public static string IniFileLocation
        {
            get{ 
                string strFileName;

                if(System.Configuration.ConfigurationManager.AppSettings["localIni"] != null && System.Configuration.ConfigurationManager.AppSettings["localIni"].Length > 0) {
                    strFileName=System.Configuration.ConfigurationManager.AppSettings["localIni"];
                } else {
                    strFileName=System.IO.Path.Combine(ipoLib.AppPath, ipoLib.INIFILE_NAME);
                }

                return(strFileName);
            }
        }

        /// <summary>
        /// Writes Data to an INI File.
        /// </summary>
        /// <param name="Section">Section to write value to.</param>
        /// <param name="Key">Key to be written to.</param>
        /// <param name="Value">Value to be written.</param>
        public static void IniWriteValue(string Section
                                       , string Key
                                       , string Value)
        {
            WritePrivateProfileString(Section
                                    , Key
                                    , Value
                                    , IniFileLocation);
        }
        
        /// <summary>
        /// Reads Data Value From an Ini File.
        /// </summary>
        /// <param name="Section">Section where key is located.</param>
        /// <param name="Key">Key to be found.</param>
        /// <returns>Value underneath specified key and section.</returns>
        public static string IniReadValue(string Section
                                        , string Key)
        {
            StringBuilder temp = new StringBuilder(255);

            int i = GetPrivateProfileString(Section
                                          , Key
                                          , ""
                                          , temp
                                          , 255
                                          , IniFileLocation);
            return temp.ToString();

        }

        /// <summary>
        /// Retrieves a collection of all values from the given .ini section.
        /// </summary>
        /// <param name="section">section to be returned.</param>
        /// <returns>Collection of section values.</returns>
        public static StringCollection GetINISection(string section) // kcf
        {
            StringCollection items = new StringCollection();
            byte[] buffer = new byte[32768];
            int bufLen=0;
            bufLen = GetPrivateProfileSection(section, buffer
                                            , buffer.GetUpperBound(0)
                                            , IniFileLocation); // kcf

            if (bufLen > 0) {
                StringBuilder sb = new StringBuilder();
                for(int i=0; i < bufLen; i++) {               

                    if (buffer[i] != 0) {
                        sb.Append((char) buffer[i]);
                    }
                    else {
                        if (sb.Length > 0) {
                            items.Add(sb.ToString());
                            sb = new StringBuilder();
                        }
                    }
                }
            }
            return items;
        }
    }
}
