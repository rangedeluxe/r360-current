using System;
using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     07/24/2008
*
* Purpose:  IPOnline pagination class
*
* Modification History
*   07/24/2008 JMC
*       - Initial version.
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// 
    /// </summary>
    public static class Pagination {

        /// <summary>
        /// Calculate the Next Starting Record Number in a Paginated Recordset
        /// </summary>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <param name="RecordCount"></param>
        /// <returns></returns>
        public static int GetNextStartRecord(int StartRecord, int MaxRecords, int RecordCount) {

            int intRetVal = 0;

            if(MaxRecords + StartRecord <= RecordCount) {
                intRetVal = MaxRecords + StartRecord;
            } else {
                intRetVal = StartRecord;
            }
            
            return(intRetVal);
        }


        /// <summary>
        /// Calculate the Previous Starting Record Number in a Paginated Recordset
        /// </summary>
        /// <param name="StartRecord"></param>
        /// <param name="MaxRecords"></param>
        /// <returns></returns>
        public static int GetPreviousStartRecord(int StartRecord, int MaxRecords) {

            int intRetVal = 0;

            if(StartRecord - MaxRecords <= 0) {
                intRetVal = 1;
            } else {
                intRetVal = StartRecord - MaxRecords;
            }
            
            return(intRetVal);
        }

        /// <summary>
        /// Calculate the Last Starting Record Number in a Paginated Recordset
        /// </summary>
        /// <param name="MaxRecords"></param>
        /// <param name="RecordCount"></param>
        /// <returns></returns>
        public static int GetLastStartRecord(int MaxRecords, int RecordCount) {

            int intRetVal = 0;

            if(RecordCount == 0) {
                intRetVal = 1;
            } else if((RecordCount / MaxRecords) * MaxRecords + 1 <= RecordCount) {
                intRetVal = (RecordCount / MaxRecords) * MaxRecords + 1;
            } else {
                intRetVal = RecordCount - (MaxRecords - 1);
            }
            
            return(intRetVal);
        }

        /// <summary>
        /// Set bookmark filter on input recordset.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="StartRec"></param>
        /// <param name="MaxRecords"></param>
        public static void PaginateRS(DataTable dt, int StartRec, int MaxRecords) {
            
            int intIndexCorrectedStartRec;
            
            try {

                if(dt == null || dt.Rows.Count == 0) {
                    return;
                }
                
                if(dt.Rows.Count <= MaxRecords) {
                    return;
                }
                
                // Reposition to starting record
                if(StartRec <= 0) {
                     StartRec = 1;
                }

                intIndexCorrectedStartRec = StartRec-1;
        
                for(int i=dt.Rows.Count -1;i>=0;--i) {
                    if((i>=intIndexCorrectedStartRec + MaxRecords) || (i<intIndexCorrectedStartRec)) {
                        dt.Rows.Remove(dt.Rows[i]);
                    }
                }
                
            } catch(Exception) {
                //????
            }
        }
    }
}
