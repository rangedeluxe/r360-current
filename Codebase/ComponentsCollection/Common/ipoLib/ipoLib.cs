using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     05/15/2003
*
* Purpose:  IntegraPAY Online Common-Function Library.
*
* Modification History
*   05/15/2003 JMC   
*       - Created class
*   11/20/2003 JMC
*       - First major enhancement : 
*            1.)Added Strong-Naming to the assembly.
*            2.)Changed the namespace of the class to narrow the scope.
*            3.)Modified Comments to utilize C# Xml format.
*            4.)Added error-handling to cleanGUID function call.
*            5.)Created getXmlResource, getTextResource, getBitmapResource
*               , and getStreamResource function calls.
*   03/03/2004 CR 6661 JMC
*       - Added overload for NVL function to handle byte datatypes.
*   06/01/2004 CR 7728 JMC
*       - Added new NVL function that will return a long.
*       - Added additional comments to function headers.
*   03/03/2005 CR 10537 JMC
*       - Added new overloaded NVL function for type Guid.
*   05/09/2005 CR 12501 JMC
*       - Added NVL DateTime overload function.
*   07/28/2005 CR 12257 JMC
*       - Added OLDecisioning Constants.  Added NVL overloads, IsNumeric, and
*         IsDate methods.
*   11/07/2006 CR 18946 JMC
*       - Added IsGuid() and IIf() methods.
* CR 22115 JMC 01/26/2010
*    -Modified the NVL (boolean) function to accept values stored as bytes.  Function will interpret values > 0 to be TRUE
* CR 28875 JCS 06/09/2010
*    -Added functions getBase64String and tryBase64Decode to support Base64 encoding passwords.
* CR 32263 WJS 08/14/2011
*    -Added support for Base64Decode
* CR 47058 WJS 10/6/2011
*   - Update to centralize point for XML Resource
* WI 89968 CRG 03/01/2013
*    Modify ipoLIB for .NET 4.5
* WI 90248 CRG 03/01/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLib
* WI 143419 DJW 6/3/2014 
*    Changed BatchID to SourceBatchID
* WI 156534 SAS 7/31/2014 
*    Changes done to int NVL return statement
******************************************************************************/
namespace WFS.RecHub.Common {

    /// <summary>
    /// ipo Common function library.
    /// </summary>
    public class ipoLib
    {
        /// <summary></summary>
        public const string CRLF = "\r\n";

        /// <summary></summary>
        public const string INIFILE_NAME = "IPOnline.ini";
           /// <summary></summary>
        public const string INISECTION_IMAGE_SERVICE = "ipoImageSvc";
        /// <summary></summary>
        public const string INISECTION_DECISIONING_API = "ipoDecisioningAPI";
        /// <summary></summary>
        public const string INISECTION_OLFIMAGE_IMPORT_SERVICE = "OLFImageImportSvc";
        /// <summary></summary>
        public const string INISECTION_HYLAND = "Hyland";
        /// <summary></summary>
        public const string INISECTION_OLFIMAGE_ICON_SERVICE = "ICONSvc";
        /// <summary></summary>
        public const string INISECTION_OLF_ICON_WEBSERVICE = "ipoICONWebServiceAPI";

        /// <summary>
        /// Returns the path of the executing assembly including the 
        /// executable name.
        /// </summary>
        public static string AppExePath
        {
            get  
            { 
                return System.Reflection.Assembly.GetCallingAssembly().Location; 
            }
        }


        /// <summary>
        /// Returns the executing assembly's executable name without the path.
        /// </summary>
        public static string AppExeName
        { 
            get 
            { 
                int intPos=AppExePath.LastIndexOf("\\");
                return(AppExePath.Substring(intPos+1
                                          , AppExePath.Length-intPos-1));
            }
        }

 
        /// <summary>
        /// Returns the application's path without the executable name.
        /// </summary>
        public static string AppPath
        { 
            get 
            { 
                return AppExePath.Substring(0
                                          , AppExePath.Length - AppExeName.Length); 
            }
        }


        /// <summary>
        /// Accepts a single character and returns a string with that  character 
        /// rapeated n number of times.
        /// </summary>
        /// <param name="charValue"></param>
        /// <param name="timesRepeated"></param>
        /// <returns></returns>
        public static string repeatChar(char charValue
                                      , long timesRepeated)
        {
            string strRetVal="";

            for(long i=0;i<timesRepeated;++i)
                { strRetVal+=charValue; }

            return strRetVal;
        }


        /// <summary>
        /// Accepts the full path to a readable (presumably text) file.  returns 
        /// the number of lines contained in that file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static long countFileLines(string filePath)
        {
            string strTemp = "";
            long lngLineCount=0;

            System.IO.TextReader tr = System.IO.File.OpenText(filePath);

            while((strTemp = tr.ReadLine()) != null)   //(tr.Peek())
            {
                ++lngLineCount;
            } 

            tr.Close();
            tr=null;

            return lngLineCount;
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static long NVL(ref System.Data.SqlClient.SqlDataReader dr
                               , string fieldName
                               , long alternateVal)
        {
            if (!(dr.IsDBNull(dr.GetOrdinal(fieldName))))
                { return dr.GetInt64(dr.GetOrdinal(fieldName)); }
            else
                { return alternateVal; }
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static string NVL(ref System.Data.SqlClient.SqlDataReader dr
                               , string fieldName
                               , string alternateVal)
        {
            if (!(dr.IsDBNull(dr.GetOrdinal(fieldName))))
                { return dr.GetString(dr.GetOrdinal(fieldName)); }
            else
                { return alternateVal; }
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static int NVL(ref System.Data.SqlClient.SqlDataReader dr
                            , string fieldName
                            , int alternateVal)
        {
            if (!(dr.IsDBNull(dr.GetOrdinal(fieldName))))
                { return dr.GetInt32(dr.GetOrdinal(fieldName)); }
            else
                { return alternateVal; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static short NVL(ref System.Data.DataRow dr
                            , string fieldName
                            , short alternateVal)
        {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (short)dr[fieldName]; }
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static string NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , string alternateVal) {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (string) dr[fieldName]; }
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static Decimal NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , Decimal alternateVal) {

            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (Decimal) dr[fieldName]; }
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static Byte NVL(ref System.Data.DataRow dr
                             , string fieldName
                             , Byte alternateVal) {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (Byte) dr[fieldName]; }
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static bool NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , bool alternateVal) {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else {
                if(dr.Table.Columns[fieldName].DataType.Name.EndsWith("Byte")) {
                    return (byte)dr[fieldName] > 0;
                } else {
                    return (bool)dr[fieldName];
                }
            }
        }



        /// <summary>
        /// Inspects the value, and returns an alternate value if the original value 
        /// is evaluated as NULL.
        /// WI 156534 SAS 7/31/2014: Changes done to int NVL return statement
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static int NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , int alternateVal) {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return Convert.ToInt32(dr[fieldName]); }
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original value 
        /// is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static Guid NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , Guid alternateVal) {
            if (dr.IsNull(fieldName))
                { return(alternateVal); }
            else
                { return new Guid(dr[fieldName].ToString()); }
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original value 
        /// is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static DateTime NVL(ref System.Data.DataRow dr
                                 , string fieldName
                                 , DateTime alternateVal) {
            if (dr.IsNull(fieldName))
                { return(alternateVal); }
            else
                { return System.Convert.ToDateTime(dr[fieldName]); }
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original value 
        /// is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static double NVL(ref System.Data.DataRow dr
                                 , string fieldName
                                 , double alternateVal) {
            if (dr.IsNull(fieldName))
                { return(alternateVal); }
            else
                { return System.Convert.ToDouble(dr[fieldName]); }
        }

        public static long NVL(ref System.Data.DataRow dr
                                 , string fieldName
                                 , long alternateVal)
        {
            if (dr.IsNull(fieldName))
            { return (alternateVal); }
            else
            { return System.Convert.ToInt64(dr[fieldName]); }
        }

        /// <summary>
        /// Ensures that a UNC path is closed with a backslash "\".
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string cleanPath(string path) {
            string strRetVal = path;

            if (path.Length > 0)
            {
                if (path.Substring(path.Length-1,1) != "\\") 
                    { strRetVal += "\\"; }
            }
            else
                { strRetVal="\\"; }

            return strRetVal;
        }


        /// <summary>
        /// Ensures that a GUID variable is opened and closed by curly braces. {}
        /// </summary>
        /// <param name="GUID"></param>
        /// <param name="keepCurlyBraces"></param>
        /// <returns></returns>
        public static string cleanGUID(string GUID
                                     , bool keepCurlyBraces) {
            string strRetVal = GUID;

            try
            {
                if (keepCurlyBraces)
                {
                    if (GUID.Length>2)
                    {
                        if (GUID.Substring(0,1) != "{")
                            { strRetVal="{"+strRetVal; }

                        if (GUID.Substring(GUID.Length-1,1)!="}")
                            { strRetVal+="}"; }
                    }
                }
                else
                {
                    if (GUID.Length > 1)
                    {
                        if (strRetVal.Substring(0,1) == "{")
                            { strRetVal=strRetVal.Substring(1,strRetVal.Length-2); }
        
                        if (strRetVal.Length > 1)
                        {
                            if (strRetVal.Substring(strRetVal.Length-1,1) == "}")
                                { strRetVal=strRetVal.Substring(0,strRetVal.Length-2); }
                        }
                    }
                }
            }
            catch
            {
                strRetVal = "";
            }

            return strRetVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static long LOF(string filePath) {
            long lngRetVal = 0;
            FileStream fs = null;

            if (getFile(filePath, out fs))
            {
                lngRetVal = fs.Length;
                fs.Close();
                fs=null;
            }

            return lngRetVal;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fs"></param>
        /// <returns></returns>
        public static bool getFile(string filePath
                                 , out System.IO.FileStream fs) {   
            fs=null;
            bool bolRetVal=false;
            const int MAX_TRIES = 10;

            for(int i=0;i<MAX_TRIES;++i)   //((tw==null) && (intTries < 10))
            {
                try
                {
                    if (System.IO.File.Exists(filePath))
                    {
                        fs = new FileStream(filePath
                                          , FileMode.Open
                                          , FileAccess.Read
                                          , FileShare.Read
                                          , 16384 );
                        bolRetVal=true;
                    }                                       
                    break;
                }
                catch
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }

            return bolRetVal;
        }

        /// <summary>
        /// Retrieves an Xml document from an assembly.
        /// </summary>
        /// <param name="identifier">Key required to locate the resource within
        /// the assembly.</param>
        /// <param name="ass">Assembly from which the resource is embedded.
        /// </param>
        /// <returns>Embedded Xml document object.</returns>
        public static XmlDocument getXmlResource(string identifier
                                               , System.Reflection.Assembly ass) {
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

            // use the strIdentifier argument to retrieve the 
            // appropriate resource from the assembly
            System.IO.Stream s = ass.GetManifestResourceStream(identifier);
            StreamReader sr = new StreamReader(s);

            // load the document from the returned stream    
            xmlDoc.Load(sr.BaseStream);
            sr.Close();

            // return the document
            return(xmlDoc);
        }

       
        /// <summary>
        /// Retrieves an Xml document from an assembly.
        /// </summary>
        /// <param name="directoryName"></param>
        /// <param name="xsl_Embed_Path"></param>
        /// <param name="XslResourceFileName"></param>
        /// <param name="ass"></param>
        /// <returns></returns>
        public static XmlDocument getXmlResource(string directoryName, string xsl_Embed_Path, 
                        string XslResourceFileName, System.Reflection.Assembly ass )
        {
            XmlDocument docXml;
            docXml = new XmlDocument();
            try
            {
                if (File.Exists(ipoLib.cleanPath(directoryName) + XslResourceFileName))
                {
                    docXml = new XmlDocument();
                    docXml.Load(ipoLib.cleanPath(directoryName) + XslResourceFileName);
                }
                else
                {
                    docXml = ipoLib.getXmlResource(xsl_Embed_Path + "." + XslResourceFileName
                                                , ass);
                }
            }
            catch
            {
                docXml = ipoLib.getXmlResource(xsl_Embed_Path + "." + XslResourceFileName
                                            , ass);
            }
            return docXml;
        }
        /// <summary>
        /// Retrieves a binary stream from an assembly.
        /// </summary>
        /// <param name="identifier">Key required to locate the resource within
        /// the assembly.</param>
        /// <param name="ass">Assembly from which the resource is embedded.
        /// </param>
        /// <returns>Embedded stream from an assembly.</returns>
        public static StreamReader getTextResource(string identifier
                                                 , System.Reflection.Assembly ass) {
            // use the strIdentifier argument to retrieve the 
            // appropriate resource from the assembly
            StreamReader sr = new StreamReader(ass.GetManifestResourceStream(identifier));

            // return the document
            return(sr);
        }

        /// <summary>
        /// Retrieves a binary stream from an assembly.
        /// </summary>
        /// <param name="identifier">Key required to locate the resource within
        /// the assembly.</param>
        /// <param name="ass">Assembly from which the resource is embedded.
        /// </param>
        /// <returns>Embedded bitmap from an assembly.</returns>
        public static Bitmap getBitmapResource(string identifier
                                             , System.Reflection.Assembly ass) {
            return((Bitmap)Image.FromStream(ass.GetManifestResourceStream(identifier)));
        }

        /// <summary>
        /// Retrieves a binary stream from an assembly.
        /// </summary>
        /// <param name="identifier">Key required to locate the resource within
        /// the assembly.</param>
        /// <param name="ass">Assembly from which the resource is embedded.
        /// </param>
        /// <returns>Binary stream containaing the embedded resource.</returns>
        public static System.IO.Stream getStreamResource(string identifier
                                                       , System.Reflection.Assembly ass) {
            return(ass.GetManifestResourceStream(identifier));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool IsDate(object dt)
        {
            try {
                System.DateTime.Parse(dt.ToString());
                return true;
            }
            catch {
                return false;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testVal"></param>
        /// <returns></returns>
        public static bool IsGuid(string testVal) {
            try {
                Guid gidTemp = new Guid(testVal);
                return(true);
            } catch(Exception) {
                return(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool IsNumeric(object dt)
        {
            try {
                double.Parse(dt.ToString());
                return true;
            }
            catch {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exp"></param>
        /// <param name="truepart"></param>
        /// <param name="falsepart"></param>
        /// <returns></returns>
        public static string IIf(bool exp, string truepart, string falsepart) {
            if(exp) {
                return(truepart);
            } else {
                return(falsepart);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryParse(string s, out Guid result) {

            Guid gidRetVal;
            bool bolRetVal;

            if(s == null) {
                gidRetVal = Guid.Empty;
                bolRetVal = false;
            } else {
                try {
                    gidRetVal = new Guid(s);
                    bolRetVal = true;
                } catch(Exception) {
                    gidRetVal = Guid.Empty;
                    bolRetVal = false;
                }
            }

            result = gidRetVal;
            
            return(bolRetVal);
        }

        /// <summary>
        /// CR 12576 EJG 01/09/2006 - Created function to determine if value is a valid currency value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
	    public static bool IsValidCurrency(string value) {

            Regex objExp = new Regex(@"^\$*\s?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$",
                                     RegexOptions.IgnoreCase);
		    //objExp.Global = False

		    return(objExp.IsMatch(value));
	    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Length"></param>
        /// <returns></returns>
        public static string Right(string Value, int Length) {

            string strRetVal = string.Empty;

            if(Value != null) {
                if(Value.Length >= Length) {
                    strRetVal = Value.Substring(Value.Length - Length);
                }
            }
            
            return(strRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="Sort"></param>
        public static DataTable SortDataTable(DataTable dt, string Sort) {

            DataTable newDT = dt.Clone();
            int rowCount = dt.Rows.Count;

            if(Sort.Length > 0) {

                DataRow[] foundRows = dt.Select(null, Sort); // Sort with Column name

                for (int i=0;i<rowCount;i++) {
                    object[] arr = new object[dt.Columns.Count];

                    for (int j=0;j<dt.Columns.Count;j++) {
                        arr[j]=foundRows[i][j];
                    }

                    DataRow data_row = newDT.NewRow();
                    data_row.ItemArray=arr;
                    newDT.Rows.Add(data_row);
                }

                //clear the incoming dt
                dt.Rows.Clear();

                for(int i = 0; i < newDT.Rows.Count; i++) {
                    object[] arr = new object[dt.Columns.Count];
                    for (int j = 0; j < dt.Columns.Count; j++) {
                        arr[j]=newDT.Rows[i][j];
                    }

                    DataRow data_row = dt.NewRow();
                    data_row.ItemArray = arr;
                    dt.Rows.Add(data_row);
                }
            }

            return(dt);
        }    

        /// <summary>
        /// Checks if a string is Base64 encoded by attempting to decode
        /// </summary>
        /// <param name="EncodedText"></param>
        /// <returns>True if text could be Base64 decoded; False otherwise</returns>
        public static bool TryBase64Decode(string EncodedText) {
            try {
                // If no exception, then it is a base64 string
                using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(EncodedText))) {
                    return (true);
                }
            } catch {
                // If exception, then assume it is a normal string
                return (false);
            }
        }

         /// <summary>
        /// Checks if a string is Base64 encoded by attempting to decode and return decode string
         /// </summary>
         /// <param name="EncodedText"></param>
         /// <param name="decodedText"></param>
         /// <returns></returns>
        public static bool TryBase64Decode(string EncodedText, out string decodedText) {
            bool rtnval = false;
            decodedText = string.Empty;
            try {
                // If no exception, then it is a base64 string
                using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(EncodedText))) {
                    decodedText = System.Text.ASCIIEncoding.ASCII.GetString(stream.ToArray());
                    rtnval = true;
                }
            } catch {
                // If exception, then assume it is a normal string
                decodedText = string.Empty;

            }
            return rtnval;
        }

       

        /// <summary>
        /// Base64 encode a string and return the result
        /// </summary>
        /// <param name="PlainText"></param>
        /// <param name="EncodingType"></param>
        /// <returns>Input text Base64 encoded</returns>
        public static string getBase64String(string PlainText, System.Text.Encoding EncodingType) {
            string EncodedText;

            try {
                // If no exception, then return encoded string
                EncodedText = Convert.ToBase64String(EncodingType.GetBytes(PlainText));
            }
            catch {
                // If exception, then encoding failed
                EncodedText = String.Empty;
            }
            return (EncodedText);
        }

        /// <summary>
        /// Base64 encode a string and return the result
        /// </summary>
        /// <param name="PlainText"></param>
        /// <returns>Input text Base64 encoded</returns>
        public static string getBase64String(string PlainText) {
            string EncodedText;

            try {
                // If no exception, then return encoded string
                EncodedText = getBase64String(PlainText, System.Text.Encoding.UTF8);
            }
            catch {
                // If exception, then encoding failed
                EncodedText = String.Empty;
            }
            return (EncodedText);
        }
    }
}
