﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.Common.ClientMapper
{
    public enum LogType
    {
        Error = 0,
        Information = 1,
        Warning = 2
    }

    public class LogEntry
    {
        public LogEntry(LogType type, string message, string source)
        {
            Type = type;
            Message = message;
            Source = source;
        }

        public LogType Type { get; }
        public string Message { get; }
        public string Source { get; }
    }
}
