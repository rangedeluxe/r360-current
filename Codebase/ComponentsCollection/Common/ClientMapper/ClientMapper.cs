﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WFS.RecHub.Common.ClientMapper
{
    public class ClientMapper
    {
        private readonly int _defaultBankId;
        private readonly List<ClientMapping> _clientMaps;

        public ClientMapper(int defaultBankId, IEnumerable<ClientMapping> clientMaps = null)
        {
            _defaultBankId = defaultBankId;
            _clientMaps = clientMaps?.ToList() ?? null;
        }

        public int GetBankIdFromClientId(int clientId)
        {
            return _clientMaps?.FirstOrDefault(c => c.ClientId == clientId)?.BankId ?? _defaultBankId;
        }
    }
}
