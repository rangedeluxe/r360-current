﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WFS.RecHub.Common.ClientMapper
{
    public class ClientMapperLoader
    {
        private readonly List<LogEntry> _logEntries = new List<LogEntry>();

        public IReadOnlyList<LogEntry> LogEntries => _logEntries;

        private ClientMapping ParseMapping(string line)
        {
            int bankId;
            int clientId;

            if (line.ToUpperInvariant().StartsWith("CLIENTID"))
                return null;

            string[] columns = line.Split(',');

            if (columns.Length != 2)
            {
                throw new Exception($"Line is malformed, must be ClientID, BankID. Line: {line}.");
            }
            if (!int.TryParse(columns[1], out bankId))
            {
                throw new Exception($"Could not convert {columns[1]} to a BankID in line: {line}.");
            }
            if (!int.TryParse(columns[0], out clientId))
            {
                throw new Exception($"Could not convert {columns[0]} to a ClientID in line: {line}.");
            }
            return
                new ClientMapping
                {
                    ClientId = clientId,
                    BankId = bankId
                };
        }

        public IEnumerable<ClientMapping> LoadFromFile(string fileName)
        {
            IEnumerable<ClientMapping> clientMapper = Enumerable.Empty<ClientMapping>();

            try
            {
                using (StreamReader reader = new StreamReader(fileName))
                    clientMapper = LoadFromReader(reader);
                _logEntries.Add(new LogEntry
                (
                    LogType.Information,
                    $"Successfully loaded mapping file {fileName}.",
                    this.GetType().Name
                ));

            }
            catch (Exception ex)
            {
                if (ex is FileNotFoundException || ex is DirectoryNotFoundException)
                    _logEntries.Add(new LogEntry
                    (
                        LogType.Warning,
                        $"Could not load {fileName}. Using default bankId.",
                        this.GetType().Name
                    ));
                else
                    _logEntries.Add(new LogEntry
                    (
                        LogType.Error,
                        $"Could not load {fileName}. {ex}.",
                        this.GetType().Name
                    ));
            }
            return clientMapper;
        }

        public IEnumerable<ClientMapping> LoadFromReader(TextReader reader)
        {
            IEnumerable<ClientMapping> clientMapper = Enumerable.Empty<ClientMapping>();
            var clientMapperList = new List<ClientMapping>();

            try
            {
                using (reader)
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {

                        try
                        {
                            var clientMap = ParseMapping(line);
                            if (clientMap != null)
                                clientMapperList.Add(clientMap);
                        }
                        catch (Exception ex)
                        {
                            _logEntries.Add(new LogEntry
                            (
                                LogType.Warning,
                                ex.Message,
                                this.GetType().Name
                            ));

                        }
                    }
                }
                clientMapper = clientMapperList;
            }
            catch (Exception ex)
            {
                _logEntries.Add(new LogEntry
                (
                    LogType.Error,
                    ex.Message,
                    this.GetType().Name
                ));
            }
            return clientMapper;
        }
    }
}
