using System;
using System.IO;
using System.Text.RegularExpressions;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     5/15/2003
*
* Purpose:  Online Logging Component
*
* Modification History
* 05/15/2003 Joel Caples
*     - Initial release.
* 11/20/2003 Joel Caples
*     - First major enhancement : 
*          1.)Added Strong-Naming to the assembly.
*          2.)Changed the namespace of the class to narrow the scope.
*          3.)Modified Comments to utilize C# Xml format.
*          4.)Eliminated the "EventType" enum in favor of "MessageType".
*          5.)Created "MessageImportance" enum to determine based on logging
*             level whether a given message should be logged.
*          6.)Created the "isImportant" function to determine based on importance
*             and logging depth whether a given message should be logged.
* 10/06/2004 CR 10127 JMC
*     - Fixed compiler warning.
* 08/29/2011 CR 34196 WJS 
*     - Ability to control log file
* 11/16/2011 CR 69538  WJS
*    - Fix Memory leaks
* WI 88724 CRG 02/21/2013 
*    - Overload the LogError function for two Parameters (Exception and Class name)
* WI 90250 CRG 03/06/2013 
*    Change the NameSpace, Framework, Using's and Flower Boxes for ipoLog
* WI 89966 CRG 03/06/2013 
*    Modify ipoLog for .NET 4.5 and NameSpace
* WI 104003 JMC 05/31/2013
*	-Updated namespace to WFS.RecHub.Common.Log
******************************************************************************/
namespace WFS.RecHub.Common.Log {

    /// <summary>
    /// ipo Logging component.
    /// </summary>
    internal class cEventLogImpl : WFS.RecHub.Common.Log.IEventLog {

        /// <summary></summary>
        private const string CRLF = "\r\n";

        private const string cSpacer = " ";
        private const string cLogTimeFormat = "MM/dd/yyyy hh:mm:ss tt";

        private const int COLUMN_WIDTH_TYPE = 11;
        private const int COLUMN_WIDTH_DATE = 22;
        private const int COLUMN_WIDTH_SOURCE = 20;
        private const string LOGFILE_TIMESTAMP_TOKEN = @"\{0:[yMHhdms].*\}";

        private string _LogFilePath = "";
        private long _LogFileMaxSize = 0;
        private MessageImportance _LoggingDepth;
        private bool _blnUseRollingLogFile;



		/// <summary>
		/// Class Constructor
		/// </summary>
		/// <param name="logFilePath">The log file path.</param>
		/// <param name="logFileMaxSize">Size of the log file max.</param>
		/// <param name="loggingDepth">The logging depth.</param>
        public cEventLogImpl(string logFilePath
                       , long logFileMaxSize
                       , MessageImportance loggingDepth) {
            _LogFileMaxSize = logFileMaxSize;
            _LoggingDepth = loggingDepth;
            _LogFilePath = logFilePath;

            // Check if logfile path contains composite date/time formatting for rolling log file.
            Regex objRegEx = new Regex (LOGFILE_TIMESTAMP_TOKEN, RegexOptions.IgnoreCase);
            _blnUseRollingLogFile = objRegEx.IsMatch(logFilePath);
        }


		/// <summary>
		/// Log Entry struct.
		/// </summary>
        private struct sLogEntry {  
            public string LogDate;
            public string EventType;
            public string Source;
            public string LogText;
        }


		/// <summary>
		/// Constant header line string appended to the beginning of the LogFile.
		/// </summary>
		/// <value>
		/// The header string.
		/// </value>
        private string headerString {
            get {
                return string.Format ("{0,-" + COLUMN_WIDTH_TYPE + "}", "Type")
                     + cSpacer
                     + string.Format ("{0,-" + COLUMN_WIDTH_DATE + "}", "Date")
                     + cSpacer
                     + string.Format ("{0,-" + COLUMN_WIDTH_SOURCE + "}", "Source")
                     + cSpacer
                     + "Description"
                     + CRLF;
            }
        }



		/// <summary>
		/// Gets the log entries to keep.
		/// </summary>
		/// <param name="LineCount">The line count.</param>
		/// <param name="LinesToKeep">The lines to keep.</param>
		/// <param name="logEntriesToKeep">The log entries to keep.</param>
        private void getLogEntriesToKeep(long LineCount
                                       , long LinesToKeep
                                       , out string[] logEntriesToKeep) {

            long lngLinesToEliminate = (LineCount - LinesToKeep);

            logEntriesToKeep = new string[LinesToKeep];
            System.IO.TextReader tr = System.IO.File.OpenText(_LogFilePath);

            try {
                // Skip lines to be eliminated.
                for(int i=0;i<lngLinesToEliminate;++i)
                    { tr.ReadLine(); }

                // Add lines to keep to the array.
                for (int i=0;i<LinesToKeep;++i)
                    { logEntriesToKeep[i] = tr.ReadLine(); }
            }
            catch(Exception e)
            {
                writeEventError(e);
            }
            finally
            {
                if (tr != null)
                {
                    try { tr.Close(); } catch {}
                    tr.Dispose();
                }
            }
        }


		/// <summary>
		/// Writes the new log file.
		/// </summary>
		/// <param name="logEntries">The log entries.</param>
        private void writeNewLogFile(ref string[] logEntries) {
            if (File.Exists(_LogFilePath))
                { File.Delete(_LogFilePath); }

            System.IO.TextWriter tw;
            getLogFile(out tw);

            try
            {
                for (int i=0;i<logEntries.Length;++i)
                {
                    tw.WriteLine(logEntries[i]);
                }
            }
            catch(Exception e)
            {
                writeEventError(e);
            }
            finally
            {
                if (tw !=null)
                {
                    try { tw.Close(); } catch {}
                    tw.Dispose();
                }
            }
        }


		/// <summary>
		/// Retrieves the size in bytes of the LogFile.
		/// </summary>
		/// <returns></returns>
        private long logFileSize() {
            long lngFileSize = 0;
            string strLogFile = string.Empty;

            strLogFile = string.Format(_LogFilePath, DateTime.Now);
            if(File.Exists(strLogFile)) {
                using (FileStream fsOriginal = new FileStream(strLogFile, System.IO.FileMode.Open))
                {
                    lngFileSize = fsOriginal.Length;

                    fsOriginal.Close();
                }
            }

            return lngFileSize;
        }


		/// <summary>
		/// Multiplies the max number of Mb's as specified in the INI  file by 1024
		/// to get the max number of bytes the Logfile can be.
		/// </summary>
		/// <returns></returns>
        private long maxFileSize() {
            return (_LogFileMaxSize * 1024);
        }


		/// <summary>
		/// Gets the log file.
		/// </summary>
		/// <param name="tw">The tw.</param>
        private void getLogFile(out System.IO.TextWriter tw) {
            string strLogFile = "";
            string strBasePath = "";

            if (_blnUseRollingLogFile) {
                strLogFile = string.Format(_LogFilePath, DateTime.Now);
            } else {
                strLogFile = _LogFilePath;
            }

            strBasePath = System.IO.Directory.GetParent(strLogFile).FullName;

            if (!System.IO.Directory.Exists(strBasePath))
                System.IO.Directory.CreateDirectory(strBasePath);

            if (System.IO.File.Exists(strLogFile)) {
                tw = new System.IO.StreamWriter(strLogFile, true); 
            } else {
                tw = new System.IO.StreamWriter(strLogFile, true);    
                tw.WriteLine(headerString);
                tw.Flush();
            }
        }

		/// <summary>
		/// Does the actual write to the output file.
		/// </summary>
		/// <param name="eventMsg">The event MSG.</param>
        private void outputText(string eventMsg) {
            Console.WriteLine(eventMsg);

            System.IO.TextWriter tw = null;
            try
            {
                
                getLogFile(out tw);

                try
                {
                    tw.WriteLine(eventMsg);
                    tw.Flush();
                    tw.Close();
                }
                catch (Exception e)
                {
                    writeEventError(e);
                    Console.WriteLine("error:{0}", e);
                }
                finally
                {
                    if (tw!=null)
                    {
                        try { tw.Close(); } catch {}
                       
                    }
                }
            }
            catch(Exception e)
            {
                writeEventError(e);
            }
            finally 
            {
                if (tw != null)
                {
                    tw.Dispose();
                }
            }
        }

//*******************************************************************************************
//*******************************************************************************************

		/// <summary>
		/// Overloaded function.
		/// </summary>
		/// <param name="msg">The MSG.</param>
		/// <param name="messageImportance">The message importance.</param>
		/// <author>
		/// Joel Caples
		/// </author>
        public void logEvent(string msg
                           , MessageImportance messageImportance) {
            logEvent(msg
                   , ""
                   , MessageType.Information
                   , messageImportance);
        }

		/// <summary>
		/// Overloaded function.
		/// </summary>
		/// <param name="msg">The MSG.</param>
		/// <param name="src">The SRC.</param>
		/// <param name="messageImportance">The message importance.</param>
        public void logEvent(string msg
                           , string src
                           , MessageImportance messageImportance) {
            logEvent(msg
                   , src
                   , MessageType.Information
                   , messageImportance);
        }

		/// <summary>
		/// Logs the event.
		/// </summary>
		/// <param name="msg">The MSG.</param>
		/// <param name="src">The SRC.</param>
		/// <param name="messageType">Type of the message.</param>
        public void logEvent(string msg
                           , string src
                           , MessageType messageType) {

            logEvent(msg, src, messageType, MessageImportance.Essential);
        }

		/// <summary>
		/// Adds the specified message to the Logfile.
		/// </summary>
		/// <param name="msg">The MSG.</param>
		/// <param name="src">The SRC.</param>
		/// <param name="messageType">Type of the message.</param>
		/// <param name="messageImportance">The message importance.</param>
        public void logEvent(string msg
                           , string src
                           , MessageType messageType
                           , MessageImportance messageImportance) {

            string strEventMsg;

            try
            {            
                if(isImportant(messageImportance)) {
                    sLogEntry tmpLog = new sLogEntry();

                    tmpLog.LogDate = System.DateTime.Now.ToString (cLogTimeFormat).PadRight (COLUMN_WIDTH_DATE);
                    tmpLog.LogText = msg;
                    tmpLog.Source = src.PadRight (COLUMN_WIDTH_SOURCE);
                    
                    switch(messageType)
                    {
                        case MessageType.Error :
                            tmpLog.EventType = string.Format ("{0,-" + COLUMN_WIDTH_TYPE + "}", "Error");
                            break;
                        case MessageType.Information :
                            tmpLog.EventType = string.Format ("{0,-" + COLUMN_WIDTH_TYPE + "}", "Information");
                            break;
                        case MessageType.Warning :
                            tmpLog.EventType = string.Format ("{0,-" + COLUMN_WIDTH_TYPE + "}", "Warning");
                            break;
                    }

                        strEventMsg=tmpLog.EventType
                                + cSpacer
                                + tmpLog.LogDate
                                + cSpacer
                                + tmpLog.Source
                                + cSpacer
                                + tmpLog.LogText;

                        outputText(strEventMsg);

                        createNewLogFileIfNecessary();
                  
                }
            }
            catch(Exception e)
            {
                writeEventError(e);
            }
        }

		/// <summary>
		/// Determines whether the specified message importance is important.
		/// </summary>
		/// <param name="messageImportance">The message importance.</param>
		/// <returns>
		///   <c>true</c> if the specified message importance is important; otherwise, <c>false</c>.
		/// </returns>
        private bool isImportant(MessageImportance messageImportance) {

            bool bolRetVal;
            MessageImportance enmImportanceLevel;

            try{enmImportanceLevel = (MessageImportance)_LoggingDepth; }
            catch{enmImportanceLevel = MessageImportance.Essential; }

            if(enmImportanceLevel == MessageImportance.Debug)
            {
                bolRetVal = true; 
            }
            else if((enmImportanceLevel == MessageImportance.Verbose) &&
                    ((messageImportance == MessageImportance.Essential)||
                     (messageImportance == MessageImportance.Verbose)
                    )
                   )
            {
                bolRetVal = true;
            }
            else if((enmImportanceLevel == MessageImportance.Essential) &&
                    (messageImportance == MessageImportance.Essential))
            {
                bolRetVal = true;
            }
            else
            {
                bolRetVal = false;
            }

            return bolRetVal;
        }


		/// <summary>
		/// Overloaded function
		/// </summary>
		/// <param name="e">The e.</param>
        public void logError(Exception e) {
            if(e.TargetSite == null) {
                logError(e
                       , e.Source
                       , string.Empty);
            } else {
                logError(e
                       , e.Source
                       , e.TargetSite.Name);
            }
        }


		/// <summary>
		/// Formats the Exception object in standard Error format, and calls the
		/// logEvent method to write the error to the Logfile.
		/// </summary>
		/// <param name="e">The e.</param>
		/// <param name="src">The SRC.</param>
		/// <param name="procName">Name of the proc.</param>
		public void logError(Exception e
						   , string src
						   , string procName) {
			string strAns;

			try {

				if(procName != "") { strAns = "Error occurred in " + procName + ".  " + e.Message + "."; } else { strAns = e.Message + "."; }

				//logEvent(strAns
				//       , e.TargetSite + " " + e.TargetSite.ToString()
				//       , MessageType.Error
				//       , MessageImportance.Essential);
				logEvent(strAns + "  " + (e.TargetSite == null ? string.Empty : e.TargetSite.ToString())
					   , src
					   , MessageType.Error
					   , MessageImportance.Essential);
			} catch {
				writeEventError(e);
			}
		}

		/// <summary>
		/// Formats the Exception object in standard Error format, and calls the
		/// logEvent method to write the error to the Log file.
		/// </summary>
		/// <param name="e">The e.</param>
		/// <param name="src">The SRC.</param>
		public void logError(Exception e, object src) {
			try {
				logEvent("Error occurred in " + (e.TargetSite == null ? string.Empty : e.TargetSite.ToString()) + " " + e.Message.ToString(), src.GetType().Name, MessageType.Error, MessageImportance.Essential);
			} catch {
				writeEventError(e);
			}
		}

		/// <summary>
		/// Logs the warning.
		/// </summary>
		/// <param name="msg">The MSG.</param>
		/// <param name="messageImportance">The message importance.</param>
        public void logWarning(string msg
                             , MessageImportance messageImportance) {
            logEvent(msg
                   , ""
                   , MessageType.Warning
                   , messageImportance);
        }


		/// <summary>
		/// Logs the warning.
		/// </summary>
		/// <param name="msg">The MSG.</param>
		/// <param name="src">The SRC.</param>
		/// <param name="messageImportance">The message importance.</param>
        public void logWarning(string msg
                             , string src
                             , MessageImportance messageImportance) {
            logEvent(msg
                   , src
                   , MessageType.Warning
                   , messageImportance);
        }


		/// <summary>
		/// Writes the event error.
		/// </summary>
		/// <param name="e">The e.</param>
		/// <remarks>
		/// This code was commented out because on a very fast machine, the Event
		/// log will fill up.  The error that will normally be handled here is a
		/// conflict when attempting to access the logfile.  This happens when
		/// parallel threads compete for file access.  Commenting this out means
		/// that some events will not be logged.  A better solution to handle
		/// this would be to use Thread blocking with a Mutex.
		/// </remarks>
        private void writeEventError(Exception e) {
////            System.Diagnostics.EventLog.WriteEntry(e.Source
////                                                 , "eventLog unable to write to logfile : " 
////                                                 + serverOptions.logFilePath 
////                                                 + ".  Error at : " + e.Source + "."
////                                                 + e.TargetSite.ToString() + e.Message + "."
////                                                 , System.Diagnostics.EventLogEntryType.Error);
        }


		/// <summary>
		/// Accepts the full path to a readable (presumably text) file.  returns
		/// the number of lines contained in that file.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <returns></returns>
        private static long countFileLines(string filePath) {
            string strTemp = "";
            long lngLineCount=0;

            System.IO.TextReader tr = System.IO.File.OpenText(filePath);

            while((strTemp = tr.ReadLine()) != null)   //(tr.Peek())
            {
                ++lngLineCount;
            } 

            tr.Close();
            tr=null;

            return lngLineCount;
        }

		/// <summary>
		/// Creates the new log file if necessary.
		/// </summary>
        private void createNewLogFileIfNecessary()
        {
            bool bolContinue; 
            int intTemp;
            string strLogFileName, strLogFile;
            try {

                strLogFile = string.Format(_LogFilePath, DateTime.Now);
                if(_LogFileMaxSize > 0) {
    
                    while(logFileSize() > maxFileSize()) {
                        bolContinue = true;
                        intTemp = 1;
                        while (bolContinue)
                        {
                            strLogFileName = strLogFile.Replace(".txt", "." + intTemp.ToString().PadLeft(6, '0') + ".txt");
                           
                            if (File.Exists(strLogFileName))
                            {
                                ++intTemp;
                                if (intTemp > 999999)
                                {
                                    //we are out of log files just go ahead and let it grow :)
                                    bolContinue = false;
                                    //throw (new Exception("Maximum number of log files exceeded."));
                                }
                            }
                            else
                            {
                                File.Move(strLogFile, strLogFileName);
                                
                                bolContinue = false;
                            }
                        }
                    }
                }
            }
            catch(Exception e) {
                writeEventError(e);
            }
        }
    }
}
