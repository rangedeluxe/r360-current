﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WFS.RecHub.Common.Log
{

	/// <summary>
	/// This is a "wrapper" class around an internal implementation of the event log.  
	/// It uses the Bridge design pattern to abstract the concrete implmentation away from clients, without modifying the public interface.
	/// </summary>
	public class cEventLog
	{
		/// <summary>
		/// Use this public method in Unit Tests / Integration Tests to disable File-Based event logging
		/// </summary>
		public static bool UseFakeInMemEventLog { get; set; }

		private readonly IEventLog _eventLog;
		public cEventLog(string logFilePath
					   , long logFileMaxSize
					   , MessageImportance loggingDepth)
		{
			// Allow for global faking of the file-based event log
			if (UseFakeInMemEventLog)
				_eventLog = InMemoryEventLog.Instance;
			else
				_eventLog = new cEventLogImpl(logFilePath, logFileMaxSize, loggingDepth);
		}

		public void logError(Exception e)
		{
			_eventLog.logError(e);
		}

		public void logError(Exception e, object src)
		{
			_eventLog.logError(e, src);
		}

		public void logError(Exception e, string src, string procName)
		{
			_eventLog.logError(e, src, procName);
		}

		public void logEvent(string msg, string src, MessageImportance messageImportance)
		{
			_eventLog.logEvent(msg, src, messageImportance);
		}

		public void logEvent(string msg, string src, MessageType messageType)
		{
			_eventLog.logEvent(msg, src, messageType);
		}

		public void logEvent(string msg, string src, MessageType messageType, MessageImportance messageImportance)
		{
			_eventLog.logEvent(msg, src, messageType, messageImportance);
		}

		public void logEvent(string msg, MessageImportance messageImportance)
		{
			_eventLog.logEvent(msg, messageImportance);
		}

		public void logWarning(string msg, string src, MessageImportance messageImportance)
		{
			_eventLog.logWarning(msg, src, messageImportance);
		}

		public void logWarning(string msg, MessageImportance messageImportance)
		{
			_eventLog.logWarning(msg, messageImportance);
		}
	}

	/// <summary>
	/// This is the public interface for cEventLog.  It is explicitly defined here to allow for simple replacement of the implementation
	/// </summary>
	internal interface IEventLog
	{
		void logError(Exception e);
		void logError(Exception e, object src);
		void logError(Exception e, string src, string procName);
		void logEvent(string msg, string src, WFS.RecHub.Common.MessageImportance messageImportance);
		void logEvent(string msg, string src, WFS.RecHub.Common.MessageType messageType);
		void logEvent(string msg, string src, WFS.RecHub.Common.MessageType messageType, WFS.RecHub.Common.MessageImportance messageImportance);
		void logEvent(string msg, WFS.RecHub.Common.MessageImportance messageImportance);
		void logWarning(string msg, string src, WFS.RecHub.Common.MessageImportance messageImportance);
		void logWarning(string msg, WFS.RecHub.Common.MessageImportance messageImportance);
	}

	/// <summary>
	/// This class provides a singleton in-memory container for event messages, which can be interrogated from a Test Method...
	/// </summary>
	public class InMemoryEventLog : IEventLog
	{
		/// <summary>
		/// The singleton instance; all events will get logged to this instance when the event log is faked.
		/// </summary>
 		public static InMemoryEventLog Instance = new InMemoryEventLog();

		private InMemoryEventLog() { }

		public class InMemEvent
		{
			public DateTime Time { get; private set; }
			public string Message { get; set; }
			public string Source { get; set; }
			public string Method { get; set; }
			public Exception Error { get; set; }
			public MessageType Type { get; set; }
			public MessageImportance Importance { get; set; }

			public InMemEvent()
			{
				Time = DateTime.Now;
			}

			public override string ToString()
			{
				return string.Format("{0} {1,-12} {2,-7} {3,-20}{4} {5}", 
					Time.ToString("HH:mm:ss.fff"), 
					Importance.ToString(),
					Type.ToString(),
					Source ?? string.Empty,
					Method ?? string.Empty, 
					(Error != null ? Error.ToString() : Message));
			}
		}

		/// <summary>
		/// The List of events.  Test methods can analyze this however they want
		/// </summary>
		public readonly List<InMemEvent> EventList = new List<InMemEvent>();

		private StringBuilder _fullLog = new StringBuilder();
		private void Add(InMemEvent e)
		{
			_fullLog.AppendLine(e.ToString());
			EventList.Add(e);
		}

		/// <summary>
		/// ToString has been overriden to dump the full contents of the event list
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return _fullLog.ToString();
		}

		/// <summary>
		/// Use this Clear method to reset the cached ToString buffer with the event list...
		/// </summary>
		public void Clear()
		{
			EventList.Clear();
			_fullLog.Clear();
		}

		public void logEvent(string message, string src, MessageType messageType, MessageImportance messageImportance)
		{
			Add(new InMemEvent() { Message = message, Source = src, Type = messageType, Importance = messageImportance });
		}

		public void logError(Exception e, string src, string procName)
		{
			Add(new InMemEvent() { Error = e, Source = src, Method = procName, Type = MessageType.Error, Importance = MessageImportance.Essential });
		}

		public void logError(Exception e)
		{
			Add(new InMemEvent() { Error = e, Type = MessageType.Error, Importance = MessageImportance.Essential });
		}

		public void logError(Exception e, object src)
		{
			Add(new InMemEvent() { Error = e, Source = src.ToString(), Type = MessageType.Error, Importance = MessageImportance.Essential });
		}

		public void logEvent(string msg, string src, MessageImportance messageImportance)
		{
			Add(new InMemEvent() { Message = msg, Source = src, Type = MessageType.Information, Importance = messageImportance });
		}

		public void logEvent(string msg, string src, MessageType messageType)
		{
			Add(new InMemEvent() { Message = msg, Source = src, Type = messageType, Importance = MessageImportance.Essential});
		}

		public void logEvent(string msg, MessageImportance messageImportance)
		{
			Add(new InMemEvent() { Message = msg, Type = MessageType.Information, Importance = messageImportance });
		}

		public void logWarning(string msg, string src, MessageImportance messageImportance)
		{
			Add(new InMemEvent() { Message = msg, Source = src, Type = MessageType.Warning, Importance = messageImportance });
		}

		public void logWarning(string msg, MessageImportance messageImportance)
		{
			Add(new InMemEvent() { Message = msg, Type = MessageType.Warning, Importance = messageImportance });
		}
	}
}
