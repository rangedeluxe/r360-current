using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

using WFS.RecHub.Common.Crypto;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: InegraPAY Online database access Component
*  Filename: cDatabase2.cs
*    Author: Joel Caples
*
* Revisions:
* CR 12257 JMC 07/28/2005
*    -Created class as a restructuring of the original cDatabase class.  This
*     implementation uses a module-level Connection object, and implements
*     transactions.  This objects also implements the IDisposable interface.
*     Any application instantiating this class should call the Dispose() method 
*     once processing has completed.
* CR 12257 JMC 07/28/2005
*    -Added ExecuteProcedure method.
* CR 12257 JMC 08/22/2005
*    -Modified ExecuteProcedure to return parameters.
* CR 14209 JMC 11/02/2005
*    -Added SetDeadlockPriority property to class.  When true, the 
*     "SET DEADLOCK_PRIORITY LOW" directive will be prepended to all SQL
*     statements prior to execution.
* CR 14211 JMC 11/04/2005
*    -Added QueryRetryAttempts property.  Modified SQL execution functions
*     to trap errors, and retry the query the specified number of times prior
*     to failure.
*    -Added onOutputMessage and onOutputError functions to handle message
*     events in a consistent manner.
* CR 15184 JMC 01/16/2006
*   -Modified Dispose method to set private variable to NULL.
* CR 18946 JMC 11/06/2006
*   -Added CreateDataTable method.
* CR 29446 JMC 04/29/2010
*   -executeNonScalarQuery(string) now returns -2 when the method fails due 
*    to a database error. 
*   -executeProcedure(string, SqlParameter[], out SqlParameter[]) now 
*    properly exit its' loop when an error occurs and will not retry 
*    infinitely.
* CR 30180 JMC 07/12/2010
*   -Added deriveParameters method to return the parameter definition for a
*    specified stored procedure (in support of CR 30178)
* CR 31831 JMC 11/11/2010
*   -Added property 'lastException' so the calling app can determine whether
*    an error has occurred without capturing the event.
* CR 32576 JMC 02/01/2011
*   -Added executeBatchInsert() function.
* CR 32576 WJS 02/09/2011 
*   Added retries to executeBatchInsert() function. Adjusted size of table
*   passed in
* CR 45231 JMC 06/15/2011
*   -Added usage of the module-level Connection object when processing within
*    a database transaction.
* CR 23161 JCS 05/17/2011
*   -Updated decryptConnectionString() to handle updated decryption class.
* WI 83239 CRG 12/19/2012
*   -Add debug logging to execute procedure functions in cDatabase.cs
* WI 90241 CRG 12/19/2012
*   -Change Schema from OLTA to approiate RecHub Schema for all Store Porcedure and calling functions
* WI 97662 EAS 05/16/2013
*   -Add execute procedures to return xml document type
* WI 103997 JMC 05/31/2013
*	-Updated namespace to WFS.RecHub.Common.Database
*	-Modified replaced ipoLib Message delegates with internal versions.
* WI 104368 JMC 06/04/2013
*	-Added CreateDataReader() method for Extract Wizard.
******************************************************************************/
namespace WFS.RecHub.Common.Database {

    /// <summary>
    /// InegraPAY Online database access class
    /// </summary>
	public class cDatabase : IDisposable
	{
        private string _ConnectionString;
        private string _DecryptedConnectionString = string.Empty;
        private bool _SetDeadlockPriority = false;
        private byte _QueryRetryAttempts = 5;
        private SqlConnection _Connection = null;
        private SqlTransaction _SQLTrans = null;
        private Exception _LastException = null;
		/// <summary>
		/// The _ debug level
		/// </summary>
		/// <description>
		/// the debug level is stored in the ipoline.ini file. For the log file
		/// it is converted to an enumeration called Message Importance as follows:
		///		0 - essential
		///		1 - verbose
		///		2 - all (debug)
		///		-1 - no debug level has been defined
		/// </description>
		private int _DebugLevel = -1;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string beginTrans() {

            string strTransactionID = "";

            strTransactionID = Guid.NewGuid().ToString().Replace("-", "");
            _SQLTrans = connection.BeginTransaction(IsolationLevel.ReadCommitted, strTransactionID);

            return(strTransactionID);
        }

        /// <summary>
        /// 
        /// </summary>
        public void commitTrans() {
            try {
                if(_SQLTrans != null) {
                    _SQLTrans.Commit();               
                    _SQLTrans.Dispose();
                    _SQLTrans = null;
                }
            } catch(Exception ex) {
                    onOutputMessage("An exception of type " + ex.GetType() +
                                    " was encountered while attempting to commit the transaction."
                                , ""
                                , DbMessageType.Error
                                , DbMessageImportance.Essential);                
            } finally {
                if(_Connection != null) {
                    _Connection.Dispose();
                    _Connection = null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void rollbackTrans() {
            try {
                if(_SQLTrans != null) {
                    _SQLTrans.Rollback();
                    _SQLTrans.Dispose();
                    _SQLTrans = null;
                }
            } catch (SqlException ex) {
                    onOutputMessage("An exception of type " + ex.GetType() +
                                    " was encountered while attempting to roll back the transaction."
                                , ""
                                , DbMessageType.Error
                                , DbMessageImportance.Essential);
            } finally {
                if(_Connection != null) {
                    _Connection.Dispose();
                    _Connection = null;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private SqlConnection connection {
            get {
                if(_Connection == null) {
                    if(_DecryptedConnectionString.Length == 0) {
                        _DecryptedConnectionString = decryptConnectionString(_ConnectionString);
                    }
                    _Connection = new SqlConnection(_DecryptedConnectionString);
                }
                if(_Connection.State != ConnectionState.Open) {
                    _Connection.Open();
                }
                
                return(_Connection);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Exception lastException {
            get {
                return(_LastException);
            }
        }

        /// <summary>
        /// Event handler to output informational text.
        /// </summary>
        public event OutputDbMessageEventHandler outputMessage;

        /// <summary>
        /// EventHandler to output error information.
        /// </summary>
        public event OutputDbErrorEventHandler outputError;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void onOutputError(System.Exception e) {
            if (outputError != null) {
                outputError(e);
            }
            _LastException = e;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        public void onOutputMessage(string message
                                  , string src
                                  , DbMessageType messageType
                                  , DbMessageImportance messageImportance) {
            if (outputMessage != null) {
                outputMessage(message
                            , src
                            , messageType
                            , messageImportance);
            }
        }

		/// <summary>
		/// Public constructor.
		/// </summary>
		/// <param name="connectionString">Required connection string.  Tells the
		/// module how to connect to the database.</param>
		public cDatabase(string connectionString) {
			_ConnectionString = connectionString;
			_DebugLevel = -1;
		}

		/// <summary>
		/// Public constructor.
		/// </summary>
		/// <param name="connectionString">Required connection string.  Tells the
		/// module how to connect to the database.</param>
		/// <param name="debugLevel">The debug level.</param>
		public cDatabase(string connectionString, int debugLevel) {
			_ConnectionString = connectionString;
			_DebugLevel = debugLevel;
		}


        /// <summary>
        /// 
        /// </summary>
        public bool SetDeadlockPriority {
            get { return(_SetDeadlockPriority); }
            set { _SetDeadlockPriority = value; }
        }

        /// <summary>
        /// This value indicates the number of times the database component 
        /// should RE-try to execute any SQL.  The database component will initially try once
        /// and retry the number of times indicated by the QueryRetryAttempts property.
        /// </summary>
        public byte QueryRetryAttempts
        {
            get { return(_QueryRetryAttempts); }
            set { _QueryRetryAttempts = value; }
        }

		/// <summary>
		/// Executes a database call that returns a recordset (SELECT).
		/// </summary>
		/// <param name="SQL">SQL Statement to be executed on the database.</param>
		/// <param name="dataset">Dataset returned contains the results of the SQL statement.</param>
		/// <returns>
		/// Boolean indicating whether the SQL statement returned results
		/// successfully.
		/// </returns>
        public bool createDataSet(string SQL
                                , out System.Data.DataSet dataset)
        {
            bool bolRetVal = false;
            int intTries = 0;
            bool bolContinue = true;

            SqlDataAdapter da = null;
            SqlCommand cmd = null;

            _LastException = null;

            try
            {       
                // CR 14209 JMC 11/02/2005 - Append DEADLOCK_PRIORITY if set.
                if(SetDeadlockPriority) {
                    SQL = "SET DEADLOCK_PRIORITY LOW;\n" + SQL;
                }

                cmd = new SqlCommand(SQL, connection);

                if(_SQLTrans != null) {
                    cmd.Transaction = _SQLTrans;
                }

                cmd.CommandText = SQL;
                da = new SqlDataAdapter();
                da.SelectCommand = cmd;
				OutputDatabaseMessage(cmd, "Creating Dataset : ");
                dataset = new DataSet();

                while(bolContinue) {
                    ++intTries;
                    try {
                        da.Fill(dataset);
                        bolRetVal=true;
                        bolContinue = false;
                    } catch(Exception ex) {
                        if(intTries > this.QueryRetryAttempts) {
                            onOutputMessage("An Error occurred while creating recordset: " + SQL, ""
                                          , DbMessageType.Error
                                          , DbMessageImportance.Essential);
                            onOutputError(ex);
                            bolContinue = false;
                        } else {
                            onOutputMessage("An Error occurred while creating recordset.  Attempting retry(" + intTries.ToString() + ")...", ""
                                          , DbMessageType.Warning
                                          , DbMessageImportance.Essential);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                onOutputError(e);

                dataset=null;
                bolRetVal = false;
            }
            finally {

                if(da != null) {
                    da.Dispose();
                    da = null;
                }

                if(cmd != null) {
                    cmd.Dispose();
                    cmd = null;
                }

                if(_SQLTrans == null && _Connection != null) {
                    TerminateConnection();
                }
            }

            return bolRetVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SQL"></param>
        /// <param name="datatable"></param>
        /// <returns></returns>
        public bool CreateDataTable(string SQL
                                  , out System.Data.DataTable datatable)
        {
            bool bolRetVal = false;
            int intTries = 0;
            bool bolContinue = true;
            datatable = null;

            _LastException = null;

            try
            {       
                // CR 14209 JMC 11/02/2005 - Append DEADLOCK_PRIORITY if set.
                if(SetDeadlockPriority) {
                    SQL = "SET DEADLOCK_PRIORITY LOW;\n" + SQL;
                }

                using (SqlCommand cmd = new SqlCommand(SQL, connection))
                {

                    if(_SQLTrans != null) {
                        cmd.Transaction = _SQLTrans;
                    }

                    cmd.CommandText = SQL;
					OutputDatabaseMessage(cmd, "Creating Data Table : ");

					using(SqlDataAdapter da = new SqlDataAdapter()) {
                        da.SelectCommand = cmd;
                        using (DataSet ds = new DataSet())
                        {

                            while(bolContinue) {
                                ++intTries;
                                try {
                                    da.Fill(ds);
                                    bolRetVal=true;
                                    bolContinue = false;
                                } catch(Exception ex) {
                                    if(intTries > this.QueryRetryAttempts) {
                                        onOutputMessage("An Error occurred while creating recordset: " + SQL, ""
                                                      , DbMessageType.Error
                                                      , DbMessageImportance.Essential);
                                        onOutputError(ex);
                                        bolContinue = false;
                                    } else {
                                        onOutputMessage("An Error occurred while creating recordset.  Attempting retry(" + intTries.ToString() + ")...", ""
                                                      , DbMessageType.Warning
                                                      , DbMessageImportance.Essential);
                                    }
                                }
                            }
                    

                            if(ds != null) {
                                if(ds.Tables.Count > 0) {
                                    datatable = ds.Tables[0];
                                } else {
                                    datatable = null;
                                }
                            }
                        }
                    }
                            
                }
            }
            catch(Exception e) {
                onOutputError(e);
                datatable = null;
                bolRetVal = false;
            }
            finally {

                if(_SQLTrans == null && _Connection != null) {
                    TerminateConnection();
                }
            }

            return bolRetVal;
        }

        /// <summary>
        /// Executes a database call that does not return a recordset
        /// (UPDATE, INSERT, DELETE, etc...)
        /// </summary>
        /// <param name="SQL">
        /// SQL Statement to be executed on the database.
        /// </param>
        /// <returns>
        /// Boolean indicating whether the SQL statement returned results 
        /// successfully.
        /// </returns>
        public int executeNonQuery(string SQL)
        {
            int intRetVal = -1;
            int intTries = 0;
            bool bolContinue = true;

            SqlCommand cmd = null;

            _LastException = null;
            
            try
            {       
                // CR 14209 JMC 11/02/2005 - Append DEADLOCK_PRIORITY if set.
                if(SetDeadlockPriority) {
                    SQL = "SET DEADLOCK_PRIORITY LOW;\n" + SQL;
                }

                cmd = new SqlCommand(SQL, connection);
                
                if(_SQLTrans != null) {
                    cmd.Transaction = _SQLTrans;
                }

				OutputDatabaseMessage(cmd, "Executing non-query : ");
                while(bolContinue) {
                    ++intTries;
                    try {
                        intRetVal = cmd.ExecuteNonQuery();
                        bolContinue = false;
                    } catch(Exception ex) {
                        if(intTries > this.QueryRetryAttempts) {
                            onOutputMessage("An Error occurred while executing SQL: " + SQL, ""
                                          , DbMessageType.Error
                                          , DbMessageImportance.Essential);
                            onOutputError(ex);
                            bolContinue = false;
                        } else {
                            onOutputMessage("An Error occurred while executing SQL.  Attempting retry(" + intTries.ToString() + ")...", ""
                                          , DbMessageType.Warning
                                          , DbMessageImportance.Essential);
                        }
                    }
                }

                cmd.Dispose();

                return (intRetVal);
            }
            catch(Exception e)
            {
                onOutputError(e);
                return -1;
            }
            finally {

                if(cmd != null) {
                    cmd.Dispose();
                    cmd = null;
                }

                if(_SQLTrans == null && _Connection != null) {
                    TerminateConnection();
                }
            }
        }

        /// <summary>
        /// Executes a database call that does not return a recordset
        /// (UPDATE, INSERT, DELETE, etc...)
        /// </summary>
        /// <param name="SQL">
        /// SQL Statement to be executed on the database.
        /// </param>
        /// <returns>
        /// Boolean indicating whether the SQL statement returned results 
        /// successfully.
        /// This was added because a successful database call will return -1
        /// and that was returning false in executeNonQuery
        /// </returns>
        public int executeNonScalarQuery(string SQL)
        {
            int intRetVal = -1; // assume success
            int intTries = 0;
            bool bolContinue = true;

            SqlCommand cmd = null;

            _LastException = null;
            
            try
            {       
                if(SetDeadlockPriority) {
                    SQL = "SET DEADLOCK_PRIORITY LOW;\n" + SQL;
                }

                cmd = new SqlCommand(SQL, connection);

                if(_SQLTrans != null) {
                    cmd.Transaction = _SQLTrans;
                }

				OutputDatabaseMessage(cmd, "Executing non-scalar-query : ");
                while(bolContinue) {
                    ++intTries;
                    try {
                        intRetVal = cmd.ExecuteNonQuery();
                        bolContinue = false;
                    } catch(Exception ex) {
                        if(intTries > this.QueryRetryAttempts) {
                            onOutputMessage("An Error occurred while executing SQL: " + SQL, ""
                                          , DbMessageType.Error
                                          , DbMessageImportance.Essential);
                            intRetVal = -2;
                            onOutputError(ex);
                            bolContinue = false;
                        } else {
                            onOutputMessage("An Error occurred while executing SQL. Attempting retry(" + intTries.ToString() + ")...", ""
                                          , DbMessageType.Warning
                                          , DbMessageImportance.Essential);
                        }
                    }
                }

                cmd.Dispose();

                return (intRetVal);
            }
            catch(Exception e)
            {
                onOutputError(e);
                return -2;  // return -2 here to indicate a failure
            }
            finally {

                if(cmd != null) {
                    cmd.Dispose();
                    cmd = null;
                }

                if(_SQLTrans == null && _Connection != null) {
                    TerminateConnection();
                }
            }
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        public bool executeProcedure(string procName, SqlParameter[] parms) {
            SqlParameter[] objOutParms;
            return(executeProcedure(procName, parms, out objOutParms));
        }

		/// <summary>
		/// Executes the procedure.
		/// </summary>
		/// <param name="procName">Name of the proc.</param>
		/// <param name="datatable">The data table.</param>
		/// <returns></returns>
		public bool executeProcedure(string procName, out DataTable datatable) {
			SqlParameter[] objOutParms = null;
			SqlParameter[] parms = new SqlParameter[] { };
			return (executeProcedure(procName, parms, out objOutParms, out datatable));
		}

		/// <summary>
		/// Executes the procedure.
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <param name="outParms"></param>
        public bool executeProcedure(string procName, SqlParameter[] parms, out SqlParameter[] outParms) {

            SqlCommand cmd = null;
            SqlParameter[] returnParms;
            int intTries = 0;
            bool bolContinue;
            bool bolRetVal;

            _LastException = null;

            try {
            
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procName;
                cmd.Connection = connection;
            
                if(_SQLTrans != null) {
                    cmd.Transaction = _SQLTrans;
                }

                foreach(SqlParameter parm in parms) {
                    cmd.Parameters.Add(parm);
                }

				OutputDatabaseMessage(cmd, "Executing Procedure : EXEC ");
                bolContinue = true;
                bolRetVal = false;

                while(bolContinue) {
                    ++intTries;
                    try {
                        cmd.ExecuteNonQuery();
                        bolContinue = false;
                        bolRetVal = true;
                    } catch(Exception ex) {
                        if(intTries > this.QueryRetryAttempts) {
                            onOutputMessage("An Error occurred while executing Procedure: " + procName, ""
                                            , DbMessageType.Error
                                            , DbMessageImportance.Essential);
                            onOutputError(ex);
                            bolContinue = false;
                        } else {
                            onOutputMessage("An Error occurred while executing Procedure.  Attempting retry(" + intTries.ToString() + ")...", ""
                                            , DbMessageType.Warning
                                            , DbMessageImportance.Essential);
                        }
                    }
                }

                returnParms = new SqlParameter[cmd.Parameters.Count];
                cmd.Parameters.CopyTo(returnParms, 0);
			} catch(Exception ex) {
				onOutputMessage("An Error occurred while running stored procedure: " + procName,
					this.GetType().Name,
                    DbMessageType.Error,
                    DbMessageImportance.Essential);
                onOutputError(ex);
                returnParms = null;
                bolRetVal = false;
            }
            finally {

                if(cmd != null) {
                    cmd.Dispose();
                    cmd = null;
                }

                if(_SQLTrans == null && _Connection != null) {
                    TerminateConnection();
                }
            }

            outParms = returnParms;

            return(bolRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <param name="datatable"></param>
        /// <returns></returns>
        public bool executeProcedure(string procName, SqlParameter[] parms, out System.Data.DataTable datatable) {
            SqlParameter[] objOutParms;
            return(executeProcedure(procName, parms, out objOutParms, out datatable));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <param name="datatable"></param>
        /// <param name="outParms"></param>
        /// <returns></returns>
        public bool executeProcedure(string procName, SqlParameter[] parms, out SqlParameter[] outParms, out System.Data.DataTable datatable) {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procName;
            cmd.Connection = connection;
            SqlParameter[] returnParms = null;
            int intTries = 0;
            bool bolContinue = true;
            DataSet ds = null;
            SqlDataAdapter da;
            bool bolRetVal;

            _LastException = null;

            if(_SQLTrans != null) {
                cmd.Transaction = _SQLTrans;
            }

            foreach(SqlParameter parm in parms) {
                cmd.Parameters.Add(parm);
            }

			OutputDatabaseMessage(cmd, "Executing Procedure : EXEC ");
            da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            datatable = null;
    
            try
            {
                ds = new DataSet();
                bolRetVal = false;

                while(bolContinue) {
                    ++intTries;
                    try {
                        da.Fill(ds);
                        bolContinue = false;
                        bolRetVal = true;
                    } catch(Exception ex) {
                        if(intTries > this.QueryRetryAttempts) {
                            onOutputMessage("An Error occurred while running stored procedure: " + procName, ""
                                          , DbMessageType.Error
                                          , DbMessageImportance.Essential);
                            onOutputError(ex);
                            bolContinue = false;
                        }else{
                            onOutputMessage("An Error occurred while running stored procedure.  Attempting retry(" + intTries.ToString() + ")...", ""
                                          , DbMessageType.Warning
                                          , DbMessageImportance.Essential);
                        }
                    }
                }
                da.Dispose();
                da = null;

                if(ds != null && ds.Tables.Count > 0) {
                    datatable = ds.Tables[0];
                } else {
                    datatable = null;
                }
                returnParms = new SqlParameter[cmd.Parameters.Count];
                cmd.Parameters.CopyTo(returnParms, 0);
			} catch(Exception ex) {
				onOutputMessage("An Error occurred while running stored procedure: " + procName,
					this.GetType().Name,
                    DbMessageType.Error,
                    DbMessageImportance.Essential);
				onOutputError(ex);

                if(datatable != null) {
                    datatable.Dispose();
                    datatable = null;
                }
                returnParms = null;
                bolRetVal = false;
            }
            finally {

                if(da != null) {
                    da.Dispose();
                    da = null;
                }

                if(ds != null) {
                    ds.Dispose();
                    ds = null;
                }

                if(cmd != null) {
                    cmd.Dispose();
                    cmd = null;
                }

                if(_SQLTrans == null && _Connection != null) {
                    TerminateConnection();
                }
            }

            outParms = returnParms;

            return(bolRetVal);
        }
        
        #region Execute Procedure returns XML document type

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <param name="xmlDocument"></param>
        /// <returns></returns>
        public bool executeProcedure(string procName, SqlParameter[] parms, out XmlDocument xmlDocument)
        {
            SqlParameter[] objOutParms;
            return (executeProcedure(procName, parms, out objOutParms, out xmlDocument));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <param name="outParms"></param>
        /// <param name="xmlDocument"></param>
        /// <returns></returns>
        public bool executeProcedure(string procName, SqlParameter[] parms, out SqlParameter[] outParms, out XmlDocument xmlDocument)
        {
            XmlReader reader = null;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procName;
            cmd.Connection = connection;
            SqlParameter[] returnParms = null;
            int intTries = 0;
            bool bolContinue = true;
            bool bolRetVal;

            _LastException = null;
            xmlDocument = null;

            if (_SQLTrans != null)
            {
                cmd.Transaction = _SQLTrans;
            }

            foreach (SqlParameter parm in parms)
            {
                cmd.Parameters.Add(parm);
            }

            OutputDatabaseMessage(cmd, "Executing Procedure : EXEC ");

            try
            {
                bolRetVal = false;

                while (bolContinue)
                {
                    ++intTries;
                    try
                    {
                        reader = cmd.ExecuteXmlReader();
                        bolContinue = false;
                        bolRetVal = true;
                    }
                    catch (Exception ex)
                    {
                        if (intTries > this.QueryRetryAttempts)
                        {
                            onOutputMessage("An Error occurred while running stored procedure: " + procName, ""
                                          , DbMessageType.Error
                                          , DbMessageImportance.Essential);
                            onOutputError(ex);
                            bolContinue = false;
                        }
                        else
                        {
                            onOutputMessage("An Error occurred while running stored procedure.  Attempting retry(" + intTries.ToString() + ")...", ""
                                          , DbMessageType.Warning
                                          , DbMessageImportance.Essential);
                        }
                    }
                }

                if (reader != null)
                {
                    xmlDocument = new XmlDocument();
                    xmlDocument.Load(reader);
                }

                returnParms = new SqlParameter[cmd.Parameters.Count];
                cmd.Parameters.CopyTo(returnParms, 0);
            }
            catch (Exception ex)
            {
                onOutputMessage("An Error occurred while running stored procedure: " + procName,
                    this.GetType().Name,
                    DbMessageType.Error,
                    DbMessageImportance.Essential);
                onOutputError(ex);

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                returnParms = null;
                bolRetVal = false;
            }
            finally
            {

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                
                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }

                if (_SQLTrans == null && _Connection != null)
                {
                    TerminateConnection();
                }
            }

            outParms = returnParms;

            return (bolRetVal);
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sp_name"></param>
        /// <param name="parms"></param>
        /// <param name="dtInsertRows"></param>
        /// <param name="RowsAffected"></param>
        /// <returns></returns>
        public bool executeBatchInsert(string sp_name, SqlParameter[] parms, DataTable dtInsertRows, out int RowsAffected) {

            SqlCommand cmd = null;
            int recordsInserted;

            bool bRetVal = false;
            bool bolContinue = true;
            int intTries = 0;
            RowsAffected = 0;
            try
            {
                cmd = new SqlCommand(sp_name, connection);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.UpdatedRowSource = UpdateRowSource.None;

                for (int i = 0; i < parms.Length; ++i)
                {
                    cmd.Parameters.Add(parms[i].ParameterName, parms[i].SqlDbType, parms[i].Size, parms[i].Value.ToString());
                    
                }

				OutputDatabaseMessage(cmd, "Executing Batch Insert: EXEC ");
                using (SqlDataAdapter adpt = new SqlDataAdapter())
                {
                    adpt.InsertCommand = cmd;
                   
                    //TODO: Make this configurable?????
                    // Specify the number of records to be Inserted/Updated in one go. Default is 1.
                    adpt.UpdateBatchSize = 2;
                    while(bolContinue) 
                    {
                        ++intTries;
                        try
                        {
                        
                            recordsInserted = adpt.Update(dtInsertRows);

                            RowsAffected = recordsInserted;

                            bolContinue = false;
                            bRetVal = true;
                        }
                        catch (Exception ex)
                        {
                            if (intTries > this.QueryRetryAttempts)
                            {
                                onOutputMessage("An Error occurred while running  executeBatchInsert: " + ex.Message, ""
                                                , DbMessageType.Error
                                                , DbMessageImportance.Essential);
                                onOutputError(ex);
                                bolContinue = false;

                                RowsAffected = 0;

                                bRetVal = false;
                            }
                            else
                            {
                                onOutputMessage("An Error occurred while executeBatchInsert.  Attempting retry(" + intTries.ToString() + ")... Error is " + ex.Message, ""
                                                , DbMessageType.Warning
                                                , DbMessageImportance.Essential);
                                System.Threading.Thread.Sleep(500);
                            }

                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                outputError(ex);
                RowsAffected = 0;
                bRetVal = false;
            }
            finally {
                
                if(cmd != null) {
                    cmd.Dispose();
                    cmd = null;
                }

                if(_SQLTrans == null && _Connection != null) {
                    TerminateConnection();
                }
            }
            return bRetVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        public bool deriveParameters(string procName, out List<SqlParameter> parms) {

            SqlCommand cmd = null;
           
            bool bolRetVal;

            try {
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = procName;
                cmd.Connection = connection;
                SqlCommandBuilder.DeriveParameters(cmd);

                List<SqlParameter> arParms = new List<SqlParameter>();
                foreach(SqlParameter parm in cmd.Parameters) {
                    arParms.Add(parm);
                }

                parms = arParms;
				OutputDatabaseMessage(cmd, "Deriving Parameters : EXEC ");
                bolRetVal = true;
            } catch(Exception) {
                parms = null;
                bolRetVal = false;
            }
            finally
            {


                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }
            }

            return(bolRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EncryptedConnectionString"></param>
        /// <returns></returns>
        private string decryptConnectionString(string EncryptedConnectionString) {

            cCrypto3DES cryp3DES;
            string[] strDBParms;
            string strNewConnectionString="";
            string strEncryptedValue;
            string strDecryptedValue;
            
            if(EncryptedConnectionString.Length > 0) {
                strDBParms = EncryptedConnectionString.Split(';');
            
                cryp3DES = new cCrypto3DES();
                for(int i=0;i<strDBParms.Length;++i) {
                    if(strDBParms[i].StartsWith("Password=")) {
                        strEncryptedValue = strDBParms[i].Substring(("Password=").Length);
                        if (!cryp3DES.Decrypt(strEncryptedValue, out strDecryptedValue)) {
                            onOutputMessage("An error occurred executing decryptConnectionString for database Password."
                                            , this.GetType().Name.ToString()
                                            , DbMessageType.Error
                                            , DbMessageImportance.Essential);

                            if (cryp3DES.LastException != null) {
                                onOutputError(cryp3DES.LastException);
                            }
                        }
                            strNewConnectionString += "Password=";
                            strNewConnectionString += strDecryptedValue + ";";

                    } else if(strDBParms[i].StartsWith("User ID=")) {
                        strEncryptedValue = strDBParms[i].Substring(("User ID=").Length);
                        if (!cryp3DES.Decrypt(strEncryptedValue, out strDecryptedValue)) {
                            onOutputMessage("An error occurred executing decryptConnectionString for database User ID."
                                            , this.GetType().Name.ToString()
                                            , DbMessageType.Error
                                            , DbMessageImportance.Essential);

                            if (cryp3DES.LastException != null) {
                                onOutputError(cryp3DES.LastException);
                            }
                        }
                            strNewConnectionString += "User ID=";
                            strNewConnectionString += strDecryptedValue + ";";

                    } else {
                        strNewConnectionString += strDBParms[i] + ";";
                    }
                }
            
                return(strNewConnectionString);
            } else {
                return("");
            }           
        }


        /// <summary>
        /// Implement IDisposable.
        /// Do not make this method virtual.
        /// A derived class should not be able to override this method.
        /// </summary>
        public void Dispose() {
            TerminateConnection();
        }

        private void TerminateConnection() {

            if(_SQLTrans != null) {
                try { 
                    _SQLTrans.Rollback(); 
                } catch {
                    // Do Nothing.
                }
                _SQLTrans = null;
            }

            try {
                if(_Connection != null && _Connection.State == ConnectionState.Open) {
                    _Connection.Close();
                }
            } catch(Exception ex) {
                if(_DebugLevel == (int)DbMessageImportance.Debug || _DebugLevel == -1) {
                    outputMessage("An exception occurred closing the database connection: " + ex.Message, this.GetType().Name.ToString(), DbMessageType.Error, DbMessageImportance.Debug);
				}
			}

            try {
                if(_Connection != null) {
                    _Connection.Dispose();
                    _Connection = null;
                }
            } catch(Exception ex) {
				if(_DebugLevel == (int) DbMessageImportance.Debug || _DebugLevel == -1) {
                    outputMessage("An exception occurred disposing the database connection: " + ex.Message, this.GetType().Name.ToString(), DbMessageType.Error, DbMessageImportance.Debug);
				}
			}
        }

		/// <summary>
		/// Outputs the database message.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="message">The message.</param>
		internal void OutputDatabaseMessage(SqlCommand command, string message) {
            if(_DebugLevel == (int)DbMessageImportance.Debug || _DebugLevel == -1) {
				try {
					StringBuilder debugOutputString = new StringBuilder();
					debugOutputString.Append(message);
					if(command.CommandType == CommandType.StoredProcedure) {
						debugOutputString.Append(command.CommandText.Trim() + " ");
						for(int i = 0; i < command.Parameters.Count; i++) {
							debugOutputString.AppendFormat("{0} = {1}"
								, command.Parameters[i].ParameterName.Trim()
								, GetParmValue(command.Parameters[i]));
							if(i < command.Parameters.Count - 1) {
								debugOutputString.Append(", ");
							}
						}
					} else {
						debugOutputString.Append(command.CommandText.Trim());
					}

					onOutputMessage(debugOutputString.ToString()
						, this.GetType().Name
                        , DbMessageType.Information
                        , DbMessageImportance.Debug);
				} catch (Exception ex) {
					onOutputError(ex);
				}
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SQL"></param>
        /// <param name="Reader"></param>
        /// <returns></returns>
        public bool CreateDataReader(string SQL,
                                     out SqlDataReader Reader) {

            bool bolRetVal = false;
            int intTries = 0;
            bool bolContinue = true;

            try {

                // CR 14209 JMC 11/02/2005 - Append DEADLOCK_PRIORITY if set.
                if(SetDeadlockPriority) {
                    SQL = "SET DEADLOCK_PRIORITY LOW;\n" + SQL;
                }

                outputMessage(
                    "Creating Dataset : " + SQL,
                    this.GetType().Name,
                    DbMessageType.Information,
                    DbMessageImportance.Debug);

                SqlDataAdapter da;
                SqlCommand cmd;

                cmd = new SqlCommand(SQL, connection);

                if(_SQLTrans != null) {
                    cmd.Transaction = _SQLTrans;
                }

                cmd.CommandText = SQL;
                da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                Reader = null;
                while(bolContinue) {
                    ++intTries;
                    try {
                        Reader = cmd.ExecuteReader();
                        bolRetVal = true;
                        bolContinue = false;
                    } catch(Exception ex) {
                        if(intTries > this.QueryRetryAttempts) {
                            outputMessage(
                                "An Error occurred while creating SqlDataReader: " + SQL + "\n\n" + ex.Message,
                                this.GetType().Name,
                                DbMessageType.Error,
                                DbMessageImportance.Essential);
                            bolContinue = false;
                        } else {
                            outputMessage(
                                "An Error occurred while creating SqlDataReader.  Attempting retry(" + intTries.ToString() + ")...",
                                this.GetType().Name,
                                DbMessageType.Warning,
                                DbMessageImportance.Verbose);
                        }
                    }
                }

                da.Dispose();
                da = null;
            } catch(Exception e) {
                onOutputError(e);

                Reader = null;
                bolRetVal = false;
            }

            return bolRetVal;
        }

		/// <summary>
		/// Gets the parameter value.
		/// </summary>
		/// <param name="parm">The parameter.</param>
		/// <returns>String version of the SQL Parameter</returns>
		internal static string GetParmValue(SqlParameter parm) {
			string value = string.Empty;
			if(parm.SqlValue == null) {
				value = "NULL";
			} else {
				switch(parm.SqlDbType) {
					case SqlDbType.BigInt:
					case SqlDbType.Bit:
					case SqlDbType.Decimal:
					case SqlDbType.Float:
					case SqlDbType.Int:
					case SqlDbType.Money:
					case SqlDbType.Real:
					case SqlDbType.SmallInt:
					case SqlDbType.SmallMoney:
						value = parm.SqlValue.ToString();
						break;
					case SqlDbType.Binary:
						value = "<Binary>";
						break;
					case SqlDbType.Image:
						value = "<Image>";
						break;
					case SqlDbType.NText:
						value = "<NText>";
						break;
					case SqlDbType.Structured:
						value = "<Structured>";
						break;
					case SqlDbType.Text:
						value = "<Text>";
						break;
					case SqlDbType.Udt:
						value = "<User Defined Type>";
						break;
					case SqlDbType.VarBinary:
						value = "<VarBinary>";
						break;
					case SqlDbType.Variant:
						value = "<variant>";
						break;
					case SqlDbType.Xml:
						value = "<Xml>";
						break;
					default:
						value = "'" + parm.SqlValue + "'";
						break;
				}
			}

			return value;
		}
    }
}
