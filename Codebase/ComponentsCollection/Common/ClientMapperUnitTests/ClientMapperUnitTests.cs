﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common.ClientMapper;

namespace ClientMapperUnitTests
{
    [TestClass]
    public class ClientMapperUnitTests
    {
        private const int DefaultBankId = 9999;

        [TestMethod]
        public void UseDefaultNoFileDefined()
        {
            var clientMapper = new ClientMapper(DefaultBankId);

            Assert.AreEqual(DefaultBankId, clientMapper.GetBankIdFromClientId(500));
        }

        [TestMethod]
        public void UseDefaultNoFileEmpty()
        {
            var clientMapper = new ClientMapper(DefaultBankId, Enumerable.Empty<ClientMapping>());

            Assert.AreEqual(DefaultBankId, clientMapper.GetBankIdFromClientId(500));
        }

        [TestMethod]
        public void FoundInFile()
        {
            IEnumerable<ClientMapping> clientMappings = new ClientMapping[]
            {
                new ClientMapping{ClientId = 1,BankId = 10},
                new ClientMapping{ClientId = 2,BankId = 20},
                new ClientMapping{ClientId = 3,BankId = 30}
            };

            var clientMapper = new ClientMapper(DefaultBankId, clientMappings);

            Assert.AreEqual(20, clientMapper.GetBankIdFromClientId(2));
        }

        [TestMethod]
        public void NotFoundInFile()
        {
            IEnumerable<ClientMapping> clientMappings = new ClientMapping[]
            {
                new ClientMapping{ClientId = 1,BankId = 10},
                new ClientMapping{ClientId = 2,BankId = 20},
                new ClientMapping{ClientId = 3,BankId = 30}
            };

            var clientMapper = new ClientMapper(DefaultBankId, clientMappings);

            Assert.AreEqual(DefaultBankId, clientMapper.GetBankIdFromClientId(100));
        }
    }
}
