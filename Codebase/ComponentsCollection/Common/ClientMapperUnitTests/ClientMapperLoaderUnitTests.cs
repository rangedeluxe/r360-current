﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common.ClientMapper;

namespace ClientMapperUnitTests
{
    [TestClass]
    public class ClientMapperLoaderUnitTests
    {
        private readonly string _testFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}";


        private void AssertLogEntries(string testMethodName, List<LogEntry> expectedLogEntries, IReadOnlyList<LogEntry> actuaLogEntries)
        {
            Assert.AreEqual(expectedLogEntries.Count, actuaLogEntries.Count, $"{testMethodName} LogEntries.Count");
            for (int i=0; i < expectedLogEntries.Count; i++)
            {
                Assert.AreEqual(expectedLogEntries[i].Type, actuaLogEntries[i].Type, $"{testMethodName} LogEntries.Type");
                Assert.AreEqual(expectedLogEntries[i].Source, actuaLogEntries[i].Source, $"{testMethodName} LogEntries.Source");
                Assert.AreEqual(expectedLogEntries[i].Message, actuaLogEntries[i].Message, $"{testMethodName} LogEntries.Message");
            }
        }

        private void CreateTestFile(string fileName)
        {
            var fileData = new StringBuilder();

            fileData.AppendLine("ClientID, BankID");
            fileData.AppendLine("1,10");
            fileData.AppendLine("2, 20");
            fileData.AppendLine("3,    30");

            File.WriteAllText(fileName, fileData.ToString());
        }

        [TestMethod]
        public void LoadFromFile_FileNotFound()
        {
            string testFileName = Path.Combine(_testFilePath, "ClientMapperTest1.txt");
            var clientMapperLoader = new ClientMapperLoader();
            var clientMappings = clientMapperLoader.LoadFromFile(testFileName);

            var expectedLogEntries = new List<LogEntry>();
            expectedLogEntries.Add(
                new LogEntry
                (
                    LogType.Warning,
                    $"Could not load {testFileName}. Using default bankId.",
                    nameof(ClientMapperLoader)
                ));

            Assert.AreEqual(0, clientMappings.Count(), "clientMappings");
            AssertLogEntries("ClientMapperLoader_LoadFromFile_FileNotFound", expectedLogEntries,clientMapperLoader.LogEntries);
        }

        [TestMethod]
        public void LoadFromFile_ValidFromFile()
        {
            string testFileName = Path.Combine(_testFilePath, "TestData.txt");
            CreateTestFile(testFileName);

            var clientMapperLoader = new ClientMapperLoader();
            var clientMappings = clientMapperLoader.LoadFromFile(testFileName);

            var expectedLogEntries = new List<LogEntry>();
            expectedLogEntries.Add(
                new LogEntry
                (
                    LogType.Information,
                    $"Successfully loaded mapping file {testFileName}.",
                    nameof(ClientMapperLoader)
                ));

            Assert.AreNotEqual(0, clientMappings.Count(), "clientMappings");
            Assert.AreEqual(3, clientMappings.Count(), "clientMappings count");
            AssertLogEntries("ClientMapperLoader_ValidFromFile", expectedLogEntries, clientMapperLoader.LogEntries);
        }

        [TestMethod]
        public void InvalidClientId()
        {
            var clientMapperLoader = new ClientMapperLoader();

            var textReader = new StringReader("ClientID, BankID\r\n1, 123\r\nA, 76\r\n500, 1");
            var clientMappings = clientMapperLoader.LoadFromReader(textReader);

            var expectedLogEntries = new List<LogEntry>();
            expectedLogEntries.Add(
                new LogEntry
                (
                    LogType.Warning,
                    "Could not convert A to a ClientID in line: A, 76.",
                    nameof(ClientMapperLoader)
                ));

            Assert.AreEqual(2, clientMappings.Count(), "clientMappings count");
            AssertLogEntries("ClientMapperLoader_ValidFromFile", expectedLogEntries, clientMapperLoader.LogEntries);
        }

        [TestMethod]
        public void InvalidBankId()
        {
            var clientMapperLoader = new ClientMapperLoader();

            var textReader = new StringReader("ClientID, BankID\r\n1, 123\r\n2, B\r\n500, 1");
            var clientMappings = clientMapperLoader.LoadFromReader(textReader);

            var expectedLogEntries = new List<LogEntry>();
            expectedLogEntries.Add(
                new LogEntry
                (
                    LogType.Warning,
                    "Could not convert  B to a BankID in line: 2, B.",
                    nameof(ClientMapperLoader)
                ));

            Assert.AreEqual(2, clientMappings.Count(), "clientMappings count");
            AssertLogEntries("ClientMapperLoader_ValidFromFile", expectedLogEntries, clientMapperLoader.LogEntries);
        }

        [TestMethod]
        public void MissingComma()
        {
            var clientMapperLoader = new ClientMapperLoader();

            var textReader = new StringReader("ClientID, BankID\r\n1, 123\r\n2 76\r\n500, 1");
            var clientMappings = clientMapperLoader.LoadFromReader(textReader);

            var expectedLogEntries = new List<LogEntry>();
            expectedLogEntries.Add(
                new LogEntry
                (
                    LogType.Warning,
                    "Line is malformed, must be ClientID, BankID. Line: 2 76.",
                    nameof(ClientMapperLoader)
                ));

            Assert.AreEqual(2, clientMappings.Count(), "clientMappings count");
            AssertLogEntries("ClientMapperLoader_ValidFromFile", expectedLogEntries, clientMapperLoader.LogEntries);
        }

        [TestMethod]
        public void MissingHeader()
        {
            var clientMapperLoader = new ClientMapperLoader();

            var textReader = new StringReader("1, 10\r\n2, 20\r\n3,   30");
            var clientMappings = clientMapperLoader.LoadFromReader(textReader);

            Assert.AreEqual(3, clientMappings.Count(), "clientMappings count");
            Assert.AreEqual(1, clientMappings.ToList()[0].ClientId, "ClientID");
            Assert.AreEqual(10, clientMappings.ToList()[0].BankId, "BankID");
        }
    }
}
