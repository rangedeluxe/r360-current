﻿using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Rx360.ServiceApi.GetWebInstallation
{
    public interface IWebConfigurationReader
    {
        [ItemNotNull]
        Task<WebConfiguration> GetWebConfigurationAsync();
    }
}