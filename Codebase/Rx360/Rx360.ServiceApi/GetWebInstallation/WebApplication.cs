﻿using System;
using JetBrains.Annotations;

namespace Rx360.ServiceApi.GetWebInstallation
{
    public class WebApplication
    {
        public WebApplication([NotNull] string virtualPath, [NotNull] string physicalPath)
        {
            if (virtualPath == null)
                throw new ArgumentNullException(nameof(virtualPath));
            if (physicalPath == null)
                throw new ArgumentNullException(nameof(physicalPath));

            VirtualPath = virtualPath;
            PhysicalPath = physicalPath;
        }

        [NotNull]
        public string PhysicalPath { get; }
        [NotNull]
        public string VirtualPath { get; }
    }
}