﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Rx360.ServiceApi.GetWebInstallation
{
    public interface IFileSystem
    {
        /// <summary>
        /// Gets the latest LastWriteTime of any binary (EXE or DLL) file in the given directory.
        /// </summary>
        /// <param name="directory">Directory path.</param>
        /// <returns>
        /// The latest LastWriteTime of any binary in the directory, or null if no binaries were found.
        /// </returns>
        Task<DateTimeOffset?> GetMaxBinaryLastWriteTimeAsync([NotNull] string directory);
    }
}