﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Rx360.Contracts;

namespace Rx360.ServiceApi.GetWebInstallation
{
    /// <summary>
    /// Translates IIS configuration and filesystem data into a list of web applications hosted by IIS.
    /// </summary>
    public class GetWebInstallationCommand : ICommand<WebInstallation>
    {
        public GetWebInstallationCommand([NotNull] IWebConfigurationReader webConfigurationReader,
            [NotNull] IFileSystem fileSystem)
        {
            if (webConfigurationReader == null)
                throw new ArgumentNullException(nameof(webConfigurationReader));
            if (fileSystem == null)
                throw new ArgumentNullException(nameof(fileSystem));

            WebConfigurationReader = webConfigurationReader;
            FileSystem = fileSystem;
        }

        private IFileSystem FileSystem { get; }
        private IWebConfigurationReader WebConfigurationReader { get; }

        [ItemNotNull]
        public async Task<WebInstallation> ExecuteAsync()
        {
            var config = await WebConfigurationReader.GetWebConfigurationAsync();
            // If it errored, return the error code
            if (config.Status != WebInstallationStatus.Success)
                return new WebInstallation {Status = config.Status};

            var webApps = (
                from iisApplication in config.Applications
                let binPath = Path.Combine(iisApplication.PhysicalPath, "bin")
                select new
                {
                    iisApplication.VirtualPath,
                    LastWriteTimeTask = FileSystem.GetMaxBinaryLastWriteTimeAsync(binPath),
                }).ToList();
            await Task.WhenAll(webApps.Select(webApp => webApp.LastWriteTimeTask));

            return new WebInstallation
            {
                Applications = webApps.Select(
                    webApp => new WebApplicationInstallation
                    {
                        VirtualPath = webApp.VirtualPath,
                        LastModified = webApp.LastWriteTimeTask.Result,
                    }).ToList(),
            };
        }
    }
}