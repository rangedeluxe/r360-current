﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Rx360.Contracts;

namespace Rx360.ServiceApi.GetWebInstallation
{
    public class WebConfiguration
    {
        public WebConfiguration([NotNull, ItemNotNull] IEnumerable<WebApplication> applications)
        {
            if (applications == null)
                throw new ArgumentNullException(nameof(applications));

            Applications = applications.ToList();
            Status = WebInstallationStatus.Success;

            if (Applications.Any(app => app == null))
                throw new InvalidOperationException($"Unexpected null value passed to {nameof(WebConfiguration)}");
        }
        public WebConfiguration(WebInstallationStatus status)
        {
            Status = status;
            Applications = new WebApplication[] {};
        }

        [NotNull, ItemNotNull]
        public IReadOnlyList<WebApplication> Applications { get; }
        public WebInstallationStatus Status { get; }
    }
}