﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.Web.Administration;
using Rx360.Contracts;

namespace Rx360.ServiceApi.GetWebInstallation
{
    public class IisWebConfigurationReader : IWebConfigurationReader
    {
        public async Task<WebConfiguration> GetWebConfigurationAsync()
        {
            return await Task.Run(() => GetWebConfigurationWithErrorHandling());
        }
        [NotNull]
        private WebConfiguration GetWebConfigurationWithErrorHandling()
        {
            try
            {
                return GetWebConfigurationInternal();
            }
            catch (COMException)
            {
                return new WebConfiguration(WebInstallationStatus.IisNotInstalled);
            }
            catch (UnauthorizedAccessException)
            {
                return new WebConfiguration(WebInstallationStatus.NoPermissionToQueryIis);
            }
        }
        [NotNull]
        private WebConfiguration GetWebConfigurationInternal()
        {
            using (var manager = new ServerManager())
            {
                return GetWebConfigurationFromManager(manager);
            }
        }
        [NotNull]
        private WebConfiguration GetWebConfigurationFromManager(ServerManager manager)
        {
            var managerSite = manager.Sites["R360"];
            if (managerSite == null)
                return new WebConfiguration(WebInstallationStatus.R360SiteNotFound);

            var applications = new List<WebApplication>();
            foreach (var managerApplication in managerSite.Applications)
            {
                var virtualPath = managerApplication.VirtualDirectories.FirstOrDefault()?.Path;
                var physicalPath = managerApplication.Path;
                var iisApplication = new WebApplication(virtualPath ?? "", physicalPath);
                applications.Add(iisApplication);
            }
            return new WebConfiguration(applications);
        }
    }
}