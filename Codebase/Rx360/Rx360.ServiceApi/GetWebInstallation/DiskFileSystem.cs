﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Rx360.ServiceApi.GetWebInstallation
{
    public class DiskFileSystem : IFileSystem
    {
        public async Task<DateTimeOffset?> GetMaxBinaryLastWriteTimeAsync(string directory)
        {
            // We require an absolute path. Bail if the path is relative or invalid.
            try
            {
                if (!Path.IsPathRooted(directory))
                    return null;
            }
            catch (ArgumentException)
            {
                // Invalid path
                return null;
            }

            return await Task.Run(() =>
            {
                try
                {
                    var files = new DirectoryInfo(directory).GetFiles("*.dll")
                        .Concat(new DirectoryInfo(directory).GetFiles("*.exe"))
                        .ToList();

                    if (!files.Any())
                        return (DateTimeOffset?) null;

                    return files.Max(f => (DateTimeOffset) f.LastWriteTime);
                }
                catch (IOException)
                {
                    return null;
                }
            });
        }
    }
}