﻿using System.Threading.Tasks;

namespace Rx360.ServiceApi
{
    public interface ICommand<TResult>
    {
        Task<TResult> ExecuteAsync();
    }
}