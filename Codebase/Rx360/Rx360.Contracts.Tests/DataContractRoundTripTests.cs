﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Rx360.Contracts
{
    /// <summary>
    /// I've run into weird cases of DataContractSerializer throwing strange errors when
    /// you try to serialize some types over WCF, so these tests are here to prove that
    /// all our DTO types can really be round-tripped through DataContractSerializer.
    /// </summary>
    [TestClass]
    public class DataContractRoundTripTests
    {
        private IEnumerable<Type> GetInterfaceTypeAndAllBaseInterfaces(Type interfaceType)
        {
            if (interfaceType == null)
                return Enumerable.Empty<Type>();

            return new[] {interfaceType}.Concat(
                from baseInterface in interfaceType.GetInterfaces()
                from ancestorInterface in GetInterfaceTypeAndAllBaseInterfaces(baseInterface)
                select ancestorInterface
                ).Distinct();
        }
        private IEnumerable<Type> UnpackGenericTypes(Type type)
        {
            var singleParameterTypesToUnpack = new[]
            {
                typeof(IList<>),
                typeof(Task<>),
            };

            if (type.IsGenericType && singleParameterTypesToUnpack.Contains(type.GetGenericTypeDefinition()))
            {
                var innerType = type.GetGenericArguments().Single();
                foreach (var interestingType in UnpackGenericTypes(innerType))
                    yield return interestingType;
            }
            else
            {
                yield return type;
            }
        }
        private T RoundTrip<T>(T originalValue)
        {
            using (var memoryStream = new MemoryStream())
            {
                var dataContractSerializer = new DataContractSerializer(typeof(T));
                dataContractSerializer.WriteObject(memoryStream, originalValue);

                memoryStream.Position = 0;
                return (T) dataContractSerializer.ReadObject(memoryStream);
            }
        }

        [TestMethod]
        public void WeHaveTestsForAllTypesUsedByServiceContracts()
        {
            // Any [ServiceContract] interface from the Rx360.Contracts assembly
            var seedServiceContract = typeof(ISetupQuery);
            var serviceContractAssembly = seedServiceContract.Assembly;
            var serviceContractTypes = (
                from type in serviceContractAssembly.GetExportedTypes()
                where type.GetCustomAttributes(typeof(ServiceContractAttribute), inherit: false).Any()
                select type
            ).ToList();
            // Sanity check
            Assert.IsTrue(serviceContractTypes.Contains(seedServiceContract),
                $"Expected to find {seedServiceContract.Name} as a service contract type");

            // Get the complete list of this interface and all its base interfaces - since when we
            // go to ask for the methods on an interface, .NET will *not* automatically include
            // methods from base interfaces (which is different from what it does for classes).
            var interfaceMethods = (
                from serviceContractType in serviceContractTypes
                from interfaceType in GetInterfaceTypeAndAllBaseInterfaces(serviceContractType)
                from method in interfaceType.GetMethods()
                select method
            ).ToList();
            var returnTypes =
                from method in interfaceMethods
                where method.ReturnType != typeof(void)
                select method.ReturnType;
            var parameterTypes =
                from method in interfaceMethods
                from parameter in method.GetParameters()
                select parameter.ParameterType;
            var allTransferTypes = returnTypes.Concat(parameterTypes).Distinct();

            // Transform the list by cracking open any instances of Task<T>, IList<T>, etc.
            var innerTypes =
                from outerType in allTransferTypes
                from type in UnpackGenericTypes(outerType)
                select type;
            // Filter out uninteresting types like string.
            var interestingTypes =
                from type in innerTypes
                where type != typeof(string)
                select type;

            var ourTestClass = typeof(DataContractRoundTripTests);
            var ourTestMethodNames = ourTestClass
                .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Select(m => m.Name)
                .ToList();
            var errors = new List<string>();
            foreach (var type in interestingTypes)
            {
                var expectedTestMethodName = $"RoundTrip_{type.Name}";
                var haveTestForType = ourTestMethodNames.Any(
                    name => name == expectedTestMethodName || name.StartsWith(expectedTestMethodName + "_"));
                if (!haveTestForType)
                {
                    errors.Add(
                        $"Type '{type.Name}' is sent over WCF but has no DataContract round-trip test; " +
                        $"please add test method {ourTestClass.Name}.{expectedTestMethodName}.");
                }
            }
            if (errors.Any())
                Assert.Fail(string.Join("\r\n", errors));
        }
        [TestMethod]
        public void RoundTrip_WindowsServiceStatus()
        {
            var lastModified = new DateTimeOffset(2016, 1, 1, 12, 34, 56, TimeSpan.Zero);
            var original = new WindowsServiceStatus
            {
                LastModified = lastModified,
                ExeName = "MyExeName",
                DisplayName = "MyDisplayName",
                DllName = "MyDllName",
            };

            var result = RoundTrip(original);

            Assert.AreEqual(lastModified, result.LastModified, "LastModified");
            Assert.AreEqual("MyExeName", result.ExeName, "ExeName");
            Assert.AreEqual("MyDisplayName", result.DisplayName, "DisplayName");
            Assert.AreEqual("MyDllName", result.DllName, "DllName");
        }
        [TestMethod]
        public void RoundTrip_WebApplicationInstallation()
        {
            var lastWriteTime = new DateTimeOffset(2016, 1, 1, 12, 34, 56, TimeSpan.Zero);
            var original = new WebApplicationInstallation
            {
                VirtualPath = "/VirtualPath",
                LastModified = lastWriteTime,
            };

            var result = RoundTrip(original);

            Assert.AreEqual("/VirtualPath", result.VirtualPath, "VirtualPath");
            Assert.AreEqual(lastWriteTime, result.LastModified, "LastModified");
        }
        [TestMethod]
        public void RoundTrip_WebInstallation()
        {
            var original = new WebInstallation
            {
                Applications = new[]
                {
                    new WebApplicationInstallation(),
                    new WebApplicationInstallation(),
                },
                Status = WebInstallationStatus.NoPermissionToQueryIis,
            };

            var result = RoundTrip(original);

            Assert.AreEqual(WebInstallationStatus.NoPermissionToQueryIis, result.Status, "Status");
            Assert.AreEqual(2, result.Applications.Count, "Applications.Count");
        }
    }
}
