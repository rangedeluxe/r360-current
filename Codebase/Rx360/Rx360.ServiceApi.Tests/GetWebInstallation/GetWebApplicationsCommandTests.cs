﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rx360.Contracts;
using Rx360.ServiceApi.GetWebInstallation.Fakes;

namespace Rx360.ServiceApi.GetWebInstallation
{
    [TestClass]
    public class GetWebApplicationsCommandTests
    {
        private IFileSystem CreateFileSystem()
        {
            return new StubIFileSystem
            {
                GetMaxBinaryLastWriteTimeAsyncString = directory => Task.FromResult((DateTimeOffset?) null),
            };
        }
        private IWebConfigurationReader CreateWebConfigurationReader(Func<WebConfiguration> getWebConfiguration)
        {
            return new StubIWebConfigurationReader
            {
                GetWebConfigurationAsync = () => Task.FromResult(getWebConfiguration()),
            };
        }
        private GetWebInstallationCommand CreateCommand(IWebConfigurationReader webConfigurationReader,
            IFileSystem fileSystem = null)
        {
            return new GetWebInstallationCommand(webConfigurationReader, fileSystem ?? CreateFileSystem());
        }

        [TestMethod]
        public async Task IisNotInstalled()
        {
            var webConfigurationReader =
                CreateWebConfigurationReader(() => new WebConfiguration(WebInstallationStatus.IisNotInstalled));

            var command = CreateCommand(webConfigurationReader);
            var response = await command.ExecuteAsync();

            Assert.AreEqual(WebInstallationStatus.IisNotInstalled, response.Status, "Status");
            Assert.AreEqual(0, response.Applications.Count, "Count");
        }
        [TestMethod]
        public async Task NoPermissionToQueryIis()
        {
            var webConfigurationReader =
                CreateWebConfigurationReader(() => new WebConfiguration(WebInstallationStatus.NoPermissionToQueryIis));

            var command = CreateCommand(webConfigurationReader);
            var response = await command.ExecuteAsync();

            Assert.AreEqual(WebInstallationStatus.NoPermissionToQueryIis, response.Status, "Status");
            Assert.AreEqual(0, response.Applications.Count, "Count");
        }
        [TestMethod]
        public async Task SiteNotFound()
        {
            var webConfigurationReader =
                CreateWebConfigurationReader(() => new WebConfiguration(WebInstallationStatus.R360SiteNotFound));

            var command = CreateCommand(webConfigurationReader);
            var response = await command.ExecuteAsync();

            Assert.AreEqual(WebInstallationStatus.R360SiteNotFound, response.Status, "Status");
            Assert.AreEqual(0, response.Applications.Count, "Count");
        }
        [TestMethod]
        public async Task NoApplications_ReturnsSuccessAndEmptyList()
        {
            var webConfigurationReader =
                CreateWebConfigurationReader(() => new WebConfiguration(new WebApplication[] {}));

            var command = CreateCommand(webConfigurationReader);
            var response = await command.ExecuteAsync();

            Assert.AreEqual(WebInstallationStatus.Success, response.Status, "Status");
            Assert.AreEqual(0, response.Applications.Count, "Count");
        }
        [TestMethod]
        public async Task WithApplications_ReturnsVirtualPaths()
        {
            var webConfigurationReader = CreateWebConfigurationReader(() => new WebConfiguration(new[]
            {
                new WebApplication("/Foo", @"C:\Foo"),
                new WebApplication("/Bar", @"C:\Bar"),
            }));

            var command = CreateCommand(webConfigurationReader);
            var response = await command.ExecuteAsync();

            Assert.AreEqual(WebInstallationStatus.Success, response.Status, "Status");
            Assert.AreEqual(2, response.Applications.Count, "Count");
            Assert.AreEqual("/Foo", response.Applications[0].VirtualPath, "VirtualPath 0");
            Assert.AreEqual("/Bar", response.Applications[1].VirtualPath, "VirtualPath 1");
        }
        [TestMethod]
        public async Task WithApplications_CalculatesLastWriteTimes()
        {
            var webConfigurationReader = CreateWebConfigurationReader(() => new WebConfiguration(new[]
            {
                new WebApplication("/Foo", @"C:\Foo"),
                new WebApplication("/Bar", @"C:\Bar"),
            }));
            DateTimeOffset? fooLastWriteTime = new DateTimeOffset(2016, 1, 1, 12, 0, 0, TimeSpan.Zero);
            DateTimeOffset? barLastWriteTime = new DateTimeOffset(2015, 1, 1, 12, 0, 0, TimeSpan.Zero);
            var fileSystem = new StubIFileSystem
            {
                GetMaxBinaryLastWriteTimeAsyncString = directory =>
                {
                    if (directory.Equals(@"C:\Foo\bin", StringComparison.InvariantCultureIgnoreCase))
                        return Task.FromResult(fooLastWriteTime);
                    if (directory.Equals(@"C:\Bar\bin", StringComparison.InvariantCultureIgnoreCase))
                        return Task.FromResult(barLastWriteTime);
                    throw new InvalidOperationException($"Unexpected directory {directory}");
                }
            };

            var command = CreateCommand(webConfigurationReader, fileSystem);
            var response = await command.ExecuteAsync();

            Assert.AreEqual(2, response.Applications.Count, "Count");
            Assert.AreEqual(fooLastWriteTime, response.Applications[0].LastModified, "LastModified 0");
            Assert.AreEqual(barLastWriteTime, response.Applications[1].LastModified, "LastModified 1");
        }
    }
}