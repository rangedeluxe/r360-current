﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Rx360.ServiceApi.GetWebInstallation
{
    [TestClass]
    public class DiskFileSystemTests
    {
        [TestInitialize]
        public void SetUp()
        {
            OriginalCurrentDirectory = Directory.GetCurrentDirectory();
            TestDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        }
        [TestCleanup]
        public void TearDown()
        {
            // Reset the current directory first, since we might be in TestDirectory
            // and need to change out before we delete it
            if (!string.IsNullOrEmpty(OriginalCurrentDirectory))
                Directory.SetCurrentDirectory(OriginalCurrentDirectory);

            if (Directory.Exists(TestDirectory))
            {
                Directory.Delete(TestDirectory, recursive: true);
            }
        }

        private string OriginalCurrentDirectory { get; set; }
        private string TestDirectory { get; set; }

        private void CreateFileInTestDirectoryWithDate(string fileName, int year, int month, int day)
        {
            var path = Path.Combine(TestDirectory, fileName);
            File.WriteAllBytes(path, new byte[] {});
            new FileInfo(path).LastWriteTime = new DateTime(year, month, day);
        }

        [TestMethod]
        public async Task DirectoryDoesNotExist_ReturnsNull()
        {
            var fileSystem = new DiskFileSystem();
            Assert.AreEqual(null, await fileSystem.GetMaxBinaryLastWriteTimeAsync(TestDirectory));
        }
        [TestMethod]
        public async Task DirectoryIsEmpty_ReturnsNull()
        {
            Directory.CreateDirectory(TestDirectory);
            var fileSystem = new DiskFileSystem();
            Assert.AreEqual(null, await fileSystem.GetMaxBinaryLastWriteTimeAsync(TestDirectory));
        }
        [TestMethod]
        public async Task DirectoryNameIsBlank_ReturnsNull()
        {
            Directory.CreateDirectory(TestDirectory);
            CreateFileInTestDirectoryWithDate("abc.dll", 2016, 1, 1);
            Directory.SetCurrentDirectory(TestDirectory);

            var fileSystem = new DiskFileSystem();
            Assert.AreEqual(null, await fileSystem.GetMaxBinaryLastWriteTimeAsync(""), "Empty");
        }
        [TestMethod]
        public async Task DirectoryNameIsWhitespace_ReturnsNull()
        {
            Directory.CreateDirectory(TestDirectory);
            CreateFileInTestDirectoryWithDate("abc.dll", 2016, 1, 1);
            Directory.SetCurrentDirectory(TestDirectory);

            var fileSystem = new DiskFileSystem();
            Assert.AreEqual(null, await fileSystem.GetMaxBinaryLastWriteTimeAsync(" "), "Whitespace");
        }
        [TestMethod]
        public async Task DirectoryNameHasInvalidCharacter_ReturnsNull()
        {
            Directory.CreateDirectory(TestDirectory);
            CreateFileInTestDirectoryWithDate("abc.dll", 2016, 1, 1);
            Directory.SetCurrentDirectory(TestDirectory);

            var fileSystem = new DiskFileSystem();
            Assert.AreEqual(null, await fileSystem.GetMaxBinaryLastWriteTimeAsync("|"), "Whitespace");
        }
        [TestMethod]
        public async Task RequiresAbsolutePath_ReturnsNullOtherwise()
        {
            Directory.CreateDirectory(TestDirectory);
            CreateFileInTestDirectoryWithDate("abc.dll", 2016, 1, 1);
            Directory.SetCurrentDirectory(TestDirectory);

            var fileSystem = new DiskFileSystem();
            Assert.AreEqual(null, await fileSystem.GetMaxBinaryLastWriteTimeAsync("."), "Relative path");
        }
        [TestMethod]
        public async Task DllIsNewest()
        {
            Directory.CreateDirectory(TestDirectory);
            CreateFileInTestDirectoryWithDate("abc.dll", 2016, 1, 15);
            CreateFileInTestDirectoryWithDate("def.dll", 2016, 7, 15);
            CreateFileInTestDirectoryWithDate("ghi.dll", 2015, 6, 30);
            CreateFileInTestDirectoryWithDate("myapp.exe", 2016, 2, 29);

            var fileSystem = new DiskFileSystem();
            Assert.AreEqual(
                new DateTimeOffset(2016, 7, 15, 0, 0, 0, TimeZoneInfo.Local.GetUtcOffset(new DateTime(2016, 7, 15))),
                await fileSystem.GetMaxBinaryLastWriteTimeAsync(TestDirectory));
        }
        [TestMethod]
        public async Task ExeIsNewest()
        {
            Directory.CreateDirectory(TestDirectory);
            CreateFileInTestDirectoryWithDate("app.exe", 2016, 1, 15);
            CreateFileInTestDirectoryWithDate("GreatPumpkin.exe", 2016, 10, 31);
            CreateFileInTestDirectoryWithDate("other.exe", 2015, 6, 30);
            CreateFileInTestDirectoryWithDate("helper.dll", 2016, 2, 29);

            var fileSystem = new DiskFileSystem();
            Assert.AreEqual(
                new DateTimeOffset(2016, 10, 31, 0, 0, 0, TimeZoneInfo.Local.GetUtcOffset(new DateTime(2016, 10, 31))),
                await fileSystem.GetMaxBinaryLastWriteTimeAsync(TestDirectory));
        }
        [TestMethod]
        public async Task IgnoresOtherExtensions()
        {
            Directory.CreateDirectory(TestDirectory);
            CreateFileInTestDirectoryWithDate("app.exe", 2015, 1, 15);
            CreateFileInTestDirectoryWithDate("app.exe.config", 2016, 9, 15);
            CreateFileInTestDirectoryWithDate("app.exe.config.oem", 2016, 10, 31);
            CreateFileInTestDirectoryWithDate("readme.txt", 2016, 11, 15);

            var fileSystem = new DiskFileSystem();
            Assert.AreEqual(
                new DateTimeOffset(2015, 1, 15, 0, 0, 0, TimeZoneInfo.Local.GetUtcOffset(new DateTime(2015, 1, 15))),
                await fileSystem.GetMaxBinaryLastWriteTimeAsync(TestDirectory));
        }
    }
}