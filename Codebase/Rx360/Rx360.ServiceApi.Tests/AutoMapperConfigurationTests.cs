﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Rx360.ServiceApi
{
    [TestClass]
    public class AutoMapperConfigurationTests
    {
        [TestMethod]
        public void AllDestinationPropertiesAreMapped()
        {
            AutoMapperConfiguration.Initialize();
            Mapper.AssertConfigurationIsValid();
        }
        [TestMethod]
        public void AllSourceEnumValuesHaveDestinations()
        {
            var enumMappings = (
                from typeMap in Mapper.Configuration.GetAllTypeMaps()
                from propertyMap in typeMap.GetPropertyMaps()
                where propertyMap.DestinationPropertyType.IsEnum
                select new {propertyMap.SourceType, DestinationType = propertyMap.DestinationPropertyType}
            ).Distinct();

            var errors = (
                from enumMapping in enumMappings
                orderby enumMapping.SourceType.Name.ToLowerInvariant()
                let destinationEnumNames = new HashSet<string>(Enum.GetNames(enumMapping.DestinationType))
                from sourceEnumName in Enum.GetNames(enumMapping.SourceType)
                where !destinationEnumNames.Contains(sourceEnumName)
                select $"{enumMapping.SourceType.FullName}.{sourceEnumName} " +
                       $"has no corresponding value in {enumMapping.DestinationType.FullName}"
            ).ToList();

            if (errors.Any())
            {
                Assert.Fail(string.Join("\r\n", errors));
            }
        }
    }
}