﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Rx360Service.Commands
{
    public static class CommandLineSplitter
    {
        [DllImport("shell32.dll", SetLastError = true)]
        private static extern IntPtr CommandLineToArgvW(
            [MarshalAs(UnmanagedType.LPWStr)] string lpCmdLine, out int pNumArgs);

        /// <summary>
        /// Splits a command-line string into arguments, using the same Windows API
        /// function that .NET uses at process startup.
        /// </summary>
        public static IReadOnlyList<string> Split(string commandLine)
        {
            // Source: http://stackoverflow.com/a/749653/87399
            int argc;
            var argv = CommandLineToArgvW(commandLine, out argc);
            if (argv == IntPtr.Zero)
                throw new System.ComponentModel.Win32Exception();
            try
            {
                var args = new string[argc];
                for (var i = 0; i < args.Length; i++)
                {
                    var p = Marshal.ReadIntPtr(argv, i * IntPtr.Size);
                    args[i] = Marshal.PtrToStringUni(p);
                }

                return args;
            }
            finally
            {
                Marshal.FreeHGlobal(argv);
            }
        }
    }
}