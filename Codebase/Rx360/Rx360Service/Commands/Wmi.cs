﻿using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Threading.Tasks;

namespace Rx360Service.Commands
{
    public static class Wmi
    {
        public static async Task<IReadOnlyList<ManagementObject>> RunQueryAsync(string queryText)
        {
            return await Task.Run(() =>
            {
                var scope = new ManagementScope();
                scope.Connect();
                var objectQuery = new ObjectQuery(queryText);
                using (var searcher = new ManagementObjectSearcher(scope, objectQuery))
                {
                    return searcher.Get().Cast<ManagementObject>().ToList();
                }
            });
        }
    }
}