﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Rx360.Contracts;
using Rx360.ServiceApi;

namespace Rx360Service.Commands
{
    public class GetWindowsServicesCommand : ICommand<IList<WindowsServiceStatus>>
    {
        public class ServiceBinaryPaths
        {
            public ServiceBinaryPaths(string exePath, string dllPath)
            {
                if (exePath == null)
                    throw new ArgumentNullException(nameof(exePath));
                if (dllPath == null)
                    throw new ArgumentNullException(nameof(dllPath));

                ExePath = exePath;
                DllPath = dllPath;
            }

            public string ExePath { get; }
            public string DllPath { get; }
        }

        private class ServiceBinary
        {
            public ServiceBinary(string fileName = "", DateTimeOffset? lastModified = null)
            {
                if (fileName == null)
                    throw new ArgumentNullException(nameof(fileName));

                FileName = fileName;
                LastModified = lastModified;
            }

            public string FileName { get; }
            public DateTimeOffset? LastModified { get; }
        }

        private WindowsServiceStatus BuildStatusObject(ManagementObject item)
        {
            var displayName = item["DisplayName"] as string ?? "";
            var commandLine = item["PathName"] as string ?? "";
            var binaryPaths = GetServiceBinaryPaths(commandLine);

            var exe = GetServiceBinaryInfo(binaryPaths.ExePath);
            var dll = GetServiceBinaryInfo(binaryPaths.DllPath);

            var programLastModified = new[] {exe.LastModified, dll.LastModified}
                .FirstOrDefault(lastModified => lastModified.HasValue);

            return new WindowsServiceStatus
            {
                DisplayName = displayName,
                ExeName = exe.FileName,
                DllName = dll.FileName,
                LastModified = programLastModified ?? default(DateTimeOffset),
            };
        }
        public async Task<IList<WindowsServiceStatus>> ExecuteAsync()
        {
            // Use WMI rather than ServiceController, because WMI can give us the command line
            const string queryText =
                "SELECT DisplayName, PathName FROM Win32_Service " +
                "WHERE " +
                "  (DisplayName LIKE 'WFS %') OR " +
                "  (DisplayName = 'Enterprise Library Semantic Logging Service')";
            var wmiResults = await Wmi.RunQueryAsync(queryText);

            return wmiResults
                .Select(BuildStatusObject)
                .OrderBy(service => service.DisplayName, StringComparer.InvariantCultureIgnoreCase)
                .ToList();
        }
        private ServiceBinary GetServiceBinaryInfo(string path)
        {
            if (string.IsNullOrEmpty(path))
                return new ServiceBinary();

            try
            {
                var fileName = Path.GetFileName(path);
                var fileInfo = new FileInfo(path);
                return fileInfo.Exists
                    ? new ServiceBinary(fileName, fileInfo.LastWriteTime)
                    : new ServiceBinary(fileName);
            }
            catch (Exception)
            {
                return new ServiceBinary();
            }
        }
        public static ServiceBinaryPaths GetServiceBinaryPaths(string commandLine)
        {
            var args = CommandLineSplitter.Split(commandLine);
            var exePath = args.FirstOrDefault() ?? "";

            var dllPath = "";
            if (Regex.IsMatch(exePath, @"\bWCFServiceHost.exe\b", RegexOptions.IgnoreCase))
            {
                var dllArg = args.FirstOrDefault(
                    arg => arg.StartsWith("-d:", StringComparison.InvariantCultureIgnoreCase));
                if (!string.IsNullOrEmpty(dllArg))
                {
                    dllPath = dllArg.Substring(3);
                }
            }

            return new ServiceBinaryPaths(exePath, dllPath);
        }
    }
}