﻿using Rx360.Contracts;
using Topshelf;

namespace Rx360Service
{
    public static class Program
    {
        public static void Main()
        {
            HostFactory.Run(hostConfigurator =>
            {
                hostConfigurator.Service<Rx360Runner>(serviceConfigurator =>
                {
                    serviceConfigurator.ConstructUsing(() => new Rx360Runner());
                    serviceConfigurator.WhenStarted(runner => runner.Start());
                    serviceConfigurator.WhenStopped(runner => runner.Stop());
                });

                hostConfigurator.RunAsLocalSystem();
                hostConfigurator.StartAutomaticallyDelayed();

                hostConfigurator.SetServiceName(Rx360Constants.ServiceName);
                hostConfigurator.SetDisplayName(Rx360Constants.ServiceDisplayName);
                hostConfigurator.SetDescription(Rx360Constants.ServiceDescription);
            });
        }
    }
}