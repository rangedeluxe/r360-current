﻿using System;
using System.ServiceModel;
using Rx360Service.Services;

namespace Rx360Service
{
    public class Rx360Runner
    {
        private ServiceHost _serviceHost;

        public void Start()
        {
            Console.WriteLine("Starting");
            _serviceHost = new ServiceHost(typeof(SetupQueryService));
            _serviceHost.Open();
            Console.WriteLine("Started");
        }
        public void Stop()
        {
            Console.WriteLine("Stopping");
            if (_serviceHost != null)
            {
                _serviceHost.Close();
                _serviceHost = null;
            }
            Console.WriteLine("Stopped");
        }
    }
}