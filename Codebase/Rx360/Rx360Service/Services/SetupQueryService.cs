﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rx360.Contracts;
using Rx360.ServiceApi;
using Rx360.ServiceApi.GetWebInstallation;
using Rx360Service.Commands;

namespace Rx360Service.Services
{
    public class SetupQueryService : ISetupQuery
    {
        private async Task<TResult> ExecuteAsync<TResult>(ICommand<TResult> command)
        {
            try
            {
                return await command.ExecuteAsync();
            }
            catch (Exception ex)
            {
                //TODO: Wouldn't it be nice if we had logging?
                Console.WriteLine(ex);
                throw;
            }
        }

        public Task<string> Ping()
        {
            return Task.FromResult("Pong");
        }
        public async Task<WebInstallation> GetWebInstallation()
        {
            return await ExecuteAsync(
                new GetWebInstallationCommand(new IisWebConfigurationReader(), new DiskFileSystem()));
        }
        public async Task<IList<WindowsServiceStatus>> GetWindowsServices()
        {
            return await ExecuteAsync(new GetWindowsServicesCommand());
        }
    }
}