﻿using System;
using System.Runtime.Serialization;

namespace Rx360.Contracts
{
    [DataContract]
    public class WebApplicationInstallation : IHasLastModified
    {
        [DataMember]
        public DateTimeOffset? LastModified { get; set; }
        [DataMember]
        public string VirtualPath { get; set; }
    }
}