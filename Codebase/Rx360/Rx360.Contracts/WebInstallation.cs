﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace Rx360.Contracts
{
    [DataContract]
    public class WebInstallation
    {
        [DataMember, NotNull]
        public IList<WebApplicationInstallation> Applications { get; set; } = new WebApplicationInstallation[] {};
        [DataMember]
        public WebInstallationStatus Status { get; set; }
    }
}