﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
// ReSharper disable ConsiderUsingAsyncSuffix

namespace Rx360.Contracts
{
    [ServiceContract]
    public interface ISetupQuery
    {
        [OperationContract]
        Task<string> Ping();

        [OperationContract]
        Task<WebInstallation> GetWebInstallation();

        [OperationContract]
        Task<IList<WindowsServiceStatus>> GetWindowsServices();
    }
}