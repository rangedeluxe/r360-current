﻿namespace Rx360.Contracts
{
    public enum WebInstallationStatus
    {
        Success,
        IisNotInstalled,
        NoPermissionToQueryIis,
        R360SiteNotFound,
    }
}