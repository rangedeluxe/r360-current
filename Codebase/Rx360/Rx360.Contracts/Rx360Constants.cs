﻿namespace Rx360.Contracts
{
    public static class Rx360Constants
    {
        public const string ServiceName = "Rx360";
        public const string ServiceDisplayName = "WFS Receivables360 Online Health Monitoring Service";
        public const string ServiceDescription =
            "Monitors the configuration and performance of the Receivables360 Online application.";
    }
}