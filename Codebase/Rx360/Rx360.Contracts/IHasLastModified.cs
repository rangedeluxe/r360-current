﻿using System;

namespace Rx360.Contracts
{
    public interface IHasLastModified
    {
        DateTimeOffset? LastModified { get; set; }
    }
}