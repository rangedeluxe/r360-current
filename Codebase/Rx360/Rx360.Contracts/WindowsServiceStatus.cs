﻿using System;
using System.Runtime.Serialization;

namespace Rx360.Contracts
{
    [DataContract]
    public class WindowsServiceStatus : IHasLastModified
    {
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string ExeName { get; set; }
        [DataMember]
        public string DllName { get; set; }
        [DataMember]
        public DateTimeOffset? LastModified { get; set; }
    }
}