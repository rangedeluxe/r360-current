﻿using System.ServiceModel;

namespace Rx360.Contracts
{
    /// <summary>
    /// Client-side channel for <see cref="ISetupQuery"/>.
    /// </summary>
    public interface ISetupQueryChannel : ISetupQuery, IClientChannel
    {
    }
}