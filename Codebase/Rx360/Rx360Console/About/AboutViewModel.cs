﻿using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using Caliburn.Light;
using Rx360.Contracts;

namespace Rx360Console.About
{
    public class AboutViewModel : Screen
    {
        public AboutViewModel()
        {
            var copyrightAttribute = Assembly.GetEntryAssembly()
                .GetCustomAttributes(typeof(AssemblyCopyrightAttribute))
                .OfType<AssemblyCopyrightAttribute>()
                .FirstOrDefault();
            Copyright = copyrightAttribute?.Copyright ?? "";

            IconAuthorCommand = new DelegateCommand(() => Process.Start(IconAuthorUrl));
        }

        public string Copyright { get; }
        public string Description => Rx360Constants.ServiceDescription;
        public string IconAuthor => "Neurovit";
        public ICommand IconAuthorCommand { get; }
        public string IconAuthorUrl => "http://neurovit.deviantart.com/";
    }
}