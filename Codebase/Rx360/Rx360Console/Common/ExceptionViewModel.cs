﻿using System;
using Caliburn.Light;

namespace Rx360Console.Common
{
    public class ExceptionViewModel : BindableObject
    {
        private readonly Exception _exception;

        public ExceptionViewModel(Exception exception)
        {
            _exception = exception;
        }

        public string ClassNameSuffix => $"({_exception.GetType().Name})";
        public string Message => _exception.Message;
    }
}