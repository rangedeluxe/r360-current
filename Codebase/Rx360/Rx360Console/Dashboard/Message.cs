﻿using System;
using JetBrains.Annotations;

namespace Rx360Console.Dashboard
{
    public class Message
    {
        public Message([NotNull] string text, bool isError)
        {
            if (text == null)
                throw new ArgumentNullException(nameof(text));

            Text = text;
            IsError = isError;
        }

        public bool IsError { get; }
        public string Text { get; }
    }
}