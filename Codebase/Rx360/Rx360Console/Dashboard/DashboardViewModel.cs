﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using Caliburn.Light;
using Rx360.Contracts;
using Rx360Console.Common;

namespace Rx360Console.Dashboard
{
    public class DashboardViewModel : Screen
    {
        private readonly BindableCollection<INotifyPropertyChanged> _items =
            new BindableCollection<INotifyPropertyChanged>();

        public IEnumerable<INotifyPropertyChanged> Items => _items;

        private async void BeginLoad()
        {
            ISetupQueryChannel service = null;
            try
            {
                _items.Add(new LoadingViewModel());

                service = new ChannelFactory<ISetupQueryChannel>("SetupQuery").CreateChannel();
                await LoadAsync(service);
                if (service.State == CommunicationState.Opened)
                    service.Close();
            }
            catch (Exception ex)
            {
                service?.Abort();
                _items.Insert(0, new ExceptionViewModel(ex));
            }
            finally
            {
                _items.RemoveRange(_items.OfType<LoadingViewModel>().ToList());
            }
        }
        private Message GetWebInstallationMessage(WebInstallationStatus status)
        {
            switch (status)
            {
                case WebInstallationStatus.Success:
                    return null;
                case WebInstallationStatus.IisNotInstalled:
                    return new Message("IIS was not found.", isError: false);
                case WebInstallationStatus.NoPermissionToQueryIis:
                    return new Message(
                        "Could not retrieve information about web applications. Make sure the " +
                        $"\"{Rx360Constants.ServiceDisplayName}\" service is running with administrator credentials.",
                        isError: true);
                case WebInstallationStatus.R360SiteNotFound:
                    return new Message("No R360 web applications were found.", isError: false);
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }
        private async Task LoadAsync(ISetupQuery service)
        {
            if (!await TryPingAsync(service))
            {
                _items.Add(new DataGridSectionViewModel(
                    "Service not running",
                    new Message(
                        $"The \"{Rx360Constants.ServiceDisplayName}\" service is not running or is not responding.",
                        isError: true)));
                return;
            }

            var services = await service.GetWindowsServices();
            _items.Add(new DataGridSectionViewModel("Windows services", items: services));

            var webInstallation = await service.GetWebInstallation();
            _items.Add(new DataGridSectionViewModel("Web applications",
                GetWebInstallationMessage(webInstallation.Status), webInstallation.Applications));
        }
        protected override void OnInitialize()
        {
            base.OnInitialize();
            BeginLoad();
        }
        private async Task<bool> TryPingAsync(ISetupQuery service)
        {
            try
            {
                await service.Ping();
                return true;
            }
            catch (EndpointNotFoundException)
            {
                return false;
            }
        }
    }
}