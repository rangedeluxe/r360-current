﻿using System;
using System.Globalization;
using System.Windows.Data;
using Humanizer;

namespace Rx360Console.Dashboard
{
    public class LastModifiedHowLongAgoValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var dateValue = value as DateTimeOffset?;
            return dateValue.HasValue
                ? $" ({dateValue.Humanize()})"
                : "";
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}