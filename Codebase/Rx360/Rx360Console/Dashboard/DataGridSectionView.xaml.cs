﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Rx360.Contracts;

namespace Rx360Console.Dashboard
{
    public partial class DataGridSectionView
    {
        public DataGridSectionView()
        {
            InitializeComponent();
        }

        private void DataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            var propertyDescriptor = e.PropertyDescriptor as PropertyDescriptor;
            var hasLastModified =
                propertyDescriptor != null &&
                typeof(IHasLastModified).IsAssignableFrom(propertyDescriptor.ComponentType);
            if (hasLastModified && e.PropertyName == nameof(IHasLastModified.LastModified))
            {
                e.Column = new DataGridTemplateColumn
                {
                    Header = e.Column.Header,
                    CellTemplate = (DataTemplate) Resources["LastModifiedCellTemplate"],
                    SortMemberPath = e.Column.SortMemberPath,
                };
            }
        }
    }
}