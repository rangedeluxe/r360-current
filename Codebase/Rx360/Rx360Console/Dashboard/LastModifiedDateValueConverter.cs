﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Rx360Console.Dashboard
{
    public class LastModifiedDateValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value?.ToString() ?? "File not found";
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}