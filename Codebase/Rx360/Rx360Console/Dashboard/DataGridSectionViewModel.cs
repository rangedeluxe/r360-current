﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Caliburn.Light;
using JetBrains.Annotations;

namespace Rx360Console.Dashboard
{
    public class DataGridSectionViewModel : BindableObject
    {
        public DataGridSectionViewModel([NotNull] string headerText, [CanBeNull] Message message = null,
            [CanBeNull] IEnumerable<object> items = null)
        {
            if (headerText == null)
                throw new ArgumentNullException(nameof(headerText));

            HeaderText = headerText;
            Items = items?.ToList() ?? new List<object>();

            if (message != null)
            {
                MessageText = message.Text;
                if (message.IsError)
                    ErrorVisibility = Visibility.Visible;
                else
                    MessageVisibility = Visibility.Visible;
            }
        }

        [NotNull]
        public string HeaderText { get; }
        public Visibility ErrorVisibility { get; } = Visibility.Collapsed;
        public Visibility MessageVisibility { get; } = Visibility.Collapsed;
        [NotNull]
        public string MessageText { get; } = "";
        public Visibility DataGridVisibility => Items.Any() ? Visibility.Visible : Visibility.Collapsed;
        [NotNull]
        public IReadOnlyList<object> Items { get; }
    }
}