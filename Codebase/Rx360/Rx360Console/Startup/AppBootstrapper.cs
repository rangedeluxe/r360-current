﻿using System.Windows;
using Caliburn.Light;
using Rx360Console.About;
using Rx360Console.Common;
using Rx360Console.Dashboard;

namespace Rx360Console.Startup
{
    public class AppBootstrapper : BootstrapperBase
    {
        private SimpleContainer _container;

        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            LogManager.Initialize(type => new DebugLogger(type));
            _container = new SimpleContainer();
            IoC.Initialize(_container);

            _container.RegisterSingleton<IWindowManager, WindowManager>();
            _container.RegisterSingleton<IEventAggregator, EventAggregator>();
            _container.RegisterSingleton<IViewModelLocator, ViewModelLocator>();
            _container.RegisterSingleton<IViewModelBinder, ViewModelBinder>();

            var typeResolver = new ViewModelTypeResolver();
            _container.RegisterInstance<IViewModelTypeResolver>(typeResolver);

            _container.RegisterPerRequest<ShellViewModel>();
            typeResolver.AddMapping<ShellView, ShellViewModel>();
            typeResolver.AddMapping<AboutView, AboutViewModel>();
            typeResolver.AddMapping<DashboardView, DashboardViewModel>();
            typeResolver.AddMapping<DataGridSectionView, DataGridSectionViewModel>();
            typeResolver.AddMapping<ExceptionView, ExceptionViewModel>();
            typeResolver.AddMapping<LoadingView, LoadingViewModel>();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }
    }
}