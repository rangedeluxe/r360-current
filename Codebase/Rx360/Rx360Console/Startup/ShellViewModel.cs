﻿using System.Windows.Input;
using Caliburn.Light;
using JetBrains.Annotations;
using Rx360Console.About;
using Rx360Console.Dashboard;

namespace Rx360Console.Startup
{
    [UsedImplicitly]
    public class ShellViewModel : Conductor<Screen>.Collection.OneActive, IHaveDisplayName
    {
        public string DisplayName => "Rx360";

        public ShellViewModel()
        {
            AboutCommand = new DelegateCommand(LoadAbout);
            ReloadDashboardCommand = new DelegateCommand(LoadDashboard);
        }

        public ICommand AboutCommand { get; }
        public ICommand ReloadDashboardCommand { get; }

        private void LoadAbout()
        {
            ActivateItem(new AboutViewModel());
        }
        private void LoadDashboard()
        {
            ActivateItem(new DashboardViewModel());
        }
        protected override void OnInitialize()
        {
            base.OnInitialize();
            LoadDashboard();
        }
    }
}