﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rx360Service.Commands;

namespace Rx360Service.Tests
{
    [TestClass]
    public class CommandLineSplitterTests
    {
        [TestMethod]
        public void SimplePath()
        {
            const string input = @"C:\Test\Foo.exe";

            var result = CommandLineSplitter.Split(input);

            Assert.AreEqual(1, result.Count, "Count");
            Assert.AreEqual(@"C:\Test\Foo.exe", result[0]);
        }
        [TestMethod]
        public void QuotedPath()
        {
            const string input = @"""C:\Path With Space\Foo.exe""";

            var result = CommandLineSplitter.Split(input);

            Assert.AreEqual(1, result.Count, "Count");
            Assert.AreEqual(@"C:\Path With Space\Foo.exe", result[0]);
        }
        [TestMethod]
        public void SpaceDelimited()
        {
            const string input = @"C:\Test\Foo.exe -a -b";

            var result = CommandLineSplitter.Split(input);

            Assert.AreEqual(3, result.Count, "Count");
            Assert.AreEqual(@"C:\Test\Foo.exe", result[0], "result[0]");
            Assert.AreEqual(@"-a", result[1], "result[1]");
            Assert.AreEqual(@"-b", result[2], "result[2]");
        }
        [TestMethod]
        public void QuotedPlusArguments()
        {
            const string input = @"""C:\Path With Space\Foo.exe"" -a ""abc def"" -b";

            var result = CommandLineSplitter.Split(input);

            Assert.AreEqual(4, result.Count, "Count");
            Assert.AreEqual(@"C:\Path With Space\Foo.exe", result[0], "result[0]");
            Assert.AreEqual(@"-a", result[1], "result[1]");
            Assert.AreEqual(@"abc def", result[2], "result[2]");
            Assert.AreEqual(@"-b", result[3], "result[3]");
        }
        [TestMethod]
        public void EscapedQuote()
        {
            // C:\Test.Foo.exe "this is \"quoted\""
            const string input = @"C:\Test\Foo.exe ""this is \""quoted\""""";

            var result = CommandLineSplitter.Split(input);

            Assert.AreEqual(2, result.Count, "Count");
            Assert.AreEqual(@"C:\Test\Foo.exe", result[0], "result[0]");
            Assert.AreEqual(@"this is ""quoted""", result[1], "result[1]");
        }
        [TestMethod]
        public void SwitchPlusQuotedValue_StripsQuotes()
        {
            const string input = @"C:\Test\Foo.exe -d:""a b c""";

            var result = CommandLineSplitter.Split(input);

            Assert.AreEqual(2, result.Count, "Count");
            Assert.AreEqual(@"C:\Test\Foo.exe", result[0], "result[0]");
            Assert.AreEqual(@"-d:a b c", result[1], "result[1]");
        }
    }
}
