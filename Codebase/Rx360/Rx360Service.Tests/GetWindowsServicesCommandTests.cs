﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rx360Service.Commands;

namespace Rx360Service.Tests
{
    [TestClass]
    public class GetWindowsServicesCommandTests
    {
        [TestMethod]
        public void GetServiceBinaryPaths_SimpleExePath()
        {
            const string commandLine = @"C:\Temp\Foo.exe";

            var results = GetWindowsServicesCommand.GetServiceBinaryPaths(commandLine);

            Assert.AreEqual(@"C:\Temp\Foo.exe", results.ExePath, "ExePath");
            Assert.AreEqual(@"", results.DllPath, "DllPath");
        }
        [TestMethod]
        public void GetServiceBinaryPaths_QuotedExePath()
        {
            const string commandLine = @"""C:\Temp\Foo.exe""";

            var results = GetWindowsServicesCommand.GetServiceBinaryPaths(commandLine);

            Assert.AreEqual(@"C:\Temp\Foo.exe", results.ExePath, "ExePath");
            Assert.AreEqual(@"", results.DllPath, "DllPath");
        }
        [TestMethod]
        public void GetServiceBinaryPaths_WcfServiceHost_WithAllArguments()
        {
            const string commandLine =
                @"""E:\WFSApps\RecHub\bin2\WCFServiceHost.exe"" " +
                @"-displayname ""WFS Data Import Service"" " +
                @"-servicename ""DataImportSvc"" " +
                @"-d:""E:\WFSApps\RecHub\wwwroot\DataImportServices\bin\DataImportServices.dll"" " +
                @"-u:https://RAAMQANoIIs.qalabs.nwk/DataImportServices/DataImportService.svc " +
                @"-c:DataImportService";

            var results = GetWindowsServicesCommand.GetServiceBinaryPaths(commandLine);

            Assert.AreEqual(@"E:\WFSApps\RecHub\bin2\WCFServiceHost.exe", results.ExePath, "ExePath");
            Assert.AreEqual(@"E:\WFSApps\RecHub\wwwroot\DataImportServices\bin\DataImportServices.dll",
                results.DllPath, "DllPath");
        }
        [TestMethod]
        public void GetServiceBinaryPaths_DllPathIgnoredIfNotWcfServiceHost()
        {
            const string commandLine =
                @"""C:\Temp\Foo.exe"" " +
                @"-displayname ""WFS Data Import Service"" " +
                @"-servicename ""DataImportSvc"" " +
                @"-d:""E:\WFSApps\RecHub\wwwroot\DataImportServices\bin\DataImportServices.dll"" " +
                @"-u:https://RAAMQANoIIs.qalabs.nwk/DataImportServices/DataImportService.svc " +
                @"-c:DataImportService";

            var results = GetWindowsServicesCommand.GetServiceBinaryPaths(commandLine);

            Assert.AreEqual(@"C:\Temp\Foo.exe", results.ExePath, "ExePath");
            Assert.AreEqual(@"", results.DllPath, "DllPath");
        }
    }
}