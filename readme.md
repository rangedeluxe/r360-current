# Notes on this repository

## JenkinsFile location
Jenkinsfiles can be found in Codebase/BuildSupport/JenkinsPipeline

## GroovyScripts location
Files can be found in Codebase/BuildSupport/GroovyScripts

## Powershell Scripts location - files can be found in Codebase/BuildSupport/PowershellScripts
GitDatabaseBuildV3.ps1 - build the (R360?) database
GitPublishApp.ps1/GitPublishApp_Pipeline.ps1 - assembles build artifacts and publishes to JFrog Artifactory

## Prerequisites not supplied from the r360-current repo

### General Prerequisites
Git

### Build Prerequisites
Visual Studio 2017 (for msbuild, used in buildDeploymentUtilities, buildExtractWizard, buildHubSolution, buildHubViews, buildOLDecisioningServices, buildR360UI)
.Net Core for "dotnet build" command in buildDitContainer.groovy, not sure what version though, maybe v4.0.30319 like the one used for ILMerge since this is all running on the same build node (I think) (used in buildDitContainer, GitDatabaseBuildV3,)
nuget.exe, version?? - currently in D:\Tools\ on the build node (used in buildExtractWizard, buildHubSolution, buildHubViews, buildOLDecisioningServices, buildR360UI)
npm, version?? - I think this is currently on the build node as it is just being pulled from the PATH (used in buildR360UI,)


### Test Prerequisites
MSTest 2017 ( included with VS 2017, used in buildDeploymentUtilities, buildDitContainer, buildExtractWizard)

### Packaging Prerequisites
ILMerge v2.12.080 and supporting .Net Framework, currently v4.0.30319 I think (needed for PublishToArtifactory in buildDeploymentUtilities,) - currently in D:\Tools\ on the build node
sn.exe, version??- currently in D:\Tools\ on the build node

### Installer Prerequisites
jfrog.exe (version??) for zipping the release in r360-current\Support\JenkinsScripts\packageRelease.ps1, I think this is currently on the build node as it is just being pulled from the PATH